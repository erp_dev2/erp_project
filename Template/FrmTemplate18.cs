﻿#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmTemplate18 : RunSystem.FrmBase18
    {
        public FrmTemplate18()
        {
            InitializeComponent();
        }

        #region Method

        #region From Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmTemplate18_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #endregion
    }
}
