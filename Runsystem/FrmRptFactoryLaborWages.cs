﻿#region Update
/*
    29/08/2020 [TKG/IOK] tambah sum untuk cek data
*/
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptFactoryLaborWages : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mEmpSystemTypeBorongan = string.Empty;

        private bool mIsNotFilterByAuthorization = false;

        #endregion

        #region Constructor

        public FrmRptFactoryLaborWages(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

            #region Standard Methods

            override protected void FrmLoad(object sender, EventArgs e)
            {
                try
                {
                    mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                    Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                    Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                    GetParameter();
                    SetGrd();
                    Sl.SetLueDeptCode(ref LueDeptCode);
                    base.FrmLoad(sender, e);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            private void GetParameter() 
            {
                mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
                mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            }

            private void SetGrd()
            {
                Grd1.Cols.Count = 13;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Payrun",
                        "End Date",
                        "Code", 
                        "Old Code",
                        "Employee's Name",
                        
                        //6-10
                        "Department",
                        "Position",
                        "Work Center",
                        "Duration",
                        "Wages/Hour",

                        //11-12
                        "Penalty/Hour",
                        "Incentive/Hour",
                    },
                        new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 80, 120, 120, 200, 
                        
                        //6-10
                        200, 200, 200, 100, 120, 
                        
                        //11-12
                        120, 120
                    }
                    );
                Sm.GrdFormatDate(Grd1, new int[] { 2 });
                Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12 }, 2);
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4, 7, 8, 9 }, false);
                Sm.SetGrdProperty(Grd1, false);
            }

            override protected void HideInfoInGrd()
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4, 7, 8 }, !ChkHideInfoInGrd.Checked);
            }

            override protected void ShowData()
            {
                Sm.ClearGrd(Grd1, false);
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                    ) return;

                var lSFC = new List<SFC>();
                var lSFCProcess1 = new List<SFCProcess1>();
                var lSFCProcess2 = new List<SFCProcess2>();
                var lResult = new List<Result>();

                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    Process1(ref lSFC);
                    if (lSFC.Count == 0)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                    {
                        Process2(ref lSFC, ref lSFCProcess1);
                        Process3(ref lSFCProcess1, ref lSFCProcess2);
                        Process4(ref lResult);
                        Process5(ref lSFCProcess2, ref lResult);
                        if (lResult.Count == 0)
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                        else
                            Process6(ref lResult);
                    }
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
                finally
                {
                    lSFC.Clear();
                    lSFCProcess1.Clear();
                    lSFCProcess2.Clear();;
                    lResult.Clear();
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }

            #endregion

            #region Grid Method

            override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
            {
                Sm.SetGridNo(Grd1, 0, 1, true);
            }

            override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
            {
            }

            #endregion

            #region Misc Control Method

            override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
            {
                if (Sm.GetLue(LueFontSize).Length != 0)
                {
                    Grd1.Font = new Font(
                        Grd1.Font.FontFamily.Name.ToString(),
                        int.Parse(Sm.GetLue(LueFontSize))
                        );
                }
            }

            #endregion

            #region Additional Method

            private void Process1(ref List<SFC> l)
            {
                l.Clear();

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SystemType", mEmpSystemTypeBorongan);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T5.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T5.EmpCode", "T5.EmpCodeOld", "T5.EmpName" });

                SQL.AppendLine("Select T1.PayrunCode, T2.EmpCode, T2.Dt, T4.Qty ");
                SQL.AppendLine("From TblPayrun T1 ");
                SQL.AppendLine("Inner Join TblPayrollProcess2 T2 On T1.PayrunCode=T2.PayrunCode ");
                SQL.AppendLine("Inner Join TblShopFloorControlHdr T3 On T3.CancelInd='N' And T2.Dt=T3.DocDt ");
                SQL.AppendLine("Inner Join TblShopFloorControl2Dtl T4 On T3.DocNo=T4.DocNo And T2.EmpCode=T4.BomCode ");
                SQL.AppendLine("Inner Join TblEmployee T5 On T2.EmpCode=T5.EmpCode " + Filter);
                SQL.AppendLine("Where T1.CancelInd='N' ");
                SQL.AppendLine("And T1.SystemType=@SystemType ");
                SQL.AppendLine("And T1.EndDt Between @DocDt1 And @DocDt2 ");
                if (!mIsNotFilterByAuthorization)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblEmployee ");
                    SQL.AppendLine("    Where EmpCode=T5.EmpCode ");
                    SQL.AppendLine("    And GrdLvlCode In ( ");
                    SQL.AppendLine("        Select T2.GrdLvlCode ");
                    SQL.AppendLine("        From TblPPAHdr T1 ");
                    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By T1.PayrunCode, T2.EmpCode, T2.Dt;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "PayrunCode", 
                        "EmpCode", "Dt", "Qty" 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new SFC()
                            {
                                PayrunCode = Sm.DrStr(dr, c[0]),
                                EmpCode = Sm.DrStr(dr, c[1]),
                                Dt = Sm.DrStr(dr, c[2]),
                                Hr = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }

            private void Process2(ref List<SFC> l1, ref List<SFCProcess1> l2)
            {
                string 
                    PayrunCode = string.Empty,
                    EmpCode = string.Empty,
                    Dt = string.Empty;
                int Index = -1;

                for (var i = 0; i < l1.Count; i++)
                {
                    if (
                        string.Compare(l1[i].PayrunCode, PayrunCode) == 0 &&
                        string.Compare(l1[i].EmpCode, EmpCode) == 0 &&
                        string.Compare(l1[i].Dt, Dt) == 0)
                    {
                        l2[Index].HourCount += l1[i].Hr;
                        //l2[Index].RecordCount += 1;
                    }
                    else
                    {
                        Index+=1;
                        l2.Add(new SFCProcess1()
                        {
                            PayrunCode = l1[i].PayrunCode,
                            EmpCode = l1[i].EmpCode,
                            Dt = l1[i].Dt,
                            HourCount = l1[i].Hr,
                            //RecordCount = 1
                        });
                        PayrunCode = l1[i].PayrunCode;
                        EmpCode = l1[i].EmpCode;
                        Dt = l1[i].Dt;
                    }
                }
            }

            private void Process3(ref List<SFCProcess1> l1, ref List<SFCProcess2> l2)
            {
                string
                    PayrunCode = string.Empty,
                    EmpCode = string.Empty;
                int Index = -1;

                for (var i = 0; i < l1.Count; i++)
                {
                    if (
                        string.Compare(l1[i].PayrunCode, PayrunCode) == 0 &&
                        string.Compare(l1[i].EmpCode, EmpCode) == 0)
                    {
                        l2[Index].WorkingDuration += l1[i].HourCount; // (l1[i].HourCount / l1[i].RecordCount);
                    }
                    else
                    {
                        Index += 1;
                        l2.Add(new SFCProcess2()
                        {
                            PayrunCode = l1[i].PayrunCode,
                            EmpCode = l1[i].EmpCode,
                            WorkingDuration = l1[i].HourCount ///l1[i].RecordCount
                        });
                        PayrunCode = l1[i].PayrunCode;
                        EmpCode = l1[i].EmpCode;
                    }
                }
            }

            private void Process4(ref List<Result> l)
            {
                l.Clear();

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SystemType", mEmpSystemTypeBorongan);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpCodeOld", "B.EmpName" });

                SQL.AppendLine("Select A.PayrunCode, C.EndDt, A.EmpCode, B.EmpCodeOld, B.EmpName, D.DeptName, E.PosName, ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T3.DocName ");
                SQL.AppendLine("    From TblPWGHdr T1 ");
                SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblWorkCenterHdr T3 On T1.WorkCenterDocNo=T3.DocNo ");
                SQL.AppendLine("    Where T1.CancelInd='N' And T2.EmpCode=A.EmpCode And T1.DocDt<=C.EndDt ");
                SQL.AppendLine("    Order By T1.DocDt Desc Limit 1 ");
                SQL.AppendLine(") As WorkCenter, ");
                SQL.AppendLine("A.Salary As WagesPerHr, ");
                SQL.AppendLine("A.DedProduction As PenaltyPerHr, ");
                SQL.AppendLine("A.IncEmployee As IncentivePerHr ");
                SQL.AppendLine("From TblPayrollProcess1 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode " + Filter);
                SQL.AppendLine("Inner Join TblPayrun C ");
                SQL.AppendLine("    On A.PayrunCode=C.PayrunCode ");
                SQL.AppendLine("    And C.CancelInd='N' ");
                SQL.AppendLine("    And C.SystemType=@SystemType ");
                SQL.AppendLine("    And C.EndDt In ");
                SQL.AppendLine("      (Select EndDt From TblPayrun ");
                SQL.AppendLine("      Where CancelInd='N' And EndDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine("Left Join TblPosition E On B.PosCode=E.PosCode ");
                if (!mIsNotFilterByAuthorization)
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblEmployee ");
                    SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                    SQL.AppendLine("    And GrdLvlCode In ( ");
                    SQL.AppendLine("        Select T2.GrdLvlCode ");
                    SQL.AppendLine("        From TblPPAHdr T1 ");
                    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By A.PayrunCode, A.EmpCode;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "PayrunCode", 

                        //1-5
                        "EndDt", "EmpCode", "EmpCodeOld", "EmpName", "DeptName", 
                        
                        //6-10
                        "PosName", "WorkCenter", "WagesPerHr", "PenaltyPerHr", "IncentivePerHr" 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Result()
                            {
                                PayrunCode = Sm.DrStr(dr, c[0]),
                                EndDt = Sm.DrStr(dr, c[1]),
                                EmpCode = Sm.DrStr(dr, c[2]),
                                EmpCodeOld = Sm.DrStr(dr, c[3]),
                                EmpName = Sm.DrStr(dr, c[4]),
                                DeptName = Sm.DrStr(dr, c[5]),
                                PosName = Sm.DrStr(dr, c[6]),
                                WorkCenter = Sm.DrStr(dr, c[7]),
                                WorkingDuration = 0m,
                                WagesPerHour = Sm.DrDec(dr, c[8]),
                                PenaltyPerHour = Sm.DrDec(dr, c[9]),
                                IncentivePerHour = Sm.DrDec(dr, c[10])
                            });
                        }
                    }
                    dr.Close();
                }
            }

            private void Process5(ref List<SFCProcess2> l1, ref List<Result> l2) 
            {
                int Temp = 0;
                for (var i = 0; i < l1.Count; i++)
                {
                    for (var j = Temp; j < l2.Count; j++)
                    {
                        if (
                            string.Compare(l1[i].PayrunCode, l2[j].PayrunCode) == 0 &&
                            string.Compare(l1[i].EmpCode, l2[j].EmpCode) == 0)
                        {
                            l2[j].WorkingDuration = l1[i].WorkingDuration;
                            if (l2[j].WorkingDuration != 0m)
                            {
                                l2[j].WagesPerHour /= l2[j].WorkingDuration;
                                l2[j].PenaltyPerHour /= l2[j].WorkingDuration;
                                l2[j].IncentivePerHour /= l2[j].WorkingDuration;
                            }
                            Temp = j;
                            break;
                        }
                    }
                }
                l2.ForEach(x => {
                    if (x.WorkingDuration == 0)
                    {
                        x.WagesPerHour = 0m;
                        x.PenaltyPerHour = 0m;
                        x.IncentivePerHour = 0m;
                    }
                });
            }

            private void Process6(ref List<Result> l)
            {
                l.Sort(
                    delegate(Result r1a, Result r1b)
                    {
                        int f = r1a.EmpName.CompareTo(r1b.EmpName);
                        return f != 0 ? f : r1a.EmpCode.CompareTo(r1b.EmpCode);
                    });
                iGRow r;
                Grd1.BeginUpdate();
                for (var i = 0; i < l.Count; i++)
                {
                    r = Grd1.Rows.Add();
                    r.Cells[0].Value = i + 1;
                    r.Cells[1].Value = l[i].PayrunCode;
                    r.Cells[2].Value = Sm.ConvertDate(l[i].EndDt);
                    r.Cells[3].Value = l[i].EmpCode;
                    r.Cells[4].Value = l[i].EmpCodeOld;
                    r.Cells[5].Value = l[i].EmpName;
                    r.Cells[6].Value = l[i].DeptName;
                    r.Cells[7].Value = l[i].PosName;
                    r.Cells[8].Value = l[i].WorkCenter;
                    r.Cells[9].Value = l[i].WorkingDuration;
                    r.Cells[10].Value = l[i].WagesPerHour;
                    r.Cells[11].Value = l[i].PenaltyPerHour;
                    r.Cells[12].Value = l[i].IncentivePerHour;
                }
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 12 });
                Grd1.EndUpdate();
            }

            #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string PayrunCode { get; set; }
            public string EndDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string WorkCenter { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal WagesPerHour { get; set; }
            public decimal PenaltyPerHour { get; set; }
            public decimal IncentivePerHour { get; set; }
        }

        private class SFC
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal Hr { get; set; }
        }

        private class SFCProcess1
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public decimal HourCount { get; set; }
            public decimal RecordCount { get; set; }
        }

        private class SFCProcess2
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public decimal WorkingDuration { get; set; }
        }

        #endregion
    }
}
