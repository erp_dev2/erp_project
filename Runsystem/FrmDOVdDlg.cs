﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOVdDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOVd mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty, mWhsCode = string.Empty;
        
        #endregion
            
        #region Constructor

        public FrmDOVdDlg(FrmDOVd FrmParent, string VdCode, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo", 
                        "Date",
                        "Vendor's"+Environment.NewLine+"DO#",

                        //6-10
                        "Item Code", 
                        "", 
                        "Item Name",
                        "Batch#",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin",
                        "Outstanding"+Environment.NewLine+"Quantity 1",
                        "Uom 1",
                        "Outstanding"+Environment.NewLine+"Quantity 2",
                        
                        //16-20
                        "Uom 2",
                        "Outstanding"+Environment.NewLine+"Quantity 3",
                        "Uom 3",
                        "Currency",
                        "Price",

                        //21-22
                        "Group",
                        "Local#"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[21].Visible = true;
                Grd1.Cols[21].Move(8);
            }
            Grd1.Cols[22].Move(4);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 22 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select B.DocNo, Right(A.Source, 3) As DNo, B.DocDt, B.VdDONo, ");
            SQL.AppendLine("    A.ItCode, C.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("    A.Qty, C.InventoryUomCode, A.Qty2, C.InventoryUomCode2, A.Qty3, C.InventoryUomCode3, D.CurCode, D.UPrice, C.ItGrpCode, B.LocalDocNo ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Inner Join TblRecvVdHdr B On Substring(A.Source, 4, Length(A.Source)-7)=B.DocNo And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("    Left Join TblStockPrice D On A.Source=D.Source ");
            SQL.AppendLine("    Where A.WhsCode=@WhsCode And (A.Qty>0 Or A.Qty2>0 Or A.Qty3>0) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "B.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "B.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + 
                        ") T Where Concat(T.DocNo, T.DNo) Not In (" + mFrmParent.GetSelectedRecvVd() + 
                        ")  Order By T.ItCode, T.DocNo",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "VdDONo", "ItCode", "ItName",  
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty",  
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",

                            //16-19
                            "CurCode", "UPrice", "ItGrpCode", "LocalDocNo"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 22);

                        mFrmParent.Grd1.Cells[Row1, 0].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 1].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 2].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 15].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 18].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 21].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 23].Value = null;

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 25 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
