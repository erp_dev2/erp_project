﻿#region Update
/*
    16/02/2022 [TKG/GSS] Merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmSO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmSOFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmSO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd1();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid
        private void SetGrd1()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Agent",
                        "Agent",
                        "Item",
                        "Item's Name",

                        //6-10
                        "Quantity",
                        "Sales UoM",
                        "Unit Price",
                        "Discount"+Environment.NewLine+"Rate 1",
                        "Discount"+Environment.NewLine+"Rate 2",
                        //11-15
                        "Discount"+Environment.NewLine+"Rate 3",
                        "Discount"+Environment.NewLine+"Rate 4",
                        "Discount"+Environment.NewLine+"Rate 5",
                        "Promo"+Environment.NewLine+"Rate",
                        "Unit Price"+Environment.NewLine+"Before Tax",
                        //16-21
                        "Tax Rate",
                        "Tax Amount",
                        "Unit Price"+Environment.NewLine+"After Tax",
                        "Total",
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        40,
                        20, 80, 100, 80, 100, 
                        80, 80, 100, 80, 80, 
                        80, 80, 80, 80, 100, 
                        80, 100, 100, 100, 100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6,  });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6  }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtStatus, DteDocDt, TxtAmt, TxtPONumb, TxtSOQuot,
                        TxtSOQuotPromo, LueCtCode, LueContactCode, MeeRemark,  ChkCancelInd
                    }, true);
                    Grd1.ReadOnly = true; ChkCancelInd.Properties.ReadOnly = true;
                    BtnSoQuotPromoDlg.Enabled = BtnSOQuotPromoMaster.Enabled = false;
                    BtnContact.Enabled = BtnSOMaster.Enabled =  false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         DteDocDt, LueCtCode, LueContactCode, TxtPONumb,
                         MeeRemark
                    }, false);
                    Grd1.ReadOnly = false; ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus(); TxtStatus.Text = "Outstanding";
                    BtnSoQuotPromoDlg.Enabled = BtnSOQuotPromoMaster.Enabled = true;
                    BtnContact.Enabled = BtnSOMaster.Enabled = true;
                    TxtStatus.Properties.ReadOnly = true;
                    BtnContact.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtStatus,DteDocDt, TxtAmt, TxtPONumb, TxtSOQuot,
                        TxtSOQuotPromo, LueCtCode, LueContactCode, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = false;
                    BtnSoQuotPromoDlg.Enabled = false;
                    BtnSOQuotPromoMaster.Enabled = true;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, TxtStatus,DteDocDt, TxtAmt, TxtPONumb, TxtSOQuot,
                  TxtSOQuotPromo, LueCtCode, LueContactCode, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                  TxtAmt
            }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #region Setlue 
       
        public  static void SetLue1(ref LookUpEdit Lue, string SQL, string ColTitle)
        {
            var ListOfObject = new List<Lue1>()
                {
                    new Lue1{Col1 = null},
                    new Lue1{Col1 = "<Refresh>"}
                };
            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var dr = new MySqlCommand(SQL, cn).ExecuteReader();
                    var c = new int[] { dr.GetOrdinal("Col1") };
                    while (dr.Read())
                        ListOfObject.Add(new Lue1 { Col1 = Sm.DrStr(dr, c[0]) });
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.SetLookUpEdit(Lue, ListOfObject);
                Lue.Properties.Columns["Col1"].Caption = ColTitle;
                Lue.Properties.DisplayMember = "Col1";
                Lue.Properties.ValueMember = "Col1";
            }
        }

        public static void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            SetLue1(
                ref Lue,  
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOHdr Where DocNo= '" + DocNo + "' Order By CtContactPersonName",
                "Contact Person Name");
        }
        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method 
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtSOQuot, "SO Quotation", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSODlg2(this, TxtSOQuot.Text, TxtSOQuotPromo.Text, Sm.GetGrdStr(Grd1, e.RowIndex, 2)));
            }
         }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtSOQuot, "SO Quotation", false))
                Sm.FormShowDialog(new FrmSODlg2(this, TxtSOQuot.Text, TxtSOQuotPromo.Text, Sm.GetGrdStr(Grd1, e.RowIndex, 2)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 19) ComputeTotal();
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SO", "TblSOHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOHdr(DocNo));
            cml.Add(SaveSODtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
            //        cml.Add(SaveSODtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtStatus, "Status", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueContactCode, "Contact Person") ||
                Sm.IsTxtEmpty(TxtSOQuot, "SO Quotation", false) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private string GenerateDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='SO'");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSOHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 5) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }


        private MySqlCommand SaveSOHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOHdr(DocNo, DocDt, Status, CtCode, CtContactPersonName, CtPONo, SOQuotPromoDocNo, SOQuotDocNo, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Status, @CtCode, @CtContactPersonName, @CtPONo, @SOQuotPromoDocNo, @SOQuotDocNo, @Amt, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", "O");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", LueContactCode.Text);
            Sm.CmParam<String>(ref cm, "@CtPONo", TxtPONumb.Text);
            Sm.CmParam<String>(ref cm, "@SOQuotPromoDocNo", TxtSOQuotPromo.Text);
            Sm.CmParam<String>(ref cm, "@SOQuotDocNo", TxtSOQuot.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSODtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Order (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSODtl(DocNo, DNo, AgtCode, ItCode, Qty, UPrice, TaxRate, DeliveryDt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AgtCode_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @TaxRate_" + r.ToString() +
                        ", @DeliveryDt_" + r.ToString() +
                        ", @Remark_" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AgtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
                    Sm.CmParamDt(ref cm, "@DeliveryDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 20));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 21));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        //private MySqlCommand SaveSODtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //        "Insert Into TblSODtl(DocNo, DNo, AgtCode, ItCode, Qty, UPrice, TaxRate, DeliveryDt, Remark, CreateBy, CreateDt) " +
        //        "Values(@DocNo, @DNo, @AgtCode, @ItCode, @Qty, @UPrice, @TaxRate, @DeliveryDt, @Remark, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 16));
        //    Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd1, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateSOHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                IsSONotCancelled() ||
                IsSOAlreadyCancelled() ||
                IsSOAlreadyProcessedToDR();
        }

        private MySqlCommand UpdateSOHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOHdr Set ");
            SQL.AppendLine("    Status=@Status, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Status", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsSONotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this sales order.");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyCancelled()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =  
                    "Select DocNo From TblSOHdr " +
                    "Where DocNo=@DocNo And Status = 'Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is cancelled already.");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyProcessedToDR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From TblDrHdr A ");
            SQL.AppendLine("Inner Join TblDRDtl B On A.DocNo=B.DocNo And B.SODocNo=@DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N'; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already processed to delivery request.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOHdr(DocNo);
                ShowSODtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Status, " +
                    "CtCode, CtContactPersonName, CtPONo, SOQuotPromoDocNo, SOQuotDocNo, Amt, Remark " +
                    "From TblSOHdr Where DocNo=@DocNo ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Status", "CtCode", "CtContactPersonName",
                        "CtPONo", "SOQuotPromoDocNo", "SOQuotDocNo", "Amt", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        String StatusInd = Sm.DrStr(dr, c[2]);
                        if (StatusInd == "O")
                        {
                            TxtStatus.EditValue = "OutStanding";
                        }
                        else if (StatusInd == "Y")
                        {
                            ChkCancelInd.Checked = true;
                            TxtStatus.EditValue = "Cancel";
                        }
                        else
                        {
                            TxtStatus.EditValue = "FullFilled";
                        }

                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        SetLueCtPersonCodeShow(ref LueContactCode, Sm.DrStr(dr, c[0]));
                        Sm.SetLue(LueContactCode, Sm.GetValue("Select CtContactPersonName From TblSOHdr Where DocNo= '" + Sm.DrStr(dr, c[0]) + "' "));
                        TxtPONumb.EditValue = Sm.DrStr(dr, c[5]);
                        TxtSOQuotPromo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtSOQuot.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    }, true
                );
        }

        private void ShowSODtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select B.DNo, " +
                    "D.AgtCode, D.AgtName, B.ItCode, C.ItName, B.Qty, C.SalesUomCode, G.DiscRt1, G.DiscRt2, G.DiscRt3, G.DiscRt4, G.DiscRt5, " +
                    "IfNull(F.DiscRate, 0) As DiscRate, B.UPrice, B.TaxRate, B.DeliveryDt, B.Remark  " +
                    "From TblSOHdr A " +
                    "Inner Join TblSODtl B On A.DocNo = B.DocNo " +
                    "Inner Join TblItem C On B.ItCode = C.ItCode  " +
                    "Left Join TblAgent D On B.AgtCode = D.AgtCode " +
                    "left Join TblSoQuotPromoAgt E On A.SOQuotPromoDocNo = E.DocNo And B.AgtCode=E.AgtCode " +
                    "Left Join TblSoQuotPromoItem F " +
                    "On A.SOQuotPromoDocNo = F.DocNo And B.ItCode = F.ItCode " +
                    "And Exists(Select DocNo From TblSoQuotPromoAgt Where DocNo=A.SOQuotPromoDocNo And AgtCode=B.AgtCode Limit 1) " +
                    "Inner Join TblSoQuotItem G On B.ItCode = G.ItCode " +
                    "Inner Join TblSOQuot H On G.DocNo = H.DocNo " +
                    "Where A.DocNo = @DocNo  And H.ActInd = 'Y' " +
                    "Group By A.DocNo, A.DocDt, A.CtContactPersonName, D.AgtName, C.ItName ",
                    new string[] 
                    { 
                        //0
                        "DNo", 
                        //1-5
                        "AgtCode", "AgtName", "ItCode", "ItName", "Qty",
                        //6-10
                        "SalesUomCode", "DiscRt1", "DiscRt2", "DiscRt3", "DiscRt4",
                        //11-15
                        "DiscRt5", "DiscRate", "UPrice", "TaxRate", "DeliveryDt",
                        //16
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);

                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 13);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 20, 15);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 16);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
                Sm.FocusGrd(Grd1, 0, 1);
                ComputeOnTheFly();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        internal void ComputeTotal()
        {
            decimal Amt = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                Amt += Sm.GetGrdDec(Grd1, Row, 19);

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeOnTheFly()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                decimal disc1, disc2, disc3, disc4, disc5, promo = 0m;
                decimal t1, t2, t3, t4, t5, t6 = 0m;
                decimal up, UPAT, Qty, tax, TAmt = 0m;

                up = Sm.GetGrdDec(Grd1, Row, 8);
                disc1 = Sm.GetGrdDec(Grd1, Row, 9);
                disc2 = Sm.GetGrdDec(Grd1, Row, 10);
                disc3 = Sm.GetGrdDec(Grd1, Row, 11);
                disc4 = Sm.GetGrdDec(Grd1, Row, 12);
                disc5 = Sm.GetGrdDec(Grd1, Row, 13);
                promo = Sm.GetGrdDec(Grd1, Row, 14);

                Qty = Sm.GetGrdDec(Grd1, Row, 6);

                t1 = up - ((up * disc1) / 100);
                t2 = t1 - ((t1 * disc2) / 100);
                t3 = t2 - ((t2 * disc3) / 100);
                t4 = t3 - ((t3 * disc4) / 100);
                t5 = t4 - ((t4 * disc5) / 100);
                t6 = t5 - ((t5 * promo) / 100);

                //price before tax
                Grd1.Rows[Row].Cells[15].Value = Sm.FormatNum(t6, 0);
                //nominal tax
                tax = Sm.GetGrdDec(Grd1, Row, 16);
                TAmt = (t6 * tax) / 100;
                Grd1.Rows[Row].Cells[17].Value = Sm.FormatNum(TAmt, 0);
                //price after tax
                Grd1.Rows[Row].Cells[18].Value = Sm.FormatNum(t6 + TAmt, 0);
                //total
                UPAT = Sm.GetGrdDec(Grd1, Row, 18);
                Grd1.Rows[Row].Cells[19].Value = Sm.FormatNum(UPAT * Qty, 0);

            }
        }

        

        #endregion

        #endregion

        #region Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            TxtSOQuot.Text = Sm.GetValue("Select MAX(DocNo) From tblSoQuot A Inner Join TblCustomer B on A.CtCode = B.CtCode Where A.CtCode = '" + Sm.GetLue(LueCtCode) + "' And A.ActInd = 'Y' ");
            SetLueCtPersonCode(ref LueContactCode, Sm.GetLue(LueCtCode));
                
            TxtSOQuotPromo.Text = null;
        }

        private void BtnDoReqDeptCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmSODlg(this));
            }
        }

        private void LueContactCode_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            //(LueContactCode.Properties.DataSource as List<Lue1>).Add
            //    (
            //        new Lue1 { Col1 = e.DisplayValue.ToString() }
            //    );
            //e.Handled = true;
        }


        private void LueContactCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueContactCode, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));       
        }

        private void BtnSoQuotPromoDlg_Click(object sender, EventArgs e)
        {
                Sm.FormShowDialog(new FrmSODlg(this));
        }

        private void BtnSOQuotPromoMaster_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOQuotPromo, "SO Quotation Promo", false))
            {
                try
                {
                    var f = new FrmSOQuotPromo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOQuotPromo.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void BtnSOMaster_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                {
                    if (TxtSOQuot.EditValue != null && TxtSOQuot.Text.Length != 0)
                    {

                        var f = new FrmSoQuot(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = TxtSOQuot.Text;
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmSoQuot(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mCtCode = Sm.GetLue(LueCtCode);
                        f.ShowDialog();
                        TxtSOQuot.EditValue = Sm.GetValue("Select MAX(DocNo) From TblSOQuot Where CtCode = '"+Sm.GetLue(LueCtCode)+"' "); 
                    }
                }
            }
        }
        #endregion

    }
}
