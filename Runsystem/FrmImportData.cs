﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmImportData : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mImportCode = string.Empty;
        private string mStateInd = string.Empty;
        internal FrmImportDataFind FrmFind;

        #endregion

        #region Constructor

        public FrmImportData(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Import Data";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mImportCode.Length != 0)
                {
                    ShowData(mImportCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtImportCode, TxtImportDesc, MeeAttribute
                    }, true);

                    ChkActInd.Properties.ReadOnly = true;
                    TxtImportCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtImportCode, TxtImportDesc, MeeAttribute
                    }, false);
                    ChkActInd.Checked = true;
                    TxtImportCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd }, false);
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtImportCode, TxtImportDesc, MeeAttribute
            });
            ChkActInd.Checked = false;
            mStateInd = string.Empty;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmImportDataFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateInd = "1";
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtImportCode, "", false)) return;
            mStateInd = "2";
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mStateInd == "1")
                {
                    SaveImportData();
                }
                else if (mStateInd == "2")
                {
                    UpdateImportData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtImportCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtImportDesc, "Description", false) ||
                Sm.IsMeeEmpty(MeeAttribute, "Attribute") ||
                IsImportCodeExisted(); 
        }

        private bool IsImportCodeExisted()
        {
            if (!TxtImportCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select ImportCode From TblImportData " +
                        "Where ImportCode =@ImportCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@ImportCode", TxtImportCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Code ( " + TxtImportCode.Text + " ) already existed.");
                    TxtImportCode.Focus();
                    return true;
                }
            }
            return false;
        }

        private void SaveImportData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;


            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblImportData(ImportCode, ActInd, ImportDesc, Attribute, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ImportCode, @ActInd, @ImportDesc, @Attribute, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ImportCode", TxtImportCode.Text);
            Sm.CmParam<String>(ref cm, "@ImportDesc", TxtImportDesc.Text);
            Sm.CmParam<String>(ref cm, "@Attribute", MeeAttribute.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ShowData(TxtImportCode.Text);
        }

        #endregion

        #region Edit Data

        private void UpdateImportData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblImportData Set ");
            SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where ImportCode=@ImportCode And ActInd='Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ImportCode", TxtImportCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ShowData(TxtImportCode.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtImportCode, "Code", false) ||
                IsDataNotDeactivated() ||
                IsDataAlreadyInactive();
        }

        private bool IsDataAlreadyInactive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ImportCode From TblImportData ");
            SQL.AppendLine("Where ActInd='N' And ImportCode=@ImportCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ImportCode", TxtImportCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is inactive already.");
                return true;
            }

            return false;
        }

        private bool IsDataNotDeactivated()
        {
            if (ChkActInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to deactivate this document.");
                return true;
            }
            return false;
        }

        #endregion

        #region Show Data

        public void ShowData(string ImportCode)
        {
            try
            {
                ClearData();
                ShowImportData(ImportCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowImportData(string ImportCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT * ");
            SQL.AppendLine("FROM TblImportData ");
            SQL.AppendLine("WHERE ImportCode=@ImportCode;");

            Sm.CmParam<String>(ref cm, "@ImportCode", ImportCode);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "ImportCode",
                    
                    //1-3
                    "ImportDesc", "ActInd", "Attribute"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtImportCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtImportDesc.EditValue = Sm.DrStr(dr, c[1]);
                     ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                     MeeAttribute.EditValue = Sm.DrStr(dr, c[3]);
                 }, true
             );
        }

        #endregion

        #endregion
    }
}
