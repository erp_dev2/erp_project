﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTaxRefundASA : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptTaxRefundASA(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 4;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Header.Cells[0, 1].Value = "Document#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = "Date";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[0, 3].Value = "Vendor";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            SetGrdWhsInfo("Pickel", 4);
            SetGrdWhsInfo("Wet Blue", 10);
            SetGrdWhsInfo("Basah", 16);
            SetGrdWhsInfo("Kering", 22);
            SetGrdWhsInfo("Finish", 28);
            SetGrdWhsInfo("Finish 2", 34);

            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 16, 22, 28, 34 }, false);
            Sm.GrdFormatDec(Grd1,
                new int[] { 
                    6, 7, 8, 9,
                    12, 13, 14, 15,
                    18, 19, 20, 21,
                    24, 25, 26, 27,
                    30, 31, 32, 33,
                    36, 37, 38, 39
                }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 16, 22, 28, 34 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data3>();
            var l4 = new List<Data4>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l2, ref l3, ref l4);
                if (l2.Count > 0)
                {
                    Process2(ref l2, ref l3, ref l4);
                    if (l2.Count > 0) Process4(ref l2);
                    Process5(ref l1, ref l2);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                l3.Clear();
                l4.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<Data2> l2, ref List<Data3> l3, ref List<Data4> l4)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string
                Filter = " ",
                DocumentTemp = string.Empty,
                DocNoTemp = string.Empty,
                WhsCodeTemp = string.Empty,
                SourceTemp = string.Empty,
                DocDtTemp = string.Empty,
                VdNameTemp = string.Empty,
                BatchNoTemp = string.Empty
                ;
            decimal QtyTemp = 0m, Qty2Temp = 0m; 

            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));
            
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.VdName, A.WhsCode, B.Source, B.Qty, B.Qty2, A.DocDt, C.VdName, B.BatchNo ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Status='A' ");
            SQL.AppendLine("    And (B.BatchNo<>'R %' And B.BatchNo<>'S %' And B.BatchNo<>'GF %' )");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            SQL.AppendLine("    And D.ItCtCode In ('001', '002', '003', '004', '005', '006', '007', '008', '009', '010') ");
            SQL.AppendLine("Where A.DocDt Between @Dt1 And @Dt2 ");
            SQL.AppendLine(Filter + " Order By A.DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ 
                    "DocNo", 
                    
                    "WhsCode", 
                    "Source", 
                    "Qty", 
                    "Qty2", 
                    "DocDt", 
                    
                    "VdName",
                    "BatchNo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        WhsCodeTemp = Sm.DrStr(dr, c[1]);
                        SourceTemp = Sm.DrStr(dr, c[2]);
                        QtyTemp = Sm.DrDec(dr, c[3]);
                        Qty2Temp = Sm.DrDec(dr, c[4]);
                        DocDtTemp = Sm.DrStr(dr, c[5]);
                        VdNameTemp = Sm.DrStr(dr, c[6]);
                        BatchNoTemp = Sm.DrStr(dr, c[7]);

                        l2.Add(new Data2() 
                        { 
                            DocNoSource = DocNoTemp,
                            DocDt = DocDtTemp,
                            VdName = VdNameTemp,
                            WhsCode = WhsCodeTemp, 
                            BatchNo = BatchNoTemp,
                            Source = SourceTemp,
                            Qty = QtyTemp,
                            Qty2 = Qty2Temp,
                            UPrice = 0m
                        });

                        if (!Sm.CompareStr(DocumentTemp, DocNoTemp))
                        {
                            l3.Add(new Data3() 
                            { 
                                DocNoSource = DocNoTemp,
                                DocDt = DocDtTemp,
                                VdName = VdNameTemp
                            });
                        }

                        l4.Add(new Data4()
                        {
                            DocNoSource = DocNoTemp,
                            Source = SourceTemp
                        });

                        DocumentTemp = DocNoTemp;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Data2> l2, ref List<Data3> l3, ref List<Data4> l4)
        {
            var l5 = new List<Data4>();
            for (var i = 0; i < l3.Count; i++)
            {
                l5.Clear();
                for (var j = 0; j < l4.Count; j++)
                {
                    if (Sm.CompareStr(l3[i].DocNoSource, l4[j].DocNoSource))
                    {
                        l5.Add(new Data4()
                        {
                            DocNoSource = l3[i].DocNoSource,
                            Source = l4[j].Source
                        });
                    }
                }
                if (l5.Count != 0) Process3(l3[i].DocNoSource, l3[i].DocDt, l3[i].VdName, ref l2, ref l5);
            }
            l5.Clear();
        }

        private void Process3(string Document, string Dt, string Vd, ref List<Data2> l2, ref List<Data4> l5)
        {
            string
                Filter = string.Empty,
                WhsCodeTemp = string.Empty,
                BatchNoTemp = string.Empty, 
                SourceTemp = string.Empty;
            decimal QtyTemp = 0m, Qty2Temp = 0m;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (var i = 0; i < l5.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(X.Source=@Source" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), l5[i].Source);
            }

            l5.Clear();

            SQL.AppendLine("Select A.WhsCode, B.BatchNoTo As BatchNo, B.SourceTo As Source, B.QtyTo As Qty, B.Qty2To As Qty2 ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And (B.BatchNoFrom Not Like 'R %' And B.BatchNoFrom Not Like 'S %' And B.BatchNoFrom Not Like 'GF %' ) ");
            if (Filter.Length > 0)
                SQL.AppendLine("    And (" + Filter.Replace("X.Source", "B.SourceFrom") + ") ");
            else
                SQL.AppendLine("    And 0=1 ");
            SQL.AppendLine("Where A.WhsCode In ('BB', 'BM MK WB', 'BS', 'KR', 'GF', 'GF 2') ");
            
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.WhsCode, B.BatchNo, B.Source, B.Qty, B.Qty2 ");
            SQL.AppendLine("From TblMutationsHdr A ");
            SQL.AppendLine("Inner Join TblMutationsDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (B.BatchNo Not Like 'R %' And B.BatchNo Not Like 'S %' And B.BatchNo Not Like 'GF %' ) ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.WhsCode In ('BB', 'BM MK WB', 'BS', 'KR', 'GF', 'GF 2') ");
            SQL.AppendLine("And A.DocNo In (");
            SQL.AppendLine("    Select Distinct X.DocNo From TblMutationsDtl X ");
            if (Filter.Length > 0)
            {
                SQL.AppendLine("    Where (" + Filter + ") ");
                SQL.AppendLine("    And (BatchNo Not Like 'R %' And BatchNo Not Like 'S %' And BatchNo Not Like 'GF %' )");
            }
            else
                SQL.AppendLine("    Where 0=1 ");
            SQL.AppendLine(") ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select A.WhsCode, B.BatchNo, B.Source, B.Qty, B.Qty2 ");
            //SQL.AppendLine("From TblRecvWhs2Hdr A ");
            //SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            //SQL.AppendLine("    And (B.BatchNo Not Like 'R %' And B.BatchNo Not Like 'S %' And B.BatchNo Not Like 'GF %') ");
            //if (Filter.Length > 0)
            //    SQL.AppendLine("    And (" + Filter.Replace("X.Source", "B.Source") + ") ");
            //else
            //    SQL.AppendLine("    And 0=1 ");
            //SQL.AppendLine("Where A.WhsCode In ('BB', 'BS', 'KR', 'GF', 'GF 2') ");
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WhsCode", "BatchNo", "Source", "Qty", "Qty2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        WhsCodeTemp = Sm.DrStr(dr, c[0]);
                        BatchNoTemp = Sm.DrStr(dr, c[1]);
                        SourceTemp = Sm.DrStr(dr, c[2]);
                        QtyTemp = Sm.DrDec(dr, c[3]);
                        Qty2Temp = Sm.DrDec(dr, c[4]);

                        l2.Add(new Data2()
                        {
                            DocNoSource = Document,
                            DocDt = Dt,
                            VdName = Vd,
                            WhsCode = WhsCodeTemp,
                            BatchNo = BatchNoTemp,
                            Source = SourceTemp,
                            Qty = QtyTemp,
                            Qty2 = Qty2Temp,
                            UPrice = 0m
                        });

                        l5.Add(new Data4()
                        {
                            DocNoSource = Document,
                            Source = SourceTemp
                        });
                    }
                    dr.Close();
                }
            }
            if (l5.Count > 0)
            {
                Process3(Document, Dt, Vd, ref l2, ref l5);
                l5.Clear();
            }
            else
                return;
        }

        private void Process4(ref List<Data2> l2)
        {
            string Filter = string.Empty, SourceTemp = string.Empty;
            decimal UPriceTemp = 0m;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (var i = 0; i < l2.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(Source=@Source" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), l2[i].Source);
            }

            SQL.AppendLine("Select Source, UPrice From TblStockPrice ");
            if (Filter.Length > 0)
                SQL.AppendLine("Where (" + Filter + ") ");
            else
                SQL.AppendLine("Where 0=1 ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Source", "UPrice" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SourceTemp = Sm.DrStr(dr, c[0]);
                        UPriceTemp = Sm.DrDec(dr, c[1]);
                        foreach (var x in l2.Where(w=>Sm.CompareStr(w.Source, SourceTemp)))
                            x.UPrice = UPriceTemp;
                    }
                    dr.Close();
                }
            }
        }

        private void Process5(ref List<Data1> l1, ref List<Data2> l2)
        {
            iGRow r;
            int n = 0;

            Grd1.BeginUpdate();
            foreach (var x in l2.OrderBy(d => d.DocNoSource).ThenBy(d => d.WhsCode))
            {
                r = Grd1.Rows.Add();
                n++;
                r.Cells[0].Value = n;
                r.Cells[1].Value = x.DocNoSource;
                r.Cells[2].Value = Sm.ConvertDate(x.DocDt);
                r.Cells[3].Value = x.VdName;
                
                Sm.SetGrdNumValueZero(Grd1, n-1, 
                new int[] 
                { 
                    6, 7, 8, 9,
                    12, 13, 14, 15,
                    18, 19, 20, 21,
                    24, 25, 26, 27,
                    30, 31, 32, 33,
                    36, 37, 38, 39
                });
            }

            int row = 0;
            string DocNoSourceTemp = string.Empty;

            foreach (var x in l2.OrderBy(d => d.DocNoSource).ThenBy(d => d.WhsCode))
            {
                for (var i = row; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 1), x.DocNoSource))
                    {
                        if (i!=0) row = i;
                    }
                    if (SetGrdValue(x, "BB", i, 4)) break;
                    if (SetGrdValue(x, "BM MK WB", i, 10)) break;
                    if (SetGrdValue(x, "BS", i, 16)) break;
                    if (SetGrdValue(x, "KR", i, 22)) break;
                    if (SetGrdValue(x, "GF", i, 28)) break;
                    if (SetGrdValue(x, "GF 2", i, 34)) break;
                }
            }

            for (int i = Grd1.Rows.Count - 1; i >= 0; i--)
            {
                if (
                    Sm.GetGrdStr(Grd1, i, 4).Length == 0 &&
                    Sm.GetGrdStr(Grd1, i, 10).Length == 0 &&
                    Sm.GetGrdStr(Grd1, i, 16).Length == 0 &&
                    Sm.GetGrdStr(Grd1, i, 22).Length == 0 &&
                    Sm.GetGrdStr(Grd1, i, 28).Length == 0 &&
                    Sm.GetGrdStr(Grd1, i, 34).Length == 0
                    )
                    Grd1.Rows.RemoveAt(i);
            }

            for (int i = 0; i < Grd1.Rows.Count; i++)
                Grd1.Cells[i, 0].Value = i + 1;
            
            if (ChkShowTotal.Checked)
            {
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]
                {
                    6, 7, 8, 9,
                    12, 13, 14, 15,
                    18, 19, 20, 21,
                    24, 25, 26, 27,
                    30, 31, 32, 33,
                    36, 37, 38, 39
                });
            }

            Sm.SetGrdProperty(Grd1, true);

            Grd1.EndUpdate();
        }

        private void SetGrdWhsInfo(string WhsName, int c)
        {
            Grd1.Header.Cells[1, c].Value = WhsName;
            Grd1.Header.Cells[1, c].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, c].SpanCols = 6;
            Grd1.Header.Cells[0, c].Value = "Source";
            Grd1.Header.Cells[0, c].SpanRows = 1;
            Grd1.Header.Cells[0, c+1].Value = "Batch#";
            Grd1.Header.Cells[0, c+1].SpanRows = 1;
            Grd1.Header.Cells[0, c+2].Value = "SF";
            Grd1.Header.Cells[0, c+2].SpanRows = 1;
            Grd1.Header.Cells[0, c+3].Value = "Lembar";
            Grd1.Header.Cells[0, c+3].SpanRows = 1;
            Grd1.Header.Cells[0, c+4].Value = "Rp";
            Grd1.Header.Cells[0, c+4].SpanRows = 1;
            Grd1.Header.Cells[0, c+5].Value = "Total";
            Grd1.Header.Cells[0, c+5].SpanRows = 1;
        }

        private bool SetGrdValue(Data2 x, string WhsCode, int r, int c)
        {
            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), x.DocNoSource))
            {
                if (Sm.CompareStr(x.WhsCode, WhsCode))
                {
                    if (Sm.GetGrdStr(Grd1, r, c).Length == 0)
                    {
                        Grd1.Cells[r, c].Value = x.Source;
                        Grd1.Cells[r, c + 1].Value = x.BatchNo;
                        Grd1.Cells[r, c + 2].Value = x.Qty;
                        Grd1.Cells[r, c + 3].Value = x.Qty2;
                        Grd1.Cells[r, c + 4].Value = x.UPrice;
                        Grd1.Cells[r, c + 5].Value = x.Qty * x.UPrice;
                        return true;
                    }
                }
            }
            return false;
        }
        
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion

        #region class

        private class Data1
        {
            public string DocNoSource { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
        }

        private class Data2
        {
            public string DocNoSource { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string WhsCode { get; set; }
            public string Source { get; set; }
            public string BatchNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal UPrice { get; set; }
        }

        private class Data3
        {
            public string DocNoSource { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
        }

        private class Data4
        {
            public string DocNoSource { get; set; }
            public string Source { get; set; }
        }

        #endregion
    }
}
