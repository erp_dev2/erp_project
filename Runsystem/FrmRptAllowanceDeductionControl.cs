﻿/*
    04/01/2018 [TKG] Data yg belum diproses di payroll processing tetap muncul
*/
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAllowanceDeductionControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsNotFilterByAuthorization = false;
        
        #endregion

        #region Constructor

        public FrmRptAllowanceDeductionControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                GetParameter();

                Sl.SetLueDeptCode(ref LueDeptCode);
                SetLueDocType(ref LueDocType);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("  Case T.DocType ");
            SQL.AppendLine("    When '1' Then 'Direct Labor Production Penalty' ");
            SQL.AppendLine("    When '2' Then 'Group of Direct Labor Production Penalty' ");
            SQL.AppendLine("    When '3' Then 'Employee Salary Adjustment' ");
            SQL.AppendLine("    When '4' Then 'Group of Employee Salary Adjustment' ");
            SQL.AppendLine("    When '5' Then 'Employee Incentive/Penalty (Standard)' ");
            SQL.AppendLine("End As DocType, T.DocNo, T.DocDt, T.EmpCode, T.EmpCodeOld, T.EmpName, T.ActInd, T.DeptCode, T.DeptName, T.Workcenter, T.InspntName, T.InspntCtName, T.Amt, T.PayrunCode ");
            SQL.AppendLine("From ( ");

            // query penalty
            SQL.AppendLine("Select X2.DocType, X2.DocNo, X2.DocDt, X2.EmpCode, X2.EmpCodeOld, X2.EmpName, X2.ActInd, X2.DeptCode, X2.DeptName, X2.Workcenter, X2.InspntName, X2.InspntCtName, X2.Amt, X2.PayrunCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  select '1' as DocType, A.DocNo, A.DocDt, B.EmpCode, D.EmpCodeOld, D.EmpName, if(ifnull(length(D.ResignDt), 0)=0,'Y','N') as ActInd, IfNull(D.DeptCode, '') As DeptCode, E.DeptName, C.DocName As Workcenter, Null As InspntName, Null As InspntCtName, B.Penalty As Amt, B.PayrunCode ");
            SQL.AppendLine("  from tblpnthdr A ");
            SQL.AppendLine("  inner join tblpntdtl4 B on A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And Exists( ");
            //SQL.AppendLine("        Select 1 From TblPayrollProcess1 T1, TblPayrun T2 ");
            //SQL.AppendLine("        Where T1.EmpCode=B.EmpCode ");
            //SQL.AppendLine("        And T1.PayrunCode=B.PayrunCode ");
            //SQL.AppendLine("        And T1.PayrunCode=T2.PayrunCode And CancelInd='N' ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("  inner join tblworkcenterhdr C on A.WorkCenterDocNo = C.DocNo ");
            SQL.AppendLine("  inner join tblemployee D on B.EmpCode = D.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=D.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("  left join tbldepartment E on D.DeptCode = E.DeptCode ");
            SQL.AppendLine("  where A.CancelInd = 'N' and A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine(")X2 ");

            SQL.AppendLine("union all ");

            // query penalty group
            SQL.AppendLine("Select X3.DocType, X3.DocNo, X3.DocDt, X3.EmpCode, X3.EmpCodeOld, X3.EmpName, X3.ActInd, X3.DeptCode, X3.DeptName, X3.Workcenter, X3.InspntName, X3.InspntCtName, X3.Amt, X3.PayrunCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  select '2' as DocType, A.DocNo, A.DocDt, B.EmpCode, D.EmpCodeOld, D.EmpName, if(ifnull(length(D.ResignDt), 0)=0,'Y','N') as ActInd, IfNull(D.DeptCode, '') As DeptCode, E.DeptName, C.DocName As Workcenter, Null as InspntName, Null As InspntCtName, B.Penalty As Amt, B.PayrunCode ");
            SQL.AppendLine("  from tblpnt2hdr A ");
            SQL.AppendLine("  inner join tblpnt2dtl5 B on A.DocNo = B.DocNo ");
            //SQL.AppendLine("    And Exists( ");
            //SQL.AppendLine("        Select 1 From TblPayrollProcess1 T1, TblPayrun T2 ");
            //SQL.AppendLine("        Where T1.EmpCode=B.EmpCode ");
            //SQL.AppendLine("        And T1.PayrunCode=B.PayrunCode ");
            //SQL.AppendLine("        And T1.PayrunCode=T2.PayrunCode And CancelInd='N' ");
            //SQL.AppendLine("    ) ");
            SQL.AppendLine("  inner join tblworkcenterhdr C on A.WorkCenterDocNo = C.DocNo ");
            SQL.AppendLine("  inner join tblemployee D on B.EmpCode = D.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=D.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("  left join tbldepartment E on D.DeptCode = E.DeptCode ");
            SQL.AppendLine("  where A.CancelInd = 'N' and A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine(")X3 ");

            SQL.AppendLine("union all ");

            // query salary adjustment
            SQL.AppendLine("Select X4.DocType, X4.DocNo, X4.DocDt, X4.EmpCode, X4.EmpCodeOld, X4.EmpName, X4.ActInd, X4.DeptCode, X4.DeptName, X4.Workcenter, X4.InspntName, X4.InspntCtName, X4.Amt, X4.PayrunCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  select '3' as DocType, A.DocNo, A.DocDt, A.EmpCode, B.EmpCodeOld, B.EmpName, if(ifnull(length(B.ResignDt), 0)=0,'Y','N') as ActInd, IfNull(B.DeptCode, '') As DeptCode, C.DeptName, Null as Workcenter, Null as InspntName, Null as InspntCtName, A.Amt, A.PayrunCode ");
            SQL.AppendLine("  from tblsalaryadjustmenthdr A ");
            SQL.AppendLine("  inner join tblemployee B on A.EmpCode = B.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=B.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("  left join tbldepartment C on B.DeptCode = C.DeptCode ");
            SQL.AppendLine("  where A.CancelInd = 'N' and A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine(")X4 ");

            SQL.AppendLine("union all ");

            // query group salary adjustment
            SQL.AppendLine("Select X5.DocType, X5.DocNo, X5.DocDt, X5.EmpCode, X5.EmpCodeOld, X5.EmpName, X5.ActInd, X5.DeptCode, X5.DeptName, X5.Workcenter, X5.InspntName, X5.InspntCtName, X5.Amt, X5.PayrunCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  select '4' as DocType, A.DocNo, A.DocDt, B.EmpCode, C.EmpCodeOld, C.EmpName, if(ifnull(length(C.ResignDt), 0)=0,'Y','N') as ActInd, IfNull(C.DeptCode, '') As DeptCode, D.DeptName, Null As Workcenter, Null As InspntName, Null as InspntCtName, A.Amt, B.PayrunCode ");
            SQL.AppendLine("  from tblsalaryadjustment2hdr A ");
            SQL.AppendLine("  inner join tblsalaryadjustment2dtl B on A.DocNo = B.DocNo ");
            SQL.AppendLine("  inner join tblemployee C on B.EmpCode = C.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=C.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("  left join tbldepartment D on C.DeptCode = D.DeptCode ");
            SQL.AppendLine("  where A.CancelInd = 'N' and A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine(")X5 ");

            SQL.AppendLine("union all ");

            // query employee incentive
            SQL.AppendLine("Select X6.DocType, X6.DocNo, X6.DocDt, X6.EmpCode, X6.EmpCodeOld, X6.EmpName, X6.ActInd, X6.DeptCode, X6.DeptName, X6.Workcenter, X6.InspntName, X6.InspntCtName, X6.Amt, X6.PayrunCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select '5' As DocType, A.DocNo, A.DocDt, B.EmpCode, E.EmpCodeOld, E.EmpName, ");
            SQL.AppendLine("  If(IfNull(Length(E.ResignDt), 0)=0,'Y','N') as ActInd, IfNull(E.DeptCode, '') As DeptCode, ");
            SQL.AppendLine("  F.DeptName, Null as Workcenter, C.InsPntName, D.InsPntCtName, A.AmtInspnt As Amt, B.PayrunCode ");
            SQL.AppendLine("  From tblempinspnthdr A ");
            SQL.AppendLine("  inner join tblempinspntdtl B on A.DocNo = B.DocNo ");
            //SQL.AppendLine("  And Exists( ");
            //SQL.AppendLine("        Select 1 From TblPayrollProcess1 T1, TblPayrun T2 ");
            //SQL.AppendLine("        Where T1.EmpCode=B.EmpCode ");
            //SQL.AppendLine("        And T1.PayrunCode=B.PayrunCode ");
            //SQL.AppendLine("        And T1.PayrunCode=T2.PayrunCode And CancelInd='N' ");
            //SQL.AppendLine("  ) ");
            SQL.AppendLine("  inner join tblinspnt C on A.InspntCode = C.InspntCode ");
            SQL.AppendLine("  inner join tblinspntcategory D on C.InspntCtCode = D.InspntCtCode ");
            SQL.AppendLine("  inner join tblemployee E on B.EmpCode = E.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=E.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("  left join tbldepartment F on E.DeptCode = F.DeptCode ");
            SQL.AppendLine("  Where A.CancelInd = 'N' and A.DocDt between @DocDt1 and @DocDt2 ");
            SQL.AppendLine(")X6 ");

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",
                        "Document#",
                        "Date", 
                        "Employee's Code", 
                        "",

                        //6-10
                        "Old Code",
                        "Employee's Name",
                        "Active",
                        "Department",
                        "Work Center",

                        //11-14
                        "Category",
                        "Name",
                        "Amount",
                        "Payrun Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 150, 80, 100, 20, 

                        //6-10
                        100, 200, 60, 200, 200, 

                        //11-14
                        200, 150, 160, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 8 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    IsFilterByDateInvalid()
                    ) return;


                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                if (!(LueDocType.EditValue == null))
                {
                    if (Sm.GetLue(LueDocType) == "1")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, false);
                        Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                    }
                    else if (Sm.GetLue(LueDocType) == "2")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, false);
                        Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                    }
                    else if (Sm.GetLue(LueDocType) == "3")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12 }, false);
                    }
                    else if (Sm.GetLue(LueDocType) == "4")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12 }, false);
                    }
                    else if (Sm.GetLue(LueDocType) == "5")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
                        Sm.GrdColInvisible(Grd1, new int[] { 11, 12 }, true);
                    }
                }
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12 }, true);

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.EmpCode", "T.EmpName", "T.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "T.DocType", true);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL() +
                        Filter + " Order By T.DocType, T.DocNo, T.DocDt, T.EmpName;",
                        new string[]
                        {
                            //0
                            "DocType", 
                            //1-5
                            "DocNo", "DocDt", "EmpCode", "EmpCodeOld", "EmpName", 
                            //6-10
                            "ActInd", "DeptName", "Workcenter", "InspntCtName", "InspntName", 
                            //11-12
                            "Amt", "PayrunCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        private void SetLueDocType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Direct Labor Production Penalty' As Col2 " +
                "Union All " +
                "Select '2' As Col1, 'Group of Direct Labor Production Penalty' As Col2 " +
                "Union All " +
                "Select '3' As Col1, 'Employee Salary Adjustment' As Col2 " +
                "Union All " +
                "Select '4' As Col1, 'Group of Employee Salary Adjustment' As Col2 " +
                "Union All " +
                "Select '5' As Col1, 'Employee Incentive/Penalty (Standard)' As Col2 ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = "Master Employee";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = "Master Employee";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
