﻿#region Update
/*
    26/02/2018 [DITA] reporting document control
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptDocumentControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDocumentControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
                SetLueVdCode(ref LueVdCode);
                Sl.SetLueFileCategory(ref LueFileCategory);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VdCode, C.VdName, A.CategoryCode, B.CategoryName, A.FileLocation, A.StartDt, ");
            SQL.AppendLine("A.EndDt, A.ValidInd, A.LocalDocNo, A.LicensedBy ");
            SQL.AppendLine("From TblVendorFileUpload A   ");
            SQL.AppendLine("Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode And B.ActInd = 'Y'  ");
            SQL.AppendLine("Inner Join TblVendor C  On A.VdCode = C.VdCode ");
            SQL.AppendLine("Where A.Status = 'A' And A.StartDt between @DocDt1 and @DocDt2  ");
            
            mSQL = SQL.ToString();//
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor Code", 
                        "Vendor Name",
                        "Category Code",
                        "Category Name",
                        "Valid From",

                        //6-8
                        "Valid To",
                        "Local Document#",
                        "Licensed By",
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 180, 100, 250, 90,

                        //6-10
                        90, 150, 150
                        
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 7, 8 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueFileCategory), "A.CategoryCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By C.VdName;",
                new string[]
                {
                    //0
                    "VdCode", 

                    //1-5
                    "VdName", "CategoryCode", "CategoryName", "StartDt", "EndDt",

                    //6-7
                    "LocalDocNo", "LicensedBy"
                },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueVdCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.VdCode As Col1, B.VdName As Col2 ");
            SQL.AppendLine("From TblVendorFileUpload A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Where A.Status = 'A' ");
            SQL.AppendLine("Order By B.VdName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkVendor_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkCategory_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }
        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void LueFileCategory_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFileCategory, new Sm.RefreshLue1(Sl.SetLueFileCategory));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
