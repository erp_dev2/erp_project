﻿#region Update
/*
    02/01/2019 [HAR] karakter textbox, option code cuma 16 digit, dibuat jadi 40 sesuai length kolom di tabelnya
    04/01/2019 [HAR] inputan par value dari text box dihganti emo edit biar bisa input karakter enter
    23/06/2021 [TKG/IMS] Tambah property dan remark
    27/06/2021 [TKG/IMS] Tambah property 4-10
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOption : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmOptionFind FrmFind;
        internal bool mIsOptionPropertyEnabled = false;

        #endregion

        #region Constructor

        public FrmOption(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtOptCat, TxtOptCode, MeeOptDesc, TxtCustomize, TxtProperty1,
                        TxtProperty2, TxtProperty3, TxtProperty4, TxtProperty5, TxtProperty6, 
                        TxtProperty7, TxtProperty8, TxtProperty9, TxtProperty10, MeeRemark
                    }, true);
                    TxtOptCat.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtOptCat, TxtOptCode, MeeOptDesc, TxtCustomize, MeeRemark
                    }, false);
                    if (mIsOptionPropertyEnabled)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtProperty1, TxtProperty2, TxtProperty3, TxtProperty4, TxtProperty5, 
                            TxtProperty6, TxtProperty7, TxtProperty8, TxtProperty9, TxtProperty10  
                        }, false);
                    TxtOptCat.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeOptDesc, TxtCustomize, MeeRemark }, false);
                    if (mIsOptionPropertyEnabled)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtProperty1, TxtProperty2, TxtProperty3, TxtProperty4, TxtProperty5, 
                            TxtProperty6, TxtProperty7, TxtProperty8, TxtProperty9, TxtProperty10  
                        }, false);
                    TxtOptCat.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtOptCat, TxtOptCode, MeeOptDesc, TxtCustomize, TxtProperty1,
                TxtProperty2, TxtProperty3, TxtProperty4, TxtProperty5, TxtProperty6, 
                TxtProperty7, TxtProperty8, TxtProperty9, TxtProperty10, MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOptionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtOptCat, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtOptCat, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblOption Where OptCat=@OptCat;" };
                Sm.CmParam<String>(ref cm, "@OptCat", TxtOptCat.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblOption(OptCat, OptCode, OptDesc, ");
                if (mIsOptionPropertyEnabled)
                {
                    SQL.AppendLine("Property1, Property2, Property3, Property4, Property5, ");
                    SQL.AppendLine("Property6, Property7, Property8, Property9, Property10, ");
                }
                SQL.AppendLine("Remark, Customize, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@OptCat, @OptCode, @OptDesc, ");
                if (mIsOptionPropertyEnabled)
                {
                    SQL.AppendLine("@Property1, @Property2, @Property3, @Property4, @Property5, ");
                    SQL.AppendLine("@Property6, @Property7, @Property8, @Property9, @Property10, ");
                }
                SQL.AppendLine("@Remark, @Customize, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update OptDesc=@OptDesc, ");
                if (mIsOptionPropertyEnabled)
                {
                    SQL.AppendLine("    Property1=@Property1, Property2=@Property2, Property3=@Property3, Property4=@Property4, Property5=@Property5, ");
                    SQL.AppendLine("    Property6=@Property6, Property7=@Property7, Property8=@Property8, Property9=@Property9, Property10=@Property10, ");
                }
                SQL.AppendLine("    Remark=@Remark, Customize=@Customize, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@OptCat", TxtOptCat.Text);
                Sm.CmParam<String>(ref cm, "@OptCode", TxtOptCode.Text);
                Sm.CmParam<String>(ref cm, "@OptDesc", MeeOptDesc.Text); 
                Sm.CmParam<String>(ref cm, "@Customize", TxtCustomize.Text);
                if (mIsOptionPropertyEnabled)
                {
                    Sm.CmParam<String>(ref cm, "@Property1", TxtProperty1.Text);
                    Sm.CmParam<String>(ref cm, "@Property2", TxtProperty2.Text);
                    Sm.CmParam<String>(ref cm, "@Property3", TxtProperty3.Text);
                    Sm.CmParam<String>(ref cm, "@Property4", TxtProperty4.Text);
                    Sm.CmParam<String>(ref cm, "@Property5", TxtProperty5.Text);
                    Sm.CmParam<String>(ref cm, "@Property6", TxtProperty6.Text);
                    Sm.CmParam<String>(ref cm, "@Property7", TxtProperty7.Text);
                    Sm.CmParam<String>(ref cm, "@Property8", TxtProperty8.Text);
                    Sm.CmParam<String>(ref cm, "@Property9", TxtProperty9.Text);
                    Sm.CmParam<String>(ref cm, "@Property10", TxtProperty10.Text);
                }
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text); 
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtOptCat.Text, TxtOptCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string OptCat, string OptCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCat, OptCode, OptDesc, Customize, ");
                if (mIsOptionPropertyEnabled)
                {
                    SQL.AppendLine("Property1, Property2, Property3, Property4, Property5, ");
                    SQL.AppendLine("Property6, Property7, Property8, Property9, Property10, ");
                }
                else
                {
                    SQL.AppendLine("Null As Property1, Null As Property2, Null As Property3, Null As Property4, Null As Property5, ");
                    SQL.AppendLine("Null As Property6, Null As Property7, Null As Property8, Null As Property9, Null As Property10, ");
                }
                SQL.AppendLine("Remark ");
                SQL.AppendLine("From TblOption Where OptCat=@OptCat And OptCode=@OptCode;");

                Sm.CmParam<String>(ref cm, "@OptCat", OptCat);
                Sm.CmParam<String>(ref cm, "@OptCode", OptCode);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "OptCat", 
                            "OptCode", "OptDesc", "Customize", "Property1", "Property2", 
                            "Property3", "Property4", "Property5", "Property6", "Property7", 
                            "Property8", "Property9", "Property10", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtOptCat.EditValue = Sm.DrStr(dr, c[0]);
                            TxtOptCode.EditValue = Sm.DrStr(dr, c[1]);
                            MeeOptDesc.EditValue = Sm.DrStr(dr, c[2]);
                            TxtCustomize.EditValue = Sm.DrStr(dr, c[3]);
                            TxtProperty1.EditValue = Sm.DrStr(dr, c[4]);
                            TxtProperty2.EditValue = Sm.DrStr(dr, c[5]);
                            TxtProperty3.EditValue = Sm.DrStr(dr, c[6]);
                            TxtProperty4.EditValue = Sm.DrStr(dr, c[7]);
                            TxtProperty5.EditValue = Sm.DrStr(dr, c[8]);
                            TxtProperty6.EditValue = Sm.DrStr(dr, c[9]);
                            TxtProperty7.EditValue = Sm.DrStr(dr, c[10]);
                            TxtProperty8.EditValue = Sm.DrStr(dr, c[11]);
                            TxtProperty9.EditValue = Sm.DrStr(dr, c[12]);
                            TxtProperty10.EditValue = Sm.DrStr(dr, c[13]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtOptCat, "Option category", false) ||
                Sm.IsTxtEmpty(TxtOptCode, "Option code", false) ||
                Sm.IsTxtEmpty(MeeOptDesc, "Option description", false) ||
                IsOptCodeExisted();
        }

        private bool IsOptCodeExisted()
        {
            if (!TxtOptCat.Properties.ReadOnly)
            {
                var cm = new MySqlCommand
                {
                    CommandText = "Select 1 From TblOption Where OptCode=@OptCode And OptCat=@OptCat Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@OptCat", TxtOptCat.Text);
                Sm.CmParam<String>(ref cm, "@OptCode", TxtOptCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Data already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsOptionPropertyEnabled = Sm.GetParameterBoo("IsOptionPropertyEnabled");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtOptCat_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtOptCat);
        }

        private void TxtOptCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtOptCode);
        }

        private void TxtCostumize_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCustomize);
        }

        private void TxtProperty1_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty1);
        }

        private void TxtProperty2_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty2);
        }

        private void TxtProperty3_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty3);
        }

        private void MeeRemark_EditValueChanged(object sender, EventArgs e)
        {
            Sm.MeeTrim(MeeRemark);
        }

        private void TxtProperty4_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty4);
        }

        private void TxtProperty5_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty5);
        }

        private void TxtProperty6_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty6);
        }

        private void TxtProperty7_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty7);
        }

        private void TxtProperty8_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty8);
        }

        private void TxtProperty9_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty9);
        }

        private void TxtProperty10_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProperty10);
        }

        #endregion

        #endregion
    }
}
