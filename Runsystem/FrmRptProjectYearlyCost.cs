﻿#region Update
/*
    16/11/2018 [TKG] Project's Yearly Cost
    18/01/2019 [MEY] Penambahan filter Project name, customer, site, cost center. project scope, project resource, project type 
    31/10/2019 [DITA/IMS] Penambahan Cutomer PO# dan Code Project
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectYearlyCost : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectYearlyCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                string Yr = Sm.ServerCurrentDateTime().Substring(0, 4);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Yr);
                Sl.SetLueYr(LueYr2, string.Empty);
                Sm.SetLue(LueYr2, Yr);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueOption(ref LueTypeCode, "ProjectType");
                Sl.SetLueOption(ref LueScope, "ProjectScope");
                Sl.SetLueOption(ref LueResource, "ProjectResource");
                Sl.SetLueCCCode(ref LueCostCenter);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Cost Category"+Environment.NewLine+"Code",
                        "Cost Category"+Environment.NewLine+"Name",
                        "1",
                        "2",
                        "3",

                        //6-9
                        "4",
                        "5",
                        "Customer PO#",
                        "Project Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 300, 170, 170, 170,  
                        
                        //6-7
                        170, 170, 150, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 5, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[8].Move(3);
            Grd1.Cols[9].Move(4);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.ClearGrd(Grd1, false);
            for (int i = 3; i < 8; i++)
                Grd1.Cols[i].Visible = false;
        }

        override protected void ShowData()
        {
            ClearData();

            if (Sm.IsLueEmpty(LueYr, "Start year")) return;
            if (Sm.IsLueEmpty(LueYr2, "End year")) return;
            
            string Yr = Sm.GetLue(LueYr), Yr2 = Sm.GetLue(LueYr2);
            
            if (int.Parse(Yr2)<int.Parse(Yr))
            {
                Sm.StdMsg(mMsgType.Warning, "End date should not be earlier than start year.");
                return;
            }

            if (int.Parse(Yr2)-int.Parse(Yr)>5)
            {
                Sm.StdMsg(mMsgType.Warning, "Maximum data displayed 5 years.");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();

            try
            {
                Process1(ref l, Yr, Yr2);
                if (l.Count > 0)
                    Process2(ref l, int.Parse(Yr), int.Parse(Yr2));
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l, string Yr, string Yr2)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select T1.Yr, T1.CCtCode, T2.CCtName, T1.Amt, T1.PONo, T1.ProjectCode ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select Left(A.DocDt, 4) As Yr, C.CCtCode,  ");
            SQL.AppendLine("    Sum(B.Qty*D.UPrice*D.ExcRate) As Amt, E.ProjectName, E.CtCode,  E.SiteCode, E.ProjectScope, E.ProjectResource, E.ProjectType, E.CCCode, G.PONo, H.ProjectCode  ");
	        SQL.AppendLine("    From TblDODeptHdr A ");
	        SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblItemCostCategory C On A.CCCode=C.CCCode And B.ItCode=C.ItCode ");
	        SQL.AppendLine("    Inner Join TblStockPrice D On B.Source=D.Source ");
            SQL.AppendLine("    Inner Join TblLopHdr E On E.DocNo=A.LopDocNo ");
            if (TxtProjectName.Text.Length > 0)
                SQL.AppendLine("And E.ProjectName Like @ProjectName ");
            if (Sm.GetLue(LueCostCenter).Length > 0)
                SQL.AppendLine("And E.CCCode = @CostCenter ");
            if (Sm.GetLue(LueCtCode).Length > 0)
                SQL.AppendLine("And E.CtCode = @CtCode ");
            if (Sm.GetLue(LueResource).Length > 0)
                SQL.AppendLine("And E.ProjectResource = @ProjectResource ");
            if (Sm.GetLue(LueScope).Length > 0)
                SQL.AppendLine("And E.ProjectScope = @ProjectScope ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("And E.SiteCode = @Site ");
            if (Sm.GetLue(LueTypeCode).Length > 0)
                SQL.AppendLine("And E.ProjectType = @ProjectType ");
           
            if (TxtPONo.Text.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblBOQHdr F On E.DocNo=F.LopDocNo And F.ActInd = 'Y' And F.Status = 'A' ");
                SQL.AppendLine("    Inner Join TblSOContractHdr G On F.DocNo=G.BOQDocNo And G.CancelInd = 'N' And G.Status = 'A' ");
                SQL.AppendLine("        And G.PONo Like @PONo ");
            }
            else
            {
                SQL.AppendLine("    Left Join TblBOQHdr F On E.DocNo=F.LopDocNo And F.ActInd = 'Y' And F.Status = 'A' ");
                SQL.AppendLine("    Left Join TblSOContractHdr G On F.DocNo=G.BOQDocNo And G.CancelInd = 'N' And G.Status = 'A' ");
            }
            if (TxtProjectCode.Text.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblProjectGroup H On H.PGCode=E.PGCode ");
                SQL.AppendLine("         And H.ProjectCode Like @ProjectCode ");
            }
            else
            {
                SQL.AppendLine("    Left Join TblProjectGroup H On H.PGCode=E.PGCode ");
            }

	        SQL.AppendLine("    Where A.CCCode Is Not Null ");
	        SQL.AppendLine("    And A.DocDt>=@Dt1 And A.DocDt<=@Dt2 ");
            SQL.AppendLine("    Group By Left(A.DocDt, 4), C.CCtCode, G.PONo, H.ProjectCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCostCategory T2 On T1.CCtCode=T2.CCtCode ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Dt1", string.Concat(Yr, "0101"));
            Sm.CmParam<String>(ref cm, "@Dt2", string.Concat(Yr2, "1231"));
            Sm.CmParam<String>(ref cm, "@ProjectName", string.Concat("%", TxtProjectName.Text, "%"));
            Sm.CmParam<String>(ref cm, "@CostCenter", Sm.GetLue(LueCostCenter));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@ProjectResource", Sm.GetLue(LueResource));
            Sm.CmParam<String>(ref cm, "@ProjectScope", Sm.GetLue(LueScope));
            Sm.CmParam<String>(ref cm, "@Site", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ProjectType", Sm.GetLue(LueTypeCode));
            Sm.CmParam<String>(ref cm, "@ProjectCode", string.Concat("%", TxtProjectCode.Text, "%"));
            Sm.CmParam<String>(ref cm, "@PONo", string.Concat("%", TxtPONo.Text, "%"));
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Yr", 
                    
                    //1-5
                    "CCtCode", 
                    "CCtName", 
                    "Amt",
                    "PONo",
                    "ProjectCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            CCtCode = Sm.DrStr(dr, c[1]),
                            CCtName = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            PONo = Sm.DrStr(dr, c[4]),
                            ProjectCode = Sm.DrStr(dr, c[5]),

                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l, int Yr, int Yr2)
        {
            int i = 0, c = 2;
            string year = string.Empty;
            iGRow r;

            Grd1.BeginUpdate();

            for (int y = Yr; y <=Yr2; y++)
            {
                c++;
                Grd1.Cols[c].Visible = true;
                Grd1.Cols[c].Text = y;
            }

            foreach (var x in l
                  .Select(s => new { s.CCtCode, s.CCtName, s.PONo, s.ProjectCode })
                  .OrderBy(o => o.CCtName)
                  .Distinct()
                  .ToList())
            {
                    r = Grd1.Rows.Add();
                    i++;
                    r.Cells[0].Value = i;
                    r.Cells[1].Value = x.CCtCode;
                    r.Cells[2].Value = x.CCtName;
                    r.Cells[3].Value = 0m;
                    r.Cells[4].Value = 0m;
                    r.Cells[5].Value = 0m;
                    r.Cells[6].Value = 0m;
                    r.Cells[7].Value = 0m;
                    r.Cells[8].Value = x.PONo;
                    r.Cells[9].Value = x.ProjectCode;
            }

            foreach(var x in l.OrderBy(o=>o.Yr))
            {
                if (!Sm.CompareStr(year, x.Yr))
                {
                    for (int col = 3; col <= 7; col++)
                    {
                        if (Sm.CompareStr(Grd1.Cols[col].Text.ToString(), x.Yr))
                        {
                            c = col;
                            break;
                        }
                    }
                    year = x.Yr;
                }
                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 1), x.CCtCode))
                    {
                        Grd1.Cells[row, c].Value = x.Amt;
                        break;
                    }
                }
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkCostCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "CostCenter");
        }

        private void ChkScope_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Scope");
        }

        private void ChkResource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Resource");
        }

        private void ChkTypeCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Type");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCostCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenter, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueScope_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueScope, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectScope");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueResource_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueResource, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectResource");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "ProjectName");
        }
        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Code");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string Yr { get; set; }
            public string CCtCode { get; set; }
            public string CCtName { get; set; }
            public decimal Amt { get; set; }
            public string ProjectCode { get; set; }
            public string PONo { get; set; }
        }

        #endregion

    }
}
