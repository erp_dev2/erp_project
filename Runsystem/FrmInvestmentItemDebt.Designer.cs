﻿namespace RunSystem
{
    partial class FrmInvestmentItemDebt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvestmentItemDebt));
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.TxtPortofolioId = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnPortofolio1 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtForeignName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentDebtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TpgDetail = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.LueAnnualDays = new DevExpress.XtraEditors.LookUpEdit();
            this.DteNextCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.DteLastCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtInterestFreq = new DevExpress.XtraEditors.TextEdit();
            this.TxtInterestType = new DevExpress.XtraEditors.TextEdit();
            this.TxtInterestRateAmt = new DevExpress.XtraEditors.TextEdit();
            this.DteMaturityDt = new DevExpress.XtraEditors.DateEdit();
            this.DteListingDt = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteUpdateDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtSpecification = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtIssuedAmt = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtIssuer = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.LueInvestmentUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnPortofolio2 = new DevExpress.XtraEditors.SimpleButton();
            this.label29 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortofolioId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentDebtCode.Properties)).BeginInit();
            this.TpgDetail.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAnnualDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFreq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRateAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuedAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(950, 0);
            this.panel1.Size = new System.Drawing.Size(70, 375);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnPortofolio2);
            this.panel2.Controls.Add(this.TpgDetail);
            this.panel2.Controls.Add(this.TxtInvestmentName);
            this.panel2.Controls.Add(this.TxtPortofolioId);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.BtnPortofolio1);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtForeignName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtInvestmentDebtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(950, 375);
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(157, 49);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 16;
            this.TxtInvestmentName.Properties.ReadOnly = true;
            this.TxtInvestmentName.Size = new System.Drawing.Size(297, 20);
            this.TxtInvestmentName.TabIndex = 16;
            // 
            // TxtPortofolioId
            // 
            this.TxtPortofolioId.EnterMoveNextControl = true;
            this.TxtPortofolioId.Location = new System.Drawing.Point(157, 28);
            this.TxtPortofolioId.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPortofolioId.Name = "TxtPortofolioId";
            this.TxtPortofolioId.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPortofolioId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPortofolioId.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPortofolioId.Properties.Appearance.Options.UseFont = true;
            this.TxtPortofolioId.Properties.MaxLength = 16;
            this.TxtPortofolioId.Properties.ReadOnly = true;
            this.TxtPortofolioId.Size = new System.Drawing.Size(297, 20);
            this.TxtPortofolioId.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(50, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Investment Code";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPortofolio1
            // 
            this.BtnPortofolio1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPortofolio1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPortofolio1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPortofolio1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPortofolio1.Appearance.Options.UseBackColor = true;
            this.BtnPortofolio1.Appearance.Options.UseFont = true;
            this.BtnPortofolio1.Appearance.Options.UseForeColor = true;
            this.BtnPortofolio1.Appearance.Options.UseTextOptions = true;
            this.BtnPortofolio1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPortofolio1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPortofolio1.Image = ((System.Drawing.Image)(resources.GetObject("BtnPortofolio1.Image")));
            this.BtnPortofolio1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPortofolio1.Location = new System.Drawing.Point(456, 26);
            this.BtnPortofolio1.Name = "BtnPortofolio1";
            this.BtnPortofolio1.Size = new System.Drawing.Size(24, 21);
            this.BtnPortofolio1.TabIndex = 14;
            this.BtnPortofolio1.ToolTip = "Find Item";
            this.BtnPortofolio1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPortofolio1.ToolTipTitle = "Run System";
            this.BtnPortofolio1.Click += new System.EventHandler(this.BtnPortofolio_Click);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(456, 6);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtForeignName
            // 
            this.TxtForeignName.EnterMoveNextControl = true;
            this.TxtForeignName.Location = new System.Drawing.Point(157, 70);
            this.TxtForeignName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtForeignName.Name = "TxtForeignName";
            this.TxtForeignName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtForeignName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtForeignName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtForeignName.Properties.Appearance.Options.UseFont = true;
            this.TxtForeignName.Properties.MaxLength = 250;
            this.TxtForeignName.Size = new System.Drawing.Size(297, 20);
            this.TxtForeignName.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(68, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Foreign Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(47, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Investment Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentDebtCode
            // 
            this.TxtInvestmentDebtCode.EnterMoveNextControl = true;
            this.TxtInvestmentDebtCode.Location = new System.Drawing.Point(157, 7);
            this.TxtInvestmentDebtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentDebtCode.Name = "TxtInvestmentDebtCode";
            this.TxtInvestmentDebtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentDebtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentDebtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentDebtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentDebtCode.Properties.MaxLength = 16;
            this.TxtInvestmentDebtCode.Properties.ReadOnly = true;
            this.TxtInvestmentDebtCode.Size = new System.Drawing.Size(297, 20);
            this.TxtInvestmentDebtCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(20, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Debt Investment Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgDetail
            // 
            this.TpgDetail.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TpgDetail.Controls.Add(this.TpgGeneral);
            this.TpgDetail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TpgDetail.Location = new System.Drawing.Point(0, 99);
            this.TpgDetail.Multiline = true;
            this.TpgDetail.Name = "TpgDetail";
            this.TpgDetail.SelectedIndex = 0;
            this.TpgDetail.ShowToolTips = true;
            this.TpgDetail.Size = new System.Drawing.Size(950, 276);
            this.TpgDetail.TabIndex = 32;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.label29);
            this.TpgGeneral.Controls.Add(this.LueAnnualDays);
            this.TpgGeneral.Controls.Add(this.DteNextCouponDt);
            this.TpgGeneral.Controls.Add(this.DteLastCouponDt);
            this.TpgGeneral.Controls.Add(this.label18);
            this.TpgGeneral.Controls.Add(this.label19);
            this.TpgGeneral.Controls.Add(this.label17);
            this.TpgGeneral.Controls.Add(this.TxtInterestFreq);
            this.TpgGeneral.Controls.Add(this.TxtInterestType);
            this.TpgGeneral.Controls.Add(this.TxtInterestRateAmt);
            this.TpgGeneral.Controls.Add(this.DteMaturityDt);
            this.TpgGeneral.Controls.Add(this.DteListingDt);
            this.TpgGeneral.Controls.Add(this.label16);
            this.TpgGeneral.Controls.Add(this.label15);
            this.TpgGeneral.Controls.Add(this.label14);
            this.TpgGeneral.Controls.Add(this.label13);
            this.TpgGeneral.Controls.Add(this.label12);
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.DteUpdateDt);
            this.TpgGeneral.Controls.Add(this.TxtSpecification);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.TxtIssuedAmt);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.TxtIssuer);
            this.TpgGeneral.Controls.Add(this.label7);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.LueCurCode);
            this.TpgGeneral.Controls.Add(this.label23);
            this.TpgGeneral.Controls.Add(this.LueInvestmentUomCode);
            this.TpgGeneral.Controls.Add(this.MeeRemark);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.LueInvestmentCtCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(942, 246);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // LueAnnualDays
            // 
            this.LueAnnualDays.EnterMoveNextControl = true;
            this.LueAnnualDays.Location = new System.Drawing.Point(609, 150);
            this.LueAnnualDays.Margin = new System.Windows.Forms.Padding(5);
            this.LueAnnualDays.Name = "LueAnnualDays";
            this.LueAnnualDays.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDays.Properties.Appearance.Options.UseFont = true;
            this.LueAnnualDays.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDays.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAnnualDays.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDays.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAnnualDays.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDays.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAnnualDays.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDays.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAnnualDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAnnualDays.Properties.DropDownRows = 30;
            this.LueAnnualDays.Properties.NullText = "[Empty]";
            this.LueAnnualDays.Properties.PopupWidth = 200;
            this.LueAnnualDays.Size = new System.Drawing.Size(319, 20);
            this.LueAnnualDays.TabIndex = 50;
            this.LueAnnualDays.ToolTip = "F4 : Show/hide list";
            this.LueAnnualDays.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAnnualDays.EditValueChanged += new System.EventHandler(this.LueAnnualDays_EditValueChanged);
            // 
            // DteNextCouponDt
            // 
            this.DteNextCouponDt.EditValue = null;
            this.DteNextCouponDt.EnterMoveNextControl = true;
            this.DteNextCouponDt.Location = new System.Drawing.Point(609, 69);
            this.DteNextCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteNextCouponDt.Name = "DteNextCouponDt";
            this.DteNextCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteNextCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteNextCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteNextCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteNextCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteNextCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteNextCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteNextCouponDt.Size = new System.Drawing.Size(116, 20);
            this.DteNextCouponDt.TabIndex = 42;
            // 
            // DteLastCouponDt
            // 
            this.DteLastCouponDt.EditValue = null;
            this.DteLastCouponDt.EnterMoveNextControl = true;
            this.DteLastCouponDt.Location = new System.Drawing.Point(609, 49);
            this.DteLastCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastCouponDt.Name = "DteLastCouponDt";
            this.DteLastCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteLastCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLastCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastCouponDt.Size = new System.Drawing.Size(116, 20);
            this.DteLastCouponDt.TabIndex = 40;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(497, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 14);
            this.label18.TabIndex = 41;
            this.label18.Text = "Next Coupon Date";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(501, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 14);
            this.label19.TabIndex = 39;
            this.label19.Text = "Last Coupon Date";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(467, 152);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(140, 14);
            this.label17.TabIndex = 49;
            this.label17.Text = "Annual Days Assumption";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestFreq
            // 
            this.TxtInterestFreq.EnterMoveNextControl = true;
            this.TxtInterestFreq.Location = new System.Drawing.Point(609, 129);
            this.TxtInterestFreq.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestFreq.Name = "TxtInterestFreq";
            this.TxtInterestFreq.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestFreq.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestFreq.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestFreq.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestFreq.Properties.MaxLength = 16;
            this.TxtInterestFreq.Properties.ReadOnly = true;
            this.TxtInterestFreq.Size = new System.Drawing.Size(319, 20);
            this.TxtInterestFreq.TabIndex = 48;
            // 
            // TxtInterestType
            // 
            this.TxtInterestType.EnterMoveNextControl = true;
            this.TxtInterestType.Location = new System.Drawing.Point(609, 109);
            this.TxtInterestType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestType.Name = "TxtInterestType";
            this.TxtInterestType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestType.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestType.Properties.MaxLength = 16;
            this.TxtInterestType.Properties.ReadOnly = true;
            this.TxtInterestType.Size = new System.Drawing.Size(319, 20);
            this.TxtInterestType.TabIndex = 46;
            // 
            // TxtInterestRateAmt
            // 
            this.TxtInterestRateAmt.EnterMoveNextControl = true;
            this.TxtInterestRateAmt.Location = new System.Drawing.Point(609, 89);
            this.TxtInterestRateAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestRateAmt.Name = "TxtInterestRateAmt";
            this.TxtInterestRateAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestRateAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestRateAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestRateAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestRateAmt.Properties.MaxLength = 16;
            this.TxtInterestRateAmt.Properties.ReadOnly = true;
            this.TxtInterestRateAmt.Size = new System.Drawing.Size(297, 20);
            this.TxtInterestRateAmt.TabIndex = 44;
            // 
            // DteMaturityDt
            // 
            this.DteMaturityDt.EditValue = null;
            this.DteMaturityDt.EnterMoveNextControl = true;
            this.DteMaturityDt.Location = new System.Drawing.Point(609, 29);
            this.DteMaturityDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteMaturityDt.Name = "DteMaturityDt";
            this.DteMaturityDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteMaturityDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteMaturityDt.Properties.Appearance.Options.UseFont = true;
            this.DteMaturityDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteMaturityDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteMaturityDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteMaturityDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteMaturityDt.Size = new System.Drawing.Size(116, 20);
            this.DteMaturityDt.TabIndex = 38;
            // 
            // DteListingDt
            // 
            this.DteListingDt.EditValue = null;
            this.DteListingDt.EnterMoveNextControl = true;
            this.DteListingDt.Location = new System.Drawing.Point(609, 9);
            this.DteListingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteListingDt.Name = "DteListingDt";
            this.DteListingDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteListingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteListingDt.Properties.Appearance.Options.UseFont = true;
            this.DteListingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteListingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteListingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteListingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteListingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteListingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteListingDt.Size = new System.Drawing.Size(116, 20);
            this.DteListingDt.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(494, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 14);
            this.label16.TabIndex = 47;
            this.label16.Text = "Interest Frequency";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(523, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Interest Type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(526, 92);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 14);
            this.label14.TabIndex = 43;
            this.label14.Text = "Interest Rate";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(525, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 14);
            this.label13.TabIndex = 37;
            this.label13.Text = "Maturity Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(535, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 14);
            this.label12.TabIndex = 35;
            this.label12.Text = "Listing Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(49, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 31;
            this.label11.Text = "Update Date";
            // 
            // DteUpdateDt
            // 
            this.DteUpdateDt.EditValue = null;
            this.DteUpdateDt.EnterMoveNextControl = true;
            this.DteUpdateDt.Location = new System.Drawing.Point(129, 130);
            this.DteUpdateDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUpdateDt.Name = "DteUpdateDt";
            this.DteUpdateDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteUpdateDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUpdateDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteUpdateDt.Properties.Appearance.Options.UseFont = true;
            this.DteUpdateDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUpdateDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUpdateDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUpdateDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUpdateDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUpdateDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUpdateDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUpdateDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUpdateDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUpdateDt.Size = new System.Drawing.Size(116, 20);
            this.DteUpdateDt.TabIndex = 32;
            // 
            // TxtSpecification
            // 
            this.TxtSpecification.EnterMoveNextControl = true;
            this.TxtSpecification.Location = new System.Drawing.Point(129, 110);
            this.TxtSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpecification.Name = "TxtSpecification";
            this.TxtSpecification.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpecification.Properties.Appearance.Options.UseFont = true;
            this.TxtSpecification.Properties.MaxLength = 250;
            this.TxtSpecification.Size = new System.Drawing.Size(319, 20);
            this.TxtSpecification.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(50, 113);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Specification";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIssuedAmt
            // 
            this.TxtIssuedAmt.EnterMoveNextControl = true;
            this.TxtIssuedAmt.Location = new System.Drawing.Point(129, 90);
            this.TxtIssuedAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIssuedAmt.Name = "TxtIssuedAmt";
            this.TxtIssuedAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIssuedAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIssuedAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIssuedAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtIssuedAmt.Properties.MaxLength = 16;
            this.TxtIssuedAmt.Properties.ReadOnly = true;
            this.TxtIssuedAmt.Size = new System.Drawing.Size(319, 20);
            this.TxtIssuedAmt.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(36, 92);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 14);
            this.label9.TabIndex = 27;
            this.label9.Text = "Issued Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIssuer
            // 
            this.TxtIssuer.EnterMoveNextControl = true;
            this.TxtIssuer.Location = new System.Drawing.Point(129, 50);
            this.TxtIssuer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIssuer.Name = "TxtIssuer";
            this.TxtIssuer.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIssuer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIssuer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIssuer.Properties.Appearance.Options.UseFont = true;
            this.TxtIssuer.Properties.MaxLength = 16;
            this.TxtIssuer.Properties.ReadOnly = true;
            this.TxtIssuer.Size = new System.Drawing.Size(319, 20);
            this.TxtIssuer.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(87, 52);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Issuer";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(72, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Currency";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(129, 30);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(319, 20);
            this.LueCurCode.TabIndex = 22;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(96, 73);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 14);
            this.label23.TabIndex = 25;
            this.label23.Text = "UoM";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentUomCode
            // 
            this.LueInvestmentUomCode.EnterMoveNextControl = true;
            this.LueInvestmentUomCode.Location = new System.Drawing.Point(129, 70);
            this.LueInvestmentUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentUomCode.Name = "LueInvestmentUomCode";
            this.LueInvestmentUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentUomCode.Properties.DropDownRows = 30;
            this.LueInvestmentUomCode.Properties.NullText = "[Empty]";
            this.LueInvestmentUomCode.Properties.PopupWidth = 200;
            this.LueInvestmentUomCode.Size = new System.Drawing.Size(319, 20);
            this.LueInvestmentUomCode.TabIndex = 26;
            this.LueInvestmentUomCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(129, 150);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(319, 20);
            this.MeeRemark.TabIndex = 34;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(80, 153);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Investment Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentCtCode
            // 
            this.LueInvestmentCtCode.EnterMoveNextControl = true;
            this.LueInvestmentCtCode.Location = new System.Drawing.Point(129, 9);
            this.LueInvestmentCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentCtCode.Name = "LueInvestmentCtCode";
            this.LueInvestmentCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentCtCode.Properties.DropDownRows = 30;
            this.LueInvestmentCtCode.Properties.NullText = "[Empty]";
            this.LueInvestmentCtCode.Properties.PopupWidth = 350;
            this.LueInvestmentCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueInvestmentCtCode.TabIndex = 20;
            this.LueInvestmentCtCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // BtnPortofolio2
            // 
            this.BtnPortofolio2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPortofolio2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPortofolio2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPortofolio2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPortofolio2.Appearance.Options.UseBackColor = true;
            this.BtnPortofolio2.Appearance.Options.UseFont = true;
            this.BtnPortofolio2.Appearance.Options.UseForeColor = true;
            this.BtnPortofolio2.Appearance.Options.UseTextOptions = true;
            this.BtnPortofolio2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPortofolio2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPortofolio2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPortofolio2.Image")));
            this.BtnPortofolio2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPortofolio2.Location = new System.Drawing.Point(456, 48);
            this.BtnPortofolio2.Name = "BtnPortofolio2";
            this.BtnPortofolio2.Size = new System.Drawing.Size(24, 21);
            this.BtnPortofolio2.TabIndex = 33;
            this.BtnPortofolio2.ToolTip = "Find Item";
            this.BtnPortofolio2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPortofolio2.ToolTipTitle = "Run System";
            this.BtnPortofolio2.Click += new System.EventHandler(this.BtnPortofolio2_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(909, 93);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(19, 14);
            this.label29.TabIndex = 74;
            this.label29.Text = "%";
            // 
            // FrmInvestmentItemDebt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 375);
            this.Name = "FrmInvestmentItemDebt";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortofolioId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentDebtCode.Properties)).EndInit();
            this.TpgDetail.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAnnualDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFreq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRateAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuedAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        internal DevExpress.XtraEditors.TextEdit TxtPortofolioId;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnPortofolio1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        internal DevExpress.XtraEditors.TextEdit TxtForeignName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentDebtCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TpgDetail;
        private System.Windows.Forms.TabPage TpgGeneral;
        internal DevExpress.XtraEditors.TextEdit TxtInterestFreq;
        internal DevExpress.XtraEditors.TextEdit TxtInterestType;
        internal DevExpress.XtraEditors.TextEdit TxtInterestRateAmt;
        internal DevExpress.XtraEditors.DateEdit DteMaturityDt;
        internal DevExpress.XtraEditors.DateEdit DteListingDt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteUpdateDt;
        internal DevExpress.XtraEditors.TextEdit TxtSpecification;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtIssuedAmt;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtIssuer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.LookUpEdit LueInvestmentCtCode;
        internal DevExpress.XtraEditors.LookUpEdit LueInvestmentUomCode;
        public DevExpress.XtraEditors.SimpleButton BtnPortofolio2;
        internal DevExpress.XtraEditors.DateEdit DteNextCouponDt;
        internal DevExpress.XtraEditors.DateEdit DteLastCouponDt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.LookUpEdit LueAnnualDays;
        private System.Windows.Forms.Label label29;
    }
}