﻿#region Update
/*
   21/04/2022 [HAR/GSS] tambah input COA
   12/05/2022 [RDA/PRODUCT] resize action bar
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvestmentCategory mFrmParent;
        private string mSQL = string.Empty;
        #endregion

        #region Constructor

        public FrmInvestmentCategoryFind(FrmInvestmentCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.InvestmentCtCode, A.InvestmentCtName, A.ActInd, ");
            SQL.AppendLine("A.AcNo, A.AcNo2, A.Acno3, A.AcNo4, A.Acno5, A.Acno6, A.Acno7, A.Acno8, A.AcNo9, ");
            SQL.AppendLine("B.AcDesc, C.AcDesc As AcDesc2, ");
            SQL.AppendLine("D.AcDesc As AcDesc3, E.AcDesc As AcDesc4, ");
            SQL.AppendLine("F.AcDesc As AcDesc5, G.AcDesc As AcDesc6, ");
            SQL.AppendLine("H.AcDesc As AcDesc7, I.AcDesc As AcDesc8, ");
            SQL.AppendLine("J.AcDesc As AcDesc9, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblInvestmentCategory A ");
            SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo3=D.AcNo ");
            SQL.AppendLine("Left Join TblCoa E On A.AcNo4=E.AcNo ");
            SQL.AppendLine("Left Join TblCoa F On A.AcNo5=F.AcNo ");
            SQL.AppendLine("Left Join TblCoa G On A.AcNo6=G.AcNo ");
            SQL.AppendLine("Left Join TblCoa H On A.AcNo7=H.AcNo ");
            SQL.AppendLine("Left Join TblCoa I On A.AcNo8=I.AcNo ");
            SQL.AppendLine("Left Join TblCoa J On A.AcNo9=J.AcNo ");

            mSQL = SQL.ToString();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "COA#"+Environment.NewLine+"(Stock)",
                        "COA Description"+Environment.NewLine+"(Stock)",
                        //6-10
                        "COA#"+Environment.NewLine+"(sales)",
                        "COA Description"+Environment.NewLine+"(sales)",
                        "COA#"+Environment.NewLine+"(receivable)",
                        "COA Description"+Environment.NewLine+"(receivable)",
                        "COA#"+Environment.NewLine+"(payable)",
                        //11-15
                        "COA Description"+Environment.NewLine+"(payable)",
                         "COA#"+Environment.NewLine+"(Loss FVPL)",
                        "COA Description"+Environment.NewLine+"(Loss FVPL)",
                        "COA#"+Environment.NewLine+"(Loss FVOCI)",
                        "COA Description"+Environment.NewLine+"(Loss FVOCI)",
                        //16-20
                        "COA#"+Environment.NewLine+"(Realized Gain/Loss)",
                        "COA Description"+Environment.NewLine+"(Realized Gain/Loss)",
                        "COA#"+Environment.NewLine+"(Interest Income)",
                        "COA Description"+Environment.NewLine+"(Interest Income)",
                        "COA#"+Environment.NewLine+"(Interest Receivable)",
                        //21-25
                        "COA Description"+Environment.NewLine+"(Interest Receivable)",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By", 
                        //26-27
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    { 
                        //0
                        50,
                        //1-5
                        100, 180, 60, 80, 150,
                        //6-10
                        80, 150, 80, 150, 80, 
                        //11-15
                        150, 80, 150, 80, 150,
                        //16-20
                        80, 150, 80, 150, 80,
                        //21-25
                        150, 100, 100, 80, 100,
                        //26
                        100, 80
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCtName.Text, new string[] { "InvestmentCtCode", "InvestmentCtName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By InvestmentCtName;",
                    new string[]
                    {
                        //0
                        "InvestmentCtCode", 
                            
                        //1-5
                        "InvestmentCtName", "ActInd", "AcNo", "AcDesc", "AcNo2", 
                        
                        //6-10
                        "AcDesc2", "AcNo3", "AcDesc3", "AcNo4", "AcDesc4",  

                        //11-15
                        "AcNo5", "AcDesc5", "AcNo6", "AcDesc6",  "AcNo7", 
                        
                        //16-20
                        "AcDesc7", "AcNo8", "AcDesc8", "AcNo9", "AcDesc9",
                        //21-24
                        "CreateBy", "CreateDt",  "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment category");
        }

        #endregion

        #endregion
    }
}
