﻿#region Update
/*
 * 
    16/06/2020 [VIN/SIER] sales target yearly
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesTargetFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesTarget _frmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesTargetFind(FrmSalesTarget FrmParent)
        {
            InitializeComponent();
            _frmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = _frmParent.Text;
                Sm.ButtonVisible(_frmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                //SetLueMonth(ref LueMth);
                if (_frmParent.mIsSalesTargetYearly)
                    LueMth.Text = string.Empty;
                else
                    Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                   Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Unique Code",
                        "Sales"+Environment.NewLine+"Code", 
                        "Sales"+Environment.NewLine+"Name",
                        "Cancel",
                        "Year",  
                       
                        //6-10
                        "Month",
                        "Amount",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",

                        //11-13
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        0, 100, 150, 60, 70,  
                        
                        //6-10
                        100, 100, 100, 100, 100,   
                        
                        //11-13
                        100, 100, 100

                    }
               );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 8, 9, 10, 11, 12, 13 }, false);
            //if(_frmParent.mIsSalesTargetYearly) 
            //    Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.UniqueCode, A.SpCode, B.SpName, A.CancelInd, A.Yr, A.Amt, ");
            SQL.AppendLine("CASE A.Mth ");
            SQL.AppendLine("  WHEN '01' THEN 'January' ");
            SQL.AppendLine("  WHEN '02' THEN 'February' ");
            SQL.AppendLine("  WHEN '03' THEN 'March' ");
            SQL.AppendLine("  WHEN '04' THEN 'April' ");
            SQL.AppendLine("  WHEN '05' THEN 'May' ");
            SQL.AppendLine("  WHEN '06' THEN 'June' ");
            SQL.AppendLine("  WHEN '07' THEN 'July' ");
            SQL.AppendLine("  WHEN '08' THEN 'August' ");
            SQL.AppendLine("  WHEN '09' THEN 'September' ");
            SQL.AppendLine("  WHEN '10' THEN 'October' ");
            SQL.AppendLine("  WHEN '11' THEN 'November' ");
            SQL.AppendLine("  WHEN '12' THEN 'December' ");
            SQL.AppendLine("END AS Mth, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM TblSalesTarget A  ");
            SQL.AppendLine("INNER JOIN TblSalesPerson B ON A.SpCode = B.SpCode ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsLueEmpty(LueYr, "Year") ||
                    (!_frmParent.mIsSalesTargetYearly && Sm.IsLueEmpty(LueMth, "Month"))
                    ) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " WHERE 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                if (!_frmParent.mIsSalesTargetYearly)
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSpCode.Text, new string[] { "A.SpCode", "B.SpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[]
                        {
                            //0
                            "UniqueCode", 
                                
                            //1-5
                            "SpCode", "SpName", "CancelInd", "Yr", "Mth",   
                            
                            //6-10
                            "Amt", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                _frmParent.mSpCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                _frmParent.mUniqueCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                _frmParent.ShowData(_frmParent.mUniqueCode, _frmParent.mSpCode);
                this.Hide();
            }
        }

        #region Additional Method

        public static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "select '01' As Col1, 'January' As Col2 union All " +
                "select '02','February' Union All " +
                "select '03','March' Union All " +
                "select '04','April' Union All " +
                "select '05','May' Union All " +
                "select '06','June' Union All " +
                "select '07','July' Union All " +
                "select '08','August' Union All " +
                "select '09','September' Union All " +
                "select '10','October' Union All " +
                "select '11','November' Union All " +
                "select '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Methods

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Misc Control Events

        private void TxtSpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }

        #endregion

        #endregion

    }
}
