﻿#region Update
/* 
    10/09/2018 [HAR] tambah informasi log meskipun working schdulenya holiday 
 */
#endregion


#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAtd2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmAtd2Find FrmFind;
        decimal Delay = Decimal.Parse(Sm.GetParameter("DelayTolerance"));
        decimal RoundSch = Decimal.Parse(Sm.GetParameter("RoundingValueSchedule"));
        private bool mIsAtdShowDataLogWhenHoliday = false;

        #endregion

        #region Constructor

        public FrmAtd2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Attendance";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLueAGCode(ref LueAGCode);
            GetParameter();

            //if (mDocNo.Length != 0)
            //{
            //    ShowData(mDocNo);
            //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            //}
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd1, 0, 0, "Employee" + Environment.NewLine + "Code", 2, 80);
            SetGrdHdr(ref Grd1, 0, 1, "Employee Old" + Environment.NewLine + "Code", 2, 100);
            SetGrdHdr(ref Grd1, 0, 2, "Employee", 2, 150);
            SetGrdHdr(ref Grd1, 0, 3, "", 2, 20);
            SetGrdHdr(ref Grd1, 0, 4, "Department", 2, 150);
            SetGrdHdr(ref Grd1, 0, 5, "Date", 2, 80);
            SetGrdHdr2(ref Grd1, 1, 6, "Attendance" + Environment.NewLine + "Log", 2);
            SetGrdHdr(ref Grd1, 0, 8, "Duration Work" + Environment.NewLine + "(Hour)", 2, 90);
            SetGrdHdr2(ref Grd1, 1, 9, "Working" + Environment.NewLine + "Schedule", 2);
            SetGrdHdr(ref Grd1, 0, 11, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + " Duration Work", 2, 100);
            SetGrdHdr(ref Grd1, 0, 12, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + "Working Schedule", 2, 120);
            SetGrdHdr2(ref Grd1, 1, 13, "Actual (Input)", 2);
            SetGrdHdr3(ref Grd1, 1, 15, "Actual", 4);
            SetGrdHdr(ref Grd1, 0, 19, "Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 20, "Resign Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 21, "No", 2, 30);
            SetGrdHdr(ref Grd1, 0, 22, "Check", 2, 50);

            Sm.GrdFormatDate(Grd1, new int[] { 5, 15, 17, 19 });
            Sm.GrdFormatDec(Grd1, new int[] {8}, 0);
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7, 9, 10});
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 15, 16, 17, 18, 19, 20, 21  });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColCheck(Grd1, new int[] { 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5, 11, 20 }, false);
            Grd1.Cols[21].Move(0);
            Grd1.Cols[22].Move(1);
            Grd1.Cols[19].Move(2);
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "OUT", 1, 80);
        }

        private void SetGrdHdr3(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "Date IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "Time IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "Date OUT", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "Time OUT", 1, 80);
        }

        private void ChkHideInfoInGrd_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, LueDeptCode, LueAGCode, DteStartDt, DteEndDt, LueYr, LueMth, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
                    DteStartDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, LueDeptCode, LueAGCode, DteStartDt, DteEndDt, LueYr, LueMth, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 13, 14 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 13, 14 });
                    ChkCancelInd.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueAGCode, DteStartDt, DteEndDt,
                LueYr, LueMth, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAtd2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueMth(LueMth);
            Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

            Sm.SetDteCurrentDate(DteDocDt);
            DteStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            DteEndDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsGrdMonthNotValid()) return;
                Sm.ClearGrd(Grd1, false);
                DateTime Dt1 = new DateTime(
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                       Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                       0, 0, 0
                       );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                    0, 0, 0
                    );

                var range = (Dt2 - Dt1).Days;
                ListOfEmp();

                if (range >= 0)
                {
                    //ReListEmp_Old(range);
                    ReListEmp();
                }
                ComputeVs(6, 9, 12);
                //IntervalIsMin();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
      
        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }           
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_Validated(object sender, EventArgs e)
        {
            
        }

        private void Grd1_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private void Grd1_TextBoxTextChanged(object sender, iGTextBoxTextChangedEventArgs e)
        {
            //if (e.ColIndex == 13)
            //{
            //    string ins = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
            //    if (ins.Length == 4 || ins.Length == 3)
            //    {
            //        Sm.FocusGrd(Grd1, e.RowIndex, 14);
            //        SetTime(Grd1, e.RowIndex, 16, 13);
            //    }
            //    else
            //    {
            //        Grd1.Cells[e.RowIndex, 16].Value = "";
            //    }
            //}

            //if (e.ColIndex == 14)
            //{
            //    string outs = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
            //    if (outs.Length == 4 || outs.Length == 3)
            //    {
            //        Sm.FocusGrd(Grd1, (e.RowIndex + 1), 13);
            //        SetTime(Grd1, e.RowIndex, 18, 14);
            //    }
            //    else
            //    {
            //        Grd1.Cells[e.RowIndex, 18].Value = "";
            //    }
            //}
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            var Value = string.Empty;
            if (e.ColIndex == 13)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime(Grd1, e.RowIndex, 16, 13);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                        Grd1.Cells[e.RowIndex, 16].Value = string.Empty;
                }
                Sm.FocusGrd(Grd1, e.RowIndex, 14);
            }

            if (e.ColIndex == 14)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime(Grd1, e.RowIndex, 18, 14);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                        Grd1.Cells[e.RowIndex, 18].Value = string.Empty ;
                }
                Sm.FocusGrd(Grd1, (e.RowIndex + 1), 13);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ATD", "TblAtdHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAtdHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && Sm.GetGrdStr(Grd1, Row, 18).Length >0 )
                    cml.Add(SaveAtdDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsGrdEmpty() ||
                IsGrdTimeNotValid() ||
                IsGrdMonthNotValid()||
                IsGrdOneOfActualEmpty()||
                IsGrdAlreadyExist()
                ;
        }

  
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input attendance.");
                return true;
            }
            return false;
        }

        private bool IsGrdMonthNotValid()
        {
            string Msg = "";
            if (Sm.GetDte(DteStartDt).Substring(4, 2) != Sm.GetDte(DteEndDt).Substring(4, 2))
            {
                Sm.StdMsg(mMsgType.Warning,
                    Msg +
                    "Amandment have different month");
                return true;
            }
            return false;
        }

        private bool IsGrdAlreadyExist()
        {
            var SQL = new StringBuilder();

            int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
            string dayPlus = Sm.GetDte(DteStartDt).Substring(0, 8);

            //SQL.AppendLine("Select * From TblAtdhdr ");
            //SQL.AppendLine("Where DeptCode='"+Sm.GetLue(LueDeptCode)+"' And CancelInd = 'N' And Exists ( ");
            //SQL.AppendLine("    Select *  From TblAtdHdr Where ");

            SQL.AppendLine("Select * from tblatdhdr where DeptCode ='" + Sm.GetLue(LueDeptCode) + "' And CancelInd = 'N' And TypeInd ='2' ");
            if(Sm.GetLue(LueAGCode).Length !=0)
                    SQL.AppendLine(" And AgCode = '"+Sm.GetLue(LueAGCode)+"' ");
            SQL.AppendLine(" And Exists ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select * from tblAtdHdr ");
            SQL.AppendLine("    Where DeptCode='" + Sm.GetLue(LueDeptCode) + "' And CancelInd = 'N' And TypeInd = '2' ");
            if (Sm.GetLue(LueAGCode).Length != 0)
                SQL.AppendLine(" And AgCode = '" + Sm.GetLue(LueAGCode) + "' ");
            SQL.AppendLine("    And (StartDt in ( ");

            for (int Row = 0; Row <= range; Row++)
            {
                SQL.AppendLine("Select '" + Sm.GetValue("Select Replace(date_add('" + dayPlus + "', interval '" + Row + "' day), '-', '') As Day ") + "'  ");
                if (Row != range)
                {
                    SQL.AppendLine("Union ALL ");
                }
            }
            SQL.AppendLine("   ) Or EndDt in ( ");

            for (int Row = 0; Row <= range; Row++)
            {
                SQL.AppendLine("Select '" + Sm.GetValue("Select Replace(date_add('" + dayPlus + "', interval '" + Row + "' day), '-', '') As Day ") + "'  ");
                if (Row != range)
                {
                    SQL.AppendLine("Union ALL ");
                }
            }
            SQL.AppendLine(" ) )) ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Amendment for this periode has been created.");
                return true;
            }
            return false;
        }

        private bool IsGrdOneOfActualEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && Sm.GetGrdStr(Grd1, Row, 18).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual Out.");
                    Sm.FocusGrd(Grd1, Row, 14);
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 18).Length > 0 && Sm.GetGrdStr(Grd1, Row, 16).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual In.");
                    Sm.FocusGrd(Grd1, Row, 13);
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdActualEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 15).Length != 0 && Sm.GetGrdStr(Grd1, Row, 16).Length == 0 )
                {
                    if (Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8)) <= Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 20)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to input Actual Out.");
                        Sm.FocusGrd(Grd1, Row, 14);
                        return true;
                    }
                }
                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 16).Length != 0 && Sm.GetGrdStr(Grd1, Row, 15).Length == 0 )
                {
                    if (Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8)) <= Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 20)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to input Actual In.");
                        Sm.FocusGrd(Grd1, Row, 13);
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 16) == "" && Sm.GetGrdStr(Grd1, Row, 18) != "")
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual In.");
                    Sm.FocusGrd(Grd1, Row, 13);
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 16) != "" && Sm.GetGrdStr(Grd1, Row, 18) == "")
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual Out.");
                    Sm.FocusGrd(Grd1, Row, 14);
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdTimeNotValid()
        {
            string Msg = "";

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                {
                    string InA;
                    string OutA;

                    Msg =
                       "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                       "Employee : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                       "Date : " + Sm.GetGrdStr(Grd1, Row, 19) + Environment.NewLine;

                    if (Sm.GetGrdStr(Grd1, Row, 16).Length != 0 && Sm.GetGrdStr(Grd1, Row, 16) != "")
                    {
                        InA = Sm.GetGrdStr(Grd1, Row, 16);
                        if (Decimal.Parse(RemoveQuotes(InA)) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Actual Time In not Valid");
                            Sm.FocusGrd(Grd1, Row, 13);
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 18).Length != 0 && Sm.GetGrdStr(Grd1, Row, 18) != "")
                    {
                        OutA = Sm.GetGrdStr(Grd1, Row, 18);
                        if (Decimal.Parse(RemoveQuotes(OutA)) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Actual Time Out not Valid");
                            Sm.FocusGrd(Grd1, Row, 14);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

     
        private MySqlCommand SaveAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAtdHdr(DocNo, DocDt, CancelInd, TypeInd, Period, DeptCode, AGCode, StartDt, EndDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, '2', @Period, @DeptCode, @AGCode, @StartDt, @EndDt, @Remark, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAtdDtl(string DocNo, int Row)
        {
            string AIn, LIn, DtIn = string.Empty;
            string AOut, LOut, DtOut = string.Empty;

            var SQL = new StringBuilder();

            
            SQL.AppendLine("Insert Into TblAtdDtl(DocNo, EmpCode, Dt, InOutL1, InOutA1, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @EmpCode, @Dt, @InOutL1, @InOutA1, @UserCode, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate key ");
            //SQL.AppendLine("  Update EmpCode=@EmpCode, Dt=@Dt, InOutL1=@InOutL1, InOutA1=@InOutA1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            //SQL.AppendLine("  Update TblAttendanceLog Set ProcessInd = 'Y' Where EmpCode=@EmpCode And Dt=@Dt; ");  
            //SQL.AppendLine("Delete from TblAtdDtl Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            LIn = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 6));
            LOut = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7));
            AIn = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 16));
            AOut = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 18));
            DtIn = Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8);
            DtOut = Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8);
            if (Sm.GetGrdStr(Grd1, Row, 16) == "")
            {
                AIn = "XXXX";
            }
            if (Sm.GetGrdStr(Grd1, Row, 18) == "")
            {
                AOut = "XXXX";
            }
            if (Sm.GetGrdStr(Grd1, Row, 6) == "")
            {
                LIn = "XXXX";
            }
            if (Sm.GetGrdStr(Grd1, Row, 7) == "")
            {
                LOut = "XXXX";
            }
            if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 &&  Sm.GetGrdStr(Grd1, Row, 18).Length > 0 && Decimal.Parse(AIn) < Decimal.Parse(AOut))
            {
                DtIn = Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8);
                DtOut = Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8);
            }

            if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && Sm.GetGrdStr(Grd1, Row, 18).Length > 0 && Decimal.Parse(AIn) > Decimal.Parse(AOut))
            {
                DtIn = Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8);
                DtOut = Sm.GetValue("Select Date_Format(Date_Add(STR_TO_DATE(" + Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8) + ", '%Y%m%d'), Interval 1 Day), '%Y%m%d')");
            }

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParamDt(ref cm, "@Dt", DtIn);
            
            Sm.CmParam<String>(ref cm, "@InOutL1", String.Concat(DtIn + LIn, DtOut + LOut));
            Sm.CmParam<String>(ref cm, "@InOutA1", String.Concat(DtIn+ AIn, DtOut + AOut));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

  
        #endregion

        #region Update data
        private void UpdateData()
        {
            if (IsInsertedDataNotValid2() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = TxtDocNo.Text;

            var cml2 = new List<MySqlCommand>();

            cml2.Add(CancelAtdHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && Sm.GetGrdStr(Grd1, Row, 18).Length > 0)
                {
                    cml2.Add(SaveAtdDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml2);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid2()
        {
            return
                IsGrdEmpty2() ||
                IsGrdOneOfActualEmpty()||
                IsGrdTimeNotValid()||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblAtdHdr Where DocNo=@DocNo And CancelInd = 'Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "this document already cancelled .");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data.");
                return true;
            }
            return false;
        }


        private MySqlCommand CancelAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAtdHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete from TblAtdDtl Where DocNo = @DocNo; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand UpdateAtdLog(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAttendanceLog Set ProcessInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime()");
            SQL.AppendLine("Where EmpCode = @EmpCode And Dt = @Dt; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd1, Row, 17));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowAtdHdr(DocNo);
                
                ListOfEmp();
                ReListDataDetail(DocNo);
                ReListEmpDurationWork();
                ComputeVs(6, 9, 12);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowAtdHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, Left(A.Period, 4) As PYr, Right(A.Period, 2) PMth,  " +
                    "A.StartDt, A.EndDt, A.DeptCode, A.AGCode, A.remark From TblAtdHdr A Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "PYr", "PMth", "StartDt",  
                        //6
                        "EndDt", "DeptCode", "AGCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sl.SetLueMth(LueMth);
                        Sl.SetLueYr(LueYr, "");
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[7]));
                        SetLueAGCode(ref LueAGCode, string.Empty, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueAGCode, Sm.DrStr(dr, c[8]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    }, true
                );
        }

        private void ReListDataDetail(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.Dt, ");
            SQL.AppendLine("Substring(InOutL1, 9, 4) As LIn, Right(InOutL1, 4) As LOut, ");
            SQL.AppendLine("Substring(InOutA1, 9, 4) As AIn, ");
            SQL.AppendLine("Right(InOutA1, 4) As AOut,  ");
            SQL.AppendLine("Left(InOutA1, 8) As Dt1,  ");
            SQL.AppendLine("Substring(InOutA1, 13, 8) As Dt2 ");
            SQL.AppendLine("From TblAtdDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("left Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Where A.DocNo='" + DocNo + "' Order by B.EmpName, A.Dt ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                       "EmpCode",
                        //1-5
                        "Dt", "LIn", "LOut", "AIn", "AOut", 
                        //6-7
                        "Dt1", "Dt2" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0) == Sm.DrStr(dr, 0) &&
                                Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8) == Sm.DrStr(dr, 1))
                            {
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 1);
                                if (Sm.DrStr(dr, 2) == "XXXX")
                                {
                                    Grd1.Cells[Row, 6].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 6, 2);
                                }

                                if (Sm.DrStr(dr, 3) == "XXXX")
                                {
                                    Grd1.Cells[Row, 7].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 7, 3);
                                }

                                if (Sm.DrStr(dr, 4) == "XXXX")
                                {
                                    Grd1.Cells[Row, 13].Value =
                                    Grd1.Cells[Row, 16].Value = ""; 
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 13, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 16, 4);
                                }

                                if (Sm.DrStr(dr, 5) == "XXXX")
                                {
                                    Grd1.Cells[Row, 14].Value =
                                    Grd1.Cells[Row, 18].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 14, 5);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 18, 5);
                                }
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 6);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 17, 7);
                               
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

     
       
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            Delay = Sm.GetParameterDec("DelayTolerance");
            RoundSch = Sm.GetParameterDec("RoundingValueSchedule");
            mIsAtdShowDataLogWhenHoliday = Sm.GetParameterBoo("IsAtdShowDataLogWhenHoliday");
        }

        public void AddGrid(int Rowr)
        {
            SetGrd();
            int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
            //int Rowr = 0; 
            string dayPlus;
            string RowCol = Sm.GetDte(DteStartDt).Substring(6, 2);
            while (Rowr <= range)
            {
                Grd1.Rows.Add();
                int dayAdd = Convert.ToInt32(RowCol) + Rowr;
                if (dayAdd.ToString().Length != 2)
                {
                    dayPlus = "0" + dayAdd.ToString();
                }
                else
                {
                    dayPlus = dayAdd.ToString();
                }
                Grd1.Cells[Rowr, 14].Value = Sm.GetDte(DteStartDt).Substring(0, 6) + dayPlus;
                Rowr++;
            }
        }

        public void SetTime(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            try
            {
                for (int Row = 0; Row < GrdXXX.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                    {
                        GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                        GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                    }
                    if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                    {
                        GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat("0", Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                        GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        static string RemoveQuotes(string input)
        {
            int index = 0;
            char[] result = new char[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != ':')
                {
                    result[index++] = input[i];
                }
            }
            return new string(result, 0, index);
        }

        public void ListOfEmp()
        {
                string days = Sm.GetDte(DteStartDt).Substring(0, 6);

                var l = new List<Employee>();

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                DateTime Dt1 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                    0, 0, 0
                    );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                    0, 0, 0
                    );

                if (Dt1 > Dt2) 
                {
                    Sm.StdMsg(mMsgType.Warning, "Range Date not valid");
                    return;
                }

                var range = (Dt2 - Dt1).Days;
                DateTime TempDt = Dt1;

                //int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
                string RowCol = Sm.GetDte(DteStartDt).Substring(6, 2);

                var SQL = new StringBuilder();
               
                    //SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, B.DeptName, ifnull(A.ResignDt, 0) As ResignDt From tblEmployee A ");
                    //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                    //SQL.AppendLine("Where (A.ResignDt Is Null Or  ");
                    //SQL.AppendLine("(ResignDt Is Not Null And  ( A.ResignDt >  '"+Sm.GetDte(DteStartDt).Substring(0, 8)+"' And A.ResignDt < '"+Sm.GetDte(DteEndDt).Substring(0, 8)+"')) ) And A.DeptCode=@DeptCode And A.SystemType = '2' ");
                    //SQL.AppendLine("Order By A.EmpName");
                
                SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, B.DeptName, ");
                SQL.AppendLine("IfNull(A.ResignDt, 0) As ResignDt, ");
                SQL.AppendLine("C.Dt As DtList,  E.In1, E.Out1, D.Dt As DtWs  ");
                SQL.AppendLine("From TblEmployee A  ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Inner Join ( ");
                for (int a = 0; a <= range; a++)
                {
                    int RowCols = Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(6, 2)) + a;
                    SQL.AppendLine("    Select Convert('" + days + RowCols.ToString("00") + "' Using Latin1) As Dt ");
                    if (a != range)
                    {
                        SQL.AppendLine("    Union All ");
                    }
                }
                SQL.AppendLine("    ) C On 0=0 ");
                SQL.AppendLine("Left Join TblEmpWorkSchedule D On A.EmpCode=D.EmpCode And C.Dt=D.Dt ");
                SQL.AppendLine("Left Join tblWorkSchedule E On D.WsCode=E.WsCode And E.ActInd = 'Y' And E.HolidayInd = 'N' ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("(   Select Y.EmpCode, Y.AGCode ");
                SQL.AppendLine("    From TblAttendanceGrpHdr X ");
                SQL.AppendLine("    Inner Join TblAttendanceGrpDtl Y On X.AgCode = Y.AGCode ");
                SQL.AppendLine("    Where X.ActInd = 'Y' ");
                SQL.AppendLine(" )F On A.EmpCode = F.EmpCode ");   
                SQL.AppendLine("Where (A.ResignDt Is Null Or  ");
                SQL.AppendLine("(ResignDt Is Not Null And  ( A.ResignDt >@DocDt1 And A.ResignDt <@DocDt2)) ) ");
                SQL.AppendLine("And A.SystemType = '2' ");
                SQL.AppendLine("And A.JoinDt <= @DocDt2 ");
                if (Sm.GetLue(LueDeptCode).Length > 0)
                    SQL.AppendLine("And A.DeptCode=@DeptCode ");
                if (Sm.GetLue(LueAGCode).Length > 0)
                    SQL.AppendLine("And F.AGCode=@AGCode");
                SQL.AppendLine("Order By A.EmpName, C.Dt;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                    Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteStartDt));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteEndDt));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "EmpCode", 
                         "EmpCodeOld", "EmpName", "DeptName", "ResignDt", "DtList",
                         "In1", "Out1"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                                Grd1.Rows.Add();
                                int Row = Grd1.Rows.Count-1;
                                Grd1.Cells[Row, 21].Value = Row + 1;
                                Grd1.Cells[Row, 0].Value = dr.GetString(c[0]);
                                Grd1.Cells[Row, 1].Value = dr.GetString(c[1]);
                                Grd1.Cells[Row, 2].Value = dr.GetString(c[2]);
                                Grd1.Cells[Row, 4].Value = dr.GetString(c[3]);

                                if (dr.GetString(c[4]) == "0")
                                    Grd1.Cells[Row, 20].Value = null;
                                else
                                    Grd1.Cells[Row, 20].Value = dr.GetString(c[4]);
                                if (dr.GetString(c[5]).Length==0)
                                    Grd1.Cells[Row, 19].Value = null;
                                else
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 5);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 9, 6);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 7);
                                CekResign(Row);                            
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                Grd1.Rows.Add();
        }

        private void ReListEmp_Old(int rangeDay)
        {
            var SQL = new StringBuilder();

            string day = Sm.GetDte(DteStartDt).Substring(0, 6);

            if (TxtDocNo.Text.Length != 0)
            {
                SQL.AppendLine("Select A.EmpCode, '1' As Tmind,  Left(InOutA1, 8) As Date, Substring(InOutA1, 13, 8) As Dt2, ");
                SQL.AppendLine("Substring(InOutA1, 9, 4) As AIn,  Right(InOutA1, 4) As AOut  ");
                SQL.AppendLine("From TblAtdHdr Az ");
                SQL.AppendLine("Inner Join TblAtdDtl A On Az.DocNo = A.DocNo ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("left Join TblDepartment C On B.DeptCode = C.DeptCode ");
                SQL.AppendLine("Where Az.DocNo = '" + TxtDocNo.Text + "'");
                SQL.AppendLine("UNION ALL ");
            }          

	        SQL.AppendLine("Select T4.EmpCode, ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then '2' Else '1' End As TmInd, T2.Dt As Date, ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T2.Dt2 else T2.Dt end As Dt2, ");
            //SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MaxTm2 Else T4.MinTm1 End As AIn,  ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MinTm1 Else T4.MinTm1 End As AIn,  ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MaxTm1 Else T4.MaxTm1 End As AOut  ");
            SQL.AppendLine("From TblEmployee T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("    Date_Format(Date_Add(STR_TO_DATE(Dt, '%Y%m%d'), Interval 1 Day), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("    From TblEmpWorkSchedule  ");
            SQL.AppendLine("    Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(") T2 On T1.EmpCode=T2.EmpCode  ");
            SQL.AppendLine("Inner Join TblWorkSchedule T3 On T2.WsCode=T3.WsCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("        Select A1.EmpCode, A1.Dt, A.MinTm As MinTm1, B.MaxTm As MaxTm1, C.MinTm As MinTm2, D.MaxTm As MaxTm2 ");
            SQL.AppendLine("        From ( ");

            SQL.AppendLine("            Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("            DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("            DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("            From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("        )A1");
            SQL.AppendLine("            left Join ( ");
            SQL.AppendLine("                Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                    DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1,");
            SQL.AppendLine("                    DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                    From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("                ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode And X1.Dt = X3.Dt");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) A On A1.EmpCode = A.EmpCode And A1.Dt = A.Dt ");
            
            #region Old
            //SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            //SQL.AppendLine("            From ( ");
            //SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            //SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            //SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            //SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            ) X1 ");
            //SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            //SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            //SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            //SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            //SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            //SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            //SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            //SQL.AppendLine("        ) A ");
            #endregion

            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Max(Left(X3.Tm, 4)) As MaxTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bOut1<=X2.Out1 Then X1.Dt Else Dt1 End, X2.bOut1) And ");
            SQL.AppendLine("                Concat(Case When X2.Out1<=X2.aOut1 Then X1.Dt Else Dt2 End, X2.aOut1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) B On A1.EmpCode=B.EmpCode And A1.Dt=B.Dt ");

            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt2 ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt2 ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bOut1<=X2.Out1 Then X1.Dt2 Else X1.Dt End, X2.bOut1) And ");
            SQL.AppendLine("                Concat(X1.Dt2, X2.aOut1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) C  On A1.EmpCode=C.EmpCode And A1.Dt=C.Dt ");

            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Max(Left(X3.Tm, 4)) As MaxTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) D On A1.EmpCode=D.EmpCode And A1.Dt=D.Dt ");
            SQL.AppendLine(") T4  ");
            SQL.AppendLine("On T2.EmpCode=T4.EmpCode  ");
            SQL.AppendLine("And T2.Dt=T4.Dt  ");
            SQL.AppendLine("Where T1.JoinDt<=@EndDt And T1.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T1.ResignDt is Null Or (T1.ResignDt is Not Null And T1.ResignDt>=@StartDt)) ");
            SQL.AppendLine("Group By T4.EmpCode, T4.Dt ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                Sm.CmParam<String>(ref cm, "@StartDt", Sm.GetDte(DteStartDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EndDt", Sm.GetDte(DteEndDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EndDt2", Sm.GetValue("Select Date_Format(Date_Add(STR_TO_DATE(" + Sm.GetDte(DteEndDt).Substring(0, 8) + ", '%Y%m%d'), Interval 1 Day), '%Y%m%d')"));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "EmpCode",
 
                        //1-5
                        "tmind", "date", "Dt2", "Ain", "Aout",   
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8), Sm.DrStr(dr, 2)))
                            {
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 2);
                                if (Sm.DrStr(dr, 4) != "XXXX")
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 6, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 13, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 16, 4);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 6].Value = 
                                    Grd1.Cells[Row, 13].Value =
                                    Grd1.Cells[Row, 16].Value = "";
                                }

                                if (Sm.DrStr(dr, 5) != "XXXX")
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 7, 5);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 14, 5);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 18, 5);
                                }
                                else
                                {
                                   Grd1.Cells[Row, 7].Value =
                                   Grd1.Cells[Row, 14].Value =
                                   Grd1.Cells[Row, 18].Value = "";
                                }

                                if (Sm.GetGrdStr(Grd1, Row, 6).Length == 0 && Sm.GetGrdStr(Grd1, Row, 7).Length == 0)
                                {
                                    Grd1.Cells[Row, 16].Value = "";
                                    Grd1.Cells[Row, 18].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 2);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 17, 3);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ReListEmpDurationWork();
        }

        private void ReListEmpDurationWork()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.BomCode As EmpCode, A.DocDt, SUM(B.Qty) As Qty ");
            SQL.AppendLine("From tblshopfloorcontrolhdr A ");
            SQL.AppendLine("Inner Join tblshopfloorcontrol2dtl B On A.DocNo = B.DocNo And B.BomType = '3' And B.Qty>0 ");
            SQL.AppendLine("Inner Join TblEmployee C On B.BomCode = C.EmpCode  ");
            SQL.AppendLine("And C.DeptCode = '" + Sm.GetLue(LueDeptCode) + "' ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("And A.DocDt between '" + Sm.GetDte(DteStartDt).Substring(0, 8) + "' And '" + Sm.GetDte(DteEndDt).Substring(0, 8) + "' ");
            SQL.AppendLine("Group By A.DocDt, B.BomCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "EmpCode", "DocDt", "Qty" });
                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 2);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            if(TxtDocNo.Text.Length==0) RoundingTime();
        }

        private void FormatGrid()
        {
            for(int Row = 0; Row<Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 6).Length == 0 )
                {
                    Grd1.Cells[Row, 6].Value = "00:00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 7).Length == 0)
                {
                    Grd1.Cells[Row, 7].Value = "00:00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 8).Length == 0)
                {
                    Grd1.Cells[Row, 8].Value = "00:00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 9).Length == 0)
                {
                    Grd1.Cells[Row, 9].Value = "00:00:00";
                }
            }
        }

        private void ComputeVs(int Col1, int Col2, int Col3)
        {
            TimeSpan TS = new TimeSpan();
            var Value = string.Empty;
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, Col1).Length != 0 && Sm.GetGrdStr(Grd1, Row, Col2).Length != 0 
                    //&& Sm.GetGrdStr(Grd1, Row, Col1) != ""
                    //&& Sm.GetGrdStr(Grd1, Row, Col2) != ""
                    //&& Sm.GetGrdStr(Grd1, Row, Col3) != "" 
                    )
                {
                    TS = DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col2)).Subtract(DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col1)));
                    Value = TS.ToString();
                    if (Value.Substring(0, 1) == "-")
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 6);
                    }
                    else
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 5);
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void IntervalIsMin_Old()
        {
            string Value1 = string.Empty, Value2 = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                // terlambat
                Value1 = Sm.GetGrdStr(Grd1, Row, 6);
                Value2 = Sm.GetGrdStr(Grd1, Row, 9);
                if (Value1.Length != 0 && Value2.Length != 0)
                {
                    //if (decimal.Parse(RemoveQuotes(Value1)) > decimal.Parse(RemoveQuotes(Value2)))
                    //{
                    //    Grd1.Rows[Row].BackColor = Color.Red;
                    //}
                    if (decimal.Parse(Value1.Replace(":", "")) > decimal.Parse(Value2.Replace(":", "")))
                    {
                        Grd1.Rows[Row].BackColor = Color.Red;
                    }
                }

                ////checklog kosong
                //if (Sm.GetGrdStr(Grd1, Row, 6).Length < 0 || Sm.GetGrdStr(Grd1, Row, 6) == "XXXX")
                //{
                //    Grd1.Rows[Row].BackColor = Color.Red;
                //}

                //////pulang lebih awal
                //if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                //{
                //    if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7))) < decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 10))))
                //    {
                //        Grd1.Rows[Row].BackColor = Color.Red;
                //    }
                //}

                //////working schedule kosong
                //if (Sm.GetGrdStr(Grd1, Row, 9).Length < 0)
                //{
                //    Grd1.Rows[Row].BackColor = Color.Red;
                //}

                //////masuk tidak boleh lbh cepat dari x mnit dri ws
                //if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                //{
                //    TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 9)).Substring(0, 2)), Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 9)).Substring(2, 2)), 0);
                //    TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(Delay), 0);
                //    string S = Convert.ToString(TS.Add(-TS2));

                //    if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 6))) < Decimal.Parse(RemoveQuotes(S.Substring(0, (S.Length - 2)))))
                //    {
                //        Grd1.Rows[Row].BackColor = Color.Red;
                //    }
                //}

                //////keluar tidak boleh lbh lambat dari x mnit dri ws
                //if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                //{
                //    TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 10)).Substring(0, 2)), Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 10)).Substring(2, 2)), 0);
                //    TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(Delay), 0);
                //    string S = Convert.ToString(TS.Add(TS2));
                        
                //    if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7))) > Decimal.Parse(RemoveQuotes(S.Substring(0, (S.Length-2)))))
                //    {
                //        Grd1.Rows[Row].BackColor = Color.Red;
                //    }
                //}
            }
        }

        private void CekResign(int Row)
        {
            decimal DateRow = 0m;
            decimal DateResign = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 19).Length>1)
            {
                DateRow = Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8));
            }
            if (Sm.GetGrdStr(Grd1, Row, 20).Length>1)
                DateResign = Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8));

            Grd1.BeginUpdate();
            if (DateResign > 0 && DateRow >= DateResign)
            {
                Grd1.Rows[Row].ReadOnly = iGBool.True;
                Grd1.Rows[Row].CellStyle.BackColor = Color.Violet;
            }
            Grd1.EndUpdate();
        }

        private void RoundingTime()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 13).Length > 0 && Sm.GetGrdStr(Grd1, Row, 13) != "")
                {
                    string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 13), 2);
                    if (Convert.ToInt32(MMIn) < 30)
                    {
                        TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 13)).Substring(0, 2)), 0, 0);
                        TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                        string LogIn = Convert.ToString(TS.Add(TS2));
                        Grd1.Cells[Row, 13].Value = LogIn.Substring(0, 5);
                        Grd1.Cells[Row, 16].Value = Sm.GetGrdStr(Grd1, Row, 13);
                    }
                    else
                    {
                        TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 13)).Substring(0, 2)), 0, 0);
                        TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(2 * RoundSch), 0);
                        string LogIn = Convert.ToString(TS.Add(TS2));
                        Grd1.Cells[Row, 13].Value = LogIn.Substring(0, 5);
                        Grd1.Cells[Row, 16].Value = Sm.GetGrdStr(Grd1, Row, 13);
                    }
                }

                if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "")
                {
                    string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 14), 2);
                    if (Convert.ToInt32(MMIn) < 30)
                    {
                        TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                        string LogOut = Convert.ToString(TS);
                        Grd1.Cells[Row, 14].Value = LogOut.Substring(0, 5);
                        Grd1.Cells[Row, 18].Value = Sm.GetGrdStr(Grd1, Row, 14);
                    }
                    else
                    {
                        TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                        TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                        string LogOut = Convert.ToString(TS.Add(TS2));
                        Grd1.Cells[Row, 14].Value = LogOut.Substring(0, 5);
                        Grd1.Cells[Row, 18].Value = Sm.GetGrdStr(Grd1, Row, 14);
                    }
                }

            }
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string DeptCode, string AGCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where (ActInd='Y' ");
            if (AGCode.Length != 0)
                SQL.AppendLine("Or AGCode='" + AGCode + "' ");
            SQL.AppendLine(") ");
            if (DeptCode.Length != 0)
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ReListEmp()
        {
            var lWS = new List<WS>();
            var lLog = new List<Log>();
            Process1(ref lWS);
            if (lWS.Count > 0)
            {
                Process2(ref lLog);
                Process3(ref lWS, ref lLog);
                Process4(ref lWS);
            }
            lWS.Clear();
            lLog.Clear();
            RoundingTime();
        }

        private void Process1(ref List<WS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select A.Dt, A.EmpCode, ");
            SQL.AppendLine("B.In1 As LogIn, B.bIn1 As bLogIn, B.aIn1 As aLogIn, ");
            SQL.AppendLine("Case When B.Out3 Is Null Then B.Out1 Else B.Out3 End As LogOut, ");
            SQL.AppendLine("Case When B.bOut3 Is Null Then B.bOut1 Else B.bOut3 End As bLogOut, ");
            SQL.AppendLine("Case When B.aOut3 Is Null Then B.aOut1 Else B.aOut3 End As aLogOut ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WsCode=B.WsCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",
 
                    //1-5
                    "EmpCode", "LogIn", "bLogIn", "aLogIn", "LogOut", 
                    
                    //6-7
                    "bLogOut", "aLogOut"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WS()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            LogIn = Sm.DrStr(dr, c[2]),
                            bLogIn = Sm.DrStr(dr, c[3]),
                            aLogIn = Sm.DrStr(dr, c[4]),
                            LogOut = Sm.DrStr(dr, c[5]),
                            bLogOut = Sm.DrStr(dr, c[6]),
                            aLogOut = Sm.DrStr(dr, c[7]),
                            IsOneDay = true,
                            IsWarning = false,
                            Dt2 = string.Empty,
                            AGCode = string.Empty,
                            AGName = string.Empty,
                            InDtL = string.Empty,
                            InTmL = string.Empty,
                            OutDtL = string.Empty,
                            OutTmL = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Log> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteStartDt)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteEndDt)).AddDays(1)));

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt, A.Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Log()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DtTm = Sm.Left(Sm.DrStr(dr, c[1]) + Sm.DrStr(dr, c[2]), 12)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<WS> lWS, ref List<Log> lLog)
        {
            string
                EmpCode = string.Empty,
                Dt = string.Empty,
                Dt0 = string.Empty,
                Dt2 = string.Empty,
                LogIn = string.Empty,
                bLogIn = string.Empty,
                aLogIn = string.Empty,
                LogOut = string.Empty,
                bLogOut = string.Empty,
                aLogOut = string.Empty
                ;

            bool IsOneDay = true;

            string Now = Sm.ServerCurrentDateTime();

            if (Now.Length > 0) Now = Sm.Left(Now, 12);

            for (var i = 0; i < lWS.Count; i++)
            {
                EmpCode = lWS[i].EmpCode;
                Dt = lWS[i].Dt;
                Dt0 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(-1)), 8);
                Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(1)), 8);

                IsOneDay = (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].LogOut) <= 0);

                lWS[i].Dt2 = Dt2;
                lWS[i].IsOneDay = IsOneDay;

                LogIn = Dt + lWS[i].LogIn;
                if (Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn) <= 0)
                    bLogIn = Dt + lWS[i].bLogIn;
                else
                    bLogIn = Dt0 + lWS[i].bLogIn;
                if (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].aLogIn) <= 0)
                    aLogIn = Dt + lWS[i].aLogIn;
                else
                    aLogIn = Dt2 + lWS[i].aLogIn;

                if (IsOneDay)
                    LogOut = Dt + lWS[i].LogOut;
                else
                    LogOut = Dt2 + lWS[i].LogOut;

                if (IsOneDay)
                    bLogOut = Dt + lWS[i].bLogOut;
                else
                {
                    if (Sm.CompareDtTm(lWS[i].bLogOut, lWS[i].LogOut) <= 0)
                        bLogOut = Dt2 + lWS[i].bLogOut;
                    else
                        bLogOut = Dt + lWS[i].bLogOut;
                }

                if (IsOneDay)
                {
                    if (Sm.CompareDtTm(lWS[i].LogOut, lWS[i].aLogOut) <= 0)
                        aLogOut = Dt + lWS[i].aLogOut;
                    else
                        aLogOut = Dt2 + lWS[i].aLogOut;
                }
                else
                    aLogOut = Dt2 + lWS[i].aLogOut;

                if (lLog.Count > 0)
                {
                    if (!mIsAtdShowDataLogWhenHoliday)
                    {
                        //IN
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogIn) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogIn) <= 0
                                )
                            .OrderBy(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].InDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].InTmL = Sm.Right(index.DtTm, 4);
                        }
                        //OUT
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogOut) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogOut) <= 0
                                )
                            .OrderByDescending(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].OutDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].OutTmL = Sm.Right(index.DtTm, 4);
                        }
                    }
                    else
                    {
                        for (int data = 0; data < lLog.Count; data++)
                        {
                            //region IN
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                            {
                                lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogIn.Length < 9)
                                {
                                    lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].InTmL = Sm.GetValue("Select MIN(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                                    {
                                        lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }


                            // Region  OUT
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                            {
                                lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogOut.Length < 9)
                                {
                                    lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].OutTmL = Sm.GetValue("Select MAX(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                                    {
                                        lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }
                            // OUT   
                        }
                    }    
                }

                if (LogIn.Length == 12 &&
                    Sm.CompareDtTm(LogIn, Now) < 0 &&
                    (lWS[i].InDtL.Length == 0 || lWS[i].InTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 &&
                    Sm.CompareDtTm(LogOut, Now) < 0 &&
                    (lWS[i].OutDtL.Length == 0 || lWS[i].OutTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogIn.Length == 12 && lWS[i].InDtL.Length > 0 && lWS[i].InTmL.Length > 0 &&
                    Sm.CompareDtTm(LogIn, lWS[i].InDtL + lWS[i].InTmL) < 0)
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 && lWS[i].OutDtL.Length > 0 && lWS[i].OutTmL.Length > 0 &&
                    Sm.CompareDtTm(lWS[i].OutDtL + lWS[i].OutTmL, LogOut) < 0)
                    lWS[i].IsWarning = true;
            }
        }

        private void Process4(ref List<WS> l)
        {
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), l[i].EmpCode) &&
                        Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8), l[i].Dt))
                    {
                        SetDt(Row, 5, l[i].Dt);
                        SetTm(Row, 6, l[i].InTmL);
                        SetTm(Row, 13, l[i].InTmL);
                        SetTm(Row, 16, l[i].InTmL);
                        SetTm(Row, 7, l[i].OutTmL);
                        SetTm(Row, 14, l[i].OutTmL);
                        SetTm(Row, 18, l[i].OutTmL);
                        SetDt(Row, 15, l[i].InDtL);
                        SetDt(Row, 17, l[i].OutDtL);

                        if (l[i].IsWarning) Grd1.Rows[Row].BackColor = Color.Red;

                        break;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void SetDt(int r, int c, string Dt)
        {
            if (Dt.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.ConvertDate(Dt);
        }

        private void SetTm(int r, int c, string Tm)
        {
            if (Tm.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.Left(Tm, 2) + ":" + Tm.Substring(2, 2);
        }

        #endregion

        #endregion

        #region Event
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            string DeptCode = Sm.GetLue(LueDeptCode);
            LueAGCode.EditValue = null;
            SetLueAGCode(ref LueAGCode, DeptCode, string.Empty);
            Sm.ClearGrd(Grd1, false);
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue3(SetLueAGCode), DeptCode, string.Empty); Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd1, false);
        }
    
        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { set; get; }
        }

        private class Date
        {
            public string DayName { set; get; }
        }

        private class WS
        {
            public string Dt { get; set; }
            public string Dt2 { get; set; }
            public string EmpCode { get; set; }
            public bool IsOneDay { get; set; }
            public string LogIn { get; set; }
            public string bLogIn { get; set; }
            public string aLogIn { get; set; }
            public string LogOut { get; set; }
            public string bLogOut { get; set; }
            public string aLogOut { get; set; }
            public string AGCode { get; set; }
            public string AGName { get; set; }
            public string InDtL { get; set; }
            public string InTmL { get; set; }
            public string OutDtL { get; set; }
            public string OutTmL { get; set; }
            public bool IsWarning { get; set; }
        }

        private class Log
        {
            public string EmpCode { get; set; }
            public string DtTm { get; set; }
        }

        #endregion
    }
}
