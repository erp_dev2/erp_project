﻿#region Update
/*
    13/03/2023 [WED/HEX] new reporting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBudgetSummary2 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mFiscalYearRange = string.Empty
            ;
        private bool
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudget2YearlyFormat = false,
            mIsRptBudgetSummaryShowCCGrp = false,
            mIsRptBudgetSummaryShowCostOfGroup = false,
            mIsCASUsedForBudget = false,
            mIsRptBudgetSummaryUseProfitCenter = false,
            mIsAllProfitCenterSelected = false
            ;

        #endregion

        #region Constructor

        public FrmRptBudgetSummary2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueYr(LueYr, string.Empty);
                if (mIsBudget2YearlyFormat)
                {
                    LblMth.Visible = LueMth.Visible = ChkMth.Visible = false;
                }
                else
                {
                    Sl.SetLueMth(LueMth);
                }
                
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsBudgetCalculateFromEstimatedPrice', 'IsMRShowEstimatedPrice', 'ReqTypeForNonBudget', ");
            SQL.AppendLine("'IsBudget2YearlyFormat', 'IsRptBudgetSummaryShowCCGrp', 'IsRptBudgetSummaryShowCostOfGroup', ");
            SQL.AppendLine("'IsCASUsedForBudget', 'IsRptBudgetSummaryUseProfitCenter', 'FiscalYearRange' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsRptBudgetSummaryShowCCGrp": mIsRptBudgetSummaryShowCCGrp = ParValue == "Y"; break;
                            case "IsRptBudgetSummaryShowCostOfGroup": mIsRptBudgetSummaryShowCostOfGroup = ParValue == "Y"; break;
                            case "IsCASUsedForBudget": mIsCASUsedForBudget = ParValue == "Y"; break;
                            case "IsRptBudgetSummaryUseProfitCenter": mIsRptBudgetSummaryUseProfitCenter = ParValue == "Y"; break;

                            //string
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "FiscalYearRange": mFiscalYearRange = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string GetSQL(string Filter1, string Filter2, String Query, string DeptCode, string Yr, string Mth, string BCCode)
        {
            var SQL = new StringBuilder();

            if (mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("Select A.Yr, '00' As Mth, B.DeptName, C.BCName, ");
                SQL.AppendLine("A.Amt1, A.Amt2, L.ProfitCenterName, ");
                SQL.AppendLine(Sm.SelectUsedBudget2());
                SQL.AppendLine("As Amt3, IfNull(M.Amt, 0.00) As Transferred, ");
                SQL.AppendLine("C.LocalCode, I.OptDesc As CCGrpDesc, J.OptDesc As CostOfGroupDesc ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(IfNull(E.Amt, A.Amt2-IfNull(F.Amt, 0.00))) Amt2 "); // ini ambil dari TblBudgetDtl (bukan langsung ambil dari Budget Summary) karena di BudgetTransfer itu, aku langsung update nilai Amt2 nya (gak on the fly). tapi di buat ifnull, karena bisa jadi ada budget yg di generate nya itu dari budget transfer, bukan dari budget request -> wed
                SQL.AppendLine("    From TblBudgetSummary A ");
                SQL.AppendLine("    Left Join TblBudgetRequestHdr B On A.Yr = B.Yr And A.DeptCode = B.DeptCode And B.CancelInd = 'N' And B.Status = 'A' ");
                SQL.AppendLine("    Left Join TblBudgetRequestDtl C On B.DocNo = C.DocNo And A.BCCode = C.BCCode ");
                SQL.AppendLine("    Left Join TblBudgetHdr D On B.DocNo = D.BudgetRequestDocNo And D.CancelInd = 'N' And D.Status != 'C' ");
                SQL.AppendLine("    Left Join TblBudgetDtl E On D.DocNo = E.DocNo And C.DNo = E.BudgetRequestDNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine(Sm.QueryBudgetTransfer());
                SQL.AppendLine("    ) F On A.Yr = F.Yr And A.DeptCode = F.DeptCode And A.BCCode = F.BCCode ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine(Filter1);
                SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine(Filter2);
                SQL.AppendLine(Sm.ComputeUsedBudget2(2, DeptCode, Yr, Mth, BCCode));
                SQL.AppendLine("Left Join TblCostCategory H On C.LocalCode=H.CCtCode And C.LocalCode Is Not Null ");
                SQL.AppendLine("Left Join TblOption I On I.OptCat='CostCenterGroup' And H.CCGrpCode=I.OptCode ");
                SQL.AppendLine("Left Join TblOption J On J.OptCat='CostOfGroup' And H.CostOfGroupCode=J.OptCode ");
                SQL.AppendLine("Left Join TblCostCenter K On C.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter L On K.ProfitCenterCode = L.ProfitCenterCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(Sm.QueryBudgetTransfer());
                SQL.AppendLine(") M On A.Yr = M.Yr And A.DeptCode = M.DeptCode And A.BCCode = M.BCCode ");
                SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
            }
            else
            {
                SQL.AppendLine("Select A.Yr, A.Mth, B.DeptName, C.BCName, ");
                SQL.AppendLine("A.Amt1, A.Amt2, L.ProfitCenterName, ");
                SQL.AppendLine(Sm.SelectUsedBudget2());
                SQL.AppendLine("As Amt3, IfNull(M.Amt, 0.00) As Transferred, ");
                SQL.AppendLine("C.LocalCode, I.OptDesc As CCGrpDesc, J.OptDesc As CostOfGroupDesc ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine(Filter2);
                SQL.AppendLine("Left Join TblCostCategory H On C.LocalCode=H.CCtCode And C.LocalCode Is Not Null ");
                SQL.AppendLine("Left Join TblOption I On I.OptCat='CostCenterGroup' And H.CCGrpCode=I.OptCode ");
                SQL.AppendLine("Left Join TblOption J On J.OptCat='CostOfGroup' And H.CostOfGroupCode=J.OptCode ");
                SQL.AppendLine("Left Join TblCostCenter K On C.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter L On K.ProfitCenterCode = L.ProfitCenterCode ");
                SQL.AppendLine(Sm.ComputeUsedBudget2(2, DeptCode, Yr, Mth, BCCode));
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(Sm.QueryBudgetTransfer());
                SQL.AppendLine(") M On A.Yr = M.Yr And A.DeptCode = M.DeptCode And A.BCCode = M.BCCode And A.Mth = M.Mth ");
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine(Filter1);
                SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Year",
                    "Month",
                    "Department",
                    "Category",
                    "Profit Center",
                    
                    //6-10
                    "Requested",
                    "Budget",
                    "Used",
                    "Budget Category's" + Environment.NewLine + "Local Code",
                    "Group",

                    //11-14
                    "Cost of Group",
                    "Transfer",
                    "Remaining",
                    "Realization"+Environment.NewLine+"%"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    80, 80, 200, 200, 200, 

                    //6-10
                    130, 130, 130, 130, 200, 

                    //11-14
                    200, 150, 150, 150
                }
            );
            Grd1.Cols[10].Visible = mIsRptBudgetSummaryShowCCGrp;
            Grd1.Cols[11].Visible = mIsRptBudgetSummaryShowCostOfGroup;
            Grd1.Cols[5].Visible = mIsRptBudgetSummaryUseProfitCenter;
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 12, 13, 14 }, 0);
            if (mIsBudget2YearlyFormat) Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter1 = " ", Filter2 = " ", Filter3 = " ", Query = string.Empty;
                int i = 0;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);

                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                if (!mIsBudget2YearlyFormat) Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtBCLocalCode.Text, "C.LocalCode", false);

                string BCCode = string.Empty;
                if (TxtBCLocalCode.Text.Length > 0) BCCode = Sm.GetValue("Select Group_Concat(Distinct Ifnull(BCCode, '')) From TblBudgetCategory Where LocalCode Like @Param ", string.Concat("%", TxtBCLocalCode.Text, "%"));

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL(Filter1, Filter2, Query, Sm.GetLue(LueDeptCode), Sm.GetLue(LueYr), Sm.GetLue(LueMth), BCCode),
                new string[]
                {
                    //0
                    "Yr", 

                    //1-5
                    "Mth", "DeptName", "BCName", "Amt1", "Amt2",

                    //6-10
                    "Amt3", "LocalCode", "CCGrpDesc", "CostOfGroupDesc", "ProfitCenterName",

                    //11-12
                    "Transferred", "Amt3"
                },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);

                        decimal remaining = Sm.DrDec(dr, c[5]) - Sm.DrDec(dr, c[12]) + Sm.DrDec(dr, c[11]);
                        decimal devider = Sm.DrDec(dr, c[5]) + Sm.DrDec(dr, c[11]);
                        decimal realization = 0m;
                        if (devider != 0m) realization = (Sm.DrDec(dr, c[12]) / devider) * 100m;

                        Grd.Cells[Row, 13].Value = remaining;
                        Grd.Cells[Row, 14].Value = realization;
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtBCLocalCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCLocalCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget Category Code Internal");
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMth_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Month");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
