﻿#region Update
/*
    02/04/2019 [DITA] Tambah inputan untuk Site
    16/06/2020 [VIN/SIER] sales target yearly
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesTarget : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mSpCode = string.Empty, //to get sales person's code
            mUniqueCode = string.Empty;  //to get sales target's unique code
        internal bool
            mIsSalesTargetSiteMandatory = false,
            mIsSalesTargetYearly = false;

        internal int mStateIndicator = 0;
        internal FrmSalesTargetFind FrmFind;

        #endregion

        #region Constructor

        public FrmSalesTarget(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            GetParameter();
            if (mIsSalesTargetSiteMandatory) LblSite.ForeColor = Color.Red;
            Sl.SetLueMth(LueMth);
            Sl.SetLueYr(LueYr, string.Empty);
            if (mIsSalesTargetYearly) 
                LblMth.ForeColor = Color.Black;

            //if (mDocNo.Length != 0)
            //{
            //    ShowData(mDocNo);
            //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            //}
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSpName, LueYr, LueMth, TxtAmt, ChkCancelInd, LueSiteCode
                    }, true);
                    BtnSpName.Enabled = false;
                    TxtSpName.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtSpName, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueYr, LueMth, TxtAmt, LueSiteCode
                    }, false);
                    BtnSpName.Enabled = true;
                    ChkCancelInd.Checked = false;
                    if (mIsSalesTargetYearly) LueMth.Enabled = false;
                    BtnSpName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSpName, LueYr, LueMth, TxtAmt, LueSiteCode
                    }, true);
                    BtnSpName.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (mIsSalesTargetYearly) LueMth.Enabled = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mSpCode = string.Empty;
            mUniqueCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSpName, LueYr, LueMth, LueSiteCode
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt
            }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesTargetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateIndicator = 1;
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            if (mIsSalesTargetYearly) 
                LueMth.Text = string.Empty;
            else
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mStateIndicator = 2;
            if (Sm.IsTxtEmpty(TxtSpName, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSpName, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var SQLDel = new StringBuilder();

                SQLDel.AppendLine("DELETE FROM TblSalesTarget T WHERE T.UniqueCode = @UniqueCode AND T.SpCode = @SpCode ");
                
                var cm = new MySqlCommand() { CommandText = SQLDel.ToString() };
                Sm.CmParam<String>(ref cm, "@UniqueCode", mUniqueCode);
                Sm.CmParam<String>(ref cm, "@SpCode", mSpCode);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mStateIndicator == 1)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            decimal UniCode = (Decimal.Parse(Sm.GetValue("SELECT IFNULL(MAX(X.UniqueCode), 0) FROM TblSalesTarget X WHERE X.SpCode = '" + mSpCode + "'"))) + 1;

            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblSalesTarget(UniqueCode, SpCode, CancelInd, Yr, Mth, Amt, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@UniqueCode, @SpCode, 'N', @Yr, @Mth, @Amt, @SiteCode, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UniqueCode", UniCode.ToString());
            Sm.CmParam<String>(ref cm, "@SpCode", mSpCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            if (mIsSalesTargetYearly) 
                Sm.CmParam<string>(ref cm, "@Mth", "00");
            else
                Sm.CmParam<String>(ref cm, "@Mth",  Sm.GetLue(LueMth));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.ExecCommand(cm);

            ShowData(UniCode.ToString(), mSpCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSpName, "Name", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                (!mIsSalesTargetYearly && Sm.IsLueEmpty(LueMth, "Month")) ||
                IsAmountNotValid() ||
                (mIsSalesTargetSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site"))||
                IsDataAlreadyExists() ||
                IsDataCancelledAlready();
        }

        private bool IsDataAlreadyExists()
        {
            var SQLd = new StringBuilder();

            SQLd.AppendLine("SELECT * FROM TblSalesTarget ");
            SQLd.AppendLine("WHERE SpCode = @SpCode AND Yr = @Yr AND Mth = @Mth AND CancelInd = 'N'  ");
            SQLd.AppendLine("ORDER BY UniqueCode DESC LIMIT 1 ");

            var cm = new MySqlCommand() { CommandText = SQLd.ToString() };
            Sm.CmParam<String>(ref cm, "@SpCode", mSpCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already exist.");
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            decimal Amount = 0m;
            if (TxtAmt.Text.Length != 0) Amount = Decimal.Parse(TxtAmt.Text);
            if (Amount < 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Amount, Please check again.");
                return true;
            }
            return false;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditSalesTarget();
            ShowData(mUniqueCode, mSpCode);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSpName, "Name", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQLCl = new StringBuilder();

            SQLCl.AppendLine("SELECT * FROM TblSalesTarget T1 WHERE T1.CancelInd='Y' AND T1.UniqueCode=@UniqueCode AND T1.SpCode = @SpCode; ");

            var cm = new MySqlCommand(){ CommandText = SQLCl.ToString() };
            Sm.CmParam<String>(ref cm, "@UniqueCode", mUniqueCode);
            Sm.CmParam<String>(ref cm, "@SpCode", mSpCode);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSpName, "Name", false);
        }

        private void EditSalesTarget()
        {
            var SQLST = new StringBuilder();

            SQLST.AppendLine("UPDATE TblSalesTarget SET CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQLST.AppendLine("WHERE UniqueCode = @UniqueCode AND SpCode = @SpCode AND CancelInd='N' ");
            
            var cm = new MySqlCommand() { CommandText = SQLST.ToString() };
            Sm.CmParam<String>(ref cm, "@UniqueCode", mUniqueCode);
            Sm.CmParam<String>(ref cm, "@SpCode", mSpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string UniqueCode, string SpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSalesTarget(UniqueCode, SpCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesTarget(string UniqueCode, string SpCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@UniqueCode", UniqueCode);
            Sm.CmParam<String>(ref cm, "@SpCode", SpCode);

            SQL.AppendLine("SELECT A.UniqueCode, A.SpCode, B.SpName, A.CancelInd, A.Yr, A.Mth, A.Amt, C.SiteCode ");
            SQL.AppendLine("FROM TblSalesTarget A ");
            SQL.AppendLine("INNER JOIN TblSalesPerson B ON A.SpCode = B.SpCode ");
            SQL.AppendLine("Left JOIN TblSite C ON A.SiteCode = C.SiteCode ");
            SQL.AppendLine("WHERE A.UniqueCode = @UniqueCode AND A.SpCode = @SpCode ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                {
                    //0
                    "UniqueCode",
                    
                    //1-5
                    "SpCode", "SpName", "CancelInd", "Yr", "Mth",
                    
                    //6-7
                    "Amt", "SiteCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    mUniqueCode = Sm.DrStr(dr, c[0]);
                    mSpCode = Sm.DrStr(dr, c[1]);
                    TxtSpName.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[5]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[6]), 0);
                    Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                }, true
            );
        }

        #endregion

        #region Additional Method

        public static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "SELECT '01' AS Col1, 'January' AS Col2 UNION ALL " +
                "SELECT '02','February' UNION ALL " +
                "SELECT '03','March' UNION ALL " +
                "SELECT '04','April' UNION ALL " +
                "SELECT '05','May' UNION ALL " +
                "SELECT '06','June' UNION ALL " +
                "SELECT '07','July' UNION ALL " +
                "SELECT '08','August' UNION ALL " +
                "SELECT '09','September' UNION ALL " +
                "SELECT '10','October' UNION ALL " +
                "SELECT '11','November' UNION ALL " +
                "SELECT '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        private void GetParameter()
        {
            mIsSalesTargetSiteMandatory = Sm.GetParameterBoo("IsSalesTargetSiteMandatory");
            mIsSalesTargetYearly = Sm.GetParameterBoo("IsSalesTargetYearly");
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnSpName_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesTargetDlg(this));
        }

        #endregion

        #region Misc Control Events

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        #endregion

        #endregion
    }
}
