﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt5Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt5 mFrmParent;
        string mSQL = string.Empty, mWhsCode= string.Empty, mCtCode=string.Empty ;
       
        #endregion

        #region Constructor

        public FrmDOCt5Dlg(FrmDOCt5 FrmParent, string WhsCode, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCtCode = CtCode;

        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, A.PropCode, C.PropName, A.BatchNo, A.Source, D.ItCtName, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, B.ItGrpCode, E.CtitCode, F.Uprice,   ");
            SQL.AppendLine("G.QtyPrev, G.Qty2Prev, G.Qty3Prev ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On B.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem Where CtCode =@CtCode ");
            SQL.AppendLine("    ) E On B.ItCode = E.itCode ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("(	 ");
	        SQL.AppendLine("    Select A.CtCode, C.ItCode,  A.CurCode, B.Uprice  ");
	        SQL.AppendLine("    From TblCtQThdr A ");
	        SQL.AppendLine("    Inner Join TblCtQTDtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    Inner Join TblitemPriceDtl C on B.ItemPriceDocNo  = C.DocNo And B.ItemPriceDNo  = C.DNo  ");
	        SQL.AppendLine("    Where A.Actind = 'Y' And A.Status= 'A' And A.CtCode = @CtCode And B.CtQtType = '02' ");
            SQL.AppendLine(")F On A.ItCode = F.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.CtCode, X.itCode, X.BatcHNo, X.Source, X2.Qty As QtyPrev, X2.Qty2 As Qty2Prev, X2.Qty3 As Qty3prev  ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select MAX(Concat(A.DocDt, A.DocNo, B.Dno)) As KeyWord,  A.CtCode, B.ItCode, B.BatchNo, B.Source    ");
            SQL.AppendLine("        From TblDoctHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where B.cancelInd = 'N' And A.CtCode = @CtCode  And A.RecurringInd = 'Y' ");
            SQL.AppendLine("        group by A.CtCode, B.ItCode, B.BatchNo, B.Source ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(A.DocDt, A.DocNO, B.Dno) As Kode, B.ItCode, B.Qty, B.Qty2, B.Qty3  ");
            SQL.AppendLine("        From tblDocthdr A ");
            SQL.AppendLine("        Inner Join tblDoctDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where B.cancelInd = 'N' And A.CtCode = @CtCode  And A.RecurringInd = 'Y' ");
            SQL.AppendLine("    )X2 On X.keyword = X2.kode And X.ItCode = X2.itCode ");
            SQL.AppendLine(")G On A.ItCode = G.ItCode And A.batchNo = G.BatchNo And A.Source = G.Source And F.CtCode = G.CtCode  ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode  ");
            SQL.AppendLine("And (A.Qty>0 Or A.Qty2>0 Or A.Qty3>0) ");
            SQL.AppendLine("And Locate(Concat('##', A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Lot, A.Bin, '##'), @SelectedItem)<1 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code", 
                        "Item's Name", 
                        //6-10
                        "Foreign Name",
                        "Item's Category",
                        "Property Code",
                        "Customer"+Environment.NewLine+"Refference",
                        "Property",
                        //11-15
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin", 
                        "Group",
                        //16-20
                        "Stock",
                        "Uom",
                        "Prev Qty",
                        "Stock 2",
                        "Uom 2",
                        //21-25
                        "Prev Qty2",
                        "Stock 3",
                        "Uom 3",
                        "Prev Qty3",
                        "Price"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 80, 20, 60, 200, 
                        //6-10
                        120, 100, 100, 120, 120, 
                        //11-15
                        120, 120, 80, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 80, 
                        //21-25
                        80, 80, 80, 80, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18, 19, 21, 22, 24, 25 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropCode.Text, new string[] { "A.PropCode", "C.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "A.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "A.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + 
                        " Order By B.ItName, A.BatchNo;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
                            //1-5
                            "ItCodeInternal", "ItName", "ForeignName", "ItCtName", "PropCode", 
                            //6-10
                            "CtItCode", "PropName", "BatchNo", "Source", "Lot", 
                            //11-15
                            "Bin", "ItGrpCode", "Qty", "InventoryUomCode", "QtyPrev",   
                            //16-20
                            "Qty2", "InventoryUomCode2", "Qty2Prev", "Qty3", "InventoryUomCode3",    
                            //21
                            "Qty3Prev","Uprice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);//localcode
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);//itname
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 6);//foreign
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);//itemcat
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);//propcode
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);//custref
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);//prop
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);//batch
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);//source
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);//lot
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 14);//bin
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 15);//group
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);//stock
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 17);//uom
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 18);//prev
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 19);//stock 2
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);//uom2
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 21);//prev2
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 22);//stock3
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 23);//uom3
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 24);//prev3 
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 25);//price

                       //mFrmParent.ComputeTotalQty();

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 2) + //itcode
                Sm.GetGrdStr(Grd1, Row, 8) + // Propcode
                Sm.GetGrdStr(Grd1, Row, 11) + //batch
                Sm.GetGrdStr(Grd1, Row, 12) + //source
                Sm.GetGrdStr(Grd1, Row, 13) + //lot
                Sm.GetGrdStr(Grd1, Row, 14); //bin
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key, 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 4) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 9) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 12) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 13) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 14) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 15) 
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }
      

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
