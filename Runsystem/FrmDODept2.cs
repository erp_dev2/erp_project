﻿#region Update
/*
    04/04/2017 [WED] Setelah InsertData(), diarahkan ke ShowData(), bukan BtnInsertClick()
    06/06/2017 [TKG] daftar cost center hanya yg aktif saja.
    20/06/2017 [TKG] Update remark di journal 
    15/07/2017 [TKG] tambah journal antar entity
    09/10/2017 [TKG] tambah filter berdasarkan otorisasi group thd cost center
    11/01/2018 [TKG] Berdasarkan parameter DODeptRequestBySource, daftar request by diambil dari table user atau requestby
    23/01/2018 [TKG] tambah entity yg bisa menfilter cost center berdasarkan parameter IsDODeptUseEntity
    30/01/2018 [TKG] saat merubah cost center dan entity, cost category dan coa-nya akan dihapus
    23/05/2018 [TKG] tambah local document#
    24/05/2018 [TKG] ubah validasi menggunakan cost center
    17/07/2018 [TKG] tambah cost center saat journal
    01/08/2018 [TKG] menggunakan parameter IsAutoJournalActived untuk validasi apakah cost category boleh kosong atau tidak.
    02/08/2018 [TKG] Berdasarkan parameter IsDODeptJournalCancelUseTheSameCCtAcNo, apakah saat journal cancel do menggunakan nomor rekening coa yg lama atau yg terakhir ada di tem cost cettagory. 
    31/10/2018 [HAR] bug saat nampilin stock
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/04/2019 [DITA] Bug : menampilkan department yg aktif saja
    20/05/2019 [TKG] parameter IsSystemUseCostCenter diganti dengan IsDODeptUseCostCenter
    16/12/2019 [TKG/IMS] journal untuk moving average
    24/02/2020 [WED/SIER] cost category di detail bisa dipilih dan di save, berdasarkan parameter MenuCodeForDODeptCostCategory
    22/07/2020 [IBL/SRN] Departemen belum ter otorisasi sesuai departemen user
    25/08/2020 [IBL/MGI] menambahkan field group berdasarkan parameter IsUseProductionWorkGroup
    07/01/2020 [DITA/SRN] validasi Acount No yang kosong saat savejournal baik insert maupun edit berdasarkan parameter : IsCheckCOAJournalNotExists
    01/06/2021 [DITA/IOK] tambah inputan WOR -> item berdasarkan WOR nya. param : IsDODept2BasedOnWOR
    02/07/2021 [VIN/IOK] Uncoment validasi di method IsGrdValueNotValid
    02/08/2021 [DITA/IOK] method ClearGrd dibikin public
    05/08/2021 [DITA/IOK] tambah param baru : ItCtCodeForDODept2BasedOnWOR
    19/08/2021 [TRI/ALL] mengubah validasi COA
    25/11/2021 [TYO/IOK] set parameter IsWORStatusOpenOnly di region field
    26/11/2021 [DEV/IOK] Pada menu DO to Departement Without DO Request (01050305), kolom Asset berubah menjadi kosong ketika mengubah Department atau Cost Center dengan parameter IsAssetColumnEmptied
    01/12/2021 [MYA/IOK] Pada menu DO to Departement Without DO Request (01050305), Asset Name, Display Name otomatis muncul jika input WOR
    26/01/2022 [RDA/IOK] Support : komponen asset pada kolom Display Name belum ikut kosong berdasarkan param IsAssetColumnEmptied
    27/01/2022 [TKG/ALL] ubah GetParameter dan proses save
    08/02/2022 [MYA/IOK] Feedback : Ketika insert di menu DO to Departement (without DOR) setelah digunakan untuk insert yang menggunakan #WOR, kolom asset pada detail masih nyantol asset yang digunakan pada saat insert sebelumnya ketika menggunakan #WOR
    27/12/2022 [MAU/BBT] validasi tidak bisa cancel dokumen apabila sedang digunakan pada transaksi Property Inventory Cost Component
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDODept2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mItCtCodeForDODept2BasedOnWOR = string.Empty,
            mAssetCode = string.Empty,
            mAssetName = string.Empty,
            mDisplayName = string.Empty; //if this application is called from other application;

        private string 
            mDocType = "14",
            mDODeptRequestBySource = string.Empty;
        internal bool mIsDODeptUseCostCenter = false,
            mIsAssetForDODept2BasedOnWOR = false;
            
        internal int mNumberOfInventoryUomCode = 1;
        iGCell fCell;
        bool fAccept;
        internal FrmDODept2Find FrmFind;
        internal bool
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsFilterByCC = false,
            mMenuCodeForDODeptCostCategory = false,
            mIsCostCategoryFilterByEntity = false,
            mIsFilterByDept = false,
            mIsUseProductionWorkGroup = false,
            mIsCheckCOAJournalNotExists = false,
            mIsDODept2BasedOnWOR = false,
            mIsDODept2FindShowWORDocNo = false,
            mIsWORStatusOpenOnly = false;
            
        private bool 
            mIsInventoryShowTotalQty = false, 
            mIsAutoJournalActived = false, 
            mIsEntityMandatory = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDODeptUseEntity = false,
            mIsDODeptJournalCancelUseTheSameCCtAcNo = false, 
            mIsMovingAvgEnabled = false,
            mIsAssetColumnEmptied = false;

        #endregion

        #region Constructor

        public FrmDODept2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Department (without DO Request)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsDODeptUseEntity) LblEntCode.ForeColor = Color.Red;
                if (mIsDODeptUseCostCenter) LblCCCode.ForeColor = Color.Red;
                SetGrd();
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                LueEmpCode.Visible = false;
                if (!mIsUseProductionWorkGroup)
                {
                   LblGroup.Visible= LueProductionWorkGroup.Visible = false;
                }
                if (!mIsDODept2BasedOnWOR)
                {
                    LblWORDocNo.Visible = TxtWORDocNo.Visible = BtnWORDocNo.Visible = false;
                }

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAcNoForSaleUseItemCategory', 'IsItGrpCodeShow', 'IsAssetForDODept2BasedOnWOR', 'DODeptRequestBySource', 'ItCtCodeForDODept2BasedOnWOR', ");
            SQL.AppendLine("'IsCheckCOAJournalNotExists', 'IsDODept2BasedOnWOR', 'IsDODept2FindShowWORDocNo', 'IsWORStatusOpenOnly', 'IsAssetColumnEmptied', ");
            SQL.AppendLine("'IsMovingAvgEnabled', 'IsCostCategoryFilterByEntity', 'IsFilterByDept', 'IsUseProductionWorkGroup', 'IsInventoryShowTotalQty', ");
            SQL.AppendLine("'IsFilterByCC', 'IsEntityMandatory', 'IsAutoJournalActived', 'IsShowForeignName', 'IsDODeptUseCostCenter', ");
            SQL.AppendLine("'MenuCodeForDODeptCostCategory', 'IsDODeptJournalCancelUseTheSameCCtAcNo', 'IsDODeptUseEntity' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "MenuCodeForDODeptCostCategory": 
                                if (ParValue.Length>0)  mMenuCodeForDODeptCostCategory = ParValue == mMenuCode; 
                                break;
                            case "IsDODeptJournalCancelUseTheSameCCtAcNo": mIsDODeptJournalCancelUseTheSameCCtAcNo = ParValue == "Y"; break;
                            case "IsDODeptUseEntity": mIsDODeptUseEntity = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsDODeptUseCostCenter": mIsDODeptUseCostCenter = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsCostCategoryFilterByEntity": mIsCostCategoryFilterByEntity = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsInventoryShowTotalQty": mIsInventoryShowTotalQty = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsDODept2BasedOnWOR": mIsDODept2BasedOnWOR = ParValue == "Y"; break;
                            case "IsDODept2FindShowWORDocNo": mIsDODept2FindShowWORDocNo = ParValue == "Y"; break;
                            case "IsWORStatusOpenOnly": mIsWORStatusOpenOnly = ParValue == "Y"; break;
                            case "IsAssetColumnEmptied": mIsAssetColumnEmptied = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsAssetForDODept2BasedOnWOR": mIsAssetForDODept2BasedOnWOR = ParValue == "Y"; break;

                            //string
                            case "ItCtCodeForDODept2BasedOnWOR": mItCtCodeForDODept2BasedOnWOR = ParValue; break;
                            case "DODeptRequestBySource": mDODeptRequestBySource = ParValue; break;
                            
                            //integer 
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length > 0)
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 8;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Sequence#",
                    
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "",
                    "Item's Code",
                    "",

                    //6-10
                    "Local Code",
                    "Item's Name",
                    "Replacement",
                    "Property Code",
                    "Property",
                    
                    //11-15
                    "Batch#",
                    "Source",
                    "Lot",
                    "Bin",
                    "Quantity",
                    
                    //16-20
                    "UoM",
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    
                    //21-25
                    "",
                    "Asset",
                    "Asset",
                    "Employee Code",
                    "Requested By",
                    
                    //26-30
                    "Position",
                    "Remark",
                    "Cost Category",
                    "Inventory's"+Environment.NewLine+"COA Account#",
                    "Cancelled"+Environment.NewLine+"DO Journal#",

                    //31-35
                    "Group",
                    "Display Name",
                    "Foreign Name",
                    "Cost Category's"+Environment.NewLine+"COA Account#",
                    "Cost Category Code",

                    //36
                    ""
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    50, 50, 20, 80, 20, 
                    
                    //6-10
                    70, 200, 100, 0, 0, 
                    
                    //11-15
                    200, 200, 60, 60, 80, 
                    
                    //16-20
                    80, 80, 80, 80, 80,  
                    
                    //21-25
                    20, 0, 200, 0, 150,
                    
                    //26-30
                    150, 350, 250, 200, 150,

                    //31-35
                    100, 300, 150, 0, 0,

                    //36
                    20
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 15, 17, 19 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5, 21, 36 });
            Grd1.Cols[32].Move(24);
            Grd1.Cols[33].Move(8);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 6, 9, 12, 17, 18, 19, 20, 22, 24, 26, 31, 34, 35 }, false);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 33 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 7, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 23, 24, 26, 28, 29, 30, 31, 32, 33, 34 });
            if (!mMenuCodeForDODeptCostCategory) Sm.GrdColInvisible(Grd1, new int[] { 36 });

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[31].Visible = true;
                Grd1.Cols[31].Move(7);
            }

            if (mMenuCodeForDODeptCostCategory) Grd1.Cols[36].Move(31);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 24;
            Grd2.FrozenArea.ColCount = 8;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Item's Code",

                        //1-5
                        "",
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",

                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",

                        //11-15
                        "Quantity",
                        "Balance",
                        "UoM",
                        "Stock",
                        "Quantity",

                        //16-20
                        "Balance",
                        "UoM",
                        "Stock",
                        "Quantity",
                        "Balance",

                        //21-23
                        "UoM",
                        "Group",
                        "Foreign Name"
                    },
                     new int[] 
                    {
                        //0
                        80,
 
                        //1-5
                        20, 60, 200, 0, 80, 
                        
                        //6-10
                        200, 200, 60, 60, 80,  
                        
                        //11-15
                        80, 80, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80,  
                        
                        //21-23
                        80, 100, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Grd2.Cols[23].Move(4);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 4, 7, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, false);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd2, new int[] { 23 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            if (mIsItGrpCodeShow)
            {
                Grd2.Cols[22].Visible = true;
                Grd2.Cols[22].Move(3);
            }

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 12, 26 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 14, 15, 16, 17 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 14, 15, 16, 17, 18, 19, 20, 21 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueDeptCode, LueEntCode, 
                        LueCCCode, LueUserCode, MeeRemark, LueProductionWorkGroup, TxtWORDocNo
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 8, 15, 17, 19, 21, 23, 25, 27 });
                    BtnWORDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueDeptCode, LueEntCode, 
                        LueCCCode, MeeRemark, LueProductionWorkGroup
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 8, 15, 17, 19, 21, 23, 25, 27 });
                    BtnWORDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueEntCode, 
                LueCCCode, LueDeptCode, LueUserCode, MeeRemark, LueEmpCode, 
                TxtJournalDocNo, LueProductionWorkGroup, TxtWORDocNo
            });
            ClearGrd();
            mAssetCode = mAssetName = mDisplayName = string.Empty;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 8 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 17, 19 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDODept2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string Param = Sm.GetParameter("NumberOfInventoryUomCode");
            if (Param.Length!=0) ParPrint(int.Parse(Param));
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") &&
                        (!mIsDODeptUseCostCenter ||
                        (mIsDODeptUseCostCenter && !Sm.IsLueEmpty(LueCCCode, "Cost Center")))
                        )
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDODept2Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCCCode)));
                    }

                    if (e.ColIndex == 21 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 4, false, "Item is empty."))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDODept2Dlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 8, 15, 17, 19, 21, 23, 25, 27 }, e.ColIndex))
                    {
                        if (e.ColIndex == 25) LueRequestEdit(Grd1, LueEmpCode, ref fCell, ref fAccept, e);

                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2, 8 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 17, 19 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);

                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();

                            ComputeSummary();
                            string Key1 = string.Empty;
                            bool IsFind = false;
                            for (int Row = Grd2.Rows.Count-1;Row >=0; Row--)
                            {
                                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                                {
                                    Key1 =
                                        Sm.GetGrdStr(Grd2, Row, 0) +
                                        Sm.GetGrdStr(Grd2, Row, 4) +
                                        Sm.GetGrdStr(Grd2, Row, 6) +
                                        Sm.GetGrdStr(Grd2, Row, 7) +
                                        Sm.GetGrdStr(Grd2, Row, 8) +
                                        Sm.GetGrdStr(Grd2, Row, 9);

                                    IsFind = false;
                                    for (int Row2 = 0; Row2 < Grd1.Rows.Count; Row2++)
                                    {
                                        if (Sm.GetGrdStr(Grd1, Row2, 4).Length > 0 &&
                                            Sm.CompareStr(
                                                Key1,
                                                Sm.GetGrdStr(Grd1, Row2, 4) +
                                                Sm.GetGrdStr(Grd1, Row2, 9) +
                                                Sm.GetGrdStr(Grd1, Row2, 11) +
                                                Sm.GetGrdStr(Grd1, Row2, 12) +
                                                Sm.GetGrdStr(Grd1, Row2, 13) +
                                                Sm.GetGrdStr(Grd1, Row2, 14)
                                                ))
                                        {
                                            IsFind = true;
                                            break;
                                        }
                                    }
                                    if (!IsFind) Grd2.Rows.RemoveAt(Row);
                                }
                            }
                            if (Grd2.Rows.Count<= 0) Grd2.Rows.Add();
                        }    
                    }
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LueWhsCode, "Warehouse") &&
                (!mIsDODeptUseCostCenter ||
                (mIsDODeptUseCostCenter && !Sm.IsLueEmpty(LueCCCode, "Cost Center")))
                )
                Sm.FormShowDialog(new FrmDODept2Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCCCode)));

            if (e.ColIndex == 21 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 4, false, "Item is empty."))
                Sm.FormShowDialog(new FrmDODept2Dlg2(this, e.RowIndex, Sm.GetLue(LueCCCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if(e.ColIndex == 36 && BtnSave.Enabled && mMenuCodeForDODeptCostCategory && !Sm.IsLueEmpty(LueCCCode, "Cost Center"))
            {
                Sm.FormShowDialog(new FrmDODept2Dlg4(this, e.RowIndex, Sm.GetLue(LueCCCode)));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 17, 19 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23, 27 }, e);

                if (e.ColIndex == 15)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 17, 19, 16, 18, 20);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 19, 17, 16, 20, 18);
                }

                if (e.ColIndex == 17)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 17, 15, 19, 18, 16, 20);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 17, 19, 15, 18, 20, 16);
                }

                if (e.ColIndex == 19)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 19, 15, 17, 20, 16, 18);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 19, 17, 15, 20, 18, 16);
                }

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 17);

                if (e.ColIndex == 23)
                {
                    Grd1.Cells[e.RowIndex, 22].Value = (Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length == 0)
                        ? null : GetAssetCode(e.RowIndex);
                }

                if (Sm.IsGrdColSelected(new int[]{15, 17, 19}, e.ColIndex)) ComputeSummary();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 17, 19 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }

            if (e.ColIndex==23 && BtnSave.Enabled && TxtDocNo.Text.Length==0)
            {
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Grd1.Cells[Row, 22].Value = Sm.GetGrdStr(Grd1, 0, 22);
                    Grd1.Cells[Row, e.ColIndex].Value = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                }
            }
            if (e.ColIndex == 25 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Grd1.Cells[Row, 24].Value = Sm.GetGrdStr(Grd1, 0, 24);
                    Grd1.Cells[Row, e.ColIndex].Value = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                    Grd1.Cells[Row, 26].Value = Sm.GetGrdStr(Grd1, 0, 26);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDODeptHdr(DocNo));
            cml.Add(SaveDODeptDtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            //        cml.Add(SaveDODeptDtl(DocNo, Row));

            cml.Add(SaveStockMovement(DocNo, "", "N"));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    cml.Add(SaveStockSummary(1, Row));

            if (mIsAutoJournalActived)
            {
                if (mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal(DocNo));
                else
                    cml.Add(SaveJournal2(DocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                ((mIsDODeptUseCostCenter || mIsFilterByCC) && 
                Sm.IsLueEmpty(LueCCCode, "Cost center")) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt))||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsJournalSettingInvalid());
        }
        private string GetPropertyInventoryCostComponentCode()
        {
            string DODDocNo = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT DocNo ");
            SQL.AppendLine(" FROM tblpropertyinventorycostcomponentdtl ");
            SQL.AppendLine(" WHERE DODDocNo = @Param ");
            SQL.AppendLine("  ");
            SQL.AppendLine(" LIMIT 1; ");
            DODDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);

            return DODDocNo;
        }

        private bool IsDocAlreadyProcessToPropertyCostComponent()
        {

            string mDODDocNo = GetPropertyInventoryCostComponentCode();

            return Sm.IsDataExist(
                " SELECT 1 FROM tbldodepthdr " +
                " WHERE DocNo IN( " +
                " SELECT DODDocNo FROM tblpropertyinventorycostcomponentdtl WHERE DODDocNo = @Param " +
                " ); " ,
                TxtDocNo.Text,
                "The document is already processed to Add. Property Cost Component document " + mDODDocNo

                );
        }
        /*private bool IsCOAJournalNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string mItCode = string.Empty;
            string mCCtCode = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (mItCode.Length > 0) mItCode += " Or ";
                    mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                }
            }

            if (mItCode.Length > 0) mItCode = " And (" + mItCode + ") ";

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (mCCtCode.Length > 0) mCCtCode += " Or ";
                    mCCtCode += " (CCtCode=@CCtCode00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@CCtCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 35));
                }
            }

            if (mCCtCode.Length > 0) mCCtCode = " And (" + mCCtCode + ") ";


            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");

            if (mIsEntityMandatory && IsEntityDifferent())
            {
                if (mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("    Select ParValue As AcNo ");
                    SQL.AppendLine("    From TblParameter Where ParCode In ('AcNoForCOGS', 'AcNoForSaleOfFinishedGoods') ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select B.AcNo ");
                    SQL.AppendLine("        From TblItem A ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode B.MovingAvgInd='N' " + mItCode);
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select AcNo2 As AcNo ");
                    SQL.AppendLine("    From TblEntity Where EntCode=@EntCodeWhs ");

                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select T.AcNo ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select AcNo ");
                        SQL.AppendLine("        From TblCostCategory Where 1=1" + mCCtCode);
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select T.AcNo ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblItemCostCategory A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And A.CCCode=B.CCCode " + mItCode);
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select AcNo1 As AcNo ");
                    SQL.AppendLine("    From TblEntity Where EntCode=@EntCodeCC ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select B.AcNo ");
                    SQL.AppendLine("        From TblItem A  ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode And B.MovingAvgInd='Y' " + mItCode);
                    SQL.AppendLine("    ) T Group By T.AcNo ");


                }
                else
                {
                    SQL.AppendLine("    Select ParValue As AcNo ");
                    SQL.AppendLine("    From TblParameter Where ParCode in ('AcNoForCOGS', 'AcNoForSaleOfFinishedGoods') ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select B.AcNo ");
                    SQL.AppendLine("        From TblItem A ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode " + mItCode);
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select AcNo2 As AcNo ");
                    SQL.AppendLine("    From TblEntity Where EntCode=@EntCodeWhs ");

                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select T.AcNo ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select AcNo ");
                        SQL.AppendLine("        From TblCostCategory Where 1=1" + mCCtCode);
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select T.AcNo ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblItemCostCategory A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And A.CCCode=B.CCCode " + mItCode);
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select AcNo1 As AcNo ");
                    SQL.AppendLine("    From TblEntity Where EntCode=@EntCodeCC ");

                }
            }
            else
            {
                if (mIsMovingAvgEnabled)
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select AcNo ");
                        SQL.AppendLine("        From TblCostCategory Where 1 = 1 " + mCCtCode);
                    }
                    else
                    {
                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblItemCostCategory A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And A.CCCode=B.CCCode " + mItCode);
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select A.AcNo ");
                    SQL.AppendLine("        From TblItem A ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode And B.MovingAvgInd='N' " + mItCode);

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo ");
                    SQL.AppendLine("        From TblItem A ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode And B.MovingAvgInd='Y' " +mItCode);
                    SQL.AppendLine("        Inner Join TblItemMovingAvg C On A.ItCode=C.ItCode ");
                }
                else
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select AcNo ");
                        SQL.AppendLine("        From TblCostCategory Where 1 = 1 " + mCCtCode);
                    }
                    else
                    {
                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblItemCostCategory A ");
                        SQL.AppendLine("        Inner Join TblCostCategory B On A.CCtCode=B.CCtCode And A.CCCode=B.CCCode " + mItCode);
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo ");
                    SQL.AppendLine("        From TblItem A  ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode " +mItCode);
                }
            }

            SQL.AppendLine(")T ; ");
           
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
               

                Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
                Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                return true;
            }

            return false;
        } */

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            
            string mAcNoForCOGS = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'AcNoForCOGS'");
            string mAcNoForSaleOfFinishedGoods = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'AcNoForSaleOfFinishedGoods'");
            
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
            if (IsJournalSettingInvalid_ItemCategory(Msg)) return true;

            if (mIsEntityMandatory && IsEntityDifferent())
            {
                //Parameter
                if (mAcNoForCOGS.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }

                if (mAcNoForSaleOfFinishedGoods.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                    return true;
                }

                //Table
                if (Sm.GetValue("Select AcNo2 From TblEntity Where EntCode = @Param", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode))).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Warehouse's COA account# (" + LueWhsCode.Text + ") is empty.");
                    return true;
                }
                if (Sm.GetValue("Select AcNo1 From TblEntity Where EntCode = @Param", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode))).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Cost Center's COA account# (" + LueCCCode.Text + ") is empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.Acno Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }


        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, CCtName = string.Empty;

            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode And B.AcNo is Null ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine("    And A.ItCode In ( ");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsCOAJournalCancelNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";


            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");

            if (mIsEntityMandatory && IsEntityDifferent())
            {
                if (mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS'  ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo2 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs  ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");

                    if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                    {

                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        if (mMenuCodeForDODeptCostCategory)
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                            SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }

                    }
                    SQL.AppendLine("    ) T Group By T.AcNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo1 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");

                    SQL.AppendLine("    Union All ");

                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo2 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs  ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");

                    if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                    {

                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                        SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        if (mMenuCodeForDODeptCostCategory)
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                            SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                            SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                            SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }

                    }
                    SQL.AppendLine("    ) T Group By T.AcNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo1 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS'  ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo2 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.ParValue As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods'  ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select T.AcNo ");
                    SQL.AppendLine("    From ( ");

                    if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                    {

                        SQL.AppendLine("        Select B.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        if (mMenuCodeForDODeptCostCategory)
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Select E.AcNo ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                    }
                    SQL.AppendLine("    ) T Group By T.AcNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select D.AcNo1 As AcNo ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC  ");
                    SQL.AppendLine("    Where A.DocNo=@DocNo ");
                }
            }
            else
            {
                if (mIsMovingAvgEnabled)
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode  ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select D.AcNo ");
                    SQL.AppendLine("        From TblDODeptDtl A ");
                    SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                    SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode  And D.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));

                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode  ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                        SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode  ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                        SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select D.AcNo ");
                    SQL.AppendLine("        From TblDODeptDtl A ");
                    SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                    SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
                }
                else
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode  ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select E.AcNo ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode  ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select D.AcNo ");
                    SQL.AppendLine("        From TblDODeptDtl A ");
                    SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                    SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode  ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
                }
            }

            SQL.AppendLine(")T ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                
                Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
                Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                return true;
            }

            return false;
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = string.Empty;

            if (Sm.GetLue(LueCCCode).Length > 0) EntCode2 = Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode));

            return (EntCode1 != EntCode2);
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock();
            if (mIsDODeptUseCostCenter)
            {
                if (!mMenuCodeForDODeptCostCategory) SetCCtName();
                SetAcNo();
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                if ((mIsAutoJournalActived &&
                    Sm.IsGrdValueEmpty(Grd1, Row, 28, false,
                    "Cost category is empty 1." + Environment.NewLine +
                    "Please contact Finance/Accounting Department !"
                    ))) return true;

                /* ini dipindah ke isJournalSettingInvalid
                if ((mIsAutoJournalActived &&
                    Sm.IsGrdValueEmpty(Grd1, Row, 29, false,
                        "COA's account# is empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !"
                        ))) return true;
                 */

                Msg =
                    "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[17].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 17) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }

                if (Grd1.Cols[20].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                {
                    Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 0) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Local Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Property : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd2, Row, 6) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd2, Row, 8) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd2, Row, 9) + Environment.NewLine + Environment.NewLine;

                    if (Sm.GetGrdDec(Grd2, Row, 12) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                        return true;
                    }

                    if (Grd2.Cols[16].Visible)
                    {

                        if (Sm.GetGrdDec(Grd2, Row, 16) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance (2) should not be less than 0.");
                            return true;
                        }
                    }

                    if (Grd2.Cols[20].Visible)
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 20) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance (3) should not be less than 0.");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveDODeptHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, ProductionWorkGroup, WORDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @DeptCode, @EntCode, @CCCode, ");
            if (mDODeptRequestBySource == "1")
            {
                SQL.AppendLine("(Select A.DocNo ");
                SQL.AppendLine("From TblRequestByHdr A, TblRequestByDtl B ");
                SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=B.DocNo And B.EmpCode=@EmpCode Limit 1), ");
            }
            else
            {
                SQL.AppendLine("Null, ");
            }
            SQL.AppendLine("@EmpCode, @ProductionWorkGroup, @WORDocNo, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString()};
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetLue(LueUserCode));
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@WORDocNo",TxtWORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDODeptDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* DODept2 - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, AcNo, CCtCode, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @ItCode_" + r.ToString() +
                        ", @ReplacementInd_" + r.ToString() +
                        ", @PropCode_" + r.ToString() +
                        ", @BatchNo_" + r.ToString() +
                        ", @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() + 
                        ", ");
                    if (mDODeptRequestBySource == "1")
                    {
                        SQL.AppendLine("(Select A.DocNo ");
                        SQL.AppendLine("From TblRequestByHdr A, TblRequestByDtl B ");
                        SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=B.DocNo And B.EmpCode=@EmpCode_" + r.ToString() + " Limit 1), ");
                    }
                    else
                        SQL.AppendLine("Null, ");
                    
                    SQL.AppendLine(
                        "  @EmpCode_" + r.ToString() +
                        ", @AssetCode_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @CCtCode_" + r.ToString() +
                        ", @Remark_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@ReplacementInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@PropCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 14));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                    Sm.CmParam<String>(ref cm, "@AssetCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 24));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 27));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 34));
                    Sm.CmParam<String>(ref cm, "@CCtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 35));

                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveDODeptDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, AcNo, CCtCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, @ReplacementInd, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, ");
        //    if (mDODeptRequestBySource == "1")
        //    {
        //        SQL.AppendLine("(Select A.DocNo ");
        //        SQL.AppendLine("From TblRequestByHdr A, TblRequestByDtl B ");
        //        SQL.AppendLine("Where A.CancelInd='N' And A.DocNo=B.DocNo And B.EmpCode=@EmpCode Limit 1), ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Null, ");
        //    }
        //    SQL.AppendLine("@EmpCode, @AssetCode, @AcNo, @CCtCode,@Remark, @UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@ReplacementInd", Sm.GetGrdBool(Grd1, Row, 8)?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 13));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 14));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 17));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 24));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 27));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 34));
        //    Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, Row, 35));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveStockMovement(string DocNo, string DNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");

            if (CancelInd == "N")
            {
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
                SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");
            }
            else
            {
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, ");
                SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (DNo.Length>0) Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(Byte Type, int Row)
        {
            //Type=1 -> Insert
            //Type=2 -> Edit
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            if (Type==1)
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            else
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('DO To Department : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                

                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");                    
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                                
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('DO To Department : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDODeptHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");
            if (mIsMovingAvgEnabled)
            {
                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On A.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            
            cml.Add(CancelDODeptDtl(DNo));

            cml.Add(SaveStockMovement(TxtDocNo.Text, DNo, "Y"));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 &&
                    Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2))
                    cml.Add(SaveStockSummary(2, Row));


            if (mIsAutoJournalActived)
            {
                if (mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal());
                else
                    cml.Add(SaveJournal2());
            }
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDODeptDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalCancelNotValid())||
                IsDocAlreadyProcessToPropertyCostComponent() ||
                IsCancelledItemNotExisted(DNo);
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelDODeptDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDODeptDtl Set ");
            SQL.AppendLine("CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";


            SQL.AppendLine("Update TblDODeptDtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select DocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {

                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                }
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {

                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                        SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                        SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    
                }
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblDODeptHdr A ");
                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.ParValue As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblParameter D On D.ParCode='AcNoForSaleOfFinishedGoods' And D.ParValue Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");

                if (mIsDODeptJournalCancelUseTheSameCCtAcNo)
                {
                    
                    SQL.AppendLine("        Select B.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mMenuCodeForDODeptCostCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                }
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                SQL.AppendLine("    D.AcNo1 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeCC And D.AcNo1 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Update TblDODeptDtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo" + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select DocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDODeptHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt ");
            SQL.AppendLine("    From (");
            if (mIsMovingAvgEnabled)
            {
                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));

                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*IfNull(H.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
            }
            else
            {
                if (mMenuCodeForDODeptCostCategory)
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On B.CCtCode=E.CCtCode And A.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select E.AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblDODeptDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter.Replace("B.", "A."));
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDODeptHdr(DocNo);
                ShowDODeptDtl(DocNo);
                ReComputeStockAfterSave();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDODeptHdr(string DocNo)
        {
            string RequestbyDocNo = string.Empty, EmpCode = string.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, ProductionWorkGroup, WORDocNo, Remark, JournalDocNo " +
                    "From TblDODeptHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LocalDocNo", "WhsCode", "DeptCode", "EntCode", 
                        
                        //6-10
                        "CCCode", "EmpCode", "RequestByDocNo", "Remark", "JournalDocNo",

                        //11-12
                        "ProductionWorkGroup", "WORDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[6]), string.Empty);
                        EmpCode = Sm.DrStr(dr, c[7]);
                        RequestbyDocNo = Sm.DrStr(dr, c[8]);
                        SetLueUserCode(ref LueUserCode, RequestbyDocNo, EmpCode);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtWORDocNo.EditValue = Sm.DrStr(dr, c[12]);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        private void ShowDODeptDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, ");
            SQL.AppendLine("B.ReplacementInd, B.PropCode, H.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.AssetCode, D.Assetname, D.DisplayName, B.EmpCode, ");
            if (mDODeptRequestBySource == "1")
            {
                SQL.AppendLine("IfNull(I.RequestName, L.EmpName) As EmpName, IfNull(K.PosName, M.PosName) As PosName, ");
            }
            else
            {
                SQL.AppendLine("I.EmpName, J.PosName, ");
            }
            SQL.AppendLine("B.Remark, IfNull(N.CCtName, F.CCtName) CCtName, G.AcNo, B.JournalDocNo, C.ItGrpCode, B.AcNo As AcNo2, B.CCtCode ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblAsset D On B.AssetCode = D.AssetCode ");
            SQL.AppendLine("Left Join TblItemCostCategory E On B.ItCode=E.ItCode And A.CCCode=E.CCCode ");
            SQL.AppendLine("Left Join TblCostCategory F On E.CCtCode=F.CCtCode And E.CCCode=F.CCCode ");
            SQL.AppendLine("Left Join TblItemCategory G On C.ItCtCode=G.ItCtCode ");
            SQL.AppendLine("Left Join TblProperty H On B.PropCode=H.PropCode ");
            if (mDODeptRequestBySource == "1")
            {
                SQL.AppendLine("Left Join TblRequestByDtl I On B.RequestByDocNo=I.DocNo And B.EmpCode = I.EmpCode ");
                SQL.AppendLine("Left Join TblEmployee J On I.EmpCode=J.EmpCode ");
                SQL.AppendLine("Left Join TblPosition K On J.PosCode=K.PosCode ");
                SQL.AppendLine("Left Join TblEmployee L On B.EmpCode = L.EmpCode ");
                SQL.AppendLine("Left Join TblPosition M On L.PosCode = M.PosCode ");
            }
            else
            {
                SQL.AppendLine("Left Join TblEmployee I On B.EmpCode = I.EmpCode ");
                SQL.AppendLine("Left Join TblPosition J On I.PosCode = J.PosCode ");
            }
            SQL.AppendLine("Left Join TblCostCategory N ON B.CCtCode = N.CCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "ReplacementInd",   
                    
                    //6-10
                    "PropCode", "PropName", "BatchNo", "Source", "Lot", 
                    
                    //11-15
                    "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",  
                    
                    //16-20
                    "Qty3", "InventoryUomCode3", "AssetCode", "AssetName", "EmpCode",   
                    
                    //21-25
                    "EmpName", "PosName", "Remark", "CCtName", "AcNo", 
                    
                    //26-30
                    "JournalDocNo", "ItGrpCode", "DisplayName", "ForeignName", "AcNo2",

                    //31
                    "CCtCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);

                    InsertSummary(
                        Sm.GetGrdStr(Grd1, Row, 4),
                        Sm.GetGrdStr(Grd1, Row, 6),
                        Sm.GetGrdStr(Grd1, Row, 7),
                        Sm.GetGrdStr(Grd1, Row, 33),
                        Sm.GetGrdStr(Grd1, Row, 9),
                        Sm.GetGrdStr(Grd1, Row, 10),
                        Sm.GetGrdStr(Grd1, Row, 11),
                        Sm.GetGrdStr(Grd1, Row, 12),
                        Sm.GetGrdStr(Grd1, Row, 13),
                        Sm.GetGrdStr(Grd1, Row, 14),
                        Sm.GetGrdStr(Grd1, Row, 16),
                        Sm.GetGrdStr(Grd1, Row, 18),
                        Sm.GetGrdStr(Grd1, Row, 20),
                        Sm.GetGrdDec(Grd1, Row, 15),
                        Sm.GetGrdDec(Grd1, Row, 17),
                        Sm.GetGrdDec(Grd1, Row, 19),
                        Sm.GetGrdStr(Grd1, Row, 31)
                    );
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2, 8 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 17, 19 });

            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private string GetAssetCode(int row)
        {
            string AssetName = Sm.GetGrdStr(Grd1, row, 23);
            var cm = new MySqlCommand() 
            { 
                CommandText = "Select AssetCode From TblAsset Where AssetName=@AssetName Limit 1;" 
            };
            Sm.CmParam<String>(ref cm, "@AssetName", AssetName);
            string AssetCode = Sm.GetValue(cm);
            if (AssetCode.Length == 0)
            {
                Sm.StdMsg(
                    mMsgType.Warning,
                    "Asset Name : " + AssetName + Environment.NewLine + 
                    "Invalid asset."
                    );
                Grd1.Cells[row, 23].Value = null;
            }
            return AssetCode;
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 24));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedInventory()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row, 0) +
                            Sm.GetGrdStr(Grd2, Row, 4) +
                            Sm.GetGrdStr(Grd2, Row, 6) +
                            Sm.GetGrdStr(Grd2, Row, 7) +
                            Sm.GetGrdStr(Grd2, Row, 8) +
                            Sm.GetGrdStr(Grd2, Row, 9) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedInventory2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row, 7) +
                            Sm.GetGrdStr(Grd2, Row, 8) +
                            Sm.GetGrdStr(Grd2, Row, 9) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd2.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 7).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd2, Row, 7);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 7), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 8), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 9), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 10, 3);
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 14, 4);
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 18, 5);

                                Grd2.Cells[row, 12].Value = Sm.GetGrdDec(Grd2, row, 10) - Sm.GetGrdDec(Grd2, row, 11);
                                Grd2.Cells[row, 16].Value = Sm.GetGrdDec(Grd2, row, 14) - Sm.GetGrdDec(Grd2, row, 15);
                                Grd2.Cells[row, 20].Value = Sm.GetGrdDec(Grd2, row, 18) - Sm.GetGrdDec(Grd2, row, 19);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ReComputeStockAfterSave()
        {
            ReComputeStock();
            ComputeSummary();

            Grd2.BeginUpdate();

            for (int row = 0; row < Grd2.Rows.Count - 1; row++)
            {
                Grd2.Cells[row, 10].Value = Sm.GetGrdDec(Grd2, row, 10) + Sm.GetGrdDec(Grd2, row, 11);
                Grd2.Cells[row, 14].Value = Sm.GetGrdDec(Grd2, row, 14) + Sm.GetGrdDec(Grd2, row, 15);
                Grd2.Cells[row, 18].Value = Sm.GetGrdDec(Grd2, row, 18) + Sm.GetGrdDec(Grd2, row, 19); 

                Grd2.Cells[row, 12].Value = Sm.GetGrdDec(Grd2, row, 10) - Sm.GetGrdDec(Grd2, row, 11);
                Grd2.Cells[row, 16].Value = Sm.GetGrdDec(Grd2, row, 14) - Sm.GetGrdDec(Grd2, row, 15);
                Grd2.Cells[row, 20].Value = Sm.GetGrdDec(Grd2, row, 18) - Sm.GetGrdDec(Grd2, row, 19); 
            }

            Grd2.EndUpdate();
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<DODept2>();
            var ldtl = new List<DODept2Dtl>();

            string[] TableName = { "DODept2", "DODept2Dtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, C.DeptName, A.Remark, D.CCName  ");
            SQL.AppendLine("From TblDODeptHdr A");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",

                         "WhsName",
                         "DeptName",
                         "Remark",
                         "CCName",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DODept2()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            DeptName = Sm.DrStr(dr, c[7]),
                            HRemark = Sm.DrStr(dr, c[8]),
                            CCName = Sm.DrStr(dr, c[9]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin,");
                SQLDtl.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, ");

                SQLDtl.AppendLine("B.Qty+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock, ");

                SQLDtl.AppendLine("B.Qty2+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty2) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock2, ");

                SQLDtl.AppendLine("B.Qty3+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty3) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock3, C.ItGrpCode ");

                SQLDtl.AppendLine("From TblDODeptHdr A ");
                SQLDtl.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.CancelInd = 'N' Order By B.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                        "ItCode" ,

                         //1-5
                         "ItName" ,
                         "BatchNo",
                         "Source", 
                         "Lot", 
                         "Bin",
                         
                         //6-10
                         "Qty" ,
                         "Qty2",
                         "Qty3",
                         "InventoryUomCode" ,
                         "InventoryUomCode2" ,

                         //11-15
                         "InventoryUomCode3" ,
                         "Remark" ,
                         "AvailableStock" ,
                         "AvailableStock2" ,
                         "AvailableStock3" ,

                         //16
                         "ItGrpCode"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new DODept2Dtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),

                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),

                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),

                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            DRemark = Sm.DrStr(drDtl, cDtl[12]),
                            AvailableStock = Sm.DrDec(drDtl, cDtl[13]),
                            AvailableStock2 = Sm.DrDec(drDtl, cDtl[14]),
                            AvailableStock3 = Sm.DrDec(drDtl, cDtl[15]),

                            ItGrpCode = Sm.DrStr(drDtl, cDtl[16])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            switch (parValue)
            {
                case 1:
                    Sm.PrintReport("DODept21", myLists, TableName, false);
                    break;
                case 2:
                    Sm.PrintReport("DODept22", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("DODept23", myLists, TableName, false);
                    break;
            }
        }

        private void SetLueUserCode(ref LookUpEdit Lue, string Param1, string Param2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mDODeptRequestBySource == "1")
            {
                //Param1 = RequestByDocNo
                //Param2 = EmpCodeCode
                if (Param2.Length > 0)
                {
                    SQL.AppendLine("Select T2.EmpCode As Col1, T2.RequestName As Col2 ");
                    SQL.AppendLine("From TblRequestByHdr T1 ");
                    SQL.AppendLine("Inner Join TblRequestByDtl T2 On T1.DocNo=T2.DocNo And T2.EmpCode=@EmpCode ");
                    SQL.AppendLine("Where T1.DocNo=@DocNo;");

                    Sm.CmParam<String>(ref cm, "@DocNo", Param1);
                    Sm.CmParam<String>(ref cm, "@EmpCode", Param2);
                }
                else
                {
                    SQL.AppendLine("Select T2.EmpCode As Col1, T2.RequestName As Col2 ");
                    SQL.AppendLine("From TblRequestByHdr T1 ");
                    SQL.AppendLine("Inner Join TblRequestByDtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
                    SQL.AppendLine("    And T3.DeptCode Is Not Null ");
                    SQL.AppendLine("    And T3.DeptCode=@DeptCode ");
                    SQL.AppendLine("    And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>=Replace(curdate(), '-', ''))) ");
                    SQL.AppendLine("Where T1.CancelInd='N' ");
                    SQL.AppendLine("Order By T2.RequestName;");

                    Sm.CmParam<String>(ref cm, "@DeptCode", Param1);
                }
            }
            else
            {
                //Param1 = DeptCode
                //Param2 = UserCode

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 From TblUser ");
                if (Param2.Length == 0)
                {
                    SQL.AppendLine("Where ExpDt Is Null ");
                    SQL.AppendLine("And UserCode In ( ");
                    SQL.AppendLine("    Select UserCode From TblEmployee ");
                    SQL.AppendLine("    Where UserCode Is Not Null ");
                    SQL.AppendLine("    And DeptCode=@DeptCode ");
                    SQL.AppendLine("    And DeptCode Is Not Null ");
                    SQL.AppendLine("    And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>=Replace(curdate(), '-', ''))) ");
                    SQL.AppendLine(") Order By UserName;");

                    Sm.CmParam<String>(ref cm, "@DeptCode", Param1);
                }
                else
                {
                    SQL.AppendLine("Where UserCode=@UserCode;");

                    Sm.CmParam<String>(ref cm, "@UserCode", Param2);
                }
            }
            cm.CommandText = SQL.ToString();
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Param2.Length > 0) Sm.SetLue(Lue, Param2);
        }

        private void SetLueEmpCode(ref LookUpEdit Lue, string DeptCode, string EmpCode)
        {
            var SQL = new StringBuilder();

            if (EmpCode.Length == 0)
            {
                SQL.AppendLine("Select A.EmpCode As Col1, A.EmpName As Col2, B.PosName As Col3 ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode And B.RequestItemInd = 'Y' ");
                SQL.AppendLine("Where A.ResignDt Is Null And IfNull(A.DeptCode, '')='" + DeptCode + "' ");
                SQL.AppendLine("Order By A.EmpName; ");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode As Col1, A.EmpName As Col2, B.PosName As Col3 ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
                SQL.AppendLine("Where A.EmpCode='" + EmpCode + "'; ");
            }

            Sm.SetLue3(
                ref Lue,
                SQL.ToString(),
                0, 40, 40, false, true, true, "Code", "Name", "Position", "Col2", "Col1");
        }

        private void SetLueCCCode(ref LookUpEdit Lue, string CCCode)
        {
            var SQL = new StringBuilder();
                        
            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter ");
            if (CCCode.Length==0)
            {
                SQL.AppendLine("Where CCCode  Not In ( ");
                SQL.AppendLine("    Select Parent From TblCostCenter ");
                SQL.AppendLine("    Where Parent Is Not Null ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And ActInd='Y' ");
                SQL.AppendLine("Order By CCName;");
            }
            else
                SQL.AppendLine("Where CCCode=@CCCode ");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            if (CCCode.Length>0) Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CCCode.Length > 0) Sm.SetLue(LueCCCode, CCCode);
        }

        private void SetCCtName()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty, CCtName = string.Empty, AcNo = string.Empty, Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 4);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 1=0;";

            SQL.AppendLine("Select A.ItCode, B.CCtName, B.AcNo ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCCode=B.CCCode And A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCtName", "AcNo" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        CCtName = Sm.DrStr(dr, 1);
                        AcNo = Sm.DrStr(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 4), ItCode))
                            {
                                Grd1.Cells[r, 28].Value = CCtName;
                                Grd1.Cells[r, 34].Value = AcNo;
                            }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetAcNo()
        {
            var cm = new MySqlCommand(); 
            var SQL = new StringBuilder();
            string ItCode = string.Empty, AcNo= string.Empty, Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 4);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 1=0;";

            SQL.AppendLine("Select A.ItCode, B.AcNo ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "AcNo" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        AcNo = Sm.DrStr(dr, 1);
                        for (int r = 0; r<Grd1.Rows.Count-1; r++)
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 4), ItCode))
                                Grd1.Cells[r, 29].Value = AcNo;
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void InsertSummary(
            string ItCode, string ItCodeInternal, string ItName, string ForeignName, string PropCode, string PropName, 
            string BatchNo, string Source, string Lot, string Bin, 
            string InventoryUomCode, string InventoryUomCode2, string InventoryUomCode3,
            decimal Qty, decimal Qty2, decimal Qty3,
            string ItGrpCode
            )
        {
            int TheRow = -1;
            string key = ItCode + PropCode + BatchNo + Source + Lot + Bin;
            for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.CompareStr(key,
                    Sm.GetGrdStr(Grd2, Row, 0) +
                    Sm.GetGrdStr(Grd2, Row, 4) +
                    Sm.GetGrdStr(Grd2, Row, 6) +
                    Sm.GetGrdStr(Grd2, Row, 7) +
                    Sm.GetGrdStr(Grd2, Row, 8) +
                    Sm.GetGrdStr(Grd2, Row, 9)
                    ))
                {
                    TheRow = Row;
                    break;
                }
            }

            if (TheRow == -1)
            {
                TheRow = Grd2.Rows.Count-1;

                Grd2.Cells[TheRow, 0].Value = ItCode;
                Grd2.Cells[TheRow, 2].Value = ItCodeInternal;
                Grd2.Cells[TheRow, 3].Value = ItName;
                Grd2.Cells[TheRow, 4].Value = PropCode;
                Grd2.Cells[TheRow, 5].Value = PropName;
                Grd2.Cells[TheRow, 6].Value = BatchNo;
                Grd2.Cells[TheRow, 7].Value = Source;
                Grd2.Cells[TheRow, 8].Value = Lot;
                Grd2.Cells[TheRow, 9].Value = Bin;
                Grd2.Cells[TheRow, 10].Value = Qty;
                Grd2.Cells[TheRow, 11].Value = 0m;
                Grd2.Cells[TheRow, 12].Value = Qty;
                Grd2.Cells[TheRow, 13].Value = InventoryUomCode;
                Grd2.Cells[TheRow, 14].Value = Qty2;
                Grd2.Cells[TheRow, 15].Value = 0m;
                Grd2.Cells[TheRow, 16].Value = Qty2;
                Grd2.Cells[TheRow, 17].Value = InventoryUomCode2;
                Grd2.Cells[TheRow, 18].Value = Qty3;
                Grd2.Cells[TheRow, 19].Value = 0m;
                Grd2.Cells[TheRow, 20].Value = Qty3;
                Grd2.Cells[TheRow, 21].Value = InventoryUomCode3;
                Grd2.Cells[TheRow, 22].Value = ItGrpCode;
                Grd2.Cells[TheRow, 23].Value = ForeignName;

                Grd2.Rows.Add();
                Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20 });
            }
        }

        internal void ComputeSummary()
        {
            string Key = string.Empty;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;

            Grd2.BeginUpdate();

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                {
                    Qty = 0m; 
                    Qty2 = 0m; 
                    Qty3 = 0m;
                    Key =
                        Sm.GetGrdStr(Grd2, Row, 0) +
                        Sm.GetGrdStr(Grd2, Row, 4) +
                        Sm.GetGrdStr(Grd2, Row, 6) +
                        Sm.GetGrdStr(Grd2, Row, 7) +
                        Sm.GetGrdStr(Grd2, Row, 8) +
                        Sm.GetGrdStr(Grd2, Row, 9);

                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row2, 4).Length != 0)
                        {
                            if (Sm.CompareStr(Key,
                                Sm.GetGrdStr(Grd1, Row2, 4) +
                                Sm.GetGrdStr(Grd1, Row2, 9) +
                                Sm.GetGrdStr(Grd1, Row2, 11) +
                                Sm.GetGrdStr(Grd1, Row2, 12) +
                                Sm.GetGrdStr(Grd1, Row2, 13) +
                                Sm.GetGrdStr(Grd1, Row2, 14)
                                ) && !Sm.GetGrdBool(Grd1, Row2, 1))
                            {
                                Qty += Sm.GetGrdDec(Grd1, Row2, 15);
                                Qty2 += Sm.GetGrdDec(Grd1, Row2, 17);
                                Qty3 += Sm.GetGrdDec(Grd1, Row2, 19);
                            }
                        }
                    }
                    Grd2.Cells[Row, 11].Value = Qty;
                    Grd2.Cells[Row, 12].Value = Sm.GetGrdDec(Grd2, Row, 10) - Qty;
                    Grd2.Cells[Row, 15].Value = Qty2;
                    Grd2.Cells[Row, 16].Value = Sm.GetGrdDec(Grd2, Row, 14) - Qty2;
                    Grd2.Cells[Row, 19].Value = Qty3;
                    Grd2.Cells[Row, 20].Value = Sm.GetGrdDec(Grd2, Row, 19) - Qty3;
                }
            }
            Grd2.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnWORDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDODept2Dlg5(this));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsAssetColumnEmptied)
                {
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                    for (int row = 0; row < Grd1.Rows.Count; row++)
                    {
                        Grd1.Cells[row, 22].Value = null;
                        Grd1.Cells[row, 23].Value = null;
                        Grd1.Cells[row, 32].Value = null;
                    }
                }

                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");

                LueUserCode.EditValue = null;
                LueEmpCode.EditValue = null;
                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    for (int col = 24; col <= 26; col++)
                        Grd1.Cells[row, col].Value = null;
                }

                if (Sm.GetLue(LueDeptCode).Length != 0)
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueUserCode }, false);
                    SetLueUserCode(ref LueUserCode, Sm.GetLue(LueDeptCode), string.Empty);
                    SetLueUserCode(ref LueEmpCode, Sm.GetLue(LueDeptCode), string.Empty);
                    //SetLueEmpCode(ref LueEmpCode, Sm.GetLue(LueDeptCode), string.Empty);
                }
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueUserCode }, true);
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);

                if (mIsDODeptUseEntity)
                {
                    var EntCode = Sm.GetLue(LueEntCode);
                    if (EntCode.Length > 0)
                    {
                        LueCCCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeByEntity), mIsFilterByCC ? "Y" : "N", EntCode);
                    }
                    else
                    {
                        LueCCCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                    }
                }
                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    Grd1.Cells[row, 28].Value = null;
                    Grd1.Cells[row, 29].Value = null;
                }
            }
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsAssetColumnEmptied)
                {
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                    for (int row = 0; row < Grd1.Rows.Count; row++)
                    {
                        Grd1.Cells[row, 22].Value = null;
                        Grd1.Cells[row, 23].Value = null;
                        Grd1.Cells[row, 32].Value = null;
                    }
                }

                if (mIsDODeptUseEntity)
                {
                    var EntCode = Sm.GetLue(LueEntCode);
                    if (EntCode.Length > 0)
                        Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeByEntity), mIsFilterByCC ? "Y" : "N", EntCode);
                    else
                        Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                }
                else
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");

                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    Grd1.Cells[row, 28].Value = null;
                    Grd1.Cells[row, 29].Value = null;
                }
            }
        }

        private void LueUserCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueUserCode, new Sm.RefreshLue3(SetLueUserCode), Sm.GetLue(LueDeptCode), string.Empty);
            
        }

        private void LueEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmpCode, new Sm.RefreshLue3(SetLueUserCode), Sm.GetLue(LueDeptCode), string.Empty);
        }

        private void LueEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEmpCode_Leave(object sender, EventArgs e)
        {
            if (LueEmpCode.Visible && fAccept && fCell.ColIndex == 25)
            {
                if (Sm.GetLue(LueEmpCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 24].Value = null;
                    Grd1.Cells[fCell.RowIndex, 25].Value = null;
                    Grd1.Cells[fCell.RowIndex, 26].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 24].Value = Sm.GetLue(LueEmpCode);
                    Grd1.Cells[fCell.RowIndex, 25].Value = LueEmpCode.GetColumnValue("Col2");
                    Grd1.Cells[fCell.RowIndex, 26].Value = LueEmpCode.GetColumnValue("Col3");
                }
                LueEmpCode.Visible = false;
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Report Class

        #region Report Class

        class DODept2
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string DeptName { get; set; }
            public string HRemark { get; set; }
            public string CCName { get; set; }
            public string PrintBy { get; set; }
        }

        class DODept2Dtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string DRemark { get; set; }
            public decimal AvailableStock { get; set; }
            public decimal AvailableStock2 { get; set; }
            public decimal AvailableStock3 { get; set; }
            public string ItGrpCode { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
        }

        #endregion

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }

        #endregion       

       

    }
}
