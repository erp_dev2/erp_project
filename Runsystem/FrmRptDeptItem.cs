﻿#region Update
/*
    19/03/2018 [TKG] reporting Department use item tambah total, digit 2 angka blkg koma
    12/04/2022 [RIS/PHT] Merubah source qty dan amount menggunakan parameter IsRptProcurementUsePORevision
    29/12/2022 [ICA/ALL] bug saat show data, bagian focus row dibuat 0, sebelumnya 1
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmRptDeptItem : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            nSQL="", 
            mSQL = string.Empty, 
            mSQL1 = string.Empty, 
            mSQL2 = string.Empty, 
            mSQL3 = string.Empty;

        private bool mIsShowForeignName = false, mIsFilterByItCt = false , mIsRptProcurementUsePORevision = false;

        #endregion

        #region Constructor

        public FrmRptDeptItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                SetLueOptDept(LueOptDept);
                Sm.SetLue(LueOptDept, "Show All Department");
                SetLueOptMonitoring(LueOptMonitoring);
                Sm.SetLue(LueOptMonitoring, "Purchase");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Department", 
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Quantity",
                        "Unit Price",
                        
                        //6-7
                        "Amount",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 100, 200, 100, 150,  
                        
                        //6-7
                        150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 2);
            Grd1.Cols[7].Move(4);
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            
            //purchase group dept
            //SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select DocDt, DeptName, ItCode, ItName, ForeignName, Sum(Qty) As Qty, Sum(Amt) As Amt, Sum(Amt)/Sum(Qty) As UPrice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.DocDt, I.DeptName, G.ItCode, H.ItName, H.ForeignName, ");
            if(mIsRptProcurementUsePORevision)
                SQL.AppendLine("ifnull(J.Qty, B.Qty) as Qty, ifnull(J.Qty, B.Qty)*E.UPrice* "); //mengambil nilai revision
            else
                SQL.AppendLine("B.Qty, B.Qty*ifnull(K.UPrice, E.UPrice)* "); //mengambil nilai default
            SQL.AppendLine("Case When IfNull((Select ParValue From TblParameter Where ParCode='MainCurCode'), '')=D.CurCode Then 1.00 ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("Select Amt From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1=D.CurCode And CurCode2=IfNull((Select ParValue From TblParameter Where ParCode='MainCurCode'), '') ");
            SQL.AppendLine("Order By RateDt Desc Limit 1), 0) ");
            SQL.AppendLine("End As Amt ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr D On C.QtDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl E On C.QtDocNo=E.DocNo And C.QtDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr F On C.MaterialRequestDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On C.MaterialRequestDocNo=G.DocNo And C.MaterialRequestDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=H.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment I On F.DeptCode=I.DeptCode ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
                SQL.AppendLine("    FROM tblpodtl A ");
                SQL.AppendLine("    INNER JOIN TblPoRevision B ON A.DocNo = B.podocno AND A.DNo = B.PoDNo ");
                SQL.AppendLine("    INNER JOIN TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
                SQL.AppendLine("    WHERE B.docno = (SELECT MAX(DocNo) FROM TblPoRevision WHERE PoDocNo = B.PoDocNo AND PODNo = B.PODNo AND STATUS = 'A') ");
                SQL.AppendLine(") J ON A.DocNo = J.PoDocNo AND B.DNo = J.PoDNo ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.DocNo AS PoDoCno, A.DNo AS PoDNo, C.DocNo AS QtDocNo, C.UPrice, B.Discountold, B.DiscountAmtOld, B.RoundingValueOld ");
            SQL.AppendLine("    From TblPoDtl A ");
            SQL.AppendLine("    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C ON B.QtDocNoOld = C.DocNo AND B.QtDNoOld = C.DNo  ");
            SQL.AppendLine("    Where B.docno = (Select MIN(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
            SQL.AppendLine(") K ON A.DocNo = K.PoDocNo And B.DNo = K.PoDNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By DeptName, ItCode, ItName ");
            SQL.AppendLine("Order By DeptName, Qty Desc ");
            //SQL.AppendLine(")T1 ");

            mSQL = SQL.ToString();

            //purchase All dept

            SQL1.AppendLine("Select DocDt, DeptName, ItCode, ItName, ForeignName, Sum(Qty) As Qty, Sum(Amt) As Amt, Sum(Amt)/Sum(Qty) As UPrice ");
            SQL1.AppendLine("From ( ");
            SQL1.AppendLine("Select A.DocDt, 'All Department' As DeptName, G.ItCode, H.ItName, H.ForeignName, ");
            if (mIsRptProcurementUsePORevision)
                SQL1.AppendLine("ifnull(J.Qty, B.Qty) as Qty, ifnull(J.Qty, B.Qty)*E.UPrice* "); //mengambil nilai revision
            else
                SQL1.AppendLine("B.Qty, B.Qty*ifnull(K.UPrice, E.UPrice)* "); //mengambil nilai default
            SQL1.AppendLine("Case When IfNull((Select ParValue From TblParameter Where ParCode='MainCurCode'), '')=D.CurCode Then 1.00 ");
            SQL1.AppendLine("Else ");
            SQL1.AppendLine("IfNull(( ");
            SQL1.AppendLine("Select Amt From TblCurrencyRate ");
            SQL1.AppendLine("Where CurCode1=D.CurCode And CurCode2=IfNull((Select ParValue From TblParameter Where ParCode='MainCurCode'), '') ");
            SQL1.AppendLine("Order By RateDt Desc Limit 1), 0) ");
            SQL1.AppendLine("End As Amt ");
            SQL1.AppendLine("From TblPOHdr A ");
            SQL1.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL1.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL1.AppendLine("Inner Join TblQtHdr D On C.QtDocNo=D.DocNo ");
            SQL1.AppendLine("Inner Join TblQtDtl E On C.QtDocNo=E.DocNo And C.QtDNo=E.DNo ");
            SQL1.AppendLine("Inner Join TblMaterialRequestHdr F On C.MaterialRequestDocNo=F.DocNo ");
            SQL1.AppendLine("Inner Join TblMaterialRequestDtl G On C.MaterialRequestDocNo=G.DocNo And C.MaterialRequestDNo=G.DNo ");
            SQL1.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL1.AppendLine("And Exists( ");
                SQL1.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL1.AppendLine("    Where ItCtCode=H.ItCtCode ");
                SQL1.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL1.AppendLine("    ) ");
            }
            SQL1.AppendLine("Inner Join TblDepartment I On F.DeptCode=I.DeptCode ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL1.AppendLine("LEFT JOIN ( ");
                SQL1.AppendLine("    SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
                SQL1.AppendLine("    FROM tblpodtl A ");
                SQL1.AppendLine("    INNER JOIN TblPoRevision B ON A.DocNo = B.podocno AND A.DNo = B.PoDNo ");
                SQL1.AppendLine("    INNER JOIN TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
                SQL1.AppendLine("    WHERE B.docno = (SELECT MAX(DocNo) FROM TblPoRevision WHERE PoDocNo = B.PoDocNo AND PODNo = B.PODNo AND STATUS = 'A') ");
                SQL1.AppendLine(") J ON A.DocNo = J.PoDocNo AND B.DNo = J.PoDNo ");
            }
            SQL1.AppendLine("Left Join ( ");
            SQL1.AppendLine("    Select A.DocNo AS PoDoCno, A.DNo AS PoDNo, C.DocNo AS QtDocNo, C.UPrice, B.Discountold, B.DiscountAmtOld, B.RoundingValueOld ");
            SQL1.AppendLine("    From TblPoDtl A ");
            SQL1.AppendLine("    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
            SQL1.AppendLine("    Inner Join TblQtDtl C ON B.QtDocNoOld = C.DocNo AND B.QtDNoOld = C.DNo  ");
            SQL1.AppendLine("    Where B.docno = (Select MIN(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
            SQL1.AppendLine(") K ON A.DocNo = K.PoDocNo And B.DNo = K.PoDNo ");
            SQL1.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL1.AppendLine(") T ");
            SQL1.AppendLine("Group By DeptName, ItCode, ItName ");
            SQL1.AppendLine("Order By DeptName, Qty Desc ");

            mSQL1 = SQL1.ToString();

            //DO Group Dept
            SQL2.AppendLine("Select DeptName, ItCode, ItName, ForeignName, Sum(Qty) As Qty, Sum(Amt) As Amt, Sum(Amt)/Sum(Qty) As UPrice ");
            SQL2.AppendLine("From ( ");
            SQL2.AppendLine("Select A.DocDt, D.DeptName, B.ItCode, C.ItName, C.ForeignName, B.Qty, (B.Qty*E.UPrice* E.ExcRate) As Amt ");
            SQL2.AppendLine("From TblDODeptHdr A ");
            SQL2.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL2.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL2.AppendLine("And Exists( ");
                SQL2.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL2.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL2.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL2.AppendLine("    ) ");
            }
            SQL2.AppendLine("Inner Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL2.AppendLine("Inner Join TblStockPrice E On B.ItCode=E.ItCode And B.BatchNo=E.BatchNo And B.Source=E.Source ");
            SQL2.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL2.AppendLine(") T ");
            SQL2.AppendLine("Group By DeptName, ItCode, ItName ");
            SQL2.AppendLine("Order By DeptName, Qty Desc ");

            mSQL2 = SQL2.ToString();

            //DO All Dept
            SQL3.AppendLine("Select 'All Department' As DeptName, ItCode, ItName, ForeignName, Sum(Qty) As Qty, Sum(Amt) As Amt, Sum(Amt)/Sum(Qty) As UPrice ");
            SQL3.AppendLine("From ( ");
            SQL3.AppendLine("Select A.DocDt, D.DeptName, B.ItCode, C.ItName, C.ForeignName, B.Qty, (B.Qty*E.UPrice* E.ExcRate) As Amt ");
            SQL3.AppendLine("From TblDODeptHdr A ");
            SQL3.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL3.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL3.AppendLine("And Exists( ");
                SQL3.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL3.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL3.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL3.AppendLine("    ) ");
            }
            SQL3.AppendLine("Inner Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL3.AppendLine("Inner Join TblStockPrice E On B.ItCode=E.ItCode And B.BatchNo=E.BatchNo And B.Source=E.Source ");
            SQL3.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL3.AppendLine(") T ");
            SQL3.AppendLine("Group By DeptName, ItCode, ItName ");
            SQL3.AppendLine("Order By DeptName, Qty Desc ");

            mSQL3 = SQL3.ToString();
        }

        private bool IsSelectedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsLueEmpty(LueOptDept, "Department Option") ||
                Sm.IsLueEmpty(LueOptMonitoring, "Monitoring Option");
        }


        override protected void ShowData()
        {
            if (IsSelectedDataNotValid()) return;

            if (Sm.GetLue(LueOptMonitoring) == "Purchase" && Sm.GetLue(LueOptDept) == "Show All Department")
            {
                nSQL = mSQL1;
            }
            else if (Sm.GetLue(LueOptMonitoring) == "Purchase" && Sm.GetLue(LueOptDept) == "Group By Department")
            {
                nSQL = mSQL;
            }
            else if (Sm.GetLue(LueOptMonitoring) == "Usage" && Sm.GetLue(LueOptDept) == "Show All Department")
            {
                nSQL = mSQL3;
            }
            else if (Sm.GetLue(LueOptMonitoring) == "Usage" && Sm.GetLue(LueOptDept) == "Group By Department")
            {
                nSQL = mSQL2;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(

                        ref Grd1, ref cm,
                        nSQL + Filter,
                        new string[]
                        {
                            //0
                            "DeptName", 

                            //1-5
                            "ItCode", "ItName", "Qty", "UPrice", "Amt",

                            //6
                            "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        }, true, true, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, (ChkHideInfoInGrd.Checked ? 4 : 2));
                Cursor.Current = Cursors.Default;
            }
        }

        
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");
        }

        public static void SetLueOptDept(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { "Show All Department", "Group By Department" });
        }

        public static void SetLueOptMonitoring(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { "Purchase", "Usage" });
        }

        internal static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion
    }
}
