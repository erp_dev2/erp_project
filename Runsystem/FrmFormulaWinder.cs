﻿#region
/*
    01/11/2017 [TKG] Aplikasi baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFormulaWinder : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmFormulaWinderFind FrmFind;

        #endregion

        #region Constructor

        public FrmFormulaWinder(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Code",
                        "Name",
                        "Speed",
                        "NE",
                        "Del MC",

                        //6-8
                        "Del Run",
                        "100% Kg/Hr",
                        "100% Del"
                    },
                     new int[] 
                    {
                        20,
                        100, 200, 120, 120, 120,
                        120, 120, 120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, 2);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtFWCode, TxtFWName, MeeRemark, ChkActInd }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    TxtFWCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFWCode, TxtFWName, MeeRemark, ChkActInd }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    TxtFWCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFWName, MeeRemark, ChkActInd }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    TxtFWName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtFWCode, TxtFWName, MeeRemark });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFormulaWinderFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFWCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                    IsDataNotValid()) return;
                
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveFormulaWinderHdr());
                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                            cml.Add(SaveFormulaWinderDtl(r));
                }

                Sm.ExecCommands(cml);

                ShowData(TxtFWCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmFormulaWinderDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 3, 4, 5, 6, 7, 8 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7, 8 });
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmFormulaWinderDlg(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, e);
        }

        #endregion

        #region Show Data

        public void ShowData(string FWCode)
        {
            try
            {
                ClearData();
                ShowFormulaWinderHdr(FWCode);
                ShowFormulaWinderDtl(FWCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowFormulaWinderHdr(string FWCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@FWCode", FWCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select FWCode, FWName, ActInd, Remark " +
                  "From TblFormulaWinderHdr Where FWCode=@FWCode;",
                 new string[] 
                   {
                      //0
                      "FWCode",
                      //1-3
                      "FWName", "ActInd", "Remark"

                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtFWCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtFWName.EditValue = Sm.DrStr(dr, c[1]);
                     ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                     MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                 }, true
             );
        }

        private void ShowFormulaWinderDtl(string FWCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@FWCode", FWCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ItCode, B.ItName, A.Speed, A.NE, A.DelMC, A.DelRun, A.MFR, A.Del " +
                    "From TblFormulaWinderDtl A " +
                    "Left Join TblItemSpinning B On A.ItCode=B.ItCode " +
                    "Where A.FWCode=@FWCode Order By B.ItName;",

                    new string[] 
                       { 
                           //0
                           "ItCode",

                           //1-5
                           "ItName", "Speed", "NE", "DelMC", "DelRun", 
                           
                           //6-7
                           "MFR", "Del" 
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFWCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtFWName, "Name", false) ||
                IsFWCodeExisted() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.")) return true;
            return false;
        }

        private bool IsFWCodeExisted()
        {
            if (!TxtFWCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblFormulaWinderHdr Where FWCode=@Param;", TxtFWCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Code ( " + TxtFWCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveFormulaWinderHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblFormulaWinderHdr ");
            SQL.AppendLine("(FWCode, FWName, ActInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@FWCode, @FWName, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update FWName=@FWName, ActInd=@ActInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtFWCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblFormulaWinderDtl Where FWCode=@FWCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@FWCode", TxtFWCode.Text);
            Sm.CmParam<String>(ref cm, "@FWName", TxtFWName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveFormulaWinderDtl(int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblFormulaWinderDtl(FWCode, ItCode, Speed, NE, DelMC, DelRun, MFR, Del, CreateBy, CreateDt) " +
                    "Values(@FWCode, @ItCode, @Speed, @NE, @DelMC, @DelRun, @MFR, @Del, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@FWCode", TxtFWCode.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@Speed", Sm.GetGrdDec(Grd1, r, 3));
            Sm.CmParam<Decimal>(ref cm, "@NE", Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<Decimal>(ref cm, "@DelMC", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@DelRun", Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@MFR", Sm.GetGrdDec(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@Del", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Misc Event

        #region Misc Control Event

        private void TxtFWCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFWCode);
        }

        private void TxtFWName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFWName);
        }

        #endregion

        #endregion
    }
}
