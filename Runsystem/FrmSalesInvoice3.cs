﻿#region Old
/*
 * 20/04/2017 [HAR] perubahan di printout dan aturan additinal discount jika ada account kepala 4 yang normalnya di debet dia tidak berlaku rule debet di debet = +, 
    27/04/2017 [TKG] bug fixing validasi sales invoice sudah diproses menjadi incoming payment.
    24/05/2017 [TKG] Journal selisih kurs.
    30/05/2017 [TKG] Berdasarkan parameter IsRemarkForJournalMandatory, Remark harus disi atau tidak.
    30/05/2017 [TKG] Update remark di journal 
    11/06/2017 [TKG] tambah validasi vat settlement ketika data hendak dicancel. 
    17/07/2017 [HAR] tambah tax di header
    19/07/2017 [TKG] tambah cancel reason, tambah entity saat journal.
    29/07/2017 [TKG] Untuk pajak saat journal menggunakan rate nomor rekening coa dan tax rate dari master tax.
    02/08/2017 [TKG] tambah entity
    15/08/2017 [TKG] untuk tax yg nilai minus, coa's accountnya menggunakan input tax coa.
    03/11/2017 [WED] insert ke tabel baru, untuk menyimpan curcode dan excrate saat ada downpayment (hanya saat insert)
    03/11/2017 [WED] update ke tabel summary2 saat edit
    31/01/2018 [HAR] tambah parameter buat ngeset due date dan taxcode 1
    02/02/2018 [ARI] tambah printout KIM
    23/02/2018 [ARI] tambah NPWP customer, remark header, keterangan, nama user dan moble number user 
    27/03/2018 [ARI] feedback printout
    29/03/2018 [ARI] tambah printout kwitansi
    03/04/2018 [ARI] feedback printout kwitansi (rubah layout)
    02/04/2018 [HAR] Rounding tax ke bawah berdasarkan parameter
    03/04/2018 [HAR] Rounding DO amount detail dan header ke atas berdasarkan parameter
    11/04/2018 [ARI] Rounding tax ke bawah, Amount Detail rounding ke atas
    13/04/2018 [ARI] remark di detail ambil dari remark do customer header
    07/05/2018 [ARI] Feedback printout 
    21/05/2018 [HAR] save journal, nilainya di rounding berdasarkan param, dialog tambah info shipping address dan remark header
    26/07/2018 [ARI] revisi printout kim
    31/07/2018 [HAR] BUG Receipt No
    07/08/2018 [TKG] Printout menampilkan foreign name item (KIM).
    20/09/2018 [WED] BUG saat print, nilai pajak nya belum di rounding berdasarkan parameter IsDOCtAmtRounded
    30/09/2018 [TKG] menghilangkan angka di belakang koma
    11/02/2019 [MEY] menonaktifkan fitur Print untuk KIM
    30/06/2019 [TKG] Berdasarkan parameter IsSalesInvoiceTaxInvDocEditable, data tax invoice document# bisa diedit atau tidak.
    21/08/2019 [DITA] Printout sale invoice baru KSM
    21/08/2019 [TKG] tambah DO local document
    25/09/2019 [DITA/SIER] Copy source dari sales invoice yg sudah pernah dibuat
    10/01/2020 [WED/ALL] pakai parameter untuk print out nya
    13/01/2020 [VIN/SIER] prinout Sales Invoice 
    21/01/2020 [DITA/SIER] BUG ADDRESS prinout Sales Invoice 
    05/03/2020 [IBL/KBN] Penomoran dokumen 6 digit, reset tiap tahun, dan berdasarkan entity
    09/03/2020 [VIN/SIER] sesuaikan printout
    16/04/2020 [VIN/SIER] printout : item yg memiliki group akan ditampilkan nama groupnya (bukan nama item) dan dikelompokkan berdasarkan group name, uom, harga, remark
    05/05/2020 [VIN/KIM] bug akun uang muka
    11/05/2020 [DITA/SIER] tambah parameter mIsSLIUseAutoFacturNumber untuk mengisi tax invoice number secara otomatis dari factur number
    23/06/2020 [HAR/SIER] bug print
    14/07/2020 [DITA/MMM] Bug Printout : Unit price tidak muncul
    21/07/2020 [VIN/SRN] Bug: add cost & discount tdk muncul di printout
    27/07/2020 [IBL/SIER] penyesuaian printout
    11/08/2020 [WED/SRN] pilihan additional discount dan cost masuk hitungan atau tidak, berdasarkan parameter SLI3TaxCalculationFormat
    12/08/2020 [WED/SRN] tmabah validasi yg di tick harus balanced di Additional Cost Discount berdasarkan parameter SLI3TaxCalculationFormat
    14/08/2020 [HAR/DMK] tambah kode customer dan print TTD
    02/09/2020 [IBL/SIER] Bug: Unknown column TTDPrintout saat print
    11/09/2020 [TKG/SIER] Bug berhubungan dengan pajak (save hdr dan journal)
    15/09/2020 [VIN/SIER] printout: ppn dan pph dipisah
    15/09/2020 [VIN/SIER] printout: remark COA non customer ditampilakn di bwh tax
    16/09/2020 [VIN/SIER] printout: remark ambil dari doct
    24/09/2020 [TKG/SIER] tambah customer category yg difilter berdasarkan otorisasi group thd customer category
    08/11/2020 [TKG/PHT] Berdasarkan parameter IsSalesInvoice3TaxInvDocumentAbleToEdit diperlukan fasilitas untuk edit faktur number di sales invoice yang sudah tercreate (dan hanya bisa sekali saja editnya)
    02/12/2020 [IBL/SIER] Lue customer menamplikan customer category, berdasarkan parameter IsCustomerComboShowCategory
    22/12/2020 [WED/PHT] param baru untuk journal piutang usaha, itu melihat rate nya berdasarkan tanggal rate DO bukan SLI
    06/01/2021 [IBL/SRN] validasi coa kosong saat save journal berdasarkan parameter IsCheckCOAJournalNotExists
    17/01/2021 [TKG/PHT] ubah GenerateReceipt
    19/01/2021 [TKG/PHT] ubah journal, apabila journal tidak balance, selisih masuk ke nomor rekening piutang usaha customer
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    01/02/2021 [WED/PHT] ubah rumus journal berdasarkan parameter IsItemCategoryUseCOAAPAR
    02/02/2021 [WED/PHT] ubah lagi rumus journal karna di item category ketambahan COA AP baru
    24/02/2021 [WED/SIER] tambah inputan Customer Category berdasarakan parameter IsCustomerComboBasedOnCategory
    12/03/2021 [ICA/SIER] Menambah checkbox Use Remark for Notes based on parameter IsSalesInvoice3UseNotesBasedOnCategory
    19/03/2021 [BRI/SIER] Penyesuaian format Printout Sales Invoice based on DO to Customer
    01/04/2021 [BRI/SIER] Penyesuaian format Printout Sales Invoice based on DO to Customer
    08/04/2021 [BRI/SIER] Feedback printout
    14/04/2021 [BRI/SIER] Feedback nambah kolom remark based on detail DO to Customer
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    15/04/2021 [RDH/SIER] Feedback nambah kolom remark based on detail DO to Customer
	21/04/2021 [BRI/SIER] Feedback printout
    21/04/2021 [IBL/PHT] save Cost Center saat save journal IsSLI3JournalUseCCCode
    22/04/2021 [WED/SRN] perhitungan tax per detail nya berdasarkan parameter SalesInvoiceTaxCalculationFormula (default : 1)
    30/04/2021 [BRI/SIER] tambah dialog untuk customer dan customer category berdasarkan param IsShowCustomerCategory
    05/05/2021 [BRI/SIER] feedback dialog customer dan customer category
    10/05/2021 [BRI/SIER] merubah deskripsi tooltip
    20/05/2021 [TRI/SIER] menambah parameter IsSalesInvoice3NotAutoChooseSalesPerson
	24/05/2021 [BRI/SIER] menambahkan footer dan validasi pada header B & C pada printout
	02/06/2021 [VIN/ALL] penyesuaian sesuai printsalesinvoice3 karena rep nya sama 
    14/06/2021 [RDH/SIER] menambah filter yang warehouse berdasarkan group  login
    15/06/2021 [VIN/ALL] Bug Save Journal
    17/06/2021 [IQA/SIER] menambahkan parameter filter mIsItCtFilteredByGroup filter berdasarkan grup login
    18/06/2021 [VIN/SIER] Bug Printout
    21/06/2021 [WED/SIER] journal Piutang Uninvoiced belum dibulatkan ke bawah ketika parameter IsDOCtAmtRounded aktif
    07/07/2021 [ICA/PHT] journal Laba rugi selisih kurs, COA dikelompokan berdasarkan debit dan credit berdasarkan parameter IsForeignCurrencyExchangeUseExpense
    05/08/2021 [SET/KSM] Penambahan field "Departement" pada menu Sales Invoice Based on DO to Customer
    08/09/2021 [ICA/ALL] tambah validasi setting journal
    10/09/2021 [TKG/KIM] Menggunakan parameter IsSalesInvoice3JournalUseItCtSalesEnabled, untuk merubah proses journalnya.
    23/09/2021 [ICA/ALL] Bug saat validasi journalSS
    30/09/2021 [VIN/ALL] tambah Query hapus Journal selisih laba rugi =0
    14/10/2021 [ARI/AMKA] merubah rujukan akun COA credit di Sales Invoice dari COA's Account (Sales) ke COA's Account (AR Uninvoiced)
    14/10/2021 [ARI/AMKA] menggunakan parameter SLIJournalIncomingAcNoSource, sebagai indikator pemilihan COA
    18/10/2021 [RDH/AMKA] tambah fields type AcnotypeARDP dan parameter IsAPARUseType untuk mengambil coa dari system option 
    22/10/2021 [RDA/AMKA] menu sales invoice dapat menarik dua atau lebih dari item category berdasarkan param IsDOCtAllowMultipleItemCategory
    28/10/2021 [IBL/AMKA] BUG: Journal Cancelling tidak menyimpan CCCode 
    01/11/2021 [DITA/AMKA] BUG : parameter IsDOCtAllowMultipleItemCategory setting di getparameter
    12/11/2021 [ISD/AMKA] membuat field customer terfilter berdasarkan Group dengan parameter IsFilterByCtCt
    03/12/2021 [IBL/AMKA] Penyesuaian jurnal. COAAmt tidak terkalulasi dengan tax. Berdasarkan parameter IsSalesInvoice3COANonTaxable
    13/12/2021 [RDA/AMKA] Penyesuaian validasi dan jurnal untuk multi item category berdasarkan param IsDOCtAllowMultipleItemCategory
    23/12/2021 [RDA/AMKA] Feedback : Penyesuaian validasi dan jurnal untuk multi item category, tax terpecah ke semua item yang dipilih
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    


 */
#endregion

#region Update
/*
    03/01/2022 [TRI/AMKA] BUG ketika input tanpa tax ada warning string was not in a correct format
    04/01/2022 [ICA/AMKA] Bug perhitungan Additional Amt dan Downpayment pada journal 
    05/01/2022 [SET+ICA/AMKA] Penyesuaian jurnal SLI based on DO ketika menggunakan disc add cost dengan parameter SalesInvoice3JournalFormula
    25/01/2022 [MYA/PHT] Penyesuaian jurnal SLI based on DO NON IDR dan penyesuaian kolom detail DO  di SLI
    11/02/2022 [VIN/ALL] BUG SLI bisa cancel sebelum di IP kan
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses Insert Data
    23/02/2022 [ISD/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    01/03/2022 [TRI/AMKA] bug param IsAPARUseType case sensitif dan bug AmtForJournal can not be null
    04/03/2022 [VIN/SIER] SetLueCtCode -> processind ='F'
    08/03/2022 [VIN/ALL] BUG Department tertupuk dengan type
    16/03/2022 [VIN/SIER] Ubah source printout 
    18/03/2022 [VIN/SIER] DO yang sudah dipilih tidak muncul kembali tambah GetSelectedDO
    07/04/2022 [DITA/PHT] saat save journal cancel tambah validasi currendt based on param IsClosingJournalBasedOnMultiProfitCenter
    14/04/2022 [VIN/AMKA] bug saat save journal -> tidak ada alias damt dan camt
    09/05/2022 [BRI/PHT] saat save journal tidak menggunakan selisih kurs berdasarkan param IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled
    02/06/2022 [BRI/PHT] Feedback : belum semua param masuk validasi param IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled
    09/06/2022 [RDA/PHT] Rounding down tax berdasarkan param SITaxRoundingDown
    13/06/2022 [RDA/PHT] Feedback : Rounding down tax berdasarkan param SITaxRoundingDown
    18/07/2022 [DITA/KIM] Additional : penyesuaian printout dengan ketambahan informasi dari tab add cost
    19/07/2022 [DITA/PHT] ketika param IsItemCategoryUseCOAAPAR aktif maka dan param SLI3TaxCalculationFormat != 1 maka trigger nya coa AR di item category 
    19/07/2022 [DITA/PHT] ubah save journal ketika  IsItemCategoryUseCOAAPAR aktif maka dan param SLI3TaxCalculationFormat != 1 sumber amount diubah
    25/07/2022 [MAU/SIER] tambah parameter baru IsSLIBasedOnDoShowForeignName , penambahan kolom Item's Foreign Name di menu SLI based on DO to Customer default hide
    26/07/2022 [BRI/SIER] tambah advance charge pada tab disc,add,cost,etc berdasarkan param mIsSLI3UseAdvanceCharge
    27/07/2022 [VIN/PHT] BUG Printout 
    02/08/2022 [RDA+HPH/SIER] penyesuaian printout SLI SIER (source detail untuk poin A dan C pada printout, footer, adjustment size font angka, dll)
    04/08/2022 [BRI/SIER] penyesuaian advance charge
    10/08/2022 [TYO/SIER] menambah chkInsidentilInd berdasarkan parameter IsSLI3UseInsidentilInformation
    10/08/2022 [TYO/SIER] DteTaxInvoiceDt = DteDocDt berdasarkan parameter IsTaxInvoiceDateSameWithDocDate
    10/08/2022 [TYO/SIER] default taxcode1 - taxcode3 berdasarkan parameter SLIBasedOnDoDefaultTax
    12/08/2022 [BRI/PHT] department menggunakan param IsFilterByDept
    13/09/2022 [SET/PHT] Source Journaldtl berdasar param IsJournalSalesInvoiceUseItemsCategory
    15/09/2022 [RDA/SIER] perbaikan tampilan printout untuk border angka tax dan tampilan angka downpayment (ketambahan tanda kurung)
    20/09/2022 [IBL/PHT] source acno piutang uninvoice utk currency non IDR masih salah.
    17/10/2022 [SET/PHT] penyesuaian jurnal (QtyPackagingUnit * UPriceBeforeTax -> Qty * UPriceBeforeTax)
    26/10/2022 [SET/PHT] penyesuaian jurnal (Qty * UPriceBeforeTax -> round(Qty * UPriceBeforeTax, 4))
    28/10/2022 [MYA/AMKA] menambah field downpayment berdasarkan parameter IsSalesInvoiceUseTabToInputDownpayment
    28/10/2022 [MYA/AMKA] menambah validasi ARDP yg dapat ditarik berdasarkan tax SLI nya, mengurutkan Tax di tab downpayment sesuai dengan tax SLI
    25/11/2022 [TYO/PRODUCT] memindah posisi tab Tax and Journal ke urutan pertama
    19/12/2022 [MYA/MNET] muncul warning ketika save SLI based on DO to Customer
    28/12/2022 [ICA/MNET] penyesuaian printout, dibuat menjadi dua halaman
    10/01/2023 [DITA/PHT] Tampilan departemnt di designer terlalu condong kedepan
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    18/01/2023 [RDA/MNET] penambahan fitur tab upload file berdasarkan parameter IsSLIBasedOnDOToCustomerAllowtoUploadFile
    30/01/2023 [MAU/PHT] Hide kolom Tax Amount dan Unit Price (After Tax)
    08/02/2023 [WED/MNET] tambah Service Delivery
    20/02/2023 [WED/MNET] Service Delivery dirubah jadi 1 tab dengan DO
    02/03/2023 [MYA/MNET] BUG : Print Out
    07/03/2023 [RDA/MNET] penyesuaian field printout (field For dihapus dan diganti address) + tambah position dari ttd
    13/03/2023 [VIN/PHT] BUG save journal 
    14/03/2023 [VIN/SIER] BUG prntout 
    16/03/2023 [RDA/MNET] adjust printout ketika terdapat data service delivery
    25/03/2023 [RDA/MNET] adjust typo printout dan penyesuaian sumber grand total
    27/03/2023 [WED/PHT] validasi nilai minus di tab discount
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice3 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, //if this application is called from other application;
            mFormPrintOutInvoice3 = string.Empty,
            mIsFormPrintOutInvoice = string.Empty,
            mFormPrintOutInvoice3Receipt = string.Empty,
            mSalesBasedDOQtyIndex = string.Empty,
            mSLI3TaxCalculationFormat = string.Empty,
            mSalesInvoiceTaxCalculationFormula = string.Empty,
            mSLIJournalIncomingAcNoSource = string.Empty,
            mSLIBasedOnDoDefaultTax = string.Empty
            ;
        private string 
            mMainCurCode = string.Empty,
            mDueDtSIValue = string.Empty,
            mTaxCodeSIValue = string.Empty,
            mEmpCodeSI = string.Empty,
            mEmpCodeTaxCollector = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mTaxInvDocument = string.Empty,
            mDOTaxInfo = string.Empty,
            mOptCat = string.Empty,
            mSalesInvoice3JournalFormula = string.Empty,
            mSalesInvoice3NonIDRJournalQtySource = String.Empty,
            mJournalDocNoFormat = string.Empty
            ;
        internal bool 
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mSITaxRoundingDown = false,
            mIsCustomerItemNameMandatory = false, 
            mIsEntityMandatory = false,
            mIsDOCtAmtRounded = false,
            mIsSLI3UseAutoFacturNumber = false,
            mIsFilterByCtCt = false,
            mIsCustomerComboShowCategory = false,
            mIsBOMShowSpecifications = false,
            mIsCustomerComboBasedOnCategory = false,
            mIsShowCustomerCategory = false,
            mIsSalesInvoice3NotAutoChooseSalesPerson = false,
            mIsItCtFilteredByGroup = false,
            mIsWarehouseFilteredByGroup = false,
            mIsSalesInvoice3UseDepartment = false,
            mIsDOCtShowPackagingUnit = false,
            mIsSLIBasedOnDoShowForeignName = false,
            mIsSLI3UseAdvanceCharge = false,
            mIsSLI3UseInsidentilInformation =false,
            mIsTaxInvoiceDateSameWithDocDate = false,
            mIsFilterByDept = false,
            mIsGroupCOAActived = false,
            mIsJournalSalesInvoiceUseItemsCategory = false,
            mIsSalesInvoiceUseTabToInputDownpayment = false,
            mIsUseServiceDelivery = Sm.IsUseMenu("FrmServiceDelivery")
            ;

        private decimal mAdditionalCostDiscAmt = 0m;
        private bool mIsSalesInvoiceTaxInvDocEditable = false,
            mIsSalesInvoice3TaxEnabled = false,
            mIsSalesInvoice3COANonTaxable = false,
            mIsSalesInvoice3TaxInvDocumentAbleToEdit = false,
            mIsCheckCOAJournalNotExists = false,
            mIsItemCategoryUseCOAAPAR= false,
            IsInsert = false,
            mIsSalesInvoice3UseNotesBasedOnCategory = false,
            mIsSLI3JournalUseCCCode = false,
            mIsForeignCurrencyExchangeUseExpense = false,
            mIsSalesInvoice3JournalUseItCtSalesEnabled = false,
            mIsJournalValidationStockOpnameEnabled = false,
            mIsAPARUseType = false,
            mIsDOCtAllowMultipleItemCategory = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = false,
            mIsSLIBasedOnDOToCustomerAllowtoUploadFile = false,
            mIsSLIBasedOnDOToCustomerUploadFileMandatory = false
            ;

        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        private byte[] downloadedData;

        internal FrmSalesInvoice3Find FrmFind;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmSalesInvoice3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mOptCat = "AcNoTypeForARDP";
                if (this.Text.Length == 0) this.Text = "Sales Invoice";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                ExecQuery();
                GetParameter();
                if (mSalesInvoiceTaxCalculationFormula != "1") mDOTaxInfo = "(DO)";
                LblCtCtCode.Visible = LueCtCtCode.Visible = mIsCustomerComboBasedOnCategory;
                LblCtCtCode2.Visible = TxtCtCtCode.Visible = !mIsCustomerComboBasedOnCategory;
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueSPCode(ref LueSPCode);

                //service delivery
                TcSalesInvoice3.SelectedTabPage = Tp7;
                //if (!mIsUseServiceDelivery)
                //{
                    TcSalesInvoice3.TabPages.Remove(Tp7);
                //}
                //end service delivery

                TcSalesInvoice3.SelectedTabPage = Tp3;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;
                TcSalesInvoice3.SelectedTabPage = Tp4;
                if (!mIsSalesInvoiceUseTabToInputDownpayment)
                {
                    TcSalesInvoice3.TabPages.Remove(Tp5);
                    label6.Text = "Total Amount";
                    label13.Text = "Total Tax";
                    label6.Left += 25; label13.Left += 16;
                    //label6.Text = "Total Without Tax";
                    //label13.Text = "Tax Amount";
                    label23.Visible = TxtDownpaymentTaxAmt.Visible = label25.Visible = TxtInvoiceBeforeTaxAmt.Visible = label26.Visible = TxtTaxForInvoice.Visible = false;
                    label14.Top -= 63; TxtAmt.Top -= 63;
                    label7.Top -= 63; LueBankAcCode.Top -= 63;
                    LblRemark.Top -= 63; MeeRemark.Top -= 63;
                    label5.Top -= 63; TxtReceiptNo.Top -= 63;
                    //label11.Top -= 63; LueType.Top -= 63;
                    ChkRemarkNotesInd.Top -= 63; ChkInsidentilInd.Top -= 63;
                    LblDeptCode.Top -= 63; LueDeptCode.Top -= 63;
                    //panel2.Height -= 42;
                }
                //TcSalesInvoice3.SelectedTabPage = Tp5;

                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);

                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueOption(ref LueType, mOptCat);
                ChkInsidentilInd.Visible = mIsSLI3UseInsidentilInformation;
                if (mIsSLI3UseAdvanceCharge)
                {
                    SetLueAdvanceChargeCode(ref LueAdvanceChargeCode); SetLueAdvanceChargeCode(ref LueAdvanceChargeCode2); SetLueAdvanceChargeCode(ref LueAdvanceChargeCode3);
                }
                if (!mIsSLI3UseAdvanceCharge)
                {
                    label12.Visible = label18.Visible = label20.Visible = false;
                    LueAdvanceChargeCode.Visible = LueAdvanceChargeCode2.Visible = LueAdvanceChargeCode3.Visible = false;
                    panel4.Visible = false;
                    Grd3.Dock = DockStyle.Fill;
                }
                SetGrd();

                if (!mIsAPARUseType)
                {
                    label11.Visible = false;
                    LueType.Visible = false;
                    ChkRemarkNotesInd.Top -= 21; 
                    ChkInsidentilInd.Top -= 21;
                    LblDeptCode.Top -= 21; LueType.Top -= 21;
                }
                else
                {
                    if (!mIsSalesInvoiceUseTabToInputDownpayment)
                    {
                        label11.Top -= 63; LueType.Top -= 63;
                    }
                }


                SetFormControl(mState.View);
                if (mIsShowCustomerCategory)
                    LblCtCtCode.ForeColor = Color.Black;
                if (!mIsShowCustomerCategory)
                    BtnCtCt.Visible = false;
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (!mIsSalesInvoice3UseNotesBasedOnCategory)
                {
                    ChkRemarkNotesInd.Visible = false;
                    if(!mIsAPARUseType)
                        ChkInsidentilInd.Top -= 21;
                    else
                        ChkInsidentilInd.Top -= 21;
                }

                if (mIsSalesInvoice3UseDepartment && !mIsSalesInvoice3UseNotesBasedOnCategory)
                {
                    LueDeptCode.Location = new System.Drawing.Point(168, 193);
                    LblDeptCode.Location = new System.Drawing.Point(93, 193);
                }
                LueDeptCode.Visible = LblDeptCode.Visible = mIsSalesInvoice3UseDepartment;

                if (!mIsSLIBasedOnDOToCustomerAllowtoUploadFile)
                {
                    TcSalesInvoice3.TabPages.Remove(Tp6);
                }

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 52;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                       //0
                        "DNo",

                        //1-5
                        "",
                        "DO#",
                        "DO D#",
                        "",
                        "DO" + Environment.NewLine + "Date",

                        //6-10
                        "DO Request#" + Environment.NewLine + "/ PL#",
                        "DO Request D#" + Environment.NewLine + "/ PL D#",
                        "",
                        "SO#",
                        "SO D#",
                        
                        //11-15
                        "",
                        "Quotation#",
                        "Quotation D#",
                        "",
                        "Item's Code",
                        
                        //16-20
                        "",
                        "Item's Name",
                        "Packaging" + Environment.NewLine + "Quantity",
                        "Packaging" + Environment.NewLine + "UoM",
                        "Quantity",

                        //21-25
                        "Sales" + Environment.NewLine + "UoM",
                        "Currency",
                        "Unit Price" + Environment.NewLine + "(Before Tax)",
                        "Tax"+ Environment.NewLine + "Rate " + mDOTaxInfo,
                        "Tax"+ Environment.NewLine + "Amount " + mDOTaxInfo,
                        
                        //26-30
                        "Unit Price" + Environment.NewLine + "(After Tax)",
                        "Amount",
                        "TOP",
                        "Local" + Environment.NewLine + "Document",
                        "Type",

                        //31-35
                        "Customer"+Environment.NewLine+"Item's Name",
                        "Entity Code",
                        "Entity",
                        "DO's Local#",
                        "Item's"+Environment.NewLine+"Local Code",

                        //36-40
                        "Specification",
                        "Remark",
                        "Tax 1",
                        "Tax 2",
                        "Tax 3",

                        //41-45
                        "Tax Amount"+Environment.NewLine+"(Sales)",
                        "Amount"+Environment.NewLine+"(Before Tax)",
                        "TaxAmt1",
                        "TaxAmt2",
                        "TaxAmt3",

                        //46-50
                        "Additional Amount",
                        "Amount Journal",
                        "Tax (Per Item)",
                        "Amount"+Environment.NewLine+"(After Tax)",
                        "Item's Foreign Name",

                        //51
                        "ServiceDeliveryDocNo"
                    },
                     new int[]
                    {
                        //0
                        0, 

                        //1-5
                        20, 150, 0, 20, 100,
 
                        //6-10
                        130, 0, 20, 130, 0, 
                        
                        //11-15
                        20, 130, 0, 20, 80, 
                        
                        //16-20
                        20, 200, 100, 80, 100, 

                        //21-25
                        80, 80, 130, 100, 100, 

                        //26-30
                        130, 130, 150, 150, 20,

                        //31-35
                        250, 0, 150, 150, 100,

                        //36 - 40
                        200, 200, 60, 60, 60, 

                        //41-45
                        150, 150, 0, 0, 0,

                        //46-50
                        150, 150, 150, 150, 200,

                        //51
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 38, 39, 40 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 8, 11, 14, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });

            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 24, 25, 26, 28, 30, 31, 32, 34, 46, 47, 48, 49, 50, 51 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 31 });

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 9, 10, 12, 13, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 });

            Grd1.Cols[31].Move(18);
            if (mIsEntityMandatory)
                Grd1.Cols[33].Move(5);
            else
                Grd1.Cols[33].Visible = false;
            Grd1.Cols[34].Move(6);


            Sm.GrdColInvisible(Grd1, new int[] { 50 }, false);
            // item's foreign name
            if (mIsSLIBasedOnDoShowForeignName)
            {
                Grd1.Cols[50].Move(19);
            }
            else
            {
                Grd1.Cols[50].Visible = false;
            }




            if (!mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 35, 36 }, false);
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 35, 36 }, true);
                Grd1.Cols[35].Move(17);
                Grd1.Cols[36].Move(21);
            }

            if (!mIsDOCtShowPackagingUnit)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, false);
            }

            if (mSalesInvoiceTaxCalculationFormula == "1") Sm.GrdColInvisible(Grd1, new int[] { 38, 39, 40, 41, 42 });


            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 13;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        "",
                        //11-12
                        "Local Name",
                        "LueName"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20,
                        //11-12
                        250, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10, 12 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10, 12 }, false);
            if (!mIsSLI3UseAdvanceCharge) Sm.GrdColInvisible(Grd3, new int[] { 11 }, false);
            Grd3.Cols[11].Move(3);

            #endregion

            #region Grd 4

            Grd4.Header.Rows.Count = 2;
            Grd4.Cols.Count = 26;
            Grd4.FrozenArea.ColCount = 2;

            Grd4.Cols[0].Width = 20;
            Grd4.Header.Cells[0, 0].Value = "";
            Grd4.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd4.Header.Cells[0, 0].SpanRows = 2;

            Grd4.Cols[1].Width = 250;
            Grd4.Header.Cells[0, 1].Value = "Document#";
            Grd4.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd4.Header.Cells[0, 1].SpanRows = 2;

            Grd4.Header.Cells[1, 2].Value = "AR Downpayment";
            Grd4.Header.Cells[1, 2].TextAlign = iGContentAlignment.TopCenter;
            Grd4.Header.Cells[1, 2].SpanCols = 14;
            Grd4.Header.Cells[0, 2].Value = "Downpayment Before Tax";
            Grd4.Header.Cells[0, 3].Value = "Tax Code1";
            Grd4.Header.Cells[0, 4].Value = "Tax 1";
            Grd4.Header.Cells[0, 5].Value = "Tax Rate";
            Grd4.Header.Cells[0, 6].Value = "Tax 1 Amount";
            Grd4.Header.Cells[0, 7].Value = "Tax Code2";
            Grd4.Header.Cells[0, 8].Value = "Tax 2";
            Grd4.Header.Cells[0, 9].Value = "Tax Rate 3";
            Grd4.Header.Cells[0, 10].Value = "Tax 2 Amount";
            Grd4.Header.Cells[0, 11].Value = "Tax Code3";
            Grd4.Header.Cells[0, 12].Value = "Tax 3";
            Grd4.Header.Cells[0, 13].Value = "Tax Rate 3";
            Grd4.Header.Cells[0, 14].Value = "Tax 3 Amount";
            Grd4.Header.Cells[0, 15].Value = "Downpayment After Tax";
            Grd4.Cols[2].Width = Grd4.Cols[15].Width = 155;
            Grd4.Cols[5].Width = Grd4.Cols[6].Width = Grd4.Cols[9].Width = Grd4.Cols[10].Width = Grd4.Cols[13].Width = Grd4.Cols[14].Width = 100;
            Grd4.Cols[3].Width = Grd4.Cols[4].Width = Grd4.Cols[7].Width = Grd4.Cols[8].Width = Grd4.Cols[11].Width = Grd4.Cols[12].Width = 180;

            Grd4.Header.Cells[1, 16].Value = "Processing Sales Invoice";
            Grd4.Header.Cells[1, 16].TextAlign = iGContentAlignment.TopCenter;
            Grd4.Header.Cells[1, 16].SpanCols = 5;
            Grd4.Header.Cells[0, 16].Value = "Downpayment Before Tax";
            Grd4.Header.Cells[0, 17].Value = "Tax 1 Amount";
            Grd4.Header.Cells[0, 18].Value = "Tax 2 Amount";
            Grd4.Header.Cells[0, 19].Value = "Tax 3 Amount";
            Grd4.Header.Cells[0, 20].Value = "Downpayment After Tax";
            Grd4.Cols[16].Width = Grd4.Cols[20].Width = 155;
            Grd4.Cols[17].Width = Grd4.Cols[18].Width = Grd4.Cols[19].Width = 100;

            Grd4.Header.Cells[1, 21].Value = "Remaining Sales Invoice";
            Grd4.Header.Cells[1, 21].TextAlign = iGContentAlignment.TopCenter;
            Grd4.Header.Cells[1, 21].SpanCols = 5;
            Grd4.Header.Cells[0, 21].Value = "Downpayment Before Tax";
            Grd4.Header.Cells[0, 22].Value = "Tax 1 Amount";
            Grd4.Header.Cells[0, 23].Value = "Tax 2 Amount";
            Grd4.Header.Cells[0, 24].Value = "Tax 3 Amount";
            Grd4.Header.Cells[0, 25].Value = "Downpayment After Tax";
            Grd4.Cols[21].Width = Grd4.Cols[25].Width = 150;
            Grd4.Cols[22].Width = Grd4.Cols[23].Width = Grd4.Cols[24].Width = 100;

            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdFormatDec(Grd4, new int[] { 2, 5, 6, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.GrdColInvisible(Grd4, new int[] { 3, 5, 7, 9, 11, 13 });
            for (int Index = 0; Index < Grd4.Cols.Count; Index++)
                Grd4.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            Sm.SetGrdProperty(Grd4, false);

            #endregion

            #region Grd 5 (Upload File)

            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile)
            {
                Grd5.Cols.Count = 7;
                Grd5.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd5,
                        new string[]
                        {
				            //0
				            "D No",
				            //1-5
				            "File Name",
                            "",
                            "D",
                            "Upload By",
                            "Date",

				            //6
				            "Time"
                        },
                         new int[]
                        {
                            0,
                            250, 20, 20, 100, 100,
                            100
                        }
                    );

                Sm.GrdColInvisible(Grd5, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd5, new int[] { 5 });
                Sm.GrdFormatTime(Grd5, new int[] { 6 });
                Sm.GrdColButton(Grd5, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 4, 5, 6 });
                Grd5.Cols[2].Move(1);
            }

            #endregion

            #region Grd 6 - Service Delivery

            if (mIsUseServiceDelivery)
            {
                Grd6.Cols.Count = 20;
                Grd6.FrozenArea.ColCount = 2;

                Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[]
                    {
                        //0
                        "",
                        //1-5
                        "SD#",
                        "SD Date",
                        "",
                        "Item Name",
                        "Quantity",
                        //6-10
                        "Sales"+Environment.NewLine+"Uom",
                        "Currency",
                        "Unit Price"+Environment.NewLine+"(Before Tax)",
                        "Local Document#",
                        "Remark",
                        //11-15
                        "Tax 1",
                        "Tax 2",
                        "Tax 3",
                        "Tax Amount"+Environment.NewLine+"(Sales)",
                        "Amount"+Environment.NewLine+"(Before Tax)",
                        //16-19
                        "TaxAmt1",
                        "TaxAmt2",
                        "TaxAmt3",
                        "SDDNo"
                    }, new int[]
                    {
                        //0
                        20,
                        //1-5
                        150, 100, 20, 180, 80, 
                        //6-10
                        120, 100, 150, 180, 200, 
                        //11-12
                        60, 60, 60, 150, 150, 
                        //16-19
                        0, 0, 0, 0
                    }
                );
                Sm.GrdColButton(Grd6, new int[] { 0, 3 });
                Sm.GrdFormatDate(Grd6, new int[] { 2 });
                Sm.GrdColCheck(Grd6, new int[] { 11, 12, 13 });
                Sm.GrdColReadOnly(true, true, Grd6, new int[] { 1, 2, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18, 19 });
                Sm.GrdFormatDec(Grd6, new int[] { 5, 8, 14, 15, 16, 17, 18 }, 0);
                Sm.GrdColInvisible(Grd6, new int[] { 3, 16, 17, 18, 19 }, false);
            }

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 15, 16, 24, 25, 26, 34 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            if (mIsUseServiceDelivery) Sm.GrdColInvisible(Grd6, new int[] { 3 }, !ChkHideInfoInGrd.Checked);

            if (mIsSLIBasedOnDoShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 50 }, !ChkHideInfoInGrd.Checked);
            }
        
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd, DteDocDt, LueCtCode, DteDueDt, 
                        TxtLocalDocNo, LueSPCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                        DteTaxInvoiceDt, TxtTaxInvDocument, LueCurCode, TxtDownpayment, LueBankAcCode, 
                        TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, MeeRemark , LueCtCtCode, ChkRemarkNotesInd,
                        LueDeptCode, LueType, LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3, ChkInsidentilInd,
                        TxtDownpaymentTaxAmt, TxtInvoiceBeforeTaxAmt, TxtTaxForInvoice
                    }, true);
                    BtnCtCt.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 38, 39, 40 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 16 });
                    //if (mIsUseServiceDelivery) Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 3, 11, 12, 13 });
                    if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                        {
                            //if (Sm.GetGrdStr(Grd5, Row, 4).Length == 0)
                            //{
                            //    Grd5.Cells[Row, 2].ReadOnly = iGBool.False;
                            //    Grd5.Cells[Row, 3].ReadOnly = iGBool.True;
                            //}
                            //else
                            //{
                                Grd5.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd5.Cells[Row, 3].ReadOnly = iGBool.False;
                            //}
                        }
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, LueTaxCode1, LueTaxCode2, LueTaxCode3,
                        LueCurCode, MeeRemark,  LueBankAcCode, LueSPCode,DteTaxInvoiceDt, ChkRemarkNotesInd,
                        LueDeptCode, LueType, LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3, ChkInsidentilInd
                    }, false);
                    BtnCtCt.Enabled = true;
                    if (mIsCustomerComboBasedOnCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, false);
                    }
                    if (mIsSLI3UseAutoFacturNumber) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, true);
                    else Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, false);
                    if (mIsShowCustomerCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, true);
                    }
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDownpayment }, mIsSalesInvoiceUseTabToInputDownpayment);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 38, 39, 40 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9, 11 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 16 });
                    //if (mIsUseServiceDelivery) Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 3, 11, 12, 13 });
                    DteDocDt.Focus();

                    if (mSLIBasedOnDoDefaultTax != string.Empty)
                    {
                        string[] mTaxCodeDefault = { };
                        LookUpEdit[] LueTaxCode = new LookUpEdit[3]{ LueTaxCode1, LueTaxCode2, LueTaxCode3 };

                        if (mSLIBasedOnDoDefaultTax.Length > 0)
                            mTaxCodeDefault = mSLIBasedOnDoDefaultTax.Split(',');

                        for(int i = 0; i < mTaxCodeDefault.Length; i++)
                        {
                            Sm.SetLue(LueTaxCode[i], mTaxCodeDefault[i]);
                        }
                    }
                    if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile) Sm.GrdColReadOnly(false, true, Grd5, new int[] { 2, 3 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd, ChkRemarkNotesInd }, false);
                    if (mIsSalesInvoice3TaxInvDocumentAbleToEdit)
                    {
                        if (!IsSalesInvoice3TaxInvDocumentNotAbleToEdit(false)) 
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, false);
                    }
                    else
                    {
                        if (mIsSalesInvoiceTaxInvDocEditable && !mIsSLI3UseAutoFacturNumber) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, false);
                    }
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAdditionalCostDiscAmt = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, MeeCancelReason, DteDocDt, LueCtCode, TxtCtCtCode, 
                 DteDueDt, TxtLocalDocNo, TxtTaxInvDocument, LueCurCode, MeeRemark, 
                 LueSPCode, LueBankAcCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                 DteTaxInvoiceDt, TxtJournalDocNo, TxtJournalDocNo2, TxtReceiptNo, LueCtCtCode,
                 LueDeptCode, LueType, LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3,
                 TxtDownpaymentTaxAmt, TxtInvoiceBeforeTaxAmt, TxtTaxForInvoice
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt,
                TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3,
                TxtDownpaymentTaxAmt, TxtInvoiceBeforeTaxAmt, TxtTaxForInvoice
            }, 0);
            ChkCancelInd.Checked = false;
            ChkRemarkNotesInd.Checked = false; 
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile) Sm.ClearGrd(Grd5, true);
            //if (mIsUseServiceDelivery)
            //{
            //    Sm.ClearGrd(Grd6, true);
            //    Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 5, 8, 14, 15, 16, 17, 18 });
            //}
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45, 46, 47 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2, 4, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoice3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData(); 
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                IsInsert = true;               
                if (mIsCustomerComboBasedOnCategory)
                {
                    SetLueCtCtCode(ref LueCtCtCode);
                    SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", string.Empty);
                }
                else
                {
                    Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                    Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                }

                if (!mIsSalesInvoice3NotAutoChooseSalesPerson)
                {
                    Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1"));
                }
                
                if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length>0)
                    DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));

                if (mTaxCodeSIValue.Length > 0)
                {
                    Sm.SetLue(LueTaxCode1, mTaxCodeSIValue);
                    ComputeAmtFromTax();
                }
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (IsDataCancelledAlready()) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var x = new StringBuilder();

            string mDocNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }

            x.AppendLine("Select T.EntCode From ( ");
            x.AppendLine("    Select Distinct D.EntCode ");
            x.AppendLine("    From TblDOCtHdr A ");
            x.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            x.AppendLine("        And Find_In_Set(A.DocNo, @Param) ");
            x.AppendLine("    Inner Join TblCostCenter C On B.CCCode = C.CCCode ");
            x.AppendLine("    Inner Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            if (mIsUseServiceDelivery)
            {
                x.AppendLine("    Union All ");
                x.AppendLine("    Select Distinct C.EntCode ");
                x.AppendLine("    From TblServiceDeliveryHdr A ");
                x.AppendLine("    Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
                x.AppendLine("        And Find_In_Set(A.DocNo, @Param) ");
                x.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode ");
            }
            x.AppendLine(") T ");
            x.AppendLine("Limit 1; ");

            mEntCode = Sm.GetValue(x.ToString(), mDocNo);

            try
            {
                if (mIsDOCtAllowMultipleItemCategory)
                {
                    ComputeAmtJournal();
                }
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            mTaxInvDocument = Sm.GetValue("Select FacturNo From TblFacturNumberDtl Where UsedInd='N' And SLIDocNo is null Limit 1; ");
            
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid() 
                ) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;

            if(mDocNoFormat == "1")
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr");
            else
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr", mEntCode, "1");

            string ReceiptNo = GenerateReceipt(Sm.GetDte(DteDocDt));
            var code1 = Sm.GetCode1ForJournalDocNo("FrmSalesInvoice3", string.Empty, string.Empty, mJournalDocNoFormat);
            var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode In (Select B.CCCode From TblDOCtHdr A Inner Join TblWarehouse B On A.WhsCode=B.WhsCode Where A.DocNo=@Param) ", Sm.GetGrdStr(Grd1, 0, 2));
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoiceHdr(DocNo, ReceiptNo));
            cml.Add(SaveSalesInvoiceDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveSalesInvoiceDtl(DocNo, Row));

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveSalesInvoiceDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSalesInvoiceDtl2(DocNo, Row));
            }

            if (Grd4.Rows.Count > 1 && mIsSalesInvoiceUseTabToInputDownpayment)
            {
                cml.Add(SaveSalesInvoiceDtl3(DocNo));
            }

            //if (mIsUseServiceDelivery)
            //{
            //    if (Grd6.Rows.Count > 1)
            //    {
            //        cml.Add(SaveSalesInvoiceDtl4(DocNo));
            //    }
            //}

            if (decimal.Parse(TxtDownpayment.Text) != 0)
            {
                var lR = new List<Rate>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                decimal DP = 0m;

                SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
                SQL.AppendLine("From TblCustomerDepositSummary2 ");
                SQL.AppendLine("Where CtCode = @CtCode ");
                SQL.AppendLine("And CurCode = @CurCode ");
                SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lR.Add(new Rate()
                            {
                                CtCode = Sm.DrStr(dr, c[0]),
                                CurCode = Sm.DrStr(dr, c[1]),
                                ExcRate = Sm.DrDec(dr, c[2]),
                                Amt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lR.Count > 0)
                {
                    DP = decimal.Parse(TxtDownpayment.Text);
                    for (int i = 0; i < lR.Count; i++)
                    {   
                        if (DP > 0)
                        {
                            cml.Add(SaveExcRate(ref lR, i, DocNo, DP));
                            DP = DP - lR[i].Amt;
                        }
                    }
                }

                cml.Add(SaveCustomerDeposit(DocNo));
            }

            cml.Add(UpdateDOCtProcessInd(DocNo, "N"));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo, code1, profitCenterCode));
            if (mIsSLI3UseAutoFacturNumber)
                cml.Add(UpdateFacturNumber(mTaxInvDocument, DocNo));

            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                {
                    if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd5, Row, 1))) return;
                    }
                }
            }
            cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                {
                    if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1")
                    {
                        //if (Sm.GetGrdStr(Grd5, Row, 1) != Sm.GetGrdStr(Grd5, Row, 4))
                        //{
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd5, Row, 1));
                        //}
                    }
                }
            }

            IsInsert = false;
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                //Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsAPARUseType && Sm.IsLueEmpty(LueType, "Type")) ||
                (mIsSalesInvoice3UseDepartment && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsCustomerComboBasedOnCategory && !mIsShowCustomerCategory && Sm.IsLueEmpty(LueCtCtCode, "Customer Category")) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsDOCtAlreadyCancelled() ||
                IsDOCtAlreadyFulfilled() ||
                //IsServiceDeliveryNotExisted() ||
                IsCurrencyNotValid() ||
                IsDownpaymentNotValid() ||
                IsFacturNumberNotValid() ||
                IsJournalAmtNotBalanced() ||
                IsJournalSettingInvalid() ||
                IsTickedAddCostDiscInvalid() ||
                (mIsEntityMandatory && IsEntityNotValid()) ||
                //(mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalNotValid()) ||
                IsItemCategoryInvalid() ||
                (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && mIsSLIBasedOnDOToCustomerUploadFileMandatory && IsUploadFileEmpty()) ||
                (mIsSalesInvoiceUseTabToInputDownpayment && IsTaxCodeNotValid());
        }

        private bool IsServiceDeliveryNotExisted()
        {
            if (!mIsUseServiceDelivery) return false;

            string DOCtDocNo = string.Empty;
            for (int i  = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (DOCtDocNo.Length > 0) DOCtDocNo += ",";
                    DOCtDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            bool IsServiceDeliveryShouldExists = UpdateServiceDeliveryData(DOCtDocNo);

            if (!IsServiceDeliveryShouldExists) return false;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 51).Length > 0)
                {
                    bool IsExists = false;
                    for (int j = 0; j < Grd6.Rows.Count; ++j)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 51) == Sm.GetGrdStr(Grd6, j, 1))
                        {
                            IsExists = true;
                            break;
                        }
                    }
                    if (!IsExists)
                    {
                        Sm.StdMsg(mMsgType.Warning, "This SD# should exists : " + Sm.GetGrdStr(Grd1, i, 51));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsUploadFileEmpty()
        {
            if (Grd5.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                return true;
            }
            return false;
        }

        private bool IsTaxCodeNotValid()
        {
            string TaxCode = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);
            bool TaxValid1 = true,
                TaxValid2 = true,
                TaxValid3 = true;

            for (int row = 0; row < Grd4.Rows.Count - 1; row++)
            {
                //TaxCode1 
                if (Sm.GetGrdStr(Grd4, row, 3).Length > 0)
                    if (!(Sm.GetGrdStr(Grd4, row, 3) == TaxCode || Sm.GetGrdStr(Grd4, row, 3) == TaxCode2 || Sm.GetGrdStr(Grd4, row, 3) == TaxCode3))
                        TaxValid1 = false;

                //TaxCode2 
                if (Sm.GetGrdStr(Grd4, row, 7).Length > 0)
                    if (!(Sm.GetGrdStr(Grd4, row, 7) == TaxCode || Sm.GetGrdStr(Grd4, row, 7) == TaxCode2 || Sm.GetGrdStr(Grd4, row, 7) == TaxCode3))
                        TaxValid2 = false;

                //TaxCode3
                if (Sm.GetGrdStr(Grd4, row, 11).Length > 0)
                    if (!(Sm.GetGrdStr(Grd4, row, 11) == TaxCode || Sm.GetGrdStr(Grd4, row, 11) == TaxCode2 || Sm.GetGrdStr(Grd4, row, 11) == TaxCode3))
                        TaxValid3 = false;


                if (!(TaxValid1 && TaxValid2 && TaxValid3))
                {
                    Sm.StdMsg(mMsgType.Warning, "Tax (Purchase Invoice) different then Tax (AP Downpayment)");
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCategoryInvalid()
        {
            if (mIsAutoJournalActived && mIsItemCategoryUseCOAAPAR && Grd1.Rows.Count > 2)
            {
                if (mIsDOCtAllowMultipleItemCategory) return false;

                string ItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, 0, 15));

                for (int i = 1; i < Grd1.Rows.Count - 1; ++i)
                {
                    string tempItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, i, 15));

                    if (tempItCtCode != ItCtCode)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Different item category detected. This will affect the journal process.");
                        Sm.FocusGrd(Grd1, i, 17);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsTickedAddCostDiscInvalid()
        {
            if (mSLI3TaxCalculationFormat == "1") return false;

            decimal mDAmt = 0m, mCAmt = 0m;

            if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd3, i, 1).Length > 0)
                    {
                        if (Sm.GetGrdBool(Grd3, i, 3)) // Debit
                            mDAmt += Sm.GetGrdDec(Grd3, i, 4);

                        if (Sm.GetGrdBool(Grd3, i, 5)) // Credit
                            mCAmt += Sm.GetGrdDec(Grd3, i, 6);
                    }
                }

                if (mDAmt != mCAmt && mDAmt != 0m && mCAmt != 0m)
                {
                    TcSalesInvoice3.SelectedTabPage = Tp3;
                    Sm.StdMsg(mMsgType.Warning, "Your chosen Debit and Credit amount should be balanced.");
                    return true;
                }
            }

            return false;
        }

        private bool IsEntityNotValid()
        {
            string EntCode = Sm.GetGrdStr(Grd1, 0, 32);
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, r, 32).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine + Environment.NewLine +
                            "Entity should not be empty."
                            );
                        return true;
                    }
                    else
                    {
                        if (!Sm.CompareStr(EntCode, Sm.GetGrdStr(Grd1, r, 32)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "DO# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                                "Item Code : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine + Environment.NewLine +
                                "One invoice document should not have more than 1 invoice."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsFacturNumberNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 25) > 0)
                    if (!mIsSLI3UseAutoFacturNumber && Sm.IsTxtEmpty(TxtTaxInvDocument, "Factur#", false)) return true;       
            }
            return false;
        }

        private bool IsLocalDocNoNotValid()
        {
            var LocalDocNo = TxtLocalDocNo.Text;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (!Sm.CompareStr(LocalDocNo, Sm.GetGrdStr(Grd1, Row, 29)))
                {
                    return (Sm.StdMsgYN("Question", 
                        "Sales invoice's local document should be the same as all existing PL/DR local document." + 
                        Environment.NewLine +
                        "Do you want to continue ?"
                        )== DialogResult.No);
                }
            }
            return false;
        }

      /*  private bool IsTaxInvDocumentNotValid()
        {
            bool IsTaxable = TxtTaxInvDocument.Text.Length > 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (IsTaxable  && Sm.GetGrdDec(Grd1, Row, 24) <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine + 
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + 
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                            "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                            "You can't choose nontaxable DO#."
                            );
                        return true;
                    }

                    if (!IsTaxable && Sm.GetGrdDec(Grd1, Row, 24) > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                            "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                            "You can't choose taxable DO#."
                            );
                        return true;
                    }
                }
            }
            return false;
        }*/

        private bool IsDocumentTypeNotValid()
        {
            string DocType = Sm.GetGrdStr(Grd1, 0, 29);
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    !Sm.CompareStr(DocType, Sm.GetGrdStr(Grd1, Row, 29))
                    )
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                            "Type : " + DocType=="1"?"Oversea DO":"Local DO" + Environment.NewLine + Environment.NewLine +
                            "You can't choose nontaxable DO#."
                            );

                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                string DOTxt = "DO";
                if (mIsUseServiceDelivery) DOTxt += "/SD";
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 " + DOTxt + ".");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Item list entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) { TcSalesInvoice3.SelectedTabPage = Tp3; return true; }
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        TcSalesInvoice3.SelectedTabPage = Tp3;
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount# : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        TcSalesInvoice3.SelectedTabPage = Tp3;
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd3, Row, 4) < 0m || Sm.GetGrdDec(Grd3, Row, 6) < 0m)
                    {
                        TcSalesInvoice3.SelectedTabPage = Tp3;
                        Sm.StdMsg(mMsgType.Warning, "Amount should not be less than 0.00 on discount, additional cost, etc.");
                        return true;
                    }

                }
            }

            return false;
        }

        private bool IsDOCtAlreadyCancelled()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        string DocNoTxt = "DO#";
                        if (mIsUseServiceDelivery) DocNoTxt += "/SD#";
                        string Msg =
                            DocNoTxt + " : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + 
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyCancelled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Some items already cancelled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyCancelled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From ( ");
            SQL.AppendLine("    Select A.DocNo, B.Qty ");
            SQL.AppendLine("    From TblDOCtHdr A ");
            SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo = B.DocNo And B.DNo = @DNo ");
            SQL.AppendLine("    Where A.DocNo = @DocNo ");
            SQL.AppendLine(") T Where Qty < Cast(@Qty As Decimal(12, 4)) ");

            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select DocNo From ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.Qty ");
                SQL.AppendLine("    From TblServiceDeliveryHdr T1 ");
                SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DocNo = T2.DocNo And T2.DNo = @DNo ");
                SQL.AppendLine("        And T1.DocNo = @DocNo ");
                SQL.AppendLine(") T Where Qty < Cast(@Qty As Decimal(12, 4)) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };            

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 20));
            return Sm.IsDataExist(cm);
        }

        private bool IsDOCtAlreadyFulfilled()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        string DocNoTxt = "DO#";
                        if (mIsUseServiceDelivery) DocNoTxt += "/SD#";
                        string Msg =
                            DocNoTxt + " : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyFulfilled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already fulfilled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyFulfilled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDOCtDtl ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo And DNo=@DNo ");
            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo From TblServiceDeliveryDtl ");
                SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo And DNo=@DNo ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            return Sm.IsDataExist(cm);
        }

        private bool IsCurrencyNotValid()
        {
            string CurCode = Sm.GetLue(LueCurCode);
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 22)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One sales invoice# only allowed 1 currency type.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            string
                ItCtName = Sm.GetValue("Select ItCtName From TblItemCategory Where ItCtCode In (Select ItCtCode From TblItem Where ItCode = @Param);", Sm.GetGrdStr(Grd1, 0, 15)),
                mCustomerAcNoAR = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoAR'"),
                mCustomerAcNoNonInvoice = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoNonInvoice'"),
                mCustomerAcNoDownPayment = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoDownPayment'"),
                mAcNoTypeForARDP = Sm.GetValue("Select Property1 from tblOption where OptCat = 'AcNoTypeForARDP' AND OptCode = @Param ",Sm.GetLue(LueType)),
                mAcNoForForeignCurrencyExchangeGains = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForForeignCurrencyExchangeGains'");

            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            if(mIsItemCategoryUseCOAAPAR)
            {
                //piutang usaha
                if(GetCOAAR().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account(AR)# (" + ItCtName + ") is empty.");
                    return true;
                }

                //pendapatan
                if(GetCOASales().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account(Sales)# (" + ItCtName + ") is empty.");
                    return true;
                }
            }
            else
            {
                //piutangusaha
                if(mCustomerAcNoAR.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
                }

                if (mIsSalesInvoice3JournalUseItCtSalesEnabled)
                {
                    //pendapatan
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "4", "Sales")) return true;
                }
                else
                {
                    //PiutangUnInvoice
                    if (mCustomerAcNoNonInvoice.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                        return true;
                    }
                }
            }

            //PPNKeluaran 
            var cek = Sm.GetLue(LueTaxCode1).Length;
            if (Sm.GetLue(LueTaxCode1).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo1 is Not Null And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode1)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Tax1's COA Account# is empty.");
                    return true;
                }
            }

            if (Sm.GetLue(LueTaxCode2).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo1 is Not Null And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode2)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Tax2's COA Account# is empty.");
                    return true;
                }
            }
            if (Sm.GetLue(LueTaxCode3).Length > 0)
            {
                if (!Sm.IsDataExist("Select 1 From TblTax Where TaxCode = @Param And AcNo1 is Not Null And AcNo2 is Not Null; ", Sm.GetLue(LueTaxCode3)))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Tax3's COA Account# is empty.");
                    return true;
                }
            }

            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {

                if (mIsAPARUseType)
                {
                     if (mAcNoTypeForARDP.Length == 0)
                     {
                         Sm.StdMsg(mMsgType.Warning, Msg + "Property1 System Option is Empty");
                        return true;
                     }
                }
                else
                {
                    if (mCustomerAcNoDownPayment.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoDownPayment is empty.");
                        return true;
                    }
                }
            }
            
            //ListOfCOA
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdStr(Grd3, row, 1).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Discount, Additional Cost, Etc's COA Account# is empty at row" + (row + 1) + ".");
                    return true;
                }
            }

            //LabaRugiSelisihKurs
            if (mAcNoForForeignCurrencyExchangeGains.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForForeignCurrencyExchangeGains is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string Index, string COAType)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo" + Index + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 15);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's " + COAType + "COA account# ( " + ItCtName + " ) is empty.");
                return true;
            }
            return false;
        }


        //private bool IsCOAJournalNotValid()
        //{

        //    var cm = new MySqlCommand();
        //    Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
        //    Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
        //    Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));

        //    if (Grd3.Rows.Count > 1)
        //    {
        //        for (int row = 0; row < Grd3.Rows.Count - 1; row++)
        //        {
        //            Sm.CmParam<String>(ref cm, "@AcNo_"+row, Sm.GetGrdStr(Grd3,row, 1));
        //        }
        //        if (mSLI3TaxCalculationFormat != "1")
        //            Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.",Sm.GetLue(LueCtCode)));
        //    }



        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select AcNo From ");
        //    SQL.AppendLine("( ");

        //    if (mIsItemCategoryUseCOAAPAR)
        //    {
        //        SQL.AppendLine("Select '"+GetCOAAR()+"' As AcNo ");
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select '"+GetCOASales()+"' As AcNo ");                
        //    }
        //    else
        //    {
        //        //Piutang Usaha
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoAR' ");

        //        //Piutang Uninvoice
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoNonInvoice' ");
        //    }

        //    //PPN Keluaran 1
        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode1 ");

        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode2 ");

        //    SQL.AppendLine("Union All ");
        //    SQL.AppendLine("Select Case When TaxRate>=0.00 Then AcNo2 Else AcNo1 End As AcNo ");
        //    SQL.AppendLine("From TblTax Where TaxCode = @TaxCode3 ");

        //    //Downpayment
        //    if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoDownPayment' ");
        //    }

        //    //List Of COA
        //    if(Grd3.Rows.Count > 1)
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select AcNo From ( ");
        //        for(int row = 0; row < Grd3.Rows.Count - 1; row++)
        //        {
        //            SQL.AppendLine("Select @AcNo_" + row + " As AcNo ");
        //            if(row < Grd3.Rows.Count - 2)
        //                SQL.AppendLine("Union All ");
        //        }
        //        SQL.AppendLine(") T1 ");
        //        if (mSLI3TaxCalculationFormat != "1")
        //            SQL.AppendLine("Where T1.AcNo Not Like @CtCode ");
        //    }

        //    //Laba Rugi Selisih Kurs
        //    if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode = 'AcNoForForeignCurrencyExchangeGains' ");
        //    }
        //    SQL.AppendLine(") Tbl ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();

        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        {
        //            "AcNo",
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                if (Sm.DrStr(dr, c[0]).Length == 0)
        //                {
        //                    Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //                    return true;
        //                }
        //            }
        //        }
        //        dr.Close();
        //    }

        //    return false;
        //}


        private MySqlCommand SaveCustomerDeposit(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Amt=Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceHdr(string DocNo, string ReceiptNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoiceHdr ");
            SQL.AppendLine("(DocNo, ReceiptNo, DocDt, CancelInd, ProcessInd, CtCode, DueDt, LocalDocNo, TaxInvDocument, TaxInvDt, CurCode, TotalAmt, TotalTax, Downpayment, Amt, AdditionalCostDiscAmt, MInd, BankAcCode, SalesName, JournalDocNo, JournalDocNo2, ");
            SQL.AppendLine("DeptCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt1, TaxAmt2, TaxAmt3, Remark, InsidentilInd, ");
            if (mIsSalesInvoice3UseNotesBasedOnCategory) SQL.AppendLine("RemarkNotesInd, ");
            if (mIsAPARUseType) SQL.AppendLine("TypeCode, ");
            if (mIsSLI3UseAdvanceCharge) SQL.AppendLine("AdvanceChargeCode, AdvanceChargeCode2, AdvanceChargeCode3, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @ReceiptNo, @DocDt, 'N', 'O', @CtCode, @DueDt, @LocalDocNo, @TaxInvDocument, @TaxInvDt, @CurCode, @TotalAmt, @TotalTax, @Downpayment, @Amt, @AdditionalCostDiscAmt, @MInd, @BankAcCode, @SalesName, Null, Null, ");
            SQL.AppendLine("@DeptCode, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt1, @TaxAmt2, @TaxAmt3, @Remark, @InsidentilInd, ");
            if (mIsSalesInvoice3UseNotesBasedOnCategory) SQL.AppendLine("@RemarkNotesInd, ");
            if (mIsAPARUseType) SQL.AppendLine("@TypeCode, ");
            if (mIsSLI3UseAdvanceCharge) SQL.AppendLine("@AdvanceChargeCode, @AdvanceChargeCode2, @AdvanceChargeCode3, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (Doctitle == "KIM")
                Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            else
                Sm.CmParam<String>(ref cm, "@ReceiptNo", string.Empty);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            if (mIsSLI3UseAutoFacturNumber)
                Sm.CmParam<String>(ref cm, "@TaxInvDocument", mTaxInvDocument);
            else
                Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AdditionalCostDiscAmt", mAdditionalCostDiscAmt);
            Sm.CmParam<String>(ref cm, "@MInd",
                (TxtTaxInvDocument.Text.Length==0 || mTaxInvDocument.Length == 0) && Sm.GetGrdStr(Grd1, 0, 30)=="1"?
                "Y":"N");
            Sm.CmParam<String>(ref cm, "@SalesName", Sm.GetValue("Select SpName From tblSalesPerson Where SpCode = '" + Sm.GetLue(LueSPCode) + "' "));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@RemarkNotesInd", ChkRemarkNotesInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TypeCode", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@LueAdvanceChargeCode", Sm.GetLue(LueAdvanceChargeCode));
            Sm.CmParam<String>(ref cm, "@LueAdvanceChargeCode2", Sm.GetLue(LueAdvanceChargeCode2));
            Sm.CmParam<String>(ref cm, "@LueAdvanceChargeCode3", Sm.GetLue(LueAdvanceChargeCode3));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@InsidentilInd", ChkInsidentilInd.Checked ? "Y" : "N");
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");
                        if (mSalesInvoiceTaxCalculationFormula != "1")
                            SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");
                        SQL.AppendLine("AmtForJournal, CreateBy, CreateDt) ");                        
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @DOCtDocNo_" + r.ToString() +
                        ", @DOCtDNo_" + r.ToString() +
                        ", 'O', @ItCode_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @UPriceBeforeTax_" + r.ToString() +
                        ", @TaxRate_" + r.ToString() +
                        ", @TaxAmt_" + r.ToString() +
                        ", @UPriceAfterTax_" + r.ToString());

                    if (mSalesInvoiceTaxCalculationFormula != "1")
                        SQL.AppendLine(
                        ", @TaxInd1_" + r.ToString() +
                        ", @TaxInd2_" + r.ToString() +
                        ", @TaxInd3_" + r.ToString() +
                        ", @TaxAmt1_" + r.ToString() +
                        ", @TaxAmt2_" + r.ToString() +
                        ", @TaxAmt3_" + r.ToString() +
                        ", @TaxAmtTotal_" + r.ToString());

                    SQL.AppendLine(", @AmtForJournal_" + r.ToString());
                    SQL.AppendLine(", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 30));
                    Sm.CmParam<String>(ref cm, "@DOCtDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@DOCtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 24));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 25));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
                    Sm.CmParam<Decimal>(ref cm, "@AmtForJournal_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 47));
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.CmParam<String>(ref cm, "@TaxInd1_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 38) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd2_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 39) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd3_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 40) ? "Y" : "N");
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 43));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 44));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 45));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 41));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSalesInvoiceDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");

        //    SQL.AppendLine("AmtForJournal, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @DOCtDocNo, @DOCtDNo, 'O', @ItCode, @QtyPackagingUnit, @Qty, @UPriceBeforeTax, @TaxRate, @TaxAmt, @UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("@TaxInd1, @TaxInd2, @TaxInd3, @TaxAmt1, @TaxAmt2, @TaxAmt3, @TaxAmtTotal, ");

        //    SQL.AppendLine("@AmtForJournal, @CreateBy, CurrentDateTime()); "); // 

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, Row, 30));
        //    Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax", Sm.GetGrdDec(Grd1, Row, 23));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, Row, 25));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", Sm.GetGrdDec(Grd1, Row, 26));
        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //    {
        //        Sm.CmParam<String>(ref cm, "@TaxInd1", Sm.GetGrdBool(Grd1, Row, 38) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd2", Sm.GetGrdBool(Grd1, Row, 39) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd3", Sm.GetGrdBool(Grd1, Row, 40) ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Sm.GetGrdDec(Grd1, Row, 43));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd1, Row, 44));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", Sm.GetGrdDec(Grd1, Row, 45));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal", Sm.GetGrdDec(Grd1, Row, 41));
        //    }
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParam<Decimal>(ref cm, "@AmtForJournal", Sm.GetGrdDec(Grd1, Row, 47));

        //    return cm;
        //}

        private MySqlCommand SaveSalesInvoiceDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        if(mIsSLI3UseAdvanceCharge)
                            SQL.AppendLine("Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc, AcInd, Remark, CreateBy, CreateDt, LocalName) ");
                        else
                            SQL.AppendLine("Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc, AcInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @OptAcDesc_" + r.ToString() +
                        ", @AcInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt ");
                    if (mIsSLI3UseAdvanceCharge) SQL.AppendLine(", @LocalName_" + r.ToString());
                    SQL.AppendLine(") ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 6));
                    Sm.CmParam<String>(ref cm, "@OptAcDesc_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@AcInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 10) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@LocalName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 11));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl4) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl4(DocNo, ARDownpaymentDocNo, ARDownpaymentBefTax, DownpaymentBefTax, TaxCode, TaxCode2, TaxCode3, TaxAmt, TaxAmt2, TaxAmt3, SLITaxAmt, SLITaxAmt2, SLITaxAmt3, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        "(@DocNo, @ARDownpaymentDocNo_" + r.ToString() +
                        ", @ARDownpaymentBefTax_" + r.ToString() +
                        ", @DownpaymentBefTax_" + r.ToString() +
                        ", @TaxCode_" + r.ToString() +
                        ", @TaxCode2_" + r.ToString() +
                        ", @TaxCode3_" + r.ToString() +
                        ", @TaxAmt_" + r.ToString() +
                        ", @TaxAmt2_" + r.ToString() +
                        ", @TaxAmt3_" + r.ToString() +
                        ", @SLITaxAmt_" + r.ToString() +
                        ", @SLITaxAmt2_" + r.ToString() +
                        ", @SLITaxAmt3_" + r.ToString() +
                        ",  @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@ARDownpaymentDocNo_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                    Sm.CmParam<decimal>(ref cm, "@ARDownpaymentBefTax_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 2));
                    Sm.CmParam<decimal>(ref cm, "@DownpaymentBefTax_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 16));
                    Sm.CmParam<String>(ref cm, "@TaxCode_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 3));
                    Sm.CmParam<String>(ref cm, "@TaxCode2_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 7));
                    Sm.CmParam<String>(ref cm, "@TaxCode3_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 11));
                    Sm.CmParam<decimal>(ref cm, "@TaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 6));
                    Sm.CmParam<decimal>(ref cm, "@TaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 10));
                    Sm.CmParam<decimal>(ref cm, "@TaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 14));
                    Sm.CmParam<decimal>(ref cm, "@SLITaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 17));
                    Sm.CmParam<decimal>(ref cm, "@SLITaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 18));
                    Sm.CmParam<decimal>(ref cm, "@SLITaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 19));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();
            return cm;
        }

        //private MySqlCommand SaveSalesInvoiceDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveSalesInvoiceDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl4) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl5(DocNo, DNo, ServiceDeliveryDocNo, ServiceDeliveryDNo, ");
                        if (mSalesInvoiceTaxCalculationFormula != "1")
                            SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");
                        SQL.AppendLine("CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ServiceDeliveryDocNo_" + r.ToString() +
                        ", @ServiceDeliveryDNo_" + r.ToString());

                    if (mSalesInvoiceTaxCalculationFormula != "1")
                        SQL.AppendLine(
                        ", @TaxInd1_" + r.ToString() +
                        ", @TaxInd2_" + r.ToString() +
                        ", @TaxInd3_" + r.ToString() +
                        ", @TaxAmt1_" + r.ToString() +
                        ", @TaxAmt2_" + r.ToString() +
                        ", @TaxAmt3_" + r.ToString() +
                        ", @TaxAmtTotal_" + r.ToString());

                    SQL.AppendLine(", @UserCode, @Dt) ");
                    SetGrd();

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ServiceDeliveryDocNo_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 1));
                    Sm.CmParam<String>(ref cm, "@ServiceDeliveryDNo_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 19));
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.CmParam<String>(ref cm, "@TaxInd1_" + r.ToString(), Sm.GetGrdBool(Grd6, r, 11) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd2_" + r.ToString(), Sm.GetGrdBool(Grd6, r, 12) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd3_" + r.ToString(), Sm.GetGrdBool(Grd6, r, 13) ? "Y" : "N");
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 16));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 17));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 18));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 14));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateDOCtProcessInd(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
            SQL.AppendLine("    And B.DocType='3' ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessIndNew ");
            SQL.AppendLine("Where A.ProcessInd=@ProcessIndOld; ");

            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("Update TblServiceDeliveryDtl A ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
                SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
                SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
                SQL.AppendLine("    And B.DocType='3' ");
                SQL.AppendLine("    And B.DocNo=@DocNo ");
                SQL.AppendLine("Set A.ProcessInd=@ProcessIndNew ");
                SQL.AppendLine("Where A.ProcessInd=@ProcessIndOld; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessIndOld", CancelInd=="N"?"O":"F");
            Sm.CmParam<String>(ref cm, "@ProcessIndNew", CancelInd=="N"?"F":"O");
            return cm;
        }

        public MySqlCommand SaveJournal(string DocNo, string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            var EntCode = Sm.GetGrdStr(Grd1, 0, 32);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            if (mIsSLI3JournalUseCCCode) SQL.AppendLine("CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            if (mIsSLI3JournalUseCCCode) SQL.AppendLine("@CCCode, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");


            // Piutang Usaha / Piutang Penjualan

            if (mIsItemCategoryUseCOAAPAR)
            {
                #region pakai COA AR
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    //penambahan kondisi mSLI3TaxCalculationFormat by dita 20/07/2022
                    if (mSLI3TaxCalculationFormat == "1")
                    {
                        if (mIsDOCtAllowMultipleItemCategory)
                        {
                            SQL.AppendLine("        SELECT D.AcNo10 AS AcNo, ");
                            SQL.AppendLine("        B.AmtForJournal As DAmt , ");
                            SQL.AppendLine("        0.00 As CAmt ");
                            SQL.AppendLine("        FROM tblsalesinvoicehdr A ");
                            SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl B ON A.DocNo = B.DocNo AND B.AmtForJournal > 0 ");
                            SQL.AppendLine("        INNER JOIN tblitem C ON B.ItCode = C.ItCode AND C.ActInd = 'Y' ");
                            SQL.AppendLine("        INNER JOIN tblitemcategory D ON C.ItCtCode = D.ItCtCode AND D.ActInd = 'Y' ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Select @COAAR As AcNo, ");
                            SQL.AppendLine("        A.TotalAmt+A.TotalTax-A.Downpayment As DAmt, ");
                            SQL.AppendLine("        0.00 As CAmt ");
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                    }
                    else
                    {
                        SQL.AppendLine("        Select @COAAR As AcNo, ");
                        SQL.AppendLine("        A.Amt As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                }
                else
                {
                    //penambahan kondisi mSLI3TaxCalculationFormat by dita 20/07/2022
                    if (mSLI3TaxCalculationFormat == "1")
                    {
                        if (mIsDOCtAllowMultipleItemCategory)
                        {
                            SQL.AppendLine("        Select D.AcNo10 As AcNo, ");
                            if (mIsDOCtAmtRounded)
                            {
                                SQL.AppendLine("        Floor(IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        (B.AmtForJournal)) As DAmt, ");
                            }
                            else
                            {
                                SQL.AppendLine("        IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        (B.AmtForJournal) As DAmt, ");
                            }
                        }
                        else
                        {
                            SQL.AppendLine("        Select @COAAR As AcNo, ");
                            if (mIsDOCtAmtRounded)
                            {
                                SQL.AppendLine("        Floor(IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment)) As DAmt, ");
                            }
                            else
                            {
                                SQL.AppendLine("        IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment) As DAmt, ");
                            }
                        }
                    }
                    else
                    {
                        SQL.AppendLine("        Select @COAAR As AcNo, ");
                        if (mIsDOCtAmtRounded)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.Amt)) As DAmt, ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.Amt) As DAmt, ");
                        }
                    }

                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    if (mIsDOCtAllowMultipleItemCategory)
                    {
                        SQL.AppendLine("        INNER JOIN tblsalesinvoicedtl B ON A.DocNo = B.DocNo AND B.AmtForJournal > 0 ");
                        SQL.AppendLine("        INNER JOIN tblitem C ON B.ItCode = C.ItCode AND C.ActInd = 'Y' ");
                        SQL.AppendLine("        INNER JOIN tblitemcategory D ON C.ItCtCode = D.ItCtCode AND D.ActInd = 'Y' ");
                    }
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

            #endregion
            }
            else
            {
                #region selain COA AR
                if (mSLI3TaxCalculationFormat == "1")
                {
                    #region Default
                    if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        A.TotalAmt+A.TotalTax-A.Downpayment As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        if (mIsDOCtAmtRounded)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment)) As DAmt, ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment) As DAmt, ");
                        }

                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    #endregion
                }
                else
                {
                    #region SRN
                    if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        A.Amt As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        if (mIsDOCtAmtRounded)
                        {
                            SQL.AppendLine("        Floor(IfNull((( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        )) * ");
                            SQL.AppendLine("        (A.Amt), 0)) As DAmt, ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.Amt) As DAmt, ");
                        }
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    #endregion
                }

                #endregion
            }

            if (mIsItemCategoryUseCOAAPAR)
            {
                // Pendapatan
                #region pakai COA AR
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    //penambahan kondisi mSLI3TaxCalculationFormat by dita 20/07/2022
                    if (mSLI3TaxCalculationFormat != "1")
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select ");

                        if (mSLIJournalIncomingAcNoSource == "1")
                            SQL.AppendLine("        @COASales As AcNo, ");
                        else if (mSLIJournalIncomingAcNoSource == "2")
                            SQL.AppendLine("        @COAARUninvoiced As AcNo, ");

                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        B.Amt As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select DocNo, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("            Floor( ");
                        SQL.AppendLine("            Sum(Qty * UPriceBeforeTax) ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("            ) ");
                        SQL.AppendLine("            As Amt ");
                        SQL.AppendLine("            From TblSalesInvoiceDtl ");
                        SQL.AppendLine("            Where DocNo = @DocNo ");
                        SQL.AppendLine("            Group By DocNo ");
                        SQL.AppendLine("        ) B On A.DocNo = B.DocNo ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select ");

                        if (mSLIJournalIncomingAcNoSource == "1")
                            SQL.AppendLine("        @COASales As AcNo, ");
                        else if (mSLIJournalIncomingAcNoSource == "2")
                            SQL.AppendLine("        @COAARUninvoiced As AcNo, ");

                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        A.TotalAmt As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");

                    if (mSLIJournalIncomingAcNoSource == "1")
                        SQL.AppendLine("        @COASales As AcNo, ");
                    else if (mSLIJournalIncomingAcNoSource == "2")
                        SQL.AppendLine("        @COAARUninvoiced As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");

                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor( ");

                    SQL.AppendLine("            Sum( ");
                    SQL.AppendLine("            IfNull( ");
                    SQL.AppendLine("                 ( ");
                    SQL.AppendLine("                      Select Amt From TblCurrencyRate ");
                    if (!mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                        SQL.AppendLine("                      Where RateDt <= C.DocDt And CurCode1 = C.CurCode And CurCode2 = @MainCurCode ");
                    else
                        SQL.AppendLine("                      Where RateDt <= B.DocDt And CurCode1 = C.CurCode And CurCode2 = @MainCurCode ");
                    SQL.AppendLine("                      Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("                ) ");
                    SQL.AppendLine("            , 0.00)  ");
                    if(mSalesInvoice3NonIDRJournalQtySource == "2")
                    {
                        SQL.AppendLine("            * (A.Qty * A.UPriceBeforeTax)) ");
                    }
                    else
                    {
                        SQL.AppendLine("            * (A.QtyPackagingUnit * A.UPriceBeforeTax)) ");
                    }
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        ) ");
                    SQL.AppendLine("            As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceDtl A ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select T1.DocNo, T2.DNo, T1.DocDt ");
                    SQL.AppendLine("            From TblDOCtHdr T1 ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("            Inner Join TblSalesInvoiceDtl T3 On T1.DocNo = T3.DOCtDocNo And T2.DNo = T3.DOCtDNo ");
                    SQL.AppendLine("                And T3.DocNo = @DocNo ");
                    if (mIsUseServiceDelivery)
                    {
                        SQL.AppendLine("            Union All ");
                        SQL.AppendLine("            Select T1.DocNo, T2.DNo, T1.DocDt ");
                        SQL.AppendLine("            From TblServiceDeliveryHdr T1 ");
                        SQL.AppendLine("            Inner Join TblServiceDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
                        SQL.AppendLine("            Inner Join TblSalesInvoiceDtl T3 On T1.DocNo = T3.DOCtDocNo And T2.DNo = T3.DOCtDNo ");
                        SQL.AppendLine("                And T3.DocNo = @DocNo ");
                    }
                    SQL.AppendLine("        ) B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
                    //SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DOCtDocNo = B.DocNo And A.DOCtDNo = B.DNo ");
                    //SQL.AppendLine("            And A.DocNo = @DocNo ");
                    //SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DocNo = C.DocNo ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceHdr C On A.DocNo = C.DocNo ");
                    SQL.AppendLine("            And A.DocNo = @DocNo ");
                }
                #endregion
            }
            else
            {
                // Piutang Uninvoice
                #region selain COA AR

                if (mIsSalesInvoice3JournalUseItCtSalesEnabled)
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select F.AcNo4, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded) SQL.AppendLine("     Floor(");
                    SQL.AppendLine("        C.UPrice*C.Qty* ");
                    SQL.AppendLine("        Case When C.CurCode = @MainCurCode Then 1.00 Else ");
	                SQL.AppendLine("        IfNull(( ");
	 	            SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt <= C.DocDt And CurCode1 = C.CurCode And CurCode2 = @MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) ");
                    SQL.AppendLine("        End ");
                    if (mIsDOCtAmtRounded) SQL.AppendLine(" )");
                    SQL.AppendLine("        As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt, T3.Qty, T3.UPrice, T2.CurCode, T3.ItCode ");
                    SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                    SQL.AppendLine("            Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo ");
                    SQL.AppendLine("                And T1.DocNo = @DocNo ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                    if (mIsUseServiceDelivery)
                    {
                        SQL.AppendLine("            Union All ");
                        SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt, T3.Qty, T3.UPrice, T2.CurCode, T3.ItCode ");
                        SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                        SQL.AppendLine("            Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo ");
                        SQL.AppendLine("                And T1.DocNo = @DocNo ");
                        SQL.AppendLine("            Inner Join TblServiceDeliveryDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                    }
                    SQL.AppendLine("        ) C On A.DocNo = C.DocNo And B.DNo = C.DNo ");
                    //SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DOCtDocNo=C.DocNo  ");
                    //SQL.AppendLine("        Inner Join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
                    SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode  ");
                    SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo4 Is Not Null  ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");   
                }
                else
                {
                    if (mSLI3TaxCalculationFormat == "1")
                    {
                        #region Default

                        if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                        {
                            if (!mIsJournalSalesInvoiceUseItemsCategory)
                            {
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                                SQL.AppendLine("        0.00 As DAmt, ");
                                SQL.AppendLine("        A.TotalAmt As CAmt ");
                                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                            }
                            else if (mIsJournalSalesInvoiceUseItemsCategory)
                            {
                                SQL.AppendLine("    Union All ");
                                SQL.AppendLine("        Select D.AcNo4 As AcNo, 0.00 As DAmt, ");
                                SQL.AppendLine("        IfNull(B.Qty, 0.00) * IfNull(B.UPriceBeforeTax, 0.00) As CAmt ");
                                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
                                SQL.AppendLine("        Inner Join TblItem C On B.ItCode = C.ItCode ");
                                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo4 Is Not Null ");
                                SQL.AppendLine("        Where A.DocNo = @DocNo ");
                            }
                        }
                        else
                        {
                            SQL.AppendLine("    Union All ");

                            #region Old Code, ngga perlu parameter kata ian via WA Chat ke wedha tanggal 22 des 2020 jam 14:57
                            //SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            //SQL.AppendLine("        0.00 As DAmt, ");
                            //if (mIsDOCtAmtRounded)
                            //{
                            //    SQL.AppendLine("        Floor(IfNull((");
                            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            //    SQL.AppendLine("        ), 0) * ");
                            //    SQL.AppendLine("        A.TotalAmt) As CAmt ");
                            //}
                            //else
                            //{
                            //    SQL.AppendLine("        IfNull(( ");
                            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            //    SQL.AppendLine("        ), 0) * ");
                            //    SQL.AppendLine("        A.TotalAmt As CAmt ");
                            //}
                            //SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            //SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
                            #endregion

                            if (!mIsJournalSalesInvoiceUseItemsCategory)
                            {
                                SQL.AppendLine("        Select Concat(D.Parvalue, E.CtCode) As AcNo, 0.00 As DAmt, ");
                                if (mIsDOCtAmtRounded)
                                    SQL.AppendLine("        Floor( ");
                                SQL.AppendLine("            Sum( ");
                                SQL.AppendLine("            IfNull( ");
                                SQL.AppendLine("                 ( ");
                                SQL.AppendLine("                      Select Amt From TblCurrencyRate ");
                                if (!mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                                    SQL.AppendLine("                      Where RateDt <= E.DocDt And CurCode1 = E.CurCode And CurCode2 = @MainCurCode ");
                                else
                                    SQL.AppendLine("                      Where RateDt <= B.DocDt And CurCode1 = E.CurCode And CurCode2 = @MainCurCode ");
                                SQL.AppendLine("                      Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("                ) ");
                                SQL.AppendLine("            , 0.00)  ");
                                //SQL.AppendLine("            * (A.QtyPackagingUnit * A.UPriceBeforeTax)) ");
                                SQL.AppendLine("            * (Round(A.Qty * A.UPriceBeforeTax, 4))) ");
                                if (mIsDOCtAmtRounded)
                                    SQL.AppendLine("        ) ");
                                SQL.AppendLine("            As CAmt ");
                                SQL.AppendLine("        From TblSalesInvoiceDtl A ");
                                SQL.AppendLine("        Inner Join ( ");
                                SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt ");
                                SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                                SQL.AppendLine("            Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                                SQL.AppendLine("            Inner Join TblDOCtDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                                if (mIsUseServiceDelivery)
                                {
                                    SQL.AppendLine("            Union All ");
                                    SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt ");
                                    SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                                    SQL.AppendLine("            Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                                    SQL.AppendLine("            Inner Join TblServiceDeliveryDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                                }
                                SQL.AppendLine("        ) B On A.DocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
                                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'CustomerAcNoNonInvoice' And D.ParValue Is Not Null ");
                                SQL.AppendLine("        Inner Join TblSalesInvoiceHdr E On A.DocNo = E.DocNo ");
                                SQL.AppendLine("        Group By Concat(D.Parvalue, E.CtCode) ");
                            }
                            else if (mIsJournalSalesInvoiceUseItemsCategory)
                            {
                                SQL.AppendLine("        Select F.AcNo4 As AcNo, 0.00 As DAmt, ");
                                if (mIsDOCtAmtRounded)
                                    SQL.AppendLine("        Floor( ");
                                SQL.AppendLine("            Sum( ");
                                SQL.AppendLine("            IfNull( ");
                                SQL.AppendLine("                 ( ");
                                SQL.AppendLine("                      Select Amt From TblCurrencyRate ");
                                if (!mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                                    SQL.AppendLine("                      Where RateDt <= D.DocDt And CurCode1 = D.CurCode And CurCode2 = @MainCurCode ");
                                else
                                    SQL.AppendLine("                      Where RateDt <= B.DocDt And CurCode1 = D.CurCode And CurCode2 = @MainCurCode ");
                                SQL.AppendLine("                      Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("                ) ");
                                SQL.AppendLine("            , 0.00)  ");
                                //SQL.AppendLine("            * (A.QtyPackagingUnit * A.UPriceBeforeTax)) ");
                                SQL.AppendLine("            * (Round(A.Qty * A.UPriceBeforeTax, 4))) ");
                                if (mIsDOCtAmtRounded)
                                    SQL.AppendLine("        ) ");
                                SQL.AppendLine("            As CAmt ");
                                SQL.AppendLine("        From TblSalesInvoiceDtl A ");
                                SQL.AppendLine("        Inner Join ( ");
                                SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt ");
                                SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                                SQL.AppendLine("            Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                                SQL.AppendLine("            Inner Join TblDOCtDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                                if (mIsUseServiceDelivery)
                                {
                                    SQL.AppendLine("            Union All ");
                                    SQL.AppendLine("            Select T1.DocNo, T1.DNo, T2.DocDt ");
                                    SQL.AppendLine("            From TblSalesInvoiceDtl T1 ");
                                    SQL.AppendLine("            Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                                    SQL.AppendLine("            Inner Join TblServiceDeliveryDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                                }
                                SQL.AppendLine("        ) B On A.DocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
                                SQL.AppendLine("        Inner Join TblSalesInvoiceHdr D On A.DocNo = D.DocNo ");
                                SQL.AppendLine("        Inner Join TblItem E On A.ItCode = E.ItCode ");
                                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode And F.AcNo4 Is Not Null ");
                                SQL.AppendLine("        Group By F.AcNo4 ");
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region SRN
                        if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            SQL.AppendLine("        C.Amt As CAmt ");
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            SQL.AppendLine("        Inner Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select DocNo, ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            Floor( ");
                            SQL.AppendLine("            Sum(Qty * UPriceBeforeTax) ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            ) ");
                            SQL.AppendLine("            As Amt ");
                            SQL.AppendLine("            From TblSalesInvoiceDtl ");
                            SQL.AppendLine("            Where DocNo = @DocNo ");
                            SQL.AppendLine("            Group By DocNo ");
                            SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            if (mIsDOCtAmtRounded)
                            {
                                SQL.AppendLine("        Floor(IfNull((");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        C.Amt) As CAmt ");
                            }
                            else
                            {
                                SQL.AppendLine("        IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        C.Amt As CAmt ");
                            }
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            SQL.AppendLine("        Inner Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                            SQL.AppendLine("            From TblSalesInvoiceDtl ");
                            SQL.AppendLine("            Where DocNo = @DocNo ");
                            SQL.AppendLine("            Group By DocNo ");
                            SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        #endregion
                    }
                }

                #endregion
            }

            // PPN Keluaran
            if (mSalesInvoiceTaxCalculationFormula == "2")
            {
                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                SQL.AppendLine("        Case When B.TaxAmt1 >= 0 Then 0.00 Else Abs(B.TaxAmt1) End As DAmt, ");
                SQL.AppendLine("        Case When B.TaxAmt1 >= 0 Then B.TaxAmt1 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd1 = 'Y' ");
                SQL.AppendLine("            And A.TaxCode1 Is Not Null ");
                SQL.AppendLine("        Inner Join TblTax C On A.TaxCode1 = C.TaxCode And C.AcNo2 Is Not Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                SQL.AppendLine("        Case When B.TaxAmt2 >= 0 Then 0.00 Else Abs(B.TaxAmt2) End As DAmt, ");
                SQL.AppendLine("        Case When B.TaxAmt2 >= 0 Then B.TaxAmt2 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd2 = 'Y' ");
                SQL.AppendLine("            And A.TaxCode2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblTax C On A.TaxCode2 = C.TaxCode And C.AcNo2 Is Not Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                SQL.AppendLine("        Case When B.TaxAmt3 >= 0 Then 0.00 Else Abs(B.TaxAmt3) End As DAmt, ");
                SQL.AppendLine("        Case When B.TaxAmt3 >= 0 Then B.TaxAmt3 Else 0.00 End As CAmt ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd3 = 'Y' ");
                SQL.AppendLine("            And A.TaxCode3 Is Not Null ");
                SQL.AppendLine("        Inner Join TblTax C On A.TaxCode3 = C.TaxCode And C.AcNo2 Is Not Null ");

                if (mIsUseServiceDelivery)
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                    SQL.AppendLine("        Case When B.TaxAmt1 >= 0 Then 0.00 Else Abs(B.TaxAmt1) End As DAmt, ");
                    SQL.AppendLine("        Case When B.TaxAmt1 >= 0 Then B.TaxAmt1 Else 0.00 End As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl5 B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd1 = 'Y' ");
                    SQL.AppendLine("            And A.TaxCode1 Is Not Null ");
                    SQL.AppendLine("        Inner Join TblTax C On A.TaxCode1 = C.TaxCode And C.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                    SQL.AppendLine("        Case When B.TaxAmt2 >= 0 Then 0.00 Else Abs(B.TaxAmt2) End As DAmt, ");
                    SQL.AppendLine("        Case When B.TaxAmt2 >= 0 Then B.TaxAmt2 Else 0.00 End As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl5 B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd2 = 'Y' ");
                    SQL.AppendLine("            And A.TaxCode2 Is Not Null ");
                    SQL.AppendLine("        Inner Join TblTax C On A.TaxCode2 = C.TaxCode And C.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Case When C.TaxRate >= 0 Then C.AcNo2 Else C.AcNo1 End As AcNo, ");
                    SQL.AppendLine("        Case When B.TaxAmt3 >= 0 Then 0.00 Else Abs(B.TaxAmt3) End As DAmt, ");
                    SQL.AppendLine("        Case When B.TaxAmt3 >= 0 Then B.TaxAmt3 Else 0.00 End As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl5 B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.DocNo = @DocNo And B.TaxInd3 = 'Y' ");
                    SQL.AppendLine("            And A.TaxCode3 Is Not Null ");
                    SQL.AppendLine("        Inner Join TblTax C On A.TaxCode3 = C.TaxCode And C.AcNo2 Is Not Null ");
                }
            }
            else
            {
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt1)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt1) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt2)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt2) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt3)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt3) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }

                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
            }

            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            
            {
                if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select ");
                    if (mIsAPARUseType)
                        SQL.AppendLine("B.Property1 as AcNo, ");
                    else
                        SQL.AppendLine("        Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.Downpayment As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    if (mIsAPARUseType)
                        SQL.AppendLine("        Inner Join tbloption B ON B.OptCode = A.TypeCode AND B.OptCat = 'AcNoTypeForARDP'");
                    else
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");

                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select ");

                    if (mIsAPARUseType)
                        SQL.AppendLine("B.Property1 as AcNo, ");
                    else
                        SQL.AppendLine("        Concat(B.ParValue, A.CtCode) As AcNo, ");

                    if (mIsDOCtAmtRounded)
                    {
                        SQL.AppendLine("        Floor(IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00)*A.Downpayment) As DAmt, ");
                    }
                    else
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                        SQL.AppendLine("        A.Downpayment As DAmt, ");
                    }
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    if (mIsAPARUseType)
                        SQL.AppendLine("        Inner Join tbloption B ON B.OptCode = A.TypeCode AND B.OptCat = 'AcNoTypeForARDP'");
                    else
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }


            //List of COA
            if(mSalesInvoice3JournalFormula == "1")
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.DAmt, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.CAmt ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl2 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                if (mSLI3TaxCalculationFormat != "1")
                {
                    SQL.AppendLine("        And B.AcNo Not Like @CtCode ");
                }
            }

            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            if(mSalesInvoice3JournalFormula == "2")
            {
                //update COA Exists
                SQL.AppendLine("UPDATE TblJournalDtl A ");
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("	SELECT B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("	C.DAmt + Case ");
                SQL.AppendLine("		When ColType = 'D' AND B.DAmt > 0 then B.DAmt ");
                SQL.AppendLine("		When ColType = 'D' AND B.CAmt > 0 Then B.CAmt*-1 ");
                SQL.AppendLine("		ELSE 0 ");
                SQL.AppendLine("	END DAmtNew, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("	C.CAmt + Case ");
                SQL.AppendLine("		When ColType = 'C' AND B.CAmt > 0 then B.CAmt ");
                SQL.AppendLine("		When ColType = 'C' AND B.DAmt > 0 then B.DAmt*-1 ");
                SQL.AppendLine("		ELSE 0 ");
                SQL.AppendLine("	END CAmtNew ");
                SQL.AppendLine("	FROM TblSalesInvoiceHdr A ");
                SQL.AppendLine("	INNER JOIN TblSalesInvoiceDtl2 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("	INNER JOIN ( ");
                SQL.AppendLine("		select DocNo, AcNo, DAmt, CAmt, ");
                SQL.AppendLine("		Case ");
                SQL.AppendLine("			When DAmt > 0 then 'D' ");
                SQL.AppendLine("			ELSE 'C' ");
                SQL.AppendLine("		END ColType ");
                SQL.AppendLine("		FROM TblJournalDtl ");
                SQL.AppendLine("		WHERE DocNo = @JournalDocNo ");
                SQL.AppendLine("	)C ON B.AcNo = C.AcNo ");
                SQL.AppendLine("	WHERE A.DocNo = @DocNo ");
                SQL.AppendLine(")B ON A.AcNo = B.AcNo AND A.DocNo = @JournalDocNo ");
                SQL.AppendLine("SET A.DAmt = B.DAmtNew, A.CAmt = B.CAmtNew; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("        Select B.AcNo, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.DAmt DAmt, ");
                if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
                {
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                }
                SQL.AppendLine("        B.CAmt CAmt ");
                SQL.AppendLine("	FROM TblSalesInvoiceHdr A ");
                SQL.AppendLine("	INNER JOIN TblSalesInvoiceDtl2 B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("	WHERE A.DocNo = @DocNo ");
                SQL.AppendLine("	AND B.AcNo NOT IN ( ");
                SQL.AppendLine("		SELECT AcNo ");
                SQL.AppendLine("		FROM TblJournalDtl ");
                SQL.AppendLine("		WHERE DocNo = @JournalDocNo ");
                SQL.AppendLine("	) ");
                SQL.AppendLine(")T; ");
            }

            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }
            else
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ParValue From TblParameter Where ParCode='SIRoundedMaxValIfJournalNotBalance' And ParValue Is Not Null ");
                SQL.AppendLine(") C On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt=B.CAmt Then ");
                SQL.AppendLine("        A.DAmt Else ");
                SQL.AppendLine("        Case When Abs(B.DAmt-B.CAmt)<IfNull(C.ParValue, 0.00) Then ");
                SQL.AppendLine("            A.DAmt-(B.DAmt-B.CAmt) Else A.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    );");
            }

            if (mIsForeignCurrencyExchangeUseExpense)
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblParameter B On B.Parcode = 'AcNoForForeignCurrencyExchangeExpense' and B.ParValue Is Not NULL ");
                SQL.AppendLine("INNER JOIN TblParameter C ON C.Parcode = 'AcNoForForeignCurrencyExchangeGains' and C.ParValue Is Not NULL  ");
                SQL.AppendLine("Set A.AcNo = ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When A.DAmt > A.CAmt then B.ParValue ");
                SQL.AppendLine("    else C.ParValue ");
                SQL.AppendLine("End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("); "); 

            }
            SQL.AppendLine("Delete From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo ");
            SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
            SQL.AppendLine("And AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mIsItemCategoryUseCOAAPAR)
            {
                Sm.CmParam<String>(ref cm, "@COAAR", GetCOAAR());
                Sm.CmParam<String>(ref cm, "@COASales", GetCOASales());
                Sm.CmParam<String>(ref cm, "@COAARUninvoiced", GetCOAARUninvoiced());
            }
            if(mIsJournalSalesInvoiceUseItemsCategory)
                Sm.CmParam<String>(ref cm, "@COASales", GetCOASales());
            //else
            //{
            //    Sm.CmParam<String>(ref cm, "@COAAR", GetCOAAR());
            //    Sm.CmParam<String>(ref cm, "@COASales", GetCOAARUninvoiced());

            //}
            if (mSLI3TaxCalculationFormat != "1")
            {
                if(mIsItemCategoryUseCOAAPAR) Sm.CmParam<String>(ref cm, "@CtCode", GetCOAAR());
                else Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.", Sm.GetLue(LueCtCode)));
            } 
            else Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.", Sm.GetLue(LueCtCode)));


            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);

            if (mJournalDocNoFormat == "1")
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            if (mIsSLI3JournalUseCCCode)
            {
                var EntSQL = new StringBuilder();

                EntSQL.AppendLine("Select T.CCCode ");
                EntSQL.AppendLine("From ( ");
                EntSQL.AppendLine("    Select B.CCCode ");
                EntSQL.AppendLine("    From TblDOCtHdr A ");
                EntSQL.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
                EntSQL.AppendLine("    Where A.DocNo = @Param ");
                if (mIsUseServiceDelivery)
                {
                    EntSQL.AppendLine("    Union All ");
                    EntSQL.AppendLine("    Select CCCode ");
                    EntSQL.AppendLine("    From TblServiceDeliveryHdr ");
                    EntSQL.AppendLine("    Where DocNo = @Param ");
                }
                EntSQL.AppendLine(") T ");
                EntSQL.AppendLine("Limit 1; ");
                
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue(EntSQL.ToString(), Sm.GetGrdStr(Grd1, 0, 2)));
            }
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateFacturNumber(string FacturNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UPDATE TblFacturNumberDtl ");
            SQL.AppendLine("SET UsedInd = 'Y', SLIDocNo =@DocNo ,LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("WHERE FacturNo = @FacturNo And UsedInd= 'N'");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FacturNo", FacturNo);

            return cm;

        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string JournalDocNo = string.Empty;

            //if(mDocNoFormat == "1")
            //    JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            //else
            //    JournalDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1");

            var code1 = Sm.GetCode1ForJournalDocNo("FrmSalesInvoice3", string.Empty, string.Empty, mJournalDocNoFormat);
            var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode In (Select B.CCCode From TblDOCtHdr A Inner Join TblWarehouse B On A.WhsCode=B.WhsCode Where A.DocNo=@Param) ", Sm.GetGrdStr(Grd1, 0, 2));

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoiceHdr());

            if (ChkCancelInd.Checked)
            {
                if (decimal.Parse(TxtDownpayment.Text) != 0)
                {
                    var lR = new List<Rate>();
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select B.CtCode, A.CurCode, A.ExcRate, A.Amt ");
                    SQL.AppendLine("From TblSalesInvoiceDtl3 A ");
                    SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("Where A.DocNo = @DocNo And B.CtCode = @CtCode ");
                    SQL.AppendLine("And A.CurCode = @CurCode ");
                    SQL.AppendLine("And A.Amt > 0 Order By A.CreateDt Desc; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                        Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lR.Add(new Rate()
                                {
                                    CtCode = Sm.DrStr(dr, c[0]),

                                    CurCode = Sm.DrStr(dr, c[1]),
                                    ExcRate = Sm.DrDec(dr, c[2]),
                                    Amt = Sm.DrDec(dr, c[3])
                                });
                            }
                        }
                        dr.Close();
                    }

                    if (lR.Count > 0)
                    {
                        for (int i = 0; i < lR.Count; i++)
                        {
                            cml.Add(UpdateSummary2(ref lR, i));

                        }
                    }

                    cml.Add(SaveCustomerDeposit(TxtDocNo.Text));
                }
                cml.Add(UpdateDOCtProcessInd(TxtDocNo.Text, "Y"));
                if (mIsAutoJournalActived) cml.Add(SaveJournal2(code1, profitCenterCode));
            }
            Sm.ExecCommands(cml);

            //EDIT UPLOAD
            //if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile)
            //{
            //    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
            //        {
            //            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd5, Row, 4).Length == 0)
            //            {
            //                if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd5, Row, 1))) return;
            //            }
            //        }
            //    }

            //    cml.Add(SaveUploadFile(TxtDocNo.Text));

            //    Sm.ExecCommands(cml);

            //    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
            //        {
            //            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd5, Row, 4).Length == 0)
            //            {
            //                UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd5, Row, 1));
            //            }
            //        }
            //    }
            //}
            //EDIT UPLOAD

            ShowData(TxtDocNo.Text);
            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile) ShowUploadFile(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ? 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                (!mIsSalesInvoice3UseNotesBasedOnCategory && !mIsSalesInvoiceTaxInvDocEditable && !mIsSalesInvoice3TaxInvDocumentAbleToEdit && IsDocumentNotCancelled()) ||
                IsDataCancelledAlready() ||
                //((mIsSLIBasedOnDOToCustomerAllowtoUploadFile && TxtStatus.Text == "Approved") && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") || !mIsSLIBasedOnDOToCustomerAllowtoUploadFile && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation")) ||
                //((mIsSLIBasedOnDOToCustomerAllowtoUploadFile && TxtStatus.Text == "Approved") && IsCancelIndNotTrue() || !mIsSLIBasedOnDOToCustomerAllowtoUploadFile && IsCancelIndNotTrue()) ||
                (ChkCancelInd.Checked && IsDataAlreadyProcessedToIncomingPayment()) ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted()) ||
                (!ChkCancelInd.Checked && IsSalesInvoice3TaxInvDocumentNotAbleToEdit(true))
                ;
        }


        private bool IsSalesInvoice3TaxInvDocumentNotAbleToEdit(bool IsShowWarningMsg)
        {
            if (!mIsSalesInvoice3TaxInvDocumentAbleToEdit) return false;

            if (Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr " +
                "Where TaxInvDocument Is Not Null " +
                "And PrevTaxInvDocument Is Not Null " +
                "And TaxInvDocument<>PrevTaxInvDocument " +
                "And DocNo=@Param Limit 1;",
                TxtDocNo.Text))
            {
                if (IsShowWarningMsg)
                    Sm.StdMsg(mMsgType.Warning, "You can't edit tax invoice#.");
                return true;
            }

            return false;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblSalesInvoiceHdr Where CancelInd='Y' And DocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            if (Sm.IsDataExist("Select 1 From TblIncomingPaymentHdr A Inner Join TblIncomingPaymentDtl B On A.DocNo=B.DocNo Where A.CancelInd='N' And B.InvoiceDocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT 1 FROM TblSalesInvoiceHdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSalesInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            if (ChkCancelInd.Checked) 
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, ");
            if (mIsSalesInvoice3TaxInvDocumentAbleToEdit)
            {
                if (!ChkCancelInd.Checked)
                    SQL.AppendLine("    TaxInvDocument=@TaxInvDocument, PrevTaxInvDocument=Case When PrevTaxInvDocument Is Null Then @TaxInvDocument Else PrevTaxInvDocument End, ");
            }
            else
            {
                if (mIsSalesInvoiceTaxInvDocEditable)
                {
                    if (!ChkCancelInd.Checked) 
                        SQL.AppendLine("    TaxInvDocument=@TaxInvDocument, PrevTaxInvDocument=@TaxInvDocument, ");
                }
            }
            if (mIsSalesInvoice3UseNotesBasedOnCategory)
            {
                if (!ChkCancelInd.Checked)
                    SQL.AppendLine("RemarkNotesInd=@RemarkNotesInd, ");
            }
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@RemarkNotesInd", ChkRemarkNotesInd.Checked ? "Y" : "N");

            return cm;
        }

        public MySqlCommand SaveJournal2(string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);
            var SelectedDt = CurrentDt;
            if (!IsClosingJournalUseCurrentDt) SelectedDt = DocDt;

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, ");
            SQL.AppendLine("EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //if (IsClosingJournalUseCurrentDt)
            //    if(mDocNoFormat == "1")
            //        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            //    else
            //        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            //else
            //    if(mDocNoFormat == "1")
            //        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            //    else
            //        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));

            if (mJournalDocNoFormat == "1")
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(SelectedDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(SelectedDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(SelectedDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoiceHdr(DocNo);
                ShowSalesInvoiceDtl(DocNo);
                ShowSalesInvoiceDtl2(DocNo);
                if(mIsSalesInvoiceUseTabToInputDownpayment) ShowSalesInvoiceDtl3(DocNo);
                //if (mIsUseServiceDelivery) ShowSalesInvoiceDtl4(DocNo);
                if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile) ShowUploadFile(DocNo);
                ComputeAmt();
                ComputeAmtFromTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowSalesInvoice(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                Sm.SetDteCurrentDate(DteDocDt);
                ShowSalesInvoiceHdr2(DocNo);
                ShowSalesInvoiceDtl12(DocNo);
                ShowSalesInvoiceDtl22(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoiceHdr(string DocNo)
        {
            if (mIsCustomerComboBasedOnCategory)
            {
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
            }

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.ReceiptNo, A.DocDt, A.CancelReason, A.CancelInd, A.DeptCode, ");
            SQL.AppendLine("A.CtCode, C.CtCtName, A.DueDt, A.LocalDocNo, A.TaxInvDocument, ");
            SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.DownPayment, A.Amt, ");
            SQL.AppendLine("A.BankAcCode, A.SalesName, A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxInvDt, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2, A.AdditionalCostDiscAmt, B.CtCtCode ");
            if (mIsSalesInvoice3UseNotesBasedOnCategory)
                SQL.AppendLine(", A.RemarkNotesInd ");
            else
                SQL.AppendLine(", 'Y' as RemarkNotesInd ");
            if (mIsAPARUseType)
                SQL.AppendLine(", A.TypeCode");
            else
                SQL.AppendLine(", Null as TypeCode ");
            if (mIsSLI3UseAdvanceCharge)
                SQL.AppendLine(", A.AdvanceChargeCode, A.AdvanceChargeCode2, A.AdvanceChargeCode3 ");
            else
                SQL.AppendLine(", Null As AdvanceChargeCode, Null As AdvanceChargeCode2, Null As AdvanceChargeCode3 ");
            if (mIsSLI3UseInsidentilInformation)
                SQL.AppendLine(", A.InsidentilInd ");
            else
                SQL.AppendLine(", Null As InsidentilInd ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblCustomerCategory C On B.CtCtCode=C.CtCtCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode = D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "ReceiptNo", "DocDt", "CancelReason", "CtCode", "DueDt", 
                    
                    //6-10
                    "LocalDocNo", "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", 
                    
                    //11-14
                    "DownPayment",  "Amt", "BankAcCode", "SalesName", "TaxCode1", 
                    
                    //16-20
                    "TaxCode2", "TaxCode3", "TaxInvDt", "CancelInd", "Remark",

                    //21-25
                    "JournalDocNo", "JournalDocNo2", "AdditionalCostDiscAmt", "CtCtName", "CtCtCode", 

                    //26-30
                    "RemarkNotesInd", "DeptCode", "TypeCode", "AdvanceChargeCode", "AdvanceChargeCode2",

                    //31-32
                    "AdvanceChargeCode3", "InsidentilInd"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtReceiptNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                    Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[5]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                    TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname  = '" + Sm.DrStr(dr, c[14]) + "'"));
                    Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[13]));
                    Sl.SetLueTaxCode(ref LueTaxCode1);
                    Sl.SetLueTaxCode(ref LueTaxCode2);
                    Sl.SetLueTaxCode(ref LueTaxCode3);
                    Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[15]));
                    Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[16]));
                    Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[17]));
                    Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[18]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                    MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[21]);
                    TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[22]);
                    mAdditionalCostDiscAmt = Sm.DrDec(dr, c[23]);
                    TxtCtCtCode.EditValue = Sm.DrStr(dr, c[24]);
                    if (mIsCustomerComboBasedOnCategory) Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[25]));
                    ChkRemarkNotesInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[26]), "Y");
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[27]));
                    Sm.SetLue(LueType, Sm.DrStr(dr, c[28]));
                    Sm.SetLue(LueAdvanceChargeCode, Sm.DrStr(dr, c[29]));
                    Sm.SetLue(LueAdvanceChargeCode2, Sm.DrStr(dr, c[30]));
                    Sm.SetLue(LueAdvanceChargeCode3, Sm.DrStr(dr, c[31]));
                    ChkInsidentilInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[32]), "Y");
                }, true
            );            
        }

        private void ShowSalesInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Dno, B.Doctype, B.DoctDocno, B.DOCtDno, D.DocDt, B.ItCode, C.ItCodeInternal, C.Specification, ");
            SQL.AppendLine("C.ItName, B.Qty QtyPackagingUnit, C.SalesuomCode As PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, C.SalesUomCode As PriceUomCode, B.UpriceBeforeTax, ");
            if (mSalesInvoiceTaxCalculationFormula != "1")
                SQL.AppendLine("B.TaxInd1, B.TaxInd2, B.TaxInd3, B.TaxAmt1, B.TaxAmt2, B.TaxAmt3, B.TaxAmtTotal, ");
            else
                SQL.AppendLine("'N' As TaxInd1, 'N' As TaxInd2, 'N' As TaxInd3, 0.00 As TaxAmt1, 0.00 As TaxAmt2, 0.00 As TaxAmt3, 0.00 As TaxAmtTotal, ");
            SQL.AppendLine("B.TaxRate, B.TaxAmt, B.UPriceAfterTax, D.DocNoInternal, D.Remark, D.Qty2, B.AmtForJournal ");

            if (mIsSLIBasedOnDoShowForeignName) SQL.AppendLine(", C.ForeignName ");
            else SQL.AppendLine(", Null As ForeignName ");

            SQL.AppendLine("From TblSalesInvoicehdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, T2.DocDt, T2.DocNoInternal, T3.Remark, T3.Qty2 ");
            SQL.AppendLine("    From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, T2.DocDt, T2.DocNoInternal, T3.Remark, T3.Qty2 ");
                SQL.AppendLine("    From TblSalesInvoiceDtl T1 ");
                SQL.AppendLine("    Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
            }
            SQL.AppendLine(") D On B.DocNo = D.DocNo And B.DNo = D.DNo ");
            //SQL.AppendLine("Inner Join TblDOCtHdr D On B.DOCtDocNo = D.DocNo ");
            //SQL.AppendLine("Inner Join TblDoctDtl E ON D.DocNo = E.DocNo AND B.DOCtDNo = E.DNo");
            SQL.AppendLine("Where B.DocType = '3' And A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "ItCode", "ItName", 

                    
                    
                    //6-10
                    "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "UPriceBeforeTax", 
                    
                    //11-15
                    "TaxRate", "TaxAmt", "UPriceAfterTax", "DocType", "DocNoInternal",

                    //16-20
                    "ItCodeInternal", "Specification", "Remark", "TaxInd1", "TaxInd2", 
                    
                    //21-25
                    "TaxInd3", "TaxAmtTotal", "TaxAmt1", "TaxAmt2", "TaxAmt3",
                    
                    //26-28
                    "Qty2", "AmtForJournal", "ForeignName",
                },
                
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 9);
                    Grd.Cells[Row, 22].Value = Sm.GetLue(LueCurCode);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 13);
                    if (mIsDOCtAmtRounded)
                        Grd.Cells[Row, 27].Value = decimal.Truncate(dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]));
                    else
                        Grd.Cells[Row, 27].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]);
                    Grd.Cells[Row, 28].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 18);
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 38, 19);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 39, 20);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 40, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 22);
                        if (mSalesBasedDOQtyIndex == "1")
                            Grd.Cells[Row, 42].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[10]);
                        else
                            Grd.Cells[Row, 42].Value = dr.GetDecimal(c[26]) * dr.GetDecimal(c[10]);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 25);
                    }
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 27);

                    
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 28);
                }, false, false, true, false 
                
               

            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45, 46, 47, 48, 49 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            if(mIsSLI3UseAdvanceCharge)
                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark, A.LocalName ");
            else
                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark, Null As LocalName ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A "); 
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark", "LocalName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowSalesInvoiceDtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT A.ARDownpaymentDocNo, A.ARDownpaymentBefTax, A.DownpaymentBefTax, A.TaxCode, A.TaxCode2, ");
            SQL.AppendLine("A.TaxCode3, B.TaxName, C.TaxName TaxName2, D.TaxName TaxName3, B.TaxRate, C.TaxRate TaxRate2, ");
            SQL.AppendLine("D.TaxRate TaxRate3, A.TaxAmt, A.TaxAmt2, A.TaxAmt3, (A.ARDownpaymentbefTax+A.TaxAmt+A.TaxAmt2+A.TaxAmt3) ARDownpaymentAfterTax ");
            SQL.AppendLine("FROM TblSalesInvoiceDtl4 A ");
            SQL.AppendLine("LEFT JOIN TblTax B ON A.TaxCode = B.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax C ON A.TaxCode2 = C.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax D ON A.TaxCode3 = D.TaxCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "ARDownpaymentDocNo", 
                    
                    //1-5
                    "ARDownpaymentBefTax", "TaxCode", "TaxName", "TaxRate", "TaxAmt",

                    //6-10
                    "TaxCode2", "TaxName2", "TaxRate2", "TaxAmt2", "TaxCode3", 

                    //11-15
                    "TaxName3", "TaxRate3", "TaxAmt3", "ARDownpaymentAfterTax", "DownpaymentBefTax"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);

                    ComputeDownpayment(Row);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 2, 5, 6, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowSalesInvoiceDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.ServiceDeliveryDocNo, B.DocDt, D.ItName, C.Qty, ");
            SQL.AppendLine("D.SalesUomCode Uom, B.CurCode, C.UPrice, B.DocNoInternal, C.Remark, ");
            SQL.AppendLine("A.TaxInd1, A.TaxInd2, A.TaxInd3, A.TaxAmtTotal, (C.Qty * C.UPrice) Amt, ");
            SQL.AppendLine("A.TaxAmt1, A.TaxAmt2, A.TaxAmt3, A.ServiceDeliveryDNo ");
            SQL.AppendLine("From TblSalesInvoiceDtl5 A ");
            SQL.AppendLine("Inner Join TblServiceDeliveryHdr B On A.ServiceDeliveryDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblServiceDeliveryDtl C On B.DocNo = C.DocNo And A.ServiceDeliveryDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "ServiceDeliveryDocNo", 
                    
                    //1-5
                    "DocDt", "ItName", "Qty", "Uom", "CurCode",

                    //6-10
                    "UPrice", "DocNoInternal", "Remark", "TaxInd1", "TaxInd2", 

                    //11-15
                    "TaxInd3", "TaxAmtTotal", "Amt", "TaxAmt1", "TaxAmt2", 

                    //16-17
                    "TaxAmt3", "ServiceDeliveryDNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5, 8, 14, 15, 16, 17, 18 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowSalesInvoiceHdr2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, ReceiptNo, DocDt, CancelReason, CancelInd, ");
            SQL.AppendLine("CtCode, DueDt, LocalDocNo, TaxInvDocument, ");
            SQL.AppendLine("CurCode, TotalAmt, TotalTax, DownPayment, Amt, ");
            SQL.AppendLine("BankAcCode, SalesName, TaxCode1, TaxCode2, TaxCode3, TaxInvDt, ");
            SQL.AppendLine("Remark, JournalDocNo, JournalDocNo2  ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "ReceiptNo", "DocDt", "CancelReason", "CtCode", "DueDt", 
                        
                        //6-10
                        "LocalDocNo", "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", 
                        
                        //11-14
                        "DownPayment",  "Amt", "BankAcCode", "SalesName", "TaxCode1", 
                        
                        //16-20
                        "TaxCode2", "TaxCode3", "TaxInvDt", "CancelInd", "Remark",

                        //21-22
                        "JournalDocNo", "JournalDocNo2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        
                        TxtReceiptNo.EditValue = Sm.DrStr(dr, c[1]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[5]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname  = '" + Sm.DrStr(dr, c[14]) + "'"));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[13]));
                        Sl.SetLueTaxCode(ref LueTaxCode1);
                        Sl.SetLueTaxCode(ref LueTaxCode2);
                        Sl.SetLueTaxCode(ref LueTaxCode3);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[17]));
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[18]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );

        }

        private void ShowSalesInvoiceDtl12(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Dno, B.Doctype, B.DoctDocno, B.DOCtDno, D.DocDt, B.ItCode, ");
            SQL.AppendLine("C.ItName, B.Qty QtyPackagingUnit, C.SalesuomCode As PackagingUnitUomCode, ");
            SQL.AppendLine("B.Qty, C.SalesUomCode As PriceUomCode, B.UpriceBeforeTax, ");
            SQL.AppendLine("B.TaxRate, B.TaxAmt, B.UPriceAfterTax, D.DocNoInternal ");
            SQL.AppendLine("From TblSalesInvoicehdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            //SQL.AppendLine("Inner Join TblDOCtHdr D On B.DOCtDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, T2.DocDt, T2.DocNoInternal ");
            SQL.AppendLine("    From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T1.DocNo, T1.DNo, T2.DocDt, T2.DocNoInternal ");
                SQL.AppendLine("    From TblSalesInvoiceDtl T1 ");
                SQL.AppendLine("    Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
            }
            SQL.AppendLine(") D On B.DocNo = D.DocNo And B.DNo = D.DNo ");
            SQL.AppendLine("Where B.DocType = '3' And A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "ItCode", "ItName", 
                    
                    //6-10
                    "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "UPriceBeforeTax", 
                    
                    //11-15
                    "TaxRate", "TaxAmt", "UPriceAfterTax", "DocType", "DocNoInternal"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 9);
                    Grd.Cells[Row, 22].Value = Sm.GetLue(LueCurCode);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 13);
                    if (mIsDOCtAmtRounded)
                        Grd.Cells[Row, 27].Value = decimal.Truncate(dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]));
                    else
                        Grd.Cells[Row, 27].Value = dr.GetDecimal(c[8]) * dr.GetDecimal(c[13]);
                    Grd.Cells[Row, 28].Value = 0m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 41, 42, 43, 44, 45, 46, 47, 48, 49 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl22(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm,
                        "select DNo, FileName, CreateBy, CreateDt from  TblSalesInvoiceFile Where DocNo=@DocNo Order By Dno",
                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd5, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd5, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        #endregion

        #region Additional Method

        private bool UpdateServiceDeliveryData(string DOCtDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<DOCtServiceDelivery>();
            int counts = 0;

            SQL.AppendLine("Select Distinct DocNo, DOCtDocNo ");
            SQL.AppendLine("From TblServiceDeliveryHdr ");
            SQL.AppendLine("Where DOCtDocNo Is Not Null ");
            SQL.AppendLine("And Find_In_Set(DOCtDocNo, @DOCtDocNo) ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And DocNo In (Select Distinct DocNo From TblServiceDeliveryDtl Where CancelInd = 'N'); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", DOCtDocNo);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DOCtDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOCtServiceDelivery()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DOCtDocNo = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                counts = l.Count();
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    foreach (var x in l.Where(w => w.DOCtDocNo == Sm.GetGrdStr(Grd1, i, 2)))
                    {
                        Grd1.Cells[i, 51].Value = x.DocNo;
                    }
                }
            }

            l.Clear();

            return counts > 0;
        }

        internal string SelectedServiceDelivery()
        {
            string data = string.Empty;
            SetGrd();

            for (int i = 0; i < Grd6.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd6, i, 1).Length > 0)
                {
                    if (data.Length > 0) data += ",";
                    data += string.Concat(Sm.GetGrdStr(Grd6, i, 1), Sm.GetGrdStr(Grd6, i, 19)); //SDDocNo, SDDNo
                }
            }

            return data;
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblsalesinvoicedtl4` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ARDownpaymentDocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ARDownpaymentBefTax` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `DownpaymentBefTax` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxCode` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxCode2` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxCode3` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxAmt` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxAmt2` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxAmt3` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `SLITaxAmt` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `SLITaxAmt2` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `SLITaxAmt3` DECIMAL(18, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `ARDownpaymentDocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `ARDownpaymentDocNo` (`ARDownpaymentDocNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `TaxCode` (`TaxCode`) USING BTREE, ");
            SQL.AppendLine("   INDEX `TaxCode2` (`TaxCode2`) USING BTREE, ");
            SQL.AppendLine("   INDEX `TaxCode3` (`TaxCode3`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblsalesinvoicedtl5` ( ");
            SQL.AppendLine("    `DocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ServiceDeliveryDocNo` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `ServiceDeliveryDNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxInd1` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxInd2` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxInd3` VARCHAR(1) NOT NULL DEFAULT 'N' COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `TaxAmt1` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxAmt2` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxAmt3` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `TaxAmtTotal` DECIMAL(12, 4) NOT NULL DEFAULT '0.0000', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(16) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`DocNo`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `DocNo` (`DocNo`, `DNo`, `ServiceDeliveryDocNo`, `ServiceDeliveryDNo`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COMMENT = 'save service delivery' ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");

            SQL.AppendLine("ALTER TABLE `tblservicedeliverydtl` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `ProcessInd` VARCHAR(1) NOT NULL DEFAULT 'O' AFTER `CancelReason`, ");
            SQL.AppendLine("    DROP INDEX IF EXISTS `DocNo`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `DocNo` (`DocNo`, `DNo`, `CancelInd`, `CreateDt`, `ProcessInd`) USING BTREE; ");

            SQL.AppendLine("ALTER TABLE `tblservicedeliveryhdr` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `ProcessInd` VARCHAR(1) NOT NULL DEFAULT 'F' AFTER `Status`, ");
            SQL.AppendLine("    DROP INDEX IF EXISTS `DocNo`, ");
            SQL.AppendLine("    ADD INDEX IF NOT EXISTS `DocNo` (`DocNo`, `DocDt`, `Status`, `CtCode`, `CreateDt`, `ProcessInd`) USING BTREE; ");

            Sm.ExecQuery(SQL.ToString());

        }

        internal void AddAdvanceCharge(string AdvanceChargeCode, string Lue, int LastRow)
        {
            decimal TotalAmt = decimal.Parse(TxtTotalAmt.Text);
            bool IsDataLueExisted = false;
            int DataLueExistedRow = 0;
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if(Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 12),Lue))
                {
                    IsDataLueExisted = true;
                    DataLueExistedRow = row;
                }
                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 12), Lue) && AdvanceChargeCode.Length == 0)
                {
                    Grd3.Rows.RemoveAt(Grd3.Rows[row].Index);
                }
            }
            if (IsDataLueExisted) LastRow = DataLueExistedRow;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                SQL.AppendLine("Select A.AdvanceChargeCode, A.LocalName, A.PercentageInd, A.Value, A.AcType, B.AcNo, B.AcDesc ");
                SQL.AppendLine("From TblAdvanceCharge A ");
                SQL.AppendLine("Inner Join TblCoa B On A.AcNo=B.AcNo ");
                SQL.AppendLine("Where A.AdvanceChargeCode=@AdvanceChargeCode; ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", AdvanceChargeCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AdvanceChargeCode", "LocalName", "PercentageInd", "Value", "AcType", "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 1, 5);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 2, 6);
                        if (Sm.DrStr(dr, c[2]) == "Y")
                        {
                            Grd3.Cells[Row + LastRow, 4].Value = Sm.DrStr(dr, c[4]) == "D" ? Sm.DrDec(dr, c[3]) * (TotalAmt / 100) : 0m;
                            Grd3.Cells[Row + LastRow, 6].Value = Sm.DrStr(dr, c[4]) == "C" ? Sm.DrDec(dr, c[3]) * (TotalAmt / 100) : 0m;
                        }
                        else
                        {
                            Grd3.Cells[Row + LastRow, 4].Value = Sm.DrStr(dr, c[4]) == "D" ? Sm.DrDec(dr, c[3]) : 0m;
                            Grd3.Cells[Row + LastRow, 6].Value = Sm.DrStr(dr, c[4]) == "C" ? Sm.DrDec(dr, c[3]) : 0m;
                        }
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 11, 1);

                        Grd3.Cells[Row + LastRow, 11].ReadOnly = iGBool.True;
                        Grd3.Cells[Row + LastRow, 11].BackColor = Color.FromArgb(224, 224, 224);
                        Grd3.Cells[Row + LastRow, 12].Value = Lue;

                        Sm.SetGrdBoolValueFalse(Grd3, Row + LastRow, new int[] { 3, 5 });

                        if (!IsDataLueExisted)
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
                            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                        }
                        Row += 1;
                    }
                }
                dr.Close();
            }
        }

        internal void AddCustomerAcNoAR(string Lue, int LastRow)
        {
            string CustomerCode = Sm.GetLue(LueCtCode);
            decimal CustomerAcNoARDAmt = 0m;
            decimal CustomerAcNoARCAmt = 0m;
            decimal CustomerAcNoARAmt = 0m;
            bool IsCustomerAcNoARExisted = false;
            bool IsAdvanceChargeNotExist = true;
            int CustomerAcNoARRow = 0;
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 12), "CustomerAcNoAR"))
                {
                    IsCustomerAcNoARExisted = true;
                    CustomerAcNoARRow = row;
                }
                if (Sm.GetGrdStr(Grd3, row, 12).Length > 0 && !Sm.CompareStr(Sm.GetGrdStr(Grd3, row, 12), "CustomerAcNoAR"))
                {
                    IsAdvanceChargeNotExist = false;
                    CustomerAcNoARDAmt += Sm.GetGrdDec(Grd3, row, 4);
                    CustomerAcNoARCAmt += Sm.GetGrdDec(Grd3, row, 6);
                }
            }
            CustomerAcNoARAmt = CustomerAcNoARDAmt - CustomerAcNoARCAmt;
            if (IsCustomerAcNoARExisted) LastRow = CustomerAcNoARRow;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                SQL.AppendLine("Select Null As AdvanceChargeCode, Null As LocalName, Null As PercentageInd,  ");
                SQL.AppendLine("0 As Value, Null As AcType, Concat(B.Parvalue,@CustomerCode) AS AcNo, A.AcDesc  ");
                SQL.AppendLine("From TblCoa A  ");
                SQL.AppendLine("Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.Parvalue Is Not Null  ");
                SQL.AppendLine("Where A.AcNo=Concat(B.Parvalue,@CustomerCode); ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CustomerCode", CustomerCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {"AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Sm.SetGrdNumValueZero(Grd3, Row + LastRow, new int[] { 3, 4 });
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 1, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 2, 1);
                        if (CustomerAcNoARAmt < 0)
                        {
                            Grd3.Cells[Row + LastRow, 4].Value = CustomerAcNoARAmt * -1;
                            Grd3.Cells[Row + LastRow, 6].Value = 0m;
                        }
                        else if (CustomerAcNoARAmt > 0)
                        {
                            Grd3.Cells[Row + LastRow, 4].Value = 0m;
                            Grd3.Cells[Row + LastRow, 6].Value = CustomerAcNoARAmt;
                        }
                        Grd3.Cells[Row + LastRow, 12].Value = "CustomerAcNoAR";

                        Sm.SetGrdBoolValueFalse(Grd3, Row + LastRow, new int[] { 3, 5 });

                        if (!IsCustomerAcNoARExisted)
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
                            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                        }
                        if (IsAdvanceChargeNotExist)
                            Grd3.Rows.RemoveAt(Grd3.Rows[Row + LastRow].Index);
                        Row += 1;
                    }
                }
                dr.Close();
            }
        }

        public static void SetLueAdvanceChargeCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AdvanceChargeCode As Col1, A.AdvanceChargeName As Col2 From TblAdvanceCharge A ");
            SQL.AppendLine("Inner Join TblParameter B ON B.Parcode = 'CustomerAcNoAR' ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("AND (B.Parvalue Is Not Null And A.Acno Not Like Concat(B.Parvalue, '%')); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedDO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private decimal ComputeAddAmt() 
        {
            decimal COAAmt = 0m, Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);
            
            //untuk perhitungan discount yg dicentang
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                string AcType = Sm.GetValue(
                    "Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd3, Row, 1) + "';");
                if (Sm.GetGrdBool(Grd3, Row, 3))
                {
                    if (AcType == "D")
                        COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                    else
                        COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Sm.GetGrdBool(Grd3, Row, 5))
                {
                    if (AcType == "C")
                        COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                    else
                        COAAmt -= Sm.GetGrdDec(Grd3, Row, 6);
                }
            }

            return Downpayment - COAAmt;
        }

        private void ComputeTaxPerItem()
        {
            decimal PriceBeforeTax = 0m, TaxRate1 = 0m, TaxRate2 = 0m, TaxRate3 = 0m, mTaxAmt1 = 0m, mTaxAmt2 = 0m, mTaxAmt3 = 0m;
            string Tax1 = string.Empty, Tax2 = string.Empty, Tax3 = string.Empty;

            // get tax code
            Tax1 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode1));
            Tax2 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode2));
            Tax3 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode3));

            // set tax rate
            TaxRate1 = IsLueEmpty(LueTaxCode1) ? 0m : decimal.Parse(Tax1) * 0.01m;
            TaxRate2 = IsLueEmpty(LueTaxCode2) ? 0m : decimal.Parse(Tax2) * 0.01m;
            TaxRate3 = IsLueEmpty(LueTaxCode3) ? 0m : decimal.Parse(Tax3) * 0.01m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                PriceBeforeTax = Sm.GetGrdDec(Grd1, Row, 20) * Sm.GetGrdDec(Grd1, Row, 23);

                mTaxAmt1 = PriceBeforeTax * TaxRate1;
                mTaxAmt2 = PriceBeforeTax * TaxRate2;
                mTaxAmt3 = PriceBeforeTax * TaxRate3;

                Grd1.Cells[Row, 48].Value = mTaxAmt1 + mTaxAmt2 + mTaxAmt3;
                Grd1.Cells[Row, 49].Value = PriceBeforeTax + Sm.GetGrdDec(Grd1, Row, 48);
            }
        }

        private void ComputeAmtJournal()
        {
            ComputeTaxPerItem();
            decimal AdditionalAmtCost = 0m, Amount = 0m;
            AdditionalAmtCost = ComputeAddAmt();  

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Amount = Sm.GetGrdDec(Grd1, Row, 49);

                if (AdditionalAmtCost < 0)
                {
                    if (Amount - AdditionalAmtCost < 0)
                    {
                        Grd1.Cells[Row, 46].Value = Amount * -1;
                    }
                    else
                    {
                        Grd1.Cells[Row, 46].Value = AdditionalAmtCost;
                    }

                    Grd1.Cells[Row, 47].Value = Amount - AdditionalAmtCost; 
                    AdditionalAmtCost = AdditionalAmtCost - Sm.GetGrdDec(Grd1, Row, 46);
                }
                else
                {
                    Grd1.Cells[Row, 46].Value = AdditionalAmtCost;
                    Grd1.Cells[Row, 47].Value = Amount - AdditionalAmtCost;
                    AdditionalAmtCost = 0;
                }
            }
        }
        
        private void ReloadCustomer()
        {
            if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length > 0)
                DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
            else
                DteDueDt.EditValue = null;
            ClearGrd();
            ComputeAmtFromTax();
            TxtCtCtCode.EditValue = null;
            var CtCode = Sm.GetLue(LueCtCode);
            if (CtCode.Length > 0)
            {
                TxtCtCtCode.EditValue = Sm.GetValue(
                    "Select B.CtCtName From TblCustomer A, TblCustomerCategory B " +
                    "Where A.CtCtCode=B.CtCtCode And A.CtCode=@Param Limit 1;",
                    CtCode);
                ShowCustomerDepositSummary(CtCode);
            }
        }

        private string GetCOASales()
        {
            string AcNo = string.Empty;
            AcNo = Sm.GetValue("Select AcNo4 From TblItemCategory Where ItCtCode In (Select ItCtCode From TblItem Where ItCode = @Param) ", Sm.GetGrdStr(Grd1, 0, 15));

            return AcNo;
        }

        private string GetCOAAR()
        {
            string AcNo = string.Empty;

            AcNo = Sm.GetValue("Select AcNo10 From TblItemCategory Where ItCtCode In (Select ItCtCode From TblItem Where ItCode = @Param) ", Sm.GetGrdStr(Grd1, 0, 15));

            return AcNo;
        }

        private string GetCOAARUninvoiced()
        {
            string AcNo = string.Empty;
            AcNo = Sm.GetValue("Select AcNo11 From TblItemCategory Where ItCtCode In (Select ItCtCode From TblItem Where ItCode = @Param) ", Sm.GetGrdStr(Grd1, 0, 15));

            return AcNo;
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetGrdStr(Grd1, 0, 2);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select C.ProfitCenterCode From TblDOCtHdr A, TblWarehouse B, TblCostCenter C " +
                    "Where A.WhsCode=B.WhsCode And B.CCCode=C.CCCode And C.ProfitCenterCode Is Not Null And A.DocNo=@Param;",
                    Value);
        }

        private MySqlCommand SaveExcRate(ref List<Rate> lR, int i, string DocNo, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=(Case @CancelInd When 'Y' Then -1 Else 1 End * " + x + "); ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            SQL1.AppendLine("Insert Into TblSalesInvoiceDtl3(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @DocNo, @CurCode, @ExcRate, @DP, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt-@DP, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand UpdateSummary2(ref List<Rate> lR, int i)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt + @Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<Decimal>(ref cm1, "@Amt", lR[i].Amt);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }


        internal void ComputeAmtFromTax()
        {
            ComputeAmt();
            //ComputeTotalWithTax(); // remark by wed. kalkulasi tax masuk ke ComputeAmt() dengan method baru ComputeTax(Amt), dimiripkan dengan SalesInvoice ComputeAmt()
        }

        //private void ComputeTotalWithTax() // remark by wed. kalkulasi tax masuk ke ComputeAmt() dengan method baru ComputeTax(Amt), dimiripkan dengan SalesInvoice ComputeAmt()
        //{
        //    decimal TotalWithoutTax = 0m, DownPayment = 0m, TaxAmt1 = 0m, TaxAmt2 = 0m, TaxAmt3 = 0m, TotalTaxAmt1 = 0m;

        //    Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtTotalTax }, 0);

        //    if (TxtTotalAmt.Text.Length != 0) TotalWithoutTax = decimal.Parse(TxtTotalAmt.Text);

        //    if (Sm.GetLue(LueTaxCode1).Length != 0)
        //    {
        //        TaxAmt1 = GetTaxRate(Sm.GetLue(LueTaxCode1)) * 0.01m * TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt1 = decimal.Truncate(TaxAmt1);
        //        if (TaxAmt1 > 0)
        //        {
        //            //TxtTaxAmt1.EditValue = Sm.FormatNum(mSITaxRoundingDown == true ? Math.Floor(TaxAmt1) : TaxAmt1, 0);
        //            //TotalTaxAmt1 = mSITaxRoundingDown == true ? Math.Floor(TaxAmt1) : TaxAmt1;

        //            TxtTaxAmt1.EditValue = Sm.FormatNum(TaxAmt1, 0);
        //            TotalTaxAmt1 = TaxAmt1;

        //        }
        //        else
        //        {
        //            TaxAmt1 = -1 * TaxAmt1;
        //            TxtTaxAmt1.EditValue = Sm.FormatNum(-1 * TaxAmt1, 0);
        //            TotalTaxAmt1 = -1 * TaxAmt1;
        //        }
        //    }

        //    if (Sm.GetLue(LueTaxCode2).Length != 0)
        //    {
        //        TaxAmt2 = GetTaxRate(Sm.GetLue(LueTaxCode2))*0.01m*TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt2 = decimal.Truncate(TaxAmt2);
        //        if (TaxAmt2 > 0)
        //        {
        //            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
        //            TotalTaxAmt1 += TaxAmt2;
        //        }
        //        else
        //        {
        //            TaxAmt2 = -1 * TaxAmt2;
        //            TxtTaxAmt2.EditValue = Sm.FormatNum(-1*TaxAmt2, 0);
        //            TotalTaxAmt1 += (-1 * TaxAmt2);
        //        }
        //    }

        //    if (Sm.GetLue(LueTaxCode3).Length != 0)
        //    {
        //        TaxAmt3 = GetTaxRate(Sm.GetLue(LueTaxCode3))*0.01m*TotalWithoutTax;
        //        if (mIsDOCtAmtRounded) TaxAmt3 = decimal.Truncate(TaxAmt3);
        //        if (TaxAmt3 > 0)
        //        {
        //            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
        //            TotalTaxAmt1 += TaxAmt3;
        //        }
        //        else
        //        {
        //            TaxAmt3 = -1 * TaxAmt3;
        //            TxtTaxAmt3.EditValue = Sm.FormatNum(-1 * TaxAmt3, 0);
        //            TotalTaxAmt1 += (-1 * TaxAmt3);
        //        }
        //    }
        //    if (mIsDOCtAmtRounded) TotalTaxAmt1 = decimal.Truncate(TotalTaxAmt1);
        //    TxtTotalTax.EditValue = Sm.FormatNum(TotalTaxAmt1, 0);
            
        //    if (TxtDownpayment.Text.Length != 0) DownPayment = decimal.Parse(TxtDownpayment.Text);
        //    if (mIsDOCtAmtRounded) DownPayment = decimal.Truncate(DownPayment);

        //    if (mIsDOCtAmtRounded)
        //        TxtAmt.EditValue = Sm.FormatNum(decimal.Truncate(TotalWithoutTax + TotalTaxAmt1 - DownPayment), 0);
        //    else
        //        TxtAmt.EditValue = Sm.FormatNum(TotalWithoutTax + TotalTaxAmt1 - DownPayment, 0);

        //    if (mSLI3TaxCalculationFormat != "1") // SRN
        //    {
        //        if (mIsDOCtAmtRounded)
        //            TxtAmt.EditValue = Sm.FormatNum(Decimal.Truncate((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt)), 0);
        //        else
        //            TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
        //    }
        //}

        internal void ComputeTaxPerDetail(bool IsFromDetail, int Row, string GrdName, ref iGrid Grd)
        {
            if (IsFromDetail) // perubahan dari detail yg di tick/untick
            {
                ComputeTaxPerDetail2(Row, GrdName, ref Grd);
            }
            else
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    ComputeTaxPerDetail2(i, "Grd1", ref Grd1);
                }

                //if (mIsUseServiceDelivery)
                //{
                //    for (int i = 0; i < Grd6.Rows.Count; ++i)
                //    {
                //        ComputeTaxPerDetail2(i, "Grd6", ref Grd6);
                //    }
                //}
            }

            ComputeAmt();
        }

        private void ComputeTaxPerDetail2(int Row, string GrdName, ref iGrid Grd)
        {
            int qtyCols = 20, uPriceBefTaxCols = 23, taxAmt1Cols = 43, taxAmt2Cols = 44, taxAmt3Cols = 45,
                taxCode1Cols = 38, taxCode2Cols = 39, taxCode3Cols = 40, taxAmtCols = 41;
            if (GrdName == "Grd6")
            {
                qtyCols = 5;
                uPriceBefTaxCols = 8;
                taxAmt1Cols = 16;
                taxAmt2Cols = 17;
                taxAmt3Cols = 18;
                taxCode1Cols = 11;
                taxCode2Cols = 12;
                taxCode3Cols = 13;
                taxAmtCols = 14;
            }
            decimal TaxAmt = 0m;
            decimal Qty = Sm.GetGrdDec(Grd, Row, qtyCols);
            decimal UPriceBeftax = Sm.GetGrdDec(Grd, Row, uPriceBefTaxCols);

            Grd.Cells[Row, taxAmt1Cols].Value = 0m;
            Grd.Cells[Row, taxAmt2Cols].Value = 0m;
            Grd.Cells[Row, taxAmt3Cols].Value = 0m;

            if (Sm.GetGrdBool(Grd, Row, taxCode1Cols) && Sm.GetLue(LueTaxCode1).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode1)) * 0.01m;
                TaxAmt += Amt;
                Grd.Cells[Row, taxAmt1Cols].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd, Row, taxCode2Cols) && Sm.GetLue(LueTaxCode2).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode2)) * 0.01m;
                TaxAmt += Amt;
                Grd.Cells[Row, taxAmt2Cols].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd, Row, taxCode3Cols) && Sm.GetLue(LueTaxCode3).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode3)) * 0.01m;
                TaxAmt += Amt;
                Grd.Cells[Row, taxAmt3Cols].Value = Amt;
            }

            Grd.Cells[Row, taxAmtCols].Value = Sm.FormatNum(TaxAmt, 0);
        }

        private void ComputeTax(decimal Amt)
        {
            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);
            decimal
                TaxAmt1 = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3 }, 0);

            if (mSalesInvoiceTaxCalculationFormula == "1")
            {
                if (TaxCode1.Length != 0) TaxAmt1 = GetTaxRate(TaxCode1) * 0.01m * Amt;
                if (TaxCode2.Length != 0) TaxAmt2 = GetTaxRate(TaxCode2) * 0.01m * Amt;
                if (TaxCode3.Length != 0) TaxAmt3 = GetTaxRate(TaxCode3) * 0.01m * Amt;
            }
            else
            {
                if (TaxCode1.Length != 0) TaxAmt1 = GetTaxAmtPerDetail(1);
                if (TaxCode2.Length != 0) TaxAmt2 = GetTaxAmtPerDetail(2);
                if (TaxCode3.Length != 0) TaxAmt3 = GetTaxAmtPerDetail(3);
            }

            if (mIsDOCtAmtRounded)
            {
                TaxAmt1 = Decimal.Truncate(TaxAmt1);
                TaxAmt2 = Decimal.Truncate(TaxAmt2);
                TaxAmt3 = Decimal.Truncate(TaxAmt3);
            }
            
            TxtTaxAmt1.Text = Sm.FormatNum(mSITaxRoundingDown == true ? Sm.RoundDown(TaxAmt1,0) : TaxAmt1, 0);
            TxtTaxAmt2.Text = Sm.FormatNum(mSITaxRoundingDown == true ? Sm.RoundDown(TaxAmt2,0) : TaxAmt2, 0);
            TxtTaxAmt3.Text = Sm.FormatNum(mSITaxRoundingDown == true ? Sm.RoundDown(TaxAmt3,0) : TaxAmt3, 0);
        }

        private bool IsLueEmpty(LookUpEdit Lue)
        {
            bool Result = true;
            if (Lue.EditValue == null ||
                Lue.Text.Trim().Length == 0 ||
                Lue.EditValue.ToString().Trim().Length == 0)
            {
                Result = true;
            }
            else
                Result = false;
            return Result;
        }

        private decimal GetTaxAmtPerDetail(byte TaxNo)
        {
            decimal TaxAmt = 0m;
            int TaxCol = 0;

            if (TaxNo == 1) TaxCol = 38;
            if (TaxNo == 2) TaxCol = 39;
            if (TaxNo == 3) TaxCol = 40;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                //decimal UPriceBefTax = Sm.GetGrdDec(Grd1, i, 23);
                //decimal Qty = Sm.GetGrdDec(Grd1, i, 20);

                //if (Sm.GetGrdBool(Grd1, i, TaxCol))
                //    TaxAmt += (Qty * UPriceBefTax) * TaxRate * 0.01m;

                if (Sm.GetGrdBool(Grd1, i, TaxCol))
                    TaxAmt += Sm.GetGrdDec(Grd1, i, (TaxCol + 5)); // hitungnya ini ada di ComputeTaxPerDetail2
            }

            //if (mIsUseServiceDelivery)
            //{
            //    if (TaxNo == 1) TaxCol = 11;
            //    if (TaxNo == 2) TaxCol = 12;
            //    if (TaxNo == 3) TaxCol = 13;

            //    for (int i = 0; i < Grd6.Rows.Count; ++i)
            //    {
            //        if (Sm.GetGrdBool(Grd6, i, TaxCol))
            //            TaxAmt += Sm.GetGrdDec(Grd6, i, (TaxCol + 5));
            //    }
            //}

            return TaxAmt;
        }

        internal void ComputeAmt()
        {
            decimal COAAmt = 0m, Amt = 0m, TotalTax = 0m, Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (mSLI3TaxCalculationFormat == "1")
            {
                #region Default
                var CustomerAcNoAR =
                Sm.GetValue(
                        "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
                        "From TblCustomer A, TblCustomerCategory B " +
                        "Where A.CtCtCode is Not Null " +
                        "And A.CtCtCode=B.CtCtCode " +
                        "And A.CtCode='" + Sm.GetLue(LueCtCode) + "' Limit 1; "
                        );
                //if (CustomerAcNoAR.Length > 0)
                //{
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    //if (Sm.CompareStr(CustomerAcNoAR, Sm.GetGrdStr(Grd3, Row, 1)))
                    //{
                    string AcType = Sm.GetValue(
                        "Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd3, Row, 1) + "';");
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 6);
                    }
                }
                #endregion
            }
            else //SRN, pilihan additional cost & discount nya ada yg masuk hitungan atau enggak
            {
                #region SRN

                mAdditionalCostDiscAmt = 0m;
                string CtCode = Sm.GetLue(LueCtCode);

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    string AcNo = Sm.GetGrdStr(Grd3, Row, 1);
                    string COAAR = Sm.GetValue("Select AcNo10 From TblItemCategory Where ItCtCode In (Select ItCtCode From TblItem Where ItCode = @Param); ", Sm.GetGrdStr(Grd1, 0, 15));
                    if (mIsItemCategoryUseCOAAPAR)
                    {
                        if (AcNo != COAAR) // kalau bukan COA AR
                        {
                            //kalau dicentang di debit, (-)
                            if (Sm.GetGrdBool(Grd3, Row, 3))
                            {
                                COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            }
                            //kalau dicentang di kredit, (+)
                            if (Sm.GetGrdBool(Grd3, Row, 5))
                            {
                                COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                            }

                            //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                            if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                            {
                                mAdditionalCostDiscAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                                mAdditionalCostDiscAmt += Sm.GetGrdDec(Grd3, Row, 6);
                            }
                        }
                    }
                    else
                    {
                        if (!AcNo.Contains(string.Concat(".", CtCode))) // kalau bukan COA customer
                        {
                            //kalau dicentang di debit, (-)
                            if (Sm.GetGrdBool(Grd3, Row, 3))
                            {
                                COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            }
                            //kalau dicentang di kredit, (+)
                            if (Sm.GetGrdBool(Grd3, Row, 5))
                            {
                                COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                            }

                            //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                            if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                            {
                                mAdditionalCostDiscAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                                mAdditionalCostDiscAmt += Sm.GetGrdDec(Grd3, Row, 6);
                            }
                        }

                    }
                }

                #endregion
            }

            Amt = COAAmt;

            if (mIsSalesInvoice3TaxEnabled)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                    decimal UPriceBefTax = Sm.GetGrdDec(Grd1, Row, 23);

                    Amt += (Qty * UPriceBefTax);
                }

                if (mSLI3TaxCalculationFormat == "1")
                {
                    if (mIsSalesInvoice3COANonTaxable)
                        ComputeTax(Amt - COAAmt);
                    else
                        ComputeTax(Amt);
                }
                else
                    ComputeTax(Amt);

                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }
            else
            {                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 27).Length > 0)
                    {
                        decimal Amount = Sm.GetGrdDec(Grd1, Row, 27);
                        Amt += Amount;
                    }
                }

                //if (mIsUseServiceDelivery)
                //{
                //    for (int Row = 0; Row < Grd6.Rows.Count; ++Row)
                //    {
                //        if (Sm.GetGrdStr(Grd6, Row, 15).Length > 0)
                //        {
                //            decimal Amount = Sm.GetGrdDec(Grd6, Row, 15);
                //            Amt += Amount;
                //        }
                //    }
                //}

                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                //    {
                //        decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                //        decimal TaxAmt = Sm.GetGrdDec(Grd1, Row, 25);
                //        TotalTax += (Qty * TaxAmt);
                //    }
                //}
                ComputeTax(Amt);
                TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);

                if (mIsDOCtAmtRounded)
                {
                    Amt = decimal.Truncate(Amt);
                    TotalTax = decimal.Truncate(TotalTax);
                    Downpayment = decimal.Truncate(Downpayment);
                }
                TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
            }

            if (mSLI3TaxCalculationFormat != "1")
            {
                // ditambahkan nilai additional cost discount yg ga dicentang
                TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
            }
        }

        internal decimal ComputeTotalAmt()
        {
            decimal TotalAmt = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                TotalAmt += Sm.GetGrdDec(Grd1, Row, 27);
            }
            return TotalAmt;

        }

        internal void ComputeDownpayment(int Row)
        {
            decimal
                Downpayment = 0m,
                RemainingAmt = 0m;

            //Processing SLI
            Downpayment = Sm.GetGrdDec(Grd4, Row, 16);
            Grd4.Cells[Row, 17].Value = Downpayment * Sm.GetGrdDec(Grd4, Row, 5) / 100;
            Grd4.Cells[Row, 18].Value = Downpayment * Sm.GetGrdDec(Grd4, Row, 9) / 100;
            Grd4.Cells[Row, 19].Value = Downpayment * Sm.GetGrdDec(Grd4, Row, 13) / 100;
            Grd4.Cells[Row, 20].Value = Downpayment + Sm.GetGrdDec(Grd4, Row, 17) + Sm.GetGrdDec(Grd4, Row, 18) + Sm.GetGrdDec(Grd4, Row, 19);

            //Remaining
            RemainingAmt = Sm.GetGrdDec(Grd4, Row, 2) - Sm.GetGrdDec(Grd4, Row, 16);
            Grd4.Cells[Row, 21].Value = RemainingAmt;
            Grd4.Cells[Row, 22].Value = RemainingAmt * Sm.GetGrdDec(Grd4, Row, 5) / 100;
            Grd4.Cells[Row, 23].Value = RemainingAmt * Sm.GetGrdDec(Grd4, Row, 9) / 100;
            Grd4.Cells[Row, 24].Value = RemainingAmt * Sm.GetGrdDec(Grd4, Row, 13) / 100;
            Grd4.Cells[Row, 25].Value = RemainingAmt + Sm.GetGrdDec(Grd4, Row, 22) + Sm.GetGrdDec(Grd4, Row, 23) + Sm.GetGrdDec(Grd4, Row, 24);

            ComputeDownpayment();
        }

        internal void ComputeDownpayment()
        {
            if (!mIsSalesInvoiceUseTabToInputDownpayment) return;
            decimal TotalDownPaymentBefTax = 0m,
                TotalDownpaymenAfterTax = 0,
                TotalDownpaymentTax = 0,
                TotalWithoutTax = 0,
                TotalDownpayment = 0;

            if (Grd4.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0 && Sm.GetGrdDec(Grd4, Row, 16) <= Sm.GetGrdDec(Grd4, Row, 2))
                    {
                        TotalDownPaymentBefTax += Sm.GetGrdDec(Grd4, Row, 16);
                        TotalDownpaymenAfterTax += Sm.GetGrdDec(Grd4, Row, 20);
                        TotalDownpaymentTax += (Sm.GetGrdDec(Grd4, Row, 17) + Sm.GetGrdDec(Grd4, Row, 18) + Sm.GetGrdDec(Grd4, Row, 19));
                    }
                }
            }

            TotalWithoutTax = decimal.Parse(TxtTotalAmt.Text);
            TotalDownpayment = decimal.Parse(TxtDownpayment.Text);


            TxtDownpayment.EditValue = Sm.FormatNum(TotalDownPaymentBefTax, 0);
            TxtDownpaymentTaxAmt.EditValue = Sm.FormatNum(TotalDownpaymenAfterTax, 0);
            TxtDPAmtBefTax.EditValue = Sm.FormatNum(TotalDownPaymentBefTax, 0);
            TxtDPTaxAmt.EditValue = Sm.FormatNum(TotalDownpaymentTax, 0);
            TxtInvoiceBeforeTaxAmt.EditValue = Sm.FormatNum(TotalWithoutTax - TotalDownpayment,0);
            TxtTaxForInvoice.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalTax.Text) - decimal.Parse(TxtDownpaymentTaxAmt.Text), 0);
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode;"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSalesInvoice3JournalUseItCtSalesEnabled', 'MainCurCode', 'IsAutoJournalActived', 'IsItCtFilteredByGroup', 'IsShowCustomerCategory', ");
            SQL.AppendLine("'IsEntityMandatory', 'EmpCodeSI', 'IsSalesInvoice3UseDepartment', 'IsForeignCurrencyExchangeUseExpense', 'IsWarehouseFilteredByGroup', ");
            SQL.AppendLine("'TaxCodeSIValue', 'EmpCodeTaxCollector', 'SalesBasedDOQtyIndex', 'DocNoFormat', 'SLI3TaxCalculationFormat', ");
            SQL.AppendLine("'SLIJournalIncomingAcNoSource', 'FormPrintOutInvoice3', 'FormPrintOutInvoice', 'FormPrintOutInvoice3Receipt', 'DueDtSIValue', ");
            SQL.AppendLine("'SITaxRoundingDown', 'IsSalesInvoiceTaxInvDocEditable', 'SalesInvoice3NonIDRJournalQtySource', 'SalesInvoiceTaxCalculationFormula', 'SalesInvoice3JournalFormula', ");
            SQL.AppendLine("'IsDOCtAllowMultipleItemCategory', 'IsDOCtShowPackagingUnit', 'IsAPARUseType', 'IsSalesInvoice3NotAutoChooseSalesPerson', 'IsSLI3JournalUseCCCode', ");
            SQL.AppendLine("'IsSalesInvoice3UseNotesBasedOnCategory', 'IsCustomerComboBasedOnCategory', 'IsItemCategoryUseCOAAPAR', 'IsBOMShowSpecifications', 'IsCheckCOAJournalNotExists', ");
            SQL.AppendLine("'IsFilterByCtCt', 'IsCustomerComboShowCategory', 'IsSalesInvoice3TaxInvDocumentAbleToEdit', 'IsSalesInvoice3COANonTaxable', 'IsSalesInvoice3TaxEnabled', ");
            SQL.AppendLine("'IsSLI3UseAutoFacturNumber', 'IsRemarkForJournalMandatory', 'IsCustomerItemNameMandatory', 'IsDOCtAmtRounded', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsSLIBasedOnDoShowForeignName', ");
            SQL.AppendLine("'IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled', 'IsSLI3UseAdvanceCharge', 'IsSLI3UseInsidentilInformation', 'IsTaxInvoiceDateSameWithDocDate', 'SLIBasedOnDoDefaultTax', ");
            SQL.AppendLine("'IsFilterByDept', 'IsGroupCOAActived', 'IsJournalSalesInvoiceUseItemsCategory', 'IsSalesInvoiceUseTabToInputDownpayment', 'JournalDocNoFormat', 'IsSLIBasedOnDOToCustomerAllowtoUploadFile', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient','PortForFTPClient','PasswordForFTPClient','UsernameForFTPClient','SharedFolderForFTPClient','HostAddrForFTPClient', 'IsSLIBasedOnDOToCustomerUploadFileMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsRemarkForJournalMandatory": mIsRemarkForJournalMandatory = ParValue == "Y"; break;
                            case "IsSLI3UseAutoFacturNumber": mIsSLI3UseAutoFacturNumber = ParValue == "Y"; break;
                            case "IsSalesInvoice3TaxEnabled": mIsSalesInvoice3TaxEnabled = ParValue == "Y"; break;
                            case "IsSalesInvoice3COANonTaxable": mIsSalesInvoice3COANonTaxable = ParValue == "Y"; break;
                            case "IsSalesInvoice3TaxInvDocumentAbleToEdit": mIsSalesInvoice3TaxInvDocumentAbleToEdit = ParValue == "Y"; break;
                            case "IsCustomerComboShowCategory": mIsCustomerComboShowCategory = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsCustomerComboBasedOnCategory": mIsCustomerComboBasedOnCategory = ParValue == "Y"; break;
                            case "IsSalesInvoice3UseNotesBasedOnCategory": mIsSalesInvoice3UseNotesBasedOnCategory = ParValue == "Y"; break;
                            case "IsSLI3JournalUseCCCode": mIsSLI3JournalUseCCCode = ParValue == "Y"; break;
                            case "IsSalesInvoice3NotAutoChooseSalesPerson": mIsSalesInvoice3NotAutoChooseSalesPerson = ParValue == "Y"; break;
                            case "IsAPARUseType": mIsAPARUseType = ParValue == "Y"; break;
                            case "IsDOCtShowPackagingUnit": mIsDOCtShowPackagingUnit = ParValue == "Y"; break;
                            case "IsDOCtAllowMultipleItemCategory": mIsDOCtAllowMultipleItemCategory = ParValue == "Y"; break;
                            case "IsSalesInvoiceTaxInvDocEditable": mIsSalesInvoiceTaxInvDocEditable = ParValue == "Y"; break;
                            case "SITaxRoundingDown": mSITaxRoundingDown = ParValue == "Y"; break;
                            case "IsSalesInvoice3JournalUseItCtSalesEnabled": mIsSalesInvoice3JournalUseItCtSalesEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsItCtFilteredByGroup": mIsItCtFilteredByGroup = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsSalesInvoice3UseDepartment": mIsSalesInvoice3UseDepartment = ParValue == "Y"; break;
                            case "IsForeignCurrencyExchangeUseExpense": mIsForeignCurrencyExchangeUseExpense = ParValue == "Y"; break;
                            case "IsWarehouseFilteredByGroup": mIsWarehouseFilteredByGroup = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled": mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = ParValue == "Y"; break;
                            case "IsSLI3UseAdvanceCharge": mIsSLI3UseAdvanceCharge = ParValue == "Y"; break;
                            case "IsSLIBasedOnDoShowForeignName": mIsSLIBasedOnDoShowForeignName = ParValue == "Y"; break;
                            case "IsSLI3UseInsidentilInformation": mIsSLI3UseInsidentilInformation = ParValue == "Y"; break;
                            case "IsTaxInvoiceDateSameWithDocDate": mIsTaxInvoiceDateSameWithDocDate = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsGroupCOAActived": mIsGroupCOAActived = ParValue == "Y"; break;
                            case "IsJournalSalesInvoiceUseItemsCategory": mIsJournalSalesInvoiceUseItemsCategory = ParValue == "Y"; break;
                            case "IsSalesInvoiceUseTabToInputDownpayment": mIsSalesInvoiceUseTabToInputDownpayment = ParValue == "Y"; break;
                            case "IsSLIBasedOnDOToCustomerAllowtoUploadFile": mIsSLIBasedOnDOToCustomerAllowtoUploadFile = ParValue == "Y"; break;
                            case "IsSLIBasedOnDOToCustomerUploadFileMandatory": mIsSLIBasedOnDOToCustomerUploadFileMandatory = ParValue == "Y"; break;

                            //string
                            case "SalesInvoice3JournalFormula": mSalesInvoice3JournalFormula = ParValue; break;
                            case "SalesInvoiceTaxCalculationFormula": mSalesInvoiceTaxCalculationFormula = ParValue; break;
                            case "SalesInvoice3NonIDRJournalQtySource": mSalesInvoice3NonIDRJournalQtySource = ParValue; break;
                            case "DueDtSIValue": mDueDtSIValue = ParValue; break;
                            case "FormPrintOutInvoice3Receipt": mFormPrintOutInvoice3Receipt = ParValue; break;
                            case "FormPrintOutInvoice": mIsFormPrintOutInvoice = ParValue; break;
                            case "FormPrintOutInvoice3": mFormPrintOutInvoice3 = ParValue; break;
                            case "SLIJournalIncomingAcNoSource": mSLIJournalIncomingAcNoSource = ParValue; break;
                            case "SLI3TaxCalculationFormat": mSLI3TaxCalculationFormat = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "SalesBasedDOQtyIndex": mSalesBasedDOQtyIndex = ParValue; break;
                            case "EmpCodeTaxCollector": mEmpCodeTaxCollector = ParValue; break;
                            case "TaxCodeSIValue": mTaxCodeSIValue = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "EmpCodeSI": mEmpCodeSI = ParValue; break;
                            case "SLIBasedOnDoDefaultTax": mSLIBasedOnDoDefaultTax = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;

                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mSalesInvoice3JournalFormula.Length == 0) mSalesInvoice3JournalFormula = "1";
            if (mSalesInvoiceTaxCalculationFormula.Length == 0) mSalesInvoiceTaxCalculationFormula = "1";
            if (mSLIJournalIncomingAcNoSource.Length == 0) mSLIJournalIncomingAcNoSource = "1";
            if (mSLI3TaxCalculationFormat.Length == 0) mSLI3TaxCalculationFormat = "1";
            if (mSalesBasedDOQtyIndex.Length <= 0) mSalesBasedDOQtyIndex = "2";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.CtCtCode As Col1, T1.CtCtName As Col2 ");
            SQL.AppendLine("From TblCustomerCategory T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCtCode = T2.CtCtCode ");
            SQL.AppendLine("    And T2.CtCode In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct A.CtCode ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo AND A.ProcessInd ='F' And B.Qty>0 ");
            SQL.AppendLine("        And A.DocNo Not In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct X2.DOCtDocNo ");
            SQL.AppendLine("            From TblSalesInvoiceHdr X1 ");
            SQL.AppendLine("            Inner Join TblSalesInvoiceDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("                And X1.CancelInd = 'N' ");
            SQL.AppendLine("        ) ");
            if (mIsUseServiceDelivery)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct A.CtCode ");
                SQL.AppendLine("        From TblServiceDeliveryHdr A ");
                SQL.AppendLine("        Inner Join TblServiceDeliveryDtl B On A.DocNo=B.DocNo AND A.ProcessInd ='F' And B.Qty>0 ");
                SQL.AppendLine("        And A.DocNo Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct X2.DOCtDocNo ");
                SQL.AppendLine("            From TblSalesInvoiceHdr X1 ");
                SQL.AppendLine("            Inner Join TblSalesInvoiceDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("                And X1.CancelInd = 'N' ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By T1.CtCtName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCtCodeBasedOnCategory(ref LookUpEdit Lue, string Code, string IsFilterByCtCt, string CtCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.CtCode As Col1, ");

            if (mIsCustomerComboShowCategory)
                SQL.AppendLine("Concat(T.CtName, ' [', T1.CtCtName, ']') As Col2 ");
            else
                SQL.AppendLine("T.CtName As Col2 ");

            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Left Join TblCustomerCategory T1 On T.CtCtCode = T1.CtCtCode ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CtCode=@Code ");
            else
            {
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("And T.CtCtCode = @CtCtCode ");
                if (IsFilterByCtCt == "Y")
                {
                    SQL.AppendLine("And (T.CtCtCode Is Null Or (T.CtCtCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                    SQL.AppendLine("    Where CtCtCode=T.CtCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("And T.CtCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct A.CtCode ");
                SQL.AppendLine("    From TblDOCtHdr A ");
                SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.Qty>0 And A.ProcessInd = 'F' ");
                SQL.AppendLine("    And A.DocNo Not In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct X2.DOCtDocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr X1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("            And X1.CancelInd = 'N' ");
                SQL.AppendLine("    ) ");
                if (mIsUseServiceDelivery)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Distinct A.CtCode ");
                    SQL.AppendLine("    From TblServiceDeliveryHdr A ");
                    SQL.AppendLine("    Inner Join TblServiceDeliveryDtl B On A.DocNo=B.DocNo And B.Qty>0 And A.ProcessInd = 'F' ");
                    SQL.AppendLine("    And A.DocNo Not In ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select Distinct X2.DOCtDocNo ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr X1 ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("            And X1.CancelInd = 'N' ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.CtName ");
            }

            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@CtCtCode", CtCtCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select Distinct A.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(C.Ctname, ' [',D.CtCtName,']') As Col2 ");
                    else
                        SQL.AppendLine("C.Ctname As Col2 ");

                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("    Select T1.DocNo, T1.DNo, T1.CtCode ");
                    SQL.AppendLine("    From TblDOCtHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        And T2.Qty > 0 And T1.ProcessInd = 'F' ");
                    if (mIsUseServiceDelivery)
                    {
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("    Select T1.DocNo, T1.DNo, T1.CtCode ");
                        SQL.AppendLine("    From TblServiceDeliveryHdr T1 ");
                        SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
                        SQL.AppendLine("        And T2.Qty > 0 And T1.ProcessInd = 'F' ");
                    }
                    SQL.AppendLine(") A ");
                    SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode And C.ActInd='Y' ");
                    if (mIsFilterByCtCt)
                    {
                        SQL.AppendLine("And (C.CtCtCode Is Null Or (C.CtCtCode Is Not Null And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                        SQL.AppendLine("    Where CtCtCode=C.CtCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ))) ");
                    }
                    SQL.AppendLine("Inner Join TblCustomerCategory D On C.CtCtCode = D.CtCtCode ");
                    SQL.AppendLine("Where Not Exists( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblSalesInvoiceHdr T1, TblSalesInvoiceDtl T2 ");
                    SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("    And T1.CancelInd='N' ");
                    SQL.AppendLine("    And T2.DOCtDocNo=A.DocNo ");
                    SQL.AppendLine("    And T2.DOCtDNo=A.DNo ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("Order By C.CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select A.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(A.CtName, ' [',B.CtCtName,']') As Col2");
                    else
                        SQL.AppendLine("A.CtName As Col2 ");
                    SQL.AppendLine("From TblCustomer A ");
                    SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode = B.CtCtCode ");
                    SQL.AppendLine("Where CtCode=@CtCode; ");
                }

                var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
                if (CtCode.Length > 0) 
                    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
                else
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

                if (CtCode.Length > 0) Sm.SetLue(LueCtCode, CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
      
        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };

        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }

        #endregion

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("Doctitle");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                var l2 = new List<InvoiceHdr>();
                var ldtl = new List<InvoiceDtl>();
                var ldtl2 = new List<InvoiceDtl2>();
                var l3 = new List<Employee>();
                var l4 = new List<EmployeeTaxCollector>();
                var l5 = new List<InvoiceHdr2>();
                var ldtl3 = new List<InvoiceDtl3>();
                var lC = new List<AddCost>();
                var lCD = new List<AddCostDisc>();
                var ldtl4 = new List<RemarkDetail>();
                var l6 = new List<Tax>();
                var l7 = new List<AddCostSIER>();
                string[] TableName = { "InvoiceHdr", "InvoiceHdr2", "InvoiceDtl3", "InvoiceDtl", "InvoiceDtl2", "Employee", "EmployeeTaxCollector", "AddCostDisc", "RemarkDetail", "Tax", "AddCostSIER" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header KIM
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes') As 'SLINotes', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes2') As 'SLINotes2', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%M %Y') As DocDtMthYr, ");
                SQL2.AppendLine("DATE_FORMAT(A.DocDt, '%d/%m/%Y') As DocDt, ");
                //SQL2.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                //SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                //SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

                SQL2.AppendLine("DATE_FORMAT(A.DueDt, '%d/%m/%Y') As DueDt, ");
                //SQL2.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                //SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                //SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");
              
                SQL2.AppendLine("A.CurCode, A.TotalTax As TotalTax, A.TotalAmt As TotalAmt, A.DownPayment As DownPayment, ");
                SQL2.AppendLine("B.CtName, B.Address, K.CtCtName, D.SAName, D.SAAddress, D.Remark, ");
                SQL2.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1, ");
                SQL2.AppendLine("IfNull(A.TaxCode2, null) As TaxCode2, ");
                SQL2.AppendLine("IfNull(A.TaxCode3, null) As TaxCode3,");

                SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
                SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
                SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
                SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
                SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
                SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
                SQL2.AppendLine("A.Amt, E.CityName, J.CntName, L.ContactPersonName, ");
                SQL2.AppendLine("F.CityName As SACityName, B.NPWP, A.ReceiptNo, ");
                SQL2.AppendLine("G.ParValue As IsDOCtAmtRounded, ");
                SQL2.AppendLine("M.Name As ShipName, ");
                SQL2.AppendLine("M.Address As ShipAddress, ");
                SQL2.AppendLine("M.Remark As ShipRemark, ");
                if (mIsSalesInvoice3UseNotesBasedOnCategory && !ChkRemarkNotesInd.Checked)
                    SQL2.AppendLine("I.Notes As RemarkSI, ");
                else
                    SQL2.AppendLine("A.Remark As RemarkSI, ");
                if (Sm.GetParameter("DocTitle") == "SIER")
                    SQL2.AppendLine(" 'Surabaya' As AddressTTD, Null As CompanyLogo2, Null As TTDPrintOut, A.CtCode, Null As SalesName, Null as SalesPosition, ");
                else if (Doctitle == "MNET")
                {
                    SQL2.AppendLine(" (Select ParValue From TblParameter Where Parcode='reportTitleSLIbasedOnDO1') As AddressTTD, @CompanyLogo2 As CompanyLogo2, concat(@TTDPrintOut,H.Parvalue) As TTDPrintOut, A.CtCode, ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='reportTitleSLIbasedOnDO2') As SalesName, ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='reportTitleSLIbasedOnDO3') As SalesPosition, ");
                }
                else
                    SQL2.AppendLine(" 'Yogyakarta' As AddressTTD, @CompanyLogo2 As CompanyLogo2, concat(@TTDPrintOut,H.Parvalue) As TTDPrintOut, A.CtCode, Null As SalesName, Null as SalesPosition, ");
                SQL2.AppendLine("B.BankVirtualAccount BankVirtualAccountBNI, H2.BankACNo BankVirtualAccountMandiri, A.CancelInd, ");
                SQL2.AppendLine("O.BankName, N.BankAcNo, N.BankAcNm, B.Address as CustAddress ");

                SQL2.AppendLine("From TblSalesInvoiceHdr A ");
                SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                //SQL2.AppendLine("Inner join ( ");
                //SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo ");
                //SQL2.AppendLine("		From TblSalesInvoiceHdr A ");
                //SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo ");
                //SQL2.AppendLine("		group by A.DocNo ");
                //SQL2.AppendLine("	) C On A.DocNo=C.DocNo ");
                //SQL2.AppendLine("Inner Join TblDoctHdr D On C.DOCtDocNo= D.DocNo ");

                SQL2.AppendLine("Inner join (  ");
                SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo  ");
                SQL2.AppendLine("		From TblSalesInvoiceHdr A  ");
                SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo  ");
                SQL2.AppendLine("		group by A.DocNo  ");
                SQL2.AppendLine("		UNION ALL ");
                SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo  ");
                SQL2.AppendLine("		From TblSalesInvoiceHdr A  ");
                SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo  ");
                SQL2.AppendLine("		inner JOIN tblservicedeliveryhdr C On B.DOCtDocNo = C.DocNo And B.DocNo = @DocNo ");
                SQL2.AppendLine("		group by A.DocNo  ");
                SQL2.AppendLine(") C On A.DocNo=C.DocNo  ");
                SQL2.AppendLine("INNER JOIN( ");
                SQL2.AppendLine("	    SELECT B.DocNo, B.SACityCode, B.SAName, B.SAAddress, B.Remark ");
                SQL2.AppendLine("	    FROM tblsalesinvoicedtl A ");
                SQL2.AppendLine("	    INNER JOIN tbldocthdr B ON A.DOCtDocNo=B.DocNo And A.DocNo = @DocNo ");
                SQL2.AppendLine("	    UNION ALL ");
                SQL2.AppendLine("	    SELECT B.DocNo, B.SACityCode, B.SAName, B.SAAddress, B.Remark ");
                SQL2.AppendLine("	    FROM tblsalesinvoicedtl A ");
                SQL2.AppendLine("	    INNER JOIN tblservicedeliveryhdr B ON A.DOCtDocNo=B.DocNo And A.DocNo = @DocNo ");
                SQL2.AppendLine(")D ON C.DOCtDocNo=D.DocNo ");

                SQL2.AppendLine("Left Join TblCity E On B.CityCode= E.CityCode ");
                SQL2.AppendLine("Left Join TblCity F On D.SACityCode= F.CityCode ");
                SQL2.AppendLine("Left Join TblParameter G On G.ParCode = 'IsDOCtAmtRounded' ");
                SQL2.AppendLine("Inner Join Tblparameter H On 0=0 And H.parCode ='SalesMemoPrintOutSignName' ");
                if (mIsSalesInvoice3UseNotesBasedOnCategory)
                    SQL2.AppendLine("Left Join TblSalesInvoiceNotes I On I.CtCtCode = B.CtCtCode ");
                SQL2.AppendLine("Left Join TblCountry J On B.CntCode= J.CntCode ");
                SQL2.AppendLine("Left Join TblCustomerCategory K On B.CtCtCode= K.CtCtCode ");
                SQL2.AppendLine("Left Join TblCustomerContactPerson L On B.CtCode= L.CtCode ");
                SQL2.AppendLine("Left Join TblCustomerShipAddress M On D.SAName= M.Name ");
                SQL2.AppendLine("Left Join TblCustomerBankVirtualAccount H2 On H2.CtCode = B.CtCode And H2.BankCode = '008' ");
                SQL2.AppendLine("Left Join TblBankAccount N On A.BankAcCode = N.BankAcCode ");
                SQL2.AppendLine("Left Join TblBank O On N.BankCode = O.BankCode ");

                SQL2.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo2", Sm.CompanyLogo2("Logo2.png"));
                    Sm.CmParam<String>(ref cm2, "@TTDPrintOut", Sm.CompanyLogo().Replace("Logo.png", ""));
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                    //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressFull",
                     "CompanyPhone",
                     "CompanyFax",

                     //6-10
                     "DocNo",
                     "DocDt",
                     "DueDt",
                     "CurCode",
                     "TotalTax",

                     //11-15
                     "TotalAmt",
                     "DownPayment", 
                     "CtName",
                     "Address",
                     "SAName",

                     //16-20
                     "SAAddress",
                     "Remark",
                     "TaxCode1",
                     "TaxCode2",
                     "TaxCode3",

                     //21-25
                     "TaxName1",
                     "TaxName2",
                     "Taxname3",
                     "TaxRate1",
                     "TaxRate2",

                     //26-30
                     "TaxRate3",
                     "Amt",
                     "CityName",
                     "SACityName",
                     "NPWP",

                     //31-35
                     "RemarkSI",
                     "ReceiptNo",
                     "IsDOCtAmtRounded",
                     "AddressTTD", 
                     "SLINotes",
                     
                     //36-40
                     "SLINotes2", 
                     "DocDtMthYr",
                     "TTDPrintOut",
                     "CompanyLogo2",
                     "CtCode",

                     //41-45
                     "CntName",
                     "CtCtName",
                     "ContactPersonName",
                     "ShipName",
                     "ShipAddress",

                     //46-50
                     "ShipRemark",
                     "BankVirtualAccountBNI",
                     "BankVirtualAccountMandiri",
                     "CancelInd",
                     "BankName",

                     //51-55
                     "BankAcNo",
                     "BankAcNm", 
                     "SalesName",
                     "CustAddress",
                     "SalesPosition"

                    });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                                CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                CompanyFax = Sm.DrStr(dr2, c2[5]),

                                DocNo = Sm.DrStr(dr2, c2[6]),
                                DocDt = Sm.DrStr(dr2, c2[7]),
                                DueDt = Sm.DrStr(dr2, c2[8]),
                                CurCode = Sm.DrStr(dr2, c2[9]),
                                TotalTax = Sm.DrDec(dr2, c2[10]),

                                TotalAmt = Sm.DrDec(dr2, c2[11]),
                                DownPayment = Sm.DrDec(dr2, c2[12]),
                                CtName = Sm.DrStr(dr2, c2[13]),
                                Address = Sm.DrStr(dr2, c2[14]),
                                SAName = Sm.DrStr(dr2, c2[15]),

                                SAAddress = Sm.DrStr(dr2, c2[16]),
                                Remark = Sm.DrStr(dr2, c2[17]),
                                TaxCode1 = Sm.DrStr(dr2, c2[18]),
                                TaxCode2 = Sm.DrStr(dr2, c2[19]),
                                TaxCode3 = Sm.DrStr(dr2, c2[20]),

                                TaxName1 = Sm.DrStr(dr2, c2[21]),
                                TaxName2 = Sm.DrStr(dr2, c2[22]),
                                TaxName3 = Sm.DrStr(dr2, c2[23]),
                                TaxRate1 = Sm.DrDec(dr2, c2[24]),
                                TaxRate2 = Sm.DrDec(dr2, c2[25]),

                                TaxRate3 = Sm.DrDec(dr2, c2[26]),
                                Amt = Decimal.Parse(TxtAmt.Text), //Sm.DrDec(dr2, c2[27]),
                                Terbilang = Sm.Terbilang(Decimal.Parse(TxtAmt.Text)), //Sm.Terbilang(Sm.DrDec(dr2, c2[27])),
                                CityName = Sm.DrStr(dr2, c2[28]),
                                SACityName = Sm.DrStr(dr2, c2[29]),
                                NPWP = Sm.DrStr(dr2, c2[30]),

                                RemarkSI = Sm.DrStr(dr2, c2[31]),
                                ReceiptNo = Sm.DrStr(dr2, c2[32]),
                                IsDOCtAmtRounded = Sm.DrStr(dr2, c2[33]),
                                AddressTTD = Sm.DrStr(dr2, c2[34]),
                                SLINotes = Sm.DrStr(dr2, c2[35]),
                                SLINotes2 = Sm.DrStr(dr2, c2[36]),
                                DocDtMthYr = Sm.DrStr(dr2, c2[37]),
                                TTDPrintOut = Sm.DrStr(dr2, c2[38]),
                                CompanyLogo2 = Sm.DrStr(dr2, c2[39]),
                                CtCode = Sm.DrStr(dr2, c2[40]),
                                CntName = Sm.DrStr(dr2, c2[41]),
                                CtCtName = Sm.DrStr(dr2, c2[42]),
                                ContactPersonName = Sm.DrStr(dr2, c2[43]),
                                ShipName = Sm.DrStr(dr2, c2[44]),
                                ShipAddress = Sm.DrStr(dr2, c2[45]),
                                ShipRemark = Sm.DrStr(dr2, c2[46]),
                                TaxCodeSier1 = LueTaxCode1.Text,
                                TaxCodeSier2 = LueTaxCode2.Text,
                                TaxCodeSier3 = LueTaxCode3.Text,
                                TaxAmtSier1 = decimal.Parse(TxtTaxAmt1.Text),
                                TaxAmtSier2 = decimal.Parse(TxtTaxAmt2.Text),
                                TaxAmtSier3 = decimal.Parse(TxtTaxAmt3.Text),
								FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",
                                BankVirtualAccountBNI = Sm.DrStr(dr2, c2[47]),
                                BankVirtualAccountMandiri = Sm.DrStr(dr2, c2[48]),

                                BankName = Sm.DrStr(dr2, c2[50]),
                                BankAcNo = Sm.DrStr(dr2, c2[51]),
                                BankAcNm = Sm.DrStr(dr2, c2[52]),
                                SalesName = Sm.DrStr(dr2, c2[53]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                CancelInd = Sm.DrStr(dr2, c2[49]),
                                CustAddress = Sm.DrStr(dr2, c2[54]),
                                SalesPosition = Sm.DrStr(dr2, c2[55]),
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Header KSM
                var cm5 = new MySqlCommand();
                var SQL5 = new StringBuilder();

                SQL5.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL5.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL5.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL5.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL5.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation1') As 'CompLocation1', ");
                SQL5.AppendLine("A.DocNo, ");
                SQL5.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                SQL5.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                SQL5.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

                SQL5.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
                SQL5.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
                SQL5.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");

                SQL5.AppendLine("A.CurCode, A.TotalTax As TotalTax, A.TotalAmt As TotalAmt, ");
                SQL5.AppendLine("B.CtName, D.SAName, D.SAAddress, ");
                SQL5.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1, ");
                SQL5.AppendLine("IfNull(A.TaxCode2, null) As TaxCode2, ");
                SQL5.AppendLine("IfNull(A.TaxCode3, null) As TaxCode3,");

                SQL5.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
                SQL5.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
                SQL5.AppendLine("(Select Z.TaxName From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
                SQL5.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
                SQL5.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
                SQL5.AppendLine("(Select Z.TaxRate From TblTax Z ");
                SQL5.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
                SQL5.AppendLine("A.Amt, E.CityName, ");
                SQL5.AppendLine("F.CityName As SACityName ");

                SQL5.AppendLine("From TblSalesInvoiceHdr A ");
                SQL5.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL5.AppendLine("Inner join ( ");
                SQL5.AppendLine("		Select A.DocNo, B.DOCtDocNo ");
                SQL5.AppendLine("		From TblSalesInvoiceHdr A ");
                SQL5.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo ");
                SQL5.AppendLine("		Where B.DNo = '001' ");
                SQL5.AppendLine("		group by A.DocNo ");
                SQL5.AppendLine("	) C On A.DocNo=C.DocNo ");
                SQL5.AppendLine("Inner Join TblDoctHdr D On C.DOCtDocNo= D.DocNo ");
                SQL5.AppendLine("Left Join TblCity E On B.CityCode= E.CityCode ");
                SQL5.AppendLine("Left Join TblCity F On D.SACityCode= F.CityCode ");
                SQL5.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn5.Open();
                    cm5.Connection = cn5;
                    cm5.CommandText = SQL5.ToString();
                    Sm.CmParam<String>(ref cm5, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm5, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr5 = cm5.ExecuteReader();
                    var c5 = Sm.GetOrdinal(dr5, new string[] 
                    {
                    //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressFull",
                     "CompanyPhone",
                     "CompLocation1",

                     //6-10
                     "DocNo",
                     "DocDt",
                     "DueDt",
                     "CurCode",
                     "TotalTax",

                     //11-15
                     "TotalAmt",
                     "CtName",
                     "SAName",
                     "SAAddress",
                     "TaxCode1",

                     //16-20
                     "TaxCode2",
                     "TaxCode3",
                     "TaxName1",
                     "TaxName2",
                     "Taxname3",

                     //21-25
                     "TaxRate1",
                     "TaxRate2",
                     "TaxRate3",
                     "Amt",
                     "CityName",

                     //26
                     "SACityName",
                    

                    });
                    if (dr5.HasRows)
                    {
                        while (dr5.Read())
                        {
                            l5.Add(new InvoiceHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr5, c5[0]),

                                CompanyName = Sm.DrStr(dr5, c5[1]),
                                CompanyAddress = Sm.DrStr(dr5, c5[2]),
                                CompanyAddressFull = Sm.DrStr(dr5, c5[3]),
                                CompanyPhone = Sm.DrStr(dr5, c5[4]),
                                CompLocation1 = Sm.DrStr(dr5, c5[5]),

                                DocNo = Sm.DrStr(dr5, c5[6]),
                                DocDt = Sm.DrStr(dr5, c5[7]),
                                DueDt = Sm.DrStr(dr5, c5[8]),
                                CurCode = Sm.DrStr(dr5, c5[9]),
                                TotalTax = Sm.DrDec(dr5, c5[10]),

                                TotalAmt = Sm.DrDec(dr5, c5[11]),
                                CtName = Sm.DrStr(dr5, c5[12]),
                                SAName = Sm.DrStr(dr5, c5[13]),
                                SAAddress = Sm.DrStr(dr5, c5[14]),
                                TaxCode1 = Sm.GetLue(LueTaxCode1), //Sm.DrStr(dr5, c5[15]),

                                TaxCode2 = Sm.GetLue(LueTaxCode2), //Sm.DrStr(dr5, c5[16]),
                                TaxCode3 = Sm.GetLue(LueTaxCode3), //Sm.DrStr(dr5, c5[17]),
                                TaxName1 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode1)), //Sm.DrStr(dr5, c5[18]),
                                TaxName2 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode2)), //Sm.DrStr(dr5, c5[19]),
                                TaxName3 = Sm.GetValue("Select TaxName From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode3)), //Sm.DrStr(dr5, c5[20]),

                                TaxRate1 = Sm.DrDec(dr5, c5[21]),
                                TaxRate2 = Sm.DrDec(dr5, c5[22]),
                                TaxRate3 = Sm.DrDec(dr5, c5[23]),
                                Amt = Decimal.Parse(TxtAmt.Text), //Sm.DrDec(dr2, c2[27]),
                                TotalAmt2 = ComputeTotalAmt(),
                               

                                CityName = Sm.DrStr(dr5, c5[25]),
                                SACityName = Sm.DrStr(dr5, c5[26]),
                                TaxAmt1 = Decimal.Parse(TxtTaxAmt1.Text),
                                TaxAmt2 = Decimal.Parse(TxtTaxAmt2.Text),
                                TaxAmt3 = Decimal.Parse(TxtTaxAmt3.Text),

                                Total = ComputeTotalAmt() + Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text),
                                Terbilang = Sm.Terbilang(ComputeTotalAmt() + Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text)), //Sm.Terbilang(Sm.DrDec(dr2, c2[27])),
                                
                                
                            });
                        }
                    }
                    dr5.Close();
                }
                myLists.Add(l5);

                #region Detail 3 KSM

                var cmDtl3 = new MySqlCommand();
                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;
                    SQLDtl3.AppendLine("Select A.ItCode, B.ItName,  ");
                    if (mSalesBasedDOQtyIndex == "1")
                    {
                        SQLDtl3.AppendLine("C.Qty, C.UPrice,  ");
                        SQLDtl3.AppendLine("(C.Qty * C.UPrice) As Amt,  ");
                    }
                    else
                    {
                        SQLDtl3.AppendLine("C.Qty2 As Qty, C.UPrice,  ");
                        SQLDtl3.AppendLine("(C.Qty2 * C.UPrice) As Amt,  "); 
                    }
                    
                    SQLDtl3.AppendLine("B.SalesuomCode As PriceUomCode ");
                    SQLDtl3.AppendLine("From TblSalesInvoiceDtl A  ");
                    SQLDtl3.AppendLine("Inner join tblitem B On A.ItCode=B.ItCode ");
                    SQLDtl3.AppendLine("Inner join ( ");

                    SQLDtl3.AppendLine("    Select T1.DocNo, T1.DNo, T3.Qty, T3.Uprice, T3.Qty2 ");
                    SQLDtl3.AppendLine("    From TblSalesInvoiceDtl T1 ");
                    SQLDtl3.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                    SQLDtl3.AppendLine("    Inner Join TblDOCtDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                    if (mIsUseServiceDelivery)
                    {
                        SQLDtl3.AppendLine("    Union All ");
                        SQLDtl3.AppendLine("    Select T1.DocNo, T1.DNo, T3.Qty, T3.Uprice, T3.Qty2 ");
                        SQLDtl3.AppendLine("    From TblSalesInvoiceDtl T1 ");
                        SQLDtl3.AppendLine("    Inner Join TblServiceDeliveryHdr T2 On T1.DOCtDocNo = T2.DocNo And T1.DocNo = @DocNo ");
                        SQLDtl3.AppendLine("    Inner Join TblServiceDeliveryDtl T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
                    }

                    SQLDtl3.AppendLine(") C On A.DocNo = C.DocNo And A.DNo = C.DNo ");

                    //SQLDtl3.AppendLine("Inner Join TblDOCthdr C On A.DOCtDocNo = C.DocNo ");
                    //SQLDtl3.AppendLine("Inner JOIN TblDOCtdtl D ON A.DOCtDocNo = D.DocNo AND A.DOCtDNo = D.DNo ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo; ");

                    cmDtl3.CommandText = SQLDtl3.ToString();
                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                    {
                        //0
                        "ItCode" ,

                        //1-5
                        "ItName" ,
                        "Qty",
                        "UPrice",
                        "Amt",
                        "PriceUomCode",

                    });

                    if (drDtl3.HasRows)
                    {
                        int No = 0;
                        while (drDtl3.Read())
                        {
                            No = No + 1;
                            ldtl3.Add(new InvoiceDtl3()
                            {
                                nomor = No,
                                ItCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                ItName = Sm.DrStr(drDtl3, cDtl3[1]),
                                Qty = Sm.DrDec(drDtl3, cDtl3[2]),
                                UPrice = Sm.DrDec(drDtl3, cDtl3[3]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[4]),
                                PriceUomCode = Sm.DrStr(drDtl3, cDtl3[5]),

                            });
                        }
                    }

                    drDtl3.Close();
                }

                myLists.Add(ldtl3);

                #endregion

                #endregion

                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    if (Doctitle == "SIER")
                    {
                        SQLDtl.AppendLine("Select '' As ItCode, DOCtRemark, ItName, Qty, UPriceAfterTax, Amt, PriceUomCode, Remark, MONTH, YEAR, ForeignName ");
                        SQLDtl.AppendLine("From ( ");
                        SQLDtl.AppendLine(" Select DOCtRemark, ItName, UPriceAfterTax, PriceUomCode, Remark, ");
                        SQLDtl.AppendLine(" Sum(Qty) As Qty, Sum(Amt) Amt, MONTH, YEAR, ForeignName ");
                        SQLDtl.AppendLine(" From ( ");
                        SQLDtl.AppendLine("     Select C.Remark As DOCtRemark, E.ItGrpName As ItName, A.Qty, A.UPriceAfterTax, ");
                        if (mIsDOCtAmtRounded)
                            SQLDtl.AppendLine("     Floor(A.Qty*A.UPriceAfterTax) As Amt, ");
                        else
                            SQLDtl.AppendLine("     A.Qty*A.UPriceAfterTax As Amt, ");
                        SQLDtl.AppendLine("     B.SalesUomCode As PriceUomCode, IfNull (D.Remark, CONCAT('Bulan ', Case monthname(C.DocDt) ");
                        SQLDtl.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                        SQLDtl.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
                        SQLDtl.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'November' When 'December' Then 'Desember' End ");
                        SQLDtl.AppendLine(" ,DATE_FORMAT(C.DocDt, ' %Y') ");
                        SQLDtl.AppendLine(" )) Remark, ");
                        SQLDtl.AppendLine(" Case monthname(C.DocDt) ");
                        SQLDtl.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                        SQLDtl.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
                        SQLDtl.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'November' When 'December' Then 'Desember' End As MONTH ");
                        SQLDtl.AppendLine(" ,DATE_FORMAT(C.DocDt, '%Y') YEAR, B.ForeignName ");
                        SQLDtl.AppendLine("     From TblSalesInvoiceDtl A ");
                        SQLDtl.AppendLine("     Inner Join TblItem B On A.ItCode=B.ItCode And B.ItGrpCode Is Not Null ");
                        SQLDtl.AppendLine("     Inner Join TblDOCthdr C On A.DOCtDocNo = C.DocNo ");
                        SQLDtl.AppendLine("     Inner Join TblDoCtDtl D On A.DOCtDocNo=D.DocNo And A.DOCtDNo=D.DNo ");
                        SQLDtl.AppendLine("     Left Join TblItemGroup E On B.ItGrpCode = E.ItGrpCode ");
                        SQLDtl.AppendLine("     Inner Join TblSalesInvoiceHdr F On A.DocNo=F.DocNo ");
                        SQLDtl.AppendLine("     Where A.DocNo=@DocNo ");
                        SQLDtl.AppendLine(" ) T Group By ItName, UPriceAfterTax, PriceUomCode, Remark ");

                        SQLDtl.AppendLine("Union All ");

                        SQLDtl.AppendLine("Select C.Remark As DOCtRemark, B.ItName, A.UPriceAfterTax, B.SalesUomCode As PriceUomCode, IfNull (D.Remark, CONCAT('Bulan ', Case monthname(C.DocDt) ");
                        SQLDtl.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                        SQLDtl.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
                        SQLDtl.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'November' When 'December' Then 'Desember' End ");
                        SQLDtl.AppendLine(" ,DATE_FORMAT(C.DocDt, ' %Y') ");
                        SQLDtl.AppendLine(" )) Remark, A.Qty, ");
                        if (mIsDOCtAmtRounded)
                            SQLDtl.AppendLine("Floor(A.Qty*A.UPriceAfterTax) As Amt, ");
                        else
                            SQLDtl.AppendLine("A.Qty*A.UPriceAfterTax As Amt, ");
                        SQLDtl.AppendLine("Case monthname(C.DocDt) ");
                        SQLDtl.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                        SQLDtl.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
                        SQLDtl.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'November' When 'December' Then 'Desember' End As MONTH ");
                        SQLDtl.AppendLine(" ,DATE_FORMAT(C.DocDt, '%Y') YEAR, B.ForeignName ");
                        SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                        SQLDtl.AppendLine("Inner join TblItem B On A.ItCode=B.ItCode And B.ItGrpCode Is Null ");
                        SQLDtl.AppendLine("Inner Join TblDOCtHdr C On A.DOCtDocNo = C.DocNo ");
                        SQLDtl.AppendLine("Inner Join TblDoCtDtl D On A.DOCtDocNo=D.DocNo And A.DOCtDNo=D.DNo ");
                        SQLDtl.AppendLine("Inner Join TblSalesInvoiceHdr E On A.DocNo=E.DocNo ");
                        SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtl.AppendLine(") Tbl Order By ItName;");

                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                            //0
                            "ItCode",

                            //1-5
                            "ItName",
                            "Qty",
                            "UPriceAfterTax",
                            "Amt",
                            "PriceUomCode",

                            //6-10
                            "Remark",
                            "MONTH",
                            "year",
                            "DOCtRemark",
                            "ForeignName"
                        });

                        if (drDtl.HasRows)
                        {
                            int No = 0;
                            while (drDtl.Read())
                            {
                                No = No + 1;
                                ldtl.Add(new InvoiceDtl()
                                {
                                    nomor = No,
                                    ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                    ItName = Sm.DrStr(drDtl, cDtl[1]),
                                    Qty = Sm.DrDec(drDtl, cDtl[2]),
                                    UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                    Remark = Sm.DrStr(drDtl, cDtl[6]),
                                    Month = Sm.DrStr(drDtl, cDtl[7]),
                                    Year = Sm.DrStr(drDtl, cDtl[8]),
                                    DOCtRemark = Sm.DrStr(drDtl, cDtl[9]),
                                    ForeignName = Sm.DrStr(drDtl, cDtl[10]),
                                });
                            }
                        }
                        drDtl.Close();
                    }
                    else
                    {
                        SQLDtl.AppendLine("Select A.ItCode, ");
                        if (Doctitle == "KIM")
                            SQLDtl.AppendLine("IfNull(B.ForeignName, B.ItName) As ItName, ");
                        else
                            SQLDtl.AppendLine("B.ItName, ");
                        SQLDtl.AppendLine("A.Qty, A.UPriceAfterTax, ");
                        if (mIsDOCtAmtRounded)
                            SQLDtl.AppendLine("Floor(A.Qty*A.UPriceAfterTax) As Amt, ");
                        else
                            SQLDtl.AppendLine("A.Qty*A.UPriceAfterTax As Amt, ");
                        SQLDtl.AppendLine("B.SalesuomCode As PriceUomCode, ");
                        SQLDtl.AppendLine("C.Remark, A.UPriceBeforeTax "); //D.Remark
                        SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                        SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                        //SQLDtl.AppendLine("Inner Join TblDOCtHdr C On A.DOCtDocNo = C.DocNo And A.DocNo = @DocNo ");
                        //SQLDtl.AppendLine("Inner Join TblDOCtDtl D On A.DOCtDocNo = D.DocNo And A.DOCtDNo = D.DNo ");
                        SQLDtl.AppendLine("INNER JOIN ( ");
                        SQLDtl.AppendLine("	    SELECT B.DocNo, C.DNo,  B.Remark ");
                        SQLDtl.AppendLine("	    FROM tblsalesinvoicedtl A ");
                        SQLDtl.AppendLine("	    INNER JOIN tbldocthdr B ON A.DOCtDocNo=B.DocNo And A.DocNo = @DocNo ");
                        SQLDtl.AppendLine("	    Inner Join TblDOCtDtl C On A.DOCtDocNo = C.DocNo And A.DOCtDNo = C.DNo ");
                        SQLDtl.AppendLine("	    UNION ALL ");
                        SQLDtl.AppendLine("	    SELECT B.DocNo, C.DNo, B.Remark ");
                        SQLDtl.AppendLine("	    FROM tblsalesinvoicedtl A ");
                        SQLDtl.AppendLine("	    INNER JOIN tblservicedeliveryhdr B ON A.DOCtDocNo=B.DocNo And A.DocNo = @DocNo ");
                        SQLDtl.AppendLine("	    Inner Join tblservicedeliverydtl C On A.DOCtDocNo = C.DocNo And A.DOCtDNo = C.DNo  ");
                        SQLDtl.AppendLine(")C ON A.DOCtDocNo = C.DocNo AND A.DOCtDNo = C.DNo ");
                        SQLDtl.AppendLine("Where A.DocNo=@DocNo; ");

                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                            //0
                            "ItCode",

                            //1-5
                            "ItName",
                            "Qty",
                            "UPriceAfterTax",
                            "Amt",
                            "PriceUomCode",

                            //6-7
                            "Remark",
                            "UPriceBeforeTax"
                        });

                        if (drDtl.HasRows)
                        {
                            int No = 0;
                            while (drDtl.Read())
                            {
                                No = No + 1;
                                ldtl.Add(new InvoiceDtl()
                                {
                                    nomor = No,
                                    ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                    ItName = Sm.DrStr(drDtl, cDtl[1]),
                                    Qty = Sm.DrDec(drDtl, cDtl[2]),
                                    UPriceAfterTax = Sm.DrDec(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                    Remark = Sm.DrStr(drDtl, cDtl[6]),
                                    UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[7])
                                });
                            }
                        }
                        drDtl.Close();
                    }
                }

                myLists.Add(ldtl);

                #endregion

                #region Detail 2

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                bool ThereisAcInd = Sm.IsDataExist("Select AcInd From TblSalesInvoiceDtl2 Where AcInd = 'Y' And DocNo = @Param Limit 1; ", TxtDocNo.Text); 
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    if (ThereisAcInd)
                    {
                        SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.CAmt),0) As CAmt, ");
                        if (Doctitle == "KIM")
                            SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)*-1 As DAmt, ifnull(sum(B.DAmt),0)*-1 + ifnull(sum(B.CAmt),0) As TAmt ");
                        else
                            SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");

                        SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                        SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                        SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                        SQLDtl2.AppendLine("Group by B.Docno ");
                    }
                    else if(!ThereisAcInd && Doctitle ==  "KIM")
                    {
                        SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.CAmt),0) As CAmt, ");
                        SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)*-1 As DAmt, ifnull(sum(B.DAmt),0)*-1 + ifnull(sum(B.CAmt),0) As TAmt ");
                        SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                        SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                        SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='N' And AcNo Not Like concat('%.', @CtCode) ");
                        SQLDtl2.AppendLine("Group by B.Docno ");
                    }
                    else
                    {
                        SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.CAmt),0) As CAmt, ");
                        SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");
                        SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                        SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                        SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                        SQLDtl2.AppendLine("Group by B.Docno ");
                    }
                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl2, "@CtCode", Sm.GetLue(LueCtCode));
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                        //0
                        "DocNo" ,

                        //1-2
                        "DAmt" ,
                        "CAmt",
                        "TAmt",

                    });

                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2()
                            {
                                DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                                DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                                CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                                TAmt = Sm.DrDec(drDtl2, cDtl2[3]),
                                ThereIsAcInd = ThereisAcInd,
                            });
                        }
                    }

                    drDtl2.Close();
                }

                myLists.Add(ldtl2);

                #endregion

                #region Signature KIM
                var cm3 = new MySqlCommand();
                var SQL3 = new StringBuilder();

                SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL3.AppendLine("From TblEmployee A ");
                SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                SQL3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeSI);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[] 
                    {
                     //0-3
                     "EmpCode",
                     "EmpName",
                     "PosName",
                     "EmpPict"
                    
                    });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new Employee()
                            {
                                EmpCode = Sm.DrStr(dr3, c3[0]),

                                EmpName = Sm.DrStr(dr3, c3[1]),
                                Position = Sm.DrStr(dr3, c3[2]),
                                EmpPict = Sm.DrStr(dr3, c3[3]),
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                #region Signature2 KIM

                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, A.Mobile ");
                SQL4.AppendLine("From TblEmployee A ");
                SQL4.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeTaxCollector);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[] 
                    {
                     //0-3
                     "EmpCode",
                     "EmpName",
                     "PosName",
                     "Mobile"
                    
                    });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            l4.Add(new EmployeeTaxCollector()
                            {
                                EmpCode = Sm.DrStr(dr4, c4[0]),

                                EmpName = Sm.DrStr(dr4, c4[1]),
                                Position = Sm.DrStr(dr4, c4[2]),
                                Mobile = Sm.DrStr(dr4, c4[3]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(l4);

                #endregion

                #region Additional Cost

                var cmC = new MySqlCommand();
                var SQLC = new StringBuilder();
                using (var cnC = new MySqlConnection(Gv.ConnectionString))
                {
                    cnC.Open();
                    cmC.Connection = cnC;

                    SQLC.AppendLine("    select DocNo, AcNo, DAmt, CAmt from tblsalesinvoicedtl2 ");
                    SQLC.AppendLine("    where acind = 'Y' ");
                    SQLC.AppendLine("    And DocNo = @DocNo ");

                    cmC.CommandText = SQLC.ToString();

                    Sm.CmParam<String>(ref cmC, "@DocNo", TxtDocNo.Text);

                    var drC = cmC.ExecuteReader();
                    var cDtC = Sm.GetOrdinal(drC, new string[] 
                    {
                     //0
                     "DocNo" ,

                     //1-3
                     "AcNo", "DAmt", "CAmt"
                    });

                    decimal mAddCost = 0m;
                    decimal mAddDisc = 0m;

                    if (drC.HasRows)
                    {
                        while (drC.Read())
                        {
                            lC.Add(new AddCost()
                            {
                                DocNo = Sm.DrStr(drC, cDtC[0]),
                                AcNo = Sm.DrStr(drC, cDtC[1]),
                                DAmt = Sm.DrDec(drC, cDtC[2]),
                                CAmt = Sm.DrDec(drC, cDtC[3]),
                            });
                        }
                    }
                    drC.Close();

                    if (lC.Count > 0)
                    {
                        for (int Row = 0; Row < lC.Count; Row++)
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", lC[Row].AcNo);
                            if (lC[Row].DAmt > 0)
                            {
                                if (AcType == "D")
                                    mAddCost += lC[Row].DAmt;
                                else
                                    mAddDisc -= lC[Row].DAmt;
                            }
                            if (lC[Row].CAmt > 0)
                            {
                                if (AcType == "C")
                                    mAddCost += lC[Row].CAmt;
                                else
                                    mAddDisc -= lC[Row].CAmt;
                            }
                        }

                        lCD.Add(new AddCostDisc()
                        {
                            AddCost = mAddCost,
                            AddDisc = mAddDisc
                        });
                    }

                }
                myLists.Add(lCD);

                #endregion

                #region Detail 4

                var cmDtl4 = new MySqlCommand();
                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;
                    SQLDtl4.AppendLine("Select GROUP_CONCAT(Distinct C.Remark SEPARATOR '\n') Remark ");
                    SQLDtl4.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl4.AppendLine("Inner join TblItem B On A.ItCode=B.ItCode And B.ItGrpCode Is Null ");
                    SQLDtl4.AppendLine("Inner Join ( ");
                    SQLDtl4.AppendLine("    Select T1.DocNo, T1.DNo, IfNull(T2.Remark, '') Remark ");
                    SQLDtl4.AppendLine("    From TblSalesInvoiceDtl T1 ");
                    SQLDtl4.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DOCtDocNo = T2.DocNo And T1.DOCtDNo = T2.DNo And T1.DocNo = @DocNo ");
                    if (mIsUseServiceDelivery)
                    {
                        SQLDtl4.AppendLine("    Union All ");
                        SQLDtl4.AppendLine("    Select T1.DocNo, T1.DNo, IfNull(T2.Remark, '') Remark ");
                        SQLDtl4.AppendLine("    From TblSalesInvoiceDtl T1 ");
                        SQLDtl4.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DOCtDocNo = T2.DocNo And T1.DOCtDNo = T2.DNo And T1.DocNo = @DocNo ");
                    }
                    SQLDtl4.AppendLine(") C On A.DocNo = C.DocNo And A.DNo = C.DNo ");
                    SQLDtl4.AppendLine("WHERE A.DocNo=@DocNo ");

                    cmDtl4.CommandText = SQLDtl4.ToString();
                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[]
                    {
                        //0
                        "Remark" 
                    });

                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ldtl4.Add(new RemarkDetail()
                            {
                                Remark = Sm.DrStr(drDtl4, cDtl4[0])
                            });
                        }
                    }

                    drDtl4.Close();
                }

                myLists.Add(ldtl4);

                #endregion

                #region Tax

                var cm6 = new MySqlCommand();
                var SQL6 = new StringBuilder();
                using (var cn6 = new MySqlConnection(Gv.ConnectionString)) 
                {
                    cn6.Open();
                    cm6.Connection = cn6;
                    SQL6.AppendLine("SELECT * FROM ");
                    SQL6.AppendLine("( ");
                    SQL6.AppendLine("SELECT B.TaxName, A.TaxAmt1 TaxAmount ");
                    SQL6.AppendLine("FROM tblsalesinvoicehdr A ");
                    SQL6.AppendLine("INNER JOIN tbltax B ON A.TaxCode1 = B.TaxCode ");
                    SQL6.AppendLine("WHERE A.TaxCode1=@TaxCode1 ");
                    SQL6.AppendLine("AND A.DocNo=@DocNo ");
                    SQL6.AppendLine("AND A.TaxAmt1 > 0 ");
                    SQL6.AppendLine("UNION ALL ");
                    SQL6.AppendLine("SELECT B.TaxName, A.TaxAmt2 TaxAmount");
                    SQL6.AppendLine("FROM tblsalesinvoicehdr A ");
                    SQL6.AppendLine("INNER JOIN tbltax B ON A.TaxCode2 = B.TaxCode ");
                    SQL6.AppendLine("WHERE A.TaxCode2=@TaxCode2 ");
                    SQL6.AppendLine("AND A.DocNo=@DocNo ");
                    SQL6.AppendLine("AND A.TaxAmt2 > 0 ");
                    SQL6.AppendLine("UNION ALL ");
                    SQL6.AppendLine("SELECT B.TaxName, A.TaxAmt3 TaxAmount");
                    SQL6.AppendLine("FROM tblsalesinvoicehdr A ");
                    SQL6.AppendLine("INNER JOIN tbltax B ON A.TaxCode3 = B.TaxCode ");
                    SQL6.AppendLine("WHERE A.TaxCode3=@TaxCode3 ");
					SQL6.AppendLine("AND A.DocNo=@DocNo ");
                    SQL6.AppendLine("AND A.TaxAmt3 > 0 ");
                    SQL6.AppendLine(") A ");

                    cm6.CommandText = SQL6.ToString();
                    Sm.CmParam<String>(ref cm6, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm6, "@TaxCode1", Sm.GetLue(LueTaxCode1));
                    Sm.CmParam<String>(ref cm6, "@TaxCode2", Sm.GetLue(LueTaxCode2));
                    Sm.CmParam<String>(ref cm6, "@TaxCode3", Sm.GetLue(LueTaxCode3));
                    var dr6 = cm6.ExecuteReader();
                    var c6 = Sm.GetOrdinal(dr6, new string[]
                    {
                        //0
                        "TaxName",
                        //1
                        "TaxAmount"
                    });
                    
                    if (dr6.HasRows)
                    {
                        int No = 0;
                        while (dr6.Read())
                        {
                            No = No + 1;
                            l6.Add(new Tax()
                            {
                                nomor = No,
                                TaxName = Sm.DrStr(dr6, c6[0]),
                                TaxAmount = Sm.DrDec(dr6, c6[1])
                            });
                        }
                    }

                    dr6.Close();
                }

                myLists.Add(l6);

                #endregion

                #region AddCostSIER

                var cm7 = new MySqlCommand();
                var SQL7 = new StringBuilder();
                using (var cn7 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn7.Open();
                    cm7.Connection = cn7;
                    SQL7.AppendLine("SELECT DocNo, OptDesc, Remark, CAmt, IF(OptDesc = 'Downpayment', 'Downpayment', LocalName) as LocalName FROM ");
                    SQL7.AppendLine("( ");
                    SQL7.AppendLine("SELECT DocNo, 'Downpayment' AS OptDesc, NULL AS Remark, DownPayment AS CAmt, Null as LocalName ");
                    SQL7.AppendLine("FROM tblsalesinvoicehdr ");
                    SQL7.AppendLine("WHERE DocNo=@DocNo AND Downpayment > 0 ");
                    SQL7.AppendLine("UNION ALL ");
                    SQL7.AppendLine("SELECT A.DocNo, B.OptDesc, A.Remark, A.CAmt, A.LocalName as LocalName ");
                    SQL7.AppendLine("FROM tblsalesinvoicedtl2 A ");
                    SQL7.AppendLine("LEFT JOIN TblOption B On A.OptAcDesc = B.OptCode And OptCat='AccountDescriptionOnSalesInvoice' ");
                    SQL7.AppendLine("WHERE DocNo=@DocNo AND CAmt > 0 ");
                    //SQL7.AppendLine("AND acind = 'Y' ");
                    SQL7.AppendLine(")A ");

                    cm7.CommandText = SQL7.ToString();
                    Sm.CmParam<String>(ref cm7, "@DocNo", TxtDocNo.Text);
                    var dr7 = cm7.ExecuteReader();
                    var c7 = Sm.GetOrdinal(dr7, new string[]
                    {
                        //0
                        "DocNo",
                        //1-4
                        "OptDesc", "Remark", "CAmt", "LocalName"
                    });

                    if (dr7.HasRows)
                    {
                        int No = 0;
                        while (dr7.Read())
                        {
                            No = No + 1;
                            l7.Add(new AddCostSIER()
                            {
                                nomor = No,
                                DocNo = Sm.DrStr(dr7, c7[0]),
                                OptDesc = Sm.DrStr(dr7, c7[1]),
                                Remark = Sm.DrStr(dr7, c7[2]),
                                CAmt = Sm.DrDec(dr7, c7[3]),
                                LocalName = Sm.DrStr(dr7, c7[4]),
                            });
                        }
                    }

                    dr7.Close();
                }

                myLists.Add(l7);

                #endregion

                Sm.PrintReport(mFormPrintOutInvoice3, myLists, TableName, false);

                if (Doctitle == "KIM")
                {
                    Sm.PrintReport(mFormPrintOutInvoice3Receipt, myLists, TableName, false);
                }
                
            }
        }

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        //internal string GenerateReceipt(string DocDt)
        //{
        //    string
        //        Yr = DocDt.Substring(2, 2),
        //        Mth = DocDt.Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle");
        //    //  DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat( ");
        //    SQL.Append("IfNull(( ");
        //    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //    SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
        //    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //    SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
        //    SQL.Append("       ) As Temp ");
        //    SQL.Append("   ), '0001') ");
        //    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
        //    SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //    SQL.Append(") As ReceiptNo");

        //    return Sm.GetValue(SQL.ToString());
        //}

        #region Upload File Method
        //UPLOAD FILE - START
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice 3 - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd5.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd5, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsSLIBasedOnDOToCustomerAllowtoUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != Sm.GetGrdStr(Grd5, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblSalesInvoiceFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteSLIFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblSalesInvoiceFile Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }
        //UPLOAD FILE - END
        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
            {
                 if (mDueDtSIValue.Length > 0 && Sm.GetDte(DteDocDt).Length>0)
                 {
                     DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
                 }
                 else
                 {
                     DteDueDt.EditValue = null;
                 }
                if (mIsTaxInvoiceDateSameWithDocDate)
                    DteTaxInvoiceDt.EditValue = DteDocDt.Text;
            };
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsCustomerComboBasedOnCategory)
                    Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue4(SetLueCtCodeBasedOnCategory), string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                else
                    Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");

                ReloadCustomer();
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), string.Empty);
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownpayment, 0);
                if (mIsDOCtAmtRounded)
                {
                    decimal Downpayment = decimal.Truncate(decimal.Parse(TxtDownpayment.Text));
                    TxtDownpayment.EditValue = Sm.FormatNum(Downpayment, 0);
                }
                ComputeAmtFromTax();
            }
        }
        

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtTaxInvDocument_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxInvDocument);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }


        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0, string.Empty, ref Grd1);
                else ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0, string.Empty, ref Grd1);
                else ComputeAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0, string.Empty, ref Grd1);
                else ComputeAmt();
            }
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsCustomerComboBasedOnCategory && IsInsert)
            {
                Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(SetLueCtCtCode));
                SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                ReloadCustomer();
            }
        }

        private void LueAdvanceChargeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSLI3UseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode), "LueAdvanceChargeCode", Grd3.Rows.Count-1);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count-1);
                ComputeAmt();
                ComputeAmtFromTax();
            }
        }

        private void LueAdvanceChargeCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSLI3UseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode2, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode2), "LueAdvanceChargeCode2", Grd3.Rows.Count-1);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count-1);
                ComputeAmt();
                ComputeAmtFromTax();
            }
        }

        private void LueAdvanceChargeCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSLI3UseAdvanceCharge)
            {
                Sm.RefreshLookUpEdit(LueAdvanceChargeCode3, new Sm.RefreshLue1(SetLueAdvanceChargeCode));
                AddAdvanceCharge(Sm.GetLue(LueAdvanceChargeCode3), "LueAdvanceChargeCode3", Grd3.Rows.Count-1);
                AddCustomerAcNoAR("CustomerAcNoAR", Grd3.Rows.Count-1);
                ComputeAmt();
                ComputeAmtFromTax();

            }
        }

        #endregion

        #region Button Event

        private void BtnCtCt_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesInvoice3Dlg4(this));
        }

        private void BtnCopySalesInvoice_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesInvoice3Dlg3(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            if (mDueDtSIValue.Length > 0)
            {
                DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteDocDt).Substring(0, 8)).AddDays(double.Parse(mDueDtSIValue));
            }
            else
            {
                DteDueDt.EditValue = null;
                var DocDt = Sm.GetDte(DteDocDt);
                if (DocDt.Length > 0 && Grd1.Rows.Count > 1)
                {
                    decimal TOP = Sm.GetGrdDec(Grd1, 0, 28);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        {
                            if (TOP > Sm.GetGrdDec(Grd1, Row, 28))
                                TOP = Sm.GetGrdDec(Grd1, Row, 28);
                        }
                    }

                    DteDueDt.EditValue = new DateTime(
                       Int32.Parse(DocDt.Substring(0, 4)),
                       Int32.Parse(DocDt.Substring(4, 2)),
                       Int32.Parse(DocDt.Substring(6, 2)),
                       0, 0, 0).AddDays((double)TOP);

                }
            }
        }

        private void BtnLocalDocNo_Click(object sender, EventArgs e)
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 29).Length > 0)
                {
                    TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 29);
                    break;
                }
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (mSalesInvoiceTaxCalculationFormula != "1")
            {
                if (e.ColIndex == 38 || e.ColIndex == 39 || e.ColIndex == 40)
                {
                    ComputeTaxPerDetail(true, e.RowIndex, "Grd1", ref Grd1);
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));
            }

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmSalesInvoice3Dlg(this, Sm.GetLue(LueCtCode)));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeAmt();
            ComputeAmtFromTax();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesInvoice3Dlg(this, Sm.GetLue(LueCtCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmDR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4)>0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6)>0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
            {
                ComputeAmt();
                ComputeAmtFromTax();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice3Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd3, ref LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoice3Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            ComputeAmtFromTax();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd4

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdDec(Grd4, e.RowIndex, e.ColIndex) > 0)
                {
                    if (Sm.GetGrdDec(Grd4, e.RowIndex, e.ColIndex) > Sm.GetGrdDec(Grd4, e.RowIndex, 2))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                        "Downpaymet Before Tax(Purchase Invoice) is bigger than Downpayment Before Tax(AP Downpayment).");
                        Grd4.Cells[e.RowIndex, e.ColIndex].Value = 0;
                    }
                    ComputeDownpayment(e.RowIndex);
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice3Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd4, ref LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            string TaxCodes = string.Empty;

            if (Sm.GetLue(LueTaxCode1).Length > 0)
                TaxCodes = Sm.GetLue(LueTaxCode1);
            if (Sm.GetLue(LueTaxCode2).Length > 0)
                TaxCodes += (TaxCodes.Length > 0 ? ("," + Sm.GetLue(LueTaxCode2)) : Sm.GetLue(LueTaxCode2));
            if (Sm.GetLue(LueTaxCode3).Length > 0)
                TaxCodes += (TaxCodes.Length > 0 ? ("," + Sm.GetLue(LueTaxCode3)) : Sm.GetLue(LueTaxCode3));

            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer")) 
                Sm.FormShowDialog(new FrmSalesInvoice3Dlg5(this, Sm.GetLue(LueCtCode), TaxCodes));
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
        }

        #endregion

        #region Grd5 (Upload File)

        //UPLOAD FILE - START
        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd5.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd5_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            //int SelectedIndex = 0;

            //if (Grd5.SelectedRows.Count > 0)
            //{
            //    for (int Index = Grd5.SelectedRows.Count - 1; Index >= 0; Index--)
            //        SelectedIndex = Grd5.SelectedRows[Index].Index;
            //}

            //if (TxtDocNo.Text.Length > 0 && TxtStatus.Text != "Approved")
            //{
            //    if (Sm.GetGrdStr(Grd5, SelectedIndex, 4).Length == 0)
            //    {
            //        Sm.GrdRemoveRow(Grd5, e, BtnSave);
            //        Sm.GrdEnter(Grd5, e);
            //        Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
            //    }
            //}
            //else if (TxtDocNo.Text.Length == 0)
            //{
            // Sm.GrdRemoveRow(Grd5, e, BtnSave);
            // Sm.GrdEnter(Grd5, e);
            // Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
            //}
        }
        //UPLOAD FILE - END

        #endregion

        #region Grd6 - Service Delivery

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                string DOCtDocNo = string.Empty;
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (DOCtDocNo.Length > 0) DOCtDocNo += ",";
                    DOCtDocNo += Sm.GetGrdStr(Grd1, i, 2);
                }
                Sm.FormShowDialog(new FrmSalesInvoice3Dlg6(this, DOCtDocNo, Sm.GetLue(LueCtCode)));
            }

            if (e.ColIndex == 3 && !Sm.IsGrdValueEmpty(Grd6, e.RowIndex, 1, false, "SD# is empty."))
            {
                var f = new FrmServiceDelivery(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd6, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (mIsUseServiceDelivery && mSalesInvoiceTaxCalculationFormula != "1")
            {
                if (e.ColIndex == 11 || e.ColIndex == 12 || e.ColIndex == 13)
                {
                    ComputeTaxPerDetail(true, e.RowIndex, "Grd6", ref Grd6);
                }
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd6, e, BtnSave);
            ComputeAmt();
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string CtCtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
            public string IsDOCtAmtRounded { get; set; }
            public string Terbilang2 { get; set; }
            public string CompanyNPWP { get; set; }
            public string CityNameCT { get; set; }
            public string BankName { get; set; }
            public string ReceiptDt { get; set; }
            public decimal AddCost { get; set; }
            public string SalesName { get; set; }
            public string PrintBy { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string LocalDocNo { get; set; }
            public string AddressTTD { get; set; }
            public string SLINotes { get; set; }
            public string SLINotes2 { get; set; }
            public string DocDtMthYr { get; set; }
            public string TTDPrintOut { get; set; }
            public string CompanyLogo2 { set; get; }
            public string CtCode { set; get; }
            public string CntName { set; get; }
            public string ContactPersonName { set; get; }
            public string ShipName { set; get; }
            public string ShipAddress { set; get; }
            public string ShipRemark { set; get; }
            public string TaxCodeSier1 { get; set; }
            public string TaxCodeSier2 { get; set; }
            public string TaxCodeSier3 { get; set; }
            public decimal TaxAmtSier1 { get; set; }
            public decimal TaxAmtSier2 { get; set; }
            public decimal TaxAmtSier3 { get; set; }
			public string FooterImage { get; set; }
            public string BankVirtualAccountBNI { get; set; }
            public string BankVirtualAccountMandiri { get; set; }
            public string CancelInd { set; get; }
            public string CustAddress { set; get; }
            public string SalesPosition { set; get; }
        }

        class InvoiceDtl
        {
            public int nomor { get; set; }
            public decimal UPriceBeforeTax { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string CTPoNo { get; set; }
            public string PriceUomCode { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtDisc { get; set; }
            public decimal AmtTax { get; set; }
            public decimal AmtCost { get; set; }
            public string PtName { get; set; }
            public decimal Qty2 { get; set; }
            public string SODocNo { get; set; }
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
            public string SACityName { get; set; }
            public string HSCode { get; set; }
            public string Color { get; set; }
            public string Material { get; set; }
            public string ItCodeInternal { get; set; }
            public string DTNAme { get; set; }
            public string CtItCode { get; set; }
            public string Top { get; set; }
            public decimal QtyPcs { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string DOCtRemark { get; set; }
            public string ForeignName { get; set; }
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
            public bool ThereIsAcInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        private class Tax 
        {
            public int nomor { get; set; }
            public string TaxName { get; set; }
            public decimal TaxAmount { get; set; }
        }

        private class AddCostSIER
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string OptDesc { get; set; }
            public string Remark { get; set; }
            public decimal CAmt { get; set; }
            public string LocalName { get; set; }
        }

        //KSM
        private class InvoiceHdr2
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompLocation1 { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public decimal TotalAmt2 { get; set; }
            public decimal Total { get; set; }
        }

        class InvoiceDtl3
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        private class AddCost
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class AddCostDisc
        {
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
        }
        private class RemarkDetail
        {
            public string Remark { get; set; }
        }

        private class DOCtServiceDelivery
        {
            public string DocNo { get; set; }
            public string DOCtDocNo { get; set; }
        }

        #endregion             

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion       

        #endregion
    }
}
