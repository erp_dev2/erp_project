﻿#region Update
/*
    31/05/2022 [SET/PRODUCT] new dialog
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecuritiesDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAcquisitionDebtSecurities mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecuritiesDlg4(FrmAcquisitionDebtSecurities FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            SetGrd();
            SetSQL();
            SetLueAcNo(ref LueAcCode);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-3
                        "Account#",
                        "Description",
                        "Type",
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, AcDesc, ");
            SQL.AppendLine("Case AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType ");
            SQL.AppendLine("From TblCoa ");
            SQL.AppendLine("Where AcNo Not In ");
            SQL.AppendLine("(Select parent From TblCoa ");
            SQL.AppendLine("Where parent is not null ) ");
            SQL.AppendLine("And Parent is not null ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Parent Like '" + Sm.GetLue(LueAcCode) + "%' ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo, AcDesc;",
                        new string[]
                        { 
                            //0
                            "AcNo",
                            "AcDesc",
                            "AcType"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                Sm.CopyGrdValue(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 4, Grd1, Grd1.CurRow.Index, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mFrmParent.Grd1.CurRow.Index, 5, Grd1, Grd1.CurRow.Index, 2);
                //mFrmParent.Grd1.Rows.Add();
                this.Close();
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueAcNo(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select AcNo As Col1, AcDesc As Col2 From TblCoA " +
                "Where Level=1 Order By AcNo",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(SetLueAcNo));
        }


        private void ChkAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account's Group");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion
    }
}
