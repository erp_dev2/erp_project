﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMutationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmMutation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMutationFind(FrmMutation FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                SetGrd();
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.WhsName As WhsName, C.CancelInd, C.Lot, C.Bin, ");
            SQL.AppendLine("C.ItCodeFrom, D.ItName As ItNameFrom, C.BatchNoFrom, C.SourceFrom, ");
            SQL.AppendLine("C.QtyFrom, D.InventoryUomCode As InventoryUomCodeFrom, C.Qty2From, D.InventoryUomCode2 As InventoryUomCodeFrom2, C.Qty3From, D.InventoryUomCode3 As InventoryUomCodeFrom3, ");
            SQL.AppendLine("C.ItCodeTo, E.ItName As ItNameTo, C.BatchNoTo, C.SourceTo, ");
            SQL.AppendLine("C.QtyTo, E.InventoryUomCode As InventoryUomCodeTo, C.Qty2To, E.InventoryUomCode2 As InventoryUomCodeTo2, C.Qty3To, E.InventoryUomCode3 As InventoryUomCodeTo3, ");
            SQL.AppendLine("C.ItemCostDocNo, F.CurCode, G.Cost, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblWareHouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=B.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblMutationDtl C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItem D On C.ItCodeFrom=D.ItCode ");
            SQL.AppendLine("Left Join TblItem E On C.ItCodeTo=E.ItCode ");
            SQL.AppendLine("Left Join TblItemCostHdr F On C.ItemCostDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblItemCostDtl G On C.ItemCostDocNo=G.DocNo And C.ItemCostDNo=G.DNo ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        { 
            
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Warehouse",
                        "Lot",
                        
                        //6-10
                        "Bin",
                        "Item's Code"+Environment.NewLine+"From",
                        "Item's Name"+Environment.NewLine+"From",
                        "Batch#"+Environment.NewLine+"From",
                        "Source"+Environment.NewLine+"From",
                        
                        //11-15
                        "Quantity From"+Environment.NewLine+"(Inventory 1)",
                        "UoM",
                        "Quantity From"+Environment.NewLine+"(Inventory 2)",
                        "UoM",
                        "Quantity From"+Environment.NewLine+"(Inventory 3)",
                        
                        //16-20
                        "UoM",
                        "Item's Code"+Environment.NewLine+"To",
                        "Item's Name"+Environment.NewLine+"To",
                        "Batch#"+Environment.NewLine+"To",
                        "Source"+Environment.NewLine+"To",
                        
                        //21-25
                        "Quantity To"+Environment.NewLine+"",
                        "UoM",
                        "Quantity To"+Environment.NewLine+"2",
                        "UoM",
                        "Quantity To"+Environment.NewLine+"3",
                        
                        //26-30
                        "UoM",
                        "Item's Cost"+Environment.NewLine+"Document#",
                        "Currency",
                        "Cost",
                        "Created"+Environment.NewLine+"By",
                        
                        //31-35
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        130, 80, 50, 120, 100, 
                        
                        //6-10
                        100, 100, 170, 170, 170, 

                        //11-15
                        100, 80, 100, 80, 100,

                        //16-20
                        80, 100, 150, 170, 170, 

                        //21-25
                        80, 80, 80, 80, 80,

                        //26-30
                        80, 130, 70, 80, 100, 

                        //31-35
                        80, 80, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 31, 34 });
            Sm.GrdFormatTime(Grd1, new int[] { 32, 35 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 21, 23, 25, 29 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 10, 13, 14, 15, 16, 20, 23, 24, 25, 26, 30, 31, 32, 33, 34, 35 }, false);
            if (!mFrmParent.mIsShowMutationPriceInfo) Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 10, 13, 14, 15, 16, 20, 23, 24, 25, 26, 30, 31, 32, 33, 34, 35 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 23, 24 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 23, 24, 25, 26 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And C.CancelInd='N' ";


                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCodeFrom.Text, new string[] { "C.ItCodeFrom", "D.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCodeTo.Text, new string[] { "C.ItCodeTo","E.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNoFrom.Text, "C.BatchNoFrom", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNoTo.Text, "C.BatchNoTo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, C.DNo;",
                        new string[]
                        {
                              //0
                              "DocNo",

                              //1-5
                              "DocDt", "CancelInd", "WhsName", "Lot", "Bin", 

                              //6-10
                              "ItCodeFrom", "ItNameFrom", "BatchNoFrom", "SourceFrom", "QtyFrom", 

                              //11-15
                              "InventoryUomCodeFrom", "Qty2From", "InventoryUomCodeFrom2", "Qty3From", "InventoryUomCodeFrom3",

                              //16-20
                              "ItCodeTo", "ItNameTo", "BatchNoTo", "SourceTo", "QtyTo", 
 
                              //21-25
                              "InventoryUomCodeTo", "Qty2To", "InventoryUomCodeTo2", "Qty3To", "InventoryUomCodeTo3",  
                              
                              //26-30
                              "ItemCostDocNo", "CurCode", "Cost", "CreateBy", "CreateDt", 
                              
                              //31-32
                              "LastUpBy", "LastUpDt"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 35, 32);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13, 15, 21, 23, 25 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                if (Grd1.Rows.Count <= 1)
                    Sm.FocusGrd(Grd1, 0, 0);
                else
                    Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCodeFrom_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCodeFrom_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item (From)");
        }

        private void TxtItCodeTo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCodeTo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item (To)");
        }

        private void TxtBatchNoFrom_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNoFrom_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "From batch#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtBatchNoTo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNoTo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "To batch#");
        }

        #endregion

        private void TxtBatchNoTo_EditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion

        private void label8_Click(object sender, EventArgs e)
        {

        }

    }
}
