﻿#region Update
/*
    02/10/2019 [HAR/TWC] ada penambahan akun 6, 7, 8
    15/12/2019 [TKG/TWC] merubah perhitungan laba rugi berjalan (custom)
    12/02/2020 [TKG/GSS] menggunakan parameter MaxAccountCategory untuk menghitung laba rugoi berjalan dan ihtisar laba rugi
    24/02/2020 [TKG/KBN] proses baru untuk CurrentEarningFormulaType (3)
    24/03/2020 [TKG/SRN] berdasarkan parameter DefaultBalanceInProfitLoss, untuk nomor rekening coa kepala diatas 5, perhitungan balancenya berdasarkan account typenya atau tidak.
    02/11/2020 [WED/PHT] tambah filter level from dan to, berdasarkan parameter IsFicoUseCOALevelFilter
    08/11/2020 [TKG/PHT] Tambah filter multi entity berdasarkan parameter IsFicoUseMultiEntityFilter
    14/01/2021 [ICA/PHT] Mengganti MultiEntity menjadi MultiProfitCenter
    25/01/2020 [TKG/PHT] ubah SetCcbEntCode divalidasi berdasarkan cost center group user
    02/02/2020 [TKG/PHT] ubah query menggunakan filter profit center
    03/02/2020 [TKG/PHT] apabila profit center tidak di-tick (consolidate), cost center yg kosong tetap ditampilkan.
    05/02/2020 [TKG/PHT] Combo box profit center diurutkan
    16/03/2021 [TKG/PHT] profit center divalidasi berdasarkan parent juga.
    16/03/2021 [TKG/PHT] divalidasi berdasarkan group profit center.
    12/04/2021 [TKG/PHT] filter multi profit center diurutkan berdasarkan kodenya.
    04/07/2021 [TKG/PHT] tambahan filter period
    06/07/2021 [TKG/PHT] filter period hanya berpengaruh ke current month
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    22/09/2021 [TKG/ALL] Berdasarkan parameter IsRptFinancialNotUseStartFromFilter, menonaktifkan start from.
    01/10/2021 [RDA/AMKA] bug filter cost center belum bisa diakses
    10/11/2021 [RIS/AMKA] Membuat field Cost Center terfilter berdasarkan group dan param IsFilterByCC
    16/11/2021 [DITA/PHT] menambah akun spesifik kepala 9
    15/02/2022 [TKG/PHT] merubah GetParameter()
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss3 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAccountingRptStartFrom = string.Empty,
            mDocTitle = string.Empty,
            mCurrentEarningFormulaType = string.Empty,
            mMaxAccountCategory = "9",
            mDefaultBalanceInProfitLoss = "0",
            mCOAInterOffice = string.Empty,
            mCOAInterOffice2 = string.Empty,
            mFormulaForComputeProfitLoss = string.Empty;
            
        private bool 
            mIsEntityMandatory = false,
            mIsRptProfitLossUseFilterPeriod = false,
            mIsJournalCostCenterEnabled = false,
            mIsFicoUseCOALevelFilter = false,
            mIsReportingFilterByEntity = false,
            //mIsFicoUseMultiProfitCenterFilter = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsRptProfitLossUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptFinancialNotUseStartFromFilter = false,
            mIsProfitLossShowInterOfficeAccount = false,
            mIsFilterByCC = false;

        string[] mCOAInterOffice3 = null;

        #endregion

        #region Constructor

        public FrmRptProfitLoss3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLuePeriod(LuePeriod);
                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                    }
                }
                if (!mIsFicoUseCOALevelFilter)
                {
                    LblLevel.Visible = LblLevel2.Visible = LueLevelFrom.Visible = LueLevelTo.Visible = false;
                }
                else
                {
                    SetLueLevelFromTo(ref LueLevelFrom);
                    SetLueLevelFromTo(ref LueLevelTo);
                }

                if (mIsReportingFilterByEntity) SetLueEntCode(ref LueEntCode);
                if (mIsRptProfitLossUseProfitCenter) 
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                PnlCCCode.Visible = mIsJournalCostCenterEnabled;
                // Sl.SetLueCCCode(ref LueCCCode);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
                PnlPeriod.Visible = mIsRptProfitLossUseFilterPeriod;
                if (mIsRptProfitLossUseFilterPeriod)
                {
                    Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                    SetGrd2();
                }
                else
                    SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptProfitLossUseFilterPeriod', 'IsEntityMandatory', 'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', 'IsProfitLossShowInterOfficeAccount', ");
            SQL.AppendLine("'IsJournalCostCenterEnabled', 'IsReportingFilterByEntity', 'IsFicoUseMultiEntityFilter', 'IsFicoUseCOALevelFilter', 'IsRptProfitLossUseProfitCenter', ");
            SQL.AppendLine("'IsFilterByCC', 'AcNoForCurrentEarning', 'AcNoForIncome', 'AcNoForCost', 'DocTitle', ");
            SQL.AppendLine("'AccountingRptStartFrom', 'MaxAccountCategory', 'CurrentEarningFormulaType', 'DefaultBalanceInProfitLoss', 'FormulaForComputeProfitLoss' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "FormulaForComputeProfitLoss": mFormulaForComputeProfitLoss = ParValue; break;
                            case "DefaultBalanceInProfitLoss": mDefaultBalanceInProfitLoss = ParValue; break;
                            case "CurrentEarningFormulaType": mCurrentEarningFormulaType = ParValue; break;
                            case "MaxAccountCategory": mMaxAccountCategory = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;

                            //boolean 
                            case "IsRptProfitLossUseProfitCenter": mIsRptProfitLossUseProfitCenter = ParValue == "Y"; break;
                            case "IsFicoUseCOALevelFilter": mIsFicoUseCOALevelFilter = ParValue == "Y"; break;
                            case "IsFicoUseMultiEntityFilter": mIsFicoUseMultiEntityFilter = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsRptProfitLossUseFilterPeriod": mIsRptProfitLossUseFilterPeriod = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsJournalCostCenterEnabled": mIsJournalCostCenterEnabled = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                            case "IsProfitLossShowInterOfficeAccount": mIsProfitLossShowInterOfficeAccount = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
            if (mCurrentEarningFormulaType.Length == 0) mCurrentEarningFormulaType = "3";
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Account Name";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 130;
            Grd1.Header.Cells[0, 2].Value = "Debit";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 130;
            Grd1.Header.Cells[0, 3].Value = "Credit";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 130;
            Grd1.Header.Cells[0, 4].Value = mDocTitle=="KIM"? "Year to Date":"Balance";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        private void SetGrd2()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Account Name";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 130;
            Grd1.Header.Cells[0, 2].Value = "Debit";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 130;
            Grd1.Header.Cells[0, 3].Value = "Credit";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 130;
            Grd1.Header.Cells[0, 4].Value = mDocTitle == "KIM" ? "Year to Date":"Balance";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 130;
            Grd1.Header.Cells[0, 5].Value = "Debit" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 130;
            Grd1.Header.Cells[0, 6].Value = "Credit" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 130;
            Grd1.Header.Cells[0, 7].Value = mDocTitle == "KIM" ? "Year to Date" + Environment.NewLine + "(Period)" : "Balance" + Environment.NewLine + "(Period)";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            if (mIsRptProfitLossUseFilterPeriod)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month") || 
                IsLevelFromToInvalid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            if (ChkDocDt.Checked)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 5, 6 }, !ChkHideInfoInGrd.Checked);
                Sm.GrdColInvisible(Grd1, new int[] { 7 }, true);
            }
            else
            {
                if (mIsRptProfitLossUseFilterPeriod)
                    Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, false);
            }

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueStartFrom);

            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var lCOAInterOffice = new List<COAInterOffice>();
                var IsFilterByEntity = ChkEntCode.Checked;

                SetProfitCenter();
                if (mIsProfitLossShowInterOfficeAccount) ProcessCOAInterOffice(ref lCOAInterOffice);
                Process1(ref lCOA);
                if (IsFilterByEntity) Process6(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    if (ChkDocDt.Checked)
                    {
                        if (mIsRptAccountingShowJournalMemorial)
                            Process7_JournalMemorial(ref lCOA, Yr, Mth);
                        else
                            Process7(ref lCOA, Yr, Mth);
                    }
                    Process4(ref lCOA);
                    Process5(ref lCOA);
                    ComputeProfitLoss(ref lCOA);

                    #region Filter by Entity

                    if (IsFilterByEntity)
                    {
                        if (lEntityCOA.Count > 0)
                         {
                             if (lCOA.Count > 0)
                             {
                                 Grd1.BeginUpdate();
                                 Grd1.Rows.Count = 0;

                                 iGRow r;

                                 r = Grd1.Rows.Add();
                                 r.Level = 0;
                                 r.TreeButton = iGTreeButtonState.Visible;
                                 r.Cells[0].Value = "COA";
                                 for (var j = 0; j < lEntityCOA.Count; j++)
                                 {
                                     for (var i = 0; i < lCOA.Count; i++)
                                     {
                                         if (lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                         {
                                             if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                             {
                                                 if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                     lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                                 {
                                                     r = Grd1.Rows.Add();
                                                     r.Level = lCOA[i].Level;
                                                     r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                     r.Cells[0].Value = lCOA[i].AcNo;
                                                     r.Cells[1].Value = lCOA[i].AcDesc;
                                                     if (ChkDocDt.Checked)
                                                     {
                                                         for (var c = 2; c < 8; c++)
                                                             r.Cells[c].Value = 0m;
                                                         r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                                         r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                                         r.Cells[4].Value = lCOA[i].Balance;
                                                         r.Cells[5].Value = lCOA[i].PeriodDAmt;
                                                         r.Cells[6].Value = lCOA[i].PeriodCAmt;
                                                         r.Cells[7].Value = lCOA[i].PeriodBalance;
                                                     }
                                                     else
                                                     {
                                                         for (var c = 2; c < 5; c++)
                                                             r.Cells[c].Value = 0m;
                                                         r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                                         r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                                         r.Cells[4].Value = lCOA[i].Balance;
                                                     }
                                                 }
                                             }
                                             else
                                             {
                                                 r = Grd1.Rows.Add();
                                                 r.Level = lCOA[i].Level;
                                                 r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                 r.Cells[0].Value = lCOA[i].AcNo;
                                                 r.Cells[1].Value = lCOA[i].AcDesc;
                                                 if (ChkDocDt.Checked)
                                                 {
                                                     for (var c = 2; c < 8; c++)
                                                         r.Cells[c].Value = 0m;
                                                     r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                                     r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                                     r.Cells[4].Value = lCOA[i].Balance;
                                                     r.Cells[5].Value = lCOA[i].PeriodDAmt;
                                                     r.Cells[6].Value = lCOA[i].PeriodCAmt;
                                                     r.Cells[7].Value = lCOA[i].PeriodBalance;
                                                 }
                                                 else
                                                 {
                                                     for (var c = 2; c < 5; c++)
                                                         r.Cells[c].Value = 0m;
                                                     r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                                     r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                                     r.Cells[4].Value = lCOA[i].Balance;
                                                 }
                                             }
                                         }
                                     }
                                 }

                                 Grd1.TreeLines.Visible = true;
                                 Grd1.Rows.CollapseAll();

                                 Grd1.EndUpdate();
                             }
                          }
                        }
                        #endregion

                    #region Not Filter By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                {
                                    if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                        lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                    {
                                        r = Grd1.Rows.Add();
                                        r.Level = lCOA[i].Level;
                                        r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                        r.Cells[0].Value = lCOA[i].AcNo;
                                        r.Cells[1].Value = lCOA[i].AcDesc;

                                        if (ChkDocDt.Checked)
                                        {
                                            for (var c = 2; c < 8; c++)
                                                r.Cells[c].Value = 0m;
                                            r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                            r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                            r.Cells[4].Value = lCOA[i].Balance;
                                            r.Cells[5].Value = lCOA[i].PeriodDAmt;
                                            r.Cells[6].Value = lCOA[i].PeriodCAmt;
                                            r.Cells[7].Value = lCOA[i].PeriodBalance;
                                        }
                                        else
                                        {
                                            for (var c = 2; c < 5; c++)
                                                r.Cells[c].Value = 0m;
                                            r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                            r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                            r.Cells[4].Value = lCOA[i].Balance;
                                        }
                                    }
                                }
                                else
                                {
                                    r = Grd1.Rows.Add();
                                    r.Level = lCOA[i].Level;
                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                    r.Cells[0].Value = lCOA[i].AcNo;
                                    r.Cells[1].Value = lCOA[i].AcDesc;

                                    if (ChkDocDt.Checked)
                                    {
                                        for (var c = 2; c < 8; c++)
                                            r.Cells[c].Value = 0m;
                                        r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                        r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                        r.Cells[4].Value = lCOA[i].Balance;
                                        r.Cells[5].Value = lCOA[i].PeriodDAmt;
                                        r.Cells[6].Value = lCOA[i].PeriodCAmt;
                                        r.Cells[7].Value = lCOA[i].PeriodBalance;
                                    }
                                    else
                                    {
                                        for (var c = 2; c < 5; c++)
                                            r.Cells[c].Value = 0m;
                                        r.Cells[2].Value = lCOA[i].YearToDateDAmt;
                                        r.Cells[3].Value = lCOA[i].YearToDateCAmt;
                                        r.Cells[4].Value = lCOA[i].Balance;
                                    }
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();

                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                    lCOAInterOffice.Clear();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptProfitLossUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            //SQL.AppendLine("Union All ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode As Col1, B.EntName As Col2 ");
                SQL.AppendLine("    From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueLevelFromTo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Level Col1, Level Col2 ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Order By Level; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsLevelFromToInvalid()
        {
            if (!mIsFicoUseCOALevelFilter) return false;

            string mLevelFrom = string.Empty, mLevelTo = string.Empty;

            mLevelFrom = Sm.GetLue(LueLevelFrom);
            mLevelTo = Sm.GetLue(LueLevelTo);

            if (mLevelTo.Length > 0 || mLevelFrom.Length > 0)
            {
                if (mLevelFrom.Length > 0 && mLevelTo.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level to is empty.");
                    LueLevelTo.Focus();
                    return true;
                }

                if (mLevelFrom.Length == 0 && mLevelTo.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level from is empty.");
                    LueLevelFrom.Focus();
                    return true;
                }

                if (mLevelTo.Length > 0 && mLevelFrom.Length > 0)
                {
                    if (Int32.Parse(mLevelFrom) > Int32.Parse(mLevelTo))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Level from should be smaller than level to.");
                        LueLevelFrom.Focus();
                        return true;
                    }
                }
            }

            return false;
        }

        //show account number, account description, parent, type
        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select A.AcNo, A.AcDesc, A.Parent, A.AcType ");
            SQL.AppendLine("From TblCOA A ");
            if (ChkEntCode.Checked) SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
            SQL.AppendLine("Where (A.AcNo LIKE '4%' Or A.AcNo LIKE '5%' ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(" Or A.AcNo LIKE '6%' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(" Or A.AcNo LIKE '7%' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(" Or A.AcNo LIKE '8%' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" Or A.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" Or A.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" Or A.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or A.AcNo = @COAInterOffice2 ");
                for(int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or A.AcNo Like Concat("+ mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(A.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ActInd='Y' ");  
            SQL.AppendLine("ORDER BY A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (mIsProfitLossShowInterOfficeAccount)
                {
                    Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                    Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            //ParentRow = 0,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            PeriodDAmt = 0m,
                            PeriodCAmt = 0m,
                            PeriodBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        //get opening balance
        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And C.ActInd='Y' ");
            SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And (Left(B.AcNo, 1)='4' Or Left(B.AcNo, 1)='5' ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='6' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='7' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='8' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='6' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='7' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" Or Left(B.AcNo, 1)='8' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
           
            SQL.AppendLine(" And A.CancelInd='N' And A.Yr=@Yr ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }
            if (ChkEntCode.Checked)
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount
        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("SELECT B.AcNo, SUM(B.DAmt) AS DAmt, SUM(B.CAmt) AS CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo; ");
            
            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("SELECT B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");

            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("SELECT B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalMemorialDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            SQL.AppendLine("And A.CancelInd='N' And A.Status='O' ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            else
            {
                SQL.AppendLine("And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("And Left(A.DocDt, 6)<=@YrMth ");
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo; ");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount untuk filter period
        private void Process7(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) AS DAmt, Sum(B.CAmt) AS CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocDt Between @StartDt And @EndDt ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo;");
            
            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1), 
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].PeriodDAmt += lJournal[i].DAmt;
                            lCOA[j].PeriodCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process7_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A  ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocDt Between @StartDt And @EndDt ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion 

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A  ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (Left(B.AcNo, 1) In ('4', '5') ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(" OR B.AcNo LIKE '6%' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(" OR B.AcNo LIKE '7%' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(" OR B.AcNo LIKE '8%' ");
            }
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or B.AcNo = @COAInterOffice2 "); 
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or B.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(B.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And A.CancelInd='N' And A.Status='O' ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("    And ((");
                SQL.AppendLine("        Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        And A.Period Is Not Null And A.Period=@Period ");
                SQL.AppendLine("        ) Or (");
                SQL.AppendLine("        Left(A.DocDt, 4) <> @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) <> @Mth ");
                SQL.AppendLine("        )) ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (ChkCCCode.Checked) SQL.AppendLine("And A.CCCode Is Not Null And A.CCCode=@CCCode ");
            if (mIsRptProfitLossUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion 

            SQL.AppendLine(") Tbl Group By AcNo; ");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            if (ChkCCCode.Checked) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            if (mIsProfitLossShowInterOfficeAccount)
            {
                Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].PeriodDAmt += lJournal[i].DAmt;
                            lCOA[j].PeriodCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        //sum to column year to date
        private void Process4(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            string AcType9 = string.Empty;

            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                if (mCurrentEarningFormulaType == "2")
                {
                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    if (mIsProfitLossShowInterOfficeAccount)
                    {
                        if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                            lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    }
                }
                if (mCurrentEarningFormulaType == "3")
                {
                        if (mDefaultBalanceInProfitLoss == "1")
                        {
                            if (lCOA[i].AcType == "D")
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                            }
                            else
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                            }

                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                                {
                                    if (lCOA[i].AcNo.Length > 1) AcType9 = Sm.GetValue("Select AcType From TblCOA where AcNo = @Param ;", Sm.Left(lCOA[i].AcNo, 4));
                                    else AcType9 = Sm.GetValue("Select AcType From TblCOA where AcNo = @Param ;", Sm.Left(lCOA[i].AcNo, 1));

                                    if (AcType9 == "D") lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                                    else lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                                }
                            }
                        }
                        else
                        {
                            if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                            if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                            if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            }
                        }
                        
                }

                if (ChkDocDt.Checked)
                {
                    //if (lCOA[i].AcType == "C")
                    //    lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;
                    //else
                    //    lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;


                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                    if (mCurrentEarningFormulaType == "2")
                    {
                        if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                            lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                        if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                            lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                        if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                            lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                        if (mIsProfitLossShowInterOfficeAccount)
                        {
                            if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                                lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                        }
                    }
                    if (mCurrentEarningFormulaType == "3")
                    {
                        if (mDefaultBalanceInProfitLoss == "1")
                        {
                            if (lCOA[i].AcType == "D")
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                            }
                            else
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;

                                if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;
                            }

                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                                {
                                    if (lCOA[i].AcNo.Length > 1) AcType9 = Sm.GetValue("Select AcType From TblCOA where AcNo = @Param ;", Sm.Left(lCOA[i].AcNo, 4));
                                    else AcType9 = Sm.GetValue("Select AcType From TblCOA where AcNo = @Param ;", Sm.Left(lCOA[i].AcNo, 1));

                                    if (AcType9 == "D") lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                                    else lCOA[i].PeriodBalance = lCOA[i].PeriodCAmt - lCOA[i].PeriodDAmt;
                                }
                            }
                        }
                        else
                        {
                            if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory >= 6)
                                lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                            if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory >= 7)
                                lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;

                            if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory >= 8)
                                lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (Sm.Left(lCOA[i].AcNo, 1) == "9")
                                    lCOA[i].PeriodBalance = lCOA[i].PeriodDAmt - lCOA[i].PeriodCAmt;
                            }

                        }
                    }
                }
            }
        }

        //checking whether this account has parent or nah
        private void Process5(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process6(ref List<EntityCOA> lEntityCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Left Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And (Left(T.AcNo, 1) In ('4','5' ");
            if (mCurrentEarningFormulaType == "2")
            {
                if (MaxAccountCategory > 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory > 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory > 8)
                    SQL.AppendLine(", '8' ");
            }
            if (mCurrentEarningFormulaType == "3")
            {
                if (MaxAccountCategory >= 6)
                    SQL.AppendLine(", '6' ");
                if (MaxAccountCategory >= 7)
                    SQL.AppendLine(", '7' ");
                if (MaxAccountCategory >= 8)
                    SQL.AppendLine(", '8' ");
            }
            SQL.AppendLine(") ");
            //SQL.AppendLine("And U.EntCode = @EntCode ");
            if (mIsProfitLossShowInterOfficeAccount)
            {
                SQL.AppendLine("Or T.AcNo = @COAInterOffice2 ");
                for (int i = 0; i < mCOAInterOffice3.Count(); i++)
                    SQL.AppendLine("Or T.AcNo Like Concat(" + mCOAInterOffice3[i] + ",'%') ");
                SQL.AppendLine("Or Find_In_Set(T.AcNo, @COAInterOffice) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (mIsProfitLossShowInterOfficeAccount)
                {
                    Sm.CmParam<String>(ref cm, "@COAInterOffice", mCOAInterOffice);
                    Sm.CmParam<String>(ref cm, "@COAInterOffice2", mCOAInterOffice2);
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProfitLoss(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            decimal balance4 = 0m;
            decimal balance5 = 0m;
            decimal balance6 = 0m;
            decimal balance7 = 0m;
            decimal balance8 = 0m;
            decimal balance9 = 0m;
            decimal ProfitLoss = 0m;
            string mAcType9 = string.Empty;
            string formula = string.Empty;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcNo == "4")
                {
                    balance4 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }

                if (lCOA[i].AcNo == "5")
                {
                    balance5 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }

                if (mCurrentEarningFormulaType == "2")
                {
                    if (lCOA[i].AcNo == "6" && MaxAccountCategory > 6)
                    {
                        balance6 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    }

                    if (lCOA[i].AcNo == "7" && MaxAccountCategory > 7)
                    {
                        balance7 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    }

                    if (lCOA[i].AcNo == "8" && MaxAccountCategory > 8)
                    {
                        balance8 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    }
                    if (mIsProfitLossShowInterOfficeAccount)
                    {
                        if (lCOA[i].AcNo == "9")
                        {
                            balance9 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                        }
                    }
                }
                if (mCurrentEarningFormulaType == "3")
                {
                    if (mDefaultBalanceInProfitLoss == "1")
                    {
                        if (lCOA[i].AcType == "D")
                        {
                            if (lCOA[i].AcNo == "6" && MaxAccountCategory >= 6)
                            {
                                balance6 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            }

                            if (lCOA[i].AcNo == "7" && MaxAccountCategory >= 7)
                            {
                                balance7 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            }

                            if (lCOA[i].AcNo == "8" && MaxAccountCategory >= 8)
                            {
                                balance8 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            }
                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (lCOA[i].AcNo == "9")
                                {
                                    balance9 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                                }
                            }

                        }
                        else
                        {
                            if (lCOA[i].AcNo == "6" && MaxAccountCategory >= 6)
                            {
                                balance6 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                            }

                            if (lCOA[i].AcNo == "7" && MaxAccountCategory >= 7)
                            {
                                balance7 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                            }

                            if (lCOA[i].AcNo == "8" && MaxAccountCategory >= 8)
                            {
                                balance8 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                            }
                            if (mIsProfitLossShowInterOfficeAccount)
                            {
                                if (lCOA[i].AcNo == "9")
                                {
                                    balance9 = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                                }
                            }

                        }


                    }
                    else
                    {
                        if (lCOA[i].AcNo == "6" && MaxAccountCategory >= 6)
                        {
                            balance6 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                        }

                        if (lCOA[i].AcNo == "7" && MaxAccountCategory >= 7)
                        {
                            balance7 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                        }

                        if (lCOA[i].AcNo == "8" && MaxAccountCategory >= 8)
                        {
                            balance8 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                        }
                        if (mIsProfitLossShowInterOfficeAccount)
                        {
                            if (lCOA[i].AcNo == "9")
                            {
                                balance9 = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                            }
                        }
                    }
                }
            }

            if (mFormulaForComputeProfitLoss.Length == 0)
                ProfitLoss = (balance4 - (balance5 + balance6 + balance7 + balance8));
            else
            {
                string SQLFormula = mFormulaForComputeProfitLoss;
                char[] delimiters = { '+', '-','(',')'};
                string[] ArrayFormula = SQLFormula.Split(delimiters);
                for (int i = 0; i < ArrayFormula.Count(); i++)
                {
                    string oldS = ArrayFormula[i];
                    string newS = "0";
                    if (ArrayFormula[i].Length > 0)
                    {
                        if (oldS == "balance4")
                        {
                            newS = balance4.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        if (oldS == "balance5")
                        {
                            newS = balance5.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        if (oldS == "balance6")
                        {
                            newS = balance6.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        if (oldS == "balance7")
                        {
                            newS = balance7.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        if (oldS == "balance8")
                        {
                            newS = balance8.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        if (oldS == "balance9")
                        {
                            newS = balance9.ToString();
                            newS = newS.Replace(',', '.');
                        }
                        SQLFormula = SQLFormula.Replace(oldS, newS);
                    }
                }

                ProfitLoss = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));

            }
            TxtProfitLoss.EditValue = Sm.FormatNum(ProfitLoss, 0);
        }

        private void GetInterOfficeAccount(ref List<COAInterOffice> lCOAInterOffice)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.OptCode As ParentTarget, B.AcNo, B.Level, B.Parent ");
            SQL.AppendLine("From TblOption A ");
            SQL.AppendLine("Inner Join TblCOA B On A.Property1 = B.AcNo ");
            SQL.AppendLine("Where A.OptCat = 'ProfitLossInterOfficeAccount' ");
            SQL.AppendLine("And A.Property1 Is Not Null ");
            SQL.AppendLine("And A.Property1 Not In('All', 'Parent') ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent", "ParentTarget", "Level" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAInterOffice.Add(new COAInterOffice()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            ParentTarget = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrInt(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCOAInterOffice(ref List<COAInterOffice> lCOAInterOffice)
        {
            GetInterOfficeAccount(ref lCOAInterOffice);
            mCOAInterOffice2 = Sm.GetValue("Select OptCode From TblOption Where  OptCat = 'ProfitLossInterOfficeAccount' And Property1 = 'Parent'; ");
            string COAInterOffice3 = Sm.GetValue("Select Group_Concat(OptCode) From TblOption Where  OptCat = 'ProfitLossInterOfficeAccount'  And Property1 = 'All'; ");
            char[] delimiters = { ',' };
            mCOAInterOffice3 = COAInterOffice3.Split(delimiters);
            if (lCOAInterOffice.Count > 0)
            {
                foreach (var a in lCOAInterOffice.OrderBy(o => o.AcNo))
                {
                    bool IsFirst = true;
                    string AcNo = string.Empty;

                    if (IsFirst)
                    {
                        AcNo = a.AcNo;
                        IsFirst = false;
                    }

                    int delimiterCount = AcNo.Count(c => c == '.');
                    int lastIndexDelimiter = AcNo.LastIndexOf('.');

                    for (int i = delimiterCount; i >= 0; --i)
                    {
                        if (AcNo == a.ParentTarget)
                        {
                            mCOAInterOffice += ',';
                            mCOAInterOffice += AcNo;
                            break;
                        }
                        if(mCOAInterOffice.Length != 0)
                            mCOAInterOffice += ',';
                        mCOAInterOffice += AcNo;

                        if (i != 0)
                        {
                            AcNo = AcNo.Substring(0, lastIndexDelimiter);
                            lastIndexDelimiter = AcNo.LastIndexOf('.');
                        }

                    }


                }
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        private void LueLevelFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelFrom, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LueLevelTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelTo, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public string AcType { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public decimal PeriodDAmt { get; set; }
            public decimal PeriodCAmt { get; set; }
            public decimal PeriodBalance { get; set; }
            
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COAInterOffice
        {
            public string AcNo { get; set; }
            public string Parent { get; set; }
            public string ParentTarget { get; set; }
            public int Level { get; set; }
        }

        #endregion
    }
}
