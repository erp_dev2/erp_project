﻿#region Update
/*
    22/12/2017 [TKG] Filter department berdasarkan group
    19/03/2017 [TKG] Filter berdasarkan otorisasi group thd site
    10/07/2018 [TKG] tambah filter dan informasi local document#
    03/09/2018 [WED] tambah kolom Usage Date
    06/08/2019 [DITA] tambah material request dan po request remark
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom project code, project name
    27/05/2020 [TKG/IMS] tambah filter Local Document#, Item's Local Code
    03/09/2020 [WED/IMS] Project Code dan Project Name ganti link query
    27/20/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
    15/04/2021 [IBL/KIM] Data yg muncul sesuai dengan menu (POR / PORTender) berdasarkan parameter MenuCodeForPORTender dan IsMRWithTenderEnabled 
    29/09/2021 [AJH/PHT] Tambah filter ProcurementType berdasarkan parameter IsMRUseProcurementType 
    25/10/2021 [WED/RM] tambah informasi kolom Take Order berdasarkan parameter IsUseECatalog
    12/11/2021 [NJP/RM] tambah informasi kolom Vendor Remark berdasarkan parameter IsUseECatalog
    15/11/2021 [NJP/RM] Penyesuaian Header Nama Kolom di bagian find antara Material Request Remark dengan PO Request Remark Kebalik
    27/12/2021 [TYO/SIER] menambah kolom Division berdasarkan parameter IsPORShowDivisionInfo
    07/01/2021 [TYO/PHT] filter by item's category berdasarkan parameter IsFilterByItCt
    21/01/2022 [VIN/PHT] BUG filter site
    07/10/2022 [HPH/SIER] filter vendor berdasarkan kategori
    01/03/2023 [MAU/HEX] menampilkan kolom MR Type berdasarkan parameter IsMRUseApprovalSheet
    10/03/2023 [MAU/HEX] BUG Sumber MRType
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPORequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPORequestFind(FrmPORequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                //Sl.SetLueVdCode(ref LueVdCode);
                SetLueVdCode2(ref LueVdCode, string.Empty);
                Sl.SetLueOption(ref LueProcType, "ProcurementType");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySite?"Y":"N");
                ChkExcludedCancelledItem.Checked = true;


                if (!mFrmParent.mIsMRUseProcurementType)
                {
                    label10.Visible = false;
                    LueProcType.Visible = false;
                    ChkProcType.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, A.LocalDocNo, ");
            SQL.AppendLine("Case When B.Status = 'O' Then 'Outstanding' ");
            SQL.AppendLine("When B.Status = 'A' Then 'Approved' ");
            SQL.AppendLine("When B.Status = 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            if (mFrmParent.mIsSiteMandatory)
                SQL.AppendLine("L.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            SQL.AppendLine("B.MaterialRequestDocNo, E.DeptName, D.ItCode,");
            SQL.AppendLine("F.ItName, F.ForeignName, ");
            SQL.AppendLine("B.Qty, F.PurchaseUomCode, ");
            SQL.AppendLine("I.VdName, J.PtName, K.DTName, G.CurCode, H.UPrice, (B.Qty*H.UPrice) As Total, D.UsageDt, M.PropName, ");
            SQL.AppendLine("B.CreateBy, B.CreateDt, B.LastUpBy, B.LastUpDt, F.ItCodeInternal, ");
            SQL.AppendLine("ifnull(B.Remark, A.Remark) As PORRemark, ifnull(D.Remark, C.Remark) As MRRemark, F.Specification, ");

            if (mFrmParent.mIsUseECatalog) SQL.AppendLine("Case B.TakeOrderInd When 'Y' Then 'Yes' When 'N' Then 'No' Else '' End As TakeOrderInd, TakeOrderRemark, ");
            else SQL.AppendLine("Null As TakeOrderInd, Null AS TakeOrderRemark, ");

            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("O.ProjectCode, O.ProjectName, ");
            else
                SQL.AppendLine("Null as ProjectCode, Null As ProjectName, ");

            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("P.OptDesc as ProcurementType ");
            else
                SQL.AppendLine("Null as ProcurementType ");

            if (mFrmParent.mIsPORShowDivisionInfo)
                SQL.AppendLine(", Q.DivisionName Division ");
            else
                SQL.AppendLine(", Null As Division ");

            if (mFrmParent.mIsMRUseApprovalSheet)
                SQL.AppendLine(", case when C.EximInd='Y' then S.OptDesc when C.EximInd='N' then R.OptDesc ELSE '' END AS MRType");
            else
                SQL.AppendLine(", null as MRType");

            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");         
            if (mFrmParent.mIsMRWithTenderEnabled)
            {
                if (mFrmParent.mMenuCodeForPORTender)
                    SQL.AppendLine("    And C.TenderInd = 'Y' ");
                else
                    SQL.AppendLine("    And C.TenderInd = 'N' ");
            }
            if (mFrmParent.mIsPORequestFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And C.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=C.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo=D.DocNo And B.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr G On B.QtDocNo=G.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And G.PtCode Is Not Null ");
                SQL.AppendLine("And G.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl H On B.QtDocNo=H.DocNo And B.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblVendor I On G.VdCode=I.VdCode ");
            if (Sm.GetParameterBoo("IsFilterByVendorCategory"))
            {
                SQL.AppendLine("And Exists(");
                SQL.AppendLine("Select 1 From tblgroupvendorcategory");
                SQL.AppendLine("Where VdCtCode = I.VdCtCode");
                SQL.AppendLine("And GrpCode In(Select GrpCode From TblUser Where UserCode = @UserCode)");
                SQL.AppendLine(")");
            }
            SQL.AppendLine("Inner Join TblPaymentTerm J On G.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblDeliveryType K On G.DTCode=K.DTCode ");
            if (mFrmParent.mIsSiteMandatory)
                SQL.AppendLine("Left Join TblSite L On A.SiteCode=L.SiteCode ");
            SQL.AppendLine("Left Join TblProperty M On B.PropCode=M.PropCode ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                #region Old Code
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T1.DocNo, T8.DNo, Group_Concat(Distinct IfNull(T6.ProjectCode, T4.ProjectCode2)) ProjectCode, Group_Concat(Distinct IfNull(T6.ProjectName, T3.ProjectName)) ProjectName ");
                //SQL.AppendLine("    From TblBOMRevisionHdr T1 ");
                //SQL.AppendLine("    Inner Join TblBOQHdr T2 On T1.BOQDocNo = T2.DocNo ");
                //SQL.AppendLine("    Inner Join TblLOPHdr T3 ON T2.LOPDocNo = T3.DocNo ");
                //SQL.AppendLine("    Left Join TblSOContractHdr T4 On T2.DocNo = T4.BOQDocNo ");
                //SQL.AppendLine("    Left Join TblNoticeToProceed T5 On T3.DocNo = T5.LOPDocNo ");
                //SQL.AppendLine("    Left Join TblProjectGroup T6 On T3.PGCode = T6.PGCode ");
                //SQL.AppendLine("    Inner Join TblMaterialRequestDtl T7 ON T1.DocNo = T7.BOMRDocNo ");
                //SQL.AppendLine("    Inner Join TblBOMRevisionDtl T8 ON T1.DOcNo = T8.DocNo And T7.BOMRDNo = T8.DNo ");
                //SQL.AppendLine("    Group By T1.DocNo, T8.DNo ");
                //SQL.AppendLine(") O On D.BOMRDocNo = O.DOcNo And D.BOMRDNo = O.DNo ");
                #endregion

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T3.DocNo, T3.DNo, Group_Concat(Distinct IfNull(T7.ProjectCode, T4.ProjectCode2)) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T7.ProjectName, T6.ProjectName)) ProjectName ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.SOCDocNo Is not Null ");
                SQL.AppendLine("    Inner join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo ");
                SQL.AppendLine("        And T2.DNo = T3.MaterialRequestDNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T1.SOCDocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T5 On T4.BOQDocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T7 On T6.PGCode = T7.PGCode ");
                SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
                SQL.AppendLine(") O On B.DocNo = O.DocNo And B.DNo = O.DNo ");
            }
            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("Left Join TblOption P On P.OptCode=A.ProcurementType AND P.OptCat='ProcurementType' ");
            if (mFrmParent.mIsPORShowDivisionInfo)
                SQL.AppendLine("Left Join TblDivision Q On C.DivisionCode = Q.DivisionCode ");

            if (mFrmParent.mIsMRUseApprovalSheet)
            {
                SQL.AppendLine("Left Join tbloption R on C.MRType = R.OptCode and R.OptCat = 'MRType' ");
                SQL.AppendLine("Left join tbloption S on C.MRType = S.OptCode and S.OptCat = 'MRRoutineType' ");
            }
                
            
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");         
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            if (mFrmParent.mIsFilterByItCt)
            {
               SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory  ");
                SQL.AppendLine("    Where ItCtCode = F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 39;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Local"+Environment.NewLine+"Document#",
                    
                    //6-10
                    "Site",
                    ((mFrmParent.Doctitle=="IMS") ? "Purchase" : "Material")+Environment.NewLine+"Request#",
                    "Department",
                    "Item's Code",
                    "Item's Name",

                    //11-15
                    "Local Code",
                    "Foreign"+Environment.NewLine+"Name",
                    "Quantity",
                    "UoM",
                    "Vendor",

                    //16-20
                    "Currency",
                    "Price",
                    "Total",
                    "Term of"+Environment.NewLine+"Payment",
                    "Delivery"+Environment.NewLine+"Type",
                    
                    //21-25
                    "Usage Date",
                    "Property",
                    "Created By",
                    "Created Date", 
                    "Created Time", 
                    
                    //26-30
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time",
                    "PO Request"+Environment.NewLine+"Remark",
                    ((mFrmParent.Doctitle=="IMS") ? "Purchase Request" : "Material Request")+Environment.NewLine+"Remark",
                    

                    //31-35
                    "Specification",
                    "Project's Code",
                    "Project's Name",
                    "Procurement Type",
                    "Take Order",

                    //36-38
                    "Vendor Remark",
                    "Division",
                    "MR Type"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 100, 150,  
                    
                    //6-10
                    150, 150, 200, 120, 250, 
                    
                    //11-15
                    130, 200, 80, 80, 200, 
                    
                    //16-20
                    60, 100, 120, 150, 150, 
                    
                    //21-25
                    120, 200, 130, 130, 130, 
                    
                    //26-30
                    130, 130, 130, 200, 200,

                    //31-35
                    300, 130, 200 ,120, 100,

                    //36-38
                    200, 180, 180
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 17, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24, 27 });
            Sm.GrdFormatTime(Grd1, new int[] { 25, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 22 }, mFrmParent.mPORequestPropCodeEnabled);
            Sm.GrdColInvisible(Grd1, new int[] { 21, 23, 24, 25, 26, 27, 28, 37 }, false);
            if (!mFrmParent.mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 35, 36 });
            else
            {
                Grd1.Cols[35].Move(21);
                Grd1.Cols[36].Move(22);
            }
            if (!mFrmParent.mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            if (mFrmParent.mPORequestPropCodeEnabled) Grd1.Cols[22].Move(13);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33 });
            if (!mFrmParent.mIsMRUseProcurementType) Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            Sm.SetGrdProperty(Grd1 , false);
            Grd1.Cols[29].Move(21);
            Grd1.Cols[30].Move(22);
            Grd1.Cols[31].Move(14);
            Grd1.Cols[34].Move(5);
            if (mFrmParent.mIsPORShowDivisionInfo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 37 }, true);
                Grd1.Cols[37].Move(8);
            }
            Sm.GrdColInvisible(Grd1, new int[] { 38 }, mFrmParent.mIsMRUseApprovalSheet);
            if (mFrmParent.mIsMRUseApprovalSheet)
                Grd1.Cols[38].Move(30);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 21, 23, 24, 25, 26, 27, 28 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And B.Status In ('O', 'A') And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtMaterialRequestDocNo.Text, "B.MaterialRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "G.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "F.ItName", "F.ItCodeInternal", "F.ForeignName" });

                if (mFrmParent.mIsMRUseProcurementType)
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProcType), "A.ProcurementType", true); ;

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "LocalDocNo", "SiteName", 
                        
                        //6-10
                        "MaterialRequestDocNo", "DeptName", "ItCode", "ItName", "ItCodeInternal", 
                        
                        //11-15
                        "ForeignName", "Qty", "PurchaseUomCode", "VdName", "CurCode", 
                        
                        //16-20
                        "UPrice", "Total", "PtName", "DTName", "UsageDt", 
                        
                        //21-25
                        "PropName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                        //26-30
                        "PORRemark", "MRRemark", "Specification", "ProjectCode", "ProjectName",

                        //31-35
                        "ProcurementType", "TakeOrderInd", "TakeOrderRemark", "Division", "MRType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);    
                Cursor.Current = Cursors.Default;
            }
        }



      internal void SetLueVdCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.VdCode As Col1 ");
            if (Sm.GetParameterBoo("IsVendorComboShowCategory"))
                SQL.AppendLine(", CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2 ");
            else
                SQL.AppendLine(", T.VdName As Col2 ");
            SQL.AppendLine("From TblVendor T ");
            SQL.AppendLine("LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode ");
            //SQL.AppendLine("inner JOIN tblgroupvendorcategory T3 ON T.VdCtCode = T3.VdCtCode");
            
            if (Code.Length > 0) 
            {
                SQL.AppendLine("Where T.VdCode=@Code ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (Sm.GetParameterBoo("IsFilterByVendorCategory"))
                {                    
                    SQL.AppendLine("And Exists(");
                    SQL.AppendLine("Select 1 From tblgroupvendorcategory");
                    SQL.AppendLine("Where VdCtCode = T.VdCtCode");
                    SQL.AppendLine("And GrpCode In(Select GrpCode From TblUser Where UserCode = @UserCode)");
                    SQL.AppendLine(");");
                }
            }

            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupDepartment ");
            SQL.AppendLine("    Where DeptCode=T.DeptCode ");
            SQL.AppendLine("    And GrpCode In ( ");
            SQL.AppendLine("        Select GrpCode From TblUser ");
            SQL.AppendLine("        Where UserCode=@UserCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
       
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtMaterialRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMaterialRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, (mFrmParent.Doctitle == "IMS" ? "Purchase request#" : "Material request#"));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsPORequestFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {

            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode2), string.Empty); //Sl.SetLueVdCode2
            Sm.FilterLueSetCheckEdit(this, sender);

        }

        //private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        //{
        //    Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        //    Sm.FilterLueSetCheckEdit(this, sender);
        //}

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySite?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkProcType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Procurement Type");
        }

        private void LueProcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcType, new Sm.RefreshLue2(Sl.SetLueOption),"ProcurementType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion


        #endregion


    }
}
