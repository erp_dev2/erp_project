﻿#region Update
/*
    27/11/2019 [VIN/IMS] tambah kolom project code, project name, customer PO# berdasarkan parameter mIsBOMShowSpecifications
    31/01/2020 [TKG/IMS] hilangkan kolom project code, project name, customer PO#
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt3Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOCt3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOCt3Find(FrmDOCt3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.RecvCtDocNo, C.WhsName, D.CtName, B.ItCode, E.ItCodeInternal, E.ItName, E.ForeignName, ");
            SQL.AppendLine("G.ItGrpName, F.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, B.Qty, E.InventoryUOMCode, B.Qty2, E.InventoryUOMCode2, B.Qty3, E.InventoryUOMCode3, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            //if (mFrmParent.mIsBOMShowSpecifications)
            //{
            //    SQL.AppendLine(" ,IFNULL(P.ProjectName, N.ProjectName) projectname");
            //    SQL.AppendLine(",IFNULL(P.ProjectCode, L.ProjectCode2) projectcode");
            //    SQL.AppendLine(",IFNULL(L.PONo, O.DocNo) CustomerPO");
            //}
            //else
            //{
            //    SQL.AppendLine("Null As ProjectName, Null As ProjectCode, Null As CustomerPO ");
            //}
            SQL.AppendLine("From TblDOCt3Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt3Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode = D.CtCode And D.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode = E.ItCode ");
            SQL.AppendLine("Left Join TblProperty F On B.PropCode = F.PropCode ");
            SQL.AppendLine("Left Join TblItemGroup G On E.ItGrpCode = G.ItGrpCode ");
            SQL.AppendLine("Left Join TblProperty F2 On B.PropCode = F2.PropCode ");
            SQL.AppendLine("Left Join TblItemGroup G2 On E.ItGrpCode = G2.ItGrpCode ");
            //if (mFrmParent.mIsBOMShowSpecifications)
            //{
            //    SQL.AppendLine("LEFT JOIN tblrecvcthdr H ON H.WhsCode=C.WhsCode");
            //    SQL.AppendLine("LEFT JOIN tbldocthdr I ON H.DOCtDocNo=I.DocNo");
            //    SQL.AppendLine("LEFT JOIN tbldrhdr J ON I.CtCode=J.CtCode");
            //    SQL.AppendLine("LEFT JOIN tblsocontractrevisionhdr K ON K.DocType=J.DocType");
            //    SQL.AppendLine("LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo");
            //    SQL.AppendLine("LEFT JOIN tblboqhdr M ON L.BOQDocNo=M.DocNo");
            //    SQL.AppendLine("LEFT JOIN tbllophdr N ON N.DocNo=M.LOPDocNo");
            //    SQL.AppendLine("LEFT JOIN tblnoticetoproceed O on O.LOPDocNo=N.DocNo");
            //    SQL.AppendLine("LEFT JOIN tblprojectgroup P ON P.PGCode=N.PGCode");
            //}

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Received#",
                        "",

                        //6-10
                        "Warehouse",
                        "Customer",
                        "Item's Code",
                        "",
                        "Local Code",

                        //11-15
                        "Item's Name",
                        "Foreign Name",
                        "Group",
                        "Property",
                        "Batch#",

                        //16-20
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",

                        //21-25
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Created By",

                        //26-30
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time"

                        ////31-33
                        //"Project Name",
                        //"Project Code",
                        //"Customer PO"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        160, 80, 50, 150, 20, 
                        
                        //6-10
                        200, 200, 100, 20, 100, 
                        
                        //11-15
                        200, 200, 160, 180, 200, 
                        
                        //16-20
                        170, 60, 60, 80, 80, 
                        
                        //21-25
                        80, 80, 80, 80, 130, 
                        
                        //26-30
                        130, 130, 130, 130, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 9 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 19, 21, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 26, 29 });
            Sm.GrdFormatTime(Grd1, new int[] { 27, 30 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 12, 13, 14, 16, 17, 18, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 12, 13, 14, 16, 17, 18, 25, 26, 27, 28, 29, 30 }, !ChkHideInfoInGrd.Checked);

            if (mFrmParent.mNumberOfInventoryUomCode == 1)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
            
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, true);
        }

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    IsFilterByDateInvalid()
                    ) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "RecvCtDocNo", "WhsName", "CtName", 
                            
                            //6-10
                            "ItCode", "ItCodeInternal", "ItName", "ForeignName", "ItGrpName", 
                            
                            //11-15
                            "PropName", "BatchNo", "Source", "Lot", "Bin", 
                            
                            //16-20
                            "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2",  "Qty3", 
                            
                            //21-25
                            "InventoryUomCode3", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 30, 25);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count <= 1)
                    Sm.FocusGrd(Grd1, 0, 0);
                else
                    Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f1 = new FrmRecvCt(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f2 = new FrmItem(mFrmParent.mMenuCode);
                f2.Tag = mFrmParent.mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f2.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f1 = new FrmRecvCt(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f2 = new FrmItem(mFrmParent.mMenuCode);
                f2.Tag = mFrmParent.mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f2.ShowDialog();
            }
        }
        
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

    }
}
