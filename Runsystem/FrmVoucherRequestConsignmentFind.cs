﻿#region Update
/*
    23/12/2020 [IBL/SRN] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestConsignmentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucherRequestConsignment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestConsignmentFind(FrmVoucherRequestConsignment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, B.BillNo, B.Amt, C.WhsName, D.VdName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblVoucherRequestConsignmentHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestConsignmentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select 'all' As WhsCode, 'ALL' As WhsName ");
	        SQL.AppendLine("    Union All ");
	        SQL.AppendLine("    Select WhsCode, WhsName From TblWarehouse "); 
            SQL.AppendLine(")C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Inner Join TblVendor D On A.VdCode = D.VdCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel", 
                    "PPBK#",
                    "Amount",
                    
                    //6-10
                    "Warehouse",
                    "Vendor",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time",

                    //11-13
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 50, 100, 120, 
                    
                    //6-10
                    120, 180, 100, 100, 100, 

                    //11-13
                    100, 100, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtBillNo.Text, new string[] { "B.BillNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "C.WhsCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[]
                    {
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "CancelInd", "BillNo", "Amt", "WhsName", 
                        
                        //6-10
                        "VdName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueWhsCode2(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'all' As Col1, 'ALL' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Distinct B.WhsCode As Col1, B.WhsName As Col2 ");
            SQL.AppendLine("From " + mFrmParent.mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Order By Col2;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_ChecekdChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtBillNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBillNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PBBK#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode2));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #endregion
    }
}
