﻿#region Update
/*
    26/04/2018 [TKG] Outgoing payment difilter berdasarkan otorisasi group thd departemen yg diambil dari PI (Y=Difilter. N=Tidak difilter)
    07/08/2018 [TKG] tambah status
    22/08/2018 [DITA] BUG Saat find filter berdasarkan vendor
    26/01/2020 [RDH/IMS] tambah kolom local docno
    17/05/2021 [BRI/ALL] tambah kolom voucher request
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPaymentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOutgoingPayment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmOutgoingPaymentFind(FrmOutgoingPayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            //SQL.AppendLine("Case A.Status ");
            //SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            //SQL.AppendLine("    When 'A' Then 'Approved' ");
            //SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            //SQL.AppendLine("End As StatusDesc, ");
            //SQL.AppendLine("C.VdName, A.CurCode, B.InvoiceDocNo, D.VdInvNo, ");
            //SQL.AppendLine("Case B.InvoiceType When '1' Then D.CurCode Else E.CurCode End As CurCode, B.Amt, F.VoucherDocNo, ");
            //SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            //SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            //SQL.AppendLine("Left Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            //SQL.AppendLine("Left Join TblPurchaseInvoiceHdr D On B.InvoiceDocNo=D.DocNo And B.InvoiceType='1' ");
            //SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr E On B.InvoiceDocNo=E.DocNo And B.InvoiceType='2' ");
            //SQL.AppendLine("Left Join TblVoucherRequestHdr F On A.VoucherRequestDocNo=F.DocNo ");
            //SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            //if (mFrmParent.mIsOutgoingPaymentFilterByPIDept)
            //{
            //    SQL.AppendLine("And A.DocNo In (");
            //    SQL.AppendLine("    Select T1.DocNo ");
            //    SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
            //    SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            //    SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo ");
            //    SQL.AppendLine("        And (T3.DeptCode Is Null ");
            //    SQL.AppendLine("        Or (T3.DeptCode Is Not Null ");
            //    SQL.AppendLine("    And Exists( ");
            //    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
            //    SQL.AppendLine("        Where DeptCode=IfNull(T3.DeptCode, '') ");
            //    SQL.AppendLine("        And GrpCode In ( ");
            //    SQL.AppendLine("            Select GrpCode From TblUser ");
            //    SQL.AppendLine("            Where UserCode=@UserCode ");
            //    SQL.AppendLine("            ) ");
            //    SQL.AppendLine("    ))) ");
            //    SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine("    Union All ");
            //    SQL.AppendLine("    Select T1.DocNo ");
            //    SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
            //    SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            //    SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine(") ");
            //}

            SQL.AppendLine("Select Distinct DocNo, DocDt, LocalDocNo, CancelInd, StatusDesc, ");
            SQL.AppendLine("VdName, VdCode, CurCode, InvoiceDocNo, VdInvNo, Amt, VoucherRequestDocNo, VoucherDocNo, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.CancelInd, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("C.VdName, C.VdCode, A.CurCode, B.InvoiceDocNo, ");
            SQL.AppendLine("D.VdInvNo, ");
            //SQL.AppendLine("D.CurCode, ");
            SQL.AppendLine("B.Amt, A.VoucherRequestDocNo, E.VoucherDocNo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo And B.InvoiceType='1' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr D On B.InvoiceDocNo=D.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (D.SiteCode Is Null Or (D.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsOutgoingPaymentFilterByPIDept)
            {
                SQL.AppendLine("And A.DocNo In (");
                SQL.AppendLine("    Select T1.DocNo ");
                SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
                SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo ");
                SQL.AppendLine("        And (T3.DeptCode Is Null ");
                SQL.AppendLine("        Or (T3.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(T3.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ))) ");
                SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T1.DocNo ");
                SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
                SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
                SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.CancelInd, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("C.VdName, C.VdCode, A.CurCode, B.InvoiceDocNo, ");
            SQL.AppendLine("Null As VdInvNo, ");
            //SQL.AppendLine("D.CurCode, ");
            SQL.AppendLine("B.Amt, A.VoucherRequestDocNo, E.VoucherDocNo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On A.DocNo=B.DocNo And B.InvoiceType='2' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblPurchaseReturnInvoiceHdr D On B.InvoiceDocNo=D.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And D.DocNo In (");
                SQL.AppendLine("    Select T.DocNo From TblPurchaseInvoiceHdr T Where 1=1 ");
                SQL.AppendLine("And (T.SiteCode Is Null Or (T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine(") Tbl ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Local Document#",
                        "Status",
                        "Cancel",                       
                        
                        //6-10
                        "Vendor",
                        "Currency",
                        "Invoice#",
                        "Vendor's Invoice",
                        "Invoice"+Environment.NewLine+"Amount",                        
                        
                        //11-15
                        "Voucher Request#",
                        "Voucher#",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",

                        //16 - 18
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 130 , 80, 60,  
                        
                        //6-10
                        200, 60, 150, 170, 120,  
                        
                        //11-15
                        130, 130, 100, 100, 100,

                        //16 - 18
                        100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 13, 14, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtInvoiceDocNo.Text, "InvoiceDocNo", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "StatusDesc", "CancelInd", "VdName", 
                            
                            //6-10
                            "CurCode", "InvoiceDocNo", "VdInvNo", "Amt", "VoucherRequestDocNo",
                            
                            //11-15
                            "VoucherDocNo", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtInvoiceDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvoiceDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }

        #endregion

        #endregion
    }
}
