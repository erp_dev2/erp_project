﻿#region Update
/*
    18/10/2019 [TKG/SIER] New Application
    11/11/2019 [TKG/SIER] ditambah site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJob : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool mIsFilterBySite = false;
        internal FrmJobFind FrmFind;

        #endregion

        #region Constructor

        public FrmJob(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                Sl.SetLueJobCtCode(ref LueJobCtCode);
                Sl.SetLueUomCode(ref LueUomCode);
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] { "", "Site Code", "Site Name" },
                    new int[] { 20, 0, 250 }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtJobCode, TxtJobName, LueJobCtCode, LueUomCode }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
                    TxtJobCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtJobCode, TxtJobName, LueJobCtCode, LueUomCode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    Sm.GrdColInvisible(Grd1, new int[] { 0 }, true);
                    TxtJobCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtJobName, LueJobCtCode, LueUomCode }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    Sm.GrdColInvisible(Grd1, new int[] { 0 }, true);
                    TxtJobName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtJobCode, TxtJobName, LueJobCtCode, LueUomCode, TxtCurCode });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtUPrice }, 0);
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJobFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            ChkActInd.Checked = true;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Delete From TblJob Where JobCode=@JobCode; "  +
                        "Delete From TblJobSite Where JobCode=@JobCode; "
                };
                Sm.CmParam<String>(ref cm, "@JobCode", TxtJobCode.Text);

                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var cml = new List<MySqlCommand>();

                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

                SQL.AppendLine("Insert Into TblJob(JobCode, JobName, JobCtCode, UomCode, ActInd, CurCode, UPrice, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@JobCode, @JobName, @JobCtCode, @UomCode, @ActInd, null, 0.00, @UserCode, @Dt) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update JobName=@JobName, JobCtCode=@JobCtCode, UomCode=@UomCode, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=@Dt; ");

                if (TxtJobCode.Properties.ReadOnly)
                    SQL.AppendLine("Delete From tblJobSite Where JobCode=@JobCode; ");

                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        {
                            SQL.AppendLine("Insert Into TblJobSite(JobCode, SiteCode, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                            SQL.AppendLine("Select JobCode, @SiteCode" + r + ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                            SQL.AppendLine("From TblJob Where JobCode=@JobCode; ");

                            Sm.CmParam<String>(ref cm, string.Concat("@SiteCode", r), Sm.GetGrdStr(Grd1, r, 1));
                        }
                    }
                    
                }
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@JobCode", TxtJobCode.Text);
                Sm.CmParam<String>(ref cm, "@JobName", TxtJobName.Text);
                Sm.CmParam<String>(ref cm, "@JobCtCode", Sm.GetLue(LueJobCtCode));
                Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                cml.Add(cm);
                Sm.ExecCommands(cml);

                ShowData(TxtJobCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string JobCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowJob(JobCode);
                ShowJobSite(JobCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJob(string JobCode)
        {
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@JobCode", JobCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select JobCode, JobName, JobCtCode, UomCode, ActInd, CurCode, UPrice From TblJob Where JobCode=@JobCode;",
                    new string[] 
                    {
                        "JobCode", 
                        "JobName", "JobCtCode", "UomCode", "ActInd", "CurCode",
                        "UPrice"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtJobCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtJobName.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetLue(LueJobCtCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueUomCode, Sm.DrStr(dr, c[3]));
                        ChkActInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, true
                );
        }

        private void ShowJobSite(string JobCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SiteCode, B.SiteName ");
            SQL.AppendLine("From TblJobSite A, TblSite B ");
            SQL.AppendLine("Where A.SiteCode=B.SiteCode And A.JobCode=@JobCode; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@JobCode", JobCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] { "SiteCode", "SiteName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtJobCode, "Job's code", false) ||
                Sm.IsTxtEmpty(TxtJobName, "Job's name", false) ||
                Sm.IsLueEmpty(LueJobCtCode, "Category") ||
                Sm.IsLueEmpty(LueUomCode, "UoM") ||
                IsCodeExisted();
        }

        private bool IsCodeExisted()
        {
            if (TxtJobCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                    "Select 1 From TblJob Where JobCode=@Param Limit 1;",
                    TxtJobCode.Text,
                    "Job code ( " + TxtJobCode.Text + " ) already existed.");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtJobCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJobCode);
        }

        private void TxtJobName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJobName);
        }

        private void LueJobCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueJobCtCode, new Sm.RefreshLue1(Sl.SetLueJobCtCode));
        }

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmJobDlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmJobDlg(this));
        }

        #endregion

        #endregion
    }
}
