﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmQualification : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mStateInd = 0;
        internal FrmQualificationFind FrmFind;

        #endregion

        #region Constructor

        public FrmQualification(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmQualification");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtQualificationCode, TxtQualificationName, ChkActInd, TxtMaxAmt, TxtMinAmt
                    }, true);
                    TxtQualificationCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtQualificationCode, TxtQualificationName, TxtMaxAmt, TxtMinAmt
                    }, false);
                    TxtQualificationCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtQualificationCode, true);
                    if (Sm.IsDataExist("Select QualificationCode From TblQualification Where QualificationCode = @Param And ActInd = 'Y'; ", TxtQualificationCode.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtQualificationName, ChkActInd, TxtMaxAmt, TxtMinAmt }, false);
                    TxtQualificationName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateInd = 0;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtQualificationCode, TxtQualificationName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtMaxAmt, TxtMinAmt }, 0);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmQualificationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateInd = 1;
            ChkActInd.Checked = true;
            SetFormControl(mState.Insert);
            TxtMaxAmt.Text = Sm.FormatNum(0m, 0);
            TxtMinAmt.Text = Sm.FormatNum(0m, 0);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtQualificationCode, "", false)) return;

            if (Sm.IsDataExist("Select QualificationCode From TblQualification Where QualificationCode = @Param And ActInd = 'Y'; ", TxtQualificationCode.Text))
            {
                mStateInd = 2;
                SetFormControl(mState.Edit);
            }
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtQualificationCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    var cm = new MySqlCommand() { CommandText = "Delete From TblQualification Where QualificationCode=@QualificationCode" };
            //    Sm.CmParam<String>(ref cm, "@QualificationCode", TxtQualificationCode.Text);
            //    Sm.ExecCommand(cm);

            //    BtnCancelClick(sender, e);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            InsertData();
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string QualificationCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@QualificationCode", QualificationCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblQualification Where QualificationCode=@QualificationCode",
                        new string[] 
                        {
                            "QualificationCode", 
                            "QualificationName", "ActInd", "MinAmt", "MaxAmt"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtQualificationCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtQualificationName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            TxtMinAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                            TxtMaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtQualificationCode, "Qualification code", false) ||
                Sm.IsTxtEmpty(TxtQualificationName, "Qualification name", false) ||
                Sm.IsTxtEmpty(TxtMinAmt, "Min. Amount", false) ||
                Sm.IsTxtEmpty(TxtMaxAmt, "Max. Amount", true) ||
                (mStateInd == 1 && IsQualificationCodeExisted()) ||
                (mStateInd == 2 && IsDataAlreadyInactive());
        }

        private bool IsDataAlreadyInactive()
        {
            if (Sm.IsDataExist("Select QualificationCode From TblQualification Where QualificationCode = @Param And ActInd = 'N'; ", TxtQualificationCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                TxtQualificationCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsQualificationCodeExisted()
        {
            if (!TxtQualificationCode.Properties.ReadOnly && Sm.IsDataExist("Select QualificationCode From TblQualification Where QualificationCode='" + TxtQualificationCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Qualification code ( " + TxtQualificationCode.Text + " ) is exists.");
                return true;
            }
            return false;
        }

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                if (mStateInd == 1)
                {
                    SQL.AppendLine("Insert Into TblQualification(QualificationCode, QualificationName, ActInd, MinAmt, MaxAmt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@QualificationCode, @QualificationName, @ActInd, @MinAmt, @MaxAmt, @UserCode2, CurrentDateTime()); ");
                }
                else if (mStateInd == 2)
                {
                    SQL.AppendLine("Update TblQualification Set ");
                    SQL.AppendLine("    QualificationName = @QualificationName, ActInd = @ActInd, MinAmt = @MinAmt, MaxAmt = @MaxAmt, LastUpBy = @UserCode2, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("Where QualificationCode = @QualificationCode And ActInd = 'Y'; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@QualificationCode", TxtQualificationCode.Text);
                Sm.CmParam<String>(ref cm, "@QualificationName", TxtQualificationName.Text);
                Sm.CmParam<Decimal>(ref cm, "@MinAmt", Decimal.Parse(TxtMinAmt.Text));
                Sm.CmParam<Decimal>(ref cm, "@MaxAmt", Decimal.Parse(TxtMaxAmt.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtQualificationCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtQualificationCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtQualificationCode);
        }

        private void TxtQualificationName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtQualificationName);
        }

        private void TxtMinAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMinAmt, 0);
        }

        private void TxtMaxAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMaxAmt, 0);
        }

        #endregion

        #endregion
    }
}
