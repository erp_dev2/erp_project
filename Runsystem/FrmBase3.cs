﻿#region Update
/*
    22/05/2018 [TKG] tambah tombol excel
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    18/10/2022 [HAR/AMKA] tambah parameter untuk mengaktifkan warning payment
    20/04/2023 [WED/ALL] activity log di simpan berdasarkan parameter IsActivityLogStored
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBase3 : Form
    {

        public FrmBase3()
        {
            InitializeComponent();
        }

        #region Method

        #region Standard Method
        
        virtual protected void HideInfoInGrd()
        {

        }

        #endregion

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            
            
        }

        #endregion

        #region Button Method

        virtual protected void BtnFindClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnInsertClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnEditClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnDeleteClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnSaveClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
        }

        virtual protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        virtual protected void BtnExcelClick(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        virtual protected void ExportToExcel()
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #region Grid Method

        virtual protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        virtual protected void GrdKeyDown(object sender, KeyEventArgs e)
        {

        }

        virtual protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        virtual protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

        }

        virtual protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {

        }

        virtual protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase3_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        private void FrmBase3_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BtnSave.Enabled &&
                Sm.StdMsgYN("Question",
                    "Data not been saved." + Environment.NewLine +
                    "Do you want to close application ?") == DialogResult.No)
                e.Cancel = true;
            else
                FrmClosing(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnFind_Click(object sender, EventArgs e)
        {
            BtnFindClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "F";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            BtnInsertClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "I";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            BtnEditClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "E";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            BtnDeleteClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "D";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (Sm.GetParameterBoo("WMenu"))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't save this data." + Environment.NewLine +
                    "Please contact finance department.");
            }
            else
            {
                BtnSaveClick(sender, e);

                if (Gv.IsActivityLogStored)
                {
                    //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                    var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                    //string FrmName = o_name.ToString();
                    string MenuCode = o_tag.ToString();
                    string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                    string CtCode = Sm.GetParameter("DocTitle");
                    string DbName = Gv.Database;
                    string DbHost = Gv.Server;
                    string Action = "S";

                    Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            BtnCancelClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "C";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            BtnPrintClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "N";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            BtnExcelClick(sender, e);

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "X";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        #endregion

        #region Misc Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                //Sm.SetGrdAutoSize(Grd1);
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(sender, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(sender, e);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            GrdEllipsisButtonClick(sender, e);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(sender, e);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdColHdrDoubleClick(sender, e);
        }

        
        private void Grd1_RequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            GrdRequestColHdrToolTipText(sender, e);
        }

        #endregion

        #endregion
    }
}
