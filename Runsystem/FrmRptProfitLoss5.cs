﻿#region Update
/*
    23/04/2020 [WED/VIR] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss5 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAccountingRptStartFrom = string.Empty,
            mDocTitle = string.Empty,
            mCurrentEarningFormulaType = string.Empty,
            mMaxAccountCategory = "9",
            mDefaultBalanceInProfitLoss = "0",
            mSQL = string.Empty,
            mTwoYearsAgo = string.Empty,
            mOneYearAgo = string.Empty,
            mThisYear = string.Empty,
            mType = string.Empty
            ;

        private bool
            mIsEntityMandatory = false,
            mIsRptProfitLossUseFilterPeriod = false,
            mIsJournalCostCenterEnabled = false;
        
        private int[] mGrdColDec = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

        #endregion

        #region Constructor

        public FrmRptProfitLoss5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueMth1(ref LueMth1);
                SetLueMth2(ref LueMth2);
                SetTimeStampVariable();
                SetGrd();
                GetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mDocTitle = Sm.GetParameter("DocTitle");
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
            mCurrentEarningFormulaType = Sm.GetParameter("CurrentEarningFormulaType");
            if (mCurrentEarningFormulaType.Length == 0) mCurrentEarningFormulaType = "3";
            mDefaultBalanceInProfitLoss = Sm.GetParameter("DefaultBalanceInProfitLoss");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptProfitLossUseFilterPeriod', 'IsJournalCostCenterEnabled', 'IsEntityMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptProfitLossUseFilterPeriod": mIsRptProfitLossUseFilterPeriod = ParValue == "Y"; break;
                            case "IsJournalCostCenterEnabled": mIsJournalCostCenterEnabled = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT *FROM TblRptDescription where doctype = 'ProfitLoss' order by description1");
            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblFicoSettingJournalToCBPHdr ");
            SQL.AppendLine("Where Doctype = 'ProfitLoss' ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("Order By Right(Concat('00000000', Sequence ), 8), Code ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "URAIAN",
                    "DETAIL", 
                    "AUDIT "+ mTwoYearsAgo,
                    "% "+ mTwoYearsAgo,
                    "RKAP"+Environment.NewLine+"TAHUN " + mThisYear,

                    //6-10
                    "%",
                    "RKAP"+Environment.NewLine + mType + Environment.NewLine+"TAHUN " + mThisYear, 
                    "%", 
                    mType + Environment.NewLine+"TAHUN " + mOneYearAgo, 
                    "%", 
                   
                    //11-15
                    mType + Environment.NewLine+"TAHUN " + mThisYear, 
                    "%",
                    "PERBANDINGAN %",
                    "DocType",
                    "Code"
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    300, 300, 200, 200, 200,  
                    
                    //6-10
                    200, 200, 200, 200, 200,

                    //11-15
                    200, 200, 200, 0, 0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15 });
            Sm.GrdFormatDec(Grd1, mGrdColDec, 0);
        }

        override protected void HideInfoInGrd()
        {
            if (mIsRptProfitLossUseFilterPeriod)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth1, "Month")
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            Sm.ClearGrd(Grd1, false);
            SetTimeStampVariable();
            SetGrd();
            
            try
            {
                var cm = new MySqlCommand();
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "Description1",
                        //1-5
                        "Description2", "Description3", "Description4", "DocType", "Code"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        for (int i = 0; i < mGrdColDec.Count(); ++i)
                        {
                            Grd.Cells[Row, mGrdColDec[i]].Value = 0m;
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 5);
                    }, true, false, false, false
                );
                ProcessData();
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, mGrdColDec);
        }

        #endregion

        #region Additional Method

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            if (mThisYear.Length > 0)
            {
                mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
                mTwoYearsAgo = (Int32.Parse(mThisYear) - 2).ToString();
            }

            string mStartDt = string.Empty;
            string mEndDt = string.Empty;
            string mQuartal = string.Empty;

            if (Sm.GetDte(DteDocDt1).Length > 0)
            {
                mThisYear = Sm.GetDte(DteDocDt1).Substring(0, 4);
                mOneYearAgo = (Int32.Parse(mThisYear) - 1).ToString();
                mTwoYearsAgo = (Int32.Parse(mThisYear) - 2).ToString();
                mStartDt = Sm.GetValue("Select Date_Format(@Param, '%d/%m/%Y') ", Sm.Left(Sm.GetDte(DteDocDt1), 8));
                mEndDt = Sm.GetValue("Select Date_Format(@Param, '%d/%m/%Y') ", Sm.Left(Sm.GetDte(DteDocDt2), 8));
                mType = mStartDt + " s.d " + mEndDt;
            }
            else
            {
                if (Sm.GetLue(LueMth1) == "01") mQuartal = "1";
                if (Sm.GetLue(LueMth1) == "04") mQuartal = "2";
                if (Sm.GetLue(LueMth1) == "07") mQuartal = "3";
                if (Sm.GetLue(LueMth1) == "10") mQuartal = "4";
                mType = "TRIWULAN " + mQuartal;
            }
        }

        private void SetLookUpEdit(DXE.LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        private void SetLueMth1(ref DXE.LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "01", "04", "07", "10" });
        }

        private void SetLueMth2(ref DXE.LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "03", "06", "09", "12" });
        }

        private void ProcessData()
        {
            if (Sm.GetDte(DteDocDt1).Length > 0) Process1();
            else Process2();
        }

        private void Process1()
        {
            var l = new List<ProfitLoss>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string mPeriodYr1 = Sm.Left(Sm.GetDte(DteDocDt1), 4);
            string mPeriodYr2 = Sm.Left(Sm.GetDte(DteDocDt2), 4);

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(
                (Int32.Parse(mPeriodYr1) - 2).ToString(), Sm.GetDte(DteDocDt1).Substring(4, 4))
                );
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(
                (Int32.Parse(mPeriodYr2) - 2).ToString(), Sm.GetDte(DteDocDt2).Substring(4, 4))
                );
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(
                (Int32.Parse(mPeriodYr1) - 1).ToString(), Sm.GetDte(DteDocDt1).Substring(4, 4))
                );
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(
                (Int32.Parse(mPeriodYr2) - 1).ToString(), Sm.GetDte(DteDocDt2).Substring(4, 4))
                );

            SQL.AppendLine("SELECT T.DocType, T.Code, SUM(T.AuditedAmt) AuditedAmt, ");
            SQL.AppendLine("SUM(T.AuditedAmt1Old) AuditedAmt1Old, Sum(T.AuditedAmt1) AuditedAmt1, ");
            SQL.AppendLine("Sum(T.RKAP) RKAP, SUM(T.RKAPQuarter) RKAPQuarter ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT D.DocType, D.Code, ");
            SQL.AppendLine("    IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00) AuditedAmt, ");
            SQL.AppendLine("    0.00 AuditedAmt1Old, 0.00 AuditedAmt1, 0.00 RKAP, 0.00 RKAPQuarter ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.DocDt BETWEEN @DocDt3 AND @DocDt4 ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
                
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, 0.00 AuditedAmt, ");
            SQL.AppendLine("    IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00) AuditedAmt1Old, ");
            SQL.AppendLine("    0.00 AuditedAmt1, 0.00 RKAP, 0.00 RKAPQuarter ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.DocDt BETWEEN @DocDt5 AND @DocDt6 ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
                
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, 0.00 AuditedAmt, 0.00 AuditedAmt1Old, ");
            SQL.AppendLine("    IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00) AuditedAmt1, ");
            SQL.AppendLine("    0.00 RKAP, 0.00 RKAPQuarter ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.DocDt BETWEEN @DocDt1 AND @DocDt2 AND A.CancelInd = 'N' ");    
            SQL.AppendLine("    UNION ALL ");
            
            SQL.AppendLine("    SELECT D.DocType, D.Code, 0.00 AuditedAmt, 0.00 AuditedAmt1Old, 0.00 AuditedAmt1, ");
            SQL.AppendLine("    B.Amt RKAP, ");
            SQL.AppendLine("    (B.Amt01) RKAPQuarter ");
            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl C ON C.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr D ON C.DocType = D.DocType ");
            SQL.AppendLine("        AND C.Code = D.Code AND D.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.Yr = LEFT(@DocDt1, 4) And A.CancelInd = 'N' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.Code; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", mThisYear);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocType",
                    
                    //1-5
                    "Code",
                    "AuditedAmt",
                    "AuditedAmt1Old",
                    "AuditedAmt1",
                    "RKAP",

                    //6
                    "RKAPQuarter"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProfitLoss()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            AuditedAmt = Sm.DrDec(dr, c[2]),
                            AuditedAmt1Old = Sm.DrDec(dr, c[3]),
                            AuditedAmt1 = Sm.DrDec(dr, c[4]),
                            RKAP = Sm.DrDec(dr, c[5]),
                            RKAPQuarter = Sm.DrDec(dr, c[6]),
                            Percentage1 = 0m,
                            Percentage2 = 0m,
                            Percentage3 = 0m,
                            Percentage4 = 0m,
                            PercentageComparation = 0m,
                            PercentageTwoYearsAgo = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 14) == x.DocType &&
                            Sm.GetGrdStr(Grd1, i, 15) == x.Code)
                        {
                            Grd1.Cells[i, 3].Value = x.AuditedAmt;
                            Grd1.Cells[i, 4].Value = x.PercentageTwoYearsAgo;
                            Grd1.Cells[i, 5].Value = x.RKAP;
                            Grd1.Cells[i, 6].Value = x.Percentage1;
                            Grd1.Cells[i, 7].Value = x.RKAPQuarter;
                            Grd1.Cells[i, 8].Value = x.Percentage2;
                            Grd1.Cells[i, 9].Value = x.AuditedAmt1Old;
                            Grd1.Cells[i, 10].Value = x.Percentage3;
                            Grd1.Cells[i, 11].Value = x.AuditedAmt1;
                            Grd1.Cells[i, 12].Value = x.Percentage4;
                            Grd1.Cells[i, 13].Value = x.PercentageComparation;
                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        private void Process2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<ProfitLoss>();

            SQL.AppendLine("SELECT T.DocType, T.Code, SUM(T.AuditedAmt) AuditedAmt, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SUM(T.AuditedAmt1Old) + SUM(T.AuditedAmt2Old) + SUM(T.AuditedAmt3Old) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt4Old) + SUM(T.AuditedAmt5Old) + SUM(T.AuditedAmt6Old) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt7Old) + SUM(T.AuditedAmt8Old) + SUM(T.AuditedAmt9Old) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt10Old) + SUM(T.AuditedAmt11Old) + SUM(T.AuditedAmt12Old) ");
            SQL.AppendLine(") AuditedAmt1Old, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SUM(T.AuditedAmt1) + SUM(T.AuditedAmt2) + SUM(T.AuditedAmt3) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt4) + SUM(T.AuditedAmt5) + SUM(T.AuditedAmt6) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt7) + SUM(T.AuditedAmt8) + SUM(T.AuditedAmt9) + ");
            SQL.AppendLine("    SUM(T.AuditedAmt10) + SUM(T.AuditedAmt11) + SUM(T.AuditedAmt12) ");
            SQL.AppendLine(") AuditedAmt1, ");
            SQL.AppendLine("SUM(T.Amt) RKAP, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SUM(T.Amt01) + SUM(T.Amt02) + SUM(T.Amt03) + ");
            SQL.AppendLine("    SUM(T.Amt04) + SUM(T.Amt05) + SUM(T.Amt06) + ");
            SQL.AppendLine("    SUM(T.Amt07) + SUM(T.Amt08) + SUM(T.Amt09) + ");
            SQL.AppendLine("    SUM(T.Amt10) + SUM(T.Amt11) + SUM(T.Amt12) ");
            SQL.AppendLine(") RKAPQuarter ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT D.DocType, D.Code, IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00) AuditedAmt, ");
            SQL.AppendLine("    0.00 AuditedAmt1Old, 0.00 AuditedAmt2Old, 0.00 AuditedAmt3Old, 0.00 AuditedAmt4Old, 0.00 AuditedAmt5Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt6Old, 0.00 AuditedAmt7Old, 0.00 AuditedAmt8Old, 0.00 AuditedAmt9Old, 0.00 AuditedAmt10Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt11Old, 0.00 AuditedAmt12Old, ");
            SQL.AppendLine("    0.00 AuditedAmt1, 0.00 AuditedAmt2, 0.00 AuditedAmt3, 0.00 AuditedAmt4, 0.00 AuditedAmt5,  ");
            SQL.AppendLine("    0.00 AuditedAmt6, 0.00 AuditedAmt7, 0.00 AuditedAmt8, 0.00 AuditedAmt9, 0.00 AuditedAmt10,  ");
            SQL.AppendLine("    0.00 AuditedAmt11, 0.00 AuditedAmt12, ");
            SQL.AppendLine("    0.00 Amt, 0.00 Amt01, 0.00 Amt02, 0.00 Amt03, 0.00 Amt04, 0.00 Amt05, 0.00 Amt06, 0.00 Amt07, 0.00 Amt08, 0.00 Amt09, 0.00 Amt10, 0.00 Amt11, 0.00 Amt12 ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where LEFT(A.DocDt, 4) = (@Yr - 2) ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, 0.00 AuditedAmt, ");

            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '01', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited1Old, ");
            else SQL.AppendLine("    0.00 Audited1Old, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '02', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited2Old, ");
            else SQL.AppendLine("    0.00 Audited2Old, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '03', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited3Old, ");
            else SQL.AppendLine("    0.00 Audited3Old, ");

            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '04', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited4Old, ");
            else SQL.AppendLine("    0.00 Audited4Old, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '05', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited5Old, ");
            else SQL.AppendLine("    0.00 Audited5Old, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '06', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited6Old, ");
            else SQL.AppendLine("    0.00 Audited6Old, ");

            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '07', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited7Old, ");
            else SQL.AppendLine("    0.00 Audited7Old, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '08', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited8Old, ");
            else SQL.AppendLine("    0.00 Audited8Old, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '09', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited9Old, ");
            else SQL.AppendLine("    0.00 Audited9Old, ");

            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '10', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited10Old, ");
            else SQL.AppendLine("    0.00 Audited10Old, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '11', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited11Old, ");
            else SQL.AppendLine("    0.00 Audited11Old, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '12', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited12Old, ");
            else SQL.AppendLine("    0.00 Audited12Old, ");

            SQL.AppendLine("    0.00 AuditedAmt1, 0.00 AuditedAmt2, 0.00 AuditedAmt3, 0.00 AuditedAmt4, 0.00 AuditedAmt5,  ");
            SQL.AppendLine("    0.00 AuditedAmt6, 0.00 AuditedAmt7, 0.00 AuditedAmt8, 0.00 AuditedAmt9, 0.00 AuditedAmt10,  ");
            SQL.AppendLine("    0.00 AuditedAmt11, 0.00 AuditedAmt12, ");
            SQL.AppendLine("    0.00 Amt, 0.00 Amt01, 0.00 Amt02, 0.00 Amt03, 0.00 Amt04, 0.00 Amt05, 0.00 Amt06, 0.00 Amt07, 0.00 Amt08, 0.00 Amt09, 0.00 Amt10, 0.00 Amt11, 0.00 Amt12 ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where LEFT(A.DocDt, 4) = (@Yr - 1) ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT D.DocType, D.Code, 0.00 AuditedAmt, ");
            SQL.AppendLine("    0.00 AuditedAmt1Old, 0.00 AuditedAmt2Old, 0.00 AuditedAmt3Old, 0.00 AuditedAmt4Old, 0.00 AuditedAmt5Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt6Old, 0.00 AuditedAmt7Old, 0.00 AuditedAmt8Old, 0.00 AuditedAmt9Old, 0.00 AuditedAmt10Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt11Old, 0.00 AuditedAmt12Old, ");

            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '01', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited1, ");
            else SQL.AppendLine("    0.00 Audited1, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '02', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited2, ");
            else SQL.AppendLine("    0.00 Audited2, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '03', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited3, ");
            else SQL.AppendLine("    0.00 Audited3, ");

            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '04', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited4, ");
            else SQL.AppendLine("    0.00 Audited4, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '05', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited5, ");
            else SQL.AppendLine("    0.00 Audited5, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '06', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited6, ");
            else SQL.AppendLine("    0.00 Audited6, ");

            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '07', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited7, ");
            else SQL.AppendLine("    0.00 Audited7, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '08', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited8, ");
            else SQL.AppendLine("    0.00 Audited8, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '09', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited9, ");
            else SQL.AppendLine("    0.00 Audited9, ");

            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '10', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited10, ");
            else SQL.AppendLine("    0.00 Audited10, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '11', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited11, ");
            else SQL.AppendLine("    0.00 Audited11, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    If(SUBSTR(A.DocDt, 5, 2) = '12', IfNull(If(C.AcType = 'D', (B.DAmt - B.CAmt), (B.CAmt - B.DAmt)), 0.00), 0.00) Audited12, ");
            else SQL.AppendLine("    0.00 Audited12, ");

            SQL.AppendLine("    0.00 Amt, 0.00 Amt01, 0.00 Amt02, 0.00 Amt03, 0.00 Amt04, 0.00 Amt05, 0.00 Amt06, 0.00 Amt07, 0.00 Amt08, 0.00 Amt09, 0.00 Amt10, 0.00 Amt11, 0.00 Amt12 ");
            SQL.AppendLine("    FROM TblJournalHdr A ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl D ON D.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo LIKE CONCAT(D.AcNo, '%') ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr E ON D.DocType = E.DocType ");
            SQL.AppendLine("        AND D.Code = E.Code AND E.ActInd = 'Y' ");
            SQL.AppendLine("    Where LEFT(A.DocDt, 4) = (@Yr) ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT C.DocType, C.Code, 0.00 AuditedAmt, ");
            SQL.AppendLine("    0.00 AuditedAmt1Old, 0.00 AuditedAmt2Old, 0.00 AuditedAmt3Old, 0.00 AuditedAmt4Old, 0.00 AuditedAmt5Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt6Old, 0.00 AuditedAmt7Old, 0.00 AuditedAmt8Old, 0.00 AuditedAmt9Old, 0.00 AuditedAmt10Old,  ");
            SQL.AppendLine("    0.00 AuditedAmt11Old, 0.00 AuditedAmt12Old, ");
            SQL.AppendLine("    0.00 AuditedAmt1, 0.00 AuditedAmt2, 0.00 AuditedAmt3, 0.00 AuditedAmt4, 0.00 AuditedAmt5,  ");
            SQL.AppendLine("    0.00 AuditedAmt6, 0.00 AuditedAmt7, 0.00 AuditedAmt8, 0.00 AuditedAmt9, 0.00 AuditedAmt10,  ");
            SQL.AppendLine("    0.00 AuditedAmt11, 0.00 AuditedAmt12, B.Amt, ");

            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    B.Amt01, "); else SQL.AppendLine("    0.00 Amt01, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    B.Amt02, "); else SQL.AppendLine("    0.00 Amt02, ");
            if (Sm.GetLue(LueMth1) == "01") SQL.AppendLine("    B.Amt03, "); else SQL.AppendLine("    0.00 Amt03, ");

            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    B.Amt04, "); else SQL.AppendLine("    0.00 Amt04, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    B.Amt05, "); else SQL.AppendLine("    0.00 Amt05, ");
            if (Sm.GetLue(LueMth1) == "04") SQL.AppendLine("    B.Amt06, "); else SQL.AppendLine("    0.00 Amt06, ");

            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    B.Amt07, "); else SQL.AppendLine("    0.00 Amt07, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    B.Amt08, "); else SQL.AppendLine("    0.00 Amt08, ");
            if (Sm.GetLue(LueMth1) == "07") SQL.AppendLine("    B.Amt09, "); else SQL.AppendLine("    0.00 Amt09, ");

            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    B.Amt10, "); else SQL.AppendLine("    0.00 Amt10, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    B.Amt11, "); else SQL.AppendLine("    0.00 Amt11, ");
            if (Sm.GetLue(LueMth1) == "10") SQL.AppendLine("    B.Amt12 "); else SQL.AppendLine("    0.00 Amt12 ");

            SQL.AppendLine("    FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPDtl C ON C.DocType = 'ProfitLoss' ");
            SQL.AppendLine("        AND B.AcNo = C.AcNo ");
            SQL.AppendLine("    INNER JOIN TblFicoSettingJournalToCBPHdr D ON C.DocType = D.DocType ");
            SQL.AppendLine("        AND C.Code = D.Code AND D.ActInd = 'Y' ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.Yr = @Yr ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.DocType, T.Code; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", mThisYear);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocType",
                    
                    //1-5
                    "Code",
                    "AuditedAmt",
                    "AuditedAmt1Old",
                    "AuditedAmt1",
                    "RKAP",

                    //6
                    "RKAPQuarter"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProfitLoss()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            Code = Sm.DrStr(dr, c[1]),
                            AuditedAmt = Sm.DrDec(dr, c[2]),
                            AuditedAmt1Old = Sm.DrDec(dr, c[3]),
                            AuditedAmt1 = Sm.DrDec(dr, c[4]),
                            RKAP = Sm.DrDec(dr, c[5]),
                            RKAPQuarter = Sm.DrDec(dr, c[6]),
                            Percentage1 = 0m,
                            Percentage2 = 0m,
                            Percentage3 = 0m,
                            Percentage4 = 0m,
                            PercentageComparation = 0m,
                            PercentageTwoYearsAgo = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 14) == x.DocType &&
                            Sm.GetGrdStr(Grd1, i, 15) == x.Code)
                        {
                            Grd1.Cells[i, 3].Value = x.AuditedAmt;
                            Grd1.Cells[i, 4].Value = x.PercentageTwoYearsAgo;
                            Grd1.Cells[i, 5].Value = x.RKAP;
                            Grd1.Cells[i, 6].Value = x.Percentage1;
                            Grd1.Cells[i, 7].Value = x.RKAPQuarter;
                            Grd1.Cells[i, 8].Value = x.Percentage2;
                            Grd1.Cells[i, 9].Value = x.AuditedAmt1Old;
                            Grd1.Cells[i, 10].Value = x.Percentage3;
                            Grd1.Cells[i, 11].Value = x.AuditedAmt1;
                            Grd1.Cells[i, 12].Value = x.Percentage4;
                            Grd1.Cells[i, 13].Value = x.PercentageComparation;
                            break;
                        }
                    }
                }
            }

            l.Clear();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueMth1_EditValueChanged(object sender, EventArgs e)
        {
            LueMth2.EditValue = null;
            if (Sm.GetLue(LueMth1).Length > 0) Sm.SetLue(LueMth2, Sm.Right(string.Concat("00", (Int32.Parse(Sm.GetLue(LueMth1)) + 2).ToString()), 2));
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Class

        private class ProfitLoss
        {
            public string DocType { get; set; }
            public string Code { get; set; }
            public decimal AuditedAmt { get; set; }
            public decimal PercentageTwoYearsAgo { get; set; }
            public decimal RKAP { get; set; }
            public decimal Percentage1 { get; set; }
            public decimal RKAPQuarter { get; set; }            
            public decimal Percentage2 { get; set; }
            public decimal AuditedAmt1Old { get; set; }
            public decimal Percentage3 { get; set; }
            public decimal AuditedAmt1 { get; set; }
            public decimal Percentage4 { get; set; }
            public decimal PercentageComparation { get; set; }
        }

        #endregion

    }
}
