﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLegalDocVerifyDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmLegalDocVerify mFrmParent;
        private string mSQL = string.Empty;
        internal byte mVdType = 0;
        #endregion

        #region Constructor

        public FrmLegalDocVerifyDlg2(FrmLegalDocVerify FrmParent, byte VdType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdType = VdType;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Kode Vendor",
                        "Nama Vendor",
                        "Nomor Identitas",
                        "NPWP",
                        "Alamat",

                        //6-8
                        "Kota",
                        "Kecamatan",
                        "Desa"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VdCode, A.VdName, A.IdentityNo, A.TIN, A.Address, ");
            SQL.AppendLine("B.CityName, C.SDName, D.VilName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblSubDistrict C On A.SDCode=C.SDCode ");
            SQL.AppendLine("Left Join TblVillage D On A.VilCode=D.VilCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.VdCtCode In (Select ParValue From TblParameter Where ParCode='VendorRMP') ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVdName.Text, new string[] { "A.VdCode", "A.VdName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.VdName;",
                        new string[] 
                        { 
                            //0
                            "VdCode", 
                            
                            //1-5
                            "VdName", "IdentityNo", "TIN", "Address", "CityName", 
                            
                            //6-7
                            "VilName", "SDName" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                string VdCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                switch (mVdType)
                {
                    case 0:
                        mFrmParent.mVdCode = VdCode;
                        mFrmParent.ShowVdInfo(mFrmParent.mVdCode, mFrmParent.TxtVdName, mFrmParent.TxtIdentityNo, mFrmParent.MeeAddress, mFrmParent.TxtVilCode, mFrmParent.TxtSDCode, mFrmParent.TxtCityCode, mFrmParent.TxtProvCode);
                        break;
                    case 1:
                        mFrmParent.mVdCode1 = VdCode;
                        mFrmParent.ShowVdInfo(mFrmParent.mVdCode1, mFrmParent.TxtVdName1, mFrmParent.TxtIdentityNo1, mFrmParent.MeeAddress1, mFrmParent.TxtVilCode1, mFrmParent.TxtSDCode1, mFrmParent.TxtCityCode1, mFrmParent.TxtProvCode1);
                        break;
                    case 2:
                        mFrmParent.mVdCode2 = VdCode;
                        mFrmParent.ShowVdInfo(mFrmParent.mVdCode2, mFrmParent.TxtVdName2, mFrmParent.TxtIdentityNo2, mFrmParent.MeeAddress2, mFrmParent.TxtVilCode2, mFrmParent.TxtSDCode2, mFrmParent.TxtCityCode2, mFrmParent.TxtProvCode2);
                        break;
                }
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmTimbanganAD4329(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmTimbanganAD4329(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVdName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor antrian");
        }

        #endregion

        #endregion
    }
}
