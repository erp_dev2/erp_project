﻿#region Update
/*
 13/09/2019 [DITA] reporting baru
 20/09/2019 [DITA] Tambah subtotal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDailyProject : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptDailyProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.ProjectName, A.CtCode, B.CtName, C.SiteName, D.LOPDocNoCount, IfNull(E.BOQDocNoCount, 0) BOQDocNoCount, IfNull(F.SOCOntractDocNoCount, 0) SOCOntractDocNoCount, ");
            SQL.AppendLine("IfNull(G.LOPApprovedCount, 0) LOPApprovedCount, IfNull(H.LOPOutstAndingCount, 0) LOPOutstAndingCount, IfNull(I.LOPCancelledCount, 0) LOPCancelledCount, ");
            SQL.AppendLine("IfNull(J.BOQApprovedCount, 0) BOQApprovedCount, IfNull(K.BOQOutstAndingCount, 0) BOQOutstAndingCount, IfNull(L.LOPLoseCount, 0) LOPLoseCount, IfNull(M.LOPTBDCount, 0) LOPTBDCount, ");
            SQL.AppendLine("IfNull(N.AuctionSchedule, 0) AuctiOnSchedule ");
            SQL.AppendLine("From TblLOPHdr A ");
           
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode And A.CancelInd = 'N' And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblSite C On A.SiteCode = C.SiteCode ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, COUNT(*) LOPDocNoCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo, COUNT(X2.DocNo) BOQDocNoCount ");
	        SQL.AppendLine("    From TblLOPHdr X1 ");
	        SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
	        SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.ActInd = 'Y' ");
	        SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo, COUNT(X3.DocNo) SOCOntractDocNoCount ");
	        SQL.AppendLine("    From TblLOPHdr X1 ");
	        SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
	        SQL.AppendLine("    Inner Join TblSOCOntractHdr X3 On X2.DocNo = X3.BOQDocNo ");
	        SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.ActInd = 'Y' And X3.CancelInd = 'N' ");
	        SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(") F On A.DocNo = F.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, COUNT(*) LOPApprovedCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) And Status = 'A' ");
	        SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") G On A.DocNo = G.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, COUNT(*) LOPOutstAndingCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) And Status = 'O' ");
	        SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") H On A.DocNo = H.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, COUNT(*) LOPCancelledCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) And Status = 'C' ");
	        SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") I On A.DocNo = I.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo, COUNT(X2.DocNo) BOQApprovedCount ");
	        SQL.AppendLine("    From TblLOPHdr X1 ");
	        SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
	        SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.Status = 'A' And X2.ActInd = 'Y' ");
	        SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(") J On A.DocNo = J.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X1.DocNo, COUNT(X2.DocNo) BOQOutstAndingCount ");
	        SQL.AppendLine("    From TblLOPHdr X1 ");
	        SQL.AppendLine("    Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
	        SQL.AppendLine("    Where X1.CancelInd = 'N' And (X1.DocDt Between @DocDt1 And @DocDt2) And X2.Status = 'O' And X2.ActInd = 'Y' ");
	        SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(") K On A.DocNo = K.DocNo ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, COUNT(*) LOPLoseCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) And ProcessInd = 'S' ");
            SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") L On A.DocNo = L.DocNo ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, COUNT(*) LOPTBDCount  ");
	        SQL.AppendLine("    From TblLOPHdr  ");
	        SQL.AppendLine("    Where CancelInd = 'N' And (DocDt Between @DocDt1 And @DocDt2) And ProcessInd != 'S' ");
	        SQL.AppendLine("    And DocNo Not In (Select X1.LOPDocNo From TblBOQHdr X1 Inner Join TblSOCOntractHdr X2 On X1.DocNo = X2.BOQDocNo ");
            SQL.AppendLine("     And X1.ActInd = 'Y' And X2.CancelInd = 'N') ");
	        SQL.AppendLine("    GROUP BY DocNo ");
            SQL.AppendLine(") M On A.DocNo = M.DocNo ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.DocNo, COUNT(X1.DocNo) AuctiOnSchedule  ");
	        SQL.AppendLine("    From TblLOPHdr X1 ");
	        SQL.AppendLine("    Inner Join TblLOPDtl X2 On X1.DocNo = X2.DocNo And X2.Status = 'F' ");
            SQL.AppendLine("    Where CancelInd = 'N' And (X1.DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine("    And X2.PhaseCode IN (Select ParValue From TblParameter Where Parcode = 'AuctiOnSchedulePhaseCode ') ");
            SQL.AppendLine("    GROUP BY X1.DocNo ");
            SQL.AppendLine(") N On A.DocNo = N.DocNo ");

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 150;
            Grd1.Header.Cells[0, 1].Value = "Document#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Project Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 250;
            Grd1.Header.Cells[0, 3].Value = "Customer";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 150;
            Grd1.Header.Cells[0, 4].Value = "Site";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 50;
            Grd1.Header.Cells[0, 5].Value = "LOP";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 120;
            Grd1.Header.Cells[0, 6].Value = "Auction Schedule";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 50;
            Grd1.Header.Cells[0, 7].Value = "BOQ";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 100;
            Grd1.Header.Cells[0, 8].Value = "SO Contract";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Grd1.Header.Cells[1, 9].Value = "LOP";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 3;
            Grd1.Header.Cells[0, 9].Value = "Approved";
            Grd1.Header.Cells[0, 10].Value = "Cancelled";
            Grd1.Header.Cells[0, 11].Value = "Outstanding";
            Grd1.Cols[9].Width = 80;
            Grd1.Cols[10].Width = 80;
            Grd1.Cols[11].Width = 80;

            Grd1.Header.Cells[1, 12].Value = "BOQ";
            Grd1.Header.Cells[1, 12].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 12].SpanCols = 2;
            Grd1.Header.Cells[0, 12].Value = "Approved";
            Grd1.Header.Cells[0, 13].Value = "Outstanding";
            Grd1.Cols[12].Width = 80;
            Grd1.Cols[13].Width = 80;

            Grd1.Header.Cells[1, 14].Value = "Status";
            Grd1.Header.Cells[1, 14].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 14].SpanCols = 3;
            Grd1.Header.Cells[0, 14].Value = "Win";
            Grd1.Header.Cells[0, 15].Value = "Lose";
            Grd1.Header.Cells[0, 16].Value = "TBD";
            Grd1.Cols[14].Width = 50;
            Grd1.Cols[15].Width = 50;
            Grd1.Cols[16].Width = 50;

            
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void ShowData()
        {
            
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Sm.ClearGrd(Grd1, false);
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "ProjectName", "CtName", "SiteName", "LOPDocNoCount", "AuctionSchedule", 
                        
                        //6-10
                        "BOQDocNoCount", "SOContractDocNoCount" ,"LOPApprovedCount", "LOPCancelledCount", "LOPOutstAndingCount",

                        //11-15
                        "BOQApprovedCount", "BOQOutstAndingCount", "SOCOntractDocNoCount", "LOPLoseCount", "LOPTBDCount"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                    }, true, false, false, false
                );

                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
        #endregion

        #endregion

        #region Event
        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        
    }
}
