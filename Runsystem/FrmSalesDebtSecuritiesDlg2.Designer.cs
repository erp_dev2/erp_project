﻿namespace RunSystem
{
    partial class FrmSalesDebtSecuritiesDlg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkInvestmentCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkInvestmentName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkInvestmentType = new DevExpress.XtraEditors.CheckEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtInvestmentBankAcc = new DevExpress.XtraEditors.TextEdit();
            this.ChkInvestmentBankAcc = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentBankAcc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkInvestmentBankAcc);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtInvestmentBankAcc);
            this.panel2.Controls.Add(this.ChkInvestmentType);
            this.panel2.Controls.Add(this.ChkInvestmentName);
            this.panel2.Controls.Add(this.ChkInvestmentCode);
            this.panel2.Controls.Add(this.TxtInvestmentName);
            this.panel2.Controls.Add(this.TxtInvestmentCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueInvestmentType);
            this.panel2.Size = new System.Drawing.Size(672, 92);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 381);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.CellDoubleClick += new TenTec.Windows.iGridLib.iGCellDoubleClickEventHandler(this.Grd1_CellDoubleClick);
            // 
            // BtnClose
            // 
            this.BtnClose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnClose.Appearance.Options.UseBackColor = true;
            this.BtnClose.Appearance.Options.UseFont = true;
            this.BtnClose.Appearance.Options.UseForeColor = true;
            this.BtnClose.Appearance.Options.UseTextOptions = true;
            this.BtnClose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(193, 24);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 16;
            this.TxtInvestmentName.Size = new System.Drawing.Size(230, 20);
            this.TxtInvestmentName.TabIndex = 14;
            this.TxtInvestmentName.Validated += new System.EventHandler(this.TxtInvestmentName_Validated);
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(193, 3);
            this.TxtInvestmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.MaxLength = 16;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(230, 20);
            this.TxtInvestmentCode.TabIndex = 11;
            this.TxtInvestmentCode.Validated += new System.EventHandler(this.TxtInvestmentCode_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(87, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 14);
            this.label7.TabIndex = 16;
            this.label7.Text = "Investment Type";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(84, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 13;
            this.label6.Text = "Investment Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(87, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "Investment Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentType
            // 
            this.LueInvestmentType.EnterMoveNextControl = true;
            this.LueInvestmentType.Location = new System.Drawing.Point(193, 45);
            this.LueInvestmentType.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentType.Name = "LueInvestmentType";
            this.LueInvestmentType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.Appearance.Options.UseBackColor = true;
            this.LueInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentType.Properties.DropDownRows = 30;
            this.LueInvestmentType.Properties.NullText = "[Empty]";
            this.LueInvestmentType.Properties.PopupWidth = 350;
            this.LueInvestmentType.Size = new System.Drawing.Size(230, 20);
            this.LueInvestmentType.TabIndex = 17;
            this.LueInvestmentType.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInvestmentType.EditValueChanged += new System.EventHandler(this.LueInvestmentType_EditValueChanged);
            // 
            // ChkInvestmentCode
            // 
            this.ChkInvestmentCode.Location = new System.Drawing.Point(425, 3);
            this.ChkInvestmentCode.Name = "ChkInvestmentCode";
            this.ChkInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentCode.Properties.Caption = " ";
            this.ChkInvestmentCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentCode.Size = new System.Drawing.Size(31, 22);
            this.ChkInvestmentCode.TabIndex = 12;
            this.ChkInvestmentCode.ToolTip = "Remove filter";
            this.ChkInvestmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentCode.ToolTipTitle = "Run System";
            this.ChkInvestmentCode.CheckedChanged += new System.EventHandler(this.ChkInvestmentCode_CheckedChanged);
            // 
            // ChkInvestmentName
            // 
            this.ChkInvestmentName.Location = new System.Drawing.Point(425, 24);
            this.ChkInvestmentName.Name = "ChkInvestmentName";
            this.ChkInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentName.Properties.Caption = " ";
            this.ChkInvestmentName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentName.Size = new System.Drawing.Size(31, 22);
            this.ChkInvestmentName.TabIndex = 15;
            this.ChkInvestmentName.ToolTip = "Remove filter";
            this.ChkInvestmentName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentName.ToolTipTitle = "Run System";
            this.ChkInvestmentName.CheckedChanged += new System.EventHandler(this.ChkInvestmentName_CheckedChanged);
            // 
            // ChkInvestmentType
            // 
            this.ChkInvestmentType.Location = new System.Drawing.Point(425, 45);
            this.ChkInvestmentType.Name = "ChkInvestmentType";
            this.ChkInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentType.Properties.Caption = " ";
            this.ChkInvestmentType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentType.Size = new System.Drawing.Size(31, 22);
            this.ChkInvestmentType.TabIndex = 18;
            this.ChkInvestmentType.ToolTip = "Remove filter";
            this.ChkInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentType.ToolTipTitle = "Run System";
            this.ChkInvestmentType.CheckedChanged += new System.EventHandler(this.ChkInvestmentType_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(2, 70);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(185, 14);
            this.label12.TabIndex = 19;
            this.label12.Text = "Investment Bank Account Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentBankAcc
            // 
            this.TxtInvestmentBankAcc.EnterMoveNextControl = true;
            this.TxtInvestmentBankAcc.Location = new System.Drawing.Point(192, 67);
            this.TxtInvestmentBankAcc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentBankAcc.Name = "TxtInvestmentBankAcc";
            this.TxtInvestmentBankAcc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentBankAcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentBankAcc.Properties.MaxLength = 16;
            this.TxtInvestmentBankAcc.Size = new System.Drawing.Size(231, 20);
            this.TxtInvestmentBankAcc.TabIndex = 20;
            this.TxtInvestmentBankAcc.Validated += new System.EventHandler(this.TxtInvestmentBankAcc_Validated);
            // 
            // ChkInvestmentBankAcc
            // 
            this.ChkInvestmentBankAcc.Location = new System.Drawing.Point(425, 67);
            this.ChkInvestmentBankAcc.Name = "ChkInvestmentBankAcc";
            this.ChkInvestmentBankAcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentBankAcc.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentBankAcc.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentBankAcc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentBankAcc.Properties.Caption = " ";
            this.ChkInvestmentBankAcc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentBankAcc.Size = new System.Drawing.Size(31, 22);
            this.ChkInvestmentBankAcc.TabIndex = 21;
            this.ChkInvestmentBankAcc.ToolTip = "Remove filter";
            this.ChkInvestmentBankAcc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentBankAcc.ToolTipTitle = "Run System";
            this.ChkInvestmentBankAcc.CheckedChanged += new System.EventHandler(this.ChkInvestmentBankAcc_CheckedChanged);
            // 
            // FrmSalesDebtSecuritiesDlg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmSalesDebtSecuritiesDlg2";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentBankAcc.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.LookUpEdit LueInvestmentType;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentType;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentName;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentCode;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentBankAcc;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentBankAcc;
    }
}