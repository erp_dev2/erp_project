﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmKPIPerspective : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty;
        internal bool mIsFilterBySite = false;
        internal FrmKPIPerspectiveFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmKPIPerspective(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bill of Material";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                LuePosition1.Visible = false;
                LuePosition2.Visible = false;
                LuePosition3.Visible = false;
                LuePosition4.Visible = false;
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                   new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Strategic Objective",
                        "Responsibility",
                        "Measures",
                        "Target (Year)",
                        "Data",
                        
                        //6-10
                        "Action Plan",
                        "PosCode",
                        "Action Plan PIC",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        150, 150, 120, 150, 150, 
                        
                        //6-10
                        200, 80, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 7 });
            
            #endregion

            #region Grid2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Strategic Objective",
                        "Responsibility",
                        "Measures",
                        "Target (Year)",
                        "Data",
                        
                        //6-10
                        "Action Plan",
                        "PosCode",
                        "Action Plan PIC",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        150, 150, 120, 150, 150, 
                        
                        //6-10
                        200, 80, 200
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 7 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Strategic Objective",
                        "Responsibility",
                        "Measures",
                        "Target (Year)",
                        "Data",
                        
                        //6-10
                        "Action Plan",
                        "PosCode",
                        "Action Plan PIC",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        150, 150, 120, 150, 150, 
                        
                        //6-10
                        200, 80, 200
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 7 });

            #endregion

            #region Grid4

            Grd4.Cols.Count = 9;
            Grd4.FrozenArea.ColCount = 1;
            Grd4.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Strategic Objective",
                        "Responsibility",
                        "Measures",
                        "Target (Year)",
                        "Data",
                        
                        //6-10
                        "Action Plan",
                        "PosCode",
                        "Action Plan PIC",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        150, 150, 120, 150, 150, 
                        
                        //6-10
                        200, 80, 200
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 7 });

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
           

        }
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtKPIName, TxtPICCode, TxtPICName, MeeRemark, 
                        LuePosition1, LuePosition2, LuePosition3, LuePosition4    
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, TxtKPIName, MeeRemark,
                      LuePosition1, LuePosition2, LuePosition3, LuePosition4
                    }, false);
                    ChkActiveInd.Checked = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, TxtKPIName,  MeeRemark,
                      LuePosition1, LuePosition2, LuePosition3, LuePosition4
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtKPIName, TxtPICCode, TxtPICName,  MeeRemark,
                LuePosition1, LuePosition2, LuePosition3, LuePosition4
            });
            ClearGrd();
            ChkActiveInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 5 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 4, 5 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmKPIPerspectiveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                
                    InsertData();
                

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //ParPrint();
        }

        #endregion

        #region Grid Method

      

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex + 1, new int[] { 4, 5 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LuePosition1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sl.SetLuePosCode(ref LuePosition1);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd2, e.RowIndex + 1, new int[] { 4, 5 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd2, LuePosition2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sl.SetLuePosCode(ref LuePosition2);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, e.RowIndex + 1, new int[] { 4, 5 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LuePosition3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLuePosCode(ref LuePosition3);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd4, e.RowIndex + 1, new int[] { 4, 5 });
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd4, LuePosition4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sl.SetLuePosCode(ref LuePosition4);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "KPIPerspective", "TblKPIPerspectiveHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveKPIPerspectiveHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveKPIPerspectiveDtl(DocNo, Row));
            }
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveKPIPerspectiveDtl2(DocNo, Row));
            }
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveKPIPerspectiveDtl3(DocNo, Row));
            }
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SaveKPIPerspectiveDtl4(DocNo, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtKPIName, "KPI Name", false) ||
                IsDocNoAlreadyProcess()||
                IsGrdEmpty();
        }

        private bool IsDocNoAlreadyProcess()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select KPIDocNo From TblKPIProcessHdr Where KPIDocNo=@DocNo And CancelInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                return true;
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveKPIPerspectiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIPerspectiveHdr(DocNo, DocDt, ActInd, KPIName, PICCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @KPIName, @PICCode,  @Remark, @UserCode, CurrentDateTime()) ");
            //region bisa edit hdr dan detail
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, ActInd=@ActInd, KPIName=@KPIName, PICCode=@PICCode, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            
            SQL.AppendLine("Delete From TblKPIPerspectiveDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblKPIPerspectiveDtl2 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblKPIPerspectiveDtl3 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblKPIPerspectiveDtl4 Where DocNo=@DocNo; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "KPIName", TxtKPIName.Text);
            Sm.CmParam<String>(ref cm, "PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveKPIPerspectiveDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIPerspectiveDtl(DocNo, DNo, Strategic, Responsibility, Measures, Target, Data, Initiative, PosCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Strategic, @Responsibility, @Measures, @Target, @Data, @Initiative, @PosCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Strategic", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Responsibility", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measures", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Data", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Initiative", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveKPIPerspectiveDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIPerspectiveDtl2(DocNo, DNo, Strategic, Responsibility, Measures, Target, Data, Initiative, PosCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Strategic, @Responsibility, @Measures, @Target, @Data, @Initiative, @PosCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Strategic", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Responsibility", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measures", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Data", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@Initiative", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveKPIPerspectiveDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIPerspectiveDtl3(DocNo, DNo, Strategic, Responsibility, Measures, Target, Data, Initiative, PosCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Strategic, @Responsibility, @Measures, @Target, @Data, @Initiative, @PosCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Strategic", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@Responsibility", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measures", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Data", Sm.GetGrdDec(Grd3, Row, 5));
            Sm.CmParam<String>(ref cm, "@Initiative", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveKPIPerspectiveDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIPerspectiveDtl4(DocNo, DNo, Strategic, Responsibility, Measures, Target, Data, Initiative, PosCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Strategic, @Responsibility, @Measures, @Target, @Data, @Initiative, @PosCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Strategic", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@Responsibility", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measures", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Target", Sm.GetGrdDec(Grd4, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Data", Sm.GetGrdDec(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@Initiative", Sm.GetGrdStr(Grd4, Row, 6));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd4, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditKPIActive(TxtDocNo.Text);
            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document Number", false) ||
                IsActIndEditedAlready();
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblKPIHdr Where DocNo=@DocNo And ActInd='N' "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already non-active.");
                return true;
            }
            return false;
        }

        private void EditKPIActive(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblKPIHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", @DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowKPIPerspectiveHdr(DocNo);
                ShowKPIPerspectiveDtl(DocNo);
                ShowKPIPerspectiveDtl2(DocNo);
                ShowKPIPerspectiveDtl3(DocNo);
                ShowKPIPerspectiveDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowKPIPerspectiveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.ActInd, A.KPIName, A.PICCode,  B.EmpName,  A.Remark From TblKPIPerspectiveHdr  A " +
                    "Inner Join TblEmployee B On A.PICCode=B.EmpCode " +
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ActInd", "KPIName", "PICCode", "EmpName",  
                        
                        //6-10
                        "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtKPIName.EditValue = Sm.DrStr(dr, c[3]);
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowKPIPerspectiveDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.Strategic, A.Responsibility, A.Measures, A.Target, ");
            SQL.AppendLine("A.Data, A.Initiative, A.PosCode, B.PosName From tblkpiperspectivedtl A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Strategic", "Responsibility", "Measures", "Target","Data", 
                    //6-10
                    "Initiative", "PosCode", "PosName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowKPIPerspectiveDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.Strategic, A.Responsibility, A.Measures, A.Target, ");
            SQL.AppendLine("A.Data, A.Initiative, A.PosCode, B.PosName From tblkpiperspectivedtl2 A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Strategic", "Responsibility", "Measures", "Target","Data", 
                    //6-10
                    "Initiative", "PosCode", "PosName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowKPIPerspectiveDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.Strategic, A.Responsibility, A.Measures, A.Target, ");
            SQL.AppendLine("A.Data, A.Initiative, A.PosCode, B.PosName From tblkpiperspectivedtl3 A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Strategic", "Responsibility", "Measures", "Target","Data", 
                    //6-10
                    "Initiative", "PosCode", "PosName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowKPIPerspectiveDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.Strategic, A.Responsibility, A.Measures, A.Target, ");
            SQL.AppendLine("A.Data, A.Initiative, A.PosCode, B.PosName From tblkpiperspectivedtl4 A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Strategic", "Responsibility", "Measures", "Target","Data", 
                    //6-10
                    "Initiative", "PosCode", "PosName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method


        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }
       
        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #region Event
        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmKPIPerspectiveDlg(this));
        }

        #endregion

        #region event Lue
        private void LuePosition1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition1, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LuePosition2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition2, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LuePosition3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition3, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LuePosition4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition4, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LuePosition1_Leave(object sender, EventArgs e)
        {
            if (LuePosition1.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LuePosition1).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 7].Value =
                    Grd1.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LuePosition1);
                    Grd1.Cells[fCell.RowIndex, 8].Value = LuePosition1.GetColumnValue("Col2");
                }
            }
            LuePosition1.Visible = false;
        }

        private void LuePosition2_Leave(object sender, EventArgs e)
        {
            if (LuePosition2.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LuePosition2).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 7].Value =
                    Grd2.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LuePosition2);
                    Grd2.Cells[fCell.RowIndex, 8].Value = LuePosition2.GetColumnValue("Col2");
                }
            }
            LuePosition2.Visible = false;
        }

        private void LuePosition3_Leave(object sender, EventArgs e)
        {
            if (LuePosition3.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LuePosition3).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LuePosition3);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LuePosition3.GetColumnValue("Col2");
                }
            }
            LuePosition3.Visible = false;
        }

        private void LuePosition4_Leave(object sender, EventArgs e)
        {
            if (LuePosition4.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LuePosition4).Length == 0)
                {
                    Grd4.Cells[fCell.RowIndex, 7].Value =
                    Grd4.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd4.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LuePosition4);
                    Grd4.Cells[fCell.RowIndex, 8].Value = LuePosition4.GetColumnValue("Col2");
                }
            }
            LuePosition4.Visible = false;
        }


        private void LuePosition1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePosition2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LuePosition3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LuePosition4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }
        #endregion

        

        #endregion

    }
}
