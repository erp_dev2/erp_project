﻿#region update 
/*
    26/08/2021 [ICA/AMKA] new apps
    15/09/2021 [ICA/AMKA] ubah formula
    24/09/2021 [WED/AMKA] kalkulasi amount journal berdasarkan parameter COACalculationFormula
    15/11/2021 [RDA/AMKA] filter profit center terfilter berdasarkan group user based on parameter IsFilterByProfitCenter
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptWIPARDetails : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private List<String> mlProfitCenter;
        private bool 
            mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false;

        #endregion

        #region Constructor

        public FrmRptWIPARDetails(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                ChkProfitCenterCode.Visible = false;
                var CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;

            SetGrdHdr(ref Grd1, 0, 0, "No", 2, 50);
            SetGrdHdr(ref Grd1, 0, 1, "KODE PROYEK", 2, 250);
            SetGrdHdr(ref Grd1, 0, 2, "NAMA PROYEK", 2, 250);

            SetGrdHdr2(ref Grd1, 1, 3, "WIP I", 4, 130);
            SetGrdHdr2(ref Grd1, 0, 3, "PIUTANG" + Environment.NewLine + "KONS.", 1, 130);
            SetGrdHdr2(ref Grd1, 0, 4, "PIUT" + Environment.NewLine + "RETENSI", 1, 130);
            SetGrdHdr2(ref Grd1, 0, 5, "PIUT. KONS." + Environment.NewLine + " BEDA KURS", 1, 130);
            SetGrdHdr2(ref Grd1, 0, 6, "TOTAL WIP I", 1, 130);

            SetGrdHdr(ref Grd1, 0, 7, "UM OWNER", 2, 130);

            SetGrdHdr(ref Grd1, 1, 8, "PIUTANG", 1, 130);
            SetGrdHdr(ref Grd1, 0, 8, "PPN" + Environment.NewLine + "KELUARAN", 1, 130);

            SetGrdHdr(ref Grd1, 1, 9, "WIP II", 1, 130);
            SetGrdHdr(ref Grd1, 0, 9, "TOTAL WIP II", 1, 130);

            SetGrdHdr(ref Grd1, 1, 10, "TOTAL", 1, 130);
            SetGrdHdr(ref Grd1, 0, 10, "WIP I + II", 1, 130);

            SetGrdHdr(ref Grd1, 1, 11, "CASH IN", 1, 130);
            SetGrdHdr(ref Grd1, 0, 11, "AWAL SD", 1, 130);

            SetGrdHdr(ref Grd1, 1, 12, "TOTAL", 1, 130);
            SetGrdHdr(ref Grd1, 0, 12, "HUTANG" + Environment.NewLine + "YSDF", 1, 130);

            SetGrdHdr(ref Grd1, 1, 13, "TOTAL", 1, 130);
            SetGrdHdr(ref Grd1, 0, 13, "HUTANG" + Environment.NewLine + "YSDF", 1, 130);

            SetGrdHdr(ref Grd1, 1, 14, "TOTAL", 1, 130);
            SetGrdHdr(ref Grd1, 0, 14, "HUTANG", 1, 130);

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        private string GetSQL(string Alias, string subSQL1, string filter, string subSQL2, string COAParent, bool IsNeedOpeningBalance)
        {
            var SQL = new StringBuilder();
            //string AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param", COAParent);

            if (IsNeedOpeningBalance)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select ProfitCenterCode, ");
                SQL.AppendLine("    Sum( ");
                //SQL.AppendLine("        Case ");
                //SQL.AppendLine("            When T.AcType = '"+AcType+"' then T.Amt ");
                //SQL.AppendLine("            Else T.Amt*-1 ");
                //SQL.AppendLine("        End ");
                SQL.AppendLine("        T.Amt ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select T3.ProfitCenterCode, ");
                SQL.AppendLine("        Sum( ");
                SQL.AppendLine("            Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("                Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ) As Amt ");
                SQL.AppendLine("        From TblJournalHdr T1 ");
                SQL.AppendLine("        Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CCCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("            And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("            And T4.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("        Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("        Where 0 = 0 ");
                SQL.AppendLine(subSQL1+filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("        Group By T3.ProfitCenterCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select T1.ProfitCenterCode, Sum(T2.Amt) As Amt ");
                SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1 ");
                SQL.AppendLine("        Inner Join TblCOAOpeningBalanceDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T1.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCOA T3 On T2.AcNo = T3.AcNo ");
                SQL.AppendLine("            And T3.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Where 0 = 0 ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2.Replace("A.", "T1."));
                SQL.AppendLine("        Group By T1.ProfitCenterCode ");
                SQL.AppendLine("    ) T Group By ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");

            }
            else
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select T3.ProfitCenterCode, ");
                SQL.AppendLine("    Sum( ");
                SQL.AppendLine("        Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("            Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From TblJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CCCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("        And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("        And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("    Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("    Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("    Where 0 = 0 ");
                SQL.AppendLine(subSQL1+filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("    Group By T3.ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
            }

            return SQL.ToString();
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            var Filter1 = " And Left(T1.DocDt, 6)=@YrMth "; // hanya bulan ini saja
            var Filter2 = " And T1.DocDt>=@Dt1 And T1.DocDt<@Dt2 "; // awal tahun sampai bulan ini
            //var Filter3 = " And T1.DocDt>=@Dt3 And T1.DocDt<@Dt2 "; // awal ada data sampai bulan ini

            if (!mIsAllProfitCenterSelected)
            {
                var Filter = string.Empty;
                int i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    i++;
                }
                if (Filter.Length == 0)
                    SQL2.AppendLine("    And 1=0 ");
                else
                    SQL2.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                if (ChkProfitCenterCode.Checked) SQL2.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
            }

            SQL.AppendLine("Select A.ProfitCenterCode, A.ProfitCenterName, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As Value1, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As Value2, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As Value3, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As Value4, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Value5, ");
            SQL.AppendLine("IfNull(G.Amt, 0.00) As Value6, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As Value7, ");
            SQL.AppendLine("IfNull(I.Amt, 0.00) As Value8, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Value9, "); 
            SQL.AppendLine("IfNull(K.Amt, 0.00) As Value10 ");
            SQL.AppendLine("From TblProfitCenter A ");
            SQL.AppendLine(GetSQL("B", " And T2.AcNo LIKE '1.1.3.1.1%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("C", " And T2.AcNo LIKE '1.1.3.2.1%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("D", " And T2.AcNo LIKE '1.1.3.1.2%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("E", " And T2.AcNo LIKE '1.1.3.2.2%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("F", " And T2.AcNo LIKE '2.1.5.1%' ", Filter2, SQL2.ToString(), "2", true));
            SQL.AppendLine(GetSQL("G", " And T2.AcNo LIKE '2.1.4.2%' ", Filter2, SQL2.ToString(), "2", true));
            SQL.AppendLine(GetSQL("H", " And T2.AcNo LIKE '1.1.3.3%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("I", " And T2.AcNo LIKE '2.1.1.1%' ", Filter2, SQL2.ToString(), "2", true));
            SQL.AppendLine(GetSQL("J", " And T2.AcNo LIKE '2.1.1.3%' ", Filter2, SQL2.ToString(), "2", true));
            SQL.AppendLine(GetSQL("K", " And T2.AcNo LIKE '4%' ", Filter2, SQL2.ToString(), "4", true));
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine(SQL2.ToString());

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterEmpty() || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand();
            string Yr = Sm.GetLue(LueYr);
            string Dt1 = string.Concat(Yr, "0101");
            string YrMth = string.Concat(Yr, Sm.GetLue(LueMth));
            string Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(string.Concat(YrMth, "01")).AddMonths(1)), 8);

            Sm.CmParam<String>(ref cm, "@YrMth", YrMth); // bulan ini
            Sm.CmParamDt(ref cm, "@Dt1", Dt1); // 1 jan 
            Sm.CmParamDt(ref cm, "@Dt2", Dt2); // 1 bulan berikutnya
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            try
            {
                SetProfitCenter();

                if (!mIsAllProfitCenterSelected)
                {
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "ProfitCenterCode",

                        //1-5
                        "ProfitCenterName", "Value1", "Value2", "Value3", "Value4", 

                        //6-10
                        "Value5", "Value6", "Value7", "Value8", "Value9", 
                        
                        //11
                        "Value10"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 5].Value = Sm.DrDec(dr, 4) + Sm.DrDec(dr, 5);
                        Grd.Cells[Row, 6].Value = Sm.GetGrdDec(Grd, Row, 3) + Sm.GetGrdDec(Grd, Row, 4) + Sm.GetGrdDec(Grd, Row, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Grd.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 6) + Sm.GetGrdDec(Grd1, Row, 9);
                        Grd.Cells[Row, 11].Value = Sm.DrDec(dr, 11) - Sm.GetGrdDec(Grd1, Row, 10) + Sm.GetGrdDec(Grd1, Row, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) + Sm.GetGrdDec(Grd, Row, 13);
                    }, true, false, false, false
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            ParPrint();
        }

        #endregion

        #region Additional Method

        private bool IsProfitCenterEmpty()
        {
            if (!ChkProfitCenterCode.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit Center is Empty.");
                return true;
            }
            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("    Where ProfitCenterCode In ( ");
                SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = ColWidth;
        }

        public string MonthName(string Mth)
        {
            switch (Mth)
            {
                case "01":
                    return "Januari";
                case "02":
                    return "Februari";
                case "03":
                    return "Maret";
                case "04":
                    return "April";
                case "05":
                    return "Mei";
                case "06":
                    return "Juni";
                case "07":
                    return "Juli";
                case "08":
                    return "Agustus";
                case "09":
                    return "September";
                case "10":
                    return "Oktober";
                case "11":
                    return "November";
                case "12":
                    return "Desember";
            }
            return "";
        }

        private void ParPrint()
        {
            var lHeader = new List<Header>();
            var lDetail = new List<Detail>();

            string[] TableName = { "Header", "Detail" };

            List<IList> myLists = new List<IList>();

            //Header 
            lHeader.Add(new Header()
            {
                Month = MonthName(Sm.GetLue(LueMth)),
                Year = Sm.GetLue(LueYr)
            });

            //Detail
            for (int row = 0; row < Grd1.Rows.Count - 1; row++)
            {
                lDetail.Add(new Detail()
                {
                    NamaProyek = Sm.GetGrdStr(Grd1, row, 2),
                    PiutKons = Sm.GetGrdDec(Grd1, row, 3),
                    PiutRetensi = Sm.GetGrdDec(Grd1, row, 4),
                    PiutKonsBedaKurs = Sm.GetGrdDec(Grd1, row, 5),
                    TotalWIPI = Sm.GetGrdDec(Grd1, row, 6),
                    UMOwner = Sm.GetGrdDec(Grd1, row, 7),
                    PPNKeluaran = Sm.GetGrdDec(Grd1, row, 8),
                    TotalWIPII = Sm.GetGrdDec(Grd1, row, 9),
                    WIPIWIPII = Sm.GetGrdDec(Grd1, row, 10),
                    AwalSD = Sm.GetGrdDec(Grd1, row, 11),
                    HutangYSDF = Sm.GetGrdDec(Grd1, row, 12),
                    HutangYSDF2 = Sm.GetGrdDec(Grd1, row, 13),
                    TotalHutang = Sm.GetGrdDec(Grd1, row, 14),
                });
            }

            myLists.Add(lHeader);
            myLists.Add(lDetail);

            Sm.PrintReport("RptWIPARDetails", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion

        #region Class

        private class Header
        {
            public string CompanyName { set; get; }
            public string Year { set; get; }
            public string Month { set; get; }
        }

        private class Detail
        {
            public string NamaProyek { set; get; }
            public decimal PiutKons { set; get; }
            public decimal PiutRetensi { set; get; }
            public decimal PiutKonsBedaKurs { set; get; }
            public decimal TotalWIPI { set; get; }
            public decimal UMOwner { set; get; }
            public decimal PPNKeluaran { set; get; }
            public decimal TotalWIPII { set; get; }
            public decimal WIPIWIPII { set; get; }
            public decimal AwalSD { set; get; }
            public decimal HutangYSDF { set; get; }
            public decimal HutangYSDF2 { set; get; }
            public decimal TotalHutang { set; get; }
        }

        #endregion
    }
}
