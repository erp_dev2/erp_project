﻿#region Update
/*
    21/08/2018 [WED] keterangan jasa di printout PPH23, ditambahin dari ServiceNote di PI
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPPNDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestPPN mFrmParent;
        private string mVATDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestPPNDlg3(FrmVoucherRequestPPN FrmParent, string VATDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVATDocNo = VATDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("Select A.DNo, A.DocType, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then 'Purchase Invoice' ");
            SQL.AppendLine("    When '2' Then 'Purchase Return Invoice' ");
            SQL.AppendLine("    When '3' Then 'Voucher Request Tax' ");
            SQL.AppendLine("End As DocTypeDesc, ");
            SQL.AppendLine("A.DocNoIn, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.DocDt ");
            SQL.AppendLine("    When '2' Then C.DocDt ");
            SQL.AppendLine("    When '3' Then D.DocDt ");
            SQL.AppendLine("End As DocDt, ");
            SQL.AppendLine("A.Amt, A.ExcRate As ExcRate, ");
            SQL.AppendLine("(A.Amt*IfNull(A.ExcRate, 0.00)) As Amt2, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.Remark, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '1' Then B.CurCode ");
            SQL.AppendLine("    When '2' Then C.CurCode ");
            SQL.AppendLine("    When '3' Then D.CurCode ");
            SQL.AppendLine("End As CurCode ");
            SQL.AppendLine("From (Select * From TblVoucherRequestPPNDtl Where DocNo = @DocNo And DocType In ('1', '2')) A ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr B On A.DocNoIn=B.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr C On A.DocNoIn=C.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestTax D On A.DocNoIn=D.DocNo ");
            SQL.AppendLine(") T ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",
                    
                    //1-5
                    "",
                    "Type",
                    "Type",
                    "Document#",
                    "Date",
                    
                    //6-10
                    "Description",
                    "Tax Invoice#",
                    "Tax Invoice Date",
                    "Amount",
                    "Rate",

                    //11-12
                    "Amount" + Environment.NewLine + "(" + mFrmParent.mMainCurCode + ")",
                    "Currency"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 0, 180, 130, 20,

                    //6-10
                    180, 180, 400, 130, 100,

                    //11-14
                    120, 100, 120, 60
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, false);
            Grd1.Cols[12].Move(9);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ", Filter2 = string.Empty, Filter3 = string.Empty, Filter4 = string.Empty;
                var cm = new MySqlCommand();

                //Sm.CmParam<String>(ref cm, "@PPNTaxCode", mFrmParent.mPPNTaxCode);
                Sm.CmParam<String>(ref cm, "@DocNo", mVATDocNo);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNoIn", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SetSQL(),
                        new string[] 
                        { 
                            //0
                            "DNo",
                            
                            //1-5
                            "DocType", "DocTypeDesc", "DocNoIn", "DocDt", "Remark", 
                            
                            //6-10
                            "TaxInvoiceNo", "TaxInvoiceDt", "Amt", "ExcRate", "Amt2",

                            //11
                            "CurCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {            
            if (Sm.StdMsgYN("Question", "Do you want to print this data ?") == DialogResult.No ||
                IsDataNotValid()
                ) return;

            string mDocNo = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdBool(Grd1, i, 1))
                {
                    if (mDocNo.Length > 0) mDocNo += ",";
                    mDocNo += Sm.GetGrdStr(Grd1, i, 4);
                }
            }

            mFrmParent.ProcessPrintPPH23(mDocNo);
        }

        private bool IsDataNotValid()
        {
            return IsGrdEmpty() || IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 &&
                    Sm.GetGrdBool(Grd1, r, 1)) return false;
            }
            Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 document.");
            return true;
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string
                DocType = Sm.GetGrdStr(Grd1, Row, 2),
                DocNo = Sm.GetGrdStr(Grd1, Row, 4);

            for (int r = 0; r < mFrmParent.Grd3.Rows.Count; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 2), DocType) && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, r, 4), DocNo)) return true;

            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

    }
}
