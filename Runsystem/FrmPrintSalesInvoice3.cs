﻿#region Update
/*
    11/02/2019 [MEY/KIM] aplikasi baru untuk print di proses Sales Invoice (based on DO To Customer)
    27/02/2019 [MEY/KIM] BUG Deskripsi tidak keluar di Printout Sales Invoice dan Kwitansi di menu baru Print Sales Invoice.
    28/02/2019 [MEY/KIM] Button Save dihapus
    10/01/2020 [WED/ALL] pakai parameter untuk print out nya
    09/03/2020 [WED/KIM] nampilkan nomor VA nya berdasarkan parameter VABNIAPIKey
    10/03/2020 [WED/KIM] rombak print out
    16/03/2020 [DITA/KIM] ganti sumber EmpName di print out
    11/12/2020 [DITA/KIM] Tambah VA mandiri di printout
 *  23/12/2020 [ICA/KIM] Penyesuaian printout
 *  21/06/2021 [VIN/KIM] dokumen SLI yang sudah Cancel tidak perlu ditampilkan 
 *  21/07/2022 [TRI/KIM] bug saat print
 */
#endregion

#region Namespace
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
#endregion

namespace RunSystem
{
    public partial class FrmPrintSalesInvoice3 : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty, mAccessInd = string.Empty;
        private string mEmpCodeSI = string.Empty, mEmpCodeTaxCollector = string.Empty, mCountPrintOut = string.Empty, mFormPrintOutInvoice3 = string.Empty, mFormPrintOutInvoice3Receipt = string.Empty, mVABNIAPIKey = string.Empty;
        internal bool mIsDOCtAmtRounded = false;

        #endregion

        #region Constructor

        public FrmPrintSalesInvoice3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mAccessInd, ref BtnSave, ref BtnExcel);
            BtnSave.Visible = false;
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            GetParameter();
            SetGrd();
            SetSQL();
            Sl.SetLueCtCode(ref LueCtCode);
        }
        #endregion

        #region Standard Method
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CtCode, B.CtName, A.Amt, A.CurCode ");
            SQL.AppendLine("From TblSalesInvoiceHdr A Inner Join TblCustomer B ON A.CtCode=B.CtCode");
            SQL.AppendLine("Where A.CancelInd='N' And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "Date",
                    "Customer",
                    "Currency",

                    //6
                    "Invoice Amount",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 150, 100, 200, 100, 
                    
                    //6
                    150, 
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }
        private void GetParameter()
        {
            mEmpCodeSI = Sm.GetParameter("EmpCodeSI");
            mEmpCodeTaxCollector = Sm.GetParameter("EmpCodeTaxCollector");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mCountPrintOut = Sm.GetParameter("CountPrintOut");
            mFormPrintOutInvoice3 = Sm.GetParameter("FormPrintOutInvoice3");
            mFormPrintOutInvoice3Receipt = Sm.GetParameter("FormPrintOutInvoice3Receipt");
            mVABNIAPIKey = Sm.GetParameter("VABNIAPIKey");
        }
        
        #region Button Method
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string mSLIDocNo = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++ )
            {
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    if (mSLIDocNo.Length > 0) mSLIDocNo += ",";
                    mSLIDocNo += Sm.GetGrdStr(Grd1, Row, 2);
                }
            }

            if (mSLIDocNo.Length > 0)
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                if (mCountPrintOut.Length <= 0) mCountPrintOut = "0";
                string SQL = "Select DocNo From TblSalesInvoiceHdr Where Find_In_Set(DocNo, @Param) And CountPrint >= " + mCountPrintOut + " Limit 1; ";

                if (Sm.IsDataExist(SQL.ToString(), mSLIDocNo))
                {

                    Sm.StdMsg(mMsgType.Warning, "This document exceeds maximum print, " + Sm.GetValue(SQL, mSLIDocNo) + ".");
                    return;
                }

                string[] mSLIDocNos = mSLIDocNo.Split(',');

                foreach (string d in mSLIDocNos)
                {
                    ParPrint(d);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No data to be printed.");
                return;
            }
        }

        #endregion

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-4
                            "DocDt", "CtName", "CurCode", "Amt",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);                  
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var l2 = new List<InvoiceHdr>();
            var ldtl = new List<InvoiceDtl>();
            var ldtl2 = new List<InvoiceDtl2>();
            var l3 = new List<Employee>();
            var l4 = new List<EmployeeTaxCollector>();

            string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "Employee", "EmployeeTaxCollector" };

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header KIM
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL2.AppendLine("A.DocNo, ");
            SQL2.AppendLine("Concat(Right(A.DocDt,2),' ',Case substring(A.DocDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
            SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
            SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DocDt,4))As DocDt, ");

            SQL2.AppendLine("Concat(Right(A.DueDt,2),' ',Case substring(A.DueDt,5,2) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' ");
            SQL2.AppendLine("WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' ");
            SQL2.AppendLine("WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END, ' ', Left(A.DueDt,4))As DueDt, ");

            SQL2.AppendLine("A.CurCode, A.TotalTax As TotalTax, A.TotalAmt As TotalAmt, A.DownPayment As DownPayment, ");
            SQL2.AppendLine("B.CtName, B.Address, D.SAName, D.SAAddress, D.Remark, ");
            SQL2.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1, ");
            SQL2.AppendLine("IfNull(A.TaxCode2, null) As TaxCode2, ");
            SQL2.AppendLine("IfNull(A.TaxCode3, null) As TaxCode3,");

            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL2.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
            SQL2.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL2.AppendLine("Inner Join TblSalesInvoiceHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
            SQL2.AppendLine("A.Amt, E.CityName, ");
            SQL2.AppendLine("F.CityName As SACityName, B.NPWP, A.Remark As RemarkSI, A.ReceiptNo, ");
            SQL2.AppendLine("G.ParValue As IsDOCtAmtRounded, ");
            SQL2.AppendLine("B.BankVirtualAccount BankVirtualAccountBNI, H.BankACNo BankVirtualAccountMandiri ");

            SQL2.AppendLine("From TblSalesInvoiceHdr A ");
            SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL2.AppendLine("Inner join ( ");
            SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo ");
            SQL2.AppendLine("		From TblSalesInvoiceHdr A ");
            SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo ");
            SQL2.AppendLine("		group by A.DocNo ");
            SQL2.AppendLine("	) C On A.DocNo=C.DocNo ");
            SQL2.AppendLine("Inner Join TblDoctHdr D On C.DOCtDocNo= D.DocNo ");
            SQL2.AppendLine("Left Join TblCity E On B.CityCode= E.CityCode ");
            SQL2.AppendLine("Left Join TblCity F On D.SACityCode= F.CityCode ");
            SQL2.AppendLine("Left Join TblParameter G On G.ParCode = 'IsDOCtAmtRounded' ");
            SQL2.AppendLine("Left Join TblCustomerBankVirtualAccount H On H.CtCode = B.CtCode And H.BankCode = '008' ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                {
                //0
                 "CompanyLogo",

                 //1-5
                 "CompanyName",
                 "CompanyAddress",
                 "CompanyAddressFull",
                 "CompanyPhone",
                 "CompanyFax",

                 //6-10
                 "DocNo",
                 "DocDt",
                 "DueDt",
                 "CurCode",
                 "TotalTax",

                 //11-15
                 "TotalAmt",
                 "DownPayment", 
                 "CtName",
                 "Address",
                 "SAName",

                 //16-20
                 "SAAddress",
                 "Remark",
                 "TaxCode1",
                 "TaxCode2",
                 "TaxCode3",

                 //21-25
                 "TaxName1",
                 "TaxName2",
                 "Taxname3",
                 "TaxRate1",
                 "TaxRate2",

                 //26-30
                 "TaxRate3",
                 "Amt",
                 "CityName",
                 "SACityName",
                 "NPWP",

                 //31-35
                 "RemarkSI",
                 "ReceiptNo",
                 "IsDOCtAmtRounded",
                 "BankVirtualAccountBNI",
                 "BankVirtualAccountMandiri"

                });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new InvoiceHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            CompanyFax = Sm.DrStr(dr2, c2[5]),

                            DocNo = Sm.DrStr(dr2, c2[6]),
                            DocDt = Sm.DrStr(dr2, c2[7]),
                            DueDt = Sm.DrStr(dr2, c2[8]),
                            CurCode = Sm.DrStr(dr2, c2[9]),
                            TotalTax = Sm.DrDec(dr2, c2[10]),

                            TotalAmt = Sm.DrDec(dr2, c2[11]),
                            DownPayment = Sm.DrDec(dr2, c2[12]),
                            CtName = Sm.DrStr(dr2, c2[13]),
                            Address = Sm.DrStr(dr2, c2[14]),
                            SAName = Sm.DrStr(dr2, c2[15]),

                            SAAddress = Sm.DrStr(dr2, c2[16]),
                            Remark = Sm.DrStr(dr2, c2[17]),
                            TaxCode1 = Sm.DrStr(dr2, c2[18]),
                            TaxCode2 = Sm.DrStr(dr2, c2[19]),
                            TaxCode3 = Sm.DrStr(dr2, c2[20]),

                            TaxName1 = Sm.DrStr(dr2, c2[21]),
                            TaxName2 = Sm.DrStr(dr2, c2[22]),
                            TaxName3 = Sm.DrStr(dr2, c2[23]),
                            TaxRate1 = Sm.DrDec(dr2, c2[24]),
                            TaxRate2 = Sm.DrDec(dr2, c2[25]),

                            TaxRate3 = Sm.DrDec(dr2, c2[26]),
                            Amt = Sm.DrDec(dr2, c2[27]),
                            //Decimal.Parse(TxtAmt.Text), //
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr2, c2[27])),
                            //Sm.Terbilang(Decimal.Parse(TxtAmt.Text)), //
                            CityName = Sm.DrStr(dr2, c2[28]),
                            SACityName = Sm.DrStr(dr2, c2[29]),
                            NPWP = Sm.DrStr(dr2, c2[30]),

                            RemarkSI = Sm.DrStr(dr2, c2[31]),
                            ReceiptNo = Sm.DrStr(dr2, c2[32]),
                            IsDOCtAmtRounded = Sm.DrStr(dr2, c2[33]),
                            BankVirtualAccountBNI = Sm.DrStr(dr2, c2[34]),
                            BankVirtualAccountMandiri = Sm.DrStr(dr2, c2[35]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, ");
                if (Doctitle == "KIM")
                    SQLDtl.AppendLine("IfNull(B.ForeignName, B.ItName) As ItName, ");
                else
                    SQLDtl.AppendLine("B.ItName, ");
                SQLDtl.AppendLine("A.Qty, A.UPriceAfterTax, ");
                if (mIsDOCtAmtRounded)
                    SQLDtl.AppendLine("Floor(A.Qty*A.UPriceAfterTax) As Amt, ");
                else
                    SQLDtl.AppendLine("A.Qty*A.UPriceAfterTax As Amt, ");
                SQLDtl.AppendLine("B.SalesuomCode As PriceUomCode, C.Remark ");
                SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                SQLDtl.AppendLine("Inner join tblitem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDOCthdr C On A.DOCtDocNo = C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo",DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "Qty",
                    "UPriceAfterTax",
                    "Amt",
                    "PriceUomCode",

                    "Remark"

                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new InvoiceDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            UPriceAfterTax = Sm.DrDec(drDtl, cDtl[3]),
                            Amt = Sm.DrDec(drDtl, cDtl[4]),
                            PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),

                            Remark = Sm.DrStr(drDtl, cDtl[6]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            #region Detail 2

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            bool ThereisAcInd = Sm.IsDataExist("Select AcInd From TblSalesInvoiceDtl2 Where AcInd = 'Y' And DocNo = @Param Limit 1; ", DocNo);
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                if (ThereisAcInd)
                {
                    SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.CAmt),0) As CAmt, ");
                    if (Doctitle == "KIM")
                        SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)*-1 As DAmt, ifnull(sum(B.DAmt),0)*-1 + ifnull(sum(B.CAmt),0) As TAmt ");
                    else
                        SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");

                    SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                    SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                    SQLDtl2.AppendLine("Group by B.Docno ");
                }
                else if (!ThereisAcInd && Doctitle == "KIM")
                {
                    SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.CAmt),0) As CAmt, ");
                    SQLDtl2.AppendLine("ifnull(sum(B.DAmt),0)*-1 As DAmt, ifnull(sum(B.DAmt),0)*-1 + ifnull(sum(B.CAmt),0) As TAmt ");
                    SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                    SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='N' And AcNo Not Like concat('%.', A.ctcode) ");
                    SQLDtl2.AppendLine("Group by B.Docno ");
                }
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                        //0
                        "DocNo" ,

                        //1-2
                        "DAmt" ,
                        "CAmt",
                        "TAmt",

                });

                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new InvoiceDtl2()
                        {
                            DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                            CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                            TAmt = Sm.DrDec(drDtl2, cDtl2[3]),
                            ThereIsAcInd = ThereisAcInd,
                        });
                    }
                }

                drDtl2.Close();
            }

            myLists.Add(ldtl2);

            #endregion

            #region Signature KIM
            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
            SQL3.AppendLine("From TblEmployee A ");
            SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            SQL3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeSI);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                {
                 //0-3
                 "EmpCode",
                 "EmpName",
                 "PosName",
                 "EmpPict"
                
                });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr3, c3[0]),

                            EmpName = Sm.DrStr(dr3, c3[1]),
                            Position = Sm.DrStr(dr3, c3[2]),
                            EmpPict = Sm.DrStr(dr3, c3[3]),
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            #region Signature2 KIM

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();
            string SPCode = Sm.GetValue("Select T2.SPCode From TblSalesInvoiceHdr T1 Inner Join TblSalesPerson T2 On T1.SalesName = T2.SPName And T1.DocNo = @Param Limit 1;", DocNo);

            //SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, A.Mobile ");
            //SQL4.AppendLine("From TblEmployee A ");
            //SQL4.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            //SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

            SQL4.AppendLine("Select C.EmpCode, C.EmpName, C.Mobile, D.PosName ");
            SQL4.AppendLine("FROM TblSalesPerson A ");
            SQL4.AppendLine("LEFT JOIN TblUser B ON A.UserCode = B.UserCode ");
            SQL4.AppendLine("LEFT JOIN TblEmployee C ON B.UserCode = C.UserCode ");
            SQL4.AppendLine("LEFT JOIN TblPosition D ON C.PosCode = D.PosCode ");
            SQL4.AppendLine("Where A.SPCode = @SPCode; ");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                //Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeTaxCollector);
                Sm.CmParam<String>(ref cm4, "@SPCode", SPCode);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                {
                 //0-3
                 "EmpCode",
                 "EmpName",
                 "PosName",
                 "Mobile"
                
                });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new EmployeeTaxCollector()
                        {
                            EmpCode = Sm.DrStr(dr4, c4[0]),

                            EmpName = Sm.DrStr(dr4, c4[1]),
                            Position = Sm.DrStr(dr4, c4[2]),
                            Mobile = Sm.DrStr(dr4, c4[3]),
                        });
                    }
                }
                dr4.Close();
            }
            myLists.Add(l4);

            #endregion

            //if (Doctitle == "KIM")
            //{
            //    Sm.PrintReport("Invoice3KIM2", myLists, TableName, false);
            //    Sm.PrintReport("ReceiptSIKIM2", myLists, TableName, false);
            //    UpdatePrint(DocNo);
            //}
            Sm.PrintReport(mFormPrintOutInvoice3, myLists, TableName, false);
            Sm.PrintReport(mFormPrintOutInvoice3Receipt, myLists, TableName, false);
            UpdatePrint(DocNo);
        }


        #endregion
     
        #region Additional Method

        private MySqlCommand SaveCountData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceHdr ");
            SQL.AppendLine("Set CountPrint = CountPrint + 1 ");
            SQL.AppendLine("Where Find_In_Set(DocNo, @DocNo)");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }
        private void UpdatePrint(string DocNo)
        {
            var cml = new List<MySqlCommand>();
            cml.Add(SaveCountData(DocNo));
            Sm.ExecCommands(cml);

        }
 
        #endregion

        #endregion

        #region Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
  
        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string NPWP { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
            public string IsDOCtAmtRounded { get; set; }
            public string BankVirtualAccountBNI { get; set; }
            public string BankVirtualAccountMandiri { get; set; }
        }

        class InvoiceDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
        }

        class InvoiceDtl2
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }

            public bool ThereIsAcInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        #endregion

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
        }

        #endregion


        #endregion

    }
}
