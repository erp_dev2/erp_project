﻿#region Update
/* 
    13/12/2017 [HAR] Bug Fixing PO nya gak muncul
    23/12/2017 [TKG] Berdasarkan parameter GrpCodeForCashier, data hanya bisa dilihat untuk TOP cash saja.
    11/01/2018 [HAR] tambah informasi purchase return invoice document amount dan balance
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasingBook : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private int mNumberOfInventoryUomCode = 1;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasingBook(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                GetParameter();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameter("IsFilterByItCt") == "Y";
            SetNumberOfInventoryUomCode();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select P.DoVdDocNo, P.DOVdDocDt, M.DocNO As MRDocNo, M.DocDt As MRDocDt, D.DocNO As RecvVdDocNo, ");
            SQL.AppendLine("    D.DocDt As RecvVdDocDt, D.WhsCode, E.Whsname, G.DocNo As PODocNo,  A.DocnO As PIDocNo, A.DocDt As PIDocDt, D.VdDoNo, A.VdInvNo, ");
            SQL.AppendLine("    Concat(ifnull(A.TaxinvoiceNo, ''), ifnull(A.TaxinvoiceNo2, ''), ifnull(A.TaxinvoiceNo3, '')) Taxinvoice, ");
            SQL.AppendLine("    H.OptDesc As PaymentType, G.VdCode, I.vdname, L.itCode, N.Itname, SUM(C.Qty) As Qty, N.InventoryUomCode, ");
            SQL.AppendLine("    SUM(C.Qty2) As Qty2, N.InventoryUomCode2, SUM(C.Qty3) As Qty3, N.InventoryUomCode3, A.CurCode, O.Uprice,  ");
            SQL.AppendLine("    A.Amt As TotalWithouttax, A.taxAmt, (A.Amt+A.TaxAmt) As AmtWithTax, A.Downpayment,  ");
            SQL.AppendLine("    ((A.Amt+A.TaxAmt)-A.Downpayment) As invoiceAmt, N.ItCtCode, P.PriDocNo, P.PriAmt, B.remark ");
            SQL.AppendLine("    From TblPurchaseInvoicehdr A ");
            SQL.AppendLine("    Inner Join TblPurchaseinvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblrecvVdDtl C On B.RecvvdDocno = C.DocNo And B.recvVdDno = C.Dno And C.CancelInd = 'N'  ");
            SQL.AppendLine("    Inner Join TblRecvVdHdr D On C.DocnO = D.DocNo And POInd = 'Y' ");
            SQL.AppendLine("    Inner Join TblWarehouse E on D.WhsCode = E.WhsCode ");
            SQL.AppendLine("    Inner Join TblPODtl F On C.PODocNo = F.DocNo And C.PODno = F.Dno And F.CancelInd ='N' ");
            SQL.AppendLine("    Inner Join tblPOHdr G On F.DocNo = G.DocnO ");
            SQL.AppendLine("    Left Join TblOption H On A.PAymentType = H.OptCode And H.OptCat = 'VoucherPaymentType' ");
            SQL.AppendLine("    Inner Join Tblvendor I On G.VdCode = I.VdCode  ");
            SQL.AppendLine("    Inner Join TblPORequestDtl J ");
            SQL.AppendLine("        On F.PorequestDocNo = J.DocNo ");
            SQL.AppendLine("        And F.PoRequestDno = J.Dno  ");
            SQL.AppendLine("        And J.CancelInd = 'N' And  J.Status = 'A'  ");
            SQL.AppendLine("    Inner Join TblPORequestHdr K On J.DocNo = K.DocNo ");
            SQL.AppendLine("    Inner Join TblmaterialRequestDtl L ");
            SQL.AppendLine("        On J.MaterialRequestDocNo = L.DocNo  ");
            SQL.AppendLine("        And J.MaterialRequestDno = L.Dno ");
            SQL.AppendLine("        And L.CancelInd = 'N' ");
            SQL.AppendLine("        And L.Status = 'A' ");
            SQL.AppendLine("        And L.ProcessInd = 'F' ");
            SQL.AppendLine("    Inner Join tblmaterialRequesthdr M On L.DocNo = M.DocNo ");
            SQL.AppendLine("    Inner Join TblItem N On L.ItCode = N.ItCode ");
            SQL.AppendLine("    Inner Join TblQtDtl O On J.QtDocNo = O.DocNo And J.QtDno = O.Dno ");
            
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select B.RecvVdDocNO, B.RecvVdDno, A.DocNo As DOVdDocNo, A.DocDt As DOVDDocDt,  ");
	        SQL.AppendLine("        B.Dno As DOVdDno, C.Docno As PRIDocNo, C.Dno As PRIDNo, D.Amt AS PriAmt  ");
	        SQL.AppendLine("        From TblDoVdhdr A ");
	        SQL.AppendLine("        Inner Join TblDOVdDtl B On A.DocNo = B.DocNO And B.CancelInd  = 'N' ");
	        SQL.AppendLine("        Left Join tblPurchaseReturnInvoiceDtl C On  B.DocNo = C.DOVdDocNo And B.Dno = C.DOVdDno ");
	        SQL.AppendLine("        Left Join tblPurchaseReturnInvoicehDR D On C.DocNo = D.DocNO And D.CancelInd = 'N' ");
            SQL.AppendLine("    )P On C.DocNo = P.RecvVdDocNo And C.DNo = P.RecvVdDNo  ");

            if (Sm.IsDataExist(
                    "Select 1 From TblUser " +
                    "Where UserCode=@Param " +
                    "And GrpCode Is Not Null " +
                    "And Find_In_Set(GrpCode, " +
                    "(Select ParValue From TblParameter " +
                    "Where ParCode='GrpCodeForCashier' " +
                    "And ParValue Is Not Null)); ",
                    Gv.CurrentUserCode))
            {
                SQL.AppendLine("    Inner Join TblQtHdr R ");
                SQL.AppendLine("        On O.DocNo=R.DocNo  ");
                SQL.AppendLine("        And Find_In_Set(R.PtCode, ");
                SQL.AppendLine("            (Select ParValue From TblParameter ");
                SQL.AppendLine("            Where ParCode='PtCodeCash' ");
                SQL.AppendLine("            And ParValue Is Not Null)) ");
            }

            SQL.AppendLine("    Where A.cancelInd = 'N' ");
            SQL.AppendLine("    And (D.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Group By P.DoVdDocNo, P.DOVdDocDt, D.DocNO, D.DocDt, D.WhsCode, E.Whsname, D.DocnO, D.VdDoNo, A.VdInvNo,  ");
            SQL.AppendLine("    H.OptDesc, I.vdname, L.itCode, N.Itname ");
            SQL.AppendLine(")T Where 0=0 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "DO to"+Environment.NewLine+"Vendor",
                        "DO To"+Environment.NewLine+"Vendor Date",
                        "",
                        "Material"+Environment.NewLine+"Request",
                        "Material"+Environment.NewLine+"Request Date",
                        //6-10
                        "",
                        "Receiving",
                        "Receiving"+Environment.NewLine+"Date",
                        "",
                        "Warehouse"+Environment.NewLine+"Code",
                        //11-15
                        "Warehouse",
                        "Purchase Order",
                        "",
                        "Purchase"+Environment.NewLine+"Invoice",
                        "Purchase"+Environment.NewLine+"Invoice Date",
                       
                        //16-20
                        "",
                        "Vendor DO",
                        "Vendor"+Environment.NewLine+"Invoice No",
                        "Tax Invoice",
                        "Payment"+Environment.NewLine+"Type",
                        //21-25
                        "Vendor",
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "Quantity",
                        "UoM",
                        //26-30 
                        "Quantity 2",
                        "UoM 2",
                        "Quantity 3",
                        "UoM 3",
                        "Currency",
                        //31-35
                        "Unit"+Environment.NewLine+"Price",
                        "Total"+Environment.NewLine+"Without Tax",
                        "Tax"+Environment.NewLine+"Amount",
                        "Amount"+Environment.NewLine+"With Tax",
                        "Downpayment",
                        //36-40
                        "Invoice"+Environment.NewLine+"Amount",
                        "Purchase"+Environment.NewLine+"Return Invoice",
                        "Purchase"+Environment.NewLine+"Return Amount",
                        "Balance Amount",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 120, 20, 150, 120, 
                        //6-10
                        20, 150, 120, 20, 100, 
                        //11-15
                        150, 150, 20, 150, 120,   
                        //16-20
                        20, 150, 150, 150, 120,  
                        //21-25
                        200, 100, 120, 100, 100, 
                        //26-30
                        100, 100,  100, 100, 100,   
                        //31-35
                        120, 120, 120, 120, 120, 
                        //36-40
                        120, 180, 120, 120, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3,6,9,13,16 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5, 8, 15 });
            Sm.GrdFormatDec(Grd1, new int[] {24, 26, 28, 31, 32, 33, 34, 35, 36, 38, 39 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1,2, 4, 5, 7, 8, 10, 11, 12, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 });
            Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }


        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28, 29 }, true);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtRecvVdDocNo.Text, new string[] { "T.RecvVdDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, new string[] { "T.MRDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPIDocNo.Text, new string[] { "T.PIDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, new string[] { "T.PODocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "T.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "DoVdDocNo", 
                            //1-5
                            "DOVdDocDt", "MRDocNo", "MRDocDt", "RecvVdDocno", "RecvVdDocDt", 
                            //6-10
                            "WhsCode", "WhsName", "PODocNo", "PIDocNo", "PIDocDt",   
                            //11-15
                            "VdDoNo", "VdInvNo", "Taxinvoice",  "PaymentType", "vdname",   
                            //16-20
                            "ItCode", "Itname", "Qty", "InventoryUomCode", "Qty2",   
                            //21-25
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",  
                            //26-30
                            "TotalWithouttax", "taxAmt", "AmtWithTax", "Downpayment", "invoiceAmt", 
                            //33
                            "PriDocNo", "PriAmt", "Remark"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 33);
                            Grd1.Cells[Row, 39].Value = Sm.FormatNum(Sm.GetGrdDec(Grd, Row, 36)-Sm.GetGrdDec(Grd, Row, 38), 0);
                        }, true, false, false, false
                    );
                //Grd1.GroupObject.Add(4);
                //Grd1.GroupObject.Add(7);
                //Grd1.GroupObject.Add(9);
                //Grd1.GroupObject.Add(11);
                //Grd1.GroupObject.Add(13);
                //Grd1.GroupObject.Add(15);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 24, 26, 28, 31, 32, 33, 34, 35, 36, 38 });
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.mMenuCode = string.Empty;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.mMenuCode = string.Empty;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkRecvVdDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material Request#");
        }

        private void ChkPORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO Request#");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkPIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchase Invoice#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtRecvVdDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }


        private void ChkPODocNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO DocNot#");
        }

        private void TxtPODocNo_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion

        
    }
}
