﻿#region Update
/* 
    01/02/2021 [VIN/PHT] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetMaintenanceYearlyDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetMaintenanceYearly mFrmParent;
        private string mSQL = string.Empty, 
            mDocNo = string.Empty, 
            mBudgetRequestSeqNo=string.Empty,
            mBCCode=string.Empty;
        private int mRow = 0;
        #endregion

        #region Constructor

        public FrmBudgetMaintenanceYearlyDlg2(FrmBudgetMaintenanceYearly FrmParent, int Row, string DocNo, string BudgetRequestSeqNo, string BCCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mDocNo = DocNo;
            mBudgetRequestSeqNo = BudgetRequestSeqNo;
            mBCCode = BCCode;
            //mCCCode = GetCCCode(CCtCode);
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {

            try
            {
                if (this.Text.Length == 0) this.Text = "List of Item Budget Request";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                ShowData();
                TxtBCCode.Text = mBCCode;
                TxtBCName.Text = Sm.GetValue("Select BCName from TblBudgetCategory where BCCode ='"+mBCCode+"' ");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                    "",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "UoM",
                    "Rate 01",
                    "Quantity 01",

                    //6-10
                    "Amount 01",
                    "Rate 02",
                    "Quantity 02",
                    "Amount 02",
                    "Rate 03",

                    //11-15
                    "Quantity 03",
                    "Amount 03",
                    "Rate 04",
                    "Quantity 04",
                    "Amount 04",

                    //16-20
                    "Rate 05",
                    "Quantity 05",
                    "Amount 05",
                    "Rate 06",
                    "Quantity 06",

                    //21-25
                    "Amount 06",
                    "Rate 07",
                    "Quantity 07",
                    "Amount 07",
                    "Rate 08",

                    //26-30
                    "Quantity 08",
                    "Amount 08",
                    "Rate 09",
                    "Quantity 09",
                    "Amount 09",

                    //31-35
                    "Rate 10",
                    "Quantity 10",
                    "Amount 10",
                    "Rate 11",
                    "Quantity 11",

                    //36-39
                    "Amount 11",
                    "Rate 12",
                    "Quantity 12",
                    "Amount 12"
                    },
                    new int[] 
                    {
                        //0
                    20,

                    //1-5
                    100, 200, 100, 130, 130,
                    
                    //6-10
                    130, 130, 130, 130, 130,

                    //11-15
                    130, 130, 130, 130, 130,

                    //16-20
                    130, 130, 130, 130, 130,

                    //21-25
                    130, 130, 130, 130, 130,

                    //26-30
                    130, 130, 130, 130, 130, 

                    //31-35
                    130, 130, 130, 130, 130, 

                    //36-39
                    130, 130, 130, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 }, false);
            
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                var cm = new MySqlCommand();
                var Filter = string.Empty;
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT  ");
                SQL.AppendLine("D.ItName, D.ItCode, E.UomName, ");
                SQL.AppendLine("C.Rate01*C.Qty01 Amt01, C.Rate02*C.Qty02 Amt02, C.Rate03*C.Qty03 Amt03,C.Rate04*C.Qty04 Amt04,C.Rate05*C.Qty05 Amt05, ");
                SQL.AppendLine("C.Rate06*C.Qty06 Amt06,C.Rate07*C.Qty07 Amt07,C.Rate08*C.Qty08 Amt08,C.Rate09*C.Qty09 Amt09, ");
                SQL.AppendLine("C.Rate10*C.Qty10 Amt10,C.Rate11*C.Qty11 Amt11,C.Rate12*C.Qty12 Amt12, ");
                SQL.AppendLine("C.Qty01, C.Qty02, C.Qty03, C.Qty04, C.Qty05, C.Qty06, C.Qty07, C.Qty08, C.Qty09, C.Qty10, C.Qty11, C.Qty12, ");
                SQL.AppendLine("C.Rate01, C.Rate02, C.Rate03, C.Rate04, C.Rate05, C.Rate06, C.Rate07, C.Rate08, C.Rate09, C.Rate10, C.Rate11, C.Rate12 ");
                SQL.AppendLine("FROM tblbudgetrequestyearlyhdr A ");
                SQL.AppendLine("INNER JOIN tblbudgetrequestyearlydtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("INNER JOIN tblbudgetrequestyearlydtl2 C ON A.DocNo=C.DocNo AND B.BCCode=C.BCCode  ");
                SQL.AppendLine("INNER JOIN tblitem D ON C.ItCode=D.ItCode ");
                SQL.AppendLine("INNER JOIN tbluom E ON D.PurchaseUOMCode = E.UomCode ");

                SQL.AppendLine("WHERE A.DocNo='" + mFrmParent.TxtBudgetRequestDocNo.Text + "' AND B.SeqNo='" + mBudgetRequestSeqNo + "' AND B.BCCode='"+mBCCode+"' ");

                Cursor.Current = Cursors.WaitCursor;
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                         //0
                         "ItName", 
                         //1-5
                         "UomName", "Rate01", "Qty01", "Amt01", "Rate02", 
                         //6-10
                         "Qty02", "Amt02", "Rate03", "Qty03", "Amt03", 
                         //11-15
                         "Rate04", "Qty04", "Amt04", "Rate05","Qty05",  
                         //16-20
                         "Amt05", "Rate06", "Qty06", "Amt06", "Rate07", 
                         //21-25
                         "Qty07", "Amt07", "Rate08", "Qty08", "Amt08", 
                         //26-30
                         "Rate09", "Qty09",  "Amt09", "Rate10", "Qty10", 
                         //31-35
                         "Amt10", "Rate11", "Qty11", "Amt11", "Rate12", 
                         //36-40
                         "Qty12", "Amt12", "ItCode"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 38);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 32);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 33);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 35);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 36);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 37);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
           
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

    }
}
