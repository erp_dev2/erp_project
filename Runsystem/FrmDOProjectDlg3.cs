﻿#region Update
/*
    10/12/2019 [WED/IMS] new apps --> DO To Project
    06/02/2020 [TKG/IMS] menampilkan informasi project implementation's item
    16/04/2020 [TKG/IMS] tambah so contract#, item finished good
    24/04/2020 [IBL/IMS] tambah kolom SO Contract# dan filter SO Contract#
    30/09/2020 [IBL/IMS] tambah informasi project name dan PO customer#
    17/11/2020 [DITA/IMS] beri method cleargrd saat choose data -> memilih data prji baru
    18/11/2020 [TKG/IMS] mengganti specification dengan remark
    12/02/2021 [WED/IMS] PRJI Draft bisa tampil, yg terpilih sekalian SOCDNo nya
    25/03/2021 [WED/IMS] validasi data yg muncul berdasarkan SeqNo nya
    09/07/2021 [TRI/IMS] item masih double dan belum urut, ditambahkan join pakai no di table tblsocontractdtl
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOProjectDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOProject mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOProjectDlg3(FrmDOProject FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, J.DNo SOContractDNo, H.SeqNo, A.DocDt, G.CtName, F.ProjectCode, ");
            SQL.AppendLine("    F.ProjectName, C.PONo, A.SOContractDocNo, ");
            SQL.AppendLine("    I.ItCode, I.ItName, I.ItCodeInternal, H.Qty, I.PurchaseUomCode, J.Remark, J.no ");
            SQL.AppendLine("    From TblProjectImplementationHdr A ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr B ON A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    Inner JOIN TblSOContractHdr C On B.SOCDocNo=C.DocNo ");
            SQL.AppendLine("    Inner JOIN TblBOQHdr D ON C.BOQDocno = D.DocNo ");
            SQL.AppendLine("    Inner JOIN TblLOPHdr E ON D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("    Inner JOIN TblProjectGroup F ON E.PGCode = F.PGCode ");
            SQL.AppendLine("    Inner JOIN TblCustomer G ON E.CtCode = G.CtCode ");
            //SQL.AppendLine("    Inner Join TblBOQDtl3 H ON D.DocNo=H.DocNo ");

            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DocNo, T2.SOCDocNo, T5.ItCode, T5.SeqNo, T4.Qty, T5.No ");
            SQL.AppendLine("        From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        Inner JOin TblSOCOntractRevisionHdr T2 On T1.SOContractDocNo = T2.DocNo ");
            SQL.AppendLine("            And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Inner JOin TblSOCOntractHdr T3 On T2.SOCDocNo = T3.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQDtl3 T4 On T3.BOQDocNo = T4.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl5 T5 On T3.DocNo = T5.DocNo And T4.ItCode = T5.ItCode And T4.SeqNo = T5.SeqNo ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select T1.DocNo, T2.SOCDocNo, T5.ItCode, T5.SeqNo, T4.Qty, T5.No ");
            SQL.AppendLine("        From TblProjectImplementationHdr T1 ");
            SQL.AppendLine("        Inner JOin TblSOCOntractRevisionHdr T2 On T1.SOContractDocNo = T2.DocNo ");
            SQL.AppendLine("            And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Inner JOin TblSOCOntractHdr T3 On T2.SOCDocNo = T3.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQDtl2 T4 On T3.BOQDocNo = T4.DocNo ");
            SQL.AppendLine("        Inner Join TblSOContractDtl4 T5 On T3.DocNo = T5.DocNo And T4.ItCode = T5.ItCode And T4.SeqNo = T5.SeqNo ");
            SQL.AppendLine("    ) H On A.DocNo = H.DocNo And C.DocNo = H.SOCDocNo ");

            SQL.AppendLine("    Inner Join TblItem I On H.ItCode=I.ItCode ");
            SQL.AppendLine("    Inner Join TblSOContractDtl J ON C.DocNo=J.DocNo And H.ItCode=J.ItCode and H.No=J.No  ");
            SQL.AppendLine("    Where A.DocType = '2' ");
            SQL.AppendLine("    And A.Status = 'A' ");
            SQL.AppendLine("    And A.ProcessInd In ('F', 'D') ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Project's Code",

                    //6-10
                    "Project's Name",
                    "Customer's PO#",
                    "SO Contract#",
                    "Item's Code",
                    "Item's Name",

                    //11-15
                    "Local Code",
                    "Quantity",
                    "UoM",
                    "Remark",
                    "SO Contract#",

                    //16
                    "SOContractDNo"
                },
                new int[]
                {
                    50,
                    150, 20, 80, 200, 100,
                    200, 180, 180, 100, 200,
                    130, 100, 80, 250, 180,
                    0
                }
            );
            Grd1.Cols[15].Move(5);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 16 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation2("***");
                f.Tag = "***";
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProjectImplementation2("***");
                f.Tag = "***";
                f.Text = Sm.GetMenuDesc("FrmProjectImplementation2");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, "T.PONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSONo.Text, "T.SOContractDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "T.ProjectCode", "T.ProjectName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocDt Desc, T.DocNo, cast(T.SOContractDNo AS INT), CAST(T.No AS INT), T.SeqNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CtName", "ProjectCode", "ProjectName", "PONo",

                        //6-10
                        "SOContractDocNo",  "ItCode", "ItName", "ItCodeInternal", "Qty", 
                        
                        //11-14
                        "PurchaseUomCode", "Remark", "SOContractDocNo", "SOContractDNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearGrd();
                mFrmParent.TxtPRJIDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.mSOContractDNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 16);
                mFrmParent.ShowProjectImplementationItem(mFrmParent.TxtPRJIDocNo.Text, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 16));
                this.Hide();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void TxtSONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        #endregion

        private void Grd1_Click(object sender, EventArgs e)
        {

        }

        #endregion

    }
}