﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemSalesTarget : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmItemSalesTargetFind FrmFind;
        public string mSPCode = string.Empty;
        public string mUniCode = string.Empty;
        internal int mStateIndicator = 0;
 
        #endregion

        #region Constructor

        public FrmItemSalesTarget(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bill of Material";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                base.FrmLoad(sender, e);

                Sl.SetLueYr(LueYr, "");

                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Item Code",
                        "",
                        "Item Local Code",
                        "Item Category",
                        "Item Name",
                        
                        //6-10 
                        "Foreign Name",
                        "Amount",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 20, 150, 150, 250, 
                        
                        //6-10
                        180, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtSpName, LueYr, MeeRemark, MeeCancelReason, ChkCancelInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnSpName.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueYr, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 2, 7 });
                    BtnSpName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       MeeCancelReason
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSpName, LueYr, MeeRemark, MeeCancelReason, ChkCancelInd     
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemSalesTargetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            mStateIndicator = 1;

            Sm.FormShowDialog(new FrmItemSalesTargetDlg(this));
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = 2;
            string SPCodeEdit = Sm.GetValue("Select SpCode From Tblsalesperson Where SpName ='"+TxtSpName.Text+"'"); 
            mUniCode = Sm.GetValue("Select * From TblItemSalesTargetHdr Where Yr = '"+Sm.GetLue(LueYr)+"' And SpCode = '"+SPCodeEdit+"'");
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mStateIndicator == 1)
                    InsertData();
                else 
                    if (mStateIndicator == 2)
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        
        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            decimal UniCode = (Decimal.Parse(Sm.GetValue("SELECT IFNULL(MAX(X.UniqueCode), 0) FROM TblItemSalesTargetHdr X WHERE X.SpCode = '" + mSPCode + "'"))) + 1;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveItemSalesTargetHdr(UniCode));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveItemSalesTargetDtl(UniCode, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(UniCode.ToString(), mSPCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
               Sm.IsTxtEmpty(TxtSpName, "Name", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsDataAlreadyExists() ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least one item.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyExists()
        {
            var SQLd = new StringBuilder();

            SQLd.AppendLine("SELECT * FROM TblItemSalesTargetHdr ");
            SQLd.AppendLine("WHERE SpCode = @SpCode AND Yr = @Yr AND CancelInd = 'N'  ");
            SQLd.AppendLine("ORDER BY UniqueCode DESC LIMIT 1 ");

            var cm = new MySqlCommand() { CommandText = SQLd.ToString() };
            Sm.CmParam<String>(ref cm, "@SpCode", mSPCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already exist.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Document# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, true,
                        "Document# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Document Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Document's Source : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.") 
                    ) return true;
            return false;
        }

        private MySqlCommand SaveItemSalesTargetHdr(decimal UniCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "INSERT INTO TblItemSalesTargetHdr(UniqueCode, SpCode, CancelInd, Yr, Remark, CancelReason, CreateBy, CreateDt) " +
                    "VALUES(@UniqueCode, @SpCode, 'N', @Yr, @Remark, @CancelReason, @UserCode, CurrentDateTime()) "
            };
           
            Sm.CmParam<String>(ref cm, "@UniqueCode", UniCode.ToString());
            Sm.CmParam<String>(ref cm, "@SpCode", mSPCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
           
            return cm;
        }

        private MySqlCommand SaveItemSalesTargetDtl(decimal UniCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemSalesTargetDtl(UniqueCode, SPCode, Dno, ItCode, Amt, CreateBy, CreateDt) " +
                    "Values(@UniqueCode, @SPCode, @Dno, @ItCode, @Amt, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@UniqueCode", UniCode.ToString());
            Sm.CmParam<String>(ref cm, "@Dno", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SPCode", mSPCode);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            CancelItemSalesTargetHdr();
            ShowData(mUniCode.ToString(), mSPCode);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSpName, "Name", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQLCl = new StringBuilder();

            SQLCl.AppendLine("SELECT * FROM TblItemSalesTargetHdr T1 WHERE T1.CancelInd='Y' AND T1.UniqueCode=@UniqueCode AND T1.SpCode = @SpCode; ");

            var cm = new MySqlCommand() { CommandText = SQLCl.ToString() };
            Sm.CmParam<String>(ref cm, "@UniqueCode", mUniCode);
            Sm.CmParam<String>(ref cm, "@SpCode", mSPCode);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private void CancelItemSalesTargetHdr()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblItemSalesTargetHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Yr=@Yr And UniqueCode=@UniqueCode And CancelInd='N' And SpCode=@SpCode; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@UniqueCode", mUniCode);
            Sm.CmParam<String>(ref cm, "@SpCode", mSPCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSpName, "Name", false);
        }

        #endregion

        #region Show Data

        public void ShowData(string UniqueCode, string SpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowItemSalesTargetHdr(UniqueCode, SpCode);
                ShowItemSalesTargetDtl(UniqueCode, SpCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowItemSalesTargetHdr(string UniqueCode, string SpCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@UniqueCode", UniqueCode);
            Sm.CmParam<String>(ref cm, "@SpCode", SpCode);

            SQL.AppendLine("SELECT A.UniqueCode, A.SpCode, B.SpName, A.CancelInd, A.Yr, A.Remark, A.CancelReason ");
            SQL.AppendLine("FROM TblItemSalesTargetHdr A ");
            SQL.AppendLine("INNER JOIN TblSalesPerson B ON A.SpCode = B.SpCode ");
            SQL.AppendLine("WHERE A.UniqueCode = @UniqueCode AND A.SpCode = @SpCode ");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "UniqueCode",
                            //1-5
                            "SpCode", "SpName", "CancelInd", "Yr", "Remark",
                            //6
                            "CancelReason"

                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        UniqueCode = Sm.DrStr(dr, c[0]);
                        SpCode = Sm.DrStr(dr, c[1]);
                        TxtSpName.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]); ;
                    }, true
                );
        }


        private void ShowItemSalesTargetDtl(string UniqueCode, string SpCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@UniqueCode", UniqueCode);
            Sm.CmParam<String>(ref cm, "@SpCode", SpCode);

            SQL.AppendLine("SELECT B.Dno, C.ItCode, C.ItCodeInternal, D.ItCtName, C.ItName, C.ForeignName, B.Amt  ");
            SQL.AppendLine("FROM TblItemSalesTargetHdr A ");
            SQL.AppendLine("INNER JOIN TblItemSalesTargetDtl B On A.UniqueCode = B.UniqueCode And A.SPCode = B.SPCode ");
            SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("WHERE A.UniqueCode = @UniqueCode AND A.SpCode = @SpCode ");

            Sm.ShowDataInGrid(
               ref Grd1, ref cm, SQL.ToString(),
               new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "ItCode", "ItCodeInternal", "ItCtName", "ItName", "ForeignName", 
                    //6
                    "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7});
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && mStateIndicator == 1)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmItemSalesTargetDlg2(this));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (mStateIndicator == 1 || mStateIndicator == 2) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmItemSalesTargetDlg2(this));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 6, 10 }, e.ColIndex) && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;
        }

        #endregion

        #region Additional Method

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event
        private void BtnSpName_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemSalesTargetDlg(this));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        #endregion
    }
}
