﻿#region Update
/*
    21/11/2022 [MAU/BBT] Mengubah Location menjadi Site
    02/12/2022 [IBL/BBT] Feedback:LueAssetCategory tidak muncul datanya
                         Asset Category From, Cost Center To, Site To, Asset Category To belum autofill.
                         AC from, AC to, Remark hilang saat setelah save.
    12/12/2022 [MAU/BBT] Mengubah Site To dan Asset Category To menjadi mandatory
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Collections;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransferRequest2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmAssetTransferRequest2Find FrmFind;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsFilterByCC = false;
        internal bool mIsAssetTransferUseLocation = false;

        #endregion

        #region Constructor

        public FrmAssetTransferRequest2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Asset Transfer Request";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCCCode(ref LueCCCode, mIsFilterByCC ? "Y" : "N"); 
                Sl.SetLueCCCode(ref LueCCCode2);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueSiteCode(ref LueSiteCode2);
                Sl.SetLueAssetCategoryCode(ref LueAssetCatCode);
                Sl.SetLueAssetCategoryCode(ref LueAssetCatCode2);
                

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Asset Code",
                        "",
                        "Asset Name",
                        "Display Name",
                        //6-7
                        "Remark",
                        "Asset Source"
                    },
                     new int[] 
                    {
                        20, 
                        20, 100, 20, 300, 250, 
                        200, 60
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCCCode, LueCCCode2, MeeRemark, LueSiteCode, LueSiteCode2,
                        LueAssetCatCode, LueAssetCatCode2
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCCCode2, LueAssetCatCode2, LueSiteCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCCCode, LueCCCode2, MeeRemark, LueSiteCode2, LueSiteCode,
                LueAssetCatCode, LueAssetCatCode2
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssetTransferRequest2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ") && !IsGrdExceedMaxRecords())
                            Sm.FormShowDialog(new FrmAssetTransferRequest2Dlg(this, e.RowIndex));
                    }
                }
              
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "1")
                {
                    var f = new FrmAsset2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "2")
                {
                    var f = new FrmAsset3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete && Sm.GetGrdStr(Grd1, 0, 2).Length == 0)
                {
                    ClearData();
                    Sm.SetDteCurrentDate(DteDocDt);
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) 
            {
                if(!IsGrdExceedMaxRecords())
                    Sm.FormShowDialog(new FrmAssetTransferRequest2Dlg(this, e.RowIndex));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "1")
                {
                    var f = new FrmAsset2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "2")
                {
                    var f = new FrmAsset3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

       
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AssetTransferRequest", "TblAssetTransferRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAssetTransferRequestHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveAssetTransferRequestDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") || 
                Sm.IsLueEmpty(LueSiteCode2, "Site To") || 
                Sm.IsLueEmpty(LueAssetCatCode2, "Asset Category")||
                Sm.IsLueEmpty(LueCCCode2, "Cost Center") || 
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }



        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 asset.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You can only fill in one asset data.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Asset is empty.")) return true;

                Msg =
                    "Asset Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Asset Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine;
            }
            return false;
        }

        private MySqlCommand SaveAssetTransferRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAssetTransferRequestHdr(DocNo, DocDt, CancelInd, CCCodeFrom, CCCodeTo, SiteCodeFrom, SiteCodeTo, AssetCategoryCodeFrom, AssetCategoryCodeTo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @CCCodeFrom, @CCCodeTo, @LocCodeFrom, @LocCodeTo, @AssetCategoryCodeFrom, @AssetCategoryCodeTo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CCCodeFrom", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@CCCodeTo", Sm.GetLue(LueCCCode2));
            Sm.CmParam<String>(ref cm, "@LocCodeFrom", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@LocCodeTo", Sm.GetLue(LueSiteCode2));
            Sm.CmParam<String>(ref cm, "@AssetCategoryCodeFrom", Sm.GetLue(LueAssetCatCode));
            Sm.CmParam<String>(ref cm, "@AssetCategoryCodeTo", Sm.GetLue(LueAssetCatCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        

        private MySqlCommand SaveAssetTransferRequestDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAssetTransferRequestDtl(DocNo, DNo, AssetCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AssetCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd1, Row,2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            // string testcAssetCode = Sm.GetGrdStr(Grd1, Row, 2);
            return cm;
        }

        // private string 

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditAssetTransferRequestCancel();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Asset Cancelled", false) ||
                IsDataAlreadyReceived()||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndEditedAlready()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblAssetTransferRequestHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyReceived()
        {
            var CancelInd = ChkCancelInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select AssetTransferRequestDocNo From TblAssetTransfer Where AssetTransferRequestDocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already Received.");
                return true;
            }
            return false;
        }

        private void EditAssetTransferRequestCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblAssetTransferRequestHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowAssetTransferRequestHdr(DocNo);
                ShowAssetTransferRequestDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssetTransferRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, CancelInd, DocDt, CCCodeFrom, CCCodeTo, SiteCodeFrom, SiteCodeTo, AssetCategoryCodeFrom, AssetCategoryCodeTo, Remark From TblAssetTransferRequestHdr Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "CancelInd", "DocDt", "CCCodeFrom", "CCCodeTo", "Remark",
                        "SiteCodeFrom", "SiteCodeTo", "AssetCategoryCodeFrom", "AssetCategoryCodeTo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCCCode2, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueSiteCode2, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueAssetCatCode, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueAssetCatCode2, Sm.DrStr(dr, c[9]));
                    }, true
                );
        }

        private void ShowAssetTransferRequestDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.AssetCode, C.AssetName, C.DisplayName, B.Remark ");
            SQL.AppendLine("From TblAssetTransferRequestHdr A ");
            SQL.AppendLine("Inner Join TblAssetTransferRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblAsset C On B.AssetCode=C.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "AssetCode", "AssetName", "DisplayName", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }



        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAssetTransferUseLocation = Sm.GetParameter("IsAssetTransferUseLocation") == "Y";
            mIsSystemUseCostCenter = Sm.GetParameter("IsSystemUseCostCenter") == "Y";
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public static void SetLueAssetCatCode(ref LookUpEdit Lue, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCategoryCode As Col1, B.AssetCategoryName As Col2 ");
            SQL.AppendLine("From TblCostCenterDtl A  ");
            SQL.AppendLine("Inner Join TblAssetCategory B On A.AssetCategoryCode = B.AssetCategoryCode ");
            SQL.AppendLine("Where ActInd = 'Y' And A.CCCode = '" + CCCode + "' ");
            SQL.AppendLine("Order By B.AssetCategoryName ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(Sl.SetLueCCCode),  mIsFilterByCC ? "Y" : "N");
            //if (Sm.GetLue(LueCCCode).Length > 0)
            //{
            //    SetLueAssetCatCode(ref LueAssetCatCode, Sm.GetLue(LueCCCode));
            //}
        }

        private void LueCCCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode2, new Sm.RefreshLue1(Sl.SetLueCCCode));
            //if (Sm.GetLue(LueCCCode2).Length > 0)
            //{
            //    SetLueAssetCatCode(ref LueAssetCatCode2, Sm.GetLue(LueCCCode2));
            //}
        }


        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void LueSiteCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode2, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void LueAssetCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCatCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
        }

        private void LueAssetCatCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCatCode2, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
        }


        #endregion

    }
}
