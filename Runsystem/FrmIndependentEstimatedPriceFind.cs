﻿#region Update
/*
    04/11/2021 [BRI/PHT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIndependentEstimatedPriceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIndependentEstimatedPrice mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmIndependentEstimatedPriceFind(FrmIndependentEstimatedPrice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();

                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDept?"Y":"N");
                Sl.SetLueOption(ref LueProcurementType, "ProcurementType");
                if (!mFrmParent.mIsMRUseProcurementType)
                {
                    label4.Visible = false; LueProcurementType.Visible = false; ChkProcurementType.Visible = false;
                };
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mFrmParent.mIsMRUseProcurementType = Sm.GetParameterBoo("IsMRUseProcurementType");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select J.DocNo, J.DocDt, C.DeptName, J.CancelInd, G.BCName, ");
            SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("B.ItCode, B.Qty, D.PurchaseUomCode, ");
            SQL.AppendLine("D.ItName, E.SiteName, B.UsageDt, B.Remark As DRemark, A.Remark As HRemark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, D.ForeignName, D.ItCodeInternal, F.UserName, D.Specification, H.Qty As CancelledQty,  ");

            if (mFrmParent.mIsUseECatalog)
                SQL.AppendLine("A.RMInd, ");
            else
                SQL.AppendLine("'N' AS RMInd, ");


            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("I.OptDesc AS ProcurementType, ");
            else
                SQL.AppendLine("Null AS ProcurementType, ");

            SQL.AppendLine("J.MRDocNo  ");

            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And A.DocType = '1' ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D ");
            SQL.AppendLine("    On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblBudgetCategory G on A.BCCode=G.BCCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblUser F On A.PICCode=F.UserCode ");
            SQL.AppendLine("Left Join TblMRQtyCancel H ON H.MRDocNo=B.DocNo AND H.MRDNo=B.DNo AND H.CancelInd='N' ");
            if(mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("Left Join TblOption	I On I.OptCode = A.ProcurementType And I.OptCat = 'ProcurementType' ");
            SQL.AppendLine("Inner Join TblIndependentEstimatedPriceHdr J On A.DocNo = J.MRDocNo ");
            SQL.AppendLine("And A.WODocNo Is Null ");
            SQL.AppendLine("And B.CancelInd = 'N' ");
            SQL.AppendLine("And J.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsMRSPPJBEnabled)
            {
                if (mFrmParent.mIsMRSPPJB)
                    SQL.AppendLine("And A.SPPJBInd='Y' ");
                else
                    SQL.AppendLine("And A.SPPJBInd='N' ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Cancel",
                        "Status",
                        "Department",

                        //6-10
                        "Site",
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Item's Name",
                        "Local Code",

                        //11-15
                        "Foreign Name",
                        "Quantity",
                        "UoM",
                        "Usage"+Environment.NewLine+"Date",
                        "Item's Remark",
                        
                        
                        //16-20
                        "Document's Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 

                        //21-25
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time",
                        "PIC",
                        "Specification",
                        "Budget Category",

                        //26-29
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "Procurement Type",
                        "RUN Market",
                        "Material Request#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 100, 200, 
                        
                        //6-10
                        150, 80, 20, 250, 100, 
                        
                        //11-15
                        200, 100, 80, 100, 300, 

                        //16-20
                        300, 100, 100, 100, 100, 
                        
                        // 21-25
                        100, 100, 150, 300, 150,

                        //26-29
                        100, 200, 80, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 28 });
            Sm.GrdColButton(Grd1, new int[] { 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 26 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 27, 28, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 17, 18, 19, 20, 21, 22 }, false);
            Grd1.Cols[23].Move(6);
            Grd1.Cols[24].Move(16);
            Grd1.Cols[26].Move(14);
            Grd1.Cols[27].Move(5);
            Grd1.Cols[28].Move(4);
            Grd1.Cols[29].Move(3);
            Grd1.Cols[3].Move(2);
            if (!mFrmParent.mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 24, 26 });
            if (!mIsFilterBySite) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            if (!mFrmParent.mIsMRUseProcurementType) Sm.GrdColInvisible(Grd1, new int[] { 27 });
            if (!mFrmParent.mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 28 });
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                //if (ChkExcludedCancelledItem.Checked)
                //    Filter = " And A.Status In ('O', 'A') And A.CancelInd='N' And B.Status In ('O', 'A') And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "J.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName" });
                if (mFrmParent.mIsMRUseProcurementType)
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProcurementType), "A.ProcurementType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "DeptName", "SiteName", 

                            //6-10
                            "ItCode", "ItName", "ItCodeInternal", "ForeignName", "Qty", 
                            
                            //11-15
                            "PurchaseUomCode", "UsageDt", "DRemark", "HRemark", "CreateBy", 
                            
                            //16-20
                            "CreateDt", "LastUpBy", "LastUpDt", "UserName", "Specification",

                            //21-25
                            "BCName", "CancelledQty", "ProcurementType", "RMInd", "MRDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(
                Sl.SetLueDeptCode), 
                string.Empty, 
                mFrmParent.mIsFilterByDept ? "Y" : "N"
                );
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueProcurementType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcurementType, new Sm.RefreshLue2(
                Sl.SetLueOption),
                //string.Empty,
                mFrmParent.mIsMRUseProcurementType ? "Y" : "N"
            );
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProcurementType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Procurement Type");
        }

        #endregion

        #endregion
    }
}
