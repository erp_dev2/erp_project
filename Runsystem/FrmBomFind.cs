﻿#region Update
/*
    03/10/2017 [WED] tambah informasi Work Center
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBomFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBom mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBomFind(FrmBom FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueOption(ref LueDocType, "BomDocType");
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -720);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "BOM#", 
                        "Date",
                        "Name", 
                        "Active",
                        "Work Center",

                        //6-10
                        "Item Code",
                        "",
                        "Item Name",
                        "Type",
                        "Code",

                        //11-15
                        "",
                        "Description",
                        "Source",
                        "Quantity",
                        "UoM",
                        
                        //16-20
                        "Value",
                        "Dimensions",
                        "Created By",   
                        "Created Date", 
                        "Created Time", 
                        
                        //21-25
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time",
                        "Item's Code Internal",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 200, 60, 180, 
                        
                        //6-10
                        100, 20, 200, 80, 130, 

                        //11-15
                        20, 180, 130, 100, 80, 

                        //16-20
                        100, 300, 130, 130, 130, 

                        //21-25
                        130, 130, 130, 150, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 7, 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 9, 18, 19, 20, 21, 22, 23, 24 }, false);
            if (!mFrmParent.mIsBOMShowDimensions) Sm.GrdColInvisible(Grd1, new int[] { 17 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 25 }, false);
            Grd1.Cols[24].Move(8);
            Grd1.Cols[25].Move(19);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 18, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocName, A.DocDt, A.ActiveInd, H.DocName As WCDocName, B2.ItCode, C.ItName, B.DocType, B.DocCode, D.OptDesc, B.Qty, G.PlanningUomCode2, B.Value, ");
            SQL.AppendLine("Case B.DocType When '3' Then E.EmpName When '2' Then F.DocName When '1' Then G.ItName End As DocDesc, C.Specification, C.ItCodeInternal,  ");
            if (mFrmParent.mIsBOMShowDimensions)
            {
                SQL.AppendLine("Case When B.DocType='1' Then ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(G.Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(G.Length, 0.00), 2) Using utf8), ' ', IfNull(G.LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(G.Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Height : ', Trim(Concat(Convert(Format(IfNull(G.Height, 0.00), 2) Using utf8), ' ', IfNull(G.HeightUomCode, '')))) ");
                SQL.AppendLine("Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(G.Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Width : ', Trim(Concat(Convert(Format(IfNull(G.Width, 0.00), 2) Using utf8), ' ', IfNull(G.WidthUomCode, '')))) ");
                SQL.AppendLine("Else '' End ");
                SQL.AppendLine(")) Else Null End As Dimensions, ");
            }
            else
            {
                SQL.AppendLine("Null As Dimensions, ");
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblBOMHdr A ");
            SQL.AppendLine("Inner Join TblBOMDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBomDtl2 B2 On A.DocNo =B2.DocNo ");
            SQL.AppendLine("Left Join TblItem C On B2.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On B.DocType=D.OptCode And D.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblEmployee E On B.DocCode=E.EmpCode And '3'=B.DocType ");
            SQL.AppendLine("Left Join TblFormulaHdr F On B.DocCode=F.DocNo And '2'=B.DocType ");
            SQL.AppendLine("Left Join TblItem G On B.DocCode=G.ItCode And '1'=B.DocType ");
            SQL.AppendLine("Left Join TblWorkCenterHdr H On A.WorkCenterDocNo = H.DocNo ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                
                if (ChkActive.Checked == true)
                {
                    Filter = "Where A.ActiveInd = 'Y' ";
                }
                else
                {
                    Filter = "Where A.ActiveInd = 'N' ";
                }


                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "C.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "B.DocType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "DocName", "ActiveInd", "WCDocName", "ItCode", 
                            
                            //6-10
                            "ItName", "DocType", "DocCode",  "DocDesc", "OptDesc", 
                            
                            //11-15
                            "Qty", "PlanningUomCode2", "Value", "Dimensions", "CreateBy", 
                            
                            //16-20
                            "CreateDt", "LastUpBy", "LastUpDt", "ItCodeInternal", "Specification" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 9), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bill of material#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueOption), "BomDocType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document type");
        }

        #endregion
    }
}
