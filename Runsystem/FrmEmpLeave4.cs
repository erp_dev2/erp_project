﻿#region Update
/*
    09/01/2020 [HAR/IMS] Menu baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave4 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty;
        internal bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false;
        private bool
            mIsApprovalByDept = false,
            mIsApprovalBySite = false;
        internal FrmEmpLeave4Find FrmFind;

        #endregion

        #region Constructor

        public FrmEmpLeave4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Leave Special    ";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                GetValue();
                SetFormControl(mState.View);
                SetGrd();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            SetGrd1();
            SetGrd2();
        }

        private void SetGrd1()
        {
            Grd1.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] { "Leave Date" },
                    new int[] { 150 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 0 });
        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 200, 100, 100, 250 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueLeaveCode, 
                        DteStartDt, TmeStartTm, TmeEndTm, TxtDurationHour, 
                        ChkBreakInd, MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnDurationHrDefault.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteStartDt, MeeRemark, TmeStartTm, TmeEndTm }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, LueLeaveCode, 
                DteStartDt, TxtEmpCode, TxtEmpName, 
                TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, 
                DteResignDt, DteLeaveStartDt, TmeStartTm, TmeEndTm, 
                MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour }, 0);
            ChkCancelInd.Checked = false;
            ChkBreakInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 
                DteStartDt, TxtEmpCode, TxtEmpName, 
                TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, DteJoinDt, 
                DteResignDt, DteLeaveStartDt, TmeStartTm, TmeEndTm, 
                MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour }, 0);
            ChkBreakInd.Checked = false;
        }

        private void ClearData3()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, 
                TxtSiteCode, DteJoinDt, DteResignDt, DteLeaveStartDt, TmeStartTm, 
                TmeEndTm, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour }, 0);
            ChkBreakInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLeave4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueLeaveCode(ref LueLeaveCode);
                Sl.SetLueLeaveCode(ref LueLeaveCode, "LEAVE018");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeaveHour", "TblEmpLeaveHour");
           
            var cml = new List<MySqlCommand>();
            var l = new List<EmpLeaveDtl>();
           
            cml.Add(SaveEmpLeaveHour(DocNo, l.Count));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueLeaveCode, "Leave name") ||
                Sm.IsDteEmpty(DteStartDt, "Leave's started date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeStartTm, "Leave's start time")) ||
                (!TmeEndTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeEndTm, "Leave's end time")) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTxtEmpty(TxtDurationHour, "Duration (hour)", true)) ||
                IsLeaveDateAlreadyUsed() ||
                IsLeaveTimeNotValid() ||
                IsDocDtForResingeeInvalid() ||
                IsLeaveStartDtForResingeeInvalid() ||
                IsLeaveEndDtForResingeeInvalid();
                ;
        }

        private bool IsDocDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveStartDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@StartDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's Start Date :" + DteStartDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's start date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveEndDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@EndDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's Date :" + DteStartDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's edate should be earlier than resign date.");
                return true;
            }
            return false;
        }

          private bool IsLeaveDateAlreadyUsed()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8);

            if (StartDt.Length != 0)
            {
                string Dt = string.Empty;
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );

                if (IsLeaveDateAlreadyUsed2(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    ))
                    return true;

                 Dt1 = Dt1.AddDays(1);

                    if (IsLeaveDateAlreadyUsed2(
                            Dt1.Year.ToString() +
                            ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                            ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                        ))
                        return true;
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed2(string Dt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From (");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpLeaveHdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo And B.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    And A.EmpCode=@EmpCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpLeave2Hdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo And C.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine(") T Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Dt);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Date : " + Sm.Right(Dt, 2) + "/" + Dt.Substring(4, 2) + "/" + Sm.Left(Dt, 4) + Environment.NewLine +
                    "Invalid leave date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveTimeNotValid()
        {
            string
                StartTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeEndTm);

            if (StartTm.Length != 0 && EndTm.Length != 0)
            {
                if (decimal.Parse(StartTm) > decimal.Parse(EndTm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave time.");
                    return true;
                }
            }
            return false;
        }

        private bool IsLeavePaid()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblLeave Where PaidInd='Y' And LeaveCode=@LeaveCode ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);

            return Sm.IsDataExist(cm);
        }

        private MySqlCommand SaveEmpLeaveHour(string DocNo, decimal NumberOfLeaveDays)
        {
            decimal DurationDay = NumberOfLeaveDays;
           
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeaveHour ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, ");
            SQL.AppendLine("LeaveCode, EmpCode, StartDt, LeaveStartDt, ");
            SQL.AppendLine("DurationHour, StartTm, EndTm, BreakInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', ");
            SQL.AppendLine("@LeaveCode, @EmpCode, @StartDt, @LeaveStartDt, ");
            SQL.AppendLine("@DurationHour, @StartTm, @EndTm, @BreakInd, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval ");
            SQL.AppendLine("(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmpLeaveHour' ");
            if (mIsApprovalBySite)
                SQL.AppendLine("And SiteCode In (Select SiteCode From TblEmployee Where EmpCode=@EmpCode And SiteCode Is Not Null) ");
            if (mIsApprovalByDept)
                SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode And DeptCode Is Not Null) ");

            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeaveHour' And DAGCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("And (DAGCode Is Null Or ");
                SQL.AppendLine("(DAGCode Is Not Null ");
                SQL.AppendLine("And DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@EmpCode ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblEmpLeaveHour Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpLeaveHour' And DocNo=@DocNo ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteLeaveStartDt));
            Sm.CmParam<Decimal>(ref cm, "@DurationHour", Decimal.Parse(TxtDurationHour.Text));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<String>(ref cm, "@BreakInd", ChkBreakInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }      

        #endregion

        #region Edit data

        private void EditData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpLeave());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblEmpLeaveHour Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand CancelEmpLeave()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpLeaveHour Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Status In ('O', 'A'); ");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpLeaveHour(DocNo);
                ShowEmpLeaveDate(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpLeaveHour(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, CancelInd, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancel' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("A.LeaveCode, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, A.StartDt, A.LeaveStartDt, ");
            SQL.AppendLine("A.StartTm, A.EndTm, A.DurationHour, A.BreakInd, A.Remark ");
            SQL.AppendLine("From TblEmpLeaveHour A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "StatusDesc", "LeaveCode", 

                        //6-10
                        "EmpCode", "EmpName", "EmpCodeOld", "PosName",  "DeptName", 
                        
                        //11-15
                        "SiteName", "JoinDt", "ResignDt", "StartDt", "LeaveStartDt",  
                        
                        //16-20
                        "StartTm", "EndTm", "DurationHour", "BreakInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                        TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                        Sl.SetLueLeaveCode(ref LueLeaveCode, Sm.DrStr(dr, c[5]));
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[12]));
                        Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[13]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[14]));
                        Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[15]));
                        TxtDurationDay.EditValue = 1m;
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[16]));
                        Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[17]));
                        TxtDurationHour.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        ChkBreakInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                    }, true
                );
        }

        private void ShowEmpLeaveDate(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select StartDt From TblEmpLeaveHour Where DocNo=@DocNo Order By StartDt;",
                    new string[] { "StartDt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    { Sm.SetGrdValue("D", Grd, dr, c, Row, 0, 0); }, 
                    false, false, false, false
                );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpLeaveHour' ");
            SQL.AppendLine("And IfNull(Status, 'O') In ('A', 'C') ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]{ "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void GetValue()
        {
            mIsApprovalByDept = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeaveHour' And DeptCode Is Not Null Limit 1;");
            mIsApprovalBySite = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeaveHour' And SiteCode Is Not Null Limit 1;");
        }

        public void ComputeDurationDay()
        {
            TxtDurationDay.EditValue = 1;
        }

        private static decimal ComputeHoliday(string EmpCode, string Dt1, string Dt2)
        {
            decimal HolidayAmt = 0m;
            string Value = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Count(1) ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode And B.HolidayInd='Y' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);

            try
            {
                Value = Sm.GetValue(cm);
                if (Value.Length > 0) HolidayAmt = decimal.Parse(Value.ToString());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return HolidayAmt;
        }

        public void ComputeDurationHour()
        {
            TxtDurationHour.EditValue = 0;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 &&
                Sm.GetTme(TmeStartTm).ToString().Length > 0 &&
                Sm.GetTme(TmeEndTm).ToString().Length > 0)
            {
                string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                StartTm = Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetTme(TmeEndTm);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(StartTm.Substring(0, 2)),
                    Int32.Parse(StartTm.Substring(2, 2)),
                    0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(EndTm.Substring(0, 2)),
                    Int32.Parse(EndTm.Substring(2, 2)),
                    0
                    );

                if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                double TotalHours = (Dt2 - Dt1).TotalHours;

                if (ChkBreakInd.Checked) TotalHours -= 1;
                if (TotalHours < 0) TotalHours = 0;

                if (TotalHours - (int)(TotalHours) > 0.5)
                    TxtDurationHour.EditValue = (int)(TotalHours) + 1;
                else
                {
                    if (TotalHours - (int)(TotalHours) == 0)
                        TxtDurationHour.EditValue = TotalHours;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) > 0 &&
                            TotalHours - (int)(TotalHours) <= 0.5)
                            TxtDurationHour.EditValue = (int)(TotalHours) + 0.5;
                    }
                }


            }
        }

        private string GetBreakSchedule()
        {
            if (TxtEmpCode.Text.Length == 0 || Sm.GetDte(DteStartDt).Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(In2, Out2) As Break ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkschedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt=@Dt And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And In2 Is Not Null And Out2 Is Not Null ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));

            var b = Sm.GetValue(cm);
            if (b.Length == 0)
                return string.Empty;
            else
                return
                    " (" +
                    Sm.Left(b, 2) + ":" + b.Substring(2, 2) + " - " +
                    b.Substring(4, 2) + ":" + Sm.Right(b, 2) +
                    ")";
        }

        public void SetLueLeaveCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select LeaveCode As Col1, LeaveName As Col2 From TblLeave Where LeaveCode='LEAVE018' Order By LeaveName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLueOption(ref LookUpEdit Lue, string OptCat)
        {
            var cm = new MySqlCommand() { CommandText = "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat=@Param And OptCode='H' Order By OptDesc;" };
            if (OptCat.Length > 0) Sm.CmParam<String>(ref cm, "@Param", OptCat);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
    
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue1(SetLueLeaveCode));
                ClearData2();
            }
        }

        private void LueLeaveType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData2();

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd 
                }, true);
                BtnDurationHrDefault.Enabled = false;

                
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd 
                    }, false);
                    BtnDurationHrDefault.Enabled = true;
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0) ClearData3();
                ComputeDurationDay();
            }
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0) ClearData3();
                ComputeDurationDay();
            }
        }

        private void TmeStartTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TmeEndTm.EditValue = TmeStartTm.EditValue;
                ComputeDurationHour();
            }
        }

        private void TmeEndTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ComputeDurationHour();
        }

        private void TxtDurationHour_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNum(TxtDurationHour.Text, 0);
        }

        private void ChkBreakInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                try
                {
                    ChkBreakInd.Text = "Include Break Time";
                    if (ChkBreakInd.Checked) ChkBreakInd.Text += GetBreakSchedule();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueLeaveCode, "Leave")) return;
            if (Sm.IsDteEmpty(DteStartDt, "Start Leave")) return;
            
            Sm.FormShowDialog(new FrmEmpLeave4Dlg(this, Sm.GetLue(LueLeaveCode)));
        }

        private void BtnDurationHrDefault_Click(object sender, EventArgs e)
        {
            ComputeDurationHour();
        }

        #endregion

        #endregion

        #region Class

        private class EmpLeaveDtl
        {
            public string LeaveDt { get; set; }
            public bool HolidayInd { get; set; }
        }

        #endregion
    }
}
