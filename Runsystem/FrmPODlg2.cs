﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPODlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPO mFrmParent;
        internal string mVdCode = "";
        private string mSQL = "";

        #endregion

        #region Constructor

        public FrmPODlg2(FrmPO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e); this.Text = "List Of Vendor Deposit";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            LblVdName.Text = Sm.GetValue("Select VdName From TblVendor Where VdCode = '"+mVdCode+"' ");
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "No",

                        //1-5
                        "Voucher"+Environment.NewLine+"Number",
                        "Voucher"+Environment.NewLine+"Date",
                        "Amount",

                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        100, 100, 150
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DocDt, A.Amt ");
            SQL.AppendLine("From TblVendorDepositBalance A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.DocNo = B.DocNo And A.DocType = '1' ");
            SQL.AppendLine("Where A.VdCode = '"+mVdCode+"' ");
            SQL.AppendLine("And A.DocType = '1' And A.DocNo not In ");
            SQL.AppendLine("(Select VoucherDocNo From TblPOHdr Where VoucherDocNo is not null) ");

          
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "";

                var cm = new MySqlCommand();

               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[] 
                        { 
                        //0
                        "DocNo",  

                        //1-5
                        "DocDt", "Amt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtVoucherDocNo.EditValue= Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtDownPayment.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3), 0);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion
    }
}
