﻿#region update
/*
      09/09/2020 [ICA/SRN] New Application
 *    25/09/2020 [ICA/SRN] Bug data tertukar
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSiteEducation : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpSiteEducation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueDivisionCode(ref LueDivisionCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private void GetParameter()
        //{
           
        //}

        protected override void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.SiteName, C.DivisionName, D.PositionStatusName, E.OptDesc AS STATUS, F.DeptName, ");
            SQL.AppendLine("COUNT(G1.EmpCode) S2, COUNT(G2.EmpCode) S1, COUNT(G3.EmpCode) D3, COUNT(G4.EmpCode) SLTA, COUNT(G5.EmpCode) SLTP, COUNT(G6.EmpCode) SD, ");
            SQL.AppendLine("COUNT(G1.EmpCode)+COUNT(G2.EmpCode)+COUNT(G3.EmpCode)+COUNT(G4.EmpCode)+COUNT(G5.EmpCode)+COUNT(G6.EmpCode) AS Total ");
            SQL.AppendLine("FROM tblemployee A ");
            SQL.AppendLine("LEFT JOIN tblsite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("LEFT JOIN tbldivision C ON A.DivisionCode = C.DivisionCode ");
            SQL.AppendLine("LEFT JOIN tblpositionstatus D ON A.PositionStatusCode = D.PositionStatusCode ");
            SQL.AppendLine("LEFT JOIN tbloption E ON A.EmploymentStatus = E.OptCode AND E.OptCat = 'EmploymentStatus' ");
            SQL.AppendLine("LEFT JOIN tbldepartment F ON A.DeptCode = F.DeptCode ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G1 ON A.EmpCode = G1.EmpCode AND G1.HighestInd='Y' AND G1.`Level` = '2' ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G2 ON A.EmpCode = G2.EmpCode AND G2.HighestInd='Y' AND G2.`Level` = '3' ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G3 ON A.EmpCode = G3.EmpCode AND G3.HighestInd='Y' AND G3.`Level` = '4' ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G4 ON A.EmpCode = G4.EmpCode AND G4.HighestInd='Y' AND G4.`Level` = '5' ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G5 ON A.EmpCode = G5.EmpCode AND G5.HighestInd='Y' AND G5.`Level` = '6' ");
            SQL.AppendLine("LEFT JOIN tblemployeeeducation G6 ON A.EmpCode = G6.EmpCode AND G6.HighestInd='Y' AND G6.`Level` = '7' ");
            SQL.AppendLine("WHERE (A.ResignDt IS NULL OR (A.ResignDt IS NOT NULL AND A.ResignDt > Left(CurrentDateTime(), 8))) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Site",
                    "Divisi",
                    "Department",
                    "Position Status",
                    "Employment"+Environment.NewLine+"Status",
                    
                    //6-10
                    "S2",
                    "S1",
                    "D3",
                    "SLTA",
                    "SLTP",

                    //11
                    "SD",
                    "TOTAL"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 270, 270, 150, 100, 

                    //6-10
                    70, 70, 70, 70, 70,

                    //11-12
                    70, 70,
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "AND 0=0";

                var cm = new MySqlCommand();
                //Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDivisionCode), "A.DivisionCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " GROUP BY B.SiteName, C.DivisionName, D.PositionStatusName, E.OptDesc, F.DeptName ",
                        new string[]
                        {
                            //0
                            "SiteName",

                            //1-5
                            "DivisionName", "DeptName", "PositionStatusName", "status", "S2",   
                            
                            //6-10
                            "S1", "D3", "SLTA", "SLTP", "SD", "Total"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ForeColor = Color.Black;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.HideSubtotals(Grd1);
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
       
        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkDivisionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion
    }
}
