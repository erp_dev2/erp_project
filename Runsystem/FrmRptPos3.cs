﻿#region Update
/* 
    24/07/2017 [TKG] menambah Pos#
    04/07/2018 [TKG] Tambah site
    13/08/2018 [TKG] tambah informasi entity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Drawing.Printing;
using System.Drawing;

#endregion

namespace RunSystem
{
    public partial class FrmRptPos3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsSiteMandatory = false;
        private string 
            mSQL = string.Empty,
            mPosNo = string.Empty,
            mShiftNo = string.Empty,
            mBsDate = string.Empty,
            mCashier = string.Empty;
        private PrintDocument pdoc = null;
        private string PrinterName = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPos3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                string CurrentDate = Sm.ServerCurrentDateTime();
                if (CurrentDate.Length == 0)
                {
                    DteDocDt1.EditValue = null;
                    DteDocDt2.EditValue = null;
                }
                else
                {
                    DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                    DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                }
                TxtPosNo.EditValue = Gv.PosNo;
                ChkPosNo.Checked = Gv.PosNo.Length > 0;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Posno, A.BsDate, A.ShiftNo, A.UserCode, A.FloatAmount, ");
            SQL.AppendLine("IfNull(B.CashAmt, 0) As CashAmt, ");
            SQL.AppendLine("IfNull(C.NonCashAmt, 0) As NonCashAmt, E.SiteName, G.EntName ");
            SQL.AppendLine("From TblPosShiftLog A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PosNo, T1.BsDate, T1.ShiftNo, T1.UserCode, Sum(Case When T1.SlsType='S' Then 1 Else -1 End * T2.PayAmtNett) As CashAmt ");
	        SQL.AppendLine("    From TblPosTrnHdr T1 ");
	        SQL.AppendLine("    Inner Join TblPosTrnPay T2 On T1.PosNo=T2.PosNo And T1.BsDate=T2.BsDate And T1.TrnNo=T2.TrnNo "); 
	        SQL.AppendLine("    Inner Join TblPosPayByType T3 On T2.PayTpNo=T3.PayTpNo And T3.IsCash='Y' ");
            SQL.AppendLine("    Where T1.BSDate Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("    Group By T1.PosNo, T1.BsDate, T1.ShiftNo, T1.UserCode ");
            SQL.AppendLine(") B ");
	        SQL.AppendLine("    On A.PosNo=B.PosNo ");
            SQL.AppendLine("    And A.BsDate=B.BsDate ");
            SQL.AppendLine("    And A.ShiftNo=B.ShiftNo "); 
	        SQL.AppendLine("    And A.UserCode=B.UserCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PosNo, T1.BsDate, T1.ShiftNo, T1.UserCode, Sum(Case When T1.SlsType='S' Then 1 Else -1 End * T2.PayAmtNett) As NonCashAmt ");
	        SQL.AppendLine("    From TblPosTrnHdr T1 ");
	        SQL.AppendLine("    Inner Join TblPosTrnPay T2 On T1.PosNo=T2.PosNo And T1.BsDate=T2.BsDate And T1.TrnNo=T2.TrnNo "); 
	        SQL.AppendLine("    Inner Join TblPosPayByType T3 On T2.PayTpNo=T3.PayTpNo And T3.IsCash='N' ");
            SQL.AppendLine("    Where T1.BSDate Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.PosNo, T1.BsDate, T1.ShiftNo, T1.UserCode ");
            SQL.AppendLine(") C ");
            SQL.AppendLine("    On A.PosNo=C.PosNo ");
            SQL.AppendLine("    And A.BsDate=C.BsDate ");
	        SQL.AppendLine("    And A.ShiftNo=C.ShiftNo ");
            SQL.AppendLine("    And A.UserCode=C.UserCode ");
            SQL.AppendLine("Left Join TblPosNo D On A.PosNo=D.PosNo ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity G On F.EntCode=G.EntCode ");
            SQL.AppendLine("Where A.BSDate Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Pos#", 
                        "Business"+Environment.NewLine+"Date", 
                        "Shift",
                        "User",
                        "Float"+Environment.NewLine+"Amount",
                        
                        //6-10
                        "Sales"+Environment.NewLine+"(Cash)",
                        "Sales"+Environment.NewLine+"(Non Cash)",
                        "Total"+Environment.NewLine+"(Cash)",
                        "Total"+Environment.NewLine+"(Cash+Non Cash)",
                        "",

                        //11-13
                        "",
                        "Site",
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        50,
                    
                        //1-5
                        60, 100, 100, 130, 150, 
                        
                        //6-10
                        150, 150, 150, 180, 20, 
                        
                        //11-13
                        20, 180, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13 });
            if (mIsSiteMandatory)
            {
                Grd1.Cols[12].Move(2);
                Grd1.Cols[13].Move(3);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (Sm.IsDteEmpty(DteDocDt1, "Start date")) return;
                if (Sm.IsDteEmpty(DteDocDt1, "End date")) return;

                var cm = new MySqlCommand();
                var Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPosNo.Text, "A.PosNo", true);

                decimal FloatAmount = 0m, CashAmt = 0m, NonCashAmt = 0m;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.PosNo, A.BsDate Desc, A.ShiftNo;",
                        new string[]
                        {
                            "PosNo", 
                            "BsDate", "ShiftNo", "UserCode", "FloatAmount", "CashAmt", 
                            "NonCashAmt", "SiteName", "EntName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            FloatAmount = dr.GetDecimal(4);
                            CashAmt = dr.GetDecimal(5);
                            NonCashAmt = dr.GetDecimal(6);
                            Grd.Cells[Row, 5].Value = FloatAmount;
                            Grd.Cells[Row, 6].Value = CashAmt;
                            Grd.Cells[Row, 7].Value = NonCashAmt;
                            Grd.Cells[Row, 8].Value = FloatAmount+CashAmt;
                            Grd.Cells[Row, 9].Value = FloatAmount + CashAmt+NonCashAmt;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if ((e.ColIndex == 10 || e.ColIndex== 11) && 
                Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                mPosNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                mBsDate = Sm.Left(Sm.GetGrdDate(Grd1, e.RowIndex, 2), 8);
                mShiftNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                mCashier = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                if (e.ColIndex == 10) PrintShiftClosing();
                if (e.ColIndex == 11) PrintDayEndClosing();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void PrintShiftClosing()
        {
            try
            {
                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);


                PaperSize psize = new PaperSize("Custom", 100, 200);
                //ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                //pdoc.DefaultPageSettings.PaperSize.Height =320;
                pdoc.DefaultPageSettings.PaperSize.Height = 820;

                pdoc.DefaultPageSettings.PaperSize.Width = 520;

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintShiftClosingPage);

                pdoc.PrinterSettings.PrinterName = Sm.GetPosSetting("PrinterName");
                pdoc.Print();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        void pdoc_PrintShiftClosingPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 0;


            Decimal CashAmount = 0;
            Decimal FloatAmount = Sm.GetDecValue(Sm.GetPosSetting("FloatAmount"));
            Decimal CashIn = Sm.GetDecValue(Sm.GetValue("SELECT Sum(CIAmt) As NumValue FROM tblposcashin Where PosNo='" + mPosNo + "' And ShiftNo='" + mShiftNo + "' And BsDate='" + mBsDate + "' "));
            Decimal CashOut = Sm.GetDecValue(Sm.GetValue("SELECT Sum(COAmt) As NumValue FROM tblposcashout Where PosNo='" + mPosNo + "' And ShiftNo='" + mShiftNo + "' And BsDate='" + mBsDate + "' "));

            PrintLine(graphics, mBsDate, Sm.PadCenter(Gv.CompanyName, Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadCenter("Shift Closing Report", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.ServerCurrentDateTime().Substring(6, 2) + "-" + Sm.ServerCurrentDateTime().Substring(4, 2) + "-" + Sm.ServerCurrentDateTime().Substring(0, 4) + " " + Sm.ServerCurrentDateTime().Substring(8, 2) + ":" + Sm.ServerCurrentDateTime().Substring(10, 2), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Pos No : " + mPosNo, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Shift No : " + mShiftNo, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Cashier : " + mCashier, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Tender Summary", Gv.MaxCharLinePrint - 15) + Sm.PadRight("Count", 5) + Sm.PadRight("Amount", 10), startX, startY, 15, ref Offset);

            // read from database
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        " Select D.SlsType, Case When D.SlsType='R' then concat(C.PayTpNm, ' Return') else C.PayTpNm end As PayTpNm, C.IsCash, Count(B.PayTpNo) As TCount, " +
                        " Sum(Case when D.SlsType = 'R' then -1*B.PayAmtNett else B.PayAmtNett end) As TPayAmt from tblPosTrnHdr A " +
                        " Inner Join tblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo " +
                        " Inner Join tblpospaybytype C on B.PayTpNo=C.PayTpNo " +
                        " Inner join tblpostrnhdr D on A.TrnNo=D.TrnNo And A.BsDate=D.BsDate And A.PosNo=D.PosNo " +
                        " Where A.PosNo='" + mPosNo + "' And A.ShiftNo='" + mShiftNo + "' And A.BsDate='" + mBsDate + "' " +
                        " Group By D.SlsType, C.PayTpNm, C.IsCash ";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("PayTpNm"),
                        dr.GetOrdinal("IsCash"),
                        dr.GetOrdinal("TCount"),
                        dr.GetOrdinal("TPayAmt"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.DrStr(dr, c[1]) == "Y")
                                CashAmount += Sm.DrDec(dr, c[3]);
                            //PrintLine(graphics, mBsDate, Sm.PadLeft(Sm.DrStr(dr, c[0]), Gv.MaxCharLinePrint - 15) + Sm.PadRight(Sm.DrStr(dr, c[2]), 5) + Sm.PadRight(String.Format("{0:###,###,###,##0}", Sm.DrDec(dr, c[3])), 10), startX, startY, 15, ref Offset);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
            string TotalTransCount = Sm.GetValue("SELECT Count(*) As NumValue FROM tblPosTrnHdr Where PosNo='" + mPosNo + "' And ShiftNo='" + mShiftNo + "' And BsDate='" + mBsDate + "' ");
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Transaction Count", Gv.MaxCharLinePrint - 10) + Sm.PadRight(String.Format("{0:###,###,###,##0}", TotalTransCount), 10), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Float Amount", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Cash In", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashIn), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Cash Out", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashOut), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Total Cash", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount + CashAmount + CashIn - CashOut), 15), startX, startY, 15, ref Offset);

            Sm.StdMsg(mMsgType.Info, "Printing Shift Report is successfully done.");
        }

        private void PrintLine(Graphics graphics, string BusinessDate, string LineStr, int startX, int startY, int spaceY, ref int Offset)
        {
            //Sm.AddToAuditRollFile(BusinessDate, LineStr);
            graphics.DrawString(LineStr, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + spaceY;
        }

        private void PrintDayEndClosing()
        {
            try
            {
                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);

                PaperSize psize = new PaperSize("Custom", 100, 200);
                //ps.DefaultPageSettings.PaperSize = psize;

                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                //pdoc.DefaultPageSettings.PaperSize.Height =320;
                pdoc.DefaultPageSettings.PaperSize.Height = 820;

                pdoc.DefaultPageSettings.PaperSize.Width = 520;

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintDayEndClosingPage);

                pdoc.PrinterSettings.PrinterName = Sm.GetPosSetting("PrinterName");
                pdoc.Print();

                //this.BeginInvoke(new MethodInvoker(this.Close));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        void pdoc_PrintDayEndClosingPage(object sender, PrintPageEventArgs e)
        {

            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 0;


            Decimal CashAmount = 0;
            Decimal FloatAmount = Sm.GetDecValue(Sm.GetPosSetting("FloatAmount"));
            Decimal CashIn = Sm.GetDecValue(Sm.GetValue("SELECT Sum(CIAmt) As NumValue FROM tblposcashin Where PosNo='" + mPosNo + "' And BsDate='" + mBsDate + "' "));
            Decimal CashOut = Sm.GetDecValue(Sm.GetValue("SELECT Sum(COAmt) As NumValue FROM tblposcashout Where PosNo='" + mPosNo + "' And BsDate='" + mBsDate + "' "));

            PrintLine(graphics, mBsDate, Sm.PadCenter(Gv.CompanyName, Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadCenter("Day End Closing Report", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.ServerCurrentDateTime().Substring(6, 2) + "-" + Sm.ServerCurrentDateTime().Substring(4, 2) + "-" + Sm.ServerCurrentDateTime().Substring(0, 4) + " " + Sm.ServerCurrentDateTime().Substring(8, 2) + ":" + Sm.ServerCurrentDateTime().Substring(10, 2), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Pos No : " + mPosNo, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Business Date : " + mBsDate, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, "Cashier : " + mCashier, startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Tender Summary", Gv.MaxCharLinePrint - 15) + Sm.PadRight("Count", 5) + Sm.PadRight("Amount", 10), startX, startY, 15, ref Offset);


            // read from database
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        " Select D.SlsType, Case When D.SlsType='R' then concat(C.PayTpNm, ' Return') else C.PayTpNm end As PayTpNm, " +
                        " C.IsCash, Count(B.PayTpNo) As TCount, " +
                        " Sum(Case when D.SlsType = 'R' then -1*B.PayAmtNett else B.PayAmtNett end) As TPayAmt from tblPosTrnHdr A " +
                        " Inner Join tblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo " +
                        " Inner Join tblpospaybytype C on B.PayTpNo=C.PayTpNo " +
                        " Inner join tblpostrnhdr D on A.TrnNo=D.TrnNo And A.BsDate=D.BsDate And A.PosNo=D.PosNo " +
                        " Where A.PosNo='" + mPosNo + "' And A.BsDate='" + mBsDate + "' " +
                        " Group By D.SlsType, C.PayTpNm, C.IsCash ";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("PayTpNm"),
                        dr.GetOrdinal("IsCash"),
                        dr.GetOrdinal("TCount"),
                        dr.GetOrdinal("TPayAmt"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (Sm.DrStr(dr, c[1]) == "Y")
                                CashAmount += Sm.DrDec(dr, c[3]);
                            PrintLine(graphics, mBsDate, Sm.PadLeft(Sm.DrStr(dr, c[0]), Gv.MaxCharLinePrint - 15) + Sm.PadRight(Sm.DrStr(dr, c[2]), 5) + Sm.PadRight(String.Format("{0:###,###,###,##0}", Sm.DrDec(dr, c[3])), 10), startX, startY, 15, ref Offset);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print." + Exc.Message);
            }
            string TotalTransCount = Sm.GetValue("SELECT Count(*) As NumValue FROM tblPosTrnHdr Where PosNo='" + mPosNo + "' And BsDate='" + mBsDate + "' ");
            PrintLine(graphics, mBsDate, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Transaction Count", Gv.MaxCharLinePrint - 10) + Sm.PadRight(String.Format("{0:###,###,###,##0}", TotalTransCount), 10), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Float Amount", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Cash In", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashIn), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Cash Out", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", CashOut), 15), startX, startY, 15, ref Offset);
            PrintLine(graphics, mBsDate, Sm.PadLeft("Total Cash", Gv.MaxCharLinePrint - 15) + Sm.PadRight(String.Format("{0:###,###,###,##0}", FloatAmount + CashAmount + CashIn - CashOut), 15), startX, startY, 15, ref Offset);

            Sm.StdMsg(mMsgType.Info, "Printing Day End Report is successfully done.");

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Pos#");
        }


        #endregion

        #endregion
    }
}
