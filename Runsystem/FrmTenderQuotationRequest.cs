﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderQuotationRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mMRDocNo = string.Empty,
            mMRDNo = string.Empty,
            mQtDocNo = string.Empty,
            mQtDNo = string.Empty,
            mTenderDocNo = string.Empty,
            mDocNo = string.Empty;

        internal FrmTenderQuotationRequestFind FrmFind;

        #endregion

        #region Constructor

        public FrmTenderQuotationRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmTenderQuotationRequest");

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Methods

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtTenderDocNo, TxtQtDocNo, TxtItCode, TxtItName, MeeRemark, 
                        TxtQty, TxtEstCurCode, TxtEstPrice, TxtQtCurCode, TxtUPrice, MeeCancelReason
                    }, true);
                    BtnTenderDocNo.Enabled = false;
                    BtnQtDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    BtnTenderDocNo.Enabled = true;
                    BtnQtDocNo.Enabled = true;
                    BtnTenderDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            mMRDocNo = string.Empty;
            mMRDNo = string.Empty;
            mQtDocNo = string.Empty;
            mQtDNo = string.Empty;
            mTenderDocNo = string.Empty;
            mDocNo = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtTenderDocNo, TxtQtDocNo, TxtItCode, TxtItName, MeeRemark,
                TxtEstCurCode, TxtQtCurCode, MeeCancelReason
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtEstPrice, TxtUPrice }, 0);
            ChkCancelInd.Checked = false;
        }

        internal void ClearData2()
        {
            mMRDocNo = string.Empty;
            mMRDNo = string.Empty;
            mQtDocNo = string.Empty;
            mQtDNo = string.Empty;
            mTenderDocNo = string.Empty;
            mDocNo = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtTenderDocNo, TxtQtDocNo, TxtItCode, TxtItName, MeeRemark,
                TxtEstCurCode, TxtQtCurCode, MeeCancelReason
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtEstPrice, TxtUPrice }, 0);
            ChkCancelInd.Checked = false;
        }

        internal void ClearData3()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtQtCurCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtUPrice }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTenderQuotationRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string DocNo = Sm.GenerateDocNo(string.Concat(Sm.ServerCurrentDateTime(), "00"), "TenderQuotationRequest", "TblTenderQuotationRequest");

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveTenderQuotationRequest(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTenderDocNo, "Tender#", false) ||
                Sm.IsTxtEmpty(TxtQtDocNo, "Quotation#", false)
                ;
        }

        private MySqlCommand SaveTenderQuotationRequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTenderQuotationRequest(DocNo, DocDt, TenderDocNo, QtDocNo, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @TenderDocNo, @QtDocNo, 'N', @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.ServerCurrentDateTime(), "00"));
            Sm.CmParam<String>(ref cm, "@TenderDocNo", TxtTenderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@QtDocNo", TxtQtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTQR());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsTQRNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedToTenderQuotation()
                ;
        }

        private bool IsTQRNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblTenderQuotationRequest ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedToTenderQuotation()
        {
            return Sm.IsDataExist(
                "Select TQRDocNo From TblTenderQuotation Where TQRDocNo=@Param;",
                TxtDocNo.Text,
                "Data already processed into Tender Quotation.");
        }

        private MySqlCommand CancelTQR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTenderQuotationRequest Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTenderQuotationRequest(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTenderQuotationRequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.CancelReason, A.CancelInd, A.TenderDocNo, A.QtDocNo, D.ItCode, E.ItName, ");
            SQL.AppendLine("D.Qty, A.Remark, B.MaterialRequestDocNo, B.MaterialRequestDNo, F.DNo As QtDNo, ");
            SQL.AppendLine("D.CurCode As EstCurCode, D.EstPrice, F.CurCode As QtCurCode, F.UPrice ");
            SQL.AppendLine("From TblTenderQuotationRequest A ");
            SQL.AppendLine("Inner Join TblTender B On A.TenderDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo = D.DocNo And B.MaterialRequestDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.CurCode, T2.UPrice ");
            SQL.AppendLine("    From TblQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Where T1.TenderDocNo Is Not Null ");
            SQL.AppendLine(") F On A.QtDocNo = F.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "CancelReason", "CancelInd", "TenderDocNo", "QtDocNo", "ItCode", 
                        //6-10
                        "ItName", "Qty", "Remark", "MaterialRequestDocNo", "MaterialRequestDNo", 
                        //11-15
                        "QtDNo", "EstCurCode", "EstPrice", "QtCurCode", "UPrice"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[1]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtTenderDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtQtDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        mQtDocNo = Sm.DrStr(dr, c[4]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        mMRDocNo = Sm.DrStr(dr, c[9]);
                        mMRDNo = Sm.DrStr(dr, c[10]);
                        mQtDNo = Sm.DrStr(dr, c[11]);
                        TxtEstCurCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtEstPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        TxtQtCurCode.EditValue = Sm.DrStr(dr, c[14]);
                        TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                    }, true
                );
        }

        #endregion

        #endregion

        #region events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion
        
        #region Button Click

        private void BtnTenderDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmTenderQuotationRequestDlg(this));
            }
        }

        private void BtnQtDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtTenderDocNo, "Tender#", false))
                {
                    Sm.FormShowDialog(new FrmTenderQuotationRequestDlg2(this, TxtTenderDocNo.Text));
                }
            }
        }

        #endregion

        #endregion
    }
}
