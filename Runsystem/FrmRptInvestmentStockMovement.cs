﻿#region Update
/*
    08/04/2018 [TKG] tambah remark
    21/11/2018 [WED] tambah created by
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    30/04/2020 [IBL/IMS] Penambahan field Nama Project dan Nomer PO Customer berdasarkan parameter IsRptStockMovementShowProjectInfo
    28/12/2020 [VIN/SRN] mengganti label kolom, qty --> qty1, uom --> uom1
    11/03/2021 [TKG/IMS] menambah heat number
    23/07/2021 [TRI/IMS] menambah parameter RptStockMovementDocNoSource, default = 1, jika 2 maka  menggunakan nomor dokumen DO TO OTH WHS PROJECT
    29/07/2021 [ICA/IMS] menambah filter multi warehouse
    02/11/2021 [DITA/IMS] masih ada bug,  ketika menampilkan data receiving yg sudah ada di stock movement juga menampilkan data receiving dari do sehingga data kedobel
    17/11/2021 [TRI/IMS] bug ketika refresh data doctype : DO to warehouse (Transfer Request)
    23/11/2021 [TRI/IMS] BUG ketika RptStockMovementDocNoSource = 2 harusnya ambil docno dari DO to Other Warehouse for Project
    25/11/2021 [TKG/IMS] untuk data yg diambil dari DOWhs4 (type 26), menggunakan function position untuk mencek posisi ':' , sample : 'abcde:12345'
    18/04/2022 [IBL/PRODUCT] rombak reporting (Perubahan filter, Penambahan kolom, perhitungan untuk kolom Acc. Qty, Acc. Acquisition Cost, Moving Avg Prive, Moving Avg Cost )
    12/05/2022 [IBL/PRODUCT] Qty, Acquisition Cost tidak sama dengan yg di transaksi
    13/05/2022 [IBL/PRODUCT] Kolom Investment Code berasal dari Investment Code di menu Investment Item Equity
    17/05/2022 [IBL/PRODUCT] Bug: Kolom Acc. Quantity, Acc. Acquisition Cost, Moving Average Price, Moving Average Cost nilainya jadi 0.
    19/05/2022 [IBL/PRODUCT] Ketika pertama refresh otomatis grouping by type and Investment Code
    30/05/2022 [IBL/PRODUCT] Bug: Attempt to divided by zero, dan tidak ter group secara otomatis.
                             Method Compute Accumulation Stock diganti ke lokal (tidak ambil dari StdMtd lagi)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptInvestmentStockMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string
            mRptStockMovementDocNoSource = string.Empty,
            mEquityInvestmentCtCode = string.Empty;
        private int 
            mNumberOfInventoryUomCode = 1;
        private bool 
            mIsInventoryShowTotalQty = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovementShowProjectInfo = false,
            mIsStockMovementHeatNumberEnabled = false;
        
            
        #endregion

        #region Constructor

        public FrmRptInvestmentStockMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsInventoryShowTotalQty', 'IsItGrpCodeShow', 'IsShowForeignName', 'IsInvTrnShowItSpec', 'RptStockMovementDocNoSource', ");
            SQL.AppendLine("'EquityInvestmentCtCode' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsInventoryShowTotalQty": mIsInventoryShowTotalQty = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCode = ParValue == "N"; break;
                            case "IsInvTrnShowItSpec": mIsInvTrnShowItSpec = ParValue == "Y"; break;

                            //String
                            case "RptStockMovementDocNoSource": mRptStockMovementDocNoSource = ParValue; break;
                            case "EquityInvestmentCtCode": mEquityInvestmentCtCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T2.OptDesc As MenuDesc, T1.InvestmentCode As InvestmentEquityCode, T5.PortofolioName As InvestmentName, T3.BankAcNo, T3.BankAcNm, ");
            SQL.AppendLine("T5.Issuer, T6.InvestmentCtCode, T6.InvestmentCtName, T7.OptDesc As InvestmentType, IfNull(T1.Qty, 0.00) As Qty, ");
            SQL.AppendLine("T4.UomCode, IfNull(T8.BasePrice, 0.00) As BasePrice, IfNull(T1.Qty, 0.00) * IfNull(T8.BasePrice, 0.00) As InvestmentCost, ");
            SQL.AppendLine("IfNull(T8.UPrice, 0.00) As AcquisitionPrice, IfNull(T1.Qty, 0.00) * IfNull(T8.UPrice, 0.00) As AcquisitionCost, T1.CreateDt, T1.PortofolioId As InvestmentCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select A.InvestmentType, A.DocType, A.DocNo, A.DNo, A.DocDt, A.BankAcCode, A.Lot, A.Bin, ");
            SQL.AppendLine("	A.InvestmentCode, A.BatchNo, A.Source, Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3, A.CreateDt, B.PortofolioId ");
            SQL.AppendLine("	From TblInvestmentStockMovement A ");
            SQL.AppendLine("	Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode ");
            SQL.AppendLine("	Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("	Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("	And C.InvestmentCtCode = @EquityInvestmentCtCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("	Group By A.DocType, A.InvestmentType, A.DocNo, A.DNo, A.BankAcCode, A.Lot, A.BatchNo, A.Source ");
            SQL.AppendLine("	Having Sum(A.Qty)<>0.00 ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Inner Join TblOption T2 On T1.DocType = T2.OptCode And T2.OptCat = 'InvestmentTransType' ");
            SQL.AppendLine("Left Join TblBankAccount T3 On T1.BankAcCode = T3.BankAcCode ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity T4 On T1.InvestmentCode = T4.InvestmentEquityCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio T5 On T4.PortofolioId = T5.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentCategory T6 On T5.InvestmentCtCode = T6.InvestmentCtCode ");
            SQL.AppendLine("	And T6.InvestmentCtCode = @EquityInvestmentCtCode ");
            SQL.AppendLine("Inner Join TblOption T7 On T1.InvestmentType = T7.OptCode And T7.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice T8 On T1.Source = T8.Source And T1.InvestmentCode = T8.InvestmentCode ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5  
                        "Document#",
                        "Date",
                        "Menu Desc",
                        "Investment's "+Environment.NewLine+"Equity Code",
                        "Investment's" + Environment.NewLine + "Name",

                        //6-10
                        "Investment's" + Environment.NewLine + "Bank Account (RDN)#",
                        "Investment's" + Environment.NewLine + "Bank Account Name",
                        "Issuer",
                        "Category Code",
                        "Category",

                        //11-15
                        "Type",
                        "Quantity",
                        "UoM",
                        "Base Price",
                        "Investment Cost",

                        //16-20
                        "Acquisition Price",
                        "Acquisition Cost",
                        "Acc. Quantity",
                        "Acc. Acquisition Cost",
                        "Moving Average" + Environment.NewLine + "Price",

                        //21-23
                        "Moving Average" + Environment.NewLine + "Cost",
                        "Create Date",
                        "Investment's Code"

                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 180, 150, 250, 
                        
                        //6-10
                        180, 200, 250, 0, 130,
                        
                        //11-15
                        150, 100, 130, 130, 130,
  
                        //16-20
                        150, 150, 150, 150, 150,

                        //21-23
                        150, 0, 150
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 4, 9, 22 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20, 21 }, 0);
            Grd1.Cols[23].Move(4);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "C.PortofolioId", "C.PortofolioName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter) + " Order By T1.InvestmentCode, T7.OptDesc, T1.CreateDt; ",
                new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "MenuDesc", "InvestmentEquityCode", "InvestmentName", "BankAcNo",
                        //6-10
                        "BankAcNm", "Issuer", "InvestmentCtCode", "InvestmentCtName", "InvestmentType",
                        //11-15
                        "Qty", "UomCode", "BasePrice", "InvestmentCost", "AcquisitionPrice",
                        //16-18
                        "AcquisitionCost", "CreateDt", "InvestmentCode"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                        Grd.Cells[Row, 18].Value = 0m;
                        Grd.Cells[Row, 19].Value = 0m;
                        Grd.Cells[Row, 20].Value = 0m;
                        Grd.Cells[Row, 21].Value = 0m;
                    }, true, false, false, false
                );

                ComputeStockAccumulaton(Sm.GetDte(DteDocDt2), mEquityInvestmentCtCode, TxtItInvestmentCode.Text, ref Grd1, 22, 4, 9, 11, 18, 19, 20, 21);
                Grd1.GroupObject.Add(11);
                Grd1.GroupObject.Add(23);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 15, 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Old Code Comment By IBL
        /*
        private List<StockMovement> PrepareDataStockMovement()
        {
            string
                Filter = " ",
                DocTypeTemp = string.Empty, DocNoTemp = string.Empty, DNoTemp = string.Empty;
            decimal QtyTemp = 0m, Qty2Temp = 0m, Qty3Temp = 0m;
            bool IsCancelled = false;
            
            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "Tbl.InvestmentCode", "Tbl.InvestmentName" });
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CancelInd, T.DocType, ");
            SQL.AppendLine("T.DocNo, ");
            SQL.AppendLine("T.DNo, T.Qty, T.Qty2, T.Qty3, T.DocDt, ");
            SQL.AppendLine("T2.SekuritasName As WhsName, T.Lot, T.Bin, T.InvestmentCode, T.PropCode, T.BatchNo, T.Source, T.Remark, T.CreateBy, ");
            SQL.AppendLine("Null As ProjectName, Null As PONo ");
            SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From TblInvestmentStockMovement T ");
            SQL.AppendLine("Left Join TblInvestmentSekuritas T2 on T.WhsCode = T2.SekuritasCode ");
            SQL.AppendLine("Where T.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And Exists (Select 1 From TblInvestmentItem Tbl Where Tbl.InvestmentCode = T.InvestmentCode And Tbl.InvestmentCtCode = @EquityInvestmentCtCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By T.DocType, T.DocNo, T.DNo, T.CancelInd Desc;");

            var l = new List<StockMovement>();

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "CancelInd", 

                    //1-5
                    "DocType", "DocNo", "DNo", "Qty", "Qty2", 

                    //6-10
                    "Qty3", "DocDt", "WhsName", "Lot", "Bin", 

                    //11-15
                    "InvestmentCode", "PropCode", "BatchNo", "Source", "Remark",

                    //16-19
                    "CreateBy", "ProjectName", "PONo", "HeatNumber"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, 0) == "Y")
                   {
                       IsCancelled = true;
                       DocTypeTemp = Sm.DrStr(dr, 1);
                       DocNoTemp = Sm.DrStr(dr, 2);
                       DNoTemp = Sm.DrStr(dr, 3);
                   }
                   else
                   {
                       QtyTemp = Sm.DrDec(dr, 4);
                       Qty2Temp = Sm.DrDec(dr, 5);
                       Qty3Temp = Sm.DrDec(dr, 6);
                       if (
                               (
                                   !IsCancelled ||
                                   (
                                       IsCancelled &&
                                           !(
                                               Sm.CompareStr(DocTypeTemp, Sm.DrStr(dr, 1)) &&
                                               Sm.CompareStr(DocNoTemp, Sm.DrStr(dr, 2)) &&
                                               Sm.CompareStr(DNoTemp, Sm.DrStr(dr, 3))
                                           )
                                   )
                               )
                               &&
                               (QtyTemp != 0 || Qty2Temp != 0 || Qty3Temp != 0)
                           )
                       {
                           DocTypeTemp = Sm.DrStr(dr, 1);
                           DocNoTemp = Sm.DrStr(dr, 2);
                           DNoTemp = Sm.DrStr(dr, 3);
                           
                           l.Add(new StockMovement()
                           {
                                DocType = DocTypeTemp,
                                DocNo = DocNoTemp,
                                DNo = DNoTemp,
                                DocDt=Sm.DrStr(dr, 7),
                                //WhsCode=Sm.DrStr(dr, 8),
                                WhsName = Sm.DrStr(dr, 8),
                                Lot =Sm.DrStr(dr, 9),
                                Bin=Sm.DrStr(dr, 10),
                                InvestmentCode = Sm.DrStr(dr, 11),
                                PropCode=Sm.DrStr(dr, 12),
                                BatchNo=Sm.DrStr(dr, 13),
                                Source=Sm.DrStr(dr, 14),
                                Qty = QtyTemp,
                                Qty2 = Qty2Temp,
                                Qty3 = Qty3Temp,
                                Remark = Sm.DrStr(dr, 15),
                                CreatedBy = Sm.DrStr(dr, 16),
                                ProjectName = Sm.DrStr(dr, 17),
                                PONo = Sm.DrStr(dr, 18),
                                HeatNumber = Sm.DrStr(dr, 19)
                           });
                       }
                       DocTypeTemp = string.Empty;
                       DocNoTemp = string.Empty;
                       DNoTemp = string.Empty;
                       QtyTemp = 0m;
                       Qty2Temp = 0m;
                       Qty3Temp = 0m;
                       IsCancelled = false;
                   }
               }, false
            );
            return l;
        }

        private List<StockMovement2> PrepareDataStockMovement2()
        {
            string
                Filter = " ",
                DocTypeTemp = string.Empty, DocNoTemp = string.Empty, DNoTemp = string.Empty;
            decimal QtyTemp = 0m;
            bool IsCancelled = false;

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "X1.InvestmentEquityCode", "X2.PortofolioName" });

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.CancelInd, T1.DocType, T1.DocNo, T1.DNo, T1.Qty, T1.Source, T1.DocDt, T1.BankAcCode, T1.InvestmentCode, T2.UPrice ");
            SQL.AppendLine("From TblInvestmentStockMovement T1 ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice T2 On T1.Source = T2.Source ");
            SQL.AppendLine("	And T1.InvestmentCode = T2.InvestmentCode ");
            SQL.AppendLine("Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And Exists ( ");
            SQL.AppendLine("    Select 1 From TblInvestmentItemEquity X1 ");
            SQL.AppendLine("    Inner Join TblInvestmentPortofolio X2 On X1.PortofolioId = X2.PortofolioId ");
            SQL.AppendLine("    Where X1.InvestmentEquityCode = T1.InvestmentCode ");
            SQL.AppendLine("    And Find_In_Set(X2.InvestmentCtCode, @EquityInvestmentCtCode)");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By T1.DocType, T1.DocNo, T1.DNo, T1.CancelInd Desc; ");

            var l = new List<StockMovement2>();

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "CancelInd", 

                    //1-5
                    "DocType", "DocNo", "DNo", "Qty", "Source",

                    //6-9
                    "DocDt", "BankAcCode", "InvestmentCode", "UPrice"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, 0) == "Y")
                   {
                       IsCancelled = true;
                       DocTypeTemp = Sm.DrStr(dr, 1);
                       DocNoTemp = Sm.DrStr(dr, 2);
                       DNoTemp = Sm.DrStr(dr, 3);
                   }
                   else
                   {
                       QtyTemp = Sm.DrDec(dr, 4);
                       if (
                               (
                                   !IsCancelled ||
                                   (
                                        IsCancelled &&
                                        !(
                                            Sm.CompareStr(DocTypeTemp, Sm.DrStr(dr, 1)) &&
                                            Sm.CompareStr(DocNoTemp, Sm.DrStr(dr, 2)) &&
                                            Sm.CompareStr(DNoTemp, Sm.DrStr(dr, 3))
                                        )
                                   )
                               )
                               && (QtyTemp != 0)
                           )
                       {
                           DocTypeTemp = Sm.DrStr(dr, 1);
                           DocNoTemp = Sm.DrStr(dr, 2);
                           DNoTemp = Sm.DrStr(dr, 3);

                           l.Add(new StockMovement2()
                           {
                               DocType = DocTypeTemp,
                               DocNo = DocNoTemp,
                               DNo = DNoTemp,
                               Qty = QtyTemp,
                               Source = Sm.DrStr(dr, 5),
                               DocDt = Sm.DrStr(dr, 6),
                               BankAcCode = Sm.DrStr(dr, 7),
                               InvestmentCode = Sm.DrStr(dr, 8),
                               UPrice = Sm.DrDec(dr, 9),
                           });
                       }
                       DocTypeTemp = string.Empty;
                       DocNoTemp = string.Empty;
                       DNoTemp = string.Empty;
                       QtyTemp = 0m;
                       IsCancelled = false;
                   }
               }, false
            );
            return l;
        }

        private List<BankAccount> PrepareDataBankAccount()
        {
            var cm = new MySqlCommand();
            var l = new List<BankAccount>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select BankAcCode, BankAcNo, BankAcNm From TblBankAccount;",
                new string[] { "BankAcCode", "BankAcNm" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new BankAccount()
                        {
                            BankAcCode = Sm.DrStr(dr, 0),
                            BankAcNo = Sm.DrStr(dr, 1),
                            BankAcName = Sm.DrStr(dr, 2)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Option> PrepareDataOption()
        {
            var cm = new MySqlCommand();
            var l = new List<Option>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select OptCode, OptDesc From TblOption Where OptCat='InvestmentTransType';",
                new string[] { "OptCode", "OptDesc" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Option()
                        {
                            OptCode = Sm.DrStr(dr, 0),
                            OptDesc = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Option> PrepareDataOption2()
        {
            var cm = new MySqlCommand();
            var l = new List<Option>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select OptCode, OptDesc From TblOption Where OptCat='InvestmentType';",
                new string[] { "OptCode", "OptDesc" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Option()
                        {
                            OptCode = Sm.DrStr(dr, 0),
                            OptDesc = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Item2> PrepareDataItem()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<Item2>();
            string Filter = " ";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "A.InvestmentEquityCode", "B.PortofolioName" });

            SQL.AppendLine("Select A.InvestmentEquityCode InvestmentCode, B.PortofolioName As InvestmentName, A.ForeignName, A.UomCode, ");
            SQL.AppendLine("A.Specification, B.Issuer, C.InvestmentCtCode, C.InvestmentCtName ");
            SQL.AppendLine("From TblInvestmentItemEquity A ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio B On A.PortofolioId = B.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentCategory C On B.InvestmentCtCode = C.InvestmentCtCode ");
            SQL.AppendLine("Where Find_In_Set(B.InvestmentCtCode, @EquityInvestmentCtCode) ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    "InvestmentCode",
                    "InvestmentName", "ForeignName", "UomCode", "Specification", "Issuer",
                    "InvestmentCtCode", "InvestmentCtName"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Item2()
                        {
                            InvestmentCode = Sm.DrStr(dr, 0),
                            InvestmentName = Sm.DrStr(dr, 1),
                            ForeignName = Sm.DrStr(dr, 2),
                            UomCode = Sm.DrStr(dr, 3),
                            Specification = Sm.DrStr(dr, 4),
                            Issuer = Sm.DrStr(dr, 5),
                            InvestmentCtCode = Sm.DrStr(dr, 6),
                            InvestmentCtName = Sm.DrStr(dr, 7),
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Transaction> PrepareDataTransaction()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<Transaction>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "C.InvestmentEquityCode", "D.PortofolioName" });

            SQL.AppendLine("Select B.Source, B.InvestmentCode, B.InvestmentType, B.AcquisitionPrice ");
            SQL.AppendLine("From TblInvestmentStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblInvestmentStockInitialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity C On B.InvestmentCode = C.InvestmentEquityCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId "); //nanti ini dikasih unioin all sama transaksinya seta
            SQL.AppendLine("Where Find_In_Set(D.InvestmentCtCode, @EquityInvestmentCtCode)");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter,
                new string[]
                {
                    "Source",
                    "InvestmentCode", "InvestmentType", "AcquisitionPrice"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   var test = Sm.DrStr(dr, 0) + Sm.DrStr(dr, 1) + Sm.DrStr(dr, 2) + Sm.DrDec(dr, 3).ToString();
                   l.Add(
                        new Transaction()
                        {
                            Source = Sm.DrStr(dr, 0),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentType = Sm.DrStr(dr, 2),
                            AcquisitionPrice = Sm.DrDec(dr, 3),
                        }
                    );
               }, false
            );
            return l;
        }

        */
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SekuritasName As Col From TblInvestmentSekuritas T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.SekuritasName = @WhsName ");
            }
            SQL.AppendLine("Order By T.SekuritasName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select SekuritasCode As WhsCode ");
                SQL.AppendLine("    From TblInvestmentSekuritas ");
                SQL.AppendLine("    Where Find_In_Set(SekuritasName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        //DocDt = End Date Filter, InvestmentCode = Filter Investment (boleh string.empty)
        private void ComputeStockAccumulaton(
            string DocDt, string InvestmentCtCode, string InvestmentCode,
            ref iGrid Grd, int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost
            )
        {
            var lacc = PrepDataStockAccumulation(DocDt, InvestmentCtCode, InvestmentCode);
            ProcessAccumulationStock(ref Grd, ref lacc, ColCreateDt, ColInvCode, ColInvCtCode, ColInvType, ColAccQty, ColAccAcqCost, ColMovingAvgPrice, ColMovingAvgCost);
        }

        private List<StockAccumulation> PrepDataStockAccumulation(string DocDt, string InvestmentCtCode, string InvestmentCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockAccumulation>();
            var l2 = new List<StockAccumulation>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, InvestmentCode, new string[] { "C.PortofolioId", "C.PortofolioName" });

            SQL.AppendLine("Select A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType, E.OptDesc,  ");
            SQL.AppendLine("IfNull(A.Qty, 0.00) As Qty, IfNull(A.Qty, 0.00) * IfNull(D.UPrice, 0.00) AcquisitionCost  ");
            SQL.AppendLine("From TblInvestmentStockMovement A  ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode  ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source = D.Source ");
            SQL.AppendLine("	And A.InvestmentCode = D.InvestmentCode ");
            SQL.AppendLine("Inner Join TblOption E On A.InvestmentType = E.OptCode And E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            if (DocDt.Length > 0) SQL.AppendLine("And A.DocDt <= @DocDt  ");
            SQL.AppendLine("And C.InvestmentCtCode = @InvestmentCtCode ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("	Select Distinct(X1.DocNo) ");
            SQL.AppendLine("	From TblInvestmentStockMovement X1 ");
            SQL.AppendLine("	Where 1=1 ");
            if (DocDt.Length > 0) SQL.AppendLine("	And X1.DocDt <= @DocDt ");
            SQL.AppendLine("	Group By X1.DocType, X1.InvestmentType, X1.DocNo, X1.DNo, X1.DocDt, X1.BankAcCode, X1.Lot, X1.BatchNo, X1.Source   ");
            SQL.AppendLine("	Having Sum(X1.Qty)=0.00 ");
            SQL.AppendLine(") ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter + "Order By A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType;",
                new string[]
                {
                    "CreateDt",
                    "InvestmentCode", "InvestmentCtCode", "InvestmentType", "OptDesc", "Qty",
                    "AcquisitionCost",
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockAccumulation()
                        {
                            CreateDt = Convert.ToDecimal(Sm.DrStr(dr, 0)),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentCtCode = Sm.DrStr(dr, 2),
                            InvestmentType = Sm.DrStr(dr, 3),
                            InvestmentTypeName = Sm.DrStr(dr, 4),
                            Qty = Sm.DrDec(dr, 5),
                            AcquisitionCost = Sm.DrDec(dr, 6),
                        }
                    );
               }, false
            );

            var lg = l.GroupBy(g => new { CreateDt = g.CreateDt, InvCode = g.InvestmentCode, InvCtCode = g.InvestmentCtCode, InvType = g.InvestmentType, InvTypeNm = g.InvestmentTypeName })
                .Select(group => new
                {
                    CreateDt = group.Key.CreateDt,
                    InvestmentCode = group.Key.InvCode,
                    InvestmentCtCode = group.Key.InvCtCode,
                    InvestmentType = group.Key.InvType,
                    InvestmentTypeName = group.Key.InvTypeNm,
                });

            foreach (var x in lg)
            {
                decimal mQty = 0m, mAcquisitionPrice = 0m;
                foreach (var y in l)
                {
                    if (y.CreateDt <= x.CreateDt &&
                        y.InvestmentCode == x.InvestmentCode &&
                        y.InvestmentCtCode == x.InvestmentCtCode &&
                        y.InvestmentType == x.InvestmentType)
                    {
                        mQty += y.Qty;
                        mAcquisitionPrice += y.AcquisitionCost;
                    }
                }
                l2.Add(
                    new StockAccumulation()
                    {
                        CreateDt2 = x.CreateDt.ToString(),
                        InvestmentCode = x.InvestmentCode,
                        InvestmentCtCode = x.InvestmentCtCode,
                        InvestmentType = x.InvestmentType,
                        InvestmentTypeName = x.InvestmentTypeName,
                        Qty = mQty,
                        AcquisitionCost = mAcquisitionPrice,
                    }
                );
            }

            return l2;
        }

        private void ProcessAccumulationStock(
            ref iGrid Grd, ref List<StockAccumulation> lacc,
            int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost
            )
        {
            foreach (var x in lacc)
            {
                for (int row = 0; row < Grd.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(Grd, row, 0).Length > 0 &&
                        Sm.GetGrdStr(Grd, row, ColCreateDt) == x.CreateDt2 &&
                        Sm.GetGrdStr(Grd, row, ColInvCode) == x.InvestmentCode &&
                        Sm.GetGrdStr(Grd, row, ColInvCtCode) == x.InvestmentCtCode &&
                        Sm.GetGrdStr(Grd, row, ColInvType) == x.InvestmentTypeName)
                    {
                        Grd.Cells[row, ColAccQty].Value = x.Qty;
                        Grd.Cells[row, ColAccAcqCost].Value = x.AcquisitionCost;
                        Grd.Cells[row, ColMovingAvgPrice].Value = (x.Qty == 0 ? 0 : x.AcquisitionCost / x.Qty);
                        Grd.Cells[row, ColMovingAvgCost].Value = (x.Qty == 0 ? 0 : x.Qty * (x.AcquisitionCost / x.Qty));
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        
        #endregion

        #region Class

        private class StockMovement
        {
            public string DocType { get; set; }
            public string OptDesc { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentName { get; set; }
            public string ForeignName { get; set; }
            public string PropCode { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Remark { get; set; }
            public string CreatedBy { get; set; }
            public string Specification { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string HeatNumber { get; set; }
        }

        private class StockMovement2
        {
            public string DocType { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string MenuDesc { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentName { get; set; }
            public string InvestmentType { get; set; }
            public string BankAcCode { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcName { get; set; }
            public string Issuer { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentCtName { get; set; }
            public string UomCode { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public decimal BasePrice { get; set; }
            public decimal InvestmentCost { get; set; }
            public decimal AcquisitionPrice { get; set; }
            public decimal AcquisitionCost { get; set; }
            public decimal AccQty { get; set; }
            public decimal AccAcquisitionCost { get; set; }
            public decimal MovingAvgPrice { get; set; }
            public decimal MovingAvgCost { get; set; }
        }

        private class BankAccount
        {
            public string BankAcCode { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcName { get; set; }
        }

        private class Option
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
        }

        private class Option2
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
        }

        private class Item2
        {
            public string InvestmentCode { get; set; }
            public string InvestmentName { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentCtName { get; set; }
            public string Specification { get; set; }
            public string ForeignName { get; set; }
            public string UomCode { get; set; }
            public string Issuer { get; set; }
        }

        private class Item
        {
            public string InvestmentCode { get; set; }
            public string InvestmentName { get; set; }
            public string ForeignName { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Specification { get; set; }

        }

        private class Transaction
        {
            public string Source { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentType { get; set; }
            public decimal AcquisitionPrice { get; set; }
        }

        private class StockAccumulation
        {
            public decimal CreateDt { get; set; }
            public string CreateDt2 { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentType { get; set; }
            public string InvestmentTypeName { get; set; }
            public decimal Qty { get; set; }
            public decimal AcquisitionCost { get; set; }
        }

        private class Warehouse
        {
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
        }

        private class Property
        {
            public string PropCode { get; set; }
            public string PropName { get; set; }
        }

        #endregion
    }
}
