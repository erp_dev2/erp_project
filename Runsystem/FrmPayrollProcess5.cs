﻿#region Update
/*
    27/07/2018 [TKG] Payroll Processing KIM.
    31/07/2018 [TKG] bug fixing tax dan tax allowance
    03/09/2018 [TKG] Berdasarkan parameter IsPayrollAmtRounded, semua amount yang berhubungan dengan payroll akan dirounding atau tidak.
    30/09/2018 [TKG] bilangkan angka di belakang koma.
    26/10/2018 [TKG] salary dan allowance deduction diprorate.
    07/11/2018 [TKG] bug deduction diprorate.
    26/10/2020 [TKG/KIM] deduction jangan diprorate
    11/05/2022 [TKG/KIM] merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcess5 : Form
    {
        #region Field, Property

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,

            mNeglectLeaveCode = "002",
            mOTRequestType = "1",
            mCoordinatorLeaveCode = "007",
            mDisasterLeaveCode = "014",
            mGoHomeEarlyLeaveCode = "017",
            mMaternityLeaveCode = "015",
            mMiscarriageLeaveCode = "016",
            mOperatorLeaveCode = "001",
            mEmpSystemTypeHarian = "1",
            mEmpSystemTypeBorongan = "2",
            mEmpSystemTypeAllIn = "3",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mPayrunPeriod2Mingguan = "2",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "1",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mWSCodesHolidayWithExtraFooding = string.Empty,
            mSSCodePension = string.Empty,
            mSSCodePension2 = string.Empty,
            mADCodeOTHoliday = string.Empty,
            mADCodeOTNonHoliday = string.Empty;

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 8m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mXMinToleranceLateMealTransportDeduction = 0m,
            mXPercentageLateMealTransportDeduction = 0m,
            mADOTHour = 6m,
            mADOTMinHour = 1m;

        private bool
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsUseLateMealTransportDeduction = false,
            mIsPPProcessAllOutstandingSS = false,
            mIsPayrollAmtRounded = false;

        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<EmployeeAllowance> mlEmployeeAllowanceMeal = null;
        private List<EmployeeAllowance> mlEmployeeAllowanceTransport = null;
        private List<EmployeeAllowanceDeduction> mlEmployeeAllowanceDeduction = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;
        
        #endregion

        #region Constructor

        public FrmPayrollProcess5(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            //mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            //mOTRequestType = Sm.GetParameter("OTRequestType");
            //mWSCodesHolidayWithExtraFooding = Sm.GetParameter("WSCodesHolidayWithExtraFooding");
            //mWSCodesForNationalHoliday = Sm.GetParameter("WSCodesForNationalHoliday");
            //mWSCodesForHoliday = Sm.GetParameter("WSCodesForHoliday");

            //mSalaryInd = Sm.GetParameter("SalaryInd");
            //mCoordinatorLeaveCode = Sm.GetParameter("CoordinatorLeaveCode");
            //mDisasterLeaveCode = Sm.GetParameter("DisasterLeaveCode");
            //mGoHomeEarlyLeaveCode = Sm.GetParameter("GoHomeEarlyLeaveCode");
            //mMaternityLeaveCode = Sm.GetParameter("MaternityLeaveCode");

            //mMiscarriageLeaveCode = Sm.GetParameter("MiscarriageLeaveCode");
            //mOperatorLeaveCode = Sm.GetParameter("OperatorLeaveCode");
            //mEmpSystemTypeHarian = Sm.GetParameter("EmpSystemTypeHarian");
            //mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            //mEmpSystemTypeAllIn = Sm.GetParameter("EmpSystemTypeAllIn");

            //mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            //mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            //mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            //mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            //mPayrunPeriod2Mingguan = Sm.GetParameter("PayrunPeriod2Mingguan");

            //mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            //mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            //mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            //mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            //mSSForRetiring = Sm.GetParameter("SSForRetiring");

            //mSSOldAgeInsurance = Sm.GetParameter("SSOldAgeInsurance");
            //mSSCodePension = Sm.GetParameter("SSCodePension");
            //mSSCodePension2 = Sm.GetParameter("SSCodePension2");
            //mXMinToleranceLateMealTransportDeduction = Sm.GetParameterDec("XMinToleranceLateMealTransportDeduction");
            //mXPercentageLateMealTransportDeduction = Sm.GetParameterDec("XPercentageLateMealTransportDeduction");

            //mADOTHour = Sm.GetParameterDec("ADOTHour");
            //mADOTMinHour = Sm.GetParameterDec("ADOTMinHour");
            //mHoliday1stHrForHO = Sm.GetParameterDec("Holiday1stHrForHO");
            //mHoliday1stHrForSite = Sm.GetParameterDec("Holiday1stHrForSite");
            //mHoliday2ndHrForHO = Sm.GetParameterDec("Holiday2ndHrForHO");

            //mHoliday2ndHrForSite = Sm.GetParameterDec("Holiday2ndHrForSite");
            //mHoliday1stHrIndex = Sm.GetParameterDec("Holiday1stHrIndex");
            //mHoliday2ndHrIndex = Sm.GetParameterDec("Holiday2ndHrIndex");
            //mHoliday3rdHrIndex = Sm.GetParameterDec("Holiday3rdHrIndex");
            //mNonNPWPTaxPercentage = Sm.GetParameterDec("NonNPWPTaxPercentage");

            //mADCodeEmploymentPeriodYr = Sm.GetParameterDec("ADCodeEmploymentPeriodYr");
            //mFieldAssignmentMinWorkDuration = Sm.GetParameterDec("FieldAssignmentMinWorkDuration");
            //mDirectLaborDailyWorkHour = Sm.GetParameterDec("DirectLaborDailyWorkHour");
            //mWorkingHr = Sm.GetParameterDec("WorkingHr");
            //mNoWorkDay2Mingguan = Sm.GetParameterDec("NoWorkDay2Mingguan");

            //mNoWorkDayBulanan = Sm.GetParameterDec("NoWorkDayBulanan");
            //mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            //mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            //mHOWorkingDayPerMth = Sm.GetParameterDec("HOWorkingDayPerMth");
            //mSiteWorkingDayPerMth = Sm.GetParameterDec("SiteWorkingDayPerMth");

            //mIsUseLateMealTransportDeduction = Sm.GetParameterBoo("IsUseLateMealTransportDeduction");
            //mIsPPProcessAllOutstandingSS = Sm.GetParameterBoo("IsPPProcessAllOutstandingSS");
            //mIsOTHolidayDeduct1Hr = Sm.GetParameterBoo("IsOTHolidayDeduct1Hr");
            //mIsPayrollAmtRounded = Sm.GetParameterBoo("IsPayrollAmtRounded");
            //mIsWorkingHrBasedOnSchedule = Sm.GetParameterBoo("IsWorkingHrBasedOnSchedule");

            //mIsUseLateValidation = Sm.GetParameterBoo("IsUseLateValidation");
            //mIsUseOTReplaceLeave = Sm.GetParameterBoo("IsUseOTReplaceLeave");
            //mIsOTRoutineRounding = Sm.GetParameterBoo("IsOTRoutineRounding");
            //mIsOTBasedOnOTRequest = Sm.GetParameterBoo("IsOTBasedOnOTRequest");
            //mIsPayrollProcessUseSiteWorkingDayPerMth = Sm.GetParameterBoo("IsPayrollProcessUseSiteWorkingDayPerMth");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'Holiday2ndHrIndex', 'Holiday3rdHrIndex', 'NonNPWPTaxPercentage', 'ADCodeEmploymentPeriodYr', 'FieldAssignmentMinWorkDuration', ");
            SQL.AppendLine("'Holiday1stHrForHO', 'Holiday1stHrForSite', 'Holiday2ndHrForHO', 'Holiday2ndHrForSite', 'Holiday1stHrIndex', ");
            SQL.AppendLine("'NoWorkDayBulanan', 'FunctionalExpenses', 'FunctionalExpensesMaxAmt', 'HOWorkingDayPerMth', 'SiteWorkingDayPerMth', ");
            SQL.AppendLine("'IsPayrollAmtRounded', 'IsUseLateMealTransportDeduction', 'IsPPProcessAllOutstandingSS', 'IsOTHolidayDeduct1Hr', 'IsWorkingHrBasedOnSchedule', ");
            SQL.AppendLine("'IsOTBasedOnOTRequest', 'IsOTRoutineRounding', 'IsUseOTReplaceLeave', 'IsUseLateValidation', 'IsPayrollProcessUseSiteWorkingDayPerMth', ");
            SQL.AppendLine("'SSCodePension2', 'SSCodePension', 'NoWorkDay2Mingguan', 'WorkingHr', 'SSOldAgeInsurance', ");
            SQL.AppendLine("'ADCodeTransport', 'ADCodeMeal', 'SSPCodeForHealth', 'SSPCodeForEmployment', 'SSForRetiring', ");
            SQL.AppendLine("'PayrunPeriod2Mingguan', 'PayrunPeriodBulanan', 'EmploymentStatusSpesial', 'EmploymentStatusLepas', 'EmploymentStatusTetap', ");
            SQL.AppendLine("'MiscarriageLeaveCode', 'OperatorLeaveCode', 'EmpSystemTypeHarian', 'EmpSystemTypeBorongan', 'EmpSystemTypeAllIn', ");
            SQL.AppendLine("'SalaryInd', 'CoordinatorLeaveCode', 'DisasterLeaveCode', 'GoHomeEarlyLeaveCode', 'MaternityLeaveCode', ");
            SQL.AppendLine("'NeglectLeaveCode', 'OTRequestType', 'WSCodesHolidayWithExtraFooding', 'WSCodesForNationalHoliday', 'WSCodesForHoliday', ");
            SQL.AppendLine("'DirectLaborDailyWorkHour', 'ADOTMinHour', 'ADOTHour', 'XPercentageLateMealTransportDeduction', 'XMinToleranceLateMealTransportDeduction' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //decimal
                            case "WorkingHr": if (ParValue.Length > 0) mWorkingHr = decimal.Parse(ParValue); break;
                            case "NoWorkDay2Mingguan": if (ParValue.Length > 0) mNoWorkDay2Mingguan = decimal.Parse(ParValue); break;
                            case "DirectLaborDailyWorkHour": if (ParValue.Length > 0) mDirectLaborDailyWorkHour = decimal.Parse(ParValue); break;
                            case "SiteWorkingDayPerMth": if (ParValue.Length > 0) mSiteWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "HOWorkingDayPerMth": if (ParValue.Length > 0) mHOWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "FunctionalExpensesMaxAmt": if (ParValue.Length > 0) mFunctionalExpensesMaxAmt = decimal.Parse(ParValue); break;
                            case "FunctionalExpenses": if (ParValue.Length > 0) mFunctionalExpenses = decimal.Parse(ParValue); break;
                            case "NoWorkDayBulanan": if (ParValue.Length > 0) mNoWorkDayBulanan = decimal.Parse(ParValue); break;
                            case "Holiday1stHrIndex": if (ParValue.Length > 0) mHoliday1stHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrForSite": if (ParValue.Length > 0) mHoliday2ndHrForSite = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrForHO": if (ParValue.Length > 0) mHoliday2ndHrForHO = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForSite": if (ParValue.Length > 0) mHoliday1stHrForSite = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForHO": if (ParValue.Length > 0) mHoliday1stHrForHO = decimal.Parse(ParValue); break;
                            case "FieldAssignmentMinWorkDuration": if (ParValue.Length > 0) mFieldAssignmentMinWorkDuration = decimal.Parse(ParValue); break;
                            case "ADCodeEmploymentPeriodYr": if (ParValue.Length > 0) mADCodeEmploymentPeriodYr = decimal.Parse(ParValue); break;
                            case "NonNPWPTaxPercentage": if (ParValue.Length > 0) mNonNPWPTaxPercentage = decimal.Parse(ParValue); break;
                            case "Holiday3rdHrIndex": if (ParValue.Length > 0) mHoliday3rdHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrIndex": if (ParValue.Length > 0) mHoliday2ndHrIndex = decimal.Parse(ParValue); break;
                            case "XMinToleranceLateMealTransportDeduction": if (ParValue.Length > 0) mXMinToleranceLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "XPercentageLateMealTransportDeduction": if (ParValue.Length > 0) mXPercentageLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "ADOTHour": if (ParValue.Length > 0) mADOTHour = decimal.Parse(ParValue); break;
                            case "ADOTMinHour": if (ParValue.Length > 0) mADOTMinHour = decimal.Parse(ParValue); break;
                            
                            //string
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "CoordinatorLeaveCode": mCoordinatorLeaveCode = ParValue; break;
                            case "DisasterLeaveCode": mDisasterLeaveCode = ParValue; break;
                            case "GoHomeEarlyLeaveCode": mGoHomeEarlyLeaveCode = ParValue; break;
                            case "MaternityLeaveCode": mMaternityLeaveCode = ParValue; break;
                            case "NeglectLeaveCode": mNeglectLeaveCode = ParValue; break;
                            case "OTRequestType": mOTRequestType = ParValue; break;
                            case "WSCodesForHoliday": mWSCodesForHoliday = ParValue; break;
                            case "WSCodesForNationalHoliday": mWSCodesForNationalHoliday = ParValue; break;
                            case "WSCodesHolidayWithExtraFooding": mWSCodesHolidayWithExtraFooding = ParValue; break;
                            case "MiscarriageLeaveCode": mMiscarriageLeaveCode = ParValue; break;
                            case "OperatorLeaveCode": mOperatorLeaveCode = ParValue; break;
                            case "EmpSystemTypeHarian": mEmpSystemTypeHarian = ParValue; break;
                            case "EmpSystemTypeBorongan": mEmpSystemTypeBorongan = ParValue; break;
                            case "EmpSystemTypeAllIn": mEmpSystemTypeAllIn = ParValue; break;
                            case "PayrunPeriod2Mingguan": mPayrunPeriod2Mingguan = ParValue; break;
                            case "PayrunPeriodBulanan": mPayrunPeriodBulanan = ParValue; break;
                            case "EmploymentStatusSpesial": mEmploymentStatusSpesial = ParValue; break;
                            case "EmploymentStatusLepas": mEmploymentStatusLepas = ParValue; break;
                            case "EmploymentStatusTetap": mEmploymentStatusTetap = ParValue; break;
                            case "SSCodePension2": mSSCodePension2 = ParValue; break;
                            case "SSCodePension": mSSCodePension = ParValue; break;
                            case "SSOldAgeInsurance": mSSOldAgeInsurance = ParValue; break;
                            case "ADCodeTransport": mADCodeTransport = ParValue; break;
                            case "ADCodeMeal": mADCodeMeal = ParValue; break;
                            case "SSPCodeForHealth": mSSPCodeForHealth = ParValue; break;
                            case "SSPCodeForEmployment": mSSPCodeForEmployment = ParValue; break;
                            case "SSForRetiring": mSSForRetiring = ParValue; break;

                            //boolean
                            case "IsPayrollAmtRounded": mIsPayrollAmtRounded = ParValue == "Y"; break;
                            case "IsUseLateMealTransportDeduction": mIsUseLateMealTransportDeduction = ParValue == "Y"; break;
                            case "IsPPProcessAllOutstandingSS": mIsPPProcessAllOutstandingSS = ParValue == "Y"; break;
                            case "IsOTHolidayDeduct1Hr": mIsOTHolidayDeduct1Hr = ParValue == "Y"; break;
                            case "IsWorkingHrBasedOnSchedule": mIsWorkingHrBasedOnSchedule = ParValue == "Y"; break;
                            case "IsOTBasedOnOTRequest": mIsOTBasedOnOTRequest = ParValue == "Y"; break;
                            case "IsOTRoutineRounding": mIsOTRoutineRounding = ParValue == "Y"; break;
                            case "IsUseOTReplaceLeave": mIsUseOTReplaceLeave = ParValue == "Y"; break;
                            case "IsUseLateValidation": mIsUseLateValidation = ParValue == "Y"; break;
                            case "IsPayrollProcessUseSiteWorkingDayPerMth": mIsPayrollProcessUseSiteWorkingDayPerMth = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-10
                        "Join Date",
                        "Resign Date",
                        "Employment" + Environment.NewLine + "Status",
                        "Type",
                        "Grade Level",
                        
                        //11-13
                        "Period",
                        "Group",
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 180, 200, 
                        
                        //6-10
                        100, 100, 130, 130, 150,

                        //11-13
                        130, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mSystemType = string.Empty; 
            mPayrunPeriod = string.Empty;
            mPGCode = string.Empty;
            mSiteCode = string.Empty;
            mHOInd = true;

            if (mlEmployeeAllowanceDeduction.Count>0)
                mlEmployeeAllowanceDeduction.Clear();

            if (mlEmployeeAllowanceMeal.Count > 0)
                mlEmployeeAllowanceMeal.Clear();

            if (mlEmployeeAllowanceTransport.Count > 0)
                mlEmployeeAllowanceTransport.Clear();

            if (mlAdvancePaymentProcess.Count > 0)
                mlAdvancePaymentProcess.Clear();

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDeptCode, TxtSystemType, TxtPayrunPeriod, TxtPGCode, TxtSiteCode,
                DteStartDt, DteEndDt 
            });
            ChkDelData.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
            SQL.AppendLine("From TblPayrun ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And IfNull(Status, 'O')='O' ");
            SQL.AppendLine("Order By Concat(PayrunCode, ' : ', PayrunName);");
            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            if (Sm.IsLueEmpty(LuePayrunCode, "Payrun")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo1();
                ShowPayrunInfo2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPayrunInfo1()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, C.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, D.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, E.PGName, A.SiteCode, F.SiteName, F.HOInd, ");
            SQL.AppendLine("A.StartDt, A.EndDt ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.SystemType=C.OptCode And C.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption D On A.PayrunPeriod=D.OptCode And D.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr E On A.PGCode=E.PGCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode;");

            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DeptCode", 
                        
                        //1-5
                        "DeptName", 
                        "SystemType", 
                        "SystemTypeDesc", 
                        "PayrunPeriod", 
                        "PayrunPeriodDesc", 
                        
                        //6-10
                        "PGCode", 
                        "PGName",
                        "SiteCode",
                        "SiteName",
                        "HOInd",
                        
                        //11-12
                        "StartDt", 
                        "EndDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        mDeptCode = Sm.DrStr(dr, c[0]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[1]);
                        mSystemType = Sm.DrStr(dr, c[2]);
                        TxtSystemType.EditValue = Sm.DrStr(dr, c[3]);
                        mPayrunPeriod = Sm.DrStr(dr, c[4]);
                        TxtPayrunPeriod.EditValue = Sm.DrStr(dr, c[5]);
                        mPGCode = Sm.DrStr(dr, c[6]);
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[7]);
                        mSiteCode = Sm.DrStr(dr, c[8]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[9]);
                        mHOInd = Sm.DrStr(dr, c[10]) == "Y";
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[11]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[12]));
                    }, true
                );
        }

        private void ShowPayrunInfo2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            if (ChkEmploymentStatus.Checked)
                SQL.AppendLine("    And T2.EmploymentStatus=@EmploymentStatus ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select EmpCode ");
            SQL.AppendLine("            From TblEmployee ");
            SQL.AppendLine("            Where EmpCode=X.EmpCode ");
            SQL.AppendLine("            And JoinDt<=@EndDt ");
            SQL.AppendLine("            And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@StartDt)) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            if (ChkEmploymentStatus.Checked) Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt",  
                    
                    //6-10
                    "ResignDt", "EmploymentStatusDesc", "SystemTypeDesc", "GrdLvlName", "PayrunPeriodDesc", 
                    
                    //11-12
                    "PGName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePayrunCode, "Payrun") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPayrunNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of employees is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPayrunNotValid()
        {
            return
                Sm.IsDataExist(
                    "Select PayrunCode From TblPayrun Where PayrunCode=@Param And IfNull(Status, 'O')='C';",
                    Sm.GetLue(LuePayrunCode),
                    "Payrun Code : " + Sm.GetLue(LuePayrunCode) + Environment.NewLine +
                    "Payrun Name : " + LuePayrunCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "This payrun already closed." + Environment.NewLine +
                    "You need to cancel it's voucher request."
                );
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var cm = new MySqlCommand();

            if (!ChkDelData.Checked)
            {
                if (Grd1.Rows.Count >= 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                        if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                            No += 1;
                        }
                    }
                }
                Filter = " And (" + Filter + ") ";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcessAD Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcessADOT Where PayrunCode=@PayrunCode " + Filter + ";");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode " + Filter + ";") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode " + Filter + ";");
            
            if (ChkDelData.Checked)
            {
                SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
                SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

                SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
                SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select A.DocNo ");
                SQL.AppendLine("    From TblEmpInsPntHdr A ");
                SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
                SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
                SQL.AppendLine("    Where A.CancelInd='N' ");
                SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine(");");

                SQL.AppendLine("Update TblEmpSCIDtl A ");
                SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine("Set A.PayrunCode=Null ");
                SQL.AppendLine("Where A.PayrunCode Is Not Null ");
                SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref List<Result2> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            bool IsFirst = true;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            var EmpCode = string.Empty;
            int n = 0;
            var l2 = new List<EmployeeAllowanceDeduction>();
            var SQL2 = new StringBuilder();
            bool IsExist = false;
            bool IsFirst2 = true;

            SQL.AppendLine("Insert Into TblPayrollProcess1 ");
            SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, NPWP, ");
            SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
            SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
            SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
            SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
            SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
            SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
            SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
            SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
            SQL.AppendLine("PerformanceStatus, GradePerformance, PerformanceValue, ");
            SQL.AppendLine("SalaryAdjustment, ADOT, Tax, TaxAllowance, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            foreach (var x in l)
            {
                i++;

                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine(
                    "(@PayrunCode_" + i.ToString() + 
                    ", @EmpCode_" + i.ToString() + 
                    ", @JoinDt_" + i.ToString() + 
                    ", @ResignDt_" + i.ToString() + 
                    ", @NPWP_" + i.ToString() +
                    ", @PTKP_" + i.ToString() +
                    ", @Salary_" + i.ToString() +
                    ", @WorkingDay_" + i.ToString() +
                    ", @PLDay_" + i.ToString() +
                    ", @PLHr_" + i.ToString() + 
                    ", @PLAmt_" + i.ToString() + 
                    ", @ProcessPLAmt_" + i.ToString() +
                    ", @UPLDay_" + i.ToString() +
                    ", @UPLHr_" + i.ToString() +
                    ", @UPLAmt_" + i.ToString() +
                    ", @ProcessUPLAmt_" + i.ToString() +
                    ", @OT1Hr_" + i.ToString() +
                    ", @OT2Hr_" + i.ToString() +
                    ", @OTHolidayHr_" + i.ToString() +
                    ", @OT1Amt_" + i.ToString() +
                    ", @OT2Amt_" + i.ToString() +
                    ", @OTHolidayAmt_" + i.ToString() +
                    ", @NonTaxableFixAllowance_" + i.ToString() +
                    ", @TaxableFixAllowance_" + i.ToString() +
                    ", @FixAllowance_" + i.ToString() +
                    ", @EmploymentPeriodAllowance_" + i.ToString() +
                    ", @IncEmployee_" + i.ToString() +
                    ", @IncMinWages_" + i.ToString() +
                    ", @IncProduction_" + i.ToString() +
                    ", @IncPerformance_" + i.ToString() +
                    ", @PresenceReward_" + i.ToString() +
                    ", @HolidayEarning_" + i.ToString() +
                    ", @ExtraFooding_" + i.ToString() +
                    ", @FieldAssignment_" + i.ToString() +
                    ", @Transport_" + i.ToString() +
                    ", @Meal_" + i.ToString() +
                    ", @Functional_" + i.ToString() +
                    ", @Housing_" + i.ToString() +
                    ", @MobileCredit_" + i.ToString() +
                    ", @Zakat_" + i.ToString() +
                    ", @ServiceChargeIncentive_" + i.ToString() +
                    ", @SSEmployerHealth_" + i.ToString() +
                    ", @SSEmployerEmployment_" + i.ToString() +
                    ", @SSEmployerPension_" + i.ToString() +
                    ", @SSEmployerPension2_" + i.ToString() +
                    ", @NonTaxableFixDeduction_" + i.ToString() +
                    ", @TaxableFixDeduction_" + i.ToString() +
                    ", @FixDeduction_" + i.ToString() +
                    ", @DedEmployee_" + i.ToString() +
                    ", @DedProduction_" + i.ToString() +
                    ", @DedProdLeave_" + i.ToString() +
                    ", @EmpAdvancePayment_" + i.ToString() +
                    ", @SSEmployeeHealth_" + i.ToString() +
                    ", @SSEmployeeEmployment_" + i.ToString() +
                    ", @SSEmployeePension_" + i.ToString() +
                    ", @SSEmployeePension2_" + i.ToString() +
                    ", @SalaryPension_" + i.ToString() +
                    ", @SSErLifeInsurance_" + i.ToString() +
                    ", @SSEeLifeInsurance_" + i.ToString() +
                    ", @SSErWorkingAccident_" + i.ToString() +
                    ", @SSEeWorkingAccident_" + i.ToString() +
                    ", @SSErRetirement_" + i.ToString() +
                    ", @SSEeRetirement_" + i.ToString() +
                    ", @SSErPension_" + i.ToString() +
                    ", @SSEePension_" + i.ToString() +
                    ", @CreditCode1_" + i.ToString() +
                    ", @CreditCode2_" + i.ToString() +
                    ", @CreditCode3_" + i.ToString() +
                    ", @CreditCode4_" + i.ToString() +
                    ", @CreditCode5_" + i.ToString() +
                    ", @CreditCode6_" + i.ToString() +
                    ", @CreditCode7_" + i.ToString() +
                    ", @CreditCode8_" + i.ToString() +
                    ", @CreditCode9_" + i.ToString() +
                    ", @CreditCode10_" + i.ToString() +
                    ", @CreditAdvancePayment1_" + i.ToString() +
                    ", @CreditAdvancePayment2_" + i.ToString() +
                    ", @CreditAdvancePayment3_" + i.ToString() +
                    ", @CreditAdvancePayment4_" + i.ToString() +
                    ", @CreditAdvancePayment5_" + i.ToString() +
                    ", @CreditAdvancePayment6_" + i.ToString() +
                    ", @CreditAdvancePayment7_" + i.ToString() +
                    ", @CreditAdvancePayment8_" + i.ToString() +
                    ", @CreditAdvancePayment9_" + i.ToString() +
                    ", @CreditAdvancePayment10_" + i.ToString() +
                    ", @PerformanceStatus_" + i.ToString() +
                    ", @GradePerformance_" + i.ToString() +
                    ", @PerformanceValue_" + i.ToString() +
                    ", @SalaryAdjustment_" + i.ToString() +
                    ", @ADOT_" + i.ToString() +
                    ", @Tax_" + i.ToString() +
                    ", @TaxAllowance_" + i.ToString() +
                    ", @Amt_" + i.ToString() + 
                    ", @UserCode, @Dt) ");

                EmpCode = x.EmpCode;
                n = 0;
                IsExist = false;
                l2.Clear();

                foreach (var index in mlEmployeeAllowanceDeduction.Where(w => Sm.CompareStr(w.EmpCode, EmpCode) && w.Amt != 0m))
                {
                    IsExist = false;
                    if (l2.Count > 0)
                    {
                        foreach (var j in l2.Where(w => Sm.CompareStr(w.EmpCode, EmpCode) && Sm.CompareStr(w.ADCode, index.ADCode)))
                        {
                            j.Amt += index.Amt;
                            IsExist = true;
                            break;
                        }
                    }
                    if (!IsExist)
                    {
                        l2.Add(new EmployeeAllowanceDeduction()
                        {
                            EmpCode = index.EmpCode,
                            ADCode = index.ADCode,
                            StartDt = index.StartDt,
                            EndDt = index.EndDt,
                            Amt = index.Amt,
                            ADType = index.ADType,
                            TaxInd = index.TaxInd
                        });
                    }
                }

                foreach (var index in l2.Where(w => Sm.CompareStr(w.EmpCode, EmpCode) && w.Amt != 0m))
                {
                    n++;
                    if (IsFirst2)
                        IsFirst2 = false;
                    else
                        SQL2.AppendLine(", ");

                    SQL2.AppendLine(
                        " (@PayrunCode_" + i.ToString() + 
                        ", @EmpCode_" + i.ToString() +
                        ", @ADCode_" + i.ToString() + "_" + n.ToString() +
                        ", @Amt_" + i.ToString() + "_" + n.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@ADCode_" + i.ToString() + "_" + n.ToString(), index.ADCode);
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString() + "_" + n.ToString(), decimal.Truncate(index.Amt));
                }

                Sm.CmParam<String>(ref cm, "@PayrunCode_" + i.ToString(), x.PayrunCode);
                Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@JoinDt_" + i.ToString(), x.JoinDt);
                Sm.CmParam<String>(ref cm, "@ResignDt_" + i.ToString(), x.ResignDt);
                Sm.CmParam<String>(ref cm, "@NPWP_" + i.ToString(), x.NPWP);
                Sm.CmParam<String>(ref cm, "@PTKP_" + i.ToString(), x.PTKP);

                if (mIsPayrollAmtRounded)
                {
                    Sm.CmParam<Decimal>(ref cm, "@Salary_" + i.ToString(), decimal.Truncate(x.Salary));
                    Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + i.ToString(), x.WorkingDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLDay_" + i.ToString(), x.PLDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLHr_" + i.ToString(), x.PLHr);
                    Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + i.ToString(), decimal.Truncate(x.PLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + i.ToString(), decimal.Truncate(x.ProcessPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + i.ToString(), x.UPLDay);
                    Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + i.ToString(), x.UPLHr);
                    Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + i.ToString(), decimal.Truncate(x.UPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + i.ToString(), decimal.Truncate(x.ProcessUPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + i.ToString(), x.OT1Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + i.ToString(), x.OT2Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + i.ToString(), x.OTHolidayHr);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + i.ToString(), decimal.Truncate(x.OT1Amt));
                    Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + i.ToString(), decimal.Truncate(x.OT2Amt));
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + i.ToString(), decimal.Truncate(x.OTHolidayAmt));
                    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance_" + i.ToString(), decimal.Truncate(x.NonTaxableFixAllowance));
                    Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance_" + i.ToString(), decimal.Truncate(x.TaxableFixAllowance));
                    Sm.CmParam<Decimal>(ref cm, "@FixAllowance_" + i.ToString(), decimal.Truncate(x.FixAllowance));
                    Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance_" + i.ToString(), decimal.Truncate(x.EmploymentPeriodAllowance));
                    Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + i.ToString(), decimal.Truncate(x.IncEmployee));
                    Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + i.ToString(), decimal.Truncate(x.IncMinWages));
                    Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + i.ToString(), decimal.Truncate(x.IncProduction));
                    Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + i.ToString(), decimal.Truncate(x.IncPerformance));
                    Sm.CmParam<Decimal>(ref cm, "@PresenceReward_" + i.ToString(), decimal.Truncate(x.PresenceReward));
                    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + i.ToString(), decimal.Truncate(x.HolidayEarning));
                    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + i.ToString(), decimal.Truncate(x.ExtraFooding));
                    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + i.ToString(), decimal.Truncate(x.FieldAssignment));
                    Sm.CmParam<Decimal>(ref cm, "@Transport_" + i.ToString(), decimal.Truncate(x.Transport));
                    Sm.CmParam<Decimal>(ref cm, "@Meal_" + i.ToString(), decimal.Truncate(x.Meal));
                    Sm.CmParam<Decimal>(ref cm, "@Functional_" + i.ToString(), decimal.Truncate(x.Functional));
                    Sm.CmParam<Decimal>(ref cm, "@Housing_" + i.ToString(), decimal.Truncate(x.Housing));
                    Sm.CmParam<Decimal>(ref cm, "@MobileCredit_" + i.ToString(), decimal.Truncate(x.MobileCredit));
                    Sm.CmParam<Decimal>(ref cm, "@Zakat_" + i.ToString(), decimal.Truncate(x.Zakat));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth_" + i.ToString(), decimal.Truncate(x.SSEmployerHealth));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment_" + i.ToString(), decimal.Truncate(x.SSEmployerEmployment));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension_" + i.ToString(), decimal.Truncate(x.SSEmployerPension));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2_" + i.ToString(), decimal.Truncate(x.SSEmployerPension2));
                    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction_" + i.ToString(), decimal.Truncate(x.NonTaxableFixDeduction));
                    Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction_" + i.ToString(), decimal.Truncate(x.TaxableFixDeduction));
                    Sm.CmParam<Decimal>(ref cm, "@FixDeduction_" + i.ToString(), decimal.Truncate(x.FixDeduction));
                    Sm.CmParam<Decimal>(ref cm, "@DedEmployee_" + i.ToString(), decimal.Truncate(x.DedEmployee));
                    Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + i.ToString(), decimal.Truncate(x.DedProduction));
                    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + i.ToString(), decimal.Truncate(x.DedProdLeave));
                    Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment_" + i.ToString(), decimal.Truncate(x.EmpAdvancePayment));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth_" + i.ToString(), decimal.Truncate(x.SSEmployeeHealth));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment_" + i.ToString(), decimal.Truncate(x.SSEmployeeEmployment));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension_" + i.ToString(), decimal.Truncate(x.SSEmployeePension));
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2_" + i.ToString(), decimal.Truncate(x.SSEmployeePension2));
                    Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + i.ToString(), decimal.Truncate(x.SalaryPension));
                    Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance_" + i.ToString(), decimal.Truncate(x.SSErLifeInsurance));
                    Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance_" + i.ToString(), decimal.Truncate(x.SSEeLifeInsurance));
                    Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident_" + i.ToString(), decimal.Truncate(x.SSErWorkingAccident));
                    Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident_" + i.ToString(), decimal.Truncate(x.SSEeWorkingAccident));
                    Sm.CmParam<Decimal>(ref cm, "@SSErRetirement_" + i.ToString(), decimal.Truncate(x.SSErRetirement));
                    Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement_" + i.ToString(), decimal.Truncate(x.SSEeRetirement));
                    Sm.CmParam<Decimal>(ref cm, "@SSErPension_" + i.ToString(), decimal.Truncate(x.SSErPension));
                    Sm.CmParam<Decimal>(ref cm, "@SSEePension_" + i.ToString(), decimal.Truncate(x.SSEePension));
                    Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment_" + i.ToString(), decimal.Truncate(x.SalaryAdjustment));
                    Sm.CmParam<String>(ref cm, "@CreditCode1_" + i.ToString(), x.CreditCode1);
                    Sm.CmParam<String>(ref cm, "@CreditCode2_" + i.ToString(), x.CreditCode2);
                    Sm.CmParam<String>(ref cm, "@CreditCode3_" + i.ToString(), x.CreditCode3);
                    Sm.CmParam<String>(ref cm, "@CreditCode4_" + i.ToString(), x.CreditCode4);
                    Sm.CmParam<String>(ref cm, "@CreditCode5_" + i.ToString(), x.CreditCode5);
                    Sm.CmParam<String>(ref cm, "@CreditCode6_" + i.ToString(), x.CreditCode6);
                    Sm.CmParam<String>(ref cm, "@CreditCode7_" + i.ToString(), x.CreditCode7);
                    Sm.CmParam<String>(ref cm, "@CreditCode8_" + i.ToString(), x.CreditCode8);
                    Sm.CmParam<String>(ref cm, "@CreditCode9_" + i.ToString(), x.CreditCode9);
                    Sm.CmParam<String>(ref cm, "@CreditCode10_" + i.ToString(), x.CreditCode10);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment1));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment2));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment3));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment4));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment5));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment6));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment7));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment8));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment9));
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10_" + i.ToString(), decimal.Truncate(x.CreditAdvancePayment10));
                    Sm.CmParam<String>(ref cm, "@PerformanceStatus_" + i.ToString(), x.PerformanceStatus);
                    Sm.CmParam<String>(ref cm, "@GradePerformance_" + i.ToString(), x.GradePerformance);
                    Sm.CmParam<Decimal>(ref cm, "@PerformanceValue_" + i.ToString(), decimal.Truncate(x.PerformanceValue));
                    Sm.CmParam<Decimal>(ref cm, "@ADOT_" + i.ToString(), decimal.Truncate(x.ADOT));
                    Sm.CmParam<Decimal>(ref cm, "@Tax_" + i.ToString(), decimal.Truncate(x.Tax));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAllowance_" + i.ToString(), decimal.Truncate(x.TaxAllowance));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), decimal.Truncate(x.Amt));
                    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + i.ToString(), decimal.Truncate(x.ServiceChargeIncentive));
                }
                else
                {
                    Sm.CmParam<Decimal>(ref cm, "@Salary_" + i.ToString(), x.Salary);
                    Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + i.ToString(), x.WorkingDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLDay_" + i.ToString(), x.PLDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLHr_" + i.ToString(), x.PLHr);
                    Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + i.ToString(), x.PLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + i.ToString(), x.ProcessPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + i.ToString(), x.UPLDay);
                    Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + i.ToString(), x.UPLHr);
                    Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + i.ToString(), x.UPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + i.ToString(), x.ProcessUPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + i.ToString(), x.OT1Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + i.ToString(), x.OT2Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + i.ToString(), x.OTHolidayHr);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + i.ToString(), x.OT1Amt);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + i.ToString(), x.OT2Amt);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + i.ToString(), x.OTHolidayAmt);
                    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance_" + i.ToString(), x.NonTaxableFixAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance_" + i.ToString(), x.TaxableFixAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@FixAllowance_" + i.ToString(), x.FixAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance_" + i.ToString(), x.EmploymentPeriodAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + i.ToString(), x.IncEmployee);
                    Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + i.ToString(), x.IncMinWages);
                    Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + i.ToString(), x.IncProduction);
                    Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + i.ToString(), x.IncPerformance);
                    Sm.CmParam<Decimal>(ref cm, "@PresenceReward_" + i.ToString(), x.PresenceReward);
                    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + i.ToString(), x.HolidayEarning);
                    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + i.ToString(), x.ExtraFooding);
                    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + i.ToString(), x.FieldAssignment);
                    Sm.CmParam<Decimal>(ref cm, "@Transport_" + i.ToString(), x.Transport);
                    Sm.CmParam<Decimal>(ref cm, "@Meal_" + i.ToString(), x.Meal);
                    Sm.CmParam<Decimal>(ref cm, "@Functional_" + i.ToString(), x.Functional);
                    Sm.CmParam<Decimal>(ref cm, "@Housing_" + i.ToString(), x.Housing);
                    Sm.CmParam<Decimal>(ref cm, "@MobileCredit_" + i.ToString(), x.MobileCredit);
                    Sm.CmParam<Decimal>(ref cm, "@Zakat_" + i.ToString(), x.Zakat);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth_" + i.ToString(), x.SSEmployerHealth);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment_" + i.ToString(), x.SSEmployerEmployment);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension_" + i.ToString(), x.SSEmployerPension);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2_" + i.ToString(), x.SSEmployerPension2);
                    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction_" + i.ToString(), x.NonTaxableFixDeduction);
                    Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction_" + i.ToString(), x.TaxableFixDeduction);
                    Sm.CmParam<Decimal>(ref cm, "@FixDeduction_" + i.ToString(), x.FixDeduction);
                    Sm.CmParam<Decimal>(ref cm, "@DedEmployee_" + i.ToString(), x.DedEmployee);
                    Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + i.ToString(), x.DedProduction);
                    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + i.ToString(), x.DedProdLeave);
                    Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment_" + i.ToString(), x.EmpAdvancePayment);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth_" + i.ToString(), x.SSEmployeeHealth);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment_" + i.ToString(), x.SSEmployeeEmployment);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension_" + i.ToString(), x.SSEmployeePension);
                    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2_" + i.ToString(), x.SSEmployeePension2);
                    Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + i.ToString(), x.SalaryPension);
                    Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance_" + i.ToString(), x.SSErLifeInsurance);
                    Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance_" + i.ToString(), x.SSEeLifeInsurance);
                    Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident_" + i.ToString(), x.SSErWorkingAccident);
                    Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident_" + i.ToString(), x.SSEeWorkingAccident);
                    Sm.CmParam<Decimal>(ref cm, "@SSErRetirement_" + i.ToString(), x.SSErRetirement);
                    Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement_" + i.ToString(), x.SSEeRetirement);
                    Sm.CmParam<Decimal>(ref cm, "@SSErPension_" + i.ToString(), x.SSErPension);
                    Sm.CmParam<Decimal>(ref cm, "@SSEePension_" + i.ToString(), x.SSEePension);
                    Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment_" + i.ToString(), x.SalaryAdjustment);
                    Sm.CmParam<String>(ref cm, "@CreditCode1_" + i.ToString(), x.CreditCode1);
                    Sm.CmParam<String>(ref cm, "@CreditCode2_" + i.ToString(), x.CreditCode2);
                    Sm.CmParam<String>(ref cm, "@CreditCode3_" + i.ToString(), x.CreditCode3);
                    Sm.CmParam<String>(ref cm, "@CreditCode4_" + i.ToString(), x.CreditCode4);
                    Sm.CmParam<String>(ref cm, "@CreditCode5_" + i.ToString(), x.CreditCode5);
                    Sm.CmParam<String>(ref cm, "@CreditCode6_" + i.ToString(), x.CreditCode6);
                    Sm.CmParam<String>(ref cm, "@CreditCode7_" + i.ToString(), x.CreditCode7);
                    Sm.CmParam<String>(ref cm, "@CreditCode8_" + i.ToString(), x.CreditCode8);
                    Sm.CmParam<String>(ref cm, "@CreditCode9_" + i.ToString(), x.CreditCode9);
                    Sm.CmParam<String>(ref cm, "@CreditCode10_" + i.ToString(), x.CreditCode10);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1_" + i.ToString(), x.CreditAdvancePayment1);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2_" + i.ToString(), x.CreditAdvancePayment2);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3_" + i.ToString(), x.CreditAdvancePayment3);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4_" + i.ToString(), x.CreditAdvancePayment4);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5_" + i.ToString(), x.CreditAdvancePayment5);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6_" + i.ToString(), x.CreditAdvancePayment6);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7_" + i.ToString(), x.CreditAdvancePayment7);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8_" + i.ToString(), x.CreditAdvancePayment8);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9_" + i.ToString(), x.CreditAdvancePayment9);
                    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10_" + i.ToString(), x.CreditAdvancePayment10);
                    Sm.CmParam<String>(ref cm, "@PerformanceStatus_" + i.ToString(), x.PerformanceStatus);
                    Sm.CmParam<String>(ref cm, "@GradePerformance_" + i.ToString(), x.GradePerformance);
                    Sm.CmParam<Decimal>(ref cm, "@PerformanceValue_" + i.ToString(), x.PerformanceValue);
                    Sm.CmParam<Decimal>(ref cm, "@ADOT_" + i.ToString(), x.ADOT);
                    Sm.CmParam<Decimal>(ref cm, "@Tax_" + i.ToString(), x.Tax);
                    Sm.CmParam<Decimal>(ref cm, "@TaxAllowance_" + i.ToString(), x.TaxAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), x.Amt);
                    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + i.ToString(), x.ServiceChargeIncentive);
                }
                Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForProratedAD_" + i.ToString(), x._RecomputedValueForProratedAD);

            }

            cm.CommandText = SQL.ToString() + ";" +
                            (IsFirst2?string.Empty:
                            "Insert Into TblPayrollProcessAD(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) Values " + 
                            SQL2.ToString() + ";")
                ;
            

            return cm;
        }

        private MySqlCommand SavePayrollProcess2(ref List<Result1> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            bool IsFirst = true;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblPayrollProcess2 ");
            SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
            SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
            SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
            SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
            SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
            SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
            SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
            SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
            SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
            SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
            SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
            SQL.AppendLine("DedProdLeave, ADOT, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            foreach (var x in l)
            {
                i++;

                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@PayrunCode_" + i.ToString() + ", @EmpCode_" + i.ToString() + ", @Dt_" + i.ToString() + ", @ProcessInd_" + i.ToString() + ", @LatestPaidDt_" + i.ToString() + ", ");
                SQL.AppendLine("@JoinDt_" + i.ToString() + ", @ResignDt_" + i.ToString() + ", @PosCode_" + i.ToString() + ", @DeptCode_" + i.ToString() + ", @GrdLvlCode_" + i.ToString() + ", @LevelCode_" + i.ToString() + ", ");
                SQL.AppendLine("@SystemType_" + i.ToString() + ", @EmploymentStatus_" + i.ToString() + ", @PayrunPeriod_" + i.ToString() + ", @PGCode_" + i.ToString() + ", @SiteCode_" + i.ToString() + ", @WorkingDay_" + i.ToString() + ", ");
                SQL.AppendLine("@WSCode_" + i.ToString() + ", @HolInd_" + i.ToString() + ", @HolidayIndex_" + i.ToString() + ", @WSHolidayInd_" + i.ToString() + ", @WSIn1_" + i.ToString() + ", ");
                SQL.AppendLine("@WSOut1_" + i.ToString() + ", @WSIn2_" + i.ToString() + ", @WSOut2_" + i.ToString() + ", @WSIn3_" + i.ToString() + ", @WSOut3_" + i.ToString() + ", ");
                SQL.AppendLine("@OneDayInd_" + i.ToString() + ", @LateInd_" + i.ToString() + ", @ActualIn_" + i.ToString() + ", @ActualOut_" + i.ToString() + ", @WorkingIn_" + i.ToString() + ", ");
                SQL.AppendLine("@WorkingOut_" + i.ToString() + ", @WorkingDuration_" + i.ToString() + ", @EmpSalary_" + i.ToString() + ", @EmpSalary2_" + i.ToString() + ", @BasicSalary_" + i.ToString() + ", @BasicSalary2_" + i.ToString() + ", @SalaryPension_" + i.ToString() + ", @ProductionWages_" + i.ToString() + ", ");
                SQL.AppendLine("@Salary_" + i.ToString() + ", @LeaveCode_" + i.ToString() + ", @LeaveType_" + i.ToString() + ", @PaidLeaveInd_" + i.ToString() + ", @LeaveStartTm,");
                SQL.AppendLine("@LeaveEndTm_" + i.ToString() + ", @LeaveDuration_" + i.ToString() + ", @PLDay_" + i.ToString() + ", @PLHr_" + i.ToString() + ", @PLAmt_" + i.ToString() + ", @ProcessPLAmt_" + i.ToString() + ", @UPLDay_" + i.ToString() + ", @UPLHr_" + i.ToString() + ", @UPLAmt_" + i.ToString() + ", @ProcessUPLAmt_" + i.ToString() + ", ");
                SQL.AppendLine("@OT1Hr_" + i.ToString() + ", @OT2Hr_" + i.ToString() + ", @OTHolidayHr_" + i.ToString() + ", @OT1Amt_" + i.ToString() + ", ");
                SQL.AppendLine("@OT2Amt_" + i.ToString() + ", @OTHolidayAmt_" + i.ToString() + ", @OTToLeaveInd_" + i.ToString() + ", @IncMinWages_" + i.ToString() + ", @IncProduction_" + i.ToString() + ", @IncEmployee_" + i.ToString() + ", ");
                SQL.AppendLine("@IncPerformance_" + i.ToString() + ", @PresenceRewardInd_" + i.ToString() + ", @HolidayEarning_" + i.ToString() + ", @ExtraFooding_" + i.ToString() + ", @FieldAssignment_" + i.ToString() + ", @Transport_" + i.ToString() + ", @Meal_" + i.ToString() + ", @ServiceChargeIncentive_" + i.ToString() + ", @DedProduction_" + i.ToString() + ", ");
                SQL.AppendLine("@DedProdLeave_" + i.ToString() + ", @ADOT_" + i.ToString() + ", @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "@PayrunCode_" + i.ToString(), x.PayrunCode);
                Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@Dt_" + i.ToString(), x.Dt);
                Sm.CmParam<String>(ref cm, "@ProcessInd_" + i.ToString(), x.ProcessInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LatestPaidDt_" + i.ToString(), x.LatestPaidDt);
                Sm.CmParam<String>(ref cm, "@JoinDt_" + i.ToString(), x.JoinDt);
                Sm.CmParam<String>(ref cm, "@ResignDt_" + i.ToString(), x.ResignDt);
                Sm.CmParam<String>(ref cm, "@PosCode_" + i.ToString(), x.PosCode);
                Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), x.DeptCode);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + i.ToString(), x.GrdLvlCode);
                Sm.CmParam<String>(ref cm, "@LevelCode_" + i.ToString(), x.LevelCode);
                Sm.CmParam<String>(ref cm, "@SystemType_" + i.ToString(), x.SystemType);
                Sm.CmParam<String>(ref cm, "@EmploymentStatus_" + i.ToString(), x.EmploymentStatus);
                Sm.CmParam<String>(ref cm, "@PayrunPeriod_" + i.ToString(), x.PayrunPeriod);
                Sm.CmParam<String>(ref cm, "@PGCode_" + i.ToString(), x.PGCode);
                Sm.CmParam<String>(ref cm, "@SiteCode_" + i.ToString(), x.SiteCode);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + i.ToString(), x.WorkingDay);
                Sm.CmParam<String>(ref cm, "@WSCode_" + i.ToString(), x.WSCode);
                Sm.CmParam<String>(ref cm, "@HolInd_" + i.ToString(), x.HolInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@HolidayIndex_" + i.ToString(), x.HolidayIndex);
                Sm.CmParam<String>(ref cm, "@WSHolidayInd_" + i.ToString(), x.WSHolidayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@WSIn1_" + i.ToString(), x.WSIn1);
                Sm.CmParam<String>(ref cm, "@WSOut1_" + i.ToString(), x.WSOut1);
                Sm.CmParam<String>(ref cm, "@WSIn2_" + i.ToString(), x.WSIn2);
                Sm.CmParam<String>(ref cm, "@WSOut2_" + i.ToString(), x.WSOut2);
                Sm.CmParam<String>(ref cm, "@WSIn3_" + i.ToString(), x.WSIn3);
                Sm.CmParam<String>(ref cm, "@WSOut3_" + i.ToString(), x.WSOut3);
                Sm.CmParam<String>(ref cm, "@OneDayInd_" + i.ToString(), x.OneDayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LateInd_" + i.ToString(), x.LateInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ActualIn_" + i.ToString(), x.ActualIn);
                Sm.CmParam<String>(ref cm, "@ActualOut_" + i.ToString(), x.ActualOut);
                Sm.CmParam<String>(ref cm, "@WorkingIn_" + i.ToString(), x.WorkingIn);
                Sm.CmParam<String>(ref cm, "@WorkingOut_" + i.ToString(), x.WorkingOut);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDuration_" + i.ToString(), x.WorkingDuration);

                if (mIsPayrollAmtRounded)
                {
                    Sm.CmParam<Decimal>(ref cm, "@EmpSalary_" + i.ToString(), decimal.Truncate(x.EmpSalary));
                    Sm.CmParam<Decimal>(ref cm, "@EmpSalary2_" + i.ToString(), decimal.Truncate(x.EmpSalary2));
                    Sm.CmParam<Decimal>(ref cm, "@BasicSalary_" + i.ToString(), decimal.Truncate(x.BasicSalary));
                    Sm.CmParam<Decimal>(ref cm, "@BasicSalary2_" + i.ToString(), decimal.Truncate(x.BasicSalary2));
                    Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + i.ToString(), decimal.Truncate(x.SalaryPension));
                    Sm.CmParam<Decimal>(ref cm, "@ProductionWages_" + i.ToString(), decimal.Truncate(x.ProductionWages));
                    Sm.CmParam<Decimal>(ref cm, "@Salary_" + i.ToString(), decimal.Truncate(x.Salary));
                    Sm.CmParam<String>(ref cm, "@LeaveCode_" + i.ToString(), x.LeaveCode);
                    Sm.CmParam<String>(ref cm, "@LeaveType_" + i.ToString(), x.LeaveType);
                    Sm.CmParam<String>(ref cm, "@PaidLeaveInd_" + i.ToString(), x.PaidLeaveInd ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@LeaveStartTm_" + i.ToString(), x.LeaveStartTm);
                    Sm.CmParam<String>(ref cm, "@LeaveEndTm_" + i.ToString(), x.LeaveEndTm);
                    Sm.CmParam<Decimal>(ref cm, "@LeaveDuration_" + i.ToString(), x.LeaveDuration);
                    Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + i.ToString(), x.UPLDay);
                    Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + i.ToString(), x.UPLHr);
                    Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + i.ToString(), decimal.Truncate(x.UPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + i.ToString(), decimal.Truncate(x.ProcessUPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@PLDay_" + i.ToString(), x.PLDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLHr_" + i.ToString(), x.PLHr);
                    Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + i.ToString(), decimal.Truncate(x.PLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + i.ToString(), decimal.Truncate(x.ProcessPLAmt));
                    Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + i.ToString(), x.OT1Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + i.ToString(), x.OT2Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + i.ToString(), x.OTHolidayHr);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + i.ToString(), decimal.Truncate(x.OT1Amt));
                    Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + i.ToString(), decimal.Truncate(x.OT2Amt));
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + i.ToString(), decimal.Truncate(x.OTHolidayAmt));
                    Sm.CmParam<String>(ref cm, "@OTToLeaveInd_" + i.ToString(), x.OTToLeaveInd ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + i.ToString(), decimal.Truncate(x.IncMinWages));
                    Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + i.ToString(), decimal.Truncate(x.IncProduction));
                    Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + i.ToString(), decimal.Truncate(x.IncEmployee));
                    Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + i.ToString(), decimal.Truncate(x.IncPerformance));
                    Sm.CmParam<String>(ref cm, "@PresenceRewardInd_" + i.ToString(), x.PresenceRewardInd ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + i.ToString(), decimal.Truncate(x.HolidayEarning));
                    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + i.ToString(), decimal.Truncate(x.ExtraFooding));
                    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + i.ToString(), decimal.Truncate(x.FieldAssignment));
                    Sm.CmParam<Decimal>(ref cm, "@Transport_" + i.ToString(), decimal.Truncate(x.Transport));
                    Sm.CmParam<Decimal>(ref cm, "@Meal_" + i.ToString(), decimal.Truncate(x.Meal));
                    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + i.ToString(), decimal.Truncate(x.ServiceChargeIncentive));
                    Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + i.ToString(), decimal.Truncate(x.DedProduction));
                    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + i.ToString(), decimal.Truncate(x.DedProdLeave));
                    Sm.CmParam<Decimal>(ref cm, "@ADOT_" + i.ToString(), decimal.Truncate(x.ADOT));
                }
                else
                {
                    Sm.CmParam<Decimal>(ref cm, "@EmpSalary_" + i.ToString(), x.EmpSalary);
                    Sm.CmParam<Decimal>(ref cm, "@EmpSalary2_" + i.ToString(), x.EmpSalary2);
                    Sm.CmParam<Decimal>(ref cm, "@BasicSalary_" + i.ToString(), x.BasicSalary);
                    Sm.CmParam<Decimal>(ref cm, "@BasicSalary2_" + i.ToString(), x.BasicSalary2);
                    Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + i.ToString(), x.SalaryPension);
                    Sm.CmParam<Decimal>(ref cm, "@ProductionWages_" + i.ToString(), x.ProductionWages);
                    Sm.CmParam<Decimal>(ref cm, "@Salary_" + i.ToString(), x.Salary);
                    Sm.CmParam<String>(ref cm, "@LeaveCode_" + i.ToString(), x.LeaveCode);
                    Sm.CmParam<String>(ref cm, "@LeaveType_" + i.ToString(), x.LeaveType);
                    Sm.CmParam<String>(ref cm, "@PaidLeaveInd_" + i.ToString(), x.PaidLeaveInd ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@LeaveStartTm_" + i.ToString(), x.LeaveStartTm);
                    Sm.CmParam<String>(ref cm, "@LeaveEndTm_" + i.ToString(), x.LeaveEndTm);
                    Sm.CmParam<Decimal>(ref cm, "@LeaveDuration_" + i.ToString(), x.LeaveDuration);
                    Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + i.ToString(), x.UPLDay);
                    Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + i.ToString(), x.UPLHr);
                    Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + i.ToString(), x.UPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + i.ToString(), x.ProcessUPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@PLDay_" + i.ToString(), x.PLDay);
                    Sm.CmParam<Decimal>(ref cm, "@PLHr_" + i.ToString(), x.PLHr);
                    Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + i.ToString(), x.PLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + i.ToString(), x.ProcessPLAmt);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + i.ToString(), x.OT1Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + i.ToString(), x.OT2Hr);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + i.ToString(), x.OTHolidayHr);
                    Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + i.ToString(), x.OT1Amt);
                    Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + i.ToString(), x.OT2Amt);
                    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + i.ToString(), x.OTHolidayAmt);
                    Sm.CmParam<String>(ref cm, "@OTToLeaveInd_" + i.ToString(), x.OTToLeaveInd ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + i.ToString(), x.IncMinWages);
                    Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + i.ToString(), x.IncProduction);
                    Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + i.ToString(), x.IncEmployee);
                    Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + i.ToString(), x.IncPerformance);
                    Sm.CmParam<String>(ref cm, "@PresenceRewardInd_" + i.ToString(), x.PresenceRewardInd ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + i.ToString(), x.HolidayEarning);
                    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + i.ToString(), x.ExtraFooding);
                    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + i.ToString(), x.FieldAssignment);
                    Sm.CmParam<Decimal>(ref cm, "@Transport_" + i.ToString(), x.Transport);
                    Sm.CmParam<Decimal>(ref cm, "@Meal_" + i.ToString(), x.Meal);
                    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + i.ToString(), x.ServiceChargeIncentive);
                    Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + i.ToString(), x.DedProduction);
                    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + i.ToString(), x.DedProdLeave);
                    Sm.CmParam<Decimal>(ref cm, "@ADOT_" + i.ToString(), x.ADOT);
                }
            }

            cm.CommandText = SQL.ToString() + ";";

            return cm;
        }

        private MySqlCommand SavePayrollProcess()
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            if (Filter.Length != 0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

           
            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (PayrunCode Is Not Null And PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblEmpInsPntHdr A ");
            SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSCIDtl A ");
            SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            bool IsFirst = true;

            SQL2.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
            SQL.AppendLine("Where (DocNo='***' And Yr='***' And Mth='***' And EmpCode='***') ");

            SQL2.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL2.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL2.AppendLine("Values ");

            foreach (var x in l)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQL2.AppendLine(", ");

                SQL.AppendLine(" Or (DocNo=@DocNo_" + i.ToString() +
                    " And Yr=@Yr_" + i.ToString() +
                    " And Mth=@Mth_" + i.ToString() +
                    " And EmpCode=@EmpCode_" + i.ToString() +
                    ") ");

                SQL2.AppendLine(
                    "(@DocNo_" + i.ToString() +
                    ", @Yr_" + i.ToString() +
                    ", @Mth_" + i.ToString() +
                    ", @EmpCode_" + i.ToString() +
                    ", @Amt_" + i.ToString() +
                    ", @PayrunCode, @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "DocNo_" + i.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "Yr_" + i.ToString(), x.Yr);
                Sm.CmParam<String>(ref cm, "Mth_" + i.ToString(), x.Mth);
                Sm.CmParam<String>(ref cm, "EmpCode_" + i.ToString(), x.EmpCode);
                if (mIsPayrollAmtRounded)
                    Sm.CmParam<Decimal>(ref cm, "Amt_" + i.ToString(), decimal.Truncate(x.Amt));
                else
                    Sm.CmParam<Decimal>(ref cm, "Amt_" + i.ToString(), x.Amt);

                i++;
            }

            cm.CommandText = SQL.ToString() + ";" + SQL2.ToString() + ";";
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SavePayrollProcess1(ref Result2 r)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    var EmpCode = r.EmpCode;
        //    int n = 0;
        //    var l = new List<EmployeeAllowanceDeduction>();
        //    bool IsExist = false;

        //    SQL.AppendLine("Insert Into TblPayrollProcess1 ");
        //    SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, NPWP, ");
        //    SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
        //    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
        //    SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
        //    SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
        //    SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
        //    SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
        //    SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
        //    SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
        //    SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
        //    SQL.AppendLine("PerformanceStatus, GradePerformance, PerformanceValue, ");
        //    SQL.AppendLine("SalaryAdjustment, ADOT, Tax, TaxAllowance, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @NPWP, ");
        //    SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
        //    SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
        //    SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, @EmploymentPeriodAllowance, ");
        //    SQL.AppendLine("@IncEmployee, @IncMinWages, @IncProduction, @IncPerformance, @PresenceReward, ");
        //    SQL.AppendLine("@HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @Functional, @Housing, @MobileCredit, @Zakat, @ServiceChargeIncentive, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @SSEmployerPension2, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
        //    SQL.AppendLine("@DedEmployee, @DedProduction, @DedProdLeave, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployeePension2, ");
        //    SQL.AppendLine("@SalaryPension, @SSErLifeInsurance, @SSEeLifeInsurance, @SSErWorkingAccident, @SSEeWorkingAccident, @SSErRetirement, @SSEeRetirement, @SSErPension, @SSEePension, ");
        //    SQL.AppendLine("@CreditCode1, @CreditCode2, @CreditCode3, @CreditCode4, @CreditCode5, @CreditCode6, @CreditCode7, @CreditCode8, @CreditCode9, @CreditCode10, ");
        //    SQL.AppendLine("@CreditAdvancePayment1, @CreditAdvancePayment2, @CreditAdvancePayment3, @CreditAdvancePayment4, @CreditAdvancePayment5, @CreditAdvancePayment6, @CreditAdvancePayment7, @CreditAdvancePayment8, @CreditAdvancePayment9, @CreditAdvancePayment10, ");
        //    SQL.AppendLine("@PerformanceStatus, @GradePerformance, @PerformanceValue, ");
        //    SQL.AppendLine("@SalaryAdjustment, @ADOT, @Tax, @TaxAllowance, @Amt, @CreateBy, CurrentDateTime()); ");

        //    foreach (var i in mlEmployeeAllowanceDeduction.Where(w => w.EmpCode == EmpCode && w.Amt != 0m))
        //    {
        //        IsExist = false;
        //        if (l.Count > 0)
        //        {
        //            foreach (var j in l.Where(w => w.EmpCode == EmpCode && w.ADCode == i.ADCode))
        //            {
        //                j.Amt += i.Amt;
        //                IsExist = true;
        //                break;
        //            }
        //        }
        //        if (!IsExist)
        //        {
        //            l.Add(new EmployeeAllowanceDeduction()
        //            {
        //                EmpCode = i.EmpCode,
        //                ADCode = i.ADCode,
        //                StartDt = i.StartDt,
        //                EndDt = i.EndDt,
        //                Amt = i.Amt,
        //                ADType = i.ADType,
        //                TaxInd = i.TaxInd
        //            });
        //        }
        //    }


        //    foreach (var i in l.Where(w => Sm.CompareStr(w.EmpCode, EmpCode) && w.Amt != 0m))
        //    {
        //        n++;

        //        SQL.AppendLine("Insert Into TblPayrollProcessAD ");
        //        SQL.AppendLine("(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@PayrunCode, @EmpCode, @ADCode0" + n.ToString() + ", @Amt0" + n.ToString() + ", @CreateBy, CurrentDateTime()); ");

        //        Sm.CmParam<String>(ref cm, "@ADCode0" + n.ToString(), i.ADCode);
        //        Sm.CmParam<Decimal>(ref cm, "@Amt0" + n.ToString(), decimal.Truncate(i.Amt));
        //    }

        //    cm.CommandText = SQL.ToString();

        //    Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
        //    Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
        //    Sm.CmParam<String>(ref cm, "@NPWP", r.NPWP);
        //    Sm.CmParam<String>(ref cm, "@PTKP", r.PTKP);

        //    if (mIsPayrollAmtRounded)
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Salary", decimal.Truncate(r.Salary));
        //        Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@PLAmt", decimal.Truncate(r.PLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", decimal.Truncate(r.ProcessPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", decimal.Truncate(r.UPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", decimal.Truncate(r.ProcessUPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", decimal.Truncate(r.OT1Amt));
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", decimal.Truncate(r.OT2Amt));
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", decimal.Truncate(r.OTHolidayAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", decimal.Truncate(r.NonTaxableFixAllowance));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", decimal.Truncate(r.TaxableFixAllowance));
        //        Sm.CmParam<Decimal>(ref cm, "@FixAllowance", decimal.Truncate(r.FixAllowance));
        //        Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance", decimal.Truncate(r.EmploymentPeriodAllowance));
        //        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", decimal.Truncate(r.IncEmployee));
        //        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", decimal.Truncate(r.IncMinWages));
        //        Sm.CmParam<Decimal>(ref cm, "@IncProduction", decimal.Truncate(r.IncProduction));
        //        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", decimal.Truncate(r.IncPerformance));
        //        Sm.CmParam<Decimal>(ref cm, "@PresenceReward", decimal.Truncate(r.PresenceReward));
        //        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", decimal.Truncate(r.HolidayEarning));
        //        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", decimal.Truncate(r.ExtraFooding));
        //        Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", decimal.Truncate(r.FieldAssignment));
        //        Sm.CmParam<Decimal>(ref cm, "@Transport", decimal.Truncate(r.Transport));
        //        Sm.CmParam<Decimal>(ref cm, "@Meal", decimal.Truncate(r.Meal));
        //        Sm.CmParam<Decimal>(ref cm, "@Functional", decimal.Truncate(r.Functional));
        //        Sm.CmParam<Decimal>(ref cm, "@Housing", decimal.Truncate(r.Housing));
        //        Sm.CmParam<Decimal>(ref cm, "@MobileCredit", decimal.Truncate(r.MobileCredit));
        //        Sm.CmParam<Decimal>(ref cm, "@Zakat", decimal.Truncate(r.Zakat));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", decimal.Truncate(r.SSEmployerHealth));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", decimal.Truncate(r.SSEmployerEmployment));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", decimal.Truncate(r.SSEmployerPension));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", decimal.Truncate(r.SSEmployerPension2));
        //        Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", decimal.Truncate(r.NonTaxableFixDeduction));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", decimal.Truncate(r.TaxableFixDeduction));
        //        Sm.CmParam<Decimal>(ref cm, "@FixDeduction", decimal.Truncate(r.FixDeduction));
        //        Sm.CmParam<Decimal>(ref cm, "@DedEmployee", decimal.Truncate(r.DedEmployee));
        //        Sm.CmParam<Decimal>(ref cm, "@DedProduction", decimal.Truncate(r.DedProduction));
        //        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", decimal.Truncate(r.DedProdLeave));
        //        Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", decimal.Truncate(r.EmpAdvancePayment));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", decimal.Truncate(r.SSEmployeeHealth));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", decimal.Truncate(r.SSEmployeeEmployment));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", decimal.Truncate(r.SSEmployeePension));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", decimal.Truncate(r.SSEmployeePension2));
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryPension", decimal.Truncate(r.SalaryPension));
        //        Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", decimal.Truncate(r.SSErLifeInsurance));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", decimal.Truncate(r.SSEeLifeInsurance));
        //        Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", decimal.Truncate(r.SSErWorkingAccident));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", decimal.Truncate(r.SSEeWorkingAccident));
        //        Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", decimal.Truncate(r.SSErRetirement));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", decimal.Truncate(r.SSEeRetirement));
        //        Sm.CmParam<Decimal>(ref cm, "@SSErPension", decimal.Truncate(r.SSErPension));
        //        Sm.CmParam<Decimal>(ref cm, "@SSEePension", decimal.Truncate(r.SSEePension));
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", decimal.Truncate(r.SalaryAdjustment));
        //        Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
        //        Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
        //        Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
        //        Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
        //        Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
        //        Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
        //        Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
        //        Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
        //        Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
        //        Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", decimal.Truncate(r.CreditAdvancePayment1));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", decimal.Truncate(r.CreditAdvancePayment2));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", decimal.Truncate(r.CreditAdvancePayment3));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", decimal.Truncate(r.CreditAdvancePayment4));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", decimal.Truncate(r.CreditAdvancePayment5));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", decimal.Truncate(r.CreditAdvancePayment6));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", decimal.Truncate(r.CreditAdvancePayment7));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", decimal.Truncate(r.CreditAdvancePayment8));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", decimal.Truncate(r.CreditAdvancePayment9));
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", decimal.Truncate(r.CreditAdvancePayment10));
        //        Sm.CmParam<String>(ref cm, "@PerformanceStatus", r.PerformanceStatus);
        //        Sm.CmParam<String>(ref cm, "@GradePerformance", r.GradePerformance);
        //        Sm.CmParam<Decimal>(ref cm, "@PerformanceValue", decimal.Truncate(r.PerformanceValue));
        //        Sm.CmParam<Decimal>(ref cm, "@ADOT", decimal.Truncate(r.ADOT));
        //        Sm.CmParam<Decimal>(ref cm, "@Tax", decimal.Truncate(r.Tax));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", decimal.Truncate(r.TaxAllowance));
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Truncate(r.Amt));
        //        Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", decimal.Truncate(r.ServiceChargeIncentive));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
        //        Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", r.NonTaxableFixAllowance);
        //        Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", r.TaxableFixAllowance);
        //        Sm.CmParam<Decimal>(ref cm, "@FixAllowance", r.FixAllowance);
        //        Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance", r.EmploymentPeriodAllowance);
        //        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
        //        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
        //        Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
        //        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
        //        Sm.CmParam<Decimal>(ref cm, "@PresenceReward", r.PresenceReward);
        //        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
        //        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
        //        Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
        //        Sm.CmParam<Decimal>(ref cm, "@Transport", r.Transport);
        //        Sm.CmParam<Decimal>(ref cm, "@Meal", r.Meal);
        //        Sm.CmParam<Decimal>(ref cm, "@Functional", r.Functional);
        //        Sm.CmParam<Decimal>(ref cm, "@Housing", r.Housing);
        //        Sm.CmParam<Decimal>(ref cm, "@MobileCredit", r.MobileCredit);
        //        Sm.CmParam<Decimal>(ref cm, "@Zakat", r.Zakat);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", r.SSEmployerHealth);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", r.SSEmployerEmployment);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", r.SSEmployerPension);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", r.SSEmployerPension2);
        //        Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", r.NonTaxableFixDeduction);
        //        Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", r.TaxableFixDeduction);
        //        Sm.CmParam<Decimal>(ref cm, "@FixDeduction", r.FixDeduction);
        //        Sm.CmParam<Decimal>(ref cm, "@DedEmployee", r.DedEmployee);
        //        Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
        //        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
        //        Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", r.EmpAdvancePayment);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", r.SSEmployeeHealth);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", r.SSEmployeeEmployment);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", r.SSEmployeePension);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", r.SSEmployeePension2);
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
        //        Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", r.SSErLifeInsurance);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", r.SSEeLifeInsurance);
        //        Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", r.SSErWorkingAccident);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", r.SSEeWorkingAccident);
        //        Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", r.SSErRetirement);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", r.SSEeRetirement);
        //        Sm.CmParam<Decimal>(ref cm, "@SSErPension", r.SSErPension);
        //        Sm.CmParam<Decimal>(ref cm, "@SSEePension", r.SSEePension);
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", r.SalaryAdjustment);
        //        Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
        //        Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
        //        Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
        //        Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
        //        Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
        //        Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
        //        Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
        //        Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
        //        Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
        //        Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", r.CreditAdvancePayment1);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", r.CreditAdvancePayment2);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", r.CreditAdvancePayment3);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", r.CreditAdvancePayment4);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", r.CreditAdvancePayment5);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", r.CreditAdvancePayment6);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", r.CreditAdvancePayment7);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", r.CreditAdvancePayment8);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", r.CreditAdvancePayment9);
        //        Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", r.CreditAdvancePayment10);
        //        Sm.CmParam<String>(ref cm, "@PerformanceStatus", r.PerformanceStatus);
        //        Sm.CmParam<String>(ref cm, "@GradePerformance", r.GradePerformance);
        //        Sm.CmParam<Decimal>(ref cm, "@PerformanceValue", r.PerformanceValue);
        //        Sm.CmParam<Decimal>(ref cm, "@ADOT", r.ADOT);
        //        Sm.CmParam<Decimal>(ref cm, "@Tax", r.Tax);
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", r.TaxAllowance);
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", r.Amt);
        //        Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
        //    }
        //    Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForProratedAD", r._RecomputedValueForProratedAD);
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
        //    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SavePayrollProcess2(ref Result1 r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPayrollProcess2 ");
        //    SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
        //    SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
        //    SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
        //    SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
        //    SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
        //    SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
        //    SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
        //    SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
        //    SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
        //    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
        //    SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
        //    SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
        //    SQL.AppendLine("DedProdLeave, ADOT, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values( ");
        //    SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
        //    SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, @LevelCode, ");
        //    SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
        //    SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
        //    SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
        //    SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
        //    SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @SalaryPension, @ProductionWages, ");
        //    SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
        //    SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
        //    SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
        //    SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
        //    SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @ServiceChargeIncentive, @DedProduction, ");
        //    SQL.AppendLine("@DedProdLeave, @ADOT, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
        //    Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
        //    Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
        //    Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
        //    Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
        //    Sm.CmParam<String>(ref cm, "@LevelCode", r.LevelCode);
        //    Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
        //    Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
        //    Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
        //    Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
        //    Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
        //    Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
        //    Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
        //    Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
        //    Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
        //    Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
        //    Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
        //    Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
        //    Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
        //    Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
        //    Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
        //    Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
        //    Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);

        //    if (mIsPayrollAmtRounded)
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@EmpSalary", decimal.Truncate(r.EmpSalary));
        //        Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", decimal.Truncate(r.EmpSalary2));
        //        Sm.CmParam<Decimal>(ref cm, "@BasicSalary", decimal.Truncate(r.BasicSalary));
        //        Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", decimal.Truncate(r.BasicSalary2));
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryPension", decimal.Truncate(r.SalaryPension));
        //        Sm.CmParam<Decimal>(ref cm, "@ProductionWages", decimal.Truncate(r.ProductionWages));
        //        Sm.CmParam<Decimal>(ref cm, "@Salary", decimal.Truncate(r.Salary));
        //        Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
        //        Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
        //        Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
        //        Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
        //        Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", decimal.Truncate(r.UPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", decimal.Truncate(r.ProcessUPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@PLAmt", decimal.Truncate(r.PLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", decimal.Truncate(r.ProcessPLAmt));
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", decimal.Truncate(r.OT1Amt));
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", decimal.Truncate(r.OT2Amt));
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", decimal.Truncate(r.OTHolidayAmt));
        //        Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", decimal.Truncate(r.IncMinWages));
        //        Sm.CmParam<Decimal>(ref cm, "@IncProduction", decimal.Truncate(r.IncProduction));
        //        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", decimal.Truncate(r.IncEmployee));
        //        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", decimal.Truncate(r.IncPerformance));
        //        Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", decimal.Truncate(r.HolidayEarning));
        //        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", decimal.Truncate(r.ExtraFooding));
        //        Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", decimal.Truncate(r.FieldAssignment));
        //        Sm.CmParam<Decimal>(ref cm, "@Transport", decimal.Truncate(r.Transport));
        //        Sm.CmParam<Decimal>(ref cm, "@Meal", decimal.Truncate(r.Meal));
        //        Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", decimal.Truncate(r.ServiceChargeIncentive));
        //        Sm.CmParam<Decimal>(ref cm, "@DedProduction", decimal.Truncate(r.DedProduction));
        //        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", decimal.Truncate(r.DedProdLeave));
        //        Sm.CmParam<Decimal>(ref cm, "@ADOT", decimal.Truncate(r.ADOT));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
        //        Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
        //        Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
        //        Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
        //        Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
        //        Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
        //        Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
        //        Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
        //        Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
        //        Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
        //        Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
        //        Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //        Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
        //        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
        //        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
        //        Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
        //        Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
        //        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
        //        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
        //        Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
        //        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
        //        Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
        //        Sm.CmParam<Decimal>(ref cm, "@Transport", r.Transport);
        //        Sm.CmParam<Decimal>(ref cm, "@Meal", r.Meal);
        //        Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
        //        Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
        //        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
        //        Sm.CmParam<Decimal>(ref cm, "@ADOT", r.ADOT);
        //    }

        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
        //    SQL.AppendLine("Where DocNo=@DocNo And Yr=@Yr And Mth=@Mth And EmpCode=@EmpCode; ");

        //    SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
        //    SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
        //    Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
        //    Sm.CmParam<String>(ref cm, "Yr", x.Yr);
        //    Sm.CmParam<String>(ref cm, "Mth", x.Mth);
        //    Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
        //    if (mIsPayrollAmtRounded)
        //        Sm.CmParam<Decimal>(ref cm, "Amt", decimal.Truncate(x.Amt));
        //    else
        //        Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #endregion

        #region Process Data

        private void ProcessData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI = new List<TI>();
            var lTI2 = new List<TI2>();
            
            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI(ref lTI);
            ProcessTI2(ref lTI2);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);
            
            #endregion

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r =>{Process4(ref r); });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI, ref lTI2); });

            #region Save Data

            var cml = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());

            cml.Add(SavePayrollProcess1(ref lResult2));
            //lResult2.ForEach(r => { cml.Add(SavePayrollProcess1(ref r)); });
            cml.Add(SavePayrollProcess2(ref lResult1));
            // lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });
            cml.Add(SavePayrollProcess());

            if (mlAdvancePaymentProcess.Count > 0)
                cml.Add(SaveAdvancePaymentProcess(ref mlAdvancePaymentProcess));
            // mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            Sm.ExecCommands(cml);

            #endregion

            LuePayrunCode.EditValue = null;
            ClearData();
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (Grd1.Rows.Count >= 1 && lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = Sm.GetLue(LuePayrunCode);
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var LevelCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty; 
                
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            LevelCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;

                            foreach (var i in 
                                lEmployeePPS
                                    .Where(Index => Index.EmpCode == EmpCodeTemp)
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, i.StartDt)>=0)
                                {
                                    DeptCodeTemp = i.DeptCode;
                                    GrdLvlCodeTemp = i.GrdLvlCode;
                                    LevelCodeTemp = i.LevelCode;
                                    EmploymentStatusTemp = i.EmploymentStatus;
                                    SystemTypeTemp = i.SystemType;
                                    PayrunPeriodTemp = i.PayrunPeriod;
                                    PGCodeTemp = i.PGCode;
                                    SiteCodeTemp = i.SiteCode;
                                    PosCodeTemp = i.PosCode;
                                    HOIndTemp = i.HOInd;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    LevelCode = LevelCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    SalaryPension = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    ServiceChargeIncentive = 0m,
                                    IncEmployee = 0m,
                                    ADOT = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });

                //lResult1.ForEach(A =>
                //{
                    
                //    A.LatestPaidDt = lEmpPaidDt.FirstOrDefault(
                //        x => A.EmpCode.Equals(x.EmpCode??"") && A.Dt.Equals(x.Dt??"")
                //        ).Dt??"";
                //});
                
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0) r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0) r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0) r.ProcessInd = false;
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalarySS = new List<EmployeeSalarySS>();
            ProcessEmployeeSalarySS(ref lEmployeeSalarySS);
            if (lEmployeeSalarySS.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var i in lEmployeeSalarySS
                        .Where(w => Sm.CompareStr(w.EmpCode, r.EmpCode))
                        .OrderByDescending(o => o.StartDt))
                    {
                        if (Sm.CompareDtTm(i.StartDt, r.Dt) <= 0)
                        {
                            r.SalaryPension = i.Amt;
                            break;
                        }
                    }
                }
                lEmployeeSalarySS.Clear();
            }

            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion

            #region Employee Allowance (Meal, Transport)

            ProcessEmployeeAllowance(ref mlEmployeeAllowanceMeal, mADCodeMeal);
            ProcessEmployeeAllowance(ref mlEmployeeAllowanceTransport, mADCodeTransport);

            #endregion


            #region Employee Allowance Deduction

            ProcessEmployeeAllowanceDeduction(ref mlEmployeeAllowanceDeduction);
            

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            var PayrunPeriodTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                string 
                    SystemTypeTemp = string.Empty, 
                    GrdLvlCodeTemp = string.Empty,
                    LevelCodeTemp = string.Empty;

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCodeTemp.Length != 0)
                    {
                        SystemTypeTemp = string.Empty;
                        PayrunPeriodTemp = string.Empty;
                        GrdLvlCodeTemp = string.Empty;
                        LevelCodeTemp = string.Empty;

                        foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                        {
                            x.Salary = x.EmpSalary2; // Berdasarkan Employee Salary
                            
                            if (x.ProcessInd)
                            {
                                if (!(
                                    Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                    Sm.CompareStr(x.SystemType, mSystemType) &&
                                    Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                    Sm.CompareStr(x.PGCode, mPGCode) &&
                                    Sm.CompareStr(x.SiteCode, mSiteCode)
                                    ))
                                    x.ProcessInd = false;
                                else
                                    LevelCodeTemp = x.LevelCode;
                                
                            }
                        }
                        foreach (var x in lResult1
                                .Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0)
                                .OrderBy(x => x.Dt))
                            x._LatestLevelCode = LevelCodeTemp;
                    }
                }
            }
        }

        private void Process4(ref Result1 r)
        {
            string
                ActualDtIn = string.Empty, 
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty, 
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode, 
                Dt = r.Dt;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                //int.Parse(r.WSIn1) > int.Parse(r.WSOut1))
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                {    
                    r.LateInd = true;
                }
            }

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            //string ROT1 = string.Empty, ROT2 = string.Empty;

            //#region Set OT Rutin (In)

            //if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            //{
            //    if (r.OneDayInd)
            //    {
            //        //Shift 1, 2
            //        //if (mIsWorkingHrBasedOnSchedule)
            //        //{
            //        //    ROT1 = r.Dt + r.WSIn3;
            //        //}
            //        //else
            //        //{
            //        if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
            //        {
            //            //1500 < 1600
            //            ROT1 = ActualDtIn + ActualTmIn;
            //        }
            //        else
            //        {
            //            //1500 > 1400
            //            ROT1 = r.Dt + r.WSIn3;
            //        }
            //        //}
            //    }
            //    else
            //    {
            //        //Shift 3
            //        //if (mIsWorkingHrBasedOnSchedule)
            //        //{
            //        //    ROT1 = Dt2 + r.WSIn3;
            //        //}
            //        //else
            //        //{
            //        if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
            //        {
            //            //0400<0500
            //            ROT1 = ActualDtIn + ActualTmIn;
            //        }
            //        else
            //        {
            //            //0400>0300
            //            ROT1 = Dt2 + r.WSIn3;
            //        }
            //        //}
            //    }
            //}

            //#endregion

            //#region Set OT Rutin (Out)

            //if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            //{
            //    if (r.OneDayInd)
            //    {
            //        ////Shift 1, 2
            //        //if (mIsWorkingHrBasedOnSchedule)
            //        //{
            //        //    ROT2 = r.Dt + r.WSOut3;
            //        //}
            //        //else
            //        //{
            //            if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
            //            {
            //                //1900<2000
            //                ROT2 = r.Dt + r.WSOut3;
            //            }
            //            else
            //            {
            //                //1900>1800
            //                ROT2 = ActualDtOut + ActualTmOut;
            //            }
            //        //}
            //    }
            //    else
            //    {
            //        //Shift 3
            //        //if (mIsWorkingHrBasedOnSchedule)
            //        //{
            //        //    ROT2 = Dt2 + r.WSOut3;
            //        //}
            //        //else
            //        //{
            //        if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
            //        {
            //            //0300<0400
            //            ROT2 = Dt2 + r.WSOut3;
            //        }
            //        else
            //        {
            //            //0300>0200
            //            ROT2 = ActualDtOut + ActualTmOut;
            //        }
            //        //}
            //    }
            //}

            //#endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            //if (ROT1.Length > 0 && ROT2.Length > 0)
            //{
            //    ROTDuration = ComputeDuration(ROT2, ROT1);
            //    if (ROTDuration < 0) ROTDuration = 0m;
            //    if (mIsOTRoutineRounding && ROTDuration > 0)
            //    {
            //        ROTDuration = RoundTo1Hour((double)ROTDuration);
            //    }
            //}

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType.Length>0)
                {
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn);
                }
                else
                {
                    //No Leave
                    WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                    if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                    {
                        // Menghitung lama waktu istirahat
                        if (r.OneDayInd)
                        {
                            // Shift 1, 2
                            if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                            //if (decimal.Parse(r.WSIn2) <= decimal.Parse(r.WSOut2))
                                BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            else
                                BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                        }
                    }
                    WorkingDuration -= BreakDuration;  
                }
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion

            #region Menghitung OT

            //decimal TotalOTHr = 0m;

            //if (r.SystemType == mEmpSystemTypeHarian)
            //{
            //    decimal OTHr = 0m;
            //    string 
            //        StartTm = string.Empty, 
            //        EndTm = string.Empty;

            //    if (mOTRequestType=="2")
            //    {
            //        #region Use OT Request

            //        TotalOTHr += ROTDuration;
            //        foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
            //        {
            //            OTHr = 0m;
            //            StartTm = x.Tm1;
            //            EndTm = x.Tm2;

            //            if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
            //            {
            //                if (EndTm == "0000")
            //                    OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
            //                else
            //                {
            //                    if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
            //                        OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
            //                    else
            //                        OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
            //                }
            //            }
            //            if (OTHr < 0) OTHr = 0;
            //            TotalOTHr += OTHr;
            //        }

            //        #endregion
            //    }
            //    else
            //    {
            //        bool
            //            IsOTAdjusted = false,
            //            IsOTSpecial = false; // beda tanggal

            //        string
            //            StartTmTemp = string.Empty,
            //            EndTmTemp = string.Empty,
            //            OTDtTemp = r.OneDayInd ? Dt : Dt2,
            //            SpecialStartDt = string.Empty,
            //            SpecialStartTm = string.Empty,
            //            SpecialEndDt = string.Empty,
            //            SpecialEndTm = string.Empty
            //            ;

            //        foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
            //        {
            //            IsOTAdjusted = false;
            //            OTHr = 0m;
            //            StartTm = x.Tm1;
            //            EndTm = x.Tm2;

            //            #region Use OT Adjustment

            //            foreach (var ota in mlOTAdjustment.Where(
            //                ota =>
            //                    ota.OTDocNo == x.OTDocNo &&
            //                    ota.EmpCode == EmpCode &&
            //                    ota.Dt == Dt
            //                    ))
            //            {
            //                IsOTAdjusted = true;
            //                StartTm = ota.Tm1;
            //                EndTm = ota.Tm2;
            //            }

            //            #endregion

            //            #region Use Attendance Barcode

            //            if (!IsOTAdjusted)
            //            {
            //                StartTmTemp = string.Empty;
            //                EndTmTemp = string.Empty;

            //                //Value1<Value2 : -1
            //                //Value1=Value2 : 0
            //                //Value1>Value2 : 1
            //                var IsOTOneDayInd = true;
            //                var Tm1Temp = string.Empty;

            //                if (r.OneDayInd)
            //                {
            //                    #region Shift 1 & 2

            //                    if (Sm.CompareDtTm(StartTm, EndTm) > 0)
            //                    {
            //                        #region Case 1

            //                        IsOTSpecial = true;

            //                        foreach (var index in mlAttendanceLog
            //                                        .Where(
            //                                            i =>
            //                                                i.EmpCode == EmpCode &&
            //                                                i.Dt == Dt &&
            //                                                Sm.CompareDtTm(Dt + "0000", (i.Dt + i.Tm)) <= 0
            //                                            )
            //                                        .OrderBy(o1 => o1.Dt)
            //                                        .ThenBy(o2 => o2.Tm)
            //                                        .Take(1)
            //                                        )
            //                        {
            //                            SpecialStartDt = index.Dt;
            //                            SpecialStartTm = index.Tm;
            //                        }

            //                        if (
            //                            SpecialStartDt.Length > 0 &&
            //                            SpecialStartTm.Length > 0 &&
            //                            Sm.CompareDtTm(SpecialStartDt + SpecialStartTm, Dt + StartTm) <= 0
            //                            )
            //                        {
            //                            SpecialStartDt = Dt;
            //                            SpecialStartTm = StartTm;
            //                        }

            //                        if (SpecialStartDt.Length > 0 && SpecialStartTm.Length > 0)
            //                        {
            //                            foreach (var index in mlAttendanceLog
            //                                   .Where(
            //                                       i =>
            //                                           i.EmpCode == EmpCode &&
            //                                           (i.Dt == Dt || i.Dt == Dt2) &&
            //                                           Sm.CompareDtTm(Dt + StartTm, (i.Dt + i.Tm)) < 0 &&
            //                                           Sm.CompareDtTm((i.Dt + i.Tm), Dt2 + EndTm) <= 0
            //                                       )
            //                                   .OrderByDescending(o1 => o1.Dt)
            //                                   .ThenByDescending(o2 => o2.Tm)
            //                                   .Take(1)
            //                                   )
            //                            {
            //                                SpecialEndDt = index.Dt;
            //                                SpecialEndTm = index.Tm;
            //                            }


            //                            if (SpecialEndDt.Length <= 0 || SpecialEndTm.Length <= 0)
            //                            {
            //                                foreach (var index in mlAttendanceLog
            //                                       .Where(
            //                                           i =>
            //                                               i.EmpCode == EmpCode &&
            //                                               i.Dt == Dt2 &&
            //                                               Sm.CompareDtTm(Dt2 + EndTm, i.Dt + i.Tm) <= 0
            //                                           )
            //                                       .OrderBy(o1 => o1.Dt)
            //                                       .ThenBy(o2 => o2.Tm)
            //                                       .Take(1)
            //                                       )
            //                                {
            //                                    SpecialEndDt = index.Dt;
            //                                    SpecialEndTm = index.Tm;
            //                                }

            //                                if (
            //                                    SpecialEndDt.Length > 0 &&
            //                                    SpecialEndTm.Length > 0 &&
            //                                    Sm.CompareDtTm(Dt2 + EndTm, SpecialEndDt + SpecialEndTm) <= 0
            //                                )
            //                                {
            //                                    SpecialEndDt = Dt2;
            //                                    SpecialEndTm = EndTm;
            //                                }
            //                            }
            //                        }
            //                        StartTmTemp = SpecialStartTm;
            //                        EndTmTemp = SpecialEndTm;

            //                        #endregion
            //                    }

            //                    if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
            //                    {
            //                        #region case 2
            //                        // Standard

            //                        foreach (var index in mlAttendanceLog
            //                        .Where(
            //                            i =>
            //                                i.EmpCode == EmpCode &&
            //                                i.Dt == Dt &&
            //                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
            //                            )
            //                        .OrderByDescending(o => o.Tm)
            //                        .Take(1)
            //                        )
            //                        {
            //                            StartTmTemp = Sm.Left(index.Tm, 4);
            //                        }

            //                        if (StartTmTemp.Length > 0)
            //                        {
            //                            if (x.Tm2 == "0000")
            //                            {
            //                                foreach (var index in mlAttendanceLog
            //                                .Where(
            //                                    i =>
            //                                        i.EmpCode == EmpCode &&
            //                                        i.Dt == Dt &&
            //                                        Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
            //                                        Sm.CompareDtTm(i.Tm, "2359") <= 0
            //                                    )
            //                                .OrderByDescending(o => o.Tm)
            //                                .Take(1)
            //                                )
            //                                {
            //                                    EndTmTemp = Sm.Left(index.Tm, 4);
            //                                }

            //                                if (EndTmTemp.Length <= 0)
            //                                {
            //                                    foreach (var index in mlAttendanceLog
            //                                        .Where(
            //                                            i =>
            //                                                i.EmpCode == EmpCode &&
            //                                                i.Dt == Dt2 &&
            //                                                Sm.CompareDtTm(i.Tm, x.Tm2) > 0
            //                                            )
            //                                        .OrderBy(o => o.Tm)
            //                                        .Take(1)
            //                                        )
            //                                    {
            //                                        EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
            //                                    }
            //                                }
            //                            }
            //                            else
            //                            {
            //                                foreach (var index in mlAttendanceLog
            //                                .Where(
            //                                    i =>
            //                                        i.EmpCode == EmpCode &&
            //                                        i.Dt == Dt &&
            //                                        Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
            //                                        Sm.CompareDtTm(i.Tm, x.Tm2) <= 0
            //                                    )
            //                                .OrderByDescending(o => o.Tm)
            //                                .Take(1)
            //                                )
            //                                {
            //                                    EndTmTemp = Sm.Left(index.Tm, 4);
            //                                }

            //                                //if (EndTmTemp.Length <= 0)
            //                                //{
            //                                    foreach (var index in mlAttendanceLog
            //                                        .Where(
            //                                            i =>
            //                                                i.EmpCode == EmpCode &&
            //                                                i.Dt == Dt &&
            //                                                Sm.CompareDtTm(i.Tm, x.Tm2) > 0
            //                                            )
            //                                        .OrderBy(o => o.Tm)
            //                                        .Take(1)
            //                                        )
            //                                    {
            //                                        EndTmTemp = Sm.Left(index.Tm, 4);
            //                                    }
            //                                //}
            //                                if (EndTmTemp.Length <= 0)
            //                                {
            //                                    EndTmTemp = Sm.Left(x.Tm2, 4);
            //                                }
            //                            }
            //                        }

            //                        #endregion
            //                    }
            //                    #endregion
            //                }
            //                else
            //                {
            //                    #region Shift 3
            //                    if (Sm.CompareDtTm(r.WSOut1, x.Tm1) <= 0)
            //                    {
            //                        if (Sm.CompareDtTm(r.WSOut1, ActualTmOut) <= 0)
            //                            StartTmTemp = r.WSOut1;
            //                        else
            //                        {
            //                            foreach (var index in mlAttendanceLog
            //                            .Where(
            //                                i =>
            //                                    i.EmpCode == EmpCode &&
            //                                    i.Dt == OTDtTemp &&
            //                                    Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
            //                                )
            //                            .OrderByDescending(o => o.Tm)
            //                            .Take(1)
            //                            )
            //                            {
            //                                StartTmTemp = Sm.Left(index.Tm, 4);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        foreach (var index in mlAttendanceLog
            //                            .Where(
            //                                i =>
            //                                    i.EmpCode == EmpCode &&
            //                                    i.Dt == OTDtTemp &&
            //                                    Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
            //                                )
            //                            .OrderByDescending(o => o.Tm)
            //                            .Take(1)
            //                            )
            //                        {
            //                            StartTmTemp = Sm.Left(index.Tm, 4);
            //                        }
            //                    }

            //                    if (StartTmTemp.Length > 0)
            //                    {
            //                        if (x.Tm2 == "0000")
            //                        {
            //                            foreach (var index in mlAttendanceLog
            //                            .Where(
            //                                i =>
            //                                    i.EmpCode == EmpCode &&
            //                                    Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + "2359") <= 0 &&
            //                                    Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

            //                                )
            //                            .OrderByDescending(o => o.Tm)
            //                            .Take(1)
            //                            )
            //                            {
            //                                EndTmTemp = Sm.Left(index.Tm, 4);
            //                            }

            //                            if (EndTmTemp.Length <= 0)
            //                            {
            //                                var OTDtTemp2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
            //                                foreach (var index in mlAttendanceLog
            //                                    .Where(
            //                                        i =>
            //                                            i.EmpCode == EmpCode &&
            //                                            i.Dt == OTDtTemp2 &&
            //                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
            //                                        )
            //                                    .OrderBy(o => o.Tm)
            //                                    .Take(1)
            //                                    )
            //                                {
            //                                    EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
            //                                }
            //                            }
            //                        }
            //                        else
            //                        {
            //                            foreach (var index in mlAttendanceLog
            //                            .Where(
            //                                i =>
            //                                    i.EmpCode == EmpCode &&
            //                                    Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm2) <= 0 &&
            //                                    Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

            //                                )
            //                            .OrderByDescending(o => o.Tm)
            //                            .Take(1)
            //                            )
            //                            {
            //                                EndTmTemp = Sm.Left(index.Tm, 4);
            //                            }

            //                            if (EndTmTemp.Length <= 0)
            //                            {
            //                                foreach (var index in mlAttendanceLog
            //                                    .Where(
            //                                        i =>
            //                                            i.EmpCode == EmpCode &&
            //                                            i.Dt == OTDtTemp &&
            //                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
            //                                        )
            //                                    .OrderBy(o => o.Tm)
            //                                    .Take(1)
            //                                    )
            //                                {
            //                                    EndTmTemp = Sm.Left(index.Tm, 4);
            //                                }
            //                            }
            //                        }
            //                    }
            //                    #endregion
            //                }

            //                if (StartTmTemp.Length <= 0 || EndTmTemp.Length <= 0)
            //                {
            //                    StartTm = string.Empty;
            //                    EndTm = string.Empty;
            //                }
            //                else
            //                {
            //                    if (!IsOTSpecial)
            //                    {
            //                        StartTm = x.Tm1;
            //                        if (x.Tm2 == "0000")
            //                        {
            //                            if (Sm.CompareDtTm(EndTmTemp, x.Tm2) != 0)
            //                                EndTm = EndTmTemp;
            //                            else
            //                                EndTm = x.Tm2;
            //                        }
            //                        else
            //                        {
            //                            if (Sm.CompareDtTm(EndTmTemp, x.Tm2) < 0)
            //                                EndTm = EndTmTemp;
            //                            else
            //                                EndTm = x.Tm2;
            //                        }
            //                    }
            //                }
            //            }

            //            #endregion

            //            if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
            //            {
            //                //Value1<Value2 : -1
            //                //Value1=Value2 : 0
            //                //Value1>Value2 : 1
            //                if (IsOTSpecial)
            //                {
            //                    OTHr = ComputeDuration(SpecialEndDt + SpecialEndTm, SpecialStartDt + SpecialStartTm);
            //                }
            //                else
            //                {
            //                    if (EndTm == "0000")
            //                    {
            //                        if (r.OneDayInd)
            //                            OTHr = ComputeDuration(Dt2 + EndTm, OTDtTemp + StartTm);
            //                        else
            //                        {
            //                            var Dt3 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
            //                            OTHr = ComputeDuration(Dt3 + EndTm, OTDtTemp + StartTm);
            //                        }
            //                    }
            //                    else
            //                        OTHr = ComputeDuration(OTDtTemp + EndTm, OTDtTemp + StartTm);
            //                }
            //            }
            //            if (OTHr < 0) OTHr = 0;
            //            TotalOTHr += OTHr;
            //        }
            //    }
            //}

            //if (mSalaryInd == "2")
            //{
            //    if (mIsOTHolidayDeduct1Hr && (r.HolInd || r.WSHolidayInd))
            //        if (TotalOTHr>=4) TotalOTHr = TotalOTHr-1;
            //}

            #endregion

            #region OT menggantikan leave

            //if (mSalaryInd=="1" && mIsUseOTReplaceLeave)
            //if (mIsUseOTReplaceLeave)
            //{
            //    var NonROTDuration = RoundTo30Minutes((double)(TotalOTHr));
            //    TotalOTHr = RoundTo30Minutes((double)(TotalOTHr + ROTDuration));
            //    if (TotalOTHr > 0m && r.LeaveDuration > 0m)
            //    {
            //        r.OTToLeaveInd = true;
            //        if (r.LeaveType != "F")
            //        {
            //            r.WorkingDuration = r.WorkingDuration + r.LeaveDuration;
            //            if (r.LeaveDuration >= NonROTDuration)
            //            {
            //                r.LeaveDuration -= NonROTDuration;
            //                NonROTDuration = 0m;
            //            }
            //            else
            //            {
            //                NonROTDuration -= r.LeaveDuration;
            //                r.LeaveDuration = 0;
            //            }
            //            r.WorkingDuration = r.WorkingDuration - r.LeaveDuration;

            //            if (r.WSOut3.Length > 0)
            //            {
            //                if (Sm.CompareDtTm(r.WSIn3, r.WSOut3) <= 0)
            //                    ROTDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn3);
            //                else
            //                    ROTDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn3);
            //            }
            //            else
            //                ROTDuration = 0m;
                        
            //            if (r.LeaveDuration >= ROTDuration)
            //            {
            //                r.LeaveDuration -= ROTDuration;
            //                ROTDuration = 0m;
            //            }
            //            else
            //            {
            //                ROTDuration -= r.LeaveDuration;
            //                r.LeaveDuration = 0;
            //            }
            //            TotalOTHr = RoundTo30Minutes((double)(NonROTDuration + ROTDuration));
            //        }
            //    }
            //}

            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length>0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (
                        r.LeaveDuration != 0m &&
                        mWorkingHr != 0m &&
                        !(r.SystemType == mEmpSystemTypeBorongan && r.LeaveCode == mOperatorLeaveCode)
                    )
                {
                    //if (r.SystemType == mEmpSystemTypeBorongan)
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.PLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.PLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.PLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.UPLDay = 1;
                }
                else
                    r.UPLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m) 
                {
                    if (mSalaryInd == "1")
                    {
                        if (r.SystemType == mEmpSystemTypeBorongan)
                        {
                            if (r.LeaveCode == mCoordinatorLeaveCode && r.LeaveType != "F")
                                r.UPLAmt = r.ProductionWages * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                        else
                        {
                            if (r._IsUseLatestGrdLvlSalary)
                                r.UPLAmt = r._LatestDailyGrdLvlSalary * (r.LeaveDuration / mWorkingHr);
                            else
                                r.UPLAmt = r.BasicSalary2 * (r.LeaveDuration / mWorkingHr);
                        }
                    }
                    if (mSalaryInd == "2")
                        r.UPLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r._DeductTHPInd)
                    r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            #region Menghitung OT Amount

            //if (r.SystemType == mEmpSystemTypeHarian && r.EmpSalary != 0m && TotalOTHr >= 0.5m)
            //{
            //    decimal TotalOTHrTemp = TotalOTHr;
            //    if (r.HolInd || r.WSHolidayInd)
            //    {
            //        r.OTHolidayHr = TotalOTHrTemp;

            //        int i = 1;
                   
            //        while (TotalOTHrTemp > 0)
            //        {
            //            if (i == 1)
            //            {
            //                if (TotalOTHrTemp >= mHoliday1stHrForHO)
            //                {
            //                    r.OTHolidayAmt = mHoliday1stHrForHO * mHoliday1stHrIndex * (r.EmpSalary / 173);
            //                    TotalOTHrTemp -= mHoliday1stHrForHO;
            //                }
            //                else
            //                {
            //                    r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * (r.EmpSalary / 173);
            //                    TotalOTHrTemp = 0m;
            //                }
            //            }
            //            if (i == 2)
            //            {
            //                if (TotalOTHrTemp >= mHoliday2ndHrForHO)
            //                {
            //                    r.OTHolidayAmt += (mHoliday2ndHrForHO * mHoliday2ndHrIndex * (r.EmpSalary / 173));
            //                    TotalOTHrTemp -= mHoliday2ndHrForHO;
            //                }
            //                else
            //                {
            //                    r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * (r.EmpSalary / 173));
            //                    TotalOTHrTemp = 0m;
            //                }
            //            }
            //            if (i == 3)
            //            {
            //                r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * (r.EmpSalary / 173));
            //                TotalOTHrTemp = 0m;
            //            }
            //            i++;
            //        }
               
            //        while (TotalOTHrTemp > 0)
            //        {
            //            if (i == 1)
            //            {
            //                if (TotalOTHrTemp >= mHoliday1stHrForSite)
            //                {
            //                    r.OTHolidayAmt = mHoliday1stHrForSite * mHoliday1stHrIndex * (r.EmpSalary / 173);
            //                    TotalOTHrTemp -= mHoliday1stHrForSite;
            //                }
            //                else
            //                {
            //                    r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * (r.EmpSalary / 173);
            //                    TotalOTHrTemp = 0m;
            //                }
            //            }
            //            if (i == 2)
            //            {
            //                if (TotalOTHrTemp >= mHoliday2ndHrForSite)
            //                {
            //                    r.OTHolidayAmt += (mHoliday2ndHrForSite * mHoliday2ndHrIndex * (r.EmpSalary / 173));
            //                    TotalOTHrTemp -= mHoliday2ndHrForSite;
            //                }
            //                else
            //                {
            //                    r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * (r.EmpSalary / 173));
            //                    TotalOTHrTemp = 0m;
            //                }
            //            }
            //            if (i == 3)
            //            {
            //                r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * (r.EmpSalary / 173));
            //                TotalOTHrTemp = 0m;
            //            }
            //            i++;
            //        }
            //    }
            //    else
            //    {
            //        if (TotalOTHrTemp <= mOTFormulaHr1)
            //        {
            //            r.OT1Hr = TotalOTHrTemp;
            //            r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * (r.EmpSalary / 173);
            //            TotalOTHrTemp = 0m;
            //        }
            //        else
            //        {
            //            r.OT1Hr = mOTFormulaHr1;
            //            r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * (r.EmpSalary / 173);
            //            TotalOTHrTemp -= mOTFormulaHr1;
            //        }

            //        if (TotalOTHrTemp>0m && TotalOTHrTemp <= mOTFormulaHr2)
            //        {
            //            r.OT2Hr = TotalOTHrTemp;
            //            r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * (r.EmpSalary / 173);
            //            TotalOTHrTemp = 0m;
            //        }
            //    }
            //}

            #endregion

            //#region Compute Transport/Meal
  
            //if ((r.LeaveCode.Length==0 && r.WorkingDuration > 0m))
            //{
            //    foreach (var i in mlEmployeeAllowanceMeal.Where(w =>Sm.CompareStr(w.EmpCode, EmpCode)))
            //        r.Meal = i.Amt;

            //    foreach (var i in mlEmployeeAllowanceTransport.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            //        r.Transport = i.Amt;
            //}

            //#endregion

            #region Unprocessed data

            if (!r.ProcessInd)
            {
                r._LatestMonthlyGrdLvlSalary = 0m;
                r._LatestDailyGrdLvlSalary = 0m;
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                //r.EmpSalary = 0m;
                //r.EmpSalary2 = 0m;
                //r.SalaryPension = 0m;
                //r.BasicSalary = 0m;
                //if (mSalaryInd == "2") r.BasicSalary2 = 0m;
                //r.ProductionWages = 0m;
                //r.Salary = 0m;
                r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr = 0m;
                r.OT2Hr = 0m;
                r.OTHolidayHr = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt = 0m;
                r.OTHolidayAmt = 0m;
                r.OTToLeaveInd = false;
                r.IncMinWages = 0m;
                r.IncProduction = 0m;
                r.IncPerformance = 0m;
                r.HolidayEarning = 0m;
                r.ExtraFooding = 0m;
                r.FieldAssignment = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                r.ServiceChargeIncentive = 0m;
                r.DedProduction = 0m;
                r.DedProdLeave = 0m;
                r.IncEmployee = 0m;
                r.ADOT = 0m;
            }
            
            #endregion
        }

        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = Sm.GetLue(LuePayrunCode),
                Filter = string.Empty,
                EmpCode = string.Empty;

            var cm = new MySqlCommand();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length>0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " (" + Filter + ") ";
            else
                Filter = " (0=1)";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, ");
            if (mIsPayrollAmtRounded)
            {
                SQL.AppendLine("Floor(IfNull(B.SalaryAdjustment, 0.00)) As SalaryAdjustment, ");
                SQL.AppendLine("Floor(IfNull(C.SSEmployerHealth, 0.00)) As SSEmployerHealth, ");
                SQL.AppendLine("Floor(IfNull(C.SSEmployeeHealth, 0.00)) As SSEmployeeHealth, ");
                SQL.AppendLine("Floor(IfNull(D.SSEmployerEmployment, 0.00)) As SSEmployerEmployment, ");
                SQL.AppendLine("Floor(IfNull(D.SSEmployeeEmployment, 0.00)) As SSEmployeeEmployment, ");
                SQL.AppendLine("Floor(IfNull(E.SSEmployerPension, 0.00)) As SSEmployerPension, ");
                SQL.AppendLine("Floor(IfNull(E.SSEmployeePension, 0.00)) As SSEmployeePension, ");
                SQL.AppendLine("Floor(IfNull(F.SSErLifeInsurance, 0.00)) As SSErLifeInsurance, ");
                SQL.AppendLine("Floor(IfNull(F.SSEeLifeInsurance, 0.00)) As SSEeLifeInsurance, ");
                SQL.AppendLine("Floor(IfNull(G.SSErWorkingAccident, 0.00)) As SSErWorkingAccident, ");
                SQL.AppendLine("Floor(IfNull(G.SSEeWorkingAccident, 0.00)) As SSEeWorkingAccident, ");
                SQL.AppendLine("Floor(IfNull(H.SSErRetirement, 0.00)) As SSErRetirement, ");
                SQL.AppendLine("Floor(IfNull(H.SSEeRetirement, 0.00)) As SSEeRetirement, ");
                SQL.AppendLine("Floor(IfNull(I.SSErPension, 0.00)) As SSErPension, ");
                SQL.AppendLine("Floor(IfNull(I.SSEePension, 0.00)) As SSEePension, ");
                SQL.AppendLine("Floor(IfNull(J.Amt, 0.00)) As Functional, ");
                SQL.AppendLine("Floor(IfNull(K.Value, 0.00)) As PerformanceValue, ");
                SQL.AppendLine("Floor(IfNull(L.Amt, 0.00)) As FixAllowance, ");
                SQL.AppendLine("Floor(IfNull(M.Amt, 0.00)) As FixDeduction, ");
                SQL.AppendLine("Floor(IfNull(O.TaxableFixedAllowance, 0.00)) As TaxableFixedAllowance ");
            }
            else
            {
                SQL.AppendLine("IfNull(B.SalaryAdjustment, 0.00) As SalaryAdjustment, ");
                SQL.AppendLine("IfNull(C.SSEmployerHealth, 0.00) As SSEmployerHealth, ");
                SQL.AppendLine("IfNull(C.SSEmployeeHealth, 0.00) As SSEmployeeHealth, ");
                SQL.AppendLine("IfNull(D.SSEmployerEmployment, 0.00) As SSEmployerEmployment, ");
                SQL.AppendLine("IfNull(D.SSEmployeeEmployment, 0.00) As SSEmployeeEmployment, ");
                SQL.AppendLine("IfNull(E.SSEmployerPension, 0.00) As SSEmployerPension, ");
                SQL.AppendLine("IfNull(E.SSEmployeePension, 0.00) As SSEmployeePension, ");
                SQL.AppendLine("IfNull(F.SSErLifeInsurance, 0.00) As SSErLifeInsurance, ");
                SQL.AppendLine("IfNull(F.SSEeLifeInsurance, 0.00) As SSEeLifeInsurance, ");
                SQL.AppendLine("IfNull(G.SSErWorkingAccident, 0.00) As SSErWorkingAccident, ");
                SQL.AppendLine("IfNull(G.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
                SQL.AppendLine("IfNull(H.SSErRetirement, 0.00) As SSErRetirement, ");
                SQL.AppendLine("IfNull(H.SSEeRetirement, 0.00) As SSEeRetirement, ");
                SQL.AppendLine("IfNull(I.SSErPension, 0.00) As SSErPension, ");
                SQL.AppendLine("IfNull(I.SSEePension, 0.00) As SSEePension, ");
                SQL.AppendLine("IfNull(J.Amt, 0.00) As Functional, ");
                SQL.AppendLine("IfNull(K.Value, 0.00) As PerformanceValue, ");
                SQL.AppendLine("IfNull(L.Amt, 0.00) As FixAllowance, ");
                SQL.AppendLine("IfNull(M.Amt, 0.00) As FixDeduction, ");
                SQL.AppendLine("IfNull(O.TaxableFixedAllowance, 0.00) As TaxableFixedAllowance ");
            }
            SQL.AppendLine("From TblEmployee A ");

            //Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr T1 ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And (T1.PayrunCode Is Null Or (T1.PayrunCode Is Not Null And T1.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");

            //SS Health
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForHealth' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");

            //SS Employment (dikurangi BPJS Pensiun)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        And T2.SSCode Not In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForEmployment' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");

            //Pension 1 (Pensiun JiwaSraya)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerPension, Sum(T2.EmployeeAmt) As SSEmployeePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSCodePension' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");


            //BPJS Jaminan Kematian
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErLifeInsurance, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeLifeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSLifeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");

            //BPJS Jaminan Kecelakaan Kerja
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErWorkingAccident, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeWorkingAccident ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSWorkingAccident' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            //BPJS Jaminan Hari Tua
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErRetirement, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeRetirement ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSOldAgeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");

            //BPJS Jaminan Pensiun
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");

            //Functional
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeFunctional' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");

            //Grade Performance
            SQL.AppendLine("Left Join TblEmployeePerformance K On A.EmpCode=K.EmpCode ");

            //Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");

            //Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");


            //Taxable Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");

            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", 
                    "ResignDt", 
                    "NPWP", 
                    "PTKP", 
                    "SalaryAdjustment", 

                    //6-10
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 
                    "SSEmployeeEmployment",
                    "SSEmployerPension",
                    
                    //11-15
                    "SSEmployeePension",
                    "SSErLifeInsurance",
                    "SSEeLifeInsurance",
                    "SSErWorkingAccident",
                    "SSEeWorkingAccident",
                    
                    //16-20
                    "SSErRetirement",
                    "SSEeRetirement",
                    "SSErPension",
                    "SSEePension",
                    "Functional",
                    
                    //21-24
                    "PerformanceValue",
                    "FixAllowance",
                    "FixDeduction",
                    "TaxableFixedAllowance"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            SalaryAdjustment = Sm.DrDec(dr, c[5]),
                            SSEmployerHealth = Sm.DrDec(dr, c[6]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[9]),
                            SSEmployerPension = Sm.DrDec(dr, c[10]),
                            SSEmployeePension = Sm.DrDec(dr, c[11]),
                            SSErLifeInsurance= Sm.DrDec(dr, c[12]),
                            SSEeLifeInsurance= Sm.DrDec(dr, c[13]),
                            SSErWorkingAccident= Sm.DrDec(dr, c[14]),
                            SSEeWorkingAccident= Sm.DrDec(dr, c[15]),
                            SSErRetirement= Sm.DrDec(dr, c[16]),
                            SSEeRetirement = Sm.DrDec(dr, c[17]),
                            SSErPension = Sm.DrDec(dr, c[18]),
                            SSEePension = Sm.DrDec(dr, c[19]),
                            Functional = Sm.DrDec(dr, c[20]),
                            PerformanceValue = Sm.DrDec(dr, c[21]),
                            FixAllowance = Sm.DrDec(dr, c[22]),
                            FixDeduction = Sm.DrDec(dr, c[23]),
                            TaxableFixAllowance = Sm.DrDec(dr, c[24]),
                            IncPerformance = 0m,
                            DedEmployee = 0m,
                            NonTaxableFixAllowance = 0m,
                            NonTaxableFixDeduction = 0m,
                            TaxableFixDeduction = 0m,
                            EmploymentPeriodAllowance = 0m,
                            SSEmployerPension2 = 0m,
                            SSEmployeePension2 = 0m,
                            Housing = 0m,
                            MobileCredit = 0m,
                            Zakat = 0m,
                            PerformanceStatus = string.Empty,
                            GradePerformance = string.Empty,
                            DedProduction = 0m,
                            IncProduction = 0m,
                            SSEmployerNonRetiring = 0m,
                            SSEmployeeOldAgeInsurance = 0m,
                            Salary = 0m,
                            WorkingDay = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            IncMinWages = 0m,
                            PresenceReward = 0m,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            FieldAssignment = 0m,
                            DedProdLeave = 0m,
                            IncEmployee = 0m,
                            ServiceChargeIncentive = 0m,
                            SalaryPension = 0m,
                            CreditCode1 = string.Empty,
                            CreditCode2 = string.Empty,
                            CreditCode3 = string.Empty,
                            CreditCode4 = string.Empty,
                            CreditCode5 = string.Empty,
                            CreditCode6 = string.Empty,
                            CreditCode7 = string.Empty,
                            CreditCode8 = string.Empty,
                            CreditCode9 = string.Empty,
                            CreditCode10 = string.Empty,
                            CreditAdvancePayment1 = 0m,
                            CreditAdvancePayment2 = 0m,
                            CreditAdvancePayment3 = 0m,
                            CreditAdvancePayment4 = 0m,
                            CreditAdvancePayment5 = 0m,
                            CreditAdvancePayment6 = 0m,
                            CreditAdvancePayment7 = 0m,
                            CreditAdvancePayment8 = 0m,
                            CreditAdvancePayment9 = 0m,
                            CreditAdvancePayment10 = 0m,
                            ADOT = 0m,
                            Tax = 0m,
                            TaxAllowance = 0m,
                            Amt = 0m,
                            _RecomputedValueForFixedAD = 1m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            decimal
                    Tax = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m,
                    JoinYr = 0m;

            ComputeTaxAllowance(ref r, ref lNTI, ref lTI2);

            string NTI = r.PTKP;

            bool IsFullYr = true;

            var Penambah =
                r.ProcessPLAmt -
                r.ProcessUPLAmt +
                //r.OT1Amt +
                //r.OT2Amt +
                //r.OTHolidayAmt +
                r.SalaryAdjustment +
                r.PerformanceValue +
                //r.ADOT +
                r.TaxableFixAllowance +
                r.SSEmployerHealth +
                r.SSEmployerEmployment +
                r.SSErPension +
                r.SSEmployerPension +
                r.Meal +
                r.Transport;

            var Pengurang = r.SSEmployeeHealth + r.SSEmployeeEmployment + r.SSEePension + r.SSEmployeePension;

            Amt = r.Salary + Penambah;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Tax = Amt - FunctionalExpenses - Pengurang;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Tax = Tax * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Tax = Tax * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Tax > NTIAmt)
                Tax -= NTIAmt;
            else
                Tax = 0m;

            Tax = Tax % 1000 >= 500 ? Tax + 1000 - Tax % 1000 : Tax - Tax % 1000;

            if (Tax > 0 && lTI.Count > 0)
            {
                decimal TaxTemp = Tax, Amt2Temp = 0m;
                Tax = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            Tax += (TaxTemp * i.TaxRate / 100);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            Tax += ((i.Amt2 - Amt2Temp) * i.TaxRate / 100);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            Tax = Tax / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            if (Tax <= 0) Tax = 0m;
            r.Tax = decimal.Truncate(Tax);
            //r.TaxAllowance = r.Tax;
        }

        private void ComputeTaxAllowance(ref Result2 r, ref List<NTI> lNTI, ref List<TI2> lTI2)
        {
            decimal
                    Value = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m,
                    JoinYr = 0m;

            string NTI = r.PTKP;

            bool IsFullYr = true;

            var Penambah =
                r.ProcessPLAmt -
                r.ProcessUPLAmt +
                //r.OT1Amt +
                //r.OT2Amt +
                //r.OTHolidayAmt +
                r.SalaryAdjustment +
                r.PerformanceValue +
                //r.ADOT +
                r.TaxableFixAllowance +
                r.SSEmployerHealth +
                r.SSEmployerEmployment +
                r.SSErPension +
                r.SSEmployerPension +
                r.Meal +
                r.Transport;


            var Pengurang = r.SSEmployeeHealth + r.SSEmployeeEmployment + r.SSEePension + r.SSEmployeePension;

            Amt = r.Salary + Penambah;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Value = Amt - FunctionalExpenses - Pengurang;

            if (r.JoinDt.Length >= 4)
                JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
                Value = Value * 12m;
            else
            {
                if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
                {
                    Value = Value * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
                    IsFullYr = false;
                }
            }

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Value > NTIAmt)
                Value -= NTIAmt;
            else
                Value = 0m;

            Value = Value % 1000 >= 500 ? Value + 1000 - Value % 1000 : Value - Value % 1000;

            if (Value > 0 && lTI2.Count > 0)
            {
                foreach (TI2 i in lTI2.OrderBy(x => x.SeqNo))
                {
                    if (Value >= i.Amt1 && Value <= i.Amt2)
                    {
                        Value = ((Value - (i.Amt1 - 1m)) * (i.Value1 / i.Value2)) + i.Value3;
                        break;
                    }
                }
            }
            Value = Value / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            if (Value <= 0) Value = 0m;
            r.TaxAllowance = decimal.Truncate(Value);
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            var EmpCode = r.EmpCode;
            decimal
                LatestSalary = 0m,
                LatestSalary2 = 0m,
                LatestSalaryPension = 0m;

            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode && x.ProcessInd).OrderBy(Index => Index.Dt))
            {
                LatestSalary = x.EmpSalary;
                LatestSalary2 = x.EmpSalary2;
                LatestSalaryPension = x.SalaryPension;

                if (x.ProcessInd)
                {
                    if (mIsPayrollAmtRounded)
                    {
                        r.PLDay += x.PLDay;
                        r.PLHr += x.PLHr;
                        r.PLAmt += decimal.Truncate(x.PLAmt);
                        r.UPLDay += x.UPLDay;
                        r.UPLHr += x.UPLHr;
                        r.UPLAmt += decimal.Truncate(x.UPLAmt);
                        r.ProcessPLAmt += decimal.Truncate(x.ProcessPLAmt);
                        r.ProcessUPLAmt += decimal.Truncate(x.ProcessUPLAmt);
                        //r.OT1Hr += x.OT1Hr;
                        //r.OT2Hr += x.OT2Hr;
                        //r.OTHolidayHr += x.OTHolidayHr;
                        //r.OT1Amt += decimal.Truncate(x.OT1Amt);
                        //r.OT2Amt += decimal.Truncate(x.OT2Amt);
                        //r.OTHolidayAmt += decimal.Truncate(x.OTHolidayAmt);
                        r.IncPerformance += decimal.Truncate(x.IncPerformance);
                        r.FieldAssignment += decimal.Truncate(x.FieldAssignment);
                        r.Transport += decimal.Truncate(x.Transport);
                        r.Meal += decimal.Truncate(x.Meal);
                        //r.ADOT += decimal.Truncate(x.ADOT);
                    }
                    else
                    {
                        r.PLDay += x.PLDay;
                        r.PLHr += x.PLHr;
                        r.PLAmt += x.PLAmt;
                        r.UPLDay += x.UPLDay;
                        r.UPLHr += x.UPLHr;
                        r.UPLAmt += x.UPLAmt;
                        r.ProcessPLAmt += x.ProcessPLAmt;
                        r.ProcessUPLAmt += x.ProcessUPLAmt;
                        //r.OT1Hr += x.OT1Hr;
                        //r.OT2Hr += x.OT2Hr;
                        //r.OTHolidayHr += x.OTHolidayHr;
                        //r.OT1Amt += x.OT1Amt;
                        //r.OT2Amt += x.OT2Amt;
                        //r.OTHolidayAmt += x.OTHolidayAmt;
                        r.IncPerformance += x.IncPerformance;
                        r.FieldAssignment += x.FieldAssignment;
                        r.Transport += x.Transport;
                        r.Meal += x.Meal;
                        //r.ADOT += x.ADOT;
                    }
                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            //untuk menghitung salary dan fixed allowance apabila pada 1 period terdiri dari lbh dari 1 payrun
            //kalau resign.
            //kalau payrun info berbeda dalam 1 periode.
            //kalau sudah pernah dibayar.

            Boolean IsPayrunDifferent = false, IsLatestPaidDtExisted = false;
            decimal
                AbsentDayForJoin = 0m,
                AbsentDayForResign = 0m,
                RecomputedProcessedWorkingDay = 0m,
                RecomputedAllWorkingDay = 0m,
                RecomputedProcessedWorkingDayAD = 0m
                ;

            foreach (var x in lResult1
                .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                .OrderBy(Index => Index.Dt))
            {
                if (Sm.CompareDtTm(x.Dt, x.JoinDt) <= 0)
                    AbsentDayForJoin += 1;

                if (x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0)
                    AbsentDayForResign += 1;
                else
                {
                    if (x.LatestPaidDt.Length > 0 && Sm.CompareDtTm(x.LatestPaidDt, x.Dt) == 0)
                        IsLatestPaidDtExisted = true;

                    if (!IsPayrunDifferent &&
                        !(
                        Sm.CompareStr(x.DeptCode, mDeptCode) &&
                        Sm.CompareStr(x.SystemType, mSystemType) &&
                        Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                        Sm.CompareStr(x.PGCode, mPGCode) &&
                        Sm.CompareStr(x.SiteCode, mSiteCode)
                        ))

                        IsPayrunDifferent = true;
                }

                if (!(x.HolInd || x.WSHolidayInd))
                {
                    //if (x.ProcessInd && x.WorkingDuration>0) RecomputedProcessedWorkingDay += 1;
                    if (x.ProcessInd) RecomputedProcessedWorkingDay += 1;
                    RecomputedAllWorkingDay += 1;
                }
            }

            r.Salary = LatestSalary;
            
            if (AbsentDayForJoin > 0 || AbsentDayForResign > 0 || IsPayrunDifferent)
            {
                if (mIsPayrollAmtRounded)
                    r.Salary = decimal.Truncate((LatestSalary / RecomputedAllWorkingDay) * RecomputedProcessedWorkingDay);
                else
                    r.Salary = (LatestSalary / RecomputedAllWorkingDay) * RecomputedProcessedWorkingDay;
            }

            //if (AbsentDayForJoin > 0 || IsPayrunDifferent)
            //{
                r.TaxableFixAllowance = 0m;
                r.NonTaxableFixAllowance = 0m;
                r.FixAllowance = 0m;

                r.TaxableFixDeduction = 0m;
                r.NonTaxableFixDeduction = 0m;
                r.FixDeduction = 0m;

                foreach(var i in mlEmployeeAllowanceDeduction.Where(w=>Sm.CompareStr(w.EmpCode, EmpCode)))
                {
                    RecomputedProcessedWorkingDayAD = 0m;
                    foreach (var x in lResult1
                        .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                        .OrderBy(Index => Index.Dt))
                    {
                        if (!(x.HolInd || x.WSHolidayInd))
                        {
                            if (x.ProcessInd) 
                            {
                                if (Sm.CompareDtTm(i.StartDt,x.Dt)<=0 && Sm.CompareDtTm(x.Dt, i.EndDt)<=0)
                                    RecomputedProcessedWorkingDayAD += 1;
                            }
                        }
                    }
                    //if (RecomputedAllWorkingDay!=0m)
                    //    i.Amt = i.Amt*(RecomputedProcessedWorkingDayAD/RecomputedAllWorkingDay);
                    //if (i.ADType == "A")
                    //{
                    //    if (i.TaxInd) 
                    //        r.TaxableFixAllowance += i.Amt;
                    //    else
                    //        r.NonTaxableFixAllowance += i.Amt;
                    //    r.FixAllowance += i.Amt;
                    //}
                    //else
                    //{
                    //    if (i.TaxInd) 
                    //        r.TaxableFixDeduction += i.Amt;
                    //    else
                    //        r.NonTaxableFixDeduction += i.Amt;
                    //    r.FixDeduction += i.Amt;
                    //}
                    
                    if (i.ADType == "A")
                    {
                        if (RecomputedAllWorkingDay != 0m)
                            i.Amt = i.Amt * (RecomputedProcessedWorkingDayAD / RecomputedAllWorkingDay);
                        if (i.TaxInd)
                            r.TaxableFixAllowance += i.Amt;
                        else
                            r.NonTaxableFixAllowance += i.Amt;
                        r.FixAllowance += i.Amt;
                    }
                    else
                    {
                        if (i.TaxInd)
                            r.TaxableFixDeduction += i.Amt;
                        else
                            r.NonTaxableFixDeduction += i.Amt;
                        r.FixDeduction += i.Amt;
                    }
                }
            //}


            if (mIsPayrollAmtRounded)
            {
                r.TaxableFixAllowance = decimal.Truncate(r.TaxableFixAllowance);
                r.NonTaxableFixAllowance = decimal.Truncate(r.NonTaxableFixAllowance);
                r.FixAllowance = decimal.Truncate(r.FixAllowance);
            }

            if (r.SalaryPension < 0m) r.SalaryPension = 0m;
            if (r.Salary < 0m) r.Salary = 0m;
            if (r.TaxableFixAllowance < 0) r.TaxableFixAllowance = 0m;
            if (r.NonTaxableFixAllowance < 0) r.NonTaxableFixAllowance = 0m;
            if (r.FixAllowance < 0) r.FixAllowance = 0m;

            decimal EmpAdvancePayment = 0m;
            int CreditAdvancePaymentNo = 0;
            string CreditCode = string.Empty;
            foreach (var x in mlAdvancePaymentProcess.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(o => o.CreditCode))
            {
                if (x.CreditCode.Length > 0)
                {
                    if (mIsPayrollAmtRounded)
                    {
                        if (Sm.CompareStr(x.CreditCode, CreditCode))
                        {
                            if (CreditAdvancePaymentNo == 1)
                                r.CreditAdvancePayment1 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 2)
                                r.CreditAdvancePayment2 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 3)
                                r.CreditAdvancePayment3 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 4)
                                r.CreditAdvancePayment4 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 5)
                                r.CreditAdvancePayment5 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 6)
                                r.CreditAdvancePayment6 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 7)
                                r.CreditAdvancePayment7 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 8)
                                r.CreditAdvancePayment8 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 9)
                                r.CreditAdvancePayment9 += decimal.Truncate(x.Amt);

                            if (CreditAdvancePaymentNo == 10)
                                r.CreditAdvancePayment10 += decimal.Truncate(x.Amt);
                        }
                        else
                        {
                            CreditAdvancePaymentNo += 1;

                            if (CreditAdvancePaymentNo == 1)
                            {
                                r.CreditCode1 = x.CreditCode;
                                r.CreditAdvancePayment1 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 2)
                            {
                                r.CreditCode2 = x.CreditCode;
                                r.CreditAdvancePayment2 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 3)
                            {
                                r.CreditCode3 = x.CreditCode;
                                r.CreditAdvancePayment3 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 4)
                            {
                                r.CreditCode4 = x.CreditCode;
                                r.CreditAdvancePayment4 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 5)
                            {
                                r.CreditCode5 = x.CreditCode;
                                r.CreditAdvancePayment5 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 6)
                            {
                                r.CreditCode6 = x.CreditCode;
                                r.CreditAdvancePayment6 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 7)
                            {
                                r.CreditCode7 = x.CreditCode;
                                r.CreditAdvancePayment7 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 8)
                            {
                                r.CreditCode8 = x.CreditCode;
                                r.CreditAdvancePayment8 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 9)
                            {
                                r.CreditCode9 = x.CreditCode;
                                r.CreditAdvancePayment9 += decimal.Truncate(x.Amt);
                            }
                            if (CreditAdvancePaymentNo == 10)
                            {
                                r.CreditCode10 = x.CreditCode;
                                r.CreditAdvancePayment10 += decimal.Truncate(x.Amt);
                            }
                        }
                    }
                    else
                    {
                        if (Sm.CompareStr(x.CreditCode, CreditCode))
                        {
                            if (CreditAdvancePaymentNo == 1)
                                r.CreditAdvancePayment1 += x.Amt;

                            if (CreditAdvancePaymentNo == 2)
                                r.CreditAdvancePayment2 += x.Amt;

                            if (CreditAdvancePaymentNo == 3)
                                r.CreditAdvancePayment3 += x.Amt;

                            if (CreditAdvancePaymentNo == 4)
                                r.CreditAdvancePayment4 += x.Amt;

                            if (CreditAdvancePaymentNo == 5)
                                r.CreditAdvancePayment5 += x.Amt;

                            if (CreditAdvancePaymentNo == 6)
                                r.CreditAdvancePayment6 += x.Amt;

                            if (CreditAdvancePaymentNo == 7)
                                r.CreditAdvancePayment7 += x.Amt;

                            if (CreditAdvancePaymentNo == 8)
                                r.CreditAdvancePayment8 += x.Amt;

                            if (CreditAdvancePaymentNo == 9)
                                r.CreditAdvancePayment9 += x.Amt;

                            if (CreditAdvancePaymentNo == 10)
                                r.CreditAdvancePayment10 += x.Amt;
                        }
                        else
                        {
                            CreditAdvancePaymentNo += 1;

                            if (CreditAdvancePaymentNo == 1)
                            {
                                r.CreditCode1 = x.CreditCode;
                                r.CreditAdvancePayment1 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 2)
                            {
                                r.CreditCode2 = x.CreditCode;
                                r.CreditAdvancePayment2 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 3)
                            {
                                r.CreditCode3 = x.CreditCode;
                                r.CreditAdvancePayment3 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 4)
                            {
                                r.CreditCode4 = x.CreditCode;
                                r.CreditAdvancePayment4 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 5)
                            {
                                r.CreditCode5 = x.CreditCode;
                                r.CreditAdvancePayment5 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 6)
                            {
                                r.CreditCode6 = x.CreditCode;
                                r.CreditAdvancePayment6 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 7)
                            {
                                r.CreditCode7 = x.CreditCode;
                                r.CreditAdvancePayment7 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 8)
                            {
                                r.CreditCode8 = x.CreditCode;
                                r.CreditAdvancePayment8 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 9)
                            {
                                r.CreditCode9 = x.CreditCode;
                                r.CreditAdvancePayment9 += x.Amt;
                            }
                            if (CreditAdvancePaymentNo == 10)
                            {
                                r.CreditCode10 = x.CreditCode;
                                r.CreditAdvancePayment10 += x.Amt;
                            }
                        }
                    }
                    CreditCode = x.CreditCode;
                }
                if (mIsPayrollAmtRounded)
                    EmpAdvancePayment += decimal.Truncate(x.Amt);
                else
                    EmpAdvancePayment += x.Amt;
            }


            r.EmpAdvancePayment = EmpAdvancePayment;

            if (mIsPayrollAmtRounded)
                r.Amt =
                    decimal.Truncate(r.Salary) -
                    decimal.Truncate(r.ProcessUPLAmt) +
                    //decimal.Truncate(r.OT1Amt) +
                    //decimal.Truncate(r.OT2Amt) +
                    //decimal.Truncate(r.OTHolidayAmt) +
                    decimal.Truncate(r.SalaryAdjustment) +
                    decimal.Truncate(r.Transport) +
                    decimal.Truncate(r.Meal) +
                    decimal.Truncate(r.PerformanceValue) +
                    //decimal.Truncate(r.ADOT) +
                    decimal.Truncate(r.FixAllowance) -
                    decimal.Truncate(r.FixDeduction) -
                    decimal.Truncate(r.EmpAdvancePayment) -
                    decimal.Truncate(r.SSEmployeeHealth) -
                    decimal.Truncate(r.SSEmployeeEmployment) -
                    decimal.Truncate(r.SSEePension) -
                    decimal.Truncate(r.SSEmployeePension);
            else
                r.Amt =
                    r.Salary -
                    r.ProcessUPLAmt +
                    //r.OT1Amt +
                    //r.OT2Amt +
                    //r.OTHolidayAmt +
                    r.SalaryAdjustment +
                    r.Transport +
                    r.Meal +
                    r.PerformanceValue +
                    //r.ADOT +
                    r.FixAllowance -
                    r.FixDeduction -
                    r.EmpAdvancePayment -
                    r.SSEmployeeHealth -
                    r.SSEmployeeEmployment -
                    r.SSEePension -
                    r.SSEmployeePension;

            //if (r.Amt < 0m) r.Amt = 0m;

            if (r.PTKP.Length > 0 && r.NPWP.Length > 0 && r.Amt > 0)
                ComputeTax(ref r, ref lNTI, ref lTI, ref lTI2);

            //if (r.Amt < 0m) r.Amt = 0m;

            r.Amt = decimal.Truncate(r.Amt);
        }

        #endregion

        #region Get Tables Data

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T1.CreditCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine(Filter.Replace("T1.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Or Not Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' " + Filter);;
            SQL.AppendLine(";");

            string Year = Sm.Left(Sm.GetDte(DteEndDt), 4), Month = Sm.GetDte(DteEndDt).Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "CreditCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            CreditCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T2.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, OTDt = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0) && Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T3.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 1));
                        No += 1;
                    }
                }
            }
            Filter = " And (" + Filter + ")";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI2(ref List<TI2> l)
        {
            string Payrun = Sm.GetLue(LuePayrunCode);
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.Value1, B.Value2, B.Value3 ");
            SQL.AppendLine("From TblTI2Hdr A ");
            SQL.AppendLine("Inner Join TblTI2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI2()
                        {
                            PayrunCode = Payrun,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Value1 = Sm.DrDec(dr, c[4]),
                            Value2 = Sm.DrDec(dr, c[5]),
                            Value3 = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd ");
            SQL.AppendLine("From TblLeaveDtl;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LeaveCode", "SystemType", "PayrunPeriod", "DeductTHPInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, C.LevelCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Where ");
            if (Filter.Length != 0)
                SQL.AppendLine(Filter + ";");
            else
                SQL.AppendLine(" 0=1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "LevelCode", "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", 

                    //11-12
                    "SiteCode", "HOInd"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            LevelCode = Sm.DrStr(dr, c[6]),
                            EmploymentStatus = Sm.DrStr(dr, c[7]),
                            SystemType = Sm.DrStr(dr, c[8]),
                            PayrunPeriod = Sm.DrStr(dr, c[9]),
                            PGCode = Sm.DrStr(dr, c[10]),
                            SiteCode = Sm.DrStr(dr, c[11]),
                            HOInd = Sm.DrStr(dr, c[12])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "In1", "Out1", 
                    
                    //6-9
                    "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            In1 = Sm.DrStr(dr, c[4]),
                            Out1 = Sm.DrStr(dr, c[5]),
                            In2 = Sm.DrStr(dr, c[6]),
                            Out2 = Sm.DrStr(dr, c[7]),
                            In3 = Sm.DrStr(dr, c[8]),
                            Out3 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("Where A.CancelInd='N'; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(Tbl.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T1.") + ") ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            if (Filter.Length != 0) SQL.AppendLine("And (" + Filter.Replace("Tbl.", "T2.") + ") ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, JoinDt, ResignDt ");
            SQL.AppendLine("From TblEmployee ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter + ");");
            else
                SQL.AppendLine("Where 1=0;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "JoinDt", "ResignDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, StartDt, Amt, Amt2 ");
            SQL.AppendLine("From TblEmployeeSalary ");
            SQL.AppendLine("Where StartDt<=@EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ");");
            else
                SQL.AppendLine("And 0=1;");

            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    { 
                        "EmpCode", 
                        "StartDt", "Amt", "Amt2" 
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalarySS(ref List<EmployeeSalarySS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r= 0; r< Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " Where ( " + Filter + ") "; 
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblParameter B On A.SSPCode=B.ParValue And B.ParCode='SSProgramForPension' And B.ParValue Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("); ");
            
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "EndDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog Where Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            var EndDtTemp = Sm.GetDte(DteEndDt);

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (Sm.GetGrdBool(Grd1, Row, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<string>(ref cm, "@PayrunCode", Sm.GetLue(LuePayrunCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeAllowance(ref List<EmployeeAllowance> l, string ADCode)
        {
            l.Clear();

            if (mADCodeMeal.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@ADCode", ADCode);

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select EmpCode, Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("Where ADCode=@ADCode ");
            if (Filter.Length > 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            else
                SQL.AppendLine(" And 0=1 ");
            SQL.AppendLine("Order By EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeAllowance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeAllowanceDeduction(ref List<EmployeeAllowanceDeduction> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            
            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }

            SQL.AppendLine("Select A.EmpCode, A.ADCode, A.StartDt, A.EndDt, A.Amt, B.ADType, B.TaxInd ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A, TblAllowanceDeduction B ");
            SQL.AppendLine("Where A.ADCode=B.ADCode ");
            SQL.AppendLine("And B.AmtType='1' ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    A.StartDt Is Not Null And A.EndDt Is Not Null ");
            SQL.AppendLine("    And ((A.StartDt<=@EndDt And @EndDt<=A.EndDt) Or (A.StartDt<=@StartDt And @StartDt<=A.EndDt)) ");
            SQL.AppendLine("    ) ");
            if (Filter.Length > 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            else
                SQL.AppendLine(" And 0=1 ");
            SQL.AppendLine("Order By A.EmpCode; ");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "ADCode", "StartDt", "EndDt", "Amt", "ADType", 
                    "TaxInd" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeAllowanceDeduction()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            EndDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                            ADType = Sm.DrStr(dr, c[5]),
                            TaxInd = Sm.DrStr(dr, c[6])=="Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                GetParameter();
                //GetValue();
                SetLuePayrunCode(ref LuePayrunCode);
                SetGrd();

                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlEmployeeAllowanceDeduction = new List<EmployeeAllowanceDeduction>();
                mlEmployeeAllowanceMeal = new List<EmployeeAllowance>();
                mlEmployeeAllowanceTransport = new List<EmployeeAllowance>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LuePayrunCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData();
            Sm.RefreshLookUpEdit(LuePayrunCode, new Sm.RefreshLue1(SetLuePayrunCode));
        }

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            ClearData();
            ShowPayrunInfo();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion

        #region Class

        private class Result1
        {
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string _LatestLevelCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal ADOT { get; set; }
        }

        private class Result2
        {
            public decimal _RecomputedValueForProratedAD { get; set; }
            public decimal _RecomputedValueForFixedAD { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
	        public decimal FixAllowance { get; set; }
	        public decimal EmploymentPeriodAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal IncMinWages { get; set; }
	        public decimal IncProduction { get; set; }
	        public decimal IncPerformance { get; set; }
	        public decimal PresenceReward { get; set; }
	        public decimal HolidayEarning { get; set; }
	        public decimal ExtraFooding { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployerPension2 { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal DedProduction { get; set; }
	        public decimal DedProdLeave { get; set; }
            public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployeePension2 { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal Functional { get; set; }
            public decimal Housing { get; set; }
            public decimal MobileCredit { get; set; }
            public decimal Zakat { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal SSErLifeInsurance { get; set; }
            public decimal SSEeLifeInsurance { get; set; }
            public decimal SSErWorkingAccident { get; set; }
            public decimal SSEeWorkingAccident { get; set; }
            public decimal SSErRetirement { get; set; }
            public decimal SSEeRetirement { get; set; }
            public decimal SSErPension { get; set; }
            public decimal SSEePension { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public string PerformanceStatus { get; set; }
            public string GradePerformance { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal ADOT { get; set; }
        }

        #region Additional Class

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public string CreditCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class TI2
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
        }

        private class EmpWorkSchedule
         {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
            public decimal IncPerformance { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class EmployeeAllowance
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeAllowanceDeduction
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
            public bool TaxInd { get; set; }
        }

        #endregion

        #endregion
    }
}
