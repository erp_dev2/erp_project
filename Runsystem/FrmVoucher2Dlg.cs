﻿#region Update
/*
    19/10/2018 [TKG] tambah informasi tipe pembayaran
    10/11/2018 [TKG] ambil info bank account dari vr
    16/09/2021 [VIN/IOK] tambah where 0=0
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucher2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucher2 mFrmParent;
        private string mSQL = string.Empty, mMInd = "N";

        #endregion

        #region Constructor

        public FrmVoucher2Dlg(FrmVoucher2 FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Voucher Request#",
                        "", 
                        "Date",
                        "Local#",
                        "Type",
                        
                        //6-10
                        "Vendor",
                        "Invoice#",
                        "Queue#",
                        "Currency",
                        "Amount",

                        //11-15
                        "Payment Type (I)",
                        "Cash/Bank Account (I)",
                        "Amount (I)",
                        "Payment Type (II)",
                        "Cash/Bank Account (II)",

                        //16-17
                        "Amount (II)",
                        "Remark"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 13, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.LocalDocNo, A.DocType, B.OptDesc As DocTypeDesc, ");
            SQL.AppendLine("    IfNull(IfNull(I.VdCode, IfNull(M.VdCode, Q.VdCode)), E.VdCode) As VdCode, ");
            SQL.AppendLine("    IfNull(IfNull(I.VdName, IfNull(M.VdName, Q.VdName)), E.VdName) As VdName, ");
            SQL.AppendLine("    D.VdInvNo, ");
            SQL.AppendLine("    IfNull(IfNull(H.QueueNo, L.QueueNo), P.QueueNo) as QueueNo, ");
            SQL.AppendLine("    A.CurCode, A.Amt, ");
            SQL.AppendLine("    R.OptDesc As PaymentTypeDesc, ");

            SQL.AppendLine("    Concat( ");
            SQL.AppendLine("        Case When T.BankName Is Not Null Then Concat(T.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("        Case When S.BankAcNo Is Not Null  ");
            SQL.AppendLine("        Then Concat(S.BankAcNo, ' [', IfNull(S.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(S.BankAcNm, '') End ");
            SQL.AppendLine("    ) As BankAcCodeDesc, ");

            //SQL.AppendLine("    Trim(Concat( ");
            //SQL.AppendLine("    Case When T.BankName Is Not Null Then Concat(T.BankName, ' ') Else '' End,  ");
            //SQL.AppendLine("    Case When S.BankAcNo Is Not Null  ");
            //SQL.AppendLine("    Then Concat(S.BankAcNo) ");
            //SQL.AppendLine("    Else IfNull(S.BankAcNm, '') End ");
            //SQL.AppendLine("    )) As BankAcCodeDesc, ");

            SQL.AppendLine("    F.Amt1, ");
            SQL.AppendLine("    U.OptDesc As PaymentTypeDesc2, ");

            SQL.AppendLine("    Concat( ");
            SQL.AppendLine("        Case When W.BankName Is Not Null Then Concat(W.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("        Case When V.BankAcNo Is Not Null  ");
            SQL.AppendLine("        Then Concat(V.BankAcNo, ' [', IfNull(V.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(V.BankAcNm, '') End ");
            SQL.AppendLine("    ) As BankAcCodeDesc2, ");

            //SQL.AppendLine("    Trim(Concat( ");
            //SQL.AppendLine("    Case When W.BankName Is Not Null Then Concat(W.BankName, ' ') Else '' End,  ");
            //SQL.AppendLine("    Case When V.BankAcNo Is Not Null  ");
            //SQL.AppendLine("    Then Concat(V.BankAcNo) ");
            //SQL.AppendLine("    Else IfNull(V.BankAcNm, '') End ");
            //SQL.AppendLine("    )) As BankAcCodeDesc2, ");

            SQL.AppendLine("    F.Amt2, ");
            SQL.AppendLine("    A.Remark ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Inner Join TblOption B On A.DocType=B.OptCode And B.OptCat='VoucherDocType' ");
            SQL.AppendLine("    Left Join TblOutgoingPaymentHdr C On A.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select T.DocNo, Group_Concat(T.VdInvNo Separator ', ') As VdInvNo ");
	        SQL.AppendLine("        From ( ");
		    SQL.AppendLine("            Select Distinct T1.DocNo, T2.VdInvNo ");        
		    SQL.AppendLine("            From TblOutgoingPaymentDtl T1 ");
		    SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T2 On T1.InvoiceDocNo=T2.DocNo And T2.VdInvNo Is Not Null ");
	        SQL.AppendLine("        ) T Group By T.DocNo ");
            SQL.AppendLine("    ) D On C.DocNo=D.DocNo ");
            SQL.AppendLine("    Left Join TblVendor E On C.VdCode=E.VdCode ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceRawMaterialHdr F On A.DocNo=F.VoucherRequestDocNo ");
            SQL.AppendLine("    Left Join TblRawMaterialVerify G On F.RawMaterialVerifyDocNo=G.DocNo ");
            SQL.AppendLine("    Left Join TblLegalDocVerifyHdr H On G.LegalDocVerifyDocNo=H.DocNo ");
            SQL.AppendLine("    Left Join TblVendor I On H.VdCode=I.VdCode ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceRawMaterialHdr J On A.DocNo=J.VoucherRequestDocNo2 And J.VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("    Left Join TblRawMaterialVerify K On J.RawMaterialVerifyDocNo=K.DocNo ");
            SQL.AppendLine("    Left Join TblLegalDocVerifyHdr L On K.LegalDocVerifyDocNo=L.DocNo ");
            SQL.AppendLine("    Left Join TblVendor M On L.VdCode=M.VdCode ");
            SQL.AppendLine("    Left Join TblPurchaseInvoiceRawMaterialHdr N On A.DocNo=N.VoucherRequestDocNo3 And N.VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("    Left Join TblRawMaterialVerify O On N.RawMaterialVerifyDocNo=O.DocNo ");
            SQL.AppendLine("    Left Join TblLegalDocVerifyHdr P On O.LegalDocVerifyDocNo=P.DocNo ");
            SQL.AppendLine("    Left Join TblVendor Q On P.VdCode=Q.VdCode ");
            SQL.AppendLine("    Left Join TblOption R On R.OptCat='VoucherPaymentType' And F.PaymentType=R.OptCode ");
            SQL.AppendLine("    Left Join TblBankAccount S On A.BankAcCode=S.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank T On S.BankCode=T.BankCode ");
            SQL.AppendLine("    Left Join TblOption U On U.OptCat='VoucherPaymentType' And F.PaymentType2=U.OptCode ");
            SQL.AppendLine("    Left Join TblBankAccount V On A.BankAcCode2=V.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank W On V.BankCode=W.BankCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.AcType2 is Null ");
            SQL.AppendLine("    And A.Status='A' ");
            if (mFrmParent.mIsUseMInd) SQL.AppendLine("    And A.MInd=@MInd ");
            SQL.AppendLine("    And A.DocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd='N') ");
            SQL.AppendLine(") Tbl ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVdInvNo.Text, "VdInvNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, "QueueNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocType, QueueNo, DocDt, DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "LocalDocNo", "DocTypeDesc", "VdName", "VdInvNo", 

                            //6-10
                            "QueueNo", "CurCode", "Amt", "PaymentTypeDesc", "BankAcCodeDesc", 

                            //11-15
                            "Amt1", "PaymentTypeDesc2", "BankAcCodeDesc2", "Amt2", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowVoucherRequestInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();

            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher request#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document type");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtVdInvNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdInvNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Invoice#");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Queue#");
        }

        #endregion

        #endregion
    }
}
