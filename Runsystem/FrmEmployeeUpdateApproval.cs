﻿#region Update
/*
    22/01/2021 [IBL/PHT] New Apps
    26/01/2021 [IBL/PHT] Tambah filter site
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeUpdateApproval : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private iGRichTextManager fManager;
        
        #endregion

        #region Constructor

        public FrmEmployeeUpdateApproval(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetLue(LueFontSize, "10");
                Sl.SetLueSiteCode2(ref LueSiteCode, "");
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "Approve",

                    //1-5
                    "Cancel",
                    "",
                    "Employee",
                    "Notes",
                    "Approver's Remark (Reason to cancel, etc)",
                }
            );

            fManager = new iGRichTextManager(Grd1);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 0, 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);

            Grd1.Cols[3].CellStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.SetGrdColHdrAutoAlign(Grd1);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.EmpCode, Concat( ");
            SQL.AppendLine("  'Employee Code : ', IfNull(T.EmpCode, ''), '\n', ");
            SQL.AppendLine("  'Employee Name : ', IfNull(T.EmpName, ''), '\n', ");
            SQL.AppendLine("  'Display Name  : ', IfNull(T.DisplayName, ''), '\n', ");
            SQL.AppendLine("  'Display Name  : ', IfNull(T.SiteName, '') ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.EmpCode, T1.EmpName, T1.DisplayName, ");
            SQL.AppendLine("    T1.EmpCodeOld, T3.SiteCode, T3.SiteName ");
            SQL.AppendLine("    From TblEmployeeUpdate T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("    Inner Join TblSite T3 On T1.SiteCode = T3.SiteCode ");
            SQL.AppendLine("    Where T1.ApproveUserCode Is Null And T1.ApproveStatus Is Null ");
            SQL.AppendLine("    And T1.SiteCode In ");
            SQL.AppendLine("    ( ");
   	        SQL.AppendLine("      Select SiteCode From TblDocApprovalSettingEmpUpdate ");
		    SQL.AppendLine("      Where UserCode Like '%#" + Gv.CurrentUserCode + "#%' ");
            SQL.AppendLine("    )  ");
            SQL.AppendLine(")T ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            string Filter = string.Empty;

            try
            {
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.EmpCode", "T.EmpName", "T.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter,
                    new string[] { "EmpCode", "Remarks" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = ChkAutoChoose.Checked;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                        Grd.Cells[Row, 4].Value = Sm.DrStr(dr, 1);
                        Grd.Cells[Row, 4].Font = new Font("Lucida Console", 9);
                        Grd.Cells[Row, 5].Value = null;
                    }, true, false, true, true
                );
                if (Grd1.Rows.Count > 1) Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                var SQL = new StringBuilder();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0 && (Sm.GetGrdBool(Grd1, Row, 0) || Sm.GetGrdBool(Grd1, Row, 1)))
                        cml.Add(UpdateEmployee(Row));
                }

                Sm.ExecCommands(cml);
                Sm.ClearGrd(Grd1, true);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && BtnSave.Enabled)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmEmployeeUpdateApprovalDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmEmployeeUpdateApprovalDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 3)));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if ((e.ColIndex == 0 || e.ColIndex == 1))
            {
                if (Sm.GetGrdBool(Grd1, e.RowIndex, e.ColIndex))
                    Grd1.Cells[e.RowIndex, (e.ColIndex == 0) ? 1 : 0].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 5].Value = null;
            }
        }

        #endregion

        #region Additional Method

        private MySqlCommand UpdateEmployee(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeeUpdate ");
            SQL.AppendLine("  Set ApproveUserCode = @UserCode, ApproveStatus = @ApproveStatus, ApproveRemark = @ApproveRemark, ");
            SQL.AppendLine("  LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode = @EmpCode And ApproveUserCode Is Null And ApproveStatus Is Null; ");

            if(Sm.GetGrdBool(Grd1, Row, 0))
            {
                SQL.AppendLine("Update TblEmployee A ");
                SQL.AppendLine("Inner Join TblEmployeeUpdate B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.EmpName = B.EmpName, A.DisplayName = B.DisplayName, A.EmploymentStatus = B.EmploymentStatus, A.UserCode = B.UserCode, A.EmpCodeOld = B.EmpCodeOld, A.ShortCode = B.ShortCode, A.BarcodeCode = B.BarcodeCode, A.DivisionCode = B.DivisionCode, A.DeptCode = B.DeptCode, A.PosCode = B.PosCode, A.PositionStatusCode = B.PositionStatusCode, ");
                SQL.AppendLine("    A.SectionCode = B.SectionCode, A.WorkGroupCode = B.WorkGroupCode, A.EntCode = B.EntCode, A.RegEntCode = B.RegEntCode, A.JoinDt = B.JoinDt, A.ResignDt = B.ResignDt, A.ContractDt = B.ContractDt, A.LeaveStartDt = B.LeaveStartDt, A.TGDt = B.TGDt, A.IDNumber = B.IDNumber, ");
                SQL.AppendLine("    A.Gender = B.Gender, A.Religion = B.Religion, A.MaritalStatus = B.MaritalStatus, A.WeddingDt = B.WeddingDt, A.BirthPlace = B.BirthPlace, A.BirthDt = B.BirthDt, A.Address = B.Address, A.CityCode = B.CityCode, A.SubDistrict = B.SubDistrict, ");
                SQL.AppendLine("    A.Village = B.Village, A.RTRW = B.RTRW, A.PostalCode = B.PostalCode, A.Domicile = B.Domicile, A.SiteCode = B.SiteCode, A.POH = B.POH, A.PerformanceStatus = B.PerformanceStatus, A.BloodType = B.BloodType, A.Phone = B.Phone, A.Mobile = B.Mobile, ");
	            SQL.AppendLine("    A.Email = B.Email, A.GrdLvlCode = B.GrdLvlCode, A.LevelCode = B.LevelCode, A.PGCode = B.PGCode, A.NPWP = B.NPWP, A.SystemType = B.SystemType, A.PayrollType = B.PayrollType, A.PTKP = B.PTKP, A.BankCode = B.BankCode, A.BankBranch = B.BankBranch, ");
	            SQL.AppendLine("    A.BankAcNo = B.BankAcNo, A.BankName = B.BankName, A.BankAcName = B.BankAcName, A.PayrunPeriod = B.PayrunPeriod, A.Mother = B.Mother, A.ClothesSize = B.ClothesSize, A.ShoeSize = B.ShoeSize, A.EducationType = B.EducationType, A.CostGroup = B.CostGroup, A.ConductInd = B.ConductInd, ");
                SQL.AppendLine("    A.ConductDtTm = B.ConductDtTm, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where B.EmpCode = @EmpCode; ");

                SQL.AppendLine("Delete From TblEmployeeFamily Where EmpCode = @EmpCode; ");

                SQL.AppendLine("Insert Into TblEmployeeFamily ( ");
                SQL.AppendLine("    EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ");
                SQL.AppendLine("    EducationLevelCode, ProfessionCode, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt ");
                SQL.AppendLine(")");
                SQL.AppendLine("Select ");
                SQL.AppendLine("    EmpCode, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, ");
                SQL.AppendLine("    EducationLevelCode, ProfessionCode, Remark, CreateBy, CreateDt, CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblEmployeeFamilyUpdate Where EmpCode = @EmpCode Order By DNo; ");
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@ApproveStatus", Sm.GetGrdBool(Grd1, Row, 0) ? "A" : "C");
            Sm.CmParam<String>(ref cm, "@ApproveRemark", Sm.GetGrdStr(Grd1, Row, 5));

            return cm;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkAutoChoose_CheckedChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < Grd1.Rows.Count; row++)
                if (Sm.GetGrdStr(Grd1, row, 3).Length != 0)
                    Grd1.Cells[row, 0].Value = ChkAutoChoose.Checked;
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        #endregion

        #endregion
    }
}
