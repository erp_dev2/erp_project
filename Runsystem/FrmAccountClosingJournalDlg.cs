﻿#region Update
// 14/02/2018 [HAR] Hanya Account kepala 3, 4, 5 yang ditampilkan
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmAccountClosingJournalDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAccountClosingJournal mFrmParent;
        private string mGrid = string.Empty;

        #endregion

        #region Constructor

        public FrmAccountClosingJournalDlg(FrmAccountClosingJournal FrmParent, string Grid)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mGrid = Grid;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetLueAcCode(ref LueAcCode);
        }

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "", 
                        "Account#", 
                        "Description", 
                        "Type"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (mGrid == "1")
                {
                    Filter = " And A.AcNo Not In (" + mFrmParent.GetSelectedCOA(mFrmParent.Grd1) + ") And A.Parent Like '" + Sm.GetLue(LueAcCode) + "%' ";
                }
                else if (mGrid == "2")
                {
                    Filter = " And A.AcNo Not In (" + mFrmParent.GetSelectedCOA(mFrmParent.Grd2) + ") And A.Parent Like '" + Sm.GetLue(LueAcCode) + "%' ";
                }
                else
                {
                    Filter = " And A.AcNo Not In (" + mFrmParent.GetSelectedCOA(mFrmParent.Grd3) + ") And A.Parent Like '" + Sm.GetLue(LueAcCode) + "%' ";
                }

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.AcDesc" });
                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcType ");
                SQL.AppendLine("From TblCoa A ");
                SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
                SQL.AppendLine("Where (A.AcNo Like '3%' Or A.AcNo Like '4%' Or A.AcNo Like '5%') ");
                SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("And A.ActInd='Y' ");
                if (Sm.GetLue(LueAcCode).Length > 0) SQL.AppendLine("A.AcNo Like '" + Sm.GetLue(LueAcCode) + "%' ");
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString() + Filter + " Order By A.AcNo, A.AcDesc;",
                        new string[] 
                        { "AcNo", "AcDesc", "AcType" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAcNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        if (mGrid == "1")
                        {
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;
                            mFrmParent.Grd1.Cells[Row1, 2].Value = "1";
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                            mFrmParent.Grd1.Rows.Add();
                        }
                        else if (mGrid == "2")
                        {
                            Row1 = mFrmParent.Grd2.Rows.Count - 1;
                            Row2 = Row;
                            mFrmParent.Grd2.Cells[Row1, 2].Value = "2";
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 4);
                            mFrmParent.Grd2.Rows.Add();
                        }
                        else
                        {
                            Row1 = mFrmParent.Grd3.Rows.Count - 1;
                            Row2 = Row;
                            mFrmParent.Grd3.Cells[Row1, 2].Value = "3";
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 4);
                            mFrmParent.Grd3.Rows.Add();
                        }                       
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsAcNoAlreadyChosen(int Row)
        {
            if (mGrid == "1")
            {
                for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 3), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            }
            else if (mGrid == "2")
            {
                for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 3), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            }
            else 
            {
                for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 3), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            }
            return false;
        }

        public static void SetLueAcCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select AcNo As Col1, AcDesc As Col2 " +
                "From TblCOA Where Level = 1 And AcNo Like '3' OR AcNo Like '4' Or AcNo Like '5' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account's Group");
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(SetLueAcCode));
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }
        
        #endregion
      
    }
}
