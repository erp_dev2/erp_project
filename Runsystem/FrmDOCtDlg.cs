﻿#region Update
/*
    18/03/2019 [TKG] menampilkan total quantity item yg dipilih pada saat double klik judul.
    17/09/2019 [WED] call method untuk ambil price dari CtQt
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    12/11/2019 [WED/SIER] tambah informasi wide dari master customer
    07/01/2020 [DITA/SIER] Tambah parameter IsFilterByItCt
    18/03/2020 [TKG/SRN] tambah stock price
    11/08/2020 [ICA/SIER] menyembunyikan kolom Batch sesuai dengan parameter IsDOCtHideBatchNo
    11/11/2020 [WED/IMS] item yang diambil berdasarkan SO Contract nya, kalau SO Contract diisi, berdasarkan parameter IsDOCtUseSOContract
    18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    01/02/2021 [DITA/PHT] data item yang diambil tidak hanya dari stock saja,tapi juga dari master item berdasarkan param : IsDOCtNotOnlyFromStock
    14/04/2021 [WED/PHT] bug ketika filter item
    14/06/2021 [RDA/SIER] menambahkan parameter IsItCtFilteredByGroup untuk menampilkan apakah item yang muncul berdasarkan group login atau tidak
    11/03/2022 [RDA/SIER] support : item yang muncul belum terfilter berdasarkan group login
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDOCtDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt mFrmParent;
        string mSQL = string.Empty, mWhsCode= string.Empty, mCtCode=string.Empty, mCtCtCode = string.Empty;
        private bool mFlag = false;
        internal bool
            mIsFilterByItCt = false
            ;

        #endregion

        #region Constructor

        public FrmDOCtDlg(FrmDOCt FrmParent, string WhsCode, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCtCode = CtCode;

            if (mFrmParent.mCustomerCategoryCodeForWideDOCt.Length > 0)
            {
                mCtCtCode = Sm.GetValue("Select CtCtCode From TblCustomer Where CtCode = @Param Limit 1;", CtCode);

                string[] p = mFrmParent.mCustomerCategoryCodeForWideDOCt.Split(',');
                foreach (string s in p)
                {
                    if (mCtCtCode == s)
                    {
                        mFlag = true;
                        break;
                    }
                }
            }
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                if(mIsFilterByItCt || mFrmParent.mIsItCtFilteredByGroup)
                    SetLueItCtCode(ref LueItCtCode);
                else
                    Sl.SetLueItCtCode(ref LueItCtCode);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");

            SQL.AppendLine("Select B.ItCtCode, A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, A.PropCode, C.PropName, A.BatchNo, A.Source, D.ItCtName, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, B.ItGrpCode, E.CtitCode, B.Specification, ");
            if (mFrmParent.mIsDOCtShowStockPrice)
                SQL.AppendLine("G.UPrice*G.ExcRate As StockPrice, ");
            else
                SQL.AppendLine("0.00 As StockPrice, ");
            if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                SQL.AppendLine("IfNull(F.Wide, 0.00) As Wide ");
            else
                SQL.AppendLine("0.00 As Wide ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On B.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem Where CtCode =@CtCode ");
            SQL.AppendLine("    ) E On B.ItCode = E.itCode ");
            if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Wide ");
                SQL.AppendLine("    From TblCustomerShipAddress ");
                SQL.AppendLine("    Where CtCode = @CtCode And DNo = @CtSADNo ");
                SQL.AppendLine(") F On 0 = 0 ");
            }
            if (mFrmParent.mIsDOCtShowStockPrice)
                SQL.AppendLine("Left Join TblStockPrice G On A.Source=G.Source ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode  ");
            SQL.AppendLine("And (A.Qty>0 Or A.Qty2>0 Or A.Qty3>0) ");
            SQL.AppendLine("And Locate(Concat('##', A.ItCode, A.PropCode, A.BatchNo, A.Source, A.Lot, A.Bin, '##'), @SelectedItem)<1 ");

            if (mIsFilterByItCt || mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            if (mFrmParent.mIsDOCtUseSOContract && mFrmParent.TxtSOContractDocNo.Text.Length > 0)
            {
                SQL.AppendLine("And A.BatchNo = @ProjectCode ");
            }

            if (mFrmParent.mIsDOCtNotOnlyFromStock)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.ItCtCode, A.ItCode, A.ItCodeInternal, A.ItName, A.ForeignName, '-' As PropCode, NULL As PropName, NULL As BatchNo, NULL As Source, B.ItCtName, '-' As Lot, '-' As Bin, ");
                SQL.AppendLine("0.00 As Qty, A.InventoryUomCode, 0.00 As Qty2, A.InventoryUomCode2, 0.00 As Qty3, A.InventoryUomCode3, A.ItGrpCode, C.CtitCode, A.Specification, ");
                SQL.AppendLine("0.00 As StockPrice, ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                    SQL.AppendLine("IfNull(D.Wide, 0.00) As Wide ");
                else
                    SQL.AppendLine("0.00 As Wide ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                SQL.AppendLine("left Join ( ");
                SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem Where CtCode =@CtCode ");
                SQL.AppendLine("    ) C On A.ItCode = C.itCode ");
                if (mFrmParent.mIsCustomerShpAddressDisplayWide && mFrmParent.mCtSADNo.Length > 0)
                {
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select Wide ");
                    SQL.AppendLine("    From TblCustomerShipAddress ");
                    SQL.AppendLine("    Where CtCode = @CtCode And DNo = @CtSADNo ");
                    SQL.AppendLine(") D On 0 = 0 ");
                }
                SQL.AppendLine("Where A.ItCode Not In ( ");
                SQL.AppendLine("    Select ItCode From TblStockSummary ");
                SQL.AppendLine("    Where WhsCode=@WhsCode  ");
                SQL.AppendLine("    And (Qty>0 Or Qty2>0 Or Qty3>0)");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.ActInd = 'Y' ");
                SQL.AppendLine("And Locate(Concat('##', A.ItCode, '##'), @SelectedItem)<1 ");

                if (mIsFilterByItCt || mFrmParent.mIsItCtFilteredByGroup)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }

            SQL.AppendLine(") T ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code", 
                    "", 
                    "Local Code", 
                    "Item's Name", 

                    //6-10
                    "Item's Category",
                    "Property Code",
                    "Property",
                    "Batch#",
                    "Source",
                    
                    //11-15
                    "Lot",
                    "Bin", 
                    "Stock",
                    "UoM",
                    "Stock",

                    //16-20
                    "UoM",
                    "Stock",
                    "UoM",
                    "Group",
                    "Customer's"+Environment.NewLine+"Reference",

                    //21-24
                    "Foreign Name",
                    "Specification",
                    "Wide",
                    "Stock's Price"
                },
                 new int[] 
                {
                    //0
                    50,
                    //1-5
                    20, 80, 20, 100, 250, 
                    //6-10
                    150, 0, 100, 200, 180, 
                    //11-15
                    60, 60, 80, 80, 80, 
                    //16-20
                    80, 80, 80, 100, 100,
                    //21-24
                    230, 300, 0, 130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17, 23, 24 }, 0);
            if (!mFrmParent.mIsCustomerShpAddressDisplayWide) Sm.GrdColInvisible(Grd1, new int[] { 23 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            if (mFrmParent.mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 10, 15, 16, 17, 18, 19 }, false);
                Grd1.Cols[21].Move(7);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 10, 15, 16, 17, 18, 19, 21 }, false);
            Grd1.Cols[20].Move(10);
            if (mFrmParent.mISDOCtShowCustomerItem == "N")
            {
               Sm.GrdColInvisible(Grd1, new int[] { 20 }, false);
            }
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[19].Visible = true;
                if (mFrmParent.mIsBOMShowSpecifications) Grd1.Cols[19].Move(7);
                else Grd1.Cols[19].Move(5);
            }
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 22 });
            Grd1.Cols[22].Move(6);
            Grd1.Cols[24].Visible = mFrmParent.mIsDOCtShowStockPrice;
            if (mFrmParent.mIsDOCtShowStockPrice) Grd1.Cols[24].Move(19);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, !mFrmParent.mIsDOCtHideBatchNo);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 1=1 ";
                var cm = new MySqlCommand();

                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@CtSADNo", mFrmParent.mCtSADNo);
                if (mFrmParent.mIsDOCtUseSOContract)
                    Sm.CmParam<String>(ref cm, "@ProjectCode", mFrmParent.TxtProjectCode.Text);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItCodeInternal", "T.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "T.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropCode.Text, new string[] { "T.PropCode", "T.PropName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "T.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "T.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "T.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + 
                        " Order By T.ItName, T.BatchNo;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "PropCode", "PropName", 
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty", 
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",
 
                            //16-20
                            "ItGrpCode", "CtItCode", "ForeignName", "Specification", "Wide",

                            //21
                            "StockPrice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 24);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 15, 18, 21, 24 });
                        if (mFrmParent.mIsCustomerShpAddressDisplayWide)
                        {
                            if (mFlag)
                            {
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 23);
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 23);
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 23);
                            }
                        }
                        mFrmParent.ComputeTotalQty();
                        mFrmParent.GetCtQtUPrice(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 2) +
                Sm.GetGrdStr(Grd1, Row, 7) +
                Sm.GetGrdStr(Grd1, Row, 9) +
                Sm.GetGrdStr(Grd1, Row, 10) +
                Sm.GetGrdStr(Grd1, Row, 11) +
                Sm.GetGrdStr(Grd1, Row, 12);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key, 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 4) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 10) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 11) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 12) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 13) 
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                if (e.ColIndex == 1)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                    for (int r= 0; r < Grd1.Rows.Count; r++)
                        Grd1.Cells[r, 1].Value = !IsSelected;
                }

                if (Sm.IsGrdColSelected(new int[] { 13, 15, 17 }, e.ColIndex))
                {
                    decimal Total = 0m;
                    for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, e.ColIndex).Length != 0) 
                            Total += Sm.GetGrdDec(Grd1, r, e.ColIndex);
                    Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
                }
            }
        }

        #endregion

        #region Additional Method

        private void SetLueItCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.ItCtCode As Col1, T1.ItCtName As Col2 ");
            SQL.AppendLine("FROM tblitemcategory T1 ");
            if (mIsFilterByItCt || mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("        Where ItCtCode=T1.ItCtCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T1.ItCtName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtPropCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
