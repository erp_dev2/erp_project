﻿#region Update
/*
    04/10/2021 [WED/PHT] new apps
 *  10/06/2021 [ICA/PHT] membuat menu CBP bisa di buka di docapproval
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan2Rev : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mDocType = string.Empty
            ;
        internal bool
            mIsCompanyBudgetPlanProfitLoss = false,
            mIsCompanyBudgetPlanBalanceSheet = false,
            mIsFilterByCC = false;

        private bool mIsCBPRevAmtBasedOnRealization = false;
        internal int[] mInputAmt = { 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28 };
        private int[] mPrevAmt = { 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29 };
        private int[] mActualAmt = { 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 };
        internal FrmCompanyBudgetPlan2RevFind FrmFind;
        internal List<AcItem> ml;

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan2Rev(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Company Budget Plan Profit Loss - Revision";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                ml = new List<AcItem>();
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                BtnEdit.Visible = false;
                GetParameter();
                Sl.SetLueYr(LueYr, string.Empty);
                SetGrd();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 45;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Account",
                    //1-5
                    "Account#", "Alias", "Allow", "Total", "Prev Total",
                    //6-10
                    "01", "Prev 01", "02", "Prev 02", "03",
                    //11-15
                    "Prev 03", "04", "Prev 04", "05", "Prev 05",
                    //16-20
                    "06", "Prev 06", "07", "Prev 07", "08", 
                    //21-25
                    "Prev 08", "09", "Prev 09", "10", "Prev 10",
                    //26-30
                    "11", "Prev 11", "12", "Prev 12", "Saved Total",
                    //31-35
                    "", "Actual Total", "Actual 01", "Actual 02", "Actual 03", 
                    //36-40
                    "Actual 04", "Actual 05", "Actual 06", "Actual 07", "Actual 08", 
                    //41-44
                    "Actual 09", "Actual 10", "Actual 11", "Actual 12"
                },
                new int[] 
                {
                    600,
                    200, 180, 20, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150,
                    20, 150, 150, 150, 150,
                    150, 150, 150, 150, 150,
                    150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 30 }, 0);
            Sm.GrdFormatDec(Grd1, mInputAmt, 0);
            Sm.GrdFormatDec(Grd1, mPrevAmt, 0);
            Sm.GrdFormatDec(Grd1, mActualAmt, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 30 });
            Sm.GrdColReadOnly(Grd1, mPrevAmt);
            Sm.GrdColReadOnly(Grd1, mActualAmt);
            Sm.GrdColReadOnly(Grd1, mInputAmt);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 30 });
            Sm.GrdColInvisible(Grd1, mPrevAmt);
            Sm.GrdColButton(Grd1, new int[] { 31 });
            Grd1.Cols[31].Move(1);
            Grd1.Cols[32].Move(7);
            Grd1.Cols[33].Move(10);
            Grd1.Cols[34].Move(13);
            Grd1.Cols[35].Move(16);
            Grd1.Cols[36].Move(19);
            Grd1.Cols[37].Move(21);
            Grd1.Cols[38].Move(24);
            Grd1.Cols[39].Move(27);
            Grd1.Cols[40].Move(30);
            Grd1.Cols[41].Move(33);
            Grd1.Cols[42].Move(36);
            Grd1.Cols[43].Move(39);
            Grd1.Cols[44].Move(42);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnOriginDocNo.Enabled = false;
                    Sm.GrdColInvisible(Grd1, mActualAmt, true);
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    Sm.GrdColInvisible(Grd1, mActualAmt, false);
                    BtnOriginDocNo.Enabled = true;
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            ml.Clear();
            mDocType = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueYr, LueCCCode, MeeRemark,
               TxtBudgetRequestDocNo, TxtStatus, TxtOriginDocNo
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 30 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, mInputAmt);
            Sm.SetGrdNumValueZero(ref Grd1, 0, mPrevAmt);
            Sm.SetGrdNumValueZero(ref Grd1, 0, mActualAmt);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                    ComputeAmt(e.RowIndex, e.ColIndex, mInputAmt);
                else
                    Sm.StdMsg(mMsgType.Info, "You can't input amount for this account");
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                if (e.ColIndex == 31 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "COA account# is empty"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmCompanyBudgetPlan2RevDlg(this, e.RowIndex,
                        BtnSave.Enabled && (TxtDocNo.Text.Length == 0)
                        ));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 31 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "COA account# is empty"))
                Sm.FormShowDialog(new FrmCompanyBudgetPlan2RevDlg(this, e.RowIndex, BtnSave.Enabled && (TxtDocNo.Text.Length == 0)));
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCompanyBudgetPlan2RevFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                SetLueCCCode(ref LueCCCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid())
                return;
            string DocNo= string.Empty;
            Cursor.Current = Cursors.WaitCursor;
            string RevNo = string.Empty;

            RevNo = Sm.GetValue("Select IfNull(Count(*), 0) From TblCompanyBudgetPlanHdr Where OriginDocNo = @Param; ", TxtOriginDocNo.Text);

            if (RevNo.Length == 0) RevNo = "0";
            RevNo = (Int32.Parse(RevNo) + 1).ToString();

            DocNo = string.Concat(TxtOriginDocNo.Text, "/REV", Sm.Right(string.Concat("000", RevNo), 3));

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCompanyBudgetPlanRevHdr(DocNo, RevNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (
                    Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, Row, 4) != 0m
                    )
                    cml.Add(SaveCompanyBudgetPlanRevDtl(DocNo, Row));

            cml.Add(UpdateCompanyBudgetPlan(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueCCCode, "Profit Center") ||
                IsGrdEmpty() ||
                IsAmountLessThenActual()
                ;
        }

        private bool IsAmountLessThenActual()
        {
            if (!mIsCBPRevAmtBasedOnRealization) return false;

            ReComputeActualAmount();

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0 && Sm.GetGrdStr(Grd1, i, 3) == "Y")
                {
                    if (Sm.GetGrdDec(Grd1, i, 4) > Sm.GetGrdDec(Grd1, i, 32))
                    {
                        var msg = new StringBuilder();

                        msg.AppendLine("Account : " + Sm.GetGrdStr(Grd1, i, 0));
                        msg.AppendLine("Total Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 4), 0));
                        msg.AppendLine("Actual Total Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 32), 0));
                        msg.AppendLine(Environment.NewLine);
                        msg.AppendLine("Total amount is bigger than actual total amount. ");

                        Sm.StdMsg(mMsgType.Warning, msg.ToString());
                        Sm.FocusGrd(Grd1, i, 4);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCompanyBudgetPlanRevHdr(string DocNo, string RevNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string CurrentDateTime = Sm.ServerCurrentDateTime();

            //doctype 2 = Profit Loss, 3= Blance Sheet
            SQL.AppendLine("Insert Into TblCompanyBudgetPlanHdr(DocNo, DocDt, Yr, CancelInd, Status, CCCode, "); 
            SQL.AppendLine("DocType, CompletedInd, BudgetRequestDocNo, RevNo, OriginDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', 'O', @CCCode, ");
            SQL.AppendLine("@DocType, 'Y', @BudgetRequestDocNo, @RevNo, @OriginDocNo, @Remark, @CreateBy, @CreateDt); ");

            SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl(DocNo, AcNo, Amt, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, AcNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @CreateBy, @CreateDt ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, @CCAcNo) ");
            if (mIsCompanyBudgetPlanProfitLoss)
                SQL.AppendLine("And Left(AcNo, 1) > 3 ");
            else
                SQL.AppendLine("And Left(AcNo, 1) Between 1 AND 3 ");
            
            SQL.AppendLine("; ");

            int i = 0;
            foreach (var x in ml)
            {
                SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl2(DocNo, AcNo, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select DocNo, @AcNo_" + i.ToString() + 
                    ", @ItCode_" + i.ToString() + 
                    ", @Rate01_" + i.ToString() +
                    ", @Rate02_" + i.ToString() +
                    ", @Rate03_" + i.ToString() +
                    ", @Rate04_" + i.ToString() +
                    ", @Rate05_" + i.ToString() +
                    ", @Rate06_" + i.ToString() +
                    ", @Rate07_" + i.ToString() +
                    ", @Rate08_" + i.ToString() +
                    ", @Rate09_" + i.ToString() +
                    ", @Rate10_" + i.ToString() +
                    ", @Rate11_" + i.ToString() +
                    ", @Rate12_" + i.ToString() + 
                    ", @Qty01_" + i.ToString() +
                    ", @Qty02_" + i.ToString() +
                    ", @Qty03_" + i.ToString() +
                    ", @Qty04_" + i.ToString() +
                    ", @Qty05_" + i.ToString() +
                    ", @Qty06_" + i.ToString() +
                    ", @Qty07_" + i.ToString() +
                    ", @Qty08_" + i.ToString() +
                    ", @Qty09_" + i.ToString() +
                    ", @Qty10_" + i.ToString() +
                    ", @Qty11_" + i.ToString() +
                    ", @Qty12_" + i.ToString() +
                    ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
                SQL.AppendLine("Where DocNo=@DocNo; ");

                Sm.CmParam<String>(ref cm, "@AcNo_" + i, x.AcNo);
                Sm.CmParam<String>(ref cm, "@ItCode_" + i, x.ItCode);
                Sm.CmParam<Decimal>(ref cm, "@Rate01_" + i, x.Rate01);
                Sm.CmParam<Decimal>(ref cm, "@Rate02_" + i, x.Rate02);
                Sm.CmParam<Decimal>(ref cm, "@Rate03_" + i, x.Rate03);
                Sm.CmParam<Decimal>(ref cm, "@Rate04_" + i, x.Rate04);
                Sm.CmParam<Decimal>(ref cm, "@Rate05_" + i, x.Rate05);
                Sm.CmParam<Decimal>(ref cm, "@Rate06_" + i, x.Rate06);
                Sm.CmParam<Decimal>(ref cm, "@Rate07_" + i, x.Rate07);
                Sm.CmParam<Decimal>(ref cm, "@Rate08_" + i, x.Rate08);
                Sm.CmParam<Decimal>(ref cm, "@Rate09_" + i, x.Rate09);
                Sm.CmParam<Decimal>(ref cm, "@Rate10_" + i, x.Rate10);
                Sm.CmParam<Decimal>(ref cm, "@Rate11_" + i, x.Rate11);
                Sm.CmParam<Decimal>(ref cm, "@Rate12_" + i, x.Rate12);
                Sm.CmParam<Decimal>(ref cm, "@Qty01_" + i, x.Qty01);
                Sm.CmParam<Decimal>(ref cm, "@Qty02_" + i, x.Qty02);
                Sm.CmParam<Decimal>(ref cm, "@Qty03_" + i, x.Qty03);
                Sm.CmParam<Decimal>(ref cm, "@Qty04_" + i, x.Qty04);
                Sm.CmParam<Decimal>(ref cm, "@Qty05_" + i, x.Qty05);
                Sm.CmParam<Decimal>(ref cm, "@Qty06_" + i, x.Qty06);
                Sm.CmParam<Decimal>(ref cm, "@Qty07_" + i, x.Qty07);
                Sm.CmParam<Decimal>(ref cm, "@Qty08_" + i, x.Qty08);
                Sm.CmParam<Decimal>(ref cm, "@Qty09_" + i, x.Qty09);
                Sm.CmParam<Decimal>(ref cm, "@Qty10_" + i, x.Qty10);
                Sm.CmParam<Decimal>(ref cm, "@Qty11_" + i, x.Qty11);
                Sm.CmParam<Decimal>(ref cm, "@Qty12_" + i, x.Qty12);
                
                ++i;
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            if (mIsCompanyBudgetPlanBalanceSheet)
                SQL.AppendLine("Where T.DocType='CompanyBudgetPlanBalanceSheetRev' ");
            else
                SQL.AppendLine("Where T.DocType='CompanyBudgetPlanProfitLossRev' ");

            SQL.AppendLine(";");

            SQL.AppendLine("Update TblCompanyBudgetPlanHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CCAcNo", GetCCAcNo());
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@RevNo", RevNo);
            Sm.CmParam<String>(ref cm, "@OriginDocNo", TxtOriginDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", TxtBudgetRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

            return cm;
        }

        private MySqlCommand SaveCompanyBudgetPlanRevDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Amt01=@Amt01, ");
            SQL.AppendLine("    Amt02=@Amt02, ");
            SQL.AppendLine("    Amt03=@Amt03, ");
            SQL.AppendLine("    Amt04=@Amt04, ");
            SQL.AppendLine("    Amt05=@Amt05, ");
            SQL.AppendLine("    Amt06=@Amt06, ");
            SQL.AppendLine("    Amt07=@Amt07, ");
            SQL.AppendLine("    Amt08=@Amt08, ");
            SQL.AppendLine("    Amt09=@Amt09, ");
            SQL.AppendLine("    Amt10=@Amt10, ");
            SQL.AppendLine("    Amt11=@Amt11, ");
            SQL.AppendLine("    Amt12=@Amt12 ");
            SQL.AppendLine("Where DocNo=@DocNo And AcNo=@AcNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateCompanyBudgetPlan(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("    Set CancelInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where (DocNo = @OriginDocNo Or (OriginDocNo Is Not Null And OriginDocNo = @OriginDocNo)) ");
            SQL.AppendLine("And DocNo != @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@OriginDocNo", TxtOriginDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowCompanyBudgetPlanHdr(DocNo);
                ShowCompanyBudgetPlanDtl2(DocNo);
                ShowAccount(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCompanyBudgetPlanHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, Yr, CancelInd, CCCode, OriginDocNo, BudgetRequestDocNo, Remark, DocType, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "Yr", "CancelInd", "CCCode", "OriginDocNo", 
                    
                    //6-9
                    "BudgetRequestDocNo", "Remark", "DocType", "StatusDesc"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[4]));
                    TxtOriginDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtBudgetRequestDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    mDocType = Sm.DrStr(dr, c[8]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowCompanyBudgetPlanDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.ItCode, B.ItName, B.PurchaseUomCode, ");
            SQL.AppendLine("A.Rate01, A.Rate02, A.Rate03, A.Rate04, A.Rate05, A.Rate06, ");
            SQL.AppendLine("A.Rate07, A.Rate08, A.Rate09, A.Rate10, A.Rate11, A.Rate12, ");
            SQL.AppendLine("A.Qty01, A.Qty02, A.Qty03, A.Qty04, A.Qty05, A.Qty06, ");
            SQL.AppendLine("A.Qty07, A.Qty08, A.Qty09, A.Qty10, A.Qty11, A.Qty12 ");
            SQL.AppendLine("From TblCompanyBudgetPlanDtl2 A, TblItem B ");
            SQL.AppendLine("Where A.ItCode=B.ItCode ");
            SQL.AppendLine("And A.DocNo=@DocNo; ");
            
            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "AcNo", 
                    //1-5
                    "ItCode", "ItName", "PurchaseUomCode", "Rate01", "Rate02",
                    //6-10
                    "Rate03", "Rate04", "Rate05", "Rate06", "Rate07", 
                    //11-15
                    "Rate08", "Rate09", "Rate10", "Rate11", "Rate12", 
                    //16-20
                    "Qty01", "Qty02", "Qty03", "Qty04", "Qty05", 
                    //21-25
                    "Qty06", "Qty07", "Qty08", "Qty09", "Qty10", 
                    //26-27
                    "Qty11", "Qty12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ml.Add(new AcItem()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            ItCode = Sm.DrStr(dr, c[1]),
                            ItName = Sm.DrStr(dr, c[2]),
                            PurchaseUomCode = Sm.DrStr(dr, c[3]),
                            Rate01 = Sm.DrDec(dr, c[4]),
                            Rate02 = Sm.DrDec(dr, c[5]),
                            Rate03 = Sm.DrDec(dr, c[6]),
                            Rate04 = Sm.DrDec(dr, c[7]),
                            Rate05 = Sm.DrDec(dr, c[8]),
                            Rate06 = Sm.DrDec(dr, c[9]),
                            Rate07 = Sm.DrDec(dr, c[10]),
                            Rate08 = Sm.DrDec(dr, c[11]),
                            Rate09 = Sm.DrDec(dr, c[12]),
                            Rate10 = Sm.DrDec(dr, c[13]),
                            Rate11 = Sm.DrDec(dr, c[14]),
                            Rate12 = Sm.DrDec(dr, c[15]),
                            Qty01 = Sm.DrDec(dr, c[16]),
                            Qty02 = Sm.DrDec(dr, c[17]),
                            Qty03 = Sm.DrDec(dr, c[18]),
                            Qty04 = Sm.DrDec(dr, c[19]),
                            Qty05 = Sm.DrDec(dr, c[20]),
                            Qty06 = Sm.DrDec(dr, c[21]),
                            Qty07 = Sm.DrDec(dr, c[22]),
                            Qty08 = Sm.DrDec(dr, c[23]),
                            Qty09 = Sm.DrDec(dr, c[24]),
                            Qty10 = Sm.DrDec(dr, c[25]),
                            Qty11 = Sm.DrDec(dr, c[26]),
                            Qty12 = Sm.DrDec(dr, c[27])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private string GetGrdAcNo()
        {
            string AcNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (AcNo.Length > 0) AcNo += ",";
                    AcNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            return AcNo;
        }

        private void ReComputeActualAmount()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<ActualAmount>();
            string AcNo = string.Empty;

            AcNo = GetGrdAcNo();
            GetReComputeActualAmountQuery(ref SQL);

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.CommandText = SQL.ToString();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Mth", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ActualAmount() 
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                ShowActualAmount(ref l);
            }

            l.Clear();
        }

        private void ShowActualAmount(ref List<ActualAmount> l)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    foreach (var x in l.Where(w => w.AcNo == Sm.GetGrdStr(Grd1, i, 1)))
                    {
                        if (x.Mth == "01") Grd1.Cells[i, 33].Value = x.Amt;
                        if (x.Mth == "02") Grd1.Cells[i, 34].Value = x.Amt;
                        if (x.Mth == "03") Grd1.Cells[i, 35].Value = x.Amt;
                        if (x.Mth == "04") Grd1.Cells[i, 36].Value = x.Amt;
                        if (x.Mth == "05") Grd1.Cells[i, 37].Value = x.Amt;
                        if (x.Mth == "06") Grd1.Cells[i, 38].Value = x.Amt;
                        if (x.Mth == "07") Grd1.Cells[i, 39].Value = x.Amt;
                        if (x.Mth == "08") Grd1.Cells[i, 40].Value = x.Amt;
                        if (x.Mth == "09") Grd1.Cells[i, 41].Value = x.Amt;
                        if (x.Mth == "10") Grd1.Cells[i, 42].Value = x.Amt;
                        if (x.Mth == "11") Grd1.Cells[i, 43].Value = x.Amt;
                        if (x.Mth == "12") Grd1.Cells[i, 44].Value = x.Amt;

                        Grd1.Cells[i, 32].Value = Sm.GetGrdDec(Grd1, i, 33) + Sm.GetGrdDec(Grd1, i, 34) +
                            Sm.GetGrdDec(Grd1, i, 35) + Sm.GetGrdDec(Grd1, i, 36) + Sm.GetGrdDec(Grd1, i, 37) +
                            Sm.GetGrdDec(Grd1, i, 38) + Sm.GetGrdDec(Grd1, i, 39) + Sm.GetGrdDec(Grd1, i, 40) +
                            Sm.GetGrdDec(Grd1, i, 41) + Sm.GetGrdDec(Grd1, i, 42) + Sm.GetGrdDec(Grd1, i, 43) +
                            Sm.GetGrdDec(Grd1, i, 44);
                    }
                }
            }
        }

        private void GetReComputeActualAmountQuery(ref StringBuilder SQL)
        {
            SQL.AppendLine("Select T.AcNo, T.Mth, Sum(T.Amt) Amt From ( ");
            SQL.AppendLine("    Select D.AcNo, Substring(A.DocDt, 5, 2) Mth, B.Amt ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
            SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.CCCode Is Not Null  ");
            SQL.AppendLine("        And A.Status = 'A'  ");
            SQL.AppendLine("        And A.CancelInd = 'N'  ");
            SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
            SQL.AppendLine("        And B.ItCode Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(A.CCCode, @CCCode) ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct X1.CCCode, X3.ItCode ");
            SQL.AppendLine("        From TblCompanyBudgetPlanHdr X1 ");
            SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.CompletedInd = 'Y' ");
            SQL.AppendLine("            And X1.CCCode Is Not Null ");
            SQL.AppendLine("            And X1.Yr = @Yr ");
            SQL.AppendLine("            And Find_In_Set(X1.CCCode, @CCCode) ");
            SQL.AppendLine("            And Find_In_Set(X2.AcNo, @AcNo) ");
            SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo ");
            SQL.AppendLine("    ) C On A.CCCode = C.CCCode And B.ItCode = C.ItCode ");
            SQL.AppendLine("    Inner Join TblCostCategory D On B.CCtCode = D.CCtCode ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select B.AcNo, Substring(A.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("    Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End As Amt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And Left(A.DocDt, 6) = @Yr ");
            SQL.AppendLine("    	And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
            SQL.AppendLine("    	And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
            SQL.AppendLine("    	And A.CCCode Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(A.CCCode, @CCCode) ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            SQL.AppendLine("    Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.AcNo, T.Mth; ");
        }

        internal void GetDetailData(string DocNo)
        {
            Sm.ClearGrd(Grd1, true);
            ml.Clear();
            ShowCompanyBudgetPlanDtl2(DocNo);
            ShowAccount(DocNo);
        }

        private void GetParameter()
        {
            mIsCompanyBudgetPlanBalanceSheet = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForCompanyBudgetPlanBalanceSheetRev"));
            mIsCompanyBudgetPlanProfitLoss = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForCompanyBudgetPlanProfitLossRev"));
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mIsCBPRevAmtBasedOnRealization = Sm.GetParameterBoo("IsCBPRevAmtBasedOnRealization");
        }

        internal void ComputeAmt(int Row, int Col, int[] Cols)
        {
            int r = Row;
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var Amt = Sm.GetGrdDec(Grd1, r, Col);
            var AmtPrev = Sm.GetGrdDec(Grd1, r, (Col + 1));
            Grd1.Cells[r, (Col + 1)].Value = Amt;
            var l = new List<string>();
            var Pos = 1;

            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l.Add(AcNo);
                }
            }

            //calculate row's total
            decimal mTotalR = 0m;
            for (int i = 0; i < Cols.Length; ++i)
            {
                mTotalR += Sm.GetGrdDec(Grd1, Row, Cols[i]);
            }

            Grd1.Cells[Row, 4].Value = mTotalR;
            Grd1.Cells[Row, 5].Value = Sm.GetGrdDec(Grd1, Row, 5);

            // calculate parent
            foreach (var x in l)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 1), x))
                    {
                        decimal mTotalP = 0m;

                        Grd1.Cells[i, Col].Value = Sm.GetGrdDec(Grd1, i, Col) - AmtPrev + Amt;
                        Grd1.Cells[i, (Col + 1)].Value = Sm.GetGrdDec(Grd1, i, (Col + 1));

                        for (int k = 0; k < Cols.Length; ++k)
                        {
                            mTotalP += Sm.GetGrdDec(Grd1, i, Cols[k]);
                        }

                        Grd1.Cells[i, 4].Value = mTotalP;
                        Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 5);
                        break;
                    }
                }
            }
        }

        internal void ComputeActual(int r, int c, ref List<AcRow> l)
        {
            var Amt = Sm.GetGrdDec(Grd1, r, c);
            if (Amt == 0m) return;
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var l2 = new List<string>();
            var Pos = 1;
            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l2.Add(AcNo);
                }
            }

            // calculate parent
            foreach (var i in l2.OrderBy(o=>o))
            {
                foreach (var j in l.Where(w=>Sm.CompareStr(w.AcNo, i)))
                    Grd1.Cells[j.Row, c].Value = Sm.GetGrdDec(Grd1, j.Row, c) + Amt;
            }
        }

        private void ShowAccount(string DocNo)
        {
            int Level = 0;
            int PrevLevel = -1;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    
                    SQL.AppendLine("Select B.Level, Concat(A.AcNo, ' ', B.AcDesc) As Account, ");
                    SQL.AppendLine("A.AcNo, B.Alias, A.Amt, A.Amt01, ");
                    SQL.AppendLine("A.Amt02, A.Amt03, ");
                    SQL.AppendLine("A.Amt04, A.Amt05, ");
                    SQL.AppendLine("A.Amt06, A.Amt07, ");
                    SQL.AppendLine("A.Amt08, A.Amt09, ");
                    SQL.AppendLine("A.Amt10, A.Amt11, ");
                    SQL.AppendLine("A.Amt12, ");
                    SQL.AppendLine("If(C.AcNo Is Null, 'N', 'Y') As Allow, ");
                    //if (!BtnSave.Enabled)
                    //{
                        SQL.AppendLine("IfNull(D.Actual, 0.00) As Actual, ");
                        SQL.AppendLine("IfNull(D.Actual01, 0.00) As Actual01, IfNull(D.Actual02, 0.00) As Actual02, IfNull(D.Actual03, 0.00) As Actual03, ");
                        SQL.AppendLine("IfNull(D.Actual04, 0.00) As Actual04, IfNull(D.Actual05, 0.00) As Actual05, IfNull(D.Actual06, 0.00) As Actual06, ");
                        SQL.AppendLine("IfNull(D.Actual07, 0.00) As Actual07, IfNull(D.Actual08, 0.00) As Actual08, IfNull(D.Actual09, 0.00) As Actual09, ");
                        SQL.AppendLine("IfNull(D.Actual10, 0.00) As Actual10, IfNull(D.Actual11, 0.00) As Actual11, IfNull(D.Actual12, 0.00) As Actual12 ");
                    //}
                    //else
                    //{
                    //    SQL.AppendLine("0.00 As Actual, ");
                    //    SQL.AppendLine("0.00 As Actual01, 0.00 As Actual02, 0.00 As Actual03, ");
                    //    SQL.AppendLine("0.00 As Actual04, 0.00 As Actual05, 0.00 As Actual06, ");
                    //    SQL.AppendLine("0.00 As Actual07, 0.00 As Actual08, 0.00 As Actual09, ");
                    //    SQL.AppendLine("0.00 As Actual10, 0.00 As Actual11, 0.00 As Actual12 ");
                    //}

                    SQL.AppendLine("From TblCompanyBudgetPlanDtl A ");
                    SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("   Select AcNo From TblCoa ");
                    SQL.AppendLine("   Where Acno Not In (Select Parent From TblCoa Where Parent Is Not Null) ");
                    SQL.AppendLine(") C On B.Acno=C.AcNo ");
                    //if (!BtnSave.Enabled)
                    //{
                        SQL.AppendLine("Left Join (");
                        SQL.AppendLine("    Select AcNo, 0.00 ");
                        for (int i = 1; i <= 12; i++)
                            SQL.AppendLine("    +Sum(Actual" + Sm.Right("0" + i.ToString(), 2) + ") ");
                        SQL.AppendLine("    As Actual ");
                        for (int i = 1; i <= 12; i++)
                            SQL.AppendLine("    ,Sum(Actual" + Sm.Right("0" + i.ToString(), 2) + ") As Actual" + Sm.Right("0" + i.ToString(), 2));
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("    Select B.AcNo ");
                        for (int i = 1; i <= 12; i++)
                            SQL.AppendLine("    ,Case Substring(D.DocDt, 5, 2) When '" + Sm.Right("0" + i.ToString(), 2) + "' Then D.Amt Else 0.00 End As Actual" + Sm.Right("0" + i.ToString(), 2));
                        SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
                        SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("    Inner Join TblBudgetCategory C On A.CCCode=C.CCCode And B.AcNo=C.AcNo ");
                        SQL.AppendLine("    Inner Join TblVoucherHdr D On C.BCCode=D.BCCode And D.CancelInd='N' And A.Yr=Left(D.DocDt, 4) And D.BCCode Is Not Null ");
                        SQL.AppendLine("    Where A.CancelInd='N' ");
                        SQL.AppendLine("    And A.CompletedInd='Y' ");
                        SQL.AppendLine("    And A.DocNo=@DocNo ");
                        SQL.AppendLine("    ) T Group By AcNo ");
                        SQL.AppendLine(") D On B.Acno=D.AcNo ");
                    //}
                    SQL.AppendLine("Where DocNo=@DocNo ");
                    SQL.AppendLine("Order By AcNo;");

                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                        
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Level", 
                        //1-5
                        "Account", "AcNo", "Alias", "Allow", "Amt",
                        //6-10
                        "Amt01", "Amt02", "Amt03", "Amt04", "Amt05",
                        //11-15
                        "Amt06", "Amt07", "Amt08", "Amt09", "Amt10",
                        //16-20
                        "Amt11", "Amt12", "Actual", "Actual01", "Actual02", 
                        //21-25
                        "Actual03", "Actual04", "Actual05", "Actual06", "Actual07", 
                        //26-30
                        "Actual08", "Actual09", "Actual10", "Actual11", "Actual12"
                    });
                    if (!dr.HasRows)
                    {
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    }
                    else
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = int.Parse(Sm.DrStr(dr, 0));
                            //Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 4);
                            
                            Grd1.Cells[Row, 4].Value = Sm.DrDec(dr, c[5]);
                            Grd1.Cells[Row, 5].Value = Sm.DrDec(dr, c[5]);
                            Grd1.Cells[Row, 6].Value = Sm.DrDec(dr, c[6]);
                            Grd1.Cells[Row, 7].Value = Sm.DrDec(dr, c[6]);
                            Grd1.Cells[Row, 8].Value = Sm.DrDec(dr, c[7]);
                            Grd1.Cells[Row, 9].Value = Sm.DrDec(dr, c[7]);
                            Grd1.Cells[Row, 10].Value = Sm.DrDec(dr, c[8]);
                            Grd1.Cells[Row, 11].Value = Sm.DrDec(dr, c[8]);
                            Grd1.Cells[Row, 12].Value = Sm.DrDec(dr, c[9]);
                            Grd1.Cells[Row, 13].Value = Sm.DrDec(dr, c[9]);
                            Grd1.Cells[Row, 14].Value = Sm.DrDec(dr, c[10]);
                            Grd1.Cells[Row, 15].Value = Sm.DrDec(dr, c[10]);
                            Grd1.Cells[Row, 16].Value = Sm.DrDec(dr, c[11]);
                            Grd1.Cells[Row, 17].Value = Sm.DrDec(dr, c[11]);
                            Grd1.Cells[Row, 18].Value = Sm.DrDec(dr, c[12]);
                            Grd1.Cells[Row, 19].Value = Sm.DrDec(dr, c[12]);
                            Grd1.Cells[Row, 20].Value = Sm.DrDec(dr, c[13]);
                            Grd1.Cells[Row, 21].Value = Sm.DrDec(dr, c[13]);
                            Grd1.Cells[Row, 22].Value = Sm.DrDec(dr, c[14]);
                            Grd1.Cells[Row, 23].Value = Sm.DrDec(dr, c[14]);
                            Grd1.Cells[Row, 24].Value = Sm.DrDec(dr, c[15]);
                            Grd1.Cells[Row, 25].Value = Sm.DrDec(dr, c[15]);
                            Grd1.Cells[Row, 26].Value = Sm.DrDec(dr, c[16]);
                            Grd1.Cells[Row, 27].Value = Sm.DrDec(dr, c[16]);
                            Grd1.Cells[Row, 28].Value = Sm.DrDec(dr, c[17]);
                            Grd1.Cells[Row, 29].Value = Sm.DrDec(dr, c[17]);
                            Grd1.Cells[Row, 30].Value = Sm.DrDec(dr, c[5]);

                            Grd1.Cells[Row, 32].Value = Sm.DrDec(dr, c[18]);
                            Grd1.Cells[Row, 33].Value = Sm.DrDec(dr, c[19]);
                            Grd1.Cells[Row, 34].Value = Sm.DrDec(dr, c[20]);
                            Grd1.Cells[Row, 35].Value = Sm.DrDec(dr, c[21]);
                            Grd1.Cells[Row, 36].Value = Sm.DrDec(dr, c[22]);
                            Grd1.Cells[Row, 37].Value = Sm.DrDec(dr, c[23]);
                            Grd1.Cells[Row, 38].Value = Sm.DrDec(dr, c[24]);
                            Grd1.Cells[Row, 39].Value = Sm.DrDec(dr, c[25]);
                            Grd1.Cells[Row, 40].Value = Sm.DrDec(dr, c[26]);
                            Grd1.Cells[Row, 41].Value = Sm.DrDec(dr, c[27]);
                            Grd1.Cells[Row, 42].Value = Sm.DrDec(dr, c[28]);
                            Grd1.Cells[Row, 43].Value = Sm.DrDec(dr, c[29]);
                            Grd1.Cells[Row, 44].Value = Sm.DrDec(dr, c[30]);

                            if (Sm.GetGrdStr(Grd1, Row, 3) == "N")
                            {
                                Grd1.Rows[Row].ReadOnly = iGBool.True;
                                Grd1.Rows[Row].BackColor = Color.FromArgb(224, 224, 224);
                            }
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevLevel >= Level) ? //(PrevAcNo.Length >= Sm.DrStr(dr, 2).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevLevel = Level; //Sm.DrStr(dr, 2);
                            Row++;
                        }
                    }
                    dr.Close();

                    if (TxtDocNo.Text.Length != 0 && !BtnSave.Enabled && !ChkCancelInd.Checked && ChkParentActual.Checked)
                    {
                        var l = new List<AcRow>();
                        for (int i = 0; i < Grd1.Rows.Count; i++)
                            l.Add(new AcRow {AcNo = Sm.GetGrdStr(Grd1, i, 1), Row = i });

                        for (int i = 0; i < Grd1.Rows.Count; i++)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 3) == "Y")
                            {
                                for (int j = 32; j <= 44; j++)
                                    ComputeActual(i, j, ref l);
                            }
                        }
                        l.Clear();
                    }

                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetCCAcNo()
        {
            var l1 = new List<string>();
            var l2 = new List<string>();
            var v = string.Empty;

            GetCCAcNo(ref l1);

            if (l1.Count() > 0)
            {
                int p = 1;
                
                foreach (var x in l1)
                {
                    p = 1;
                    v = x;
                    l2.Add(v);
                    while (p > 0)
                    {
                        p = v.LastIndexOf(".");
                        if (p > 0)
                        {
                            v = v.Substring(0, p);
                            l2.Add(v);
                        }
                    }
                }
                l1.Clear();
                v = string.Empty; //digunakan kembali utk proses lain

                foreach (var x in l2.Distinct())
                {
                    if (v.Length > 0) v += ",";
                    v += x;
                }
            }
            return v.Length == 0?"***":v;

        }

        private void GetCCAcNo(ref List<string> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.AcNo ");
            SQL.AppendLine("From TblCostCategory A ");
            SQL.AppendLine("Inner Join TblCoa B On A.AcNo=B.AcNo And B.ActInd='Y' ");
            SQL.AppendLine("Where A.AcNo Is Not Null ");
            SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("Order By A.AcNo; ");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read()) l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        internal void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CCCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.NotParentInd='Y' ");
                SQL.AppendLine("And T.ActInd='Y' ");
                SQL.AppendLine("And T.CBPInd='Y' ");
                SQL.AppendLine("And T.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("And T.ProfitCenterCode In (");
                SQL.AppendLine("    Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
            Sm.ClearGrd(Grd1, true);
        }
        
        #endregion

        #region Button Clicks

        private void BtnOriginDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCompanyBudgetPlan2RevDlg3(this));
        }

        private void BtnBudgetRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget Request Yearly", false))
            {
                var f = new FrmBudgetRequestYearly("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBudgetRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class ActualAmount
        {
            public string AcNo { get; set; }
            public string Mth { get; set; }
            public decimal Amt { get; set; }
        }

        internal class AcItem
        {
            public string AcNo { set; get; }
            public string ItCode { set; get; }
            public string ItName { get; set; }
            public string PurchaseUomCode { get; set; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
        }

        internal class AcRow
        {
            public string AcNo { set; get; }
            public int Row { set; get; }
        }

        internal class ImportCSV
        {
            public string AcNo { set; get; }
            public string ItCode { set; get; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
            public bool IsValid { get; set; }
        }

        internal class GrdAccount
        {
            public string AcNo { set; get; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            
        }


        #endregion
       
    }
}
