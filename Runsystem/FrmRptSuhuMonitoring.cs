﻿#region Update
/*
    03/06/2020 [TKG/IOK] menggunakan periode
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSuhuMonitoring : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptSuhuMonitoring(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select DocDt,  Oven as Boiler, ");
            SQL.AppendLine("    Case  ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 700 and  Cast(right(Tm, 4) As UNSIGNED) <1500 Then '1' ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 1500 and  Cast(right(Tm, 4)  As UNSIGNED) <2300 Then '2' ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 2300 Or  Cast(right(Tm, 4) As UNSIGNED) <700 Then '3' ");
            SQL.AppendLine("    else 'NoShift' End As ShiftCode,  ");
            SQL.AppendLine("    Case  ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 700 and  Cast(right(Tm, 4) As UNSIGNED) <1500 Then 'Shift1' ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 1500 and  Cast(right(Tm, 4)  As UNSIGNED) <2300 Then 'Shift2' ");
            SQL.AppendLine("    when  Cast(right(Tm, 4) As UNSIGNED) >= 2300 Or  Cast(right(Tm, 4) As UNSIGNED) <700 Then 'Shift3' ");
            SQL.AppendLine("    else 'NoShift' End As ShiftName, concat(left(CreateDt, 8), right(Tm, 4)) aS tm, Temperature ");
            SQL.AppendLine("    From TbltEMpLog ");
            SQL.AppendLine("    Where DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(")Z ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Boiler",
                        "Shift Code",
                        "Shift Name",
                        "Time",
                        //6
                        "Temperature",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 120, 80, 100, 80,
                        //6
                        100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatTime(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 5 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtBoiler.Text, "Z.Boiler", false);
                Sm.FilterStr(ref Filter, ref cm, TxtShift.Text, new string[] { "Z.ShiftCode", "Z.ShiftName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocDt Desc;",
                        new string[]
                        {

                            //0
                            "DocDt", 

                            //1-5
                            "Boiler", "ShiftCode", "ShiftName", "Tm", "Temperature", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkBoiler_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Boiler#");
        }

        private void ChkShift_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shift#");
        }

        private void TxtBoiler_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtShift_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
