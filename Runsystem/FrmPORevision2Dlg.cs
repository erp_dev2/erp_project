﻿#region Update
/* 
    24/01/2022 [TYO/PHT] New apps, based on PORevisionDlg
    24/01/2022 [TYO/PHT] Filter Item by Item category group user login by parameter IsFilterByItCt
    27/01/2022 [TYO/PHT] validasi Uncheck/Check Addendum Indicator dari IEP
    07/02/2022 [TYO/PHT] menambah validasi saat choose PO
    11/02/2022 [RIS/PHT] BUG : Addendum Availablity
    23/02/2022 [TYO/PHT] BUG : validasi saat choose PO
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORevision2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORevision2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPORevision2Dlg(FrmPORevision2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e); this.Text = "List Of PO";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
            SetGrd();
            SetSQL();
            if (!mFrmParent.mIsPORevisionShowPOService) label4.Visible = TxtPOServiceDocNo.Visible = ChkPOServiceDocNo.Visible = false;
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "No",

                        //1-5
                        "",
                        "PO#",
                        "PO"+Environment.NewLine+"DNo",
                        "PO Request#",
                        "PO Request"+Environment.NewLine+"DNo",

                        //6-10
                        "Item's Code",
                        "",
                        "Item's Name",    
                        "Quantity",
                        "UoM",

                        //11-15
                        "Quotation#",
                        "Quotation"+Environment.NewLine+"DNo",
                        "Vendor Code",
                        "Vendor Name",
                        "Term of"+Environment.NewLine+"Payment",

                         //16-20
                        "Currency",
                        "Unit Price",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",

                        //21-25
                        "Total",
                        "Estimated"+Environment.NewLine+"Received Date",
                        "PO's Remark",
                        "Item's Remark",
                        "Estimated"+Environment.NewLine+"Time Arrived",

                        //26-30
                        "Local Code",
                        "Specification",
                        "Tax Code",
                        "PO For Service#",
                        "Grand Total PO",

                        //31-32
                        "Addendum Availability",
                        "Addendum Indicator",
                    },
                     new int[]
                    {
                        //0
                        30,
                        //1-5
                        20, 180, 20, 180, 50,
                        //6-10
                        80, 20, 150, 100, 80,  
                        //11-15
                        150, 80, 100, 150, 100, 
                        //16-20
                        80, 100, 80, 100, 100, 
                        //21-25
                        150, 100, 150, 150, 150,
                        //26-30
                        150, 200, 0, 180, 150,

                        //31-32
                        150, 80
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 17, 18, 19, 20, 21}, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 7 });
            Sm.GrdFormatDate(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 12, 13, 25, 26, 27, 28, 30, 31 ,32 }, false);

            if (mFrmParent.mIsBOMShowSpecifications)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27 }, true);
                Grd1.Cols[26].Move(8);
                Grd1.Cols[27].Move(10);
            }
            if (mFrmParent.mIsPORevisionShowPOService)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, true);
                Grd1.Cols[29].Move(3);
            }
            if (mFrmParent.mIsPORevisionUseETA) Sm.GrdColInvisible(Grd1, new int[] { 25 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  6, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, K.DocDt, A.DNo, A.PORequestDocNo, A.PORequestDNo, ");
            SQL.AppendLine("E.ItCode, H.ItName, ifnull(M.Qty, A.Qty) As Qty, H.PurchaseUomCode, ");
            SQL.AppendLine("C.QtDocNo, C.QtDNo, F.VdCode, I.VdName, J.PtName, F.CurCode, G.UPrice, ifnull(M.Discount, A.Discount) As Discount, ifnull(M.DiscountAmt, A.DiscountAmt) As DiscountAmt, ifnull(M.RoundingValue, A.RoundingValue) As RoundingValue, A.EstRecvDt, ");
            SQL.AppendLine("(((100 - ifnull(M.Discount, A.Discount))/100) * (ifnull(M.Qty, A.Qty) * G.UPrice)) - ifnull(M.DiscountAmt, A.DiscountAmt) + ifnull(M.RoundingValue, A.RoundingValue) As Total, K.Remark As RemarkHdr, A.Remark As RemarkDtl, ");
            if(mFrmParent.mIsPORevisionUseETA) 
                SQL.AppendLine("A.EstTimeArrival, ");
            else
                SQL.AppendLine("Null As EstTimeArrival, ");
            SQL.AppendLine("H.ItCodeInternal, H.Specification, K.TaxCode1, L.DocNo POSDocNo, K.Amt GrandTotal, K.AddendumAvailability, M.AddendumInd ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Inner Join TblPOHdr K on A.DocNo = K.DocNo And K.Status='A' ");
            SQL.AppendLine("Inner Join TblPORequestHdr B On A.PORequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On A.PORequestDocNo=C.DocNo And A.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (D.SiteCode Is Null Or ( ");
                SQL.AppendLine("    D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And (F.PtCode Is Null Or ( ");
                SQL.AppendLine("    And F.PtCode Is Not Null ");
                SQL.AppendLine("    And F.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("Inner Join TblVendor I On F.VdCode=I.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm J On F.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblIndependentEstimatedPriceHdr M On C.MaterialRequestDocNo = M.MRDocNo ");
            SQL.AppendLine("Left JOIN ( ");
            SQL.AppendLine("SELECT DocNo from tblmaterialrequestdtl ");
            SQL.AppendLine("where MaterialRequestServiceDocNo is Not NULL ");
            SQL.AppendLine("GROUP BY DocNo ");
            SQL.AppendLine(") L On L.DocNo=E.DocNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
            SQL.AppendLine("    FROM tblpodtl A ");
            SQL.AppendLine("    Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo AND B.PoDocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
            SQL.AppendLine("    Where B.docno = (Select max(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
            SQL.AppendLine(") M ON A.DocNo = M.PoDocNo AND A.DNo = M.PoDNo ");
            SQL.AppendLine("Where A.CancelInd='N'  ");

            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory  ");
                SQL.AppendLine("    Where ItCtCode = H.ItCtCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPOServiceDocNo.Text, "L.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "K.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "H.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[] 
                        { 
                        //0
                        "DocNo",  
                        //1-5
                        "DNo", "PORequestDocNo", "PORequestDNo", "ItCode", "ItName", 
                        //6-10
                        "Qty", "PurchaseUomCode", "QtDocNo", "QtDNo", "VdCode",  
                        //11-15
                        "VdName", "PtName", "CurCode", "UPrice", "Discount", 
                        //16-20
                        "DiscountAmt", "RoundingValue", "EstRecvDt", "Total", "RemarkHdr", 
                        //21-25
                        "RemarkDtl", "EstTimeArrival", "ItCodeInternal", "Specification", "TaxCode1",  
                        //26-29
                        "POSDocNo", "GrandTotal", "AddendumAvailability", "AddendumInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 32, 29);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0)) 
            {
                if (Sm.GetGrdBool(Grd1, Grd1.CurRow.Index, 32) == true && Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 31) <= 0)
                    Sm.StdMsg(mMsgType.Info, "Item cannot be selected because Addendum Indicator is checked and Addendum Availability is 0 or less than 0");

                else
                {
                    mFrmParent.TxtPODocNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    mFrmParent.DNoPO = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                    mFrmParent.TxtPORDocNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.DNoPOR = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                    mFrmParent.TxtQtOld.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                    mFrmParent.DNoQtOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                    mFrmParent.mVdCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13);
                    mFrmParent.TxtVdCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);
                    mFrmParent.mItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                    mFrmParent.TxtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                    mFrmParent.TxtQtyOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                    mFrmParent.TxtQtyNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                    mFrmParent.TxtDiscountAmtOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19), 0);
                    mFrmParent.TxtDiscountAmtNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19), 0);
                    mFrmParent.TxtUnitPriceOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17), 0);
                    mFrmParent.TxtUnitPriceNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17), 0);
                    mFrmParent.TxtDiscOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 18), 0);
                    mFrmParent.TxtRoundingValueOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 20), 0);
                    mFrmParent.TxtTotalOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 21), 0);
                    mFrmParent.TxtDiscNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 18), 0);
                    mFrmParent.TxtRoundingValueNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 20), 0);
                    mFrmParent.TxtTotalNew.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 21), 0);
                    mFrmParent.MeePORemarkHdrOld.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 23);
                    mFrmParent.MeePORemarkDtlOld.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 24);
                    mFrmParent.MeePORemarkHdrNew.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 23);
                    mFrmParent.MeePORemarkDtlNew.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 24);
                    if (mFrmParent.mIsPORevisionUseETA)
                    {
                        Sm.SetDte(mFrmParent.DteEstTimeArrivalOld, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 25));
                        Sm.SetDte(mFrmParent.DteEstTimeArrivalNew, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 25));
                    }
                    if (mFrmParent.mIsPORevisionInfoOnlyEnabled)
                    {
                        Sm.SetLue(mFrmParent.LueTaxCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 28));
                        Sm.SetDte(mFrmParent.DteEstRecvDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 22));
                    }
                    mFrmParent.TxtPOServiceDocNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 29);

                    mFrmParent.TxtGrandTotal.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 30), 0);

                    if (Sm.GetGrdBool(Grd1, Grd1.CurRow.Index, 32) == false)
                        mFrmParent.TxtAddendumAvailability.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 31),0);
                    else
                        mFrmParent.TxtAddendumAvailability.Text = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 31) - Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 30), 0);
                    mFrmParent.mAddendumInd = Sm.GetGrdBool(Grd1, Grd1.CurRow.Index, 32);

                    mFrmParent.ComputeTotalNew();
                    mFrmParent.AddendumValidated();
                    this.Close();
                }
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        
        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void TxtPOServiceDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkPOServiceDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO For Service#");
        }
        #endregion

        
        #endregion

        

    }
}
