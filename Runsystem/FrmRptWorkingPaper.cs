﻿#region Update
/*
    28/04/2020 [DITA/SRN] new apps
    27/05/2020 [WED/SRN] item yang muncul berdasarkan master item nya, bukan dari stock nya
    28/05/2020 [WED/SRN] subcategory tidak mandatory
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWorkingPaper : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptWorkingPaper(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                //SetSQL();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueItScCode(ref LueSubCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select A.WhsCode, B.WhsName, A.ItCode, C.ItCodeInternal, C.ItName, C.ItCtCode, A.Qty Stock, C.ItScCode ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select WhsCode, ItCode, ");
            //SQL.AppendLine("    Sum(Qty) As Qty ");
            //SQL.AppendLine("    From TblStockSummary ");
            //SQL.AppendLine("    Where Qty<>0 ");
            //SQL.AppendLine("    Group By WhsCode, ItCode ");
            //SQL.AppendLine(") A ");
            //SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            //SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            #endregion

            SQL.AppendLine("SELECT B.WhsCode, B.WhsName, A.ItCode, A.ItCodeInternal, A.ItName, A.ItCtCode, IFNULL(C.Qty, 0.00) Stock, A.ItScCode ");
            SQL.AppendLine("FROM TblItem A ");
            SQL.AppendLine("INNER JOIN TblWarehouse B ON B.WhsCode = @WhsCode ");
            SQL.AppendLine("    AND A.ItCtCode = @ItCtCode ");
            if (Sm.GetLue(LueSubCtCode).Length > 0)
                SQL.AppendLine("    AND A.ItScCode = @ItScCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT X1.WhsCode, X1.ItCode, SUM(X1.Qty) Qty ");
            SQL.AppendLine("    FROM TblStockSummary X1 ");
            SQL.AppendLine("    INNER JOIN TblItem X2 ON X1.ItCode = X2.ItCode ");
	        SQL.AppendLine("         AND X2.ItCtCode = @ItCtCode ");
            if (Sm.GetLue(LueSubCtCode).Length > 0)
	            SQL.AppendLine("         AND X2.ItScCode = @ItScCode ");
            SQL.AppendLine("    WHERE X1.WhsCode = @WhsCode ");
            SQL.AppendLine("    GROUP BY X1.WhsCode, X1.ItCode ");
            SQL.AppendLine(") C ON A.ItCode = C.ItCode ");

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt)
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select A.WhsCode, B.WhsName, A.ItCode, C.ItCodeInternal, C.ItName, C.ItCtCode, A.Qty Stock, C.ItScCode ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select WhsCode, ItCode, ");
            //SQL.AppendLine("    Sum(Qty) As Qty ");
            //SQL.AppendLine("    From TblStockMovement ");
            //SQL.AppendLine("    Where DocDt<=" + DocDt);
            //SQL.AppendLine("    Group By WhsCode, ItCode ");
            //SQL.AppendLine("    Having (Sum(Qty)<>0) ");
            //SQL.AppendLine(") A ");
            //SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            //SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            #endregion

            SQL.AppendLine("SELECT B.WhsCode, B.WhsName, A.ItCode, A.ItCodeInternal, A.ItName, A.ItCtCode, IFNULL(C.Qty, 0.00) Stock, A.ItScCode ");
            SQL.AppendLine("FROM TblItem A ");
            SQL.AppendLine("INNER JOIN TblWarehouse B ON B.WhsCode = @WhsCode ");
            SQL.AppendLine("    AND A.ItCtCode = @ItCtCode ");
            if (Sm.GetLue(LueSubCtCode).Length > 0)
                SQL.AppendLine("    AND A.ItScCode = @ItScCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT X1.WhsCode, X1.ItCode, SUM(X1.Qty) Qty ");
            SQL.AppendLine("    FROM TblStockMovement X1 ");
            SQL.AppendLine("    INNER JOIN TblItem X2 ON X1.ItCode = X2.ItCode ");
            SQL.AppendLine("         AND X2.ItCtCode = @ItCtCode ");
            if (Sm.GetLue(LueSubCtCode).Length > 0)
                SQL.AppendLine("         AND X2.ItScCode = @ItScCode ");
            SQL.AppendLine("         AND X1.DocDt <= @DocDt ");
            SQL.AppendLine("    WHERE X1.WhsCode = @WhsCode ");
            SQL.AppendLine("    GROUP BY X1.WhsCode, X1.ItCode ");
            SQL.AppendLine(") C ON A.ItCode = C.ItCode ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Product's Code", 
                        "",
                        "Local Code",
                        "Item's Name", 

                        //6
                        "Final Stock"
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 100, 20, 100, 200, 
                        
                        //6
                        80
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 3});
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 6 });
            Sm.GrdColInvisible(Grd1, new int[] {  4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4}, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueItCtCode, "Item's Category") //||
                //Sm.IsLueEmpty(LueSubCtCode, "Sub Item's Category")
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter = string.Empty,
                    DocDt = (ChkDocDt.Checked) ? Sm.GetDte(DteDocDt).Substring(0, 8) : "";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueSubCtCode));
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));              

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    ((ChkDocDt.Checked && decimal.Parse(DocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                    SetSQL2(DocDt) : SetSQL()) +
                    Filter + " Order By A.ItCtCode, A.ItScCode; ",
                    new string[]
                    {
                        //0
                        "WhsName", 

                        //1-5
                        "ItCode", "ItCodeInternal", "ItName", "Stock"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);                  
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        override protected void PrintData()
        {
            var l = new List<WPHdr>();
            var lDtl = new List<WPDtl>();
            int Numb = 0;
            string[] TableName = { "WPHdr", "WPDtl" };
            List<IList> myLists = new List<IList>();
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            #region Header

            l.Add(new WPHdr()
            {
                WhsName = LueWhsCode.Text,
                DocDt = DteDocDt.Text,
                ItCtName = LueItCtCode.Text,
                ItScName = LueSubCtCode.Text,
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            });

            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                Numb = Numb + 1;

                lDtl.Add(new WPDtl()
                {
                    No = Numb,
                    ItCode = Sm.GetGrdStr(Grd1, i, 2),
                    ItName = Sm.GetGrdStr(Grd1, i, 5),
                    FinalStock = Sm.GetGrdDec(Grd1, i, 6),
                    DisplayStock = null,
                    WhsStock = null,
                    Balance = null,
                    Total = null,
                });
            }
            myLists.Add(lDtl);

            #endregion

            Sm.PrintReport("WorkingPaper", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
        }

        private void LueSubCtCode_EditValueChanged(object sender, EventArgs e)
        {
           Sm.RefreshLookUpEdit(LueSubCtCode, new Sm.RefreshLue1(Sl.SetLueItScCode));
           Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSubCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sub Category");
        }

        #endregion

        #region Class

        private class WPHdr
        {
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string ItCtName { get; set; }
            public string ItScName { get; set; }
            public string PrintBy { get; set; }
        }
        private class WPDtl
        {
            public int No { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal FinalStock { get; set; }
            public string DisplayStock { get; set; }
            public string WhsStock { get; set; }
            public string Total { get; set; }
            public string Balance { get; set; }
        }

        #endregion

        
    }
}
