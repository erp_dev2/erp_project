﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintCapitalStatement : RunSystem.FrmBase11
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPrintCapitalStatement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        
        protected override void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            Sl.SetLueAcNo(ref LueCOAAc);
        }

        protected override void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        private void ParPrint()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Periode") || Sm.IsDteEmpty(DteDocDt2, "Periode") || Sm.IsLueEmpty(LueCOAAc, "Account")) return;

            Cursor.Current = Cursors.WaitCursor;
            DateTime Dte1 = Sm.ConvertDate(Sm.GetDte(DteDocDt1).Substring(0, 8)).AddDays(-30);

            var Yr = Sm.GetDte(DteDocDt1).Substring(0, 4);
            var Mth = Sm.GetDte(DteDocDt1).Substring(5, 2);

            var lHdr = new List<PMHdr>();
            var l = new List<COA>();

            string[] TableName = { "PMHdr", "COA" };

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            SQL.AppendLine("DATE_FORMAT(@DocDt1, '%M %d, %Y')As DocDt1, DATE_FORMAT(@DocDt2, '%M %d, %Y') As DocDt2, ");
            SQL.AppendLine("(Select AcNo From TblCoa Where AcNo=@AcNo Limit 1) As AcNo,");
            SQL.AppendLine("(Select AcDesc From TblCoa Where AcNo=@AcNo Limit 1) As AcDesc");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueCOAAc));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "DocDt1",
                         //6-8
                         "DocDt2",
                         "Acno",
                         "AcDesc"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lHdr.Add(new PMHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            Period1 = Sm.DrStr(dr, c[5]),
                            Period2 = Sm.DrStr(dr, c[6]),
                            AcNo = Sm.DrStr(dr, c[7]),
                            AcDesc =  Sm.DrStr(dr, c[8]),
                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(lHdr);
            #endregion

            #region Detail
            try
            {
                var lCOA = new List<COA>();

                Process1(ref lCOA);
                if (lCOA.Count > 0)
                {
                    Process2(ref lCOA, Yr);
                    Process3(ref lCOA);
                    Process4(ref lCOA);
                    if (lCOA.Count > 0)
                    {
                        for (var i = 0; i < lCOA.Count; i++)
                        {
                            l.Add(new COA()
                            {
                                DocNo = lCOA[i].DocNo,
                                Mth = lCOA[i].Mth,
                                Dt=lCOA[i].Dt,
                                AcNo = lCOA[i].AcNo,
                                AcDesc = lCOA[i].AcDesc,
                                AcType = lCOA[i].AcType,
                                remark = lCOA[i].remark,
                                SaldoAwal = lCOA[i].SaldoAwal,
                                DAmt = lCOA[i].DAmt,
                                CAmt = lCOA[i].CAmt,
                                Balance = lCOA[i].Balance
                            });
                        }
                    }
                }
                myLists.Add(l);

                Sm.PrintReport("CapitalStatement", myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            #endregion
        }
        
        #endregion

        #region Additional Method

        //Set account berdasarkan account filter
        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText =
                    "Select T.AcNo, T.AcDesc, T.AcType From TblCOA T Where T.ActInd='Y' And T.AcNo='"+Sm.GetLue(LueCOAAc)+"' Order By T.AcNo;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            remark = string.Empty,
                            SaldoAwal = 0m,
                            CAmt = 0m,
                            DAmt = 0m,
                            Balance = 0m,
                            DocNo = string.Empty,
                            Mth = string.Empty,
                            Dt = string.Empty,
                        });
                    }
                }
                dr.Close();
            }
        }

        //ambil opening balance sebagai saldo awal untuk coa yang dipilih
        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int Temp = 0;

            SQL.AppendLine("Select A.DocNo,  B.AcNo, ifnull(B.Amt, 0) As Amt, Substring(A.DocDt, 5, 2) As Mth, Right(A.DocDt, 2) As Dt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.Docno ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select B.ACNo, A.Yr, MAX(Concat(A.DocDt, A.DocNo)) As Key1 ");
	        SQL.AppendLine("    From TblCOAOpeningbalancehdr A ");
	        SQL.AppendLine("    Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
	        SQL.AppendLine("    group By AcNo, A.Yr ");
            SQL.AppendLine(")C On Concat(A.DocDt, A.DocNo) = C.key1 And B.AcNo = C.AcNo And A.Yr = C.Yr ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Yr=@Yr ");
            SQL.AppendLine("And B.Acno=@AcNo ");
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueCOAAc));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "Amt", "Mth", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < lCOA.Count; i++)
                        {
                            if (string.Compare(lCOA[i].AcNo, dr.GetString(1)) == 0)
                            {
                                lCOA[i].DocNo = dr.GetString(0);
                                lCOA[i].remark = "Opening Balance";
                                lCOA[i].SaldoAwal = dr.GetDecimal(2);
                                lCOA[i].Balance = dr.GetDecimal(2);
                                lCOA[i].Mth = dr.GetString(3);
                                lCOA[i].Dt = dr.GetString(4);
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //ambil data journal berdasarkan periode untuk coa yang dipilih 
        private void Process3(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int Temp = 0;

            SQL.AppendLine("Select B.DocNo, B.AcNo, ifnull(B.DAmt, 0) As DAmt, ifnull(B.Camt, 0) CAmt, ifnull(B.Remark, '-') Remark, ");
            SQL.AppendLine("Substring(A.DocDt, 5, 2) As Mth, Right(A.DocDt, 2) As Dt ");
            SQL.AppendLine("FROM TbljournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.docNO = B.DocNO ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And B.AcNo=@AcNo And (B.CAmt+B.Damt) >0 ");
            SQL.AppendLine("Order By A.DocDt;");

            Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueCOAAc));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "DAmt", "CAmt", "Remark", "Mth", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            DocNo = dr.GetString(0), 
                            AcNo = lCOA[0].AcNo,
                            AcDesc = lCOA[0].AcDesc,
                            AcType = lCOA[0].AcType,
                            remark = dr.GetString(4),
                            SaldoAwal = 0m,
                            CAmt = dr.GetDecimal(2),
                            DAmt = dr.GetDecimal(3),
                            Balance = 0m,
                            Mth = dr.GetString(5),
                            Dt = dr.GetString(6),
                        });
                    }
                }
                dr.Close();
            }
        }

        //hitung saldo awal dan balance tiap row 
        private void Process4(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (i == 0)
                {
                    lCOA[i].SaldoAwal = lCOA[0].SaldoAwal;
                    lCOA[i].Balance = lCOA[0].Balance;
                }
                else
                {
                    lCOA[i].SaldoAwal = lCOA[i - 1].Balance;
                    if (lCOA[i].AcType == "D")
                        lCOA[i].Balance = lCOA[i].SaldoAwal + lCOA[i].DAmt - lCOA[i].CAmt;
                    if (lCOA[i].AcType == "C")
                        lCOA[i].Balance = lCOA[i].SaldoAwal + lCOA[i].CAmt - lCOA[i].DAmt;
                }
            }
        }

        #endregion

        #region Class

        private class PMHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Period1 { get; set; }
            public string Period2 { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        private class COA
        {
            public string DocNo { get; set; }
            public string Mth { get; set; }
            public string Dt { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string remark { get; set; }
            public decimal SaldoAwal { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }



}
