﻿#region Update
/*
    27/09/2018 [TKG] tambah journal
    28/09/2018 [TKG] bug business partner name tidak muncul
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    20/05/2019 [TKG] ubah proses journal pada saat cancel berdasarkan monthly closing.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGiroSettlement : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string
            mSQL = string.Empty,
            mMainCurCode = string.Empty,
            mCreateDocType = "09", // lihat option GiroDocType
            mCancelDocType = "10"; // lihat option GiroDocType
        private bool mIsAutoJournalActived = false;
        internal FrmGiroSettlementFind FrmFind;

        #endregion

        #region Constructor

        public FrmGiroSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                LblBusinessPartnerType.Visible = LueBusinessPartnerType.Visible = false;
                SetLueBusinessPartnerType(ref LueBusinessPartnerType);
                Sl.SetLueBankCode(ref LueBankCode);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, MeeRemark
                    }, true);
                    TxtDocNo.Focus();
                    BtnBusinesPartnerCode.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    BtnBusinesPartnerCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, MeeCancelReason, MeeRemark, TxtBusinesPartnerCode,
                 LueBusinessPartnerType, LueBusinessPartnerName, TxtGiroNo, LueBankCode, TxtCurCode,
                 TxtJournalDocNo, TxtJournalDocNo2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGiroSettlementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmGiroSettlementDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData();
                else EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                var SQL = new StringBuilder();
                string BusinessPartnerCode = string.Empty, BusinessPartnerType = string.Empty;

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.BusinessPartnerCode, ");
                SQL.AppendLine("A.BusinessPartnerType, A.BankCode, A.GiroNo, A.Remark, B.CurCode, B.Amt, A.JournalDocNo, A.JournalDocNo2 ");
                SQL.AppendLine("From TblGiroSettlement A ");
                SQL.AppendLine("Inner Join TblGiroSummary B On A.BusinessPartnerCode = B.BusinessPartnerCode ");
                SQL.AppendLine("    And A.BusinessPartnerType = B.BusinessPartnerType ");
                SQL.AppendLine("    And A.GiroNo = B.GiroNo ");
                SQL.AppendLine("    And A.BankCode = B.BankCode ");
                SQL.AppendLine("Where A.DocNo = @DocNo; ");

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        "DocNo", 
                        "DocDt", "CancelReason", "CancelInd", "BusinessPartnerCode", "BusinessPartnerType",
                        "BankCode", "GiroNo", "Remark", "CurCode", "Amt",
                        "JournalDocNo", "JournalDocNo2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        BusinessPartnerCode = Sm.DrStr(dr, c[4]);
                        BusinessPartnerType = Sm.DrStr(dr, c[5]);
                        TxtBusinesPartnerCode.EditValue = BusinessPartnerCode;
                        SetLueBusinessPartnerName(ref LueBusinessPartnerName, BusinessPartnerType);
                        Sm.SetLue(LueBusinessPartnerType, BusinessPartnerType);
                        Sm.SetLue(LueBusinessPartnerName, BusinessPartnerCode);
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[6]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[11]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
            ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GiroSettlement", "TblGiroSettlement");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveGiroSettlement(DocNo));
            cml.Add(SaveGiroMovement(DocNo, 1));
            cml.Add(UpdateGiroSummary(1));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtBusinesPartnerCode, "Business Partner#", false) ||
                Sm.IsLueEmpty(LueBusinessPartnerType, "Business Partner Type") ||
                Sm.IsLueEmpty(LueBankCode, "Bank") ||
                Sm.IsTxtEmpty(TxtGiroNo, "Giro#", false) ||
                IsGiroSummaryNotActive();
        }

        private bool IsGiroSummaryNotActive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblGiroSummary ");
            SQL.AppendLine("Where GiroNo = @Param1 ");
            SQL.AppendLine("And BusinessPartnerCode = @Param2 ");
            SQL.AppendLine("And BusinessPartnerType = @Param3 ");
            SQL.AppendLine("And BankCode = @Param4 ");
            SQL.AppendLine("And ActInd = 'N'; ");

            return Sm.IsDataExist(SQL.ToString(), TxtGiroNo.Text, TxtBusinesPartnerCode.Text, Sm.GetLue(LueBusinessPartnerType), Sm.GetLue(LueBankCode));
        }

        private MySqlCommand SaveGiroSettlement(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGiroSettlement(DocNo, DocDt, CancelInd, BusinessPartnerCode, ");
            SQL.AppendLine("BusinessPartnerType, BankCode, GiroNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @BusinessPartnerCode, ");
            SQL.AppendLine("@BusinessPartnerType, @BankCode, @GiroNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", TxtBusinesPartnerCode.Text);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", Sm.GetLue(LueBusinessPartnerType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGiroMovement(string DocNo, byte Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGiroMovement(DocType, DocNo, BankCode, GiroNo, BusinessPartnerCode, ");
            SQL.AppendLine("BusinessPartnerType, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocType, @DocNo, @BankCode, @GiroNo, @BusinessPartnerCode, ");
            SQL.AppendLine("@BusinessPartnerType, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", (Type == 1) ? mCreateDocType : mCancelDocType);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", TxtBusinesPartnerCode.Text);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", Sm.GetLue(LueBusinessPartnerType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateGiroSummary(byte Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblGiroSummary ");
            SQL.AppendLine("Set ActInd = @ActInd, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where BusinessPartnerCode = @BusinessPartnerCode ");
            SQL.AppendLine("And BusinessPartnerType = @BusinessPartnerType ");
            SQL.AppendLine("And BankCode = @BankCode ");
            SQL.AppendLine("And GiroNo = @GiroNo ");
            SQL.AppendLine("And ActInd = @ActInd2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ActInd", (Type == 1) ? "N" : "Y");
            Sm.CmParam<String>(ref cm, "@ActInd2", (Type == 1) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", TxtBusinesPartnerCode.Text);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", Sm.GetLue(LueBusinessPartnerType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private string GetEntity()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct EntCode From TblVoucherHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And EntCode Is Not Null ");
            SQL.AppendLine("And DocNo In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblGiroMovement ");
            SQL.AppendLine("    Where DocType='01' ");
            SQL.AppendLine("    And BusinessPartnerCode=@BusinessPartnerCode ");
            SQL.AppendLine("    And BusinessPartnerType=@BusinessPartnerType ");
            SQL.AppendLine("    And BankCode=@BankCode ");
            SQL.AppendLine("    And GiroNo=@GiroNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By CreateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", TxtBusinesPartnerCode.Text);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", Sm.GetLue(LueBusinessPartnerType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);

            return Sm.GetValue(cm);
        }

        private string GetBankAccount()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode From TblVoucherHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And DocNo In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblGiroMovement ");
            SQL.AppendLine("    Where DocType='01' ");
            SQL.AppendLine("    And BusinessPartnerCode=@BusinessPartnerCode ");
            SQL.AppendLine("    And BusinessPartnerType=@BusinessPartnerType ");
            SQL.AppendLine("    And BankCode=@BankCode ");
            SQL.AppendLine("    And GiroNo=@GiroNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By CreateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", TxtBusinesPartnerCode.Text);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", Sm.GetLue(LueBusinessPartnerType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);

            return Sm.GetValue(cm);
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            string 
                EntCode = GetEntity(), 
                BankAcCode = GetBankAccount();

            SQL.AppendLine("Update TblGiroSettlement Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(" Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Giro Settlement : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblGiroSettlement Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T1.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T2.AcNo, T2.DAmt, T2.CAmt, @EntCode, T1.CreateBy, T1.CreateDt ");
            SQL.AppendLine("From TblJournalHdr T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.AcNo, Sum(T.Amt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Concat(C.ParValue, A.BusinessPartnerCode) As AcNo, ");
            SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * B.Amt As Amt ");
            SQL.AppendLine("        From TblGiroSettlement A ");
            SQL.AppendLine("        Inner Join TblGiroSummary B ");
            SQL.AppendLine("            On A.BusinessPartnerCode=B.BusinessPartnerCode ");
            SQL.AppendLine("            And A.BusinessPartnerType=B.BusinessPartnerType ");
            SQL.AppendLine("            And A.BankCode=B.BankCode ");
            SQL.AppendLine("            And A.GiroNo=B.GiroNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='AcNoForGiroAP' And C.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.AcNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T.AcNo, 0.00 As DAmt, Sum(T.Amt) As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.COAAcNo As AcNo, ");
            SQL.AppendLine("        Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * B.Amt As Amt ");
            SQL.AppendLine("        From TblGiroSettlement A ");
            SQL.AppendLine("        Inner Join TblGiroSummary B ");
            SQL.AppendLine("            On A.BusinessPartnerCode=B.BusinessPartnerCode ");
            SQL.AppendLine("            And A.BusinessPartnerType=B.BusinessPartnerType ");
            SQL.AppendLine("            And A.BankCode=B.BankCode ");
            SQL.AppendLine("            And A.GiroNo=B.GiroNo ");
            SQL.AppendLine("        Inner Join ( " );
            SQL.AppendLine("            Select Distinct COAAcNo ");
            SQL.AppendLine("            From TblBankAccount ");
            SQL.AppendLine("            Where COAAcNo Is Not Null ");
            SQL.AppendLine("            And BankAcCode=@BankAcCode ");
            SQL.AppendLine("        ) C On 1=1 ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Where T.AcNo Is Not Null ");
            SQL.AppendLine("    Group By T.AcNo ");
            SQL.AppendLine(") T2 On 1=1 ");
            SQL.AppendLine("Where T1.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblGiroSettlement ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelGiroSettlement());
            cml.Add(SaveGiroMovement(TxtDocNo.Text, 2));
            cml.Add(UpdateGiroSummary(2));

            if (mIsAutoJournalActived && ChkCancelInd.Checked) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsPOAlreadyReceived();
        }

        private bool IsPOAlreadyReceived()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.PODocNo ");
            SQL.AppendLine("From tblgirosummary A ");
            SQL.AppendLine("Inner Join TblGiroMovement B On  ");
	        SQL.AppendLine("    A.GiroNo = B.GiroNo ");
	        SQL.AppendLine("    And A.BusinessPartnerCode=B.BusinessPartnerCode ");
	        SQL.AppendLine("    And A.BusinessPartnerType=B.BusinessPartnerType ");
	        SQL.AppendLine("    And A.BankCode=B.BankCode ");
	        SQL.AppendLine("    And B.DocType='01' ");
            SQL.AppendLine("Inner Join tblvoucherhdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join tblapdownpayment D On C.VoucherRequestDocNo=D.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join tblrecvvddtl E On D.PODocNo = E.DocNo And E.CancelInd = 'N' And E.Status In ('O', 'A') ");
            SQL.AppendLine("Where A.GiroNo = @Param1 ");
            SQL.AppendLine("And A.BusinessPartnerCode=@Param2 ");
            SQL.AppendLine("And A.BusinessPartnerType=@Param3 ");
            SQL.AppendLine("And A.BankCode=@Param4 ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtGiroNo.Text, TxtBusinesPartnerCode.Text, Sm.GetLue(LueBusinessPartnerType), Sm.GetLue(LueBankCode)))
            {
                Sm.StdMsg(mMsgType.Warning, "PO that using this giro is already received.");
                return true;
            }

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select DocNo From TblGiroSettlement Where CancelInd='Y' And DocNo=@Param; ", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelGiroSettlement()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblGiroSettlement Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();

            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine(Sm.GetNewJournalDocNo(CurrentDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo(DocDt, 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblGiroSettlement Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");

            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblGiroSettlement ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblGiroSettlement ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        internal void ShowGiroSummaryData(string BusinessPartnerCode, string BusinessPartnerType, string BankCode, string GiroNo)
        {
            SetLueBusinessPartnerName(ref LueBusinessPartnerName, BusinessPartnerType);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select BusinessPartnerCode, BusinessPartnerType, BankCode, GiroNo, CurCode, Amt ");
            SQL.AppendLine("From TblGiroSummary ");
            SQL.AppendLine("Where BusinessPartnerCode = @BusinessPartnerCode ");
            SQL.AppendLine("And BusinessPartnerType = @BusinessPartnerType ");
            SQL.AppendLine("And BankCode = @BankCode ");
            SQL.AppendLine("And GiroNo = @GiroNo; ");

            Sm.CmParam<String>(ref cm, "@BusinessPartnerCode", BusinessPartnerCode);
            Sm.CmParam<String>(ref cm, "@BusinessPartnerType", BusinessPartnerType);
            Sm.CmParam<String>(ref cm, "@BankCode", BankCode);
            Sm.CmParam<String>(ref cm, "@GiroNo", GiroNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "BusinessPartnerCode",

                    //1-5
                    "BusinessPartnerType",
                    "BankCode",
                    "GiroNo",
                    "CurCode",
                    "Amt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtBusinesPartnerCode.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetLue(LueBusinessPartnerName, Sm.DrStr(dr, c[0]));
                    Sm.SetLue(LueBusinessPartnerType, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[2]));
                    TxtGiroNo.EditValue = Sm.DrStr(dr, c[3]);
                    TxtCurCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                }, true
            );
        }

        private void SetLueBusinessPartnerType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Vendor' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Customer' As Col2 ");
            
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueBusinessPartnerName(ref DXE.LookUpEdit Lue, string BusinessPartnerType)
        {
            var SQL = new StringBuilder();

            if (BusinessPartnerType == "1")
            {
                SQL.AppendLine("Select VdCode As Col1, VdName As Col2 ");
                SQL.AppendLine("From TblVendor;");
            }
            else
            {
                SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                SQL.AppendLine("From TblCustomer;");
            }

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeTrim(MeeCancelReason);
            ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        private void LueBusinessPartnerType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBusinessPartnerType, new Sm.RefreshLue1(SetLueBusinessPartnerType));
        }

        private void LueBusinessPartnerName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBusinessPartnerName, new Sm.RefreshLue2(SetLueBusinessPartnerName), string.Empty);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        #endregion

        #region Button Click

        private void BtnBusinesPartnerCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmGiroSettlementDlg(this));
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
