﻿#region Update
/*
    14/04/2021 [WED/ALL] passing VdCode dan tambah parameter IsOutgoingPaymentAmtUseCOAAmt
    21/04/2021 [WED/ALL] berdasarkan parameter IsCOACouldBeChosenMoreThanOnce, maka COA selain VendorAcNoAP bisa dipilih lebih dari sekali
    24/12/2021 [RDA/AMKA] validasi coa item category berdasarkan param IsOutgoingPaymentAmtUseCOAAmt, ketika param off maka validasi melihat param VendorAcNoAP
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPayment2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmOutgoingPayment2 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmOutgoingPayment2Dlg3(FrmOutgoingPayment2 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of COA's Account#";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                if (mFrmParent.mIsOutgoingPaymentAmtUseCOAAmt)
                {
                    SetSQL(1);
                    ShowData2();
                }
                else
                    SetSQL(2);
                Sl.SetLueAcCtCode(ref LueAcCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 300, 150, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetSQL(byte Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcTypeDesc, C.AcCtName ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");
            SQL.AppendLine("And Locate(Concat('##', A.AcNo, '##'), @SelectedAcNo)<1 ");
            if (Type == 1)
            {
                if (mFrmParent.mIsOutgoingPaymentAmtUseCOAAmt)
                {
                    SQL.AppendLine("    And ( ");
                    SQL.AppendLine("        A.AcNo In ('"+ GetAcNoItem() +"') ");
                    SQL.AppendLine("        Or ");
                    SQL.AppendLine("        A.AcNo In (Select ParValue From TblParameter Where ParCode = 'AdditionalCostCOAForOutgoingPayment' And ParValue Is Not Null) ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    SQL.AppendLine("    And ( ");
                    SQL.AppendLine("        A.AcNo In (Select Concat(ParValue, @VdCode) From TblParameter Where ParCode = 'VendorAcNoAP' And ParValue Is not Null) ");
                    SQL.AppendLine("        Or ");
                    SQL.AppendLine("        A.AcNo In (Select ParValue From TblParameter Where ParCode = 'AdditionalCostCOAForOutgoingPayment' And ParValue Is Not Null) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("And A.ActInd='Y' ");

            mSQL = string.Empty;

            mSQL = SQL.ToString();
        }

        private void ShowData2()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (mFrmParent.mIsOutgoingPaymentAmtUseCOAAmt)
                {

                    string Filter = " And 0=0 ";

                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@SelectedAcNo", mFrmParent.GetSelectedAcNo());
                    Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);

                    Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                    Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);

                    Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "AcTypeDesc", "AcCtName"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            }, true, false, false, false
                    );
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedAcNo", mFrmParent.GetSelectedAcNo());
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);

                if (mFrmParent.mIsOutgoingPaymentAmtUseCOAAmt)
                    SetSQL(2);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "AcTypeDesc", "AcCtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAcNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd4.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd4, Row1, 2, Grd1, Row2, 3);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd4, Row1, new int[] { 3, 4 });

                        mFrmParent.Grd4.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd4, mFrmParent.Grd4.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsAcNoAlreadyChosen(int Row)
        {
            string AcNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd4.Rows.Count - 1; Index++)
            {
                if (mFrmParent.mIsCOACouldBeChosenMoreThanOnce)
                {
                    if (AcNo == string.Concat(mFrmParent.mVendorAcNoAP, mVdCode))
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 1), AcNo)) return true;
                    }
                }
                else
                    if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd4, Index, 1), AcNo)) return true;
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private string GetAcNoItem()
        {
            var SQL = new StringBuilder();
            string AcNo = string.Empty;

            SQL.AppendLine("Select Group_Concat(Distinct(B.AcNo9)) ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Where Find_In_Set(A.ItCode, @Param); ");

            return AcNo = Sm.GetValue(SQL.ToString(), GetRecvVdItCode());
        }

        private string GetRecvVdItCode()
        {
            string mItCode = string.Empty;

            for (int i = 0; i < mFrmParent.Grd5.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd5, i, 3).Length > 0) mItCode += ",";
                mItCode += Sm.GetGrdStr(mFrmParent.Grd5, i, 3);
            }

            return (mItCode.Length > 0 ? mItCode : "XXX");
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        #endregion

        #endregion

    }
}
