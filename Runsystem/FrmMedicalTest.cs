﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmMedicalTest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mEmpCode = string.Empty;
        internal FrmMedicalTestFind FrmFind;
      
        #endregion

        #region Constructor

        public FrmMedicalTest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                tabControl1.SelectTab("TpgAmnesis");
                SetLue01(ref Lue01);
                SetLue02(ref Lue02);
                SetLue03(ref Lue03);
                SetLue04(ref Lue04);
                SetLue05(ref Lue05);

                tabControl1.SelectTab("TpgFisik");
                tabControl1.SelectTab("TpgAmnesis");

                //if this application is called from other application
                if (mEmpCode.Length != 0)
                {
                    ShowData(mEmpCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
             if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {                                                                                   
                        TxtDocNo, DteDocDt, TxtRecruitmentDocNo, TxtRecruitmentName, TxtGender, TxtUmur, TxtPosCode, TxtDeptCode, TxtCompany,
                        Lue01, Lue02, TxtPenyakit02, Lue03, TxtPenyakit03, Lue04, Lue05, TxtBB, TxtTB, TxtTD, TxtDN,
                        MeeCondition, MeeAwareness, MeeEkstremitas, MeeNutrition, MeeLiver, MeeNose, MeeHeart, MeeConclusion,
                        MeeClinic, MeeLab, MeeNeck, MeeEye, MeeMouth, MeeStomach, MeeEar
                    }, true);
                    TxtDocNo.Focus();
                    BtnShowEmployeeRecruitment.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtCompany, Lue01, Lue02, TxtPenyakit02, Lue03, TxtPenyakit03, Lue04, Lue05, TxtBB, TxtTB, TxtTD, TxtDN,
                        MeeCondition, MeeAwareness, MeeEkstremitas, MeeNutrition, MeeLiver, MeeNose, MeeHeart, MeeConclusion,
                        MeeClinic, MeeLab, MeeNeck, MeeEye, MeeMouth, MeeStomach, MeeEar
                    }, false);
                    DteDocDt.Focus();
                    BtnShowEmployeeRecruitment.Enabled = true;
                    Sm.FormatNumTxt(TxtBB, 0);
                    Sm.FormatNumTxt(TxtTB, 0);
                    Sm.FormatNumTxt(TxtTD, 0);
                    Sm.FormatNumTxt(TxtDN, 0);
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtCompany, Lue01, Lue02, TxtPenyakit02, Lue03, TxtPenyakit03, Lue04, Lue05, TxtBB, TxtTB, TxtTD, TxtDN,
                       MeeCondition, MeeAwareness, MeeEkstremitas, MeeNutrition, MeeLiver, MeeNose, MeeHeart, MeeConclusion,
                       MeeClinic, MeeLab, MeeNeck, MeeEye, MeeMouth, MeeStomach, MeeEar
                    }, false);
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtRecruitmentDocNo, TxtRecruitmentName, TxtGender, TxtUmur, TxtPosCode, TxtDeptCode, TxtCompany,
                Lue01, Lue02, TxtPenyakit02, Lue03, TxtPenyakit03, Lue04, Lue05, TxtBB, TxtTB, TxtTD, TxtDN,
                MeeCondition, MeeAwareness, MeeEkstremitas, MeeNutrition, MeeLiver, MeeNose, MeeHeart, MeeConclusion,
                MeeClinic, MeeLab, MeeNeck, MeeEye, MeeMouth, MeeStomach, MeeEar
            });
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtBB, TxtTB, TxtTD, TxtDN
            }, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
               if (FrmFind == null) FrmFind = new FrmMedicalTestFind(this);
               Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);

        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);

        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = string.Empty;
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MedicalTest", "TblMedicalTest");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveMedicalTest(DocNo));

                Sm.ExecCommands(cml);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                //ClearData();
                ShowMedicalTest(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMedicalTest(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

             var SQL = new StringBuilder();

             SQL.AppendLine("Select A.DocNo, A.DocDt, A.RecruitmentDocNo, B.EmpName, E.OptDesc As Gender, Ifnull(YEAR(CURDATE())-Left(B.BirthDt,4),'0')As Umur, ");
             SQL.AppendLine("C.PosName, D.DeptName, A.Company, A.Anamnesis01, A.Anamnesis02, A.Anamnesis021,A.Anamnesis03, A.Anamnesis031, "); 
             SQL.AppendLine("A.Anamnesis04, A.Anamnesis05, A.BB, A.TB, A.TD, A.DN, A.Conditions, A.Awareness, A.Nutrition, A.Eye, A.Nose, A.Mouth, A.Ear, A.Neck, ");
             SQL.AppendLine("A.Heart, A.Liver, A.Stomach, A.Ekstremitas, A.Lab, A.Clinic, A.Conclusion ");
             SQL.AppendLine("From TblMedicalTest A ");
             SQL.AppendLine("Inner Join tblemployeerecruitment B On A.RecruitmentDocNo=B.DocNo");
             SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
             SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
             SQL.AppendLine("Left Join TblOption E On E.OptCat='Gender' And B.Gender=E.OptCode");
             SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "RecruitmentDocNo", "EmpName", "Gender", "Umur", 

                            //6-10
                            "PosName", "DeptName", "Company", "Anamnesis01", "Anamnesis02", 

                            //11-15
                            "Anamnesis021", "Anamnesis03", "Anamnesis031", "Anamnesis04", "Anamnesis05",

                            //16-20
                            "BB", "TB", "TD", "DN", "Conditions", 

                            //21-25
                            "Awareness", "Nutrition", "Eye", "Nose", "Mouth", 

                            //26-30
                            "Ear", "Neck", "Heart", "Liver", "Stomach",  

                            //31-34
                            "Ekstremitas", "Lab", "Clinic", "Conclusion"

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = DocNo;

                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtRecruitmentDocNo.EditValue = Sm.DrStr(dr, c[2]);
                            TxtRecruitmentName.EditValue = Sm.DrStr(dr, c[3]);
                            TxtGender.EditValue = Sm.DrStr(dr, c[4]);
                            TxtUmur.EditValue = Sm.DrStr(dr, c[5]);
                            TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                            TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                            TxtCompany.EditValue = Sm.DrStr(dr, c[8]);
                            Sm.SetLue(Lue01, Sm.DrStr(dr, c[9]));
                            Sm.SetLue(Lue02, Sm.DrStr(dr, c[10]));

                            TxtPenyakit02.EditValue = Sm.DrStr(dr, c[11]);
                            Sm.SetLue(Lue03, Sm.DrStr(dr, c[12]));
                            TxtPenyakit03.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(Lue04, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(Lue05, Sm.DrStr(dr, c[15]));
                            TxtBB.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[16]),0);
                            TxtTB.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[17]),0);
                            TxtTD.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[18]),0);
                            TxtDN.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[19]),0);
                            MeeCondition.EditValue = Sm.DrStr(dr, c[20]); 

                            MeeAwareness.EditValue = Sm.DrStr(dr, c[21]);
                            MeeNutrition.EditValue = Sm.DrStr(dr, c[22]);
                            MeeEye.EditValue = Sm.DrStr(dr, c[23]);
                            MeeNose.EditValue = Sm.DrStr(dr, c[24]);
                            MeeMouth.EditValue = Sm.DrStr(dr, c[25]);
                            MeeEar.EditValue = Sm.DrStr(dr, c[26]);
                            MeeNeck.EditValue = Sm.DrStr(dr, c[27]);
                            MeeHeart.EditValue = Sm.DrStr(dr, c[28]);
                            MeeLiver.EditValue = Sm.DrStr(dr, c[29]);
                            MeeStomach.EditValue = Sm.DrStr(dr, c[30]); 

                            MeeEkstremitas.EditValue = Sm.DrStr(dr, c[31]);
                            MeeLab.EditValue = Sm.DrStr(dr, c[32]);
                            MeeClinic.EditValue = Sm.DrStr(dr, c[33]);
                            MeeConclusion.EditValue = Sm.DrStr(dr, c[34]);
                           
                        }, false
                    );
            }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtRecruitmentDocNo, "Document Recruitment ", false)
                ;
        }

        private MySqlCommand SaveMedicalTest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMedicalTest(DocNo, DocDt, RecruitmentDocNo, Company, Anamnesis01, Anamnesis02, Anamnesis021, Anamnesis03, Anamnesis031,");
            SQL.AppendLine("Anamnesis04, Anamnesis05, BB, TB, TD, DN, Conditions, Awareness, Nutrition, Eye, Nose, Mouth, Ear, Neck, Heart, Liver, Stomach, Ekstremitas,");
            SQL.AppendLine("Lab, Clinic, Conclusion, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @RecruitmentDocNo, @Company, @Anamnesis01, @Anamnesis02, @Anamnesis021, @Anamnesis03, @Anamnesis031, ");
            SQL.AppendLine("@Anamnesis04, @Anamnesis05, @BB, @TB, @TD, @DN, @Conditions, @Awareness, @Nutrition, @Eye, @Nose, @Mouth, @Ear, @Neck, @Heart, @Liver, @Stomach, @Ekstremitas,");
            SQL.AppendLine("@Lab, @Clinic, @Conclusion, @UserCode, CurrentDateTime())");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    Company=@Company, Anamnesis01=@Anamnesis01, Anamnesis02=@Anamnesis02, Anamnesis021=@Anamnesis021, Anamnesis03=@Anamnesis03, Anamnesis031=@Anamnesis031, ");
            SQL.AppendLine("    Anamnesis04=@Anamnesis04, Anamnesis05=@Anamnesis05, BB=@BB, TB=@TB, TD=@TD, DN=@DN, Conditions=@Conditions, Awareness=@Awareness, Nutrition=@Nutrition, Eye=@Eye, Nose=@Nose, Mouth=@Mouth, Ear=@Ear, Neck=@Neck, Heart=@Heart, Liver=@Liver, Stomach=@Stomach, Ekstremitas=@Ekstremitas, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
                                
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            if (TxtDocNo.Text.Length > 0)
            {
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            }
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@RecruitmentDocNo", TxtRecruitmentDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Company", TxtCompany.Text);
            Sm.CmParam<String>(ref cm, "@Anamnesis01", Sm.GetLue(Lue01));
            Sm.CmParam<String>(ref cm, "@Anamnesis02", Sm.GetLue(Lue02));
            Sm.CmParam<String>(ref cm, "@Anamnesis021", TxtPenyakit02.Text);
            Sm.CmParam<String>(ref cm, "@Anamnesis03", Sm.GetLue(Lue03));
            Sm.CmParam<String>(ref cm, "@Anamnesis031", TxtPenyakit03.Text);
            Sm.CmParam<String>(ref cm, "@Anamnesis04", Sm.GetLue(Lue04));
            Sm.CmParam<String>(ref cm, "@Anamnesis05", Sm.GetLue(Lue05));
            Sm.CmParam<Decimal>(ref cm, "@BB", Decimal.Parse(TxtBB.Text));
            Sm.CmParam<Decimal>(ref cm, "@TB", Decimal.Parse(TxtTB.Text));
            Sm.CmParam<Decimal>(ref cm, "@TD", Decimal.Parse(TxtTD.Text));
            Sm.CmParam<Decimal>(ref cm, "@DN", Decimal.Parse(TxtDN.Text));
            Sm.CmParam<String>(ref cm, "@Conditions", MeeCondition.Text);
            Sm.CmParam<String>(ref cm, "@Awareness", MeeAwareness.Text);
            Sm.CmParam<String>(ref cm, "@Nutrition", MeeNutrition.Text);
            Sm.CmParam<String>(ref cm, "@Eye", MeeEye.Text);
            Sm.CmParam<String>(ref cm, "@Nose", MeeNose.Text);
            Sm.CmParam<String>(ref cm, "@Mouth", MeeMouth.Text);
            Sm.CmParam<String>(ref cm, "@Ear", MeeEar.Text);
            Sm.CmParam<String>(ref cm, "@Neck", MeeNeck.Text);
            Sm.CmParam<String>(ref cm, "@Heart", MeeHeart.Text);
            Sm.CmParam<String>(ref cm, "@Liver", MeeLiver.Text);
            Sm.CmParam<String>(ref cm, "@Stomach", MeeStomach.Text);
            Sm.CmParam<String>(ref cm, "@Ekstremitas", MeeEkstremitas.Text);
            Sm.CmParam<String>(ref cm, "@Lab", MeeLab.Text);
            Sm.CmParam<String>(ref cm, "@Clinic", MeeClinic.Text);
            Sm.CmParam<String>(ref cm, "@Conclusion", MeeConclusion.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        public static void SetLue01(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '01' As Col1, 'Yes' As Col2 Union All " +
                "select '02','No'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLue02(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
               "Select '01' As Col1, 'Yes' As Col2 Union All " +
               "Select '02','No'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLue03(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '01' As Col1, 'Yes' As Col2 Union All " +
                "Select '02','No'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLue04(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '01' As Col1, 'Yes' As Col2 Union All " +
                "Select '02','No'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLue05(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '01' As Col1, 'Yes' As Col2 Union All " +
                "Select '02','No'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void BtnShowEmployeeRecruitment_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMedicalTestDlg(this));
        }

        private void TxtBB_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtBB, 0);
        }

        private void TxtTB_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTB, 0);
        }

        private void TxtTD_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTD, 0);
        }

        private void TxtDN_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDN, 0);
        }

        private void Lue01_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(Lue01, new Sm.RefreshLue1(SetLue01));
        }

        private void Lue02_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(Lue02, new Sm.RefreshLue1(SetLue02));
        }

        private void Lue03_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(Lue03, new Sm.RefreshLue1(SetLue03));
        }

        private void Lue04_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(Lue04, new Sm.RefreshLue1(SetLue04));
        }

        private void Lue05_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(Lue05, new Sm.RefreshLue1(SetLue05));
        }
       
        #endregion

    }
}
