﻿namespace RunSystem
{
    partial class FrmYearlyClosingJournalEntryDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAccountNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkAccountNo = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkAcDesc = new DevExpress.XtraEditors.CheckEdit();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkChildInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccountNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAccountNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkChildInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(684, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Size = new System.Drawing.Size(68, 473);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(68, 22);
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefresh.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnRefresh.Size = new System.Drawing.Size(68, 31);
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnChoose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnChoose.Size = new System.Drawing.Size(68, 31);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnPrint.Size = new System.Drawing.Size(68, 31);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnExcel.Size = new System.Drawing.Size(68, 31);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.LueLevel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.ChkAcDesc);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtAccountNo);
            this.panel2.Controls.Add(this.ChkAccountNo);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panel2.Size = new System.Drawing.Size(684, 80);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 25;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(684, 393);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            // 
            // BtnClose
            // 
            this.BtnClose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnClose.Appearance.Options.UseBackColor = true;
            this.BtnClose.Appearance.Options.UseFont = true;
            this.BtnClose.Appearance.Options.UseForeColor = true;
            this.BtnClose.Appearance.Options.UseTextOptions = true;
            this.BtnClose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnClose.Size = new System.Drawing.Size(68, 31);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 14);
            this.label2.TabIndex = 10;
            this.label2.Text = "Account";
            // 
            // TxtAccountNo
            // 
            this.TxtAccountNo.EnterMoveNextControl = true;
            this.TxtAccountNo.Location = new System.Drawing.Point(93, 7);
            this.TxtAccountNo.Name = "TxtAccountNo";
            this.TxtAccountNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAccountNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAccountNo.Properties.MaxLength = 80;
            this.TxtAccountNo.Size = new System.Drawing.Size(288, 20);
            this.TxtAccountNo.TabIndex = 11;
            this.TxtAccountNo.Validated += new System.EventHandler(this.TxtAccountNo_Validated);
            // 
            // ChkAccountNo
            // 
            this.ChkAccountNo.Location = new System.Drawing.Point(386, 8);
            this.ChkAccountNo.Name = "ChkAccountNo";
            this.ChkAccountNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAccountNo.Properties.Appearance.Options.UseFont = true;
            this.ChkAccountNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAccountNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAccountNo.Properties.Caption = " ";
            this.ChkAccountNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAccountNo.Size = new System.Drawing.Size(27, 22);
            this.ChkAccountNo.TabIndex = 12;
            this.ChkAccountNo.ToolTip = "Remove filter";
            this.ChkAccountNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAccountNo.ToolTipTitle = "Run System";
            this.ChkAccountNo.CheckedChanged += new System.EventHandler(this.ChkAccountNo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Description";
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(93, 28);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 250;
            this.TxtAcDesc.Size = new System.Drawing.Size(288, 20);
            this.TxtAcDesc.TabIndex = 14;
            this.TxtAcDesc.Validated += new System.EventHandler(this.TxtAcDesc_Validated);
            // 
            // ChkAcDesc
            // 
            this.ChkAcDesc.Location = new System.Drawing.Point(386, 27);
            this.ChkAcDesc.Name = "ChkAcDesc";
            this.ChkAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcDesc.Properties.Appearance.Options.UseFont = true;
            this.ChkAcDesc.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcDesc.Properties.Caption = " ";
            this.ChkAcDesc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcDesc.Size = new System.Drawing.Size(27, 22);
            this.ChkAcDesc.TabIndex = 15;
            this.ChkAcDesc.ToolTip = "Remove filter";
            this.ChkAcDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcDesc.ToolTipTitle = "Run System";
            this.ChkAcDesc.CheckedChanged += new System.EventHandler(this.ChkAcDesc_CheckedChanged);
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(93, 50);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 12;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(83, 20);
            this.LueLevel.TabIndex = 17;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueLevel_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(1, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Account Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkChildInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(425, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(255, 76);
            this.panel4.TabIndex = 23;
            // 
            // ChkChildInd
            // 
            this.ChkChildInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkChildInd.Location = new System.Drawing.Point(110, 6);
            this.ChkChildInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkChildInd.Name = "ChkChildInd";
            this.ChkChildInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkChildInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkChildInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkChildInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkChildInd.Properties.Appearance.Options.UseFont = true;
            this.ChkChildInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkChildInd.Properties.Caption = "Auto Selected Child";
            this.ChkChildInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkChildInd.ShowToolTips = false;
            this.ChkChildInd.Size = new System.Drawing.Size(132, 22);
            this.ChkChildInd.TabIndex = 15;
            // 
            // FrmYearlyClosingJournalEntryDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 473);
            this.Name = "FrmYearlyClosingJournalEntryDlg";
            this.Text = "List of COA";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccountNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAccountNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkChildInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtAccountNo;
        private DevExpress.XtraEditors.CheckEdit ChkAccountNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkAcDesc;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkChildInd;
    }
}