﻿#region Update
/*
    09/02/2023 [WED/MNET] new apps. pilih service delivery
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice3Dlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoice3 mFrmParent;
        private string mDOCtDocNo = string.Empty, mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesInvoice3Dlg6(FrmSalesInvoice3 FrmParent, string DOCtDocNo, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDOCtDocNo = DOCtDocNo;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SalesInvoice3Dlg6 - SetSQL */ ");
            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, C.ItName, B.Qty, C.SalesUOMCode Uom, ");
            SQL.AppendLine("A.CurCode, B.UPrice, A.DocNoInternal, B.Remark, A.DOCtDocNo ");
            SQL.AppendLine("From TblServiceDeliveryHdr A ");
            SQL.AppendLine("Inner Join TblServiceDeliveryDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Status = 'A' And B.CancelInd = 'N' ");
            SQL.AppendLine("    And(A.DOCtDocNo Is Null Or(A.DOCtDocNo Is Not Null And Find_In_Set(A.DOCtDocNo, @DOCtDocNo))) ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CtCode = @CtCode ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Document#",
                        "Date",
                        "",
                        "Item Name",

                        //6-10
                        "Quantity",
                        "Sales"+Environment.NewLine+"Uom",
                        "Currency",
                        "Service Price",
                        "Local#",

                        //11-14
                        "Remark",
                        "DNo",
                        "Amount",
                        "DO#"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 180, 80, 20, 180, 
                        
                        //6-10
                        120, 120, 100, 150, 150, 
                        
                        //11-14
                        200, 0, 180, 180
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 9, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 12 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[13].Move(10);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string DocNo = string.Empty,
                    Filter = string.Empty;

                string selectedServiceDelivery = mFrmParent.SelectedServiceDelivery();

                if (selectedServiceDelivery.Length > 0)
                    Filter = " Where Not Find_In_Set(Concat(A.DocNo, B.DNo), @SelectedDocNo) ";
                else
                    Filter = " Where 0 = 0 ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@SelectedDocNo", selectedServiceDelivery);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@DOCtDocNo", mDOCtDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNoInternal.Text, new string[] { "A.DocNoInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "ItName", "Qty", "Uom", "CurCode", 
            
                        //6-10
                        "UPrice", "DocNoInternal", "Remark", "DNo", "DOCtDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Grd.Cells[Row, 13].Value = Sm.FormatNum(Sm.DrDec(dr, c[3]) * Sm.DrDec(dr, c[6]), 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd6.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 1, Grd1, Row2, 2); //DocNo
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 2, Grd1, Row2, 3); //DocDt
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 4, Grd1, Row2, 5); //ItName
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 5, Grd1, Row2, 6); //Qty
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 6, Grd1, Row2, 7); //Uom
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 7, Grd1, Row2, 8); //CurCode
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 8, Grd1, Row2, 9); //UPrice
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 9, Grd1, Row2, 10); //DocNoInternal
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 10, Grd1, Row2, 11); //Remark
                        Sm.CopyGrdValue(mFrmParent.Grd6, Row1, 19, Grd1, Row2, 12); //DNo
                        mFrmParent.Grd6.Cells[Row1, 11].Value = mFrmParent.Grd6.Cells[Row1, 12].Value = mFrmParent.Grd6.Cells[Row1, 13].Value = false;
                        mFrmParent.Grd6.Cells[Row1, 15].Value = Sm.GetGrdDec(Grd1, Row2, 6) * Sm.GetGrdDec(Grd1, Row2, 9);
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd6, Row1, new int[] { 14, 16, 17, 18 });

                        mFrmParent.Grd6.Rows.Add();

                        mFrmParent.ComputeTaxPerDetail(true, Row1, "Grd6", ref mFrmParent.Grd6);

                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd6, mFrmParent.Grd6.Rows.Count - 1, new int[] { 5, 8, 14, 15, 16, 17, 18 });
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 service delivery document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int row = 0; row <= mFrmParent.Grd6.Rows.Count - 1; row++)
                if (Sm.CompareStr(
                    string.Concat(Sm.GetGrdStr(mFrmParent.Grd6, row, 1), Sm.GetGrdStr(mFrmParent.Grd6, row, 19)),
                    string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 12))
                    )) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmServiceDelivery(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNoInternal_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNoInternal_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
