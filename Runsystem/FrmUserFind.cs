﻿#region Update
/*
    23/10/2020 [TKG/SIER] tambah employee
    09/04/2021 [TKG/PHT] tambah PwdLastUpDt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmUserFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmUser mFrmParent;

        #endregion

        #region Constructor

        public FrmUserFind(FrmUser FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.SetLueGrpCode(ref LueGrpCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "User's Code", 
                        "User's Name",
                        "Group",
                        "Employee's Code",
                        "Employee's Name",

                        //6-10
                        "Expired", 
                        "Password's Last Update",
                        "Created By",   
                        "Created Date", 
                        "Created Time",

                        //11-13
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.UserCode, A.UserName, B.GrpName, C.EmpCode, C.EmpName, A.ExpDt, A.PwdLastUpDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblUser A ");
            SQL.AppendLine("Left Join TblGroup B On A.GrpCode=B.GrpCode ");
            SQL.AppendLine("Left Join TblEmployee C On A.UserCode=C.UserCode ");
            SQL.AppendLine("Where IfNull(A.GrpCode, '')<>");
            SQL.AppendLine("Case When Exists( ");
            SQL.AppendLine("    Select 1 From TblUser Where GrpCode='SysAdm' And UserCode=@UserCode) ");
            SQL.AppendLine("    Then '' Else 'SysAdm' End ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.UserName;");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtUserName.Text, new string[] { "A.UserCode", "A.UserName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGrpCode), "A.GrpCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "UserCode", 
                                
                            //1-5
                            "UserName", "GrpName", "EmpCode", "EmpName", "ExpDt", 
                            
                            //6-10
                            "PwdLastUpDt", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtUserName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkUserName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrpCode, new Sm.RefreshLue1(mFrmParent.SetLueGrpCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Group");
        }

        #endregion

        #endregion
    }
}
