﻿#region Update
/*
    05/11/2019 [WED/IMS] new apps
    11/11/2019 [WED/IMS] POQty ambil dari PODtl2, tambah recv qty dan outstanding qty+
    19/01/2021 [IBL/IMS] Tambah kolom local code dan specification berdasarkan parameter IsBOMShowSpecifications
    26/03/2021 [WED/IMS] bug salah index kolom deviation date
    31/03/2021 [WED/IMS] ubah source kolom "Received" dari RecvVd ke RecvExpedition --> pakai parameter PORecvDeviationSource. kalau 1 dari RecvVd, kalau 2 dari RecvExpedition
    09/04/2021 [WED/IMS] hanya PO yang MR nya ada SOContract nya aja, dan PO Umum tidak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPORecvDeviation : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mNoOfDayPORecvDeviation = string.Empty,
            mPORecvDeviationSource = string.Empty;
        private bool mIsBOMShowSpecifications = false;

        #endregion

        #region Constructor

        public FrmRptPORecvDeviation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                GetParameter();
                ChkExclRecvItem.Checked = true;
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DNo, G.VdName, D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, IfNull(B.Qty, A.Qty) Qty, ");
            SQL.AppendLine("IFNULL(B.EstRecvDt, A.EstRecvDt) EstRecvDt, H.DocNo RecvVdDocNo, H.DNo RecvVdDNo, I.DocDt RecvDt, IfNull(H.Qty, 0.00) RecvQty, ");
            SQL.AppendLine("DATEDIFF(IFNULL(B.EstRecvDt, A.EstRecvDt), IFNULL(I.DocDt, Left(CurrentDateTime(), 8))) DeviationDt, ");
            SQL.AppendLine("J.SumPOQty, IfNull(J.SumRecvQty, 0.00) SumRecvQty, ");
            SQL.AppendLine("IFNULL((IfNull(H.Qty, 0.00) - IfNull(B.Qty, A.Qty)), 0.00) OutstandingRecv ");
            SQL.AppendLine("FROM TblPODtl A ");
            SQL.AppendLine("LEFT JOIN TblPODtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.PORequestDocNo = B.PORequestDocNo ");
	        SQL.AppendLine("    AND A.PORequestDNo = B.PORequestDNo ");
            SQL.AppendLine("INNER JOIN TblPORequestDtl C ON A.PORequestDocNo = C.DocNo AND A.PORequestDNo = C.DNo ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    And Upper(A.DocNo) Not Like '%UMUM%' ");
            SQL.AppendLine("INNER JOIN TblMaterialRequestDtl D ON C.MaterialRequestDocNo = D.DocNo AND C.MaterialRequestDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D1 On D.DocNo = D1.DocNo And D1.SOCDocNo Is Not Null ");
            SQL.AppendLine("INNER JOIN TblItem E ON D.ItCode = E.ItCode ");
            SQL.AppendLine("INNER JOIN TblPOHdr F ON A.DocNo = F.DocNo AND (F.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            SQL.AppendLine("INNER JOIN TblVendor G ON F.VdCode = G.VdCode ");
            if (mPORecvDeviationSource == "1")
            {
                SQL.AppendLine("LEFT JOIN TblRecvVdDtl H ON A.DocNo = H.PODocNo AND A.DNo = H.PODNo ");
                SQL.AppendLine("    AND H.CancelInd = 'N' AND H.Status = 'A' ");
                SQL.AppendLine("LEFT JOIN TblRecvVdHdr I ON H.DocNo = I.DocNo ");
            }
            else
            {
                SQL.AppendLine("Left Join TblRecvExpeditionDtl H On A.DocNo = H.PODocNo And A.DNo = H.PODNo ");
                SQL.AppendLine("    And H.CancelInd = 'N' And H.Status = 'A' ");
                SQL.AppendLine("Left Join TblRecvExpeditionHdr I On H.DocNo = I.DocNo ");
            }
            
            SQL.AppendLine("INNER JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT X1.DocNo, X1.DNo, X1.Qty SumPOQty, SUM(X2.Qty) SumRecvQty ");
            SQL.AppendLine("    FROM TblPODtl X1 ");
            if (mPORecvDeviationSource == "1")
                SQL.AppendLine("    LEFT JOIN TblRecvVdDtl X2 ON X1.DocNo = X2.PODocNo AND X1.DNo = X2.PODNo ");
            else
                SQL.AppendLine("    Left Join TblRecvExpeditionDtl X2 On X1.DocNo = X2.PODocNo And X1.DNo = X2.PODNo ");
            SQL.AppendLine("        AND X2.CancelInd = 'N' AND X2.Status = 'A' ");
            SQL.AppendLine("    WHERE X1.CancelInd = 'N' ");
            SQL.AppendLine("    GROUP BY X1.DocNo, X1.DNo, X1.Qty ");
            SQL.AppendLine(") J ON A.DocNo = J.DocNo AND A.DNo = J.DNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "PO#",
                    "Vendor",
                    "Item Code",
                    "Item Name",
                    "Item Local" + Environment.NewLine + "Code",

                    //6-10
                    "PO"+Environment.NewLine+"Quantity",
                    "Estimated" + Environment.NewLine + "Received Date",
                    "Received#",
                    "Received Date",
                    "Received"+Environment.NewLine+"Quantity",

                    //11-15
                    "Outstanding"+Environment.NewLine+"Received",
                    "Deviation Day(s)",
                    "PODNo",
                    "Sum PO Qty",
                    "Sum Recv Qty",

                    //16
                    "Specification"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    190, 200, 120, 150, 120,

                    //6-10
                    100, 120, 150, 120, 120, 
                    
                    //11-15
                    120, 120, 0, 120, 120,

                    //16
                    200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 10, 11, 14, 15 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 13, 14, 15, 16 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });

            if (mIsBOMShowSpecifications)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(6);
                Grd1.Cols[5].Move(4);
            }

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                if (ChkExclRecvItem.Checked) Filter += " And A.ProcessInd <> 'F' ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "E.ItName", "E.ItCodeInternal" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.DocNo, A.DNo;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "VdName", "ItCode", "ItName", "ItCodeInternal", "Qty",

                        //6-10
                        "EstRecvDt", "RecvVdDocNo", "RecvDt", "RecvQty", "DeviationDt",

                        //11-15
                        "DNo", "SumPOQty", "SumRecvQty", "OutstandingRecv", "Specification"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        //Grd.Cells[Row, 11].Value = 0m;
                        //Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 14);
                        decimal mOuts = Sm.DrDec(dr, c[14]);
                        if (mOuts < 0) mOuts = mOuts * -1;
                        Grd1.Cells[Row, 11].Value = mOuts;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        if (Int32.Parse(mNoOfDayPORecvDeviation) > 0 && Sm.DrStr(dr, c[7]).Length <= 0)
                        {
                            if (Sm.DrInt(dr, c[10]) <= Int32.Parse(mNoOfDayPORecvDeviation))
                            {
                                Grd.Rows[Row].BackColor = Color.LightCoral;
                            }
                        }
                    }, true, false, false, false
                );
                //CalculateOutstandingQty();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mNoOfDayPORecvDeviation = Sm.GetParameter("NoOfDayPORecvDeviation");
            mPORecvDeviationSource = Sm.GetParameter("PORecvDeviationSource");

            if (mPORecvDeviationSource.Length == 0) mPORecvDeviationSource = "1";

            if (mNoOfDayPORecvDeviation.Length <= 0) mNoOfDayPORecvDeviation = "0";
        }

        private void CalculateOutstandingQty()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdDec(Grd1, i, 14) == Sm.GetGrdDec(Grd1, i, 15))
                    {
                        Grd1.Cells[i, 11].Value = 0m;
                    }
                    else
                    {
                        if (Sm.GetGrdDec(Grd1, i, 15) == 0)
                        {
                            Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 6);
                        }
                        if (Sm.GetGrdDec(Grd1, i, 10) >= Sm.GetGrdDec(Grd1, i, 6))
                        {
                            if(Sm.GetGrdDec(Grd1, i, 15) >= Sm.GetGrdDec(Grd1, i, 6))
                            {
                                Grd1.Cells[i, 11].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 14) - Sm.GetGrdDec(Grd1, i, 15);
                            }
                        }
                        if (Sm.GetGrdDec(Grd1, i, 10) < Sm.GetGrdDec(Grd1, i, 6))
                        {
                            // masih ada bug disini, harus pakai list, cuman belum kepikiran mau kek gimana.
                            // karena, PO nya tercatat bisa dipecah per DNo nya, tapi Recv hanya ambil 1 DNo
                            // reporting nya ditampilkan per pecahan DNO PO tsb
                            Grd1.Cells[i, 11].Value = Sm.GetGrdDec(Grd1, i, 6) - Sm.GetGrdDec(Grd1, i, 10);
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
