﻿#region Update
/*
    06/02/2019 [TKG] New Application
    31/05/2021 [MYA/KIM] penambahan field collector yg ambil dr master employee di SLI Control
    14/06/2021 [MYA/KIM] menambah parameter untuk filter field collector yg ambil dr master employee di SLI Control 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoiceControl : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSalesInvoiceControlFind FrmFind;
        internal string EmpCode = string.Empty,
            mDeptCodeForSLIPICCollector = string.Empty;
        
        #endregion

        #region Constructor

        public FrmSalesInvoiceControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                ShowSalesInvoiceControl();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mDeptCodeForSLIPICCollector = Sm.GetParameter("DeptCodeForSLIPICCollector");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[]{ "Process", "Control Code", "Process Description", "Process" },
                    new int[]{ 100, 100, 300, 0 }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0, 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeCancelReason, MeeRemark }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnSalesInvoiceDocNo.Enabled = false;
                    BtnSalesInvoicePICCollector.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark }, false);
                    BtnSalesInvoiceDocNo.Enabled = true;
                    BtnSalesInvoicePICCollector.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    BtnSalesInvoicePICCollector.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, MeeCancelReason, TxtSalesInvoiceDocNo, TxtCtCode, 
               MeeRemark, TxtPICCollector
            });
            EmpCode = string.Empty;
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 3 });
        }

        #endregion

        #region Grid Method

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex==0 &&
                (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0 || Sm.GetGrdBool(Grd1, e.RowIndex, 3))) 
                e.DoDefault = false;
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoiceControlFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ShowSalesInvoiceControl();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid())
                return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoiceControl", "TblSalesInvoiceControlHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoiceControlHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length>0) 
                    cml.Add(SaveSalesInvoiceControlDtl(DocNo, r));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPICCollector, "PIC Collector", false) ||
                IsGrdEmpty() ||
                //IsSalesInvoiceAlreadyProcessed() ||
                IsSalesInvoiceAlreadyCancelled()
                ;
        }

        private bool IsSalesInvoiceAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceControlHdr Where SalesInvoiceDocNo=@Param And CancelInd='N';",
                TxtSalesInvoiceDocNo.Text,
                "This sales invoice already processed."
                );
        }

        private bool IsSalesInvoiceAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr Where DocNo=@Param And CancelInd='Y';",
                TxtSalesInvoiceDocNo.Text,
                "This sales invoice already cancelled."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSalesInvoiceControlHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoiceControlHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, SalesInvoiceDocNo, Remark, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @SalesInvoiceDocNo, @Remark, @EmpCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SalesInvoiceDocNo", TxtSalesInvoiceDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesInvoiceControlDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesInvoiceControlDtl ");
            SQL.AppendLine("(DocNo, ProcessInd, SalesInvoiceControlCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @ProcessInd, @SalesInvoiceControlCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdBool(Grd1, r, 0)?"Y":"N");
            Sm.CmParam<String>(ref cm, "@SalesInvoiceControlCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesInvoiceControl());
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) 
                    cml.Add(SaveSalesInvoiceControlDtl(TxtDocNo.Text, r));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsTxtEmpty(TxtPICCollector, "PIC Collector", false) ||
                IsDocAlreadyCancelled() ||
                IsGrdEmpty() ||
                IsSalesInvoiceAlreadyCancelled();
        }

        private bool IsDocAlreadyCancelled()
        {
            return Sm.IsDataExist(
               "Select 1 From TblSalesInvoiceControlHdr Where DocNo=@Param And CancelInd='Y';",
               TxtDocNo.Text,
               "This document already cancelled."
               );
        }

        private MySqlCommand EditSalesInvoiceControl()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblSalesInvoiceControlHdr Set CancelInd=@CancelInd, CancelReason=@CancelReason, EmpCode=@EmpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where DocNo=@DocNo;" +
                    "Delete From TblSalesInvoiceControlDtl Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowSalesInvoiceControlHdr(DocNo);
                ShowSalesInvoiceControlDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoiceControlHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.SalesInvoiceDocNo, C.CtName, A.Remark, A.EmpCode ");
            SQL.AppendLine("From TblSalesInvoiceControlHdr A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.SalesInvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "SalesInvoiceDocNo", "CtName", 
                        
                        //6-7
                        "Remark", "EmpCode" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtSalesInvoiceDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                        TxtPICCollector.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowSalesInvoiceControlDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ProcessInd, A.SalesInvoiceControlCode, B.OptDesc ");
            SQL.AppendLine("From TblSalesInvoiceControlDtl A ");
            SQL.AppendLine("Inner Join TblOption B On A.SalesInvoiceControlCode=B.OptCode And B.OptCat='SalesInvoiceControl' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.OptCode;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { "ProcessInd", "SalesInvoiceControlCode", "OptDesc" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        private void ShowSalesInvoiceControl()
        {
            var cm = new MySqlCommand();
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, 
                "Select OptCode, OptDesc From TblOption Where OptCat='SalesInvoiceControl' Order By OptCode;",
                new string[] { "OptCode", "OptDesc" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button Event

        private void BtnSalesInvoiceDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesInvoiceControlDlg(this));
        }

        private void BtnSalesInvoiceDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSalesInvoiceDocNo, "Sales invoice#", false))
            {
                var f1 = new FrmSalesInvoice(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSalesInvoiceDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnSalesInvoicePICCollector_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesInvoiceControlDlg2(this));
        }

        #endregion

        #endregion
    }
}
