﻿namespace RunSystem
{
    partial class FrmChrOmset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblCategoryCode = new System.Windows.Forms.Label();
            this.LueCategoryCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LblSubCategory = new System.Windows.Forms.Label();
            this.LueFilterCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.LblList = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtTotalOmset = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCategoryCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFilterCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalOmset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(926, 0);
            this.panel1.Size = new System.Drawing.Size(70, 504);
            // 
            // BtnWord
            // 
            this.BtnWord.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnWord.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWord.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWord.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWord.Appearance.Options.UseBackColor = true;
            this.BtnWord.Appearance.Options.UseFont = true;
            this.BtnWord.Appearance.Options.UseForeColor = true;
            this.BtnWord.Appearance.Options.UseTextOptions = true;
            this.BtnWord.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWord.Click += new System.EventHandler(this.BtnWord_Click);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 482);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPDF
            // 
            this.BtnPDF.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPDF.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPDF.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPDF.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPDF.Appearance.Options.UseBackColor = true;
            this.BtnPDF.Appearance.Options.UseFont = true;
            this.BtnPDF.Appearance.Options.UseForeColor = true;
            this.BtnPDF.Appearance.Options.UseTextOptions = true;
            this.BtnPDF.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPDF.Click += new System.EventHandler(this.BtnPDF_Click);
            // 
            // Chart
            // 
            this.Chart.ChartArea.BackInterior = new Syncfusion.Drawing.BrushInfo(System.Drawing.Color.Transparent);
            this.Chart.ChartArea.CursorLocation = new System.Drawing.Point(0, 0);
            this.Chart.ChartArea.CursorReDraw = false;
            this.Chart.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // 
            // 
            this.Chart.Legend.Location = new System.Drawing.Point(659, 75);
            this.Chart.Legend.Visible = false;
            this.Chart.Location = new System.Drawing.Point(0, 70);
            this.Chart.PrimaryXAxis.Font = new System.Drawing.Font("Tahoma", 8F);
            this.Chart.PrimaryXAxis.Format = "string";
            this.Chart.PrimaryXAxis.GridLineType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryXAxis.LineType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryXAxis.Margin = true;
            this.Chart.PrimaryXAxis.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryXAxis.TitleColor = System.Drawing.SystemColors.ControlText;
            this.Chart.PrimaryXAxis.ValueType = Syncfusion.Windows.Forms.Chart.ChartValueType.Custom;
            this.Chart.PrimaryYAxis.Font = new System.Drawing.Font("Tahoma", 8F);
            this.Chart.PrimaryYAxis.GridLineType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryYAxis.LineType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryYAxis.Margin = true;
            this.Chart.PrimaryYAxis.TickColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Chart.PrimaryYAxis.TitleColor = System.Drawing.SystemColors.ControlText;
            this.Chart.Size = new System.Drawing.Size(926, 434);
            this.Chart.TabIndex = 19;
            this.Chart.Text = "Omset";
            // 
            // 
            // 
            this.Chart.Title.Name = "Default";
            this.Chart.ToolBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(926, 70);
            this.panel2.TabIndex = 9;
            // 
            // LblCategoryCode
            // 
            this.LblCategoryCode.AutoSize = true;
            this.LblCategoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCategoryCode.ForeColor = System.Drawing.Color.Red;
            this.LblCategoryCode.Location = new System.Drawing.Point(53, 7);
            this.LblCategoryCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCategoryCode.Name = "LblCategoryCode";
            this.LblCategoryCode.Size = new System.Drawing.Size(56, 14);
            this.LblCategoryCode.TabIndex = 13;
            this.LblCategoryCode.Text = "Category";
            this.LblCategoryCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCategoryCode
            // 
            this.LueCategoryCode.EnterMoveNextControl = true;
            this.LueCategoryCode.Location = new System.Drawing.Point(112, 4);
            this.LueCategoryCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCategoryCode.Name = "LueCategoryCode";
            this.LueCategoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCategoryCode.Properties.Appearance.Options.UseFont = true;
            this.LueCategoryCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCategoryCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCategoryCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCategoryCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCategoryCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCategoryCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCategoryCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCategoryCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCategoryCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCategoryCode.Properties.DropDownRows = 30;
            this.LueCategoryCode.Properties.NullText = "[Empty]";
            this.LueCategoryCode.Properties.PopupWidth = 500;
            this.LueCategoryCode.Size = new System.Drawing.Size(165, 20);
            this.LueCategoryCode.TabIndex = 14;
            this.LueCategoryCode.ToolTip = "F4 : Show/hide list";
            this.LueCategoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCategoryCode.EditValueChanged += new System.EventHandler(this.LueCategoryCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LblSubCategory);
            this.panel4.Controls.Add(this.LueFilterCategory);
            this.panel4.Controls.Add(this.LblList);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtTotalOmset);
            this.panel4.Controls.Add(this.LueCategoryCode);
            this.panel4.Controls.Add(this.LblCategoryCode);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(641, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(285, 70);
            this.panel4.TabIndex = 14;
            // 
            // LblSubCategory
            // 
            this.LblSubCategory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSubCategory.ForeColor = System.Drawing.Color.Black;
            this.LblSubCategory.Location = new System.Drawing.Point(28, 28);
            this.LblSubCategory.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSubCategory.Name = "LblSubCategory";
            this.LblSubCategory.Size = new System.Drawing.Size(81, 14);
            this.LblSubCategory.TabIndex = 15;
            this.LblSubCategory.Text = "Filter";
            this.LblSubCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFilterCategory
            // 
            this.LueFilterCategory.EnterMoveNextControl = true;
            this.LueFilterCategory.Location = new System.Drawing.Point(112, 25);
            this.LueFilterCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LueFilterCategory.Name = "LueFilterCategory";
            this.LueFilterCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFilterCategory.Properties.Appearance.Options.UseFont = true;
            this.LueFilterCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFilterCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFilterCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFilterCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFilterCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFilterCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFilterCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFilterCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFilterCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFilterCategory.Properties.DropDownRows = 30;
            this.LueFilterCategory.Properties.NullText = "[Empty]";
            this.LueFilterCategory.Properties.PopupWidth = 500;
            this.LueFilterCategory.Size = new System.Drawing.Size(165, 20);
            this.LueFilterCategory.TabIndex = 16;
            this.LueFilterCategory.ToolTip = "F4 : Show/hide list";
            this.LueFilterCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFilterCategory.EditValueChanged += new System.EventHandler(this.LueFilterCategory_EditValueChanged);
            // 
            // LblList
            // 
            this.LblList.AutoSize = true;
            this.LblList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblList.ForeColor = System.Drawing.Color.Red;
            this.LblList.Location = new System.Drawing.Point(6, 27);
            this.LblList.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblList.Name = "LblList";
            this.LblList.Size = new System.Drawing.Size(0, 14);
            this.LblList.TabIndex = 19;
            this.LblList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Total Omset";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalOmset
            // 
            this.TxtTotalOmset.EnterMoveNextControl = true;
            this.TxtTotalOmset.Location = new System.Drawing.Point(112, 46);
            this.TxtTotalOmset.Name = "TxtTotalOmset";
            this.TxtTotalOmset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalOmset.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalOmset.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalOmset.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalOmset.Properties.MaxLength = 250;
            this.TxtTotalOmset.Size = new System.Drawing.Size(165, 20);
            this.TxtTotalOmset.TabIndex = 18;
            this.TxtTotalOmset.Validated += new System.EventHandler(this.TxtTotalOmset_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(173, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 45;
            this.label3.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(14, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 10;
            this.label2.Text = "Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(187, 7);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt2.TabIndex = 12;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(51, 7);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt1.TabIndex = 11;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // FrmChrOmset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 504);
            this.Name = "FrmChrOmset";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCategoryCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFilterCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalOmset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblCategoryCode;
        public DevExpress.XtraEditors.LookUpEdit LueCategoryCode;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtTotalOmset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        public DevExpress.XtraEditors.LookUpEdit LueFilterCategory;
        private System.Windows.Forms.Label LblList;
        private System.Windows.Forms.Label LblSubCategory;
    }
}