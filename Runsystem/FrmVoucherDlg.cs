﻿#region Update
/*
    31/05/2017 [TKG] Pada layar pencarian voucher request, ditambah informasi department, pic, bank account
    03/10/2018 [DITA] Update Filter berdasarkan Departmen Group
    18/03/2020 [TKG] tambah filter remark
    24/03/2020 [IBL/KBN] Memunculkan KOLOM DUE DATE saat INSERT VOUCHER
    18/11/2020 [VIN/IMS] SO COntract Termin DP 1&2
    19/02/2021 [WED/IMS] tidak menarik VR Special. via Voucher Special
    26/07/2022 [TYO/SIER] Group filter doc Type berdasarkan parameter IsGroupFilterByType
    05/08/2022 [TYO/SIER] Penyesuaian filter by group
    24/03/2023 [BRI/PHT] bug parameter IsVoucherSwitchingNotFilterByDept
    30/03/2023 [WED/PHT] tambah kolom Employee Name Cash Advance PIC berdasarkan parameter IsVRCashAdvanceUseEmployeePIC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucher mFrmParent;
        private string mSQL = string.Empty, mMInd = "N";

        #endregion

        #region Constructor

        public FrmVoucherDlg(FrmVoucher FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -4);
                Sl.SetLueVoucherDocType(ref LueDocType, mFrmParent.mIsGroupFilterByType ? "Y" : "N");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "", 
                        "Date",
                        "Local#",
                        "Type",
                        
                        //6-10
                        "Amount",
                        "Department",
                        "Person In Charge",
                        "Bank Account",
                        "Remark",

                        //11-12
                        "Employee Cash Adv. PIC",
                        "Due Date"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            if (!mFrmParent.mIsVRCashAdvanceUseEmployeePIC) Sm.GrdColInvisible(Grd1, new int[] { 11 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.LocalDocNo, A.DocType, B.OptDesc As DocTypeDesc, ");
            SQL.AppendLine("    A.Amt, A.DueDt, ");
            if (mFrmParent.mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine("    H.EmpName As EmpNameCashAdv, ");
            else SQL.AppendLine("    Null As EmpNameCashAdv, ");
            SQL.AppendLine("    Concat( ");
            SQL.AppendLine("    Case When A.Remark Is Null Then '' Else Concat(A.Remark, '\n') End, ");
            SQL.AppendLine("    Case When C.VdName Is Null Then '' Else Concat('Vendor : ', C.VdName) End, ");
            SQL.AppendLine("    Case When C.Remark Is Null Then '' Else C.Remark End ");
            SQL.AppendLine("    ) As Remark, C.VdName, D.DeptName, E.UserName, ");
            SQL.AppendLine("    Concat( ");
            SQL.AppendLine("        Case When G.BankName Is Not Null Then Concat(G.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("        Case When F.BankAcNo Is Not Null  ");
            SQL.AppendLine("        Then Concat(F.BankAcNo, ' [', IfNull(F.BankAcNm, ''), ']') ");
            SQL.AppendLine("        Else IfNull(F.BankAcNm, '') End ");
            SQL.AppendLine("    ) As BankAcDesc, A.CreateDt ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='VoucherDocType' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select DocNo, VdName, ");
            SQL.AppendLine("        Group_Concat(X.Remark Separator '') As Remark ");
            SQL.AppendLine("        From ( ");
            
            SQL.AppendLine("            Select T1.VoucherRequestDocNo As DocNo, ");
            SQL.AppendLine("            T4.VdName, ");
            SQL.AppendLine("            Concat(");
            SQL.AppendLine("                Concat('\nDue Date : ', ");
            SQL.AppendLine("                    Right(IfNull(T1.DueDt, T3.DueDt), 2), '/', ");
            SQL.AppendLine("                    MonthName(Str_To_Date(Substring(IfNull(T1.DueDt, T3.DueDt), 5, 2), '%m')), '/', ");
            SQL.AppendLine("                    Left(IfNull(T1.DueDt, T3.DueDt), 4) ");
            SQL.AppendLine("            ), ");
            SQL.AppendLine("            Case When T3.VdInvNo Is Null Then '' Else Concat(', Invoice# : ', T3.VdInvNo) End,");
            SQL.AppendLine("            Concat(', Amount : ', Convert(Format(T2.Amt, 2) Using utf8)) ");
            SQL.AppendLine("            ) As Remark ");
            SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T3 On  T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            Inner Join TblVendor T4 On T1.VdCode=T4.VdCode ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And T1.Status='A' ");
            SQL.AppendLine("            And T1.VoucherRequestDocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd='N') ");

            SQL.AppendLine("            Union All ");

            SQL.AppendLine("            Select A.VoucherRequestDocNo As DocNo, C.VdName, Null As Remark ");
            SQL.AppendLine("            From TblAPDownpayment A ");
            SQL.AppendLine("            Inner Join TblPOhdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.Status='A' ");
            SQL.AppendLine("            And A.VoucherRequestDocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd='N') ");

            SQL.AppendLine("        ) X Group By DocNo, VdName ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo ");
            SQL.AppendLine("    Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("    Left Join TblUser E On A.PIC=E.UserCode ");
            SQL.AppendLine("    Left Join TblBankAccount F On A.BankAcCode=F.BankAcCode ");
            SQL.AppendLine("    Left Join TblBank G On F.BankCode=G.BankCode ");
            if (mFrmParent.mIsVRCashAdvanceUseEmployeePIC) SQL.AppendLine("    Left Join TblEmployee H On A.EmpCodeCashAdv = H.EmpCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine("    And A.DocType Not In ('65', '66', '67') ");
            if (mFrmParent.mForbiddenVoucherDocType.Length > 0)
                SQL.AppendLine("    And !FIND_IN_SET (B.OptCode, @mForbiddenVoucherDocType) ");

            if (mFrmParent.mIsGroupFilterByType)
            {
                SQL.AppendLine("    And A.DocType In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct DocType ");
                SQL.AppendLine("        From TblGroupType ");
                SQL.AppendLine("        Where GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            if (mFrmParent.mIsUseMInd) SQL.AppendLine("    And A.MInd=@MInd ");
            SQL.AppendLine("    And A.DocNo Not In (Select VoucherRequestDocNo From TblVoucherHdr Where CancelInd='N') ");
            if ((mFrmParent.mIsVoucherFilteredByDept || mFrmParent.mIsFilterByDeptHR) && !mFrmParent.mIsVoucherSwitchingNotFilterByDept)
            {
                SQL.AppendLine("    And (A.DeptCode Is Null Or (A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
            }
            if (mFrmParent.mIsVoucherSwitchingNotFilterByDept)
            {
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    (A.BankAcCode Is Null ");
                SQL.AppendLine("        Or(A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode = A.BankAcCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
                SQL.AppendLine("    Or ");
                SQL.AppendLine("    (A.BankAcCode2 Is Null ");
                SQL.AppendLine("        Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
                SQL.AppendLine("    And ");
                SQL.AppendLine("    Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");

                SQL.AppendLine("And (A.BankAcCode Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");

                SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");
            }
            else if (mFrmParent.mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (A.BankAcCode Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");

                SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
            }

            SQL.AppendLine(") T ");
          
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@mForbiddenVoucherDocType", Sm.GetParameter("ForbiddenVoucherDocType"));

                if (mFrmParent.mIsVoucherFilteredByDept)
                {
                    Sm.CmParam<String>(ref cm, "@VoucherDocTypeManual", mFrmParent.mVoucherDocTypeManual);
                }
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVdCode.Text, "VdName", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRemark.Text, "Remark", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "LocalDocNo", "DocTypeDesc", "Amt", "DeptName",

                            //6-10
                            "UserName", "BankAcDesc", "Remark", "EmpNameCashAdv", "DueDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
                Grd1.Cols.AutoWidth();
                Grd1.Rows.AutoHeight();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowVoucherRequestInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
            else
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least one voucher request.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        
        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueVoucherDocType), mFrmParent.mIsGroupFilterByType ? "Y" : "N");
            //Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Document's type");
        }


        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtRemark_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRemark_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Remark");
        }

        #endregion

        #endregion
    }
}
