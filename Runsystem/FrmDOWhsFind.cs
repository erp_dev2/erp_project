﻿#region Update
/*
    09/08/2017 [ARI] tambah kolom status
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    19/01/2020 [TKG/IMS] tambah informasi local code, specification
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhsFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOWhs mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmDOWhsFind(FrmDOWhs FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, Case When A.Status='O' then 'Outstanding' when A.Status = 'A' ");
            SQL.AppendLine("Then 'Approved' When A.Status = 'C' Then 'Cancelled' End as Status, C.WhsName, D.WhsName As WhsName2, ");
            SQL.AppendLine("E.DocNo As RecvWhsDocNo, B.ItCode, F.ItCodeInternal, F.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, E.Qty As RecvQty, B.Qty2, E.Qty2 As RecvQty2, B.Qty3, E.Qty3 As RecvQty3, ");
            SQL.AppendLine("F.InventoryUomCode, F.InventoryUomCode2, F.InventoryUomCode3,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, F.ItGrpCode, F.ForeignName, F.Specification ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And A.TransferRequestWhsDocNo is null ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode2=D.WhsCode ");
            //SQL.AppendLine("Left Join TblRecvWhsDtl E On B.DocNo=E.DOWhsDocNo And B.DNo=E.DOWhsDNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, DOWhsDocNo, DOWhsDno, SUM(Qty) Qty, SUM(Qty2) Qty2, SUM(Qty3) Qty3 ");
	        SQL.AppendLine("    From TblrecvWhsDtl ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
	        SQL.AppendLine("    Group By DocNo, DOWhsDocNo, DOWhsDno ");
            SQL.AppendLine(")E On B.DocNo=E.DOWhsDocNo And B.DNo=E.DOWhsDNo   ");
            SQL.AppendLine("Inner Join TblItem F On B.ItCode=F.ItCode ");
            SQL.AppendLine("Where 0 = 0 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "From",
                        
                        //6-10
                        "To",
                        "Received#",
                        "",
                        "Item's Code",
                        "",
                        
                        //11-15
                        "Local Code",
                        "Item's Name",
                        "Foreign Name",
                        "Batch#",
                        "Source",
                        
                        //16-20
                        "Lot",
                        "Bin",
                        "DO",    
                        "Received",
                        "UoM",
                        
                        //21-25 
                        "DO", 
                        "Received",
                        "UoM",
                        "DO",    
                        "Received",
                        
                        //26-30
                        "UoM",
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        
                        //31-34
                        "Last Updated Date",
                        "Last Updated Time",
                        "Group",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        150, 80, 50, 150, 180, 
                        
                        //6-10
                        200, 150, 20, 100, 20, 
                        
                        //11-15
                        100, 200, 180, 200, 180,  

                        //16-20
                        60, 60, 80, 80, 80,   

                        //21-25
                        80, 80, 80, 80, 80,   

                        //26-30
                        80, 130, 130, 130, 130, 

                        //31-34
                        130, 130, 120, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 8, 10 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 21, 22, 24, 25 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 28, 31 });
            Sm.GrdFormatTime(Grd1, new int[] { 29, 32 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 15, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 });
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[33].Visible = true;
                Grd1.Cols[33].Move(12);
            }
            Grd1.Cols[34].Move(14);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 15, 27, 28, 29, 30, 31, 32, 34 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtRecvWhsDocNo.Text, "E.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode2), "A.WhsCode2", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "F.ItCodeInternal", "F.ItName", "F.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "Status", "WhsName", "WhsName2", 
                            
                            //6-10
                            "RecvWhsDocNo", "ItCode", "ItCodeInternal", "ItName", "ForeignName",    
                            
                            //11-15
                            "BatchNo", "Source", "Lot", "Bin", "Qty",    
                            
                            //16-20
                            "RecvQty", "InventoryUomCode", "Qty2", "RecvQty2", "InventoryUomCode2", 
                            
                            //21-25
                            "Qty3", "RecvQty3", "InventoryUomCode3", "CreateBy", "CreateDt",  

                            //26-29
                            "LastUpBy", "LastUpDt", "ItGrpCode", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 29);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmRecvWhs(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmRecvWhs(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //if (mIsInventoryShowTotalQty) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }
        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse (From)");
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse (To)");
        }

        private void TxtRecvWhsDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRecvWhsDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received document#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion
    }
}
