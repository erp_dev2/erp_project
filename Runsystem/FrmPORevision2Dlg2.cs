﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORevision2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORevision2 mFrmParent;
        private string mSQL = string.Empty;
        private string mItCode, mItName;

        #endregion

        #region Constructor

        public FrmPORevision2Dlg2(FrmPORevision2 FrmParent, string ItCode, string ItName)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItCode = ItCode;
            mItName = ItName;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            TxtItCode.Text = mItCode;
            TxtItName.Text = mItName;

            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -90);
            SetGrd();
            SetSQL();
            Sl.SetLueVdCode(ref LueVdCode);
            Sl.SetLuePtCode(ref LuePtCode);
            ShowData();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "DNo",
                        "",
                        "Date",
                        "Vendor Code",

                        //6-10
                        "Vendor",
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",
                        "Price",
                        "Remaining"+Environment.NewLine+"Credit Limit",
                        
                        //11
                        "Remark"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 10 }, false);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.VdCode, C.VdName, D.PtName, A.CurCode, B.UPrice, B.Remark, ");
            SQL.AppendLine("IfNull(C.CreditLimit, 0)-IfNull(( ");
            SQL.AppendLine("    Select Sum(T.Qty) From TblPORequestDtl T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.QtDocNo=A.DocNo And T.QtDNo=B.DNo ");
            SQL.AppendLine("    And Concat(T.DocNo, T.DNo) Not In ( ");
            SQL.AppendLine("        Select Concat(DocNo, DNo) From TblDocApproval ");
            SQL.AppendLine("        Where DocType='PORequest' And IfNull(Status, '')='C') ");
            SQL.AppendLine("    ), 0) As RemainingCreditLimit ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode And B.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode And C.ActInd='Y' ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.VdCode, T2.ItCode, ");
            SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo, T2.DNo)) As Key1  ");
            SQL.AppendLine("    From TblQtHdr T1, TblQtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And T1.PtCode Is Not Null ");
                SQL.AppendLine("And T1.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Group By T1.VdCode, T2.ItCode ");
            SQL.AppendLine("    ) E On Concat(A.DocDt, B.DocNo, B.DNo)=E.Key1 And A.VdCode=E.VdCode And B.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.Status='A' ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And A.PtCode Is Not Null ");
                SQL.AppendLine("And A.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@ItCode", mItCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePtCode), "A.PtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "VdCode", "VdName", "PtName",  
                            
                            //6-9
                            "CurCode", "UPrice", "RemainingCreditLimit", "Remark" 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0) && Sm.CompareStr(mFrmParent.mVdCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5)))
            {
                mFrmParent.TxtQtNew.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.DNoQtNew = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtUnitPriceNew.Text = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                mFrmParent.ComputeTotalNew();
                this.Close();
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor is different, please select same Vendor ("+mFrmParent.TxtVdCode.Text+")");
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.mItCode = mItCode;
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.mItCode = mItCode;
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Term of payment");
        }

        private void LblQt_Click(object sender, EventArgs e)
        {
            var f = new FrmQt(mFrmParent.mMenuCode);
            f.Tag = mFrmParent.mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mItCode = mItCode;
            f.ShowDialog();
            ShowData();
        }

        #endregion

        #endregion

    }
}
