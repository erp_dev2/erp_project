﻿#region Update
/*
    22/02/2021 [ICA/PHT] Menambah informasi COA di SetLueCCtCode 
    10/06/2021 [MYA/PHT] Menampilkan cost center child di menu updating item cost category
    20/06/2021 [TKG/PHT] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    16/08/2022 [SET/PRODUCT] validasi Cost center berdasar group menggunakan param IsFilterByCC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmItemCostCategory : RunSystem.FrmBase5
    {
        #region Field

        internal string 
            mMenuCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int ColInd = 0;
        private bool 
            mIsCostCategoryShowCOA = false,
            //mIsShowCostCenterChild = false,
            mIsItemCostCategoryUseProfitCenter = false,
            mIsFilterByCC = false;

        #endregion

        #region Constructor

        public FrmItemCostCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void SetSQL()
        {
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, B.ItCtName, D.CCtName "); 
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            if (mIsItemCostCategoryUseProfitCenter)
            {
                SQL.AppendLine("Inner Join TblItemCostCategory C On A.ItCode = C.ItCode And C.CCCode =@CCCode ");
                SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCode And D.CCCode =@CCCode ");
            }
            else
            {
                SQL.AppendLine("Left Join TblItemCostCategory C On A.ItCode = C.ItCode And C.CCCode =@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory D On C.CCtCode = D.CCtCode And D.CCCode =@CCCode ");
            }
            SQL.AppendLine("Where A.ActInd='Y' ");
          
            mSQL = SQL.ToString();
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                GetParameter();
                SetGrd();
                SetSQL();
                if (mIsItemCostCategoryUseProfitCenter)
                    SetLueCCCodeFilterByProfitCenter(ref LueCC, string.Empty, "N");
                else
                    SetLueCCCode(ref LueCC);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }  

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        "Cost Category"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //
                        100, 300, 250, 300
                    }

                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, LueCC, LueCCt, LueItCtCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        override protected void ShowData()
        {
            try
            {
                if(Sm.IsLueEmpty(LueCC, "Cost Center")) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCt2), "C.CCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[] 
                        { "ItCode", "ItName", "ForeignName", "CCtName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.WaitCursor;
            }
        }

        override protected void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdBool(Grd1, Row, 0) == true)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveItemCostCategory(Row));
                        ColInd = Row;
                    }
                Sm.ExecCommands(cml);
           
            ShowData();
            FocusGrid(ColInd);   
        }

        private void FocusGrid(int Col)
        {
                Sm.FocusGrd(Grd1, Col, 0);
                Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueCC, "Cost center") ||
                Sm.IsLueEmpty(LueCCt, "Cost category") ||
                IsGrdEmpty(); 
        }

        private MySqlCommand SaveItemCostCategory(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemCostCategory(ItCode, CCCode, CCtCode, CreateBy, CreateDt) " +
                    "Values(@ItCode, @CCCode, @CCtCode, @CreateBy, CurrentDateTime()) "+
                    "ON Duplicate Key Update CCtCode = @CCtCode,LastUpBy=@UserCode, LastUpDt=CurrentDateTime();  "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetLue(LueCCt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to select at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsChooseData()
        {
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0))
                    {
                        if (IsChoose == false) IsChoose = true;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
            }
            return false;
        }

        #region Setlue

        public void SetLueCCtCode(ref LookUpEdit Lue, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCtCode As Col1, ");
            if(mIsCostCategoryShowCOA)
                SQL.AppendLine("Concat(CCtName,' [', IFNULL(AcNo, ''), '] ') As Col2 ");
            else
                SQL.AppendLine("CCtName As Col2 ");
            SQL.AppendLine("From TblCostCategory ");
            SQL.AppendLine("Where CCCode=@CCCode Order By CCtName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                //"Select CCtCode As Col1, CCtName As Col2 "+
                //"From TblCostCategory " +
                //"Where CCCOde='" + CCCode + "' Order By CCtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode As Col1, A.CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Where A.ActInd='Y'  ");
            if (mIsFilterByCC)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=A.CCCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            //if (mIsShowCostCenterChild) SQL.AppendLine("AND NotParentInd = 'Y' ");
            SQL.AppendLine("Order By A.CCName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCCCodeFilterByProfitCenter(ref LookUpEdit Lue, string Code, string Editable)
        {
            var SQL = new StringBuilder();

            if (Code.Length > 0)
            {
                if (Editable == "N")
                {
                    SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter ");
                    SQL.AppendLine("Where CCCode=@Code");
                    if (mIsFilterByCC)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                        SQL.AppendLine("    Where CCCode=A.CCCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ); ");
                    }
                }
                else
                {
                    SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
                    SQL.AppendLine("From TblCostCenter ");
                    SQL.AppendLine("Where CCCode=@Code Or (1=1 ");
                    SQL.AppendLine("    And NotParentInd='Y' ");
                    SQL.AppendLine("    And ActInd='Y' ");
                    SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And ProfitCenterCode In (");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                    if (mIsFilterByCC)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                        SQL.AppendLine("    Where CCCode=A.CCCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    SQL.AppendLine("   Order By CCName;");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Select A.CCCode As Col1, A.CCName As Col2 ");
                SQL.AppendLine("From TblCostCenter A ");
                SQL.AppendLine("Inner Join TblGroupProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
                SQL.AppendLine("Inner Join TblUser C On B.GrpCode=C.GrpCode And C.UserCode=@UserCode ");
                SQL.AppendLine("Where A.NotParentInd='Y' ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("And A.ProfitCenterCode Is Not Null ");
                if (mIsFilterByCC)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                    SQL.AppendLine("    Where CCCode=A.CCCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By A.CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsCostCategoryShowCOA = Sm.GetParameterBoo("IsCostCategoryShowCOA");
            //mIsShowCostCenterChild = Sm.GetParameterBoo("IsShowCostCenterChild");
            mIsItemCostCategoryUseProfitCenter = Sm.GetParameterBoo("IsItemCostCategoryUseProfitCenter");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCC_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsItemCostCategoryUseProfitCenter)
                Sm.RefreshLookUpEdit(LueCC, new Sm.RefreshLue3(SetLueCCCodeFilterByProfitCenter), string.Empty, "N");
            else
                Sm.RefreshLookUpEdit(LueCC, new Sm.RefreshLue1(SetLueCCCode));
            string CCCode = Sm.GetLue(LueCC);
            if (CCCode.Length == 0) CCCode = "***";
            SetLueCCtCode(ref LueCCt, CCCode);
            SetLueCCtCode(ref LueCCt2, CCCode);
            ClearGrd();
        }

        private void LueCCt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCt, new Sm.RefreshLue2(SetLueCCtCode), Sm.GetLue(LueCC)); 
        }

        private void ChkCCt2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost category");
        }

        private void LueCCt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCt2, new Sm.RefreshLue2(SetLueCCtCode), Sm.GetLue(LueCC));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
