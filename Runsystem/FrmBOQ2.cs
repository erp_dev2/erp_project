﻿#region Update
/*
    11/09/2019 [WED/IMS] new apps for IMS (BOQ with BOM)
    24/09/2019 [WED/IMS] convert tabel nya ke TblBOQ biasa
    24/09/2019 [WED/IMS] validasi, kalau LOP sudah dibuat di BOQ lain, tidak boleh disimpan
    01/10/2019 [WED/IMS] minimal 1 tab terisi (item inventory atau service)
    11/10/2019 [WED/IMS] TotalBOM belom masuk ke clear data
    14/10/2019 [WED/IMS] BOM ambil dari Result
    05/11/2019 [VIN/IMS] menambahkan attachment file
    05/11/2019 [VIN/IMS] Credit limit tidak mandatory
    07/11/2019 [WED/IMS] BOQ tambah input prosentase
    20/11/2019 [DITA/IMS] tambah insert ke BOM Revision
    29/11/2019 [WED/IMS] total inventory belum terkalkulasi pajak nya
    11/12/2019 [DITA+VIN/IMS] Printout BOQ baru
    16/12/2019 [WED/YK] Tax bisa di klik selama masih draft
    06/01/2020 [WED/IMS] parameter baru untuk PPHInventory
    19/01/2020 [TKG/IMS] perhitungan otomatis after sales dan space nego
    19/01/2020 [TKG/IMS] pembulatan round up (satuan) di jasa (sph dan total price), pembulatan round up (ratusan) di inventory (unit price (sph) dan total price (sph)) 
    31/01/2020 [TKG/IMS] 
        BOQ (Tab Service) after sales pengalinya dari total
        BOQ (Tab Service) space nego pengalinya dari kolom "total (+pph)" 
        BOQ (Tab Service) kolom after sales sampai space nego tidak terkunci
        BOQ (Tab Service) Total, Total (+ PPH), SPH dan Total Price di roundup kesatuan
        Penambahan kolom untuk mengisi nomor SPH*
        BOQ (Tab Inventory) space nego pengalinya dari kolom "total (+pph)" 
        BOQ (Tab Inventory) Unit Price (+ PPH) roundup ke satuan
        BOQ (Tab Inventory) Unit Price (SPH) roundup ke ratusan
    06/02/2020 [TKG/IMS] 
        pengalinya dari kolom "unit price (+pph)" , jadi user hanya mengisi ..% di kolom "space nego %" nanti mengali otomatis
        Kolom after sales ;
        - dibuka
        - pengalinya dari kolom "Total" , jadi user hanya mengisi ...% di kolom aftes sales "aftes sales %" 

        Kolom space nego 
        - dibuka
        - pengalinya dari kolom "Total (+pph)" , jadi user hanya mengisi ..% di kolom "space nego %" nanti mengali otomatis
    16/02/2020 [TKG/IMS] tidak menggunakan BOM, langsung menggunakan item
    24/04/2020 [IBL/IMS] Ubah SPHNo menjadi mandatory
    08/05/2020 [VIN/IMS] item bisa di tambah dan dikurangi saat processind masih Draft
    12/05/2020 [WED/IMS] urutan kebalik di grid boq inventory
    18/05/2020 [VIN/IMS] penyesuaian printout BOQ barang & jasa 
    04/06/2020 [TKG/IMS] tambah tax percentage service dan inventory
    08/06/2020 [WED/IMS] BOQ yang sudah dibuat SO Contract active tidak bisa di cancel. yg sudah final, masih bisa cancel
    01/07/2020 [VIN/IMS] feedback printout BOQ service dan inventory data detail tidak muncul
    24/09/2020 [DITA/IMS] tmabah inputan No di detail. item di service/inventory bisa dipilih berkali2 dan remark bisa diedit berkali2 -> param IsRemarkHdrEditableOvertime 
    27/10/2020 [VIN/IMS] tambah inputan SeqNo di detail.di service/inventory 
    11/11/2020 [ICA/IMS] menambah kolom Item Dismantel di Tab Service dan Tab Inventory
    12/11/2020 [ICA/IMS] Beberapa kolom di tab service dan inventory dibuat editable
    25/02/2021 [WED/IMS] SPH masih bisa di edit walaupun sudah final
    25/02/2021 [WED/IMS] tambah kolom Tax di tab Service. Editable sampai sebelum SO Contract. akan mempengaruhi nilai Total +PPH
                         Ada kolom Init Tax Amount (hidden) untuk nge cek ada perbedaan atau enggak di nilai tax awal dengan yg saat ini. karna untuk validasi edit setelah ada SO Contract aktif.
    25/02/2021 [WED/IMS] bug di kalkulasi space nego dan after sales di tab service. kalkulasi nya double.
    26/02/2021 [IBL/IMS] Jasa -> Jika kolom tax pada dtl > 0 maka tax percentage ambil dari kolom tax, jika tidak maka tax percentage ambil dari TxtTaxPercentageService
    08/03/2021 [IBL/IMS] Penyesuaian printout
    03/05/2021 [RDH/IMS] -> tab service roundup SPH dan Total service
    21/04/2022 [RIS/PRODUCT] Menambah lue tax
    31/05/2022 [DITA/YK] Masih muncul warning di boq ketika param PPNDefaultSetting kosong -> ComputeTax
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal FrmBOQ2Find FrmFind;
        private decimal mBOQTaxPercentage = 10m, mBOQ2PPHPercentage = 0m, mBOQ2PPHPercentageInventory = 0m;
        private string mBOQPrintOutBODName = string.Empty, mRptName = string.Empty, mBOQDocType = "2",
            mProcessInd=string.Empty;
        private bool mIsBOQPrintOutShowCreatedUser = false,
            mIsRemarkHdrEditableOvertime =false,
            isinsert = false;
        internal bool mIsFilterBySite = false,
          mIsCreditLimitMandatory = false;
        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty,
          mBOQAutoApprovedByEmpCode = string.Empty,
          mBOQVerifiedByEmpCode = string.Empty,
          mPPNDefaultSetting = string.Empty;

        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmBOQ2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmBOQ2");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                
                TcBOQ2.SelectedTabPage = this.TpBOM;
                SetGrd3();

                TcBOQ2.SelectedTabPage = this.TpBOQ;
                SetGrd();

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueRingCode(ref LueRingCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLuePtCode(ref LuePtCode);
                SetLueSPCode(ref LueSPCode);
                Sl.SetLueAgingAP(ref LueAgingAP);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");
                Sl.SetLueOption(ref LuePPN, "PPNForProject");
                if (mIsCreditLimitMandatory) LblCreditLimit.ForeColor = Color.Red;
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mCtCode.Length != 0)
                {
                    if (IsUserAbleToInsert())
                    {
                        BtnInsertClick();
                        Sm.SetLue(LueCtCode, mCtCode);
                        Sm.SetControlReadOnly(LueCtCode, true);
                        SetLueCtPersonCode(ref LueCtContactPersonName, mCtCode, string.Empty);
                    }
                    else
                    {
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    }
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region BOQ Service

        private void SetGrd()
        {
            Grd2.Cols.Count = 27;
            Grd2.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Specification#",
                    "",
                    "Item's Code",
                    "Item's Name",
                    "Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Minimal"+Environment.NewLine+"Service",
                    "Material",
                    "Total",

                    //11-15
                    "After Sales",
                    "Total"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",
                    "SPH",
                    "Total Price",

                    //16-20
                    "Remark",
                    "Material"+Environment.NewLine+"(%)",
                    "After Sales"+Environment.NewLine+"(%)",
                    "Space Nego"+Environment.NewLine+"(%)",
                    "No",

                    //21-25
                    "Sequence"+Environment.NewLine+"No",
                    "",
                    "Item Dismantle's"+Environment.NewLine+"Code", 
                    "Item Dismantle's"+Environment.NewLine+"Name", 
                    "Tax",

                    //26
                    "Init Tax Amt"
                },
                new int[] 
                {
                    20,
                    200, 20, 100, 180, 180, 
                    100, 100, 120, 120, 120, 
                    120, 120, 120, 120, 120, 
                    200, 120, 120, 120, 100,
                    100, 20, 120, 180, 120,
                    0
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 0, 2, 22 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 26 }, 0);
            Sm.GrdFormatDec(Grd2, new int[] { 25 }, 4);
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 3, 4, 5, 7, 10, 12, 14, 15, 23, 24, 26 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 3, 26 });
            Grd2.Cols[25].Move(12);
            Grd2.Cols[17].Move(10);
            Grd2.Cols[18].Move(13);
            Grd2.Cols[19].Move(16);
            Grd2.Cols[2].Move(5);
            Grd2.Cols[1].Move(6);
            Grd2.Cols[20].Move(2);
            Grd2.Cols[21].Move(1);
            Grd2.Cols[22].Move(6);
            Grd2.Cols[23].Move(7);
            Grd2.Cols[24].Move(8);

        }

        #endregion

        #region BOQ Inventory

        private void SetGrd3()
        {
            Grd3.Cols.Count = 30;
            Grd3.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Specification",
                    "",
                    "Item's Code",
                    "Item's Name",
                    "Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Cost"+Environment.NewLine+"of Goods",
                    "After Sales",
                    "COM",

                    //11-15
                    "OH",
                    "Margin",
                    "Design Cost",
                    "Unit Price"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",

                    //16-20
                    "Unit Price"+Environment.NewLine+"(SPH)",
                    "Total Price"+Environment.NewLine+"(SPH)",
                    "Remark",
                    "After Sales"+Environment.NewLine+"(%)",
                    "COM"+Environment.NewLine+"(%)",

                    //21-25
                    "OH"+Environment.NewLine+"(%)",
                    "Margin"+Environment.NewLine+"(%)",
                    "Design Cost"+Environment.NewLine+"(%)",
                    "Space Nego"+Environment.NewLine+"(%)",
                    "No",

                    //26-29
                    "Seq"+Environment.NewLine+"No",
                    "",
                    "Item Dismantle's"+Environment.NewLine+"Code", 
                    "Item Dismantle's"+Environment.NewLine+"Name", 
                },
                new int[] 
                {
                    20,
                    200, 20, 100, 180, 180, 
                    120, 100, 120, 120, 120, 
                    120, 120, 120, 120, 120,
                    120, 120, 200, 120, 120,
                    120, 120, 120, 120, 100,
                    100, 20, 120, 180
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 0, 2, 27 });
            Sm.GrdFormatDec(Grd3, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 3, 4, 5, 7, 14, 16, 17, 28, 29 });
            Sm.GrdColInvisible(Grd3, new int[] { 1, 2, 3 });
            Grd3.Cols[19].Move(10);
            Grd3.Cols[20].Move(12);
            Grd3.Cols[21].Move(14);
            Grd3.Cols[22].Move(16);
            Grd3.Cols[23].Move(18);
            Grd3.Cols[24].Move(21);
            Grd3.Cols[2].Move(5);
            Grd3.Cols[1].Move(6);
            Grd3.Cols[25].Move(2);
            Grd3.Cols[26].Move(1);
            Grd3.Cols[27].Move(7);
            Grd3.Cols[28].Move(8);
            Grd3.Cols[29].Move(9);
        }

        #endregion

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            mProcessInd = LueProcessInd.Text;
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCtCode, LueCtContactPersonName,LueRingCode, 
                        LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode, LueAgingAP, 
                        LueCurCode, TxtCreditLimit, TxtCtReplace, MeeRemark, TxtLOPDocNo, 
                        TxtDirectCost, TxtIndirectCost, TxtPolicy, LueProcessInd, TxtFile, 
                        ChkFile, TxtSPHNo, TxtTaxPercentageServices, TxtTaxPercentageInventory, LuePPN
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkBankGuaranteeInd.Properties.ReadOnly = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = true;
                    ChkTaxInd.Properties.ReadOnly = true;
                    BtnCtContactPersonName.Enabled = false;
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    BtnLOPDocNo.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = true;
                    Grd2.ReadOnly = Grd3.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode, 
                        LueRingCode, LueAgingAP, LueCurCode, TxtCreditLimit, MeeRemark, 
                        TxtDirectCost, TxtIndirectCost, TxtPolicy, TxtSPHNo, TxtTaxPercentageServices, 
                        TxtTaxPercentageInventory
                    }, false);
                    DteDocDt.Focus();
                    TxtQtMth.EditValue = '0';
                    ChkActInd.Checked = true;
                    ChkTaxInd.Properties.ReadOnly = false;
                    ChkBankGuaranteeInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Checked = true;
                    BtnCtContactPersonName.Enabled = true;
                    BtnCtReplace.Enabled = true;
                    BtnCtReplace2.Enabled = true;
                    BtnLOPDocNo.Enabled = true;
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = false;
                    //Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 13 });
                    Grd2.ReadOnly = Grd3.ReadOnly = false;
                    Sm.GrdColReadOnly(false, false, Grd2, new int[] { 0, 20, 22 });
                    Sm.GrdColReadOnly(false, false, Grd3, new int[] { 0, 25, 27 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSPHNo }, false);
                    if (mIsRemarkHdrEditableOvertime && ChkActInd.Checked == true) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    if (mProcessInd == "Draft")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueCtContactPersonName, LueSPCode, DteQtStartDt, TxtQtMth, LuePtCode, 
                            LueRingCode, LueProcessInd, LueAgingAP, LueCurCode, TxtCreditLimit, 
                            TxtDirectCost, TxtIndirectCost, TxtPolicy, MeeRemark
                        }, false);
                        Grd2.ReadOnly = Grd3.ReadOnly = false;
                        Sm.GrdColReadOnly(false, false, Grd2, new int[] { 20, 22 });
                        Sm.GrdColReadOnly(false, false, Grd3, new int[] { 25, 27 });
                        BtnCtContactPersonName.Enabled = true;
                        ChkBankGuaranteeInd.Properties.ReadOnly = false;
                        ChkTaxInd.Properties.ReadOnly = false;
                        if (ChkTaxInd.Checked)
                        {
                            LuePPN.Properties.ReadOnly = false;
                            LuePPN.BackColor = Color.FromArgb(255, 255, 255);
                        }
                    }
                    else
                    {
                        if (!IsUserUnAbleToEditBOQDtl())
                        {
                            Grd2.ReadOnly = Grd3.ReadOnly = false;
                            Sm.GrdColReadOnly(true, false, Grd2, new int[] { 0, 6, 20, 21, 22 });
                            Sm.GrdColReadOnly(true, false, Grd3, new int[] { 0, 6, 7, 14, 16, 17, 25, 26, 27 });
                        }
                        ChkBankGuaranteeInd.Properties.ReadOnly = true;
                    }
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    BtnCtReplace.Enabled = false;
                    BtnCtReplace2.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            ChkTaxInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQtMth, LueCtCode, LueCtContactPersonName,LueRingCode, LueSPCode, DteQtStartDt,LuePtCode,
                LueAgingAP, LueCurCode, TxtCtReplace, MeeRemark, TxtLOPDocNo, TxtStatus, LueProcessInd, TxtFile, TxtSPHNo, LuePPN
            });
            ClearGrd();
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtTotalBOM, TxtCreditLimit, TxtSubTotal, TxtTax, TxtGrandTotal, 
                TxtDirectCost, TxtIndirectCost, TxtPolicy, TxtSubTotalRBPS, TxtPercentage
            }, 0);
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            { TxtTaxPercentageServices, TxtTaxPercentageInventory }, 8);
            ChkPrintSignatureInd.Checked = false;
            ChkActInd.Checked = false;
            ChkFile.Checked = false;
            ChkBankGuaranteeInd.Checked = false;
            PbUpload.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 25, 26 });
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBOQ2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteQtStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
                TxtTaxPercentageServices.EditValue = Sm.FormatNum(mBOQ2PPHPercentage, 0);
                TxtTaxPercentageInventory.EditValue = Sm.FormatNum(mBOQ2PPHPercentageInventory, 0);
                Sm.SetLue(LuePPN, mPPNDefaultSetting);
                isinsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            isinsert = true;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            isinsert = false;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        #region Grid 2 - Service Item

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 25, 26 }, e);
                if (Sm.IsGrdColSelected(new int[] { 6, 8, 9, 11, 13, 17, 18, 19, 25 }, e.ColIndex))
                {
                    ComputeMaterialPercentage(e.RowIndex, e.ColIndex);
                    ComputeTotalDtl(e.RowIndex);
                    ComputeAfterSalesPercentage(e.RowIndex, e.ColIndex);
                    ComputeAfterSalesAmt(e.RowIndex, e.ColIndex);
                    ComputeTotalPPHDtl(e.RowIndex);
                    ComputeSpaceNegoPercentage(e.RowIndex, e.ColIndex);
                    ComputeSpaceNegoAmt(e.RowIndex, e.ColIndex);
                    ComputeSPHDtl(e.RowIndex);
                    ComputeTotalPriceDtl(e.RowIndex);
                    ComputeSubTotal();
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 22 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 3, false, "Item's code is Empty"))
                    Sm.FormShowDialog(new FrmBOQ2Dlg5(this, "1", e.RowIndex));
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtLOPDocNo, "LOP", false) && TxtDocNo.Text.Length >= 0)
                {
                    Sm.FormShowDialog(new FrmBOQ2Dlg3(this, "1"));
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 22 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 3, false, "Item's code is Empty"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmBOQ2Dlg5(this, "1", e.RowIndex));
                }
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtLOPDocNo, "LOP", false) && TxtDocNo.Text.Length >= 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmBOQ2Dlg3(this, "1"));
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length >= 0)
                {
                    Sm.GrdRemoveRow(Grd2, e, BtnSave);
                    ComputeSubTotal();
                }
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Grid 3 - Inventory Item

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 }, e);
                if (Sm.IsGrdColSelected(new int[] { 6, 8, 9, 10, 11, 12, 13, 15, 19, 20, 21, 22, 23, 24 }, e.ColIndex))
                {
                    ComputeAfterSalesPercentage2(e.RowIndex, e.ColIndex);
                    ComputeCOMPercentage(e.RowIndex, e.ColIndex);
                    ComputeOHPercentage(e.RowIndex, e.ColIndex);
                    ComputeMarginPercentage(e.RowIndex, e.ColIndex);
                    ComputeDesignCostPercentage(e.RowIndex, e.ColIndex);
                    ComputeSpaceNegoPercentage2(e.RowIndex, e.ColIndex);
                    ComputeUPricePPHDtl2(e.RowIndex);
                    ComputeUPriceSPHDtl2(e.RowIndex);
                    ComputeTotalPriceSPHDtl2(e.RowIndex);
                    ComputeTotalBOM();
                }
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 27 && !Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 3, false, "Item's code is Empty"))
                    Sm.FormShowDialog(new FrmBOQ2Dlg5(this, "2", e.RowIndex));
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtLOPDocNo, "LOP", false) && TxtDocNo.Text.Length >= 0)
                {
                    Sm.FormShowDialog(new FrmBOQ2Dlg3(this, "2"));
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 27 && !Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 3, false, "Item's code is Empty"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmBOQ2Dlg5(this, "2", e.RowIndex));
                }
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtLOPDocNo, "LOP", false) && TxtDocNo.Text.Length >= 0)
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmBOQ2Dlg3(this, "2"));
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length >= 0)
                {
                    Sm.GrdRemoveRow(Grd3, e, BtnSave);
                    ComputeTotalBOM();
                }
                Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid() 
               ) return;

            if (decimal.Parse(TxtDirectCost.Text) == 0 || decimal.Parse(TxtIndirectCost.Text) == 0 || decimal.Parse(TxtPolicy.Text) == 0)
            {
                if (
                Sm.StdMsgYN("Question",
                       "Direct Cost: " + TxtDirectCost.Text + Environment.NewLine +
                       "Indirect Cost: " + TxtIndirectCost.Text + Environment.NewLine +
                       "Policy: " + TxtPolicy.Text + Environment.NewLine +
                       "Do you want to save this data ?"
                      ) == DialogResult.No
                    ) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOQ", "TblBOQHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBOQ2Hdr(DocNo));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0) cml.Add(SaveBOQ2Dtl(DocNo, Row));
            }
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 3).Length > 0) cml.Add(SaveBOQ2Dtl2(DocNo, Row));
            }

            if (TxtCtReplace.Text.Length != 0)
                cml.Add(CancelBOQReplacement(TxtCtReplace.Text));
           

            Sm.ExecCommands(cml);
            if (TxtFile.Text.Length > 0)UploadFile(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person") ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsTxtEmpty(TxtSPHNo, "SPH#", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                (mIsCreditLimitMandatory && Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true)) ||
                IsLOPAlreadyProceed() ||
                IsLOPCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsLOPAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where LOPDocNo = @Param ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data has been proceed in BOQ#" + Sm.GetValue(SQL.ToString(), TxtLOPDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsLOPCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblLOPHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C' Or ProcessInd = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP document is cancelled.");
                TxtLOPDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count-1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 20, false, "No in BOQ Service is empty.")) { TcBOQ2.SelectedTabPage = this.TpBOQ; return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 21, false, "SeqNo in BOQ Service is empty.")) { TcBOQ2.SelectedTabPage = this.TpBOQ; return true; }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd2, Row, 20)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in BOQ Service exceed maximum length character (4).");
                        return true;
                    }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd2, Row, 21)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Sequence No in BOQ Service exceed maximum length character (3).");
                        return true;
                    }
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 23, false, "Item Dismantle's Code is empty.")) { TcBOQ2.SelectedTabPage = this.TpBOQ; return true; } 
                    if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0)
                    {
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 6, true, "Quantity is zero.")) { TcBOQ2.SelectedTabPage = this.TpBOQ; return true; }
                    }
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count-1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 25, false, "No in BOQ Inventory is empty.")) { TcBOQ2.SelectedTabPage = this.TpBOM; return true; }
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 26, false, "Sequence No in BOQ Inventory is empty.")) { TcBOQ2.SelectedTabPage = this.TpBOM; return true; }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd3, Row, 25)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in BOQ Inventory exceed maximum length character (4).");
                        return true;
                    }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd3, Row, 26)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Sequence No in BOQ Inventory exceed maximum length character (3).");
                        return true;
                    }
                    if (Sm.GetGrdStr(Grd3, Row, 3).Length > 0)
                    {
                        if (Sm.IsGrdValueEmpty(Grd3, Row, 6, true, "Quantity is zero.")) { TcBOQ2.SelectedTabPage = this.TpBOM; return true; }
                        if (Sm.IsGrdValueEmpty(Grd3, Row, 8, true, "Cost of Goods is zero.")) { TcBOQ2.SelectedTabPage = this.TpBOM; return true; }
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count <= 1 && Grd3.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "At least 1 item service or inventory is listed.");
                return true;
            }
            
            return false;
        }

        private MySqlCommand SaveBOQ2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQHdr(DocNo, DocDt, DocType, ActInd, Status, ProcessInd, LOPDocNo, CtCode, ");
            SQL.AppendLine("CtContactPersonName, RingCode, SPCode, QtStartDt, QtMth, PtCode, AgingAp, CurCode, ");
            SQL.AppendLine("CreditLimit, PrintSignatureInd, CtQtDocNoReplace, SubTotal, Tax, TaxInd, GrandTotal, ");
            SQL.AppendLine("DirectCost, IndirectCost, Policy, SubTotalRBPS, SPHNo, TaxPercentageServices, TaxPercentageInventory, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt, BankGuaranteeInd, TotalBOM, TaxCode) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, @ActInd, 'O', 'D', @LOPDocNo, @CtCode, ");
            SQL.AppendLine("@CtContactPersonName, @RingCode, @SPCode, @QtStartDt, @QtMth, @PtCode, @AgingAp, @CurCode, ");
            SQL.AppendLine("@CreditLimit, @PrintSignatureInd, @CtQtDocNoReplace, @SubTotal, @Tax, @TaxInd, @GrandTotal, ");
            SQL.AppendLine("@DirectCost, @IndirectCost, @Policy, @SubTotalRBPS, @SPHNo, @TaxPercentageServices, @TaxPercentageInventory, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime(), @BankGuaranteeInd, @TotalBOM, @TaxCode); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mBOQDocType);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", TxtCtReplace.Text);
            Sm.CmParam<Decimal>(ref cm, "@SubTotal", Decimal.Parse(TxtSubTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxInd", ChkTaxInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@Policy", Decimal.Parse(TxtPolicy.Text));
            Sm.CmParam<Decimal>(ref cm, "@SubTotalRBPS", Decimal.Parse(TxtSubTotalRBPS.Text));
            Sm.CmParam<String>(ref cm, "@SPHNo", TxtSPHNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@TotalBOM", Decimal.Parse(TxtTotalBOM.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxPercentageServices", Decimal.Parse(TxtTaxPercentageServices.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxPercentageInventory", Decimal.Parse(TxtTaxPercentageInventory.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LuePPN));

            return cm;        
        }

        private MySqlCommand SaveBOQ2Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQDtl2(DocNo, No, SeqNo, ItCode, ItCodeDismantle, Qty, Amt, MinimalServiceAmt, ");
            SQL.AppendLine("MaterialAmt, MaterialPercentage, AfterSalesAmt, AfterSalesPercentage, TotalPPHAmt, ");
            SQL.AppendLine("SpaceNegoAmt, SpaceNegoPercentage, SPHAmt, TotalAmt, Tax, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @No, @SeqNo, @ItCode, @ItCodeDismantle, @Qty, @Amt, @MinimalServiceAmt, ");
            SQL.AppendLine("@MaterialAmt, @MaterialPercentage, @AfterSalesAmt, @AfterSalesPercentage, @TotalPPHAmt, ");
            SQL.AppendLine("@SpaceNegoAmt, @SpaceNegoPercentage, @SPHAmt, @TotalAmt, @Tax, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
            
            SQL.AppendLine("On Duplicate Key Update ");

            SQL.AppendLine("    Qty = @Qty, Amt = @Amt, ");
            SQL.AppendLine("    MinimalServiceAmt = @MinimalServiceAmt, MaterialAmt = @MaterialAmt, MaterialPercentage = @MaterialPercentage, ");
            SQL.AppendLine("    AfterSalesAmt = @AfterSalesAmt, AfterSalesPercentage = @AfterSalesPercentage, TotalPPHAmt = @TotalPPHAmt, ");
            SQL.AppendLine("    SpaceNegoAmt = @SpaceNegoAmt, SpaceNegoPercentage = @SpaceNegoPercentage, SPHAmt = @SPHAmt, TotalAmt = @TotalAmt, ");
            SQL.AppendLine("    Tax = @Tax, Remark = @Remark, SeqNo=@SeqNo, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 3));
            //Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            //Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@MinimalServiceAmt", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@MaterialAmt", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@AfterSalesAmt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@TotalPPHAmt", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@SpaceNegoAmt", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@SPHAmt", Sm.GetGrdDec(Grd2, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd2, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@MaterialPercentage", Sm.GetGrdDec(Grd2, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@AfterSalesPercentage", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@SpaceNegoPercentage", Sm.GetGrdDec(Grd2, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 16));
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd2, Row, 20));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd2, Row, 21));
            Sm.CmParam<String>(ref cm, "@ItCodeDismantle", Sm.GetGrdStr(Grd2, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd2, Row, 25));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOQ2Dtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQDtl3(DocNo, SeqNo, No, ItCode, ItCodeDismantle, Qty, CostOfGoodsAmt, ");
            SQL.AppendLine("AfterSalesAmt, AfterSalesPercentage, COMAmt, COMPercentage, OHAmt, OHPercentage, ");
            SQL.AppendLine("MarginAmt, MarginPercentage, DesignCostAmt, DesignCostPercentage, UPricePPHAmt, ");
            SQL.AppendLine("SpaceNegoAmt, SpaceNegoPercentage, UPriceSPHAmt, ");
            SQL.AppendLine("TotalPriceSPHAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SeqNo, @No, @ItCode, @ItCodeDismantle, @Qty, @CostOfGoodsAmt, ");
            SQL.AppendLine("@AfterSalesAmt, @AfterSalesPercentage, @COMAmt, @COMPercentage, @OHAmt, @OHPercentage, ");
            SQL.AppendLine("@MarginAmt, @MarginPercentage, @DesignCostAmt, @DesignCostPercentage, @UPricePPHAmt, ");
            SQL.AppendLine("@SpaceNegoAmt, @SpaceNegoPercentage, @UPriceSPHAmt, ");
            SQL.AppendLine("@TotalPriceSPHAmt, @Remark, @CreateBy, CurrentDateTime()) ");

            SQL.AppendLine("On Duplicate Key Update ");

            SQL.AppendLine("    Qty = @Qty, CostOfGoodsAmt = @CostOfGoodsAmt, ");
            SQL.AppendLine("    AfterSalesAmt = @AfterSalesAmt, AfterSalesPercentage = @AfterSalesPercentage, COMAmt = @COMAmt, ");
            SQL.AppendLine("    COMPercentage = @COMPercentage, OHAmt = @OHAmt, OHPercentage = @OHPercentage, ");
            SQL.AppendLine("    MarginAmt = @MarginAmt, MarginPercentage = @MarginPercentage, DesignCostAmt = @DesignCostAmt, ");
            SQL.AppendLine("    DesignCostPercentage = @DesignCostPercentage, UPricePPHAmt = @UPricePPHAmt, ");
            SQL.AppendLine("    SpaceNegoAmt = @SpaceNegoAmt, SpaceNegoPercentage = @SpaceNegoPercentage, UPriceSPHAmt = @UPriceSPHAmt, ");
            SQL.AppendLine("    Remark = @Remark, TotalPriceSPHAmt = @TotalPriceSPHAmt, SeqNo=@SeqNo, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd3, Row, 1));
            //Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@CostOfGoodsAmt", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@AfterSalesAmt", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@COMAmt", Sm.GetGrdDec(Grd3, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@OHAmt", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@MarginAmt", Sm.GetGrdDec(Grd3, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@DesignCostAmt", Sm.GetGrdDec(Grd3, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@UPricePPHAmt", Sm.GetGrdDec(Grd3, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@SpaceNegoAmt", Sm.GetGrdDec(Grd3, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@UPriceSPHAmt", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@TotalPriceSPHAmt", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@AfterSalesPercentage", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@COMPercentage", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@OHPercentage", Sm.GetGrdDec(Grd3, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@MarginPercentage", Sm.GetGrdDec(Grd3, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@DesignCostPercentage", Sm.GetGrdDec(Grd3, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@SpaceNegoPercentage", Sm.GetGrdDec(Grd3, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 18));
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd3, Row, 25));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd3, Row, 26));
            Sm.CmParam<String>(ref cm, "@ItCodeDismantle", Sm.GetGrdStr(Grd3, Row, 28));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand UpdateBOQFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }


        private MySqlCommand CancelBOQReplacement(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOQHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            string DocNo = TxtDocNo.Text;
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsActIndDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(UpdateBOQHdr(TxtDocNo.Text));

            if (ChkActInd.Checked)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0) cml.Add(SaveBOQ2Dtl(TxtDocNo.Text, Row));
                }

                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 3).Length > 0) cml.Add(SaveBOQ2Dtl2(TxtDocNo.Text, Row));
                }
            }

            cml.Add(AddApprovalData());

            //if (Sm.GetLue(LueProcessInd) == "F")
            //{
            //    string DocNoRev = string.Concat(TxtDocNo.Text, "/0001");
            //    int Row2 = 0;
            //    cml.Add(SaveBOMRevisionHdr(DocNoRev));

            //    for (int Row = 0; Row < Grd2.Rows.Count; ++Row)
            //    {
            //        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
            //        {
            //            Row2 += 1;
            //            cml.Add(SaveBOMRevisionDtl(DocNoRev, Row));
            //        }
            //    }

            //    for (int Row = 0; Row < Grd3.Rows.Count; ++Row)
            //    {
            //        if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
            //        {
            //            cml.Add(SaveBOMRevisionDtl2(DocNoRev, Row2, Row));
            //            Row2 += 1;
            //        }
            //    }
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person") ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsTxtEmpty(TxtSPHNo, "SPH#", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                (mIsCreditLimitMandatory && Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true)) ||
                IsLOPCancelled() ||
                IsActIndEditedAlready() ||
                //IsProcessedToSOContract() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsTaxForbiddenToEdit()
                ;
        }

        private bool IsTaxForbiddenToEdit()
        {
            bool IsProcessedToSOContract = false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where BOQDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            IsProcessedToSOContract = Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text);

            if (IsProcessedToSOContract)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdDec(Grd2, i, 25) != Sm.GetGrdDec(Grd2, i, 26))
                    {
                        Sm.StdMsg(mMsgType.Warning, "You could not edit tax in row #" + (i+1).ToString() + " due to it's already processed to SO Contract.");
                        Sm.FocusGrd(Grd2, i, 25);
                        return true;
                    }
                }
            }

            return false;
        }

        

        private bool IsProcessedToSOContract()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select 1 ");
            //SQL.AppendLine("From TblBOQHdr ");
            //SQL.AppendLine("Where DocNo = @Param ");
            //SQL.AppendLine("And ProcessInd = 'F'; ");

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("And BOQDocNo = @Param ");
            SQL.AppendLine("Limit 1 ");
            SQL.AppendLine("; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to SOContract #" + Sm.GetValue(SQL.ToString(), TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsActIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblBOQHdr Where DocNo=@DocNo And (ActInd='N' Or Status = 'C'); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already inactive.");
                return true;
            }
            return false;
        }

        //private MySqlCommand SaveBOMRevisionHdr(string DocNo)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Insert Into TblBOMRevisionHdr(DocNo, DocDt, BOQDocNo, Remark, CreateBy, CreateDt)");
        //    SQL.AppendLine("Select @DocNo, @DocDt, @BOQDocNo, @Remark, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBOQHdr Where DocNo = @BOQDocNo And ActInd = 'Y' And ProcessInd = 'F'; ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveBOMRevisionDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Insert Into TblBOMRevisionDtl(DocNo, DNo, MaterialResultInd, BOMDocNo, BOMDNo, Qty, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocNo, @DNo, '2', @BOMDocNo, @BOMDNo, @Qty, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBOQHdr A ");
        //    SQL.AppendLine("Where A.DocNo = @BOQDocNo ");
        //    SQL.AppendLine("And A.ActInd = 'Y' And A.ProcessInd = 'F'; ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
        //    Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveBOMRevisionDtl2(string DocNo, int DNo, int Row)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Insert Into TblBOMRevisionDtl(DocNo, DNo, MaterialResultInd, BOMDocNo, BOMDNo, Qty, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocNo, @DNo, '2', @BOMDocNo, @BOMDNo, @Qty, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblBOQHdr A ");
        //    SQL.AppendLine("Where A.DocNo = @BOQDocNo ");
        //    SQL.AppendLine("And A.ActInd = 'Y' And A.ProcessInd = 'F'; ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (DNo + 1).ToString(), 5));
        //    Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd3, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateBOQHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    ProcessInd = @ProcessInd, ");
            SQL.AppendLine("    LOPDocNo = @LOPDocNo, CtContactPersonName = @CtContactPersonName, ");
            SQL.AppendLine("    RingCode = @RingCode, SPCode = @SPCode, QtStartDt = @QtStartDt, ");
            SQL.AppendLine("    QtMth = @QtMth, PtCode = @PtCode, AgingAp = @AgingAp,  ");
            SQL.AppendLine("    CurCode = @CurCode, CreditLimit = @CreditLimit,  ");
            SQL.AppendLine("    PrintSignatureInd = @PrintSignatureInd, SPHNo=@SPHNo, Remark = @Remark, ");
            SQL.AppendLine("    ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime(), BankGuaranteeInd=@BankGuaranteeInd, ");
            SQL.AppendLine("    TotalBOM = @TotalBOM ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            SQL.AppendLine("Update TblBOQHdr Set  ");
            SQL.AppendLine("    SubTotal = @SubTotal, Tax = @Tax, TaxInd = @TaxInd, GrandTotal = @GrandTotal, ");
            SQL.AppendLine("    DirectCost = @DirectCost, IndirectCost = @IndirectCost, Policy = @Policy, SubTotalRBPS = @SubTotalRBPS, TaxCode = @TaxCode ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            SQL.AppendLine("Delete From TblBOQDtl2 Where DocNo=@DocNo And Exists (Select 1 From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            SQL.AppendLine("Delete From TblBOQDtl3 Where DocNo=@DocNo And Exists (Select 1 From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxInd", (ChkTaxInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<Decimal>(ref cm, "@SubTotal", Decimal.Parse(TxtSubTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@Policy", Decimal.Parse(TxtPolicy.Text));
            Sm.CmParam<Decimal>(ref cm, "@SubTotalRBPS", Decimal.Parse(TxtSubTotalRBPS.Text));
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SPHNo", TxtSPHNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@TotalBOM", Decimal.Parse(TxtTotalBOM.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LuePPN));

            return cm;
        }

        private MySqlCommand AddApprovalData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BOQ' ");
                SQL.AppendLine("And T.SiteCode In ( Select SiteCode From TblLOPHdr Where DocNo = @LOPDocNo ) ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblBOQHdr ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    And ActInd = 'Y' ");
                SQL.AppendLine("    And ProcessInd = 'F' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.GrandTotal*1 ");
                SQL.AppendLine("    From TblBOQHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBOQHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BOQ' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBOQ2Hdr(DocNo);
                ShowBOQ2Dtl(DocNo);
                ShowBOQ2Dtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBOQ2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From TblBOQHdr Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "ActInd", "CtCode", "CtContactPersonName", "RingCode", 
                    
                    //6-10
                    "SPCode", "QtStartDt", "QtMth", "PtCode", "AgingAP", 

                    //11-15
                    "CurCode", "CreditLimit", "PrintSignatureInd", "CtQtDocNoReplace", "Remark",

                    //16-20
                    "LOPDocNo", "Status", "SubTotal", "Tax", "GrandTotal", 
                    
                    //21-25
                    "TaxInd", "DirectCost", "IndirectCost", "Policy", "SubTotalRBPS",
                    
                    //26-30
                    "ProcessInd", "BankGuaranteeInd", "TotalBOM", "FileName", "SPHNo",

                    //31-32
                    "TaxPercentageServices", "TaxPercentageInventory", "TaxCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]), Sm.DrStr(dr, c[4]));
                    //Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[6]));
                    Sm.SetDte(DteQtStartDt, Sm.DrStr(dr, c[7]));
                    TxtQtMth.EditValue = Sm.DrStr(dr, c[8]);
                    
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueAgingAP, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[11]));
                    TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y");
                    TxtCtReplace.EditValue = Sm.DrStr(dr, c[14]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[16]);
                    string mStatusDesc = string.Empty;
                    if (Sm.DrStr(dr, c[17]) == "A") mStatusDesc = "Approved";
                    if (Sm.DrStr(dr, c[17]) == "C") mStatusDesc = "Cancelled";
                    if (Sm.DrStr(dr, c[17]) == "O") mStatusDesc = "Outstanding";
                    TxtStatus.EditValue = mStatusDesc;
                    TxtSubTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    TxtTotalBOM.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                    TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                    TxtGrandTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                    ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[21]), "Y");
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    TxtPolicy.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    TxtSubTotalRBPS.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[26]));
                    ChkBankGuaranteeInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[27]), "Y");
                    TxtFile.EditValue = Sm.DrStr(dr, c[29]);
                    TxtSPHNo.EditValue = Sm.DrStr(dr, c[30]);
                    TxtTaxPercentageServices.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 3);
                    TxtTaxPercentageInventory.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[32]), 3);
                    if (Sm.DrStr(dr, c[33]) == "")
                        Sm.SetLue(LuePPN, mPPNDefaultSetting);
                    else
                        Sm.SetLue(LuePPN, Sm.DrStr(dr, c[33]));
                }, true
            );
            ComputePercentage();
        }

        private void ShowBOQ2Dtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.No, A.SeqNo, CAST(A.seqno AS UNSIGNED) AS SeqNo, B.Specification, A.ItCode, A.ItCodeDismantle, B.ItName, C.ItName as ItNameDismantle, B.ItCodeInternal, A.Qty, ");
            SQLDtl.AppendLine("B.PurchaseUomCode, A.MinimalServiceAmt, A.MaterialAmt, A.Amt, A.AfterSalesAmt, ");
            SQLDtl.AppendLine("A.TotalPPHAmt, A.SpaceNegoAmt, A.SPHAmt, A.Tax, A.TotalAmt, A.Remark, A.MaterialPercentage, ");
            SQLDtl.AppendLine("A.AfterSalesPercentage, A.SpaceNegoPercentage ");
            SQLDtl.AppendLine("From TblBOQDtl2 A ");
            SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQLDtl.AppendLine("Left Join TblItem C On A.ItCodeDismantle=C.ItCode ");
            SQLDtl.AppendLine("Where A.DocNo=@DocNo ORDER BY SeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "Specification",
                    
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Qty", "PurchaseUomCode", 
        
                    //6-10
                    "MinimalServiceAmt", "MaterialAmt", "Amt", "AfterSalesAmt", "TotalPPHAmt", 
        
                    //11-15
                    "SpaceNegoAmt", "SPHAmt", "TotalAmt", "Remark", "MaterialPercentage", 

                    //16-120
                    "AfterSalesPercentage", "SpaceNegoPercentage", "No", "SeqNo", "ItCodeDismantle",

                    //21-22
                    "ItNameDismantle", "Tax"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 25, 26 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowBOQ2Dtl2(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.No, A.SeqNo, CAST(A.seqno AS UNSIGNED) AS SeqNo, B.Specification, A.ItCode, A.ItCodeDismantle, B.ItName, C.ItName as ItNameDismantle, B.ItCodeInternal, A.Qty, ");
            SQLDtl.AppendLine("B.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQLDtl.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, A.Remark, ");
            SQLDtl.AppendLine("A.AfterSalesPercentage, A.COMPercentage, A.OHPercentage, A.MarginPercentage, A.DesignCostPercentage, A.SpaceNegoPercentage ");
            SQLDtl.AppendLine("From TblBOQDtl3 A ");
            SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQLDtl.AppendLine("Left Join TblItem C On A.ItCodeDismantle=C.ItCode ");
            SQLDtl.AppendLine("Where A.DocNo=@DocNo ORDER BY SeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "Specification",
                    
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Qty", "PurchaseUomCode", 
        
                    //6-10
                    "CostOfGoodsAmt", "AfterSalesAmt", "COMAmt", "OHAmt", "MarginAmt", 
        
                    //11-15
                    "DesignCostAmt", "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", "TotalPriceSPHAmt", 

                    //16-20
                    "Remark", "AfterSalesPercentage", "COMPercentage", "OHPercentage", "MarginPercentage", 
                    
                    //21-25
                    "DesignCostPercentage", "SpaceNegoPercentage", "No", "SeqNo", "ItCodeDismantle",

                    //26
                    "ItNameDismantle"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        private bool IsExceedMaxChar(string Data)
        {
            return Data.Length > 4;
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "BOQ", "BOQDtlInventory", "BOQDtlService", "BOQSign", "BOQSign2", "BOQSign3" };

            var l = new List<BOQ>();
            var l2 = new List<BOQDtlInventory>();
            var l3 = new List<BOQDtlService>();
            var lDtlS = new List<BOQSign>();
            var lDtlS2 = new List<BOQSign2>();
            var lDtlS3 = new List<BOQSign2>();
            int Nomor = 1;
            int nomor2 = 1;


            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',  ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, IFNULL (D.ProjectName, B.ProjectName) ProjectName, ");
            SQL.AppendLine("D.ProjectCode , E.PtName ");
            SQL.AppendLine("From TblBOQHdr A  ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo=B.DocNo  ");
            SQL.AppendLine("Left Join TblSite C On B.SiteCode = C.SiteCode  ");
            SQL.AppendLine("Left Join TblProjectGroup D ON B.PGCode = D.PGCode ");
            SQL.AppendLine("Left Join TblPaymentTerm E ON A.PtCode = E.PtCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "DocNo", 
                         "DocDt",

                         //6-7
                         "ProjectName",
                         "ProjectCode",
                         "PtName"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BOQ()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            ProjectName = Sm.DrStr(dr, c[6]),
                            ProjectCode = Sm.DrStr(dr, c[7]),
                            PtName = Sm.DrStr(dr, c[8]),
                            SPHNo = TxtSPHNo.Text,
                            CUrrency = LueCurCode.Text,

                            Remark = MeeRemark.Text
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region BOQ Detail Inventory

            for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
            {
                    l2.Add(new BOQDtlInventory()
                    {
                        No = Sm.GetGrdStr(Grd3, i, 25),
                        ItCodeInternal = Sm.GetGrdStr(Grd3, i, 5),
                        ItName = Sm.GetGrdStr(Grd3, i, 4),
                        Qty = Sm.GetGrdDec(Grd3, i, 6),
                        PurchaseUOM = Sm.GetGrdStr(Grd3, i, 7),
                        CostofGoodsAmt = Sm.GetGrdDec(Grd3, i, 8),

                        AfterSalesPercentage = Sm.GetGrdDec(Grd3, i, 19),
                        //COMPercentage = Sm.GetGrdDec(Grd3, i, 20),
                        //OHPercentage = Sm.GetGrdDec(Grd3, i, 21),
                        //MarginPercentage = Sm.GetGrdDec(Grd3, i, 22),
                        DesignCostAmt = Sm.GetGrdDec(Grd3, i, 13),
                        UPricePPHAmt = Sm.GetGrdDec(Grd3, i, 14),
                        SpaceNegoAmt = Sm.GetGrdDec(Grd3, i, 15),
                        UPriceSPHAmt = Sm.GetGrdDec(Grd3, i, 16),

                        TotalPriceSPHAmt = Sm.GetGrdDec(Grd3, i, 17),
                        //Remark = Sm.GetGrdStr(Grd3, i, 18),
                        Spesification = Sm.GetGrdStr(Grd3, i, 18),
                        UOM = Sm.GetGrdStr(Grd3, i, 18),
                        AfterSalesAmt = Sm.GetGrdDec(Grd3, i, 9),
                        COMAmt = Sm.GetGrdDec(Grd3, i, 10),
                        OHAmt = Sm.GetGrdDec(Grd3, i, 11),
                        MarginAmt = Sm.GetGrdDec(Grd3, i, 12)
                    });
                    Nomor += 1;
                
            }
            myLists.Add(l2);

            #endregion

            #region BOQ Detail Services

            for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
            {
                l3.Add(new BOQDtlService()
                {
                    No = Sm.GetGrdDec(Grd2, i, 20),
                    ItCodeInternal = Sm.GetGrdStr(Grd2, i, 5),
                    ItName = Sm.GetGrdStr(Grd2, i, 4),
                    Qty = Sm.GetGrdDec(Grd2, i, 6),
                    PurchaseUOM = Sm.GetGrdStr(Grd2, i, 7),
                    MinimalServiceAmt = Sm.GetGrdDec(Grd2, i, 8),
                    AfterSalesPercentage = Sm.GetGrdDec(Grd2, i, 18),
                    MaterialAmt = Sm.GetGrdDec(Grd2, i, 9),
                    Amt = Sm.GetGrdDec(Grd2, i, 10),
                    TotalPPHAmt = Sm.GetGrdDec(Grd2, i, 12),
                    SpaceNegoAmt = Sm.GetGrdDec(Grd2, i, 13),
                    UPriceSPHAmt = Sm.GetGrdDec(Grd2, i, 14),
                    TotalAmt = Sm.GetGrdDec(Grd2, i, 15),
                    ItNameDismantle = Sm.GetGrdStr(Grd2, i, 24),
                });
                nomor2 += 1;

            }
            myLists.Add(l3);

            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T5.DeptName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblBOQHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where  A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Left Join TblDepartment T5 On T5.DeptCode = T2.DeptCode ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6-7
                         "LastupDt",
                         "DeptName"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new BOQSign()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]).ToUpper(),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6]),
                            DeptName = Sm.DrStr(drDtlS, cDtlS[7])
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                SQLDtlS2.AppendLine("Select A.EmpName, B.PosName, C.DeptName  ");
                SQLDtlS2.AppendLine("From TblEmployee A  ");
                SQLDtlS2.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS2.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode  ");

                SQLDtlS2.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode", mBOQAutoApprovedByEmpCode);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-2
                         "PosName" ,
                         "DeptName"
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new BOQSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                            DeptName = Sm.DrStr(drDtlS2, cDtlS2[2])
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);

            //Diperiksa Oleh
            var cmDtlS3 = new MySqlCommand();

            var SQLDtlS3 = new StringBuilder();
            using (var cnDtlS3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS3.Open();
                cmDtlS3.Connection = cnDtlS3;

                SQLDtlS3.AppendLine("Select A.EmpName, B.PosName, C.DeptName  ");
                SQLDtlS3.AppendLine("From TblEmployee A  ");
                SQLDtlS3.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS3.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode  ");

                SQLDtlS3.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS3.CommandText = SQLDtlS3.ToString();
                Sm.CmParam<String>(ref cmDtlS3, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS3, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS3, "@EmpCode", mBOQVerifiedByEmpCode);
                var drDtlS3 = cmDtlS3.ExecuteReader();
                var cDtlS3 = Sm.GetOrdinal(drDtlS3, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-2
                         "PosName",
                         "DeptName"
                        });
                if (drDtlS3.HasRows)
                {
                    while (drDtlS3.Read())
                    {

                        lDtlS3.Add(new BOQSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS3, cDtlS3[0]),
                            PosName = Sm.DrStr(drDtlS3, cDtlS3[1]),
                            DeptName = Sm.DrStr(drDtlS3, cDtlS3[2])
                        });
                    }
                }
                drDtlS3.Close();
            }
            myLists.Add(lDtlS3);


            #endregion

            if(Grd2.Rows.Count > 1)
                Sm.PrintReport("BOQJasa", myLists, TableName, false);
            if (Grd3.Rows.Count > 1)
                Sm.PrintReport("BOQBarang", myLists, TableName, false);
        }

        internal string GetSelectedBOMData()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + Sm.GetGrdStr(Grd2, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOMData2()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + Sm.GetGrdStr(Grd3, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ComputeMaterialPercentage(int Row, int Col)
        {
            //Grd2 kolom 9 & 17(%)
            if (Col == 9 || Col == 17)
            {
                decimal mMinimalServiceAmt = Sm.GetGrdDec(Grd2, Row, 8);
                decimal mMaterialAmt = Sm.GetGrdDec(Grd2, Row, 9);
                decimal mMaterialPercentage = Sm.GetGrdDec(Grd2, Row, 17);
                if (Col == 9)
                {
                    if (mMinimalServiceAmt == 0m)
                        Grd2.Cells[Row, 17].Value = 0m;
                    else
                        Grd2.Cells[Row, 17].Value = Math.Round(((mMaterialAmt / mMinimalServiceAmt) * 100m), 2);
                }
                if (Col == 17)
                {
                    Grd2.Cells[Row, 9].Value = Math.Round((mMaterialPercentage / 100m), 2) * mMinimalServiceAmt;
                }
            }
        }


        private void ComputeTotalDtl(int Row)
        {
            // Jasa --> Total = Minimal Service + Material
            decimal mTotal = 0m;
            mTotal = Sm.GetGrdDec(Grd2, Row, 8) + Sm.GetGrdDec(Grd2, Row, 9);
            Grd2.Cells[Row, 10].Value = Math.Ceiling(mTotal);
        }

        private void ComputeAfterSalesAmt(int Row, int Col)
        {

            //decimal Amt = 0m, AfterSalesPercentage = 0m;
            //if (Sm.GetGrdStr(Grd2, Row, 10).Length > 0) Amt = Sm.GetGrdDec(Grd2, Row, 10);
            //if (Sm.GetGrdStr(Grd2, Row, 18).Length > 0) AfterSalesPercentage = Sm.GetGrdDec(Grd2, Row, 18);

            //Grd2.Cells[Row, 11].Value = Amt * AfterSalesPercentage*0.01m;
            
        }

        private void ComputeAfterSalesPercentage(int Row, int Col)
        {
            //Grd2 kolom 11 & 18(%)
            if (Col == 11 || Col == 18)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, Row, 10);
                decimal mAfterSalesAmt = Sm.GetGrdDec(Grd2, Row, 11);
                decimal mAfterSalesPercentage = Sm.GetGrdDec(Grd2, Row, 18);
                if (Col == 11)
                {
                    if (Amt==0m)
                        Grd2.Cells[Row, 18].Value = 0m;
                    else
                        Grd2.Cells[Row, 18].Value = Math.Round(((mAfterSalesAmt / Amt) * 100m), 2);
                }
                if (Col == 18) Grd2.Cells[Row, 11].Value = Math.Round((mAfterSalesPercentage / 100m), 2) * Amt;
            }
        }

        private void ComputeTotalPPHDtl(int Row)
        {
            // Jasa --> Total (+PPH) = (Total + After Sales) * BOQ2PPHPercentage

            // new 25/02/2021 by wed
            // Jasa --> Total (+PPH) = ((Total + After Sales) * BOQ2PPHPercentage) + Tax
            decimal mTotal = 0m;
            decimal TaxPercentageServices = decimal.Parse(TxtTaxPercentageServices.Text);
            //mTotal = (Sm.GetGrdDec(Grd2, Row, 10) + Sm.GetGrdDec(Grd2, Row, 11)) * TaxPercentageServices; 

            // 26/02/2021 by ibl
            // kalau kolom tax pd dtl > 0 maka tax percentage ambil dari kolom tax, jika tidak maka tax percentage ambil dari TxtTaxPercentageService
            if (Sm.GetGrdDec(Grd2, Row, 25) != 0)
                mTotal = ((Sm.GetGrdDec(Grd2, Row, 10) + Sm.GetGrdDec(Grd2, Row, 11)) * Sm.GetGrdDec(Grd2, Row, 25));
            else
                mTotal = ((Sm.GetGrdDec(Grd2, Row, 10) + Sm.GetGrdDec(Grd2, Row, 11)) * TaxPercentageServices);

            Grd2.Cells[Row, 12].Value = Sm.FormatNum(mTotal, 0);
        }

        private void ComputeSpaceNegoAmt(int Row, int Col)
        {
            //decimal SpaceNegoPercentage = 0m, TotalPPHAmt = 0m;
            //if (Sm.GetGrdStr(Grd2, Row, 19).Length > 0) SpaceNegoPercentage = Sm.GetGrdDec(Grd2, Row, 19);
            //if (Sm.GetGrdStr(Grd2, Row, 12).Length > 0) TotalPPHAmt = Sm.GetGrdDec(Grd2, Row, 12);
            //Grd2.Cells[Row, 13].Value = TotalPPHAmt*SpaceNegoPercentage * 0.01m;   
        }

        private void ComputeSpaceNegoPercentage(int Row, int Col)
        {
            //Grd2 kolom 13 & 19(%)
            if (Col == 13 || Col == 19)
            {
                decimal TotalPPHAmt = Sm.GetGrdDec(Grd2, Row, 12);
                decimal mSpaceNegoAmt = Sm.GetGrdDec(Grd2, Row, 13);
                decimal mSpaceNegoPercentage = Sm.GetGrdDec(Grd2, Row, 19);
                if (Col == 13) Grd2.Cells[Row, 19].Value = Math.Round(((mSpaceNegoAmt / TotalPPHAmt) * 100m), 2);
                if (Col == 19) Grd2.Cells[Row, 13].Value = Math.Round((mSpaceNegoPercentage / 100m), 2) * TotalPPHAmt;
            }
        }

        private void ComputeAfterSalesPercentage2(int Row, int Col)
        {
            //Grd3 kolom 9 & 19(%)
            if (Col == 9 || Col == 19)
            {
                decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                decimal mAfterSalesAmt = Sm.GetGrdDec(Grd3, Row, 9);
                decimal mAfterSalesPercentage = Sm.GetGrdDec(Grd3, Row, 19);
                if (Col == 9) Grd3.Cells[Row, 19].Value = Math.Round(((mAfterSalesAmt / mCostOfGoodsAmt) * 100m), 2);
                if (Col == 19) Grd3.Cells[Row, 9].Value = Math.Round((mAfterSalesPercentage / 100m), 2) * mCostOfGoodsAmt;
            }
        }

        private void ComputeCOMPercentage(int Row, int Col) 
        {
            //Grd3 kolom 10 & 20(%)
            if (Col == 10 || Col == 20)
            {
                decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                decimal mCOMAmt = Sm.GetGrdDec(Grd3, Row, 10);
                decimal mCOMPercentage = Sm.GetGrdDec(Grd3, Row, 20);
                if (Col == 10) Grd3.Cells[Row, 20].Value = Math.Round(((mCOMAmt / mCostOfGoodsAmt) * 100m), 2);
                if (Col == 20) Grd3.Cells[Row, 10].Value = Math.Round((mCOMPercentage / 100m), 2) * mCostOfGoodsAmt;
            }
        }

        private void ComputeOHPercentage(int Row, int Col)
        {
            //Grd3 kolom 11 & 21(%)
            if (Col == 11 || Col == 21)
            {
                decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                decimal mOHAmt = Sm.GetGrdDec(Grd3, Row, 11);
                decimal mOHPercentage = Sm.GetGrdDec(Grd3, Row, 21);
                if (Col == 11) Grd3.Cells[Row, 21].Value = Math.Round(((mOHAmt / mCostOfGoodsAmt) * 100m), 2);
                if (Col == 21) Grd3.Cells[Row, 11].Value = Math.Round((mOHPercentage / 100m), 2) * mCostOfGoodsAmt;
            }
        }

        private void ComputeMarginPercentage(int Row, int Col)
        {
            //Grd3 kolom 12 & 22(%)
            if (Col == 12 || Col == 22)
            {
                decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                decimal mMarginAmt = Sm.GetGrdDec(Grd3, Row, 12);
                decimal mMarginPercentage = Sm.GetGrdDec(Grd3, Row, 22);
                if (Col == 12) Grd3.Cells[Row, 22].Value = Math.Round(((mMarginAmt / mCostOfGoodsAmt) * 100m), 2);
                if (Col == 22) Grd3.Cells[Row, 12].Value = Math.Round((mMarginPercentage / 100m), 2) * mCostOfGoodsAmt;
            }
        }

        private void ComputeDesignCostPercentage(int Row, int Col)
        {
            //Grd3 kolom 13 & 23(%)
            if (Col == 13 || Col == 23)
            {
                decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                decimal mDesignCost = Sm.GetGrdDec(Grd3, Row, 13);
                decimal mDesignCostPercentage = Sm.GetGrdDec(Grd3, Row, 23);
                if (Col == 13) Grd3.Cells[Row, 23].Value = Math.Round(((mDesignCost / mCostOfGoodsAmt) * 100m), 2);
                if (Col == 23) Grd3.Cells[Row, 13].Value = Math.Round((mDesignCostPercentage / 100m), 2) * mCostOfGoodsAmt;
            }
        }

        private void ComputeSpaceNegoPercentage2(int Row, int Col)
        {
            //Grd3 kolom 15 & 24(%)
            if (Col == 15 || Col == 24)
            {
                decimal UPricePPHAmt = Sm.GetGrdDec(Grd3, Row, 14);
                decimal mSpaceNegoAmt = Sm.GetGrdDec(Grd3, Row, 15);
                decimal mSpaceNegoPercentage = Sm.GetGrdDec(Grd3, Row, 24);
                //decimal mCostOfGoodsAmt = Sm.GetGrdDec(Grd3, Row, 8);
                if (Col == 15)
                {
                    if (UPricePPHAmt == 0m)
                        Grd3.Cells[Row, 24].Value = 0m;
                    else
                        Grd3.Cells[Row, 24].Value = Math.Round(((mSpaceNegoAmt / UPricePPHAmt) * 100m), 2);
                }
                else
                {
                    if (Col == 24) Grd3.Cells[Row, 15].Value = Math.Round((mSpaceNegoPercentage / 100m), 2) * UPricePPHAmt;
                }
            }
        }

        private void ComputeSPHDtl(int Row)
        {
            // Jasa --> SPH = Total (+PPH) + Space Nego
            decimal mTotal = 0m;
            mTotal = Sm.GetGrdDec(Grd2, Row, 12) + Sm.GetGrdDec(Grd2, Row, 13);
            Grd2.Cells[Row, 14].Value = (Math.Ceiling(mTotal * 0.01m)) * 100m;
        }

        private void ComputeTotalPriceDtl(int Row)
        {
            // Jasa --> Total Price = SPH * Qty
            decimal mTotal = 0m;
            mTotal = Sm.GetGrdDec(Grd2, Row, 14) * Sm.GetGrdDec(Grd2, Row, 6);
            Grd2.Cells[Row, 15].Value = (Math.Ceiling(mTotal * 0.01m)) * 100m;
        }

        private void ComputeUPricePPHDtl2(int Row)
        {
            // Barang --> UPrice (+PPH) = (Cost of Goods + After Sales + COM + OH + Margin + Design Cost) * mBOQ2PPHPercentageInventory
            decimal mTotal = 0m;
            decimal TaxPercentageInventory = decimal.Parse(TxtTaxPercentageInventory.Text);
            mTotal = (Sm.GetGrdDec(Grd3, Row, 8) + Sm.GetGrdDec(Grd3, Row, 9) + Sm.GetGrdDec(Grd3, Row, 10) +
                Sm.GetGrdDec(Grd3, Row, 11) + Sm.GetGrdDec(Grd3, Row, 12) + Sm.GetGrdDec(Grd3, Row, 13)) * TaxPercentageInventory;
            Grd3.Cells[Row, 14].Value = Math.Ceiling(mTotal);
        }

        private void ComputeUPriceSPHDtl2(int Row)
        {
            //Barang --> Unit Price (SPH) = UPrice (+PPH) + Space Nego
            decimal mTotal = 0m;
            mTotal = Sm.GetGrdDec(Grd3, Row, 14) + Sm.GetGrdDec(Grd3, Row, 15);
            Grd3.Cells[Row, 16].Value = (Math.Ceiling(mTotal * 0.01m)) * 100m;
        }

        private void ComputeTotalPriceSPHDtl2(int Row)
        {
            //Barang --> Total Price (SPH) = Unit Price (SPH) * Quantity
            decimal mTotal = 0m;
            mTotal = Sm.GetGrdDec(Grd3, Row, 16) * Sm.GetGrdDec(Grd3, Row, 6);
            Grd3.Cells[Row, 17].Value = (Math.Ceiling(mTotal * 0.01m)) * 100m;
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mBOQTaxPercentage = Sm.GetParameterDec("BOQTaxPercentage");
            mBOQPrintOutBODName = Sm.GetParameter("BOQPrintOutBODName");
            mRptName = Sm.GetParameter("FormPrintBOQ");
            mIsBOQPrintOutShowCreatedUser = Sm.GetParameterBoo("IsBOQPrintOutShowCreatedUser");
            mBOQ2PPHPercentage = Sm.GetParameterDec("BOQ2PPHPercentage");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsCreditLimitMandatory = Sm.GetParameterBoo("IsCreditLimitMandatory");
            mBOQAutoApprovedByEmpCode = Sm.GetParameter("BOQAutoApprovedByEmpCode");
            mBOQVerifiedByEmpCode = Sm.GetParameter("BOQVerifiedByEmpCode");
            mBOQ2PPHPercentageInventory = Sm.GetParameterDec("BOQ2PPHPercentageInventory");
            mIsRemarkHdrEditableOvertime = Sm.GetParameterBoo("IsRemarkHdrEditableOvertime");
            mPPNDefaultSetting = Sm.GetParameter("PPNDefaultSetting");
        }

        private void ComputeSubTotal()
        {
            decimal mSubtotal = 0m;

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 3).Length > 0)
                    {
                        mSubtotal += Sm.GetGrdDec(Grd2, i, 15);
                    }
                }
            }

            TxtSubTotal.EditValue = Sm.FormatNum(mSubtotal, 0);
            ComputeTax();
        }

        private void ComputeTotalBOM()
        {
            decimal mSubtotal = 0m;

            if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd3, i, 3).Length > 0)
                    {
                        mSubtotal += Sm.GetGrdDec(Grd3, i, 17);
                    }
                }
            }

            TxtTotalBOM.EditValue = Sm.FormatNum(mSubtotal, 0);
            ComputeTax();
        }

        private void ComputeTax()
        {
            if (BtnSave.Enabled)
            {
                decimal mTax = 0m, mSubtotal = 0m, tax = 0m;
                mSubtotal = Decimal.Parse(TxtSubTotal.Text) + Decimal.Parse(TxtTotalBOM.Text);

                var taxcode = Sm.GetLue(LuePPN);
              
                if (ChkTaxInd.Checked)
                {
                    tax = taxcode.Length > 0 ? Decimal.Parse(Sm.GetValue("select taxrate from tbltax where taxcode = @param ", taxcode)) : 0m;
                    mTax = (tax * 0.01m) * mSubtotal;
                }

                TxtTax.EditValue = Sm.FormatNum(mTax, 0);
                ComputeGrandTotal();
            }
        }

        private void ComputeGrandTotal()
        {
            decimal mGrandTotal = 0m, mSubtotal = 0m, mTax = 0m;
            mSubtotal = Decimal.Parse(TxtSubTotal.Text) + Decimal.Parse(TxtTotalBOM.Text);
            mTax = Decimal.Parse(TxtTax.Text);
            mGrandTotal = mSubtotal + mTax;
            TxtGrandTotal.EditValue = Sm.FormatNum(mGrandTotal, 0);
        }

        private void ComputePercentage()
        {
            decimal mGrandTotal = 0m, mCreditLimit = 0m, mPercentage = 0m;
            mGrandTotal = decimal.Parse(TxtGrandTotal.Text);
            mCreditLimit = decimal.Parse(TxtCreditLimit.Text);
            if (mCreditLimit > 0)
            {
                mPercentage = (mGrandTotal / mCreditLimit) * 100 ;
            }
            else
                mPercentage = 0;
            TxtPercentage.EditValue = Sm.FormatNum(mPercentage, 0);
        }

        private void ComputeSubTotalRBPS()
        {
            decimal mSubTotalRBPS = 0m, mDirectCost = 0m, mPolicy = 0m, mIndirectCost = 0m;
            mDirectCost = Decimal.Parse(TxtDirectCost.Text);
            mIndirectCost = Decimal.Parse(TxtIndirectCost.Text);
            mPolicy = Decimal.Parse(TxtPolicy.Text);
            mSubTotalRBPS = mDirectCost + mIndirectCost + mPolicy;
            TxtSubTotalRBPS.EditValue = Sm.FormatNum(mSubTotalRBPS, 0);
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'BOQ' ");
            SQL.AppendLine("And SiteCode Is Not Null ");
            SQL.AppendLine("And SiteCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.SiteCode ");
            SQL.AppendLine("    From TblLOPHdr T ");
            SQL.AppendLine("    Where T.DocNo = @Param ");
            SQL.AppendLine("); ");

            return Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text);
        }

        private bool IsUserAbleToInsert()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblGroupMenu A, TblMenu B, TblUser C ");
            SQL.AppendLine("Where A.MenuCode=B.MenuCode ");
            SQL.AppendLine("And Left(A.AccessInd, 1)='Y' ");
            SQL.AppendLine("And B.Param='FrmBOQ' ");
            SQL.AppendLine("And A.GrpCode=C.GrpCode ");
            SQL.AppendLine("And C.UserCode=@Param;");

            return (Sm.IsDataExist(SQL.ToString(), Gv.CurrentUserCode));
        }

        private bool IsUserUnAbleToEditBOQDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TbLBOQHdr A  ");
            SQL.AppendLine("Inner Join TblSOContractHdr B ON A.DocNo = B.BOQDocNo  ");
            SQL.AppendLine("    And A.ActInd = 'Y' ");
            SQL.AppendLine("    And (A.Status = 'A' OR A.Status = 'O') ");
            SQL.AppendLine("Inner Join TblDRDtl C ON B.DocNo = C.SODocNo ");
            SQL.AppendLine("Inner Join TblDRHdr D ON C.DocNo = D.DocNo And D.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo = @Param ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text);
        }

        private void BtnInsertClick()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
                TxtTaxPercentageServices.EditValue = Sm.FormatNum(mBOQ2PPHPercentage, 0);
                TxtTaxPercentageInventory.EditValue = Sm.FormatNum(mBOQ2PPHPercentageInventory, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueSPCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode, string ContactPersonName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode=@CtCode ");
            if (ContactPersonName.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @ContactPersonName As Col1 ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand();
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactPersonName);

            Sm.SetLue1(ref Lue, ref cm, "Contact Person");
            if (ContactPersonName.Length > 0) Sm.SetLue(Lue, ContactPersonName);

        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateBOQFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if ( TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if ( TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if ( TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }
        
        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmBOQ2Dlg2(this));
            }
        }

        private void BtnCtContactPersonName_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                }
            }
        }

        private void BtnCtReplace_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer") &&
                !Sm.IsLueEmpty(LueCurCode, "Currency"))
                Sm.FormShowDialog(new FrmBOQ2Dlg(this, Sm.GetLue(LueCurCode)));
        }

        private void BtnCtReplace2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                if (TxtCtReplace.EditValue != null && TxtCtReplace.Text.Length != 0)
                {
                    var f = new FrmBOQ2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtCtReplace.Text;
                    f.ShowDialog();
                }                
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Misc Control Event

        private void TxtSPHNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSPHNo);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCtContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueCtContactPersonName, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueCtContactPersonName, false);
                }
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtCtReplace  
                });
                
                ClearGrd();
            }
        }

        private void LueCtContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue3(SetLueCtPersonCode), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LuePPN_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePPN, new Sm.RefreshLue2(Sl.SetLueOption), "PPNForProject");
                var test = Sm.GetLue(LuePPN);
                if (Sm.GetLue(LuePPN) == "" && isinsert)
                    Sm.SetLue(LuePPN, mPPNDefaultSetting);
                if (ChkTaxInd.Checked)
                {
                    ComputeTax();
                }
            }
        }

        private void LueRingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueAgingAP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueAgingAP, new Sm.RefreshLue1(Sl.SetLueAgingAP));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                TxtCtReplace.EditValue = null;
                ClearGrd();
            }
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCreditLimit, 0);
                ComputePercentage();
            }
        }


        private void TxtDirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtIndirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtIndirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtPolicy_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtPolicy, 0);
                ComputeSubTotalRBPS();
            }
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkTaxInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeTax();
                if (ChkTaxInd.Checked)
                {
                    LuePPN.Properties.ReadOnly = false;
                    LuePPN.BackColor = Color.FromArgb(255, 255, 255);
                }
                else
                {
                    LuePPN.Properties.ReadOnly = true;
                    LuePPN.BackColor = Color.FromArgb(195, 195, 195);
                }
            }
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
        }

        private void TxtTaxPercentageServices_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtTaxPercentageServices, 8);
                if (TxtDocNo.Text.Length == 0) Sm.ClearGrd(Grd2, true);
            }
        }

        private void TxtTaxPercentageInventory_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtTaxPercentageInventory, 8);
                if (TxtDocNo.Text.Length == 0) Sm.ClearGrd(Grd3, true);
            }
        }


        #endregion

        #endregion

        #region Class

        private class BOQ
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }

            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ProjectName { get; set; }

            public string ProjectCode { get; set; }
            public string PtName { get; set; }
            public string SPHNo { get; set; }
            public string TermOfPayment { get; set; }
            public string CUrrency { get; set; }

            public string Remark { get; set; }

        }

        private class BOQDtlInventory
        {
            public string No { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUOM { get; set; }
            public decimal CostofGoodsAmt { get; set; }
            public decimal AfterSalesPercentage { get; set; }
            public decimal COMPercentage { get; set; }
            public decimal OHPercentage { get; set; }
            public decimal MarginPercentage { get; set; }
            public decimal DesignCostAmt { get; set; }
            public decimal UPricePPHAmt { get; set; }
            public decimal SpaceNegoAmt { get; set; }
            public decimal UPriceSPHAmt { get; set; }
            public decimal TotalPriceSPHAmt { get; set; }
            public string Remark { get; set; }
            public string Spesification { get; set; }
            public string UOM { get; set; }
            public decimal COGS { get; set; }
            public decimal COMAmt { get; set; }
            public decimal OHAmt { get; set; }
            public decimal MarginAmt { get; set; }
            public decimal AfterSalesAmt { get; set; }


        }

        private class BOQDtlService
        {
            public decimal No { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUOM { get; set; }
            public decimal MinimalServiceAmt { get; set; }
            public decimal MaterialAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal AfterSalesPercentage { get; set; }
            public decimal TotalPPHAmt { get; set; }
            public decimal SpaceNegoAmt { get; set; }
            public decimal UPriceSPHAmt { get; set; }
            public decimal TotalAmt { get; set; }
            public string ItNameDismantle { get; set; }
        }

        private class BOQSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string DeptName { get; set; }

        }

        private class BOQSign2
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }

        }

        private class BOQSign3
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }

        }

        #endregion
    }
}
