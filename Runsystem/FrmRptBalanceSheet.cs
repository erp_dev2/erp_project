﻿#region Update
/* 
    06/07/2017 [WED] tambah filter Entity, berdasarkan parameter IsEntityMandatory
    18/10/2017 [TKG] 
        berdasarkan parameter IsCOAAssetUseStartYr, COAAssetStartYr, COAAssetAcNo, 
        memproses nomor rekening asset dari tahun di parameter COAAssetStartYr.
    20/10/2017 [WED] perubahan perhitungan balance
    04/12/2017 [TKG] Ubah proses data reporting disesuaikan untuk data dengan nomor rekening2 yang tidak rapi.
    12/05/2018 [TKG] hilangkan perhitungan laba rugi berjalan
    28/05/2018 [TKG] bug fixing karena status aktif atau tidak nomor coa
    06/06/2018 [TKG] draft balance sheet
    07/06/2018 [TKG] update draft balance sheet
    19/07/2018 [TKG] logic perhitungan nomor rekening parent diubah untuk mengakomodasi apabila anak nomor rekening anak yg panjangnya tidak sama.
    15/01/2019 [DITA] Balance Sheet hanya menampilkan akun nomor 1, 2, dan 3
    15/10/2019 [WED/IMS] filter COA Alias
    15/12/2019 [TKG/TWC] merubah perhitungan laba rugi berjalan (custom)
    12/02/2020 [TKG/GSS] menggunakan parameter MaxAccountCategory untuk menghitung laba rugoi berjalan dan ihtisar laba rugi
    24/02/2020 [TKG/KBN] proses baru untuk CurrentEarningFormulaType (3)
    28/04/2020 [WED/WOM] Journal ambil dari Period (bukan DocDt) berdasarkan parameter IsAccountingRptUseJournalPeriod
    02/11/2020 [WED/PHT] tambah filter level from dan to, berdasarkan parameter IsFicoUseCOALevelFilter
    08/11/2020 [TKG/PHT] Tambah filter multi entity berdasarkan parameter IsFicoUseMultiEntityFilter
    13/01/2021 [IBL/IMS] Tambah filter Date berdasarkan parameter IsRptBalanceSheetUseEndDt
    18/01/2021 [VIN/PHT] Tambah filter multi profit center berdasarkan parameter IsFicoUseMultiProfitCenterFilter
    25/01/2020 [TKG/PHT] ubah SetCcbEntCode divalidasi berdasarkan cost center group user
    02/02/2020 [TKG/PHT] ubah query menggunakan filter profit center
    03/02/2020 [TKG/PHT] apabila profit center tidak di-tick (consolidate), cost center yg kosong tetap ditampilkan.
    05/02/2020 [TKG/PHT] Combo box profit center diurutkan
    16/03/2021 [TKG/PHT] profit center divalidasi berdasarkan parent juga.
    16/03/2021 [TKG/PHT] divalidasi berdasarkan group profit center.
    26/03/2021 [ICA/IMS] bug saat mAcNoForCurrentEarning kosong
    12/04/2021 [TKG/PHT] filter multi profit center diurutkan berdasarkan kodenya.
    04/07/2021 [TKG/PHT] tambahan filter period
    06/07/2021 [TKG/PHT] filter period hanya berpengaruh ke current month
    15/09/2021 [BRI/ALL] start from disable berdasarkan param IsRptFinancialNotUseStartFromFilter
    21/09/2021 [TKG/ALL] Berdasarkan parameter IsRptAccountingShowJournalMemorial, menampilkan outstanding journal memorial
    22/09/2021 [TKG/ALL] param IsRptFinancialNotUseStartFromFilter, start from harus dikosongkan isinya kalau tidak maka validasinya tetap tdk jalan.
    01/10/2021 [RDA/AMKA] bug filter cost center belum bisa digunakan
    15/02/2022 [TKG/PHT] merubah GetParameter()
    23/05/2022 [DITA/TWC] bug ketika IsAccountingRptUseJournalPeriod = Y data yang diambil belum berdasar period yang dipilih
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBalanceSheet : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mBalanceSheetDraft = "0",
            mAcNoForCurrentEarning2 = "9",
            mAcNoForActiva = "1",
            mAcNoForPassiva = "2",
            mAcNoForCapital = "3",
            mCurrentEarningFormulaType = "1",
            mMaxAccountCategory = "9";

        private bool
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsRptBalanceSheetCurrentEarningUseProfitLoss = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsFicoUseCOALevelFilter = false,
            mIsReportingFilterByEntity = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsRptBalanceSheetUseEndDt = false,
            //mIsFicoUseMultiProfitCenterFilter = false
            mIsRptBalanceSheetUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptFinancialNotUseStartFromFilter = false,
            mIsRptAccountingShowJournalMemorial = false;
            
        #endregion

        #region Constructor

        public FrmRptBalanceSheet(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueDt(LueDt);
                Sm.SetLue(LueDt, CurrentDateTime.Substring(6, 2));
                Sl.SetLuePeriod(LuePeriod);
                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                    }
                }
                if (!mIsFicoUseCOALevelFilter)
                {
                    LblLevel.Visible = LblLevel2.Visible = LueLevelFrom.Visible = LueLevelTo.Visible = false;
                }
                else
                {
                    SetLueLevelFromTo(ref LueLevelFrom);
                    SetLueLevelFromTo(ref LueLevelTo);
                }

                if (mIsReportingFilterByEntity) SetLueEntCode(ref LueEntCode);
                if (mIsRptBalanceSheetUseProfitCenter)    
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                if (mIsAccountingRptUseJournalPeriod) LblPeriod.ForeColor = Color.Red;
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            //mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptBalanceSheetUseEndDt', 'IsEntityMandatory', 'IsCOAAssetUseStartYr', 'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', ");
            SQL.AppendLine("'IsFicoUseCOALevelFilter', 'IsFicoUseMultiEntityFilter', 'IsReportingFilterByEntity', 'IsAccountingRptUseJournalPeriod', 'IsRptBalanceSheetUseProfitCenter', ");
            SQL.AppendLine("'IsRptBalanceSheetCurrentEarningUseProfitLoss', 'CurrentEarningFormulaType', 'AcNoForCost', 'AcNoForIncome', 'MaxAccountCategory', ");
            SQL.AppendLine("'AccountingRptStartFrom', 'AcNoForCurrentEarning', 'COAAssetStartYr', 'COAAssetAcNo', 'BalanceSheetDraft', ");
            SQL.AppendLine("'AcNoForCurrentEarning2', 'AcNoForActiva', 'AcNoForPassiva', 'AcNoForCapital' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            case "AcNoForCapital": mAcNoForCapital = ParValue; break;
                            case "AcNoForPassiva" : mAcNoForPassiva = ParValue; break;
                            case "AcNoForActiva": mAcNoForActiva = ParValue; break;
                            case "AcNoForCurrentEarning2": mAcNoForCurrentEarning2 = ParValue; break;
                            case "BalanceSheetDraft": mBalanceSheetDraft = ParValue; break;
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "CurrentEarningFormulaType": mCurrentEarningFormulaType = ParValue; break;
                            case "MaxAccountCategory": mMaxAccountCategory = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;

                            //boolean
                            case "IsRptBalanceSheetUseProfitCenter": mIsRptBalanceSheetUseProfitCenter = ParValue == "Y"; break;
                            case "IsAccountingRptUseJournalPeriod": mIsAccountingRptUseJournalPeriod = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsFicoUseMultiEntityFilter": mIsFicoUseMultiEntityFilter = ParValue == "Y"; break;
                            case "IsFicoUseCOALevelFilter": mIsFicoUseCOALevelFilter = ParValue == "Y"; break;
                            case "IsRptBalanceSheetCurrentEarningUseProfitLoss": mIsRptBalanceSheetCurrentEarningUseProfitLoss = ParValue == "Y"; break;
                            case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsRptBalanceSheetUseEndDt": mIsRptBalanceSheetUseEndDt = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Alias";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "Opening Balance";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 2;
            Grd1.Header.Cells[0, 3].Value = "Debit";
            Grd1.Header.Cells[0, 4].Value = "Credit";
            Grd1.Cols[3].Width = 130;
            Grd1.Cols[4].Width = 130;

            Grd1.Header.Cells[1, 5].Value = "Month To Date";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 2;
            Grd1.Header.Cells[0, 5].Value = "Debit";
            Grd1.Header.Cells[0, 6].Value = "Credit";
            Grd1.Cols[5].Width = 130;
            Grd1.Cols[6].Width = 130;

            Grd1.Header.Cells[1, 7].Value = "Current Month";
            Grd1.Header.Cells[1, 7].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 7].SpanCols = 2;
            Grd1.Header.Cells[0, 7].Value = "Debit";
            Grd1.Header.Cells[0, 8].Value = "Credit";
            Grd1.Cols[7].Width = 130;
            Grd1.Cols[8].Width = 130;

            Grd1.Header.Cells[1, 9].Value = "Year To Date";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 2;
            Grd1.Header.Cells[0, 9].Value = "Debit";
            Grd1.Header.Cells[0, 10].Value = "Credit";
            Grd1.Cols[9].Width = 130;
            Grd1.Cols[10].Width = 130;

            Grd1.Header.Cells[0, 11].Value = "Balance";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Cols[11].Width = 150;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 });
            if (mBalanceSheetDraft=="1") Sm.GrdColInvisible(Grd1, new int[] { 11 });
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month") ||
                (mIsRptBalanceSheetUseEndDt && Sm.IsLueEmpty(LueDt, "Date")) ||
                (mIsAccountingRptUseJournalPeriod && Sm.IsLueEmpty(LuePeriod, "Period")) ||
                //(mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                //IsEntityInvalid() ||
                IsLevelFromToInvalid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var Yr = Sm.GetLue(LueYr);
            var Mth = (mIsAccountingRptUseJournalPeriod) ? Sm.GetLue(LuePeriod) : Sm.GetLue(LueMth);
            var Dt = Sm.GetLue(LueDt);
            var StartFrom = Sm.GetLue(LueStartFrom);
            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var IsFilterByEntity = ChkEntCode.Checked;

                SetProfitCenter();

                Process1(ref lCOA);
                //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                
                //if ((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate") || CcbEntCode.Text.Trim().Length > 0 && !GetCcbCode().Contains("Consolidate"))
                if (IsFilterByEntity) Process8(ref lEntityCOA);
                
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, Dt, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, Dt, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, Dt, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, Dt, string.Empty);
                    }
                    if (mIsRptAccountingShowJournalMemorial)
                        Process4_JournalMemorial(ref lCOA, Yr, Mth, Dt);
                    else
                        Process4(ref lCOA, Yr, Mth, Dt);
                    Process5(ref lCOA);
                    Process6(ref lCOA);
                    Process7(ref lCOA);
                    Process9(ref lCOA);

                    #region Filtered By Entity

                    //if (mIsEntityMandatory &&
                    //((Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate") ||
                    // CcbEntCode.Text.Trim().Length > 0 && !GetCcbCode().Contains("Consolidate")))
                    //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    if (IsFilterByEntity)
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lCOA.Count > 0)
                            {
                                Grd1.BeginUpdate();
                                Grd1.Rows.Count = 0;

                                iGRow r;

                                r = Grd1.Rows.Add();
                                r.Level = 0;
                                r.TreeButton = iGTreeButtonState.Visible;
                                r.Cells[0].Value = "COA";
                                for (var j = 0; j < lEntityCOA.Count; j++)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (Process10(lCOA[i]) == true)
                                        {
                                            if ((Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5") && lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                            {
                                                if (mIsFicoUseCOALevelFilter && Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                                {
                                                    if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                                    {
                                                        r = Grd1.Rows.Add();
                                                        r.Level = lCOA[i].Level;
                                                        r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                        r.Cells[0].Value = lCOA[i].AcNo;
                                                        r.Cells[1].Value = lCOA[i].Alias;
                                                        r.Cells[2].Value = lCOA[i].AcDesc;
                                                        for (var c = 3; c < 11; c++)
                                                            r.Cells[c].Value = 0m;
                                                        r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                        r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                        r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                        r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                        r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                        r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                        if (mBalanceSheetDraft == "1")
                                                        {
                                                            if (lCOA[i].AcType == "D")
                                                            {
                                                                r.Cells[9].Value = lCOA[i].Balance;
                                                                r.Cells[10].Value = 0m;
                                                            }
                                                            else
                                                            {
                                                                r.Cells[9].Value = 0m;
                                                                r.Cells[10].Value = lCOA[i].Balance;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                            r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                        }
                                                        r.Cells[11].Value = lCOA[i].Balance;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    r = Grd1.Rows.Add();
                                                    r.Level = lCOA[i].Level;
                                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                    r.Cells[0].Value = lCOA[i].AcNo;
                                                    r.Cells[1].Value = lCOA[i].Alias;
                                                    r.Cells[2].Value = lCOA[i].AcDesc;
                                                    for (var c = 3; c < 11; c++)
                                                        r.Cells[c].Value = 0m;
                                                    r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                    r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                    r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                    r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                    r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                    r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                    if (mBalanceSheetDraft == "1")
                                                    {
                                                        if (lCOA[i].AcType == "D")
                                                        {
                                                            r.Cells[9].Value = lCOA[i].Balance;
                                                            r.Cells[10].Value = 0m;
                                                        }
                                                        else
                                                        {
                                                            r.Cells[9].Value = 0m;
                                                            r.Cells[10].Value = lCOA[i].Balance;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                        r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                    }
                                                    r.Cells[11].Value = lCOA[i].Balance;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                Grd1.TreeLines.Visible = true;
                                Grd1.Rows.CollapseAll();

                                decimal
                                    OpeningBalanceDAmt = 0m,
                                    OpeningBalanceCAmt = 0m,
                                    MonthToDateDAmt = 0m,
                                    MonthToDateCAmt = 0m,
                                    CurrentMonthDAmt = 0m,
                                    CurrentMonthCAmt = 0m,
                                    YearToDateDAmt = 0m,
                                    YearToDateCAmt = 0m,
                                    Balance = 0m;

                                for (var i = 0; i < lCOA.Count; i++)
                                {
                                    if (Process10(lCOA[i]) == true)
                                    {
                                    if (lCOA[i].Parent.Length == 0 && Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5")
                                    {
                                        OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                        OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                        MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                        MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                        CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                        CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                        if (mBalanceSheetDraft == "1")
                                        {
                                            if (lCOA[i].AcType == "D")
                                                YearToDateDAmt += lCOA[i].Balance;
                                            else
                                                YearToDateCAmt += lCOA[i].Balance;
                                        }
                                        else
                                        {
                                            YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                            YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                        }
                                        Balance += lCOA[i].Balance;
                                    }
                                }
                                }

                                r = Grd1.Rows.Add();
                                r.Cells[0].Value = "Total";
                                for (var c = 3; c < 11; c++)
                                    r.Cells[c].Value = 0m;

                                r.Cells[3].Value = OpeningBalanceDAmt;
                                r.Cells[4].Value = OpeningBalanceCAmt;
                                r.Cells[5].Value = MonthToDateDAmt;
                                r.Cells[6].Value = MonthToDateCAmt;
                                r.Cells[7].Value = CurrentMonthDAmt;
                                r.Cells[8].Value = CurrentMonthCAmt;
                                r.Cells[9].Value = YearToDateDAmt;
                                r.Cells[10].Value = YearToDateCAmt;
                                r.Cells[11].Value = Balance;
                                r.BackColor = Color.LightSalmon;

                                Grd1.EndUpdate();
                            }
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Process10(lCOA[i]) == true)
                                {
                                    if (Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5")
                                    {
                                        if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                        {
                                            if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                            {
                                                r = Grd1.Rows.Add();
                                                r.Level = lCOA[i].Level;
                                                r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                r.Cells[0].Value = lCOA[i].AcNo;
                                                r.Cells[1].Value = lCOA[i].Alias;
                                                r.Cells[2].Value = lCOA[i].AcDesc;
                                                for (var c = 3; c < 11; c++)
                                                    r.Cells[c].Value = 0m;
                                                r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                if (mBalanceSheetDraft == "1")
                                                {
                                                    if (lCOA[i].AcType == "D")
                                                    {
                                                        r.Cells[9].Value = lCOA[i].Balance;
                                                        r.Cells[10].Value = 0m;
                                                    }
                                                    else
                                                    {
                                                        r.Cells[9].Value = 0m;
                                                        r.Cells[10].Value = lCOA[i].Balance;
                                                    }
                                                }
                                                else
                                                {
                                                    r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                    r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                }
                                                r.Cells[11].Value = lCOA[i].Balance;
                                            }
                                        }
                                        else
                                        {
                                            r = Grd1.Rows.Add();
                                            r.Level = lCOA[i].Level;
                                            r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                            r.Cells[0].Value = lCOA[i].AcNo;
                                            r.Cells[1].Value = lCOA[i].Alias;
                                            r.Cells[2].Value = lCOA[i].AcDesc;
                                            for (var c = 3; c < 11; c++)
                                                r.Cells[c].Value = 0m;
                                            r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                            r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                            r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                            r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                            r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                            r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                            if (mBalanceSheetDraft == "1")
                                            {
                                                if (lCOA[i].AcType == "D")
                                                {
                                                    r.Cells[9].Value = lCOA[i].Balance;
                                                    r.Cells[10].Value = 0m;
                                                }
                                                else
                                                {
                                                    r.Cells[9].Value = 0m;
                                                    r.Cells[10].Value = lCOA[i].Balance;
                                                }
                                            }
                                            else
                                            {
                                                r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                            }
                                            r.Cells[11].Value = lCOA[i].Balance;
                                        }
                                    }
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();

                            decimal
                                OpeningBalanceDAmt = 0m,
                                OpeningBalanceCAmt = 0m,
                                MonthToDateDAmt = 0m,
                                MonthToDateCAmt = 0m,
                                CurrentMonthDAmt = 0m,
                                CurrentMonthCAmt = 0m,
                                YearToDateDAmt = 0m,
                                YearToDateCAmt = 0m,
                                Balance = 0m;

                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Process10(lCOA[i]) == true)
                                {
                                    if (lCOA[i].Parent.Length == 0 && Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5")
                                    {
                                        OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                        OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                        MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                        MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                        CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                        CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                        if (mBalanceSheetDraft == "1")
                                        {
                                            if (lCOA[i].AcType == "D")
                                                YearToDateDAmt += lCOA[i].Balance;
                                            else
                                                YearToDateCAmt += lCOA[i].Balance;
                                        }
                                        else
                                        {
                                            YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                            YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                        }
                                        Balance += lCOA[i].Balance;
                                    }
                                }
                            }

                            r = Grd1.Rows.Add();
                            r.Cells[0].Value = "Total";
                            for (var c = 3; c < 11; c++)
                                r.Cells[c].Value = 0m;

                            r.Cells[3].Value = OpeningBalanceDAmt;
                            r.Cells[4].Value = OpeningBalanceCAmt;
                            r.Cells[5].Value = MonthToDateDAmt;
                            r.Cells[6].Value = MonthToDateCAmt;
                            r.Cells[7].Value = CurrentMonthDAmt;
                            r.Cells[8].Value = CurrentMonthCAmt;
                            r.Cells[9].Value = YearToDateDAmt;
                            r.Cells[10].Value = YearToDateCAmt;
                            r.Cells[11].Value = Balance;
                            r.BackColor = Color.LightSalmon;

                            Grd1.EndUpdate();
                        }
                    }
                    #endregion

                }
                lCOA.Clear();
                lEntityCOA.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptBalanceSheetUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetLueLevelFromTo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Level Col1, Level Col2 ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Order By Level; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsLevelFromToInvalid()
        {
            if (!mIsFicoUseCOALevelFilter) return false;

            string mLevelFrom = string.Empty, mLevelTo = string.Empty;

            mLevelFrom = Sm.GetLue(LueLevelFrom);
            mLevelTo = Sm.GetLue(LueLevelTo);

            if (mLevelTo.Length > 0 || mLevelFrom.Length > 0)
            {
                if (mLevelFrom.Length > 0 && mLevelTo.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level to is empty.");
                    LueLevelTo.Focus();
                    return true;
                }

                if (mLevelFrom.Length == 0 && mLevelTo.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level from is empty.");
                    LueLevelFrom.Focus();
                    return true;
                }

                if (mLevelTo.Length > 0 && mLevelFrom.Length > 0)
                {
                    if (Int32.Parse(mLevelFrom) > Int32.Parse(mLevelTo))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Level from should be smaller than level to.");
                        LueLevelFrom.Focus();
                        return true;
                    }
                }
            }

            return false;
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            //SQL.AppendLine("Union All ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode As Col1, B.EntName As Col2 ");
                SQL.AppendLine("    From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.AcNo, A.Alias, A.AcDesc, A.Parent, A.AcType ");
            SQL.AppendLine("From TblCOA A ");
            if (ChkEntCode.Checked) SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
            SQL.AppendLine("Where A.ActInd='Y'  ");
            SQL.AppendLine("Order By A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Alias", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Alias = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[3]).Length==0?1:-1,
                            AcType = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not in (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            //if (mIsRptBalanceSheetUseProfitCenter)
            //{
            //    SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
            //    if (ChkProfitCenterCode.Checked)
            //        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
            //    else
            //    {
            //        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
            //        SQL.AppendLine("        Select ProfitCenterCode ");
            //        SQL.AppendLine("        From TblCostCenter ");
            //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
            //        SQL.AppendLine("        And ActInd='Y' ");
            //        SQL.AppendLine("        And CCCode In ( ");
            //        SQL.AppendLine("            Select Distinct CCCode From TblGroupCostCenter T ");
            //        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //        SQL.AppendLine("        ) ");
            //        SQL.AppendLine("    ) ");
            //    }
            //}
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        if (ChkProfitCenterCode.Checked) 
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNoForCurrentEarning", mAcNoForCurrentEarning);
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();

                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string Dt, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo;");

            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string Dt, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From (");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Status='O' And A.CancelInd='N' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo;");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            if (mCurrentEarningFormulaType == "2")
            {
                if (
                    mAcNoForCurrentEarning.Length == 0 ||
                    mAcNoForCurrentEarning2.Length == 0 ||
                    mAcNoForActiva.Length == 0 ||
                    mAcNoForPassiva.Length == 0 ||
                    mAcNoForCapital.Length == 0
                    ) return;

                Process5c(ref lCOA);
            }
            else
            {
                if (mCurrentEarningFormulaType == "3")
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForActiva.Length == 0 ||
                        mAcNoForPassiva.Length == 0 ||
                        mAcNoForCapital.Length == 0
                        ) return;

                    Process5d(ref lCOA);
                }
                else
                    return;
            }
        }

        private void Process5c(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                CurrentProfiLossIndex2 = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory > 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }


                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory > 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory > 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            var CurrentProfiLossParentIndex2 = -1;
            var CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            var f2 = true;

            //Month To Date
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].MonthToDateDAmt - lCOA[IncomeIndex].MonthToDateCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].MonthToDateCAmt - lCOA[CostIndex1].MonthToDateDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].MonthToDateCAmt - lCOA[CostIndex2].MonthToDateDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].MonthToDateCAmt - lCOA[CostIndex3].MonthToDateDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].MonthToDateCAmt - lCOA[CostIndex4].MonthToDateDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].MonthToDateDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].MonthToDateCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].CurrentMonthDAmt - lCOA[IncomeIndex].CurrentMonthCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].CurrentMonthCAmt - lCOA[CostIndex1].CurrentMonthDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].CurrentMonthCAmt - lCOA[CostIndex2].CurrentMonthDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].CurrentMonthCAmt - lCOA[CostIndex3].CurrentMonthDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].CurrentMonthCAmt - lCOA[CostIndex4].CurrentMonthDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].CurrentMonthDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].CurrentMonthCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5d(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory >= 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }


                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory >= 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory >= 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }


            //Current Month
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process6(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt +
                    lCOA[i].CurrentMonthDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt +
                    lCOA[i].CurrentMonthCAmt;

                if (mCurrentEarningFormulaType != "2" && mCurrentEarningFormulaType != "3")
                {
                    if (lCOA[i].AcType == "D")
                        lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                    else
                        lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i+1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                return;
            }

            if (mIsRptBalanceSheetCurrentEarningUseProfitLoss)
            {
                decimal IncomeD = 0m, IncomeC = 0m, CostD = 0m, CostC = 0m;

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForIncome)))
                {
                    IncomeD = x.YearToDateDAmt;
                    IncomeC = x.YearToDateCAmt;
                }

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCost)))
                {
                    CostD = x.YearToDateDAmt;
                    CostC = x.YearToDateCAmt;
                }

                foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                {
                    
                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    i.Balance = (IncomeC-IncomeD) - (CostD-CostC);
                }
            }
            else
            {
                //foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                //{
                //    i.MonthToDateDAmt = 0m;
                //    i.CurrentMonthDAmt = 0m;
                //    i.YearToDateDAmt = i.OpeningBalanceDAmt;

                //    i.MonthToDateCAmt = 0m;
                //    i.CurrentMonthCAmt = 0m;
                //    i.YearToDateCAmt = i.OpeningBalanceCAmt;

                //    if (i.AcType == "D")
                //        i.Balance = i.YearToDateDAmt - i.YearToDateCAmt;
                //    else
                //        i.Balance = i.YearToDateCAmt - i.YearToDateDAmt;
                //}

                decimal IncomeD = 0m, IncomeC = 0m, CostD = 0m, CostC = 0m;

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForIncome)))
                {
                    IncomeD = x.YearToDateDAmt;
                    IncomeC = x.YearToDateCAmt;
                }

                foreach (var x in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCost)))
                {
                    CostD = x.YearToDateDAmt;
                    CostC = x.YearToDateCAmt;
                }

                foreach (var i in lCOA.Where(w => Sm.CompareStr(w.AcNo, mAcNoForCurrentEarning)))
                {

                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    if (i.AcType == "D")
                    {
                        i.Balance = (i.OpeningBalanceDAmt-i.OpeningBalanceCAmt)+(IncomeC - IncomeD) - (CostD - CostC);
                    }
                    else
                    {
                        i.Balance = (i.OpeningBalanceCAmt-i.OpeningBalanceDAmt) + (IncomeC - IncomeD) - (CostD - CostC);
                    }
                }
            }

            if (mBalanceSheetDraft == "1" && mAcNoForCurrentEarning.Length != 0) //ica menambahkan kondisi mAcNoForCurrentEarning.Length
            {
                foreach 
                    (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && w.HasChild))
                {
                    i.YearToDateDAmt = 0m;
                    i.YearToDateCAmt = 0m;
                    i.Balance = 0m;
                }

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && !w.HasChild))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(mAcNoForCurrentEarning, 1) && w.HasChild))
                    {
                        if (
                            i.AcNo.Count(x => x == '.') == j.AcNo.Count(x => x == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                            i.AcNo.Count(x => x == '.') != j.AcNo.Count(x => x == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                        {
                            j.YearToDateDAmt += i.YearToDateDAmt;
                            j.YearToDateCAmt += i.YearToDateCAmt;
                            j.Balance += i.Balance;
                            if (string.Compare(j.AcNo, i.AcNo) == 0)
                                break;
                        }
                    }
                }
            }
        }

        private bool Process10(COA lCOA)
        {
            if (lCOA.AcNo.Length == 1)
            {
                if (lCOA.AcNo == "1" || lCOA.AcNo == "2" || lCOA.AcNo == "3")
                    return true;
                else
                    return false;
            }
            else
            {
                if ( Sm.Left(lCOA.AcNo, 2) == "1." || Sm.Left(lCOA.AcNo, 2) == "2." || Sm.Left(lCOA.AcNo, 2) == "3.")
                    return true;
                else
                    return false;
            } 
        }

        private void SetLueDt(LookUpEdit Lue)
        {
            Sl.SetLookUpEdit(Lue,
                new string[] {
                    null,
                    "01", "02", "03", "04", "05",
                    "06", "07", "08", "09", "10",
                    "11", "12", "13", "14", "15",
                    "16", "17", "18", "19", "20",
                    "21", "22", "23", "24", "25",
                    "26", "27", "28", "29", "30",
                    "31"
                });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }


        private void LueLevelFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelFrom, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LueLevelTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelTo, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        #endregion
    }
}
