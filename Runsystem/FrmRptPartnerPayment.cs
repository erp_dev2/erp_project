﻿#region Update
/*    
    06/08/2019 [DITA] New Reporting
    28/08/2019 [WED] tambah kolom balance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmRptPartnerPayment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPartnerPayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

             SQL.AppendLine("Select F.PnCode, F.PnName, B.SurveyDocNo, MonthName(Str_To_Date(C.Mth, '%m')) As Mth, C.Yr, ");  
             SQL.AppendLine("B.Amt As Debit, D.TotalAmt As Credit ");
             SQL.AppendLine("From TblPLPHdr A  ");
             SQL.AppendLine("Inner Join TblPLPDtl B On A.DocNo = B.DocNo ");
             SQL.AppendLine("Inner Join TblLoanSummary C On B.SurveyDocNo = C.SurveyDocNo And B.SurveyDNo = C.SurveyDNo  ");
             SQL.AppendLine("Inner Join TblSurvey D On A.SurveyDocNo = D.DocNo  ");
             SQL.AppendLine("Inner Join TblRequestLP E On D.RQLPDocNo = E.DocNo  ");
             SQL.AppendLine("Inner Join TblPartner F On E.PnCode = F.PnCode  ");
             SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2  ");
             SQL.AppendLine("And A.Status In ('O', 'A')  ");
             SQL.AppendLine("And A.CancelInd = 'N' ");
             SQL.AppendLine("And D.Status In ('O', 'A')  ");
             SQL.AppendLine("And D.CancelInd = 'N' ");
             mSQL = SQL.ToString();
 
            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Partner"+Environment.NewLine+"Code",
                    "Partner Name",
                    "Survey#",
                    "Month",
                    "Year",

                    //6-8
                    "Debit",
                    "Credit",
                    "Balance"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 200, 150, 100, 80, 
                    
                    //6-8
                    100, 100, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPartnerCode.Text, new string[] { "F.PnCode", "F.PnName" });
                Sm.FilterStr(ref Filter, ref cm, TxtSurveyDocNo.Text, new string[] { "B.SurveyDocNo" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By F.PnName, B.SurveyDocNo, C.Yr, C.Mth; ",
                    new string[]
                    {
                        //0
                        "PnCode", 

                        //1-5
                        "PnName", "SurveyDocNo", "Mth", "Yr", "Debit", 
                        
                        //6-7
                        "Credit"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Grd.Cells[Row, 8].Value = 0m;
                    }, true, false, false, false
                );

                if (Grd1.Rows.Count > 0) ComputeBalance();

                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void ComputeBalance()
        {
            decimal mBalance = 0m, mCredit = 0m, mDebit = 0m;
            string mPnCode = string.Empty, mSurveyDocNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (mPnCode.Length == 0) mPnCode = Sm.GetGrdStr(Grd1, i, 1);
                if (mSurveyDocNo.Length == 0) mSurveyDocNo = Sm.GetGrdStr(Grd1, i, 3);

                if (mPnCode == Sm.GetGrdStr(Grd1, i, 1) && mSurveyDocNo == Sm.GetGrdStr(Grd1, i, 3))
                {
                    mCredit = Sm.GetGrdDec(Grd1, i, 7);
                    mDebit += Sm.GetGrdDec(Grd1, i, 6);
                    mBalance = mCredit - mDebit;
                    Grd1.Cells[i, 8].Value = mBalance;
                }
                else
                {
                    mPnCode = Sm.GetGrdStr(Grd1, i, 1);
                    mSurveyDocNo = Sm.GetGrdStr(Grd1, i, 3);
                    mDebit = Sm.GetGrdDec(Grd1, i, 6);
                    mCredit = Sm.GetGrdDec(Grd1, i, 7);
                    mBalance = mCredit - mDebit;
                    Grd1.Cells[i, 8].Value = mBalance;
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion
       
        #region Event

        private void TxtPartnerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPartnerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }
        private void TxtSurveyDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSurveyDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Survey#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }
        #endregion

    }
}
