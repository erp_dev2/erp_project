﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionShift : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmProductionShiftFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionShift(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProductionShiftCode, TxtProductionShiftName
                    }, true);
                    TxtProductionShiftCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProductionShiftCode, TxtProductionShiftName
                    }, false);
                    TxtProductionShiftCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtProductionShiftCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProductionShiftName
                    }, false);
                    TxtProductionShiftName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProductionShiftCode, TxtProductionShiftName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionShiftFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProductionShiftCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProductionShiftCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblProductionShift Where ProductionShiftCode=@ProductionShiftCode" };
                Sm.CmParam<String>(ref cm, "@ProductionShiftCode", TxtProductionShiftCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblProductionShift(ProductionShiftCode, ProductionShiftName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ProductionShiftCode, @ProductionShiftName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ProductionShiftName=@ProductionShiftName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ProductionShiftCode", TxtProductionShiftCode.Text);
                Sm.CmParam<String>(ref cm, "@ProductionShiftName", TxtProductionShiftName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtProductionShiftCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CntCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ProductionShiftCode", CntCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblProductionShift Where ProductionShiftCode=@ProductionShiftCode",
                        new string[] 
                        {
                            "ProductionShiftCode", "ProductionShiftName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtProductionShiftCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtProductionShiftName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProductionShiftCode, "Production shift code", false) ||
                Sm.IsTxtEmpty(TxtProductionShiftName, "Production shift name", false) ||
                IsCntCodeExisted();
        }

        private bool IsCntCodeExisted()
        {
            if (!TxtProductionShiftCode.Properties.ReadOnly && Sm.IsDataExist("Select ProductionShiftCode From TblProductionShift Where ProductionShiftCode='" + TxtProductionShiftCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Production shift code ( " + TxtProductionShiftCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtProductionShiftName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProductionShiftCode);
        }
        private void TxtProductionShiftCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProductionShiftCode);
        }
        #endregion

        #endregion
    }
}
