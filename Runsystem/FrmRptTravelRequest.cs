﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTravelRequest : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptTravelRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.DocNo, DATE_FORMAT(A.DocDt,'%d/%b/%Y') 'Date', (case when A.Status = 'A' then 'Approved' ");
            SQL.AppendLine("    when A.Status = 'O' then 'Outstanding' ");
            SQL.AppendLine("    when A.Status = 'C' then 'Cancelled' ");
            SQL.AppendLine("    end )as Status, DATE_FORMAT(A.StartDt,'%d/%b/%Y') 'StartDt',DATE_FORMAT(A.EndDt,'%d/%b/%Y') 'EndDt' , ");
            SQL.AppendLine("    C.SiteName, D.CityName, A.TravelService,'IDR' as Currency, E.EmpName 'PICName',E.EmpCode 'PICCode',H.EmpName,H.EmpCode, ");
            SQL.AppendLine("    B.Amt1 'Meal', B.Amt2 'Daily', B.Amt3 'CityTransport', B.Amt4 'Transport', B.Amt5 'Accomodation', ");
            SQL.AppendLine("    B.Amt6 'Allowance', B.Detasering, (B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) 'Total',  ");
            SQL.AppendLine("    B.VoucherRequestDocNo 'VRDocNo', DATE_FORMAT(F.DocDt,'%d/%b/%Y') 'VRDt' , F.VoucherDocNo 'VDocNo',  ");
            SQL.AppendLine("    DATE_FORMAT(G.DocDt,'%d/%b/%Y') 'VDt', A.SiteCode, A.CreateDt ");
            SQL.AppendLine("    From tbltravelrequesthdr A ");
            SQL.AppendLine("    Inner join tbltravelrequestdtl7 B on A.DocNo = B.DocNo ");
            SQL.AppendLine("    and (A.DocDt between @DocDt1 and @DocDt2) ");
            SQL.AppendLine("    Inner join tblsite C on A.SiteCode = C.SiteCode ");
            SQL.AppendLine("    Left join tblcompanycity D on A.CityCode = D.CityCode ");
            SQL.AppendLine("    Inner join tblemployee E on A.PICCode = E.EmpCode ");
            SQL.AppendLine("    Left join tblvoucherrequesthdr F on B.VoucherRequestDocNo = F.DocNo ");
            SQL.AppendLine("    Left join tblvoucherhdr G on F.VoucherDocNo = G.DocNo ");
            SQL.AppendLine("    Inner join tblemployee H on B.EmpCode = H.EmpCode ");
            SQL.AppendLine("   ) T ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 4;

            Grd1.Cols[0].Width = 40;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2; 

            Grd1.Cols[1].Width = 150;
            Grd1.Header.Cells[0, 1].Value = "Document#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 100;
            Grd1.Header.Cells[0, 2].Value = "Date";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 100;
            Grd1.Header.Cells[0, 3].Value = "Status";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 100;
            Grd1.Header.Cells[0, 4].Value = "Start Date";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 100;
            Grd1.Header.Cells[0, 5].Value = "End Date";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 130;
            Grd1.Header.Cells[0, 6].Value = "Site";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 130;
            Grd1.Header.Cells[0, 7].Value = "City";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 200;
            Grd1.Header.Cells[0, 8].Value = "Travel Service";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Grd1.Cols[9].Width = 70;
            Grd1.Header.Cells[0, 9].Value = "Currency";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;

            Grd1.Cols[10].Width = 100;
            Grd1.Header.Cells[0, 10].Value = "PIC Code";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;

            Grd1.Cols[11].Width = 150;
            Grd1.Header.Cells[0, 11].Value = "PIC Name";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;

            Grd1.Header.Cells[1, 12].Value = "Detail";
            Grd1.Header.Cells[1, 12].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 12].SpanCols = 9;
            Grd1.Header.Cells[0, 12].Value = "Employee Code";
            Grd1.Header.Cells[0, 13].Value = "Employee Name";
            Grd1.Header.Cells[0, 14].Value = "Meal";
            Grd1.Header.Cells[0, 15].Value = "Daily";
            Grd1.Header.Cells[0, 16].Value = "City Transport";
            Grd1.Header.Cells[0, 17].Value = "Transport";
            Grd1.Header.Cells[0, 18].Value = "Accomodation";
            Grd1.Header.Cells[0, 19].Value = "Allowance";
            Grd1.Header.Cells[0, 20].Value = "Datasering";
            Grd1.Cols[12].Width = 130;
            Grd1.Cols[13].Width = 150;
            Grd1.Cols[14].Width = 120;
            Grd1.Cols[15].Width = 120;
            Grd1.Cols[16].Width = 120;
            Grd1.Cols[17].Width = 120;
            Grd1.Cols[18].Width = 120;
            Grd1.Cols[19].Width = 120;
            Grd1.Cols[20].Width = 120;

            Grd1.Cols[21].Width = 130;
            Grd1.Header.Cells[0, 21].Value = "Total";
            Grd1.Header.Cells[0, 21].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 21].SpanRows = 2;

            Grd1.Cols[22].Width = 120;
            Grd1.Header.Cells[0, 22].Value = "Voucher" + Environment.NewLine + "Request#";
            Grd1.Header.Cells[0, 22].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 22].SpanRows = 2;

            Grd1.Cols[23].Width = 100;
            Grd1.Header.Cells[0, 23].Value = "Voucher Request" + Environment.NewLine + "Date";
            Grd1.Header.Cells[0, 23].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 23].SpanRows = 2;

            Grd1.Cols[24].Width = 120;
            Grd1.Header.Cells[0, 24].Value = "Voucher#";
            Grd1.Header.Cells[0, 24].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 24].SpanRows = 2;

            Grd1.Cols[25].Width = 100;
            Grd1.Header.Cells[0, 25].Value = "Voucher Date";
            Grd1.Header.Cells[0, 25].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 25].SpanRows = 2;

           
            Sm.GrdFormatDec(Grd1, new int[] { 14,15,16,17,18,19,20,21 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 10,12 }, false);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10,12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;


            try
            {

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

               Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
               Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                //Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
               Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
               Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.PICCode", "T.PICName", "T.EmpCode", "T.EmpName" });
               Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1,
                        ref cm,
                        mSQL + Filter + " Order By T.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",  

                            //1-5
                            "Date", "Status", "StartDt", "EndDt", "SiteName",

                            //6-10
                            "CityName", "TravelService", "Currency", "PICCode", "PICName",

                            //11-15
                            "EmpCode", "EmpName", "Meal", "Daily", "CityTransport",

                            //16-20
                            "Transport", "Accomodation", "Allowance", "Detasering", "Total",

                             //21-24
                            "VRDocNo", "VRDt", "VDocNo", "VDt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            if (Sm.DrStr(dr, 2) == "Cancelled")
                            {
                                Grd.Rows[Row].BackColor = Color.Red;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            }
                            
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                           
                        }, true, false, false, false
                    );
                //iGSubtotalManager.BackColor = Color.LightSalmon;
                //iGSubtotalManager.ShowSubtotalsInCells = true;
                //iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }
        #endregion
    }
}
