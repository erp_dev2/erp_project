﻿#region Update
/*
    03/02/2020 [HAR/TWC] POS TWC ticketing
 *  09/06/2020 [HAR/TWC] tambah filter var 1 dan var 2
 *  21/11/2020 [HAR/TWC] tambah informasi bankaccount
    18/10/2021 [HAR/TWC] tambah informasi costcenter dan active indicator
    01/04/2022 [HAR/TWC] tambah filter period
    09/02/2023 [HAR/TWC] tambah validasi untuk terusan akan mabil dari itcode dahulu baru ke groupname
    */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmTWCJournalSettingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmTWCJournalSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTWCJournalSettingFind(FrmTWCJournalSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


         override protected void SetSQL()
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Select X.IDno, X.ProcessCode, X.AcType, X.VarA, X.VarB, X.ProcessName, ");
                SQL.AppendLine("X.AcNo, X.AcDesc, X.bankacCode, ifnull(X.BankAcNm, null) As BankAcNm, X.CCCode, X2.CCName, X.ActInd, X.createby, X.createDt, X.LastupBy, X.lastupdt ");                
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select T1.IDNo, T1.ProcessType As ProcessCode, T1.AcType, T1.Var1 As VarA, T1.Var2 As VarB,  ");
                SQL.AppendLine("    Case When T1.ProcessType = '1' THEN 'FLAT' ");
                SQL.AppendLine("    When T1.ProcessType = '2' Then 'TERUSAN' ");
                SQL.AppendLine("    When T1.ProcessType = '3' Then 'CASH' ");
                SQL.AppendLine("    When T1.ProcessType = '4' Then 'NON CASH' ");
                SQL.AppendLine("    End As ProcessName, T1.AcNo, T2.AcDesc, T1.bankAcCode, T1.BankAcNm, T1.CCCode, T1.ActInd, T1.createby, T1.createDt, T1.LastupBy, T1.lastupdt ");
                SQL.AppendLine("    from ( ");
                SQL.AppendLine("        SELECT A.IDNO, A.processtype, A.AcType, A.Var1, A.Var2, A.Acno, A.BankAcCode, Concat(B.BankAcNo, ' ', B.BankAcNm) AS BankAcNm, A.CCCode, A.ActInd, IFNULL(A.createBY, 'Sys') As CreateBY, IFNULL(A.Createdt, '202001010000') As CreateDt, IFNULL(A.LastUpBY, 'Sys') LastUpBy, IFNULL(A.LastUpdt, '202001010000')  LastUpDt ");
                SQL.AppendLine("        From TblTwcJournalSetting A ");
                SQL.AppendLine("        LEFT JOIN TblBankAccount B ON A.BankAcCode = B.BankAcCode ");
                SQL.AppendLine("        Union ALL ");
                SQL.AppendLine("        Select null,  Z.*, null As AcNo, '' as BankAcCode, '' as BankAcNm, null as CCCode, null as ActInd, 'Sys' as Createby, '202001010000' as CreateDt, 'Sys' as lastupby, '202001010000' as lastupdt From  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            select '1' As ProcessType, 'D' As AcType, Gronm As var1, null As Var2 "); 
                SQL.AppendLine("            From TblEdw A ");
                SQL.AppendLine("            Where JnStick = 'FLAT' And WhsCode <> 0 ");
            //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) "); 
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group by Gronm ");
                SQL.AppendLine("            UNION ALL ");
                SQL.AppendLine("            SELECT '1' As ProcessType, 'D' As AcType,  SUBSTRING_index(itcode, '#', 1) As var1, null As Var2   ");
                SQL.AppendLine("            From TblEdw A  ");
                SQL.AppendLine("            Where JnStick = 'FLAT' And WhsCode <> 0  ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT)  ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            AND ItCode LIKE '%#%'  ");
                SQL.AppendLine("            AND SUBSTRING_index(itcode, '#', 1)  NOT IN  ");
                SQL.AppendLine("            ( ");
	            SQL.AppendLine("                select Gronm  ");
	            SQL.AppendLine("                From TblEdw A  ");
	            SQL.AppendLine("                Where JnStick = 'FLAT' And WhsCode <> 0  ");
                //SQL.AppendLine("                And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT)  ");
                SQL.AppendLine("                And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("                GROUP by Gronm  "); 
                SQL.AppendLine("            ) ");
                SQL.AppendLine("            Group by SUBSTRING_index(itcode, '#', 1)  ");
                SQL.AppendLine("            union all ");
                SQL.AppendLine("            select '1' As ProcessType, 'C' As AcType, trf_name As var1, null As Var2  ");
                SQL.AppendLine("            From TblEdw A ");
                SQL.AppendLine("            Where JnStick = 'FLAT' And WhsCode <> 0 ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group by trf_name ");
                SQL.AppendLine("            union all ");
                SQL.AppendLine("            select '2' As ProcessType, 'D' As AcType, trf_name As var1, null As Var2 From TblEdw A ");
                SQL.AppendLine("            Where JnStick = 'TERUSAN' And WhsCode <> 0 ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group BY trf_Name ");
                SQL.AppendLine("            Union ALL ");
                SQL.AppendLine("            select '2' As ProcessType, 'C' As AcType, trf_name As var1, if (SUBSTRING_index(itcode, '#', 1) = 'TWC',  (SUBSTRING_index(itcode, '#', 1)), gronm) As Var2 From TblEdw A ");
                SQL.AppendLine("            Where JnStick = 'TERUSAN' And WhsCode <> 0 ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group BY trf_Name, if (SUBSTRING_index(itcode, '#', 1) = 'TWC',  (SUBSTRING_index(itcode, '#', 1)), gronm)  ");
                
                SQL.AppendLine("            union all ");
                SQL.AppendLine("            select '3' As ProcessType, 'D' As AcType, SUBSTRING_index(itcode, '#', 1) As Var1, null As Var2 ");
                SQL.AppendLine("            From TblEdw A ");
                SQL.AppendLine("            Where paytpno = 'CASH' And WhsCode = 0 And ItCode like '%#%' ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group By SUBSTRING_index(itcode, '#', 1) ");
                SQL.AppendLine("            Union ALL ");
                SQL.AppendLine("            select '3' As ProcessType, 'C', SUBSTRING_index(itcode, '#', 1) As Var1, null As Var2 ");
                SQL.AppendLine("            From TblEdw A ");
                SQL.AppendLine("            Where paytpno = 'CASH' And WhsCode = 0 And ItCode like '%#%' ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group BY SUBSTRING_index(itcode, '#', 1) ");
                SQL.AppendLine("            union all ");
                SQL.AppendLine("            select '4' As ProcessType, 'D' As AcType, SUBSTRING_index(itcode, '#', 1) As Var1, edcCode As Var2 From TblEdw A ");
                SQL.AppendLine("            Where paytpNo != 'CASH' And WhsCode = 0 And ItCode like '%#%' ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            group BY SUBSTRING_index(itcode, '#', 1), edcCode ");
                SQL.AppendLine("            union all ");
                SQL.AppendLine("            select '4' As ProcessType, 'C' As AcType, SUBSTRING_index(itcode, '#', 1) As Var1, null As Var2 From TblEdw A ");
                SQL.AppendLine("            Where paytpNo != 'CASH' And WhsCode = 0 And ItCode like '%#%' ");
                //SQL.AppendLine("            And cast(Left(CurrentdateTime(), 6) AS INT) BETWEEN  (cast(Left(CurrentdateTime(), 6) AS INT)-3) and cast(Left(CurrentdateTime(), 6) AS INT) ");
                SQL.AppendLine("            And A.bsdate Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            group BY SUBSTRING_index(itcode, '#', 1) ");
                SQL.AppendLine("        )Z ");
                SQL.AppendLine("        Where Concat(Z.processtype, Z.Actype, ifnull(Z.var1, '#'), ifnull(Z.var2, '#')) ");
                SQL.AppendLine("        not in ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Concat(processtype, Actype, ifnull(var1, '#'), ifnull(var2, '#')) As keyword ");
                SQL.AppendLine("            From TblTwcJournalSetting ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        )T1 ");
                SQL.AppendLine("    Left Join TblCOA T2 On T1.Acno = T2.Acno ");
                SQL.AppendLine(")X ");
                SQL.AppendLine("Left Join TblCostcenter X2 On X.CCCode = X2.CCCode ");

            mSQL = SQL.ToString();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "IDNO",
                        "Process"+Environment.NewLine+"Code", 
                        "Process"+Environment.NewLine+"Name",
                        "Account"+Environment.NewLine+"Type",   
                        "var 1", 

                        //6-10
                        "Var 2", 
                        "Account"+Environment.NewLine+"Number", 
                        "Description",
                        "BankAcCode",
                        "Bank Account"+Environment.NewLine+"Name",

                        //11-15
                        "Cost Center"+Environment.NewLine+"Code",
                        "Cost Center",
                        "Active",
                        "Created By",
                        "Created Date", 

                        //16
                        "Created Time",
                        "Last Updated By", 
                        "Last Updated Date",
                        "Last Updated Time",
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        50, 80, 150, 80, 120,                          
                        //6-10
                        120, 100, 250, 130, 130,   
                        //11-15
                        120, 120, 80, 130, 130,  
                        //16-18
                        130,  130, 130, 130
                    }
                );
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColCheck(Grd1, new int[] { 13 });
            Sm.GrdColInvisible(Grd1, new int[]{ 11 }, false);
            Grd1.Cols[13].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 ";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtProcess.Text, new string[] { "X.ProcessCode", "X.ProcessName" });
                Sm.FilterStr(ref Filter, ref cm, TxtVar1.Text, "X.VarA", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVar2.Text, "X.VarB", false);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By X.IDNo ",
                        new string[]
                        {
                            //0
                            "IDNo", 
                                
                            //1-5
                             "ProcessCode", "ProcessName", "AcType", "VarA", "VarB",

                            //6-10
                             "AcNo","AcDesc", "BankAcCode", "bankAcNm", "CCCode",   
                            
                            //11-15
                            "CCName", "ActInd", "CreateBy",  "CreateDt", "LastUpBy", 
                            //16
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 13, 12);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 16);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData();
                mFrmParent.TxtIDNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1); ;
                mFrmParent.TxtProcessCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtProcessName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtVar1.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtVar2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtAcType.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                Sm.SetLue(mFrmParent.LueBankAcCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9));
                Sm.SetLue(mFrmParent.LueCCCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11));
                mFrmParent.ChkActInd.Checked = Sm.GetGrdBool(Grd1, Grd1.CurRow.Index, 13);
                this.Hide();
            }
        }


        #endregion

        #region Event

        private void TxtProcess_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProcess_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Process");
        }

        private void ChkVar1_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Var1");
        }

        private void ChkVar2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Var2");
        }

        private void TxtVar1_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtVar2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }


        #endregion

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
    }
}
