﻿#region Update
/*
    27/09/2018 [HAR] bug set grid
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    23/10/2019 [DITA/IMS] Printout BOM
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
    15/03/2022 [TKG/GSS] merubah GetParameter dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmBom : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty;
        internal bool mIsBOMShowDimensions = false,
             mIsBOMShowSpecifications = false;
        internal FrmBomFind FrmFind;
        private string mMainCurCode = string.Empty;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmBom(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion
        
        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bill of Material";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);
                Sl.SetLueBomValueCode(ref LueBomValue);
                LueBomValue.Visible = false;
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Type",
                    "Document#",
                    "",
                    "Name",
                    "Source",
                    
                    //6-10
                    "Quantity",
                    "UoM",
                    "Value Code",
                    "Value From",
                    "Value",

                    //11-14
                    "Old Code",
                    "Dimensions",
                    "Specification",
                    "Item's Code"+Environment.NewLine+"Internal"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    0, 130, 20, 250, 130, 
                    
                    //6-10
                    80, 80, 80, 100, 80,

                    //11-14
                    100, 300, 300, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 8, 14 });
            if (!mIsBOMShowDimensions) Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 13 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 4, 5, 7, 8, 10, 11, 12, 13, 14 });
            Grd1.Cols[11].Move(5);
            Grd1.Cols[14].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtItCode, TxtItName, TxtQty, TxtUom, 
                        DteDocDt, TxtDocName,  LueBomValue, MeeRemark, TxtWCDocNo, 
                        TxtSource
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 6, 9 });
                    ChkActiveInd.Properties.ReadOnly = true;
                    BtnItCode.Enabled = false;
                    BtnWCDocNo.Enabled = false;
                    BtnBomDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtDocName, TxtQty, MeeRemark,  LueBomValue
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6, 9 });
                    BtnItCode.Enabled = true;
                    BtnWCDocNo.Enabled = true;
                    BtnBomDocNo.Enabled = true;
                    TxtQty.Focus();
                    break;
                case mState.Edit:
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, TxtItCode, TxtItName, DteDocDt, TxtDocName, MeeRemark, TxtUom, TxtWCDocNo,  LueBomValue, TxtSource
            });
            ChkActiveInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtQty }, 0); 
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 10 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBomFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            ChkActiveInd.Checked = true;
            Sm.FormShowDialog(new FrmBomDlg(this));
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

                string[] TableName = { "BOMHdr", "BOMDtl", "BOMSign" };
                List<IList> myLists = new List<IList>();
                var l = new List<BOMHdr>();
                var l2 = new List<BOMDtl>();
                var lSign = new List<BOMSign>();
                var lSign2 = new List<BOMSign2>();
                var lDtlSign = new List<BOMDtlSign>();
                decimal Numb = 0;
               
                #region BOM Header

                var cm = new MySqlCommand();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
                SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, C.ItName, A.Remark, ");
                SQL.AppendLine(" C.Specification, C.PlanningUomCode, B.Qty ");
                SQL.AppendLine(" From TblBOMHdr A ");
                SQL.AppendLine(" Inner Join TblBomDtl2 B On A.DocNo = B.DocNo ");
                SQL.AppendLine(" Inner Join TblItem C On B.ItCode = C.ItCode ");
                SQL.AppendLine(" Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyLogo",
                         "DocNo", 
                         "DocDt", 
                         "ItName",
                         "Specification",

                         //6-8
                         "Qty", 
                         "PlanningUomCode",
                         "Remark"
                        
                         
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new BOMHdr()
                            {
                                CompanyName = Sm.DrStr(dr, c[0]),

                                CompanyLogo = Sm.DrStr(dr, c[1]),
                                DocNo = Sm.DrStr(dr, c[2]),
                                DocDt = Sm.DrStr(dr, c[3]),
                                ItName = Sm.DrStr(dr, c[4]),
                                Specification = Sm.DrStr(dr, c[5]),

                                Qty = Sm.DrDec(dr, c[6]),
                                Uom = Sm.DrStr(dr, c[7]),
                                Remark = Sm.DrStr(dr, c[8]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                               
                            });
                        }
                    }
                    dr.Close();
                }

                myLists.Add(l);
                #endregion

                #region BOM Detail

                var cm2 = new MySqlCommand();

                var SQL2 = new StringBuilder();

                SQL2.AppendLine(" Select E.ItCodeInternal, E.Specification,  ");
                SQL2.AppendLine(" A.Qty, E.PlanningUomCode2,  ");
                SQL2.AppendLine(" Case A.DocType ");
                SQL2.AppendLine(" When '3' Then Concat(C.EmpName, ' (', IfNull(G.PosName, '-'), ' / ', IfNull(H.DeptName, '-'), ')') ");
                SQL2.AppendLine(" When '2' Then D.DocName  ");
                SQL2.AppendLine(" When '1' Then E.ItName End As DocName, E.Remark ");
                SQL2.AppendLine(" From TblBOMDtl A ");
                SQL2.AppendLine(" Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='BomDocType' ");
                SQL2.AppendLine(" Left Join TblEmployee C On A.DocCode=C.EmpCode And '3'=A.DocType ");
                SQL2.AppendLine(" Left Join TblFormulaHdr D On A.DocCode=D.DocNo And '2'=A.DocType ");
                SQL2.AppendLine(" Left Join TblItem E On A.DocCode=E.ItCode And '1'=A.DocType  ");
                SQL2.AppendLine(" Left Join TblOption F On A.BomValueFrom = F.OptCode And F.OptCat='BOMValueFrom' ");
                SQL2.AppendLine(" Left Join TblPosition G On C.PosCode=G.PosCode  ");
                SQL2.AppendLine(" Left Join TblDepartment H On C.DeptCode=H.DeptCode  ");
                SQL2.AppendLine(" Where A.DocNo=@DocNo;  ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ItCodeInternal",

                         //1-5
                         "Specification",
                         "Qty", 
                         "PlanningUomCode2", 
                         "DocName",
                         "Remark"

                         
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            Numb = Numb + 1; 
                            l2.Add(new BOMDtl()
                            {
                                Number = Numb,
                                ItCodeInternal = Sm.DrStr(dr2, c2[0]),

                                Specification = Sm.DrStr(dr2, c2[1]),
                                Qty = Sm.DrDec(dr2, c2[2]),
                                Uom = Sm.DrStr(dr2, c2[3]),
                                ItName = Sm.DrStr(dr2, c2[4]),
                                Remark = Sm.DrStr(dr2, c2[5]),

                            });
                        }
                    }
                    dr2.Close();
                }

                myLists.Add(l2);
                #endregion

                #region Detail Signature

                //Dibuat Oleh
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Dibuat oleh,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                    SQLDtl.AppendLine("    From TblBOMHdr A ");
                    SQLDtl.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl.AppendLine("    Where A.ActiveInd='Y' And A.DocNo=@DocNo ");
                    SQLDtl.AppendLine(") T1 ");
                    SQLDtl.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                    SQLDtl.AppendLine("Order By T1.Level; ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            lSign.Add(new BOMSign()
                            {
                                Signature = Sm.DrStr(drDtl, cDtl[0]),
                                UserName = Sm.DrStr(drDtl, cDtl[1]),
                                PosName = Sm.DrStr(drDtl, cDtl[2]),
                                Space = Sm.DrStr(drDtl, cDtl[3]),
                                DNo = Sm.DrStr(drDtl, cDtl[4]),
                                Title = Sm.DrStr(drDtl, cDtl[5]),
                                LastUpDt = Sm.DrStr(drDtl, cDtl[6])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(lSign);

  
                #endregion 
                Sm.PrintReport("BOM", myLists, TableName, false);

            }
        }
      

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBomDlg2(this, TxtWCDocNo.Text));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 6, 9, 10 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 10 });
                }
            }
            
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "1"))
                {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "2"))
                {
                    var f2 = new FrmFormula(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "3"))
                {
                    //var f3 = new FrmWorkCenter(mMenuCode);
                    //f3.Tag = mMenuCode;
                    //f3.WindowState = FormWindowState.Normal;
                    //f3.StartPosition = FormStartPosition.CenterScreen;
                    //f3.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    //f3.ShowDialog();
                    var f3 = new FrmEmployee(mMenuCode);
                    f3.Tag = mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f3.ShowDialog();
                }
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd1, ref LueBomValue, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sl.SetLueBomValueCode(ref LueBomValue);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmBomDlg2(this, TxtWCDocNo.Text));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "1"))
                {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "2"))
                {
                    var f2 = new FrmFormula(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "3"))
                {
                    var f3 = new FrmEmployee(mMenuCode);
                    f3.Tag = mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[]{ 6, 10 }, e.ColIndex) && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOM", "TblBOMHdr");
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveBom(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveBomDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item Code", false) ||
                Sm.IsTxtEmpty(TxtQty, "Quantity", true)||
                Sm.IsTxtEmpty(TxtDocName, "Bill Of Material Name", false) ||
                Sm.IsDteEmpty(DteDocDt, "Bill Of Material date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Document# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, true,
                        "Document# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Document Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Document's Source : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.") //||
                    //Sm.IsGrdValueEmpty(Grd1, Row, 10, true,
                    //    "Document# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    //    "Document Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    //    "Document's Source : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                    //    "Value should be greater than 0.")
                    ) return true;
            return false;
        }

        private MySqlCommand SaveBom(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* BOM */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblBomHdr(DocNo, DocDt, DocName, ActiveInd, WorkCenterDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocName, 'Y', @WorkCenterDocNo, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblBomDtl2(DocNo, Dno, ItCode, ItType, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, '001', @ItCode, '1', @Qty, @UserCode, @Dt); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblBomDtl(DocNo, DNo, DocType, DocCode, Qty, BomValueFrom, Value, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @DocCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @BomValueFrom_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@DocCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@BomValueFrom_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", TxtWCDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveBomDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblBomDtl(DocNo, DNo, DocType, DocCode, Qty, BomValueFrom, Value, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @DocType, @DocCode, @Qty, @BomValueFrom, @Value, @CreateBy, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@BomValueFrom", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveBomDtl2(string DocNo)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblBomDtl2(DocNo, Dno, ItCode, ItType, Qty, CreateBy, CreateDt) " +
        //            "Values(@DocNo, '001', @ItCode, @ItType, @Qty, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditBOMHdr();
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Bill of material#", false) ||
                IsActiveIndEditedAlready();
        }

        private bool IsActiveIndEditedAlready()
        {
            var ActiveInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand() 
            { 
                CommandText = "Select 1 From TblBomHdr Where DocNo=@DocNo And ActiveInd=@ActiveInd "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ActiveInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is " + (Sm.CompareStr(ActiveInd, "Y")?"":"non") +"activated already.");
                return true;
            }
            return false;
        }

        private void EditBOMHdr()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Update TblBomHdr Set ActiveInd=@ActiveInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowBomHdr(DocNo);
                ShowBomDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowData2(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, true);
                ShowBomDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBomHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, B.ItCode, A.WorkCenterDocNo, C.ItName, C.PlanningUomCode, B.Qty, A.DocDt, A.DocName, A.ActiveInd, A.Remark " +
                    "From TblBomHdr A "+
                    "Inner Join TblBomDtl2 B On A.DocNo = B.DocNo "+
                    "Inner Join TblItem C On B.ItCode = C.ItCode " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "ItCode", "ItName", "WorkCenterDocNo", "PlanningUomCode", "Qty", 
                        
                        //6-9
                        "DocDt", "DocName", "ActiveInd", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[1]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtWCDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtUom.EditValue = Sm.DrStr(dr, c[4]);
                        TxtQty.EditValue = Sm.DrDec(dr, c[5]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[6]));
                        TxtDocName.EditValue = Sm.DrStr(dr, c[7]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[8]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    }, true
                );
        }

        private void ShowBomDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.DocCode, B.OptDesc, A.Qty, E.PlanningUomCode2, A.Value, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '3' Then Concat(C.EmpName, ' (', IfNull(G.PosName, '-'), ' / ', IfNull(H.DeptName, '-'), ')') ");
            SQL.AppendLine("    When '2' Then D.DocName ");
            SQL.AppendLine("    When '1' Then E.ItName End As DocName, ");
            SQL.AppendLine("A.BOMValueFrom, F.OptDesc As BomValueName, ");
            SQL.AppendLine("Case When A.DocType='3' Then C.EmpCodeOld Else Null End As OldCode, ");
            SQL.AppendLine("E.Specification, E.ItCodeInternal,  ");
            if (mIsBOMShowDimensions)
            {
                SQL.AppendLine("Case When A.DocType='1' Then ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(E.Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(E.Length, 0.00), 2) Using utf8), ' ', IfNull(E.LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(E.Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Height : ', Trim(Concat(Convert(Format(IfNull(E.Height, 0.00), 2) Using utf8), ' ', IfNull(E.HeightUomCode, '')))) ");
                SQL.AppendLine("Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(E.Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Width : ', Trim(Concat(Convert(Format(IfNull(E.Width, 0.00), 2) Using utf8), ' ', IfNull(E.WidthUomCode, '')))) ");
                SQL.AppendLine("Else '' End ");
                SQL.AppendLine(")) Else Null End As Dimensions ");
            }
            else
                SQL.AppendLine("Null As Dimensions ");
            SQL.AppendLine("From TblBOMDtl A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblEmployee C On A.DocCode=C.EmpCode And '3'=A.DocType ");
            SQL.AppendLine("Left Join TblFormulaHdr D On A.DocCode=D.DocNo And '2'=A.DocType ");
            SQL.AppendLine("Left Join TblItem E On A.DocCode=E.ItCode And '1'=A.DocType ");
            SQL.AppendLine("Left Join TblOption F On A.BomValueFrom = F.OptCode And F.OptCat='BOMValueFrom' ");
            SQL.AppendLine("Left Join TblPosition G On C.PosCode=G.PosCode ");
            SQL.AppendLine("Left Join TblDepartment H On C.DeptCode=H.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocType", 

                    //1-5
                    "DocCode", "DocName", "OptDesc", "Qty", "PlanningUomCode2", 
                    
                    //6-10
                    "BOMValueFrom", "BomValueName", "Value", "OldCode", "Dimensions",

                    //11-12
                    "Specification", "ItCodeInternal"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'IsBOMShowDimensions', 'IsBOMShowSpecifications' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsBOMShowDimensions": mIsBOMShowDimensions = ParValue == "Y"; break;
                            
                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ComputeValue(int Row)
        {
            try
            {
                var BOMValueFrom = Sm.GetGrdStr(Grd1, Row, 8);
                Grd1.Cells[Row, 10].Value = 0m;
                if (BOMValueFrom == "1")
                {
                    Grd1.Cells[Row, 10].ReadOnly = iGBool.False;
                    Grd1.Cells[Row, 10].BackColor = Color.White;
                }
                else
                {
                    Grd1.Cells[Row, 10].ReadOnly = iGBool.True;
                    Grd1.Cells[Row, 10].BackColor = Color.FromArgb(224, 224, 224);
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdStr(Grd1, Row, 1) == "1")
                    {
                        if (BOMValueFrom == "2") ComputeValueFromStockValue(Row);
                        if (BOMValueFrom == "3") ComputeValueFromItemPrice(Row);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ComputeValueFromStockValue(int Row)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.UPrice*C.ExcRate As Value ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ItCode=@ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Order By A.DocDt Desc, B.CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            var Value = Sm.GetValue(cm);
            if (Value.Length == 0) Value = "0";
            Grd1.Cells[Row, 10].Value = decimal.Parse(Value);
        }

        private void ComputeValueFromItemPrice(int Row)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UPrice* ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As Value ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode ");
            SQL.AppendLine("Order By A.DocDt Desc, B.CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            var Value = Sm.GetValue(cm);
            if (Value.Length == 0) Value = "0";
            Grd1.Cells[Row, 10].Value = decimal.Parse(Value);
        }

        internal string GetSelectedDocument()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += ((SQL.Length != 0?", ":"") + "'" + Sm.GetGrdStr(Grd1, Row, 1)+Sm.GetGrdStr(Grd1, Row, 2) + "'");
                
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBomDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBomDlg4(this));
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBomDlg(this));
        }

        private void BtnWCDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBomDlg3(this));
        }

        private void BtnWCDocNo2_Click(object sender, EventArgs e)
        {
            if (TxtWCDocNo.Text.Length != 0)
            {
                var f1 = new FrmWorkCenter(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtWCDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void TxtDocName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocName);
        }

        private void TxtQty_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
        }

        private void LueBomValue_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBomValue, new Sm.RefreshLue1(Sl.SetLueBomValueCode));
        }

        private void LueBomValue_Leave(object sender, EventArgs e)
        {
            if (LueBomValue.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueBomValue).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = null;
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueBomValue);
                    Grd1.Cells[fCell.RowIndex, 9].Value = LueBomValue.GetColumnValue("Col2");
                }
                ComputeValue(fCell.RowIndex);
                LueBomValue.Visible = false;
            }
        }

        private void LueBomValue_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        #endregion       

       
        #endregion

        #region Class

        private class BOMHdr
        {
            public string PrintBy { set; get; }
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string Remark { get; set; }
 
        }

        private class BOMDtl
        {
            public decimal Number { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string Remark { get; set; }
 
        }

        private class BOMSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class BOMSign2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class BOMDtlSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        #endregion
    }
}
