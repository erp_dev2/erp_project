﻿#region Update
/*
    19/12/2022 [ICA/MNET] new Apps
    29/12/2022 [ICA/MNET] Menambah Document Approval
    10/02/2023 [MYA/MNET] jurnal net off payment saat ini masih kosong. seharusnya mengambil daridepartemen di menu net off payment
    20/02/2023 [DITA/MNET] ubah method setluenetoffcode
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmNetOffPayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty, 
            mCtCode = string.Empty,
            mVdCode = string.Empty;
        private bool
            mIsNetOffPaymentUseCOA = false,
            mIsPurchaseInvoiceUseApproval = false
            ;
        internal bool
            mIsIncomingPaymentShowDORemark = false,
            mIsFilterByDept = false,
            mIsFilterBySite = false,
            mIsPITotalWithoutTaxInclDownpaymentEnabled = false,
            mIsOutgoingPaymentFilterByPIDept = false,
            mIsGroupCOAActived = false,
            mIsNetOffPaymentAmtUseCOAAmt = false;

        internal FrmNetOffPaymentFind FrmFind;

        #endregion

        #region Constructor

        public FrmNetOffPayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Net Off Payment";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                SetFormControl(mState.View);

                //Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                //Sm.SetLue(LueFontSize, "9");
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankCode(ref LueBankCode);
                SetLueNetOffCode(ref LueNetOffCode, string.Empty);
                TpCOA.PageVisible = TpCOA2.PageVisible = mIsNetOffPaymentUseCOA;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Invoice#",
                        "Date",
                        "",
                        "Type Code",

                        //6-10
                        "Type",
                        "Currency",
                        "Outstanding"+Environment.NewLine+"Amount",
                        "Amount",
                        "Due Date",
                        
                        //11-13
                        "Description for Voucher Request",
                        "CBD",
                        "DO's Remark"

                    },
                     new int[]
                    {
                        //0
                        0,

                        //1-5
                        20, 150, 100, 20, 0,
                        
                        //6-10
                        130, 60, 130, 130, 80, 
                        
                        //11-13
                        400, 0, 400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 10, 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 10 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5, 12 }, false);
            if (!mIsIncomingPaymentShowDORemark)
                Sm.GrdColInvisible(Grd1, new int[] { 13 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 16;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Invoice#",
                        "Date",
                        "",
                        "Type",

                        //6-10
                        "Type",
                        "Vendor Code",
                        "Vendor",
                        "Currency",
                        "Outstanding"+Environment.NewLine+"Amount",

                        //11-15
                        "Amount",
                        "Due Date",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        "Voucher Request's Description",
                        "Dept Code"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        20, 130, 80, 20, 0,
                        
                        //6-10
                        130, 0, 250, 70, 100, 
                        
                        //11-15
                        100, 100, 130, 400, 0
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd2, new int[] { 10, 11 }, 0);
            Sm.GrdFormatDate(Grd2, new int[] { 3, 12 });
            Sm.GrdColButton(Grd2, new int[] { 1, 4 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 3, 5, 7, 15 }, false);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Duplicated"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColCheck(Grd3, new int[] { 6 });
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2 });

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Duplicated"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6
                        0
                    }
                );
            Sm.GrdColCheck(Grd4, new int[] { 6 });
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd4, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 1, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2, 6 });

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 4;
            Grd5.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[]
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[]
                    {
                        150,
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 2 });

            #endregion
        }

        private  void HideInfoInGrd()
        {
            bool IsHide = !ChkHideInfoInGrd.Checked;
            Sm.GrdColInvisible(Grd1, new int[] { }, IsHide);
            Sm.GrdColInvisible(Grd2, new int[] { 3 }, IsHide);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, IsHide);
            Sm.GrdColInvisible(Grd4, new int[] { 1 }, IsHide);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, LueNetOffCode, LueDeptCode, TxtStatus, MeeCancelReason, ChkCancelInd, MeeRemark,
                        TxtPIAmt, TxtSLIAmt, LuePaymentType, LueBankAcCode, LueBankCode, TxtGiroNo, 
                        TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtTotalAmt, DteDueDt, LueAcType
                    }, true);
                    BtnNetOffCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 9, 11 }); 
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 11, 14 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
                    Sm.GrdColInvisible(Grd2, new int[] { 10 }, false);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueDeptCode, MeeRemark, LuePaymentType, LueBankAcCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 9, 11 });
                    Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                    Sm.GrdColInvisible(Grd2, new int[] { 10 }, true);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 11, 14 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 3, 4, 5 });
                    BtnNetOffCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    BtnNetOffCode.Enabled = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, LueNetOffCode, LueDeptCode, TxtStatus, MeeCancelReason, MeeRemark, 
                LueAcType, LuePaymentType, LueBankAcCode, LueBankCode, TxtGiroNo, DteDueDt,
                TxtVoucherDocNo, TxtVoucherRequestDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtPIAmt, TxtSLIAmt, TxtTotalAmt
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9 });
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 12 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 10, 11 });
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 3, 4 });
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmNetOffPaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetLue(LueAcType, "C");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //ParPrint();
        }

        private void BtnNetOffCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmNetOffPaymentDlg(this));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "NetOffPayment", "TblNetOffPaymentHdr");

            string VoucherRequestDocNo = string.Empty;

            VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveNetOffPaymentHdr(DocNo, VoucherRequestDocNo));

            cml.Add(SaveNetOffPaymentDtl(DocNo));

            cml.Add(SaveNetOffPaymentDtl2(DocNo));

            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));

            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, DocNo));

            if(mIsNetOffPaymentUseCOA)
            {
                cml.Add(SaveNetOffPaymentDtl3(DocNo));
                cml.Add(SaveNetOffPaymentDtl4(DocNo));
            }

            cml.Add(UpdateSalesInvoiceProcessInd(DocNo));
            cml.Add(UpdateSalesReturnInvoiceProcessInd(DocNo));
            cml.Add(UpdatePurchaseInvoiceHdr(DocNo, 1));
            cml.Add(UpdatePurchaseReturnInvoiceHdr(DocNo, 1));

            Sm.ExecCommands(cml);
            ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueNetOffCode, "Net Off Name") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document of sales invoice.");
                return true;
            }
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document of purchase invoice.");
                return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            return false;
        }

        private bool IsSalesReturnInvoiceAlreadyCancelled(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 5) != "2") return false;

            string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            return Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                DocNo,
                "Sales Return Invoice# : " + DocNo + Environment.NewLine +
                "This document already cancelled."
                );
        }

        private bool IsSalesReturnInvoiceAlreadyFulfilled(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 5) != "2") return false;

            string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            return Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr " +
                "Where CancelInd='F' And DocNo=@Param;",
                DocNo,
                "Sales Return Invoice# : " + DocNo + Environment.NewLine +
                "This document already fulfilled."
                );
        }

        private bool IsSalesInvoiceAlreadyCancelled(string DocNo)
        {
            return Sm.IsDataExist(
                "Select DocNo From TblSalesInvoiceHdr " +
                "Where CancelInd='Y' And DocNo=@Param " +
                "Union ALL " +
                "Select DocNo From TblSalesInvoice2Hdr " +
                "Where CancelInd='Y' And DocNo=@Param ",
                DocNo,
                "Sales Invoice# : " + DocNo + Environment.NewLine +
                "This document already cancelled."
                );
        }

        private bool IsPurchaseInvoiceAlreadyCancelled(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where Find_In_Set(DocNo, @Param) ");
            if (mIsPurchaseInvoiceUseApproval) SQL.AppendLine("    And (CancelInd = 'Y' Or Status = 'C') ");
            else SQL.AppendLine("    And CancelInd = 'Y' ");

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("    Where Find_In_Set(DocNo, @Param) ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            return Sm.IsDataExist(cm, "Purchase Invoice# : " + DocNo + Environment.NewLine +
                "This document already cancelled.");
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Sales Invoice# is empty.") ||
                    IsSalesInvoiceAlreadyCancelled(Sm.GetGrdStr(Grd1, Row, 2)) ||
                    IsSalesReturnInvoiceAlreadyCancelled(Row) ||
                    IsSalesReturnInvoiceAlreadyFulfilled(Row)
                    )
                    return true;
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Purchase Invoice# is empty.") ||
                    IsPurchaseInvoiceAlreadyCancelled(Sm.GetGrdStr(Grd2, Row, 2))
                    )
                    return true;
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, r, 1, false, "COA's account (1) is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, r, 3) == 0m && Sm.GetGrdDec(Grd3, r, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd3, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, r, 2) + Environment.NewLine + Environment.NewLine +
                            "Debit and credit value should not be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, r, 3) != 0m && Sm.GetGrdDec(Grd3, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd3, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd3, r, 2) + Environment.NewLine + Environment.NewLine +
                            "You have to fill in either debit or credit value.");
                        return true;
                    }
                }
            }

            if (Grd4.Rows.Count > 1)
            {
                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, r, 1, false, "COA's account (2) is empty.")) return true;
                    if (Sm.GetGrdDec(Grd4, r, 3) == 0m && Sm.GetGrdDec(Grd4, r, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd4, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd4, r, 2) + Environment.NewLine + Environment.NewLine +
                            "Debit and credit value should not be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd4, r, 3) != 0m && Sm.GetGrdDec(Grd4, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd4, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd4, r, 2) + Environment.NewLine + Environment.NewLine +
                            "You have to fill in either debit or credit value.");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveNetOffPaymentHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Net Off Payment - Hdr */ ");
            SQL.AppendLine("Insert Into TblNetOffPaymentHdr ");
            SQL.AppendLine("(DocNo, DocDt, PartnerEntityCode, DeptCode, CancelInd, Status, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt,");
            SQL.AppendLine("VoucherRequestDocNo, Amt, COAAmt, COAAmt2, Remark,  ");
            SQL.AppendLine("CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DocDt, @PartnerEntityCode, @DeptCode, 'N', 'O', ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@VoucherRequestDocNo, @Amt, @COAAmt, @COAAmt2, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PartnerEntityCode", Sm.GetLue(LueNetOffCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<decimal>(ref cm, "@Amt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<decimal>(ref cm, "@COAAmt", (mIsNetOffPaymentUseCOA ? ComputeCOAAmtSLI() : 0m));
            Sm.CmParam<decimal>(ref cm, "@COAAmt2", (mIsNetOffPaymentUseCOA ? ComputeCOAAmtPI() : 0m));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveNetOffPaymentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Incoming Payment - Dtl (Sales Invoice) */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblNetOffPaymentDtl(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @InvoiceDocno_" + r.ToString() +
                        ", @InvoiceType_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@InvoiceDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@InvoiceType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveNetOffPaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Net Off Payment - Dtl (Purchase Invoice) */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblNetOffPaymentDtl2(DocNo, DNo, InvoiceDocno, InvoiceType, Amt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @InvoiceDocno_" + r.ToString() +
                        ", @InvoiceType_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@InvoiceDocNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@InvoiceType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 11));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 13));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveNetOffPaymentDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Net Off Payment (COASLI) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblNetOffPaymentDtl3(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveNetOffPaymentDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Net Off Payment (COAPI) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblNetOffPaymentDtl4(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 5));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string NetOffPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @DeptCode, @DocType, Null, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @CurCode, @Amt, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            //Save DocApproval
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @NetOffPaymentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='NetOffPayment' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1.00) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0.00)) ");
            SQL.AppendLine("And T.DeptCode=@DeptCode ");
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='NetOffPayment' ");
            SQL.AppendLine("    And DocNo=@NetOffPaymentDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblNetOffPaymentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@NetOffPaymentDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='NetOffPayment' ");
            SQL.AppendLine("    And DocNo=@NetOffPaymentDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@NetOffPaymentDocNo", NetOffPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", "76");
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", "IDR");
            Sm.CmParam<decimal>(ref cm, "@Amt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            //untuk proses tab coa
            //if (mIsIncomingPaymentUseCOA)
            //{
            //    //Sm.CmParam<Decimal>(ref cm, "@COAAmt", decimal.Parse(TxtCOAAmt.Text));
            //    Sm.CmParam<String>(ref cm, "@VoucherRequestDNo", Sm.Right("000" + (Grd1.Rows.Count).ToString(), 3));
            //    Sm.CmParam<String>(ref cm, "@VoucherRequestDescription", Sm.GetGrdStr(Grd1, 0, 11));
            //}

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo, string NetOffPaymentDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Net Off Payment - VoucherRequestDtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @Description, @Amt, @Description, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", LueNetOffCode.Text+"/"+ NetOffPaymentDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSalesInvoiceProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();
            //update sales invoice
            SQL.AppendLine("Update TblSalesInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt2 ");
            SQL.AppendLine("    From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("    Inner Join TblNetOffPaymentDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.InvoiceType='1' ");
            SQL.AppendLine("        And T2.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T2.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.ProcessInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblNetOffPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("    ); ");

            //update salesinvoice 2
            SQL.AppendLine("Update TblSalesInvoice2Hdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt2 ");
            SQL.AppendLine("    From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("    Inner Join TblNetOffPaymentDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.InvoiceType='3' ");
            SQL.AppendLine("        And T2.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T2.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.ProcessInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblNetOffPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='3' ");
            SQL.AppendLine("    ); ");

            //Update sales invoice 5
            SQL.AppendLine("Update TblSalesInvoice5Hdr A  ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt2  ");
            SQL.AppendLine("    From TblNetOffPaymentHdr T1  ");
            SQL.AppendLine("    Inner Join TblNetOffPaymentDtl T2  ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("        And T2.InvoiceType='5'  ");
            SQL.AppendLine("        And T2.DocNo=@DocNo  ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C'  ");
            SQL.AppendLine("    Group By T2.InvoiceDocNo  ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("Set A.ProcessInd =  ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then  ");
            SQL.AppendLine("        If(A.Amt = 0, 'F', 'O')  ");
            SQL.AppendLine("    Else  ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.Amt, 'F', 'P')  ");
            SQL.AppendLine("    End  ");
            SQL.AppendLine("Where A.DocNo In (  ");
            SQL.AppendLine("    Select InvoiceDocNo From TblNetOffPaymentDtl  ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='5'  ");
            SQL.AppendLine("    );  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdateSalesReturnInvoiceProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt2 ");
            SQL.AppendLine("    From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("    Inner Join TblNetOffPaymentDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.InvoiceType='2' ");
            SQL.AppendLine("        And T2.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("    Group By T2.InvoiceDocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Set A.IncomingPaymentInd = ");
            SQL.AppendLine("    Case When IfNull(B.Amt2, 0) = 0 Then ");
            SQL.AppendLine("        If(A.TotalAmt = 0, 'F', 'O') ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If(IfNull(B.Amt2, 0)>=A.TotalAmt, 'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblNetOffPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='2' ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdatePurchaseInvoiceHdr(string DocNo, byte Type)
        {
            // Type = 1 => Insert
            // Type = 2 => Cancel
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseInvoiceHdr X1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, ");
            if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
                SQL.AppendLine("    (A.Amt+A.TaxAmt) As Amt1, ");
            else
                SQL.AppendLine("    (A.Amt+A.TaxAmt-A.DownPayment) As Amt1, ");
            SQL.AppendLine("    IfNull(B.Amt, 0.00) As Amt2 ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T.PurchaseInvoiceDocNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T3 ");
            SQL.AppendLine("                On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("                And T3.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='1') ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("            Union All ");
            SQL.AppendLine("            Select T1.PurchaseInvoiceDocNo, T1.Amt ");
            SQL.AppendLine("            From TblApsHdr T1 ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T2 ");
            SQL.AppendLine("                On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='1') ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            Union All ");
            SQL.AppendLine("            Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
            SQL.AppendLine("            From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblNetOffPaymentDtl2 T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T3 ");
            SQL.AppendLine("                On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("                And T3.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='1') ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        ) T Group By T.PurchaseInvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.PurchaseInvoiceDocNo ");
            SQL.AppendLine("    Where A.DocNo In ( ");
            SQL.AppendLine("        Select InvoiceDocNo From TblOutgoingPaymentDtl ");
            SQL.AppendLine("        Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("Set X1.ProcessInd = ");
            SQL.AppendLine("    Case When IfNull(X2.Amt2, 0.00) = 0.00 Then ");
            SQL.AppendLine("        Case When X2.Amt1 = 0.00 Then ");
            if (Type == 1)
                SQL.AppendLine("            'F' ");
            else
                SQL.AppendLine("            'O' ");
            SQL.AppendLine("        Else 'O' End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(X2.Amt2, 0.00)>=X2.Amt1 Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where X1.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblNetOffPaymentDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='1' ");
            SQL.AppendLine("    ); ");

            //if (Type != 1)
            //{
            //    SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            //    SQL.AppendLine("    ProcessInd='O' ");
            //    SQL.AppendLine("Where DocNo Not In ( ");
            //    SQL.AppendLine("    Select B.InvoiceDocNo ");
            //    SQL.AppendLine("    From TblNetOffPaymentHdr A, TblNetOffPaymentDtl B ");
            //    SQL.AppendLine("    Where A.DocNo=@DocNo ");
            //    SQL.AppendLine("    And B.InvoiceType='1' ");
            //    SQL.AppendLine("    And A.CancelInd='N' ");
            //    SQL.AppendLine("    And A.Status In ('O', 'A') ");
            //    SQL.AppendLine("    And A.DocNo=B.DocNo ");
            //    SQL.AppendLine(") ");
            //    if (mIsPITotalWithoutTaxInclDownpaymentEnabled)
            //        SQL.AppendLine("And (Amt+TaxAmt)=0.00;");
            //    else
            //        SQL.AppendLine("And (Amt+TaxAmt-DownPayment)=0.00;");
            //}

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdatePurchaseReturnInvoiceHdr(string DocNo, byte Type)
        {
            // Type = 1 => Insert
            // Type = 2 => Cancel

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPurchaseReturnInvoiceHdr X1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.Amt As Amt1, IfNull(B.Amt, 0)+IfNull(C.Amt, 0) As Amt2 ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceHdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And T3.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='2') ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt ");
            SQL.AppendLine("        From TblNetOffPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentDtl2 T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblPurchaseReturnInvoiceHdr T3 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("            And T3.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='2') ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("        Group By T2.InvoiceDocNo ");
            SQL.AppendLine("    )C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    Where A.DocNo In (Select InvoiceDocNo From TblNetOffPaymentDtl2 Where DocNo=@DocNo And InvoiceType='2') ");
            SQL.AppendLine(") X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("Set X1.ProcessInd= ");
            SQL.AppendLine("    Case When X2.Amt2 = 0 Then ");
            SQL.AppendLine("        Case When X2.Amt1 = 0 Then ");
            if (Type == 1)
                SQL.AppendLine("            'F' ");
            else
                SQL.AppendLine("            'O' ");
            SQL.AppendLine("            Else 'O' End ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When X2.Amt2>=X2.Amt1 Then 'F' Else 'P' End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where X1.DocNo In ( ");
            SQL.AppendLine("    Select InvoiceDocNo From TblOutgoingPaymentDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And InvoiceType='2'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelNetOffPaymentHdr());
            cml.Add(UpdateSalesInvoiceProcessInd(TxtDocNo.Text));
            cml.Add(UpdateSalesReturnInvoiceProcessInd(TxtDocNo.Text));
            cml.Add(UpdatePurchaseInvoiceHdr(TxtDocNo.Text, 2));
            cml.Add(UpdatePurchaseReturnInvoiceHdr(TxtDocNo.Text, 2));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsDataProcessedAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblNetOffPaymentHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelNetOffPaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNetOffPaymentHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowNetOffPaymentHdr(DocNo);
                ShowNetOffPaymentDtl(DocNo);
                ShowNetOffPaymentDtl2(DocNo);
                ShowNetOffPaymentDtl34(DocNo, "3", ref Grd3);
                ShowNetOffPaymentDtl34(DocNo, "4", ref Grd4);
                ShowDocApproval(DocNo);
                ComputePIAmt();
                ComputeSLIAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowNetOffPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, ");
            SQL.AppendLine("(Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, A.CancelReason ");

            SQL.AppendLine("From TblNetOffPaymentHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "PartnerEntityCode", "DeptCode", "StatusDesc", "CancelInd", 
 
                        //6-10
                        "CancelReason", "Remark", "AcType", "PaymentType", "BankAcCode",   
                        
                        //11-15
                        "BankCode", "GiroNo", "DueDt", "VoucherRequestDocNo", "VoucherDocNo", 

                        //16
                        "Amt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueNetOffCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]);
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[5]) == "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[9]));
                        Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[11]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[12]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[13]));
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]); ;
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                    }, true
                );
        }

        private void ShowNetOffPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, B.DueDt, C.DOCtRemark ");
            SQL.AppendLine("From TblNetOffPaymentDtl A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T3.Remark Order By T2.DNo Separator ' ') As DOCtRemark ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T3 On T2.DOCtDocNo=T3.DocNo And T3.Remark Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo In ( ");
            SQL.AppendLine("        Select Distinct X2.DocNo ");
            SQL.AppendLine("        From TblIncomingPaymentDtl X1 ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr X2 On X1.InvoiceDocNo=X2.DocNo ");
            SQL.AppendLine("        Where X1.InvoiceType='1' ");
            SQL.AppendLine("        And X1.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine("    ) C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='1' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Return Invoice' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, Null As DueDt, Null As DOCtRemark ");
            SQL.AppendLine("From TblNetOffPaymentDtl A ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='2' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo, ");
            SQL.AppendLine("A.InvoiceType, 'Sales Invoice 2' As InvoiceTypeDesc, ");
            SQL.AppendLine("A.Amt, A.Remark, ");
            SQL.AppendLine("B.DocDt, B.CurCode, B.DueDt, Null As DOCtRemark  ");
            SQL.AppendLine("From TblNetOffPaymentDtl A ");
            SQL.AppendLine("Left Join TblSalesInvoice2Hdr B On A.InvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Where A.InvoiceType='3' And A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DNo, A.InvoiceDocNo,  ");
            SQL.AppendLine("A.InvoiceType, 'Sales Project' As InvoiceTypeDesc,  ");
            SQL.AppendLine("A.Amt, A.Remark,  ");
            SQL.AppendLine("B.DocDt, B.CurCode, Null As DueDt, Null As DOCtRemark  ");
            SQL.AppendLine("From TblNetOffPaymentDtl A  ");
            SQL.AppendLine("Left Join TblSalesReturnInvoiceHdr B On A.InvoiceDocNo=B.DocNo  ");
            SQL.AppendLine("Where A.InvoiceType='5' And A.DocNo=@DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "InvoiceDocNo", "DocDt", "InvoiceType", "InvoiceTypeDesc", "CurCode", 
                    
                    //6-9
                    "Amt", "DueDt", "Remark", "DOCtRemark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Grd.Cells[Row, 8].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowNetOffPaymentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DNo, C.InvoiceDocNo, ");
            SQL.AppendLine("Case C.InvoiceType When '1' Then E.DocDt Else F.DocDt End As InvoiceDt, ");
            SQL.AppendLine("C.InvoiceType, ");
            SQL.AppendLine("Case C.InvoiceType When '1' Then 'Purchase Invoice' Else 'Purchase Return Invoice' End As InvoiceTypeDesc, ");
            SQL.AppendLine("B.VdCode, D.VdName, ");
            SQL.AppendLine("Case C.InvoiceType When '1' Then E.CurCode Else F.CurCode End As CurCode, ");
            SQL.AppendLine("C.Amt, ");
            SQL.AppendLine("Case C.InvoiceType When '1' Then E.DueDt Else Null End As DueDt, ");
            SQL.AppendLine("Case C.InvoiceType When '1' Then E.VdInvNo Else Null End As VdInvNo, ");
            SQL.AppendLine("C.Remark, IfNull(A.DeptCode, IfNull(E.DeptCode, F.DeptCode)) DeptCode ");
            SQL.AppendLine("From TblNetOffPaymentHdr A ");
            SQL.AppendLine("Inner Join TblPartnerEntity B On A.PartnerEntityCode = B.PartnerEntityCode ");
            SQL.AppendLine("Inner Join TblNetOffPaymentDtl2 C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblVendor D On B.VdCode=D.VdCode ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr E On C.InvoiceDocNo=E.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr F On C.InvoiceDocNo=F.DocNo And C.InvoiceType='2' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By C.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "InvoiceDocNo", "InvoiceDt", "InvoiceType", "InvoiceTypeDesc", "VdCode", 

                    //6-10
                    "VdName", "CurCode", "Amt", "DueDt", "VdInvNo", 

                    //11-15
                    "Remark", "DeptCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 10, 11 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowNetOffPaymentDtl34(string DocNo, string Tbl, ref iGrid Grd34)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
            SQL.AppendLine("From TblNetOffPaymentDtl" + Tbl + " A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd34, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd34, Grd34.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd34, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='NetOffPayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        #endregion

        #region Grd Event

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmNetOffPaymentDlg2(this, mCtCode));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                var DocTypeSLI = Sm.GetValue("Select DocType From TblSalesInvoiceDtl Where DocNo=@Param Limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                if (DocTypeSLI == "1")
                {
                    var f1 = new FrmSalesInvoice("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f1 = new FrmSalesInvoice3("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
            }

            if (BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0 &&
                Sm.IsGrdColSelected(new int[] { 9, 11 }, e.ColIndex))
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeSLIAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 9 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 11 }, e);
            if (e.ColIndex == 9)
                ComputeSLIAmt();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
                Sm.FormShowDialog(new FrmNetOffPaymentDlg2(this, mCtCode));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var DocTypeSLI = Sm.GetValue("Select DocType From TblSalesInvoiceDtl Where DocNo=@Param Limit 1;", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                    if (DocTypeSLI == "1")
                    {
                        var f1 = new FrmSalesInvoice("***");
                        f1.Tag = "***";
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                    else
                    {
                        var f1 = new FrmSalesInvoice3("***");
                        f1.Tag = "***";
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                        f1.ShowDialog();
                    }
                }
            }
        }

        #endregion

        #region Grd2 

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmNetOffPaymentDlg3(this, mVdCode));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice3(mMenuCode);
                    f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0 && Sm.IsGrdColSelected(new int[] { 4, 13 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputePIAmt();
            }
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
                Sm.FormShowDialog(new FrmNetOffPaymentDlg3(this, mVdCode));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice3(mMenuCode);
                    f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 11 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 14 }, e);
            if (e.ColIndex == 11)
                ComputePIAmt();
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                //ComputeCOAAmt(ref Grd3, 1);
                //if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                //ComputeCOAAmt(ref Grd3, 1);
                //if (mIsIncomingPaymentAmtUseCOAAmt) ComputeAmt();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name") && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmNetOffPaymentDlg4(this, Grd3, true, mCtCode, Sm.GetLue(LueBankAcCode)));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name") && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account")) 
                Sm.FormShowDialog(new FrmNetOffPaymentDlg4(this, Grd3, true, mCtCode, Sm.GetLue(LueBankAcCode)));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            //ComputeCOAAmt(ref Grd3, 1);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid4

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 4].Value = 0;
                //ComputeCOAAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length != 0)
            {
                Grd4.Cells[e.RowIndex, 3].Value = 0;
                //ComputeCOAAmt();
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmNetOffPaymentDlg5(this, mVdCode));
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueNetOffCode, "Net Off Name"))
            {
                if (mIsNetOffPaymentAmtUseCOAAmt)
                {
                    if (mVdCode.Length > 0)
                        Sm.FormShowDialog(new FrmNetOffPaymentDlg5(this, mVdCode));
                }
                else
                    Sm.FormShowDialog(new FrmNetOffPaymentDlg5(this, mVdCode));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            //ComputeCOAAmt();
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Additional Method 

        #region Computation 

        internal void ComputeSLIAmt()
        {
            decimal amt = 0m;
            for(int row=0; row < Grd1.Rows.Count-1; row++)
            {
                amt += Sm.GetGrdDec(Grd1, row, 9);
            }

            TxtSLIAmt.EditValue = Sm.FormatNum(amt, 0);
            ComputeAmt();
            SetAcType();
        }

        internal void ComputePIAmt()
        {
            decimal amt = 0m;
            for (int row = 0; row < Grd2.Rows.Count - 1; row++)
            {
                amt += Sm.GetGrdDec(Grd2, row, 11);
            }

            TxtPIAmt.EditValue = Sm.FormatNum(amt, 0);
            ComputeAmt();
            SetAcType();
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;

            Amt = Math.Abs(decimal.Parse(TxtSLIAmt.Text) - decimal.Parse(TxtPIAmt.Text));

            TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private decimal ComputeCOAAmtSLI()
        {
            decimal COAAmt = 0m;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.COAAcNo As AcNo ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode ");
            SQL.AppendLine("Left Join TblBankAccount C On C.BankAcCode = @BankAcCode ");
            SQL.AppendLine("Where A.CtCtCode is Not Null ");
            SQL.AppendLine("And A.CtCode=@CtCode Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            var AcNo = Sm.GetValue(cm);

            string AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd3, Row, 1)))
                {
                    if (Sm.GetGrdDec(Grd3, Row, 3) != 0)
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 3);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0)
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                    }
                }
            }
            return COAAmt;
        }

        private decimal ComputeCOAAmtPI()
        {
            string AcNo = string.Empty, AcType = string.Empty;
            decimal COAAmt = 0m;

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT CONCAT( ");
            SQL.AppendLine("   C.ParValue, A.VdCode ");
            SQL.AppendLine(") AS AcNo ");
            SQL.AppendLine("FROM TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("INNER JOIN TblVendor B ON A.VdCode = B.VdCode ");
            SQL.AppendLine("LEFT JOIN TblParameter C ON C.Parcode = 'VendorAcNoAP' ");
            SQL.AppendLine("WHERE A.VdCode = @VdCode LIMIT 1; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            AcNo = Sm.GetValue(cm);

            AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo=@Param;", AcNo);

            for(int Row=0; Row<Grd1.Rows.Count-1; Row++)
            {
                if (AcNo.Length > 0 && Sm.Find_In_Set(Sm.GetGrdStr(Grd4, Row, 1), AcNo))
                {
                    if (Sm.GetGrdDec(Grd4, Row, 3) != 0)
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd4, Row, 3);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd4, Row, 3);
                    }
                    if (Sm.GetGrdDec(Grd4, Row, 4) != 0)
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd4, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd4, Row, 4);
                    }
                }
            }
            return COAAmt;

        }

        #endregion

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsIncomingPaymentShowDORemark', 'IsOutgoingPaymentApprovalNeedDept', 'IsOutgoingPaymentUsePIWithCOA', 'IsAutoGeneratePurchaseLocalDocNo', ");
            SQL.AppendLine("'IsFilterByDept', 'IsFilterBySite', 'IsOutgoingPaymentFilterByPIDept', 'IsPITotalWithoutTaxInclDownpaymentEnabled', 'IsGroupCOAActived', ");
            SQL.AppendLine("'IsNetOffPaymentAmtUseCOAAmt', 'IsPurchaseInvoiceUseApproval', 'IsNetOffPaymentUseCOA' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsIncomingPaymentShowDORemark": mIsIncomingPaymentShowDORemark = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsIncomingPaymentShowDORemark = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsPITotalWithoutTaxInclDownpaymentEnabled": mIsPITotalWithoutTaxInclDownpaymentEnabled = ParValue == "Y"; break;
                            case "IsOutgoingPaymentFilterByPIDept": mIsOutgoingPaymentFilterByPIDept = ParValue == "Y"; break;
                            case "IsGroupCOAActived": mIsGroupCOAActived = ParValue == "Y"; break;
                            case "IsNetOffPaymentAmtUseCOAAmt": mIsNetOffPaymentAmtUseCOAAmt = ParValue == "Y"; break;
                            case "IsPurchaseInvoiceUseApproval": mIsPurchaseInvoiceUseApproval = ParValue == "Y"; break;
                            case "IsNetOffPaymentUseCOA": mIsNetOffPaymentUseCOA = ParValue == "Y"; break;

                            //string
                        }
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedSalesInvoice()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedPurchaseInvoice()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            Sm.GetGrdStr(Grd2, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                    {
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void SetLueNetOffCode(ref LookUpEdit Lue, string PartnerEntityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PartnerEntityCode as Col1, PartnerEntityName as Col2 ");
            SQL.AppendLine("From TblPartnerEntity ");
            if (PartnerEntityCode.Length > 0) SQL.AppendLine("Where PartnerEntityCode = @Code ");
            SQL.AppendLine("; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", PartnerEntityCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetAcType()
        {
            decimal
                SLIAmt = decimal.Parse(TxtSLIAmt.Text),
                PIAmt = decimal.Parse(TxtPIAmt.Text);

            if (SLIAmt > PIAmt)
                Sm.SetLue(LueAcType, "D");
            else
                Sm.SetLue(LueAcType, "C");
        }

        #endregion

        #region Event 

        private void LueNetOffCode_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueNetOffCode).Length > 0)
            {
                mCtCode = Sm.GetValue("Select CtCode From TblPartnerEntity Where PartnerEntityCode = @Param", Sm.GetLue(LueNetOffCode));
                mVdCode = Sm.GetValue("Select VdCode From TblPartnerEntity Where PartnerEntityCode = @Param", Sm.GetLue(LueNetOffCode));
            }
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion
    }
}
