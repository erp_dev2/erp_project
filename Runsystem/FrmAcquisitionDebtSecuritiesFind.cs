﻿#region Update

/*
 * 11/04/2022 [SET/PRODUCT] Menu find baru
 * 17/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
 * 19/05/2022 [IBL/PRODUCT] Mengganti source InvestmentEquityCode menjadi InvestmentCode
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecuritiesFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAcquisitionDebtSecurities mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecuritiesFind(FrmAcquisitionDebtSecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Investment Code",
                        "Investment Name",
                        "Investmnent Bank Account (RDN)",

                        //6-10
                        "Investment Bank Account Name",
                        "Issuer",
                        "Category",
                        "Type",
                        "Nominal Amount",

                        //11-15
                        "Price",
                        "Investment Cost",
                        "Maturity Date",
                        "Interest Rate",
                        "Interest Frequency",

                        //16-20
                        "Interest Type",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",

                        //21-22
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 50, 150, 150,  

                        //6-10
                        100, 100, 100, 100, 150,

                        //11-15
                        150, 150, 150, 150, 150,
                        
                        //16-20
                        150, 150, 150, 100, 100, 

                        //21-24
                        100, 100, 100, 100,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 10,11, 12, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.SekuritasCode, B.SekuritasName, C.BankAcNo, C.BankAcNm, F.Issuer, G.InvestmentCtName, H.OptDesc InvestmentType, ");
            SQL.AppendLine("A.Amt, A.AcquisitionPrice, A.InvestmentCost, E.MaturityDt, E.InterestRateAmt, E.InterestFreq, E.InterestType, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblacquisitiondebtsecuritieshdr A ");
            SQL.AppendLine("INNER JOIN tblinvestmentsekuritas B ON A.SekuritasCode = B.SekuritasCode ");
            SQL.AppendLine("INNER JOIN tblbankaccount C ON A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblacquisitiondebtsecuritiesdtl D ON A.DocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemdebt E ON A.InvestmentCode = E.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio F ON E.PortofolioId = F.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory G ON F.InvestmentCtCode = G.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tbloption H ON A.InvestmentType = H.OptCode AND H.OptCat = 'InvestmentType' ");

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                //Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                //Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " GROUP BY A.DocNo Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "SekuritasCode", "SekuritasName", "BankAcNo", "BankAcNm",

                            //6-10
                            "Issuer", "InvestmentCtName", "InvestmentType", "Amt", "AcquisitionPrice", 
                            
                            //11-15
                            "InvestmentCost", "MaturityDt", "InterestRateAmt", "InterestFreq", "InterestType", 
                            
                            //16-19
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 22, 19);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }


        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion

    }
}
