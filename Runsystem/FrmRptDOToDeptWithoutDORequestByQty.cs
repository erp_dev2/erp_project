﻿#region Update
/*
    // 17/09/2017 [TKG] tambah informasi warehouse dan filter berdasarkan warehouse      
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDOToDeptWithoutDORequestByQty : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        internal bool mIsShowForeignName = false, mIsReportDoDeptShowValue = false;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptDOToDeptWithoutDORequestByQty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsReportDoDeptShowValue = Sm.GetParameterBoo("IsReportDoDeptShowValue");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, D.ItName, D.ForeignName, I.AssetName, I.DisplayName, E.DeptName, G.CCName, F.EmpName, ");
            SQL.AppendLine("A.DocNo, A.DocDt, C.WhsName, B.Lot, B.Bin, H.CurCode, H.UPrice, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, ");
            SQL.AppendLine("Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblEmployee F On B.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left Join TblCostCenter G On A.CCCode = G.CCCode ");
            SQL.AppendLine("Left Join TblStockPrice H On B.Source=H.Source ");
            SQL.AppendLine("Left Join TblAsset I On B.AssetCode=I.AssetCode ");
            SQL.AppendLine("Where A.DORequestDeptDocNo Is Null  ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.ItCode, D.ItName, D.ForeignName, E.DeptName, G.CCName, I.AssetName, I.DisplayName, F.EmpName, ");
            SQL.AppendLine("A.DocNo, A.DocDt, C.WhsName, B.Lot, B.Bin, H.CurCode, H.UPrice, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3 ");
            SQL.AppendLine("Order By ItName, DeptName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5  
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Foreign Name",
                        "Asset",
                        "Display Name",

                        //6-10
                        "Department",
                        "Cost Center",
                        "Requested By",
                        "Document#",
                        "Date",

                        //11-15
                        "Warehouse",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        
                        //16-20
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Percentage",
                        
                        //21-23
                        "Currency",
                        "Price",
                        "Value"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 250, 200, 250, 250, 
                        
                        //6-10
                        200, 200, 200, 130, 80,   
                        
                        //11-15
                        200, 60, 60, 100, 80, 
                        
                        //16-20
                        100, 80, 100, 80, 100, 

                        //21-23
                        80, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 20, 22, 23 }, 0);
            Sm.GrdColPercentBar(Grd1, new int[] { 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12, 13 }, false);
            if (!mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            if (!mIsReportDoDeptShowValue) Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                decimal TotalAmt = 0m;
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName", "D.ForeignName" });

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 16, 18, 20, 23 });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "ItCode",   
                            
                            //1-5
                            "ItName", "ForeignName", "AssetName", "DisplayName", "DeptName", 
                            
                            //6-10
                            "CCName", "EmpName", "DocNo", "DocDt", "WhsName",  
                            
                            //11-15
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", "UPrice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Grd.Cells[Row, 23].Value = Sm.GetGrdDec(Grd1, Row, 14) * Sm.GetGrdDec(Grd1, Row, 22);
                            TotalAmt = TotalAmt + Sm.GetGrdDec(Grd1, Row, 14); 
                        }, true, false, false, false
                    );
                if (TotalAmt != 0m)
                {
                    for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                        Grd1.Cells[intX, 20].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 14) / TotalAmt);
                }
                else
                {
                    for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                        Grd1.Cells[intX, 20].Value = 0m;
                }
                Grd1.GroupObject.Add(6);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            Sm.GrdCustomDrawCellForeground(Grd1, new int[] { 20 }, sender, e);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion   

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #endregion
    }
}
