﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

namespace RunSystem
{
    public partial class FrmPosProduct : Form
    {
        private string mMenuCode = "";
        private bool InsertRights = false, EditRights = false, DeleteRights = false;

        #region Constructor
        public FrmPosProduct(string MenuCode)
		{
			InitializeComponent();
			mMenuCode = MenuCode;
        }
        #endregion

        private void FrmPosProduct_Load(object sender, EventArgs e)
        {
            InitControl();
            Sm.SetFormUserRights(Gv.CurrentUserCode, mMenuCode, ref InsertRights, ref EditRights, ref DeleteRights);
            SetFormControl("View");

        }

        private void SetFormControl(string Type)
        {
            Sm.SetFrmButtonEnabled(Type,
                ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel,
                InsertRights, EditRights, DeleteRights);
            switch (Type)
            {
                case "View":
                    Sm.SetControlReadOnly(LueSupplierCode, true);
                    Sm.SetControlReadOnly(LueGroup1, true);
                    Sm.SetControlReadOnly(LueGroup2, true);
                    Sm.SetControlReadOnly(LueGroup3, true);
                    Sm.SetControlReadOnly(LueGroup4, true);
                    Sm.SetControlReadOnly(TxtPdName, true);
                    Sm.SetControlReadOnly(TxtWeight, true);
                    Sm.SetControlReadOnly(TxtMinPrice, true);
                    Sm.SetControlReadOnly(TxtUnitPrice, true);
                    Sm.SetControlReadOnly(TxtPdCode, true);
                    Sm.SetControlReadOnly(TxtBarCode, true);
                    Sm.SetControlReadOnly(TxtLocNo, true);
                    break;
                case "Insert":
                    Sm.SetControlReadOnly(LueSupplierCode, false);
                    Sm.SetControlReadOnly(LueGroup1, false);
                    Sm.SetControlReadOnly(LueGroup2, false);
                    Sm.SetControlReadOnly(LueGroup3, false);
                    Sm.SetControlReadOnly(LueGroup4, false);
                    Sm.SetControlReadOnly(TxtPdName, false);
                    Sm.SetControlReadOnly(TxtWeight, false);
                    Sm.SetControlReadOnly(TxtMinPrice, false);
                    Sm.SetControlReadOnly(TxtUnitPrice, false);
                    Sm.SetControlReadOnly(TxtPdCode, true);
                    Sm.SetControlReadOnly(TxtBarCode, true);
                    Sm.SetControlReadOnly(TxtLocNo, true);
                    break;
                case "Edit":
                    Sm.SetControlReadOnly(LueSupplierCode, false);
                    Sm.SetControlReadOnly(LueGroup1, false);
                    Sm.SetControlReadOnly(LueGroup2, false);
                    Sm.SetControlReadOnly(LueGroup3, false);
                    Sm.SetControlReadOnly(LueGroup4, false);
                    Sm.SetControlReadOnly(TxtPdName, false);
                    Sm.SetControlReadOnly(TxtWeight, false);
                    Sm.SetControlReadOnly(TxtMinPrice, false);
                    Sm.SetControlReadOnly(TxtUnitPrice, false);
                    Sm.SetControlReadOnly(TxtPdCode, true);
                    Sm.SetControlReadOnly(TxtBarCode, true);
                    Sm.SetControlReadOnly(TxtLocNo, true);
                    break;
                default:
                    break;
            }
        }

        private void InitControl()
        {
            Sl.SetLueVdCode(ref LueSupplierCode);
            //Sl.SetLuePosGroup(ref LueGroup1, "1");
            //Sl.SetLuePosGroup(ref LueGroup2, "2");
            //Sl.SetLuePosGroup(ref LueGroup3, "3");
            //Sl.SetLuePosGroup(ref LueGroup4, "4");
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            SetFormControl("Insert");
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void GenerateProductCode()
        {
            if ((Sm.GetLue(LueGroup1) != "") && (Sm.GetLue(LueSupplierCode) != ""))
            {
                string ProductHeader = Sm.GetLue(LueGroup1) + Sm.GetLue(LueSupplierCode) + Sm.ServerCurrentDateTime().Substring(2, 2);
                if (Sm.GetLue(LueSupplierCode) == "00") ProductHeader = Sm.GetLue(LueGroup1) + Sm.GetLue(LueSupplierCode); 
                string LastNumber = Sm.GetValue("SELECT TOP 1 RIGHT(PdCode, 6) As PdCode FROM TblPosPrice where LEFT(PdCode, LEN('" + ProductHeader + "'))='" + ProductHeader + "' ORDER BY PdCode DESC");
                if (LastNumber == "") LastNumber = "000001";
                else
                {
                    LastNumber = "00000" + (Decimal.Parse(LastNumber) + 1).ToString();
                    LastNumber = LastNumber.Substring(LastNumber.Length - 6, 6);
                }
                TxtPdCode.Text = ProductHeader + LastNumber;
            }
        }

        private void LueSupplierCode_EditValueChanged(object sender, EventArgs e)
        {
            GenerateProductCode();
        }

        private void LueGroup1_EditValueChanged(object sender, EventArgs e)
        {
            GenerateProductCode();
        }

    }
}
