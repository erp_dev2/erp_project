﻿#region Update
/*
    19/04/2018 [ARI] tambah entity untuk KMI
    24/09/2019 [WED] tambah alias
    19/05/2020 [WED/YK] dibatasi berdasarkan parameter IsCOAFilteredByGroup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmCOAFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCOA mFrmParent;
        internal bool mIsCOAShowEntity = false;

        #endregion

        #region Constructor

        public FrmCOAFind(FrmCOA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsCOAShowEntity = Sm.GetParameterBoo("IsCOAShowEntity");
                SetGrd();
                SetLueAcCode(ref LueAcCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Account#", 
                        "Description",
                        "Alias",
                        "Active",
                        "Parent",
                        
                        //6-10
                        "Level",
                        "Type",
                        "Existed Entity",
                        "Not Existed Entity",
                        "Created"+Environment.NewLine+"By",                           
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 300, 300, 80, 200, 
                        
                        //6-10
                        60, 60, 250, 250, 100, 

                        //11-15
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 8, 9, 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            if(mIsCOAShowEntity)
            Sm.GrdColInvisible(Grd1, new int[] { 3, 8, 9, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.AcDesc", "A.Alias" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCode), "A.Parent", true);

                if (mIsCOAShowEntity)
                {
                    SQL.AppendLine("Select A.AcNo, A.AcDesc, A.Alias, A.ActInd, E.AcDesc As ParentAcDesc, A.Level, B.OptDesc, C.EntName as Entity1, ");
                    SQL.AppendLine("D.EntName as Entity2, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                    SQL.AppendLine("from TblCOA A ");
                    SQL.AppendLine("Inner Join TblOption B on A.AcType=B.OptCode And B.OptCat='AccountType' ");
                    if (mFrmParent.mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        //SQL.AppendLine("        And AcNo = A.AcNo ");
                        SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("left join ( ");
                    SQL.AppendLine("	select t1.acno, ");
                    SQL.AppendLine("	Group_Concat(Distinct T3.Entname Order By T3.Entname Separator ', ') as EntName ");
                    SQL.AppendLine("	from tblcoa t1 ");
                    SQL.AppendLine("	inner join tblcoadtl t2 on t1.acno=t2.acno ");
                    SQL.AppendLine("	inner join tblentity t3 on t2.entcode=t3.entcode ");
                    SQL.AppendLine("	group by t1.acno ");
                    SQL.AppendLine(") C on A.AcNo=C.AcNo ");
                    SQL.AppendLine("left join ( ");
                    SQL.AppendLine("	select t1.acno, ");
                    SQL.AppendLine("	Group_Concat(Distinct T2.entname Order By T2.entname Separator ', ') as entname ");
                    SQL.AppendLine("	from tblcoa t1 ");
                    SQL.AppendLine("	inner join tblentity t2 on 1=1 ");
                    SQL.AppendLine("	where not exists( ");
                    SQL.AppendLine("		select 1 ");
                    SQL.AppendLine("		from tblcoadtl ");
                    SQL.AppendLine("		where acno=t1.acno ");
                    SQL.AppendLine("		and entcode=t2.entcode ");
                    SQL.AppendLine("		) ");
                    SQL.AppendLine("	group by t1.acno ");
                    SQL.AppendLine(") D on A.AcNo=D.AcNo ");
                    SQL.AppendLine("Left Join TblCOA E on A.Parent=E.AcNo ");
                    SQL.AppendLine(Filter + "Order By A.AcNo;");
                }
                else
                {
                    SQL.AppendLine("Select A.AcNo, A.AcDesc, A.Alias, A.ActInd, C.AcDesc As ParentAcDesc, ");
                    SQL.AppendLine("A.Level, B.OptDesc, Null as Entity1, Null As Entity2, ");
                    SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                    SQL.AppendLine("From TblCOA A ");
                    SQL.AppendLine("Inner Join TblOption B on A.AcType=B.OptCode And B.OptCat='AccountType' ");
                    if (mFrmParent.mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        //SQL.AppendLine("        And AcNo = A.AcNo ");
                        SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("Left Join TblCOA C on A.Parent=C.AcNo ");
                    SQL.AppendLine(Filter + "Order By A.AcNo;");
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "AcNo",

                        //1-5
                        "AcDesc",
                        "Alias",
                        "ActInd",
                        "ParentAcDesc",
                        "Level",
                        
                        //6-10
                        "OptDesc", 
                        "Entity1",
                        "Entity2",
                        "CreateBy",
                        "CreateDt",  

                        //11-12
                        "LastUpBy",
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        public static void SetLueAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo As Col1, ");
            SQL.AppendLine("If(Alias is NULL, AcDesc, CONCAT(AcDesc, ' [', Alias, ']')) AS Col2 ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Level = 1 ");
            SQL.AppendLine("Order By AcNo; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #region Grid Method
        
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void ChkAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account's Group");
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(SetLueAcCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
