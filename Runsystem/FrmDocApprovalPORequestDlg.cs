﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalPORequestDlg : RunSystem.FrmBase6
    {
        #region Field

        private string mSQL = string.Empty;
        internal string mItCode = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalPORequestDlg(string VdCode, string ItCode)
        {
            InitializeComponent();
            mItCode = ItCode;
            mVdCode = VdCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "Vendor Quotation History";
            SetGrd();
            SetSQL();
            ShowData();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Vendor"+Environment.NewLine+"Code",
                        "Vendor"+Environment.NewLine+"Name", 
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "Term Of "+Environment.NewLine+" Payment",
                        //6-8
                        "Currency", 
                        "Unit "+Environment.NewLine+" Price",
                        "Quotation "+Environment.NewLine+" Date",
                        "Quotation "+Environment.NewLine+" Number"
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.VdCode, C.VdName,  B.ItCode, D.ItName, E.PtName, A.CurCode, B.UPrice, "); 
            SQL.AppendLine("A.DocDt, A.DocNo ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode And C.ActInd='Y' "); 
            SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm E On A.PtCode= E.PtCode "); 
            SQL.AppendLine("Where A.VdCode=@VdCode And B.ItCode = @ItCode ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@ItCode", mItCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc",
                        new string[] 
                        { 
                            //0
                            "VdCode",
                            
                            //1-5
                            "VdName", "ItCode", "ItName", "PtName", "CurCode",  
                            
                            //6-8
                            "UPrice", "DocDt", "DocNo", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion
        #endregion
    }
}
