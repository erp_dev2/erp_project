﻿#region Update
/*
    09/06/2017 [WED] tambah source dari Propose Candidate.
    07/08/2017 [HAR] tambah informasi entity.
    04/09/2017 [TKG] Data employee yg muncul tanpa perlu ada data PPS-nya.
    30/10/2017 [TKG] otorisasi department dan site berdasarkan group
    31/03/2018 [TKG] filter by level
    18/05/2018 [TKG] bug saat filter site
    11/12/2019 [WED/IMS] tambah informasi Contract Date, berdasarkan parameter IsEmpContractDtMandatory
    23/07/2020 [IBL/SIER] ambil informasi Acting Official (PLT) 
    24/07/2020 [WED/SIER] ambil informasi Section dari master employee, berdasarkan parameter IsPPSUseSection
    22/11/2020 [TKG/PHT] tambah level
    30/09/2021 [ICA/PHT] bug source levelcode index salah
    16/11/2021 [IBL/SIER] ActingOfficialInd ambil dari master employee. Berdasarkan parameter ActingOfficialIndSource
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPSDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPPS mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPPSDlg(FrmPPS FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Employee";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("Select 'Master Employee' As DataSource, ");
            SQL.AppendLine("Null As PRCDocNo, Null As PRCDNo, ");
            SQL.AppendLine("A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.DeptCode Else B.DeptCode End As DeptCode, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then N.DeptName Else C.DeptName End As DeptName, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.PosCode Else B.PosCode End As PosCode, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then O.PosName Else D.PosName End As PosName, ");
            SQL.AppendLine("E.OptDesc as Gender, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.GrdLvlCode Else B.GrdLvlCode End As GrdLvlCode, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then P.GrdLvlName Else F.GrdLvlName End As GrdLvlName, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.Employmentstatus Else B.Employmentstatus End As Employmentstatus, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then R.OptDesc Else H.OptDesc End As EmpStat, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.SystemType Else B.SystemType End As SystemType, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then Q.OptDesc Else G.OptDesc End As SysType, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.PayrunPeriod Else B.PayrunPeriod End As PayrunPeriod, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then S.OptDesc Else I.OptDesc End As PayPer, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.PGCode Else B.PGCode End As PGCode, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then T.PGName Else J.PGName End As PGName, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then A.SiteCode Else B.SiteCode End As SiteCode, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then U.SiteName Else K.SiteName End As SiteName, ");
            SQL.AppendLine("A.ResignDt, A.ContractDt, ");
            SQL.AppendLine("Case When B.EmpCode Is Null Then W.EntName Else M.EntName End As EntName, ");
            SQL.AppendLine("A.PositionStatusCode, X.PositionStatusName, ");
            if(mFrmParent.mActingOfficialIndSource == "1")
                SQL.AppendLine("Case When B.EmpCode Is Null Then O.ActingOfficialInd Else D.ActingOfficialInd End As ActingOfficialInd, ");
            else
                SQL.AppendLine("A.ActingOfficialInd, ");
            if (mFrmParent.mIsPPSUseSection)
            {
                SQL.AppendLine("Case When B.EmpCode Is Null Then Y.SectionCode Else Z.SectionCode End As SectionCode, ");
                SQL.AppendLine("Case When B.EmpCode Is Null Then Y.SectionName Else Z.SectionName End As SectionName, ");
            }
            else
                SQL.AppendLine("Null As SectionCode, Null As SectionName, ");
            if (mFrmParent.mIsPPSUseLevel)
            {
                SQL.AppendLine("Case When B.EmpCode Is Null Then AA.LevelCode Else AB.LevelCode End As LevelCode, ");
                SQL.AppendLine("Case When B.EmpCode Is Null Then AA.LevelName Else AB.LevelName End As LevelName ");
            }
            else
                SQL.AppendLine("Null As LevelCode, Null As LevelName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblEmployeePPS B On A.EmpCode=B.EmpCode And B.EndDt Is Null ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And (B.DeptCode Is Null Or ");
                SQL.AppendLine("(B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (B.SiteCode Is Null Or ");
                SQL.AppendLine("(B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblOption E On A.Gender=E.OptCode and E.OptCat='Gender' "); 
            SQL.AppendLine("left Join TblGradeLevelHdr F On B.GrdlvlCode = F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption G On B.SystemType=G.OptCode and G.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblOption H On B.EmploymentStatus=H.OptCode and H.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption I On B.PayrunPeriod=I.OptCode and I.OptCat='PayrunPeriod' "); 
            SQL.AppendLine("left Join TblPayrollGrpHdr J On B.PGCode = J.PGCode ");
            SQL.AppendLine("Left Join TblSite K On B.SiteCode = K.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter L On K.ProfitCenterCode = L.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity M On L.EntCode = M.EntCode ");
            SQL.AppendLine("Left Join TblDepartment N On A.DeptCode=N.DeptCode ");
            SQL.AppendLine("Left Join TblPosition O On A.PosCode=O.PosCode ");
            SQL.AppendLine("left Join TblGradeLevelHdr P On A.GrdlvlCode=P.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption Q On A.SystemType=Q.OptCode and Q.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblOption R On A.EmploymentStatus=R.OptCode and R.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption S On A.PayrunPeriod=S.OptCode and S.OptCat='PayrunPeriod' ");
            SQL.AppendLine("left Join TblPayrollGrpHdr T On A.PGCode=T.PGCode ");
            SQL.AppendLine("Left Join TblSite U On A.SiteCode=U.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter V On U.ProfitCenterCode=V.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity W On V.EntCode=W.EntCode ");
            SQL.AppendLine("Left Join TblPositionStatus X On A.PositionStatusCode=X.PositionStatusCode ");
            if (mFrmParent.mIsPPSUseSection)
            {
                SQL.AppendLine("Left Join TblSection Y On A.SectionCode = Y.SectionCode ");
                SQL.AppendLine("Left Join TblSection Z On B.SectionCode = Z.SectionCode ");
            }
            if (mFrmParent.mIsPPSUseLevel)
            {
                SQL.AppendLine("Left Join TblLevelHdr AA On A.LevelCode=AA.LevelCode ");
                SQL.AppendLine("Left Join TblLevelHdr AB On B.LevelCode=AB.LevelCode ");
            }
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=(Select concat(replace(curdate(), '-', '')))) Or A.ResignDt Is Null) ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ");
                SQL.AppendLine("(A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or (");
                SQL.AppendLine("A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine("And A.GrdLvlCode Is Not Null ");
                SQL.AppendLine("And A.GrdLvlCode In ( ");
                SQL.AppendLine("    Select X.GrdLvlCode From TblGradeLevelHdr X ");
                SQL.AppendLine("    Where X.LevelCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where LevelCode=X.LevelCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'Propose Candidate' As DataSource, T1.DocNo As PRCDocNo, T1.DNo As PRCDNo, T1.EmpCode, T2.EmpCodeOld, T2.EmpName, T3.DeptCode, T4.DeptName, T3.PosCode, T5.PosName, ");
            SQL.AppendLine("T6.OptDesc as Gender, T3.GrdLvlCode, T7.GrdLvlName, T3.Employmentstatus, T3.Systemtype, T3.PayrunPeriod, ");
            SQL.AppendLine("T9.OptDesc As EmpStat, T8.OptDesc As SysType, T10.OptDesc As PayPer, T3.PGCode, T11.PGName, T3.SiteCode, T12.SiteName, T2.ResignDt, T2.ContractDt, T15.EntName, ");
            SQL.AppendLine("T2.PositionStatusCode, T16.PositionStatusName, T5.ActingOfficialInd, ");
            if (mFrmParent.mIsPPSUseSection)
                SQL.AppendLine("T17.SectionCode, T17.SectionName, ");
            else
                SQL.AppendLine("Null As SectionCode, Null As SectionName, ");
            if (mFrmParent.mIsPPSUseLevel)
                SQL.AppendLine("T18.LevelCode, T18.LevelName ");
            else
                SQL.AppendLine("Null As LevelCode, Null As LevelName ");
            SQL.AppendLine("From TblProposeCandidateDtl2 T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And (T2.DeptCode Is Null Or ");
                SQL.AppendLine("(T2.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(T2.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine("And T2.GrdLvlCode Is Not Null ");
                SQL.AppendLine("And T2.GrdLvlCode In ( ");
                SQL.AppendLine("    Select X.GrdLvlCode From TblGradeLevelHdr X ");
                SQL.AppendLine("    Where X.LevelCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where LevelCode=X.LevelCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblEmployeePPS T3 On T2.EmpCode = T3.EmpCode And T3.EndDt Is Null ");
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And (T3.SiteCode Is Null Or ");
                SQL.AppendLine("(T3.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(T3.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Left Join TblDepartment T4 On T2.DeptCode = T4.DeptCode ");
            SQL.AppendLine("Left Join TblPosition T5 On T2.PosCode = T5.PosCode ");
            SQL.AppendLine("Left Join TblOption T6 On T2.Gender = T6.OptCode And T6.OptCat = 'Gender' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr T7 On T3.GrdLvlCode = T7.GrdLvlCode ");
            SQL.AppendLine("Left Join (Select OptCode, optDesc From  Tbloption Where OptCat = 'EmpSystemType') T8 On T3.SystemType = T8.OptCode ");
            SQL.AppendLine("Left Join (Select OptCode, optDesc From  Tbloption Where OptCat = 'EmploymentStatus') T9 On T3.EmploymentStatus = T9.OptCode ");
            SQL.AppendLine("Left Join (Select OptCode, optDesc From  Tbloption Where OptCat = 'PayrunPeriod') T10 On T3.PayrunPeriod = T10.OptCode ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr T11 On T3.PGCode = T11.PGCode ");
            SQL.AppendLine("Left Join TblSite T12 On T3.SiteCode = T12.SiteCode ");
            SQL.AppendLine("Inner Join TblProposeCandidateHdr T13 On T1.DocNo = T13.DocNo ");
            SQL.AppendLine("Left Join TblProfitCenter T14 On T12.ProfitCenterCode = T14.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity T15 On T14.EntCode = T15.EntCode ");
            SQL.AppendLine("Left Join TblPositionStatus T16 On T2.PositionStatusCode=T16.PositionStatusCode ");
            if (mFrmParent.mIsPPSUseSection)
                SQL.AppendLine("Left Join TblSection T17 On T2.SectionCode = T17.SectionCode ");
            if (mFrmParent.mIsPPSUseLevel)
                SQL.AppendLine("Left Join TblLevelHdr T18 On T2.LevelCode = T18.LevelCode ");
            SQL.AppendLine("Where T13.CancelInd = 'N' And T13.ProcessInd = 'O' And T1.PPSDocNo Is Null And ((T2.ResignDt Is Not Null And T2.ResignDt>=(Select concat(replace(curdate(), '-', '')))) Or T2.ResignDt Is Null) ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=T1.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                     //0
                    "No.",

                    //1-5
                    "Employee's"+Environment.NewLine+"Code", 
                    "Old Code",
                    "Employee's Name",
                    "Dept Code",
                    "Department",

                    //6-10
                    "PosCode",
                    "Position",
                    "Gender",
                    "GrdLvlCode",
                    "Grade Level",

                    //11-15
                    "EmpStat",
                    "Employment"+Environment.NewLine+"Status",
                    "SysType",
                    "System Type",
                    "PayPeriod",

                    //16-20
                    "Payrun"+Environment.NewLine+"Period",
                    "Payroll"+Environment.NewLine+"Group Code",
                    "Payroll"+Environment.NewLine+"Group",
                    "Site Code",
                    "Site",

                    //21-25
                    "Entity",
                    "Resign Date",
                    "Source",
                    "PRCDocNo",
                    "PRCDNo",

                    //26-30
                    "Position Status Code",
                    "Position Status Name",
                    "Contract Date",
                    "ActingOfficialInd",
                    "Section Code",

                    //31-33
                    "Section",
                    "Level Code",
                    "Level"
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 6, 9, 11, 13, 15, 17, 19, 20, 21, 23, 24, 25, 26, 29, 30, 32 }, false);
            if (!mFrmParent.mIsPPSUseSection)
                Sm.GrdColInvisible(Grd1, new int[] { 31 });
            if (!mFrmParent.mIsPPSUseLevel)
                Sm.GrdColInvisible(Grd1, new int[] { 33 });
            else
                Grd1.Cols[33].Move(21);
            Sm.GrdFormatDate(Grd1, new int[] { 22, 28 });
            if (!mFrmParent.mIsEmpContractDtMandatory) Sm.GrdColInvisible(Grd1, new int[] { 28 });
            Grd1.Cols[23].Move(1);
            if (mFrmParent.mIsFilterBySiteHR)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 20 }, true);
                Sm.GrdColInvisible(Grd1, new int[] { 21 }, true);
                Grd1.Cols[20].Move(5);
                Grd1.Cols[21].Move(6);
            }
            Grd1.Cols[27].Move(9);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 23 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.EmpCode", "T.EmpName", "T.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By T.EmpName;",
                    new string[] 
                    { 
                        //0
                        "EmpCode", 
                        //1-5
                        "EmpCodeOld", "EmpName", "DeptCode", "DeptName", "PosCode",
                        //6-10
                         "PosName", "Gender", "GrdLvlCode", "GrdLvlName", "Employmentstatus",   
                        //11-15
                        "EmpStat","Systemtype", "SysType", "PayrunPeriod", "PayPer",
                        //16-20
                        "PGCode", "PGName", "SiteCode", "SiteName", "EntName", 
                        //21-25
                        "ResignDt", "DataSource", "PRCDocNo", "PRCDNo", "PositionStatusCode",
                        //26-30
                        "PositionStatusName", "ContractDt", "ActingOfficialInd", "SectionCode", "SectionName",
                        //31-32
                        "LevelCode", "LevelName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtDeptName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                mFrmParent.TxtPosition.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtEmploymentStatus.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                mFrmParent.TxtSystemType.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);
                mFrmParent.TxtPayrunPeriod.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 16);
                mFrmParent.TxtPGCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 18);
                mFrmParent.TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 20);
                mFrmParent.TxtEntityName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 21);

                mFrmParent.IsFromDlg = true;
                if (mFrmParent.mIsPPSDepartmentBasedOnSite) Sl.SetLueDeptCode(ref mFrmParent.LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y":"N");
                if (mFrmParent.mIsDepartmentPositionEnabled) Sl.SetLuePosCode(ref mFrmParent.LuePosCode);
                if (mFrmParent.mIsPositionUseLevel) Sl.SetLueLevelCode(ref mFrmParent.LueLevelCode, string.Empty);

                Sm.SetLue(mFrmParent.LueSiteCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19));

                //if (mFrmParent.mIsPPSDepartmentBasedOnSite) Sl.SetLueDeptCodeBasedOnSiteCode(ref mFrmParent.LueDeptCode, string.Empty, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19), mFrmParent.mIsFilterByDeptHR ? "Y" : "N");

                Sm.SetLue(mFrmParent.LueDeptCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4));
                Sm.SetLue(mFrmParent.LuePosCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6));
                Sm.SetLue(mFrmParent.LueGrdLvlCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9));
                Sm.SetLue(mFrmParent.LueEmploymentStatus, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11));
                Sm.SetLue(mFrmParent.LueSystemType, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13));
                Sm.SetLue(mFrmParent.LuePayrunPeriod, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 15));
                Sm.SetLue(mFrmParent.LuePGCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17));

                mFrmParent.TxtGrade.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                mFrmParent.DeptCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.PosCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.GrdLvlCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.EmploymentStatusOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.SystemTypeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 13);
                mFrmParent.PayrunPeriodOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 15);
                mFrmParent.PGCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17);
                mFrmParent.SiteCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19);
                Sm.SetDte(mFrmParent.DteResignDtOld, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 22));
                Sm.SetDte(mFrmParent.DteResignDtNew, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 22));

                mFrmParent.mProposeCandidateDocNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 24);
                mFrmParent.mProposeCandidateDNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 25);

                mFrmParent.mPositionStatusCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 26);
                mFrmParent.TxtPositionStatusCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 27);
                Sm.SetLue(mFrmParent.LuePositionStatusCode, mFrmParent.mPositionStatusCodeOld);
                mFrmParent.ChkActingOfficialOld.Checked = Sm.CompareStr(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 29), "Y");
                mFrmParent.ChkActingOfficialNew.Checked = Sm.CompareStr(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 29), "Y");

                if (mFrmParent.mIsPPSUseSection)
                {
                    mFrmParent.mSectionCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 30);
                    mFrmParent.TxtSectionCodeOld.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 31);
                    mFrmParent.SetLueSection(ref mFrmParent.LueSectionCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 30));
                    Sm.SetLue(mFrmParent.LueSectionCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 30));
                }
                if (mFrmParent.mIsPPSUseLevel)
                {
                    mFrmParent.mLevelCodeOld = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 32);
                    mFrmParent.TxtLevelCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 33);
                    Sl.SetLueLevelCode(ref mFrmParent.LueLevelCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 32));
                }

                mFrmParent.IsFromDlg = false;
                this.Close();
            }

        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }
     
        #endregion

        #endregion

        #region Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
