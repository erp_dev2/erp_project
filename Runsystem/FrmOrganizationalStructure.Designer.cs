﻿namespace RunSystem
{
    partial class FrmOrganizationalStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgFunction = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgJob = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgAuthorized = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgExperience = new System.Windows.Forms.TabPage();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEducation = new System.Windows.Forms.TabPage();
            this.LueMajor = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEducation = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgCompetence = new System.Windows.Forms.TabPage();
            this.LueCompetenceCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPotence = new System.Windows.Forms.TabPage();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgWorkIndicator = new System.Windows.Forms.TabPage();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgSpecification = new System.Windows.Forms.TabPage();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtJobHolderQty = new DevExpress.XtraEditors.TextEdit();
            this.TxtLevel = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPosName2 = new DevExpress.XtraEditors.TextEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtMaxAge = new DevExpress.XtraEditors.TextEdit();
            this.TxtMinAge = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtSoftCompetence = new DevExpress.XtraEditors.TextEdit();
            this.TxtHardCompetence = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.iGrid2 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.iGrid3 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid4 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid5 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid6 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.iGrid7 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.iGrid8 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.iGrid9 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgFunction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgJob.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgAuthorized.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgExperience.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpgEducation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgCompetence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgPotence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpgWorkIndicator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpgSpecification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobHolderQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName2.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxAge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinAge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSoftCompetence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHardCompetence.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid8)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid9)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(809, 0);
            this.panel1.Size = new System.Drawing.Size(70, 399);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.TxtLevel);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtJobHolderQty);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueParent);
            this.panel2.Controls.Add(this.LuePosCode);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(809, 399);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(48, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "Master Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(141, 3);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 12;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 500;
            this.LuePosCode.Size = new System.Drawing.Size(218, 20);
            this.LuePosCode.TabIndex = 10;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(94, 90);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Parent";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(141, 87);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 12;
            this.LueParent.Properties.MaxLength = 16;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 500;
            this.LueParent.Size = new System.Drawing.Size(218, 20);
            this.LueParent.TabIndex = 18;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgFunction);
            this.tabControl1.Controls.Add(this.TpgJob);
            this.tabControl1.Controls.Add(this.TpgAuthorized);
            this.tabControl1.Controls.Add(this.TpgExperience);
            this.tabControl1.Controls.Add(this.TpgEducation);
            this.tabControl1.Controls.Add(this.TpgCompetence);
            this.tabControl1.Controls.Add(this.TpgPotence);
            this.tabControl1.Controls.Add(this.TpgWorkIndicator);
            this.tabControl1.Controls.Add(this.TpgSpecification);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 153);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(809, 246);
            this.tabControl1.TabIndex = 33;
            // 
            // TpgFunction
            // 
            this.TpgFunction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgFunction.Controls.Add(this.Grd1);
            this.TpgFunction.Location = new System.Drawing.Point(4, 26);
            this.TpgFunction.Name = "TpgFunction";
            this.TpgFunction.Size = new System.Drawing.Size(801, 216);
            this.TpgFunction.TabIndex = 3;
            this.TpgFunction.Text = "Function";
            this.TpgFunction.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(801, 216);
            this.Grd1.TabIndex = 34;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TpgJob
            // 
            this.TpgJob.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgJob.Controls.Add(this.Grd2);
            this.TpgJob.Location = new System.Drawing.Point(4, 26);
            this.TpgJob.Name = "TpgJob";
            this.TpgJob.Size = new System.Drawing.Size(764, 50);
            this.TpgJob.TabIndex = 4;
            this.TpgJob.Text = "Job Desc";
            this.TpgJob.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 50);
            this.Grd2.TabIndex = 34;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpgAuthorized
            // 
            this.TpgAuthorized.Controls.Add(this.Grd3);
            this.TpgAuthorized.Location = new System.Drawing.Point(4, 26);
            this.TpgAuthorized.Name = "TpgAuthorized";
            this.TpgAuthorized.Size = new System.Drawing.Size(764, 50);
            this.TpgAuthorized.TabIndex = 7;
            this.TpgAuthorized.Text = "Authorized";
            this.TpgAuthorized.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 50);
            this.Grd3.TabIndex = 34;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgExperience
            // 
            this.TpgExperience.Controls.Add(this.LueLevel);
            this.TpgExperience.Controls.Add(this.Grd5);
            this.TpgExperience.Location = new System.Drawing.Point(4, 26);
            this.TpgExperience.Name = "TpgExperience";
            this.TpgExperience.Padding = new System.Windows.Forms.Padding(3);
            this.TpgExperience.Size = new System.Drawing.Size(764, 50);
            this.TpgExperience.TabIndex = 9;
            this.TpgExperience.Text = "Experience";
            this.TpgExperience.UseVisualStyleBackColor = true;
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(106, 31);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 20;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(171, 20);
            this.LueLevel.TabIndex = 35;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueLevel_EditValueChanged);
            this.LueLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLevel_KeyDown);
            this.LueLevel.Leave += new System.EventHandler(this.LueLevel_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(3, 3);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(758, 44);
            this.Grd5.TabIndex = 34;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpgEducation
            // 
            this.TpgEducation.Controls.Add(this.LueMajor);
            this.TpgEducation.Controls.Add(this.LueEducation);
            this.TpgEducation.Controls.Add(this.Grd6);
            this.TpgEducation.Location = new System.Drawing.Point(4, 26);
            this.TpgEducation.Name = "TpgEducation";
            this.TpgEducation.Padding = new System.Windows.Forms.Padding(3);
            this.TpgEducation.Size = new System.Drawing.Size(764, 50);
            this.TpgEducation.TabIndex = 10;
            this.TpgEducation.Text = "Education";
            this.TpgEducation.UseVisualStyleBackColor = true;
            // 
            // LueMajor
            // 
            this.LueMajor.EnterMoveNextControl = true;
            this.LueMajor.Location = new System.Drawing.Point(414, 43);
            this.LueMajor.Margin = new System.Windows.Forms.Padding(5);
            this.LueMajor.Name = "LueMajor";
            this.LueMajor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.Appearance.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMajor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMajor.Properties.DropDownRows = 20;
            this.LueMajor.Properties.NullText = "[Empty]";
            this.LueMajor.Properties.PopupWidth = 500;
            this.LueMajor.Size = new System.Drawing.Size(171, 20);
            this.LueMajor.TabIndex = 36;
            this.LueMajor.ToolTip = "F4 : Show/hide list";
            this.LueMajor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMajor.EditValueChanged += new System.EventHandler(this.LueMajor_EditValueChanged);
            this.LueMajor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueMajor_KeyDown);
            this.LueMajor.Leave += new System.EventHandler(this.LueMajor_Leave);
            // 
            // LueEducation
            // 
            this.LueEducation.EnterMoveNextControl = true;
            this.LueEducation.Location = new System.Drawing.Point(175, 47);
            this.LueEducation.Margin = new System.Windows.Forms.Padding(5);
            this.LueEducation.Name = "LueEducation";
            this.LueEducation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducation.Properties.Appearance.Options.UseFont = true;
            this.LueEducation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEducation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEducation.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducation.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEducation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducation.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEducation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEducation.Properties.DropDownRows = 20;
            this.LueEducation.Properties.NullText = "[Empty]";
            this.LueEducation.Properties.PopupWidth = 500;
            this.LueEducation.Size = new System.Drawing.Size(171, 20);
            this.LueEducation.TabIndex = 35;
            this.LueEducation.ToolTip = "F4 : Show/hide list";
            this.LueEducation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEducation.EditValueChanged += new System.EventHandler(this.LueEducation_EditValueChanged);
            this.LueEducation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEducation_KeyDown);
            this.LueEducation.Leave += new System.EventHandler(this.LueEducation_Leave);
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(3, 3);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(758, 44);
            this.Grd6.TabIndex = 34;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpgCompetence
            // 
            this.TpgCompetence.Controls.Add(this.LueCompetenceCode);
            this.TpgCompetence.Controls.Add(this.Grd4);
            this.TpgCompetence.Location = new System.Drawing.Point(4, 26);
            this.TpgCompetence.Name = "TpgCompetence";
            this.TpgCompetence.Size = new System.Drawing.Size(801, 216);
            this.TpgCompetence.TabIndex = 8;
            this.TpgCompetence.Text = "Competence";
            this.TpgCompetence.UseVisualStyleBackColor = true;
            // 
            // LueCompetenceCode
            // 
            this.LueCompetenceCode.EnterMoveNextControl = true;
            this.LueCompetenceCode.Location = new System.Drawing.Point(60, 25);
            this.LueCompetenceCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceCode.Name = "LueCompetenceCode";
            this.LueCompetenceCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceCode.Properties.DropDownRows = 20;
            this.LueCompetenceCode.Properties.NullText = "[Empty]";
            this.LueCompetenceCode.Properties.PopupWidth = 500;
            this.LueCompetenceCode.Size = new System.Drawing.Size(171, 20);
            this.LueCompetenceCode.TabIndex = 35;
            this.LueCompetenceCode.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceCode.EditValueChanged += new System.EventHandler(this.LueCompetenceCode_EditValueChanged);
            this.LueCompetenceCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCompetenceCode_KeyDown);
            this.LueCompetenceCode.Leave += new System.EventHandler(this.LueCompetenceCode_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(801, 216);
            this.Grd4.TabIndex = 34;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgPotence
            // 
            this.TpgPotence.Controls.Add(this.Grd7);
            this.TpgPotence.Location = new System.Drawing.Point(4, 26);
            this.TpgPotence.Name = "TpgPotence";
            this.TpgPotence.Size = new System.Drawing.Size(764, 50);
            this.TpgPotence.TabIndex = 11;
            this.TpgPotence.Text = "Potency";
            this.TpgPotence.UseVisualStyleBackColor = true;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(764, 50);
            this.Grd7.TabIndex = 34;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd7_EllipsisButtonClick);
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // TpgWorkIndicator
            // 
            this.TpgWorkIndicator.Controls.Add(this.Grd8);
            this.TpgWorkIndicator.Location = new System.Drawing.Point(4, 26);
            this.TpgWorkIndicator.Name = "TpgWorkIndicator";
            this.TpgWorkIndicator.Size = new System.Drawing.Size(764, 50);
            this.TpgWorkIndicator.TabIndex = 12;
            this.TpgWorkIndicator.Text = "Work Indicator";
            this.TpgWorkIndicator.UseVisualStyleBackColor = true;
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 50);
            this.Grd8.TabIndex = 35;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // TpgSpecification
            // 
            this.TpgSpecification.Controls.Add(this.Grd9);
            this.TpgSpecification.Location = new System.Drawing.Point(4, 26);
            this.TpgSpecification.Name = "TpgSpecification";
            this.TpgSpecification.Size = new System.Drawing.Size(764, 50);
            this.TpgSpecification.TabIndex = 13;
            this.TpgSpecification.Text = "Specification";
            this.TpgSpecification.UseVisualStyleBackColor = true;
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 0);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(764, 50);
            this.Grd9.TabIndex = 35;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(10, 132);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(127, 14);
            this.label20.TabIndex = 21;
            this.label20.Text = "Number of Job Holder";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJobHolderQty
            // 
            this.TxtJobHolderQty.EnterMoveNextControl = true;
            this.TxtJobHolderQty.Location = new System.Drawing.Point(141, 129);
            this.TxtJobHolderQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJobHolderQty.Name = "TxtJobHolderQty";
            this.TxtJobHolderQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJobHolderQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJobHolderQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJobHolderQty.Properties.Appearance.Options.UseFont = true;
            this.TxtJobHolderQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtJobHolderQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtJobHolderQty.Properties.MaxLength = 16;
            this.TxtJobHolderQty.Size = new System.Drawing.Size(218, 20);
            this.TxtJobHolderQty.TabIndex = 22;
            this.TxtJobHolderQty.Validated += new System.EventHandler(this.TxtJobHolderQty_Validated);
            // 
            // TxtLevel
            // 
            this.TxtLevel.EnterMoveNextControl = true;
            this.TxtLevel.Location = new System.Drawing.Point(141, 108);
            this.TxtLevel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel.Name = "TxtLevel";
            this.TxtLevel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLevel.Properties.MaxLength = 2;
            this.TxtLevel.Size = new System.Drawing.Size(117, 20);
            this.TxtLevel.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(102, 110);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 19;
            this.label15.Text = "Level";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.LueDeptCode);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.LueSiteCode);
            this.panel3.Controls.Add(this.TxtPosName2);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(809, 153);
            this.panel3.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(64, 69);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Department";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(141, 66);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 12;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(218, 20);
            this.LueDeptCode.TabIndex = 16;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(109, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 14);
            this.label7.TabIndex = 13;
            this.label7.Text = "Site";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(88, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 11;
            this.label6.Text = "Position";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(141, 45);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 12;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Size = new System.Drawing.Size(218, 20);
            this.LueSiteCode.TabIndex = 14;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // TxtPosName2
            // 
            this.TxtPosName2.EnterMoveNextControl = true;
            this.TxtPosName2.Location = new System.Drawing.Point(141, 24);
            this.TxtPosName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosName2.Name = "TxtPosName2";
            this.TxtPosName2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosName2.Properties.Appearance.Options.UseFont = true;
            this.TxtPosName2.Properties.MaxLength = 200;
            this.TxtPosName2.Size = new System.Drawing.Size(218, 20);
            this.TxtPosName2.TabIndex = 12;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtMaxAge);
            this.panel4.Controls.Add(this.TxtMinAge);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtSoftCompetence);
            this.panel4.Controls.Add(this.TxtHardCompetence);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(565, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(244, 153);
            this.panel4.TabIndex = 23;
            // 
            // TxtMaxAge
            // 
            this.TxtMaxAge.EnterMoveNextControl = true;
            this.TxtMaxAge.Location = new System.Drawing.Point(168, 24);
            this.TxtMaxAge.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMaxAge.Name = "TxtMaxAge";
            this.TxtMaxAge.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMaxAge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMaxAge.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMaxAge.Properties.Appearance.Options.UseFont = true;
            this.TxtMaxAge.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMaxAge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMaxAge.Properties.MaxLength = 3;
            this.TxtMaxAge.Size = new System.Drawing.Size(69, 20);
            this.TxtMaxAge.TabIndex = 27;
            this.TxtMaxAge.Validated += new System.EventHandler(this.TxtMaxAge_Validated);
            // 
            // TxtMinAge
            // 
            this.TxtMinAge.EnterMoveNextControl = true;
            this.TxtMinAge.Location = new System.Drawing.Point(168, 3);
            this.TxtMinAge.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinAge.Name = "TxtMinAge";
            this.TxtMinAge.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinAge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinAge.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinAge.Properties.Appearance.Options.UseFont = true;
            this.TxtMinAge.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinAge.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinAge.Properties.MaxLength = 3;
            this.TxtMinAge.Size = new System.Drawing.Size(69, 20);
            this.TxtMinAge.TabIndex = 25;
            this.TxtMinAge.Validated += new System.EventHandler(this.TxtMinAge_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(106, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 14);
            this.label9.TabIndex = 26;
            this.label9.Text = "Max. Age";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(109, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 14);
            this.label10.TabIndex = 24;
            this.label10.Text = "Min. Age";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGreen;
            this.label2.Location = new System.Drawing.Point(9, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(230, 14);
            this.label2.TabIndex = 32;
            this.label2.Text = "Max sum of above competences are 100";
            // 
            // TxtSoftCompetence
            // 
            this.TxtSoftCompetence.EnterMoveNextControl = true;
            this.TxtSoftCompetence.Location = new System.Drawing.Point(168, 111);
            this.TxtSoftCompetence.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSoftCompetence.Name = "TxtSoftCompetence";
            this.TxtSoftCompetence.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSoftCompetence.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSoftCompetence.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSoftCompetence.Properties.Appearance.Options.UseFont = true;
            this.TxtSoftCompetence.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSoftCompetence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSoftCompetence.Properties.MaxLength = 3;
            this.TxtSoftCompetence.Size = new System.Drawing.Size(69, 20);
            this.TxtSoftCompetence.TabIndex = 31;
            this.TxtSoftCompetence.Validated += new System.EventHandler(this.TxtSoftCompetence_Validated);
            // 
            // TxtHardCompetence
            // 
            this.TxtHardCompetence.EnterMoveNextControl = true;
            this.TxtHardCompetence.Location = new System.Drawing.Point(168, 90);
            this.TxtHardCompetence.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHardCompetence.Name = "TxtHardCompetence";
            this.TxtHardCompetence.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHardCompetence.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHardCompetence.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHardCompetence.Properties.Appearance.Options.UseFont = true;
            this.TxtHardCompetence.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHardCompetence.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHardCompetence.Properties.MaxLength = 3;
            this.TxtHardCompetence.Size = new System.Drawing.Size(69, 20);
            this.TxtHardCompetence.TabIndex = 29;
            this.TxtHardCompetence.Validated += new System.EventHandler(this.TxtHardCompetence_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 14);
            this.label3.TabIndex = 30;
            this.label3.Text = "Soft Competence (0 - 100)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 14);
            this.label4.TabIndex = 28;
            this.label4.Text = "Hard Competence (0 - 100)";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.iGrid1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(801, 216);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Function";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // iGrid1
            // 
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.DefaultRow.Sortable = false;
            this.iGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 19;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.RowHeader.Visible = true;
            this.iGrid1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(801, 216);
            this.iGrid1.TabIndex = 34;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage2.Controls.Add(this.iGrid2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(764, 50);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Job Desc";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // iGrid2
            // 
            this.iGrid2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid2.DefaultRow.Height = 20;
            this.iGrid2.DefaultRow.Sortable = false;
            this.iGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid2.Header.Height = 19;
            this.iGrid2.Location = new System.Drawing.Point(0, 0);
            this.iGrid2.Name = "iGrid2";
            this.iGrid2.RowHeader.Visible = true;
            this.iGrid2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid2.SingleClickEdit = true;
            this.iGrid2.Size = new System.Drawing.Size(764, 50);
            this.iGrid2.TabIndex = 34;
            this.iGrid2.TreeCol = null;
            this.iGrid2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.iGrid3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 50);
            this.tabPage3.TabIndex = 7;
            this.tabPage3.Text = "Authorized";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // iGrid3
            // 
            this.iGrid3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid3.DefaultRow.Height = 20;
            this.iGrid3.DefaultRow.Sortable = false;
            this.iGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid3.Header.Height = 19;
            this.iGrid3.Location = new System.Drawing.Point(0, 0);
            this.iGrid3.Name = "iGrid3";
            this.iGrid3.RowHeader.Visible = true;
            this.iGrid3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid3.SingleClickEdit = true;
            this.iGrid3.Size = new System.Drawing.Size(764, 50);
            this.iGrid3.TabIndex = 34;
            this.iGrid3.TreeCol = null;
            this.iGrid3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lookUpEdit1);
            this.tabPage4.Controls.Add(this.iGrid4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(764, 50);
            this.tabPage4.TabIndex = 9;
            this.tabPage4.Text = "Experience";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(106, 31);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 20;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(171, 20);
            this.lookUpEdit1.TabIndex = 35;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid4
            // 
            this.iGrid4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid4.DefaultRow.Height = 20;
            this.iGrid4.DefaultRow.Sortable = false;
            this.iGrid4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid4.Header.Height = 19;
            this.iGrid4.Location = new System.Drawing.Point(3, 3);
            this.iGrid4.Name = "iGrid4";
            this.iGrid4.RowHeader.Visible = true;
            this.iGrid4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid4.SingleClickEdit = true;
            this.iGrid4.Size = new System.Drawing.Size(758, 44);
            this.iGrid4.TabIndex = 34;
            this.iGrid4.TreeCol = null;
            this.iGrid4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.lookUpEdit2);
            this.tabPage5.Controls.Add(this.lookUpEdit3);
            this.tabPage5.Controls.Add(this.iGrid5);
            this.tabPage5.Location = new System.Drawing.Point(4, 26);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(764, 50);
            this.tabPage5.TabIndex = 10;
            this.tabPage5.Text = "Education";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(414, 43);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 20;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(171, 20);
            this.lookUpEdit2.TabIndex = 36;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(175, 47);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 20;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(171, 20);
            this.lookUpEdit3.TabIndex = 35;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid5
            // 
            this.iGrid5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid5.DefaultRow.Height = 20;
            this.iGrid5.DefaultRow.Sortable = false;
            this.iGrid5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid5.Header.Height = 19;
            this.iGrid5.Location = new System.Drawing.Point(3, 3);
            this.iGrid5.Name = "iGrid5";
            this.iGrid5.RowHeader.Visible = true;
            this.iGrid5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid5.SingleClickEdit = true;
            this.iGrid5.Size = new System.Drawing.Size(758, 44);
            this.iGrid5.TabIndex = 34;
            this.iGrid5.TreeCol = null;
            this.iGrid5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.lookUpEdit4);
            this.tabPage6.Controls.Add(this.iGrid6);
            this.tabPage6.Location = new System.Drawing.Point(4, 26);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(764, 50);
            this.tabPage6.TabIndex = 8;
            this.tabPage6.Text = "Competence";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(60, 25);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 20;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(171, 20);
            this.lookUpEdit4.TabIndex = 35;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid6
            // 
            this.iGrid6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid6.DefaultRow.Height = 20;
            this.iGrid6.DefaultRow.Sortable = false;
            this.iGrid6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid6.Header.Height = 19;
            this.iGrid6.Location = new System.Drawing.Point(0, 0);
            this.iGrid6.Name = "iGrid6";
            this.iGrid6.RowHeader.Visible = true;
            this.iGrid6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid6.SingleClickEdit = true;
            this.iGrid6.Size = new System.Drawing.Size(764, 50);
            this.iGrid6.TabIndex = 34;
            this.iGrid6.TreeCol = null;
            this.iGrid6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.iGrid7);
            this.tabPage7.Location = new System.Drawing.Point(4, 26);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(801, 216);
            this.tabPage7.TabIndex = 11;
            this.tabPage7.Text = "Potency";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // iGrid7
            // 
            this.iGrid7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid7.DefaultRow.Height = 20;
            this.iGrid7.DefaultRow.Sortable = false;
            this.iGrid7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid7.Header.Height = 19;
            this.iGrid7.Location = new System.Drawing.Point(0, 0);
            this.iGrid7.Name = "iGrid7";
            this.iGrid7.RowHeader.Visible = true;
            this.iGrid7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid7.SingleClickEdit = true;
            this.iGrid7.Size = new System.Drawing.Size(801, 216);
            this.iGrid7.TabIndex = 34;
            this.iGrid7.TreeCol = null;
            this.iGrid7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.iGrid8);
            this.tabPage8.Location = new System.Drawing.Point(4, 26);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(764, 50);
            this.tabPage8.TabIndex = 12;
            this.tabPage8.Text = "Work Indicator";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // iGrid8
            // 
            this.iGrid8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid8.DefaultRow.Height = 20;
            this.iGrid8.DefaultRow.Sortable = false;
            this.iGrid8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid8.Header.Height = 19;
            this.iGrid8.Location = new System.Drawing.Point(0, 0);
            this.iGrid8.Name = "iGrid8";
            this.iGrid8.RowHeader.Visible = true;
            this.iGrid8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid8.SingleClickEdit = true;
            this.iGrid8.Size = new System.Drawing.Size(764, 50);
            this.iGrid8.TabIndex = 35;
            this.iGrid8.TreeCol = null;
            this.iGrid8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.iGrid9);
            this.tabPage9.Location = new System.Drawing.Point(4, 26);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(801, 216);
            this.tabPage9.TabIndex = 13;
            this.tabPage9.Text = "Specification";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // iGrid9
            // 
            this.iGrid9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid9.DefaultRow.Height = 20;
            this.iGrid9.DefaultRow.Sortable = false;
            this.iGrid9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid9.Header.Height = 19;
            this.iGrid9.Location = new System.Drawing.Point(0, 0);
            this.iGrid9.Name = "iGrid9";
            this.iGrid9.RowHeader.Visible = true;
            this.iGrid9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid9.SingleClickEdit = true;
            this.iGrid9.Size = new System.Drawing.Size(801, 216);
            this.iGrid9.TabIndex = 35;
            this.iGrid9.TreeCol = null;
            this.iGrid9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // FrmOrganizationalStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 399);
            this.Name = "FrmOrganizationalStructure";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgFunction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgJob.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgAuthorized.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgExperience.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpgEducation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgCompetence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgPotence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpgWorkIndicator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpgSpecification.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobHolderQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName2.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxAge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinAge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSoftCompetence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHardCompetence.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid8)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueParent;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgFunction;
        private System.Windows.Forms.TabPage TpgJob;
        private System.Windows.Forms.TabPage TpgAuthorized;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgCompetence;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceCode;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.TextEdit TxtJobHolderQty;
        internal DevExpress.XtraEditors.TextEdit TxtLevel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage TpgExperience;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.TabPage TpgEducation;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraEditors.LookUpEdit LueMajor;
        private DevExpress.XtraEditors.LookUpEdit LueEducation;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtSoftCompetence;
        private DevExpress.XtraEditors.TextEdit TxtHardCompetence;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        public DevExpress.XtraEditors.TextEdit TxtPosName2;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.TabPage TpgPotence;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraEditors.TextEdit TxtMaxAge;
        private DevExpress.XtraEditors.TextEdit TxtMinAge;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage TpgWorkIndicator;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private System.Windows.Forms.TabPage TpgSpecification;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        private System.Windows.Forms.TabPage tabPage1;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid1;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid2;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid3;
        private System.Windows.Forms.TabPage tabPage4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid4;
        private System.Windows.Forms.TabPage tabPage5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid5;
        private System.Windows.Forms.TabPage tabPage6;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid6;
        private System.Windows.Forms.TabPage tabPage7;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid7;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid8;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid9;
    }
}