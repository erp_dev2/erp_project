﻿namespace RunSystem
{
    partial class FrmRptMutation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkDocDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueItCtCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBatchNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkBatchNo = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtBatchNo2 = new DevExpress.XtraEditors.TextEdit();
            this.ChkBatchNo2 = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode2.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtBatchNo2);
            this.panel2.Controls.Add(this.ChkBatchNo2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtBatchNo);
            this.panel2.Controls.Add(this.ChkBatchNo);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkDocDt);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Size = new System.Drawing.Size(772, 77);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 396);
            this.Grd1.TabIndex = 24;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 77);
            this.panel3.Size = new System.Drawing.Size(772, 396);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(227, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 10;
            this.label3.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(6, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Mutated From";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Date";
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(95, 26);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 30;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 300;
            this.LueItCtCode.Size = new System.Drawing.Size(238, 20);
            this.LueItCtCode.TabIndex = 21;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // ChkDocDt
            // 
            this.ChkDocDt.Location = new System.Drawing.Point(370, 3);
            this.ChkDocDt.Name = "ChkDocDt";
            this.ChkDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocDt.Properties.Caption = " ";
            this.ChkDocDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocDt.Size = new System.Drawing.Size(19, 22);
            this.ChkDocDt.TabIndex = 12;
            this.ChkDocDt.ToolTip = "Remove filter";
            this.ChkDocDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocDt.ToolTipTitle = "Run System";
            this.ChkDocDt.CheckedChanged += new System.EventHandler(this.ChkDocDt_CheckedChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(243, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt2.TabIndex = 11;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(101, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt1.TabIndex = 9;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(68, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 14);
            this.label2.TabIndex = 22;
            this.label2.Text = "To";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode2
            // 
            this.LueItCtCode2.EnterMoveNextControl = true;
            this.LueItCtCode2.Location = new System.Drawing.Point(95, 48);
            this.LueItCtCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode2.Name = "LueItCtCode2";
            this.LueItCtCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode2.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode2.Properties.DropDownRows = 30;
            this.LueItCtCode2.Properties.NullText = "[Empty]";
            this.LueItCtCode2.Properties.PopupWidth = 300;
            this.LueItCtCode2.Size = new System.Drawing.Size(238, 20);
            this.LueItCtCode2.TabIndex = 23;
            this.LueItCtCode2.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode2.EditValueChanged += new System.EventHandler(this.LueItCtCode2_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueItCtCode);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.LueItCtCode2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(429, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(339, 73);
            this.panel4.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Batch# (From)";
            // 
            // TxtBatchNo
            // 
            this.TxtBatchNo.EnterMoveNextControl = true;
            this.TxtBatchNo.Location = new System.Drawing.Point(101, 26);
            this.TxtBatchNo.Name = "TxtBatchNo";
            this.TxtBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBatchNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBatchNo.Properties.MaxLength = 250;
            this.TxtBatchNo.Size = new System.Drawing.Size(265, 20);
            this.TxtBatchNo.TabIndex = 14;
            this.TxtBatchNo.Validated += new System.EventHandler(this.TxtBatchNo_Validated);
            // 
            // ChkBatchNo
            // 
            this.ChkBatchNo.Location = new System.Drawing.Point(370, 24);
            this.ChkBatchNo.Name = "ChkBatchNo";
            this.ChkBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBatchNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBatchNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBatchNo.Properties.Caption = " ";
            this.ChkBatchNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBatchNo.Size = new System.Drawing.Size(19, 22);
            this.ChkBatchNo.TabIndex = 15;
            this.ChkBatchNo.ToolTip = "Remove filter";
            this.ChkBatchNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBatchNo.ToolTipTitle = "Run System";
            this.ChkBatchNo.CheckedChanged += new System.EventHandler(this.ChkBatchNo_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Batch# (To)";
            // 
            // TxtBatchNo2
            // 
            this.TxtBatchNo2.EnterMoveNextControl = true;
            this.TxtBatchNo2.Location = new System.Drawing.Point(101, 48);
            this.TxtBatchNo2.Name = "TxtBatchNo2";
            this.TxtBatchNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBatchNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtBatchNo2.Properties.MaxLength = 250;
            this.TxtBatchNo2.Size = new System.Drawing.Size(265, 20);
            this.TxtBatchNo2.TabIndex = 17;
            this.TxtBatchNo2.Validated += new System.EventHandler(this.TxtBatchNo2_Validated);
            // 
            // ChkBatchNo2
            // 
            this.ChkBatchNo2.Location = new System.Drawing.Point(370, 46);
            this.ChkBatchNo2.Name = "ChkBatchNo2";
            this.ChkBatchNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBatchNo2.Properties.Appearance.Options.UseFont = true;
            this.ChkBatchNo2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBatchNo2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBatchNo2.Properties.Caption = " ";
            this.ChkBatchNo2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBatchNo2.Size = new System.Drawing.Size(19, 22);
            this.ChkBatchNo2.TabIndex = 18;
            this.ChkBatchNo2.ToolTip = "Remove filter";
            this.ChkBatchNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBatchNo2.ToolTipTitle = "Run System";
            this.ChkBatchNo2.CheckedChanged += new System.EventHandler(this.ChkBatchNo2_CheckedChanged);
            // 
            // FrmRptMutation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptMutation";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode2.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkDocDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtBatchNo;
        private DevExpress.XtraEditors.CheckEdit ChkBatchNo;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtBatchNo2;
        private DevExpress.XtraEditors.CheckEdit ChkBatchNo2;
    }
}