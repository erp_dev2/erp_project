﻿#region Update
/*
    21/11/2018 [TKG] Project's Monthly Cost
    18/01/2018 [DITA] Penambahan filter pada Project's Monthly Cost
    31/10/2019 [DITA/IMS] Penambahan Cutomer PO# dan Code Project
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectMonthlyCost : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mYr = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectMonthlyCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                string Yr = Sm.ServerCurrentDateTime().Substring(0, 4);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Yr);
                base.FrmLoad(sender, e);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueCCCode(ref LueCostCenter);
                Sl.SetLueOption(ref LueTypeCode, "ProjectType");
                Sl.SetLueOption(ref LueScope, "ProjectScope");
                Sl.SetLueOption(ref LueResource, "ProjectResource");
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Cost Category's"+Environment.NewLine+"Code",
                        "Cost Category's"+Environment.NewLine+"Name",
                        "Total",
                        "01",
                        "01",

                        //6-10
                        "02",
                        "02",
                        "03",
                        "03",
                        "04",

                        //11-15
                        "04",
                        "05",
                        "05",
                        "06",
                        "06",

                        //16-20
                        "07",
                        "07",
                        "08",
                        "08",
                        "09",

                        //21-25
                        "09",
                        "10",
                        "10",
                        "11",
                        "11",

                        //26-29
                        "12",
                        "12",
                        "Customer PO#",
                        "Project Code",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 300, 170, 150, 20,  
                        
                        //6-10
                        150, 20, 150, 20, 150,

                        //11-15
                        20, 150, 20, 150, 20,

                        //16-20
                        150, 20, 150, 20, 150,

                        //21-25
                        20, 150, 20, 150, 20,

                        //26-29
                        150, 20, 150, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 29 });
            for (int i = 1; i < 13; i++)
            {
                Grd1.Header.Cells[0, (i * 2) + 2].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[0, (i * 2) + 2].SpanCols = 2;
            }
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[28].Move(3);
            Grd1.Cols[29].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mYr = string.Empty;
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count > 0)
            {
                Grd1.Cells[0, 3].Value = 0m;
                for (int i = 1; i < 13; i++)
                    Grd1.Cells[0, (i * 2) + 2].Value = 0m;
            }
        }

        override protected void ShowData()
        {
            ClearData();

            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            
            Cursor.Current = Cursors.WaitCursor;

            mYr = Sm.GetLue(LueYr);
            var l = new List<Result>();

            try
            {
                Process1(ref l);
                if (l.Count > 0)
                    Process2(ref l);
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.Mth, T1.CCtCode, T2.CCtName, T1.Amt, T1.PONo, T1.ProjectCode ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select Substring(A.DocDt, 5, 2) As Mth, C.CCtCode,  ");
            SQL.AppendLine("    Sum(B.Qty*D.UPrice*D.ExcRate) As Amt, G.PONo, H.ProjectCode  ");
	        SQL.AppendLine("    From TblDODeptHdr A ");
	        SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblItemCostCategory C On A.CCCode=C.CCCode And B.ItCode=C.ItCode ");
	        SQL.AppendLine("    Inner Join TblStockPrice D On B.Source=D.Source ");
            SQL.AppendLine("    Inner Join TblLopHdr E On A.LOPDocNo=E.DocNo ");
            if (TxtProjectName.Text.Length > 0)
            {
                SQL.AppendLine("    And E.ProjectName Like @ProjectName ");
            }
            if (Sm.GetLue(LueCtCode).Length > 0)
            {
                SQL.AppendLine("    And E.CtCode = @CtCode ");
            }
            if (Sm.GetLue(LueSiteCode).Length > 0)
            {
                SQL.AppendLine("    And E.SiteCode = @SiteCode ");
            }
            if (Sm.GetLue(LueCostCenter).Length > 0)
            {
                SQL.AppendLine("    And E.CCCode = @CCCode ");
            }
            if (Sm.GetLue(LueScope).Length > 0)
            {
                SQL.AppendLine("    And E.ProjectScope = @ProjectScope ");
            }
            if (Sm.GetLue(LueResource).Length > 0)
            {
                SQL.AppendLine("    And E.ProjectResource = @ProjectResource ");
            }
            if (Sm.GetLue(LueTypeCode).Length > 0)
            {
                SQL.AppendLine("    And E.ProjectType = @ProjectType ");
            }
            if (TxtPONo.Text.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblBOQHdr F On E.DocNo=F.LopDocNo And F.ActInd = 'Y' And F.Status = 'A' ");
                SQL.AppendLine("    Inner Join TblSOContractHdr G On F.DocNo=G.BOQDocNo And G.CancelInd = 'N' And G.Status = 'A' ");
                SQL.AppendLine("        And G.PONo Like @PONo ");
            }
            else
            {
                SQL.AppendLine("    Left Join TblBOQHdr F On E.DocNo=F.LopDocNo And F.ActInd = 'Y' And F.Status = 'A' ");
                SQL.AppendLine("    Left Join TblSOContractHdr G On F.DocNo=G.BOQDocNo And G.CancelInd = 'N' And G.Status = 'A' ");
            }
            if (TxtProjectCode.Text.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblProjectGroup H On H.PGCode=E.PGCode ");
                SQL.AppendLine("         And H.ProjectCode Like @ProjectCode ");
            }
            else
            {
                SQL.AppendLine("    Left Join TblProjectGroup H On H.PGCode=E.PGCode ");
            }

            SQL.AppendLine("    Where A.CCCode Is Not Null ");
            SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("    Group By Substring(A.DocDt, 5, 2), C.CCtCode, G.PONo, H.ProjectCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCostCategory T2 On T1.CCtCode=T2.CCtCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", mYr);
            Sm.CmParam<String>(ref cm, "@ProjectName", string.Concat("%", TxtProjectName.Text, "%"));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCostCenter));
            Sm.CmParam<String>(ref cm, "@ProjectScope", Sm.GetLue(LueScope));
            Sm.CmParam<String>(ref cm, "@ProjectResource", Sm.GetLue(LueResource));
            Sm.CmParam<String>(ref cm, "@ProjectType", Sm.GetLue(LueTypeCode));
            Sm.CmParam<String>(ref cm, "@ProjectCode", string.Concat("%", TxtProjectCode.Text, "%"));
            Sm.CmParam<String>(ref cm, "@PONo", string.Concat("%", TxtPONo.Text, "%"));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Mth", 
                    
                    //1-3
                    "CCtCode", 
                    "CCtName", 
                    "Amt",
                    "PONo",
                    "ProjectCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            Mth = Sm.DrStr(dr, c[0]),
                            CCtCode = Sm.DrStr(dr, c[1]),
                            CCtName = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            PONo = Sm.DrStr(dr, c[4]),
                            ProjectCode = Sm.DrStr(dr, c[5]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            int i = 0, c = 2;
            string month = string.Empty;
            iGRow r;

            Grd1.BeginUpdate();

            foreach (var x in l
                  .Select(s => new { s.CCtCode, s.CCtName, s.PONo, s.ProjectCode })
                  .OrderBy(o => o.CCtName)
                  .Distinct()
                  .ToList())
            {
                    r = Grd1.Rows.Add();
                    i++;
                    r.Cells[0].Value = i;
                    r.Cells[1].Value = x.CCtCode;
                    r.Cells[2].Value = x.CCtName;
                    r.Cells[3].Value = 0m;
                    r.Cells[4].Value = 0m;
                    r.Cells[6].Value = 0m;
                    r.Cells[8].Value = 0m;
                    r.Cells[10].Value = 0m;
                    r.Cells[12].Value = 0m;
                    r.Cells[14].Value = 0m;
                    r.Cells[16].Value = 0m;
                    r.Cells[18].Value = 0m;
                    r.Cells[20].Value = 0m;
                    r.Cells[22].Value = 0m;
                    r.Cells[24].Value = 0m;
                    r.Cells[26].Value = 0m;
                    r.Cells[28].Value = x.PONo;
                    r.Cells[29].Value = x.ProjectCode;
            }

            foreach(var x in l.OrderBy(o=>o.Mth))
            {
                for (int row = 0; row < Grd1.Rows.Count; row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 1), x.CCtCode))
                    {
                        Grd1.Cells[row, (int.Parse(x.Mth) * 2) + 2].Value = x.Amt;
                        break;
                    }
                }
            }
            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                Grd1.Cells[row, 3].Value = 
                    Sm.GetGrdDec(Grd1, row, 4)+
                    Sm.GetGrdDec(Grd1, row, 6)+
                    Sm.GetGrdDec(Grd1, row, 8)+
                    Sm.GetGrdDec(Grd1, row, 10)+
                    Sm.GetGrdDec(Grd1, row, 12)+
                    Sm.GetGrdDec(Grd1, row, 14)+
                    Sm.GetGrdDec(Grd1, row, 16)+
                    Sm.GetGrdDec(Grd1, row, 18)+
                    Sm.GetGrdDec(Grd1, row, 20)+
                    Sm.GetGrdDec(Grd1, row, 22)+
                    Sm.GetGrdDec(Grd1, row, 24)+
                    Sm.GetGrdDec(Grd1, row, 26);
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27 }, e.ColIndex) && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptProjectMonthlyCostDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCtName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtYr.EditValue = mYr;
                f.TxtMth.EditValue = Grd1.Cols[e.ColIndex].Text;
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27 }, e.ColIndex) && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRptProjectMonthlyCostDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCtName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtYr.EditValue = mYr;
                f.TxtMth.EditValue = Grd1.Cols[e.ColIndex].Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCostCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenter, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueScope_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueScope, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectScope");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueResource_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueResource, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectResource");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectType");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkScope_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Scope Of Work");
        }

        private void ChkResource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Resource");
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        private void ChkCostCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        private void ChkTypeCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Type");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Code");
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string Mth { get; set; }
            public string CCtCode { get; set; }
            public string CCtName { get; set; }
            public decimal Amt { get; set; }
            public string ProjectCode { get; set; }
            public string PONo { get; set; }
        }

        #endregion
    }
}
