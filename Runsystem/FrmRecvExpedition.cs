﻿#region Update
/*
    28/01/2020 [TKG/IMS] New application
    04/02/2020 [TKG/IMS] tambah validasi receiving vendor
    24/04/2020 [TKG/IMS] item yg sudah direceived partial sisanya bisa diproses oleh expedisi lagi
    16/07/2020 [TKG/IMS] jasa bisa diproses juga
    02/10/2020 [TKG/IMS] receiving expedition ketika diproses di recv vd sisanya akan hangus kalau tidak diproses, dan harus diproses kembali di recv expedition
    04/11/2020 [TKG/IMS] hanya item non jasa yg bisa diproses
    06/11/2020 [DITA/IMS] bug warning tidak muncul saat cancel document
    18/12/2020 [WED/IMS] approval per item
    29/12/2020 [TKG/IMS] bug saat cancel
    21/01/2021 [ICA/IMS] menghapus G.ServiceItemInd='N' di setLueVdCode
    03/03/2021 [WED/IMS] Lot bukan dari combobox tapi textbox biasa
    04/04/2021 [WED/IMS] vendor yang muncul melihat rejected indicator nya
    08/04/2021 [WED/IMS] perbaikan query lagi
    04/08/2021 [vin/IMS] PO service dapat ditarik 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvExpedition : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvExpeditionFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvExpedition(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item By Expedition";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueVdCode(ref LueVdCode);
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin);
                LueLot.Visible = false;
                LueBin.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                     //0
                    "DNo",

                    //1-5
                    "Cancel", 
                    "Old Cancel", 
                    "Cancel Reason",
                    "", 
                    "PO#", 

                    //6-10
                    "PO DNo",
                    "PO's Local#",
                    "Item's Code", 
                    "Item's Name", 
                    "Local Code",

                    //11-15
                    "Specification",
                    "Batch#", 
                    "Source",
                    "Lot", 
                    "Bin", 

                    //16-20
                    "Quantity"+Environment.NewLine+"(Purchase)", 
                    "UoM"+Environment.NewLine+"(Purchase)", 
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)", 
                    "Quantity"+Environment.NewLine+"(Inventory)", 

                    //21-25
                    "UoM"+Environment.NewLine+"(Inventory)", 
                    "Quantity"+Environment.NewLine+"(Inventory)", 
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Remark",
                    "Project's Code",

                    //26-30
                    "Project's Name",
                    "Customer's PO#",
                    "No",
                    "Status",
                    "PO#",
                },
                 new int[] 
                {
                    //0
                    0,

                    //1-5
                    50, 0, 200, 20, 150, 
                    
                    //6-10
                    0, 150, 120, 200, 130,  
                    
                    //11-15
                    250, 150, 180, 60, 60, 
                    
                    //16-20
                    100, 100, 100, 100, 100, 
        
                    //21-25
                    80, 100, 80, 200, 150,

                    //26-29
                    200, 150, 50, 120, 150
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 9, 10, 11, 13, 17, 19, 21, 23, 25, 26, 27, 28, 29, 30 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18, 20, 22 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8, 11, 13, 14, 15, 20, 21, 22, 23, 5 });
            Grd1.Cols[29].Move(5);
            Grd1.Cols[30].Move(6);
            Grd1.Cols[28].Move(0);
            ShowInventoryUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 3;
            Grd2.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                         //0
                        "PO#", 
                        
                        //1-5
                        "PO DNo",
                        "Item's Code", 
                        "Item's Name", 
                        "UoM", 
                        "Outstanding", 
                        
                        //6-9
                        "Received",
                        "Balance",
                        "Local Code",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        150,

                        //1-5
                        0, 100, 200, 80, 100, 
                        
                        //6-9
                        100, 100, 130, 200
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Grd2.Cols[8].Move(4);
            Grd2.Cols[9].Move(5);
            
            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, TxtVdDONo, 
                        MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 4, 12, 14, 15, 16, 18, 20, 22, 24, 30 });
                    Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, TxtVdDONo, 
                        MeeRemark 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 12, 14, 15, 16, 18, 20, 22, 24 });
                    Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 14 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, 
                TxtVdDONo, MeeRemark
            });
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 16, 18, 20, 22 });
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvExpeditionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetLueVdCode(ref LueVdCode, "");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0)
                {
                    if (e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2))
                        e.DoDefault = false;
                }
                else
                {
                    if (e.ColIndex == 4)
                    {
                        e.DoDefault = false;
                        if (!Sm.IsLueEmpty(LueVdCode, "Vendor"))
                            if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvExpeditionDlg(this, Sm.GetLue(LueVdCode)));
                    }
                    else
                    {
                        if (Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
                        {
                            if (Sm.IsGrdColSelected(new int[] { 12, 14, 15, 16, 18, 20, 22, 24 }, e.ColIndex))
                            {
                                //if (e.ColIndex == 14) LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                                if (e.ColIndex == 15) LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                            }
                        }
                        else
                            e.DoDefault = false;
                    }
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            string PODocNo = string.Empty, PODNo = string.Empty;

                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                PODocNo = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 5); 
                                PODNo =  Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 6);

                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);

                                RemovePOInfo(PODocNo, PODNo);
                            }
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                            SetSeqNo();
                        }
                    }
                }
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmRecvExpeditionDlg(this, Sm.GetLue(LueVdCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 16, 18, 20, 22 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 24 }, e);

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 8, 18, 20, 22, 19, 21, 23);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 8, 18, 22, 20, 19, 23, 21);
                }

                if (e.ColIndex == 20)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 20, 18, 22, 21, 19, 23);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 20, 22, 18, 21, 23, 19);
                }

                if (e.ColIndex == 22)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 22, 18, 20, 23, 19, 21);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 22, 20, 18, 23, 21, 19);
                }

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 16);
                
                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 16);
                
                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 22, Grd1, e.RowIndex, 16);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 18);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 22, Grd1, e.RowIndex, 18);

                if (e.ColIndex == 20 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 21), Sm.GetGrdStr(Grd1, e.RowIndex, 23)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 22, Grd1, e.RowIndex, 20);

                if (e.ColIndex==1 || e.ColIndex==16) ComputeQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvExpedition", "TblRecvExpeditionHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvExpeditionHdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0) cml.Add(SaveRecvExpeditionDtl(DocNo, r));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ComputeOutstandingQty();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyNotValid() ||
                IsBalanceNotValid() ||
                IsPORevisionExisted();
        }

        private bool IsPORevisionExisted()
        {
            string DocNo = string.Empty, DNo = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 5);
                    DNo = Sm.GetGrdStr(Grd1, r, 6);
                    if (Sm.IsDataExist(
                        "Select 1 From TblPORevision Where PODocNo=@Param1 And PODNo=@Param2 And CancelInd='N' And Status='O';",
                        DocNo, DNo, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO# : " + DocNo + Environment.NewLine +
                            "PO's Local# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                            "There's an outstanding revision (PO) for this PO's item.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                   "PO's Local# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine;
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "PO# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, string.Concat(Msg, Environment.NewLine, "Quantity (Purchase) should not be 0.00.")) ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 18, true, string.Concat(Msg, Environment.NewLine,"Quantity (Inventory) should not be 0.00.")) ||
                    (Grd1.Cols[20].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 20, true, string.Concat(Msg, Environment.NewLine,"Quantity (Inventory) should not be 0.00."))) ||
                    (Grd1.Cols[22].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 22, true, string.Concat(Msg, Environment.NewLine,"Quantity (Inventory) should not be 0.00."))) 
                    )
                    return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                {
                    if (
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 19)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 18)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 23)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 22)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 18) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 23)) && Sm.GetGrdDec(Grd1, Row, 18) != Sm.GetGrdDec(Grd1, Row, 22)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 21), Sm.GetGrdStr(Grd1, Row, 23)) && Sm.GetGrdDec(Grd1, Row, 20) != Sm.GetGrdDec(Grd1, Row, 22))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "PO's Local# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + 
                            "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine +
                            "Quantity information is not valid.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsBalanceNotValid()
        {
            for (int Row = 0; Row <= Grd2.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdDec(Grd2, Row, 7) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd2, Row, 0) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd2, Row, 8) + Environment.NewLine + 
                        "Balance : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 7), 0) + Environment.NewLine +
                        "UoM : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine + 
                        "Balance is less than 0.00.");
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveRecvExpeditionHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblRecvExpeditionHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, WhsCode, VdCode, VdDONo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @VdCode, @VdDONo, @Remark, @UserCode, CurrentDateTime()); ");

    
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdDONo", TxtVdDONo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvExpeditionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblRecvExpeditionDtl ");
            SQL.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, 'O', @PODocNo, @PODNo, @ItCode, ");
            if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                SQL.AppendLine("@ProjectCode, ");
            else
                SQL.AppendLine("IfNull(@BatchNo, '-'), ");
            SQL.AppendLine("Concat(@DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), ");
            SQL.AppendLine("@QtyPurchase, @Qty, @Qty2, @Qty3, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, @DNo, DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='RecvExpedition' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblRecvExpeditionDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo = @DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And DNo = @DNo ");
            SQL.AppendLine("); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@PODocNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@PODNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@QtyPurchase", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0) cml.Add(UpdateLot(Row));
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                    cml.Add(EditRecvExpeditionDtl(Row));
            }            

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select DNo, CancelInd From TblRecvExpeditionDtl Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                //IsCancelledRecvVdNotExisted(DNo) ||
                IsRecvVdExisted() ||
                IsCancelReasonEmpty();
        }

        private bool IsCancelReasonEmpty()
        { 
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "PO's Local# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private bool IsCancelledRecvVdNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsRecvVdExisted()
        {
            string Msg = string.Empty;
            string Query =
                "Select 1 From TblRecvVdDtl " +
                "Where CancelInd='N' And Status In ('O', 'A') " +
                "And RecvExpeditionDocNo Is Not Null " +
                "And RecvExpeditionDocNo=@Param1 " +
                "And RecvExpeditionDNo=@Param2 " +
                "Limit 1;";

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0 && Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2))
                {
                    if (Sm.IsDataExist(Query, TxtDocNo.Text, Sm.GetGrdStr(Grd1, r, 0), string.Empty, string.Empty))
                    {
                        Msg = "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                         "PO's Local# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                         "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                         "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                         "Item's Local Code : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + Environment.NewLine +
                         "This item already processed to vendor's received transaction.";

                        Sm.StdMsg(mMsgType.Warning, Msg);
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand EditRecvExpeditionDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvExpeditionDtl Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateLot(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvExpeditionDtl ");
            SQL.AppendLine("Set Lot = @Lot ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 14));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRecvExpeditionHdr(DocNo);
                ShowRecvExpeditionDtl(DocNo);
                ShowPOInfo(DocNo);
                //for (int r = 0; r<Grd1.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd1, r, 5).Length>0) ShowPOInfo(r);
                SetSeqNo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvExpeditionHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var VdCode =String.Empty;

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.WhsCode, A.VdCode, A.VdDONo, A.Remark ");
            SQL.AppendLine("From TblRecvExpeditionHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "LocalDocNo", "WhsCode", "VdCode", "VdDONo", 
                        //6
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        VdCode = Sm.DrStr(dr, c[4]);
                        SetLueVdCode(ref LueVdCode, VdCode);
                        Sm.SetLue(LueVdCode, VdCode);
                        TxtVdDONo.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowRecvExpeditionDtl(string DocNo)
        {            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, ifnull(L.DocNo, A.PODocNo)PODocNo, A.PODNo, F.LocalDocNo, ");
            SQL.AppendLine("CAse A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.QtyPurchase, E.PurchaseUomCode, ");
            SQL.AppendLine("A.Qty, E.InventoryUomCode, A.Qty2, E.InventoryUomCode2, A.Qty3, E.InventoryUomCode3, ");
            SQL.AppendLine("A.Remark, K.ProjectCode, K.ProjectName, H.PONo ");
            SQL.AppendLine("From TblRecvExpeditionDtl A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Inner Join TblPOHdr F On A.PODocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr G On C.MaterialRequestDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr H ON G.SOCDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblBOQHdr I On H.BOQDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr J On I.LOPDocNo=J.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup K On J.PGCode=K.PGCode ");
            SQL.AppendLine("Left Join tblmaterialrequestdtl L ON D.DocNo = L.DocNo AND D.DNo = L.DNo ");
            SQL.AppendLine("  AND L.MaterialRequestServiceDocNo IS NOT NULL ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "CancelReason", "PODocNo", "PODNo", "LocalDocNo", 

                    //6-10
                    "ItCode", "ItName", "ItCodeInternal", "Specification", "BatchNo", 

                    //11-15
                    "Source", "Lot", "Bin", "QtyPurchase", "PurchaseUomCode", 

                    //16-20
                    "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 

                    //21-25
                    "InventoryUomCode3", "Remark", "ProjectCode", "ProjectName", "PONo",

                    //26
                    "StatusDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 3);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 16, 18, 20, 22 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPOInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, D.ItCode, E.ItName, E.ForeignName, E.PurchaseUomCode, ");
            SQL.AppendLine("B.Qty-IfNull(( ");
            SQL.AppendLine("    Select Sum(T.QtyPurchase-(T.QtyPurchase-T.RecvVdQtyPurchase)) From TblRecvExpeditionDtl T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo<>@DocNo ");
            SQL.AppendLine("    ), 0.00) - ");
            SQL.AppendLine("   IfNull(( ");
            SQL.AppendLine("        Select Sum(T1.Qty) From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.PODocNo=B.DocNo And T1.PODNo=B.DNo ");
            SQL.AppendLine("    ), 0.00) ");
            SQL.AppendLine("As OutstandingQty, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Sum(T.QtyPurchase) From TblRecvExpeditionDtl T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo=@DocNo ");
            SQL.AppendLine(") As ReceivedQty, ");
            SQL.AppendLine("E.ItCodeInternal, E.Specification ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Concat(B.DocNo, B.DNo) In (Select Distinct Concat(PODocNo, PODNo) From TblRecvExpeditionDtl Where DocNo=@DocNo) ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DNo", "ItCode", "ItName", "PurchaseUomCode", "OutstandingQty",

                    //6-8
                    "ReceivedQty", "ItCodeInternal", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Grd.Cells[Row, 7].Value = Sm.GetGrdDec(Grd, Row, 5) - Sm.GetGrdDec(Grd, Row, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private string GetDteFormat(DateTime Value)
        {
            return
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                Sm.Right(Value.Year.ToString(), 2);
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 28].Value = r + 1;
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot", "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;", "Bin");
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            //mIsRecvVdSplitOutstandingPO = Sm.GetParameterBoo("IsRecvVdSplitOutstandingPO");
            //mPODocTitle = Sm.GetParameter("DocTitle");
            //mPODocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='PO'");
            //mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsRecvVdRemarkForApprovalMandatory");
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsRecvVdApprovalBasedOnWhs = Sm.GetParameterBoo("IsRecvVdApprovalBasedOnWhs");
            //mPOWithTitleLength = 4 + mPODocTitle.Length + mPODocAbbr.Length + 4 + 4; //4=nomor urut(0001), 4=bln+tahun(1115), 4 = jmlh "/"
            //mPOWithoutTitleLength = 4 + mPODocAbbr.Length + 4 + 3;
            //mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            //mRecvVdAutoBatchNoFormat = Sm.GetParameter("RecvVdAutoBatchNoFormat");
            //mIsShowSeqNoInRecvVd = Sm.GetParameterBoo("IsShowSeqNoInRecvVd");
            //mIsBatchNoUseDocDtIfEmpty = Sm.GetParameterBoo("IsBatchNoUseDocDtIfEmpty");
            //mIsRecvVdLocalDocNoBasedOnMRLocalDocNo = Sm.GetParameterBoo("IsRecvVdLocalDocNoBasedOnMRLocalDocNo");
            //mRecvVdCustomsDocCodeManualInput = Sm.GetParameter("RecvVdCustomsDocCodeManualInput");
            //mIsProjectGroupEnabled = Sm.GetParameterBoo("IsProjectGroupEnabled");
            //mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mNumberOfInventoryUomCode = (NumberOfInventoryUomCode.Length == 0) ? 1 : int.Parse(NumberOfInventoryUomCode);
        }

        internal void ShowPOInfo(int Row)
        {
            bool IsExisted = false;
            decimal ReceivedQty = 0m;

            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2 <= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length>0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1), Sm.GetGrdStr(Grd1, Row, 5)+Sm.GetGrdStr(Grd1, Row, 6)))
                    {
                        IsExisted = true;
                        ReceivedQty = Sm.GetGrdDec(Grd2, Row2, 7);
                        if (ReceivedQty < 0) ReceivedQty = 0m;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                int Row2 = Grd2.Rows.Count - 1;

                Grd2.Cells[Row2, 0].Value = Sm.GetGrdStr(Grd1, Row, 5);
                Grd2.Cells[Row2, 1].Value = Sm.GetGrdStr(Grd1, Row, 6);
                Grd2.Cells[Row2, 2].Value = Sm.GetGrdStr(Grd1, Row, 8);
                Grd2.Cells[Row2, 3].Value = Sm.GetGrdStr(Grd1, Row, 9);
                Grd2.Cells[Row2, 4].Value = Sm.GetGrdStr(Grd1, Row, 17);
                Grd2.Cells[Row2, 5].Value = Grd2.Cells[Row2, 6].Value = Sm.GetGrdDec(Grd1, Row, 16);
                Grd2.Cells[Row2, 7].Value = 0m;
                Grd2.Cells[Row2, 8].Value = Sm.GetGrdStr(Grd1, Row, 10);
                Grd2.Cells[Row2, 9].Value = Sm.GetGrdStr(Grd1, Row, 11);
                Grd2.Rows.Add();
            }
            else
            {
                Grd1.Cells[Row, 16].Value = ReceivedQty;
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 19)))
                    Sm.CopyGrdValue(Grd1, Row, 18, Grd1, Row, 16);
                else
                    Grd1.Cells[Row, 18].Value = 0m;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 21)))
                    Sm.CopyGrdValue(Grd1, Row, 20, Grd1, Row, 16);
                else
                    Grd1.Cells[Row, 20].Value = 0m;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 23)))
                    Sm.CopyGrdValue(Grd1, Row, 22, Grd1, Row, 16);
                else
                    Grd1.Cells[Row, 22].Value = 0m;
            }
                
            ComputeQty();
        }

        internal void RemovePOInfo(string PODocNo, string PODNo)
        {
            var IsExisted = false;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    if (
                        Sm.GetGrdStr(Grd1, Row, 5).Length > 0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 6), PODocNo + PODNo))
                    {
                        IsExisted = true;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                for (int Row = Grd2.Rows.Count-1; Row >= 0; Row--)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0) + Sm.GetGrdStr(Grd2, Row, 1), PODocNo + PODNo))
                    {
                        Grd2.Rows.RemoveAt(Row);
                        break;
                    }
                }
            }
            else
                ComputeQty();
        }

        internal void ComputeQty()
        {
            decimal ReceivedQty = 0m;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2<= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length > 0)
                    {
                        ReceivedQty = 0m;
                        for (int Row1 = 0; Row1 <= Grd1.Rows.Count - 1; Row1++)
                        {
                            if (
                                !Sm.GetGrdBool(Grd1, Row1, 1) &&
                                Sm.GetGrdStr(Grd1, Row1, 5).Length > 0 && 
                                Sm.CompareStr(
                                    Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1),
                                    Sm.GetGrdStr(Grd1, Row1, 5) + Sm.GetGrdStr(Grd1, Row1, 6)))
                                ReceivedQty += Sm.GetGrdDec(Grd1, Row1, 16);
                        }
                        Grd2.Cells[Row2, 6].Value = ReceivedQty;
                        Grd2.Cells[Row2, 7].Value = Sm.GetGrdDec(Grd2, Row2, 5) - ReceivedQty;
                    }
                }
            }
            
        }

        //private bool IsPOEmpty(iGRequestEditEventArgs e)
        //{
        //    if (Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length == 0)
        //    {
        //        e.DoDefault = false;
        //        //Sm.StdMsg(mMsgType.Warning, "PO document number is empty.");
        //        return true;
        //    }
        //    return false;
        //}

        private void ComputeOutstandingQty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, ");
            SQL.AppendLine("(If(A.CancelInd='N', A.Qty, 0.00)-IfNull(B.Qty2, 0.00)-IfNull(C.Qty3, 0.00)) As OutstandingQty ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, ");
            //SQL.AppendLine("    Sum(QtyPurchase-(QtyPurchase-RecvVdQtyPurchase)) As Qty2 ");
            //SQL.AppendLine("    Sum(Case When RecvVdQtyPurchase<=0.00 Then QtyPurchase Else RecvVdQtyPurchase End) As Qty2 ");
		    //SQL.AppendLine("    From TblRecvExpeditionDtl ");
            //SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            //SQL.AppendLine("    And CancelInd='N' And Status In ('O', 'A') Group By PODocNo, PODNo ");
            SQL.AppendLine("    Select X1.PODocNo As DocNo, X1.PODNo As DNo, ");
            SQL.AppendLine("    Sum(Case When IfNull(X2.RejectedInd, 'N') = 'Y' Then X1.RecvVdQtyPurchase Else Case When X1.RecvVdQtyPurchase<=0.00 Then X1.QtyPurchase Else X1.RecvVdQtyPurchase End End) As Qty2 ");
            SQL.AppendLine("    From TblRecvExpeditionDtl X1 ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct T1.RecvExpeditionDocNo, T2.PODocNo, T2.PODNo, IfNull(T2.RejectedInd, 'N') RejectedInd ");
            SQL.AppendLine("        From TblRecvVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.RecvExpeditionDocNo Is Not Null ");
            SQL.AppendLine("            And T2.CancelInd = 'N' ");
            SQL.AppendLine("            And T2.Status IN ('O', 'A') ");
            SQL.AppendLine("            And Locate(Concat('##', T2.PODocNo, T2.PODNo, '##'), @Param)>0  ");
            SQL.AppendLine("    ) X2 On X1.DocNo = X2.RecvExpeditionDocNo And X1.PODocNo = X2.PODocNo And X1.PODNo = X2.PODNo ");
            SQL.AppendLine("    Where Locate(Concat('##', X1.PODocNo, X1.PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And X1.CancelInd='N' And X1.Status In ('O', 'A') Group By X1.PODocNo, X1.PODNo ");
	        SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
	        SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel ");
            SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And CancelInd='N' Group By PODocNo, PODNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And A.DNo=C.DNo  ");
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @Param)>0 ");
            SQL.AppendLine("Order By A.DocNo;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedPO());
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "OutstandingQty" });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && 
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 1), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 2);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
            ComputeQty();
        }

        internal string GetSelectedPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                           "##" +
                            Sm.GetGrdStr(Grd2, Row, 0) +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VdCode As Col1, VdName As Col2 ");
            SQL.AppendLine("From TblVendor ");

            if (VdCode.Length == 0)
            {
                //SQL.AppendLine("Where VdCode In ( ");
                //SQL.AppendLine("    Select Distinct A.VdCode ");
                //SQL.AppendLine("    From TblPOHdr A, TblPODtl B ");
                //SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                //SQL.AppendLine("    And B.CancelInd='N' ");
                //SQL.AppendLine("    And B.ProcessInd<>'F' ");
                //SQL.AppendLine("    ) ");

                SQL.AppendLine("Where VdCode In ( ");
                SQL.AppendLine("    Select Distinct VdCode From ( ");
                SQL.AppendLine("        Select A.VdCode, ");
                SQL.AppendLine("        B.Qty-IfNull(C.Qty2, 0.00)-IfNull(D.Qty3, 0.00) As OutstandingQty ");
                SQL.AppendLine("        From TblPOHdr A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd<>'F' ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(Case When IfNull(T4.RejectedInd, 'N') = 'Y' Then T1.RecvVdQtyPurchase Else Case When T1.RecvVdQtyPurchase<=0.00 Then T1.QtyPurchase Else T1.RecvVdQtyPurchase End End) As Qty2 ");
                SQL.AppendLine("            From TblRecvExpeditionDtl T1 ");
                SQL.AppendLine("            Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
                SQL.AppendLine("            Inner Join TblPOHdr T3 ");
                SQL.AppendLine("                On T2.DocNo=T3.DocNo ");
                SQL.AppendLine("                And T3.Status='A' ");
                SQL.AppendLine("            Left Join  ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select X1.RecvExpeditionDocNo, X2.PODocNo, X2.PODNo, X2.RejectedInd, X2.QtyPurchase ");
                SQL.AppendLine("                From TblRecvVdHdr X1 ");
                SQL.AppendLine("                Inner Join TblRecvVdDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("                    And X1.RecvExpeditionDocNo Is Not Null ");
                SQL.AppendLine("                    And X2.CancelInd = 'N' ");
                SQL.AppendLine("                    And X2.Status In ('O', 'A') ");
                SQL.AppendLine("            ) T4 On T1.DocNo = T4.RecvExpeditionDocNo And T1.PODOcNo = T4.PODocNo And T1.PODNo = T4.PODNo ");
                SQL.AppendLine("            Where T1.CancelInd='N' ");
                SQL.AppendLine("            And T1.Status In ('O', 'A') ");
                SQL.AppendLine("            Group By T1.PODocNo, T1.PODNo ");
                SQL.AppendLine("        ) C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
                SQL.AppendLine("            From TblPOQtyCancel T1 ");
                SQL.AppendLine("            Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
                SQL.AppendLine("            Inner Join TblPOHdr T3 ");
                SQL.AppendLine("                On T2.DocNo=T3.DocNo ");
                SQL.AppendLine("                And T3.Status='A' ");
                SQL.AppendLine("            Where T1.CancelInd='N' ");
                SQL.AppendLine("            Group By T1.PODocNo, T1.PODNo ");
                SQL.AppendLine("        ) D On A.DocNo=D.DocNo And B.DNo=D.DNo  ");
                SQL.AppendLine("        Inner Join TblPORequestDtl E On B.PORequestDocNo=E.DocNo And B.PORequestDNo=E.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
                SQL.AppendLine("        Inner Join TblItem G On F.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.Status='A' ");
                SQL.AppendLine("    ) X ");
                SQL.AppendLine("    Where X.OutstandingQty>0.00 ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where VdCode='" + VdCode + "' ");
            }
            SQL.AppendLine("Order By VdName; ");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVdDONo);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 15)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 15].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 15].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 14)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 14].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 14].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }

        #endregion

        #endregion
    }
}
