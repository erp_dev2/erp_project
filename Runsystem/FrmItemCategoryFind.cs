﻿#region Update
/*
    12/04/2017 [WED] revisi untuk excel, memunculkan kode item yang berawalan angka 0.
    14/11/2017 [TKG] tambah nomor rekening coa untuk sales, COGS, Sales Return, Purchase Return
    27/11/2019 [TKG/IMS] tambah indikator moving average 
    26/11/2019 [HAR/YK] BUG menampilkan nama account description
    19/01/2020 [TKG/IMS] tambah alias nomor rekening coa
    06/04/2020 [HAR/YK] bug menampilkan lastupby lastupdate 
    27/01/2021 [ICA/PHT] Menambah kolom COA AP dan COA AR berdasarkan param IsItemCategoryUserCOAAPAR
    02/02/2021 [ICA/PHT] Menambah kolom COA AP dan COA AR berdasarkan param IsItemCategoryUserCOAAPAR
    19/08/2021 [RDA/ALL] Menambahkan kolom COA AP Uninvoiced berdasarkan param IsItemCategoryUserCOAAPAR
    08/02/2022 [IBL/PHT] Menambah kolom COA Stock Beginning berdasarkan param IsItemCategoryUseCOAStockBeginning
    22/12/2022 [VIN/PHT] BUG Export to excel
    16/03/2023 [VIN/ALL] kolom date time salah set
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmItemCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmItemCategory mFrmParent;
        private string mSQL = string.Empty;
        #endregion

        #region Constructor

        public FrmItemCategoryFind(FrmItemCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCtCode, A.ItCtName, A.ActInd, A.WastedInd, A.MovingAvgInd, ");
            SQL.AppendLine("A.AcNo, A.AcNo2, A.AcNo3, A.AcNo4, A.AcNo5, A.AcNo6, A.AcNo7, ");
            if (mFrmParent.mIsItemCategoryUseCOAAPAR)
                SQL.AppendLine("A.AcNo8, A.AcNo9, A.AcNo10, A.AcNo11, ");
            else
                SQL.AppendLine("null As AcNo8, null As AcNo9, null as AcNo10, null as AcNo11, ");
            if(mFrmParent.mIsItemCategoryUseCOAStockBeginning)
                SQL.AppendLine("A.AcNo12, ");
            else
                SQL.AppendLine("null As AcNo12, ");
            if (mFrmParent.mIsCOAUseAlias)
            {
                SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc, ");
                SQL.AppendLine("Concat(C.AcDesc, Case When C.Alias Is Null Then '' Else Concat(' [', C.Alias, ']') End) As AcDesc2, ");
                SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc3, ");
                SQL.AppendLine("Concat(E.AcDesc, Case When E.Alias Is Null Then '' Else Concat(' [', E.Alias, ']') End) As AcDesc4, ");
                SQL.AppendLine("Concat(F.AcDesc, Case When F.Alias Is Null Then '' Else Concat(' [', F.Alias, ']') End) As AcDesc5, ");
                SQL.AppendLine("Concat(G.AcDesc, Case When G.Alias Is Null Then '' Else Concat(' [', G.Alias, ']') End) As AcDesc6, ");
                SQL.AppendLine("Concat(H.AcDesc, Case When H.Alias Is Null Then '' Else Concat(' [', H.Alias, ']') End) As AcDesc7, ");
                if (mFrmParent.mIsItemCategoryUseCOAAPAR)
                {
                    SQL.AppendLine("Concat(I.AcDesc, Case When I.Alias Is Null Then '' Else Concat(' [', I.Alias, ']') End) As AcDesc8, ");
                    SQL.AppendLine("Concat(J.AcDesc, Case When J.Alias Is Null Then '' Else Concat(' [', J.Alias, ']') End) As AcDesc9, ");
                    SQL.AppendLine("Concat(K.AcDesc, Case When K.Alias Is Null Then '' Else Concat(' [', K.Alias, ']') End) As AcDesc10, ");
                    SQL.AppendLine("Concat(L.AcDesc, Case When L.Alias Is Null Then '' Else Concat(' [', L.Alias, ']') End) As AcDesc11, ");
                }
                else
                    SQL.AppendLine("Null as AcDesc8, Null As AcDesc9, Null As AcDesc10, Null As AcDesc11, ");

                if (mFrmParent.mIsItemCategoryUseCOAStockBeginning)
                    SQL.AppendLine("Concat(M.AcDesc, Case When M.Alias Is Null Then '' Else Concat(' [', M.Alias, ']') End) As AcDesc12, ");
                else
                    SQL.AppendLine("Null as AcDesc12, ");
            }
            else
            {
                SQL.AppendLine("B.AcDesc, C.AcDesc As AcDesc2, D.AcDesc As AcDesc3, ");
                SQL.AppendLine("E.AcDesc As AcDesc4, F.AcDesc As AcDesc5, G.AcDesc As AcDesc6, H.AcDesc As AcDesc7, ");
                if (mFrmParent.mIsItemCategoryUseCOAAPAR)
                {
                    SQL.AppendLine("I.AcDesc As AcDesc8, J.AcDesc As AcDesc9, K.AcDesc As AcDesc10, L.AcDesc As AcDesc11, ");
                }
                else
                    SQL.AppendLine("Null As AcDesc8, Null As AcDesc9, Null As AcDesc10, Null As AcDesc11, ");
                if (mFrmParent.mIsItemCategoryUseCOAStockBeginning)
                    SQL.AppendLine("M.AcDesc As AcDesc12, ");
                else
                    SQL.AppendLine("Null as AcDesc12, ");
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblItemCategory A ");
            SQL.AppendLine("Left Join TblCoa B On A.AcNo=B.AcNo ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo3=D.AcNo ");
            SQL.AppendLine("Left Join TblCoa E On A.AcNo4=E.AcNo ");
            SQL.AppendLine("Left Join TblCoa F On A.AcNo5=F.AcNo ");
            SQL.AppendLine("Left Join TblCoa G On A.AcNo6=G.AcNo ");
            SQL.AppendLine("Left Join TblCoa H On A.AcNo7=H.AcNo ");
            if (mFrmParent.mIsItemCategoryUseCOAAPAR)
            {
                SQL.AppendLine("Left Join TblCoa I On A.AcNo8=I.AcNo ");
                SQL.AppendLine("Left Join TblCoa J On A.AcNo9=J.AcNo ");
                SQL.AppendLine("Left Join TblCoa K On A.AcNo10=K.AcNo ");
                SQL.AppendLine("Left Join TblCoa L On A.AcNo11=L.AcNo ");
            }
            if(mFrmParent.mIsItemCategoryUseCOAStockBeginning)
                SQL.AppendLine("Left Join TblCoa M On A.AcNo12=M.AcNo ");

            mSQL = SQL.ToString();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Wasted",
                        "Moving Avg.",
                        
                        //6-10
                        "COA#"+Environment.NewLine+"(Stock)",
                        "COA Description"+Environment.NewLine+"(Stock)",
                        "COA#"+Environment.NewLine+"(Cost)",
                        "COA Description"+Environment.NewLine+"(Cost)",
                        "COA#"+Environment.NewLine+"(COGM)",
                        
                        //11-15
                        "COA Description"+Environment.NewLine+"(COGM)",
                        "COA#"+Environment.NewLine+"(Sales)",
                        "COA Description"+Environment.NewLine+"(Sales)",
                        "COA#"+Environment.NewLine+"(COGS)",
                        "COA Description"+Environment.NewLine+"(COGS)",
                        
                        //16-20
                        "COA#"+Environment.NewLine+"(Sales Return)",
                        "COA Description"+Environment.NewLine+"(Sales Return)",
                        "COA#"+Environment.NewLine+"(Purchase Return)",
                        "COA Description"+Environment.NewLine+"(Purchase Return)",
                        "COA#"+Environment.NewLine+"(AP Uninvoiced)",
                        
                        //21-25
                        "COA Description"+Environment.NewLine+"(AP Uninvoiced)",
                        "COA#"+Environment.NewLine+"(AP Invoiced)",
                        "COA Description"+Environment.NewLine+"(AP Invoiced)",
                        "COA#"+Environment.NewLine+"(AR)",
                        "COA Description"+Environment.NewLine+"(AR)",

                        //26-30
                        "COA#"+Environment.NewLine+"(AR Uninvoiced)",
                        "COA Description"+Environment.NewLine+"(AR Uninvoiced)",
                        "COA#"+Environment.NewLine+"(Stock Beginning)",
                        "COA Description"+Environment.NewLine+"(Stock Beginning)",
                        "Created By",
                       
                        //31 - 33
                        "Created Date",
                        "Created Time",
                        "Last Updated By", 
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    { 
                        50, 
                        100, 250, 60, 60, 100, 
                        100, 200, 100, 200, 100, 
                        200, 100, 200, 100, 200, 
                        100, 200, 100, 200, 100,
                        200, 100, 200, 100, 200, 
                        100, 200, 100, 200, 130,
                        130, 130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 31, 34 });
            Sm.GrdFormatTime(Grd1, new int[] { 32, 35 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 30, 31, 32, 33, 34, 35 }, false);
            if (!mFrmParent.mIsMovingAvgEnabled) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            if (!mFrmParent.mIsItemCategoryUseCOAAPAR) Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25, 26, 27 }, false);
            if (!mFrmParent.mIsItemCategoryUseCOAStockBeginning) Sm.GrdColInvisible(Grd1, new int[] { 28, 29 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 30, 31, 32, 33, 34, 35 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCtName.Text, new string[] { "ItCtCode", "ItCtName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By ItCtName;",
                    new string[]
                    {
                        //0
                        "ItCtCode", 
                            
                        //1-5
                        "ItCtName", "ActInd", "WastedInd", "MovingAvgInd", "AcNo", 
                        
                        //6-10
                        "AcDesc", "AcNo2", "AcDesc2", "AcNo3", "AcDesc3", 
                        
                        //11-15
                        "AcNo4", "AcDesc4", "AcNo5", "AcDesc5", "AcNo6", 
                        
                        //16-20
                        "AcDesc6", "AcNo7", "AcDesc7", "AcNo8", "AcDesc8", 
                        
                        //21-25
                        "AcNo9", "AcDesc9", "AcNo10", "AcDesc10", "AcNo11", 

                        //26-30
                        "AcDesc11", "AcNo12", "AcDesc12", "CreateBy", "CreateDt",

                        //31-32
                        "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 30);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 32);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 35, 32);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                //Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
