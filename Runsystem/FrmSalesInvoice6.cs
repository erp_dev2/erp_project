﻿#region Update
/*
    27/11/2019 [TKG/SIER] New application (1 do sumber hanya boleh 1 si, belum ada validasi di SI-nya, karena belum ada contoh untuk pola input SI nya)
    06/04/2020 [TKG/SIER] ubah proses copy berdasarkan customer/whs/shipping address/currency
    27/07/2022 [ICA/SIER] menambah tab disc and cost dan menambah field advance charge, menyamakan rumus jurnal dengan salesinvoice3
    03/08/2022 [ICA/SIER] bug masih ada duplicate data di sales invoice detail karena hanya filter by shipping address di process2 (sebelum penambahan advance charge)
    09/08/2022 [ICA/SIER] Ubah method ketika menghitung COA, Tax dan Amt karena Advance charge bisa berupa percentase
                            Menambah kolom Customer Category dan Total Amt. 
    17/11/2022 [VIN/SIER] BUG SaveData -> SaveSalesInvoiceDtl2 
    12/04/2023 [VIN/SIER] tambah validasi final/draft 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice6 : RunSystem.FrmBase15
    {
        #region Field

        internal string 
            mMenuCode = string.Empty, 
            mMainCurCode = string.Empty, 
            mAccessInd = string.Empty,
            mSLI3TaxCalculationFormat = string.Empty, 
            CustomerAcNoARType = string.Empty,
            mSalesInvoice3JournalFormula = string.Empty;
        private string
            CustomerAcNoAR = string.Empty;
        private bool 
            mIsAutoJournalActived = false, 
            mIsSalesInvoice3TaxEnabled = false,
            mIsSalesInvoice3COANonTaxable = false,
            mIsSalesInvoice3JournalUseItCtSalesEnabled = false,
            mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = false,
            mIsCopyingSLIShowAdditionalInformation = false,
            mIsSLI3UseAdvanceCharge = false,
            mSITaxRoundingDown = false
            ;
        internal bool
            mIsDOCtAmtRounded = false,
            mIsDOCtUseProcessInd = false;
        private decimal 
            mAdditionalCostDiscAmt = 0m,
            COAAmt = 0m,
            CustomerAcNoARAmt = 0m,
            AmtSLI = 0m,
            TotalTaxSLI = 0m;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSalesInvoice6(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueTaxCode(ref LueTaxCode1);
                TcSalesInvoice3.SelectedTabPage = Tp3;
                SetLueAdvanceCharge(ref LueAdvanceChargeCode);
                SetLueAdvanceCharge(ref LueAdvanceChargeCode2);
                SetLueAdvanceCharge(ref LueAdvanceChargeCode3);
                Sl.SetLueOption(ref LueOption, "AccountDescriptionOnSalesInvoice");
                LueOption.Visible = false;
                TcSalesInvoice3.SelectedTabPage = Tp2;

                if(mIsCopyingSLIShowAdditionalInformation)
                {
                    if(!mIsSLI3UseAdvanceCharge)
                    {
                        AdvanceCharge.Visible = AdvanceCharge2.Visible = AdvanceCharge3.Visible = false;
                        LueAdvanceChargeCode.Visible = LueAdvanceChargeCode2.Visible = LueAdvanceChargeCode3.Visible = false;
                        panel9.Visible = false;
                        Grd3.Dock = DockStyle.Fill;
                    }
                }
                else
                {
                    Tp3.PageVisible = false;
                }

                ClearData();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mSLI3TaxCalculationFormat = Sm.GetParameter("SLI3TaxCalculationFormat");
            mIsSalesInvoice3TaxEnabled = Sm.GetParameterBoo("IsSalesInvoice3TaxEnabled");
            mIsSalesInvoice3COANonTaxable = Sm.GetParameterBoo("IsSalesInvoice3COANonTaxable");
            mIsSalesInvoice3JournalUseItCtSalesEnabled = Sm.GetParameterBoo("IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled");
            mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = Sm.GetParameterBoo("IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled");
            mSalesInvoice3JournalFormula = Sm.GetParameter("SalesInvoice3JournalFormula");
            mSITaxRoundingDown = Sm.GetParameterBoo("SITaxRoundingDown");
            mIsSLI3UseAdvanceCharge = Sm.GetParameterBoo("IsSLI3UseAdvanceCharge");
            mIsCopyingSLIShowAdditionalInformation = Sm.GetParameterBoo("IsCopyingSLIShowAdditionalInformation");
            if (mSLI3TaxCalculationFormat.Length == 0) mSLI3TaxCalculationFormat = "1";
            if (mSalesInvoice3JournalFormula.Length == 0) mSalesInvoice3JournalFormula = "1";
            mIsDOCtUseProcessInd = Sm.GetParameterBoo("IsDOCtUseProcessInd");

        }

        private void SetGrd()
        {
            #region Grd2

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Processed",
                        "Warning",
                        "Document#", 
                        "",
                        "Date", 

                        //6-10
                        "Customer",
                        "Customer Category",
                        "Warehouse",
                        "Shipping Address",
                        "Total Amount",

                        //11-14
                        "Remark",
                        "Customer's Code",
                        "Currency",
                        "Warehouse Code"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //1-5
                        80, 200, 150, 20, 100,
                        
                        //6-10
                        200, 200, 200, 300, 150, 

                        //11-15
                        250, 0, 0, 0
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 1 });
            Sm.GrdColButton(Grd2, new int[] { 0, 4 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDate(Grd2, new int[] { 5 });
            Sm.GrdFormatDec(Grd2, new int[] { 10 }, 0);
            Sm.SetGrdProperty(Grd2, false);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 14;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        "",
                        //11-13
                        "Local Name", 
                        "Lue Source",
                        "%"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20,
                        //
                        150, 150, 50
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10, 13 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2, 7, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10, 12 }, false);
            Grd3.Cols[11].Visible = mIsSLI3UseAdvanceCharge;
            Grd3.Cols[11].Move(3);
            Grd3.Cols[13].Move(8);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                 DteDueDt, LueTaxCode1, LueAdvanceChargeCode, LueAdvanceChargeCode2, LueAdvanceChargeCode3
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Grd2.Rows.AutoHeight();
            Grd3.Rows.AutoHeight();
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                Process();

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                Sm.SetDteCurrentDate(DteDocDt);
                ClearData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice6Dlg(this));
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmSalesInvoice6Dlg(this));
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmDOCt("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmSalesInvoice6Dlg2(this));
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoice6Dlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sl.SetLueOption(ref LueOption, "AccountDescriptionOnSalesInvoice");
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4) > 0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6) > 0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
            {
                //ComputeAmt();
                //ComputeAmtFromTax();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            //ComputeAmt();
            //ComputeAmtFromTax();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnSave);
        }


        #endregion

        #region Additional Method

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document's date") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, r, 3, false, "DO# is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, r, 9, false, "Shipping address is empty.")) return true;
                    if (Sm.GetGrdStr(Grd2, r, 3).Length > 0 && !Sm.GetGrdBool(Grd2, r, 1))
                    {
                        if (IsDocumentAlreadyProcessedToSalesInvoice(r) ||
                            IsDocumentAlreadyCancelled(r))
                            return true;
                    }
                }
            }
            return false;
        }

        private void Process()
        {
            var l1 = new List<Result1>();
            var l2 = new List<Result2>();

            string SAAddressTemp = string.Empty, CtCodeTemp = string.Empty, CurCodeTemp = string.Empty, WhsCodeTemp = string.Empty;
            bool IsExists = false;

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 3).Length > 0 && !Sm.GetGrdBool(Grd2, r, 1))
                {
                    IsExists = false;
                    SAAddressTemp = Sm.GetGrdStr(Grd2, r, 9);
                    CtCodeTemp = Sm.GetGrdStr(Grd2, r, 12);
                    CurCodeTemp = Sm.GetGrdStr(Grd2, r, 13);
                    WhsCodeTemp = Sm.GetGrdStr(Grd2, r, 14);
                    if (l1.Count > 0)
                    {
                        foreach (var x in l1.Where(w => 
                            Sm.CompareStr(w.SAAddress, SAAddressTemp) &&
                            Sm.CompareStr(w.CtCode, CtCodeTemp) &&
                            Sm.CompareStr(w.CurCode, CurCodeTemp) &&
                            Sm.CompareStr(w.WhsCode, WhsCodeTemp)
                            ))
                        {
                            IsExists = true;
                            break;
                        }
                    }
                    if (!IsExists)
                    {
                        l1.Add(new Result1()
                        {
                            SAAddress = SAAddressTemp,
                            CtCode = CtCodeTemp,
                            CurCode = CurCodeTemp,
                            WhsCode = WhsCodeTemp
                        });
                    }
                                        
                }
            }
            if (l1.Count() > 0)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 3).Length > 0 && !Sm.GetGrdBool(Grd2, r, 1))
                    {
                        l2.Add(new Result2()
                        {
                            SAAddress = Sm.GetGrdStr(Grd2, r, 9),
                            CtCode = Sm.GetGrdStr(Grd2, r, 12),
                            CurCode = Sm.GetGrdStr(Grd2, r, 13),
                            WhsCode = Sm.GetGrdStr(Grd2, r, 14),
                            DOCtDocNo = Sm.GetGrdStr(Grd2, r, 3)
                        });
                    }
                }

                foreach (var x in l1)
                {
                    x.TotalAmt = ComputeTotalAmt(x.SAAddress, x.CtCode, x.CurCode, x.WhsCode);

                    ComputeAmt(x.TotalAmt, x.CtCode);
                    SaveData(x.SAAddress, x.CtCode, x.CurCode, x.WhsCode, x.TotalAmt, ref l2);

                    COAAmt = mAdditionalCostDiscAmt = CustomerAcNoARAmt = AmtSLI = TotalTaxSLI = 0m;
                    CustomerAcNoAR = CustomerAcNoARType = string.Empty;
                }
            }

            l1.Clear();
            l2.Clear();
        }

        private void SaveData(string SAAddress, string CtCode, string CurCode, string WhsCode, decimal TotalAmt, ref List<Result2> l)
        {
            bool IsFirstOrExisted = true;
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @CurrentDt:=CurrentDateTime(); ");
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Set @DocNo:=");
            SQL.AppendLine(Sm.GetNewDocNo(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr", "1"));
            SQL.AppendLine(";");

            #region Insert TblSalesInvoiceDtl
            SQL.AppendLine("Insert Into TblSalesInvoiceDtl ");
            SQL.AppendLine("(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, ");
            SQL.AppendLine("QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("'3', B.DocNo, B.DNo, 'O', ");
            SQL.AppendLine("B.ItCode, B.QtyPackagingUnit, B.Qty, B.UPrice, 1.00, 0.00, B.UPrice, ");
            SQL.AppendLine("@UserCode, @CurrentDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where 1=0 ");

            int i = 0;
            foreach (var x in l
                .Where(w => Sm.CompareStr(w.SAAddress, SAAddress) &&
                            Sm.CompareStr(w.CtCode, CtCode) &&
                            Sm.CompareStr(w.CurCode, CurCode) &&
                            Sm.CompareStr(w.WhsCode, WhsCode))
                .OrderBy(o => o.DOCtDocNo)
                )
            {
                SQL.AppendLine("Or A.DocNo=@DOCtDocNo_"+i.ToString() + " ");
                Sm.CmParam<String>(ref cm, "@DOCtDocNo_" + i.ToString(), x.DOCtDocNo);
                i++;
            }
            SQL.AppendLine("; ");
            #endregion

            #region Insert TblSalesInvoiceHdr
            SQL.AppendLine("Insert Into TblSalesInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, ProcessInd, CtCode, DueDt, CurCode, TotalAmt, TotalTax, Downpayment, Amt, BankAcCode, TaxCode1, TaxCode2, TaxCode3, Remark, ");
            SQL.AppendLine("AdvanceChargeCode, AdvanceChargeCode2, AdvanceChargeCode3, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @DocNo, @DocDt, 'N', 'O', @CtCode, @DueDt, @CurCode, ");
            SQL.AppendLine("@TotalAmt, @TotalTax, 0, @Amt, ");
            SQL.AppendLine("Null, @TaxCode1, Null, Null, Null, ");
            if (mIsSLI3UseAdvanceCharge)
                SQL.AppendLine("@AdvanceChargeCode, @AdvanceChargeCode2, @AdvanceChargeCode3, ");
            else
                SQL.AppendLine("NUll, Null, Null, ");
            SQL.AppendLine("@UserCode, @CurrentDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ");
            SQL.AppendLine("    Sum(Floor(A.Qty*A.UPriceBeforeTax)) As TotalAmt ");
            SQL.AppendLine("    From TblSalesInvoiceDtl A ");
            SQL.AppendLine("    Inner Join TblDOCtHdr B On A.DOCtDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join TblTax T2 On T2.TaxCode=@TaxCode1; ");
            #endregion

            #region Insert TblSalesInvoiceDtl2

            if(Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    string cek = Sm.GetGrdStr(Grd3, r, 1);
                    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                    {
                        if (IsFirstOrExisted)
                        {
                            SQL.AppendLine("Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc, AcInd, Remark, ");
                            if (mIsSLI3UseAdvanceCharge)
                                SQL.AppendLine("LocalName, ");
                            SQL.AppendLine("CreateBy, CreateDt) ");
                            SQL.AppendLine("Values ");
                            IsFirstOrExisted = false;
                        }
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine(
                            " (@DocNo, @DNo_" + r.ToString() +
                            ", @AcNo_" + r.ToString() +
                            ", @DAmt_" + r.ToString() +
                            ", @CAmt_" + r.ToString() +
                            ", @OptAcDesc_" + r.ToString() +
                            ", @AcInd_" + r.ToString() +
                            ", @Remark_" + r.ToString());
                        if (mIsSLI3UseAdvanceCharge) SQL.AppendLine(", @LocalName_"+r.ToString());
                        SQL.AppendLine(", @UserCode, @Dt) ");
                    }
                    //else
                    //{
                    //    SQL.AppendLine(", ");
                    //    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @CustomerAcNoAR, if(@CustomerAcNoARType = 'D', @CustomerAcNoARAmt, 0.00), if(@CustomerAcNoARType = 'C', @CustomerAcNoARAmt, 0.00), NULL, 'N', NULL, ");
                    //    if (mIsSLI3UseAdvanceCharge)
                    //        SQL.AppendLine("Null, ");
                    //    SQL.AppendLine(" @UserCode, @Dt) ");
                    //}

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), (Sm.GetGrdBool(Grd3, r, 13) ? Sm.GetGrdDec(Grd3, r, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, r, 4)));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), (Sm.GetGrdBool(Grd3, r, 13) ? Sm.GetGrdDec(Grd3, r, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, r, 6)));
                    Sm.CmParam<String>(ref cm, "@OptAcDesc_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@AcInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 10) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@LocalName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 11));

                }
                if (!IsFirstOrExisted) SQL.AppendLine("; ");
            }

            #endregion

            SQL.AppendLine("Update TblDOCtDtl A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
            SQL.AppendLine("    And B.DocType='3' ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Set A.ProcessInd='F' ");
            SQL.AppendLine("Where A.ProcessInd='O'; ");

            #region Save Journal

            if (mIsAutoJournalActived)
            {
                SQL.AppendLine("Set @EntCode:=( ");
                SQL.AppendLine("    Select Distinct C.EntCode ");
                SQL.AppendLine("    From TblWarehouse A, TblCostCenter B, TblProfitCenter C ");
                SQL.AppendLine("    Where A.CCCode=B.CCCode ");
                SQL.AppendLine("    And B.ProfitCenterCode=C.ProfitCenterCode ");
                SQL.AppendLine("    And A.WhsCode=@WhsCode Limit 1);");

                SQL.AppendLine("Set @JournalDocNo:=");
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
                SQL.AppendLine(";");

                SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
                SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
                SQL.AppendLine("Concat('Sales Invoice : ', @DocNo), ");
                SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
                SQL.AppendLine("Null, @UserCode, @CurrentDt); ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, @EntCode As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                #region Piutang Usaha
                if (mSLI3TaxCalculationFormat == "1")
                {
                    #region Default
                    if (Sm.CompareStr(mMainCurCode, CurCode))
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        A.TotalAmt+A.TotalTax-A.Downpayment As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        if (mIsDOCtAmtRounded)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment)) As DAmt, ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment) As DAmt, ");
                        }

                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    #endregion
                }
                else
                {
                    #region SRN
                    if (Sm.CompareStr(mMainCurCode, CurCode))
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        SQL.AppendLine("        A.Amt As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                        if (mIsDOCtAmtRounded)
                        {
                            SQL.AppendLine("        Floor(IfNull((( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        )) * ");
                            SQL.AppendLine("        (A.Amt), 0)) As DAmt, ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0) * ");
                            SQL.AppendLine("        (A.Amt) As DAmt, ");
                        }
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                        SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    #endregion
                }
                #endregion

                #region Piutang Uninvoice
                if (mIsSalesInvoice3JournalUseItCtSalesEnabled)
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select F.AcNo4, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded) SQL.AppendLine("     Floor(");
                    SQL.AppendLine("        D.UPrice*D.Qty* ");
                    SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=C.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) ");
                    SQL.AppendLine("        End ");
                    if (mIsDOCtAmtRounded) SQL.AppendLine(" )");
                    SQL.AppendLine("        As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DOCtDocNo=C.DocNo  ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
                    SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode  ");
                    SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo4 Is Not Null  ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    if (mSLI3TaxCalculationFormat == "1")
                    {
                        #region Default

                        if (Sm.CompareStr(mMainCurCode, CurCode))
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            SQL.AppendLine("        A.TotalAmt As CAmt ");
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("    Union All ");

                            #region Old Code, ngga perlu parameter kata ian via WA Chat ke wedha tanggal 22 des 2020 jam 14:57
                            //SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            //SQL.AppendLine("        0.00 As DAmt, ");
                            //if (mIsDOCtAmtRounded)
                            //{
                            //    SQL.AppendLine("        Floor(IfNull((");
                            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            //    SQL.AppendLine("        ), 0) * ");
                            //    SQL.AppendLine("        A.TotalAmt) As CAmt ");
                            //}
                            //else
                            //{
                            //    SQL.AppendLine("        IfNull(( ");
                            //    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            //    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            //    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            //    SQL.AppendLine("        ), 0) * ");
                            //    SQL.AppendLine("        A.TotalAmt As CAmt ");
                            //}
                            //SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            //SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            //SQL.AppendLine("        Where A.DocNo=@DocNo ");
                            #endregion

                            SQL.AppendLine("        Select Concat(D.Parvalue, E.CtCode) As AcNo, 0.00 As DAmt, ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("        Floor( ");

                            SQL.AppendLine("            Sum( ");
                            SQL.AppendLine("            IfNull( ");
                            SQL.AppendLine("                 ( ");
                            SQL.AppendLine("                      Select Amt From TblCurrencyRate ");
                            if (!mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                                SQL.AppendLine("                      Where RateDt <= E.DocDt And CurCode1 = E.CurCode And CurCode2 = @MainCurCode ");
                            else
                                SQL.AppendLine("                      Where RateDt <= C.DocDt And CurCode1 = E.CurCode And CurCode2 = @MainCurCode ");
                            SQL.AppendLine("                      Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("                ) ");
                            SQL.AppendLine("            , 0.00)  ");
                            SQL.AppendLine("            * (A.QtyPackagingUnit * A.UPriceBeforeTax)) ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("        ) ");
                            SQL.AppendLine("            As CAmt ");
                            SQL.AppendLine("        From TblSalesInvoiceDtl A ");
                            SQL.AppendLine("        Inner Join TblDOCtDtl B ");
                            SQL.AppendLine("            On A.DOCtDocNo = B.DocNo ");
                            SQL.AppendLine("            And A.DOCtDNo = B.DNo ");
                            SQL.AppendLine("            And A.DocNo = @DocNo ");
                            SQL.AppendLine("        Inner Join TblDOCtHdr C On B.DocNo = C.DocNo ");
                            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'CustomerAcNoNonInvoice' And D.ParValue Is Not Null ");
                            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr E On A.DocNo = E.DocNo ");
                            SQL.AppendLine("        Group By Concat(D.Parvalue, E.CtCode) ");
                        }

                        #endregion
                    }
                    else
                    {
                        #region SRN
                        if (Sm.CompareStr(mMainCurCode, CurCode))
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            SQL.AppendLine("        C.Amt As CAmt ");
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            SQL.AppendLine("        Inner Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select DocNo, ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            Sum( ");
                            SQL.AppendLine("            Floor(Qty * UPriceBeforeTax) ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            ) ");
                            SQL.AppendLine("            As Amt ");
                            SQL.AppendLine("            From TblSalesInvoiceDtl ");
                            SQL.AppendLine("            Where DocNo = @DocNo ");
                            SQL.AppendLine("            Group By DocNo ");
                            SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        else
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, ");
                            if (mIsDOCtAmtRounded)
                            {
                                SQL.AppendLine("        Floor(IfNull((");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        C.Amt) As CAmt ");
                            }
                            else
                            {
                                SQL.AppendLine("        IfNull(( ");
                                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                                SQL.AppendLine("        ), 0) * ");
                                SQL.AppendLine("        C.Amt As CAmt ");
                            }
                            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                            SQL.AppendLine("        Inner Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                            SQL.AppendLine("            From TblSalesInvoiceDtl ");
                            SQL.AppendLine("            Where DocNo = @DocNo ");
                            SQL.AppendLine("            Group By DocNo ");
                            SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                            SQL.AppendLine("        Where A.DocNo=@DocNo ");
                        }
                        #endregion
                    }
                }
                #endregion

                #region PPN Keluaran
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt1)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt1) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt2)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt2) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TaxAmt3)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TaxAmt3) Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Floor(Abs(A.TotalAmt*B.TaxRate*0.01)) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then Floor(A.TotalAmt*B.TaxRate*0.01) Else 0.00 End As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }

                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");

                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt1) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt1 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt2) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt2 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Case When B.TaxRate>=0.00 Then B.AcNo2 Else B.AcNo1 End As AcNo, ");
                    if (mIsDOCtAmtRounded || mSITaxRoundingDown)
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End) As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End) As DAmt, ");
                            SQL.AppendLine("        Floor(IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End) As CAmt ");
                        }
                    }
                    else
                    {
                        if (mIsSalesInvoice3COANonTaxable)
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TaxAmt3) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TaxAmt3 Else 0.00 End As CAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then 0.00 Else Abs(A.TotalAmt*B.TaxRate*0.01) End As DAmt, ");
                            SQL.AppendLine("        IfNull(( ");
                            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("        ), 0.00)* ");
                            SQL.AppendLine("        Case When A.TotalAmt*B.TaxRate*0.01>=0.00 Then A.TotalAmt*B.TaxRate*0.01 Else 0.00 End As CAmt ");
                        }
                    }
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
                }
                #endregion

                #region List COA

                if (mSalesInvoice3JournalFormula == "1")
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo, ");
                    if (!Sm.CompareStr(mMainCurCode, CurCode))
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                    }
                    SQL.AppendLine("        B.DAmt, ");
                    if (!Sm.CompareStr(mMainCurCode, CurCode))
                    {
                        SQL.AppendLine("        IfNull(( ");
                        SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("        ), 0.00) * ");
                    }
                    SQL.AppendLine("        B.CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl2 B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    if (mSLI3TaxCalculationFormat != "1")
                    {
                        SQL.AppendLine("        And B.AcNo <> @CustomerAcNoAR ");
                    }
                }

                #endregion

                #region Laba rugi selisih kurs

                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select ParValue As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
                }

                #endregion


                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine("    Where AcNo Is Not Null ");
                SQL.AppendLine("    Group By AcNo  ");
                SQL.AppendLine(") T;  ");

                if (!Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("Update TblJournalDtl A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select DAmt, CAmt From (");
                    SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                    SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine(") B On 0=0 ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                    SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                    SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                    SQL.AppendLine("And A.AcNo In ( ");
                    SQL.AppendLine("    Select ParValue From TblParameter ");
                    SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                    SQL.AppendLine("    And ParValue Is Not Null ");
                    SQL.AppendLine("    );");
                }

                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            }

            #endregion

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@CurCode", CurCode);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoAR", CustomerAcNoAR);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoARType", CustomerAcNoARType);
            Sm.CmParam<decimal>(ref cm, "@AdditionalCostDiscAmt", mAdditionalCostDiscAmt);
            Sm.CmParam<decimal>(ref cm, "@COAAmt", COAAmt);
            Sm.CmParam<decimal>(ref cm, "@CustomerAcNoARAmt", CustomerAcNoARAmt);
            Sm.CmParam<decimal>(ref cm, "@TotalAmt", TotalAmt + COAAmt);
            Sm.CmParam<decimal>(ref cm, "@TotalTax", TotalTaxSLI);
            Sm.CmParam<decimal>(ref cm, "@Amt", AmtSLI);
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", Sm.GetLue(LueAdvanceChargeCode));
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode2", Sm.GetLue(LueAdvanceChargeCode2));
            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode3", Sm.GetLue(LueAdvanceChargeCode3));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            

            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);

        }

        private decimal ComputeTotalAmt(string SAAddress, string CtCode, string CurCode, string WhsCode)
        {
            string DocNo = string.Empty;
            decimal Amt = 0m;

            for (int row = 0; row < Grd2.Rows.Count - 1; row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 9), SAAddress)
                    && Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 12), CtCode)
                    && Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 13), CurCode)
                    && Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 14), WhsCode))
                {
                    Amt += Sm.GetGrdDec(Grd2, row, 10);
                }
            }
            return Amt;
        }

        private void ComputeAmt(decimal TotalAmt, string CtCode)
        {
            decimal debit = 0m, credit = 0m, TaxRate = 0m;
            COAAmt = mAdditionalCostDiscAmt = CustomerAcNoARAmt = AmtSLI = TotalTaxSLI = 0m;
            CustomerAcNoAR = CustomerAcNoARType = string.Empty;

            CustomerAcNoAR =
                Sm.GetValue(
                        "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
                        "From TblCustomer A, TblCustomerCategory B " +
                        "Where A.CtCtCode is Not Null " +
                        "And A.CtCtCode=B.CtCtCode " +
                        "And A.CtCode='" + CtCode + "' Limit 1; "
                        );
            TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode = @Param", Sm.GetLue(LueTaxCode1)); 

            if (mSLI3TaxCalculationFormat == "1")
            {
                #region Default
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    string AcType = Sm.GetValue(
                        "Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd3, Row, 1) + "';");
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        if (AcType == "D")
                            COAAmt += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 4));
                        else
                            COAAmt -= (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 4));
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        if (AcType == "C")
                            COAAmt += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 6));
                        else
                            COAAmt -= (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 6));
                    }
                }
                #endregion
            }
            else //SRN, pilihan additional cost & discount nya ada yg masuk hitungan atau enggak
            {
                #region SRN

                mAdditionalCostDiscAmt = 0m;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    //kalau dicentang di debit, (-)
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        COAAmt -= (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 4));
                    }
                    //kalau dicentang di kredit, (+)
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        COAAmt += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 6));
                    }

                    //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                    if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        mAdditionalCostDiscAmt -= (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 4));
                        mAdditionalCostDiscAmt += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 6));
                    }
                }

                #endregion
            }

            TotalAmt = TotalAmt + COAAmt;

            if (mIsSalesInvoice3TaxEnabled)
            {
                if (mSLI3TaxCalculationFormat == "1")
                {
                    if (mIsSalesInvoice3COANonTaxable)
                    {
                        if (Sm.GetLue(LueTaxCode1).Length > 0) TotalTaxSLI = TaxRate * 0.01m * (TotalAmt - COAAmt);
                    }
                    else
                    {
                        if (Sm.GetLue(LueTaxCode1).Length > 0) TotalTaxSLI = TaxRate * 0.01m * TotalAmt;
                    }
                }
                else
                    if (Sm.GetLue(LueTaxCode1).Length > 0) TotalTaxSLI = TaxRate * 0.01m * TotalAmt;
            }
            else
            {
                if (Sm.GetLue(LueTaxCode1).Length > 0) TotalTaxSLI = TaxRate * 0.01m * TotalAmt;
            }

            if (mIsDOCtAmtRounded)
            {
                TotalTaxSLI = Decimal.Truncate(TotalTaxSLI);
                TotalAmt = Decimal.Truncate(TotalAmt);
            }

            TotalTaxSLI = (mSITaxRoundingDown == true ? Sm.RoundDown(TotalTaxSLI, 0) : TotalTaxSLI);

            AmtSLI = TotalAmt + TotalTaxSLI;

            if (mSLI3TaxCalculationFormat != "1")
            {
                // ditambahkan nilai additional cost discount yg ga dicentang
                AmtSLI = AmtSLI + mAdditionalCostDiscAmt;
            }

            #region compute CustomerAcNoARAmt
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                debit += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 4)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 4));
                credit += (Sm.GetGrdBool(Grd3, Row, 13) ? Sm.GetGrdDec(Grd3, Row, 6)/100 * TotalAmt : Sm.GetGrdDec(Grd3, Row, 6));
            }

            if (debit != credit)
                CustomerAcNoARType = (debit < credit ? "D" : "C");

            CustomerAcNoARAmt = COAAmt + mAdditionalCostDiscAmt;
            #endregion
        }

        private bool IsDocumentAlreadyProcessedToSalesInvoice(int r)
        {
            var Msg = string.Empty;
            var SQL = new StringBuilder();

            Grd2.Cells[r, 2].Value = string.Empty;

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSalesInvoiceHdr T1, TblSalesInvoiceDtl T2 ");
            SQL.AppendLine("Where T1.DocNo=T2.DocNo And T2.DOCtDocNo=@Param And T1.CancelInd='N'; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, r, 3)))
            {
                Msg =
                    "Document# : " + Sm.GetGrdStr(Grd2, r, 3) + Environment.NewLine +
                    "Warehouse : " + Sm.GetGrdStr(Grd2, r, 8) + Environment.NewLine +
                    "Customer : " + Sm.GetGrdStr(Grd2, r, 6) + Environment.NewLine + Environment.NewLine +
                    "This DO already processed to sales invoice.";
                Sm.StdMsg(mMsgType.Warning, Msg);
                Grd2.Cells[r, 1].Value = true;
                Grd2.Cells[r, 2].Value = "Already processed to sales invoice.";
                Grd2.Rows.AutoHeight();
                Sm.FocusGrd(Grd2, r, 2);
                return true;
            }
            return false;
        }

        private bool IsDocumentAlreadyCancelled(int r)
        {
            var Msg = string.Empty;
            var SQL = new StringBuilder();

            Grd2.Cells[r, 2].Value = string.Empty;

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOCtDtl ");
            SQL.AppendLine("Where DocNo=@Param And CancelInd='N'; ");

            if (!Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd2, r, 3)))
            {
                Msg =
                    "Document# : " + Sm.GetGrdStr(Grd2, r, 3) + Environment.NewLine +
                    "Warehouse : " + Sm.GetGrdStr(Grd2, r, 8) + Environment.NewLine +
                    "Customer : " + Sm.GetGrdStr(Grd2, r, 6) + Environment.NewLine + Environment.NewLine +
                    "This DO already cancelled.";
                Sm.StdMsg(mMsgType.Warning, Msg);
                Grd2.Cells[r, 1].Value = false;
                Grd2.Cells[r, 2].Value = "Already cancelled.";
                Grd2.Rows.AutoHeight();
                Sm.FocusGrd(Grd2, r, 2);
                return true;
            }
            return false;
        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueAdvanceCharge(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.AdvanceChargeCode As Col1, A.AdvanceChargeName As Col2 ");
                SQL.AppendLine("From TblAdvanceCharge A ");
                SQL.AppendLine("INNER JOIN TblParameter B ON B.Parcode = 'CustomerAcNoAR' ");
                SQL.AppendLine("Where ActInd = 'Y' ");
                //SQL.AppendLine("	AND A.PercentageInd = 'N' ");
                SQL.AppendLine("	AND (B.Parvalue IS NOT NULL AND A.Acno not LIKE CONCAT(B.Parvalue, '%')); ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetCOAAcNoInGrid(string Lue, string AdvanceChargeCode, int LastRow)
        {
            bool IsDataLueExisted = false;
            int DataLueExistedRow = 0;
            for(int row = 0; row<Grd3.Rows.Count-1;row++)
            {
                if (Sm.GetGrdStr(Grd3, row, 12) == Lue)
                {
                    IsDataLueExisted = true;
                    DataLueExistedRow = row;
                }
            }

            if (IsDataLueExisted) LastRow = DataLueExistedRow;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                SQL.AppendLine("SELECT A.AcNo, B.AcDesc, A.LocalName, A.AcType, A.Value, A.PercentageInd ");
                SQL.AppendLine("FROM TblAdvanceCharge A ");
                SQL.AppendLine("INNER JOIN TblCOA B ON A.AcNo = B.AcNo ");
                SQL.AppendLine("WHERE A.AdvanceChargeCode = @AdvanceChargeCode ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", AdvanceChargeCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "LocalName", "AcType", "Value", "PercentageInd" });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 1, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 2, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row + LastRow, 11, 2);
                        Sm.SetGrdValue("B", Grd3, dr, c, Row + LastRow, 13, 5);
                        Grd3.Cells[Row + LastRow, 4].Value = Sm.DrStr(dr, c[3]) == "D" ? Sm.DrDec(dr, c[4]) : 0m;
                        Grd3.Cells[Row + LastRow, 6].Value = Sm.DrStr(dr, c[3]) == "C" ? Sm.DrDec(dr, c[4]) : 0m;

                        Grd3.Cells[Row + LastRow, 11].ReadOnly = iGBool.True;
                        Grd3.Cells[Row + LastRow, 11].BackColor = Color.FromArgb(224, 224, 224);
                        Grd3.Cells[Row + LastRow, 12].Value = Lue;
                        Sm.SetGrdBoolValueFalse(Grd3, Row + LastRow, new int[] { 3, 5 });

                        if(!IsDataLueExisted)
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
                            Sm.SetGrdNumValueZero(Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                        }

                        Row += 1;
                    }
                }
                dr.Close();
            }
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void LueAdvanceChargeCode_EditValueChanged(object sender, EventArgs e)
        {
            SetCOAAcNoInGrid("LueAdvanceCharge", Sm.GetLue(LueAdvanceChargeCode), Grd3.Rows.Count - 1);
        }

        private void LueAdvanceChargeCode2_EditValueChanged(object sender, EventArgs e)
        {
            SetCOAAcNoInGrid("LueAdvanceCharge2", Sm.GetLue(LueAdvanceChargeCode2), Grd3.Rows.Count - 1);
        }

        private void LueAdvanceChargeCode3_EditValueChanged(object sender, EventArgs e)
        {
            SetCOAAcNoInGrid("LueAdvanceCharge3", Sm.GetLue(LueAdvanceChargeCode3), Grd3.Rows.Count - 1);
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue2(Sl.SetLueOption), string.Empty);
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result1
        {
            public string SAAddress { get; set; }
            public string CtCode { get; set; }
            public string CurCode { get; set; }
            public string WhsCode { get; set; }
            public string CustomerAcNoAR { get; set; }
            public decimal TotalAmt { get; set; }
        }

        private class Result2
        {
            public string SAAddress { get; set; }
            public string CtCode { get; set; }
            public string CurCode { get; set; }
            public string WhsCode { get; set; }
            public string DOCtDocNo { get; set; }
        }

        #endregion
    }
}
