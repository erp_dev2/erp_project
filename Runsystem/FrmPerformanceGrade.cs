﻿#region Update
/*
    23/08/2018 [WED] BUG saat save GrdProsentaseStart & GrdProsentaseEnd, save nya masih sebagai string
    21/04/2022 [TYO/HIN] Hide label5 dan label6 berdasarkan parameter PerformanceGradeFormat
    30/09/2022 [TYO/HIN] Menyesuaikan validasi untuk Prosentase Start dan Prosentase End
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceGrade : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPerformanceGradeFind FrmFind;

        private string mPerformanceGradeFormat = string.Empty;
        #endregion

        #region Constructor

        public FrmPerformanceGrade(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();

            if (mPerformanceGradeFormat == "1")
                label5.Visible = label6.Visible = true;
            else
                label5.Visible = label6.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGradeCode, TxtGrdProsentaseStart, TxtGrdProsentaseEnd, TxtStatus
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtGradeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtGradeCode, TxtGrdProsentaseStart, TxtGrdProsentaseEnd, TxtStatus
                    }, false);
                    ChkActiveInd.Checked = true;
                    TxtGradeCode.Focus();
                    break;
                case mState.Edit:
                   Sm.SetControlReadOnly(TxtGradeCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrdProsentaseStart,TxtGrdProsentaseEnd, TxtStatus
                    }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    TxtGrdProsentaseStart.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtGradeCode, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtGrdProsentaseStart, TxtGrdProsentaseEnd
            }, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mPerformanceGradeFormat = Sm.GetParameter("PerformanceGradeFormat");
        }
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPerformanceGradeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGradeCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGradeCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPerformancegrade Where GrdCode=@GrdCode" };
                Sm.CmParam<String>(ref cm, "@GrdCode", TxtGradeCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPerformanceGrade(GrdCode, GrdProsentaseStart, GrdProsentaseEnd, ActInd, GrdStatus, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@GrdCode, @GrdProsentaseStart, @GrdProsentaseEnd, @ActInd, @GrdStatus, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update GrdProsentaseStart=@GrdProsentaseStart, GrdProsentaseEnd=@GrdProsentaseEnd, ActInd=@ActInd, GrdStatus=@GrdStatus, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@GrdCode", TxtGradeCode.Text);
                Sm.CmParam<Decimal>(ref cm, "@GrdProsentaseStart", Decimal.Parse(TxtGrdProsentaseStart.Text));
                Sm.CmParam<Decimal>(ref cm, "@GrdProsentaseEnd", Decimal.Parse(TxtGrdProsentaseEnd.Text));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@GrdStatus", TxtStatus.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtGradeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string GrdCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@GrdCode", GrdCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From tblperformancegrade Where GrdCode = @GrdCode",
                        new string[] 
                        {
                            "GrdCode", "GrdProsentaseStart","GrdProsentaseEnd", "GrdStatus", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtGradeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtGrdProsentaseStart.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
                            TxtGrdProsentaseEnd.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                            TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGradeCode, "Grade Code", false) ||
                IsGrdProsentaseZero() ||
                Sm.IsTxtEmpty(TxtStatus, "Status", false) ||
                IsGradeCodeExisted();
        }

        private bool IsGrdProsentaseZero()
        {
            bool Result = true;
            if (mPerformanceGradeFormat == "1")
            {
                bool Start = Sm.IsTxtEmpty(TxtGrdProsentaseStart, "Prosentase Start", true);
                bool End = Sm.IsTxtEmpty(TxtGrdProsentaseEnd, "Prosentase End", true);
                if (Start == false && End == false)
                    Result = false;
                else
                    Result = true;
            }
            else
                Result = false;
            
            return Result;
        }

        private bool IsGradeCodeExisted()
        {
            if (!TxtGradeCode.Properties.ReadOnly && Sm.IsDataExist("Select GrdCode From TblPerformanceGrade Where GrdCode ='" + TxtGradeCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Grade code ( " + TxtGradeCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGrdProsentaseStart_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtGrdProsentaseStart, 0);
        }

        private void TxtGrdProsentaseEnd_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtGrdProsentaseEnd, 0);
        }

        private void TxtGradeCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGradeCode);
        }

        private void ChkActiveInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkActiveInd.Checked == true)
                {
                    string ActiveInd = Sm.GetValue("SELECT A.ActInd FROM TblPerformanceGrade A WHERE A.GrdCode = '" + TxtGradeCode.Text + "'");
                    if (ActiveInd == "N")
                    {
                        Sm.StdMsg(mMsgType.Warning, "Performance Grade Can't Set To Active Because It's Already Not Active");
                        ChkActiveInd.Checked = false;
                    }
                }

            }
        }

        private void TxtStatus_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtStatus);
        }

        #endregion
        
        #endregion

    }
}
