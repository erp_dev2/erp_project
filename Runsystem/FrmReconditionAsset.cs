﻿#region Update
/*
    24/01/2022 [IBL/PRODUCT] New Apps
    07/02/2022 [IBL/PRODUCT] ADD: Tambahan validasi ketika asset yg dipilih tidak ada recondition date nya.
    08/02/2022 [IBL/PRODUCT] BUG: muncul warning Data truncated for column 'UPrice' at row 1
    06/07/2022 [IBL/PRODUCT] Update Asset Value dan economic life di Master Asset saat diproses di Recondition Asset
    15/07/2022 [IBL/PRODUCT] Jika yg dipilih adalah Initial master maka eco life yg diupdate adalah yg remaining ecolife
    20/07/2022 [IBL/PRODUCT] Jika yg dipilih adl initial master asset maka Asset Acc. Depr. on Current Date akumulasi Depreciation Value di DBA + Acc Depr Opening Balance (Initial master Asset)
    05/08/2022 [IBL/PRODUCT] Bug: Asset's Acc. Depr. on Current Date belum sesuai saat rekondisi ke dua.
    15/08/2022 [IBL/PRODUCT] Bug: Saat rekondisi asset saldo menurun, depreciation rate belum sesuai
    16/08/2022 [TYO/PRODUCT] Inisialisasi mIsFilterByDept
    16/08/2022 [RDA/PRODUCT] Penambahan update CancelByInd
    04/01/2023 [RDA/BBT] penambahan cost center ketika pembentukan journal dan journal cancel
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmReconditionAsset : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private string mReconditionDate = string.Empty;
        private bool
            mIsAutoJournalActived = false,
            mIsReconditionAssetAllowToUploadFile = false,
            mIsReconditionAssetUploadFileMandatory = false
            ;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mDeprMethod = string.Empty
            ;
        private decimal mAssetAnnualPercentageRate = 0m;
        internal bool mIsFilterByDept = false;
        internal FrmReconditionAssetFind FrmFind;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmReconditionAsset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Recondition of Asset";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                if (mIsReconditionAssetAllowToUploadFile &&
                    mIsReconditionAssetUploadFileMandatory

                    ) label22.ForeColor = Color.Red;
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DO to Department#",

                        //1-5
                        "DOD DNo",
                        "",
                        "Item's Code",
                        "Item's Name",
                        "UoM",

                        //6-9
                        "Unit Price",
                        "Quantity",
                        "Amount",
                        "Outstanding Qty"
                    },
                    new int[]
                    {
                        //0
                        150,
 
                        //1-5
                        0, 20, 120, 200, 100, 
                        
                        //6-9
                        120, 100, 130, 0
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 9 });
            #endregion

            #region Grd2
            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 100, 80, 200 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, DteReconditionDate, MeeReconditionReason, TxtEcoLife3, MeeCancelReason,
                        TxtFile
                    }, true);
                    ChkCancelInd.Enabled = false;
                    BtnAssetCode.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsReconditionAssetAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       DteDocDt, DteReconditionDate, MeeReconditionReason, TxtEcoLife3
                    }, false);
                    BtnAssetCode.Enabled = true;
                    if (mIsReconditionAssetAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, TxtReconditionValue, DteDocDt, DteReconditionDate, TxtStatus,
                TxtAssetCategory, TxtAssetCode, TxtAssetName, TxtSiteName, TxtProfitCenter,
                TxtCostCenter, TxtAssetValue1, TxtAssetValue2, TxtAssetNBV1, TxtAssetNBV2,
                TxtResidualValue, TxtEcoLife1, TxtEcoLife2, TxtEcoLife3, MeeReconditionReason,
                TxtFile, MeeCancelReason
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtReconditionValue, TxtAssetValue1, TxtAssetValue2, TxtAssetNBV1, TxtAssetNBV2,
                TxtResidualValue, TxtEcoLife1, TxtEcoLife2, TxtEcoLife3
            }, 0);
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8, 9 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmReconditionAssetFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                TxtStatus.EditValue = "Outstanding";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ReconditionAsset", "TblReconditionAssetHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveReconditionAssetHdr(DocNo));
            cml.Add(SaveReconditionAssetDtl(DocNo));

            if (mIsAutoJournalActived && !IsDocApprovalSettingExists())
                cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);
            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            ShowData(DocNo);
            Sm.StdMsg(mMsgType.Info, "Business Assets Depreciation Document for this asset has been cancelled." + Environment.NewLine +
                "You must recreate the Business Asset Depreciation document for this asset." + Environment.NewLine +
                "Asset's Code : " + TxtAssetCode.Text + Environment.NewLine +
                "ASset's Name : " + TxtAssetName.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAssetCode, "Asset Code", false) ||
                Sm.IsDteEmpty(DteReconditionDate, "Date of Recondition") ||
                IsReconditionDateNotValid() ||
                Sm.IsTxtEmpty(TxtAssetNBV1, "Asset's NBV before Recondition", true) ||
                Sm.IsTxtEmpty(TxtEcoLife3, "Economic Life (Months) After Recondition", true) ||
                Sm.IsTxtEmpty(MeeReconditionReason, "Recondition Reason", false) ||
                IsEcoLifeAfterReconditionInvalid() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                (mIsReconditionAssetAllowToUploadFile && IsFileMandatory()) ||
                (mIsReconditionAssetAllowToUploadFile && IsUploadFileNotValid());
        }

        private bool IsReconditionDateNotValid()
        {
            string ReconditionDt = string.Empty, ReconditionDt2 = string.Empty;

            if (mReconditionDate.Length > 0 && Sm.GetDte(DteReconditionDate).Length > 0)
            {
                ReconditionDt = Sm.Left(Sm.GetDte(DteReconditionDate), 6);
                ReconditionDt2 = Sm.Left(mReconditionDate, 6);

                if (ReconditionDt != ReconditionDt2)
                {
                    Sm.StdMsg(mMsgType.Warning, "Date of Recondition is invalid!");
                    return true;
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "You have to make at least one Accumulation Asset Depreciation Journal for this asset code to fill Date of Recondition!");
                return true;
            }

            return false;
        }

        private bool IsEcoLifeAfterReconditionInvalid()
        {
            decimal EcoLife1 = Decimal.Parse(TxtEcoLife1.Text),
                EcoLife3 = Decimal.Parse(TxtEcoLife3.Text);

            if (EcoLife3 > EcoLife1)
            {
                Sm.StdMsg(mMsgType.Warning, "Economic Life (Months) After Recondition can't be more than"+Environment.NewLine+
                    "Economic Life (Months) on Acquisition Date!");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, true, "Quantity should be bigger than 0.00.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 7) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning,
                    "Quantity should not be bigger than outstanding quantity." + Environment.NewLine+Environment.NewLine +
                    "DOD Document# : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                    "Item's Name   : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Quantity      : " + Sm.GetGrdDec(Grd1, Row, 7) + Environment.NewLine +
                    "Outstanding Quantity : " + Sm.GetGrdStr(Grd1, Row, 9)
                    );
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveReconditionAssetHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            decimal mAssetAnnualPercentageRate2 = ComputeAnnualPercentageRate();

            SQL.AppendLine("Insert Into TblReconditionAssetHdr(DocNo, DocDt, Status, AssetCode, ReconditionDt, ReconditionValue, ");
            SQL.AppendLine("AssetValue1, AssetValue2, NBVValue1, NBVValue2, EcoLife1, EcoLife2, EcoLife3, ");
            SQL.AppendLine("PercentageAnnualDepreciation, PercentageAnnualDepreciation2, ReconditionReason, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @AssetCode, @ReconditionDt, @ReconditionValue, ");
            SQL.AppendLine("@AssetValue1, @AssetValue2, @NBVValue1, @NBVValue2, @EcoLife1, @EcoLife2, @EcoLife3, ");
            SQL.AppendLine("@PercentageAnnualDepreciation, @PercentageAnnualDepreciation2, @ReconditionReason, @CreateBy, CurrentDateTime()); ");

            if (IsDocApprovalSettingExists())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='ReconditionAsset'; ");
            }

            if (!IsDocApprovalSettingExists())
            {
                SQL.AppendLine("Update TblReconditionAssetHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
                SQL.AppendLine("    Select DocNo From TblDocApproval ");
                SQL.AppendLine("    Where DocType='ReconditionAsset' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblDepreciationAssetHdr  ");
                SQL.AppendLine("    Set CancelInd = 'Y', CancelByInd = '2' ");
                SQL.AppendLine("Where CancelInd = 'N' ");
                SQL.AppendLine("And AssetCode = @AssetCode; ");

                SQL.AppendLine("Update TblAsset A ");
                SQL.AppendLine("Inner Join TblReconditionAssetHdr B On A.AssetCode = B.AssetCode ");
                SQL.AppendLine("    And B.DocNo = @DocNo ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("A.EcoLife = If(A.AssetSource = '1', @EcoLife3, A.EcoLife), ");
                SQL.AppendLine("A.EcoLifeYr = If(A.AssetSource = '1', @EcoLife3/12, A.EcoLifeYr), ");
                SQL.AppendLine("A.RemEcoLifeMth = If(A.AssetSource = '2', @EcoLife3, A.RemEcoLifeMth), ");
                SQL.AppendLine("A.RemEcoLifeYr = If(A.AssetSource = '2', @EcoLife3/12, A.RemEcoLifeYr), ");
                SQL.AppendLine("A.AssetValue = A.AssetValue + IfNull(B.ReconditionValue, 0.00), ");
                SQL.AppendLine("A.PercentageAnnualDepreciation = @PercentageAnnualDepreciation2 ");
                SQL.AppendLine("Where A.AssetCode = @AssetCode; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ReconditionDt", Sm.GetDte(DteReconditionDate));
            Sm.CmParam<Decimal>(ref cm, "@ReconditionValue", Decimal.Parse(TxtReconditionValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@AssetValue1", Decimal.Parse(TxtAssetValue1.Text));
            Sm.CmParam<Decimal>(ref cm, "@AssetValue2", Decimal.Parse(TxtAssetValue2.Text));
            Sm.CmParam<Decimal>(ref cm, "@NBVValue1", Decimal.Parse(TxtAssetNBV1.Text));
            Sm.CmParam<Decimal>(ref cm, "@NBVValue2", Decimal.Parse(TxtAssetNBV2.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife1", Decimal.Parse(TxtEcoLife1.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife2", Decimal.Parse(TxtEcoLife2.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife3", Decimal.Parse(TxtEcoLife3.Text));
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", mAssetAnnualPercentageRate);
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation2", mAssetAnnualPercentageRate2);
            Sm.CmParam<String>(ref cm, "@ReconditionReason", MeeReconditionReason.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveReconditionAssetDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Insert Into TblReconditionAssetDtl(DocNo, DNo, DODDocNo, DODDNo, UPrice, Qty, Amt , CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_"+i.ToString()+ ", @DODDocNo_" + i.ToString() + ", @DODDNo_" + i.ToString() + ", @UPrice_" + i.ToString() + ", @Qty_" + i.ToString() + ", @Amt_" + i.ToString() + ", @CreateBy, CurrentDateTime())");

                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("00" + (i + 1).ToString(),3));
                    Sm.CmParam<String>(ref cm, "@DODDocNo_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 0));
                    Sm.CmParam<String>(ref cm, "@DODDNo_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 1));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 8));
                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @row:= 0; ");
            SQL.AppendLine("Update TblReconditionAssetHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Recondition of Asset : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, (Select CCCode From TblAsset Where AssetCode=@AssetCode), ReconditionReason, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblReconditionAssetHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From ( ");
            SQL.AppendLine("        Select B.AcNo, A.ReconditionValue As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblReconditionAssetHdr A ");
            SQL.AppendLine("		Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("		Union All ");
            SQL.AppendLine("		Select E.AcNo, 0.00 As DAmt, A.Amt As CAmt ");
            SQL.AppendLine("		From TblReconditionAssetDtl A ");
            SQL.AppendLine("		Inner Join TblDODeptHdr B On A.DODDocNo = B.DocNo ");
            SQL.AppendLine("		Inner Join TblDODeptDtl C On A.DODDocNo = C.DocNo And A.DODDNo = C.DNo ");
            SQL.AppendLine("		Inner Join TblItemCostCategory D On C.ItCode = D.ItCode And B.CCCode = D.CCCode ");
            SQL.AppendLine("		Inner Join TblCostCategory E On D.CCtCode = E.CCtCode ");
            SQL.AppendLine("		Where A.DocNo = @DocNo ");
            SQL.AppendLine("    )Tbl Where AcNo Is Not Null Group By AcNo ");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.ServerCurrentDate(), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            try
            {
                if (
                    Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsAssetAlreadyCancelled()
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(UpdateReconditionAsset());
                cml.Add(UpdateAsset());
                if (mIsAutoJournalActived)
                    cml.Add(SaveJournal2());

                Sm.ExecCommands(cml);

                ShowData(TxtDocNo.Text);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsAssetAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblReconditionAssetHdr " +
                "Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand UpdateReconditionAssetFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblReconditionAssetHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateReconditionAsset()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Update TblReconditionAssetHdr ");
            SQL.AppendLine("    Set CancelReason = @CancelReason, ");
            SQL.AppendLine("    CancelInd = 'Y' ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        private MySqlCommand UpdateAsset()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAsset A ");
            SQL.AppendLine("Inner Join TblReconditionAssetHdr B On A.AssetCode = B.AssetCode And B.DocNo = @DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select * From ( ");
            SQL.AppendLine("		Select AssetCode, IfNull(EcoLife3,0.00) As RecEcoLife, ");
            SQL.AppendLine("	    IfNull(EcoLife3/12, 0.00) As RecEcoLifeYr, ");
            SQL.AppendLine("        PercentageAnnualDepreciation2 As RecPercentageAnnualDepreciation");
            SQL.AppendLine("		From TblReconditionAssetHdr ");
            SQL.AppendLine("		Where AssetCode = @AssetCode ");
            SQL.AppendLine("		And CancelInd = 'N' And Status = 'A' ");
            SQL.AppendLine("		And Exists( ");
            SQL.AppendLine("			Select 1 From TblReconditionAssetHdr Where AssetCode = @AssetCode ");
            SQL.AppendLine("			And CancelInd = 'N' Order By DocDt Desc Limit 1 ");
            SQL.AppendLine("	    ) ");
            SQL.AppendLine("		Order By DocDt Desc Limit 1 ");
            SQL.AppendLine("	)X1 ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select * From ( ");
            SQL.AppendLine("		Select AssetCode, IfNull(EcoLife1,0.00) As RecEcoLife, ");
            SQL.AppendLine("		IfNull(EcoLife1/12, 0.00) As RecEcoLifeYr, ");
            SQL.AppendLine("        PercentageAnnualDepreciation As RecPercentageAnnualDepreciation");
            SQL.AppendLine("		From TblReconditionAssetHdr ");
            SQL.AppendLine("		Where AssetCode = @AssetCode And Status = 'A' ");
            SQL.AppendLine("		And Not Exists( ");
            SQL.AppendLine("			Select 1 From TblReconditionAssetHdr Where AssetCode = @AssetCode ");
            SQL.AppendLine("			And CancelInd = 'N' Order By DocDt Desc Limit 1 ");
            SQL.AppendLine("	    ) ");
            SQL.AppendLine("	    Order By DocDt Asc Limit 1 ");
            SQL.AppendLine("	)X2 ");
            SQL.AppendLine(")C On A.AssetCode = C.AssetCode ");
            SQL.AppendLine("Set  ");
            SQL.AppendLine("A.EcoLife = If(A.AssetSource = '1', C.RecEcoLife, A.EcoLife), ");
            SQL.AppendLine("A.EcoLifeYr = If(A.AssetSource = '1', C.RecEcoLifeYr, A.EcoLifeYr), ");
            SQL.AppendLine("A.RemEcoLifeMth = If(A.AssetSource = '2', C.RecEcoLife, A.RemEcoLifeMth), ");
            SQL.AppendLine("A.RemEcoLifeYr = If(A.AssetSource = '2', C.RecEcoLifeYr, A.RemEcoLifeYr), ");
            SQL.AppendLine("A.AssetValue = A.AssetValue - IfNull(B.ReconditionValue, 0.00), ");
            SQL.AppendLine("A.PercentageAnnualDepreciation = C.RecPercentageAnnualDepreciation ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblReconditionAssetHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y'; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblReconditionAssetHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblReconditionAssetHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowReconditionAssetHdr(DocNo);
                ShowReconditionAssetDtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowReconditionAssetHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string mDBADocNo = string.Empty;

            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.CancelReason, A.ReconditionValue, A.DocDt, A.ReconditionDt, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("	When 'O' Then 'Outstanding' ");
            SQL.AppendLine("	When 'A' Then 'Approved' ");
            SQL.AppendLine("	When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As Status, ");
            SQL.AppendLine("C.AssetCategoryName, B.AssetCode, B.AssetName, D.SiteName, ");
            SQL.AppendLine("F.ProfitCenterName, E.CCName, A.AssetValue1, A.AssetValue2, ");
            SQL.AppendLine("A.NBVValue1, A.NBVValue2, B.ResidualValue, A.EcoLife1, A.EcoLife2, ");
            SQL.AppendLine("A.EcoLife3, A.ReconditionReason, A.FileName ");
            SQL.AppendLine("From TblReconditionAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("Inner Join TblAssetCategory C On B.AssetCategoryCode = C.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode2 = D.SiteCode ");
            SQL.AppendLine("Left Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode = F.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
            ref cm, SQL.ToString(),
            new string[]
            { 
                //0
                "DocNo",

                //1-5
                "CancelReason", "CancelInd", "ReconditionValue", "DocDt", "ReconditionDt",

                //6-10
                "Status", "AssetCategoryName", "AssetCode", "AssetName", "SiteName",

                //11-15
                "ProfitCenterName", "CCName", "AssetValue1", "AssetValue2", "NBVValue1", 

                //16-20
                "NBVValue2", "ResidualValue", "EcoLife1", "EcoLife2", "EcoLife3",

                //21-22
                "ReconditionReason", "FileName"
            },
            (MySqlDataReader dr, int[] c) =>
            {
                TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                MeeCancelReason.EditValue = Sm.DrStr(dr, c[1]);
                ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                TxtReconditionValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[4]));
                Sm.SetDte(DteReconditionDate, Sm.DrStr(dr, c[5]));
                TxtStatus.EditValue = Sm.DrStr(dr, c[6]);
                TxtAssetCategory.EditValue = Sm.DrStr(dr, c[7]);
                TxtAssetCode.EditValue = Sm.DrStr(dr, c[8]);
                TxtAssetName.EditValue = Sm.DrStr(dr, c[9]);
                TxtSiteName.EditValue = Sm.DrStr(dr, c[10]);
                TxtProfitCenter.EditValue = Sm.DrStr(dr, c[11]);
                TxtCostCenter.EditValue = Sm.DrStr(dr, c[12]);
                TxtAssetValue1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                TxtAssetValue2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                TxtAssetNBV1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                TxtAssetNBV2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                TxtResidualValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                TxtEcoLife1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                TxtEcoLife2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                TxtEcoLife3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                MeeReconditionReason.EditValue = Sm.DrStr(dr, c[21]);
                TxtFile.EditValue = Sm.DrStr(dr, c[22]);
            }, true
            );
        }

        private void ShowReconditionAssetDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DODDocNo, A.DODDNo, C.ItCode, C.ItName, C.InventoryUOMCode, ");
            SQL.AppendLine("A.UPrice, A.Qty, A.Amt ");
            SQL.AppendLine("From TblReconditionAssetDtl A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DODDocNo = B.DocNo And A.DODDNo = B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DODDocNo",
                    
                    //1-5
                    "DODDNo", "ItCode", "ItName", "InventoryUOMCode", "UPrice", 
        
                    //6-7
                    "Qty", "Amt", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, IfNull(B.UserName, A.UserCode) As UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ReconditionAsset' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsReconditionAssetAllowToUploadFile = Sm.GetParameterBoo("IsReconditionAssetAllowToUploadFile");
            mIsReconditionAssetUploadFileMandatory = Sm.GetParameterBoo("IsReconditionAssetUploadFileMandatory");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        private bool IsDocApprovalSettingExists()
        {
            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'ReconditionAsset' Limit 1; "))
                return true;
            return false;
        }

        internal void ShowDepreciationAssetData(string DocNo)
        {
            var l = new List<DepreciationAsset>();

            ShowDepreciationAsset(DocNo);
            ProcessAssetAcc1(ref l, DocNo);
            //ProcessAssetAcc2(ref l, DocNo);
            ComputeReconditionAmt();
            ComputeNBVValue();
        }

        private void ShowDepreciationAsset(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.AssetCategoryName, A.AssetCode, C.AssetName, ");
            SQL.AppendLine("E.SiteName, H.ProfitCenterName, G.CCName, IfNull(B.AssetValue, 0.00) As AssetValue, ");
            SQL.AppendLine("C.ResidualValue, A.EcoLife, IfNull(A.EcoLife, 0.00) - IfNull(B.MthPurchase, 0.00) As EcoLifeCurDt, ");
            SQL.AppendLine("B.ReconditionDt, A.PercentageAnnualDepreciation, B.AccDeprValue, C.DepreciationCode ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.MthPurchase, T2.AccDeprValue, T3.AssetValue,");
            SQL.AppendLine("    Replace(Date_Add(Concat(T2.Yr, T2.Mth, '01'), Interval 1 Month ), '-','') As ReconditionDt ");
            SQL.AppendLine("    From TblDepreciationAssetHdr T1 ");
            SQL.AppendLine("    Inner Join TblDepreciationAssetDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblAsset T3 On T1.AssetCode = T3.AssetCode ");
            SQL.AppendLine("    Where T2.JournalDocNo Is Not Null ");
            SQL.AppendLine("    And T1.DocNo = @DocNo ");
            SQL.AppendLine("    Order By T2.DNo Desc Limit 1 ");
            SQL.AppendLine(")B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblAsset C On A.AssetCode = C.AssetCode And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblAssetCategory D On C.AssetCategoryCode = D.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblSite E On C.SiteCode2 = E.SiteCode ");
            SQL.AppendLine("Left Join TblCOA F On C.AcNo = F.AcNo ");
            SQL.AppendLine("Inner Join TblCostCenter G On C.CCCode = G.CCCode ");
            SQL.AppendLine("Inner Join TblProfitCenter H On G.ProfitCenterCode = H.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
            ref cm, SQL.ToString(),
            new string[]
            { 
                //0
                "AssetCategoryName", 

                //1-5
                "AssetCode", "AssetName", "SiteName", "ProfitCenterName", "CCName",  

                //6-10
                "AssetValue", "ResidualValue", "EcoLife", "EcoLifeCurDt", "ReconditionDt",

                //11-13
                "PercentageAnnualDepreciation", "AccDeprValue", "DepreciationCode"
            },
            (MySqlDataReader dr, int[] c) =>
            {
                TxtAssetCategory.EditValue = Sm.DrStr(dr, c[0]);
                TxtAssetCode.EditValue = Sm.DrStr(dr, c[1]);
                TxtAssetName.EditValue = Sm.DrStr(dr, c[2]);
                TxtSiteName.EditValue = Sm.DrStr(dr, c[3]);
                TxtProfitCenter.EditValue = Sm.DrStr(dr, c[4]);
                TxtCostCenter.EditValue = Sm.DrStr(dr, c[5]);
                TxtAssetValue1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                TxtResidualValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[7]), 0);
                TxtEcoLife1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                TxtEcoLife2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                Sm.SetDte(DteReconditionDate, Sm.DrStr(dr, c[10]));
                mReconditionDate = Sm.DrStr(dr, c[10]);
                mAssetAnnualPercentageRate = Sm.DrDec(dr, c[11]);
                TxtAssetValue2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                mDeprMethod = Sm.DrStr(dr, c[13]);

            }, true
            );
        }

        private void ComputeNBVValue()
        {
            TxtAssetNBV1.EditValue = Sm.FormatNum(Sm.GetDecValue(TxtAssetValue1.Text) - Sm.GetDecValue(TxtAssetValue2.Text), 0);
            TxtAssetNBV2.EditValue = Sm.FormatNum(Sm.GetDecValue(TxtAssetNBV1.Text) + Sm.GetDecValue(TxtReconditionValue.Text), 0);
        }

        private void ProcessAssetAcc1(ref List<DepreciationAsset> l, string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, B.DNo, B.DepreciationValue, B.JournalDocNo ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And B.JournalDocNo Is Not Null ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "DNo",

                    //1-4
                    "DepreciationValue",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepreciationAsset()
                        {
                            DNo = Sm.DrStr(dr, c[0]),
                            DepreciationValue = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAssetAcc2(ref List<DepreciationAsset> l, string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            decimal AccDeprValue = 0m;

            SQL.AppendLine("Select DNo ");
            SQL.AppendLine("From TblDepreciationAssetDtl ");
            SQL.AppendLine("Where JournalDocNo Is Not Null ");
            SQL.AppendLine("And DocNo = @Param ");
            SQL.AppendLine("Order By DNo Desc Limit 1 ");

            SQL2.AppendLine("Select If(AssetSource = '2', AccDepr, 0.00) As AccDepr ");
            SQL2.AppendLine("From TblAsset Where AssetCode = @Param;");

            string mDNo = Sm.GetValue(SQL.ToString(), DocNo);

            for (int i = 0; i < l.Count; i++)
            {
                if(i == 0)
                    l[i].AccumulationValue = l[i].DepreciationValue;
                else
                    l[i].AccumulationValue = l[i].DepreciationValue + l[i - 1].AccumulationValue;
            }

            foreach (var x in l)
            {
                if (x.DNo == mDNo)
                {
                    AccDeprValue = Sm.GetValueDec(SQL2.ToString(), TxtAssetCode.Text);
                    TxtAssetValue2.EditValue = Sm.FormatNum(x.AccumulationValue + AccDeprValue, 0);
                    break;
                }
            }
        }

        internal void ComputeReconditionAmt()
        {
            decimal ReconditionAmt = 0m;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                Grd1.Cells[i, 8].Value = Sm.GetGrdDec(Grd1, i, 6) * Sm.GetGrdDec(Grd1, i, 7);
                ReconditionAmt += Sm.GetGrdDec(Grd1, i, 8);
            }

            TxtReconditionValue.EditValue = Sm.FormatNum(ReconditionAmt, 0);
            ComputeNBVValue();
        }

        internal decimal ComputeAnnualPercentageRate()
        {
            decimal month = Convert.ToDecimal(TxtEcoLife3.Text);
            decimal PercentageDepreciation = 0m;
            decimal year = month / 12;

            if (year != 0)
            {
                if (mDeprMethod == "1")
                {
                    PercentageDepreciation = (1 / year) * 100; // 1 is equal to 100% ( 100/100 = 1 )
                    PercentageDepreciation = Math.Round(PercentageDepreciation, 2);
                }
                else if (mDeprMethod == "2")
                {
                    PercentageDepreciation = (100 / year) * 2; // 100 is equal to 100%
                    PercentageDepreciation = Math.Round(PercentageDepreciation, 2);

                }
            }
            else
                PercentageDepreciation = 0m;

            return PercentageDepreciation;
        }

        private void UploadFile(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateReconditionAssetFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsReconditionAssetAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblReconditionAssetHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileMandatory()
        {
            if (!mIsReconditionAssetUploadFileMandatory) return false;
            
            if (TxtFile.Text == "" || TxtFile.Text == "openFileDialog1")
            {
                Sm.StdMsg(mMsgType.Warning, "Supporting File is Empty");
                return true;
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = "Recondition of Asset";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.PDF";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnAssetCode_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
                Sm.FormShowDialog(new FrmReconditionAssetDlg(this));
        }

        private void TxtEcoLife3_Validated(object sender, EventArgs e)
        {
            if(BtnSave.Enabled) Sm.FormatNumTxt(TxtEcoLife3, 0);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 2)
                Sm.FormShowDialog(new FrmReconditionAssetDlg2(this));
        }

        private void Grd1_EllispsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 2)
                Sm.FormShowDialog(new FrmReconditionAssetDlg2(this));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 7 && BtnSave.Enabled)
            {
                ComputeReconditionAmt();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeReconditionAmt();
        }

        #endregion

        #endregion

        #region Class

        private class DepreciationAsset
        {
            public string DNo { get; set; }
            public decimal DepreciationValue { get; set; }
            public decimal AccumulationValue { get; set; }
        }

        #endregion
    }
}
