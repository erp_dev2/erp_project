﻿namespace RunSystem
{
    partial class FrmSI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSI));
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.BtnConsignee = new DevExpress.XtraEditors.SimpleButton();
            this.label32 = new System.Windows.Forms.Label();
            this.DteCloseDt = new DevExpress.XtraEditors.DateEdit();
            this.CloseTme = new DevExpress.XtraEditors.TimeEdit();
            this.StuffTme = new DevExpress.XtraEditors.TimeEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblLocalDoc = new System.Windows.Forms.Label();
            this.BtnSource = new DevExpress.XtraEditors.SimpleButton();
            this.MeeConsignee = new DevExpress.XtraEditors.MemoExEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.MeeNotify = new DevExpress.XtraEditors.MemoExEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.DteStfDt = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.BtnSP2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSP = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPEB = new DevExpress.XtraEditors.TextEdit();
            this.TxtKpbc = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.MeeDelivery = new DevExpress.XtraEditors.MemoExEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtHsCode = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtBL = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.LueNotify = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.DteDischargeDt = new DevExpress.XtraEditors.DateEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.DteLoadingDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPIUser = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.LueVdCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtShippingLine = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtMotherVessel = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtFeeder = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.MeeRemark2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.ChkProforma = new DevExpress.XtraEditors.CheckEdit();
            this.TxtSize = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeMark = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.MeeReceipt = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueDischarge = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueLoading = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TpgSI = new System.Windows.Forms.TabControl();
            this.TpgResult = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TpgBL = new System.Windows.Forms.TabPage();
            this.LueBL = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteCloseDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCloseDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseTme.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StuffTme.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConsignee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPEB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKpbc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDischargeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDischargeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLoadingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLoadingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShippingLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotherVessel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFeeder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProforma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReceipt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDischarge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLoading.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.TpgSI.SuspendLayout();
            this.TpgResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgBL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(804, 0);
            this.panel1.Size = new System.Drawing.Size(70, 510);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TpgSI);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(804, 510);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.BtnDownload);
            this.panel3.Controls.Add(this.BtnConsignee);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.DteCloseDt);
            this.panel3.Controls.Add(this.CloseTme);
            this.panel3.Controls.Add(this.StuffTme);
            this.panel3.Controls.Add(this.TxtLocalDocNo);
            this.panel3.Controls.Add(this.LblLocalDoc);
            this.panel3.Controls.Add(this.BtnSource);
            this.panel3.Controls.Add(this.MeeConsignee);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.TxtSource);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.MeeNotify);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.DteStfDt);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.BtnSP2);
            this.panel3.Controls.Add(this.BtnSP);
            this.panel3.Controls.Add(this.TxtPEB);
            this.panel3.Controls.Add(this.TxtKpbc);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtSPDocNo);
            this.panel3.Controls.Add(this.MeeDelivery);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtHsCode);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.TxtBL);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.LueNotify);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.panel19);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(804, 353);
            this.panel3.TabIndex = 16;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(357, 48);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 54;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // BtnConsignee
            // 
            this.BtnConsignee.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnConsignee.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnConsignee.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnConsignee.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnConsignee.Appearance.Options.UseBackColor = true;
            this.BtnConsignee.Appearance.Options.UseFont = true;
            this.BtnConsignee.Appearance.Options.UseForeColor = true;
            this.BtnConsignee.Appearance.Options.UseTextOptions = true;
            this.BtnConsignee.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnConsignee.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnConsignee.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsignee.Image")));
            this.BtnConsignee.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnConsignee.Location = new System.Drawing.Point(356, 276);
            this.BtnConsignee.Name = "BtnConsignee";
            this.BtnConsignee.Size = new System.Drawing.Size(24, 21);
            this.BtnConsignee.TabIndex = 50;
            this.BtnConsignee.ToolTip = "Copy notify party\'s information";
            this.BtnConsignee.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnConsignee.ToolTipTitle = "Run System";
            this.BtnConsignee.Click += new System.EventHandler(this.BtnConsignee_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(41, 197);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 14);
            this.label32.TabIndex = 39;
            this.label32.Text = "Closing Date";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteCloseDt
            // 
            this.DteCloseDt.EditValue = null;
            this.DteCloseDt.EnterMoveNextControl = true;
            this.DteCloseDt.Location = new System.Drawing.Point(119, 193);
            this.DteCloseDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteCloseDt.Name = "DteCloseDt";
            this.DteCloseDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCloseDt.Properties.Appearance.Options.UseFont = true;
            this.DteCloseDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCloseDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteCloseDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteCloseDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteCloseDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCloseDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteCloseDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCloseDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteCloseDt.Properties.MaxLength = 8;
            this.DteCloseDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteCloseDt.Size = new System.Drawing.Size(120, 20);
            this.DteCloseDt.TabIndex = 40;
            // 
            // CloseTme
            // 
            this.CloseTme.EditValue = null;
            this.CloseTme.EnterMoveNextControl = true;
            this.CloseTme.Location = new System.Drawing.Point(250, 193);
            this.CloseTme.Name = "CloseTme";
            this.CloseTme.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CloseTme.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseTme.Properties.Appearance.Options.UseFont = true;
            this.CloseTme.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CloseTme.Properties.Mask.EditMask = "HH:mm";
            this.CloseTme.Size = new System.Drawing.Size(103, 20);
            this.CloseTme.TabIndex = 41;
            // 
            // StuffTme
            // 
            this.StuffTme.EditValue = null;
            this.StuffTme.EnterMoveNextControl = true;
            this.StuffTme.Location = new System.Drawing.Point(250, 172);
            this.StuffTme.Name = "StuffTme";
            this.StuffTme.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StuffTme.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StuffTme.Properties.Appearance.Options.UseFont = true;
            this.StuffTme.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StuffTme.Properties.Mask.EditMask = "HH:mm";
            this.StuffTme.Size = new System.Drawing.Size(103, 20);
            this.StuffTme.TabIndex = 38;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(119, 46);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            // 
            // LblLocalDoc
            // 
            this.LblLocalDoc.AutoSize = true;
            this.LblLocalDoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLocalDoc.Location = new System.Drawing.Point(20, 49);
            this.LblLocalDoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLocalDoc.Name = "LblLocalDoc";
            this.LblLocalDoc.Size = new System.Drawing.Size(95, 14);
            this.LblLocalDoc.TabIndex = 21;
            this.LblLocalDoc.Text = "Local Document";
            this.LblLocalDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSource
            // 
            this.BtnSource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSource.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSource.Appearance.Options.UseBackColor = true;
            this.BtnSource.Appearance.Options.UseFont = true;
            this.BtnSource.Appearance.Options.UseForeColor = true;
            this.BtnSource.Appearance.Options.UseTextOptions = true;
            this.BtnSource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSource.Image = ((System.Drawing.Image)(resources.GetObject("BtnSource.Image")));
            this.BtnSource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSource.Location = new System.Drawing.Point(356, 87);
            this.BtnSource.Name = "BtnSource";
            this.BtnSource.Size = new System.Drawing.Size(24, 21);
            this.BtnSource.TabIndex = 27;
            this.BtnSource.ToolTip = "Add Costumer Notify Party";
            this.BtnSource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSource.ToolTipTitle = "Run System";
            this.BtnSource.Click += new System.EventHandler(this.BtnSource_Click);
            // 
            // MeeConsignee
            // 
            this.MeeConsignee.EnterMoveNextControl = true;
            this.MeeConsignee.Location = new System.Drawing.Point(119, 277);
            this.MeeConsignee.Margin = new System.Windows.Forms.Padding(5);
            this.MeeConsignee.Name = "MeeConsignee";
            this.MeeConsignee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.Appearance.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConsignee.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeConsignee.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeConsignee.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeConsignee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeConsignee.Properties.MaxLength = 300;
            this.MeeConsignee.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeConsignee.Properties.ShowIcon = false;
            this.MeeConsignee.Size = new System.Drawing.Size(234, 20);
            this.MeeConsignee.TabIndex = 49;
            this.MeeConsignee.ToolTip = "F4 : Show/hide text";
            this.MeeConsignee.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeConsignee.ToolTipTitle = "Run System";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(53, 279);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 14);
            this.label18.TabIndex = 48;
            this.label18.Text = "Consignee";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(119, 88);
            this.TxtSource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Properties.ReadOnly = true;
            this.TxtSource.Size = new System.Drawing.Size(234, 20);
            this.TxtSource.TabIndex = 26;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(70, 91);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 14);
            this.label22.TabIndex = 25;
            this.label22.Text = "Source";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeNotify
            // 
            this.MeeNotify.EnterMoveNextControl = true;
            this.MeeNotify.Location = new System.Drawing.Point(119, 151);
            this.MeeNotify.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNotify.Name = "MeeNotify";
            this.MeeNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.Appearance.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeNotify.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNotify.Properties.MaxLength = 1000;
            this.MeeNotify.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeNotify.Properties.ShowIcon = false;
            this.MeeNotify.Size = new System.Drawing.Size(234, 20);
            this.MeeNotify.TabIndex = 35;
            this.MeeNotify.ToolTip = "F4 : Show/hide text";
            this.MeeNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNotify.ToolTipTitle = "Run System";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(50, 154);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 14);
            this.label20.TabIndex = 34;
            this.label20.Text = "Also Notify";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStfDt
            // 
            this.DteStfDt.EditValue = null;
            this.DteStfDt.EnterMoveNextControl = true;
            this.DteStfDt.Location = new System.Drawing.Point(119, 172);
            this.DteStfDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStfDt.Name = "DteStfDt";
            this.DteStfDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStfDt.Properties.Appearance.Options.UseFont = true;
            this.DteStfDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStfDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStfDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStfDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStfDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStfDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStfDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStfDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStfDt.Properties.MaxLength = 8;
            this.DteStfDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStfDt.Size = new System.Drawing.Size(120, 20);
            this.DteStfDt.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(35, 175);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 14);
            this.label16.TabIndex = 36;
            this.label16.Text = "Stuffing Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSP2
            // 
            this.BtnSP2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSP2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSP2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSP2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSP2.Appearance.Options.UseBackColor = true;
            this.BtnSP2.Appearance.Options.UseFont = true;
            this.BtnSP2.Appearance.Options.UseForeColor = true;
            this.BtnSP2.Appearance.Options.UseTextOptions = true;
            this.BtnSP2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSP2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSP2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSP2.Image")));
            this.BtnSP2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSP2.Location = new System.Drawing.Point(377, 110);
            this.BtnSP2.Name = "BtnSP2";
            this.BtnSP2.Size = new System.Drawing.Size(22, 21);
            this.BtnSP2.TabIndex = 31;
            this.BtnSP2.ToolTip = "Show Shipment Planning";
            this.BtnSP2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSP2.ToolTipTitle = "Run System";
            this.BtnSP2.Click += new System.EventHandler(this.BtnSP2_Click);
            // 
            // BtnSP
            // 
            this.BtnSP.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSP.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSP.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSP.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSP.Appearance.Options.UseBackColor = true;
            this.BtnSP.Appearance.Options.UseFont = true;
            this.BtnSP.Appearance.Options.UseForeColor = true;
            this.BtnSP.Appearance.Options.UseTextOptions = true;
            this.BtnSP.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSP.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSP.Image = ((System.Drawing.Image)(resources.GetObject("BtnSP.Image")));
            this.BtnSP.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSP.Location = new System.Drawing.Point(356, 110);
            this.BtnSP.Name = "BtnSP";
            this.BtnSP.Size = new System.Drawing.Size(22, 21);
            this.BtnSP.TabIndex = 30;
            this.BtnSP.ToolTip = "List of Shipment Planning";
            this.BtnSP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSP.ToolTipTitle = "Run System";
            this.BtnSP.Click += new System.EventHandler(this.BtnSP_Click);
            // 
            // TxtPEB
            // 
            this.TxtPEB.EnterMoveNextControl = true;
            this.TxtPEB.Location = new System.Drawing.Point(119, 256);
            this.TxtPEB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPEB.Name = "TxtPEB";
            this.TxtPEB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPEB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPEB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPEB.Properties.Appearance.Options.UseFont = true;
            this.TxtPEB.Size = new System.Drawing.Size(234, 20);
            this.TxtPEB.TabIndex = 47;
            // 
            // TxtKpbc
            // 
            this.TxtKpbc.EnterMoveNextControl = true;
            this.TxtKpbc.Location = new System.Drawing.Point(119, 298);
            this.TxtKpbc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKpbc.Name = "TxtKpbc";
            this.TxtKpbc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKpbc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKpbc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKpbc.Properties.Appearance.Options.UseFont = true;
            this.TxtKpbc.Size = new System.Drawing.Size(234, 20);
            this.TxtKpbc.TabIndex = 51;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(80, 302);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 14);
            this.label23.TabIndex = 51;
            this.label23.Text = "KPBC";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(88, 259);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 14);
            this.label7.TabIndex = 46;
            this.label7.Text = "PEB";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSPDocNo
            // 
            this.TxtSPDocNo.EnterMoveNextControl = true;
            this.TxtSPDocNo.Location = new System.Drawing.Point(119, 109);
            this.TxtSPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSPDocNo.Name = "TxtSPDocNo";
            this.TxtSPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSPDocNo.Properties.ReadOnly = true;
            this.TxtSPDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtSPDocNo.TabIndex = 29;
            // 
            // MeeDelivery
            // 
            this.MeeDelivery.EnterMoveNextControl = true;
            this.MeeDelivery.Location = new System.Drawing.Point(119, 319);
            this.MeeDelivery.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDelivery.Name = "MeeDelivery";
            this.MeeDelivery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.Appearance.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDelivery.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDelivery.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeDelivery.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDelivery.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDelivery.Properties.MaxLength = 400;
            this.MeeDelivery.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeDelivery.Properties.ShowIcon = false;
            this.MeeDelivery.Size = new System.Drawing.Size(234, 20);
            this.MeeDelivery.TabIndex = 53;
            this.MeeDelivery.ToolTip = "F4 : Show/hide text";
            this.MeeDelivery.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDelivery.ToolTipTitle = "Run System";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(7, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "Shipment Planning";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(21, 321);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 14);
            this.label9.TabIndex = 52;
            this.label9.Text = "Place of Delivery";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHsCode
            // 
            this.TxtHsCode.EnterMoveNextControl = true;
            this.TxtHsCode.Location = new System.Drawing.Point(119, 235);
            this.TxtHsCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHsCode.Name = "TxtHsCode";
            this.TxtHsCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHsCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtHsCode.Size = new System.Drawing.Size(234, 20);
            this.TxtHsCode.TabIndex = 45;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(61, 238);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 14);
            this.label21.TabIndex = 44;
            this.label21.Text = "HS Code";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBL
            // 
            this.TxtBL.EnterMoveNextControl = true;
            this.TxtBL.Location = new System.Drawing.Point(119, 214);
            this.TxtBL.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBL.Name = "TxtBL";
            this.TxtBL.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBL.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBL.Properties.Appearance.Options.UseFont = true;
            this.TxtBL.Size = new System.Drawing.Size(234, 20);
            this.TxtBL.TabIndex = 43;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(20, 217);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 14);
            this.label19.TabIndex = 42;
            this.label19.Text = "Booking / BL No";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(119, 67);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(234, 20);
            this.LueCtCode.TabIndex = 24;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.Validated += new System.EventHandler(this.LueCtCode_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(56, 70);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 14);
            this.label17.TabIndex = 23;
            this.label17.Text = "Customer";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueNotify
            // 
            this.LueNotify.EnterMoveNextControl = true;
            this.LueNotify.Location = new System.Drawing.Point(119, 130);
            this.LueNotify.Margin = new System.Windows.Forms.Padding(5);
            this.LueNotify.Name = "LueNotify";
            this.LueNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.Appearance.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueNotify.Properties.DropDownRows = 30;
            this.LueNotify.Properties.NullText = "[Empty]";
            this.LueNotify.Properties.PopupWidth = 300;
            this.LueNotify.Size = new System.Drawing.Size(234, 20);
            this.LueNotify.TabIndex = 33;
            this.LueNotify.ToolTip = "F4 : Show/hide list";
            this.LueNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueNotify.EditValueChanged += new System.EventHandler(this.LueNotify_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(44, 133);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "Notify Party";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.label35);
            this.panel19.Controls.Add(this.DteDischargeDt);
            this.panel19.Controls.Add(this.label34);
            this.panel19.Controls.Add(this.DteLoadingDt);
            this.panel19.Controls.Add(this.TxtPIUser);
            this.panel19.Controls.Add(this.label31);
            this.panel19.Controls.Add(this.label30);
            this.panel19.Controls.Add(this.LueVdCode2);
            this.panel19.Controls.Add(this.label29);
            this.panel19.Controls.Add(this.LueVdCode);
            this.panel19.Controls.Add(this.TxtShippingLine);
            this.panel19.Controls.Add(this.label28);
            this.panel19.Controls.Add(this.TxtMotherVessel);
            this.panel19.Controls.Add(this.label27);
            this.panel19.Controls.Add(this.TxtFeeder);
            this.panel19.Controls.Add(this.label26);
            this.panel19.Controls.Add(this.MeeRemark2);
            this.panel19.Controls.Add(this.label25);
            this.panel19.Controls.Add(this.ChkProforma);
            this.panel19.Controls.Add(this.TxtSize);
            this.panel19.Controls.Add(this.label14);
            this.panel19.Controls.Add(this.MeeMark);
            this.panel19.Controls.Add(this.MeeRemark);
            this.panel19.Controls.Add(this.label33);
            this.panel19.Controls.Add(this.MeeReceipt);
            this.panel19.Controls.Add(this.label15);
            this.panel19.Controls.Add(this.LueDischarge);
            this.panel19.Controls.Add(this.label8);
            this.panel19.Controls.Add(this.label3);
            this.panel19.Controls.Add(this.LueLoading);
            this.panel19.Controls.Add(this.label2);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel19.Location = new System.Drawing.Point(401, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(399, 349);
            this.panel19.TabIndex = 36;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(6, 91);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(131, 14);
            this.label35.TabIndex = 62;
            this.label35.Text = "Date Port of Discharge";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDischargeDt
            // 
            this.DteDischargeDt.EditValue = null;
            this.DteDischargeDt.EnterMoveNextControl = true;
            this.DteDischargeDt.Location = new System.Drawing.Point(140, 88);
            this.DteDischargeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDischargeDt.Name = "DteDischargeDt";
            this.DteDischargeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDischargeDt.Properties.Appearance.Options.UseFont = true;
            this.DteDischargeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDischargeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDischargeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDischargeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDischargeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDischargeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDischargeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDischargeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDischargeDt.Properties.MaxLength = 8;
            this.DteDischargeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDischargeDt.Size = new System.Drawing.Size(120, 20);
            this.DteDischargeDt.TabIndex = 63;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(16, 28);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(121, 14);
            this.label34.TabIndex = 56;
            this.label34.Text = "Date Port of Loading";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLoadingDt
            // 
            this.DteLoadingDt.EditValue = null;
            this.DteLoadingDt.EnterMoveNextControl = true;
            this.DteLoadingDt.Location = new System.Drawing.Point(140, 25);
            this.DteLoadingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLoadingDt.Name = "DteLoadingDt";
            this.DteLoadingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLoadingDt.Properties.Appearance.Options.UseFont = true;
            this.DteLoadingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLoadingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLoadingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLoadingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLoadingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLoadingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLoadingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLoadingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLoadingDt.Properties.MaxLength = 8;
            this.DteLoadingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLoadingDt.Size = new System.Drawing.Size(120, 20);
            this.DteLoadingDt.TabIndex = 57;
            // 
            // TxtPIUser
            // 
            this.TxtPIUser.EnterMoveNextControl = true;
            this.TxtPIUser.Location = new System.Drawing.Point(140, 298);
            this.TxtPIUser.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPIUser.Name = "TxtPIUser";
            this.TxtPIUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPIUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPIUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPIUser.Properties.Appearance.Options.UseFont = true;
            this.TxtPIUser.Size = new System.Drawing.Size(248, 20);
            this.TxtPIUser.TabIndex = 83;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(91, 301);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(46, 14);
            this.label31.TabIndex = 82;
            this.label31.Text = "PI User";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(101, 279);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(36, 14);
            this.label30.TabIndex = 80;
            this.label30.Text = "EMKL";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode2
            // 
            this.LueVdCode2.EnterMoveNextControl = true;
            this.LueVdCode2.Location = new System.Drawing.Point(140, 277);
            this.LueVdCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode2.Name = "LueVdCode2";
            this.LueVdCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode2.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode2.Properties.DropDownRows = 30;
            this.LueVdCode2.Properties.NullText = "[Empty]";
            this.LueVdCode2.Properties.PopupWidth = 300;
            this.LueVdCode2.Size = new System.Drawing.Size(248, 20);
            this.LueVdCode2.TabIndex = 81;
            this.LueVdCode2.ToolTip = "F4 : Show/hide list";
            this.LueVdCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode2.EditValueChanged += new System.EventHandler(this.LueVdCode2_EditValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(31, 258);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 14);
            this.label29.TabIndex = 78;
            this.label29.Text = "Vendor Forwarder";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(140, 256);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 300;
            this.LueVdCode.Size = new System.Drawing.Size(248, 20);
            this.LueVdCode.TabIndex = 79;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            // 
            // TxtShippingLine
            // 
            this.TxtShippingLine.EnterMoveNextControl = true;
            this.TxtShippingLine.Location = new System.Drawing.Point(140, 235);
            this.TxtShippingLine.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShippingLine.Name = "TxtShippingLine";
            this.TxtShippingLine.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShippingLine.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShippingLine.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShippingLine.Properties.Appearance.Options.UseFont = true;
            this.TxtShippingLine.Size = new System.Drawing.Size(248, 20);
            this.TxtShippingLine.TabIndex = 77;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(58, 238);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 14);
            this.label28.TabIndex = 76;
            this.label28.Text = "Shipping Line";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMotherVessel
            // 
            this.TxtMotherVessel.EnterMoveNextControl = true;
            this.TxtMotherVessel.Location = new System.Drawing.Point(140, 214);
            this.TxtMotherVessel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMotherVessel.Name = "TxtMotherVessel";
            this.TxtMotherVessel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMotherVessel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMotherVessel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMotherVessel.Properties.Appearance.Options.UseFont = true;
            this.TxtMotherVessel.Size = new System.Drawing.Size(248, 20);
            this.TxtMotherVessel.TabIndex = 75;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(53, 217);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 14);
            this.label27.TabIndex = 74;
            this.label27.Text = "Mother Vessel";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFeeder
            // 
            this.TxtFeeder.EnterMoveNextControl = true;
            this.TxtFeeder.Location = new System.Drawing.Point(140, 193);
            this.TxtFeeder.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFeeder.Name = "TxtFeeder";
            this.TxtFeeder.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFeeder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFeeder.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFeeder.Properties.Appearance.Options.UseFont = true;
            this.TxtFeeder.Size = new System.Drawing.Size(248, 20);
            this.TxtFeeder.TabIndex = 73;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(92, 196);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 14);
            this.label26.TabIndex = 72;
            this.label26.Text = "Feeder";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark2
            // 
            this.MeeRemark2.EnterMoveNextControl = true;
            this.MeeRemark2.Location = new System.Drawing.Point(140, 172);
            this.MeeRemark2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark2.Name = "MeeRemark2";
            this.MeeRemark2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeRemark2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark2.Properties.MaxLength = 400;
            this.MeeRemark2.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark2.Properties.ShowIcon = false;
            this.MeeRemark2.Size = new System.Drawing.Size(248, 20);
            this.MeeRemark2.TabIndex = 71;
            this.MeeRemark2.ToolTip = "F4 : Show/hide text";
            this.MeeRemark2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark2.ToolTipTitle = "Run System";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(103, 175);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 14);
            this.label25.TabIndex = 70;
            this.label25.Text = "Note";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkProforma
            // 
            this.ChkProforma.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkProforma.Location = new System.Drawing.Point(138, 321);
            this.ChkProforma.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkProforma.Name = "ChkProforma";
            this.ChkProforma.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkProforma.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProforma.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkProforma.Properties.Appearance.Options.UseBackColor = true;
            this.ChkProforma.Properties.Appearance.Options.UseFont = true;
            this.ChkProforma.Properties.Appearance.Options.UseForeColor = true;
            this.ChkProforma.Properties.Caption = "Print With Proforma Invoice";
            this.ChkProforma.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProforma.ShowToolTips = false;
            this.ChkProforma.Size = new System.Drawing.Size(224, 22);
            this.ChkProforma.TabIndex = 84;
            // 
            // TxtSize
            // 
            this.TxtSize.EnterMoveNextControl = true;
            this.TxtSize.Location = new System.Drawing.Point(140, 130);
            this.TxtSize.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSize.Name = "TxtSize";
            this.TxtSize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSize.Properties.Appearance.Options.UseFont = true;
            this.TxtSize.Size = new System.Drawing.Size(248, 20);
            this.TxtSize.TabIndex = 67;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(53, 134);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 14);
            this.label14.TabIndex = 66;
            this.label14.Text = "Container Size";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeMark
            // 
            this.MeeMark.EnterMoveNextControl = true;
            this.MeeMark.Location = new System.Drawing.Point(140, 109);
            this.MeeMark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeMark.Name = "MeeMark";
            this.MeeMark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMark.Properties.Appearance.Options.UseFont = true;
            this.MeeMark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeMark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeMark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeMark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeMark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeMark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeMark.Properties.MaxLength = 400;
            this.MeeMark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeMark.Properties.ShowIcon = false;
            this.MeeMark.Size = new System.Drawing.Size(248, 20);
            this.MeeMark.TabIndex = 65;
            this.MeeMark.ToolTip = "F4 : Show/hide text";
            this.MeeMark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeMark.ToolTipTitle = "Run System";
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(140, 151);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(248, 20);
            this.MeeRemark.TabIndex = 69;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(17, 154);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(120, 14);
            this.label33.TabIndex = 68;
            this.label33.Text = "Description of Goods";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeReceipt
            // 
            this.MeeReceipt.EnterMoveNextControl = true;
            this.MeeReceipt.Location = new System.Drawing.Point(140, 46);
            this.MeeReceipt.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReceipt.Name = "MeeReceipt";
            this.MeeReceipt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.Appearance.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReceipt.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReceipt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeReceipt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReceipt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReceipt.Properties.MaxLength = 400;
            this.MeeReceipt.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeReceipt.Properties.ShowIcon = false;
            this.MeeReceipt.Size = new System.Drawing.Size(248, 20);
            this.MeeReceipt.TabIndex = 59;
            this.MeeReceipt.ToolTip = "F4 : Show/hide text";
            this.MeeReceipt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReceipt.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(105, 112);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 14);
            this.label15.TabIndex = 64;
            this.label15.Text = "Mark";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDischarge
            // 
            this.LueDischarge.EnterMoveNextControl = true;
            this.LueDischarge.Location = new System.Drawing.Point(140, 67);
            this.LueDischarge.Margin = new System.Windows.Forms.Padding(5);
            this.LueDischarge.Name = "LueDischarge";
            this.LueDischarge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.Appearance.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDischarge.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDischarge.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDischarge.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDischarge.Properties.DropDownRows = 30;
            this.LueDischarge.Properties.NullText = "[Empty]";
            this.LueDischarge.Properties.PopupWidth = 300;
            this.LueDischarge.Size = new System.Drawing.Size(248, 20);
            this.LueDischarge.TabIndex = 61;
            this.LueDischarge.ToolTip = "F4 : Show/hide list";
            this.LueDischarge.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDischarge.EditValueChanged += new System.EventHandler(this.LueDischarge_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(36, 71);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 14);
            this.label8.TabIndex = 60;
            this.label8.Text = "Port of Discharge";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(42, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 14);
            this.label3.TabIndex = 58;
            this.label3.Text = "Place of Receipt";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLoading
            // 
            this.LueLoading.EnterMoveNextControl = true;
            this.LueLoading.Location = new System.Drawing.Point(140, 4);
            this.LueLoading.Margin = new System.Windows.Forms.Padding(5);
            this.LueLoading.Name = "LueLoading";
            this.LueLoading.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.Appearance.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLoading.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLoading.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLoading.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLoading.Properties.DropDownRows = 30;
            this.LueLoading.Properties.NullText = "[Empty]";
            this.LueLoading.Properties.PopupWidth = 300;
            this.LueLoading.Size = new System.Drawing.Size(248, 20);
            this.LueLoading.TabIndex = 55;
            this.LueLoading.ToolTip = "F4 : Show/hide list";
            this.LueLoading.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLoading.EditValueChanged += new System.EventHandler(this.LueLoading_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(46, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 14);
            this.label2.TabIndex = 54;
            this.label2.Text = "Port of Loading";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(119, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(82, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(119, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtDocNo.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(42, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgSI
            // 
            this.TpgSI.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TpgSI.Controls.Add(this.TpgResult);
            this.TpgSI.Controls.Add(this.TpgBL);
            this.TpgSI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TpgSI.Location = new System.Drawing.Point(0, 353);
            this.TpgSI.Multiline = true;
            this.TpgSI.Name = "TpgSI";
            this.TpgSI.SelectedIndex = 0;
            this.TpgSI.ShowToolTips = true;
            this.TpgSI.Size = new System.Drawing.Size(804, 157);
            this.TpgSI.TabIndex = 23;
            // 
            // TpgResult
            // 
            this.TpgResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgResult.Controls.Add(this.Grd1);
            this.TpgResult.Controls.Add(this.panel4);
            this.TpgResult.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgResult.Location = new System.Drawing.Point(4, 26);
            this.TpgResult.Name = "TpgResult";
            this.TpgResult.Size = new System.Drawing.Size(796, 127);
            this.TpgResult.TabIndex = 0;
            this.TpgResult.Text = "Sales Order ";
            this.TpgResult.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(792, 86);
            this.Grd1.TabIndex = 85;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit_1);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 86);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(792, 37);
            this.panel4.TabIndex = 64;
            // 
            // TpgBL
            // 
            this.TpgBL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgBL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgBL.Controls.Add(this.LueBL);
            this.TpgBL.Controls.Add(this.Grd2);
            this.TpgBL.Location = new System.Drawing.Point(4, 26);
            this.TpgBL.Name = "TpgBL";
            this.TpgBL.Size = new System.Drawing.Size(764, 0);
            this.TpgBL.TabIndex = 1;
            this.TpgBL.Text = "B/L Requirement";
            this.TpgBL.UseVisualStyleBackColor = true;
            // 
            // LueBL
            // 
            this.LueBL.EnterMoveNextControl = true;
            this.LueBL.Location = new System.Drawing.Point(100, 25);
            this.LueBL.Margin = new System.Windows.Forms.Padding(5);
            this.LueBL.Name = "LueBL";
            this.LueBL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBL.Properties.Appearance.Options.UseFont = true;
            this.LueBL.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBL.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBL.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBL.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBL.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBL.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBL.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBL.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBL.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBL.Properties.DropDownRows = 12;
            this.LueBL.Properties.NullText = "[Empty]";
            this.LueBL.Properties.PopupWidth = 500;
            this.LueBL.Size = new System.Drawing.Size(171, 20);
            this.LueBL.TabIndex = 53;
            this.LueBL.ToolTip = "F4 : Show/hide list";
            this.LueBL.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBL.EditValueChanged += new System.EventHandler(this.LueBL_EditValueChanged);
            this.LueBL.Leave += new System.EventHandler(this.LueBL_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 0);
            this.Grd2.TabIndex = 86;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.textEdit4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(748, 35);
            this.panel7.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(18, 8);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 33;
            this.label10.Text = "Total";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(57, 6);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 16;
            this.textEdit4.Size = new System.Drawing.Size(118, 20);
            this.textEdit4.TabIndex = 18;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.textEdit5);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(748, 35);
            this.panel8.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(18, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 14);
            this.label11.TabIndex = 33;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(57, 6);
            this.textEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 16;
            this.textEdit5.Size = new System.Drawing.Size(118, 20);
            this.textEdit5.TabIndex = 18;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.textEdit6);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(748, 35);
            this.panel9.TabIndex = 36;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(18, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 33;
            this.label12.Text = "Total";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(57, 6);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 16;
            this.textEdit6.Size = new System.Drawing.Size(118, 20);
            this.textEdit6.TabIndex = 18;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.label13);
            this.panel10.Controls.Add(this.textEdit7);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(748, 35);
            this.panel10.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(18, 8);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 14);
            this.label13.TabIndex = 33;
            this.label13.Text = "Total";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(57, 6);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.MaxLength = 16;
            this.textEdit7.Size = new System.Drawing.Size(118, 20);
            this.textEdit7.TabIndex = 18;
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 468);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 12;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 488);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 13;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // FrmSI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 510);
            this.Name = "FrmSI";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteCloseDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCloseDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseTme.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StuffTme.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConsignee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStfDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPEB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKpbc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDischargeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDischargeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLoadingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLoadingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShippingLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMotherVessel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFeeder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProforma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReceipt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDischarge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLoading.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.TpgSI.ResumeLayout(false);
            this.TpgResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgBL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit  TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TpgSI;
        private System.Windows.Forms.TabPage TpgResult;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgBL;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit textEdit5;
        protected System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        protected System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit textEdit7;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueLoading;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueNotify;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        protected System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtBL;
        private DevExpress.XtraEditors.MemoExEdit MeeDelivery;
        private DevExpress.XtraEditors.MemoExEdit MeeReceipt;
        internal DevExpress.XtraEditors.TextEdit TxtHsCode;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtKpbc;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label33;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtSPDocNo;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnSP;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        internal DevExpress.XtraEditors.TextEdit TxtPEB;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.MemoExEdit MeeMark;
        internal DevExpress.XtraEditors.TextEdit TxtSize;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnSP2;
        private DevExpress.XtraEditors.LookUpEdit LueBL;
        internal DevExpress.XtraEditors.DateEdit DteStfDt;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.MemoExEdit MeeConsignee;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.MemoExEdit MeeNotify;
        public DevExpress.XtraEditors.SimpleButton BtnSource;
        internal DevExpress.XtraEditors.TextEdit TxtSource;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.LookUpEdit LueDischarge;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label LblLocalDoc;
        private DevExpress.XtraEditors.CheckEdit ChkProforma;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark2;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtShippingLine;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtMotherVessel;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtFeeder;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.LookUpEdit LueVdCode;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.LookUpEdit LueVdCode2;
        internal DevExpress.XtraEditors.TextEdit TxtPIUser;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TimeEdit StuffTme;
        internal DevExpress.XtraEditors.TimeEdit CloseTme;
        internal DevExpress.XtraEditors.DateEdit DteCloseDt;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.DateEdit DteLoadingDt;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.DateEdit DteDischargeDt;
        public DevExpress.XtraEditors.SimpleButton BtnConsignee;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD1;
        private System.Windows.Forms.OpenFileDialog OD1;

    }
}