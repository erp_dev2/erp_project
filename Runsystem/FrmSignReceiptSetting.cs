﻿#region Update
/*
   25/04/2022 [DITA/PHT] NEW APPS
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSignReceiptSetting : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty
            ;
        iGCell fCell;
        bool fAccept;

         internal FrmSignReceiptSettingFind FrmFind;

        #endregion

        #region Constructor

        public FrmSignReceiptSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmSignReceiptSetting");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueProfitCenterCode(ref LueProfitCenterCode, string.Empty, "Y");
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetLueDeptCodeFilterByProfitCenter(ref LueDeptCode, string.Empty,string.Empty);
                LueSignCode.Visible = false;
                LuePosCode.Visible = false;
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "DNo",

                    //1-5
                    "Sign Code",
                    "Sign",
                    "Position Code",
                    "Position",
                    "Start Amount",

                    //6
                    "End Amount",

                   
                },
                new int[]
                {
                    10,
                    100, 150, 100, 200, 150, 
                    150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3 });

        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, ChkCancelInd, TxtDocType, LueSiteCode, LueProfitCenterCode, LueDeptCode
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtDocType, LueSiteCode, LueProfitCenterCode, LueDeptCode
                    }, false);
                    DteDocDt.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocType, LueSiteCode, LueProfitCenterCode, LueDeptCode, ChkCancelInd
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocType.Focus();
                    Grd1.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtDocNo, DteDocDt, TxtDocType, LueSiteCode, LueProfitCenterCode, LueDeptCode
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSignReceiptSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()
               ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SignReceiptSetting", "TblSignReceiptSettinghdr");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveSignReceiptSettingHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSignReceiptSettingDtl(DocNo, Row));
            }
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtDocType, "Document Type", false) ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Profit Center") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsDocumentAlreadyCancel() ||
                IsSignReceiptSettingExisted() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblSignReceiptSettingHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }
        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Sign is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Position is empty.")) return true;
               
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 sign setting.");
                return true;
            }

            return false;
        }

        private bool IsSignReceiptSettingExisted()
        {
            if (TxtDocNo.Text.Length <= 0)
            {
                if (Sm.IsDataExist("Select 1 From TblSignReceiptSettingHdr Where DocType ='" + TxtDocType.Text +
                    "' And SiteCode='" + Sm.GetLue(LueSiteCode) +
                    "' And ProfitCenterCode='" + Sm.GetLue(LueProfitCenterCode) +
                    "' And DeptCode='" + Sm.GetLue(LueDeptCode) + "' And CancelInd = 'N'; "))
                {
                    Sm.StdMsg(mMsgType.Warning, "This setting already existed.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveSignReceiptSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Sign Receipt Setting - Hdr */ ");
            SQL.AppendLine("Insert Into TblSignReceiptSettingHdr(DocNo, DocDt, CancelInd, DocType, SiteCode, ProfitCenterCode, DeptCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, @DocType, @SiteCode, @ProfitCenterCode, @DeptCode, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DocType", TxtDocType.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSignReceiptSettingDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Sign Receipt Setting - Dtl */ ");
            SQL.AppendLine("Insert Into TblSignReceiptSettingDtl (DocNo, DNo, SignCode, PosCode, StartAmt, EndAmt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SignCode, @PosCode, @StartAmt, @EndAmt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

            SQL.AppendLine("On Duplicate Key Update ");

            SQL.AppendLine("    SignCode = @SignCode, PosCode = @PosCode, ");
            SQL.AppendLine("    StartAmt = @StartAmt, EndAmt = @EndAmt, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SignCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@StartAmt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@EndAmt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            var SQL = new StringBuilder();
            cml.Add(UpdateSignReceiptSettingHdr(TxtDocNo.Text));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSignReceiptSettingDtl(TxtDocNo.Text, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }
        private MySqlCommand UpdateSignReceiptSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* Update Sign Receipt Setting - Hdr */ ");
            SQL.AppendLine("Update TblSignReceiptSettingHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, ");
            SQL.AppendLine("    DocType = @DocType, SiteCode = @SiteCode, ");
            SQL.AppendLine("    ProfitCenterCode = @ProfitCenterCode, DeptCode = @DeptCode, ");
            SQL.AppendLine("    LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            SQL.AppendLine("Delete From TblSignReceiptSettingDtl Where DocNo=@DocNo; ");


           cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DocType", TxtDocType.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSignReceiptSettingHdr(DocNo);
                ShowSignReceiptSettingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSignReceiptSettingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, DocType,   ");
            SQL.AppendLine("SiteCode, ProfitCenterCode, DeptCode ");
            SQL.AppendLine("From TblSignReceiptSettingHdr  ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "DocType", "SiteCode", "ProfitCenterCode", 
                        
                        //6
                        "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtDocType.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                    }, true
                );
        }

        private void ShowSignReceiptSettingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.SignCode, B.OptDesc SignName, A.PosCode, C.PosName, A.StartAmt, A.EndAmt ");
            SQL.AppendLine("From TblSignReceiptSettingDtl A ");
            SQL.AppendLine("Inner Join TblOption B On A.SignCode = B.OptCode And B.OptCat = 'SignReceiptSettingType' ");
            SQL.AppendLine("Inner Join TblPosition C On A.PosCode = C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "SignCode", "SignName", "PosCode", "PosName", "StartAmt", 
                    
                    //6
                    "EndAmt",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                   
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }
        #endregion

        #region Grid Method

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 2)
                {
                    LueRequestEdit(Grd1, LueSignCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sl.SetLueOption(ref LueSignCode, "SignReceiptSettingType");
                }
                if (e.ColIndex == 4)
                {
                    LueRequestEdit(Grd1, LuePosCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sl.SetLuePosCode(ref LuePosCode);
                }

                if (Sm.IsGrdColSelected(new int[] { 2, 4 }, e.ColIndex) && BtnSave.Enabled)
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6});
                }

            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }


        #endregion

        #region Additional Method

        private void SetLueDeptCodeFilterByProfitCenter(ref DXE.LookUpEdit Lue, string ProfitCenterCode, string Code)
        {
            var SQL = new StringBuilder();

            if (Code.Length > 0)
            {
                SQL.AppendLine("Select DeptCode As Col1, DeptName As Col2 From TblDepartment ");
                SQL.AppendLine("Where DeptCode=@Code;");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.DeptCode As Col1, B.DeptName As Col2 ");
                SQL.AppendLine("From TblCostCenter A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode And A.DeptCode Is Not Null ");
                SQL.AppendLine("Where A.NotParentInd='Y' ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("And A.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("And ProfitCenterCode In (");
                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("And ProfitCenterCode = @ProfitCenterCode");
                SQL.AppendLine("Order By B.DeptName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDocType);
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue3(Sl.SetLueProfitCenterCode), string.Empty, "Y");
            
            LueDeptCode.EditValue = null;
            string ProfitCenterCode = Sm.GetLue(LueProfitCenterCode);
            LueDeptCode.EditValue = "<Refresh>";
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCodeFilterByProfitCenter), ProfitCenterCode, string.Empty);


        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueProfitCenterCode).Length > 0)
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCodeFilterByProfitCenter), Sm.GetLue(LueProfitCenterCode), string.Empty);
        }

        private void LueSignCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSignCode, new Sm.RefreshLue2(Sl.SetLueOption), "SignReceiptSettingType");
        }

        private void LueSignCode_Leave(object sender, EventArgs e)
        {
            if (LueSignCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueSignCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value =
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueSignCode);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueSignCode.GetColumnValue("Col2");
                }
                LueSignCode.Visible = false;
            }
        }

        private void LueSignCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSignCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LuePosCode_Leave(object sender, EventArgs e)
        {
            if (LuePosCode.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LuePosCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value =
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LuePosCode);
                    Grd1.Cells[fCell.RowIndex, 4].Value = LuePosCode.GetColumnValue("Col2");
                }
                LuePosCode.Visible = false;
            }
        }

        private void LuePosCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }



        #endregion
    }
}
