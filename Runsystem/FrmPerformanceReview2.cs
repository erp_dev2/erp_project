﻿#region Update
/*
    18/05/2022 [MYA/HIN] New Apps New Performance Review
    23/05/2022 [ICA/HIN] Penyesuaian Printout
    23/11/2022 [HPH/HIN] Penyesuaian Printout
    23/11/2022 [HPH/HIN] Penyesuaian tampilan fields, menambah fields pada setiap tab agar mempermudah user melihat total weight dan total score tiap tab
    29/11/2022 [BRI/HIN] Bug validasi Grd
    08/12/2022 [WED/HIN] bug docapproval type
    14/12/2022 [WED/HIN] bug fix docapproval
    08/02/2023 [VIN/HIN] feedback approval performancereview2, ngelihat site dan dept

 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceReview2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty, GoalsProcessDocNo = string.Empty;
        internal FrmPerformanceReview2Find FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPerformanceReview2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueOption(ref LuePeriod, "GoalsProcessPeriode");
                SetLueGrdCode(ref LueGrdCode);
                LueNoteRecommendation.Visible = false;
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, GoalsProcessDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and"+Environment.NewLine+"Target",
                        "EvaluationCode",
                        "Type",

                        //6-9
                        "Realization"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 120, 100, 150,
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 });

            #endregion

            #region Grid2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and"+Environment.NewLine+"Target",
                        "EvaluationCode",
                        "Type",

                        //6-9
                        "Realization"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 120, 100, 150, 
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd2, new int[] { 4 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 10;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and"+Environment.NewLine+"Target",
                        "EvaluationCode",
                        "Type",

                        //6-9
                        "Realization"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 120, 100, 150, 
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd3, new int[] { 4 });

            #endregion

            #region Grid4

            Grd4.Cols.Count = 10;
            Grd4.FrozenArea.ColCount = 1;
            Grd4.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and"+Environment.NewLine+"Target",
                        "EvaluationCode",
                        "Type",

                        //6-9
                        "Realization"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                       //0
                        0,
 
                        //1-5
                        200, 120, 120, 100, 150, 
                        
                        //6-8
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd4, new int[] { 4 });

            #endregion

            #region Grid5

            Grd5.Cols.Count = 4;
            Grd5.FrozenArea.ColCount = 1;
            Grd5.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-3
                        "Note Code",
                        "Note and Recomendation",
                        "Remark",
                    },
                    new int[]
                    {
                       //0
                        0,
 
                        //1-5
                        100, 200, 200,
                    }
                );
            //Sm.GrdFormatDec(Grd5, new int[] { 2, 6, 7, 8 }, 0);-
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1 });

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {


        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, TxtPICName, MeeRemark, TxtGoalsProcessDocNo,
                        TxtPICCode, LueYr, LuePeriod, MeeCancelReason, ChkCancelInd,
                        TxtEvaluator, TxtTotalScore, LueGrdCode ,TxtStatus, LueNoteRecommendation, TxtTotalWeightLearning, 
                        TxtTotalScoreLearning, TxtTotalScoreInternalProcess, TxtTotalWeightInternalProcess, TxtTotalWeightCustomer, TxtTotalScoreCustomer,
                        TxtTotalWeightFinancial, TxtTotalScoreFinancial
                    }, true);
                    BtnGoals.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 1, 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, MeeRemark, LueNoteRecommendation
                    }, false);
                    BtnGoals.Enabled = true;
                    //Sm.GrdColReadOnly(false, true, Grd2, new int[] { 6, 9 });
                    //Sm.GrdColReadOnly(false, true, Grd3, new int[] { 6, 9 });
                    //Sm.GrdColReadOnly(false, true, Grd4, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 1, 2, 3 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      ChkCancelInd, MeeCancelReason, LueNoteRecommendation
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 6, 9 });
                    if (TxtStatus.Text == "Approved")
                    {
                        Sm.GrdColReadOnly(true, true, Grd5, new int[] { 1, 2, 3 });
                    }
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtPICName,  MeeRemark, TxtStatus, TxtPICCode, LueGrdCode,
                MeeCancelReason, LueYr, LuePeriod, TxtGoalsProcessDocNo, TxtEvaluator, LueNoteRecommendation
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotalScore, TxtTotalScoreLearning, TxtTotalWeightLearning, TxtTotalScoreInternalProcess, 
                TxtTotalWeightInternalProcess, TxtTotalWeightCustomer, TxtTotalScoreCustomer,
                TxtTotalWeightFinancial, TxtTotalScoreFinancial
            }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd5, true);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPerformanceReview2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint();
        }

        #endregion

        #region Grid Method

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {          
                if (BtnSave.Enabled)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeTotalScore();
                    ComputeTotalWeight();
                    ComputeTotalScoreDtl();
                }
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {         
                if (BtnSave.Enabled)
                {
                    Sm.GrdRemoveRow(Grd2, e, BtnSave);
                    ComputeTotalScore();
                    ComputeTotalWeight();
                    ComputeTotalScoreDtl();                    
                }
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputeTotalScore();
                ComputeTotalWeight();
                ComputeTotalScoreDtl();
            }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                ComputeTotalScore();
                ComputeTotalWeight();
                ComputeTotalScoreDtl();               
            }           
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
            
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd5, LueNoteRecommendation, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                Sl.SetLueOption(ref LueNoteRecommendation, "NoteRecommendation");
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            var IsNeedApproval = IsDocNeedApproval();

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PerformanceReview", "TblNewPerformanceReviewHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveNewPerformanceReviewHdr(DocNo, IsNeedApproval));


            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveNewPerformanceReviewDtl(DocNo, Row));
            }

            if (IsNeedApproval && TxtDocNo.Text.Length == 0)
                cml.Add(SaveDocApproval(DocNo));

            Sm.ExecCommands(cml);
            string GoalsProcessDocNo = TxtGoalsProcessDocNo.Text;
            ShowData(DocNo, GoalsProcessDocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPICName, "PIC Name", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LuePeriod, "Period") ||
                //IsDocNoAlreadyProcess() ||
                IsGoalsProcessDocNoAlreadyProcess() ||
                IsDataCancelledAlready() ||
                IsGrdEmpty();
        }

        private bool IsDocNoAlreadyProcess()
        {
            if (ChkCancelInd.Checked)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select GoalsProcessDocNo From TblNewPerformanceReviewHdr Where GoalsProcessDocNo=@GoalsProcessDocNo And CancelInd='N'"
                };

                Sm.CmParam<String>(ref cm, "@GoalsProcessDocNo", TxtGoalsProcessDocNo.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGoalsProcessDocNoAlreadyProcess()
        {
            if (TxtDocNo.Text.Length == 0)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select GoalsProcessDocNo From TblNewPerformanceReviewHdr Where GoalsProcessDocNo=@GoalsProcessDocNo " +
                    "And CancelInd = 'N' And Yr=@Yr And Period=@Period;"
                };

                Sm.CmParam<String>(ref cm, "@GoalsProcessDocNo", TxtGoalsProcessDocNo.Text);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Goals document already Process in " + LuePeriod.Text + " - " + Sm.GetLue(LueYr) + ".");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1 && Grd3.Rows.Count == 1 && Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblNewPerformanceReviewHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveNewPerformanceReviewHdr(string DocNo, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblNewPerformanceReviewHdr(DocNo, DocDt, Status, Evaluator, GoalsProcessDocNo, Yr, Period, TotalScore, GrdCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Status, @Evaluator, @GoalsProcessDocNo, @Yr, @Period, @TotalScore, @GrdCode, @Remark, @UserCode, CurrentDateTime()) ");
            //region bisa edit hdr dan detail
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, CancelInd=@CancelInd, CancelReason=@CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblNewPerformanceReviewDtl Where DocNo=@DocNo; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", IsNeedApproval ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@Evaluator", TxtEvaluator.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@GoalsProcessDocNo", TxtGoalsProcessDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));
            Sm.CmParam<Decimal>(ref cm, "@TotalScore", Convert.ToDecimal(TxtTotalScore.Text));
            Sm.CmParam<String>(ref cm, "@GrdCode", Sm.GetLue(LueGrdCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveNewPerformanceReviewDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblNewPerformanceReviewDtl(DocNo, DNo, NoteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @NoteCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@NoteCode", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Inner Join tblnewperformancereviewhdr A ON  A.DocNo=@DocNo ");
            SQL.AppendLine("Inner Join TblGoalsProcessHdr B On A.GoalsProcessDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblGoalsSettingHdr C On B.GoalsDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join tblemployee D ON C.PICCode=D.EmpCode AND T.DeptCode=D.DeptCode AND T.SiteCode=D.SiteCode ");
            SQL.AppendLine("Where T.DocType='PerformanceReview2' ");
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblNewPerformanceReviewHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PerformanceReview2' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string GoalsProcessDocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowNewPerformanceReviewHdr(DocNo);
                ShowNewPerformanceReviewDtl(DocNo);
                ShowGoalsProcessDtl(GoalsProcessDocNo);
                ShowGoalsProcessDtl2(GoalsProcessDocNo);
                ShowGoalsProcessDtl3(GoalsProcessDocNo);
                ShowGoalsProcessDtl4(GoalsProcessDocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowNewPerformanceReviewHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("	when A.`Status` = 'O' then 'Outstanding'  ");
            SQL.AppendLine("	when A.`Status` = 'A' then 'Approved'  ");
            SQL.AppendLine("	when A.`Status` = 'C' then 'Cancelled'  ");
            SQL.AppendLine("END AS `Status`,   ");
            SQL.AppendLine("A.Evaluator, A.CancelInd, A.CancelReason, A.GoalsProcessDocNo, A.Yr, A.Period, ");
            SQL.AppendLine("A.TotalScore, A.GrdCode, C.PICCode, D.EmpName, A.Remark");
            SQL.AppendLine("From tblNewPerformanceReviewHdr A ");
            SQL.AppendLine("Inner Join TblGoalsProcessHdr B On A.GoalsProcessDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblGoalsSettingHdr C On B.GoalsDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On C.PICCode = D.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Status", "Evaluator", "CancelReason", "Cancelind",  
                        
                        //6-10
                        "GoalsProcessDocNo", "Yr", "Period", "TotalScore", "GrdCode",
                        
                        //11-13
                        "PICCode", "EmpName", "Remark"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        TxtEvaluator.EditValue = Sm.DrStr(dr, c[3]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                        TxtGoalsProcessDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LuePeriod, Sm.DrStr(dr, c[8]));
                        TxtTotalScore.EditValue = Sm.DrDec(dr, c[9]);
                        Sm.SetLue(LueGrdCode, Sm.DrStr(dr, c[10]));
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[12]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                    }, true
                );
        }

        private void ShowNewPerformanceReviewDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.DNo, B.NoteCode, C.OptDesc as Note, B.Remark ");
            SQL.AppendLine("FROM TblNewPerformanceReviewHdr A ");
            SQL.AppendLine("INNER JOIN TblNewPerformanceReviewDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN tbloption C ON C.OptCat = 'NoteRecommendation' and B.NoteCode = C.OptCode");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-2
                    "NoteCode", "Note", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
        }

        internal void ShowGoalsProcessDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("From TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl B ON B.DocNo = A.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowGoalsProcessDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("From TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl2 B ON B.DocNo = A.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl2 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowGoalsProcessDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("From TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl3 B ON B.DocNo = A.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl3 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        internal void ShowGoalsProcessDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("From TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl4 B ON B.DocNo = A.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl4 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        internal void ShowGoalsSettingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Grd1.Cells[Row, 6].Value = 0m;
                    Grd1.Cells[Row, 7].Value = 0m;
                    Grd1.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowGoalsSettingDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd2.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                    Grd2.Cells[Row, 6].Value = 0m;
                    Grd2.Cells[Row, 7].Value = 0m;
                    Grd2.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowGoalsSettingDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd3.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Grd3.Cells[Row, 6].Value = 0m;
                    Grd3.Cells[Row, 7].Value = 0m;
                    Grd3.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        internal void ShowGoalsSettingDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd4.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                    Grd4.Cells[Row, 6].Value = 0m;
                    Grd4.Cells[Row, 7].Value = 0m;
                    Grd4.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            //mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        private void SetLueGrdCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select GrdCode As Col1, GrdStatus As Col2 ");
                SQL.AppendLine("From TblPerformanceGrade A ");
                //SQL.AppendLine("WHere @Grd > A.GrdProsentaseStart AND @Grd < A.GrdProsentaseEnd ");
                SQL.AppendLine("Order By GrdCode; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                //Sm.CmParam<String>(ref cm, "@Grd", TxtTotalScore.Text);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeTotalScore()
        {

            decimal Test = 0m;
            for (int r = 0; r < Grd1.Rows.Count-1; r++)
            {

                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                    Test += Sm.GetGrdDec(Grd1, r, 8);
            }
            for (int r = 0; r < Grd2.Rows.Count-1; r++)
            {

                if (Sm.GetGrdStr(Grd2, r, 8).Length > 0)
                    Test += Sm.GetGrdDec(Grd2, r, 8);
            }
            for (int r = 0; r < Grd3.Rows.Count-1; r++)
            {

                if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                    Test += Sm.GetGrdDec(Grd3, r, 8);
            }
            for (int r = 0; r < Grd4.Rows.Count-1; r++)
            {

                if (Sm.GetGrdStr(Grd4, r, 8).Length > 0)
                    Test += Sm.GetGrdDec(Grd4, r, 8);
            }

            TxtTotalScore.Text = Sm.FormatNum(Test, 0);
           
        }

        internal void ComputeTotalWeight()
        {
            decimal Test = 0m;
            decimal Test1 = 0m;
            decimal Test2 = 0m;
            decimal Test3 = 0m;

            
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {

                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Test += Sm.GetGrdDec(Grd1, r, 2);
                }
                TxtTotalWeightFinancial.EditValue = Sm.FormatNum(Test, 0);
            
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                        Test1 += Sm.GetGrdDec(Grd2, r, 2);
                }
                TxtTotalWeightCustomer.EditValue = Sm.FormatNum(Test1, 0);

                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd3, r, 2).Length > 0)
                        Test2 += Sm.GetGrdDec(Grd3, r, 2);
                }
                TxtTotalWeightInternalProcess.EditValue = Sm.FormatNum(Test2, 0);

                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd4, r, 2).Length > 0)
                        Test3 += Sm.GetGrdDec(Grd4, r, 2);
                }
                TxtTotalWeightLearning.EditValue = Sm.FormatNum(Test3, 0);
            
        }

        internal void ComputeTotalScoreDtl()
        {
            decimal Test = 0m;
            decimal Test1 = 0m;
            decimal Test2 = 0m;
            decimal Test3 = 0m;

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                        Test += Sm.GetGrdDec(Grd1, r, 8);
                }
                TxtTotalScoreFinancial.EditValue = Sm.FormatNum(Test, 0);
            
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 8).Length > 0)
                        Test1 += Sm.GetGrdDec(Grd2, r, 8);
                }
                TxtTotalScoreCustomer.EditValue = Sm.FormatNum(Test1, 0);
            
                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                        Test2 += Sm.GetGrdDec(Grd3, r, 8);
                }
                TxtTotalScoreInternalProcess.EditValue = Sm.FormatNum(Test2, 0);

                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd4, r, 8).Length > 0)
                        Test3 += Sm.GetGrdDec(Grd4, r, 8);
                }
                TxtTotalScoreLearning.EditValue = Sm.FormatNum(Test3, 0);            
        }
    

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private bool IsDocNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PerformanceReview2' Limit 1;");

        }

        private void ParPrint()
        {
            string[] TableName = { "PerformanceReviewHdr", "PerformanceReviewDtl", "PerformanceReviewDtl2", "PerformanceReviewSignature" };

            string mDocTitle = Sm.GetParameter("DocTitle");

            var l = new List<PerformanceReviewHdr>();
            var ldtl = new List<PerformanceReviewDtl>();
            var ldtl2 = new List<PerformanceReviewDtl2>();
            var lSignature = new List<PerformanceReviewSignature>();

            List<IList> myLists = new List<IList>();

            #region header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT @CompanyLogo CompanyLogo, C.PICCode, D.EmpName PICName, D.UserCode, D.PosName, D.DivisionName, D.SiteName, ");
            SQL.AppendLine("E.Property1 MONTH1, E.Property2 MONTH2, B.Yr, ");
            SQL.AppendLine("F.EmpName Evaluator, G.PosName EvaluatorPosName, F.EmpCode EvaluatorUserCode, H.EmpName TopEvaluator, J.GrdStatus Grade, ");
            SQL.AppendLine("I.PosName TopEvaluatorPosName, H.EmpCode TopEvaluatorUserCode ");
            SQL.AppendLine("FROM TblNewPerformanceReviewHdr A ");           
            SQL.AppendLine("INNER JOIN TblGoalsProcessHdr B ON A.GoalsProcessDocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingHdr C ON B.GoalsDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("	SELECT A.EmpCode, A.EmpName, A.UserCode, B.PosName, C.DivisionName, D.SiteName ");
            SQL.AppendLine("	FROM TblEmployee A  ");
            SQL.AppendLine("	LEFT JOIN TblPosition B ON A.PosCode = B.PosCode  ");
            SQL.AppendLine("	LEFT JOIN TblDivision C ON A.DivisionCode = C.DivisionCode ");
            SQL.AppendLine("	LEFT JOIN TblSite D ON A.SiteCode = D.SiteCode ");
            SQL.AppendLine("	WHERE A.EmpCode = @PICCode ");
            SQL.AppendLine(")D ON C.PICCode = D.EmpCode ");
            SQL.AppendLine("INNER JOIN TblOption E ON E.OptCat = 'GoalsProcessPeriode' AND E.OptCode = B.Period ");
            SQL.AppendLine("LEFT JOIN TblEmployee F ON C.EvaluatorCode = F.EmpCode ");
            SQL.AppendLine("LEFT JOIN TblPosition G ON F.PosCode = G.PosCode ");
            SQL.AppendLine("LEFT JOIN TblEmployee H ON C.TopEvaluatorCode = H.EmpCode ");
            SQL.AppendLine("LEFT JOIN TblPosition I ON H.PosCode = I.PosCode ");
            SQL.AppendLine("LEFT JOIN tblperformancegrade J ON J.GrdCode = A.GrdCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "PICCode",
                         "PICName",
                         "UserCode",
                         "PosName",
                         "DivisionName",
 
                         //6-10
                         "SiteName",
                         "Evaluator",
                         "EvaluatorUserCode",
                         "EvaluatorPosName",
                         "TopEvaluator", 
                         
                         //11-15
                         "TopEvaluatorUserCode",
                         "TopEvaluatorPosName",
                         "Yr",
                         "Month1",
                         "Month2",

                         //16
                         "Grade",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PerformanceReviewHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            PICCode = Sm.DrStr(dr, c[1]),
                            PICName = Sm.DrStr(dr, c[2]),
                            PICUserCode = Sm.DrStr(dr, c[3]),
                            PICPosName = Sm.DrStr(dr, c[4]),
                            PICDivisionName = Sm.DrStr(dr, c[5]),
                            PICSiteName = Sm.DrStr(dr, c[6]),
                            EvaluatorName = Sm.DrStr(dr, c[7]),
                            EvaluatorUserCode = Sm.DrStr(dr, c[8]),
                            EvaluatorPosName = Sm.DrStr(dr, c[9]),
                            TopEvaluatorName = Sm.DrStr(dr, c[10]),
                            TopEvaluatorUserCode = Sm.DrStr(dr, c[11]),
                            TopEvaluatorPosName = Sm.DrStr(dr, c[12]),
                            Year = Sm.DrStr(dr, c[13]),
                            Month = Sm.DrStr(dr, c[14]),
                            Month2 = Sm.DrStr(dr, c[15]),
                            Grade = Sm.DrStr(dr, c[16]),
                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail1

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("SELECT C.DNo, 'Financial' AS Perspective, D.WorkGoals, D.Quality, D.Measure, E.OptDesc Evaluation, ");
            SQLDtl.AppendLine("C.Realization, C.Value, C.QualityValue, C.RealizationProof, '1' as Seq ");
            SQLDtl.AppendLine("FROM TblNewPerformanceReviewHdr A ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessHdr B ON A.GoalsProcessDocNo = B.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessDtl C ON B.DocNo = C.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsSettingDtl D ON B.GoalsDocNo = D.DocNo AND C.DNo = D.DNo ");
            SQLDtl.AppendLine("LEFT JOIN TblOption E ON E.OptCat = 'GoalsSettingEvaluation' AND E.OptCode = D.Evaluation ");
            SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");

            SQLDtl.AppendLine("Union All ");

            SQLDtl.AppendLine("SELECT C.DNo, 'Customer' AS Perspective, D.WorkGoals, D.Quality, D.Measure, E.OptDesc Evaluation, ");
            SQLDtl.AppendLine("C.Realization, C.Value, C.QualityValue, C.RealizationProof, '2' as Seq ");
            SQLDtl.AppendLine("FROM TblNewPerformanceReviewHdr A ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessHdr B ON A.GoalsProcessDocNo = B.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessDtl2 C ON B.DocNo = C.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsSettingDtl2 D ON B.GoalsDocNo = D.DocNo AND C.DNo = D.DNo ");
            SQLDtl.AppendLine("LEFT JOIN TblOption E ON E.OptCat = 'GoalsSettingEvaluation' AND E.OptCode = D.Evaluation ");
            SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");

            SQLDtl.AppendLine("Union All ");

            SQLDtl.AppendLine("SELECT C.DNo, 'Internal Business Process' AS Perspective, D.WorkGoals, D.Quality, D.Measure, E.OptDesc Evaluation, ");
            SQLDtl.AppendLine("C.Realization, C.Value, C.QualityValue, C.RealizationProof, '3' as Seq ");
            SQLDtl.AppendLine("FROM TblNewPerformanceReviewHdr A ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessHdr B ON A.GoalsProcessDocNo = B.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessDtl3 C ON B.DocNo = C.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsSettingDtl3 D ON B.GoalsDocNo = D.DocNo AND C.DNo = D.DNo ");
            SQLDtl.AppendLine("LEFT JOIN TblOption E ON E.OptCat = 'GoalsSettingEvaluation' AND E.OptCode = D.Evaluation ");
            SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");

            SQLDtl.AppendLine("Union All ");

            SQLDtl.AppendLine("SELECT C.DNo, 'People Development' AS Perspective, D.WorkGoals, D.Quality, D.Measure, E.OptDesc Evaluation, ");
            SQLDtl.AppendLine("C.Realization, C.Value, C.QualityValue, C.RealizationProof, '4' as Seq ");
            SQLDtl.AppendLine("FROM TblNewPerformanceReviewHdr A ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessHdr B ON A.GoalsProcessDocNo = B.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsProcessDtl4 C ON B.DocNo = C.DocNo ");
            SQLDtl.AppendLine("INNER JOIN TblGoalsSettingDtl4 D ON B.GoalsDocNo = D.DocNo AND C.DNo = D.DNo ");
            SQLDtl.AppendLine("LEFT JOIN TblOption E ON E.OptCat = 'GoalsSettingEvaluation' AND E.OptCode = D.Evaluation ");
            SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "DNo",

                         //1-5
                         "Perspective",
                         "WorkGoals",
                         "Quality",
                         "Measure",
                         "Evaluation",

                         //6-9
                         "Realization",
                         "Value",
                         "QualityValue",
                         "RealizationProof",
                         "Seq"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new PerformanceReviewDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Perspective = Sm.DrStr(drDtl, cDtl[1]),
                            WorkGoals = Sm.DrStr(drDtl, cDtl[2]),
                            Quality = Sm.DrDec(drDtl, cDtl[3]),
                            Mesure = Sm.DrStr(drDtl, cDtl[4]),
                            Evaluation = Sm.DrStr(drDtl, cDtl[5]),
                            Realization = Sm.DrDec(drDtl, cDtl[6]),
                            Value = Sm.DrDec(drDtl, cDtl[7]),
                            QualityValue = Sm.DrDec(drDtl, cDtl[8]),
                            RealizationProof = Sm.DrStr(drDtl, cDtl[9]),
                            Sequence = Sm.DrStr(drDtl, cDtl[10]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("SELECT A.OptCode NoteCode, A.OptDesc NoteName, A.Property1 NoteDesc, B.Remark ");
            SQLDtl2.AppendLine("FROM TblOption A ");
            SQLDtl2.AppendLine("LEFT JOIN tblnewperformancereviewdtl B ON A.OptCat = 'NoteRecommendation' AND A.OptCode = B.NoteCode AND B.DocNo = @DocNo ");
            SQLDtl2.AppendLine("WHERE A.OptCat = 'NoteRecommendation' ");

            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                        {
                         //0
                         "NoteCode",

                         //1-5
                         "NoteName",
                         "NoteDesc",
                         "Remark",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new PerformanceReviewDtl2()
                        {
                            NoteCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            NoteName = Sm.DrStr(drDtl2, cDtl2[1]),
                            NoteDesc = Sm.DrStr(drDtl2, cDtl2[2]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[3]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature

            var cmSign = new MySqlCommand();

            var SQLSign = new StringBuilder();
            using (var cnSign = new MySqlConnection(Gv.ConnectionString))
            {
                cnSign.Open();
                cmSign.Connection = cnSign;

                SQLSign.AppendLine("SELECT D.EmpName CreateBy, DATE_FORMAT(LEFT(A.CreateDt, 8), '%d/%m/%Y') CreateDt, B.EmpName, B.Date, C.EmpName EmpName2, C.Date Date2 ");
                SQLSign.AppendLine("FROM TblNewPerformanceReviewHdr A ");
                SQLSign.AppendLine("LEFT JOIN ( ");
                SQLSign.AppendLine("	SELECT A.DocNo, A.UserCode, C.EmpName, DATE_FORMAT(LEFT(A.LastUpDt, 8), '%d/%m/%Y') Date ");
                SQLSign.AppendLine("	FROM TblDocApproval A ");
                SQLSign.AppendLine("	INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQLSign.AppendLine("		AND A.DocNo = @DocNo ");
                SQLSign.AppendLine("		AND A.ApprovalDNo = B.DNo ");
                SQLSign.AppendLine("		AND A.Status = 'A' ");
                SQLSign.AppendLine("		AND B.Level = '1' ");
                SQLSign.AppendLine("	INNER JOIN TblEmployee C ON A.UserCode = C.UserCode ");
                SQLSign.AppendLine(")B ON A.DocNo = B.DocNo ");
                SQLSign.AppendLine("LEFT JOIN ( ");
                SQLSign.AppendLine("	SELECT A.DocNo, A.UserCode, C.EmpName, DATE_FORMAT(LEFT(A.LastUpDt, 8), '%d/%m/%Y') Date ");
                SQLSign.AppendLine("	FROM TblDocApproval A ");
                SQLSign.AppendLine("	INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQLSign.AppendLine("		AND A.DocNo = @DocNo ");
                SQLSign.AppendLine("		AND A.ApprovalDNo = B.DNo ");
                SQLSign.AppendLine("		AND A.Status = 'A' ");
                SQLSign.AppendLine("		AND B.Level = '2' ");
                SQLSign.AppendLine("	INNER JOIN TblEmployee C ON A.UserCode = C.UserCode ");
                SQLSign.AppendLine(")C ON A.DocNo = C.DocNo ");
                SQLSign.AppendLine("Left Join TblEmployee D On A.CreateBy = D.UserCode ");
                SQLSign.AppendLine("WHERE A.DocNo = @DocNo  ");


                cmSign.CommandText = SQLSign.ToString();
                Sm.CmParam<String>(ref cmSign, "@DocNo", TxtDocNo.Text);
                var drSign = cmSign.ExecuteReader();
                var cSign = Sm.GetOrdinal(drSign, new string[]
                        {
                         //0
                         "CreateBy",

                         //1-5
                         "CreateDt",
                         "EmpName" ,
                         "Date" ,
                         "EmpName2",
                         "Date2",
                        });
                if (drSign.HasRows)
                {
                    while (drSign.Read())
                    {

                        lSignature.Add(new PerformanceReviewSignature()
                        {
                            CreateBy = Sm.DrStr(drSign, cSign[0]),
                            CreateDt = Sm.DrStr(drSign, cSign[1]),
                            Evaluator = Sm.DrStr(drSign, cSign[2]),
                            ApprovalDt = Sm.DrStr(drSign, cSign[3]),
                            TopEvaluator = Sm.DrStr(drSign, cSign[4]),
                            ApprovalDt2 = Sm.DrStr(drSign, cSign[5]),
                        });
                    }
                }
                drSign.Close();
            }
            myLists.Add(lSignature);
            #endregion

            Sm.PrintReport("PerformanceReview2", myLists, TableName, false);
        }

        #endregion

        #region Event
        private void BtnGoals_Click(object sender, EventArgs e)
        {
            //if (Sm.IsLueEmpty(LueYr, "Year")) return;
            Sm.FormShowDialog(new FrmPerformanceReview2Dlg(this));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtTotalScore_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtTotalScore.Text.Length > 0)
                {
                    string GrdCode = Sm.GetValue("SELECT * FROM tblperformancegrade A WHERE " + TxtTotalScore.Text.Replace(',', '.') + " >= A.GrdProsentaseStart AND " + TxtTotalScore.Text.Replace(',', '.') + "  <= A.GrdProsentaseEnd;");
                    //Sm.FormatNumTxt(TxtTotalScore, 0);
                    Sm.SetLue(LueGrdCode, GrdCode);
                }
            }
        }

        private void TxtTotalScore_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtTotalScore.Text.Length > 0)
                {
                    string GrdCode = Sm.GetValue("SELECT * FROM tblperformancegrade A WHERE " + TxtTotalScore.Text.Replace(',', '.') + " >= A.GrdProsentaseStart AND " + TxtTotalScore.Text.Replace(',', '.') + "  <= A.GrdProsentaseEnd;");
                    //Sm.FormatNumTxt(TxtTotalScore, 0);
                    Sm.SetLue(LueGrdCode, GrdCode);
                }
            }
        }

        private void TxtTotalWeight_EditValueChaged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtTotalWeightLearning.Text.Length > 0)
                {
                    ComputeTotalWeight();
                }
            }   
         }


        #region Misc Control Event

        private void LueGrdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdCode, new Sm.RefreshLue1(SetLueGrdCode));
        }


        #endregion

        #endregion

        #region Event Lue

        private void LueNoteRecommendation_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueNoteRecommendation, new Sm.RefreshLue2(Sl.SetLueOption), "NoteRecommendation");
        }

        private void LueNoteRecommendation_Leave(object sender, EventArgs e)
        {
            if (LueNoteRecommendation.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueNoteRecommendation).Length == 0)
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value =
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                }
                else
                {
                    Grd5.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueNoteRecommendation);
                    Grd5.Cells[fCell.RowIndex, 2].Value = LueNoteRecommendation.GetColumnValue("Col2");
                }
            }
            LueNoteRecommendation.Visible = false;
        }

        private void LueNoteRecommendation_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        #endregion

        #endregion

        #region Class 

        private class PerformanceReviewHdr
        {
            public string CompanyName { get; set; }
            public string CompanyLogo { get; set; }
            public string PICCode { get; set; }
            public string PICName { get; set; }
            public string PICUserCode { get; set; }
            public string PICPosName { get; set; }
            public string PICDivisionName { get; set; }
            public string PICSiteName { get; set; }
            public string Month { get; set; }
            public string Month2 { get; set; }
            public string Year { get; set; }
            public string EvaluatorName { get; set; }
            public string EvaluatorUserCode { get; set; }
            public string EvaluatorPosName { get; set; }
            public string TopEvaluatorName { get; set; }
            public string TopEvaluatorUserCode { get; set; }
            public string TopEvaluatorPosName { get; set; }
            public string Grade { get; set; }
        }

        private class PerformanceReviewDtl
        {
            public string DNo { get; set; }
            public string Perspective { get; set; }
            public string WorkGoals { get; set; }
            public decimal Quality { get; set; }
            public string Mesure { get; set; }
            public string Evaluation { get; set; }
            public decimal Realization { get; set; }
            public decimal Value { get; set; }
            public decimal QualityValue { get; set; }
            public string RealizationProof { get; set; }
            public string Sequence { get; set; }
        }
        
        private class PerformanceReviewDtl2
        {
            public string NoteCode { get; set; }
            public string NoteName { get; set; }
            public string NoteDesc { get; set; }
            public string Remark { get; set; }
        }

        private class PerformanceReviewSignature
        {
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string Evaluator { get; set; }
            public string ApprovalDt { get; set; }
            public string TopEvaluator { get; set; }
            public string ApprovalDt2 { get; set; }
        }

        #endregion
    }
}
