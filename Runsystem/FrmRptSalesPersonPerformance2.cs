﻿#region update
// 27/09/2017 [har] retur pos nilainya minus
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesPersonPerformance2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesPersonPerformance2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                Sl.SetLueSPCode(ref LueSalesPerson);
                SetGrd();
                

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[8].CellStyle = myPercentBarStyle;
                Grd1.Cols[9].CellStyle = myPercentBarStyle;
                Grd1.Cols[10].CellStyle = myPercentBarStyle;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
         
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 200;
            Grd1.Header.Cells[0, 1].Value = "Customer";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 150;
            Grd1.Header.Cells[0, 2].Value = "Sales Person";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 150;
            Grd1.Header.Cells[0, 3].Value = "SO";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 120;
            Grd1.Header.Cells[0, 4].Value = "Date";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Header.Cells[1, 5].Value = "Amount";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 3;

            Grd1.Cols[5].Width = 150;
            Grd1.Header.Cells[0, 5].Value = "SO";
            Grd1.Cols[6].Width = 150;
            Grd1.Header.Cells[0, 6].Value = "DO";
            Grd1.Cols[7].Width = 150;
            Grd1.Header.Cells[0, 7].Value = "Voucher";
            
            Grd1.Cols[8].Width = 100;
            Grd1.Header.Cells[1, 8].Value = "Prosentase % ";
            Grd1.Header.Cells[1, 8].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 8].SpanCols = 3;

            Grd1.Cols[8].Width = 100;
            Grd1.Header.Cells[0, 8].Value = "SO";
            Grd1.Cols[9].Width = 100;
            Grd1.Header.Cells[0, 9].Value = "DO";
            Grd1.Cols[10].Width = 100;
            Grd1.Header.Cells[0, 10].Value = "Voucher";

            Grd1.Cols[11].Width = 120;
            Grd1.Header.Cells[0, 11].Value = "Voucher Date";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;

            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10}, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 11 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[11].Move(5);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start date")) return true;
            if (Sm.IsDteEmpty(DteDocDt2, "End date")) return true;

            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            var LSI = new List<L1>();

            try
            {
                Process1(ref LSI);
                Process2(ref LSI);
                Process3(ref LSI);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<L1> LP1)
        {
            LP1.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select Distinct A.InvoiceDocNo As SIDocNo, DATE_FORMAT(B.DocDt,'%d/%b/%Y') As VcDate ");
            SQL.AppendLine("    From  ");
            SQL.AppendLine("    TblIncomingPaymentDtl A ");
            SQL.AppendLine("    Inner Join   ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select C.DocNo, SUM(E.Amt) As AmtInvoice, C.Amt As AmtIP, B.Amt As AmtVc, A.DocDt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo And B.CancelInd = 'N' And B.Status = 'A' ");
            SQL.AppendLine("        Inner Join TblInComingPaymentHdr C On B.Docno = C.VoucherRequestDocNo And C.CancelInd = 'N' And C.Status = 'A' ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("        Inner JOin TblSalesInvoiceHdr E On D.InvoiceDocNo = E.DocNo And E.CancelInd = 'N' And E.ProcessInd = 'F' ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        Group BY C.DocNo  ");
            SQL.AppendLine("    )B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AmtInvoice = B.AmtIP ");
            SQL.AppendLine("    Where B.DocDt Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "' ");
            //pos data
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select Concat(A.TrnNo, '-', A.BsDate) As TrNo, DATE_FORMAT(A.BsDate,'%d/%b/%Y')  ");
            SQL.AppendLine("    From TblPostrnhdr A ");
            SQL.AppendLine("    Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo      ");
            SQL.AppendLine("    Inner Join Tblsalesperson C On A.UserCode = C.UserCode    ");
            SQL.AppendLine("    Left Join   ");
            SQL.AppendLine("    (   ");
            SQL.AppendLine("        select DocDt, Group_concat(VoucherDocNo) VoucherDocNo    ");
            SQL.AppendLine("        From TblVoucherRequestHdr D    ");
            SQL.AppendLine("        Where  DocType = '10' And D.CancelInd='N'   ");
            SQL.AppendLine("        Group By DocDt   ");
            SQL.AppendLine("    )D On A.BsDate = D.DocDt  ");
            SQL.AppendLine("    Where A.BSDate Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "' ");
            SQL.AppendLine(")Z ");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SIDocNo", "VcDate"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LP1.Add(new L1()
                        {
                            SIDocNo = Sm.DrStr(dr, c[0]),
                            VcDocDt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }


        private void Process2(ref List<L1> LP1)
        {
            int Temp = 0;
           
            //LP2.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select Y.SODOcNo, Y.SIDocNo ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.SODOcNo, A.DocNo  As SIDocNo, B.DocDt ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Inner Join TblSoHdr B On A.SoDocNo = B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    And A.SODocno is not null ");
            //SQL.AppendLine("And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select distinct if(D.PLDocno is null, F.SODocNo, H.SODocNo) As SODocNo, A.DocNo, ");
            SQL.AppendLine("    if(D.PLDocno is null, I.DocDt, J.DocDt) As DocDt ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 C On B.DOctDocNo = C.DocNO And B.DOctDno = C.Dno ");
            SQL.AppendLine("    Inner Join TblDoCt2hdr D On C.DocNo = D.DocNo ");
            SQL.AppendLine("    Left Join TblDrhdr E On D.DRDocno = E.Docno ");
            SQL.AppendLine("    Left Join TblDRDtl F On E.Docno = F.DocNo ");
            SQL.AppendLine("    Left JOin TblPLHdr G On D.PlDocno = G.DocNo ");
            SQL.AppendLine("    Left Join TBlPlDtl H On G.DocNo = H.DocNo ");
            SQL.AppendLine("    Left Join TblSOHdr I On F.SODocNo = I.DocNo ");
            SQL.AppendLine("    Left Join TblSOHdr J On H.SODocNo = J.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    And A.SODocno is null ");
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select Concat(A.TrnNo, '-', A.BsDate) As TrnNo, Concat(A.TrnNo, '-', A.BsDate) As TrnNo, A.BsDate  ");
            SQL.AppendLine("    From TblPostrnhdr A ");
            SQL.AppendLine("    Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo      ");
            SQL.AppendLine("    Inner Join Tblsalesperson C On A.UserCode = C.UserCode    ");
            SQL.AppendLine("    Left Join   ");
            SQL.AppendLine("    (   ");
            SQL.AppendLine("        select DocDt, Group_concat(VoucherDocNo) VoucherDocNo    ");
            SQL.AppendLine("        From TblVoucherRequestHdr D    ");
            SQL.AppendLine("        Where  DocType = '10' And D.CancelInd='N'   ");
            SQL.AppendLine("        Group By DocDt   ");
            SQL.AppendLine("    )D On A.BsDate = D.DocDt  ");
            SQL.AppendLine(")Y ");
            SQL.AppendLine("Where Y.DocDt Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "' ");

           


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SODocNo",
                    //1
                    "SIDocNo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (string.Compare(LP1[i].SIDocNo, dr.GetString(1)) == 0)
                            {
                                LP1[i].SODocNo = dr.GetString(c[0]);
                                LP1[i].SIDocNo = dr.GetString(c[1]);
                                LP1[i].VcDocDt = LP1[i].VcDocDt;
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

      
        private void Process3(ref List<L1> LP1)
        {
            int Temp = 0;
            decimal TotalAmtSO = 0;
            decimal TotalAmtDO = 0;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;


            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSalesPerson), "X.SPCode", true);

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select X.CtName, ifnull(X.SpName, '-') As SpName, X.DocNo, DATE_FORMAT(X.DocDt,'%d/%b/%Y') As DocDt1, X.AmtSO, X.AmtDO,  X.SpCode, X.DocDt From ( ");
            SQL.AppendLine("        Select B.Ctname,  ifnull(E.Spname, C.Spname) As Spname, A.DocNo, A.DocDt,  A.Amt As AmtSO, ");
            SQL.AppendLine("        (ifnull(F.AmtDODR, 0)+ifnull(G.AmtDOPL, 0)) AmtDO, ifnull(D.SpCode, C.SpCode) As SpCode ");
            SQL.AppendLine("        from tblSOHdr A ");
            SQL.AppendLine("        Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("        Left Join TblSalesPerson C On A.SpCode = C.SpCode ");
            SQL.AppendLine("        Inner Join TblCtQtHdr D On A.CtQtDocNo = D.Docno ");
            SQL.AppendLine("        Left Join TblSalesPerson E On D.SpCode = E.SpCode ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select X1.SODocNo, SUM(X1.Amount) As AmtDODR ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select E.DocNo As SODocNO, H.ItCode, B.Qty QtyInventory, ");
            SQL.AppendLine("                if(I.PriceuomCode = J.InventoryUomCode, B.Qty, ");
            SQL.AppendLine("                If(I.PriceUomCode = J.SalesuomCode, (B.Qty*J.SalesuomCOdeConvert12), B.Qty / J.SalesuomCOdeConvert21)) ");
            SQL.AppendLine("                As QtySales, (G.Uprice + (G.UPrice*F.Taxrate*0.01)) As UPrice, ");
            SQL.AppendLine("                if(I.PriceuomCode = J.InventoryUomCode, B.Qty, ");
            SQL.AppendLine("                If(I.PriceUomCode = J.SalesuomCode, (B.Qty*J.SalesuomCOdeConvert12), B.Qty / J.SalesuomCOdeConvert21)) ");
            SQL.AppendLine("                * (G.Uprice + (G.UPrice*F.Taxrate*0.01)) As Amount ");
            SQL.AppendLine("                From TblDoCt2Hdr  A ");
            SQL.AppendLine("                Inner Join TblDoCt2Dtl2 B On A.docno = B.Docno And DrDocno Is not Null ");
            SQL.AppendLine("                Inner Join TblDrHdr C On A.DRDocno = C.DocNo ");
            SQL.AppendLine("                Inner Join TblDRDtl D On A.DRDocno = D.DocNo And B.DrDno = D.Dno ");
            SQL.AppendLine("                Inner Join TblSOHdr E On D.SODocNo=E.DocNo ");
            SQL.AppendLine("                Inner Join TblSODtl F On D.SODocNo=F.DocNo And D.SODNo=F.DNo ");
            SQL.AppendLine("                Inner Join TblCtQtDtl G On E.CtQtDocNo=G.DocNo And F.CtQtDNo=G.DNo ");
            SQL.AppendLine("                Inner Join TblItemPriceDtl H On G.ItemPriceDocNo=H.DocNo And G.ItemPriceDNo=H.DNo ");
            SQL.AppendLine("                Inner Join TblItemPricehdr I On G.itemPriceDocno = I.DocNo ");
            SQL.AppendLine("                Inner Join TblItem J On H.ItCode=J.ItCode ");
            SQL.AppendLine("            )X1 ");
            SQL.AppendLine("            Group By X1.SODocNo ");
            SQL.AppendLine("        )F On A.DocNo  = F.SODocNo ");
            SQL.AppendLine("        Left Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select X1.SODocNo, SUM(X1.Amount) As AmtDOPL ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select E.DocNo As SODocNO, B.Qty QtyInventory,  ");
            SQL.AppendLine("                if(I.PriceuomCode = J.InventoryUomCode, B.Qty,  ");
            SQL.AppendLine("                If(I.PriceUomCode = J.SalesuomCode, (B.Qty*J.SalesuomCOdeConvert12), B.Qty / J.SalesuomCOdeConvert21)) ");
            SQL.AppendLine("                As QtySales, (G.Uprice + (G.UPrice*F.Taxrate*0.01)) As UPrice, ");
            SQL.AppendLine("                if(I.PriceuomCode = J.InventoryUomCode, B.Qty,  ");
            SQL.AppendLine("                (If(I.PriceUomCode = J.SalesuomCode, (B.Qty*J.SalesuomCOdeConvert12), B.Qty / J.SalesuomCOdeConvert21)))* ");
            SQL.AppendLine("                (G.Uprice + (G.UPrice*F.Taxrate*0.01)) As Amount ");
            SQL.AppendLine("                From TblDocT2Hdr A ");
            SQL.AppendLine("                Inner Join TblDocT2Dtl3 B On A.DocNo = B.DocNo And A.PlDocNo Is Not Null ");
            SQL.AppendLine("                Inner Join TblPLhdr C On A.PlDocNo = C.DocNo  ");
            SQL.AppendLine("                Inner Join TblPlDtl D On A.PlDocNo = D.DocNo And B.PlDno = D.Dno And A.SectionNo = D.SectionNo ");
            SQL.AppendLine("                Inner Join TblSOhdr E On D.SoDocNo = E.DocNo ");
            SQL.AppendLine("                Inner Join TblSOdtl F On D.SoDocNo = F.DocNo And D.SODno = F.Dno ");
            SQL.AppendLine("                Inner Join TblCtQtDtl G On E.CtQtDocNo=G.DocNo And F.CtQtDNo=G.DNo ");
            SQL.AppendLine("                Inner Join TblItemPriceDtl H On G.ItemPriceDocNo=H.DocNo And G.ItemPriceDNo=H.DNo ");
            SQL.AppendLine("                Inner Join TblItemPricehdr I On G.itemPriceDocno = I.DocNo ");
            SQL.AppendLine("                Inner Join TblItem J On H.ItCode=J.ItCode ");
            SQL.AppendLine("            )X1 ");
            SQL.AppendLine("            Group By X1.SODocNo  ");
            SQL.AppendLine("        )G On A.DocNo = G.SODocNo ");
            SQL.AppendLine("    )X ");
            //nyambung ke POS
            SQL.AppendLine("    Union ALL ");
            SQL.AppendLine("    Select if(Z1.SlsType='S', ");
            SQL.AppendLine("    (Select CtName From TblCustomer Where CtCode = (Select Parvalue From TblParameter A Where parCode = 'StoreCtCode')), ");
            SQL.AppendLine("    (Select Concat(CtName, ' (Retur)')  From TblCustomer Where CtCode = (Select Parvalue From TblParameter A Where parCode = 'StoreCtCode'))) As CtName, ");
            SQL.AppendLine("    Z2.Spname, Concat(Z1.TrnNo, '-', Z1.BsDate) TrnNo,  DATE_FORMAT(Z1.BSDate,'%d/%b/%Y') As DocDt1, If(length(Z1.VoucherDocNo) = 0, 0, Z1.PayAmtNett) AmtSO,  ");
            SQL.AppendLine("    If(length(Z1.VoucherDocNo) = 0, 0, Z1.PayAmtNett) AmtDO, Z1.SpCode, Z1.BSDate ");
            SQL.AppendLine("    From (     ");
            SQL.AppendLine("          Select A.TrnNo, C.SpCode, A.BSDate, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth,   ");
            SQL.AppendLine("          Right(A.BSDate, 2) As Dt, if(A.SlsType='S', B.PayAmtNett, (-1*B.PayAmtNett)) PayAmtNett, D.VoucherDocNo, A.SlsType  ");
            SQL.AppendLine("          From TblPostrnhdr A     ");
            SQL.AppendLine("          Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo   ");  
            SQL.AppendLine("          Inner Join Tblsalesperson C On A.UserCode = C.UserCode   ");
            SQL.AppendLine("          Left Join   ");
            SQL.AppendLine("          (  ");
            SQL.AppendLine("               select DocDt, Group_concat(VoucherDocNo) VoucherDocNo    ");
            SQL.AppendLine("               From TblVoucherRequestHdr D   ");
            SQL.AppendLine("               Where  DocType = '10' And D.CancelInd='N'  ");
            SQL.AppendLine("               Group By DocDt  ");
            SQL.AppendLine("           )D On A.BsDate = D.DocDt   ");
            SQL.AppendLine("    )Z1 ");
            SQL.AppendLine("    Inner Join Tblsalesperson Z2 On Z1.SPCode = Z2.SpCode ");
            SQL.AppendLine("    Where Z1.BSDate Between '" + Sm.Left(Sm.GetDte(DteDocDt1), 8) + "' And '" + Sm.Left(Sm.GetDte(DteDocDt2), 8) + "' ");
            SQL.AppendLine(")X ");

            if (Filter.Length != 0)
                SQL.AppendLine(" " + Filter + " ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString()+" Order By X.DocDt1 ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Ctname",  

                    //1-5
                    "Spname", "DocNo", "DocDt1", "AmtSO", "AmtDO"
                });
                if (dr.HasRows)
                {
                    int r = 0;
                           
                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        { 
                            if (LP1[i].SODocNo == dr.GetString(2))
                            {
                                r = r + 1;
                                Grd1.Rows.Add();
                                int Row = Grd1.Rows.Count - 1;
                                Grd1.Cells[Row, 0].Value = r + 1;
                                Grd1.Cells[Row, 1].Value = dr.GetString(c[0]);
                                Grd1.Cells[Row, 2].Value = dr.GetString(c[1]);
                                Grd1.Cells[Row, 3].Value = dr.GetString(c[2]);
                                Grd1.Cells[Row, 4].Value = dr.GetString(c[3]);
                                Grd1.Cells[Row, 5].Value = dr.GetDecimal(c[4]);
                                Grd1.Cells[Row, 6].Value = dr.GetDecimal(c[5]);
                                Grd1.Cells[Row, 7].Value = dr.GetDecimal(c[4]);
                                TotalAmtSO = TotalAmtSO + dr.GetDecimal(c[4]);
                                TotalAmtDO = TotalAmtDO + dr.GetDecimal(c[5]);
                                Grd1.Cells[Row, 11].Value = LP1[i].VcDocDt;
                                break;
                            }
                        }
                    }
                    Grd1.GroupObject.Add(2);
                    Grd1.Group();
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7 });
                    for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                    {
                        if (TotalAmtSO > 0)
                        {
                            Grd1.Cells[intX, 8].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 5) / TotalAmtSO);
                            Grd1.Cells[intX, 10].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 5) / TotalAmtSO);
                        }
                        if (TotalAmtDO > 0)
                        {
                            Grd1.Cells[intX, 9].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 6) / TotalAmtDO);
                        }
                    }
                    Grd1.Rows.CollapseAll();
                }
                dr.Close();
            }
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkSalesPerson_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales Person");
        }

        private void LueSalesPerson_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSalesPerson, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }


        #endregion

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex == 8 || e.ColIndex == 9 || e.ColIndex == 10)
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null)
                    return;

                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    double myValue = (double)myObjValue;
                    int myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }

        }

       
       

        #endregion

        #region Class

        private class L1
        {
            public string SODocNo { get; set; }
            public string SIDocNo { get; set; }
            public string DocDt { get; set; }
            public string VcDocDt { get; set; }
        }

        #endregion
    }
}
