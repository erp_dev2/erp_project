﻿#region Update
/*
    18/02/2022 [DITA/PHT] New Apps
    04/03/2022 [DITA/PHT] Berdasarkan parameter ClosingBalanceInCashProfitCenterLevelToBeValidated, profit center yg bisa dipilih hanya dengan level tertentu saja.
    16/02/2023 [WED/PHT]  IncrementalSearch -> true di CcbProfitCenter
    13/03/2023 [MYA/BBT] Membuat penyesuaian di Closing Balance In Cash/Bank Account
    27/03/2023 [WED/PHT] tambah kolom Cancel
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCash2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmClosingBalanceInCash2 mFrmParent;
        private List<String> mlProfitCenter = null;
        private bool mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmClosingBalanceInCash2Find(FrmClosingBalanceInCash2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueMth(LueMth);
                mlProfitCenter = new List<String>();
                if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                else
                {
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Year",
                    "Month",
                    "Bank Account",
                       
                        
                    //6-10
                    "Bank Account Name",
                    "Amount",
                    "Entity",
                    "Description",
                    "Created"+Environment.NewLine+"By",   

                    //11-15
                    "Created"+Environment.NewLine+"Date",  
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",

                    //16-18
                    "Profit Center Code",
                    "Profit Center",
                    "Cancel"
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, false);
            Grd1.Cols[18].Move(3);
            Grd1.Cols[17].Move(5);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterInvalid()) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                SetProfitCenter();

                string Filter = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.CancelInd, ");
                SQL.AppendLine("Case When A.Mth = '01' Then 'January' ");
                SQL.AppendLine("When A.Mth = '02' Then 'Febuary' ");
                SQL.AppendLine("When A.Mth = '03' Then 'March' ");
                SQL.AppendLine("When A.Mth = '04' Then 'April' ");
                SQL.AppendLine("When A.Mth = '05' Then 'May' ");
                SQL.AppendLine("When A.Mth = '06' Then 'June' ");
                SQL.AppendLine("When A.Mth = '07' Then 'July' ");
                SQL.AppendLine("When A.Mth = '08' Then 'August' ");
                SQL.AppendLine("When A.Mth = '09' Then 'September' ");
                SQL.AppendLine("When A.Mth = '10' Then 'October' ");
                SQL.AppendLine("When A.Mth = '11' Then 'November' ");
                SQL.AppendLine("When A.Mth = '12' Then 'December' ");
                SQL.AppendLine("End As Mth, ");
                SQL.AppendLine("B.BankAcCode, Concat(C.BankAcNm, C.bankAcNo) As BankAcNm, B.Amt, ");
                SQL.AppendLine("E.EntName, ");
                SQL.AppendLine("Concat( ");
                SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(D.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End ");
                SQL.AppendLine(") As BankAcDesc, F.ProfitCenterCode, F.ProfitCenterName, ");
                SQL.AppendLine("B.CreateBy, B.CreateDt, B.LastUpBy, B.LastUpDt ");
                SQL.AppendLine("From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("Inner Join TblClosingbalanceInCashDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode And C.HiddenInd='N' ");
                SQL.AppendLine("Left Join TblBank D On C.BankCode=D.BankCode ");
                SQL.AppendLine("Left Join TblEntity E On C.EntCode=E.EntCode ");
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    SQL.AppendLine("Inner Join TblProfitCenter F On A.ProfitCenterCode=F.ProfitCenterCode ");
                    SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_2.Length > 0) Filter_2 += " Or ";
                            Filter_2 += " (A.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_2.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter_2 + ") ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode2) ");
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                        }
                        else
                        {
                            SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                            SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("    ) ");
                        }
                    }
                }
                else
                {
                    SQL.AppendLine("Left Join TblProfitCenter F On A.ProfitCenterCode=F.ProfitCenterCode ");
                }


                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "B.BankAcCode", "BankAcNm" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,SQL.ToString() + Filter + " Order By A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "Yr", "Mth", "BankAcCode", "BankAcNm",    
                            
                            //6-10
                            "Amt", "EntName", "BankAcDesc", "CreateBy", "CreateDt", 
                            
                            //11-15
                            "LastUpBy", "LastUpDt", "ProfitCenterCode", "ProfitCenterName", "CancelInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 18, 15);                        }, true, false, false, true
                    );
                Grd1.GroupObject.Add(1);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
        }


        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            if (mFrmParent.mClosingBalanceInCashProfitCenterLevelToBeValidated > 0m)
                SQL.AppendLine("    And Level=@ClosingBalanceInCashProfitCenterLevelToBeValidated ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<decimal>(ref cm, "@ClosingBalanceInCashProfitCenterLevelToBeValidated", mFrmParent.mClosingBalanceInCashProfitCenterLevelToBeValidated);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }
        private bool IsProfitCenterInvalid()
        {
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter && Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion
    }
}
