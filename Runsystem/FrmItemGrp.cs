﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemGrp : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmItemGrpFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Item Group";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrpCode, ChkActInd, TxtGrpName, MeeRemark
                    }, true);
                    TxtGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtGrpCode, TxtGrpName, MeeRemark
                    }, false);
                    TxtGrpCode.Focus();
                    ChkActInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtGrpName, MeeRemark
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtGrpCode, TxtGrpName, MeeRemark
            });
            ChkActInd.Properties.ReadOnly = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmItemGrpFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemGroup(ItGrpCode, ItGrpName, ActInd, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItGrpCode, @ItGrpName, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update ActInd=@ActInd, ItGrpName=@ItGrpName, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItGrpCode", TxtGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@ItGrpName", TxtGrpName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #region Show Data

        public void ShowData(string GrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItGrpCode", GrpCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblItemGroup Where ItGrpCode = @ItGrpCode",
                        new string[] 
                        {
                            "ItGrpCode", "ItGrpName", "ActInd", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGrpCode, "Item group code", false) ||
                Sm.IsTxtEmpty(TxtGrpName, "Item group name", false) ||
                IsEntCodeExisted() ||
                IsDataInActiveAlready();
        }

        private bool IsEntCodeExisted()
        {
            if (!TxtGrpCode.Properties.ReadOnly && Sm.IsDataExist("Select ItGrpCode From TblItemGroup Where ItGrpCode ='" + TxtGrpCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Group code ( " + TxtGrpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }
        private bool IsDataInActiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select ItGrpCode From TblItemGroup " +
                    "Where ActInd='N' And ItGrpCode=@ItGrpCode;"
            };
            Sm.CmParam<String>(ref cm, "@ItGrpCode", TxtGrpCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion
    }
}
