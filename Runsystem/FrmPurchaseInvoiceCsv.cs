﻿#region Update
/*
    14/03/2018 [WED] Tambah informasi Nomor NPWP dari master Vendor
    14/03/2018 [WED] Di CSV nya untuk No NPWP melihat dulu ke master Vendor. Kalau kosong, maka isi nya 000000000000000. Brutto dan PPH decimal point nya dihilangkan.
    15/03/2018 [WED] query OP --> Voucher di bikin sub query
    05/04/2018 [TKG] non IDR menggunakan tax rate pajak
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceCsv : RunSystem.FrmBase5
    {
        #region Field

        private string mMenuCode = string.Empty, mSQL = string.Empty, mMainCurCode = string.Empty;
        
        #endregion

        #region Constructor

        public FrmPurchaseInvoiceCsv(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetPeriod(ref DteVoucherDt1, ref DteVoucherDt2, ref ChkVoucherDt, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, DATE_FORMAT(A.DocDt, '%d/%b/%Y') As DocDt, A.VdCode, replace(B.TIN, ';', '') As TIN, replace(B.VdName, ';', '') As VdName, replace(B.Address, ';', '') As VdAddress, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, C.TaxName As Tax1, D.TaxName As Tax2, E.TaxName As Tax3, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo, ';', '') As TaxInvoiceNo, replace(A.TaxInvoiceNo2, ';', '') As TaxInvoiceNo2, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo3, ';', '') As TaxInvoiceNo3, IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt2, '%d/%m/%Y'), '') As TaxInvoiceDt2, IfNull(DATE_FORMAT(A.TaxInvoiceDt3, '%d/%m/%Y'), '') As TaxInvoiceDt3, ");
            SQL.AppendLine("A.CurCode, A.COATaxInd, Case When A.TaxRateAmt=0.00 Then 1.00 Else A.TaxRateAmt End As TaxRateAmt, F.Voucher ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode1 = C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2 = D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3 = E.TaxCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T4.DocNo, ' (', Date_Format(T4.DocDt, '%d/%m/%Y'), ')') Order By T4.DocDt, T4.DocNo Separator ', ') As Voucher ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo = T2.InvoiceDocNo And T2.InvoiceType = '1' ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo = T3.DocNo And T3.Status = 'A' And T3.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo = T4.VoucherRequestDocNo And T4.CancelInd = 'N' ");
            if (ChkVoucherDt.Checked)
                SQL.AppendLine("        And T4.DocDt Between @VoucherDt1 And @VoucherDt2 ");
            if (ChkDocDt.Checked)
                SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.ProcessInd = 'F' ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And (T1.TaxCode1 = '002' Or T1.TaxCode2 = '002' Or T1.TaxCode3 = '002') ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") F On A.DocNo = F.DocNo ");
            SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "",
                    "Date",
                    "Vendor",
                    
                    //6-10
                    "Vendor Address",
                    "Tax Invoice#",
                    "Tax Invoice"+Environment.NewLine+"Date",
                    "PPH Amount",
                    "Total Without Tax",

                    //11-12
                    "Taxpayer" +Environment.NewLine+"Identification#",
                    "Voucher"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 120, 20, 80, 180, 
                    
                    //6-10
                    280, 120, 90, 120, 120,

                    //11-12
                    180, 400
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Grd1.Cols[11].Move(7);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        #region Standard Method

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<Hdr>();
                AddDataHdr(ref l);
                if (l.Count > 0)
                {
                    ShowDataHdr(ref l);
                    ComputeTotalWithoutTax(ref l);
                    ProcessPPHAmt(ref l);
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10 });
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            if (
                Sm.StdMsgYN("Question", "Do you want to generate csv file ?") == DialogResult.No ||
                IsDataNotValid()
                )
                return;

            var lCP = new List<CSVPPH23>();
            try
            {
                var FileName = "PPH23";
                var sf = new SaveFileDialog();
                sf.Filter = "CSV files (*.csv)|*.csv";
                sf.FileName = FileName;

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length>0)
                        {
                            lCP.Add(new CSVPPH23()
                            {
                                MasaPajak = (Sm.GetGrdStr(Grd1, r, 8).Length > 8) ? Sm.GetGrdStr(Grd1, r, 8).Substring(3, 2) : "",
                                TahunPajak = (Sm.GetGrdStr(Grd1, r, 8).Length > 8) ? Sm.Right(Sm.GetGrdStr(Grd1, r, 8), 4) : "",
                                VdName = Sm.GetGrdStr(Grd1, r, 5),
                                VdAddress = Sm.GetGrdStr(Grd1, r, 6),
                                TaxInvoiceNo = Sm.GetGrdStr(Grd1, r, 7).Replace(",", "."),
                                TaxInvoiceDt = Sm.GetGrdStr(Grd1, r, 8),
                                PPHAmt = Math.Truncate(Sm.GetGrdDec(Grd1, r, 9)),
                                Brutto = Math.Truncate(Sm.GetGrdDec(Grd1, r, 10)),
                                TIN = (Sm.GetGrdStr(Grd1, r, 11).Length > 0) ? 
                                        Sm.GetGrdStr(Grd1, r, 11).Replace(",", string.Empty).Replace("-", string.Empty).Replace(".", string.Empty) : 
                                        "000000000000000"
                            });
                        }
                    }
                    if (lCP.Count > 0)
                    {
                        CreateCSVFile(ref lCP, sf.FileName);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            lCP.Clear();
        }

        #endregion

        #region Additional Method

        private void AddDataHdr(ref List<Hdr> l)
        {
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = GetSQL() + " Order By A.DocDt, A.DocNo; ";

                if (ChkDocDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }

                if (ChkVoucherDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@VoucherDt1", Sm.GetDte(DteVoucherDt1));
                    Sm.CmParamDt(ref cm, "@VoucherDt2", Sm.GetDte(DteVoucherDt2));
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "VdCode", "VdName", "VdAddress", "TaxCode1", 
                    
                    //6-10
                    "TaxCode2", "TaxCode3", "Tax1", "Tax2", "Tax3", 

                    //11-15
                    "TaxInvoiceNo", "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", 
                    
                    //16-20
                    "TaxInvoiceDt3", "COATaxInd", "TIN", "TaxRateAmt", "Voucher"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Hdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            VdCode = Sm.DrStr(dr, c[2]),
                            VdName = Sm.DrStr(dr, c[3]).Replace("\r\n", " ").Replace(",", " "),
                            VdAddress = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),
                            TaxCode1 = Sm.DrStr(dr, c[5]),

                            TaxCode2 = Sm.DrStr(dr, c[6]),
                            TaxCode3 = Sm.DrStr(dr, c[7]),
                            Tax1 = Sm.DrStr(dr, c[8]),
                            Tax2 = Sm.DrStr(dr, c[9]),
                            Tax3 = Sm.DrStr(dr, c[10]),

                            TaxInvoiceNo = Sm.DrStr(dr, c[11]),
                            TaxInvoiceNo2 = Sm.DrStr(dr, c[12]),
                            TaxInvoiceNo3 = Sm.DrStr(dr, c[13]),
                            TaxInvoiceDt = Sm.DrStr(dr, c[14]),
                            TaxInvoiceDt2 = Sm.DrStr(dr, c[15]),

                            TaxInvoiceDt3 = Sm.DrStr(dr, c[16]),
                            COATaxInd = Sm.DrStr(dr, c[17]),
                            TIN = Sm.DrStr(dr, c[18]).Replace("\r\n", " "),
                            TaxRateAmt = Sm.DrDec(dr, c[19]),
                            Voucher = Sm.DrStr(dr, c[20])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDataHdr(ref List<Hdr> l)
        {
            string mTaxInvoiceNo = string.Empty, mTaxInvoiceDt = string.Empty;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;

            iGRow r;

            int nos = 1;
            for (int x = 0; x < l.Count; x++)
            {
                if(l[x].TaxCode1 == "002")
                {
                    mTaxInvoiceNo = l[x].TaxInvoiceNo;
                    mTaxInvoiceDt = l[x].TaxInvoiceDt;
                }

                if (l[x].TaxCode2 == "002")
                {
                    mTaxInvoiceNo = l[x].TaxInvoiceNo2;
                    mTaxInvoiceDt = l[x].TaxInvoiceDt2;
                }

                if (l[x].TaxCode3 == "002")
                {
                    mTaxInvoiceNo = l[x].TaxInvoiceNo3;
                    mTaxInvoiceDt = l[x].TaxInvoiceDt3;
                }

                r = Grd1.Rows.Add();
                r.Cells[0].Value = nos++;
                r.Cells[2].Value = l[x].DocNo;
                r.Cells[4].Value = l[x].DocDt;
                r.Cells[5].Value = l[x].VdName;
                r.Cells[6].Value = l[x].VdAddress;
                r.Cells[7].Value = mTaxInvoiceNo;
                r.Cells[8].Value = mTaxInvoiceDt;
                r.Cells[9].Value = 0m;
                r.Cells[10].Value = 0m;
                r.Cells[11].Value = l[x].TIN;
                r.Cells[12].Value = l[x].Voucher;
            }

            Grd1.EndUpdate();
        }

        private void ComputeTotalWithoutTax(ref List<Hdr> l)
        {
            var ld = new List<Dtl>();
            var ld2 = new List<Dtl2>();
            var lc = new List<AmtCOA>();

            ProcessDtl(ref l, ref ld);
            ProcessDtl2(ref l, ref ld2);
            ProcessAmtCOA(ref l, ref lc);

            for (int x = 0; x < l.Count; x++)
            {
                decimal mTotalWithoutTax = 0;

                if(ld.Count > 0)
                {
                    for (int y = 0; y < ld.Count; y++)
                    {
                        if (l[x].DocNo == ld[y].DocNo)
                        {
                            mTotalWithoutTax += ld[y].Total;
                            break;
                        }
                    }
                }

                if(ld2.Count > 0)
                {
                    for (int z = 0; z < ld2.Count; z++)
                    {
                        if (l[x].DocNo == ld2[z].DocNo)
                        {
                            mTotalWithoutTax += ld2[z].Amt;
                        }
                    }
                }

                if(lc.Count > 0)
                {
                    for (int a = 0; a < lc.Count; a++)
                    {
                        if(l[x].DocNo == lc[a].DocNo)
                        {
                            string mVendorAcNoDownpayment = Sm.GetValue("Select Concat(ParValue, '" + l[x].VdCode + "') As AcNo From TblParameter Where ParCode = 'VendorAcNoAP'; ");
                            if(lc[a].AcNo == mVendorAcNoDownpayment)
                            {
                                string mAcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = '" + lc[a].AcNo + "'");
                                if(lc[a].DAmt != 0)
                                {
                                    string AcType = "D";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].DAmt;
                                    else mTotalWithoutTax -= lc[a].DAmt;
                                }

                                if (lc[a].CAmt != 0)
                                {
                                    string AcType = "C";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].CAmt;
                                    else mTotalWithoutTax -= lc[a].CAmt;
                                }
                            }
                        }
                    }
                }
                if (Sm.CompareStr(l[x].CurCode, mMainCurCode))
                    Grd1.Cells[x, 10].Value = Math.Truncate(mTotalWithoutTax);
                else
                    Grd1.Cells[x, 10].Value = Math.Truncate(mTotalWithoutTax * l[x].TaxRateAmt);
            }

            ld.Clear();
            ld2.Clear();
            lc.Clear();
        }

        private void ProcessDtl(ref List<Hdr> l, ref List<Dtl> ld)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mDocNo.Length > 0) mDocNo += ",";
                mDocNo += l[i].DocNo;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0.00)=0.00 Then 1.00 Else (100.00-D.Discount)/100.00 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue) ");
            SQL.AppendLine(") As Total ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdhdr B2 On A.RecvVdDocNo=B2.DocNo ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("Where Find_In_Set(A.DocNo, '" + mDocNo + "') ");
            SQL.AppendLine("Group By A.DocNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Total" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new Dtl()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Total = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDtl2(ref List<Hdr> l, ref List<Dtl2> ld2)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mDocNo.Length > 0) mDocNo += ",";
                mDocNo += l[i].DocNo;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.AmtType, Sum(A.Amt) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblOption B On A.AmtType=B.OptCode And B.OptCat='InvoiceAmtType' ");
            SQL.AppendLine("Where Find_In_Set(A.DocNo, '" + mDocNo + "') ");
            SQL.AppendLine("Group By A.DocNo, A.AmtType; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AmtType", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld2.Add(new Dtl2()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            AmtType = Sm.DrStr(dr, c[1]),
                            Amt = (Sm.DrStr(dr, c[1]) == "2") ? Sm.DrDec(dr, c[2]) : (Sm.DrDec(dr, c[2]) * -1)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAmtCOA(ref List<Hdr> l, ref List<AmtCOA> lc)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].COATaxInd == "N")
                {
                    if (mDocNo.Length > 0) mDocNo += ",";
                    mDocNo += l[i].DocNo;
                }
            }

            if (mDocNo.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo, AcNo, DAMt, CAmt ");
                SQL.AppendLine("From TblPurchaseInvoiceDtl4 ");
                SQL.AppendLine("Where Find_In_Set(DocNo, '" + mDocNo + "'); ");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "DAmt", "CAMt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lc.Add(new AmtCOA()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                AcNo = Sm.DrStr(dr, c[1]),
                                DAmt = Sm.DrDec(dr, c[2]),
                                CAmt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void ProcessPPHAmt(ref List<Hdr> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                decimal mTaxAmt = 0m, mTotalWithoutTax = 0m;
                if (Sm.GetGrdStr(Grd1, i, 10).Length != 0) mTotalWithoutTax = Sm.GetGrdDec(Grd1, i, 10);

                if (l[i].TaxCode1 == "002") mTaxAmt = GetTaxRate(l[i].TaxCode1) / 100 * mTotalWithoutTax;
                if (l[i].TaxCode2 == "002") mTaxAmt = GetTaxRate(l[i].TaxCode2) / 100 * mTotalWithoutTax;
                if (l[i].TaxCode3 == "002") mTaxAmt = GetTaxRate(l[i].TaxCode3) / 100 * mTotalWithoutTax;

                Grd1.Cells[i, 9].Value = Math.Truncate((mTaxAmt < 0) ? (mTaxAmt * -1) : mTaxAmt);
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsChosen = false;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (!IsChosen && Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0) IsChosen = true;
            }
            if (!IsChosen)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private void CreateCSVFile(ref List<CSVPPH23> lCP, string FileName)
        {
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));

            sw.Write(
                /*1*/  "Kode Form Bukti Potong;Masa Pajak;Tahun Pajak;Pembetulan;NPWP WP yang Dipotong;Nama WP yang Dipotong;Alamat WP yang Dipotong;" +
                /*2*/  "Nomor Bukti Potong;Tanggal Bukti Potong;Nilai Bruto 1;Tarif 1;PPh Yang Dipotong  1;Nilai Bruto 2;" +
                /*3*/  "Tarif 2;PPh Yang Dipotong  2;Nilai Bruto 3;Tarif 3;PPh Yang Dipotong  3;Nilai Bruto 4;Tarif 4;" +
                /*4*/  "PPh Yang Dipotong  4;Nilai Bruto 5;Tarif 5;PPh Yang Dipotong  5;Nilai Bruto 6a/Nilai Bruto 6;" +
                /*5*/  "Tarif 6a/Tarif 6;PPh Yang Dipotong  6a/PPh Yang Dipotong  6;Nilai Bruto 6b/Nilai Bruto 7;" +
                /*6*/  "Tarif 6b/Tarif 7;PPh Yang Dipotong  6b/PPh Yang Dipotong  7;Nilai Bruto 6c/Nilai Bruto 8;" +
                /*7*/  "Tarif 6c/Tarif 8;PPh Yang Dipotong  6c/PPh Yang Dipotong  8;Nilai Bruto 9;Tarif 9;" +
                /*8*/  "PPh Yang Dipotong  9;Nilai Bruto 10;Perkiraan Penghasilan Netto10;Tarif 10;PPh Yang Dipotong  10;" +
                /*9*/  "Nilai Bruto 11;Perkiraan Penghasilan Netto11;Tarif 11;PPh Yang Dipotong  11;Nilai Bruto 12;" +
                /*10*/ "Perkiraan Penghasilan Netto12;Tarif 12;PPh Yang Dipotong  12;Nilai Bruto 13;Tarif 13;" +
                /*11*/ "PPh Yang Dipotong  13;Kode Jasa 6d1 PMK-244/PMK.03/2008;" +
                /*12*/ "Nilai Bruto 6d1;Tarif 6d1;PPh Yang Dipotong  6d1;" +
                /*13*/ "Kode Jasa 6d2 PMK-244/PMK.03/2008;Nilai Bruto 6d2;Tarif 6d2;PPh Yang Dipotong  6d2;" +
                /*14*/ "Kode Jasa 6d3 PMK-244/PMK.03/2008;Nilai Bruto 6d3;Tarif 6d3;PPh Yang Dipotong  6d3;" +
                /*15*/ "Kode Jasa 6d4 PMK-244/PMK.03/2008;Nilai Bruto 6d4;Tarif 6d4;PPh Yang Dipotong  6d4;" +
                /*16*/ "Kode Jasa 6d5 PMK-244/PMK.03/2008;Nilai Bruto 6d5;Tarif 6d5;PPh Yang Dipotong  6d5;" +
                /*17*/ "Kode Jasa 6d6 PMK-244/PMK.03/2008;Nilai Bruto 6d6;Tarif 6d6;PPh Yang Dipotong  6d6;" +
                /*18*/ "Jumlah Nilai Bruto ;Jumlah PPh Yang Dipotong"
                    );

            foreach (var x in lCP)
            {
                sw.Write(sw.NewLine);
                sw.Write(
                    /*1*/  "F113306" + ";" + x.MasaPajak + ";" + x.TahunPajak + ";" + "0;" + x.TIN + ";" + x.VdName + ";" + x.VdAddress + ";" +
                    /*2*/  x.TaxInvoiceNo + ";" + x.TaxInvoiceDt + ";0;15;0;0;" +
                    /*3*/  "15;0;0;15;0;0;15;" +
                    /*4*/  "0;0;2;0;0;" +
                    /*5*/  "2;0;0;" +
                    /*6*/  "2;0;0;" +
                    /*7*/  "2;0;;;" +
                    /*8*/  ";;;;;" +
                    /*9*/  ";;;;;" +
                    /*10*/ ";;;;;" +
                    /*11*/ ";13;" +
                    /*12*/ x.Brutto + ";2;" + x.PPHAmt + ";" +
                    /*13*/ "0;0;2;0;" +
                    /*14*/ "0;0;2;0;" +
                    /*15*/ "0;0;2;0;" +
                    /*16*/ "0;0;2;0;" +
                    /*17*/ "0;0;2;0;" +
                    /*18*/ x.Brutto + ";" + x.PPHAmt
                );
            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
                e.DoDefault = false;

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Invoice date");
        }

        private void DteVoucherDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteVoucherDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkVoucherDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Voucher date");
        }

        #endregion

        #endregion

        #region Class

        private class Dtl
        {
            public string DocNo { get; set; }
            public decimal Total { get; set; }
        }

        private class Dtl2
        {
            public string DocNo { get; set; }
            public string AmtType { get; set; }
            public decimal Amt { get; set; }
        }

        private class AmtCOA
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Hdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string VdAddress { get; set; }
            public string TaxCode1 { get; set; }
            public string Tax1 { get; set; }
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string TaxCode2 { get; set; }
            public string Tax2 { get; set; }
            public string TaxInvoiceNo2 { get; set; }
            public string TaxInvoiceDt2 { get; set; }
            public string TaxCode3 { get; set; }
            public string Tax3 { get; set; }
            public string TaxInvoiceNo3 { get; set; }
            public string TaxInvoiceDt3 { get; set; }
            public string COATaxInd { get; set; }
            public string TIN { get; set; }
            public string CurCode { get; set; }
            public decimal TaxRateAmt { get; set; }
            public string Voucher { get; set; }
        }

        private class CSVPPH23
        {
            public string MasaPajak { get; set; }
            public string TahunPajak { get; set; }
            public string VdName { get; set; }
            public string VdCode { get; set; }
            public string VdAddress { get; set; }
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public decimal PPHAmt { get; set; }
            public decimal Brutto { get; set; }
            public string TIN { get; set; }
        }

        #endregion
    }
}
