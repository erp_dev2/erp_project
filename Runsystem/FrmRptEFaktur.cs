﻿#region Update
/*
    13/08/2018 [TKG] tambah informasi dari ap downpayment
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEFaktur : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEFaktur(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueVdCode(ref LueVdCode);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-14);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    select X.*, ");
            SQL.AppendLine("    if(X.statusFak = 'Faktur Pajak Normal-Pengganti',  Concat('011', X.noFak) , Concat('010', X.noFak)) As NoFak2,  Left(X.TmScan, 10) As Dt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select '1' As DocType, A.DocNo, A.VdCode, A.DocDt, 'FM' As FM, ExtractValue(B.RemarkXml, '//kdJenisTransaksi') AS TrxCode, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//fgPengganti') As Fg, ExtractValue(B.RemarkXml, '//nomorFaktur') As NoFak, ");
            SQL.AppendLine("        SubString(ExtractValue(B.RemarkXml, '//tanggalFaktur'), 4, 2) As MthFak, Right(ExtractValue(B.RemarkXml, '//tanggalFaktur'), 4) As YrFak, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//tanggalFaktur') As DtFak, ExtractValue(B.RemarkXml, '//npwpPenjual') As Vdnpwp, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//namaPenjual') As VdName, ExtractValue(B.RemarkXml, '//alamatPenjual') As VdAddress, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//jumlahDpp') As Dpp, ExtractValue(B.RemarkXml, '//jumlahPpn') As ppn, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//jumlahPpnBm') As ppnbm, '1' IsCreditable, ExtractValue(B.RemarkXml, '//npwpLawanTransaksi') As Selfnpwp, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//namaLawanTransaksi') As Selfname,  ");
            SQL.AppendLine("        Concat(Date_format(Left(B.CreateDt, 8), '%Y-%m-%d'),' ', time_format(right(Concat(B.CreateDt, '01'), 6), '%H:%i:%s')) As TmScan, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//referensi') As Reff, A.VDInvNo, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//statusFaktur') As StatusFak, 'ANWID' As InvFor ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl3 B ");
            SQL.AppendLine("            On A.DocNo = B.DocNo  ");
            SQL.AppendLine("            And B.RemarkXml Is Not Null ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select '2' As DocType, A.DocNo, C.VdCode, A.DocDt, 'FM' As FM, ExtractValue(B.RemarkXml, '//kdJenisTransaksi') AS TrxCode, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//fgPengganti') As Fg, ExtractValue(B.RemarkXml, '//nomorFaktur') As NoFak, ");
            SQL.AppendLine("        SubString(ExtractValue(B.RemarkXml, '//tanggalFaktur'), 4, 2) As MthFak, Right(ExtractValue(B.RemarkXml, '//tanggalFaktur'), 4) As YrFak, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//tanggalFaktur') As DtFak, ExtractValue(B.RemarkXml, '//npwpPenjual') As Vdnpwp, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//namaPenjual') As VdName, ExtractValue(B.RemarkXml, '//alamatPenjual') As VdAddress, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//jumlahDpp') As Dpp, ExtractValue(B.RemarkXml, '//jumlahPpn') As ppn, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//jumlahPpnBm') As ppnbm, '1' IsCreditable, ExtractValue(B.RemarkXml, '//npwpLawanTransaksi') As Selfnpwp, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//namaLawanTransaksi') As Selfname,  ");
            SQL.AppendLine("        Concat(Date_format(Left(B.CreateDt, 8), '%Y-%m-%d'),' ', time_format(right(Concat(B.CreateDt, '01'), 6), '%H:%i:%s')) As TmScan, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//referensi') As Reff, Null As VDInvNo, ");
            SQL.AppendLine("        ExtractValue(B.RemarkXml, '//statusFaktur') As StatusFak, 'ANWID' As InvFor ");
            SQL.AppendLine("        From TblAPDownpayment A ");
            SQL.AppendLine("        Inner Join TblAPDownpaymentDtl2 B ");
            SQL.AppendLine("            On A.DocNo = B.DocNo  ");
            SQL.AppendLine("            And B.RemarkXml Is Not Null ");
            SQL.AppendLine("        Left Join TblPOHdr C On A.PODocNo=C.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(" )X " + Filter);
            SQL.AppendLine(" Order By DocType; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "FM",
                        "Transaction"+Environment.NewLine+"Type",
                        "Replacement"+Environment.NewLine+"Invoice Tax",
                        "Invoice Tax Number",
                        "Tax Period",                        
                        //6-10
                        "Tax Year",
                        "Invoice Tax"+Environment.NewLine+"Date",
                        "Vendor NPWP", 
                        "Vendor",
                        "Vendor Address",
                        //11-15
                        "DPP",
                        "PPN",
                        "PPNBM",
                        "Is Creditable",
                        "NPWP",
                        //16-20
                        "Name",
                        "Scanning Date"+Environment.NewLine+"And Time",
                        "Reference",
                        "Remark",
                        "Status",
                        //21-24
                        "Recorded By",
                        "Invoice Tax Number",
                        "Invoice Tax Date",
                        "Document Number"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        80, 100, 120, 180, 100,
                        //6-10
                        100, 100, 150, 200, 300, 
                        //11-15
                        120, 120, 120, 100, 150, 
                        //16-20
                        200, 120, 200, 120, 120, 
                        //21-24
                        120, 180, 120, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 24 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsFilterByDateInvalid()
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 1=1 ";
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtTaxInvDocNo.Text, new string[] { "X.Nofak", "X.Nofak2" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                        { 
                            //0
                            "FM",  
                            //1-5
                            "TrxCode", "Fg", "NoFak", "MthFak", "YrFak", 
                            //6-10
                            "DTFak", "VdNPWP", "VdName", "VdAddress", "Dpp", 
                            //11-15
                            "PPN", "PPNBM", "IsCreditable", "selfnpwp", "Selfname", 
                            //16-20
                            "TMScan", "Reff", "VDInvNo", "StatusFak", "InvFor",  
                            //21-23
                            "Nofak2", "Dt", "DocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                        }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 13 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 2].Value = "'" + Sm.GetGrdStr(Grd1, Row, 2);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 8].Value = "'" + Sm.GetGrdStr(Grd1, Row, 8);
                Grd1.Cells[Row, 15].Value = "'" + Sm.GetGrdStr(Grd1, Row, 15);
                Grd1.Cells[Row, 19].Value = "'" + Sm.GetGrdStr(Grd1, Row, 19);
                Grd1.Cells[Row, 22].Value = "'" + Sm.GetGrdStr(Grd1, Row, 22);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 2].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 2).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 8].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 8), Sm.GetGrdStr(Grd1, Row, 8).Length - 1);
                Grd1.Cells[Row, 15].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 15).Length - 1);
                Grd1.Cells[Row, 19].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 19).Length - 1);
                Grd1.Cells[Row, 22].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 22), Sm.GetGrdStr(Grd1, Row, 22).Length - 1);
            }
            Grd1.EndUpdate();
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event 
        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkTaxInvDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Tax Invoice Number");
        }

        private void TxtTaxInvDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion        
    }
}
