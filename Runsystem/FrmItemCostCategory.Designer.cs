﻿namespace RunSystem
{
    partial class FrmItemCostCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LueCCt = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueCC = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkItCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkAutoChoose = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueCCt2 = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCCt2 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoChoose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCt2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 477);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkCCt2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueCCt2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.ChkItCode);
            this.panel2.Controls.Add(this.ChkItCtCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.LueItCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueCCt);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueCC);
            this.panel2.Size = new System.Drawing.Size(788, 120);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 120);
            this.panel3.Size = new System.Drawing.Size(788, 379);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(788, 379);
            this.Grd1.TabIndex = 24;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Location = new System.Drawing.Point(0, 457);
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(788, 0);
            this.panel1.Size = new System.Drawing.Size(70, 499);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(15, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 14);
            this.label1.TabIndex = 21;
            this.label1.Text = "Cost Category";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCt
            // 
            this.LueCCt.EnterMoveNextControl = true;
            this.LueCCt.Location = new System.Drawing.Point(103, 92);
            this.LueCCt.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCt.Name = "LueCCt";
            this.LueCCt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt.Properties.Appearance.Options.UseFont = true;
            this.LueCCt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCt.Properties.DropDownRows = 30;
            this.LueCCt.Properties.MaxLength = 40;
            this.LueCCt.Properties.NullText = "[Empty]";
            this.LueCCt.Properties.PopupWidth = 400;
            this.LueCCt.Size = new System.Drawing.Size(395, 20);
            this.LueCCt.TabIndex = 22;
            this.LueCCt.ToolTip = "F4 : Show/hide list";
            this.LueCCt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCt.EditValueChanged += new System.EventHandler(this.LueCCt_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(27, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 14);
            this.label8.TabIndex = 10;
            this.label8.Text = "Cost Center";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCC
            // 
            this.LueCC.EnterMoveNextControl = true;
            this.LueCC.Location = new System.Drawing.Point(103, 4);
            this.LueCC.Margin = new System.Windows.Forms.Padding(5);
            this.LueCC.Name = "LueCC";
            this.LueCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.Appearance.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCC.Properties.DropDownRows = 30;
            this.LueCC.Properties.MaxLength = 40;
            this.LueCC.Properties.NullText = "[Empty]";
            this.LueCC.Properties.PopupWidth = 400;
            this.LueCC.Size = new System.Drawing.Size(395, 20);
            this.LueCC.TabIndex = 11;
            this.LueCC.ToolTip = "F4 : Show/hide list";
            this.LueCC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCC.EditValueChanged += new System.EventHandler(this.LueCC_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(66, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Item";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(103, 48);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(395, 20);
            this.TxtItCode.TabIndex = 16;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(501, 46);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCode.TabIndex = 17;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // ChkItCtCode
            // 
            this.ChkItCtCode.Location = new System.Drawing.Point(501, 24);
            this.ChkItCtCode.Name = "ChkItCtCode";
            this.ChkItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCtCode.Properties.Caption = " ";
            this.ChkItCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCtCode.TabIndex = 14;
            this.ChkItCtCode.ToolTip = "Remove filter";
            this.ChkItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCtCode.ToolTipTitle = "Run System";
            this.ChkItCtCode.CheckedChanged += new System.EventHandler(this.ChkItCtCode_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(5, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 12;
            this.label9.Text = "Item\'s Category";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(103, 26);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 30;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 400;
            this.LueItCtCode.Size = new System.Drawing.Size(395, 20);
            this.LueItCtCode.TabIndex = 13;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.ChkAutoChoose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(619, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(165, 116);
            this.panel4.TabIndex = 28;
            // 
            // ChkAutoChoose
            // 
            this.ChkAutoChoose.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkAutoChoose.Location = new System.Drawing.Point(0, 94);
            this.ChkAutoChoose.Name = "ChkAutoChoose";
            this.ChkAutoChoose.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAutoChoose.Properties.Appearance.Options.UseFont = true;
            this.ChkAutoChoose.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAutoChoose.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkAutoChoose.Properties.Caption = "Automatic choose item";
            this.ChkAutoChoose.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAutoChoose.Size = new System.Drawing.Size(165, 22);
            this.ChkAutoChoose.TabIndex = 23;
            this.ChkAutoChoose.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAutoChoose.ToolTipTitle = "Run System";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(15, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "Cost Category";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCt2
            // 
            this.LueCCt2.EnterMoveNextControl = true;
            this.LueCCt2.Location = new System.Drawing.Point(103, 70);
            this.LueCCt2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCt2.Name = "LueCCt2";
            this.LueCCt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt2.Properties.Appearance.Options.UseFont = true;
            this.LueCCt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCt2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCt2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCt2.Properties.DropDownRows = 30;
            this.LueCCt2.Properties.MaxLength = 40;
            this.LueCCt2.Properties.NullText = "[Empty]";
            this.LueCCt2.Properties.PopupWidth = 400;
            this.LueCCt2.Size = new System.Drawing.Size(395, 20);
            this.LueCCt2.TabIndex = 19;
            this.LueCCt2.ToolTip = "F4 : Show/hide list";
            this.LueCCt2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCt2.EditValueChanged += new System.EventHandler(this.LueCCt2_EditValueChanged);
            // 
            // ChkCCt2
            // 
            this.ChkCCt2.Location = new System.Drawing.Point(501, 69);
            this.ChkCCt2.Name = "ChkCCt2";
            this.ChkCCt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCCt2.Properties.Appearance.Options.UseFont = true;
            this.ChkCCt2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCCt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCCt2.Properties.Caption = " ";
            this.ChkCCt2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCCt2.Size = new System.Drawing.Size(19, 22);
            this.ChkCCt2.TabIndex = 20;
            this.ChkCCt2.ToolTip = "Remove filter";
            this.ChkCCt2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCCt2.ToolTipTitle = "Run System";
            this.ChkCCt2.CheckedChanged += new System.EventHandler(this.ChkCCt2_CheckedChanged);
            // 
            // FrmItemCostCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 499);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmItemCostCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCCt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoChoose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCt2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.LookUpEdit LueCCt;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueCC;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCtCode;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkAutoChoose;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.LookUpEdit LueCCt2;
        private DevExpress.XtraEditors.CheckEdit ChkCCt2;

    }
}