﻿#region Update
/*
    6/12/2019 [DITA+VIN/IMS] New App
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmComponentSemesterAssessment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmComponentSemesterAssessmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmComponentSemesterAssessment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method 

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            
            if (this.Text.Length == 0) this.Text = "Component Semester Assessment";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueASType, "ComponentSemesterAssessmentType");
                LueASType.Visible = false;
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "DNo",

                        //1-4
                        "Type Code",
                        "Type Name", 
                        "Component",
                        "Remark"
                       
                    },
                    new int[] 
                    {
                        //0
                        100,

                        //1-4
                        150, 150, 300, 300
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] {0, 1 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAssessmentCode, TxtAssessmentName, MeeRemark, ChkActInd
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtAssessmentCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAssessmentName, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtAssessmentName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtAssessmentCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         ChkActInd
                    }, false);
                    Grd1.ReadOnly = true;
                    TxtAssessmentName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAssessmentCode, TxtAssessmentName, MeeRemark, LueASType
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmComponentSemesterAssessmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                string AssessmentCode;
                if (TxtAssessmentCode.Text.Length == 0) AssessmentCode = GenerateAssesssmentCode();
                else AssessmentCode = TxtAssessmentCode.Text;
                
                var cml = new List<MySqlCommand>();
               
                cml.Add(SaveComponentSemesterAssessment(AssessmentCode));

                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SaveComponentSemesterAssessmentDtl(ref Grd1, AssessmentCode, Row));
                }

                Sm.ExecCommands(cml);

                ShowData(AssessmentCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AssessmentCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowComponentSemesterAssessment(AssessmentCode);
                ShowComponentSemesterAssessmentDtl(ref Grd1, AssessmentCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowComponentSemesterAssessment(string AssessmentCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssessmentCode", AssessmentCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "SELECT AssessmentCode, AssessmentName, Remark, ActInd FROM tblcomponentsemesterassessmenthdr WHERE AssessmentCode=@AssessmentCode;",
                    new string[] { 
                        //0
                        "AssessmentCode", 
                        //1-3
                        "ActInd", "AssessmentName", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAssessmentCode.EditValue = Sm.DrStr(dr, c[0]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y");
                        TxtAssessmentName.EditValue = Sm.DrStr(dr, c[2]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);

                    }, true
                );
        }

        private void ShowComponentSemesterAssessmentDtl(ref iGrid GrdTemp, string AssessmentCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AssessmentDNo, A.Type, A.Component, A.Remark, B.Optdesc As TypeName");
            SQL.AppendLine("FROM TblComponentSemesterAssessmentDtl A ");
            SQL.AppendLine("Inner Join TblOption B On A.Type = B.OptCode Where B.OptCat = 'ComponentSemesterAssessmentType'  ");
            SQL.AppendLine("And A.AssessmentCode=@AssessmentCode  ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AssessmentCode", AssessmentCode);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "AssessmentDNo",
                        //1-4
                        "Type" , "TypeName", "Component", "Remark"
                    },


                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAssessmentName, "Assessment Name", false) ||
                IsGrdEmpty();
        }
        private string GenerateAssesssmentCode()
        {
            return Sm.GetValue(
                "SELECT CONCAT('AS',Right(CONCAT('00000', ASCodeMax) ,5)) AS AssessmentCode " +
                "From (Select ifnull(Max(Cast(SUBSTRING(Trim(AssessmentCode),3,7) As DECIMAL)), 00000)+1 ASCodeMax From TblComponentSemesterAssessmentHdr) T;"
                );
        }
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveComponentSemesterAssessment(string AssessmentCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblComponentSemesterAssessmenthdr(AssessmentCode, AssessmentName, Remark, ActInd, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@AssessmentCode, @AssessmentName, @Remark, @ActInd, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY ");
            SQL.AppendLine("   UPDATE AssessmentName=@AssessmentName, Remark = @Remark, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("DELETE FROM TblComponentSemesterAssessmentDtl WHERE AssessmentCode=@AssessmentCode; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssessmentCode", AssessmentCode);
            Sm.CmParam<String>(ref cm, "@AssessmentName", TxtAssessmentName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveComponentSemesterAssessmentDtl(ref iGrid Grd, string AssessmentCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblComponentSemesterAssessmentDtl(AssessmentCode, AssessmentDNo, Type, Component, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@AssessmentCode, @AssessmentDNo, @Type, @Component, @Remark, @UserCode, CurrentDateTime()) ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssessmentCode", AssessmentCode);
            Sm.CmParam<String>(ref cm, "@AssessmentDNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@Type", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<String>(ref cm, "@Component", Sm.GetGrdStr(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }
        #endregion
        #endregion

        #region Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 2, 3, 4 }, e.ColIndex))
                {
                    if (e.ColIndex == 2) LueRequestEdit(Grd1, LueASType, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }

        }
        private void LueASType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueASType, new Sm.RefreshLue2(Sl.SetLueOption), "ComponentSemesterAssessmentType");
        }
        private void LueASType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }
        private void LueASType_Leave(object sender, EventArgs e)
        {
            if (LueASType.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueASType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueASType);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueASType.GetColumnValue("Col2");
                }
                LueASType.Visible = false;
            }
        }
        #endregion

    }
}
