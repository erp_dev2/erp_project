﻿#region Update
/*
    25/02/2023 [WED/MNET] new reporting
    27/02/2023 [WED/MNET] header Estimated Cost Recorded ambil dari PRJI ter pilih
    28/02/2023 [WED/MNET] class di public
    02/03/2023 [WED/MNET] penyesuaian nama menu untuk RecvVd dan RecvVd2 ketika dia auto DO atau bukan
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetRealization : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetRealization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);                
                SetGrd();
                TxtRealizationAmt.EditValue = TxtEstimatedCostRecorded.EditValue = TxtBalanceAmt.EditValue = Sm.FormatNum(0m, 0);
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, E.ProjectName, F.CtName, G.DocNo As RBPDocNo, G.ResourceItCode, G.ItName, G.COGSAmt, ");
            SQL.AppendLine("G.DroppingRequestDocNo, IfNull(G.DroppingRequestAmt, 0.00) DroppingRequestAmt, G.DroppingRequestType, G.DroppingRequestTypeCode ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    And A.Status = 'A' And A.CancelInd = 'N' And A.ProcessInd = 'F' ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer F On E.CtCode = F.CtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T1.PRJIDocNo, T1.ResourceItCode, T2.ItName, T3.Amt COGSAmt, T5.DocNo DroppingRequestDocNo, ");
            SQL.AppendLine("    T6.Amt DroppingRequestAmt, T7.OptDesc DroppingRequestType, T6.DocType DroppingRequestTypeCode ");
            SQL.AppendLine("    From TblProjectImplementationRBPHdr T1 ");
            SQL.AppendLine("    Inner Join TblItem T2 On T1.ResourceItCode = T2.ItCode ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl2 T3 On T1.PRJIDocNo = T3.DocNo And T1.ResourceItCode = T3.ResourceItCode ");
            SQL.AppendLine("    Inner Join TblProjectImplementationRBPDtl T4 On T1.DocNo = T4.DocNo ");
            SQL.AppendLine("    Left Join TblDroppingRequestDtl T5 On T4.DocNo = T5.PRBPDocNo And T4.DNo = T5.PRBPDNo ");
            SQL.AppendLine("        And T5.DocNo In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select DocNo From TblDroppingRequestHdr Where CancelInd = 'N' And Status = 'A' And ProcessInd = 'F' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Left Join TblDroppingRequestHdr T6 On T5.DocNo = T6.DocNo ");
            SQL.AppendLine("    Left Join TblOption T7 On T6.DocType = T7.OptCode And T7.OptCat = 'DroppingRequestDocType' ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine(") G On A.DocNo = G.PRJIDocNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Project Implementation#",
                    "Project Name",
                    "Customer",
                    "RBP Project Implementation",
                    "", 
                    
                    //6-10
                    "Item Name",
                    "Item Code",
                    "COGS Amount",
                    "Dropping Request",
                    "Dropping Request"+Environment.NewLine+"Amount",

                    //11-15
                    "",
                    "Dropping Request Type",
                    "Document For Realization",
                    "",
                    "Realization Date",

                    //16-19
                    "Realization Amount",
                    "Difference Amount",
                    "Dropping request type code",
                    "AutoDO Ind"
                },
                new int[]
                {
                    //0
                    50, 

                    //1-5
                    180, 200, 200, 180, 20,  
                    
                    //6-10
                    150, 100, 150, 180, 150,

                    //11-15
                    20, 180, 200, 20, 100,

                    //16-19
                    150, 150, 0, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 16, 17 }, 0);
            //Sm.GrdFormatDate(Grd1, new int[] { 15 });
            Sm.GrdColButton(Grd1, new int[] { 5, 11, 14 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 18, 19 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Project Implementation", false)) return;

                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL,
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-5
                        "ProjectName", "CtName", "RBPDocNo", "ItName", "ResourceItCode", 
                        //6-10
                        "COGSAmt", "DroppingRequestDocNo", "DroppingRequestAmt", "DroppingRequestType", "DroppingRequestTypeCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 10);
                    }, true, false, false, false
                );
                if (Grd1.Rows.Count > 0)
                {
                    ProcessRealizationDocs(TxtDocNo.Text);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
            {
                var f = new FrmProjectImplementationRBP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length > 0)
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length > 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length > 0)
                {
                    int counts = 0;

                    string[] docs = Sm.GetGrdStr(Grd1, e.RowIndex, 13).Split(',');
                    foreach (var x in docs.Where(w => w.Length > 0))
                    {
                        counts++;
                    }

                    if (counts > 1)
                    {
                        Sm.FormShowDialog(new FrmRptProjectBudgetRealizationDlg2(this, Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 18)));
                    }
                    else
                    {
                        switch (Sm.GetGrdStr(Grd1, e.RowIndex, 18))
                        {
                            case "1":

                                string MenuCode = string.Empty;

                                if (Sm.GetGrdStr(Grd1, e.RowIndex, 19) == "Y") MenuCode = Sm.GetParameter("MenuCodeForRecvVdAutoCreateDO");
                                else MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmRecvVd' And MenuCode Not In (Select ParValue From TblParameter WHere parCode = 'MenuCodeForRecvVdAutoCreateDO' And ParValue Is Not Null) LImit 1; ");

                                var f = new FrmRecvVd(MenuCode);
                                f.Tag = MenuCode;
                                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                                f.WindowState = FormWindowState.Normal;
                                f.StartPosition = FormStartPosition.CenterScreen;
                                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                                f.ShowDialog();
                                break;
                            case "2":

                                if (Sm.GetGrdStr(Grd1, e.RowIndex, 19) == "Y") MenuCode = Sm.GetParameter("MenuCodeForRecvVd2AutoCreateDO");
                                else MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmRecvVd2' And MenuCode Not In (Select ParValue From TblParameter WHere parCode = 'MenuCodeForRecvVd2AutoCreateDO' And ParValue Is Not Null) LImit 1; ");

                                var g = new FrmRecvVd2(MenuCode);
                                g.Tag = MenuCode;
                                g.Text = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                                g.WindowState = FormWindowState.Normal;
                                g.StartPosition = FormStartPosition.CenterScreen;
                                g.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                                g.ShowDialog();
                                break;
                            case "3":
                                var h = new FrmCashAdvanceSettlement(mMenuCode);
                                h.Tag = mMenuCode;
                                h.WindowState = FormWindowState.Normal;
                                h.StartPosition = FormStartPosition.CenterScreen;
                                h.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                                h.ShowDialog();
                                break;
                            case "4":
                                var i = new FrmPettyCashDisbursement(mMenuCode);
                                i.Tag = mMenuCode;
                                i.WindowState = FormWindowState.Normal;
                                i.StartPosition = FormStartPosition.CenterScreen;
                                i.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                                i.ShowDialog();
                                break;
                            case "5":
                                var j = new FrmVoucher(mMenuCode);
                                j.Tag = mMenuCode;
                                j.WindowState = FormWindowState.Normal;
                                j.StartPosition = FormStartPosition.CenterScreen;
                                j.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                                j.ShowDialog();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ProcessRealizationDocs(string DocNo)
        {
            var l = new List<DroppingRequest>();
            var l2 = new List<DroppingRealization>();
            string DroppingRequestDocNo = string.Empty;

            PrepDroppingRequest(ref l, ref l2, ref DroppingRequestDocNo);
            GetRealizationDocs(ref l2, ref DroppingRequestDocNo);
            DisplayRealizationDocs(ref l2);

            l.Clear(); l2.Clear();
        }

        private decimal GetEstimatedCOGSAmt(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Sum(EstimatedCost), 0.00) Amt ");
            SQL.AppendLine("From TblProjectImplementationDtl ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And SettledInd = 'Y' ");
            SQL.AppendLine("; ");

            return Decimal.Parse(Sm.GetValue(SQL.ToString(), DocNo));
        }

        private void DisplayRealizationDocs(ref List<DroppingRealization> l2)
        {
            decimal RealizationAmt = 0m, EstimatedCOGSAmt = GetEstimatedCOGSAmt(TxtDocNo.Text), BalanceAmt = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0)
                {
                    Grd1.Cells[Row, 17].Value = Sm.FormatNum(0m, 0);

                    foreach(var x in l2.Where(w => w.DroppingRequestDocNo == Sm.GetGrdStr(Grd1, Row, 9) && w.DocType == Sm.GetGrdStr(Grd1, Row, 18)))
                    {
                        Grd1.Cells[Row, 13].Value = x.RealizationDocNo;
                        Grd1.Cells[Row, 15].Value = x.RealizationDocDt;
                        Grd1.Cells[Row, 16].Value = Sm.FormatNum(x.RealizationAmt, 0);
                        Grd1.Cells[Row, 17].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 10) - x.RealizationAmt), 0);
                        Grd1.Cells[Row, 19].Value = x.AutoDOInd ? "Y" : "N";

                        RealizationAmt += x.RealizationAmt;
                    }
                }
            }

            BalanceAmt = EstimatedCOGSAmt - RealizationAmt;

            TxtRealizationAmt.EditValue = Sm.FormatNum(RealizationAmt, 0);
            TxtEstimatedCostRecorded.EditValue = Sm.FormatNum(EstimatedCOGSAmt, 0);
            TxtBalanceAmt.EditValue = Sm.FormatNum(BalanceAmt, 0);
        }

        private void GetRealizationDocs(ref List<DroppingRealization> l2, ref string DroppingRequestDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.* From ( ");

            SQL.AppendLine(Sm.QueryRealizationDocs());

            SQL.AppendLine(") T ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DroppingRequestDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "Realization",
                    "DocType",
                    "RealizationDocDt",
                    "RealizationAmt",
                    "AutoDOInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l2.Where(w => 
                            w.DroppingRequestDocNo == Sm.DrStr(dr, c[0]) && 
                            w.DocType == Sm.DrStr(dr, c[2]))
                            )
                        {
                            x.RealizationDocNo = Sm.DrStr(dr, c[1]);
                            x.RealizationDocDt = Sm.DrStr(dr, c[3]);
                            x.RealizationAmt = Sm.DrDec(dr, c[4]);
                            x.AutoDOInd = Sm.DrStr(dr, c[5]) == "Y";
                        }
                    }
                }
                dr.Close();
            }
        }

        private void PrepDroppingRequest(ref List<DroppingRequest> l, ref List<DroppingRealization> l2, ref string DroppingRequestDocNo)
        {
            var x = new List<DroppingRequest>();

            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    x.Add(new DroppingRequest()
                    {
                        ProjectImplementationDocNo = Sm.GetGrdStr(Grd1, Row, 1),
                        DroppingRequestDocNo = Sm.GetGrdStr(Grd1, Row, 9),
                        DocType = Sm.GetGrdStr(Grd1, Row, 18)
                    });
                }
            }

            if (x.Count > 0)
            {
                l = x.Distinct().ToList();

                foreach (var t in l)
                {
                    l2.Add(new DroppingRealization()
                    {
                        ProjectImplementationDocNo = t.ProjectImplementationDocNo,
                        DroppingRequestDocNo = t.DroppingRequestDocNo,
                        DocType = t.DocType,
                        RealizationDocNo = string.Empty,
                        RealizationDocDt = string.Empty,
                        RealizationAmt = 0m,
                        RemunerationInd = string.Empty,
                        DirectCostInd = string.Empty,
                        IndirectCostInd = string.Empty,
                        AutoDOInd = false
                    });

                    if (DroppingRequestDocNo.Length > 0) DroppingRequestDocNo += ",";
                    DroppingRequestDocNo += t.DroppingRequestDocNo;
                };
            }

            x.Clear();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtRealizationAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRealizationAmt, 0);
        }

        private void TxtEstimatedCostRecorded_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtEstimatedCostRecorded, 0);
        }

        private void TxtBalanceAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtBalanceAmt, 0);
        }

        #endregion

        #region Button Click

        private void BtnPRJIDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRptProjectBudgetRealizationDlg(this));
        }

        #endregion

        #endregion

        #region Class

        public class DroppingRequest
        {
            public string ProjectImplementationDocNo { get; set; }
            public string DroppingRequestDocNo { get; set; }
            public string DocType { get; set; }
        }

        public class DroppingRealization
        {
            public string ProjectImplementationDocNo { get; set; }
            public string DroppingRequestDocNo { get; set; }
            public string DocType { get; set; }
            public string RealizationDocNo { get; set; }
            public string RealizationDocDt { get; set; }
            public decimal RealizationAmt { get; set; } 
            public string RemunerationInd { get; set; }
            public string DirectCostInd { get; set; }
            public string IndirectCostInd { get; set; }
            public bool AutoDOInd { get; set; }
        }

        #endregion
    }
}
