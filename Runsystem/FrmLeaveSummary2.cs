﻿#region Update
/*
    27/12/2019 [HAR/IMS] IMS
 *  27/07/2020 [HAR/IMS] BUG nilai used tidak muncul
 *  10/08/2020 [HAR/IMS] feedback : bisa input data leave summary yang mengupdate data lama di tahun yang sama
 *  09/06/2021 [TRI/IMS] Pilihan departemen dibuat hanya menampilkan departemen yang aktif

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLeaveSummary2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mAnnualLeaveCode = string.Empty;
        internal string mLongServiceLeaveCode = string.Empty;
        internal FrmLeaveSummary2Find FrmFind;
        internal bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmLeaveSummary2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Employee's Leave Summary";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetLueLeaveCode(ref LueLeave);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Old"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Department",
                        
                        //6-10
                        "Position",
                        "Join"+Environment.NewLine+"Date",
                        "Start Leave"+Environment.NewLine+"Date",
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date",
                        
                        //11-13
                        "Number"+Environment.NewLine+"of Days",
                        "Total"+Environment.NewLine+"Leave",
                        "Balance",
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 100, 180, 150, 
                        
                        //6-10
                        150, 120, 120, 120, 120,  
                        
                        //11-15
                        80, 80, 80
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt,LueYr, LueLeave,
                        LueDeptCode, MeeRemark, 
                    }, true);
                    BtnProcess.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 12 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueDeptCode, LueLeave, MeeRemark
                    }, false);
                    BtnProcess.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, LueDeptCode, LueLeave,  MeeRemark, 
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 13  });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            if (BtnSave.Enabled)
            {
                ClearGrd();
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeaveSummary2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
               
                var DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);
                Sm.SetLue(LueYr, Sm.Left(DocDt, 4));
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                ComputeLeave(e.RowIndex);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LeaveSummary", "TblLeaveSummaryHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLeaveSummaryHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLeaveSummaryDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
            Sm.IsDteEmpty(DteDocDt, "Date") ||
            Sm.IsLueEmpty(LueYr, "Year") ||
            Sm.IsLueEmpty(LueDeptCode, "Department") ||
            Sm.IsLueEmpty(LueLeave, "Leave") ||
            IsGrdEmpty();
            //IsLeaveSummaryProcessedAlready();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsLeaveSummaryProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From TblLeaveSummaryHdr A ");
            SQL.AppendLine("Inner Join TblLeaveSummaryDtl B On A.DocNo = B.Docno ");
            SQL.AppendLine("Where B.Yr=@Yr And A.DeptCode = @DeptCode ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Leave year for this department alreeady exist.");
                return true;
            }

            return false;
        }


        private MySqlCommand SaveLeaveSummaryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLeaveSummaryHdr(DocNo, DocDt, DeptCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @DeptCode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveLeaveSummaryDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLeaveSummaryDtl ");
            SQL.AppendLine("(DocNo, DNo, Yr, EmpCode, LeaveCode, InitialDt, StartDt, EndDt, NoOfDays1, NoOfDays2, NoOfDays3, NoOfDays4, Balance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @Yr, @EmpCode, @LeaveCode, @InitialDt, @StartDt, @EndDt, @NoOfDays1, @NoOfDays2, @NoOfDays3, @NoOfDays4, @Balance, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblLeaveSummary(Yr, EmpCode, LeaveCode, InitialDt, StartDt, EndDt, NoOfDays1, NoOfDays2, NoOfDays3, NoOfDays4, Balance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@Yr, @EmpCode, @LeaveCode, @InitialDt, @StartDt, @EndDt, @NoOfDays1, @NoOfDays2, @NoOfDays3, @NoOfDays4, @Balance, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY Update ");
            SQL.AppendLine("    NoOfDays1=@NoOfDays1, NoOfDays2=@NoOfDays2, NoOfDays3=@NoOfDays3,  NoOfDays4=@NoOfDays4, Balance=@Balance, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ; ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("00", (r + 1).ToString()), 3));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeave));
            Sm.CmParamDt(ref cm, "@InitialDt", Sm.GetGrdDate(Grd1, r, 8));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, r, 9));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays1", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays2", Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays3", 0m);
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays4", 0m);
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd1, r, 13));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowLeaveSummaryHdr(DocNo);
                ShowLeaveSummaryDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLeaveSummaryHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, DeptCode, Remark " +
                    "From TblLeaveSummaryHdr Where DocNo=@DocNo;",
                    new string[] { 
                        //0
                        "DocNo",

                        //1-3
                        "DocDt", "DeptCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowLeaveSummaryDtl(string DocNo)
        {
            int SeqNo = 0;
            string Yr = string.Empty;
            string Leave = string.Empty;
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Yr, A.LeaveCode, A.EmpCode, B.EmpCodeOld, B.EmpName, ");
            SQL.AppendLine("D.DeptName, C.PosName, B.JoinDt, A.InitialDt, A.StartDt, A.EndDt,  ");
            SQL.AppendLine("NoOfDays1, NoOfDays2, Balance ");
            SQL.AppendLine("From TblLeaveSummaryDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocNo ;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",

                           //1-5
                           "Yr", "LeaveCode", "EmpCode", "EmpCodeOld", "EmpName", 
                           
                           //6-10
                           "DeptName", "PosName", "JoinDt", "InitialDt", "StartDt", 
                           
                           //11-14
                           "EndDt", "NoOfDays1", "NoOfDays2", "Balance"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        SeqNo += 1;
                        Yr = Sm.DrStr(dr, 1);
                        Leave = Sm.DrStr(dr, 2);
                        Grd.Cells[Row, 0].Value = SeqNo;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 14);
                    }, false, false, true, false
            );
            Sm.SetLue(LueYr, Yr);
            Sm.SetLue(LueLeave, Leave);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }
   
        private void ShowEmployeeInfo()
        {
            try
            {
                int SeqNo = 0;               
                var SQL = new StringBuilder();
                string CurrentDateTime2 = Sm.ServerCurrentDateTime();

                SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.Empname, B.Deptname, C.Posname, A.JoinDt, ");
                SQL.AppendLine("ifnull(Replace(DATE_ADD(A.JoinDt, INTERVAL 1 YEAR), '-', ''), A.leaveStartDt) LeaveStartDt, ");
                SQL.AppendLine("TIMESTAMPDIFF(YEAR, A.JoinDt, ifnull(Replace(DATE_ADD(A.JoinDt, INTERVAL 1 YEAR), '-', ''), A.leaveStartDt)) As bal, ifnull(D.NoOfDays2, 0) As EmpLeave  ");
                SQL.AppendLine("From tblEMployee A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("left Join TblPosition C on A.posCode = C.PosCode ");
                SQL.AppendLine("Left Join TblLeaveSummary D On A.EmpCode = D.EmpCode And D.LeaveCode = @LeaveCode And D.Yr=@Yr ");
                SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>='" + CurrentDateTime2.Substring(0, 8) + "') Or ResignDt Is Null) ");                
                SQL.AppendLine("And A.DeptCode =@DeptCode ");
                if (mIsFilterBySiteHR)
                {
                    SQL.AppendLine("    And A.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                if (mIsFilterByDeptHR)
                {
                    SQL.AppendLine("    And A.DeptCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("        Where DeptCode=IfNull(A.DeptCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }

                var cm = new MySqlCommand();

               Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
               Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeave));
               Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
               Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",  

                    //1-5
                    "EmpCodeOld", "EmpName", "Deptname", "Posname", "JoinDt",

                    //6-8
                    "LeaveStartDt", "Bal", "EmpLeave"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    SeqNo += 1;
                    Grd.Cells[Row, 0].Value = SeqNo;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    ProcessDate(Sm.DrStr(dr, 6), Grd1, Row); 
                }, true, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13 });
                Sm.FocusGrd(Grd1, 0, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueLeaveCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.LeaveCode As Col1, T.LeaveName As Col2 From TblLeave T ");
            SQL.AppendLine("Where T.LeaveCode =@AnnualLeaveCode Or T.LeaveCode = @LongServiceLeaveCode ");
          

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mAnnualLeaveCode);
            Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode", mLongServiceLeaveCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ProcessDate(string InitialLeaveDt, iGrid Grdxx, int Rowx)
        {
             DateTime Dt1 = new DateTime(
                    Int32.Parse(InitialLeaveDt.Substring(0, 4)),
                    Int32.Parse(InitialLeaveDt.Substring(4, 2)),
                    Int32.Parse(InitialLeaveDt.Substring(6, 2)),
                    0, 0, 0
                    );

            string Yr = Sm.GetLue(LueYr);
            string NumbOfDayLeave = Sm.GetValue("Select ifnull(NoOfDay, 0) As Num From TblLeave Where LeaveCode =@Param", Sm.GetLue(LueLeave));
            decimal NumOfLeave = NumbOfDayLeave.Length>0 ? Decimal.Parse(NumbOfDayLeave) : 0m; 

            if (Sm.GetLue(LueLeave) == mAnnualLeaveCode)
            {
                if (InitialLeaveDt.Length > 0)
                {
                    if (Convert.ToInt32(Sm.Left(InitialLeaveDt, 4)) == Convert.ToInt32(Yr))
                    {
                        Grdxx.Cells[Rowx, 9].Value = Sm.ConvertDate(InitialLeaveDt);
                        Grdxx.Cells[Rowx, 10].Value = Sm.ConvertDate(string.Concat(Yr, "12", "31"));
                        Grdxx.Cells[Rowx, 11].Value = NumOfLeave - Decimal.Parse(InitialLeaveDt.Substring(4, 2)) + 1;
                        //Grdxx.Cells[Rowx, 12].Value = 0m;
                        if (Sm.GetGrdDec(Grd1, Rowx, 11) >= Sm.GetGrdDec(Grd1, Rowx, 12))
                            Grdxx.Cells[Rowx, 13].Value = Sm.GetGrdDec(Grd1, Rowx, 11) - Sm.GetGrdDec(Grd1, Rowx, 12);
                        else
                            Grdxx.Cells[Rowx, 13].Value = 0m;
                    }
                    else if (Convert.ToInt32(Sm.Left(InitialLeaveDt, 4)) < Convert.ToInt32(Yr))
                    {
                        Grdxx.Cells[Rowx, 9].Value = Sm.ConvertDate(string.Concat(Yr, "01", "01"));
                        Grdxx.Cells[Rowx, 10].Value = Sm.ConvertDate(string.Concat(Yr, "12", "31"));
                        Grdxx.Cells[Rowx, 11].Value = NumOfLeave;
                        //Grdxx.Cells[Rowx, 12].Value = 0m;
                        if (Sm.GetGrdDec(Grd1, Rowx, 11) >= Sm.GetGrdDec(Grd1, Rowx, 12))
                            Grdxx.Cells[Rowx, 13].Value = Sm.GetGrdDec(Grd1, Rowx, 11) - Sm.GetGrdDec(Grd1, Rowx, 12);
                        else
                            Grdxx.Cells[Rowx, 13].Value = 0m;
                    }
                    else if (Convert.ToInt32(Sm.Left(InitialLeaveDt, 4)) > Convert.ToInt32(Yr))
                    {
                        Grdxx.Cells[Rowx, 9].Value = Sm.ConvertDate(InitialLeaveDt);
                        Grdxx.Cells[Rowx, 10].Value = Sm.ConvertDate(string.Concat(Sm.Left(InitialLeaveDt, 4), "12", "31"));
                        Grdxx.Cells[Rowx, 11].Value = 0m;
                        Grdxx.Cells[Rowx, 12].Value = 0m;
                        Grdxx.Cells[Rowx, 13].Value = 0m;
                    }
                }
            }
           
        }

        private void ComputeLeave(int Rowx)
        {
            if (Sm.GetGrdDec(Grd1, Rowx, 11) >= Sm.GetGrdDec(Grd1, Rowx, 12))
                Grd1.Cells[Rowx, 13].Value = Sm.GetGrdDec(Grd1, Rowx, 11) - Sm.GetGrdDec(Grd1, Rowx, 12);
            else
            {
                if (Sm.GetGrdDec(Grd1, Rowx, 11) > 0)
                    Sm.StdMsg(mMsgType.Warning, "Total leave is bigger than number of days.");
                else
                    Sm.StdMsg(mMsgType.Warning, "You have no day for this leave.");
                Grd1.Cells[Rowx, 13].Value = 0m;
                Grd1.Cells[Rowx, 12].Value = 0m;
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ClearData2();

            if (Sm.IsLueEmpty(LueDeptCode, "Department")) return;
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueLeave, "Leave")) return;
            
            ShowEmployeeInfo();
        }

        private void LueLeave_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueLeave, new Sm.RefreshLue1(SetLueLeaveCode));
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            }
        }
        #endregion

        
    }
}
