﻿#region Update
/*
    21/03/2018 [WED] remark function yang ke deposit summary
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
 *  28/01/2021 [ICA/SIER] Menambah TabCOA dan field Settled Amount berdasarkan param IsReturnARDPUseSettledAmt
 *  17/02/2021 [ICA/SIER] Lup COA pertama berdasarkan parameter COAReturnARDPAcNo
 *  23/02/2021 [ICA/SIER] Lup COA kedua berdasarkan bank account
 *  31/03/2021 []ICA/SIER] Menampilkan field customer category berdasarakan param IsShowCustomerCategory
 *  09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
 *  15/10/2021 [BRI/AMKA] menambah kolom Type mandatory berdasarkan param mIsAPARUseType
 *  28/10/2021 [RIS/AMKA] Membuat field Department terfilter berdasarkan group dan param IsFilterByDept
    29/10/2021 [BRI/AMKA] bug fix
    13/12/2021 [TRI/AMKA] Fixing BUG saat klik tombol loop voucher belum ambil dari voucher docno
    27/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    31/01/2023 [VIN/KBN] bug save return ARDP.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmReturnARDownpayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCtCode = string.Empty, mEntCode = string.Empty;
        internal decimal mSummary = 0m;
        private bool 
            mIsReturnARDownpaymentApprovalBasedOnDept = false,
            mIsReturnARDownpaymentUseEntity = false,
            mIsAPARUseType = false,
            mIsFilterByDept = false;
        internal bool
            mIsReturnARDPUseSettledAmt = false,
            mIsShowCustomerCategory = false,
            mIsFilterByCtCt = false;
        internal FrmReturnARDownpaymentFind FrmFind;

        #endregion

        #region Constructor

        public FrmReturnARDownpayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Return AR Downpayment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetGrd();
                Sl.SetLueUserCode(ref LuePIC);
                //Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueOption(ref LueAcNoType, "AcNoTypeForARDP");
                if (!mIsAPARUseType)
                {
                    label19.Visible = false;
                    LueAcNoType.Visible = false;
                }

                if (mIsReturnARDownpaymentApprovalBasedOnDept)
                    LblDeptCode.ForeColor = Color.Red;

                base.FrmLoad(sender, e);

                // if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsReturnARDPUseSettledAmt)
                {
                    LblSettledAmt.Visible = TxtSettledAmt.Visible = mIsReturnARDPUseSettledAmt;
                    tabControl1.TabPages.Remove(TpgCOAAccount);
                }

                if (!mIsShowCustomerCategory || Sm.GetParameter("DocTitle") != "SIER")
                {
                    label12.Visible = TxtCtCtName.Visible = false;

                    label10.Location = new System.Drawing.Point(99, 155);
                    TxtEntCode.Location = new System.Drawing.Point(142, 152);
                    label3.Location = new System.Drawing.Point(83, 176);
                    TxtCurCode.Location = new System.Drawing.Point(142, 173);
                    label4.Location = new System.Drawing.Point(3, 197);
                    TxtSummary.Location = new System.Drawing.Point(142, 194);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsReturnARDownpaymentApprovalBasedOnDept = Sm.GetParameter("IsReturnARDownpaymentApprovalBasedOnDept") == "Y";
            mIsReturnARDownpaymentUseEntity = Sm.GetParameter("IsReturnARDownpaymentUseEntity") == "Y";
            mIsReturnARDPUseSettledAmt = Sm.GetParameterBoo("IsReturnARDPUseSettledAmt");
            mIsShowCustomerCategory = Sm.GetParameterBoo("IsShowCustomerCategory");
            mIsAPARUseType = Sm.GetParameterBoo("IsAPARUseType");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterByCtCt = Sm.GetParameterBoo("IsFilterByCtCt");
        }

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 4;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1}, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCancelReason, ChkCancelInd,  TxtReturnAmt, LueBankAcCode, 
                        LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, LuePIC, 
                        LueDeptCode, MeeRemark, LueAcNoType, TxtSettledAmt, TxtCtCtName
                    }, true);
                    BtnCtCode.Enabled = false;
                    Grd2.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtReturnAmt, LueBankAcCode, LuePaymentType, LuePIC, 
                        LueDeptCode, MeeRemark, LueAcNoType, TxtSettledAmt
                    }, false);
                    BtnCtCode.Enabled = true;
                    Grd2.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Grd2.ReadOnly = true;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCtCode = string.Empty;
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtStatus, 
                TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtCtCode, TxtEntCode, TxtCurCode, 
                LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                LuePIC, LueDeptCode, MeeRemark, LueAcNoType, TxtSettledAmt, TxtCtCtName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtReturnAmt, TxtSummary, TxtSettledAmt }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, false);
            ClearGrd2();
        }

        #region Clear Grid

        private void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmReturnARDownpaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ReturnARDownpayment", "TblReturnARDownpayment");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveReturnARDownpayment(DocNo, GenerateVoucherRequestDocNo()));
            //cml.Add(UpdateDepositSummary());
            //cml.Add(UpdateDepositSummary2(DocNo));

            if (mIsReturnARDPUseSettledAmt)
            {
                if (Grd2.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveReturnARDownpaymentDtl2(DocNo, Row));
                } 
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtCtCode, "Customer", false) ||
                Sm.IsTxtEmpty(TxtReturnAmt, "Amount", true) ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                (mIsAPARUseType && Sm.IsLueEmpty(LueAcNoType, "Type")) ||
                IsBankAcCodeNotValid() ||
                (mIsReturnARDPUseSettledAmt && IsGrdEmpty())||
                (mIsReturnARDPUseSettledAmt && IsGrdValueNotValid()) ||
                IsAmtNotValid() ||
                (mIsReturnARDPUseSettledAmt && IsJournalAmtNotBalanced()) ||
                (mIsReturnARDPUseSettledAmt && IsBankAcCodeNotValid2());
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Decimal.Parse(TxtSettledAmt.Text) > 0)
            {
                if (Grd2.Rows.Count == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 COA.");
                    return true;
                }
            }
            return false;
        }

        private bool IsAmtNotValid()
        {
            decimal SummaryAmt = 0m, ReturnAmt = 0m, ReturnSettledAmt = 0m;
            var Amt = string.Empty;
            var SQL = new StringBuilder();
            //var cm = new MySqlCommand();

            if (mIsReturnARDPUseSettledAmt)
                SQL.AppendLine("Select A.Amt-(IfNull(B.Amt, 0) + IfNull(B.SetlledAmt, 0)) As Amt ");
            else
                SQL.AppendLine("Select A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.CtCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, Sum(T1.Amt) As Amt, Sum(T1.SettledAmt) As SetlledAmt ");
            SQL.AppendLine("    From TblReturnARDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.CtCode, IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.CtCode=B.CtCode ");
            if (mIsReturnARDownpaymentUseEntity)
            {
                SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(B.EntCode, '') ");
            }
            SQL.AppendLine("    And A.CurCode=B.CurCode ");
            SQL.AppendLine("Where A.CtCode='" + mCtCode + "' ");
            if (mIsReturnARDownpaymentUseEntity)
            {
                SQL.AppendLine("And IfNull(A.EntCode, '')='" + mEntCode + "' ");
            }
            SQL.AppendLine("And A.CurCode='"+ TxtCurCode.Text + "';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            //Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            //Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            //Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);

            Amt = Sm.GetValue(cm);
            if (Amt.Length > 0) SummaryAmt = decimal.Parse(Amt);
            ReturnAmt = decimal.Parse(TxtReturnAmt.Text);

            if (mIsReturnARDPUseSettledAmt)
            {
                ReturnSettledAmt = decimal.Parse(TxtReturnAmt.Text) + decimal.Parse(TxtSettledAmt.Text);
                if (ReturnSettledAmt > SummaryAmt)
                {
                    Sm.StdMsg(mMsgType.Warning, "Return amount (" + Sm.FormatNum(ReturnAmt, 0) + ") + Settled amount (" + Sm.FormatNum(TxtSettledAmt.Text, 0) + ")  is bigger than existing deposit amount (" + Sm.FormatNum(SummaryAmt, 0) + ") ");
                    return true;
                }
            }
            else if (ReturnAmt > SummaryAmt)
            {
                Sm.StdMsg(mMsgType.Warning, "Return amount (" + Sm.FormatNum(ReturnAmt, 0) + ")  is bigger than existing deposit amount (" + Sm.FormatNum(SummaryAmt, 0) + ") ");
                return true;
            }

            
            return false;
        }

        private bool IsBankAcCodeNotValid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select BankAcCode From TblBankAccount " +
                    "Where BankAcCode=@BankAcCode And CurCode=@CurCode;"
            };
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank Account : " + LueBankAcCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Currency : " + TxtCurCode.Text + Environment.NewLine + Environment.NewLine +
                    "Invalid Currency."
                    );
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd2, Row, 3) == 0m && Sm.GetGrdDec(Grd2, Row, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Account# : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;
            decimal ReturnSetlledAmt = Decimal.Parse(TxtReturnAmt.Text) + Decimal.Parse(TxtSettledAmt.Text);

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd2, Row, 3);
                if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd2, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }

            if (Grd2.Rows.Count > 1)
            {
                if (mIsReturnARDPUseSettledAmt && Credit != ReturnSetlledAmt)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Settled Amt : " + Sm.FormatNum(TxtSettledAmt.Text, 0) + Environment.NewLine +
                        "Returned Amt : " + Sm.FormatNum(TxtReturnAmt.Text, 0) + Environment.NewLine + Environment.NewLine +
                        "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                        "Total credit is not balanced."
                        );
                    return true;
                }
            }

            return false;
        }

        private bool IsBankAcCodeNotValid2()
        {
            if (Grd2.Rows.Count > 1)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select COAAcNo From TblBankAccount " +
                        "Where BankAcCode=@BankAcCode;"
                };
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

                if (!Sm.CompareStr(Sm.GetValue(cm), Sm.GetGrdStr(Grd2, 1, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Bank Account : " + LueBankAcCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                        "COA of Bank : " + Sm.GetGrdStr(Grd2, 1, 2)+ Environment.NewLine + Environment.NewLine +
                        "COA of Bank is not match."
                        );
                    return true;
                }

                if (Decimal.Parse(TxtReturnAmt.Text) != Sm.GetGrdDec(Grd2, 1, 4))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Returned Amount : " + Sm.FormatNum(Decimal.Parse(TxtReturnAmt.Text), 0) + Environment.NewLine +
                        "Credit of " + Sm.GetGrdStr(Grd2, 1, 2) + " : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, 1, 4), 0) + Environment.NewLine + Environment.NewLine +
                        "Returned Amount and Credit is not balance."
                        );
                    return true;
                }
                return false;
            }

            return false;
        }

        private MySqlCommand SaveReturnARDownpayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblReturnARDownpayment ");
            if (mIsAPARUseType)
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, CtCode, EntCode, CurCode, VoucherRequestDocNo,  BankAcCode, PaymentType, BankCode, GiroNo, DueDt, DepositAmt, Amt, PIC, DeptCode, SettledAmt, Remark, AcNoType, CreateBy, CreateDt) ");
            else
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, CtCode, EntCode, CurCode, VoucherRequestDocNo,  BankAcCode, PaymentType, BankCode, GiroNo, DueDt, DepositAmt, Amt, PIC, DeptCode, SettledAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            if (mIsAPARUseType)
                SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @CtCode, @EntCode, @CurCode, @VoucherRequestDocNo, @BankAcCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @DepositAmt, @Amt, @PIC, @DeptCode, @SettledAmt, @Remark, @AcNoType, @CreateBy, CurrentDateTime()); ");
            else
                SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @CtCode, @EntCode, @CurCode, @VoucherRequestDocNo, @BankAcCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @DepositAmt, @Amt, @PIC, @DeptCode, @SettledAmt, @Remark, @CreateBy, CurrentDateTime()); ");
            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', @DeptCode, ");
            SQL.AppendLine("'51', Null, 'C', @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, @PIC, 0, @CurCode, @Amt, Null, Concat('Return AR Downpayment (', @CtName, '). ', @Remark), @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Remark, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ReturnARDownpayment' ");
            if (mIsReturnARDownpaymentApprovalBasedOnDept)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblReturnARDownpayment Where DocNo=@DocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ReturnARDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblReturnARDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ReturnARDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@CtName", TxtCtCode.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtSummary.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtReturnAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Decimal.Parse(TxtSettledAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@AcNoType", Sm.GetLue(LueAcNoType));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveReturnARDownpaymentDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblReturnARDownpaymentDtl2(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateDepositSummary()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCustomerDepositSummary ");
        //    SQL.AppendLine("    Set Amt = Amt - @Amt, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where CtCode = @CtCode ");
        //    if (mIsReturnARDownpaymentUseEntity)
        //        SQL.AppendLine("And EntCode = @EntCode ");
        //    SQL.AppendLine("And CurCode = @CurCode; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
        //    if (mIsReturnARDownpaymentUseEntity)
        //        Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
        //    Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtReturnAmt.Text));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdateDepositSummary2(string DocNo)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    var SQLD = new StringBuilder();
        //    var cmD = new MySqlCommand();
        //    var l = new List<DS>();
        //    l.Clear();

        //    SQL.AppendLine("Select CtCode, EntCode, CurCode, ExcRate, Amt ");
        //    SQL.AppendLine("From TblCustomerDepositSummary2 ");
        //    SQL.AppendLine("Where CtCode = @CtCode ");
        //    if (mIsReturnARDownpaymentUseEntity)
        //        SQL.AppendLine("And EntCode = @EntCode ");
        //    SQL.AppendLine("And CurCode = @CurCode ");
        //    SQL.AppendLine("And Amt > 0 ");
        //    SQL.AppendLine("And CurCode <> 'IDR' ");
        //    SQL.AppendLine("Order By CreateDt Desc;");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
        //        if (mIsReturnARDownpaymentUseEntity)
        //            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
        //        Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        {
        //             //0
        //             "CtCode",

        //             //1-4
        //             "EntCode",
        //             "CurCode",
        //             "ExcRate",
        //             "Amt"
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new DS()
        //                {
        //                    CtCode = Sm.DrStr(dr, c[0]),
        //                    EntCode = Sm.DrStr(dr, c[1]),
        //                    CurCode = Sm.DrStr(dr, c[2]),
        //                    ExcRate = Sm.DrDec(dr, c[3]),
        //                    Amt = Sm.DrDec(dr, c[4])
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    if (l.Count > 0)
        //    {
        //        decimal ReturnedAmt = Decimal.Parse(TxtReturnAmt.Text);
        //        for (int i = 0; i < l.Count; i++)
        //        {
        //            if (ReturnedAmt <= l[i].Amt)
        //            {
        //                SQLD.AppendLine("Update TblCustomerDepositSummary2 ");
        //                SQLD.AppendLine("    Set Amt = Amt - " + ReturnedAmt + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                SQLD.AppendLine("Where CtCode = @CtCode0" + i + " ");
        //                if (mIsReturnARDownpaymentUseEntity)
        //                    SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                //SQLD.AppendLine("And Amt > 0 ");
        //                SQLD.AppendLine("; ");

        //                SQLD.AppendLine("Insert Into TblReturnARDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", " + ReturnedAmt + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                Sm.CmParam<String>(ref cmD, "@CtCode0" + i + "", l[i].CtCode);
        //                if (mIsReturnARDownpaymentUseEntity)
        //                    Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                break;
        //            }
        //            else
        //            {
        //                if (i == (l.Count - 1)) // kalau dia record terakhir dari List Deposit Summary 2, Amount nya dikurangin sejumlah ReturnedAmt yang tersisa (berpotensi nilai Amt di CustomerDepositSummary2 itu minus)
        //                {
        //                    SQLD.AppendLine("Update TblCustomerDepositSummary2 ");
        //                    SQLD.AppendLine("    Set Amt = Amt - " + ReturnedAmt + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                    SQLD.AppendLine("Where CtCode = @CtCode0" + i + " ");
        //                    if (mIsReturnARDownpaymentUseEntity)
        //                        SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                    SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                    SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                    //SQLD.AppendLine("And Amt > 0 ");
        //                    SQLD.AppendLine("; ");

        //                    SQLD.AppendLine("Insert Into TblReturnARDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                    SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", " + ReturnedAmt + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                    Sm.CmParam<String>(ref cmD, "@CtCode0" + i + "", l[i].CtCode);
        //                    if (mIsReturnARDownpaymentUseEntity)
        //                        Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                    Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                    Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                    Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                    Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                    ReturnedAmt = 0;
        //                }
        //                else
        //                {
        //                    SQLD.AppendLine("Update TblCustomerDepositSummary2 ");
        //                    SQLD.AppendLine("    Set Amt = Amt - @Amt0" + i + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                    SQLD.AppendLine("Where CtCode = @CtCode0" + i + " ");
        //                    if (mIsReturnARDownpaymentUseEntity)
        //                        SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                    SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                    SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                    //SQLD.AppendLine("And Amt > 0 ");
        //                    SQLD.AppendLine("; ");

        //                    SQLD.AppendLine("Insert Into TblReturnARDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                    SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", @Amt0" + i + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                    Sm.CmParam<String>(ref cmD, "@CtCode0" + i + "", l[i].CtCode);
        //                    if (mIsReturnARDownpaymentUseEntity)
        //                        Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                    Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                    Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                    Sm.CmParam<Decimal>(ref cmD, "@Amt0" + i + "", l[i].Amt);
        //                    Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                    Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                    ReturnedAmt = ReturnedAmt - l[i].Amt;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        SQLD.AppendLine("Select 1; ");
        //    }

        //    cmD.CommandText = SQLD.ToString();

        //    return cmD;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditReturnARDownpayment());
            //cml.Add(EditDepositSummary());
            //cml.Add(EditDepositSummary2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblReturnARDownpayment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document is already cancelled.");
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherRequestHdr " +
                "Where CancelInd='N' And Status<>'C' And VoucherDocNo Is Not Null And DocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document is already process to voucher.");
        }

        private MySqlCommand EditReturnARDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblReturnARDownpayment Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand EditDepositSummary()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCustomerDepositSummary A ");
        //    SQL.AppendLine("Inner Join TblReturnARDownpayment B On A.CtCode = B.CtCode And A.CurCode = B.CurCode ");
        //    if (mIsReturnARDownpaymentUseEntity)
        //        SQL.AppendLine("And A.EntCode = B.EntCode ");
        //    SQL.AppendLine("    Set A.Amt = A.Amt + B.Amt, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where B.DocNo = @DocNo And B.Status <> 'C'; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand EditDepositSummary2()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCustomerDepositSummary2 A ");
        //    SQL.AppendLine("Inner Join ");
        //    SQL.AppendLine("( ");
        //    SQL.AppendLine("    Select T1.DocNo, T1.Status, T1.CancelInd, T1.CtCode, T1.EntCode, T1.CurCode, T2.ExcRate, T2.Amt ");
        //    SQL.AppendLine("    From TblReturnARDownpayment T1 ");
        //    SQL.AppendLine("    Inner Join TblReturnARDownpaymentDtl T2 On T1.DocNo = T2.DocNo ");
        //    SQL.AppendLine("    Where T1.DocNo = @DocNo ");
        //    SQL.AppendLine(")B On A.CtCode = B.CtCode And A.CurCode = B.CurCode And A.ExcRate = B.ExcRate ");
        //    if (mIsReturnARDownpaymentUseEntity)
        //        SQL.AppendLine("And A.EntCode = B.EntCode ");
        //    SQL.AppendLine("Set A.Amt = A.Amt + B.Amt, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where B.Status <> 'C'; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
                SQL.AppendLine("A.CancelReason, A.CancelInd, A.VoucherRequestDocNo, E.VoucherDocNo, A.CtCode, B.CtName, A.EntCode, C.EntName, ");
                SQL.AppendLine("A.CurCode, A.DepositAmt As Summary, A.Amt, A.BankAcCode, A.SettledAmt, ");
                SQL.AppendLine("A.PaymentType, A.BankCode, A.GiroNo, A.DueDt, A.PIC, A.DeptCode, A.Remark ");
                if (mIsAPARUseType)
                    SQL.AppendLine(", A.AcNoType ");
                else
                    SQL.AppendLine(", Null As AcNoType ");
                SQL.AppendLine("From TblReturnARDownpayment A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode "); 
                SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                           //0
                           "DocNo", 

                           // 1-5
                           "DocDt", "StatusDesc", "CancelReason", "CancelInd", "VoucherRequestDocNo", 
                           
                           //6-10
                           "VoucherDocNo", "CtCode", "CtName", "EntCode", "EntName", 
                           
                           //11-15
                           "CurCode", "Summary", "Amt", "BankAcCode", "PaymentType", 
                           
                            //16-20
                           "BankCode", "GiroNo", "DueDt", "PIC", "DeptCode", 
                           
                           //21-23
                           "Remark", "SettledAmt", "AcNoType"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[5]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[6]);
                            mCtCode = Sm.DrStr(dr, c[7]);
                            TxtCtCode.EditValue = Sm.DrStr(dr, c[8]);
                            mEntCode = Sm.DrStr(dr, c[9]);
                            TxtEntCode.EditValue = Sm.DrStr(dr, c[10]);
                            TxtCurCode.EditValue = Sm.DrStr(dr, c[11]);
                            TxtSummary.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[12]), 0);
                            TxtReturnAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                            Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[15]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[16]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[17]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[18]));
                            Sm.SetLue(LuePIC, Sm.DrStr(dr, c[19]));
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[20]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[21]);
                            TxtSettledAmt.Text = Sm.FormatNum(Sm.DrStr(dr, c[22]), 0);
                            Sm.SetLue(LueAcNoType, Sm.DrStr(dr, c[23]));
                        }, true
                    );
                if(mIsReturnARDPUseSettledAmt) ShowReturnARDownpaymentDtl2(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowReturnARDownpaymentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
            SQL.AppendLine("From TblReturnARDownpaymentDtl2 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ReturnARDownpayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",

                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Additional Method

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
        }

        private void TxtReturnAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtReturnAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            ClearGrd2();
        }

        private void TxtSettledAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtSettledAmt, 0);
        }

        private void TxtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd2();
        }

        private void LueAcNoType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcNoType, new Sm.RefreshLue2(Sl.SetLueOption), "AcNoTypeForARDP");
        }

        #endregion

        #region Button Event

        private void BtnCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmReturnARDownpaymentDlg(this));
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Method

        #region Grid 2 Method

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                if (!Sm.IsTxtEmpty(TxtCtCode, "Customer", false) && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmReturnARDownpaymentDlg2(this, e.RowIndex, Sm.GetLue(LueBankAcCode)));
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                if (!Sm.IsTxtEmpty(TxtCtCode, "Customer", false) && !Sm.IsLueEmpty(LueBankAcCode, "Bank Account"))
                    Sm.FormShowDialog(new FrmReturnARDownpaymentDlg2(this, e.RowIndex, Sm.GetLue(LueBankAcCode))); 
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion


        #region Class

        class DS
        {
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
            public string EntCode { get; set; }
        }

        #endregion
    }
}
