﻿#region Update
/*
    08/02/2021 [HAR/PHT] BUG saat filter 
    18/05/2021 [TKG/PHT] tambah otorisasi berdasarkan profit center
    03/06/2021 [VIN/PHT] bug saat chose data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetCategoryDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetCategory mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBudgetCategoryDlg(FrmBudgetCategory FrmParent)
        {
            InitializeComponent();
            this.Text = "List of Cost Center";
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Grd1.ReadOnly = true;
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]{ "No.", "Code", "Name" },
                new int[] { 50, 150, 350 }
            );

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.CCName From TblCostCenter A ");
            SQL.AppendLine("Where 1=1 ");
            if (mFrmParent.mIsBudgetCategoryUseProfitCenter)
            {
                SQL.AppendLine("And A.NotParentInd='Y' ");
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                SQL.AppendLine("    Select CCCode ");
                SQL.AppendLine("    From TblCostCenter ");
                SQL.AppendLine("    Where ActInd='Y' ");
                SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And ProfitCenterCode In (");
                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    )))) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "CCCode", "CCName"});
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo;",
                        new string[] 
                        { 
                            "CCCode",
                            "CCName",
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            mFrmParent.mCCCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
            mFrmParent.TxtCCCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
            mFrmParent.TxtCostCenterCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);

            this.Close();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center");
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

    }
}
