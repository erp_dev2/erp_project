﻿#region Update
/*
    31/12/2019 [WED/WMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMPacking : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mDocSeqNo = string.Empty,
            mOutboundDocNo = string.Empty,
            mOutboundDocSeqNo = string.Empty,
            mBarcode = string.Empty,
            mProdCode = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty;
        internal FrmIMMPackingFind FrmFind;
        private bool mIsNew = false;

        #endregion

        #region Constructor

        public FrmIMMPacking(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",
                    
                    //1-5
                    "Bin",
                    "Source",
                    "Product's Code",
                    "Product's Description",
                    "Seller",
                    
                    //6-8
                    "Quantity",
                    "PickingDocNo",
                    "PickingDocSeqNo"
                },
                 new int[] 
                {
                    //0
                    20,

                    //1-5
                    130, 130, 100, 200, 150, 

                    //6-8
                    150, 0, 0
                }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, MeeCancelReason, TxtBarcode, TxtOutbondDocNo }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    BtnOutboundDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, TxtBarcode }, false);
                    Grd1.ReadOnly = false;
                    BtnOutboundDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueWhsCode, MeeCancelReason, TxtBarcode, TxtOutbondDocNo });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMPackingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            MeeCancelReason.Focus();

        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData(sender, e);
                else EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsTxtEmpty(TxtOutbondDocNo, "Outbound#", false))
                        Sm.FormShowDialog(new FrmIMMPackingDlg2(this, Sm.GetLue(LueWhsCode), mProdCode));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsTxtEmpty(TxtOutbondDocNo, "Outbound#", false))
                Sm.FormShowDialog(new FrmIMMPackingDlg2(this, Sm.GetLue(LueWhsCode), mProdCode));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            decimal DocSeqNo = 0m;

            GenerateDocNo(ref DocNo, ref DocSeqNo);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveIMMPackingHdr(DocNo, DocSeqNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    cml.Add(SaveIMMPackingDtl(DocNo, DocSeqNo, r));
                    cml.Add(UpdateIMMPickingDtl(DocNo, DocSeqNo, r));
                }
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void GenerateDocNo(ref string DocNo, ref decimal DocSeqNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMPackingHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
            DocSeqNo += 1;
        }

        private bool IsInsertedDataNotValid()
        {
            string mPickingDocNo2 = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    string mPickingDocNo = Sm.GetGrdStr(Grd1, i, 7);
                    string mPickingDocSeqNo = Sm.GetGrdStr(Grd1, i, 8);

                    if (mPickingDocNo2.Length > 0) mPickingDocNo2 += ",";
                    mPickingDocNo2 += string.Concat(mPickingDocNo, mPickingDocSeqNo);
                }
            }

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtOutbondDocNo, "Outbound#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItemHasBeenPacked(mPickingDocNo2) ||
                IsItemWhsInvalid() ||
                IsStockInvalid()
                ;
        }

        private bool IsItemHasBeenPacked(string PickingDocNo)
        {
            if (PickingDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                string mPickingDocNo = string.Empty, mPickingDocSeqNo = string.Empty, mSource = string.Empty, mPackingDocNo2 = string.Empty;
                int mRow = 0;

                SQL.AppendLine("Select DocNo, DocSeqNo, Source, Concat(PackingDocNo, PackingDocSeqNo) PackingDocNo2 ");
                SQL.AppendLine("From TblIMMPickingDtl ");
                SQL.AppendLine("Where Find_In_Set(Concat(DocNo, DocSeqNo), @Param) ");
                SQL.AppendLine("And PackingDocNo Is Not Null ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@Param", PickingDocNo);

                if (Sm.IsDataExist(SQL.ToString(), PickingDocNo))
                {
                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.CommandText = SQL.ToString();
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DocSeqNo", "Source", "PackingDocNo2" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                mPickingDocNo = Sm.DrStr(dr, c[0]);
                                mPickingDocSeqNo = Sm.DrStr(dr, c[1]);
                                mSource = Sm.DrStr(dr, c[2]);
                                mPackingDocNo2 = Sm.DrStr(dr, c[3]);
                            }
                        }
                        dr.Close();
                    }

                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        if (mPickingDocNo == Sm.GetGrdStr(Grd1, i, 7) &&
                            mPickingDocSeqNo == Sm.GetGrdStr(Grd1, i, 8) &&
                            mSource == Sm.GetGrdStr(Grd1, i, 2))
                        {
                            mRow = i;
                            break;
                        }
                    }

                    Sm.StdMsg(mMsgType.Warning, "This data has been packed #" + mPackingDocNo2);
                    return true;
                }
            }

            return false;
        }

        private bool IsItemWhsInvalid()
        {
            var SQL = new StringBuilder();
            string mSource = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mSource.Length > 0) mSource += ",";
                    mSource += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            if (mSource.Length > 0)
            {
                SQL.AppendLine("Select Source ");
                SQL.AppendLine("From TblIMMStockSummary ");
                SQL.AppendLine("Where WhsCode != @Param1 ");
                SQL.AppendLine("And Find_In_Set(Source, @Param2) ");
                SQL.AppendLine("And Qty > 0 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This source : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty) + " is not from warehouse " + LueWhsCode.Text);
                    return true;
                }
            }
            return false;
        }

        private bool IsStockInvalid()
        {
            var SQL = new StringBuilder();
            string mSource = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mSource.Length > 0) mSource += ",";
                    mSource += string.Concat(Sm.GetGrdStr(Grd1, i, 1), Sm.GetGrdStr(Grd1, i, 2));
                }
            }

            if (mSource.Length > 0)
            {
                SQL.AppendLine("Select Source ");
                SQL.AppendLine("From TblIMMStockSummary ");
                SQL.AppendLine("Where WhsCode = @Param1 ");
                SQL.AppendLine("And Find_In_Set(Concat(Bin, Source), @Param2) ");
                SQL.AppendLine("And Qty <= 0 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This source : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty) + " has no stock.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 product.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Source is empty."))
                    return true;
            }
            return false;
        }

        private MySqlCommand SaveIMMPackingHdr(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMPackingHdr ");
            SQL.AppendLine("(DocNo, DocSeqNo, DocDt, CancelInd, WhsCode, OutboundDocNo, OutboundDocSeqNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @DocDt, 'N', @WhsCode, @OutboundDocNo, @OutboundDocSeqNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@OutboundDocNo", mOutboundDocNo);
            Sm.CmParam<Decimal>(ref cm, "@OutboundDocSeqNo", Decimal.Parse(mOutboundDocSeqNo));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIMMPackingDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMPackingDtl(DocNo, DocSeqNo, Source, PickingDocNo, PickingDocSeqNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @Source, @PickingDocNo, @PickingDocSeqNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@PickingDocNo", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@PickingDocSeqNo", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateIMMPickingDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMPickingDtl Set ");
            SQL.AppendLine("    PackingDocNo = @DocNo, ");
            SQL.AppendLine("    PackingDocSeqNo = @DocSeqNo, ");
            SQL.AppendLine("    LastUpBy = @UserCode, ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @PickingDocNo ");
            SQL.AppendLine("And DocSeqNo = @PickingDocSeqNo ");
            SQL.AppendLine("And Source = @Source ");
            SQL.AppendLine("And PackingDocNo Is Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@PickingDocNo", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@PickingDocSeqNo", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditIMMPackingHdr());

            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation");
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblIMMPackingHdr Where CancelInd='Y' And DocNo=@Param1 And DocSeqNo=@Param2;",
                mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditIMMPackingHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMPackingHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblIMMPickingDtl Set ");
            SQL.AppendLine("    PackingDocNo = NULL, ");
            SQL.AppendLine("    PackingDocSeqNo = NULL, ");
            SQL.AppendLine("    LastUpBy = @UserCode, ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where PackingDocNo Is Not Null ");
            SQL.AppendLine("And PackingDocNo = @DocNo ");
            SQL.AppendLine("And PackingDocSeqNo = @DocSeqNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                mDocNo = DocNo;
                mDocSeqNo = DocSeqNo;
                ShowIMMPackingHdr(DocNo, DocSeqNo);
                ShowIMMPackingDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMPackingHdr(string DocNo, string DocSeqNo)
        {
            mDocNo = DocNo;
            mDocSeqNo = DocSeqNo;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocDt, WhsCode, CancelReason, CancelInd, Concat(OutboundDocNo, OutboundDocSeqNo) OutboundDocNo2, ");
            SQL.AppendLine("OutboundDocNo, OutboundDocSeqNo ");
            SQL.AppendLine("From TblIMMPackingHdr ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And DocSeqNo = @DocSeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] { "DocDt", "WhsCode", "CancelReason", "CancelInd", "OutboundDocNo2", "OutboundDocNo", "OutboundDocSeqNo" },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                    Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[1]));                    
                    MeeCancelReason.Text = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    mOutboundDocNo = Sm.DrStr(dr, c[5]);
                    mOutboundDocSeqNo = Sm.DrStr(dr, c[6]);
                }, true
            );
        }

        private void ShowIMMPackingDtl(string DocNo, string DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("B2.Bin, B.Source, C.ProdCode, D.ProdDesc, E.SellerName, B.PickingDocNo, B.PickingDocSeqNo ");
            SQL.AppendLine("From TblIMMPackingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPackingDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B1 On B.PickingDocNo = B1.DocNo And B.PickingDocSeqNo = B1.DocSeqNo And B.Source = B1.Source ");
            SQL.AppendLine("Inner Join TblIMMPickingHdr B2 On B1.DocNo = B2.DocNo And B1.DocSeqNo = B2.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On A.WhsCode = C.WhsCode And B.Source=C.Source And B2.Bin = C.Bin ");
            SQL.AppendLine("Inner Join TblIMMProduct D On C.ProdCode=D.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller E On C.SellerCode=E.SellerCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Order By A.DocNo, A.DocSeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Bin", 

                    //1-5
                    "Source", 
                    "ProdCode", 
                    "ProdDesc", 
                    "SellerName", 
                    "PickingDocNo",

                    //6
                    "PickingDocSeqNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Grd.Cells[Row, 6].Value = 1m;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedPicked()
        {
            string SQL = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 7).Length > 0)
                {
                    if (SQL.Length > 0) SQL += ",";
                    SQL += string.Concat(Sm.GetGrdStr(Grd1, i, 7), Sm.GetGrdStr(Grd1, i, 8), Sm.GetGrdStr(Grd1, i, 2));
                }
            }

            return (SQL.Length == 0) ? "XXX" : SQL;
        }

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMPacking';");
        }

        internal string GetSelectedSource()
        {
            var Source = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (Source.Length > 0) Source += ",";
                    Source += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            return (Source.Length == 0) ? "XXX" : Source;
        }

        private void ShowBarcodeInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.Bin, A.Source, D.ProdCode, E.ProdDesc, F.SellerName, D.Qty, ");
            SQL.AppendLine("B.DocNo PickingDocNo, B.DocSeqNo PickingDocSeqNo ");
            SQL.AppendLine("From TblIMMStockMovement A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B ON A.DocType = '11' ");
            SQL.AppendLine("    And A.Source = @Source ");
            SQL.AppendLine("    And A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("    And A.Source = B.Source ");
            SQL.AppendLine("    And B.PackingDocNo Is Null ");
            SQL.AppendLine("Inner Join TblIMMPickingHdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("    And B.DocSeqNo = C.DocSeqNo ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblIMMStockSummary D On C.WhsCode = D.WhsCode ");
            SQL.AppendLine("    And C.Bin = D.Bin ");
            SQL.AppendLine("    And B.Source = D.Source ");
            SQL.AppendLine("    And D.Qty > 0.00 ");
            SQL.AppendLine("Inner Join TblIMMProduct E On D.ProdCode = E.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller F On D.SellerCode = F.SellerCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode = A.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("; ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, Source = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 3);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.Source=@Source0" + r.ToString() + " ) ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not (" + Filter + ") Limit 1;";
                else
                    Filter = " Limit 1;";
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Source", TxtBarcode.Text);

                int Row = Grd1.Rows.Count - 1;
                Grd1.BeginUpdate();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString() + Filter;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Bin", 
                        //1-5
                        "Source", "ProdCode", "ProdDesc", "SellerName", "Qty", 
                        //6-7
                        "PickingDocNo", "PickingDocSeqNo"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Row++;
                        }
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        Console.Beep();
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                TxtBarcode.EditValue = null;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnOutboundDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmIMMPackingDlg(this));
            }
        }

        private void BtnOutboundDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtOutbondDocNo, "Outbound#", false))
            {
                var f = new FrmIMMOutboundOrder(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = mOutboundDocNo;
                f.mDocSeqNo = mOutboundDocSeqNo;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void TxtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtBarcode.Focus();
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
                Sm.ClearGrd(Grd1, true);
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        #endregion

        #endregion
    }
}
