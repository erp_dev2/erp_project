﻿#region Update
/*
    21/08/2019 [TKG] tambah display name
    09/09/2019 [DITA] Bug saat klik lup year
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDepreciationAsset3Dlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptDepreciationAsset3 mFrmParent;
        private string mSQL = string.Empty, mYear = string.Empty, mAssetCode = string.Empty, mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDepreciationAsset3Dlg(FrmRptDepreciationAsset3 FrmParent, string DocNo, string AssetCode, string Year)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mAssetCode = AssetCode;
            mDocNo = DocNo;
            mYear = Year;
        }
        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document", 
                    "Asset's Code",
                    "Asset's Name",
                    "Display Name",
                    "Item's Code",
                    
                    //6-10
                    "Item's Name",
                    "Asset"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Method",
                    "Year",
                    "Month",

                    //11-12
                    "Depreciation"+Environment.NewLine+"Value",
                    "Cost"+Environment.NewLine+"Value"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 120, 200, 200, 100, 

                    //6-10
                    200, 130, 150, 60, 60, 

                    //11-12
                    150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6, 12 }, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, A.AssetCode, C.AssetName, C.DisplayName, C.ItCode, D.ItName,  ");
            SQL.AppendLine("A.AssetValue, H.OptDesc As DepMethod, B.Yr, B.Mth,  B.DepreciationValue, B.CostValue ");
            SQL.AppendLine("From tbldepreciationassethdr A ");
            SQL.AppendLine("Inner Join tbldepreciationassetdtl B On B.DocNo = A.DocNo ");
            SQL.AppendLine("Inner Join tblasset C On C.AssetCode = A.AssetCode ");
            SQL.AppendLine("Inner join tblitem D On D.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join tbloption H On A.DepreciationCode=H.OptCode And H.OptCat = 'DepreciationMethod' ");
            SQL.AppendLine("Where A.CancelInd = 'N' And  A.DocNo=@DocNo And A.AssetCode=@AssetCode And B.Yr=@Yr ");
            SQL.AppendLine("Order By B.Yr, B.Mth;");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@AssetCode", mAssetCode);
            Sm.CmParam<String>(ref cm, "@Yr", mYear);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "AssetCode", "AssetName", "DisplayName", "ItCode", "ItName", 

                        //6-10
                        "AssetValue", "DepMethod", "Yr", "Mth", "DepreciationValue", 
                        
                        //11
                        "CostValue"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    }, false, false, false, false
                );
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion
    }
}
