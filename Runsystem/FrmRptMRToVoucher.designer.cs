﻿namespace RunSystem
{
    partial class FrmRptMRToVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtMRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkMRDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPODocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkPODocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtVCRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkVCRDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtVCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkVCDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkPIDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkOPDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPODocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVCRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOPDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(755, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtOPDocNo);
            this.panel2.Controls.Add(this.ChkOPDocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtPIDocNo);
            this.panel2.Controls.Add(this.ChkPIDocNo);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtPODocNo);
            this.panel2.Controls.Add(this.ChkPODocNo);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtMRDocNo);
            this.panel2.Controls.Add(this.ChkMRDocNo);
            this.panel2.Size = new System.Drawing.Size(755, 120);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.RowResizeMode = TenTec.Windows.iGridLib.iGRowResizeMode.RowHdrAndAllCols;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(755, 353);
            this.Grd1.TabIndex = 40;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 120);
            this.panel3.Size = new System.Drawing.Size(755, 353);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(119, 69);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(239, 20);
            this.LueDeptCode.TabIndex = 35;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(80, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 37;
            this.label2.Text = "Item";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(119, 91);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(239, 20);
            this.TxtItCode.TabIndex = 38;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(363, 89);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCode.TabIndex = 39;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // ChkDeptCode
            // 
            this.ChkDeptCode.Location = new System.Drawing.Point(363, 67);
            this.ChkDeptCode.Name = "ChkDeptCode";
            this.ChkDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCode.Properties.Caption = " ";
            this.ChkDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCode.Size = new System.Drawing.Size(19, 22);
            this.ChkDeptCode.TabIndex = 36;
            this.ChkDeptCode.ToolTip = "Remove filter";
            this.ChkDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCode.ToolTipTitle = "Run System";
            this.ChkDeptCode.CheckedChanged += new System.EventHandler(this.ChkDeptCode_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(40, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 34;
            this.label7.Text = "Department";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(120, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 9;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(238, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(85, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 10;
            this.label3.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "Material Request#";
            // 
            // TxtMRDocNo
            // 
            this.TxtMRDocNo.EnterMoveNextControl = true;
            this.TxtMRDocNo.Location = new System.Drawing.Point(120, 26);
            this.TxtMRDocNo.Name = "TxtMRDocNo";
            this.TxtMRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtMRDocNo.Properties.MaxLength = 250;
            this.TxtMRDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtMRDocNo.TabIndex = 13;
            this.TxtMRDocNo.Validated += new System.EventHandler(this.TxtMRDocNo_Validated);
            // 
            // ChkMRDocNo
            // 
            this.ChkMRDocNo.Location = new System.Drawing.Point(340, 25);
            this.ChkMRDocNo.Name = "ChkMRDocNo";
            this.ChkMRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkMRDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkMRDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkMRDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkMRDocNo.Properties.Caption = " ";
            this.ChkMRDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkMRDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkMRDocNo.TabIndex = 14;
            this.ChkMRDocNo.ToolTip = "Remove filter";
            this.ChkMRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkMRDocNo.ToolTipTitle = "Run System";
            this.ChkMRDocNo.CheckedChanged += new System.EventHandler(this.ChkMRDocNo_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(86, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "PO#";
            // 
            // TxtPODocNo
            // 
            this.TxtPODocNo.EnterMoveNextControl = true;
            this.TxtPODocNo.Location = new System.Drawing.Point(120, 48);
            this.TxtPODocNo.Name = "TxtPODocNo";
            this.TxtPODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPODocNo.Properties.MaxLength = 250;
            this.TxtPODocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtPODocNo.TabIndex = 16;
            this.TxtPODocNo.Validated += new System.EventHandler(this.TxtPODocNo_Validated);
            // 
            // ChkPODocNo
            // 
            this.ChkPODocNo.Location = new System.Drawing.Point(340, 47);
            this.ChkPODocNo.Name = "ChkPODocNo";
            this.ChkPODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPODocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkPODocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPODocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPODocNo.Properties.Caption = " ";
            this.ChkPODocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPODocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkPODocNo.TabIndex = 17;
            this.ChkPODocNo.ToolTip = "Remove filter";
            this.ChkPODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPODocNo.ToolTipTitle = "Run System";
            this.ChkPODocNo.CheckedChanged += new System.EventHandler(this.ChkPODocNo_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueSiteCode);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.ChkSiteCode);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtVCRDocNo);
            this.panel4.Controls.Add(this.ChkVCRDocNo);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtVCDocNo);
            this.panel4.Controls.Add(this.ChkVCDocNo);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.ChkDeptCode);
            this.panel4.Controls.Add(this.ChkItCode);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtItCode);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(362, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(389, 116);
            this.panel4.TabIndex = 24;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(118, 47);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(240, 20);
            this.LueSiteCode.TabIndex = 32;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(85, 49);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 14);
            this.label11.TabIndex = 31;
            this.label11.Text = "Site";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(363, 45);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(19, 22);
            this.ChkSiteCode.TabIndex = 33;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(2, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 14);
            this.label10.TabIndex = 25;
            this.label10.Text = "Voucher Request#";
            // 
            // TxtVCRDocNo
            // 
            this.TxtVCRDocNo.EnterMoveNextControl = true;
            this.TxtVCRDocNo.Location = new System.Drawing.Point(118, 4);
            this.TxtVCRDocNo.Name = "TxtVCRDocNo";
            this.TxtVCRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVCRDocNo.Properties.MaxLength = 250;
            this.TxtVCRDocNo.Size = new System.Drawing.Size(240, 20);
            this.TxtVCRDocNo.TabIndex = 26;
            this.TxtVCRDocNo.Validated += new System.EventHandler(this.TxtVCRDocNo_Validated);
            // 
            // ChkVCRDocNo
            // 
            this.ChkVCRDocNo.Location = new System.Drawing.Point(363, 1);
            this.ChkVCRDocNo.Name = "ChkVCRDocNo";
            this.ChkVCRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVCRDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkVCRDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVCRDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVCRDocNo.Properties.Caption = " ";
            this.ChkVCRDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVCRDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkVCRDocNo.TabIndex = 27;
            this.ChkVCRDocNo.ToolTip = "Remove filter";
            this.ChkVCRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVCRDocNo.ToolTipTitle = "Run System";
            this.ChkVCRDocNo.CheckedChanged += new System.EventHandler(this.ChkVCRDocNo_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 14);
            this.label9.TabIndex = 28;
            this.label9.Text = "Voucher#";
            // 
            // TxtVCDocNo
            // 
            this.TxtVCDocNo.EnterMoveNextControl = true;
            this.TxtVCDocNo.Location = new System.Drawing.Point(118, 26);
            this.TxtVCDocNo.Name = "TxtVCDocNo";
            this.TxtVCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVCDocNo.Properties.MaxLength = 250;
            this.TxtVCDocNo.Size = new System.Drawing.Size(240, 20);
            this.TxtVCDocNo.TabIndex = 29;
            this.TxtVCDocNo.Validated += new System.EventHandler(this.TxtVCDocNo_Validated);
            // 
            // ChkVCDocNo
            // 
            this.ChkVCDocNo.Location = new System.Drawing.Point(363, 25);
            this.ChkVCDocNo.Name = "ChkVCDocNo";
            this.ChkVCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVCDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkVCDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVCDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVCDocNo.Properties.Caption = " ";
            this.ChkVCDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVCDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkVCDocNo.TabIndex = 30;
            this.ChkVCDocNo.ToolTip = "Remove filter";
            this.ChkVCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVCDocNo.ToolTipTitle = "Run System";
            this.ChkVCDocNo.CheckedChanged += new System.EventHandler(this.ChkVCDocNo_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(91, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 14);
            this.label5.TabIndex = 18;
            this.label5.Text = "PI#";
            // 
            // TxtPIDocNo
            // 
            this.TxtPIDocNo.EnterMoveNextControl = true;
            this.TxtPIDocNo.Location = new System.Drawing.Point(120, 70);
            this.TxtPIDocNo.Name = "TxtPIDocNo";
            this.TxtPIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPIDocNo.Properties.MaxLength = 250;
            this.TxtPIDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtPIDocNo.TabIndex = 19;
            this.TxtPIDocNo.Validated += new System.EventHandler(this.TxtPIDocNo_Validated);
            // 
            // ChkPIDocNo
            // 
            this.ChkPIDocNo.Location = new System.Drawing.Point(340, 69);
            this.ChkPIDocNo.Name = "ChkPIDocNo";
            this.ChkPIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPIDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkPIDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPIDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPIDocNo.Properties.Caption = " ";
            this.ChkPIDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPIDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkPIDocNo.TabIndex = 20;
            this.ChkPIDocNo.ToolTip = "Remove filter";
            this.ChkPIDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPIDocNo.ToolTipTitle = "Run System";
            this.ChkPIDocNo.CheckedChanged += new System.EventHandler(this.ChkPIDocNo_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(-1, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Outgoing Payment#";
            // 
            // TxtOPDocNo
            // 
            this.TxtOPDocNo.EnterMoveNextControl = true;
            this.TxtOPDocNo.Location = new System.Drawing.Point(120, 92);
            this.TxtOPDocNo.Name = "TxtOPDocNo";
            this.TxtOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtOPDocNo.Properties.MaxLength = 250;
            this.TxtOPDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtOPDocNo.TabIndex = 22;
            this.TxtOPDocNo.Validated += new System.EventHandler(this.TxtOPDocNo_Validated);
            // 
            // ChkOPDocNo
            // 
            this.ChkOPDocNo.Location = new System.Drawing.Point(340, 91);
            this.ChkOPDocNo.Name = "ChkOPDocNo";
            this.ChkOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkOPDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkOPDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkOPDocNo.Properties.Caption = " ";
            this.ChkOPDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkOPDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkOPDocNo.TabIndex = 23;
            this.ChkOPDocNo.ToolTip = "Remove filter";
            this.ChkOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkOPDocNo.ToolTipTitle = "Run System";
            this.ChkOPDocNo.CheckedChanged += new System.EventHandler(this.ChkOPDocNo_CheckedChanged);
            // 
            // FrmRptMRToVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 473);
            this.Name = "FrmRptMRToVoucher";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPODocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVCRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkOPDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtMRDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkMRDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtPODocNo;
        private DevExpress.XtraEditors.CheckEdit ChkPODocNo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit TxtOPDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkOPDocNo;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtPIDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkPIDocNo;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit TxtVCRDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkVCRDocNo;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit TxtVCDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkVCDocNo;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;

    }
}