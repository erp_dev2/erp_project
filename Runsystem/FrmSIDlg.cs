﻿#region Update
/*
    13/08/2017 [TKG] tambah local document# so
 *  30/07/2019 [DITA] tambah customer's PO 
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSIDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSI mFrmParent;
        string mSQL = string.Empty, mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSIDlg(FrmSI FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SAName, E.ItCode, F.ItCodeInternal, F.ItName, B.PackagingUnitUomCode, ");
            SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, D.PriceUomCode, F.InventoryUomCode, B.DeliveryDt, ");
            SQL.AppendLine("B.QtyPackagingUnit, (B.QtyPackagingUnit-IfNull(G.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit,  ");
            SQL.AppendLine("B.Qty, (B.Qty-IfNull(G.Qty, 0)) As OutstandingQty, A.LocalDocNo, A.CtPONo ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd4<>'F' ");
            SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo,  ");
	        SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty ");  
	        SQL.AppendLine("    From TblSIHdr T1  ");
	        SQL.AppendLine("    Inner Join TblSIDtl T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo And T3.CancelInd='N' And T3.Status Not In ('M', 'F') And T3.CtCode=@CtCode ");
	        SQL.AppendLine("    Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo and T4.ProcessInd<>'F' ");
	        SQL.AppendLine("    Inner Join TblSP T5 On T1.SpDocNo = T5.DocNo  ");
            SQL.AppendLine("    Where  T5.Status <> 'C' "); 
            SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") G On B.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("Where A.CancelInd='N' And A.OverSeaInd = 'Y' ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And A.CtCode=@CtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Shipping Name",
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's Name",

                        //6-10
                        "Item's"+Environment.NewLine+"Local Code",
                        "Packaging"+Environment.NewLine+"UoM", 
                        "Quantity"+Environment.NewLine+"(Packaging)",
                        "Oustanding Quantity"+Environment.NewLine+"(Packaging)",
                        "",
                        
                        //11-15
                        "Sales Order#",
                        "SO"+Environment.NewLine+"Date",
                        "SO DNo",
                        "Quantity"+Environment.NewLine+"(Sales)",
                        "Outstanding Quantity"+Environment.NewLine+"(Sales)",
                        
                        //16-20
                        "UoM"+Environment.NewLine+"(Sales)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Delivery"+Environment.NewLine+"Date",
                        mFrmParent.mLocalDocument=="1"?"Sales"+Environment.NewLine+"Contract#":"Local"+Environment.NewLine+"Document#",
                        "Customer's PO#"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 200, 80, 20, 250,  
                        
                        //6-10
                        80, 100, 100, 100, 20, 
                        
                        //11-15
                        150, 130, 80, 100, 100,   
                        
                        //16-20
                        80, 80, 100, 130, 100 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 14, 15 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13 }, false);
            Grd1.Cols[19].Move(14);
            Grd1.Cols[20].Move(15);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "F.ItCodeInternal", "F.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "SAName", 
                            
                            //1-5
                            "ItCode", "ItName", "ItCodeInternal", "PackagingUnitUomCode", "QtyPackagingUnit",  
                            
                            //6-10
                            "OutstandingQtyPackagingUnit", "DocNo", "DocDt", "DNo", "Qty",
                            
                            //11-15
                            "OutstandingQty", "PriceUomCode", "InventoryUomCode", "DeliveryDt", "LocalDocNo",

                            //16
                            "CtPONo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItemAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 19);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 11, 14 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 17, 19, 20 });
                    }
                }
            }

            mFrmParent.ComputeUom(); 

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItemAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 5) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 7) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 15),
                    Sm.GetGrdStr(Grd1, Row, 3) +
                    Sm.GetGrdStr(Grd1, Row, 11) +
                    Sm.GetGrdStr(Grd1, Row, 13) +
                    Sm.GetGrdStr(Grd1, Row, 7) +
                    Sm.GetGrdStr(Grd1, Row, 16)
                    )) return true;
            return false;
        }


        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region  Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
