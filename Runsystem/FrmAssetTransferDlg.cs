﻿#region Update
/*
 * 03/08/2017 [WED] Tambah kolom dan filter Location From dan Location To
   24/05/2018 [HAR] update ke master asset (costcenter dan asset category)
   18/08/2022 [TYO/PRODUCT] filter cost center berdasarkan parameter IsFilterByCC
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransferDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAssetTransfer mFrmParent;
        private string mSQL;

        #endregion

        #region Constructor

        public FrmAssetTransferDlg(FrmAssetTransfer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLueCCCode(ref LueCCCode, mFrmParent.mIsFilterByCC ? "Y" : "N");
                Sl.SetLueCCCode(ref LueCCCode2, mFrmParent.mIsFilterByCC ? "Y" : "N");
                Sl.SetLueLocCode(ref LueLocCode);
                Sl.SetLueLocCode(ref LueLocCode2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CCName As CCNameFrom, C.CCName As CCNameTo, A.Remark, ");
            SQL.AppendLine("D.LocName As LocNameFrom, E.LocName As LocNameTo, IfNull(D.LocCode, '') As LocCodeFrom, IfNull(E.LocCode, '') As LocCodeTo, ");
            SQL.AppendLine("G.AssetCategoryname AstFrom, H.AssetCategoryName AstTo ");
            SQL.AppendLine("From TblAssetTransferRequestHdr A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCodeFrom = B.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter C On A.CCCodeTo = C.CCCode ");
            SQL.AppendLine("Left Join TblLocation D On A.LocCodeFrom = D.LocCode ");
            SQL.AppendLine("Left Join TblLocation E On A.LocCodeTo = E.LocCode ");
            SQL.AppendLine("left Join TblAssetcategory G On A.AssetCategoryCodeFrom = G.AssetCategoryCode ");
            SQL.AppendLine("left Join TblAssetcategory H On A.AssetCategoryCodeTo = H.AssetCategoryCode ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.DocNo Not In (Select AssetTransferRequestDocNo From TblAssetTransfer) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                   "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Cost Center From",
                    "Cost Center To",

                    //6-10
                    "Location From",
                    "Location To",
                    "Remark",
                    "LocCodeFrom",
                    "LocCodeTo",
                    
                    //11-12
                    "Asset Category"+Environment.NewLine+"From",
                    "Asset Category"+Environment.NewLine+"To",
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 20, 100, 150, 150,

                    //6-10
                    200, 200, 200, 0, 0, 
                    //11-12
                    100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9, 10 }, false);
            if (!mFrmParent.mIsAssetTransferUseLocation)
                Sm.GrdColInvisible(Grd1, new int[] { 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCodeFrom", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode2), "A.CCCodeTo", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "A.LocCodeFrom", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode2), "A.LocCodeTo", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
 
                            //1-5
                            "DocDt", "CCNameFrom", "CCNameTo", "LocNameFrom", "LocNameTo", 
                            
                            //6-10
                            "Remark", "LocCodeFrom", "LocCodeTo", "AstFrom", "AstTo"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtAssetTransferRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowAssetTransferDtl(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ShowCostCenter(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.mLocCodeFrom = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.mLocCodeTo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                mFrmParent.TxtAssetCatCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.TxtAssetCatCode2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAssetTransferRequest(mFrmParent.mMenuCode);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAssetTransferRequest(mFrmParent.mMenuCode);
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Request#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center From");
        }

        private void ChkCCCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center To");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(Sl.SetLueCCCode), mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode2, new Sm.RefreshLue2(Sl.SetLueCCCode), mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueLocCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode2, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location From");
        }

        private void ChkLocCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location To");
        }

        #endregion

        #endregion

    }
}
