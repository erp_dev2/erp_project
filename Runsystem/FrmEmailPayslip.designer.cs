﻿namespace RunSystem
{
    partial class FrmEmailPayslip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPayrunCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnVoucherDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnVoucherDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.ChkPayrunCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkEmpCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.MeeSendingTo = new DevExpress.XtraEditors.MemoExEdit();
            this.LblSendingTo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPayrunCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPayrunCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSendingTo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkEmpCode);
            this.panel2.Controls.Add(this.ChkPayrunCode);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnVoucherDocNo2);
            this.panel2.Controls.Add(this.BtnVoucherDocNo);
            this.panel2.Controls.Add(this.TxtPayrunCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtVoucherDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(796, 74);
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(796, 435);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(796, 435);
            this.Grd1.TabIndex = 20;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Location = new System.Drawing.Point(0, 467);
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(72, 5);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 16;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(288, 20);
            this.TxtVoucherDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Voucher#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPayrunCode
            // 
            this.TxtPayrunCode.EnterMoveNextControl = true;
            this.TxtPayrunCode.Location = new System.Drawing.Point(72, 26);
            this.TxtPayrunCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPayrunCode.Name = "TxtPayrunCode";
            this.TxtPayrunCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPayrunCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPayrunCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPayrunCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPayrunCode.Properties.MaxLength = 300;
            this.TxtPayrunCode.Size = new System.Drawing.Size(288, 20);
            this.TxtPayrunCode.TabIndex = 15;
            this.TxtPayrunCode.Validated += new System.EventHandler(this.TxtPayrunCode_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(15, 29);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "Payrun#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVoucherDocNo
            // 
            this.BtnVoucherDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucherDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucherDocNo.Location = new System.Drawing.Point(363, 3);
            this.BtnVoucherDocNo.Name = "BtnVoucherDocNo";
            this.BtnVoucherDocNo.Size = new System.Drawing.Size(31, 20);
            this.BtnVoucherDocNo.TabIndex = 12;
            this.BtnVoucherDocNo.ToolTip = "Find Voucher";
            this.BtnVoucherDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherDocNo.Click += new System.EventHandler(this.BtnVoucherDocNo_Click);
            // 
            // BtnVoucherDocNo2
            // 
            this.BtnVoucherDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnVoucherDocNo2.Appearance.Options.UseFont = true;
            this.BtnVoucherDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnVoucherDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherDocNo2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnVoucherDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnVoucherDocNo2.Location = new System.Drawing.Point(396, 3);
            this.BtnVoucherDocNo2.Name = "BtnVoucherDocNo2";
            this.BtnVoucherDocNo2.Size = new System.Drawing.Size(31, 20);
            this.BtnVoucherDocNo2.TabIndex = 13;
            this.BtnVoucherDocNo2.ToolTip = "Show Voucher Information";
            this.BtnVoucherDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherDocNo2.ToolTipTitle = "Run System";
            this.BtnVoucherDocNo2.Click += new System.EventHandler(this.BtnVoucherDocNo2_Click);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(72, 47);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 300;
            this.TxtEmpCode.Size = new System.Drawing.Size(288, 20);
            this.TxtEmpCode.TabIndex = 18;
            this.TxtEmpCode.Validated += new System.EventHandler(this.TxtEmpCode_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Employee";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkPayrunCode
            // 
            this.ChkPayrunCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPayrunCode.Location = new System.Drawing.Point(363, 27);
            this.ChkPayrunCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPayrunCode.Name = "ChkPayrunCode";
            this.ChkPayrunCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPayrunCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPayrunCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPayrunCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPayrunCode.Properties.Appearance.Options.UseFont = true;
            this.ChkPayrunCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPayrunCode.Properties.Caption = "";
            this.ChkPayrunCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPayrunCode.Size = new System.Drawing.Size(26, 22);
            this.ChkPayrunCode.TabIndex = 16;
            this.ChkPayrunCode.CheckedChanged += new System.EventHandler(this.ChkPayrunCode_CheckedChanged);
            // 
            // ChkEmpCode
            // 
            this.ChkEmpCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkEmpCode.Location = new System.Drawing.Point(363, 47);
            this.ChkEmpCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkEmpCode.Name = "ChkEmpCode";
            this.ChkEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEmpCode.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.ChkEmpCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEmpCode.Properties.Appearance.Options.UseForeColor = true;
            this.ChkEmpCode.Properties.Caption = "";
            this.ChkEmpCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEmpCode.Size = new System.Drawing.Size(26, 22);
            this.ChkEmpCode.TabIndex = 19;
            this.ChkEmpCode.CheckedChanged += new System.EventHandler(this.ChkEmpCode_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.MeeSendingTo);
            this.panel4.Controls.Add(this.LblSendingTo);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(431, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(361, 70);
            this.panel4.TabIndex = 21;
            // 
            // MeeSendingTo
            // 
            this.MeeSendingTo.EnterMoveNextControl = true;
            this.MeeSendingTo.Location = new System.Drawing.Point(75, 4);
            this.MeeSendingTo.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSendingTo.Name = "MeeSendingTo";
            this.MeeSendingTo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeSendingTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSendingTo.Properties.Appearance.Options.UseBackColor = true;
            this.MeeSendingTo.Properties.Appearance.Options.UseFont = true;
            this.MeeSendingTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSendingTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSendingTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSendingTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSendingTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSendingTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSendingTo.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSendingTo.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSendingTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSendingTo.Properties.MaxLength = 400;
            this.MeeSendingTo.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSendingTo.Properties.ReadOnly = true;
            this.MeeSendingTo.Properties.ShowIcon = false;
            this.MeeSendingTo.Size = new System.Drawing.Size(284, 22);
            this.MeeSendingTo.TabIndex = 66;
            this.MeeSendingTo.ToolTip = "F4 : Show/hide text";
            this.MeeSendingTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSendingTo.ToolTipTitle = "Run System";
            // 
            // LblSendingTo
            // 
            this.LblSendingTo.AutoSize = true;
            this.LblSendingTo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSendingTo.ForeColor = System.Drawing.Color.Black;
            this.LblSendingTo.Location = new System.Drawing.Point(5, 7);
            this.LblSendingTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSendingTo.Name = "LblSendingTo";
            this.LblSendingTo.Size = new System.Drawing.Size(67, 14);
            this.LblSendingTo.TabIndex = 22;
            this.LblSendingTo.Text = "Sending to";
            this.LblSendingTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmEmailPayslip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 509);
            this.KeyPreview = true;
            this.Name = "FrmEmailPayslip";
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPayrunCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPayrunCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmpCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSendingTo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtPayrunCode;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkEmpCode;
        private DevExpress.XtraEditors.CheckEdit ChkPayrunCode;
        private System.Windows.Forms.Panel panel4;
        protected internal System.Windows.Forms.Label LblSendingTo;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeSendingTo;
    }
}