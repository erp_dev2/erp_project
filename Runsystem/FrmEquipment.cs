﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEquipment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEquipmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmEquipment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtEquipmentCode, TxtEquipmentName, ChkActInd }, true);
                    TxtEquipmentCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtEquipmentCode, TxtEquipmentName }, false);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    TxtEquipmentCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtEquipmentCode, TxtEquipmentName }, true);
                    Sm.SetControlReadOnly(ChkActInd, false);
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtEquipmentCode, TxtEquipmentName });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEquipmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEquipmentCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtEquipmentCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var SQLDel = new StringBuilder();

                SQLDel.AppendLine("DELETE FROM TblEquipment WHERE EquipmentCode=@EquipmentCode ");
                
                var cm = new MySqlCommand() { CommandText = SQLDel.ToString() };
                Sm.CmParam<String>(ref cm, "@EquipmentCode", TxtEquipmentCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblEquipment(EquipmentCode, EquipmentName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@EquipmentCode, @EquipmentName, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@EquipmentCode", TxtEquipmentCode.Text);
                Sm.CmParam<String>(ref cm, "@EquipmentName", TxtEquipmentName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtEquipmentCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string EquipmentCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@EquipmentCode", EquipmentCode);

                var SQLShow = new StringBuilder();

                SQLShow.AppendLine("SELECT EquipmentCode, EquipmentName, ActInd From TblEquipment Where EquipmentCode=@EquipmentCode ");

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQLShow.ToString(),
                        new string[] { "EquipmentCode", "EquipmentName", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtEquipmentCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtEquipmentName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEquipmentCode, "Equipment code", false) ||
                Sm.IsTxtEmpty(TxtEquipmentName, "Equipment name", false) ||
                IsEquipmentCodeExisted();
        }

        private bool IsEquipmentCodeExisted()
        {
            if (!TxtEquipmentCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "Select EquipmentCode From TblEquipment Where EquipmentCode=@EquipmentCode;";
                Sm.CmParam<String>(ref cm, "@EquipmentCode", TxtEquipmentCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Equipment code ( " + TxtEquipmentCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void TxtEquipmentCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEquipmentCode);
        }

        private void TxtEquipName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEquipmentName);
        }

        #endregion

        #endregion

    }
}
