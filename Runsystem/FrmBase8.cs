﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase8 : Form
    {
        public FrmBase8()
        {
            InitializeComponent();
        }

        #region Method

        #region From Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Button Method

        virtual protected void BtnAddClick(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            BtnAddClick(sender, e);
        }

        #endregion

        #region Form Event

        private void FrmBase8_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #endregion
    }
}
