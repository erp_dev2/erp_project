﻿#region Update
/*
    21/07/2019 [TKG] New application
    12/08/2019 [TKG] ubah proses source
    28/08/2019 [TKG] menghilangkan angka di celakang koma pada quantity
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmIMMRecvSeller : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            //mIMMRecvSellerWhsCode = string.Empty,
            mIMMRecvSellerLocCode = string.Empty,
            mIMMRecvSellerBin = string.Empty,
            mDocNo = string.Empty, mDocSeqNo = string.Empty; //if this application is called from other application;
        internal FrmIMMRecvSellerFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmIMMRecvSeller(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Receiving Product From Seller";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();                
                SetFormControl(mState.View);
                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Tc1.SelectedTabPage = Tp1;
                Tp2.PageVisible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        { 
            mDocTitle = Sm.GetParameter("DocTitle");
            mIMMRecvSellerLocCode = Sm.GetParameter("IMMRecvSellerLocCode");
            mIMMRecvSellerBin = Sm.GetParameter("IMMRecvSellerBin");
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMRecvSeller';");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(Grd1, new string[] { "Source" }, new int[] { 300 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason, LueWhsCode }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueWhsCode }, false);
                    BtnImport.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Tp1.PageVisible = true;
            Tc1.SelectedTabPage = Tp1;
            Tp2.PageVisible = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtPODocNo, TxtSellerCode, 
                TxtWhsCode, TxtBin, TxtProdCode, TxtProdDesc, TxtCurCode, 
                TxtColor, DteExpiredDt, LueWhsCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty }, 1);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtPrice, TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, false);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMRecvSellerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Tp2.PageVisible = true;
                Tc1.SelectedTabPage = Tp2;
                Tp1.PageVisible = false;
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                LueWhsCode.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void Insert1(ref List<ImportData> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            bool IsFirst = true;
            string
                SellerCodeTemp = string.Empty,
                SellerNameTemp = string.Empty,
                ProdCodeTemp = string.Empty,
                ProdDescTemp = string.Empty,
                ColorTemp = string.Empty,
                CurCodeTemp = string.Empty,
                ExpiredDtTemp = string.Empty;
            decimal QtyTemp = 0m, PriceTemp = 0m;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            SellerCodeTemp = arr[0].Trim();
                            SellerNameTemp = arr[1].Trim();
                            ProdCodeTemp = arr[2].Trim();
                            ProdDescTemp = arr[3].Trim();
                            ColorTemp = arr[4].Trim();
                            CurCodeTemp = arr[5].Trim();
                            if (arr[6].Trim().Length > 0)
                                PriceTemp = decimal.Parse(arr[6].Trim());
                            else
                                PriceTemp = 0m;
                            if (arr[7].Trim().Length > 0)
                                QtyTemp = decimal.Parse(arr[7].Trim());
                            else
                                QtyTemp = 0m;
                            ExpiredDtTemp = arr[8].Trim();
                            if (ExpiredDtTemp.Length > 0)
                            {
                                ExpiredDtTemp = ExpiredDtTemp.Replace("-", "");
                                ExpiredDtTemp = ExpiredDtTemp.Replace(":", "");
                                ExpiredDtTemp = ExpiredDtTemp.Replace(" ", "");
                                ExpiredDtTemp = Sm.Left(ExpiredDtTemp, 8);
                            }
                            l.Add(new ImportData()
                            {
                                SellerCode = SellerCodeTemp,
                                SellerName = SellerNameTemp,
                                ProdCode = ProdCodeTemp,
                                ProdDesc = ProdDescTemp,
                                Color = ColorTemp,
                                CurCode = CurCodeTemp,
                                Price = PriceTemp,
                                Qty = QtyTemp,
                                ExpiredDt = ExpiredDtTemp
                            });
                        }
                    }
                }
            }
        }

        private void Insert2(ref string DocNo, ref decimal DocSeqNo, string CurrentDt)
        {
            string Yr = Sm.Left(CurrentDt, 4);
            string Mth = CurrentDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMRecvSellerHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
        }

        private void Insert3(ref List<ImportData> l, string DocNo, decimal DocSeqNo)
        {
            var s = DocSeqNo;
            foreach(var i in l)
            {
                s += 1m;
                i.PODocNo = string.Concat(DocNo, ((int)s).ToString());
                i.DocNo = DocNo;
                i.DocSeqNo = (int)s;
                i.WhsCode = Sm.GetLue(LueWhsCode);
                i.LocCode = mIMMRecvSellerLocCode;
                i.Bin = mIMMRecvSellerBin;
            }
        }

        private void Insert4(ref List<ImportData> l, string CurrentDt)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            int n = 0;
            decimal s = 1;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", CurrentDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@LocCode", mIMMRecvSellerLocCode);
            Sm.CmParam<String>(ref cm, "@Bin", mIMMRecvSellerBin);

            foreach (var i in l)
            {
                n++;

                SQL.AppendLine("Insert Into TblIMMSeller(SellerCode, SellerName, CreateBy, CreateDt) ");
                SQL.AppendLine("Select SellerCode, SellerName, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select @SellerCode" + n.ToString() + " As SellerCode, ");
                SQL.AppendLine("    @SellerName" + n.ToString() + " As SellerName ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Where Not Exists(Select 1 From TblIMMSeller Where SellerCode=@SellerCode" + n.ToString() + "); ");

                SQL.AppendLine("Insert Into TblIMMProduct(ProdCode, ProdDesc, Color, CreateBy, CreateDt) ");
                SQL.AppendLine("Select ProdCode, ProdDesc, Color, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select @ProdCode" + n.ToString() + " As ProdCode, ");
                SQL.AppendLine("    @ProdDesc" + n.ToString() + " As ProdDesc, ");
                SQL.AppendLine("    @Color" + n.ToString() + " As Color ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Where Not Exists(Select 1 From TblIMMProduct Where ProdCode=@ProdCode" + n.ToString() + "); ");

                SQL.AppendLine("Insert Into TblIMMRecvSellerHdr ");
                SQL.AppendLine("(DocNo, DocSeqNo, DocDt, CancelInd, CancelReason, WhsCode, LocCode, Bin, PODocNo, SellerCode, ProdCode, CurCode, Price, Qty, ExpiredDt, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo" + n.ToString() + ", @DocSeqNo" + n.ToString() + ", @DocDt, 'N', Null, @WhsCode, @LocCode, @Bin, @PODocNo" + n.ToString() + ", @SellerCode" + n.ToString() + ", @ProdCode" + n.ToString() + ", @CurCode" + n.ToString() + ", @Price" + n.ToString() + ", @Qty" + n.ToString() + ", @ExpiredDt" + n.ToString() + ", @UserCode, CurrentDateTime()); ");

                s = 1m;

                while(s<=i.Qty)
                {
                    SQL.AppendLine("Insert Into TblIMMRecvSellerDtl ");
                    SQL.AppendLine("(DocNo, DocSeqNo, Source, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    SQL.AppendLine("(@DocNo" + n.ToString() + ", @DocSeqNo" + n.ToString() + ", ");
                    SQL.AppendLine(string.Concat("@Source", n.ToString(), "_", s.ToString()));
                    SQL.AppendLine(", @UserCode, CurrentDateTime()); ");

                    SQL.AppendLine("Insert Into TblIMMStockMovement ");
                    SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    SQL.AppendLine("('01', @DocNo" + n.ToString() + ", @DocSeqNo" + n.ToString() + ", ");
                    SQL.AppendLine(string.Concat("@Source", n.ToString(), "_", s.ToString()));
                    SQL.AppendLine(", @DocDt, @WhsCode, @Bin, @ProdCode" + n.ToString() + ", 1.00, @UserCode, CurrentDateTime()); ");

                    SQL.AppendLine("Insert Into TblIMMStockSummary ");
                    SQL.AppendLine("(WhsCode, Bin, Source, ProdCode, SellerCode, Qty, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    SQL.AppendLine("(@WhsCode, @Bin, ");
                    SQL.AppendLine(string.Concat("@Source", n.ToString(), "_", s.ToString()));
                    SQL.AppendLine(", @ProdCode" + n.ToString() + ", @SellerCode" + n.ToString() + ", 1.00, @UserCode, CurrentDateTime()); ");

                    SQL.AppendLine("Insert Into TblIMMStockPrice ");
                    SQL.AppendLine("(Source, ProdCode, SellerCode, CurCode, Price, ExpiredDt, Status, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values (");
                    SQL.AppendLine(string.Concat("@Source", n.ToString(), "_", s.ToString()));
                    SQL.AppendLine(", @ProdCode" + n.ToString() + ", @SellerCode" + n.ToString() + ", @CurCode" + n.ToString() + ", @Price" + n.ToString() + ", @ExpiredDt" + n.ToString() + ", '1', @UserCode, CurrentDateTime()); ");

                    Sm.CmParam<String>(ref cm, string.Concat("@Source", n.ToString(), "_", s.ToString()), 
                        //string.Concat(i.DocNo, i.DocSeqNo, "/", ((int)s).ToString())
                        GenerateSource(i.DocNo, i.DocSeqNo, s)
                        );
                    s++;
                }

                Sm.CmParam<String>(ref cm, string.Concat("@DocNo", n.ToString()), i.DocNo);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@DocSeqNo", n.ToString()), i.DocSeqNo);
                Sm.CmParam<String>(ref cm, string.Concat("@PODocNo", n.ToString()), i.PODocNo);
                Sm.CmParam<String>(ref cm, string.Concat("@SellerCode", n.ToString()), i.SellerCode);
                Sm.CmParam<String>(ref cm, string.Concat("@SellerName", n.ToString()), i.SellerName);
                Sm.CmParam<String>(ref cm, string.Concat("@ProdCode", n.ToString()), i.ProdCode);
                Sm.CmParam<String>(ref cm, string.Concat("@ProdDesc", n.ToString()), i.ProdDesc);
                Sm.CmParam<String>(ref cm, string.Concat("@Color", n.ToString()), i.Color);
                Sm.CmParam<String>(ref cm, string.Concat("@CurCode", n.ToString()), i.CurCode);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@Price", n.ToString()), i.Price);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@Qty", n.ToString()), i.Qty);
                Sm.CmParamDt(ref cm, string.Concat("@ExpiredDt", n.ToString()), i.ExpiredDt);
            }
            
            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private string GenerateSource(string DocNo, decimal DocSeqNo, decimal s)
        {
            string Source = string.Empty;
            Source = string.Concat(
                ((int)s).ToString(),
                GenerateSource1((Sm.Right(DocNo.Replace("/", string.Empty), 5))), 
                ((int)DocSeqNo).ToString()
            );

            if (Source.Length<14)
                Source = string.Concat(Source,
                    Sm.Right("METYSNURIKABOLG", 14 - Source.Length)
                    );
            return Source;
        }

        private string GenerateSource1(string DocNo)
        {
            // 01908
            string value =
                GenerateSource2(Sm.Left(DocNo, 1)) +
                GenerateSource2(DocNo.Substring(1, 1)) +
                GenerateSource2(DocNo.Substring(2, 1)) +
                GenerateSource2((Sm.Right(DocNo, 2)).Replace("0", string.Empty)) 
                ;
            return value;
        }

        private string GenerateSource2(string v)
        {
            switch (v)
            { 
                case "0":
                    v = "Z";
                    break;
                case "1":
                    v = "Y";
                    break;
                case "2":
                    v = "X";
                    break;
                case "3":
                    v = "W";
                    break;
                case "4":
                    v = "V";
                    break;
                case "5":
                    v = "U";
                    break;
                case "6":
                    v = "T";
                    break;
                case "7":
                    v = "S";
                    break;
                case "8":
                    v = "R";
                    break;
                case "9":
                    v = "Q";
                    break;
                case "10":
                    v = "P";
                    break;
                case "11":
                    v = "O";
                    break;
                case "12":
                    v = "N";
                    break;
            }
            return v;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No||IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditIMMRecvSellerHdr());
            
            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsProductAlreadyInUse();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblIMMRecvSellerHdr Where CancelInd='Y' And DocNo=@Param1 And DocSeqNo=@Param2;",
                mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsProductAlreadyInUse()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblIMMRecvSellerHdr A ");
            SQL.AppendLine("Inner Join TblIMMRecvSellerDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C ");
            SQL.AppendLine("    On C.Qty=0.00 ");
            SQL.AppendLine("    And B.Source=C.Source ");
            SQL.AppendLine("    And A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And A.Bin=C.Bin ");
            SQL.AppendLine("Where A.DocNo=@Param1 And A.DocSeqNo=@Param2 ");
            SQL.AppendLine("Limit 1;");

            if (Sm.IsDataExist(SQL.ToString(), mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Some products already in use.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditIMMRecvSellerHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMRecvSellerHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '02', DocNo, DocSeqNo, Source, DocDt, WhsCode, Bin, ProdCode, -1.00*Qty, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMStockMovement ");
            SQL.AppendLine("Where DocType='01' And DocNo=@DocNo And DocSeqNo=@DocSeqNo;");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMRecvSellerHdr B ");
            SQL.AppendLine("    On B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCode ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("Inner Join TblIMMRecvSellerDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=C.DocSeqNo ");
            SQL.AppendLine("    And A.Source=C.Source ");
            SQL.AppendLine("Set A.Qty=0.00, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime(); ");
        
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                ClearData();
                ShowIMMRecvSellerHdr(DocNo, DocSeqNo);
                ShowIMMRecvSellerDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMRecvSellerHdr(string DocNo, string DocSeqNo)
        {
            mDocNo = DocNo;
            mDocSeqNo = DocSeqNo;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.CancelInd, A.CancelReason, B.WhsName, A.Bin, ");
            SQL.AppendLine("A.PODocNo, C.SellerName, A.ProdCode, D.ProdDesc, A.CurCode, A.Price, A.Qty, D.Color, A.ExpiredDt ");
            SQL.AppendLine("From TblIMMRecvSellerHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMSeller C On A.SellerCode=C.SellerCode ");
            SQL.AppendLine("Inner Join TblIMMProduct D On A.ProdCode=D.ProdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocDt", 

                        //1-5
                        "CancelReason", "CancelInd", "WhsName", "Bin", "PODocNo",
 
                        //6-10
                        "SellerName", "ProdCode", "ProdDesc", "CurCode", "Price",   
                        
                        //11-13
                        "Qty", "Color", "ExpiredDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        MeeCancelReason.Text = Sm.DrStr(dr, c[1]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtWhsCode.Text = Sm.DrStr(dr, c[3]);
                        TxtBin.Text = Sm.DrStr(dr, c[4]);
                        TxtPODocNo.Text = Sm.DrStr(dr, c[5]);
                        TxtSellerCode.Text = Sm.DrStr(dr, c[6]);
                        TxtProdCode.Text = Sm.DrStr(dr, c[7]);
                        TxtProdDesc.Text = Sm.DrStr(dr, c[8]);
                        TxtCurCode.Text = Sm.DrStr(dr, c[9]);
                        TxtPrice.Text = Sm.FormatNum(Sm.DrDec(dr, c[10]), 2);
                        TxtQty.Text = Sm.FormatNum(Sm.DrDec(dr, c[11]), 1);
                        TxtAmt.Text = Sm.FormatNum(Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[11]), 2);
                        TxtColor.Text = Sm.DrStr(dr, c[12]);
                        Sm.SetDte(DteExpiredDt, Sm.DrStr(dr, c[13]));
                    }, true
                );
        }

        private void ShowIMMRecvSellerDtl(string DocNo, string DocSeqNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, "Select Source From TblIMMRecvSellerDtl Where DocNo=@DocNo And DocSeqNo=@DocSeqNo;",
                new string[]{ "Source" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            var l = new List<ImportData>();
            string DocNo = string.Empty;
            string CurrentDt = Sm.ServerCurrentDate();
            decimal DocSeqNo = 0m;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                Insert1(ref l);
                if (l.Count > 0)
                {
                    Insert2(ref DocNo, ref DocSeqNo, CurrentDt);
                    Insert3(ref l, DocNo, DocSeqNo);
                    Insert4(ref l, CurrentDt);

                    Sm.StdMsg(mMsgType.Info, "Importing data process is completed.");

                    BtnCancelClick(sender, e);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        #endregion

        #endregion 

        #region Class

        private class ImportData
        {
            public string SellerCode { get; set; }
            public string SellerName { get; set; }
            public string ProdCode { get; set; }
            public string ProdDesc { get; set; }
            public string Color { get; set; }
            public string CurCode { get; set; }
            public decimal Price { get; set; }
            public decimal Qty { get; set; }
            public string DocNo { get; set; }
            public decimal DocSeqNo { get; set; }
            public string WhsCode { get; set; }
            public string LocCode { get; set; }
            public string Bin { get; set; }
            public string PODocNo { get; set; }
            public string ExpiredDt { get; set; }
        }

        #endregion
    }
}
