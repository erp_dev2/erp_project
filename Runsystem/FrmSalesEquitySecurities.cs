﻿#region Update
/*
    25/04/2022 [SET/PRODUCT] Menu baru
    18/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
                             merubah perhiutngan, Moving Average Cost = Qty * Moiving Average Price
    19/05/2022 [SET/PRODUCT] merubah posisi tampilan Investment Name & Investment Code
                             membuat validasi untuk qty yang diinput tidak lebih dari qty di stock investment
    20/05/2022 [SET/PRODUCT] menyesuikan perhitungan Amount di tab Expenses & Other Detail
                             membuat editable Amount di tab Expenses & Other Detail
                             bisa hapus row di tab Expenses & Other Detail
    23/05/2022 [IBL/PRODUCT] Bug: Hasil perkalian Rate dg Sales Amt di Tab Expenses & Other Details blm sesuai (ComputeAmtDtl2())
                             Data belum masuk ke investmentstockprice.
                             Update ke investmentstocksummarynya masih salah
    25/05/2022 [IBL/PRODUCT] Base Price dan Acquisition Price ambil dari Moving avg price terahir dari Report Investment Movement Summary
    09/06/2022 [IBL/PRODUCT] Tab expenses : Tambah kolom tax, otomatis munculin data dari master expenses type, bisa isi manual.
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesEquitySecurities : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        iGCell fCell;
        bool fAccept;
        internal FrmSalesEquitySecuritiesFind FrmFind;
        private string 
            mDocType = "03",
            mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesEquitySecurities(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                //GetParameter();
                SetLueFinancialInstitutionCode(ref LueFinancialInstitution, string.Empty);
                Sl.SetLueOption(ref LueType, "InvestmentType");
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Investment Code", "Investment Name", "OptCode", "Type", "Category", 

                    //6-10
                    "Investment Bank Account (RDN)", "Investment Bank Account", "Quantity", "UoM", "Moving Average Price", 

                    //11-15
                    "Moving Average Cost", "Recorded Market Price", "Recorded Market", "Recorded Unrealized", "Selling Price", 

                    //16-20
                    "Sales Amount", "Realized Gain/Loss", "InvestmentCtCode", "Acc. Acquisition Qty", "Acc.Acquisition Cost", 
                    
                    //21-24
                    "CreateDt", "Source", "InvestmentEquityCode", "Base Price"
                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    150, 150, 50, 150, 150, 
                    
                    //6-10
                    200, 200, 100, 100, 200, 

                    //11-15
                    200, 200, 200, 200, 200, 
                    
                    //16-20
                    200, 200, 100, 200, 200, 
                    
                    //21-24
                    100, 100, 120, 120

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 24 }, 0);
            //Sm.GrdFormatDate(Grd1, new int[] {  });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 10, 11, 12, 13, 14, 18, 19, 20, 21, 22, 23, 24 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] {  });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "",
                    
                    //1-5
                    "Doc Type Code", "Document Type", "CoA Account", "CoA Name", "Doc Ref#", 

                    //6-10
                    "Doc Date", "Amount", "Remarks", "Rate", "Amt",

                    //11-14
                    "Tax", "DNo", "MultiplierField", ""

                },
                new int[]
                {
                    //0
                    20,

                    //1-5
                    100, 150, 100, 150, 120, 
                    
                    //6-10
                    100, 100, 100, 100, 100,

                    //11-14
                    30, 0, 0, 20

                }
            );
            Sm.GrdColCheck(Grd2, new int[] { 11 });
            Sm.GrdColButton(Grd2, new int[] { 14 });
            Sm.GrdFormatDate(Grd2, new int[] { 6 });
            Sm.GrdFormatDec(Grd2, new int[] { 7, 9, 10 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 9, 10, 12, 13 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { });
            Grd2.Cols[14].Move(3);
            Grd2.Cols[11].Move(7);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 5;
            //Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "No",
                    
                    //1-4
                    "User", "Status", "Date", "Remark",

                },
                new int[]
                {
                    //0
                    25,

                    //1-5
                    100, 100, 100, 100,

                }
            );
            //Sm.GrdColCheck(Grd1, new int[] { 1, 2, 8 });
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdFormatDec(Grd3, new int[] {  }, 0);
            Sm.GrdColButton(Grd3, new int[] {  });
            Sm.GrdColInvisible(Grd3, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtDocNo, MeeCancelReason, LueFinancialInstitution, TxtStatus, TxtSalesAmount, TxtTax,
                        DteDocDt2, LueFinancialInstitution, MeeRemark, DteDocDt3, TxtCurCode, TxtRate, TxtTotalExpenses,
                        TxtTotalCost, TxtDocNoVoucher, ChkCancelInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    LueType.Visible = false;
                    BtnRefreshData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueFinancialInstitution, DteDocDt2, DteDocDt3, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 8, 15 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 2, 5, 6, 7, 8, 11, 14 });
                    TxtRate.Text = "1";
                    TxtStatus.Text = "Outstanding";
                    Sm.FormatNumTxt(TxtRate, 0);
                    Sm.FormatNumTxt(TxtSalesAmount, 0);
                    Sm.FormatNumTxt(TxtTotalExpenses, 0);
                    Sm.FormatNumTxt(TxtTax, 0);
                    Sm.FormatNumTxt(TxtTotalCost, 0);
                    BtnRefreshData.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    //Sm.GrdColReadOnly(false, true, Grd1, new int[] {  });
                    MeeCancelReason.Focus();
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueFinancialInstitution, DteDocDt2, DteDocDt3, TxtStatus,
                TxtCurCode, TxtRate, TxtSalesAmount, TxtTotalExpenses, TxtTax, TxtTotalCost, MeeRemark, TxtDocNoVoucher
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            //Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 8 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 9, 10 });

            Sm.ClearGrd(Grd3, true);
        }

        #endregion

        #region Button Method

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteDocDt2);
                SetSettlementDate(Sm.GetDte(DteDocDt2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesEquitySecuritiesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) ||
                ChkCancelInd.Checked) return;
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesEquitySecurities", "TblSalesEquitySecuritiesHdr");

            cml.Add(SaveSalesEquitySecurities(DocNo));
            cml.Add(SaveSalesEquitySecuritiesDtl2(DocNo));
            cml.Add(SaveStock(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueFinancialInstitution, "Financial Instition") ||
                IsGrdValueNotValid() ||
                IsStockQtyEmpty();
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Investment is null")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Investment type is null")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 8) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Document Type is null")) return true;
            }

            return false;
        }
        
        private bool IsStockQtyEmpty()
        {
            string Msg = string.Empty;
            decimal StockQty = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                string BankAcCode = Sm.GetGrdStr(Grd1, Row, 6);
                string InvestmentCode = Sm.GetGrdStr(Grd1, Row, 23);
                string Qty = Sm.GetValue("SELECT Qty " +
                    "FROM tblinvestmentstocksummary " +
                    "WHERE BankAcCode = '" + BankAcCode + "' " +
                    "AND InvestmentCode = '" + InvestmentCode + "' ");
                StockQty = Convert.ToDecimal(Qty);
                if (Sm.GetGrdDec(Grd1, Row, 8) > StockQty)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity " + Sm.GetGrdDec(Grd1, Row, 8) + " is more than Stock Quantity " + StockQty + ".");
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveSalesEquitySecurities(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();

            //Hdr
            SQL.AppendLine("Insert Into TblSalesEquitySecuritiesHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, SekuritasCode, TradeDt, SettlementDt, Status, CurCode, Rate, SalesAmt, ");
            SQL.AppendLine("TotalExpenses, TaxAmt, TotalCost, Remark, VoucherDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @SekuritasCode, @TradeDt, @SettlementDt, @Status, @CurCode, @Rate, ");
            SQL.AppendLine("@SalesAmt, @TotalExpenses, @TaxAmt, @TotalCost, @Remark, @VoucherDocNo, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            //Doc Approval
            //SQL.AppendLine("INSERT INTO tbldocapproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("SELECT T.DocType, @DocNo, '001', T.DNo, @Usercode, CurrentDateTime() ");
            //SQL.AppendLine("FROM tbldocapprovalsetting T ");
            //SQL.AppendLine("WHERE DocType = ''; ");
            //SQL.AppendLine("");

            //Dtl
            SQLDtl.AppendLine("Insert Into tblsalesequitysecuritiesdtl ");
            SQLDtl.AppendLine("(DocNo, DNo, Status, InvestmentCode, InvestmentType, BankAcCode, Qty, MovingAvgPrice, MovingAvgCost, RecordedMarketPrice, RecordedMarket, ");
            SQLDtl.AppendLine("RecordedUnrealized, SellPrice, SalesAmt, RealizedGainLoss, AccumulationAcquisitionQty, AccumulationAcquisitionCost, Source, ");
            SQLDtl.AppendLine("InvestmentEquityCode, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", 'O', @InvestmentCode_" + r.ToString() + ", @InvestmentType_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@BankAcCode_" + r.ToString() + ", @Qty_" + r.ToString() + ", @MovingAvgPrice_" + r.ToString() + ", @MovingAvgCost_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@RecordedMarketPrice_" + r.ToString() + ", @RecordedMarket_" + r.ToString() + ", @RecordedUnrealized_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@SellPrice_" + r.ToString() + ", @SalesAmt_" + r.ToString() + ", @RealizedGainLoss_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@AccumulationAcquisitionQty_" + r.ToString() + ", @AccumulationAcquisitionCost_" + r.ToString() + ", ");
                SQLDtl.AppendLine("CONCAT(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() + "), @InvestmentEquityCode_" + r.ToString() + ", ");
                SQLDtl.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                Sm.CmParam<Decimal>(ref cm, "@MovingAvgPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                Sm.CmParam<Decimal>(ref cm, "@MovingAvgCost_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                Sm.CmParam<Decimal>(ref cm, "@RecordedMarketPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
                Sm.CmParam<Decimal>(ref cm, "@RecordedMarket_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                Sm.CmParam<Decimal>(ref cm, "@RecordedUnrealized_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
                Sm.CmParam<Decimal>(ref cm, "@SellPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                Sm.CmParam<Decimal>(ref cm, "@SalesAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
                Sm.CmParam<Decimal>(ref cm, "@RealizedGainLoss_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                Sm.CmParam<Decimal>(ref cm, "@AccumulationAcquisitionQty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                Sm.CmParam<Decimal>(ref cm, "@AccumulationAcquisitionCost_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                Sm.CmParam<String>(ref cm, "@InvestmentEquityCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 23));
            }
            SQLDtl.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SekuritasCode", Sm.GetLue(LueFinancialInstitution));
            Sm.CmParamDt(ref cm, "@TradeDt", Sm.GetDte(DteDocDt2));
            Sm.CmParamDt(ref cm, "@SettlementDt", Sm.GetDte(DteDocDt3));
            Sm.CmParam<String>(ref cm, "@Status", TxtStatus.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Rate", decimal.Parse(TxtRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@SalesAmt", decimal.Parse(TxtSalesAmount.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalExpenses", decimal.Parse(TxtTotalExpenses.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCost", decimal.Parse(TxtTotalCost.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesEquitySecuritiesDtl2(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //Dtl2
            SQL.AppendLine("Insert Into tblsalesequitysecuritiesdtl2 ");
            SQL.AppendLine("(DocNo, DNo, ExpensesCode, ExpensesDNo, ExpensesDocType, AcNo, TaxInd, DocDt, DocRef, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ExpensesCode_" + r.ToString() + ", @ExpensesDNo_" + r.ToString() + ", ");
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                    SQL.AppendLine("Null, Null, Null, ");
                else
                    SQL.AppendLine("@ExpensesDocType_" + r.ToString() + ", @AcNo_" + r.ToString() + ", @TaxInd_" + r.ToString() + ", ");
                SQL.AppendLine("@DocDt_" + r.ToString() + ", @DocRef_" + r.ToString() + ", @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@ExpensesCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                Sm.CmParam<String>(ref cm, "@ExpensesDNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 12));
                Sm.CmParam<String>(ref cm, "@ExpensesDocType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                Sm.CmParam<String>(ref cm, "@TaxInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 11) ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@DocRef_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                Sm.CmParamDt(ref cm, "@DocDt_" + r.ToString(), Sm.GetGrdDate(Grd2, r, 6));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 7));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 8));
            }
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* SalesEquitySecurities - Stock */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, InvestmentType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, Source, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, B.InvestmentType, A.DocNo, B.DNo, A.DocDt, NULL, B.BankAcCode, '-', '-', B.InvestmentEquityCode, '-', B.Source, -1*B.Qty, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From TblSalesEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblSalesEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */ ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N'; ");

            SQL.AppendLine("Insert Into TblInvestmentStockSummary (BankAcCode, Lot, Bin, Source, InvestmentCode, BatchNo, PropCode, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.BankAcCode, '-', '-', B.Source, B.InvestmentEquityCode, '-', '-', -1*B.Qty, A.Remark, @UserCode, @Dt ");
            SQL.AppendLine("From TblSalesEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblSalesEquitySecuritiesDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo;");

            SQL.AppendLine("Insert Into TblInvestmentStockPrice(Source, InvestmentCode, BatchNo, PropCode, CurCode, UPrice, BasePrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.Source, B.InvestmentEquityCode, '-', '-', A.CurCode, @UPrice, @BasePrice, A.Rate, A.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblSalesEquitySecuritiesDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            //SQL.AppendLine("Update TblInvestmentStockSummary As T1 ");
            //SQL.AppendLine("INNER JOIN ( ");
            //SQL.AppendLine("    Select A.BankAcCode, A.Lot, A.Bin, A.Source, A.InvestmentCode, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty ");
            //SQL.AppendLine("    From TblInvestmentStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct BankAcCode, Lot, Bin, Source, InvestmentCode ");
            //SQL.AppendLine("        From TblInvestmentStockMovement ");
            //SQL.AppendLine("        Where DocType=@DocType ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='N' ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("    On A.BankAcCode=B.BankAcCode And A.InvestmentCode=B.InvestmentCode ");
            //SQL.AppendLine("    Where (A.Qty<>0) ");
            //SQL.AppendLine("    Group By A.BankAcCode, A.InvestmentCode ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.BankAcCode=T2.BankAcCode ");
            //SQL.AppendLine("    And T1.InvestmentCode=T2.InvestmentCode ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=@Dt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, 0, 24));
            Sm.CmParam<Decimal>(ref cm, "@BasePrice", Sm.GetGrdDec(Grd1, 0, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesEquitySecurities());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false);
        }

        private MySqlCommand EditSalesEquitySecurities()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update Tblsalesequitysecuritieshdr Set ");
            SQL.AppendLine("    CancelInd = @CancelInd, CancelReason = @CancelReason, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblInvestmentStockMovement(DocType, InvestmentType, DocNo, DNo, DocDt, WhsCode, BankAcCode, Lot, Bin, InvestmentCode, BatchNo, Source, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, B.InvestmentType, A.DocNo, B.DNo, A.DocDt, NULL, B.BankAcCode, '-', '-', B.InvestmentEquityCode, '-', B.Source, B.Qty, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesEquitySecuritiesHdr A ");
            SQL.AppendLine("Inner Join TblSalesEquitySecuritiesDtl B On A.DocNo=B.DocNo /*And B.Status='A' */ ");
            SQL.AppendLine("Where A.DocNo=@DocNo AND A.CancelInd = 'Y'; ");

            SQL.AppendLine("Update TblInvestmentStockSummary A ");
            SQL.AppendLine("Inner Join TblSalesEquitySecuritiesDtl B On A.Source = B.Source And A.InvestmentCode = B.InvestmentEquityCode ");
            SQL.AppendLine("Set A.Qty=0, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo; ");

            //SQL.AppendLine("Update TblInvestmentStockSummary As T1 ");
            //SQL.AppendLine("INNER JOIN ( ");
            //SQL.AppendLine("    Select A.BankAcCode, A.Lot, A.Bin, A.Source, A.InvestmentCode, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty ");
            //SQL.AppendLine("    From TblInvestmentStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct BankAcCode, Lot, Bin, Source, InvestmentCode ");
            //SQL.AppendLine("        From TblInvestmentStockMovement ");
            //SQL.AppendLine("        Where DocType=@DocType ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='N' ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("    On A.BankAcCode=B.BankAcCode And A.InvestmentCode=B.InvestmentCode ");
            //SQL.AppendLine("    Where (A.Qty<>0) ");
            //SQL.AppendLine("    Group By A.BankAcCode, A.InvestmentCode ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.BankAcCode=T2.BankAcCode ");
            //SQL.AppendLine("    And T1.InvestmentCode=T2.InvestmentCode ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, B.SekuritasCode, A.TradeDt, A.SettlementDt, A.Status, ");
                SQL.AppendLine("A.CurCode, A.Rate, A.SalesAmt, A.TotalExpenses, A.TaxAmt, A.TotalCost, A.Remark, A.VoucherDocNo ");
                SQL.AppendLine("FROM tblsalesequitysecuritieshdr A ");
                SQL.AppendLine("INNER JOIN tblinvestmentsekuritas B ON A.SekuritasCode = B.SekuritasCode ");
                SQL.AppendLine("Where DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "CancelReason", "SekuritasCode", "TradeDt", 

                            //6-10
                            "SettlementDt", "Status", "CurCode", "Rate", "SalesAmt", 

                            //11-15
                            "TotalExpenses", "TaxAmt", "TotalCost", "Remark", "VoucherDocNo",

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            MeeCancelReason.Text = Sm.DrStr(dr, c[3]);
                            //SetLueFinancialInstitutionCode(ref LueFinancialInstitution, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueFinancialInstitution, Sm.DrStr(dr, c[4]));
                            Sm.SetDte(DteDocDt2, Sm.DrStr(dr, c[5]));
                            Sm.SetDte(DteDocDt3, Sm.DrStr(dr, c[6]));
                            TxtStatus.Text = Sm.DrStr(dr, c[7]);
                            TxtCurCode.Text = Sm.DrStr(dr, c[8]);
                            TxtRate.Text = Sm.DrStr(dr, c[9]);
                            TxtSalesAmount.Text = Sm.DrStr(dr, c[10]);
                            TxtTotalExpenses.Text = Sm.DrStr(dr, c[11]);
                            TxtTax.Text = Sm.DrStr(dr, c[12]);
                            TxtTotalCost.Text = Sm.DrStr(dr, c[13]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                            TxtDocNoVoucher.Text = Sm.DrStr(dr, c[15]);
                        }, true
                    );
                Sm.FormatNumTxt(TxtRate, 0);
                Sm.FormatNumTxt(TxtSalesAmount, 0);
                Sm.FormatNumTxt(TxtTotalExpenses, 0);
                Sm.FormatNumTxt(TxtTax, 0);
                Sm.FormatNumTxt(TxtTotalCost, 0);
                ShowAcquisitionEquitySecuritiesDtl(DocNo);
                ShowAcquisitionEquitySecuritiesDtl2(DocNo);
                //ComputeInvestmentCost();
                //ComputeTotalExpenses();
                //ComputeTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAcquisitionEquitySecuritiesDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT B.PortofolioId InvestmentCode, C.PortofolioName InvestmentName, D.OptCode, D.OptDesc InvestmentType, E.InvestmentCtName, ");
            SQL.AppendLine("A.BankAcCode, F.BankAcNm, A.Qty, B.UomCode, A.MovingAvgPrice, A.MovingAvgCost, A.RecordedMarketPrice, A.RecordedMarket, ");
            SQL.AppendLine("A.RecordedUnrealized, A.SellPrice, A.SalesAmt, A.RealizedGainLoss, C.InvestmentCtCode, A.AccumulationAcquisitionQty, A.AccumulationAcquisitionCost, A.Source ");
            SQL.AppendLine("FROM tblsalesequitysecuritiesdtl A ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemequity B ON A.InvestmentEquityCode = B.InvestmentEquityCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio C ON B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.InvestmentType = D.OptCode AND D.OptCat = 'InvestmentType' ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory E ON C.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tblbankaccount F ON A.BankAcCode = F.BankAcCode");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] {
                    //0
                    "InvestmentCode", 
                    
                    //1-5
                    "InvestmentName", "OptCode", "InvestmentType", "InvestmentCtName", "BankAcCode", 
                    
                    //6-10
                    "BankAcNm", "Qty", "UomCode", "MovingAvgPrice", "MovingAvgCost", 
                    
                    //11-15
                    "RecordedMarketPrice", "RecordedMarket", "RecordedUnrealized", "SellPrice", "SalesAmt",

                    //16-20
                    "RealizedGainLoss", "InvestmentCtCode", "AccumulationAcquisitionQty", "AccumulationAcquisitionCost", "Source"
                },
                (MySqlDataReader dr, iGrid Grd1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 20);
                }, false, false, true, false
            );
            TxtSalesAmount.Text = Sm.GetGrdStr(Grd1, 0, 16);
            Sm.FormatNumTxt(TxtSalesAmount, 0);
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowAcquisitionEquitySecuritiesDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ExpensesCode, IfNull(B.DocType, A.ExpensesDocType) As DocType, IfNull(B.AcNo, A.AcNo) As AcNo, ");
            SQL.AppendLine("IfNull(C.AcDesc, D.AcDesc) As AcDesc, A.DocRef, A.DocDt, A.Amt, A.Remark, B.Rate, B.Amt As Amount, IfNull(B.TaxInd, A.TaxInd) As TaxInd, ");
            SQL.AppendLine("A.ExpensesDNo, B.Formula ");
            SQL.AppendLine("From TblSalesEquitySecuritiesDtl2 A ");
            SQL.AppendLine("Left Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("	And A.ExpensesDNo = B.DNo ");
            SQL.AppendLine("Left Join TblCoa C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] {
                    "ExpensesCode",

                    "DocType", "AcNo", "AcDesc", "DocRef", "DocDt",

                    "Amt", "Remark", "Rate", "Amount", "TaxInd",

                    "ExpensesDNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grd2, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            //Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            //Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void ComputeStock()
        {
            string Dt = string.Empty, InvestCtCode = string.Empty, InvestCode = string.Empty;

            InvestCode = Sm.GetGrdStr(Grd1, 0, 23);
            InvestCtCode = Sm.GetGrdStr(Grd1, 0, 18);
            Dt = Sm.GetDte(DteDocDt2);

            ComputeStockAccumulaton(Dt, InvestCtCode, InvestCode, ref Grd1, 21, 23, 18, 4, 19, 20, 10, 11, 24);
            Grd1.Cells[0, 11].Value = 0m;

        }

        internal void ComputeRecordedMarket()
        {
            decimal RecordedMarketPrice = 0m, Qty = 0m;

            Qty = Sm.GetGrdDec(Grd1, 0, 8);
            RecordedMarketPrice = Sm.GetGrdDec(Grd1, 0, 12);

            Grd1.Cells[0, 13].Value = Qty * RecordedMarketPrice;

            ComputeRecordedUnreal();
        }

        internal void ComputeRecordedUnreal()
        {
            decimal RecordedMarket = 0m, MovAvgCost = 0m;

            RecordedMarket = Sm.GetGrdDec(Grd1, 0, 13);
            MovAvgCost = Sm.GetGrdDec(Grd1, 0, 11);

            Grd1.Cells[0, 14].Value = RecordedMarket - MovAvgCost;
        }

        internal void ComputeRealGainLoss()
        {
            decimal SalesAmt = 0m, MovAvgCost = 0m;

            SalesAmt = Sm.GetGrdDec(Grd1, 0, 16);
            MovAvgCost = Sm.GetGrdDec(Grd1, 0, 11);

            Grd1.Cells[0, 17].Value = SalesAmt - MovAvgCost;
        }

        internal void ComputeSalesAmount()
        {
            decimal Qty = 0m, SellPrice = 0m, MovAvgPrice = 0m;
            //string Dt = string.Empty, InvestCtCode = string.Empty, InvestCode = string.Empty;

            Qty = Sm.GetGrdDec(Grd1, 0, 8);
            SellPrice = Sm.GetGrdDec(Grd1, 0, 15);
            MovAvgPrice = Sm.GetGrdDec(Grd1, 0, 10);

            //InvestCode = Sm.GetGrdStr(Grd1, 0, 23);
            //InvestCtCode = Sm.GetGrdStr(Grd1, 0, 18);
            //Dt = Sm.GetDte(DteDocDt);

            Grd1.Cells[0, 16].Value = Qty * SellPrice;
            TxtSalesAmount.Text = Convert.ToString(Qty * SellPrice);
            Sm.FormatNumTxt(TxtSalesAmount, 0);
            Grd1.Cells[0, 11].Value = Qty * MovAvgPrice;

            ComputeAmtDtl2();
            ComputeRealGainLoss();
        }

        internal void SetLueFinancialInstitutionCode(ref DXE.LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SekuritasCode Col1, SekuritasName Col2 ");
                SQL.AppendLine("From TblInvestmentSekuritas ");
                if (Code.Length > 0)
                    SQL.AppendLine("Where SekuritasCode = @Code; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSettlementDate(string TradeDt)
        {
            String mTradeDt = Sm.Left(TradeDt, 8);
            String mSettlementDt = Sm.ConvertDate(mTradeDt).AddDays(2).ToString("yyyyMMdd");
            Sm.SetDte(DteDocDt3, mSettlementDt);
        }

        internal void ComputeTotalExpenses()
        {
            decimal TotalExpenses = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (!Sm.GetGrdBool(Grd2, Row, 11))
                    TotalExpenses += Sm.GetGrdDec(Grd2, Row, 7);
            }
            TxtTotalExpenses.Text = Convert.ToString(TotalExpenses);
            Sm.FormatNumTxt(TxtTotalExpenses, 0);
            if (BtnSave.Enabled)
                ComputeTotalCost();

            //ComputeAcquisitionPrice();
        }

        internal void ComputeTax()
        {
            decimal Tax = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd2, Row, 11))
                    Tax += Sm.GetGrdDec(Grd2, Row, 7);
            }
            TxtTax.Text = Convert.ToString(Tax);
            Sm.FormatNumTxt(TxtTax, 0);
            ComputeTotalCost();
            //ComputeAcquisitionPrice();
        }

        internal void ComputeTotalCost()
        {
            decimal SalesAmt = 0m,
            TotalExpenses = 0m,
            Tax = 0m;

            if (TxtSalesAmount.Text != null)
            {
                SalesAmt = Convert.ToDecimal(TxtSalesAmount.Text);
            }
            if (TxtTotalExpenses.Text != null)
            {
                TotalExpenses = Convert.ToDecimal(TxtTotalExpenses.Text);
            }
            if (TxtTax.Text != null)
            {
                Tax = Convert.ToDecimal(TxtTax.Text);
            }

            decimal TotalCost = SalesAmt + TotalExpenses + Tax;

            TxtTotalCost.Text = Convert.ToString(TotalCost);
            Sm.FormatNumTxt(TxtTotalCost, 0);
            //Grd1.Cells[0, 11].Value = TotalCost;

            //ComputeAcquisitionPrice();
        }

        internal void ComputeAmtDtl2()
        {
            decimal Rate = 0m, Amt = 0m;

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    Rate = Sm.GetGrdDec(Grd2, r, 9);
                    Amt = Sm.GetGrdDec(Grd2, r, 10);
                    if (Rate == 0)
                        Grd2.Cells[r, 7].Value = Amt;
                    else
                        Grd2.Cells[r, 7].Value = (Rate / 100) * Sm.GetGrdDec(Grd1, 0, 16);
                }
            }

            ComputeTotalExpenses();
            ComputeTax();
        }

        internal void ComputeAmtDtl2(List<String> ExpensesCode)
        {
            decimal Rate = 0m, Amt = 0m;

            if (ExpensesCode.Count > 0)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    for (int i = 0; i < ExpensesCode.Count; i++)
                    {
                        if (Sm.GetGrdStr(Grd2, r, 1) + Sm.GetGrdStr(Grd2, r, 12) == ExpensesCode[i])
                        {
                            Rate = Sm.GetGrdDec(Grd2, r, 9);
                            Amt = Sm.GetGrdDec(Grd2, r, 10);

                            if (Rate == 0)
                                Grd2.Cells[r, 7].Value = Amt;
                            if (Amt == 0)
                                Grd2.Cells[r, 7].Value = (Rate / 100) * Sm.GetGrdDec(Grd1, 0, 16);
                        }
                    }
                }
            }

            ComputeTotalExpenses();
            ComputeTax();
        }

        internal static void ComputeStockAccumulaton(
            string DocDt, string InvestmentCtCode, string InvestmentCode,
            ref iGrid Grd, int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost, int ColBasePrice
            )
        {
            var lacc = PrepDataStockAccumulation(DocDt, InvestmentCtCode, InvestmentCode);
            ProcessAccumulationStock(ref Grd, ref lacc, ColCreateDt, ColInvCode, ColInvCtCode, ColInvType, ColAccQty, ColAccAcqCost, ColMovingAvgPrice, ColMovingAvgCost, ColBasePrice);
        }

        internal static List<StockAccumulation> PrepDataStockAccumulation(string DocDt, string InvestmentCtCode, string InvestmentCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockAccumulation>();
            var l2 = new List<StockAccumulation>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, InvestmentCode, new string[] { "A.InvestmentCode", "C.PortofolioName" });

            SQL.AppendLine("Select A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType, E.OptDesc,  ");
            SQL.AppendLine("IfNull(A.Qty, 0.00) As Qty, IfNull(A.Qty, 0.00) * IfNull(D.UPrice, 0.00) AcquisitionCost,  ");
            SQL.AppendLine("IfNull(D.BasePrice, 0.00) BasePrice");
            SQL.AppendLine("From TblInvestmentStockMovement A  ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode  ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source = D.Source ");
            SQL.AppendLine("	And A.InvestmentCode = D.InvestmentCode ");
            SQL.AppendLine("Inner Join TblOption E On A.InvestmentType = E.OptCode And E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            if (DocDt.Length > 0) SQL.AppendLine("And A.DocDt <= @DocDt  ");
            SQL.AppendLine("And C.InvestmentCtCode = @InvestmentCtCode ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("	Select Distinct(X1.DocNo) ");
            SQL.AppendLine("	From TblInvestmentStockMovement X1 ");
            SQL.AppendLine("	Where 1=1 ");
            if (DocDt.Length > 0) SQL.AppendLine("	And X1.DocDt <= @DocDt ");
            SQL.AppendLine("	Group By X1.DocType, X1.InvestmentType, X1.DocNo, X1.DNo, X1.DocDt, X1.BankAcCode, X1.Lot, X1.BatchNo, X1.Source   ");
            SQL.AppendLine("	Having Sum(X1.Qty)=0.00 ");
            SQL.AppendLine(") ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter + "Order By A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType;",
                new string[]
                {
                    "CreateDt",
                    "InvestmentCode", "InvestmentCtCode", "InvestmentType", "OptDesc", "Qty",
                    "AcquisitionCost", "BasePrice"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockAccumulation()
                        {
                            CreateDt = Convert.ToDecimal(Sm.DrStr(dr, 0)),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentCtCode = Sm.DrStr(dr, 2),
                            InvestmentType = Sm.DrStr(dr, 3),
                            InvestmentTypeName = Sm.DrStr(dr, 4),
                            Qty = Sm.DrDec(dr, 5),
                            AcquisitionCost = Sm.DrDec(dr, 6),
                            BasePrice = Sm.DrDec(dr, 7),
                        }
                    );
               }, false
            );

            var lg = l.GroupBy(g => new { CreateDt = g.CreateDt, InvCode = g.InvestmentCode, InvCtCode = g.InvestmentCtCode, InvType = g.InvestmentType, InvTypeNm = g.InvestmentTypeName })
                .Select(group => new
                {
                    CreateDt = group.Key.CreateDt,
                    InvestmentCode = group.Key.InvCode,
                    InvestmentCtCode = group.Key.InvCtCode,
                    InvestmentType = group.Key.InvType,
                    InvestmentTypeName = group.Key.InvTypeNm,
                });

            foreach (var x in lg)
            {
                decimal mQty = 0m, mAcquisitionPrice = 0m, mBasePrice = 0m;
                foreach (var y in l)
                {
                    if (y.CreateDt <= x.CreateDt &&
                        y.InvestmentCode == x.InvestmentCode &&
                        y.InvestmentCtCode == x.InvestmentCtCode &&
                        y.InvestmentType == x.InvestmentType)
                    {
                        mQty += y.Qty;
                        mAcquisitionPrice += y.AcquisitionCost;
                    }
                }
                l2.Add(
                    new StockAccumulation()
                    {
                        CreateDt2 = x.CreateDt.ToString(),
                        InvestmentCode = x.InvestmentCode,
                        InvestmentCtCode = x.InvestmentCtCode,
                        InvestmentType = x.InvestmentType,
                        InvestmentTypeName = x.InvestmentTypeName,
                        Qty = mQty,
                        AcquisitionCost = mAcquisitionPrice,
                    }
                );
            }

            return l2;
        }

        private static void ProcessAccumulationStock(
            ref iGrid Grd, ref List<StockAccumulation> lacc,
            int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColAccQty, int ColAccAcqCost, int ColMovingAvgPrice, int ColMovingAvgCost, int ColBasePrice
            )
        {
            foreach (var x in lacc)
            {
                for (int row = 0; row < Grd.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(Grd, row, 1).Length > 0 &&
                        Sm.GetGrdStr(Grd, row, ColCreateDt) == x.CreateDt2 &&
                        Sm.GetGrdStr(Grd, row, ColInvCode) == x.InvestmentCode &&
                        Sm.GetGrdStr(Grd, row, ColInvCtCode) == x.InvestmentCtCode &&
                        Sm.GetGrdStr(Grd, row, ColInvType) == x.InvestmentTypeName)
                    {
                        Grd.Cells[row, ColAccQty].Value = x.Qty;
                        Grd.Cells[row, ColAccAcqCost].Value = x.AcquisitionCost;
                        Grd.Cells[row, ColMovingAvgPrice].Value = x.AcquisitionCost / x.Qty;
                        Grd.Cells[row, ColMovingAvgCost].Value = x.Qty * (x.AcquisitionCost / x.Qty);
                        Grd.Cells[row, ColBasePrice].Value = x.AcquisitionCost / x.Qty;
                    }
                }
            }
        }

        private void ShowExpensesData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ExpensesCode, B.DocType, B.AcNo, C.AcDesc, B.Rate, B.Amt, B.TaxInd, B.Formula, B.DNo ");
            SQL.AppendLine("From TblExpensesTypeHdr A ");
            SQL.AppendLine("Inner Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.TransactionCode = @TransactionCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TransactionCode", mMenuCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {
                        //0
                        "ExpensesCode",

                        //1-5
                        "DocType", "AcNo", "AcDesc", "Rate",  "Amt", 

                        //6-8
                        "TaxInd", "DNo", "Formula"
                },
                (MySqlDataReader dr, iGrid Grid1, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 5);
                    Sm.SetGrdValue("B", Grd2, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 13, 8);
                    Grd2.Cells[Row, 6].Value = DteDocDt2.Text;
                    Grd2.Cells[Row, 7].Value = 0m;
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd2, Grd2.Rows.Count - 1, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 9, 10 });
            IsExpensesCodeExists();
            ComputeAmtDtl2();
        }

        internal void IsExpensesCodeExists()
        {
            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length == 0)
                    GrdColsReadOnly(false, true, Grd2, r, new int[] { 0, 2, 11, 14 });
                else
                    GrdColsReadOnly(true, true, Grd2, r, new int[] { 0, 2, 11, 14 });
            }
        }

        private void GrdColsReadOnly(bool ReadOnly, bool IsChangeBackColor, iGrid Grd, int RowIndex, int[] ColIndex)
        {
            if (ReadOnly)
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.FromArgb(224, 224, 224);
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.True;
                }
            }
            else
            {
                for (int i = 0; i < ColIndex.Length; i++)
                {
                    if (IsChangeBackColor) Grd.Cells[RowIndex, ColIndex[i]].BackColor = Color.White;
                    Grd.Cells[RowIndex, ColIndex[i]].ReadOnly = iGBool.False;
                }
            }
        }

        internal string GetSelectedExpenses()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            '#' +
                            Sm.GetGrdStr(Grd2, Row, 12) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (e.ColIndex == 4) Sm.LueRequestEdit(ref Grd1, ref LueType, ref fCell, ref fAccept, e);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7, 9, 10, 11, 12 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] {  }, e);

                if (e.ColIndex == 8 || e.ColIndex == 15)
                    ComputeSalesAmount();

                //test Recorded Market Price manual (editable)
                //if (e.ColIndex == 12)
                //    ComputeRecordedMarket();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmSalesEquitySecuritiesDlg(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
            ComputeTotalExpenses();
            ComputeTax();
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 7, 9, 10 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { }, e);
                if (Grd2.Rows.Count > 1)
                {
                    ComputeTotalExpenses();
                    ComputeTax();
                }
                //if (Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length > 0)
                //    ComputeAmtDtl2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.GrdRequestEdit(Grd2, e.RowIndex);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 9, 10 });
            if (e.ColIndex == 6) Sm.DteRequestEdit(Grd2, DteUsageDt, ref fCell, ref fAccept, e);
            if (Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 8 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                IsExpensesCodeExists();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsGrdValueEmpty(Grd1, 0, 1, false, "Investment Name"))
            {
                if (e.ColIndex == 0)
                    Sm.FormShowDialog(new FrmSalesEquitySecuritiesDlg2(this, e.RowIndex));
                if (e.ColIndex == 14)
                    Sm.FormShowDialog(new FrmSalesEquitySecuritiesDlg3(this));
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFinancialInstitution_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFinancialInstitution, new Sm.RefreshLue2(SetLueFinancialInstitutionCode), string.Empty);
        }

        private void TxtCurCode_EditValueChanged(object sender, EventArgs e)
        {
            var Rate = string.Empty;
            if (TxtCurCode.Text.Length > 0)
                if (TxtCurCode.Text == "IDR")
                    TxtRate.Text = "1";
                else
                {
                    Rate = Sm.GetValue("SELECT Amt " +
                        "FROM tblcurrencyrate WHERE CurCode1 = '" + TxtCurCode.Text + "' AND CurCode2 = 'IDR' " +
                        "AND RateDt = (SELECT MAX(RateDt) FROM tblcurrencyrate) ");
                    TxtRate.Text = Rate;
                }
                Sm.FormatNumTxt(TxtRate, 0);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "InvestmentType");
        }

        private void LueType_Validated(object sender, EventArgs e)
        {
            LueType.Visible = false;
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (LueType.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueType).Trim();
                    Grd1.Cells[fCell.RowIndex, 4].Value = LueType.GetColumnValue("Col2");
                }
            }
        }

        private void TxtInvestmentCost_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length > 0)
            {
                Sm.ClearGrd(Grd1, true);
                Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20 });
                SetSettlementDate(Sm.GetDte(DteDocDt2));
            }
        }

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 &&
                !Sm.IsGrdValueEmpty(Grd1, 0, 1, false, "You have to choose Investment Item first.") &&
                !Sm.IsGrdValueEmpty(Grd1, 0, 16, true, "You have to fill in the Sales Amount first."))
            {
                ShowExpensesData();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class StockAccumulation
        {
            public decimal CreateDt { get; set; }
            public string CreateDt2 { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentType { get; set; }
            public string InvestmentTypeName { get; set; }
            public decimal Qty { get; set; }
            public decimal AcquisitionCost { get; set; }
            public decimal BasePrice { get; set; }
        }

        #endregion
    }
}
