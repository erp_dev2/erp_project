﻿#region Update
/* 
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPR : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmPPRFind FrmFind;
        string mPPRDeptCode = string.Empty, mPPRBankAcCode = string.Empty, mPPRPaymentType = string.Empty;

        #endregion

        #region Constructor

        public FrmPPR(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Payrun";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        
                        //6-10
                        "Document#",
                        "DNo",
                        "Type",
                        "Type",
                        "Amount",

                        //11
                        "ProcessInd"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 100, 200, 180, 200, 

                        //6-10
                        130, 0, 0, 150, 100,

                        //11
                        0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 7, 8, 11 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        "Amount",
                        
                        //6-9
                        "Voucher Request#",
                        "",
                        "Voucher#",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        100, 200, 180, 200, 100,   
                        
                        //6-9
                        130, 20, 130, 20
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 7, 9 });
            Sm.GrdFormatDec(Grd2, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 8 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10 });
            ClearGrd2();
        }

        private void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPPRFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PPR", "TblPPRHdr");
            SetVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SavePPRHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePPRDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SavePPRDtl2(DocNo, Row));

            cml.Add(UpdateProcessInfo(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsProductionWagesAlreadyCancelled() ||
                IsProductionPenaltyAlreadyCancelled() ||
                IsDataAlreadyFulfilled();
        }

        private bool IsDataAlreadyFulfilled()
        {
            SetProcessInd();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    !Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 11), "O")
                    )
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Position : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Department : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "Document# : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Type : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                        "Invalid document."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsProductionWagesAlreadyCancelled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblProductionWagesHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;", 
                GetSelectedDocNo2(), 
                ref DocNo,
                "Document# (Wages) : " + DocNo + Environment.NewLine +
                "This document already cancelled.");
        }

        private bool IsProductionPenaltyAlreadyCancelled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblProductionPenaltyHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;",
                GetSelectedDocNo2(),
                ref DocNo,
                "Document# (Penalty) : " + DocNo + Environment.NewLine +
                "This document already cancelled.");
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Employee is empty.")) return true;
            return false;
        }

        private MySqlCommand SavePPRHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPPRHdr(DocNo, DocDt, CancelInd, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePPRDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPRDtl(DocNo, DNo, EmpCode, WPDocType, WPDocNo, WPDNo, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @WPDocType, @WPDocNo, @WPDNo, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@WPDocType", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@WPDocNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@WPDNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePPRDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPRDtl2(DocNo, DNo, EmpCode, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @VoucherRequestDocNo, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'A', @DeptCode, 'P', Null, 'C', @BankAcCode, @PaymentType, Null, Null, Null, @CreateBy, 1, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='MainCurCode'), @Amt, Null, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Description, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@Description", "Upah " + Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mPPRDeptCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mPPRBankAcCode);
            Sm.CmParam<String>(ref cm, "@PaymentType", mPPRPaymentType);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateProcessInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionWagesDtl3 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct T2.WPDocNo, T2.WPDNo ");
            SQL.AppendLine("    From TblPPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPRDtl T2 On T1.DocNo=T2.DocNo And T2.WPDocType='1' ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.CancelInd=@CancelInd ");
            SQL.AppendLine(") B On A.DocNo=B.WPDocNo And A.DNo=B.WPDNo ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessInd; ");

            SQL.AppendLine("Update TblProductionWagesDtl4 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct T2.WPDocNo, T2.WPDNo ");
            SQL.AppendLine("    From TblPPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPRDtl T2 On T1.DocNo=T2.DocNo And T2.WPDocType='2' ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.CancelInd=@CancelInd ");
            SQL.AppendLine(") B On A.DocNo=B.WPDocNo And A.DNo=B.WPDNo ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessInd; ");

            SQL.AppendLine("Update TblProductionPenaltyDtl3 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct T2.WPDocNo, T2.WPDNo ");
            SQL.AppendLine("    From TblPPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPRDtl T2 On T1.DocNo=T2.DocNo And T2.WPDocType='3' ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.CancelInd=@CancelInd ");
            SQL.AppendLine(") B On A.DocNo=B.WPDocNo And A.DNo=B.WPDNo ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessInd; ");

            SQL.AppendLine("Update TblProductionPenaltyDtl4 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct T2.WPDocNo, T2.WPDNo ");
            SQL.AppendLine("    From TblPPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPRDtl T2 On T1.DocNo=T2.DocNo And T2.WPDocType='4' ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.CancelInd=@CancelInd ");
            SQL.AppendLine(") B On A.DocNo=B.WPDocNo And A.DNo=B.WPDNo ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessInd; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", ChkCancelInd.Checked?"O":"F");
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mPPRBankAcCode + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }


        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        Type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mPPRBankAcCode + "' ");

        //    var SQL = new StringBuilder();

        //    if (Type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + Type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPPHdr());
            cml.Add(UpdateProcessInfo(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                IsPPRNotCancelled() ||
                IsPPRAlreadyCancelled() ||
                IsVoucherRequestAlreadyProcessed();
        }

        private bool IsPPRNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsPPRAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblPPRHdr " +
                "Where DocNo=@Param And CancelInd='Y' Limit 1;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsVoucherRequestAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherRequestHdr " +
                "Where DocNo In ( "+
                "   Select VoucherRequestDocNo From TblPPRDtl2 " +
                "   Where DocNo=@Param " +
                ") And VoucherDocNo Is Not Null;",
                TxtDocNo.Text,
                "One or more of the following voucher requests already processed to voucher."
                );
        }

        private MySqlCommand EditPPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPPRHdr Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select VoucherRequestDocNo From TblPPRDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPPRHdr(DocNo);
                ShowPPRDtl(DocNo);
                ShowPPRDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPPRHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, Remark From TblPPRHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", "DocDt", "CancelInd", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[2]) == "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowPPRDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.WPDocType, A.WPDocNo, A.WPDNo, A.Amt, ");
            SQL.AppendLine("If(A.WPDocType In ('1', '2'), 'Wages', 'Penalty') As WPDocTypeDesc ");
            SQL.AppendLine("From TblPPRDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "Deptname", "WPDocNo", 
                    
                    //6-9
                    "WPDNo", "WPDocType", "WPDocTypeDesc", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Grd.Cells[Row, 11].Value = null;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPPRDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("E.Amt, A.VoucherRequestDocNo, E.VoucherDocNo ");
            SQL.AppendLine("From TblPPRDtl2 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "Deptname", "Amt", 
                    
                    //6-7
                    "VoucherRequestDocNo", "VoucherDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedDocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedDocNo2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 6) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void SetSummary()
        {
            bool IsFind = false;
            int r = 0;

            ClearGrd2();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                {
                    IsFind = false;
                    for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                    {
                        if (Sm.CompareGrdStr(Grd1, Row, 2, Grd2, Row2, 1))
                        {
                            Grd2.Cells[Row2, 5].Value = Sm.GetGrdDec(Grd2, Row2, 5) + Sm.GetGrdDec(Grd1, Row, 10);
                            IsFind = true;
                            break;
                        }
                    }
                    if (!IsFind)
                    { 
                        r = Grd2.Rows.Count-1;
                        Sm.CopyGrdValue(Grd2, r, 1, Grd1, Row, 2);
                        Sm.CopyGrdValue(Grd2, r, 2, Grd1, Row, 3);
                        Sm.CopyGrdValue(Grd2, r, 3, Grd1, Row, 4);
                        Sm.CopyGrdValue(Grd2, r, 4, Grd1, Row, 5);
                        Sm.CopyGrdValue(Grd2, r, 5, Grd1, Row, 10);
                        Grd2.Rows.Add();
                    }
                }
            }
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5 });
        }

        private void GetParameter()
        {
            mPPRDeptCode = Sm.GetParameter("PPRDeptCode");
            mPPRBankAcCode = Sm.GetParameter("PPRBankACCode");
            mPPRPaymentType = Sm.GetParameter("PPRPaymentType");
        }

        private void SetVoucherRequestDocNo()
        {
            string DocNo = GenerateVoucherRequestDocNo(); //15/01/0001/VR

            if (DocNo.Length > 9)
            {
                int i = 0;
                string SeqNo = DocNo.Substring(6, 4);

                DocNo = DocNo.Replace(SeqNo, "XXXX");

                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                    {
                        Grd2.Cells[Row, 6].Value = DocNo.Replace("XXXX",
                            Sm.Right("0000" + ((int.Parse(SeqNo)) + i).ToString(), 4)
                            );
                        i++;
                    }
                }
            }
        }

        private void SetProcessInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As DocType, DocNo, DNo, ProcessInd ");
            SQL.AppendLine("From TblProductionWagesDtl3 ");
            SQL.AppendLine("Where Locate(Concat('##', '1', DocNo, DNo, '##'), @SelectedDocNo)>0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As DocType, DocNo, DNo, ProcessInd ");
            SQL.AppendLine("From TblProductionWagesDtl4 ");
            SQL.AppendLine("Where Locate(Concat('##', '2', DocNo, DNo, '##'), @SelectedDocNo)>0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As DocType, DocNo, DNo, ProcessInd ");
            SQL.AppendLine("From TblProductionPenaltyDtl3 ");
            SQL.AppendLine("Where Locate(Concat('##', '3', DocNo, DNo, '##'), @SelectedDocNo)>0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '4' As DocType, DocNo, DNo, ProcessInd ");
            SQL.AppendLine("From TblProductionPenaltyDtl4 ");
            SQL.AppendLine("Where Locate(Concat('##', '4', DocNo, DNo, '##'), @SelectedDocNo)>0; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedDocNo", GetSelectedDocNo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocType",
 
                        //1-3
                        "DocNo", "DNo", "ProcessInd"
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 8), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 6), Sm.DrStr(dr, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 2))
                                )
                            {
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 3);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete) SetSummary();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length==0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPPRDlg(this));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length==0 && e.ColIndex == 1) 
                Sm.FormShowDialog(new FrmPPRDlg(this));
        }

        #endregion

        #region Grid 2

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd2, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #endregion
    }
}
