﻿#region Update
/*
    28/11/2022 [MAU/BBT] Menu baru
    23/12/2022 [MAU/BBT] BUG : menampilkan seluruh doctype pada detail property inventory cost component berdasarkan property code
    27/12/2022 [MAU/BBT] FEEDBACK : penyesuaian kesalahan penulisan pada tab property cost component dan tab property transfer ref.
    28/12/2022 [MAU/BBT] BUG : Memperbesar field initial cost center
    29/12/2022 [MAU/BBT] FEEDBACK : field kecamatan dan kelurahan tidak mandatory, Penyesuaian validasi ketika dokumen di cancel dan complete.
    30/12/2022 [DITA/BBT] tambah doc approval
    03/01/2023 [MAU/BBT] Bug : penyesuaian title tab page menjadi rata tengah
    04/01/2023 [MAU/BBT] menambahkan validasi pada field Dasar Pelepasan Hak
    05/01/2023 [DITA/BBT] property inventory cost component yg ditampilkan hanya yg sudah approve saja
    05/01/2023 [MAU/BBT] Feedback 
                            -> menambah validasi mandatory field 'Dasar Pelepasan Hak'
                            -> field dropdown property category dijadikan mandatory
                            -> bisa save data tanpa mengisi field kecamatan dan desa/kelurahan
                            -> penyesuaian kesalahan penulisan pada tab property transfer ref.
    06/01/2023 [IBL/BBT] Tambah showpropertyinventorymutation() 
    10/01/2023 [MAU/BBT] penyelesaian BUG pada tab upload file (tidak bisa upload file), penyesuaian rata tengah pada judul tab Property Transfer Ref.
    10/01/2023 [MAU/BBT] Feedback : property inventory category yang tidak tercentang checkbox initial categorynya tetap dapat ditampilkan ketika show data
    11/01/2023 [MAU/BBT] BUG : duplicate property code 
    11/01/2023 [SET/BBT] Penyesuaian dengan menu Property Inventory Transfer
    11/01/2023 [SET/BBT] penyesuaian btn lup grd3 menu Property Inventory Transfer
    12/01/2023 [IBL/BBT] DocApproval hanya dicantolin ke department aja, ngga dicantolin ke site.
    12/01/2023 [IBL/BBT] Property transfer yang sudah cancel tidak dimunculkan di tab Property Transfer
    12/01/2023 [IBL/BBT] Upload file blm nyantol validasi
    12/01/2023 [IBL/BBT] Clear Grd 3, dan tab certificate information
    13/01/2023 [IBL/BBT] Clear Grd 2 dan Grd6
    13/01/2023 [IBL/BBT] Certificate information tidak muncul
    02/02/2023 [WED/BBT] perubahan label
    14/02/2023 [IBL/BBT] Tambah copy data
    15/02/2023 [SET/BBT] penyesuaian showpropertycostcomponent
    21/03/2023 [VIN/BBT] feedback tambah length kolom
    21/03/2023 [VIN/BBT] BUG show Data
    14/04/2023 [SET/BBT] penyesuain ketika ada trx Multi Cost Component (MCC)
    27/04/2023 [SET/BBT] BtnParent_Click
    02/05/2023 [SET/BBT] BtnPropertyMutationDocNo_Click
    03/05/2023 [SET/BBT] Penyesuaian berdasar transaksi MCC
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mPropertyCode = string.Empty; 
        internal FrmPropertyInventoryFind FrmFind;

        //private bool x ;
        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty,
          mInitialCategory = string.Empty;

        private byte[] downloadedData;

        private bool mIsPropertyInventoryAllowToUploadFile = false;

        #endregion

        #region Constructor

        public FrmPropertyInventory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Property Inventory";
            try
            {
                ExecQuery();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                BtnDelete.Visible = false;
                TcPropertyInventory.TabPages.Remove(TpFairValue);
                SetFormControl(mState.View);

                TcPropertyInventory.SelectedTabPage = TpCertificateInformation;
                AssignCertificateInfoLabel();

                TcPropertyInventory.SelectedTabPage = TpHistoricalInformation;
                Sl.SetLueOption(ref LueAcquisitionType, "AcquisitionType");
                Sl.SetLueProvCode(ref LueProvince);
                Sl.SetLueCityCode(ref LueCity);
                // public static void SetLueCityCode(ref LookUpEdit Lue, string CntCode, string CityCode)

                GetParameter();
               
                SetGrd();
                TcPropertyInventory.SelectedTabPage = TpGeneral;
                if (mPropertyCode.Length != 0)
                {
                    ShowData(mPropertyCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }



        private void BtnInsert_Click(object sender, EventArgs e)
        {
            try
            {

                ClearData();
                SetFormControl(mState.Insert);
                SetLuePropertyCategory(ref LuePropertyCategory, string.Empty);
                DteRegistrationDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                //Sl.SetLueAssetCode(ref TxtParent, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPropertyCode, "", false)) return;
            SetFormControl(mState.Edit);
        }
        override protected void BtnSaveClick(object sender, EventArgs e)
        {            
            try
            {
                if (TxtPropertyCode.Text.Length == 0)
                {
                    ChkActInd.Checked = true;
                    InsertData();
                }                    
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (!BtnSave.Enabled && !Sm.IsTxtEmpty(TxtPropertyCode, "Property Code", false))
            {
                if (
                    TcPropertyInventory.SelectedTabPage != TpPropertyCostComponent && 
                    TcPropertyInventory.SelectedTabPage != TpMutationDocumentRef &&
                    TcPropertyInventory.SelectedTabPage != TpPropertyTransferRef
                    )
                {
                    Sm.StdMsg(mMsgType.Info, "You could only export either Property Cost Component, Mutation Document Ref, or Property Transfer Ref's tab.");
                    return;
                }
                else
                {
                    iGrid Grd = null;

                    if (TcPropertyInventory.SelectedTabPage == TpPropertyCostComponent)
                        Grd = Grd1;

                    if (TcPropertyInventory.SelectedTabPage == TpMutationDocumentRef)
                        Grd = Grd2;

                    if (TcPropertyInventory.SelectedTabPage == TpPropertyTransferRef)
                        Grd = Grd3;

                    if (Grd != null)
                    {
                        if (Grd.Rows.Count > 1)
                            Sm.ExportToExcel(Grd);
                        else
                        {
                            Sm.StdMsg(mMsgType.Info, "No data to export.");
                            return;
                        }
                    }                    
                }
            }
        }

        //override protected void FrmClosing(object sender, FormClosingEventArgs e)
        //{
        //    if (FrmFind != null) FrmFind.Close();
        //}
        #endregion

        #region Standard Method
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPropertyCode, TxtPropertyName, TxtDisplayName, TxtParent, TxtPropertyMutationDocNo, TxtExternalCode, TxtStatus, TxtCancelReason, TxtDeactivationReason,
                        LuePropertyCategory, TxtShortCode, TxtItCode, TxtItName, DteRegistrationDt, TxtCCCode, TxtAcNo, TxtAcDesc, TxtSiteCode,
                        TxtPropertyInventoryValue, TxtUoM, TxtInventoryQty, TxtUPrice, TxtRemStockQty, TxtRemStockValue, LueAcquisitionType, LueProvince, LueCity,
                        TxtDistrict, MeeDistrict, TxtVillage, MeeVillage, TxtRightsNumberAndNIB, DteCertificatePublishDt, TxtRightsHolder, DteRightsExpiredDt, DteDecreeDt,
                        TxtDecreeNo, TxtIndicativePotentialArea, TxtSpatialInfoPage, MeeRightsReleaseBasis
                    }, true);
                    ChkCompleteInd.Properties.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    ChkActInd.Properties.ReadOnly = true;
                    BtnItCode.Enabled = false;
                    BtnSiteCode.Enabled = false;
                    if(TxtParent.Text.Length > 0)
                        BtnParent.Enabled = true;
                    else
                        BtnParent.Enabled = false;
                    if(TxtPropertyMutationDocNo.Text.Length > 0)
                        BtnPropertyMutationDocNo.Enabled = true;
                    else
                        BtnPropertyMutationDocNo.Enabled = false;
                    BtnCCCode.Enabled = false;
                    LblCopyData.Visible = TxtCopyData.Visible = false;
                    BtnCopyData.Enabled = BtnCopyData.Visible = false;                    
                    
                    TxtPropertyCode.Focus();
                    //Grd1.ReadOnly = false;
                    //Grd2.ReadOnly = true;
                    //Grd3.ReadOnly = true;
                    Grd5.ReadOnly = false;
                    //Grd6.ReadOnly = true;

                    //BtnFile.Enabled = false;
                    //BtnDownload.Enabled = false;
                    //if (TxtAssetCode.Text.Length > 0)
                    //    BtnDownload.Enabled = true;
                    for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 1).Length == 0)
                        {
                            Grd5.Cells[Row, 2].ReadOnly = iGBool.False;
                            Grd5.Cells[Row, 3].ReadOnly = iGBool.True;
                            Grd5.Cells[Row, 1].ReadOnly = iGBool.True;
                        }
                        else
                        {
                            Grd5.Cells[Row, 2].ReadOnly = iGBool.True;
                            Grd5.Cells[Row, 3].ReadOnly = iGBool.False;
                            Grd5.Cells[Row, 1].ReadOnly = iGBool.True;
                        }
                    }
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPropertyName, TxtDisplayName, TxtExternalCode, LuePropertyCategory, TxtShortCode,DteRegistrationDt, LueAcquisitionType,
                        LueProvince, LueCity, TxtDistrict, MeeDistrict, TxtVillage, MeeVillage, TxtRightsNumberAndNIB, DteCertificatePublishDt, TxtRightsHolder,
                        DteRightsExpiredDt, DteDecreeDt, TxtDecreeNo, TxtIndicativePotentialArea, TxtSpatialInfoPage, MeeRightsReleaseBasis
                    }, false);
                    ChkCompleteInd.Properties.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkActInd.Properties.ReadOnly = false;

                    BtnPropertyMutationDocNo.Enabled = true;
                    BtnItCode.Enabled = true;
                    BtnSiteCode.Enabled = true;
                    BtnParent.Enabled = true;
                    BtnCCCode.Enabled = true;
                    LblCopyData.Visible = TxtCopyData.Visible = true;
                    BtnCopyData.Enabled = BtnCopyData.Visible = true;

                    ChkActInd.Properties.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCompleteInd.Properties.ReadOnly = false;



                    //Grd1.ReadOnly = false;
                    //Grd2.ReadOnly = false;
                    //Grd3.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    //Grd6.ReadOnly = false;
                    break;
                case mState.Edit:

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPropertyName, TxtDisplayName, TxtExternalCode,
                        TxtCancelReason, TxtDeactivationReason, TxtShortCode, DteRegistrationDt,
                        LueAcquisitionType, LueProvince, LueCity, TxtDistrict, MeeDistrict, TxtVillage, MeeVillage, TxtRightsNumberAndNIB,
                        DteCertificatePublishDt, TxtRightsHolder, DteRightsExpiredDt, DteDecreeDt, TxtDecreeNo,
                        TxtIndicativePotentialArea, TxtSpatialInfoPage, MeeRightsReleaseBasis
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPropertyCode,TxtParent, TxtPropertyMutationDocNo,  TxtStatus, LuePropertyCategory,
                        TxtCCCode, TxtAcNo, TxtAcDesc, TxtSiteCode, TxtPropertyInventoryValue, TxtUoM,
                        TxtInventoryQty, TxtUPrice, TxtRemStockQty, TxtRemStockValue

                    }, true);

                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCompleteInd.Properties.ReadOnly = false;
                    ChkActInd.Properties.ReadOnly = false;

                    BtnItCode.Enabled = true;
                    BtnParent.Enabled = true;
                    BtnPropertyMutationDocNo.Enabled = true;
                    BtnCCCode.Enabled = true;
                    BtnSiteCode.Enabled = true;
                    

                    //Grd1.ReadOnly = false;
                    //Grd2.ReadOnly = true;
                    //Grd3.ReadOnly = true;
                    Grd5.ReadOnly = false;
                    //Grd6.ReadOnly = true;

                    if (!ChkActInd.Checked || ChkCancelInd.Checked || ChkCompleteInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            TxtPropertyCode, TxtPropertyName, TxtDisplayName, TxtExternalCode,
                             TxtShortCode, DteRegistrationDt,
                            LueAcquisitionType, LueProvince, LueCity, TxtDistrict, MeeDistrict, TxtVillage, MeeVillage, TxtRightsNumberAndNIB,
                            DteCertificatePublishDt, TxtRightsHolder, DteRightsExpiredDt, DteDecreeDt, TxtDecreeNo,
                            TxtIndicativePotentialArea, TxtSpatialInfoPage, MeeRightsReleaseBasis
                        }, true);

                        BtnItCode.Enabled = false;
                        BtnParent.Enabled = false;
                        BtnPropertyMutationDocNo.Enabled = false;
                        BtnCCCode.Enabled = false;
                        BtnSiteCode.Enabled = false;
                    }

                    if (ChkCancelInd.Checked || TxtStatus.Text == "Approved") {
                        ChkCancelInd.Properties.ReadOnly = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCancelReason }, true);

                    }
                    
                    if (ChkCompleteInd.Checked) ChkCompleteInd.Properties.ReadOnly = true;

                    if (ChkCancelInd.Checked) 
                        Grd5.ReadOnly = true;

                    break;
            }
        }
        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtPropertyCode, TxtPropertyName, TxtDisplayName, TxtParent, TxtPropertyMutationDocNo, TxtExternalCode, TxtStatus, TxtCancelReason, TxtDeactivationReason,
                LuePropertyCategory, TxtShortCode, TxtItCode, TxtItName, DteRegistrationDt, TxtCCCode, TxtAcNo, TxtAcDesc, TxtSiteCode,
                TxtPropertyInventoryValue, TxtUoM, TxtInventoryQty, TxtUPrice, TxtRemStockQty, TxtRemStockValue, LueAcquisitionType, LueProvince, LueCity,
                TxtDistrict, MeeDistrict, TxtVillage, MeeVillage, TxtRightsNumberAndNIB, DteCertificatePublishDt, TxtRightsHolder, DteRightsExpiredDt, DteDecreeDt,
                TxtDecreeNo, TxtIndicativePotentialArea, TxtSpatialInfoPage, MeeRightsReleaseBasis, TxtCertificateType, TxtCertificateNo, TxtCertificateIssuedDt,
                TxtCertificateExpiredDt, MeeCertExpDt, TxtCertificateIssuedLoc, TxtNIB, TxtRegistrationBasis, MeeRegistrationBasis, TxtBasisDocumentDt, TxtSurveyDocNo,
                TxtSurveyDocDt, TxtAreaBasedOnSurveyDoc, TxtCopyData
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtIndicativePotentialArea, TxtPropertyInventoryValue, TxtRemStockValue, TxtInventoryQty, TxtUPrice, TxtRemStockQty, TxtRemStockValue
            }, 0);
            ChkCompleteInd.Checked = false;
            ChkCancelInd.Checked = false;
            ChkActInd.Checked = false;
            
            // PbUpload.Value = 0;
            // ChkFile.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);

        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);

            Sm.SetGrdNumValueZero(Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6 });
        }
        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PropertyCode From TblPropertyInventoryHdr ");
            SQL.AppendLine("Where CancelInd='Y' And PropertyCode = @PropertyCode ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PropertyCode", TxtPropertyCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }
        


        private bool IsInsertedDataNotValid()
        {

            return
                Sm.IsTxtEmpty(TxtPropertyName, "Property Name", false) ||
                Sm.IsLueEmpty(LuePropertyCategory, "Property Category") ||
                Sm.IsTxtEmpty(TxtItCode, "Item", false) ||
                Sm.IsTxtEmpty(TxtCCCode, "Cost Center", false) ||
                Sm.IsTxtEmpty(TxtSiteCode, "Site", false) ||
                Sm.IsDteEmpty(DteRegistrationDt, "Registration Date") ||
                Sm.IsLueEmpty(LueAcquisitionType, "Acquisition Type") ||
                Sm.IsLueEmpty(LueProvince, "Province") ||
                Sm.IsLueEmpty(LueCity, "City") ||
                Sm.IsMeeEmpty(MeeRightsReleaseBasis, "Dasar Pelepasan Hak") ||
                IsUploadFileNotValid(true)
                ;
        }

        private bool IsEditedDataNotValid()
        {

            return
                Sm.IsTxtEmpty(TxtPropertyName, "Property Name", false) ||
                Sm.IsLueEmpty(LuePropertyCategory, "Property Category") ||
                Sm.IsTxtEmpty(TxtItCode, "Item", false) ||
                Sm.IsTxtEmpty(TxtCCCode, "Cost Center", false) ||
                Sm.IsTxtEmpty(TxtSiteCode, "Site", false) ||
                Sm.IsDteEmpty(DteRegistrationDt, "Registration Date") ||
                Sm.IsLueEmpty(LueAcquisitionType, "Acquisition Type") ||
                Sm.IsLueEmpty(LueProvince, "Province") ||
                Sm.IsLueEmpty(LueCity, "City") ||
                IsPropertyInventoryDeactivated() ||
                IsPropertyInventoryCancel() ||
                ((ChkCancelInd.Checked || !ChkActInd.Checked) &&
                 IsPropCodeAlreadyProceedToOtherTrx()) ||
                 IsUploadFileNotValid(false)
                 ;
        }

        private bool IsUploadFileNotValid(bool IsInsert)
        {
            bool mResult = false;
            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (IsInsert)
                {
                    if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd5, Row, 1)))
                    {
                        mResult = true;
                        break;
                    }
                }
                else
                {
                    if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd5, Row, 1)) && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd5, Row, 4).Length == 0)
                    {
                        mResult = true;
                        break;
                    }
                }
            }

            return mResult;
        }

        private bool IsPropertyInventoryDeactivated()
        {

            if (ChkActInd.Checked == false) 
                if (Sm.IsTxtEmpty(TxtDeactivationReason, "Deactivaion Reason", false)) 
                    return true;
            return false;
                
        }

        private bool IsPropertyInventoryCancel()
        {
            if (ChkCancelInd.Checked) 
                if (Sm.IsTxtEmpty(TxtCancelReason, "Cancel Reason", false)) 
                    return true;
            return false;

        }

        private bool IsPropCodeAlreadyProceedToOtherTrx()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', B.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("Left Join TblMenu B On 1 = 1 And B.Param = 'FrmPropertyInventoryCost' ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryMutation' ");
            SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryTransfer' ");
            SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@PropertyCode", TxtPropertyCode.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Remarks", });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "This Property Code is already proceed to other transaction." +
                                Environment.NewLine + Environment.NewLine +
                                Sm.DrStr(dr, c[0]));
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }

        private string GeneratePropertyCode(string DocType)
        {
            // *tahun*/*bulan*/PROPKodeMaster* - 2022/10/PROP/0001
            string
                PropertyDt = Sm.GetDte(DteRegistrationDt),
                PropertyCategoryCode = Sm.GetLue(LuePropertyCategory);
            //PropertyCategoryName = LuePropertyCategory.GetColumnValue("Col2").ToString();
            string
                Mth = PropertyDt.Substring(4, 2),
                Yr = PropertyDt.Substring(0, 4),
                Yr2 = PropertyDt.Substring(0, 4),
                YrMth = Sm.Left(PropertyDt, 6),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"); 

            var SQL = new StringBuilder();


            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("@Param2, '/', @Param3 , " + "'/','" + DocAbbr + "','/', ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Right(Concat('00000', Convert(PropertyCode + 1, Char)), 5) From( ");
            SQL.AppendLine("        Select CONVERT(Substring(Mid(PropertyCode,14,5), 1, 5), DECIMAL) As PropertyCode From TblPropertyInventoryHdr  ");
            SQL.AppendLine("        Where Left(RegistrationDt, 6) = @Param1 ");
            SQL.AppendLine("        And PropertyCode like '%/"+DocAbbr+"/%'  ");
            SQL.AppendLine("        Order By MID(PropertyCode,14, 5) Desc Limit 1 ");
            SQL.AppendLine("        ) As Temp");
            SQL.AppendLine("    ), '00001')  ");
            SQL.AppendLine(") As PropertyCode ");

            return Sm.GetValue(SQL.ToString(), YrMth, Yr, Mth);
        }

        #region Set Grid 
        private void SetGrd()
        {
            #region Grid 1
            Grd1.Cols.Count = 7;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    "",

                    "Document#",
                    "Type",
                    "Add. Cost Component Reason",
                    "Property Category",
                    "Date",

                    "Cost Component Value"

                },
                new int[]
                {
                    20,

                    200, 100, 250, 150, 80,

                    150

                }

                );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });


            #endregion

            #region Grid 2
            Grd2.Cols.Count = 9;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    "",

                    "Property Code",
                    "Property Name",
                    "Property Category",
                    "UoM",
                    "Mutated Quantity",

                    "Mutated Amount",
                    "Inventory Mutation Doc#",
                    "Mutation Date"

                },
                new int[]
                {
                    20,

                    200, 100, 250, 50, 150,

                    150, 150, 100

                }

                );
            Sm.GrdFormatDate(Grd2, new int[] { 8 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            #endregion

            #region Grid 3 
            Grd3.Cols.Count = 10;

            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    "",


                    "Transfer Document#",
                    "Date",
                    "Property Category From",
                    "Property Category To",
                    "Site From",

                    "Site To",
                    "Cost Center From",
                    "Cost Center To",
                    "Remarks"

                },
                new int[]
                {
                    20,

                    200, 80, 250, 250, 250,

                    250, 250, 250, 300

                }

                );
            Sm.GrdFormatDate(Grd3, new int[] { 2 });
            Sm.GrdFormatDec(Grd3, new int[] { }, 0);
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColInvisible(Grd3, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            #endregion

            #region Grid 5
            Grd5.Cols.Count = 5;
            Grd5.FrozenArea.ColCount = 3;
            Grd5.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[]
                    {
                        //0
                        "D No",
                        //1-4
                        "File Name",
                        "U",
                        "D",
                        "File Name2"
                    },
                     new int[]
                    {
                        0,
                        250, 20, 20, 250
                    }
                );

            Sm.GrdColInvisible(Grd5, new int[] { 0, 4 }, false);
            Sm.GrdColButton(Grd5, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd5, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 1, 4 });
            if(!mIsPropertyInventoryAllowToUploadFile) Sm.GrdColReadOnly(Grd5, new int[] { 2 });
            #endregion

            #region Grid 6
            Grd6.ReadOnly = true;
            Grd6.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd6, new int[] { 3 });
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd5, new int[] { 0 }, false);
            #endregion

            #region Grid 7
            Grd7.Cols.Count = 9;

            Sm.GrdHdrWithColWidth(
                Grd7,
                new string[]
                {
                    "",

                    "Document#",
                    "Date",
                    "CoA Fair Value",
                    "Fair Value Type",
                    "Remaining Quantity",

                    "UoM",
                    "Unit Price",
                    "Fair Value Adjustment"

                },
                new int[]
                {
                    20,

                    200, 100, 150, 100, 150,

                    100, 100, 150

                }

                );
            Sm.GrdFormatDate(Grd7, new int[] { 2 });
            Sm.GrdFormatDec(Grd7, new int[] { 5, 7, 8 }, 0);
            Sm.GrdColButton(Grd7, new int[] { 0 });
            Sm.GrdColInvisible(Grd7, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd7, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });

            #endregion
        }
        #endregion

        #region Save Data

        #region Insert Data
        private void InsertData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;

                string PropertyCode = string.Empty;
                string PropertyCategoryCode = Sm.GetLue(LuePropertyCategory);

                if (TxtPropertyCode.Text.Length == 0)
                {

                    PropertyCode = GeneratePropertyCode("PropertyInventory");
                }
                else
                    PropertyCode = TxtPropertyCode.Text;

                var cml = new List<MySqlCommand>();

                cml.Add(SavePropertyInventory(PropertyCode));

                //cml.Add(DeletePropertyInventoryFile());

                //if (Grd5.Rows.Count > 1) cml.Add(SavePropertyDtl(PropertyCode));
                cml.Add(SavePropertyInventoryFile(PropertyCode));
                if(ChkCompleteInd.Checked) cml.Add(SaveDocApproval(PropertyCode));
                Sm.ExecCommands(cml);

                //if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                //    UploadFile(PropertyCode);

                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                    {
                        if ( Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1")
                        {
                            //if (Sm.GetGrdStr(Grd5, Row, 1) != Sm.GetGrdStr(Grd5, Row, 4))
                            //{
                                UploadFile(PropertyCode, Row, Sm.GetGrdStr(Grd5, Row, 1));
                            //}
                        }
                    }
                }

                ShowData(PropertyCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        private void EditData()
        {

            try
            {
                if (TxtCancelReason.Text.Length > 0) ChkCancelInd.Checked = true;
                if (TxtDeactivationReason.Text.Length > 0) ChkActInd.Checked = false;
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
                if (ChkCancelInd.Checked) ChkActInd.Checked = false;
                if (IsDataCancelledAlready()) return;
                Cursor.Current = Cursors.WaitCursor;
                var PropertyCode = TxtPropertyCode.Text;
                var PropertyCategoryCode = Sm.GetLue(LuePropertyCategory);
                var cml = new List<MySqlCommand>();

                //if (ChkCancelInd.Checked)
                //    ChkActInd.Checked = false;

                //if (!ChkActInd.Checked)
                //    ChkCancelInd.Checked = true;

                cml.Add(UpdateProperty());
                if (ChkCompleteInd.Checked) cml.Add(SaveDocApproval(PropertyCode));

                cml.Add(SavePropertyInventoryFile(TxtPropertyCode.Text));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
                    {
                        if ( Sm.GetGrdStr(Grd5, Row, 1).Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd5, Row, 4).Length == 0)
                        {
                            UploadFile(TxtPropertyCode.Text, Row, Sm.GetGrdStr(Grd5, Row, 1));
                        }
                    }
                }
                //

                ShowData(PropertyCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        private MySqlCommand UpdateProperty()
        {
            var SQL = new StringBuilder();

            SQL.Append("UPDATE tblpropertyinventoryhdr SET ");
            SQL.Append("	PropertyName = @PropertyName, ");
            SQL.Append("	DisplayName = @DisplayName, ");
            SQL.Append("	Parent = @Parent, ");
            SQL.Append("	PropertyMutationDocNo = @PropertyMutationDocNo, ");
            SQL.Append("	ExternalCode = @ExternalCode, ");
            SQL.Append("	STATUS = @STATUS, ");
            SQL.Append("	CompleteInd = @CompleteInd, ");
            SQL.Append("	CancelInd = @CancelInd, ");
            SQL.Append("	CancelReason = @CancelReason, ");
            SQL.Append("	ActInd = @ActInd, ");
            SQL.Append("	DeactivationReason = @DeactivationReason, ");
            //SQL.Append("	PropertyCategoryCode = @PropertyCategoryCode, ");
            SQL.Append("	ShortCode = @ShortCode, ");
            SQL.Append("	ItCode = @ItCode, ");
            SQL.Append("	RegistrationDt = Left(@RegistrationDt,8) , ");
            SQL.Append("	CCCode = @CCCode, ");
            SQL.Append("	AcNo = @AcNo, ");
            SQL.Append("	SiteCode = @SiteCode, ");
            //SQL.Append("	PropertyInventoryValue = @PropertyInventoryValue, ");
            SQL.Append("	UomCode = @UomCode, ");
            //SQL.Append("	InventoryQty = @InventoryQty, ");
            //SQL.Append("	Uprice = @Uprice, ");
            //SQL.Append("	RemStockQty = @RemStockQty, ");
            //SQL.Append("	RemStockValue = @RemStockValue, ");
            SQL.Append("	AcquisitionType = @AcquisitionType, ");
            SQL.Append("	ProvCode = @ProvCode, ");
            SQL.Append("	CityCode = @CityCode, ");
            SQL.Append("	District = @District, ");
            SQL.Append("	Village = @Village, ");
            SQL.Append("	RightsNumberAndNIB = @RightsNumberAndNIB, ");
            SQL.Append("	CertificatePublishDt = Left(@CertificatePublishDt,8) , ");
            SQL.Append("	RightsHolder = @RightsHolder, ");
            SQL.Append("	RightsExpiredDt = Left(@RightsExpiredDt,8) , ");
            SQL.Append("	DecreeDt = Left(@DecreeDt,8) , ");
            SQL.Append("	DecreeNo = @DecreeNo, ");
            SQL.Append("	RightsReleaseBasis = @RightsReleaseBasis, ");
            SQL.Append("	IndicativePotentialArea = @IndicativePotentialArea, ");
            SQL.Append("	SpatialInfoPage = @SpatialInfoPage, ");
            SQL.Append("	LastUpBy = @UserCode, ");
            SQL.Append("	LastUpDt = CurrentDateTime() ");
            SQL.Append("	WHERE PropertyCode = @PropertyCode ");

            //if (ChkActInd.Checked)
            //    CancelData();

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PropertyCode", TxtPropertyCode.Text);
            Sm.CmParam<String>(ref cm, "@PropertyName", TxtPropertyName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", TxtParent.Text);
            Sm.CmParam<String>(ref cm, "@PropertyMutationDocNo", TxtPropertyMutationDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ExternalCode", TxtExternalCode.Text);
            Sm.CmParam<String>(ref cm, "@Status", ChkCancelInd.Checked ? "C" : "O");
            Sm.CmParam<String>(ref cm, "@CompleteInd", ChkCompleteInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", TxtCancelReason.Text);
            //if (ChkCancelInd.Checked = true) ChkActInd.Checked = false;
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            //if (ChkActInd.Checked = false) ChkCancelInd.Checked = true;
            Sm.CmParam<String>(ref cm, "@DeactivationReason", TxtDeactivationReason.Text);
            //Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", Sm.GetLue(LuePropertyCategory));
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParamDt(ref cm, "@RegistrationDt", Sm.GetDte(DteRegistrationDt));
            Sm.CmParam<String>(ref cm, "@CCCode", GetCCCode());
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", GetSiteCode());
            //Sm.CmParam<Decimal>(ref cm, "@PropertyInventoryValue", Decimal.Parse(TxtPropertyInventoryValue.Text));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUoM.Text);
            //Sm.CmParam<Decimal>(ref cm, "@InventoryQty", Decimal.Parse(TxtInventoryQty.Text));
            //Sm.CmParam<Decimal>(ref cm, "@UPrice", Decimal.Parse(TxtPropertyInventoryValue.Text) / Decimal.Parse(TxtInventoryQty.Text));
            //Sm.CmParam<Decimal>(ref cm, "@RemStockQty", Decimal.Parse(TxtInventoryQty.Text));
            //Sm.CmParam<Decimal>(ref cm, "@RemStockValue", Decimal.Parse(TxtRemStockQty.Text) * Decimal.Parse(TxtUPrice.Text));
            Sm.CmParam<String>(ref cm, "@AcquisitionType", Sm.GetLue(LueAcquisitionType));
            Sm.CmParam<String>(ref cm, "@ProvCode", Sm.GetLue(LueProvince));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@District", MeeDistrict.Text);
            Sm.CmParam<String>(ref cm, "@Village", MeeVillage.Text);
            Sm.CmParam<String>(ref cm, "@RightsNumberAndNIB", TxtRightsNumberAndNIB.Text);
            Sm.CmParam(ref cm, "@CertificatePublishDt", Sm.GetDte(DteCertificatePublishDt));
            Sm.CmParam<String>(ref cm, "@RightsHolder", TxtRightsHolder.Text);
            Sm.CmParam(ref cm, "@RightsExpiredDt", Sm.GetDte(DteRightsExpiredDt));
            Sm.CmParam(ref cm, "@DecreeDt", Sm.GetDte(DteDecreeDt));
            Sm.CmParam<String>(ref cm, "@DecreeNo", TxtDecreeNo.Text);
            Sm.CmParam<String>(ref cm, "@RightsReleaseBasis", MeeRightsReleaseBasis.Text);
            Sm.CmParam<Decimal>(ref cm, "@IndicativePotentialArea", Decimal.Parse(TxtIndicativePotentialArea.Text));
            Sm.CmParam<String>(ref cm, "@SpatialInfoPage", TxtSpatialInfoPage.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;



        }


        private MySqlCommand SavePropertyInventoryFile(string PropertyCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            bool IsFirstOrExisted = true;

            //SQL.AppendLine("/* PropertyDtl  ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd5.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into tblpropertyinventorydtl(PropertyCode, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@PropertyCode, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd5, r, 1));
                }

            }
            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }



            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        private MySqlCommand UpdatePropertyInventoryFile(string PropertyCode, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where PropertyCode=@PropertyCode and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }
        private MySqlCommand DeletePropertyInventoryFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblPropertyInventoryDtl Where PropertyCode=@PropertyCode; "
            };
            Sm.CmParam<String>(ref cm, "@PropertyCode", TxtPropertyCode.Text);

            return cm;
        }

        private bool IsPropertyAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select PropertyCode From TblPropertyInventoryHdr " +
                "Where PropertyCode=@Param And ActInd='N';",
                TxtPropertyCode.Text,
                "This Property already not active.");
        }
        private void GetCOA()
        {

            string AcNoSQL = "SELECT A.AcNo1 FROM tblpropertyinventorycategory A " +
                            "LEFT JOIN tblcoa B ON A.AcNo1 = B.AcNo WHERE A.PropertyCategoryName = @Param";
            string AcNo = Sm.GetValue(AcNoSQL, LuePropertyCategory.Text);

            string AcDescSQL = "SELECT B.AcDesc FROM tblpropertyinventorycategory A " +
                "LEFT JOIN tblcoa B ON A.AcNo1 = B.AcNo WHERE A.PropertyCategoryName = @Param";
            string AcDesc = Sm.GetValue(AcDescSQL, LuePropertyCategory.Text);

            TxtAcNo.Text = AcNo;

            TxtAcDesc.Text = AcDesc;
        }

        private string GetCCCode()
        {
            string CCSQL = "SELECT A.CCCode FROM tblcostcenter A WHERE A.CCName = @Param";
            string CCCode = Sm.GetValue(CCSQL, TxtCCCode.Text);
            return CCCode;
        }

        private string GetSiteCode()
        {
            string SiteCodeSQL = "Select A.SiteCode From tblsite A Where A.SiteName = @Param";
            string SiteCode = Sm.GetValue(SiteCodeSQL, TxtSiteCode.Text);
            return SiteCode;
        }
        private MySqlCommand SavePropertyInventory(string PropertyCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PropertyHdr */ ");

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.Append("Insert Into tblpropertyinventoryhdr(PropertyCode, PropertyName, DisplayName, Parent, PropertyMutationDocNo, ExternalCode, Status, CompleteInd, CancelInd,	");
            SQL.Append("CancelReason, ActInd, DeactivationReason, PropertyCategoryCode, ShortCode, ItCode, RegistrationDt, CCCode, AcNo, SiteCode, ");
            SQL.Append("PropertyInventoryValue, UomCode, InventoryQty, UPrice, RemStockQty, RemStockValue, AcquisitionType,	");
            SQL.Append("ProvCode, CityCode, District, Village, RightsNumberAndNIB, CertificatePublishDt , RightsHolder, RightsExpiredDt ,	");
            SQL.Append("DecreeDt , DecreeNo, RightsReleaseBasis, IndicativePotentialArea, SpatialInfoPage, CreateBy, CreateDt ) 	");

            SQL.Append("Values(@PropertyCode, @PropertyName,@DisplayName, @Parent, @PropertyMutationDocNo, @ExternalCode, @Status, @CompleteInd, @CancelInd , @CancelReason, @ActInd, @DeactivationReason, 	");
            SQL.Append("@PropertyCategoryCode, @ShortCode, @ItCode, Left(@RegistrationDt,8) , @CCCode, @AcNo, @SiteCode, ");
            SQL.Append("@PropertyInventoryValue, ");
            SQL.Append("@UomCode, @InventoryQty, @UPrice, @RemStockQty, @RemStockValue, @AcquisitionType, 	");

            SQL.Append("@ProvCode, @CityCode, @District, @Village, @RightsNumberAndNIB, Left(@CertificatePublishDt,8) , @RightsHolder, Left(@RightsExpiredDt,8) ,  	");
            SQL.Append("Left(@DecreeDt,8) , @DecreeNo, @RightsReleaseBasis, @IndicativePotentialArea, @SpatialInfoPage, @UserCode, @Dt) ; 	");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };


            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.CmParam<String>(ref cm, "@PropertyName", TxtPropertyName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", TxtParent.Text);
            Sm.CmParam<String>(ref cm, "@PropertyMutatuonDocNo", TxtPropertyMutationDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ExternalCode", TxtExternalCode.Text);
            Sm.CmParam<String>(ref cm, "@Status", ChkCancelInd.Checked ? "C" : "O");
            Sm.CmParam<String>(ref cm, "@CompleteInd", ChkCompleteInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", TxtCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DeactivationReason", TxtDeactivationReason.Text);
            Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", Sm.GetLue(LuePropertyCategory));
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam(ref cm, "@RegistrationDt", Sm.GetDte(DteRegistrationDt));
            Sm.CmParam<String>(ref cm, "@CCCode", GetCCCode());
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", GetSiteCode());
            Sm.CmParam<Decimal>(ref cm, "@PropertyInventoryValue", Decimal.Parse(TxtPropertyInventoryValue.Text));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUoM.Text);
            Sm.CmParam<Decimal>(ref cm, "@InventoryQty", Decimal.Parse(TxtInventoryQty.Text));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Decimal.Parse(TxtUPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemStockQty", Decimal.Parse(TxtRemStockQty.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemStockValue", Decimal.Parse(TxtRemStockValue.Text));
            Sm.CmParam<String>(ref cm, "@AcquisitionType", Sm.GetLue(LueAcquisitionType));
            Sm.CmParam<String>(ref cm, "@ProvCode", Sm.GetLue(LueProvince));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@District", MeeDistrict.Text);
            Sm.CmParam<String>(ref cm, "@Village", MeeVillage.Text);
            Sm.CmParam<String>(ref cm, "@RightsNumberAndNIB", TxtRightsNumberAndNIB.Text);
            Sm.CmParam(ref cm, "@CertificatePublishDt", Sm.GetDte(DteCertificatePublishDt));
            Sm.CmParam<String>(ref cm, "@RightsHolder", TxtRightsHolder.Text);
            Sm.CmParam(ref cm, "@RightsExpiredDt", Sm.GetDte(DteRightsExpiredDt));
            Sm.CmParam(ref cm, "@DecreeDt", Sm.GetDte(DteDecreeDt));
            Sm.CmParam<String>(ref cm, "@DecreeNo", TxtDecreeNo.Text);
            Sm.CmParam<String>(ref cm, "@RightsReleaseBasis", MeeRightsReleaseBasis.Text);
            Sm.CmParam<Decimal>(ref cm, "@IndicativePotentialArea", Decimal.Parse(TxtIndicativePotentialArea.Text));
            Sm.CmParam<String>(ref cm, "@SpatialInfoPage", TxtSpatialInfoPage.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDocApproval(string PropertyCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @PropertyCode, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PropertyInventory' ");
            SQL.AppendLine("And T.DeptCode = @DeptCode ");
            //SQL.AppendLine("And T.SiteCode = @SiteCode ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select PropertyInventoryValue ");
            SQL.AppendLine("    From TblPropertyInventoryHdr  ");
            SQL.AppendLine("    Where PropertyCode=@PropertyCode ");
            SQL.AppendLine("), 0.00)) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update CreateBy=@UserCode, CreateDt=CurrentDateTime() ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPropertyInventoryHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where PropertyCode = @PropertyCode ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @PropertyCode ");
            SQL.AppendLine("    And DocType='PropertyInventory' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", GetSiteCode());
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetValue("Select DeptCode From TblCostCenter Where CCCode = @Param; ", GetCCCode()));
            return cm;

        }


        #endregion

        #endregion

        #endregion


        #region Button Method
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPropertyInventoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        #endregion

        #region Show Data
        public void ShowData(string PropertyCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                ShowPropertyInventoryHdr(PropertyCode);
                ShowPropertyInventoryDtl(PropertyCode);
                ShowPropertyCostComponent(PropertyCode);
                //ShowFairValue(PropertyCode);
                ShowPropertyInventoryMutation(PropertyCode);
                ShowPropertyInventoryTransfer(PropertyCode);
                ShowPropertyInventoryCertificate(PropertyCode);
                ShowDocApproval(PropertyCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPropertyInventoryHdr(string PropertyCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);

            SQL.AppendLine(" SELECT A.PropertyCode, A.PropertyName, A.DisplayName, A.Parent, A.PropertyMutationDocNo, A.ExternalCode, Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, A.CompleteInd, A.CancelInd, ");
            SQL.AppendLine(" A.CancelReason, A.ActInd, A.DeactivationReason, A.PropertyCategoryCode, A.ShortCode, A.ItCode, A.RegistrationDt, C.CCName, A.AcNo, B.SiteName, ");
            SQL.AppendLine(" A.PropertyInventoryValue, A.UomCode, A.InventoryQty, A.UPrice, A.RemStockQty, A.RemStockValue, A.AcquisitionType, A.ProvCode, A.CityCode, ");
            SQL.AppendLine(" A.District, A.Village, A.RightsNumberAndNIB, A.CertificatePublishDt, A.RightsHolder, A.RightsExpiredDt, A.DecreeDt, A.DecreeNo, IfNull((A.PropertyInventoryValue/ A.InventoryQty), 0) UPrice, ");
            SQL.AppendLine(" A.RightsReleaseBasis, A.IndicativePotentialArea, A.SpatialInfoPage, D.ItName, A.CreateBy, A.CreateDt ");
            SQL.AppendLine(" FROM tblpropertyinventoryhdr A ");
            SQL.AppendLine(" LEFT JOIN tblsite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine(" LEFT JOIN tblcostcenter C ON A.CCCode = C.CCCode ");
            SQL.AppendLine(" LEFT JOIN tblitem D ON A.ItCode = D.ItCode ");
            SQL.AppendLine(" LEFT JOIN tblpropertyinventorycostcomponenthdr E ON A.PropertyCode = E.PropertyCode ");
            SQL.AppendLine(" WHERE A.PropertyCode = @PropertyCode ");


            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "PropertyCode", 
                    
                    //1-5
                    "PropertyName", "DisplayName", "Parent", "PropertyMutationDocNo", "ExternalCode",  
                    
                    //6-10
                    "StatusDesc", "CompleteInd", "CancelInd", "CancelReason", "ActInd", 
                    
                    //11-15
                    "DeactivationReason", "PropertyCategoryCode", "ShortCode", "ItCode", "RegistrationDt",

                    //16-20
                    "CCName", "AcNo", "SiteName", "PropertyInventoryValue", "UomCode", 
                    
                    //21-25
                    "InventoryQty", "UPrice", "RemStockQty", "RemStockValue", "AcquisitionType", 

                    //26-30
                    "ProvCode", "CityCode", "District", "Village", "RightsNumberAndNIB", 
                    
                    //31-35
                    "CertificatePublishDt", "RightsHolder", "RightsExpiredDt", "DecreeDt", "DecreeNo",
                    
                    //36-40
                    "RightsReleaseBasis", "IndicativePotentialArea", "SpatialInfoPage", "ItName", "UPrice"

                },
                (MySqlDataReader dr, int[] c) =>
                {

                    TxtPropertyCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtPropertyName.EditValue = Sm.DrStr(dr, c[1]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                    TxtParent.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPropertyMutationDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtExternalCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[6]);
                    ChkCompleteInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[7]), "Y");
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[8]), "Y");
                    TxtCancelReason.EditValue = Sm.DrStr(dr, c[9]);
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[10]), "Y");
                    TxtDeactivationReason.EditValue = Sm.DrStr(dr, c[11]);
                    //Sm.SetLue(LuePropertyCategory, Sm.DrStr(dr, c[12]));
                    SetLuePropertyCategory(ref LuePropertyCategory, Sm.DrStr(dr, c[12]));
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[13]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[14]);
                    Sm.SetDte(DteRegistrationDt, Sm.DrStr(dr, c[15]));
                    TxtCCCode.EditValue = Sm.DrStr(dr, c[16]);
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[17]);
                    TxtSiteCode.EditValue = Sm.DrStr(dr, c[18]);
                    //Sm.SetLue(LueAcquisitionType, Sm.DrStr(dr, c[25]));
                    //TxtPropertyInventoryValue.EditValue = Sm.DrStr(dr, c[19]);
                    TxtPropertyInventoryValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0); 
                    TxtUoM.EditValue = Sm.DrStr(dr, c[20]);
                    TxtInventoryQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                    TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0); // inventoryvalue / inventoryqty
                    TxtRemStockQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    TxtRemStockValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0); //remstockqty * uprice
                    Sm.SetLue(LueAcquisitionType, Sm.DrStr(dr, c[25]));
                    Sm.SetLue(LueProvince, Sm.DrStr(dr, c[26]));
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[27]));
                    //TxtDistrict.EditValue = Sm.DrStr(dr, c[28]);
                    MeeDistrict.EditValue = Sm.DrStr(dr, c[28]);
                    //TxtVillage.EditValue = Sm.DrStr(dr, c[29]);
                    MeeVillage.EditValue = Sm.DrStr(dr, c[29]);
                    TxtRightsNumberAndNIB.EditValue = Sm.DrStr(dr, c[30]);
                    Sm.SetDte(DteCertificatePublishDt, Sm.DrStr(dr, c[31]));
                    TxtRightsHolder.EditValue = Sm.DrStr(dr, c[32]);
                    Sm.SetDte(DteRightsExpiredDt, Sm.DrStr(dr, c[33]));
                    Sm.SetDte(DteDecreeDt, Sm.DrStr(dr, c[34]));
                    TxtDecreeNo.EditValue = Sm.DrStr(dr, c[35]);
                    MeeRightsReleaseBasis.Text = Sm.DrStr(dr, c[36]);
                    TxtIndicativePotentialArea.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                    TxtSpatialInfoPage.EditValue = Sm.DrStr(dr, c[38]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[39]);

                }, true
            );
        }

        private void ShowPropertyInventoryDtl(string PropertyCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm,
                   "select DNo, FileName from  TblPropertyInventoryDtl Where PropertyCode=@PropertyCode Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd5, dr, c, Row, 4, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowPropertyCostComponent(string PropertyCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                //" SELECT A.DocNo, C.OptDesc AS DocType, A.CostComponentReason, E.PropertyCategoryName, Left(A.DocDt,8) as DocDt, A.CostComponentAmt " +
                //" FROM tblpropertyinventorycostcomponenthdr A " +
                //" INNER JOIN tblpropertyinventorycostcomponentdtl B ON A.DocNo = B.DocNo " +
                //" INNER JOIN tbloption C ON B.DocType = C.OptCode AND C.OptCat = 'PropertyInventoryDocType' " +
                //" INNER JOIN tblpropertyinventoryhdr D ON A.PropertyCode = D.PropertyCode " +
                //" INNER JOIN tblpropertyinventorycategory E ON D.PropertyCategoryCode = E.PropertyCategoryCode " +
                //" WHERE A.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " + 
                //" GROUP BY A.DocNo",


                " SELECT A.DocNo, A.DocType, A.CostComponentReason, A.PropertyCategoryName, A.DocDt, A.CostComponentAmt " +
                " FROM( " +
                "    SELECT A.DocNo, A.Status , C.OptDesc AS DocType, A.CostComponentReason, E.PropertyCategoryName, Left(A.DocDt, 8) as DocDt, A.CostComponentAmt " +
                "    FROM tblpropertyinventorycostcomponenthdr A " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl B ON A.DocNo = B.DocNo " +
                "    INNER JOIN tbloption C ON B.DocType = C.OptCode AND C.OptCat = 'PropertyInventoryDocType' " +
                "    INNER JOIN tblpropertyinventoryhdr D ON A.PropertyCode = D.PropertyCode " +
                "    INNER JOIN tblpropertyinventorycategory E ON A.PropertyCategoryCode = E.PropertyCategoryCode " +
                "    WHERE A.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "    GROUP BY A.DocNo " +

                "    UNION ALL " +

                "    SELECT A.DocNo, A.Status, C.OptDesc AS DocType, A.CostComponentReason, E.PropertyCategoryName, Left(A.DocDt, 8) as DocDt, A.CostComponentAmt " +
                "    FROM tblpropertyinventorycostcomponenthdr A " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl2 B ON A.DocNo = B.DocNo " +
                "    INNER JOIN tbloption C ON B.DocType = C.OptCode AND C.OptCat = 'PropertyInventoryDocType' " +
                "    INNER JOIN tblpropertyinventoryhdr D ON A.PropertyCode = D.PropertyCode " +
                "    INNER JOIN tblpropertyinventorycategory E ON A.PropertyCategoryCode = E.PropertyCategoryCode " +
                "    WHERE A.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "    GROUP BY A.DocNo " +

                "    UNION ALL " +

                "    SELECT A.DocNo, A.Status, C.OptDesc AS DocType, A.CostComponentReason, E.PropertyCategoryName, Left(A.DocDt, 8) as DocDt, A.CostComponentAmt " +
                "    FROM tblpropertyinventorycostcomponenthdr A " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl3 B ON A.DocNo = B.DocNo " +
                "    INNER JOIN tbloption C ON B.DocType = C.OptCode AND C.OptCat = 'PropertyInventoryDocType' " +
                "    INNER JOIN tblpropertyinventoryhdr D ON A.PropertyCode = D.PropertyCode " +
                "    INNER JOIN tblpropertyinventorycategory E ON A.PropertyCategoryCode = E.PropertyCategoryCode " +
                "    WHERE A.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "    GROUP BY A.DocNo " +

                "    Union All " +
                "    SELECT A.DocNo, A.Status , D.OptDesc AS DocType, A.CostComponentReason, F.PropertyCategoryName, Left(A.DocDt, 8) as DocDt, B.CostComponentAmt " +
                "    FROM tblpropertyinventorycostcomponenthdr A " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl4 B ON A.DocNo = B.DocNo " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl C ON A.DocNo = C.DocNo " +
                "    INNER JOIN tbloption D ON C.DocType = D.OptCode AND D.OptCat = 'PropertyInventoryDocType' " +
                "    INNER JOIN tblpropertyinventoryhdr E ON B.PropertyCode = E.PropertyCode " +
                "    INNER JOIN tblpropertyinventorycategory F ON B.PropertyCategoryCode = F.PropertyCategoryCode " +
                "    WHERE B.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "    GROUP BY A.DocNo " +

                "    Union All " +
                "    SELECT A.DocNo, A.Status , D.OptDesc AS DocType, A.CostComponentReason, F.PropertyCategoryName, Left(A.DocDt, 8) as DocDt, B.CostComponentAmt " +
                "    FROM tblpropertyinventorycostcomponenthdr A " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl4 B ON A.DocNo = B.DocNo " +
                "    INNER JOIN tblpropertyinventorycostcomponentdtl2 C ON A.DocNo = C.DocNo " +
                "    INNER JOIN tbloption D ON C.DocType = D.OptCode AND D.OptCat = 'PropertyInventoryDocType' " +
                "    INNER JOIN tblpropertyinventoryhdr E ON B.PropertyCode = E.PropertyCode " +
                "    INNER JOIN tblpropertyinventorycategory F ON B.PropertyCategoryCode = F.PropertyCategoryCode " +
                "    WHERE B.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "    GROUP BY A.DocNo " +
                " )A WHERE Status = 'A'; ",


            new string[]
                {
                    "DocNo",

                    "DocType","CostComponentReason","PropertyCategoryName", "DocDt", "CostComponentAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                    

                }, false, false, true, true

           );
        }
        
        private void ShowFairValue(string PropertyCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                "SELECT A.DocNo, A.DocDt, A.AcNo, A.FairValueType, A.RemainingQty, A.UPrice, A.FairValueAdj, A.UoMCode " +
                "FROM ( " +
                "   SELECT A.DocNo, A.`Status`, Left(A.DocDt, 8) as DocDt, F.Property1 AcNo, C.OptDesc AS DocType, F.OptDesc FairValueType, D.InventoryQty RemainingQty, B.UPriceAdj UPrice, " +
                "   A.CostComponentAmt FairValueAdj, D.UoMCode " +
                "   FROM tblpropertyinventorycostcomponenthdr A " +
                "   INNER JOIN tblpropertyinventorycostcomponentdtl3 B ON A.DocNo = B.DocNo " +
                "   INNER JOIN tbloption C ON B.DocType = C.OptCode AND C.OptCat = 'PropertyInventoryDocType' " +
                "   INNER JOIN tblpropertyinventoryhdr D ON A.PropertyCode = D.PropertyCode " +
                "   Left JOIN tblpropertyinventorycategory E ON A.PropertyCategoryCode = E.PropertyCategoryCode " +
                "   INNER JOIN tbloption F ON F.OptCat='PropertyCostFairValueType' AND B.FairValueType=F.OptCode " +
                "   WHERE A.PropertyCode = @PropertyCode AND A.CancelInd = 'N' " +
                "   GROUP BY A.DocNo " +
                ") A " +
                "WHERE `Status` = 'A'; ",


            new string[]
                {
                    "DocNo",

                    "DocDt", "AcNo", "FairValueType", "RemainingQty", "UomCode",

                    "UPrice", "FairValueAdj"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd7, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 8, 7);
                    

                }, false, false, true, true

           );
        }

        private void ShowPropertyInventoryMutation(string PropertyCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.PropertyCode, C.PropertyName, D.PropertyCategoryCode, E.InventoryUOMCode, ");
            SQL.AppendLine("IfNull(B.Qty, 0.00) As MutatedQty, IfNull(B.MutatedAmt, 0.00) As MutatedAmt, A2.DocNo, A2.DocDt ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationHdr A2 On A.PropertyMutationDocNo = A2.DocNo ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.PropertyMutationDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPropertyInventoryHdr C On B.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory D On C.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem E On C.ItCode = E.ItCode ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode; ");

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(ref Grd2, ref cm, SQL.ToString(),
            new string[]
                {
                    "PropertyCode",

                    "PropertyName","PropertyCategoryCode","InventoryUOMCode", "MutatedQty", "MutatedAmt",

                    "DocNo", "DocDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 8, 7);
                }, false, false, true, true
           );
        }

        private void ShowPropertyInventoryTransfer(string PropertyCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.DocNo, B.DocDt, C.PropertyCategoryName PropertyCtFrom, D.PropertyCategoryName PropertyCtTo, E.SiteName SiteFrom, F.SiteName SiteTo, G.CCName CCFrom, ");
            SQL.AppendLine("H.CCName CCTo, B.Remark ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl A2 On A.PropertyCode = A2.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferHdr B On A2.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory C On B.PropertyCategoryCodeFrom = C.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory D On B.PropertyCategoryCodeTo = D.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblSite E On B.SiteCodeFrom = E.SiteCode ");
            SQL.AppendLine("Inner Join TblSite F On B.SiteCodeTo = F.SiteCode ");
            SQL.AppendLine("Inner Join TblCostCenter G On B.CCCodeFrom = G.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter H On B.CCCodeTo = H.CCCode ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And B.CancelInd = 'N' ");
            SQL.AppendLine("And B.Status = 'A'; ");

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(ref Grd3, ref cm, SQL.ToString(),
            new string[]
                {
                    "DocNo",

                    "DocDt","PropertyCtFrom","PropertyCtTo", "SiteFrom", "SiteTo",

                    "CCFrom", "CCTo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd3, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 9, 8);
                }, false, false, true, true
           );
        }

        private void ShowPropertyInventoryCertificate(string PropertyCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);

            SQL.AppendLine("SELECT D.OptDesc CertificateType, C.CertificateNo, Date_Format(C.CertificateIssuedDt, '%d/%m/%Y') CertIssuedDt, C.CertificateExpDt CertExpDt, ");
            SQL.AppendLine("C.CertificateIssuedLocation, C.NIB, C.RegistrationBasis, Date_Format(C.BasisDocDt, '%d/%m/%Y') BasisDocDt, C.SurveyDocNo, Date_Format(C.SurveyDocDt, '%d/%m/%Y') SurveyDocDt, ");
            SQL.AppendLine("C.AreaBased ");
            SQL.AppendLine("FROM tblpropertyinventoryhdr A ");
            SQL.AppendLine("Inner JOIN tblpropertyinventorytransferdtl B ON A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Inner JOIN tblpropertyinventorytransferhdr C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("Left JOIN tbloption D ON D.OptCat = 'CertificateType' And C.CertificateType = D.OptCode ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And C.CancelInd = 'N' And C.Status = 'A' ");
            SQL.AppendLine("Order By C.CreateDt Desc Limit 1;");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "CertificateType", 
                    
                    //1-5
                    "CertificateNo", "CertIssuedDt", "CertExpDt", "CertificateIssuedLocation", "NIB",  
                    
                    //6-10
                    "RegistrationBasis", "BasisDocDt", "SurveyDocNo", "SurveyDocDt", "AreaBased",

                },
                (MySqlDataReader dr, int[] c) =>
                {

                    TxtCertificateType.EditValue = Sm.DrStr(dr, c[0]);
                    TxtCertificateNo.EditValue = Sm.DrStr(dr, c[1]);
                    TxtCertificateIssuedDt.EditValue = Sm.DrStr(dr, c[2]);
                    //TxtCertificateExpiredDt.EditValue = Sm.DrStr(dr, c[3]);
                    MeeCertExpDt.EditValue = Sm.DrStr(dr, c[3]);
                    TxtCertificateIssuedLoc.EditValue = Sm.DrStr(dr, c[4]);
                    TxtNIB.EditValue = Sm.DrStr(dr, c[5]);
                    //TxtRegistrationBasis.EditValue = Sm.DrStr(dr, c[6]);
                    MeeRegistrationBasis.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBasisDocumentDt.EditValue = Sm.DrStr(dr, c[7]);
                    TxtSurveyDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    TxtSurveyDocDt.EditValue = Sm.DrStr(dr, c[9]);
                    TxtAreaBasedOnSurveyDoc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);

                }, false
            );
        }

        private void ShowDocApproval(string PropertyCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='PropertyInventory' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@PropertyCode ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm, SQL.ToString(),
                    new string[] { "ApprovalDNo", "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        #endregion

        #region Additional Method

        internal void ShowCopyData(string PropertyCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@PropertyCode", PropertyCode);

            SQL.AppendLine("Select A.PropertyCode, A.PropertyName, A.DisplayName, A.ExternalCode, A.PropertyCategoryCode, A.ShortCode, A.ItCode, B.ItName, A.RegistrationDt, C.CCName, ");
            SQL.AppendLine("E.AcNo, E.AcDesc, F.SiteName, B.InventoryUOMCode, ");
            SQL.AppendLine("A.AcquisitionType, A.ProvCode, A.CityCode, A.District, A.Village ,A.RightsNumberAndNIB As NIB, A.CertificatePublishDt, A.RightsHolder, A.RightsExpiredDt, ");
            SQL.AppendLine("A.DecreeDt, A.DecreeNo, A.RightsReleaseBasis, A.IndicativePotentialArea, A.SpatialInfoPage ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory D On A.PropertyCategoryCode = D.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblCOA E On D.AcNo1 = E.AcNo ");
            SQL.AppendLine("Inner Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "PropertyCode", 
                    
                    //1-5
                    "PropertyName", "DisplayName", "ExternalCode", "PropertyCategoryCode", "ShortCode", 
                    
                    //6-10
                    "ItCode", "ItName", "RegistrationDt", "CCName", "AcNo", 

                    //11-15
                    "AcDesc", "SiteName", "InventoryUOMCode", "AcquisitionType", "ProvCode", 

                    //16-20
                    "CityCode", "District", "Village", "NIB", "CertificatePublishDt", 

                    //21-25
                    "RightsHolder", "RightsExpiredDt", "DecreeDt", "DecreeNo", "RightsReleaseBasis", 

                    //26-27
                    "IndicativePotentialArea", "SpatialInfoPage"

                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtCopyData.EditValue = Sm.DrStr(dr, c[0]);
                    TxtPropertyName.EditValue = Sm.DrStr(dr, c[1]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                    TxtExternalCode.EditValue = Sm.DrStr(dr, c[3]);
                    Sm.SetLue(LuePropertyCategory, Sm.DrStr(dr, c[4]));
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetDte(DteRegistrationDt, Sm.DrStr(dr, c[8]));
                    TxtCCCode.EditValue = Sm.DrStr(dr, c[9]);
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[10]);
                    TxtAcDesc.EditValue = Sm.DrStr(dr, c[11]);
                    TxtSiteCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtUoM.EditValue = Sm.DrStr(dr, c[13]);
                    Sm.SetLue(LueAcquisitionType, Sm.DrStr(dr, c[14]));
                    Sm.SetLue(LueProvince, Sm.DrStr(dr, c[15]));
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[16]));
                    MeeDistrict.EditValue = Sm.DrStr(dr, c[17]);
                    MeeVillage.EditValue = Sm.DrStr(dr, c[18]);
                    TxtRightsNumberAndNIB.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetDte(DteCertificatePublishDt, Sm.DrStr(dr, c[20]));
                    TxtRightsHolder.EditValue = Sm.DrStr(dr, c[21]);
                    Sm.SetDte(DteRightsExpiredDt, Sm.DrStr(dr, c[22]));
                    Sm.SetDte(DteDecreeDt, Sm.DrStr(dr, c[23]));
                    TxtDecreeNo.EditValue = Sm.DrStr(dr, c[24]);
                    MeeRightsReleaseBasis.EditValue = Sm.DrStr(dr, c[25]);
                    TxtIndicativePotentialArea.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 0);
                    TxtSpatialInfoPage.EditValue = Sm.DrStr(dr, c[27]);
                }, false
            );
        }

        private void AssignCertificateInfoLabel()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select OptCode, OptDesc From TblOption Where OptCat = 'PropertyInformationCertificateLabel'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        switch(Sm.DrStr(dr, c[0]))
                        {
                            case "01" : LblCertificateType.Text = Sm.DrStr(dr, c[1]); break;
                            case "02" : LblCertificateNo.Text = Sm.DrStr(dr, c[1]); break;
                            case "03" : LblCertificateIssuedDt.Text = Sm.DrStr(dr, c[1]); break;
                            case "04" : LblCertificateExpiredDt.Text = Sm.DrStr(dr, c[1]); break;
                            case "05" : LblCertificateIssuedLoc.Text = Sm.DrStr(dr, c[1]); break;
                            case "06" : LblNIB.Text = Sm.DrStr(dr, c[1]); break;
                            case "07" : LblRegistrationBasis.Text = Sm.DrStr(dr, c[1]); break;
                            case "08" : LblBasisDocumentDt.Text = Sm.DrStr(dr, c[1]); break;
                            case "09" : LblSurveyDocNo.Text = Sm.DrStr(dr, c[1]); break;
                            case "10" : LblSurveyDocDt.Text = Sm.DrStr(dr, c[1]); break;
                            case "11" : LblAreaBasedOnSurveyDoc.Text = Sm.DrStr(dr, c[1]); break;
                        }
                    }
                }
            }
        }

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Ignore Into TblParameter(ParCode, ParDesc, ParValue, Customize, CreateBy, CreateDt)  ");
            //SQL.AppendLine("Values('PropertyInventoryCertificateInformationLabelFormat', 'Format label pada tab Certificate Information master Property Inventory. 1 : Bhs Inggris, 2 : Bhs Indonesia', '1', 'BBT', 'WEDHA', CurrentDateTime()); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '01', 'Jenis Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '02', 'No. Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '03', 'Penerbitan Sertipikat', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '04', 'Tanggal Berakhirnya Hak', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('PropertyInformationCertificateLabel', '05', 'Letak Tanah', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '06', 'Nomor Identifikasi Bidang (NIB)', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '07', 'Dasar Pendaftaran', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '08', 'Tanggal Dasar Pendaftaran', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '09', 'No. Surat Ukur', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '10', 'Tanggal Surat Ukur', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");
            //SQL.AppendLine("INSERT IGNORE INTO `tbloption` (`OptCat`, `OptCode`, `OptDesc`, `Customize`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('PropertyInformationCertificateLabel', '11', 'Luas Berdasarkan Surat Ukur (m2)', 'BBT', 'WEDHA', '202302020900', NULL, NULL); ");

            SQL.AppendLine("ALTER TABLE `tblpropertyinventorytransferhdr` ");
            SQL.AppendLine("    CHANGE COLUMN `CertificateExpDt` `CertificateExpDt` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'; ");

            SQL.AppendLine("ALTER TABLE `tblpropertyinventorytransferhdr` ");
            SQL.AppendLine("    CHANGE COLUMN `RegistrationBasis` `RegistrationBasis` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci'; ");

            SQL.AppendLine("ALTER TABLE `tblpropertyinventoryhdr` ");
            SQL.AppendLine("    CHANGE COLUMN `District` `District` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' , ");
            SQL.AppendLine("    CHANGE COLUMN `Village` `Village` VARCHAR(1000) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            var cml = new List<MySqlCommand>();
            cml.Add(cm);

            Sm.ExecCommands(cml);

            cml.Clear();
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;
            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsPropertyInventoryAllowToUploadFile','FormatFTPClient', 'FileSizeMaxUploadFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'PasswordForFTPClient', 'UsernameForFTPClient', 'SharedFolderForFTPClient', 'HostAddrForFTPClient'");

            SQL.AppendLine(");");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            case "IsPropertyInventoryAllowToUploadFile": mIsPropertyInventoryAllowToUploadFile = ParValue == "Y"; break;

                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }

            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
        }

        private void BtnParent_Click(object sender, EventArgs e)
        {
            if (TxtParent.Text.Length > 0)
            {
                var f = new FrmPropertyInventory(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPropertyCode = TxtParent.Text;
                f.ShowDialog();
            }
        }

        private void BtnPropertyMutationDocNo_Click(object sender, EventArgs e)
        {
            if (TxtPropertyMutationDocNo.Text.Length > 0)
            {
                var f = new FrmPropertyInventoryMutation(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPropertyMutationDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnSiteCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmPropertyInventoryDlg2(this));
        }

        private void BtnCCCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmPropertyInventoryDlg3(this));
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmPropertyInventoryDlg(this));
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmPropertyInventoryDlg4(this));
        }

        public static void SetLuePropertyCategory(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PropertyCategoryCode As Col1, PropertyCategoryName As Col2 From TblPropertyInventoryCategory where actind = 'Y' and InitialCategoryInd = 'Y' " +
                "Order By PropertyCategoryName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLuePropertyCategory(ref LookUpEdit Lue, string PropertyCategoryCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PropertyCategoryCode As Col1, A.PropertyCategoryName As Col2 ");
            SQL.AppendLine("From TblPropertyInventoryCategory A  ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (BtnSave.Enabled)
                SQL.AppendLine(" And InitialCategoryInd = 'Y'  ");
            if (PropertyCategoryCode.Length > 0)
                SQL.AppendLine("And A.PropertyCategoryCode = @PropertyCategoryCode ");
            SQL.AppendLine("Order By A.PropertyCategoryName ");

            cm.CommandText = SQL.ToString();
            if (PropertyCategoryCode.Length > 0) Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", PropertyCategoryCode);

            Sm.SetLue2(
                ref Lue,
                ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PropertyCategoryCode.Length > 0) Sm.SetLue(Lue, PropertyCategoryCode);
        }


        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string PropertyCode, int Row, string FileName)
        {
            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePropertyInventoryFile(PropertyCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsPropertyInventoryAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd5, Row, 1) != Sm.GetGrdStr(Grd5, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblPropertyInventoryDtl ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #endregion

        #region Events
        #region Misc Control Events
        private void LuePropertyCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePropertyCategory, new Sm.RefreshLue1(SetLuePropertyCategory));
            GetCOA();

            //mInitialCategory = Sm.GetValue("SELECT A.initialcategoryind   FROM tblpropertyinventorycategory A WHERE A.PropertyCategoryCode = @Param ", Sm.GetLue(LuePropertyCategory));

            //if (mInitialCategory == "Y")
                
            //else
            //    TxtInventoryQty.EditValue = 0;


            // Default Value 
            TxtPropertyInventoryValue.EditValue = 0;
            TxtInventoryQty.EditValue = 1;
            TxtUPrice.EditValue = Decimal.Parse(TxtPropertyInventoryValue.Text) / Decimal.Parse(TxtInventoryQty.Text);
            TxtRemStockQty.EditValue = Decimal.Parse(TxtInventoryQty.Text);
            TxtRemStockValue.EditValue = Decimal.Parse(TxtRemStockQty.Text) * Decimal.Parse(TxtUPrice.Text);

            //Decimal.Parse(TxtIndicativePotentialArea.Text)
        }

        private void LueProvince_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProvince, new Sm.RefreshLue1(Sl.SetLueProvCode));
            }
        }

        private void LueCity_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCity, new Sm.RefreshLue1(Sl.SetLueCityCode));
            }
        }

        private void LueAquisitionType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcquisitionType, new Sm.RefreshLue2(Sl.SetLueOption), "AcquisitionType");
        }

        private void TxtCancelReason_Validated(object sender, EventArgs e)
        {

        }

        #endregion

        #region Grid Method

        #region Grd1
        protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPropertyInventoryCost(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPropertyInventoryCost(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                var test = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        #endregion

        #region Grd2

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 7).Length > 0)
                {
                    var f = new FrmPropertyInventoryMutation(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 7);
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 7).Length > 0)
                {
                    var f = new FrmPropertyInventoryMutation(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 7);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Grd3

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    var f = new FrmPropertyInventoryTransfer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    var f = new FrmPropertyInventoryTransfer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Grd5
        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd5.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }
        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
                if (Sm.GetGrdStr(Grd5, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd5, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd5, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd5, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }
        private void Grd5_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtPropertyCode.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
                Sm.GrdEnter(Grd5, e);
                Sm.GrdTabInLastCell(Grd5, e, BtnFind, BtnSave);
            }
        }
        #endregion

        #region Grd7

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.GetGrdStr(Grd7, e.RowIndex, 1).Length > 0)
            {
                var f = new FrmPropertyInventoryCost(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd7, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
        #endregion
    }
}
