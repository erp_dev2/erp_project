﻿#region Update
/*
    27/12/2022 [SET/BBT] Menu Find baru
    13/01/2023 [SET/BBT] Bug Filter Property Code
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryTransferFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPropertyInventoryTransfer mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPropertyInventoryTransferFind(FrmPropertyInventoryTransfer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
            Sl.SetLueCCCode(ref LueCCCode, mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sl.SetLueCCCode(ref LueCCCode2);
            Sl.SetLueLocCode(ref LueSiteCode);
            Sl.SetLueLocCode(ref LueSiteCode2);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Case when A.Status = 'O' Then 'Outstanding' when A.Status = 'A' Then 'Approved' Else 'Cancel' End As Status, A.CancelInd, A.DocDt, A.DocNo As PropertyTransferDocNo, D.CCName As CCNameFrom, E.CCName As CCNameTo, F.SiteName SiteNameFrom, ");
            SQL.AppendLine("G.SiteName SiteNameTo, B.PropertyCode, H.PropertyName, H.DisplayName, I.PropertyCategoryName PropertyCategoryFrom, J.PropertyCategoryName PropertyCategoryTo, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join tblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join tblPropertyInventoryHdr C On B.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCodeFrom  = D.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On A.CCCodeTo = E.CCCode ");
            SQL.AppendLine("Inner Join Tblsite F On A.SiteCodeFrom = F.SiteCode ");
            SQL.AppendLine("Inner Join Tblsite G On A.SiteCodeTo = G.SiteCode ");
            SQL.AppendLine("INNER JOIN tblpropertyinventoryhdr H ON B.PropertyCode = H.PropertyCode ");
            SQL.AppendLine("INNER JOIN tblpropertyinventoryCategory I ON A.PropertyCategoryCodeFrom = I.PropertyCategoryCode ");
            SQL.AppendLine("INNER JOIN tblpropertyinventoryCategory J ON A.PropertyCategoryCodeTo = J.PropertyCategoryCode ");
            //SQL.AppendLine("Left Join TblLocation H On A.LocCodeTo = H.LocCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Status",
                    "Cancel",
                    "Date",
                    "Property Transfer#",

                    //6-10
                    "Property Category From",
                    "Property Category To",
                    "Cost Center From",
                    "Cost Center To",
                    "Site From",

                    //11-15
                    "Site To",
                    "Property Code",
                    "",
                    "Property Name",
                    "Display Name",
                    
                    //16-19
                    "Remark",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 

                    //21-22
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[]
                {
                    //0
                    30,

                    //1-5
                    120, 100, 60, 80, 120, 

                    //6-10
                    200, 200, 200, 200, 200,  

                    //11-15
                    200, 100, 20, 120, 200,  

                    //16-20
                    200, 100, 100, 100, 100, 

                    //21-22
                    100, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 13 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            //if (!mFrmParent.mIsAssetTransferUseLocation)
            //    Sm.GrdColInvisible(Grd1, new int[] { 7, 8 } );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCodeFrom", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode2), "A.CCCodeTo", true);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "A.LocCodeFrom", true);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode2), "A.LocCodeTo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyCode.Text, new string[] { "B.PropertyCode", "H.PropertyName", "H.DisplayName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, B.DNo",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "Status", "CancelInd", "DocDt", "PropertyTransferDocNo", "CCNameFrom",   
                            
                            //6-10
                            "CCnameTo", "SiteNameFrom", "SiteNameTo", "PropertyCode", "PropertyName", 
                            
                            //11-15
                            "DisplayName", "Remark", "PropertyCategoryFrom", "PropertyCategoryTo", "CreateBy", 
                            
                            //16
                            "CreateDt", "LastUpBy", "LastUpDt"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);

                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 17);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 22, 18);
                        }, true, true, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center From");
        }

        private void ChkCCCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center To");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(Sl.SetLueCCCode), mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode2, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPropertyCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }

        private void TxtPropertyCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueLocCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode2, new Sm.RefreshLue1(Sl.SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location From");
        }

        private void ChkLocCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location To");
        }

        #endregion

        #endregion

    }
}
