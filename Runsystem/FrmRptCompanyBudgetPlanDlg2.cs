﻿#region Update
/*
    03/01/2022 [WED/PHT] new dialog cost center parent
    17/05/2022 [DITA/PHT] ubah menjadi directorate group
    16/06/2022 [DITA/PHT] hide kolom NotParentInd
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyBudgetPlanDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private byte mDocType = 0;
        private FrmRptCompanyBudgetPlan mFrmParent;

        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlanDlg2(FrmRptCompanyBudgetPlan FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-3
                        "Directorate Group"+Environment.NewLine+ "Code",
                        "Directorate Group",
                        "NotParentInd"
                    },
                    new int[]
                    {
                        //0
                        60,

                        //1-3
                        120, 250, 0
                    }
                );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3});
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DirectorateGroupCode, DirectorateGroupName, NotParentInd ");
            SQL.AppendLine("From TblDirectorateGroup ");
            SQL.AppendLine("Where ActInd = 'Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And  0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDirectorateGroupCode.Text, new string[] { "DirectorateGroupCode", "DirectorateGroupName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By DirectorateGroupName;",
                    new string[] { "DirectorateGroupCode", "DirectorateGroupName", "NotParentInd"  },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3) == "Y")
                    mFrmParent.mDirectorateGroupCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                else
                    mFrmParent.mDirectorateGroupCode = Sm.GetValue("Select Distinct Group_Concat(DirectorateGroupCode) From TblDirectorateGroup Where ParentCode like Concat(@Param, '%') And NotParentInd = 'Y' ", Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.TxtDirectorateGroup.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2)+" - "+ Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                this.Close();
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDirectorateGroupCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDirectorateGroupCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Directorate Group");
        }

        #endregion

        #endregion
    }
}