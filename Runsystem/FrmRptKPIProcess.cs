﻿#region Update
/*
    22/05/2018 [HAR] bug saat akumulasi nilai bulanan
    06/06/2018 [HAR] bug KPI process cancel masih dikalkulasi
    17/07/2018 [HAR] feedback achievment dengan nilai 0 tetep dimunculin
    18/12/2018 [WED] tambah filter site dan employee
    19/12/2018 [WED] KPI Name tidak mandatory
    22/01/2018 [HAR] tambah validasi year
    25/03/2021 [ICA/TWC] memunculkan nilai Achievement 
    30/12/2021 [DEV/ALL] Penambahan kolom "Maximum/Minimum" dengan parameter IsKPIUseMaxMin
 */
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptKPIProcess : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, misKPIUseMaxMin = false;
        #endregion

        #region Constructor

        public FrmRptKPIProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetLueKPICode(ref LueKPI);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                var CurrentDate = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDate.Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Z.DocNo, Z.KpiName, Z.Dno, Z.result, Z.uom, Z.Type, Z.Target, SUM(Z.Mth01) As Mth01, ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("    Z.OptDesc, ");
            else
                SQL.AppendLine("    Null As OptDesc, ");
            SQL.AppendLine("SUM(Z.Mth02) Mth02, SUM(Z.Mth03) Mth03, SUM(Z.Mth04) Mth04, SUM(Z.Mth05) Mth05,  ");
            SQL.AppendLine("SUM(Z.Mth06) Mth06, SUM(Z.Mth07) Mth07, SUM(Z.Mth08) Mth08, SUM(Z.Mth09) Mth09,  ");
            SQL.AppendLine("SUM(Z.Mth10) Mth10, SUM(Z.Mth11) Mth11, SUM(Z.Mth12) Mth12 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select distinct X.DocNo, X.KpiName,  X.Dno, X.result, X.uom, X.Type, X.Target, ");
            if (misKPIUseMaxMin)
                SQL.AppendLine("    X.OptDesc, ");
            else
                SQL.AppendLine("    Null As OptDesc, ");
            SQL.AppendLine("    Case X.Mth When '01' Then X.Val Else 0.0000 End As Mth01,  ");
            SQL.AppendLine("    Case X.Mth When '02' Then X.Val Else 0.0000 End As Mth02,  ");
            SQL.AppendLine("    Case X.Mth When '03' Then X.Val Else 0.0000 End As Mth03,  ");
            SQL.AppendLine("    Case X.Mth When '04' Then X.Val Else 0.0000 End As Mth04,  ");
            SQL.AppendLine("    Case X.Mth When '05' Then X.Val Else 0.0000 End As Mth05,  ");
            SQL.AppendLine("    Case X.Mth When '06' Then X.Val Else 0.0000 End As Mth06,  ");
            SQL.AppendLine("    Case X.Mth When '07' Then X.Val Else 0.0000 End As Mth07,  ");
            SQL.AppendLine("    Case X.Mth When '08' Then X.Val Else 0.0000 End As Mth08,  ");
            SQL.AppendLine("    Case X.Mth When '09' Then X.Val Else 0.0000 End As Mth09,  ");
            SQL.AppendLine("    Case X.Mth When '10' Then X.Val Else 0.0000 End As Mth10,  ");
            SQL.AppendLine("    Case X.Mth When '11' Then X.Val Else 0.0000 End As Mth11,  ");
            SQL.AppendLine("    Case X.Mth When '12' Then X.Val Else 0.0000 End As Mth12   ");
            SQL.AppendLine("    From ( ");
		    SQL.AppendLine("        Select distinct A.DocNo, A.KpiName,  B.Dno, B.result, B.uom, B.type, B.Target,  "); 
            if (misKPIUseMaxMin)
                SQL.AppendLine("    E.OptDesc, ");
            else
                SQL.AppendLine("    Null As OptDesc, ");
            SQL.AppendLine("        Case  ");
		    SQL.AppendLine("        When B.Type = 'SUM' Then ifnull(C.Yr, 0) ");
		    SQL.AppendLine("        When B.Type = 'AVG' Then ifnull(D.Yr, 0) ");
		    SQL.AppendLine("        End As Yr, ");
		    SQL.AppendLine("        Case  ");
		    SQL.AppendLine("        When B.Type = 'SUM' Then ifnull(C.Mth, 0) ");
		    SQL.AppendLine("        When B.Type = 'AVG' Then ifnull(D.Mth, 0) ");
		    SQL.AppendLine("        End As Mth, ");
		    SQL.AppendLine("        Case  ");
		    SQL.AppendLine("        When B.Type = 'SUM' Then ifnull(C.Val, 0) ");
		    SQL.AppendLine("        When B.Type = 'AVG' Then ifnull(D.Val, 0) ");
		    SQL.AppendLine("        End As Val ");
		    SQL.AppendLine("        From TblKPIHdr A  ");
		    SQL.AppendLine("        Inner Join tblKPIDtl B On A.DocNo= B.DocNo ");
            if (Sm.GetLue(LueKPI).Length > 0)
            {
                SQL.AppendLine("            And A.DocNo = @DocNo ");
            }
            SQL.AppendLine("        Inner Join TblEmployee B1 On A.PICCode = B1.EmpCode ");
            
            if(TxtEmpCode.Text.Length > 0)
            {
                SQL.AppendLine("            And (B1.EmpCode Like @EmpCode Or B1.EmpName Like @EmpCode) ");
            }

            if(Sm.GetLue(LueSiteCode).Length > 0)
            {
                SQL.AppendLine("            And B1.SiteCode = @SiteCode ");
                if(mIsFilterBySiteHR)
                {
                    SQL.AppendLine("            And B1.SiteCode Is Not Null ");
                    SQL.AppendLine("            And Exists( ");
                    SQL.AppendLine("                Select 1 From TblGroupSite ");
                    SQL.AppendLine("                Where SiteCode=IfNull(B1.SiteCode, '') ");
                    SQL.AppendLine("                And GrpCode In ( ");
                    SQL.AppendLine("                    Select GrpCode From TblUser ");
                    SQL.AppendLine("                    Where UserCode=@UserCode ");
                    SQL.AppendLine("                ) ");
                    SQL.AppendLine("            ) ");
                }
            }

		    SQL.AppendLine("        Left Join  ");
		    SQL.AppendLine("        ( ");
		    SQL.AppendLine("            Select 'SUM' As Func, A.kpiDocNo, B.KPIDno, Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As mth, SUM(Value) As Val  ");
			SQL.AppendLine("            from tblkpiProcesshdr A  ");
			SQL.AppendLine("            Inner Join tblKpiProcessDtl b On  A.doCno = B.DocNo ");
            if (Sm.GetLue(LueKPI).Length > 0) SQL.AppendLine("                And A.KPIDocNo = @DocNo ");
            SQL.AppendLine("            Inner Join TblKpiHdr C On A.KPIDocnO = C.DocNO And C.Yr = @Yr   ");
            SQL.AppendLine("            Where A.Cancelind = 'N' "); //And Left(A.DocDt, 4) = @Yr
			SQL.AppendLine("            Group By A.kpiDocNo, B.KPIDno, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");
		    SQL.AppendLine("        )C On A.Docno = C.KPIDOcno And B.Dno = C.KPIDno ");
		    SQL.AppendLine("        Left Join  ");
		    SQL.AppendLine("        ( ");
			SQL.AppendLine("            Select 'AVG' As Func, A.kpiDocNo, B.KPIDno, Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As mth, AVG(Value) As Val  ");
			SQL.AppendLine("            from tblkpiProcesshdr A  ");
            SQL.AppendLine("            Inner Join tblKpiProcessDtl b On  A.doCno = B.DocNo ");
            if (Sm.GetLue(LueKPI).Length > 0) SQL.AppendLine("                And A.KPIDocNo = @DocNo ");
            SQL.AppendLine("            Inner Join TblKpiHdr C On A.KPIDocnO = C.DocNO And C.Yr = @Yr   ");
            SQL.AppendLine("            Where A.Cancelind = 'N' "); //And Left(A.DocDt, 4) = @Yr
			SQL.AppendLine("            Group By A.kpiDocNo, B.KPIDno, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");
		    SQL.AppendLine("        )D On A.Docno = D.KPIDOcno And B.Dno = D.KPIDno ");
            if(misKPIUseMaxMin) SQL.AppendLine("  Left Join TblOption E On B.MaxMin= E.OptCode AND E.OptCat='KPIMaxMin'  ");
            SQL.AppendLine("        Where A.ActInd = 'Y' And A.Yr=@Yr  ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine(")Z  ");          
           
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd1, 0, 0, "No", 2, 40);
            SetGrdHdr(ref Grd1, 0, 1, "KPI Name", 2, 140);
            SetGrdHdr(ref Grd1, 0, 2, "Result "+Environment.NewLine+" Indicator", 2, 200);
            SetGrdHdr(ref Grd1, 0, 3, "UOM", 2, 80);
            SetGrdHdr(ref Grd1, 0, 4, "Type", 2, 80);
            SetGrdHdr(ref Grd1, 0, 5, "Achievement", 2, 150);
            SetGrdHdr2(ref Grd1, 1, 6, "Month", 12);
            SetGrdHdr(ref Grd1, 0, 18, "Standart", 2, 140);
            SetGrdHdr(ref Grd1, 0, 19, "Maximum/Minimum", 2, 120);

            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 19 }, false);
            Grd1.Cols[18].Move(4);            
            if (misKPIUseMaxMin)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, true);
                Grd1.Cols[19].Move(5);
            }
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "January", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "February", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "Maret", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "April", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 4, "May", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 5, "June", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 6, "July", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 7, "August", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 8, "September", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 9, "October", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 10, "November", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 11, "December", 1, 80);
        }
    
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " Where 0=0 ";

                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueKPI), "Z.DocNo", true);
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetLue(LueKPI));
                Sm.CmParam<String>(ref cm, "@EmpCode", string.Concat("%", TxtEmpCode.Text, "%"));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Group By Z.DocNo, Z.KpiName, Z.Dno, Z.result, Z.uom, Z.Type, Z.Target; ", //" Group By Z.DocNo, Z.Dno, Z.result, Z.uom, Z.Type; ",
                        new string[]
                        {
                            //0
                            "KpiName", 
                            //1-5
                            "Result","uom", "type", "Mth01", "Mth02",
                            //6-10
                            "Mth03", "Mth04", "Mth05", "Mth06", "Mth07",
                            //11-15
                            "Mth08", "Mth09", "Mth10", "Mth11", "Mth12",
                            //16-17
                            "target", "OptDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 17);
                            ComputeAchieve(Row);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        public static void SetLueKPICode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DocNo As Col1, KPIName As Col2 From TblKPIHdr Where ActInd = 'Y' Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            misKPIUseMaxMin = Sm.GetParameterBoo("IsKPIUseMaxMin");
        }

        public void ComputeAchieve(int Row)
        {
            try
            {
                if (Sm.GetGrdStr(Grd1, Row, 4) == "SUM")
                {
                    for (int Col = 6; Col <= 17; Col++)
                    {
                        decimal sum = Sm.GetGrdDec(Grd1, Row, 5);
                         Grd1.Cells[Row, 5].Value = Sm.FormatNum((sum + Sm.GetGrdDec(Grd1, Row, Col)), 0);
                    }
                }
                else
                {
                    int avg = 0;
                    decimal AvgTotal = 0;
                    for (int Col = 6; Col <= 17; Col++)
                    {
                          avg = avg + 1;
                            AvgTotal = AvgTotal + Sm.GetGrdDec(Grd1, Row, Col);
                    }
                    if (avg > 0)
                    {
                        Grd1.Cells[Row, 5].Value = Sm.FormatNum((AvgTotal / avg), 0);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion 

        #endregion

        #region Event

        private void LueKPI_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueKPI, new Sm.RefreshLue1(SetLueKPICode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkKPI_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "KPI Name");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion
    }
}
