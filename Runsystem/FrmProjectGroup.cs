﻿#region Update
/*
    23/09/2019 [TKG/IMS] new application
    23/10/2019 [VIN/IMS] Pembuatan menu master proyek
    28/05/2021 [IBL/IMS] Penambahan check InternalInd berdasarkan parameter IsProjectGroupUseInternalInd
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmProjectGroupFind FrmFind;
        internal bool mIsProjectGroupUseInternalInd = false;

        
        #endregion

        #region Constructor

        public FrmProjectGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                if (!mIsProjectGroupUseInternalInd)
                {
                    ChkInternalInd.Visible = false;
                }
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtPGCode, TxtPGName, ChkActInd, TxtProjectCode, MeeProjectName, LueCtCode }, true);
                    TxtPGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPGCode, TxtPGName, ChkActInd, TxtProjectCode, MeeProjectName, LueCtCode }, false);
                    TxtPGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPGName, ChkActInd, TxtProjectCode, MeeProjectName, LueCtCode }, false);
                    TxtPGName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPGCode, TxtPGName, TxtProjectCode, MeeProjectName, LueCtCode });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPGCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPGCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblProjectGroup Where PGCode=@PGCode" };
                Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Insert Into TblProjectGroup(PGCode, PGName, ActInd, ProjectCode, ProjectName, CtCode, InternalInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PGCode, @PGName, @ActInd, @ProjectCode, @ProjectName, @CtCode, @InternalInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ActInd=@ActInd, PGName=@PGName, ProjectCode=@ProjectCode, ProjectName=@ProjectName, CtCode=@CtCode, InternalInd=@InternalInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
                Sm.CmParam<String>(ref cm, "@PGName", TxtPGName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ProjectCode", TxtProjectCode.Text);
                Sm.CmParam<String>(ref cm, "@ProjectName", MeeProjectName.Text);
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@InternalInd", ChkInternalInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPGCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        internal void ShowData(string PGCode)
        {
            try
            {
                ClearData();
                ShowPG(PGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPG(string PGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PGCode", PGCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select PGCode, PGName, ActInd, ProjectCode, ProjectName, CtCode, InternalInd From TblProjectGroup Where PGCode=@PGCode;",
                    new string[] {
                        //0
                        "PGCode", 
                        //1-5
                        "PGName", "ActInd", "ProjectCode" , "ProjectName", "CtCode",

                        //6
                        "InternalInd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPGName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[3]);
                        MeeProjectName.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        ChkInternalInd.Checked = Sm.DrStr(dr, c[6]) == "Y";
                    }, true
                );
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPGCode, "Project group's code", false) ||
                Sm.IsTxtEmpty(TxtPGName, "Project group's name", false) ||
                IsPGExisted() ||
                IsDataInactiveAlready();

        }

        private bool IsPGExisted()
        {
            if (!TxtPGCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select 1 From TblProjectGroup Where PGCode=@Param Limit 1;", TxtPGCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Project group code ( " + TxtPGCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblProjectGroup Where ActInd='N' And PGCode=@Param;",
                TxtPGCode.Text,
                "This Project group already not actived.");
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsProjectGroupUseInternalInd = Sm.GetParameterBoo("IsProjectGroupUseInternalInd");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPGCode);
        }

        private void TxtPGName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPGName);
        }
        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtProjectCode);
        }
        private void MeeProjectName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeTrim(MeeProjectName);
        }
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
        }

        
        #endregion

        
        #endregion
    
    }
}
