﻿#region Update
/*
    11/12/2019 [WED/IMS] new apps
    20/02/2020 [TKG/IMS] Tambah filter menampilkan data dari outstanding material request
    23/04/2020 [IBL/IMS] Tambah filter Document# dan tambah kolom Document# di grid
    24/04/2020 [TKG/IMS] ubah query untuk menampilkan informasi project dan Customer's PO#
    28/09/2020 [TKG/IMS] auto select jetika double klik judulnya
    10/06/2021 [VIN/IMS] item yang sudah PO tidak bisa ditarik lagi 
    11/06/2021 [VIN/IMS] bug item tidak muncul
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorItemCategoryGroupDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVendorItemCategoryGroup mFrmParent;

        #endregion

        #region Constructor

        public FrmVendorItemCategoryGroupDlg2(FrmVendorItemCategoryGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Item's Code",
                    "Local Code",
                    "Item's Name",
                    "Foreign Name",

                    //6-10
                    "Vendor Collection#",
                    "Vendor Item Colection Dno",
                    "Vendor Code Colection Dno",
                    "Specification",
                    "Project's Code",

                    //11-14
                    "Project's Name",
                    "Quantity",
                    "UoM",
                    "Document#"
                },
                new int[] 
                {
                    50,
                    20, 120, 120, 250, 200, 
                    180, 80, 80, 300, 100, 
                    200, 100, 100, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[14].Move(5);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            string CurrentDateTime2 = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Select A.DocNo, B.Dno, B.MRDocNo, C.Dno2, A.DocDt, C.VdCode, B.ItCode, D.ItName, E.ItCtname, D.ItCodeInternal, D.Foreignname, D.Specification, ");
            SQL.AppendLine("L.ProjectCode, L.ProjectName, I.PONo, G.Qty, D.InventoryUomCode Uom ");
            SQL.AppendLine("From TblItemVendorCollectionhdr A ");
            SQL.AppendLine("Inner Join TblItemVendorCollectionDtl B On A.Docno = B.Docno ");
            if (ChkOutstandingMaterialRequest.Checked)
            {
                SQL.AppendLine("And B.ItCode In ( ");
                SQL.AppendLine("    Select Distinct ItCode From (");
                SQL.AppendLine("        Select X2.ItCode, X2.Qty-IfNull(X3.Qty, 0)-IfNull(X4.Qty, 0) As Qty ");
                SQL.AppendLine("        From TblMaterialRequestHdr X1 ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl X2 ");
                SQL.AppendLine("            On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            And IfNull(X2.Status,'X')='A' ");
                SQL.AppendLine("            And X2.CancelInd='N' ");
                SQL.AppendLine("            And IfNull(X2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("            And X2.TenderDocNo Is Null ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.MRDocNo As DocNo, T1.MRDNo As DNo, Sum(T1.Qty) Qty ");
                SQL.AppendLine("            From TblMRQtyCancel T1 ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                SQL.AppendLine("                On T1.MRDocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.MRDNo=T2.DNo ");
                SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                SQL.AppendLine("            Where T1.CancelInd = 'N' ");
                SQL.AppendLine("            Group By T1.MRDocNo, T1.MRDNo ");
                SQL.AppendLine("        ) X3 On X2.DocNo=X3.DocNo And X2.DNo=X3.DNo ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select MaterialRequestDocNo As DocNo, T1.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
                SQL.AppendLine("            From TblPORequestDtl T1 ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                SQL.AppendLine("                On T1.MaterialRequestDocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.MaterialRequestDNo=T2.DNo ");
                SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                SQL.AppendLine("                And T2.CancelInd='N' ");
                SQL.AppendLine("            Where T1.CancelInd='N' And T1.Status<>'C' ");
                SQL.AppendLine("            Group By T1.MaterialRequestDocNo, T1.MaterialRequestDNo ");
                SQL.AppendLine("        ) X4 On X2.DocNo=X4.DocNo And X2.DNo=X4.DNo ");
                SQL.AppendLine("    ) X Where Qty>0.00 ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblItemVendorCollectionDtl2 C ");
            SQL.AppendLine("    On B.Docno=C.DocNo ");
            SQL.AppendLine("    And B.Dno=C.Dno  ");
            SQL.AppendLine("    And Not Find_In_Set(Concat(A.DocNo, B.DNo, C.DNo2), @SelectedItemVendorCollection) ");
            //SQL.AppendLine("    And C.VdCode=@VdCode ");
            SQL.AppendLine("    And Concat(C.VdCode, C.DocNo, C.Dno, C.Dno2) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(A.VdCode, B.itemVendorCollectionDocNo, B.itemVendorCollectionDno, B.ItemVendorCollectionDno2) As KeyWord ");
            SQL.AppendLine("        From tblvendoritctgroupHdr A ");
            SQL.AppendLine("        Inner Join TblVendorItCtGroupDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.itCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode = E.itCtCode ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.ItCode, C.VdCode, ");
            SQL.AppendLine("    Max(Concat(A.DocDt, A.DocNo, B.DNo, C.Dno2)) As Key1   ");
            SQL.AppendLine("    From TblItemVendorCollectionhdr A ");
            SQL.AppendLine("    Inner Join TblItemVendorCollectionDtl B On A.Docno = B.Docno And B.Cancelind = 'N' ");
            SQL.AppendLine("    Inner Join TblItemVendorCollectionDtl2 C on B.Docno = C.DocNo And B.Dno = C.Dno And C.VdCode = @VdCode ");
            SQL.AppendLine("    Where A.DocDt <= @DocDt And ((A.ExpiredDt is not null and expiredDt>=@DocDt) Or A.ExpiredDt is null)   ");
            SQL.AppendLine("    Group By B.ItCode, C.VdCode ");
            SQL.AppendLine(")F On Concat(A.DocDt, B.DocNo, B.DNo, C.Dno2)=F.Key1 And C.VdCode=F.VdCode And B.ItCode=F.ItCode  ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On B.MRDocNo = G.DocNo And B.MRDNo = G.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr H On G.DocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblSOContractHdr I On H.SOCDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblBOQHdr J On I.BoqDocNo=J.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr K On J.LopDocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup L on K.PGCode=L.PGCode ");
            SQL.AppendLine("Where CONCAT(B.ItCode, G.DocNo, G.DNo) NOT IN  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT CONCAT(E.ItCode, D.DocNo, D.DNo) FROM TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo   ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode  ");
            SQL.AppendLine("    WHERE A.VdCode=@VdCode ");
            SQL.AppendLine(") ");
            SQL.AppendLine(" Order By D.ItName;");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedItemVendorCollection", mFrmParent.GetSelectedItemVendorCollection());
                Sm.CmParam<String>(ref cm, "@VdCode", mFrmParent.mVdCode);
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(mFrmParent.DteDocDt));
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName", "D.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, new string[] { "B.MRDocNO" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                    { 
                        "ItCode", 
                        "ItCodeInternal", "ItName", "ForeignName", "DocNo", "Dno",  
                        "Dno2", "Specification", "ProjectCode", "ProjectName", "PONo",
                        "Qty", "Uom", "MRDocNo"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 13);

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = string.Concat(Sm.GetGrdStr(Grd1, Row, 6), Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row, 8));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(mFrmParent.Grd1, Index, 6), Sm.GetGrdStr(mFrmParent.Grd1, Index, 7)), ItCode))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r= 0; r< Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

    }
}
