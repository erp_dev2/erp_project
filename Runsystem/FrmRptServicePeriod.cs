﻿#region Update
/*
    01/11/2019 [HAR/SIER] : RewardPeriodYr order by nilai terkceil 
    03/06/2020 [DITA/SIER] : tambah kolom YearsOfService
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptServicePeriod : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private List<Period> lPeriod = null;
        private int mFirstColumnForWhs = -1;

        #endregion

        #region Constructor

        public FrmRptServicePeriod(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DivisionName, C.DeptName, D.PosName, E.SiteName, A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("A.IDNumber, F.OptDesc As Gender, G.OptDesc As EmploymentStatus, ");
            SQL.AppendLine("Concat(TIMESTAMPDIFF(YEAR, A.JoinDt, Left(CurrentDateTime(), 8) ) , ' Year(s) ', "); 
			SQL.AppendLine("TIMESTAMPDIFF(MONTH, A.JoinDt, Left(CurrentDateTime(), 8)) % 12 ,  ' Month(s) ', ");
            SQL.AppendLine("FLOOR(TIMESTAMPDIFF(DAY, A.JoinDt, Left(CurrentDateTime(), 8)) % 30.4375), ' Day(s)') YearsOfService ");
            SQL.AppendLine("from tblemployee A ");
            SQL.AppendLine("Left Join tbldivision B On A.DivisionCode=B.DivisionCode ");
            SQL.AppendLine("Left Join tbldepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join tblposition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join tblsite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join tbloption F On F.OptCat='Gender' and A.Gender=F.OptCode ");
            SQL.AppendLine("Left Join tbloption G On G.OptCat='EmploymentStatus' and A.EmploymentStatus=G.OptCode");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                     //0
                        "No.",

                        //1-5
                        "Employee#",
                        "Employee Name", 
                        "Employee"+Environment.NewLine+"Old Code",
                        "Division",
                        "Department",
                        
                        //6-10 
                        "Position",
                        "Site",
                        "Join Date",
                        "Resign Date",
                        "ID Number",
                       
                        //11-14
                        "Gender",
                        "Employment"+Environment.NewLine+"Status",
                        "Years Of Service",
                        "JoinDt",
                        
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 130, 170, 150,  
                        
                        //6-10
                        250, 150, 100, 100, 150,   
                        
                        //11-14
                        100, 100, 200, 0

                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 8, 9 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 14 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                int colscount = 15;
                lPeriod = new List<Period>();
                SetPeriodToColumn();

                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1),Sm.GetDte(DteDocDt2),"A.JoinDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "EmpCode",

                            //1-5
                            "EmpName", "EmpCodeOld", "DivisionName", "DeptName", "PosName", 
                            
                            //6-10
                             "SiteName", "JoinDt", "ResignDt", "IDNumber", "Gender",   
                            
                            //11-12
                            "EmploymentStatus", "YearsOfService"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 7);

                            for (int cols = 0; cols < lPeriod.Count; cols++)
                            {
                                string joindt = Sm.GetGrdStr(Grd1, Row, 14);
                                DateTime DtTm1 = new DateTime(
                                Int32.Parse(joindt.Substring(0, 4)),
                                Int32.Parse(joindt.Substring(4, 2)),
                                Int32.Parse(joindt.Substring(6, 2)),
                                0, 0, 0
                                );

                                Grd1.Cells[Row, colscount + cols].Value = Convert.ToDateTime(DtTm1.AddYears(Convert.ToInt32(lPeriod[cols].OptCode)).ToString()).ToString("dd/MM/yyyy"); 
                            }

                            //for(int cols =0;cols < lWhs.Count;cols++)
                            //{
                            //    Grd1.Cells[Row, colscount + cols].Value = Sm.ConvertDate(decimal.Parse(lWhs[cols].OptCode) + decimal.Parse(Sm.Left(Sm.GetGrdStr(Grd1, Row, 13), 4)) + Sm.Right(Sm.GetGrdStr(Grd1, Row, 13), 4));
                            //}
                        }, true, false, false, false
                    );
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional

        private void SetPeriodToColumn()
        {
            lPeriod.Clear();
            Grd1.Cols.Count = 15;
            var LatestColumn = Grd1.Cols.Count - 1;
            var cm1 = new MySqlCommand();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                var PeriodColumn = new StringBuilder();
                string Filter = " ";

                PeriodColumn.AppendLine("Select OPtCode, OptDesc From tbloption Where OptCat='RewardPeriodYr' order BY CAST(Optcode As Int) ");

                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandTimeout = 600;
                cm1.CommandText = PeriodColumn.ToString();
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { "OptCode", "OptDesc" });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForWhs == -1) mFirstColumnForWhs = LatestColumn;
                        lPeriod.Add(new Period()
                        {
                            OptCode = Sm.DrStr(dr1, c1[0]),
                            OptDesc = Sm.DrStr(dr1, c1[1]),
                            Column = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr1, c1[1]);
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 120;
                        Sm.GrdFormatDate(Grd1, new int[] { LatestColumn });
                        Sm.GrdColReadOnly(true, false, Grd1, new int[] { LatestColumn });
                    }
                }
                dr1.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Join date");
        }

        #endregion

        #region class

        private class Period
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
            public int Column { get; set; }

        }

        #endregion
    }
}
