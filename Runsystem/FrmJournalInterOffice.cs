﻿#region Update
/*
    02/07/2021 [WED/PHT] new apps
    27/08/2021 [WED/PHT] COA double boleh dilakukan di detailnya
    11/02/2022 [ICA/PHT] COA kepala 9 boleh dipilih di detail, dengan validasi tidak boleh sama dengan COA di header
    18/02/2022 [WED/PHT] method PrepC1, PrepC2, PrepC3 dibuat public agar bisa di akses di Voucher dan TrnReprocessJournal
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    17/03/2022 [ISD/PHT] tambah rujukan coa inter office dengan parameter JournalInterOfficeCOASource
    20/03/2022 [TKG/PHT] tambah validasi pengecekan parent dan cost center di method PrepC1+PrepC2,
                        merubah GetParameter() dan proses save
    11/04/2022 [DITA/PHT] ubah validassi dan current date closing journal saat cancel journal interoffice
    11/05/2022 [RDA/PHT] membuat field centang di sebelah cost center 1 dan cost center 2 (untuk generate doc approval berdasarkan dept cost center yg dicentang) 
    09/06/2022 [TYO/PHT] menambah lable Aprroval di ChkCCInd & ChkCCInd2
    21/10/2022 [RDA/PHT] bug savejournal2 masih belum bisa meng-cancel journal existing
    09/01/2023 [DITA/PHT] perubahan didalam method = IsClosingJournalInvalid
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    16/01/2022 [DITA/PHT] validsi closing journal tidak melihat approval saat insert
    19/01/2023 [TKG/PHT] bug journal interoffice harus berdasarkan profit center, dll.
    31/01/2023 [VIN+TKG/PHT] bug journal cancel.
    21/03/2023 [VIN+TKG/PHT] bug journal create dno lihat tbljournalinterofficedtl3.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJournalInterOffice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocNo = string.Empty
            ;
        internal FrmJournalInterOfficeFind FrmFind;

        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mJournalDocNoFormat = string.Empty
        ;

        internal string
            mAcNoForAccountsInterOffice = string.Empty,
            mJournalInterOfficeCOASource = string.Empty
            ;

        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAutoJournalActived = false
            ;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmJournalInterOffice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Control

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Journal for Inter Office";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                GetParameter();
                TcJournalInterOffice.SelectedTabPage = Tp3;
                TcJournalInterOffice.SelectedTabPage = Tp2;
                TcJournalInterOffice.SelectedTabPage = Tp1;
                SetGrd();
                SetFormControl(mState.View);         
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                     //0
                    "DNo",

                    //1-5
                    "", 
                    "Account#", 
                    "Account",
                    "Debit", 
                    "Credit"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 180, 200, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "D No",
                    //1-4
                    "File Name",
                    "U",
                    "D",
                    "File Name2"
                },
                new int[] 
                {
                    0, 
                    250, 20, 20, 250
                }
            );

            Sm.GrdColInvisible(Grd2, new int[] { 0, 4 }, false);
            Sm.GrdColButton(Grd2, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd2, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 4 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 5;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "D No",
                    //1-4
                    "Journal#",
                    "",
                    "Journal Cancel#",
                    ""
                },
                new int[] 
                {
                    0, 
                    180, 20, 180, 20
                }
            );

            Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
            Sm.GrdColButton(Grd3, new int[] { 2, 4 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 3 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, TxtCCCode, TxtCCName, TxtCCCode2, TxtCCName2,
                        TxtAcNo, TxtAcDesc, MeeRemark, MeeCancelReason, ChkCancelInd, ChkCCInd, ChkCCInd2
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 4 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 3 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 2, 4 });
                    BtnCCCode1.Enabled = false;
                    BtnCCCode2.Enabled = false;
                    BtnAcNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, ChkCCInd, ChkCCInd2
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 4, 5 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2 });
                    BtnCCCode1.Enabled = true;
                    BtnCCCode2.Enabled = true;
                    BtnAcNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 3 });
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd
                    }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtCCCode, TxtCCName, TxtCCCode2, TxtCCName2,
                TxtAcDesc, TxtAcNo, MeeRemark, MeeCancelReason, TxtStatus
            });
            ChkCancelInd.Checked = false;
            ChkCCInd.Checked = false;
            ChkCCInd2.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5 });
            Sm.FocusGrd(Grd1, 0, 0);

            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);

            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJournalInterOfficeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        #region Grid 1

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                Sm.FormShowDialog(new FrmJournalInterOfficeDlg2(this, false));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4, 5 }, e);

                if (e.ColIndex == 4)
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length != 0) Grd1.Cells[e.RowIndex, 5].Value = 0m;
                }

                if (e.ColIndex == 5)
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length != 0) Grd1.Cells[e.RowIndex, 4].Value = 0m;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd2.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd2_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 3

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, e.ColIndex - 1, false, "Journal# is empty."))
                {
                    ShowJournalTransaction(e.RowIndex, e.ColIndex - 1);
                }
            }

            if (e.ColIndex == 4)
            {
                if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, e.ColIndex - 1, false, "Journal cancel# is empty."))
                {
                    ShowJournalTransaction(e.RowIndex, e.ColIndex - 1);
                }
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            bool IsFileExists = Grd2.Rows.Count > 1;

            if (!IsFileExists)
            {
                if (Sm.StdMsgYN("Question", "You have no file to upload. Do you wish to continue ?") == DialogResult.No)
                {
                    TcJournalInterOffice.SelectedTabPage = Tp2;
                    return;
                }
            }

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "JournalInterOffice", "TblJournalInterOfficeHdr");
            bool IsApprovalExists = GetApprovalInfo();
            var code1 = Sm.GetCode1ForJournalDocNo("FrmJournalInterOffice", string.Empty, string.Empty, mJournalDocNoFormat);
            //var code2 = code1 == "K" ? "M" : "K";

            var cml = new List<MySqlCommand>();

            cml.Add(SaveJournalInterOfficeHdr(DocNo));
            cml.Add(SaveJournalInterOfficeDtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; ++Row)
            //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
            //        cml.Add(SaveJournalInterOfficeDtl(DocNo, Row));

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 1).Length > 0 && Sm.GetGrdStr(Grd2, i, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(i, Sm.GetGrdStr(Grd2, i, 1))) return;
                        cml.Add(SaveJournalInterOfficeDtl2(DocNo, i));
                    }
                }
            }
            if (mIsClosingJournalBasedOnMultiProfitCenter)
            {
                var lProfitCenterCode = new List<string>();
                GetProfitCenterCode(ref lProfitCenterCode);
                foreach (var x in lProfitCenterCode)
                {
                    if (Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), x))
                    {
                        cml.Clear();
                        return;
                    }
                }
                lProfitCenterCode.Clear();
            }

            if (mIsAutoJournalActived && !IsApprovalExists)
            {
                var l1 = new List<CC1>();
                var l2 = new List<CC2>();
                var l3 = new List<CCSeq>();

                PrepC1(ref l1, TxtCCCode.Text);
                PrepC2(ref l2, TxtCCCode2.Text);
                PrepSeq(ref l1, ref l2, ref l3);
                
                if (l3.Count > 0)
                {
                    
                    int val = 0;
                    string JNDocNo = string.Empty;

                    Grd3.BeginUpdate();

                    for (int i = 0; i < l3.Count; ++i)
                    {
                        //var code3 = code1;                        
                        var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", l3[i].CCCode);

                        if (i == 0 || i == l3.Count - 1)
                        {
                            val += 1;
                            //if (i != 0) code3 = code2;
                            if (mJournalDocNoFormat == "1")
                                JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);
                            else
                            {
                                // JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), val, code1, profitCenterCode, string.Empty, string.Empty, string.Empty))
                                JNDocNo = Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                            }

                            Grd3.Rows.Add();
                            Grd3.Cells[Grd3.Rows.Count - 2, 1].Value = JNDocNo;

                            cml.Add(SaveJournal(DocNo, JNDocNo, true, i, l3[i].CCCode));
                        }
                        else
                        {
                            for (int j = 1; j <= 2; ++j)
                            {
                                val += 1;
                                //if (val % 2 == 0) code3 = code2;
                                //else code3 = code1;
                                if (mJournalDocNoFormat == "1")
                                    JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);
                                else
                                {
                                    // JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), val, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                                    JNDocNo = Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                                }

                                Grd3.Rows.Add();
                                Grd3.Cells[Grd3.Rows.Count - 2, 1].Value = JNDocNo;

                                cml.Add(SaveJournal(DocNo, JNDocNo, false, j, l3[i].CCCode));
                            }
                        }
                    }

                    Grd3.EndUpdate();
                }

                l1.Clear(); l2.Clear(); l3.Clear();
            }

            //if (Grd3.Rows.Count > 1)
            //{
            //    cml.Add(SaveJournalInterOfficeDtl3(DocNo));
            //    //for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
            //    //    if (Sm.GetGrdStr(Grd3, i, 1).Length > 0) cml.Add(SaveJournalInterOfficeDtl3(DocNo, i));
            //}

            Sm.ExecCommands(cml);

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 1).Length > 0 && 
                        Sm.GetGrdStr(Grd2, i, 1) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd2, i, 1) != Sm.GetGrdStr(Grd2, i, 4))
                            UploadFile(DocNo, i, Sm.GetGrdStr(Grd2, i, 1));
                    }
                }
            }

            ShowData(DocNo);
        }

        private void GetProfitCenterCode(ref List<CCSeq> lCostCenter, ref List<string> lProfitCenterCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Query = new StringBuilder();
            int i = 0;

            foreach (var x in lCostCenter.Distinct())
            {
                Query.AppendLine(" Or CCCode=@CCCode_" + i.ToString());
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), x.CCCode);
                i++;
            }

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null And (CCCode='***' ");
            SQL.AppendLine(Query.ToString());
            SQL.AppendLine("); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read()) lProfitCenterCode.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private string GetProfitCenterCode(string CCode)
        {
            if (CCode.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter  " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    CCode);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtCCName, "Cost Center 1", false) ||
                Sm.IsTxtEmpty(TxtCCName2, "Cost Center 2", false) ||
                Sm.IsTxtEmpty(TxtAcDesc, "COA (InterOffice)", false) ||
                IsCostCenterDuplicate() ||
                IsCCIndInvalid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCOAInvalid() ||
                IsBalanceInvalid()
                ;
        }

        private bool IsCCIndInvalid()
        {
            if (!ChkCCInd.Checked && !ChkCCInd2.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Cost center check field is still empty, please choose one.");
                return true;
            }

            return false;
        }

        private bool IsCostCenterDuplicate()
        {
            if (TxtCCCode.Text == TxtCCCode2.Text)
            {
                Sm.StdMsg(mMsgType.Warning, "Duplicate cost center found.");
                TxtCCName2.Focus();
                return true;
            }

            return false;
        }

        private bool IsCOAInvalid()
        {
            for(int row = 0; row < Grd1.Rows.Count-1;row++)
            {
                if(TxtAcNo.Text == Sm.GetGrdStr(Grd1, row, 2))
                {
                    Sm.StdMsg(mMsgType.Warning, "Coa detail must be different from coa inter office.");
                    Sm.FocusGrd(Grd1, row, 3);
                    return true;
                }
            }

            #region Old

            // is in detail tab uses interoffice account

            //int charLen = mAcNoForAccountsInterOffice.Length;
            //if (charLen > 0)
            //{
            //    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            //    {
            //        if (Sm.Left(Sm.GetGrdStr(Grd1, i, 2), charLen) == mAcNoForAccountsInterOffice)
            //        {
            //            Sm.StdMsg(mMsgType.Warning, "You can't choose this account due to interoffice purposes.");
            //            Sm.FocusGrd(Grd1, i, 3);
            //            return true;
            //        }
            //    }
            //}

            //for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            //{
            //    for (int j = (i + 1); j < Grd1.Rows.Count - 1; ++j)
            //    {
            //        if (Sm.GetGrdStr(Grd1, i, 2) == Sm.GetGrdStr(Grd1, j, 2))
            //        {
            //            Sm.StdMsg(mMsgType.Warning, "Duplicate account found.");
            //            Sm.FocusGrd(Grd1, j, 3);
            //            return true;
            //        }
            //    }
            //}

            #endregion

            return false;
        }

        private bool IsBalanceInvalid()
        {
            decimal DAmt = 0m, CAmt = 0m;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                DAmt += Sm.GetGrdDec(Grd1, i, 4);
                CAmt += Sm.GetGrdDec(Grd1, i, 5);
            }

            if (DAmt != CAmt)
            {
                var mMsg = new StringBuilder();

                mMsg.AppendLine("Debit Amount : " + Sm.FormatNum(DAmt, 0));
                mMsg.AppendLine("Credit Amount :  " + Sm.FormatNum(CAmt, 0) + Environment.NewLine);
                mMsg.AppendLine("Debit and credit amount is not balance. ");

                Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "COA's account is empty.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 4) == 0m && Sm.GetGrdDec(Grd1, Row, 5) == 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Acount No : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Account Description : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "cannot be 0.");
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveJournalInterOfficeHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* Journal InterOffice (Hdr) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblJournalInterOfficeHdr(DocNo, DocDt, Status, CCCode, CCCode2, CCInd, CCInd2, AcNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @CCCode, @CCCode2, @CCInd, @CCInd2, @AcNo, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='JournalInterOffice' ");
            //SQL.AppendLine("And T.SiteCode In ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select B.SiteCode ");
            //SQL.AppendLine("    From TblCostCenter A ");
            //SQL.AppendLine("    Inner Join TblSite B On A.ProfitCenterCode = B.ProfitCenterCode ");
            //SQL.AppendLine("    Where A.CCCode = @CCCode2 ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("And T.DeptCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.DeptCode ");
            SQL.AppendLine("    From TblCostCenter A ");
            SQL.AppendLine("    Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            if(ChkCCInd.Checked)
                SQL.AppendLine("    Where A.CCCode = @CCCode ");
            else
                SQL.AppendLine("    Where A.CCCode = @CCCode2 ");
            SQL.AppendLine(") ");
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblJournalInterOfficeHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='JournalInterOffice' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
            Sm.CmParam<String>(ref cm, "@CCCode2", TxtCCCode2.Text);
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);            
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CCInd", ChkCCInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CCInd2", ChkCCInd2.Checked ? "Y" : "N");
            return cm;
        }

        private MySqlCommand SaveJournalInterOfficeDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Journal InterOffice (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblJournalInterOfficeDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveJournalInterOfficeDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblJournalInterOfficeDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveJournalInterOfficeDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Journal InterOffice (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblJournalInterOfficeDtl2(DocNo, DNo, FileName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, NULL, @UserCode, @Dt) ");
            SQL.AppendLine("On Duplicate Key Update LastUpBy=@UserCode, LastUpDt=@Dt;");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournalInterOfficeDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Journal InterOffice (Dtl3) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblJournalInterOfficeDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @JournalDocNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveJournalInterOfficeDtl3(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblJournalInterOfficeDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @JournalDocNo, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = SQL.ToString()
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveJournal(string DocNo, string JNDocNo, bool IsEdgeData, int index, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");
            SQL.AppendLine("Set @JNDocNo:=(" + JNDocNo + "); ");
            SQL.AppendLine("Set @DNo:=(Select ( ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('000', Convert(no+1, Char)), 3) From ( ");
            SQL.AppendLine("       Select Convert(DNo, Decimal) As no ");
            SQL.AppendLine("       From TblJournalInterOfficeDtl3 ");
            SQL.AppendLine("       Where DocNo=@DocNo ");
            SQL.AppendLine("       Order By DNo Desc Limit 1 ");
            SQL.AppendLine("       ) T ");
            SQL.AppendLine("   ), '001') ");
            SQL.AppendLine(")); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            SQL.AppendLine("CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JNDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Journal Inter Office : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCode As CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalInterOfficeHdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, DAmt DAmt, CAmt CAmt From ( "); //// wed : aslinya DAmt CAmt di sum

            if (IsEdgeData)
            {
                if (index == 0)
                {
                    SQL.AppendLine("Select AcNo, DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblJournalInterOfficeDtl ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And DAmt != 0.00 And CAmt = 0.00 ");
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select T1.AcNo, 0.00 As DAmt, T2.CAmt CAmt "); // wed : aslinya T2.CAmt di sum
                    SQL.AppendLine("From TblJournalInterOfficeHdr T1 ");
                    SQL.AppendLine("Inner Join TblJournalInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("Where T1.DocNo = @DocNo ");
                    SQL.AppendLine("And T2.DAmt = 0.00 And T2.CAmt != 0.00 ");
                    //SQL.AppendLine("Group By T1.AcNo ");
                }
                else // data index terakhir
                {
                    SQL.AppendLine("Select T1.AcNo, T2.DAmt DAmt, 0.00 As CAmt "); // wed : aslinya T2.DAmt di sum
                    SQL.AppendLine("From TblJournalInterOfficeHdr T1 ");
                    SQL.AppendLine("Inner Join TblJournalInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("Where T1.DocNo = @DocNo ");
                    SQL.AppendLine("And T2.DAmt != 0.00 And T2.CAmt = 0.00 ");
                    //SQL.AppendLine("Group By T1.AcNo ");
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select AcNo, 0.00 As DAmt, CAmt ");
                    SQL.AppendLine("From TblJournalInterOfficeDtl ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And DAmt = 0.00 And CAmt != 0.00 ");
                    
                }
            }
            else
            {
                if (index % 2 == 0)
                {
                    SQL.AppendLine("Select AcNo, CAmt As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblJournalInterOfficeDtl ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And DAmt = 0.00 And CAmt != 0.00 ");
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select T1.AcNo, 0.00 As DAmt, T2.DAmt CAmt "); // wed : aslinya T2.DAmt di sum
                    SQL.AppendLine("From TblJournalInterOfficeHdr T1 ");
                    SQL.AppendLine("Inner Join TblJournalInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("Where T1.DocNo = @DocNo ");
                    SQL.AppendLine("And T2.DAmt != 0.00 And T2.CAmt = 0.00 ");
                    //SQL.AppendLine("Group By T1.AcNo ");
                }
                else
                {
                    SQL.AppendLine("Select T1.AcNo, T2.DAmt DAmt, 0.00 As CAmt "); // wed : aslinya T2.DAmt di sum
                    SQL.AppendLine("From TblJournalInterOfficeHdr T1 ");
                    SQL.AppendLine("Inner Join TblJournalInterOfficeDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("Where T1.DocNo = @DocNo ");
                    SQL.AppendLine("And T2.DAmt != 0.00 And T2.CAmt = 0.00 ");
                    //SQL.AppendLine("Group By T1.AcNo ");
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select AcNo, 0.00 As DAmt, CAmt ");
                    SQL.AppendLine("From TblJournalInterOfficeDtl ");
                    SQL.AppendLine("Where DocNo = @DocNo ");
                    SQL.AppendLine("And DAmt = 0.00 And CAmt != 0.00 ");
                    
                }
            }

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            //SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JNDocNo; ");

            SQL.AppendLine("Insert Into TblJournalInterOfficeDtl3(DocNo, DNo, JournalDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @JNDocNo, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            // Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string JNDocNo, bool IsClosingJournalUseCurrentDt, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @JNDocNo:=(" + JNDocNo + "); ");


            SQL.AppendLine("Update TblJournalInterOfficeDtl3 Set ");
            SQL.AppendLine("    JournalDocNo2=@JNDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo = @DNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblJournalInterOfficeHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("); ");


            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JNDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select T1.JournalDocNo ");
            SQL.AppendLine("    From TblJournalInterOfficeDtl3 T1 ");
            SQL.AppendLine("    Inner Join TblJournalInterOfficeHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    And T1.DNo = @DNo ");
            SQL.AppendLine("    And T2.CancelInd = 'Y' ");
            SQL.AppendLine("    And T1.JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JNDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select T1.JournalDocNo ");
            SQL.AppendLine("    From TblJournalInterOfficeDtl3 T1 ");
            SQL.AppendLine("    Inner Join TblJournalInterOfficeHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    And T1.DNo = @DNo ");
            SQL.AppendLine("    And T2.CancelInd = 'Y' ");
            SQL.AppendLine("    And T1.JournalDocNo Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            //Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            bool IsApprovalExists = GetApprovalInfo();
            var code1 = Sm.GetCode1ForJournalDocNo("FrmJournalInterOffice", string.Empty, string.Empty, mJournalDocNoFormat);

            cml.Add(EditJournalInterOffice());

            if (mIsAutoJournalActived)
            {
                string JNDocNo = string.Empty;
                int val = 0;
                var CurrentDt = Sm.ServerCurrentDate();
                var DocDt = Sm.GetDte(DteDocDt);
                var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(DocDt, GetProfitCenterCode(TxtCCCode.Text)) : Sm.IsClosingJournalUseCurrentDt(DocDt);

                for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                {
                    val += 1;
                    var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode In (Select CCCode From TblJournalHdr Where DocNo = @Param) ", Sm.GetGrdStr(Grd3, i, 1));
                    if (mJournalDocNoFormat == "1")
                        JNDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), val);
                    else
                    {
                        //JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), val, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                        JNDocNo = Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty);
                    }
                        
                    cml.Add(SaveJournal2(JNDocNo, IsClosingJournalUseCurrentDt, i));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                IsDataAlreadyCancelled() ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)));
                // Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt));
        }

        private bool IsClosingJournalInvalid(bool IsAutoJournalActived, bool IsCancel, string Dt)
        {
            if (!IsAutoJournalActived) return false;
            if (
                (!Sm.IsClosingJournalInvalid(true, IsCancel, Dt, GetProfitCenterCode(TxtCCCode.Text)) &&
                !Sm.IsClosingJournalInvalid(true, IsCancel, Dt, GetProfitCenterCode(TxtCCCode2.Text))))
            {
                return false;
            }
            return true;
               
        }

        private void GetProfitCenterCode(ref List<string> lProfitCenterCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblCostCenter ");
            SQL.AppendLine("Where ProfitCenterCode Is Not Null ");
            SQL.AppendLine("And CCCode In (@CCCode, @CCCode2); ");

            Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
            Sm.CmParam<String>(ref cm, "@CCCode2", TxtCCCode2.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read()) lProfitCenterCode.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblJournalInterOfficeHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C') ");
            SQL.AppendLine("; ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text);
        }

        private MySqlCommand EditJournalInterOffice()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblJournalInterOfficeHdr ");
            SQL.AppendLine("Set CancelInd = 'Y', CancelReason = @CancelReason, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowJournalInterOfficeHdr(DocNo);
                ShowJournalInterOfficeDtl(DocNo);
                ShowJournalInterOfficeDtl2(DocNo);
                ShowJournalInterOfficeDtl3(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJournalInterOfficeHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CCCode, B.CCName, A.CCCode2, C.CCName As CCName2, A.AcNo, D.AcDesc, A.Remark, A.CCInd, A.CCInd2 ");
            SQL.AppendLine("From  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, DocDt, CancelInd, CancelReason, ");
            SQL.AppendLine("    Status, CCCode, CCCode2, AcNo, Remark, CCInd, CCInd2 ");
            SQL.AppendLine("    From TblJournalInterOfficeHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode2 = C.CCCode ");
            SQL.AppendLine("Inner Join TblCOA D On A.AcNo = D.AcNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] { 
                    //0
                    "DocNo",
                    //1-5
                    "DocDt",  "CancelReason", "CancelInd", "StatusDesc", "CCCode", 
                    //6-10
                    "CCName", "CCCode2", "CCName2", "AcNo", "AcDesc", 
                    //11-13
                    "Remark", "CCInd", "CCInd2"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    TxtCCCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtCCName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtCCCode2.EditValue = Sm.DrStr(dr, c[7]);
                    TxtCCName2.EditValue = Sm.DrStr(dr, c[8]);
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[9]);
                    TxtAcDesc.EditValue = Sm.DrStr(dr, c[10]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    ChkCCInd.Checked = Sm.DrStr(dr, c[12]) == "Y";
                    ChkCCInd2.Checked = Sm.DrStr(dr, c[13]) == "Y";
                }, true
            );
        }

        private void ShowJournalInterOfficeDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.AcNo, B.AcDesc, A.DAmt, A.CAmt ");
            SQL.AppendLine("From TblJournalInterOfficeDtl A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order by A.DNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-4
                    "AcNo", "AcDesc",  "DAmt", "CAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowJournalInterOfficeDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, FileName ");
            SQL.AppendLine("From TblJournalInterOfficeDtl2 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(), new string[] { "DNo", "FileName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowJournalInterOfficeDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, JournalDocNo, JournalDocNo2 ");
            SQL.AppendLine("From TblJournalInterOfficeDtl3 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("Order by DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(), new string[] { "DNo", "JournalDocNo", "JournalDocNo2" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Additional Methods

        #region Process Journal

        public static void PrepC1(ref List<CC1> l1, string CCCode)
        {
            var v = string.Empty;
            var SQL = "Select Parent From TblCostCenter Where CCCode = @Param; ";
            var Result = Sm.GetValue(SQL.ToString(), CCCode);

            if (l1.Count == 0) l1.Add(new CC1() { CCCode = CCCode, Chosen = false });
            
            while (Result.Length > 0)
            {
                l1.Add(new CC1() { CCCode = Result, Chosen = false });
                v = Result;
                Result = Sm.GetValue(SQL, Result);
                if (Sm.CompareStr(v, Result)) // kalau parent dan cost center codenya sama (salah data).
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost center code : " + v + Environment.NewLine + "Invalid cost center's parent.");
                    Result = string.Empty;
                }
            }
        }

        public static void PrepC2(ref List<CC2> l2, string CCCode)
        {
            var v = string.Empty;
            var SQL = "Select Parent From TblCostCenter Where CCCode = @Param; ";
            var Result = Sm.GetValue(SQL, CCCode);

            if (l2.Count == 0) l2.Add(new CC2() { CCCode = CCCode, Chosen = false });
            while (Result.Length > 0)
            {
                l2.Add(new CC2() { CCCode = Result, Chosen = false });
                v = Result;
                Result = Sm.GetValue(SQL, Result);
                if (Sm.CompareStr(v, Result)) // kalau parent dan cost center codenya sama (salah data).
                {
                    Sm.StdMsg(mMsgType.Warning,"Cost center code : " + v + Environment.NewLine + "Invalid cost center's parent.");
                    Result = string.Empty;
                }
            }
        }

        public static void PrepSeq(ref List<CC1> l1, ref List<CC2> l2, ref List<CCSeq> l3)
        {
            bool mFlag = false;
            int index1 = 0, index2 = 0;

            for (int i = 0; i < l1.Count; ++i)
            {
                for (int j = 0; j < l2.Count; ++j)
                {
                    l1[i].Chosen = true;
                    l2[j].Chosen = true;

                    if (l1[i].CCCode == l2[j].CCCode)
                    {
                        index1 = i;
                        index2 = j - 1; // biar cost center yang sama gak ke print 2 kali di proses CCSequence. karna CCCode di index j ini sudah di print di index i nantinya
                        mFlag = true;
                        break;
                    }
                }
                if (mFlag) break;
            }

            for (int i = 0; i < l1.Count; ++i)
            {
                if (l1[i].Chosen && i <= index1)
                {
                    l3.Add(new CCSeq() { CCCode = l1[i].CCCode });
                }
            }

            for (int j = l2.Count - 1; j >= 0; --j)
            {
                if (l2[j].Chosen && j <= index2)
                {
                    l3.Add(new CCSeq() { CCCode = l2[j].CCCode });
                }
            }

        }

        #endregion

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private bool GetApprovalInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'JournalInterOffice' ");

            //SQL.AppendLine("And SiteCode In ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select B.SiteCode ");
            //SQL.AppendLine("    From TblCostCenter A ");
            //SQL.AppendLine("    Inner Join TblSite B On A.ProfitCenterCode = B.ProfitCenterCode ");
            //SQL.AppendLine("    Where A.CCCode = @Param ");

            SQL.AppendLine("And DeptCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.DeptCode ");
            SQL.AppendLine("    From TblCostCenter A ");
            SQL.AppendLine("    Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("    Where A.CCCode = @Param ");

            SQL.AppendLine(") ");
            SQL.AppendLine("Limit 1 ");
            SQL.AppendLine("; ");

            return Sm.IsDataExist(SQL.ToString(), ChkCCInd.Checked ? TxtCCCode.Text : TxtCCCode2.Text);

        }

        private void ShowJournalTransaction(int Row, int Cols)
        {
            var f = new FrmJournal(mMenuCode);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mDocNo = Sm.GetGrdStr(Grd3, Row, Cols);
            f.ShowDialog();
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'AcNoForAccountsInterOffice', 'FormatFTPClient', 'FileSizeMaxUploadFTPClient', 'IsAutoJournalActived', 'IsClosingJournalBasedOnMultiProfitCenter', ");
            SQL.AppendLine("'JournalInterOfficeCOASource', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;

                            //string
                            case "AcNoForAccountsInterOffice": mAcNoForAccountsInterOffice = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "JournalInterOfficeCOASource":mJournalInterOfficeCOASource = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mJournalInterOfficeCOASource.Length == 0) mJournalInterOfficeCOASource = "1";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblJournalInterOfficeDtl2 Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0 && Sm.GetGrdStr(Grd2, Row, 1) != Sm.GetGrdStr(Grd2, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblJournalInterOfficeDtl2 ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnCCCode1_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmJournalInterOfficeDlg(this, 1));
        }

        private void BtnCCCode2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmJournalInterOfficeDlg(this, 2));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmJournalInterOfficeDlg2(this, true));
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void ChkCCInd1_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkCCInd2.Checked && ChkCCInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Cost Center 2 already checked, please choose one.");
                ChkCCInd.Checked = false;
            }
        }

        private void ChkCCInd2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkCCInd.Checked && ChkCCInd2.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Cost Center 1 already checked, please choose one.");
                ChkCCInd2.Checked = false;
            }
        }

        #endregion

        #endregion

        #region Class

        public class CC1
        {
            public string CCCode { get; set; }
            public bool Chosen { get; set; }
        }

        public class CC2
        {
            public string CCCode { get; set; }
            public bool Chosen { get; set; }
        }

        public class CCSeq
        {
            public string CCCode { get; set; }
        }

        #endregion
    }
}
