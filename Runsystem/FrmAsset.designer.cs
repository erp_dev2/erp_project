﻿namespace RunSystem
{
    partial class FrmAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAsset));
            this.label5 = new System.Windows.Forms.Label();
            this.DteAssetDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAssetCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAssetValue = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtEcoLifeYr = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDepreciationCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LueCC = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPercentageAnnualDepreciation = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtEcoLifeMth = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.LblAssetCategoryCode = new System.Windows.Forms.Label();
            this.LueAssetCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TcAsset = new DevExpress.XtraTab.XtraTabControl();
            this.TpRecvVd = new DevExpress.XtraTab.XtraTabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtLocation = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.LueWideUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtWide = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueDiameterUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDiameter = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueVolumeUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueWidthUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueHeightUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLengthUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtVolume = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtWidth = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtHeight = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtLength = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TpVRManual = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.label32 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.LueLocation2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.LueSubLocation = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.LueLocation = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.LueSubType = new DevExpress.XtraEditors.LookUpEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.LueSubClassification = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.LueClassification = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ChkFiskalInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeDisabledReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkRentedInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSoldInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkAssetType = new DevExpress.XtraEditors.CheckEdit();
            this.ChkLeasingInd = new DevExpress.XtraEditors.CheckEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteAssetDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAssetDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDepreciationCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentageAnnualDepreciation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCategory.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcAsset)).BeginInit();
            this.TcAsset.SuspendLayout();
            this.TpRecvVd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWideUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWide.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDiameterUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiameter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVolumeUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWidthUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueHeightUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLength.Properties)).BeginInit();
            this.TpVRManual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpAdditional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocation2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubClassification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClassification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFiskalInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDisabledReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRentedInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSoldInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLeasingInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(890, 0);
            this.panel1.Size = new System.Drawing.Size(70, 610);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.TxtDisplayName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtAssetName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtAssetCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueParent);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Size = new System.Drawing.Size(890, 610);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(12, 28);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Asset Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteAssetDt
            // 
            this.DteAssetDt.EditValue = null;
            this.DteAssetDt.EnterMoveNextControl = true;
            this.DteAssetDt.Location = new System.Drawing.Point(214, 71);
            this.DteAssetDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteAssetDt.Name = "DteAssetDt";
            this.DteAssetDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAssetDt.Properties.Appearance.Options.UseFont = true;
            this.DteAssetDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteAssetDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteAssetDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteAssetDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteAssetDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAssetDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteAssetDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteAssetDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteAssetDt.Properties.MaxLength = 8;
            this.DteAssetDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteAssetDt.Size = new System.Drawing.Size(170, 20);
            this.DteAssetDt.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(109, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 14);
            this.label2.TabIndex = 33;
            this.label2.Text = "Date of Purchase";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetCode
            // 
            this.TxtAssetCode.EnterMoveNextControl = true;
            this.TxtAssetCode.Location = new System.Drawing.Point(87, 4);
            this.TxtAssetCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetCode.Name = "TxtAssetCode";
            this.TxtAssetCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetCode.Properties.MaxLength = 40;
            this.TxtAssetCode.Size = new System.Drawing.Size(351, 20);
            this.TxtAssetCode.TabIndex = 10;
            this.TxtAssetCode.Validated += new System.EventHandler(this.TxtAssetCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(15, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Asset Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(87, 25);
            this.TxtAssetName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 200;
            this.TxtAssetName.Size = new System.Drawing.Size(351, 20);
            this.TxtAssetName.TabIndex = 12;
            this.TxtAssetName.Validated += new System.EventHandler(this.TxtAssetName_Validated);
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(387, 49);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(24, 21);
            this.BtnItCode.TabIndex = 31;
            this.BtnItCode.ToolTip = "Find Item";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(412, 49);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(352, 20);
            this.TxtItName.TabIndex = 32;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(214, 50);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(170, 20);
            this.TxtItCode.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(177, 53);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Item";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetValue
            // 
            this.TxtAssetValue.EnterMoveNextControl = true;
            this.TxtAssetValue.Location = new System.Drawing.Point(214, 92);
            this.TxtAssetValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetValue.Name = "TxtAssetValue";
            this.TxtAssetValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssetValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetValue.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAssetValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAssetValue.Size = new System.Drawing.Size(170, 20);
            this.TxtAssetValue.TabIndex = 34;
            this.TxtAssetValue.Validated += new System.EventHandler(this.TxtAssetValue_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(139, 95);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 14);
            this.label16.TabIndex = 33;
            this.label16.Text = "Asset Value";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEcoLifeYr
            // 
            this.TxtEcoLifeYr.EnterMoveNextControl = true;
            this.TxtEcoLifeYr.Location = new System.Drawing.Point(214, 113);
            this.TxtEcoLifeYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEcoLifeYr.Name = "TxtEcoLifeYr";
            this.TxtEcoLifeYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEcoLifeYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseFont = true;
            this.TxtEcoLifeYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEcoLifeYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEcoLifeYr.Size = new System.Drawing.Size(66, 20);
            this.TxtEcoLifeYr.TabIndex = 36;
            this.TxtEcoLifeYr.Validated += new System.EventHandler(this.TxtEcoLife_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(128, 116);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 14);
            this.label4.TabIndex = 35;
            this.label4.Text = "Economic Life";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDepreciationCode
            // 
            this.LueDepreciationCode.EnterMoveNextControl = true;
            this.LueDepreciationCode.Location = new System.Drawing.Point(214, 134);
            this.LueDepreciationCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDepreciationCode.Name = "LueDepreciationCode";
            this.LueDepreciationCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.Appearance.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDepreciationCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDepreciationCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDepreciationCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDepreciationCode.Properties.DropDownRows = 5;
            this.LueDepreciationCode.Properties.NullText = "[Empty]";
            this.LueDepreciationCode.Properties.PopupWidth = 350;
            this.LueDepreciationCode.Size = new System.Drawing.Size(170, 20);
            this.LueDepreciationCode.TabIndex = 41;
            this.LueDepreciationCode.ToolTip = "F4 : Show/hide list";
            this.LueDepreciationCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDepreciationCode.EditValueChanged += new System.EventHandler(this.LueDepreciationCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(286, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 14);
            this.label7.TabIndex = 37;
            this.label7.Text = "Year";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(85, 137);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "Depreciation Method ";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(106, 179);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 14);
            this.label9.TabIndex = 45;
            this.label9.Text = "Initial Cost Center";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCC
            // 
            this.LueCC.EnterMoveNextControl = true;
            this.LueCC.Location = new System.Drawing.Point(214, 176);
            this.LueCC.Margin = new System.Windows.Forms.Padding(5);
            this.LueCC.Name = "LueCC";
            this.LueCC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.Appearance.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCC.Properties.DropDownRows = 30;
            this.LueCC.Properties.MaxLength = 40;
            this.LueCC.Properties.NullText = "[Empty]";
            this.LueCC.Properties.PopupWidth = 450;
            this.LueCC.Size = new System.Drawing.Size(432, 20);
            this.LueCC.TabIndex = 46;
            this.LueCC.ToolTip = "F4 : Show/hide list";
            this.LueCC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCC.EditValueChanged += new System.EventHandler(this.LueCC_EditValueChanged);
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(87, 46);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 200;
            this.TxtDisplayName.Size = new System.Drawing.Size(351, 20);
            this.TxtDisplayName.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 49);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 13;
            this.label10.Text = "Display Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(390, 158);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 14);
            this.label11.TabIndex = 44;
            this.label11.Text = "%";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPercentageAnnualDepreciation
            // 
            this.TxtPercentageAnnualDepreciation.EnterMoveNextControl = true;
            this.TxtPercentageAnnualDepreciation.Location = new System.Drawing.Point(214, 155);
            this.TxtPercentageAnnualDepreciation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPercentageAnnualDepreciation.Name = "TxtPercentageAnnualDepreciation";
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseFont = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPercentageAnnualDepreciation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPercentageAnnualDepreciation.Size = new System.Drawing.Size(170, 20);
            this.TxtPercentageAnnualDepreciation.TabIndex = 43;
            this.TxtPercentageAnnualDepreciation.Validated += new System.EventHandler(this.TxtPercentageAnnualDepreciation_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(65, 158);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 14);
            this.label12.TabIndex = 42;
            this.label12.Text = "Annual Depreciation Rate";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(214, 219);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 16;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(550, 20);
            this.TxtAcDesc.TabIndex = 50;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(502, 197);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 49;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(214, 198);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 16;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(286, 20);
            this.TxtAcNo.TabIndex = 48;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(108, 201);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 14);
            this.label14.TabIndex = 47;
            this.label14.Text = "COA\'s Account# ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(391, 116);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 14);
            this.label15.TabIndex = 39;
            this.label15.Text = "Months";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEcoLifeMth
            // 
            this.TxtEcoLifeMth.EnterMoveNextControl = true;
            this.TxtEcoLifeMth.Location = new System.Drawing.Point(322, 113);
            this.TxtEcoLifeMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEcoLifeMth.Name = "TxtEcoLifeMth";
            this.TxtEcoLifeMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEcoLifeMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEcoLifeMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEcoLifeMth.Properties.Appearance.Options.UseFont = true;
            this.TxtEcoLifeMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEcoLifeMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEcoLifeMth.Size = new System.Drawing.Size(63, 20);
            this.TxtEcoLifeMth.TabIndex = 38;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(214, 261);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 16;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(550, 20);
            this.TxtAcDesc2.TabIndex = 54;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(502, 239);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo2.TabIndex = 53;
            this.BtnAcNo2.ToolTip = "Find COA\'s Account";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(214, 240);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 16;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(286, 20);
            this.TxtAcNo2.TabIndex = 52;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 243);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(202, 14);
            this.label18.TabIndex = 51;
            this.label18.Text = "Accumulation Depreciation Account";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblAssetCategoryCode
            // 
            this.LblAssetCategoryCode.AutoSize = true;
            this.LblAssetCategoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAssetCategoryCode.ForeColor = System.Drawing.Color.Black;
            this.LblAssetCategoryCode.Location = new System.Drawing.Point(116, 11);
            this.LblAssetCategoryCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAssetCategoryCode.Name = "LblAssetCategoryCode";
            this.LblAssetCategoryCode.Size = new System.Drawing.Size(94, 14);
            this.LblAssetCategoryCode.TabIndex = 25;
            this.LblAssetCategoryCode.Text = "Asset Category ";
            this.LblAssetCategoryCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAssetCategory
            // 
            this.LueAssetCategory.EnterMoveNextControl = true;
            this.LueAssetCategory.Location = new System.Drawing.Point(214, 8);
            this.LueAssetCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LueAssetCategory.Name = "LueAssetCategory";
            this.LueAssetCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategory.Properties.Appearance.Options.UseFont = true;
            this.LueAssetCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAssetCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAssetCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAssetCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAssetCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAssetCategory.Properties.DropDownRows = 30;
            this.LueAssetCategory.Properties.NullText = "[Empty]";
            this.LueAssetCategory.Properties.PopupWidth = 450;
            this.LueAssetCategory.Size = new System.Drawing.Size(432, 20);
            this.LueAssetCategory.TabIndex = 26;
            this.LueAssetCategory.ToolTip = "F4 : Show/hide list";
            this.LueAssetCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAssetCategory.EditValueChanged += new System.EventHandler(this.LueAssetCategory_EditValueChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TcAsset);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 93);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(890, 517);
            this.panel3.TabIndex = 52;
            // 
            // TcAsset
            // 
            this.TcAsset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcAsset.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcAsset.Location = new System.Drawing.Point(0, 0);
            this.TcAsset.Name = "TcAsset";
            this.TcAsset.SelectedTabPage = this.TpRecvVd;
            this.TcAsset.Size = new System.Drawing.Size(890, 517);
            this.TcAsset.TabIndex = 24;
            this.TcAsset.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpRecvVd,
            this.TpVRManual,
            this.TpAdditional});
            // 
            // TpRecvVd
            // 
            this.TpRecvVd.Appearance.Header.Options.UseTextOptions = true;
            this.TpRecvVd.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpRecvVd.Controls.Add(this.Grd1);
            this.TpRecvVd.Name = "TpRecvVd";
            this.TpRecvVd.Size = new System.Drawing.Size(884, 489);
            this.TpRecvVd.Text = "Receiving Item From Vendor";
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(884, 489);
            this.Grd1.TabIndex = 21;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.ChkFile);
            this.TpGeneral.Controls.Add(this.BtnDownload);
            this.TpGeneral.Controls.Add(this.PbUpload);
            this.TpGeneral.Controls.Add(this.BtnFile);
            this.TpGeneral.Controls.Add(this.TxtFile);
            this.TpGeneral.Controls.Add(this.label24);
            this.TpGeneral.Controls.Add(this.TxtLocation);
            this.TpGeneral.Controls.Add(this.label19);
            this.TpGeneral.Controls.Add(this.TxtShortCode);
            this.TpGeneral.Controls.Add(this.label23);
            this.TpGeneral.Controls.Add(this.LueWideUomCode);
            this.TpGeneral.Controls.Add(this.TxtWide);
            this.TpGeneral.Controls.Add(this.label22);
            this.TpGeneral.Controls.Add(this.LueDiameterUomCode);
            this.TpGeneral.Controls.Add(this.TxtDiameter);
            this.TpGeneral.Controls.Add(this.label35);
            this.TpGeneral.Controls.Add(this.LueVolumeUomCode);
            this.TpGeneral.Controls.Add(this.LueWidthUomCode);
            this.TpGeneral.Controls.Add(this.LueHeightUomCode);
            this.TpGeneral.Controls.Add(this.LueLengthUomCode);
            this.TpGeneral.Controls.Add(this.TxtVolume);
            this.TpGeneral.Controls.Add(this.label13);
            this.TpGeneral.Controls.Add(this.TxtWidth);
            this.TpGeneral.Controls.Add(this.label17);
            this.TpGeneral.Controls.Add(this.TxtHeight);
            this.TpGeneral.Controls.Add(this.label20);
            this.TpGeneral.Controls.Add(this.TxtLength);
            this.TpGeneral.Controls.Add(this.label21);
            this.TpGeneral.Controls.Add(this.LblAssetCategoryCode);
            this.TpGeneral.Controls.Add(this.LueAssetCategory);
            this.TpGeneral.Controls.Add(this.TxtAcDesc2);
            this.TpGeneral.Controls.Add(this.TxtItCode);
            this.TpGeneral.Controls.Add(this.label2);
            this.TpGeneral.Controls.Add(this.BtnAcNo2);
            this.TpGeneral.Controls.Add(this.DteAssetDt);
            this.TpGeneral.Controls.Add(this.TxtAcNo2);
            this.TpGeneral.Controls.Add(this.label6);
            this.TpGeneral.Controls.Add(this.label18);
            this.TpGeneral.Controls.Add(this.label15);
            this.TpGeneral.Controls.Add(this.TxtItName);
            this.TpGeneral.Controls.Add(this.TxtEcoLifeMth);
            this.TpGeneral.Controls.Add(this.BtnItCode);
            this.TpGeneral.Controls.Add(this.TxtAcDesc);
            this.TpGeneral.Controls.Add(this.label16);
            this.TpGeneral.Controls.Add(this.TxtAssetValue);
            this.TpGeneral.Controls.Add(this.BtnAcNo);
            this.TpGeneral.Controls.Add(this.label4);
            this.TpGeneral.Controls.Add(this.TxtAcNo);
            this.TpGeneral.Controls.Add(this.TxtEcoLifeYr);
            this.TpGeneral.Controls.Add(this.label14);
            this.TpGeneral.Controls.Add(this.LueDepreciationCode);
            this.TpGeneral.Controls.Add(this.label11);
            this.TpGeneral.Controls.Add(this.label7);
            this.TpGeneral.Controls.Add(this.TxtPercentageAnnualDepreciation);
            this.TpGeneral.Controls.Add(this.label8);
            this.TpGeneral.Controls.Add(this.label12);
            this.TpGeneral.Controls.Add(this.LueCC);
            this.TpGeneral.Controls.Add(this.label9);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(766, 489);
            this.TpGeneral.Text = "General";
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(454, 438);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 77;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(496, 438);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(26, 21);
            this.BtnDownload.TabIndex = 78;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(214, 461);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(319, 23);
            this.PbUpload.TabIndex = 80;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(471, 438);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(23, 19);
            this.BtnFile.TabIndex = 79;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(214, 439);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(232, 20);
            this.TxtFile.TabIndex = 76;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(183, 443);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(24, 14);
            this.label24.TabIndex = 75;
            this.label24.Text = "File";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocation
            // 
            this.TxtLocation.EnterMoveNextControl = true;
            this.TxtLocation.Location = new System.Drawing.Point(214, 416);
            this.TxtLocation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocation.Name = "TxtLocation";
            this.TxtLocation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocation.Properties.Appearance.Options.UseFont = true;
            this.TxtLocation.Properties.MaxLength = 200;
            this.TxtLocation.Size = new System.Drawing.Size(319, 20);
            this.TxtLocation.TabIndex = 74;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(154, 419);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 14);
            this.label19.TabIndex = 73;
            this.label19.Text = "Location";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(214, 29);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 30;
            this.TxtShortCode.Size = new System.Drawing.Size(170, 20);
            this.TxtShortCode.TabIndex = 28;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(141, 32);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 14);
            this.label23.TabIndex = 27;
            this.label23.Text = "Short Code";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWideUomCode
            // 
            this.LueWideUomCode.EnterMoveNextControl = true;
            this.LueWideUomCode.Location = new System.Drawing.Point(383, 393);
            this.LueWideUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWideUomCode.Name = "LueWideUomCode";
            this.LueWideUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWideUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueWideUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWideUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWideUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWideUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWideUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWideUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWideUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWideUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWideUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWideUomCode.Properties.DropDownRows = 30;
            this.LueWideUomCode.Properties.NullText = "[Empty]";
            this.LueWideUomCode.Properties.PopupWidth = 200;
            this.LueWideUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueWideUomCode.TabIndex = 72;
            this.LueWideUomCode.ToolTip = "F4 : Show/hide list";
            this.LueWideUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWideUomCode.EditValueChanged += new System.EventHandler(this.LueWideUomCode_EditValueChanged);
            // 
            // TxtWide
            // 
            this.TxtWide.EnterMoveNextControl = true;
            this.TxtWide.Location = new System.Drawing.Point(214, 393);
            this.TxtWide.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWide.Name = "TxtWide";
            this.TxtWide.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWide.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWide.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWide.Properties.Appearance.Options.UseFont = true;
            this.TxtWide.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWide.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWide.Size = new System.Drawing.Size(166, 20);
            this.TxtWide.TabIndex = 71;
            this.TxtWide.Validated += new System.EventHandler(this.TxtWide_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(142, 396);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 14);
            this.label22.TabIndex = 70;
            this.label22.Text = "Wide/Area";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDiameterUomCode
            // 
            this.LueDiameterUomCode.EnterMoveNextControl = true;
            this.LueDiameterUomCode.Location = new System.Drawing.Point(383, 349);
            this.LueDiameterUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDiameterUomCode.Name = "LueDiameterUomCode";
            this.LueDiameterUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDiameterUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDiameterUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDiameterUomCode.Properties.DropDownRows = 30;
            this.LueDiameterUomCode.Properties.NullText = "[Empty]";
            this.LueDiameterUomCode.Properties.PopupWidth = 200;
            this.LueDiameterUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueDiameterUomCode.TabIndex = 66;
            this.LueDiameterUomCode.ToolTip = "F4 : Show/hide list";
            this.LueDiameterUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDiameterUomCode.EditValueChanged += new System.EventHandler(this.LueDiameterUomCode_EditValueChanged);
            // 
            // TxtDiameter
            // 
            this.TxtDiameter.EnterMoveNextControl = true;
            this.TxtDiameter.Location = new System.Drawing.Point(214, 349);
            this.TxtDiameter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiameter.Name = "TxtDiameter";
            this.TxtDiameter.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiameter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiameter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiameter.Properties.Appearance.Options.UseFont = true;
            this.TxtDiameter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiameter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiameter.Size = new System.Drawing.Size(166, 20);
            this.TxtDiameter.TabIndex = 65;
            this.TxtDiameter.Validated += new System.EventHandler(this.TxtDiameter_Validated);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(151, 352);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 14);
            this.label35.TabIndex = 64;
            this.label35.Text = "Diameter";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVolumeUomCode
            // 
            this.LueVolumeUomCode.EnterMoveNextControl = true;
            this.LueVolumeUomCode.Location = new System.Drawing.Point(383, 371);
            this.LueVolumeUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVolumeUomCode.Name = "LueVolumeUomCode";
            this.LueVolumeUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVolumeUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVolumeUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVolumeUomCode.Properties.DropDownRows = 30;
            this.LueVolumeUomCode.Properties.NullText = "[Empty]";
            this.LueVolumeUomCode.Properties.PopupWidth = 200;
            this.LueVolumeUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueVolumeUomCode.TabIndex = 69;
            this.LueVolumeUomCode.ToolTip = "F4 : Show/hide list";
            this.LueVolumeUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVolumeUomCode.EditValueChanged += new System.EventHandler(this.LueVolumeUomCode_EditValueChanged);
            // 
            // LueWidthUomCode
            // 
            this.LueWidthUomCode.EnterMoveNextControl = true;
            this.LueWidthUomCode.Location = new System.Drawing.Point(383, 327);
            this.LueWidthUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWidthUomCode.Name = "LueWidthUomCode";
            this.LueWidthUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWidthUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWidthUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWidthUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWidthUomCode.Properties.DropDownRows = 30;
            this.LueWidthUomCode.Properties.NullText = "[Empty]";
            this.LueWidthUomCode.Properties.PopupWidth = 200;
            this.LueWidthUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueWidthUomCode.TabIndex = 63;
            this.LueWidthUomCode.ToolTip = "F4 : Show/hide list";
            this.LueWidthUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWidthUomCode.EditValueChanged += new System.EventHandler(this.LueWidthUomCode_EditValueChanged);
            // 
            // LueHeightUomCode
            // 
            this.LueHeightUomCode.EnterMoveNextControl = true;
            this.LueHeightUomCode.Location = new System.Drawing.Point(383, 305);
            this.LueHeightUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueHeightUomCode.Name = "LueHeightUomCode";
            this.LueHeightUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueHeightUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueHeightUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueHeightUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueHeightUomCode.Properties.DropDownRows = 30;
            this.LueHeightUomCode.Properties.NullText = "[Empty]";
            this.LueHeightUomCode.Properties.PopupWidth = 200;
            this.LueHeightUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueHeightUomCode.TabIndex = 60;
            this.LueHeightUomCode.ToolTip = "F4 : Show/hide list";
            this.LueHeightUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueHeightUomCode.EditValueChanged += new System.EventHandler(this.LueHeightUomCode_EditValueChanged);
            // 
            // LueLengthUomCode
            // 
            this.LueLengthUomCode.EnterMoveNextControl = true;
            this.LueLengthUomCode.Location = new System.Drawing.Point(383, 283);
            this.LueLengthUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLengthUomCode.Name = "LueLengthUomCode";
            this.LueLengthUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLengthUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLengthUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLengthUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLengthUomCode.Properties.DropDownRows = 30;
            this.LueLengthUomCode.Properties.NullText = "[Empty]";
            this.LueLengthUomCode.Properties.PopupWidth = 200;
            this.LueLengthUomCode.Size = new System.Drawing.Size(150, 20);
            this.LueLengthUomCode.TabIndex = 57;
            this.LueLengthUomCode.ToolTip = "F4 : Show/hide list";
            this.LueLengthUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLengthUomCode.EditValueChanged += new System.EventHandler(this.LueLengthUomCode_EditValueChanged);
            // 
            // TxtVolume
            // 
            this.TxtVolume.EnterMoveNextControl = true;
            this.TxtVolume.Location = new System.Drawing.Point(214, 371);
            this.TxtVolume.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVolume.Name = "TxtVolume";
            this.TxtVolume.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVolume.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVolume.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVolume.Properties.Appearance.Options.UseFont = true;
            this.TxtVolume.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVolume.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVolume.Size = new System.Drawing.Size(166, 20);
            this.TxtVolume.TabIndex = 68;
            this.TxtVolume.Validated += new System.EventHandler(this.TxtVolume_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(159, 374);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 14);
            this.label13.TabIndex = 67;
            this.label13.Text = "Volume";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWidth
            // 
            this.TxtWidth.EnterMoveNextControl = true;
            this.TxtWidth.Location = new System.Drawing.Point(214, 327);
            this.TxtWidth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWidth.Name = "TxtWidth";
            this.TxtWidth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWidth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWidth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWidth.Properties.Appearance.Options.UseFont = true;
            this.TxtWidth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWidth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWidth.Size = new System.Drawing.Size(166, 20);
            this.TxtWidth.TabIndex = 62;
            this.TxtWidth.Validated += new System.EventHandler(this.TxtWidth_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(167, 330);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 14);
            this.label17.TabIndex = 61;
            this.label17.Text = "Width";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHeight
            // 
            this.TxtHeight.EnterMoveNextControl = true;
            this.TxtHeight.Location = new System.Drawing.Point(214, 305);
            this.TxtHeight.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHeight.Name = "TxtHeight";
            this.TxtHeight.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHeight.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHeight.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHeight.Properties.Appearance.Options.UseFont = true;
            this.TxtHeight.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHeight.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHeight.Size = new System.Drawing.Size(166, 20);
            this.TxtHeight.TabIndex = 59;
            this.TxtHeight.Validated += new System.EventHandler(this.TxtHeight_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(164, 308);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 14);
            this.label20.TabIndex = 58;
            this.label20.Text = "Height";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLength
            // 
            this.TxtLength.EnterMoveNextControl = true;
            this.TxtLength.Location = new System.Drawing.Point(214, 283);
            this.TxtLength.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLength.Name = "TxtLength";
            this.TxtLength.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLength.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLength.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLength.Properties.Appearance.Options.UseFont = true;
            this.TxtLength.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLength.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLength.Size = new System.Drawing.Size(166, 20);
            this.TxtLength.TabIndex = 56;
            this.TxtLength.Validated += new System.EventHandler(this.TxtLength_Validated);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(161, 286);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 14);
            this.label21.TabIndex = 55;
            this.label21.Text = "Length";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpVRManual
            // 
            this.TpVRManual.Appearance.Header.Options.UseTextOptions = true;
            this.TpVRManual.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpVRManual.Controls.Add(this.Grd2);
            this.TpVRManual.Name = "TpVRManual";
            this.TpVRManual.Size = new System.Drawing.Size(766, 489);
            this.TpVRManual.Text = "Voucher Request (Manual)";
            // 
            // Grd2
            // 
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 489);
            this.Grd2.TabIndex = 21;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Controls.Add(this.label32);
            this.TpAdditional.Controls.Add(this.LueSiteCode);
            this.TpAdditional.Controls.Add(this.label31);
            this.TpAdditional.Controls.Add(this.LueLocation2);
            this.TpAdditional.Controls.Add(this.label30);
            this.TpAdditional.Controls.Add(this.LueSubLocation);
            this.TpAdditional.Controls.Add(this.label29);
            this.TpAdditional.Controls.Add(this.LueLocation);
            this.TpAdditional.Controls.Add(this.label28);
            this.TpAdditional.Controls.Add(this.LueSubType);
            this.TpAdditional.Controls.Add(this.label27);
            this.TpAdditional.Controls.Add(this.LueType);
            this.TpAdditional.Controls.Add(this.label26);
            this.TpAdditional.Controls.Add(this.LueSubClassification);
            this.TpAdditional.Controls.Add(this.label25);
            this.TpAdditional.Controls.Add(this.LueClassification);
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 489);
            this.TpAdditional.Text = "Additional";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(118, 192);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 14);
            this.label32.TabIndex = 39;
            this.label32.Text = "Site";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(153, 189);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 450;
            this.LueSiteCode.Size = new System.Drawing.Size(298, 20);
            this.LueSiteCode.TabIndex = 40;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(1, 167);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(148, 14);
            this.label31.TabIndex = 37;
            this.label31.Text = " KPH/BKPH/TPK Location ";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLocation2
            // 
            this.LueLocation2.EnterMoveNextControl = true;
            this.LueLocation2.Location = new System.Drawing.Point(153, 164);
            this.LueLocation2.Margin = new System.Windows.Forms.Padding(5);
            this.LueLocation2.Name = "LueLocation2";
            this.LueLocation2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation2.Properties.Appearance.Options.UseFont = true;
            this.LueLocation2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLocation2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLocation2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLocation2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLocation2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLocation2.Properties.DropDownRows = 30;
            this.LueLocation2.Properties.NullText = "[Empty]";
            this.LueLocation2.Properties.PopupWidth = 450;
            this.LueLocation2.Size = new System.Drawing.Size(298, 20);
            this.LueLocation2.TabIndex = 38;
            this.LueLocation2.ToolTip = "F4 : Show/hide list";
            this.LueLocation2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLocation2.EditValueChanged += new System.EventHandler(this.LueLocation2_EditValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(67, 143);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 14);
            this.label30.TabIndex = 35;
            this.label30.Text = "Sub Location ";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSubLocation
            // 
            this.LueSubLocation.EnterMoveNextControl = true;
            this.LueSubLocation.Location = new System.Drawing.Point(153, 140);
            this.LueSubLocation.Margin = new System.Windows.Forms.Padding(5);
            this.LueSubLocation.Name = "LueSubLocation";
            this.LueSubLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubLocation.Properties.Appearance.Options.UseFont = true;
            this.LueSubLocation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubLocation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSubLocation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubLocation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSubLocation.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubLocation.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSubLocation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubLocation.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSubLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSubLocation.Properties.DropDownRows = 30;
            this.LueSubLocation.Properties.NullText = "[Empty]";
            this.LueSubLocation.Properties.PopupWidth = 450;
            this.LueSubLocation.Size = new System.Drawing.Size(298, 20);
            this.LueSubLocation.TabIndex = 36;
            this.LueSubLocation.ToolTip = "F4 : Show/hide list";
            this.LueSubLocation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSubLocation.EditValueChanged += new System.EventHandler(this.LueSubLocation_EditValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(92, 118);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 14);
            this.label29.TabIndex = 33;
            this.label29.Text = "Location ";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLocation
            // 
            this.LueLocation.EnterMoveNextControl = true;
            this.LueLocation.Location = new System.Drawing.Point(153, 115);
            this.LueLocation.Margin = new System.Windows.Forms.Padding(5);
            this.LueLocation.Name = "LueLocation";
            this.LueLocation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation.Properties.Appearance.Options.UseFont = true;
            this.LueLocation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLocation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLocation.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLocation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocation.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLocation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLocation.Properties.DropDownRows = 30;
            this.LueLocation.Properties.NullText = "[Empty]";
            this.LueLocation.Properties.PopupWidth = 450;
            this.LueLocation.Size = new System.Drawing.Size(298, 20);
            this.LueLocation.TabIndex = 34;
            this.LueLocation.ToolTip = "F4 : Show/hide list";
            this.LueLocation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLocation.EditValueChanged += new System.EventHandler(this.LueLocation_EditValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(85, 94);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 14);
            this.label28.TabIndex = 31;
            this.label28.Text = "Sub Type ";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSubType
            // 
            this.LueSubType.EnterMoveNextControl = true;
            this.LueSubType.Location = new System.Drawing.Point(153, 91);
            this.LueSubType.Margin = new System.Windows.Forms.Padding(5);
            this.LueSubType.Name = "LueSubType";
            this.LueSubType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubType.Properties.Appearance.Options.UseFont = true;
            this.LueSubType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSubType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSubType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSubType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSubType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSubType.Properties.DropDownRows = 30;
            this.LueSubType.Properties.NullText = "[Empty]";
            this.LueSubType.Properties.PopupWidth = 450;
            this.LueSubType.Size = new System.Drawing.Size(298, 20);
            this.LueSubType.TabIndex = 32;
            this.LueSubType.ToolTip = "F4 : Show/hide list";
            this.LueSubType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSubType.EditValueChanged += new System.EventHandler(this.LueSubType_EditValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(110, 70);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 29;
            this.label27.Text = "Type ";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(153, 67);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 30;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 450;
            this.LueType.Size = new System.Drawing.Size(298, 20);
            this.LueType.TabIndex = 30;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(47, 46);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(102, 14);
            this.label26.TabIndex = 27;
            this.label26.Text = "Sub Classification ";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSubClassification
            // 
            this.LueSubClassification.EnterMoveNextControl = true;
            this.LueSubClassification.Location = new System.Drawing.Point(153, 43);
            this.LueSubClassification.Margin = new System.Windows.Forms.Padding(5);
            this.LueSubClassification.Name = "LueSubClassification";
            this.LueSubClassification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubClassification.Properties.Appearance.Options.UseFont = true;
            this.LueSubClassification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubClassification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSubClassification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubClassification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSubClassification.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubClassification.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSubClassification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSubClassification.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSubClassification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSubClassification.Properties.DropDownRows = 30;
            this.LueSubClassification.Properties.NullText = "[Empty]";
            this.LueSubClassification.Properties.PopupWidth = 450;
            this.LueSubClassification.Size = new System.Drawing.Size(298, 20);
            this.LueSubClassification.TabIndex = 28;
            this.LueSubClassification.ToolTip = "F4 : Show/hide list";
            this.LueSubClassification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSubClassification.EditValueChanged += new System.EventHandler(this.LueSubClassifiaction_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(72, 22);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 14);
            this.label25.TabIndex = 25;
            this.label25.Text = "Classification ";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueClassification
            // 
            this.LueClassification.EnterMoveNextControl = true;
            this.LueClassification.Location = new System.Drawing.Point(153, 19);
            this.LueClassification.Margin = new System.Windows.Forms.Padding(5);
            this.LueClassification.Name = "LueClassification";
            this.LueClassification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClassification.Properties.Appearance.Options.UseFont = true;
            this.LueClassification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClassification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueClassification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClassification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueClassification.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClassification.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueClassification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClassification.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueClassification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueClassification.Properties.DropDownRows = 30;
            this.LueClassification.Properties.NullText = "[Empty]";
            this.LueClassification.Properties.PopupWidth = 450;
            this.LueClassification.Size = new System.Drawing.Size(298, 20);
            this.LueClassification.TabIndex = 26;
            this.LueClassification.ToolTip = "F4 : Show/hide list";
            this.LueClassification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueClassification.EditValueChanged += new System.EventHandler(this.LueClassification_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 588);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 10;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 568);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 11;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(41, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Parent";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(87, 67);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 450;
            this.LueParent.Size = new System.Drawing.Size(351, 20);
            this.LueParent.TabIndex = 16;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ChkFiskalInd);
            this.panel7.Controls.Add(this.MeeDisabledReason);
            this.panel7.Controls.Add(this.ChkRentedInd);
            this.panel7.Controls.Add(this.ChkSoldInd);
            this.panel7.Controls.Add(this.ChkActiveInd);
            this.panel7.Controls.Add(this.ChkAssetType);
            this.panel7.Controls.Add(this.ChkLeasingInd);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(495, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(395, 93);
            this.panel7.TabIndex = 53;
            // 
            // ChkFiskalInd
            // 
            this.ChkFiskalInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFiskalInd.Location = new System.Drawing.Point(22, 64);
            this.ChkFiskalInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFiskalInd.Name = "ChkFiskalInd";
            this.ChkFiskalInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFiskalInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFiskalInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFiskalInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFiskalInd.Properties.Appearance.Options.UseFont = true;
            this.ChkFiskalInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFiskalInd.Properties.Caption = "Fiskal";
            this.ChkFiskalInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFiskalInd.Size = new System.Drawing.Size(125, 22);
            this.ChkFiskalInd.TabIndex = 31;
            // 
            // MeeDisabledReason
            // 
            this.MeeDisabledReason.EnterMoveNextControl = true;
            this.MeeDisabledReason.Location = new System.Drawing.Point(189, 5);
            this.MeeDisabledReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDisabledReason.Name = "MeeDisabledReason";
            this.MeeDisabledReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDisabledReason.Properties.Appearance.Options.UseFont = true;
            this.MeeDisabledReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDisabledReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDisabledReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDisabledReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDisabledReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDisabledReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDisabledReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDisabledReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDisabledReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDisabledReason.Properties.MaxLength = 400;
            this.MeeDisabledReason.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeDisabledReason.Properties.ShowIcon = false;
            this.MeeDisabledReason.Size = new System.Drawing.Size(183, 20);
            this.MeeDisabledReason.TabIndex = 26;
            this.MeeDisabledReason.ToolTip = "F4 : Show/hide text";
            this.MeeDisabledReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDisabledReason.ToolTipTitle = "Run System";
            // 
            // ChkRentedInd
            // 
            this.ChkRentedInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRentedInd.Location = new System.Drawing.Point(187, 26);
            this.ChkRentedInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRentedInd.Name = "ChkRentedInd";
            this.ChkRentedInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRentedInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRentedInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRentedInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRentedInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRentedInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRentedInd.Properties.Caption = "Rented Assets";
            this.ChkRentedInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRentedInd.Size = new System.Drawing.Size(111, 22);
            this.ChkRentedInd.TabIndex = 28;
            // 
            // ChkSoldInd
            // 
            this.ChkSoldInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSoldInd.Location = new System.Drawing.Point(187, 46);
            this.ChkSoldInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSoldInd.Name = "ChkSoldInd";
            this.ChkSoldInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSoldInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSoldInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSoldInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSoldInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSoldInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSoldInd.Properties.Caption = "Sold Asssets";
            this.ChkSoldInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSoldInd.Size = new System.Drawing.Size(104, 22);
            this.ChkSoldInd.TabIndex = 30;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(22, 5);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active, Disabled Reason :";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.Size = new System.Drawing.Size(166, 22);
            this.ChkActiveInd.TabIndex = 25;
            this.ChkActiveInd.CheckedChanged += new System.EventHandler(this.ChkActiveInd_CheckedChanged);
            // 
            // ChkAssetType
            // 
            this.ChkAssetType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkAssetType.Location = new System.Drawing.Point(22, 26);
            this.ChkAssetType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkAssetType.Name = "ChkAssetType";
            this.ChkAssetType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkAssetType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetType.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkAssetType.Properties.Appearance.Options.UseBackColor = true;
            this.ChkAssetType.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetType.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAssetType.Properties.Caption = "Work Center";
            this.ChkAssetType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetType.Size = new System.Drawing.Size(104, 22);
            this.ChkAssetType.TabIndex = 27;
            // 
            // ChkLeasingInd
            // 
            this.ChkLeasingInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkLeasingInd.Location = new System.Drawing.Point(22, 46);
            this.ChkLeasingInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkLeasingInd.Name = "ChkLeasingInd";
            this.ChkLeasingInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkLeasingInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLeasingInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkLeasingInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkLeasingInd.Properties.Appearance.Options.UseFont = true;
            this.ChkLeasingInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkLeasingInd.Properties.Caption = "Asset Insurance";
            this.ChkLeasingInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLeasingInd.Size = new System.Drawing.Size(125, 22);
            this.ChkLeasingInd.TabIndex = 29;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 610);
            this.Name = "FrmAsset";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteAssetDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteAssetDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDepreciationCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentageAnnualDepreciation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEcoLifeMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCategory.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcAsset)).EndInit();
            this.TcAsset.ResumeLayout(false);
            this.TpRecvVd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpGeneral.ResumeLayout(false);
            this.TpGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWideUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWide.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDiameterUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiameter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVolumeUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWidthUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueHeightUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLengthUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLength.Properties)).EndInit();
            this.TpVRManual.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpAdditional.ResumeLayout(false);
            this.TpAdditional.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocation2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSubClassification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClassification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkFiskalInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDisabledReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRentedInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSoldInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLeasingInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteAssetDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtAssetCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtAssetName;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtAssetValue;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtEcoLifeYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueDepreciationCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.LookUpEdit LueCC;
        internal DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtPercentageAnnualDepreciation;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtEcoLifeMth;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label LblAssetCategoryCode;
        private DevExpress.XtraEditors.LookUpEdit LueAssetCategory;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl TcAsset;
        private DevExpress.XtraTab.XtraTabPage TpRecvVd;
        private DevExpress.XtraTab.XtraTabPage TpVRManual;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        private DevExpress.XtraEditors.LookUpEdit LueDiameterUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtDiameter;
        private System.Windows.Forms.Label label35;
        private DevExpress.XtraEditors.LookUpEdit LueVolumeUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueWidthUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueHeightUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueLengthUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtVolume;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtWidth;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtHeight;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtLength;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueWideUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtWide;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueParent;
        internal DevExpress.XtraEditors.TextEdit TxtLocation;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.CheckEdit ChkFiskalInd;
        private DevExpress.XtraEditors.MemoExEdit MeeDisabledReason;
        private DevExpress.XtraEditors.CheckEdit ChkRentedInd;
        private DevExpress.XtraEditors.CheckEdit ChkSoldInd;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        private DevExpress.XtraEditors.CheckEdit ChkAssetType;
        private DevExpress.XtraEditors.CheckEdit ChkLeasingInd;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit LueLocation2;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.LookUpEdit LueSubLocation;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.LookUpEdit LueLocation;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LookUpEdit LueSubType;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit LueType;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.LookUpEdit LueSubClassification;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.LookUpEdit LueClassification;
    }
}