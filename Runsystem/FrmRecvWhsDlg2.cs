﻿#region Update
/*
    28/08/2019 [WED] new apps for TPB
    08/09/2020 [DITA/MAI] menambahkan field Nilai Jaminan (Insurance Amount), nomor SKEP (Decree#), tanggal SKEP (Decree Date) di Tab Kawasan Berikat

 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhsDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvWhs mFrmParent;
        private string mSQL = string.Empty,
            mKB_Server = string.Empty,
            mKB_Database = string.Empty,
            mKB_DBUser = string.Empty,
            mKB_DBPwd = string.Empty,
            mKB_Port = string.Empty,
            mCustomsDocCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvWhsDlg2(FrmRecvWhs FrmParent, string CustomsDocCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCustomsDocCode = CustomsDocCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                GetParameter();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Contract#",
                        "Contract Date",
                        "Packing List#",
                        "Packing List Date",
                        "Registration#",

                        //6-10
                        "Registration Date",
                        "Vendor",
                        "Item's Group",
                        "Quantity",
                        "Packaging",

                        //11-15
                        "Packaging Quantity",
                        "Submission#",
                        "Customs Document Code",
                        "Insurance#",
                        "Insurance Date",
                       

                        //16-19
                        "Insurance"+Environment.NewLine+"Due Date",
                        "Insurance Amount",
                        "Decree#",
                        "Decree Date"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 120, 150, 120, 130,
                        
                        //6-10
                        120, 200, 300, 100, 150,

                        //11-15
                        130, 150, 180, 150, 100,

                        //16-19
                        100, 120, 220, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 4, 6, 15, 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 14, 15, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 17 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            var l = new List<KB_Data>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    l.RemoveAll(w => w.ProcessInd);
                    if (l.Count > 0)
                        Process3(ref l);
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParam<string>(ref cm, "@CustomsDocCode", mCustomsDocCode);
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.Nomor_Daftar", false);

            SQL.AppendLine("Select A.Nomor_Daftar As KBRegistrationNo, Date_Format(A.Tanggal_Daftar, '%Y%m%d') As KBRegistrationDt, ");
            SQL.AppendLine("B.Nomor_Dokumen As KBContractNo, Date_Format(B.Tanggal_Dokumen, '%Y%m%d') As KBContractDt, ");
            SQL.AppendLine("C.Nomor_Dokumen As KBPLNo, Date_Format(C.Tanggal_Dokumen, '%Y%m%d') As KBPLDt, ");
            SQL.AppendLine("A.Nama_Pengusaha As Vendor, D.Item, A.Jumlah_Barang As Qty, F.URAIAN_KEMASAN As KBPackaging, IfNull(E.Jumlah_Kemasan, 0) As KBPackagingQty,  ");
            SQL.AppendLine("A.Nomor_Aju As KBSubmissionNo, A.Kode_Dokumen_Pabean ");
            SQL.AppendLine(", IFNULL(G.NILAI_JAMINAN, 0.00) KBInsuranceAmt, G.Nomor_Jaminan KBInsuranceNo, ");
            SQL.AppendLine("DATE_FORMAT(G.Tanggal_Jaminan, '%Y%m%d') KBInsuranceDt, DATE_FORMAT(G.Tanggal_Jatuh_Tempo, '%Y%m%d') KBInsuranceDueDt, ");
            SQL.AppendLine("H.Nomor_Dokumen KBDecreeNo, DATE_FORMAT(H.Tanggal_Dokumen, '%Y%m%d') KBDecreeDt ");
            SQL.AppendLine("From tpb_header A ");
            SQL.AppendLine("Left Join tpb_dokumen B On A.ID=B.ID_HEADER And B.Kode_Jenis_Dokumen='315' ");
            SQL.AppendLine("Left Join tpb_dokumen C On A.ID=C.ID_HEADER And C.Kode_Jenis_Dokumen='217' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.ID_Header, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T3.Kode_Barang, ' (', T3.Uraian, ')') Separator ', ') As Item ");
            SQL.AppendLine("    From tpb_header T1 ");
            SQL.AppendLine("    Inner Join tpb_dokumen T2 On T1.ID=T2.ID_HEADER And T2.Kode_Jenis_Dokumen='315' ");
            SQL.AppendLine("        And Date_Format(T2.Tanggal_Dokumen, '%Y%m%d') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join tpb_barang T3 On T1.ID=T3.ID_HEADER ");
            SQL.AppendLine("    Where T1.Kode_Dokumen_Pabean=@CustomsDocCode ");
            SQL.AppendLine("    Group By T3.ID_Header ");
            SQL.AppendLine(") D On A.ID=D.ID_Header ");
            SQL.AppendLine("Left Join tpb_kemasan E On A.ID=E.ID_HEADER ");
            SQL.AppendLine("Left Join referensi_kemasan F On E.KODE_JENIS_KEMASAN=F.KODE_KEMASAN ");
            SQL.AppendLine("Left Join Tpb_Jaminan G ON A.ID = G.ID_Header ");
            SQL.AppendLine("LEFT JOIN Tpb_Dokumen H ON A.Id = H.ID_Header AND H.Kode_Jenis_Dokumen='911' ");
            SQL.AppendLine("Where A.Kode_Dokumen_Pabean=@CustomsDocCode ");
            SQL.AppendLine("And Date_Format(A.Tanggal_Daftar, '%Y%m%d') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.Kode_Dokumen_Pabean, A.Tanggal_Daftar, A.Nomor_Daftar;");

            using (var cn = new MySqlConnection(ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "KBRegistrationNo", 
                    
                    //1-5
                    "KBRegistrationDt", 
                    "KBContractNo", 
                    "KBContractDt", 
                    "KBPLNo", 
                    "KBPLDt",

                    //6-10
                    "Vendor",
                    "Item",
                    "Qty",
                    "KBPackaging",
                    "KBPackagingQty",

                    //11-15
                    "KBSubmissionNo",
                    "Kode_Dokumen_Pabean",
                    "KBInsuranceNo",
                    "KBInsuranceDt",
                    "KBInsuranceDueDt",

                    //16-18
                    "KBInsuranceAmt",
                    "KBDecreeNo",
                    "KBDecreeDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_Data()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            KBRegistrationDt = Sm.DrStr(dr, c[1]),
                            KBContractNo = Sm.DrStr(dr, c[2]),
                            KBContractDt = Sm.DrStr(dr, c[3]),
                            KBPLNo = Sm.DrStr(dr, c[4]),
                            KBPLDt = Sm.DrStr(dr, c[5]),
                            Vendor = Sm.DrStr(dr, c[6]),
                            Item = Sm.DrStr(dr, c[7]),
                            Qty = Sm.DrDec(dr, c[8]),
                            KBPackaging = Sm.DrStr(dr, c[9]),
                            KBPackagingQty = Sm.DrDec(dr, c[10]),
                            KBSubmissionNo = Sm.DrStr(dr, c[11]),
                            CustomsDocCode = Sm.DrStr(dr, c[12]),
                            ProcessInd = false,
                            DocNo = string.Empty,
                            KBInsuranceNo = Sm.DrStr(dr, c[13]),
                            KBInsuranceDt = Sm.DrStr(dr, c[14]),
                            KBInsuranceDueDt = Sm.DrStr(dr, c[15]),
                            KBInsuranceAmt = Sm.DrDec(dr, c[16]),
                            KBDecreeNo = Sm.DrStr(dr, c[17]),
                            KBDecreeDt = Sm.DrStr(dr, c[18]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var l2 = new List<KB_Data2>();

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.KBRegistrationNo=@KBRegistrationNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@KBRegistrationNo0" + i.ToString(), l[i].KBRegistrationNo);
            }

            if (Filter.Length != 0) Filter = " And (" + Filter + ") ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select A.KBRegistrationNo, ");
            SQL.AppendLine("Group_Concat(Distinct A.DocNo Order By A.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("And (A.KBRegistrationDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Group By A.KBRegistrationNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "KBRegistrationNo", "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new KB_Data2()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                foreach (var i in l)
                {
                    foreach (var j in l2.Where(w => Sm.CompareStr(w.KBRegistrationNo, i.KBRegistrationNo)))
                    {
                        if (j.DocNo.Length > 0)
                        {
                            i.DocNo = j.DocNo;
                            i.ProcessInd = true;
                        }
                        break;
                    }
                }
                l2.Clear();
            }
        }

        private void Process3(ref List<KB_Data> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].KBContractNo;
                if (l[i].KBContractDt.Length > 0) r.Cells[2].Value = Sm.ConvertDate(l[i].KBContractDt);
                r.Cells[3].Value = l[i].KBPLNo;
                if (l[i].KBPLDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].KBPLDt);
                r.Cells[5].Value = l[i].KBRegistrationNo;
                if (l[i].KBRegistrationDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(l[i].KBRegistrationDt);
                r.Cells[7].Value = l[i].Vendor;
                r.Cells[8].Value = l[i].Item;
                r.Cells[9].Value = l[i].Qty;
                r.Cells[10].Value = l[i].KBPackaging;
                r.Cells[11].Value = l[i].KBPackagingQty;
                r.Cells[12].Value = l[i].KBSubmissionNo;
                r.Cells[13].Value = l[i].CustomsDocCode;
                r.Cells[14].Value = l[i].KBInsuranceNo;
                r.Cells[15].Value = l[i].KBInsuranceDt;
                r.Cells[16].Value = l[i].KBInsuranceDueDt;
                r.Cells[17].Value = l[i].KBInsuranceAmt;
                r.Cells[18].Value = l[i].KBDecreeNo;
                if (l[i].KBDecreeDt.Length > 0) r.Cells[19].Value = Sm.ConvertDate(l[i].KBDecreeDt);
            }
            Grd1.EndUpdate();
        }

        private string ConnectionString
        {
            get
            {
                return
                    string.Concat(
                    @"Server=", mKB_Server, ";Database=", mKB_Database, ";Uid=", mKB_DBUser, ";Password=", mKB_DBPwd, ";Allow User Variables=True;Connection Timeout=1200;",
                    mKB_Port.Length == 0 ? string.Empty : string.Concat("Port=", mKB_Port, ";"));
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtKBContractNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                if (Sm.GetGrdDate(Grd1, Row, 2).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBContractDt, Sm.GetGrdDate(Grd1, Row, 2).Substring(0, 8));
                mFrmParent.TxtKBPLNo.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                if (Sm.GetGrdDate(Grd1, Row, 4).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBPLDt, Sm.GetGrdDate(Grd1, Row, 4).Substring(0, 8));
                mFrmParent.TxtKBRegistrationNo.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                if (Sm.GetGrdDate(Grd1, Row, 6).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBRegistrationDt, Sm.GetGrdDate(Grd1, Row, 6).Substring(0, 8));
                mFrmParent.TxtKBPackaging.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 11), 0);
                mFrmParent.TxtKBSubmissionNo.EditValue = Sm.GetGrdStr(Grd1, Row, 12);
                //mFrmParent.TxtCustomsDocCode.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.mKBInsuranceNo = Sm.GetGrdStr(Grd1, Row, 14);
                mFrmParent.mKBInsuranceDt = Sm.GetGrdStr(Grd1, Row, 15);
                mFrmParent.mKBInsuranceDueDt = Sm.GetGrdStr(Grd1, Row, 16);
                mFrmParent.TxtKBInsuranceAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 17), 0);
                mFrmParent.TxtKBDecreeNo.EditValue = Sm.GetGrdStr(Grd1, Row, 18);
                if (Sm.GetGrdDate(Grd1, Row, 19).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBDecreeDt, Sm.GetGrdDate(Grd1, Row, 19).Substring(0, 8));
                mFrmParent.ClearGrd();
                this.Close();
            }
        }

        private void GetParameter()
        {
            mKB_Server = Sm.GetParameter("KB_Server");
            mKB_Database = Sm.GetParameter("KB_Database");
            mKB_DBUser = Sm.GetParameter("KB_DBUser");
            mKB_DBPwd = Sm.GetParameter("KB_DBPwd");
            mKB_Port = Sm.GetParameter("KB_Port");
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

        #region Class

        private class KB_Data
        {
            public string KBRegistrationNo { get; set; }
            public string KBRegistrationDt { get; set; }
            public string KBContractNo { get; set; }
            public string KBContractDt { get; set; }
            public string KBPLNo { get; set; }
            public string KBPLDt { get; set; }
            public string KBSubmissionNo { get; set; }
            public string Vendor { get; set; }
            public string Item { get; set; }
            public decimal Qty { get; set; }
            public string KBPackaging { get; set; }
            public decimal KBPackagingQty { get; set; }
            public bool ProcessInd { get; set; }
            public string CustomsDocCode { get; set; }
            public string DocNo { get; set; }
            public string KBInsuranceNo { get; set; }
            public string KBInsuranceDt { get; set; }
            public string KBInsuranceDueDt { get; set; }
            public decimal KBInsuranceAmt { get; set; }
            public string KBDecreeNo { get; set; }
            public string KBDecreeDt { get; set; }
        }

        private class KB_Data2
        {
            public string KBRegistrationNo { get; set; }
            public string DocNo { get; set; }
        }

        #endregion

    }
}
