#region Update
/*
    04/02/2022 [ICA/PRODUCT] new apps
    08/02/2022 [ISD/PHT] tambah filter multi profit center dengan parameter IsRptAssetBookValueUseMultiProfitCenterFilter
    13/07/2022 [RDA/PRODUCT] perbaikan nilai kolom ketika asset yg ditarik dari data master asset dan initial master asset 
    16/08/2022 [RDA/PRODUCT] penyesuaian kolom : 
                            1.Asset's Value 
                            2.Date of Recondition
                            3.Recondition Reason 
                            4.Recondition Amount
                            5.Total Asset's Value
                            6.Asset's Acc.Depr. on Recondition Date
                            7.Remaining Economic Life on Start Date
                            8.Remaining Economic Life on End Date, Acc.Depr. on Start Date
                            9.NBV on Start Date, Depreciation Value to End Date
                            10.Acc.Depr. on End Date  
                            11.NBV on End Date  
                            12.Asset Value on End Date
    13/07/2022 [RDA/PRODUCT] perbaikan nilai kolom depreciation value to end date dan duplikat data
    26/08/2022 [RDA/PRODUCT] perbaikan nilai rekondisi dan economic life
    29/08/2022 [RDA/PRODUCT] perbaikan nilai asset value dan total asset value ketika ada perbaikan rekondisi dan economic life  
    31/08/2022 [RDA/PRODUCT] perbaikan nilai depreciation value to end date 
    01/09/2022 [RDA/PRODUCT] perbaikan nilai acc. depr. on start date
    16/09/2022 [IBL/PRODUCT] penyesuaian nilai remaining eco life on start date, remaining eco life on end date, Acc. Depr. On Start Date, & Depreciation Value to End Date
                             serta menjadikan tiap asset hanya memunculkan 1 baris data (nilai2 yg double akan dirangkum jadi 1)
    29/09/2022 [IBL/PRODUCT] Jika Purchase Date (untuk Master Asset) atau Depreciation Date (untuk Initial Master Asset) lebih dari Filter End Date, maka dokumen tersebut tidak muncul di reporting Asset Book Value.
                             Feedback : Kolom economic life on start date dan Acc. Depr Value on Start Date belum sesuai.
    07/10/2022 [IBL/PRODUCT] Feedback : muncul warning ketika direfresh
*/
#endregion

#region Namespace

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sl = RunSystem.SetLue;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmRptAssetBookValue : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool
            mIsAllProfitCenterSelected = false,
            mIsRptAssetBookValueUseMultiProfitCenterFilter = false;
        private string
            mQueryProfitCenter = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAssetBookValue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL(string.Empty);

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "1950");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                DteStartDt.DateTime = Sm.ConvertDate(CurrentDateTime);
                DteEndDt.DateTime = Sm.ConvertDate(CurrentDateTime);

                Sl.SetLueAssetCategoryCode(ref LueAssetCtCode);
                int ypoint = 21;
                if (mIsRptAssetBookValueUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode2);
                    label3.Visible = LueProfitCenterCode.Visible = ChkProfitCenterCode.Visible = false;
                    label7.Top = label7.Top - ypoint; CcbProfitCenterCode2.Top = CcbProfitCenterCode2.Top - ypoint; ChkProfitCenterCode2.Top = ChkProfitCenterCode2.Top - ypoint;
                    label5.Top = label5.Top - ypoint; LueSiteCode.Top = LueSiteCode.Top - ypoint; ChkSiteCode.Top = ChkSiteCode.Top - ypoint;
                }
                else
                {
                    Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);
                    label7.Visible = CcbProfitCenterCode2.Visible = ChkProfitCenterCode2.Visible = false;
                    label5.Top = label5.Top - ypoint; LueSiteCode.Top = LueSiteCode.Top - ypoint; ChkSiteCode.Top = ChkSiteCode.Top - ypoint;
                }
                Sl.SetLueSiteCode(ref LueSiteCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAssetBookValueUseMultiProfitCenterFilter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAssetBookValueUseMultiProfitCenterFilter": mIsRptAssetBookValueUseMultiProfitCenterFilter = ParValue == "Y"; break;

                        }
                    }
                }
                dr.Close();
            }
        }

        /* Old Code SetSQL(). Comment by ibl 14/09/2022
        private void SetSQL(string mQueryProfitCenter)
        {
            var SQL = new StringBuilder();

            #region Old

            //SQL.AppendLine("SELECT A.AssetCode, A.AssetName, E.AssetCategoryName, A.AssetDt, ");
            //SQL.AppendLine("ifnull(A.AssetValue, 0.00) - ifnull(A.AccDepr, 0.00) - ifnull(F.ReconditionValue, 0.00) AssetValue, "); //A.AssetValue,
            //SQL.AppendLine("F.ReconditionDt, F.ReconditionReason, ifnull(F.ReconditionValue, 0.00) ReconditionValue, ");
            //SQL.AppendLine("ifnull(A.ResidualValue, 0.00) ResidualValue, ifnull(F.AssetValue2, 0.00) AccDeprRoADt, G.DepreciationDt, ifnull(G.EcoLife, 0.00) EcoLife, ifnull(G.EcoLifeYr, 0.00) EcoLifeYr, ");
            //SQL.AppendLine("ifnull(F.EcoLife2, 0.00) EcoLifeRoA, ifnull(F.EcoLifeRoAYr, 0.00) EcoLifeRoAYr, ifnull(H.RemainingEcoStartDt, 0.00) RemainingEcoStartDt, ifnull(H.RemainingEcoEndDt, 0.00) RemainingEcoEndDt, ");
            //SQL.AppendLine("D.OptDesc DepreciationMethod, ifnull(G.PercentageAnnualDepreciation, 0.00) AnnualDepreciationRate, ifnull(H.DepreciationValue, 0.00) DepreciationValue, ");
            //SQL.AppendLine("ifnull(H.AccDeprValueStartDt, 0.00) AccDeprValueStartDt, ifnull(H.NBVStartDt, 0.00) NBVStartDt, 0.00 AS AssetSold, ifnull(I.AccDeprValueEndDt, 0.00) AccDeprValueEndDt, ifnull(I.NBVEndDt, 0.00) NBVEndDt, ");
            //SQL.AppendLine("IFNULL(I.AccDeprValueEndDt, 0.00) + IFNULL(I.NBVEndDt, 0.00) AssetValueEndDt, "); //IFNULL(AssetValueEndDt, 0.00) AssetValueEndDt,  
            //SQL.AppendLine("A.AcNo, J.AcDesc, A.AcNo2 AcNoDepreciation, K.AcDesc AcDescDepreciation, ");
            //SQL.AppendLine("L.OptDesc Classification, M.OptDesc SubClassification, N.OptDesc AssetType, O.OptDesc SubType, P.SiteName, A.ShortCode, A.Parent, A.DisplayName, A.ActiveInd, ");
            //SQL.AppendLine("A.AssetType WorkCenter, A.LeasingInd, A.RentedInd, A.SoldInd, A.FiskalInd, B.CCCOde, B.CCName, C.ProfitCenterCode, C.ProfitCenterName ");
            //SQL.AppendLine("FROM TblAsset A ");
            //SQL.AppendLine("INNER JOIN TblCostCenter B ON A.CCCode = B.CCCode  ");
            //SQL.AppendLine("INNER JOIN TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode ");
            //if (mIsRptAssetBookValueUseMultiProfitCenterFilter) SQL.AppendLine(mQueryProfitCenter);
            //SQL.AppendLine("INNER JOIN TblOption D ON D.OptCat = 'DepreciationMethod' AND A.DepreciationCode = D.OptCode ");
            //SQL.AppendLine("LEFT JOIN TblAssetCategory E ON A.AssetCategoryCode = E.AssetCategoryCode ");
            ////SQL.AppendLine("LEFT JOIN ( ");
            ////SQL.AppendLine("	SELECT A.DocNo, A.AssetCode, A.ReconditionDt, A.ReconditionReason, A.ReconditionValue, A.Ecolife2, ");
            ////SQL.AppendLine("	IFNULL(A.EcoLife2, 0.00)/12 EcoLifeRoAYr, A.AssetValue2 ");
            ////SQL.AppendLine("	FROM TblReconditionAssetHdr A ");
            ////SQL.AppendLine("	WHERE A.CancelInd = 'N' AND A.Status = 'A' ");
            ////SQL.AppendLine("		And A.DocNo = ( ");
            ////SQL.AppendLine("			SELECT DocNo ");
            ////SQL.AppendLine("			FROM TblReconditionAssetHdr ");
            ////SQL.AppendLine("			WHERE AssetCode = A.AssetCode ");
            ////SQL.AppendLine("				AND CancelInd = 'N' ");
            ////SQL.AppendLine("				AND STATUS = 'A' ");
            ////SQL.AppendLine("			ORDER BY CreateDt Desc ");
            ////SQL.AppendLine("			LIMIT 1 ) ");
            ////SQL.AppendLine(")F ON A.AssetCode = F.AssetCode ");
            //SQL.AppendLine("LEFT JOIN (  ");
            //SQL.AppendLine("    SELECT A.DocNo, A.AssetCode, A.ReconditionDt, A.ReconditionReason, B.ReconditionValue, A.Ecolife2,  ");
            //SQL.AppendLine("    IFNULL(A.EcoLife2, 0.00)/12 EcoLifeRoAYr, A.AssetValue2  ");
            //SQL.AppendLine("    FROM TblReconditionAssetHdr A  ");
            //SQL.AppendLine("    INNER JOIN( ");
            //SQL.AppendLine("        SELECT MAX(DocNo) DocNo, AssetCode, SUM(ReconditionValue) ReconditionValue ");
            //SQL.AppendLine("        FROM tblReconditionAssetHdr ");
            //SQL.AppendLine("        GROUP BY AssetCode ");
            //SQL.AppendLine("    )B ON A.DocNo = B.DocNo AND A.AssetCode = B.AssetCode ");
            //SQL.AppendLine("    WHERE A.CancelInd = 'N' AND A.Status = 'A'  ");
            //SQL.AppendLine(")F ON A.AssetCode = F.AssetCode  ");
            //SQL.AppendLine("Left Join TblDepreciationAssetHdr G On A.AssetCode = G.AssetCode ");
            //SQL.AppendLine("    And G.CancelInd = 'N' ");
            //SQL.AppendLine("    And G.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = G.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1) ");
            ////SQL.AppendLine("LEFT JOIN ( ");
            ////SQL.AppendLine("	SELECT A.DocNo, A.AssetCode, B.DepreciationValue, ");
            ////SQL.AppendLine("	B.AccDeprValue AccDeprValueStartDt, B.NBV NBVStartDt, CONCAT(C.Yr, C.Mth, '01'), ");
            ////SQL.AppendLine("	Case ");
            ////SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) < 0 Then 0 ");
            ////SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) + 1 ");
            ////SQL.AppendLine("	End RemainingEcoStartDt, ");
            ////SQL.AppendLine("	Case ");
            ////SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) < 0 Then 0 ");
            ////SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) + 1 ");
            ////SQL.AppendLine("	End RemainingEcoEndDt ");
            ////SQL.AppendLine("	FROM TblDepreciationAssetHdr A ");
            ////SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            ////SQL.AppendLine("	INNER JOIN ( ");
            ////SQL.AppendLine("		SELECT A.DocNo, A.DNo, Mth, Yr ");
            ////SQL.AppendLine("		FROM TblDepreciationAssetDtl A ");
            ////SQL.AppendLine("		WHERE A.DNo = (SELECT MAX(DNo) FROM TblDepreciationAssetDtl WHERE DocNo = A.DocNo GROUP BY DocNo) ");
            ////SQL.AppendLine("	)C ON B.DocNo = C.DocNo ");
            ////SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1) ");
            ////SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2)) ");
            ////SQL.AppendLine(")H ON A.AssetCode = H.AssetCode And G.DocNo = H.DocNo ");
            //SQL.AppendLine("LEFT JOIN (  ");
            //SQL.AppendLine("	SELECT A.DocNo, A.AssetCode, B.DepreciationValue,  ");
            //SQL.AppendLine("	case  ");
            //SQL.AppendLine("		when D.AssetSource = '1' then IFNULL(E.AccDeprValue, 0.00)  ");
            //SQL.AppendLine("		when D.AssetSource = '2' then IFNULL(E.AccDeprValue, 0.00) + IFNULL(D.AccDepr, 0.00) ");
            //SQL.AppendLine("		ELSE 0.00 ");
            //SQL.AppendLine("	END AccDeprValueStartDt,  ");
            //SQL.AppendLine("	B.NBV NBVStartDt, CONCAT(C.Yr, C.Mth, '01'),  ");
            //SQL.AppendLine("	Case  ");
            //SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) < 0 Then 0  ");
            //SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) + 1  ");
            //SQL.AppendLine("	End RemainingEcoStartDt,  ");
            //SQL.AppendLine("	Case  ");
            //SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) < 0 Then 0  ");
            //SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) + 1  ");
            //SQL.AppendLine("	End RemainingEcoEndDt  ");
            //SQL.AppendLine("	FROM TblDepreciationAssetHdr A  ");
            //SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N'  ");
            //SQL.AppendLine("	INNER JOIN (  ");
            //SQL.AppendLine("		SELECT A.DocNo, A.DNo, Mth, Yr  ");
            //SQL.AppendLine("		FROM TblDepreciationAssetDtl A  ");
            //SQL.AppendLine("		WHERE A.DNo = (SELECT MAX(DNo) FROM TblDepreciationAssetDtl WHERE DocNo = A.DocNo GROUP BY DocNo)  ");
            //SQL.AppendLine("	)C ON B.DocNo = C.DocNo  ");
            //SQL.AppendLine("	LEFT JOIN tblasset D ON A.AssetCode = D.AssetCode ");
            //SQL.AppendLine("	LEFT JOIN ( ");
            //SQL.AppendLine("		SELECT SUM(AccDeprValue) AccDeprValue, DocNo ");
            //SQL.AppendLine("		From tbldepreciationassetdtl ");
            //SQL.AppendLine("		WHERE journaldocno IS NOT NULL ");
            //SQL.AppendLine("		AND Concat(Yr, Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2)) ");
            //SQL.AppendLine("		GROUP BY DocNo ");
            //SQL.AppendLine("	)E On B.DocNo = E.DocNo ");
            //SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1)  ");
            //SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            //SQL.AppendLine(")H ON A.AssetCode = H.AssetCode And G.DocNo = H.DocNo  ");
            ////SQL.AppendLine("LEFT JOIN( ");
            ////SQL.AppendLine("	SELECT A.AssetCode, B.AccDeprValue AccDeprValueEndDt, B.NBV NBVEndDt, (B.AccDeprValue + B.NBV) AssetValueEndDt ");
            ////SQL.AppendLine("	FROM TblDepreciationAssetHdr A ");
            ////SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            ////SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1) ");
            ////SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2)) ");
            ////SQL.AppendLine(")I ON A.AssetCode = I.AssetCode ");
            //SQL.AppendLine("LEFT JOIN(  ");
            //SQL.AppendLine("	SELECT A.AssetCode,  ");
            //SQL.AppendLine("	case  ");
            //SQL.AppendLine("		when D.AssetSource = '1' then IFNULL(E.AccDeprValue, 0.00)  ");
            //SQL.AppendLine("		when D.AssetSource = '2' then IFNULL(E.AccDeprValue, 0.00) + IFNULL(D.AccDepr, 0.00) ");
            //SQL.AppendLine("		ELSE 0.00 ");
            //SQL.AppendLine("	END AccDeprValueEndDt,  ");
            //SQL.AppendLine("	B.NBV NBVEndDt, (B.AccDeprValue + B.NBV) AssetValueEndDt  ");
            //SQL.AppendLine("	FROM TblDepreciationAssetHdr A  ");
            //SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N'  ");
            //SQL.AppendLine("	LEFT JOIN tblasset D ON A.AssetCode = D.AssetCode ");
            //SQL.AppendLine("	LEFT JOIN ( ");
            //SQL.AppendLine("		SELECT SUM(AccDeprValue) AccDeprValue, DocNo ");
            //SQL.AppendLine("		From tbldepreciationassetdtl ");
            //SQL.AppendLine("		WHERE journaldocno IS NOT NULL ");
            //SQL.AppendLine("		AND Concat(Yr, Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2)) ");
            //SQL.AppendLine("		GROUP BY DocNo ");
            //SQL.AppendLine("	)E On B.DocNo = E.DocNo ");
            //SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1)  ");
            //SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            //SQL.AppendLine(")I ON A.AssetCode = I.AssetCode ");
            //SQL.AppendLine("LEFT JOIN TblCOA J ON A.AcNo = J.AcNo ");
            //SQL.AppendLine("LEFT JOIN TblCOA K ON A.AcNo2 = K.AcNo ");
            //SQL.AppendLine("LEFT JOIN TblOption L ON L.OptCat = 'AssetClassification' AND L.OptCode = A.Classification ");
            //SQL.AppendLine("LEFT JOIN TblOption M ON M.OptCat = 'AssetSubClassification' AND M.OptCode = A.SubClassification ");
            //SQL.AppendLine("LEFT JOIN TblOption N ON N.OptCat = 'AssetType' AND N.OptCode = A.Type ");
            //SQL.AppendLine("LEFT JOIN TblOption O ON O.OptCat = 'AssetSubType' AND O.OptCode = A.SubType ");
            //SQL.AppendLine("LEFT JOIN TblSite P ON A.SiteCode = P.SiteCode ");
            //SQL.AppendLine("WHERE A.ActiveInd = 'Y' ");
            ////SQL.AppendLine("    AND A.AssetDt BETWEEN @StartDt AND @EndDt ");

            #endregion

            SQL.AppendLine("SELECT DISTINCT A.AssetCode, A.AssetName, E.AssetCategoryName, A.AssetDt,  ");
            SQL.AppendLine("ifnull(A.AssetValue, 0.00) - IFNULL(F2.ReconditionValue, 0.00) AssetValue,  ");
            SQL.AppendLine("F.ReconditionDt, F.ReconditionReason, ifnull(F.ReconditionValue, 0.00) ReconditionValue,  ");
            SQL.AppendLine("ifnull(G.ResidualValue, 0.00) ResidualValue, ifnull(F.AssetValue2, 0.00) AccDeprRoADt,   ");
            SQL.AppendLine("G.DepreciationDt, ifnull(G.EcoLife, 0.00) EcoLife, ifnull(G.EcoLifeYr, 0.00) EcoLifeYr,  ");
            SQL.AppendLine("ifnull(F.EcoLife3, 0.00) EcoLifeRoA, ifnull(F.EcoLifeRoAYr, 0.00) EcoLifeRoAYr,  ");
            SQL.AppendLine("IFNULL(R.RemainingEcoStartDt, 0.00) RemainingEcoStartDt, IFNULL(S.RemainingEcoEndDt, 0.00) RemainingEcoEndDt,  ");
            SQL.AppendLine("D.OptDesc DepreciationMethod, ifnull(G.PercentageAnnualDepreciation, 0.00) AnnualDepreciationRate, ifnull(H.DepreciationValue, 0.00) DepreciationValue,  ");
            SQL.AppendLine("IFNULL(H2.AccDepr, 0.00) AccDeprValueStartDt, ifnull(H.NBVStartDt, 0.00) NBVStartDt, 0.00 AS AssetSold,  ");
            SQL.AppendLine("IFNULL(I2.AccDeprValueEndDt, 0.00) AccDeprValueEndDt,   ");
            SQL.AppendLine("IFNULL(I2.NBVEndDt, 0.00) NBVEndDt, ");
            SQL.AppendLine("IFNULL(Q.AssetValueEndDt, 0.00) AssetValueEndDt, ");
            SQL.AppendLine("A.AcNo, J.AcDesc, A.AcNo2 AcNoDepreciation, K.AcDesc AcDescDepreciation,  ");
            SQL.AppendLine("L.OptDesc Classification, M.OptDesc SubClassification, N.OptDesc AssetType, O.OptDesc SubType, P.SiteName, A.ShortCode, A.Parent, A.DisplayName, A.ActiveInd,  ");
            SQL.AppendLine("A.AssetType WorkCenter, A.LeasingInd, A.RentedInd, A.SoldInd, A.FiskalInd, B.CCCOde, B.CCName, C.ProfitCenterCode, C.ProfitCenterName , ");
            SQL.AppendLine("(ifnull(A.AssetValue, 0.00) - IF(F.ReconditionDt is not null, ifnull(F.ReconditionValue, 0.00),  ");
            SQL.AppendLine("IFNULL(F2.ReconditionValue, 0.00))) + ifnull(F.ReconditionValue, 0.00) TotalAssetValue, ");
            SQL.AppendLine("((ifnull(A.AssetValue, 0.00) - IF(F.ReconditionDt is not null, ifnull(F.ReconditionValue, 0.00),  ");
            SQL.AppendLine("IFNULL(F2.ReconditionValue, 0.00))) + ifnull(F.ReconditionValue, 0.00)) - ifnull(F.AssetValue2, 0.00) - ifnull(G.ResidualValue, 0.00) AssetDepreciableValue,  ");
            SQL.AppendLine("IFNULL(T.DepreciationValueEndDt, 0.00) DepreciationValueEnd ");
            SQL.AppendLine("FROM TblAsset A ");
            SQL.AppendLine("INNER JOIN TblCostCenter B ON A.CCCode = B.CCCode  ");
            SQL.AppendLine("INNER JOIN TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode ");
            if (mIsRptAssetBookValueUseMultiProfitCenterFilter) SQL.AppendLine(mQueryProfitCenter);
            SQL.AppendLine("INNER JOIN TblOption D ON D.OptCat = 'DepreciationMethod' AND A.DepreciationCode = D.OptCode ");
            SQL.AppendLine("LEFT JOIN TblAssetCategory E ON A.AssetCategoryCode = E.AssetCategoryCode ");
            SQL.AppendLine("LEFT JOIN (   ");
            SQL.AppendLine("    SELECT A.DocNo, A.AssetCode, A.ReconditionDt, A.ReconditionReason, B.ReconditionValue, A.Ecolife3,   ");
            SQL.AppendLine("    IFNULL(A.EcoLife3, 0.00)/12 EcoLifeRoAYr, A.AssetValue2   ");
            SQL.AppendLine("    FROM TblReconditionAssetHdr A   ");
            SQL.AppendLine("    INNER JOIN(  ");
            SQL.AppendLine("        SELECT DocNo, AssetCode, SUM(ReconditionValue) ReconditionValue ");
            SQL.AppendLine("        FROM tblReconditionAssetHdr  ");
            SQL.AppendLine("        WHERE CancelInd = 'N' ");
            SQL.AppendLine("        AND STATUS = 'A' ");
            SQL.AppendLine("        GROUP BY DocNo, AssetCode  ");
            SQL.AppendLine("    )B ON A.DocNo = B.DocNo AND A.AssetCode = B.AssetCode  ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' AND A.Status = 'A'   ");
            SQL.AppendLine("    AND A.ReconditionDt BETWEEN @StartDt AND @EndDt ");
            SQL.AppendLine(")F ON A.AssetCode = F.AssetCode   ");
            SQL.AppendLine("LEFT JOIN (   ");
            SQL.AppendLine("    SELECT A.AssetCode, B.ReconditionValue  ");
            SQL.AppendLine("    FROM TblReconditionAssetHdr A   ");
            SQL.AppendLine("    INNER JOIN(  ");
            SQL.AppendLine("        SELECT MAX(DocNo) DocNo, AssetCode, SUM(ReconditionValue) ReconditionValue  ");
            SQL.AppendLine("        FROM tblReconditionAssetHdr  ");
            SQL.AppendLine("        WHERE CancelInd = 'N' ");
            SQL.AppendLine("        GROUP BY AssetCode  ");
            SQL.AppendLine("    )B ON A.DocNo = B.DocNo AND A.AssetCode = B.AssetCode  ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' AND A.Status = 'A'   ");
            SQL.AppendLine(")F2 ON A.AssetCode = F2.AssetCode   ");
            SQL.AppendLine("Left Join TblDepreciationAssetHdr G On A.AssetCode = G.AssetCode  ");
            SQL.AppendLine("    And G.CancelInd = 'N'  ");
            SQL.AppendLine("    And G.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = G.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1) ");
            SQL.AppendLine("LEFT JOIN (   ");
            SQL.AppendLine("	SELECT A.DocNo, A.AssetCode, B.DepreciationValue,   ");
            SQL.AppendLine("	B.AccDeprValue AccDeprValueStartDt, ");
            SQL.AppendLine("	B.NBV NBVStartDt, CONCAT(C.Yr, C.Mth, '01'),   ");
            SQL.AppendLine("	Case   ");
            SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) < 0 Then 0   ");
            SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@StartDt, 6)) + 1   ");
            SQL.AppendLine("	End RemainingEcoStartDt,   ");
            SQL.AppendLine("	Case   ");
            SQL.AppendLine("		When Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) < 0 Then 0   ");
            SQL.AppendLine("		ELSE Period_Diff(CONCAT(C.Yr, C.Mth), LEFT(@EndDt, 6)) + 1   ");
            SQL.AppendLine("	End RemainingEcoEndDt   ");
            SQL.AppendLine("	FROM TblDepreciationAssetHdr A   ");
            SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N'   ");
            SQL.AppendLine("	INNER JOIN (   ");
            SQL.AppendLine("		SELECT A.DocNo, A.DNo, Mth, Yr   ");
            SQL.AppendLine("		FROM TblDepreciationAssetDtl A   ");
            SQL.AppendLine("		WHERE A.DNo = (SELECT MAX(DNo) FROM TblDepreciationAssetDtl WHERE DocNo = A.DocNo GROUP BY DocNo)   ");
            SQL.AppendLine("	)C ON B.DocNo = C.DocNo   ");
            SQL.AppendLine("	LEFT JOIN tblasset D ON A.AssetCode = D.AssetCode  ");
            SQL.AppendLine("	LEFT JOIN (  ");
            SQL.AppendLine("		SELECT SUM(AccDeprValue) AccDeprValue, DocNo  ");
            SQL.AppendLine("		From tbldepreciationassetdtl  ");
            SQL.AppendLine("		WHERE journaldocno IS NOT NULL  ");
            SQL.AppendLine("		AND Concat(Yr, Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            SQL.AppendLine("		GROUP BY DocNo  ");
            SQL.AppendLine("	)E On B.DocNo = E.DocNo  ");
            SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1) ");
            SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))   ");
            SQL.AppendLine(")H ON A.AssetCode = H.AssetCode And G.DocNo = H.DocNo   ");
            SQL.AppendLine("LEFT JOIN(   ");
            SQL.AppendLine("	SELECT A.AssetCode,   ");
            SQL.AppendLine("	B.AccDeprValue AccDeprValueEndDt, ");
            SQL.AppendLine("	B.NBV NBVEndDt, (B.AccDeprValue + B.NBV) AssetValueEndDt   ");
            SQL.AppendLine("	FROM TblDepreciationAssetHdr A   ");
            SQL.AppendLine("	INNER JOIN TblDepreciationAssetDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N'   ");
            SQL.AppendLine("	LEFT JOIN tblasset D ON A.AssetCode = D.AssetCode  ");
            SQL.AppendLine("	LEFT JOIN (  ");
            SQL.AppendLine("		SELECT SUM(AccDeprValue) AccDeprValue, DocNo  ");
            SQL.AppendLine("		From tbldepreciationassetdtl  ");
            SQL.AppendLine("		WHERE journaldocno IS NOT NULL  ");
            SQL.AppendLine("		AND Concat(Yr, Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            SQL.AppendLine("		GROUP BY DocNo  ");
            SQL.AppendLine("	)E On B.DocNo = E.DocNo  ");
            SQL.AppendLine("	WHERE A.DocNo = (SELECT DocNo FROM TblDepreciationAssetHdr WHERE AssetCode = A.AssetCode AND CancelInd = 'N' ORDER BY CreateDt DESC LIMIT 1)   ");
            SQL.AppendLine("		And Concat(B.Yr, B.Mth) = CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))   ");
            SQL.AppendLine(")I ON A.AssetCode = I.AssetCode  ");
            SQL.AppendLine("LEFT JOIN(  ");
            SQL.AppendLine("	SELECT  ");
            SQL.AppendLine("	case ");
            SQL.AppendLine("		when @StartDt >= T1.OpeningBalanceDt ");
            SQL.AppendLine("		then ");
            SQL.AppendLine("			if(@StartDt < DATE_FORMAT(LAST_DAY(@StartDt), '%Y%m%d'), ");
            SQL.AppendLine("				if(T1.AssetSource = '2', IFNULL(T1.AccDepr, 0.00), 0.00), ");
            SQL.AppendLine("				IFNULL(T3.AccDeprValue, 0.00) ");
            SQL.AppendLine("				)  ");
            SQL.AppendLine("		ELSE  ");
            SQL.AppendLine("			0.00 ");
            SQL.AppendLine("	END AccDepr, ");
            SQL.AppendLine("	T1.AssetCode, IFNULL(T1.AccDepr, 0.00) + IFNULL(T2.DepreciationValue, 0.00) AS AccDepr2, IFNULL(T2.RemainingEcoStartDt, 0.00) RemainingEcoStartDt  ");
            SQL.AppendLine("	FROM tblasset T1  ");
            SQL.AppendLine("	Left JOIN (  ");
            SQL.AppendLine("	SELECT X.AssetCode, SUM(X.DepreciationValue) AS DepreciationValue, X.RemainingEcoStartDt  ");
            SQL.AppendLine("	FROM (  ");
            SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt  ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE a.cancelind = 'Y'  ");
            SQL.AppendLine("			AND a.CancelByInd = '2'  ");
            SQL.AppendLine("			AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            SQL.AppendLine("			UNION ALL  ");
            SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt  ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE a.cancelind = 'N'  ");
            SQL.AppendLine("			AND a.CancelByInd = '1'  ");
            SQL.AppendLine("			AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))   ");
            SQL.AppendLine("		)X GROUP BY X.AssetCode  ");
            SQL.AppendLine("	) T2 ON T1.AssetCode = T2.AssetCode ");
            SQL.AppendLine("	LEFT JOIN ( ");
            SQL.AppendLine("			SELECT X1.AssetCode, X1.AccDeprValue FROM ( ");
            SQL.AppendLine("			SELECT CONCAT(b.Yr, b.Mth) As YrMth, a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt , ");
            SQL.AppendLine("			b.JournalDocNo ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE a.cancelind = 'Y'  ");
            SQL.AppendLine("			AND a.CancelByInd = '2'  ");
            SQL.AppendLine("			AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= SUBSTRING(@StartDt, 1, 6) ");
            SQL.AppendLine("			UNION ALL  ");
            SQL.AppendLine("			SELECT CONCAT(b.Yr, b.Mth) As YrMth, a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt , ");
            SQL.AppendLine("			b.JournalDocNo ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE a.cancelind = 'N'  ");
            SQL.AppendLine("			AND a.CancelByInd = '1'  ");
            SQL.AppendLine("			AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= SUBSTRING(@StartDt, 1, 6) ");
            SQL.AppendLine("			AND a.AssetCode = @AssetCode ");
            SQL.AppendLine("		)X1 INNER JOIN ( ");
            SQL.AppendLine("			SELECT MAX(CONCAT(Yr,Mth)) AS YrMth FROM ( ");
            SQL.AppendLine("				SELECT b.Mth, b.Yr ");
            SQL.AppendLine("				FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("				INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno ");
            SQL.AppendLine("				WHERE a.cancelind = 'Y'  ");
            SQL.AppendLine("				AND a.CancelByInd = '2'  ");
            SQL.AppendLine("				AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("				And CONCAT(b.Yr, b.Mth) <= SUBSTRING(@StartDt, 1, 6) ");
            SQL.AppendLine("				UNION ALL  ");
            SQL.AppendLine("				SELECT b.Mth, b.Yr ");
            SQL.AppendLine("				FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("				INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno ");
            SQL.AppendLine("				WHERE a.cancelind = 'N'  ");
            SQL.AppendLine("				AND a.CancelByInd = '1'  ");
            SQL.AppendLine("				AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("				And CONCAT(b.Yr, b.Mth) <= SUBSTRING(@StartDt, 1, 6) ");
            SQL.AppendLine("			)T ");
            SQL.AppendLine("		)X2 ON X1.YrMth = X2.YrMth ");
            SQL.AppendLine("	) T3 ON T1.AssetCode = T3.AssetCode  ");
            SQL.AppendLine(")H2 ON A.AssetCode = H2.AssetCode  ");
            SQL.AppendLine("LEFT JOIN( ");
            //SQL.AppendLine("	SELECT T1.AccDepr, T1.AssetCode, IFNULL(T1.AccDepr, 0.00) + IFNULL(T2.DepreciationValue, 0.00) AS AccDepr2, IFNULL(T2.RemainingEcoEndDt, 0.00) RemainingEcoEndDt ");
            //SQL.AppendLine("	FROM tblasset T1 ");
            //SQL.AppendLine("	Left JOIN ( ");
            //SQL.AppendLine("	SELECT X.AssetCode, SUM(X.DepreciationValue) AS DepreciationValue, RemainingEcoEndDt ");
            //SQL.AppendLine("	FROM ( ");
            //SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, a.EcoLife - b.MthPurchase AS RemainingEcoEndDt ");
            //SQL.AppendLine("			FROM tbldepreciationassethdr a ");
            //SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno ");
            //SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno ");
            //SQL.AppendLine("			WHERE a.cancelind = 'Y' ");
            //SQL.AppendLine("			AND a.CancelByInd = '2' ");
            //SQL.AppendLine("			AND b.journaldocno IS NOT NULL ");
            //SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            //SQL.AppendLine("			UNION ALL ");
            //SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, a.EcoLife - b.MthPurchase AS RemainingEcoEndDt ");
            //SQL.AppendLine("			FROM tbldepreciationassethdr a ");
            //SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno ");
            //SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno ");
            //SQL.AppendLine("			WHERE a.cancelind = 'N' ");
            //SQL.AppendLine("			AND a.CancelByInd = '1' ");
            //SQL.AppendLine("			AND b.journaldocno IS NOT NULL ");
            //SQL.AppendLine("			And CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            //SQL.AppendLine("		)X GROUP BY X.AssetCode ");
            //SQL.AppendLine("	) T2 ON T1.AssetCode = T2.AssetCode ");

            //=====

            //SQL.AppendLine("    SELECT A.DocNo, A.AssetCode, SUM(B.DepreciationValue) DepreciationValue, MAX(B.AccDeprValue) AccDeprValueEndDt, MIN(B.NBV) NBVEndDt, A.EcoLife - MAX(B.MthPurchase) EcoLifeEnd ");
            //SQL.AppendLine("    FROM tbldepreciationassethdr A ");
            //SQL.AppendLine("    INNER JOIN tbldepreciationassetdtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("    WHERE B.JournalDocNo IS NOT NULL ");
            //SQL.AppendLine("    AND CONCAT(B.Yr, B.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2)) ");
            //SQL.AppendLine("    GROUP BY A.DocNo, A.AssetCode ");

            SQL.AppendLine("SELECT X.AssetCode, SUM(X.DepreciationValue) DepreciationValue, X.AccDeprValueEndDt, X.NBVEndDt, X.EcoLifeEnd ");
            SQL.AppendLine("FROM( ");
            SQL.AppendLine("    SELECT A.DocNo, A.AssetCode, SUM(B.DepreciationValue) DepreciationValue, MAX(B.AccDeprValue) AccDeprValueEndDt, MIN(B.NBV) NBVEndDt, A.EcoLife - MAX(B.MthPurchase) EcoLifeEnd  ");
            SQL.AppendLine("    FROM tbldepreciationassethdr A  ");
            SQL.AppendLine("    INNER JOIN tbldepreciationassetdtl B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("    INNER JOIN ( ");
            SQL.AppendLine("        SELECT MAX(X1.DocNo) DocNo, X1.AssetCode ");
            SQL.AppendLine("        FROM tbldepreciationassethdr X1 ");
            SQL.AppendLine("        INNER JOIN tbldepreciationassetdtl X2 ON X1.DocNo = X2.DocNo  ");
            SQL.AppendLine("        WHERE X2.JournalDocNo IS NOT NULL ");
            SQL.AppendLine("        AND CONCAT(X2.Yr, X2.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            SQL.AppendLine("        GROUP BY X1.AssetCode  ");
            SQL.AppendLine("    )C ON A.DocNo = C.DocNo AND A.AssetCode = C.AssetCode ");
            SQL.AppendLine("    WHERE B.JournalDocNo IS NOT NULL ");
            SQL.AppendLine("    AND CONCAT(B.Yr, B.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            SQL.AppendLine("    GROUP BY A.DocNo, A.AssetCode  ");
            SQL.AppendLine(" ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("     ");
            SQL.AppendLine("    SELECT A.DocNo, A.AssetCode, SUM(B.DepreciationValue) DepreciationValue, MAX(B.AccDeprValue) AccDeprValueEndDt, MIN(B.NBV) NBVEndDt, A.EcoLife - MAX(B.MthPurchase) EcoLifeEnd  ");
            SQL.AppendLine("    FROM tbldepreciationassethdr A  ");
            SQL.AppendLine("    INNER JOIN tbldepreciationassetdtl B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("    INNER JOIN ( ");
            SQL.AppendLine("        SELECT MAX(X1.DocNo) DocNo, X1.AssetCode ");
            SQL.AppendLine("        FROM tbldepreciationassethdr X1 ");
            SQL.AppendLine("        INNER JOIN tbldepreciationassetdtl X2 ON X1.DocNo = X2.DocNo  ");
            SQL.AppendLine("        WHERE X2.JournalDocNo IS NOT NULL ");
            SQL.AppendLine("        AND X1.DocNo NOT IN ( ");
            SQL.AppendLine("            SELECT MAX(X1.DocNo) DocNo ");
            SQL.AppendLine("            FROM tbldepreciationassethdr X1 ");
            SQL.AppendLine("            INNER JOIN tbldepreciationassetdtl X2 ON X1.DocNo = X2.DocNo  ");
            SQL.AppendLine("            WHERE X2.JournalDocNo IS NOT NULL ");
            SQL.AppendLine("            AND CONCAT(X2.Yr, X2.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            SQL.AppendLine("            GROUP BY X1.AssetCode  ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        AND CONCAT(X2.Yr, X2.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            SQL.AppendLine("        GROUP BY X1.AssetCode  ");
            SQL.AppendLine("    )C ON A.DocNo = C.DocNo AND A.AssetCode = C.AssetCode ");
            SQL.AppendLine("    WHERE B.JournalDocNo IS NOT NULL ");
            SQL.AppendLine("    AND CONCAT(B.Yr, B.Mth) BETWEEN CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  AND CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            SQL.AppendLine("    GROUP BY A.DocNo, A.AssetCode  ");
            SQL.AppendLine(")X GROUP BY X.AssetCode ");

            SQL.AppendLine(")I2 ON A.AssetCode = I2.AssetCode ");
            SQL.AppendLine("LEFT JOIN TblCOA J ON A.AcNo = J.AcNo  ");
            SQL.AppendLine("LEFT JOIN TblCOA K ON A.AcNo2 = K.AcNo  ");
            SQL.AppendLine("LEFT JOIN TblOption L ON L.OptCat = 'AssetClassification' AND L.OptCode = A.Classification  ");
            SQL.AppendLine("LEFT JOIN TblOption M ON M.OptCat = 'AssetSubClassification' AND M.OptCode = A.SubClassification  ");
            SQL.AppendLine("LEFT JOIN TblOption N ON N.OptCat = 'AssetType' AND N.OptCode = A.Type  ");
            SQL.AppendLine("LEFT JOIN TblOption O ON O.OptCat = 'AssetSubType' AND O.OptCode = A.SubType  ");
            SQL.AppendLine("LEFT JOIN TblSite P ON A.SiteCode = P.SiteCode  ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("	SELECT A.AssetCode, A.AssetValue - B.ReconditionValue StartAssetValue, A.AssetValue - B.ReconditionValue + C.ReconditionValue AssetValueEndDt ");
            SQL.AppendLine("	FROM tblasset A ");
            SQL.AppendLine("	INNER JOIN( ");
            SQL.AppendLine("		SELECT AssetCode, SUM(ReconditionValue) ReconditionValue ");
            SQL.AppendLine("		FROM tblreconditionassethdr ");
            SQL.AppendLine("		WHERE CancelInd = 'N' AND Status = 'A'  ");
            SQL.AppendLine("		GROUP BY AssetCode ");
            SQL.AppendLine("	)B ON A.AssetCode = B.AssetCode ");
            SQL.AppendLine("	LEFT JOIN ");
            SQL.AppendLine("	( ");
            SQL.AppendLine("		SELECT A.AssetCode, A.ReconditionDt, IFNULL(B.ReconditionValue,0.00) ReconditionValue ");
            SQL.AppendLine("		FROM tblreconditionassethdr A ");
            SQL.AppendLine("		LEFT JOIN( ");
            SQL.AppendLine("			SELECT AssetCode, SUM(ReconditionValue) ReconditionValue ");
            SQL.AppendLine("			FROM tblreconditionassethdr ");
            SQL.AppendLine("			WHERE CancelInd = 'N' AND Status = 'A' AND ReconditionDt <= @EndDt ");
            SQL.AppendLine("			GROUP BY AssetCode ");
            SQL.AppendLine("		)B ON A.AssetCode = B.AssetCode ");
            SQL.AppendLine("		WHERE A.CancelInd = 'N' AND A.Status = 'A' ");
            SQL.AppendLine("	)C ON B.AssetCode = C.AssetCode ");
            SQL.AppendLine(")Q ON A.AssetCode = Q.AssetCode ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("		SELECT DISTINCT X.AssetCode, MIN(X.RemainingEcoStartDt) RemainingEcoStartDt ");
            SQL.AppendLine("		FROM (  ");
            SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt, ");
            SQL.AppendLine("			a.DepreciationDt, a.EcoLife, If(C.DocNo IS NULL, '0', '1') AS ROAInd ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            //SQL.AppendLine("			and a.cancelind = 'Y'  ");
            //SQL.AppendLine("			AND a.CancelByInd = '2'  ");
            //SQL.AppendLine("			-- AND b.journaldocno IS NOT NULL  ");
            //SQL.AppendLine("			UNION ALL  ");
            //SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @StartDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoStartDt, ");
            //SQL.AppendLine("			a.DepreciationDt, a.EcoLife, If(C.DocNo IS NULL, '0', '1') AS ROAInd ");
            //SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            //SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            //SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            //SQL.AppendLine("			WHERE CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  ");
            //SQL.AppendLine("			and a.cancelind = 'N'  ");
            //SQL.AppendLine("			AND a.CancelByInd = '1'  ");
            //SQL.AppendLine("			-- AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("		)X WHERE CONCAT(X.Yr, X.Mth) = CONCAT(@Yr, SUBSTRING(@StartDt, 5, 2))  GROUP BY X.AssetCode ");
            SQL.AppendLine(")R ON A.AssetCode = R.AssetCode ");
            SQL.AppendLine("LEFT JOIN( ");
            SQL.AppendLine("		SELECT DISTINCT X.AssetCode, MIN(X.RemainingEcoEndDt) RemainingEcoEndDt ");
            SQL.AppendLine("		FROM (  ");
            SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @EndDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoEndDt, ");
            SQL.AppendLine("			a.DepreciationDt, a.EcoLife, If(C.DocNo IS NULL, '0', '1') AS ROAInd ");
            SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            SQL.AppendLine("			WHERE CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            //SQL.AppendLine("			and a.cancelind = 'Y'  ");
            //SQL.AppendLine("			AND a.CancelByInd = '2'  ");
            //SQL.AppendLine("			-- AND b.journaldocno IS NOT NULL  ");
            //SQL.AppendLine("			UNION ALL  ");
            //SQL.AppendLine("			SELECT a.DocNo, b.Mth, b.Yr, a.AssetCode, b.DepreciationValue, c.AssetValue2, b.AccDeprValue, if(a.DepreciationDt = @EndDt, a.EcoLife, a.EcoLife - b.MthPurchase) AS RemainingEcoEndDt, ");
            //SQL.AppendLine("			a.DepreciationDt, a.EcoLife, If(C.DocNo IS NULL, '0', '1') AS ROAInd ");
            //SQL.AppendLine("			FROM tbldepreciationassethdr a  ");
            //SQL.AppendLine("			INNER JOIN tbldepreciationassetdtl b ON a.docno = b.docno  ");
            //SQL.AppendLine("			LEFT JOIN tblreconditionassethdr c ON b.reconditiondocno = c.docno  ");
            //SQL.AppendLine("			WHERE CONCAT(b.Yr, b.Mth) <= CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2))  ");
            //SQL.AppendLine("			and a.cancelind = 'N'  ");
            //SQL.AppendLine("			AND a.CancelByInd = '1'  ");
            //SQL.AppendLine("			-- AND b.journaldocno IS NOT NULL  ");
            SQL.AppendLine("		)X WHERE CONCAT(X.Yr, X.Mth) = CONCAT(@Yr, SUBSTRING(@EndDt, 5, 2)) GROUP BY X.AssetCode ");
            SQL.AppendLine(")S ON A.AssetCode = S.AssetCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.AssetCode, Sum(X.DepreciationValue) As DepreciationValueEndDt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    	Select A.AssetCode, B.* ");
            SQL.AppendLine("    	From TblDepreciationAssetHdr A ");
            SQL.AppendLine("    	Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    		And A.CancelInd ='Y' ");
            SQL.AppendLine("    		And A.CancelByInd = '2' ");
            SQL.AppendLine("    		And B.JournalDocNo Is Not Null ");
            SQL.AppendLine("    	Union All ");
            SQL.AppendLine("    	Select A.AssetCode, B.* ");
            SQL.AppendLine("    	From TblDepreciationAssetHdr A ");
            SQL.AppendLine("    	Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    		And A.CancelInd ='N' ");
            SQL.AppendLine("    		And B.JournalDocNo Is Not Null ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Where Concat(X.Yr, X.Mth) <= Concat(@Yr, Substring(@EndDt, 5, 2)) ");
            SQL.AppendLine("    Group By X.AssetCode Order By X.AssetCode, X.Yr, X.Mth ");
            SQL.AppendLine(")T On A.AssetCode = T.AssetCode ");
            SQL.AppendLine("WHERE A.ActiveInd = 'Y'  ");

            mSQL = SQL.ToString();
        }
        */

        private void SetSQL(string mQueryProfitCenter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, A.AssetName, D.AssetCategoryName, A.AssetDt As PurchaseDt, ");
            SQL.AppendLine("IfNull(A.AssetValue, 0.00) - IfNull(F.TotReconditionValue, 0.00) As AssetValue, F.ReconditionDt, F.ReconditionReason, ");
            SQL.AppendLine("IfNull(F.ReconditionValue, 0.00) As ReconditionValue, IfNull(A.AssetValue, 0.00) - IfNull(F.TotReconditionValue, 0.00) + IfNull(F.ReconditionValue, 0.00) As TotalAssetValue, ");
            SQL.AppendLine("G.ResidualValue, F.AssetValue2 AccDeprRoADt, G.DepreciationDt, IfNull(G.EcoLifeYr, 0.00) EcoLifeYr, IfNull(G.EcoLife, 0.00) EcoLifeMth, IfNull(F.EcoLife3, 0.00) EcoLifeRoA, ");
            SQL.AppendLine("IfNull(F.EcoLifeRoAYr, 0.00) EcoLifeRoAYr, E.OptDesc As DepMethod, G.PercentageAnnualDepreciation, 0.00 As DepreciationValueMth, ");
            SQL.AppendLine("IfNull(A.AssetValue, 0.00) - IfNull(F.TotReconditionValue, 0.00) + IfNull(F.ReconditionValEndDt, 0.00) As AssetValueEndDt, ");
            SQL.AppendLine("A.AcNo, H.AcDesc, A.AcNo2 AcNoDepreciation, I.AcDesc AcDescDepreciation, J.OptDesc Classification, K.OptDesc SubClassification, ");
            SQL.AppendLine("L.OptDesc AssetType, M.OptDesc SubType, N.SiteName, A.ShortCode, A.Parent, A.DisplayName, A.ActiveInd, A.AssetType WorkCenter, ");
            SQL.AppendLine("B.CCCode, B.CCName, A.LeasingInd, A.RentedInd, A.SoldInd, A.FiskalInd, C.ProfitCenterCode, C.ProfitCenterName, ");
            SQL.AppendLine("IfNull( ");
            SQL.AppendLine("	G.DepreciationDt, ");
            SQL.AppendLine("	If(A.AssetSource = '1', A.AssetDt, A.OpeningBalanceDt) ");
            SQL.AppendLine(") As DeprDt ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode ");
            if (mIsRptAssetBookValueUseMultiProfitCenterFilter) SQL.AppendLine(mQueryProfitCenter);
            SQL.AppendLine("Inner Join TblAssetCategory D On A.AssetCategoryCode = D.AssetCategoryCode ");
            SQL.AppendLine("Inner Join TblOption E ON E.OptCat = 'DepreciationMethod' AND A.DepreciationCode = E.OptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select T1.AssetCode, T1.TotReconditionValue, T2.ReconditionValue, T3.ReconditionDt, T3.ReconditionReason, T3.AssetValue2, ");
            SQL.AppendLine("	T3.EcoLife3, T3.EcoLifeRoAYr, T4.ReconditionValue As ReconditionValEndDt ");
            SQL.AppendLine("	From( ");
            SQL.AppendLine("		Select AssetCode, Sum(ReconditionValue) As TotReconditionValue ");
            SQL.AppendLine("		From TblReconditionAssetHdr ");
            SQL.AppendLine("		Where CancelInd = 'N' ");
            SQL.AppendLine("		And Status = 'A' ");
            SQL.AppendLine("		Group By AssetCode ");
            SQL.AppendLine("	)T1 ");
            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select AssetCode, Sum(ReconditionValue) As ReconditionValue ");
            SQL.AppendLine("		From TblReconditionAssetHdr ");
            SQL.AppendLine("		Where CancelInd = 'N' ");
            SQL.AppendLine("		And Status = 'A' ");
            SQL.AppendLine("		And ReconditionDt Between @StartDt And @EndDt ");
            SQL.AppendLine("		Group By AssetCode ");
            SQL.AppendLine("	)T2 On T1.AssetCode = T2.AssetCode ");
            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select X1.AssetCode, X1.ReconditionDt, X1.ReconditionReason, X1.AssetValue2, ");
            SQL.AppendLine("		X1.Ecolife3, IfNull(X1.EcoLife3, 0.00)/12 EcoLifeRoAYr ");
            SQL.AppendLine("		From TblReconditionAssetHdr X1 ");
            SQL.AppendLine("		Inner Join ( ");
            SQL.AppendLine("			Select AssetCode, Max(CreateDt) As CreateDt ");
            SQL.AppendLine("			From TblReconditionAssetHdr ");
            SQL.AppendLine("			Where CancelInd = 'N' ");
            SQL.AppendLine("			And Status = 'A' ");
            SQL.AppendLine("			And ReconditionDt Between @StartDt And @EndDt ");
            SQL.AppendLine("			Group By AssetCode ");
            SQL.AppendLine("		)X2 On X1.AssetCode = X2.AssetCode And X1.CreateDt = X2.CreateDt ");
            SQL.AppendLine("		Where X1.CancelInd = 'N' ");
            SQL.AppendLine("		And X1.Status = 'A' ");
            SQL.AppendLine("		And X1.ReconditionDt Between @StartDt And @EndDt ");
            SQL.AppendLine("	)T3 On T1.AssetCode = T3.AssetCode ");
            SQL.AppendLine("	Left Join ( ");
            SQL.AppendLine("		Select AssetCode, Sum(ReconditionValue) As ReconditionValue ");
            SQL.AppendLine("		From TblReconditionAssetHdr ");
            SQL.AppendLine("		Where CancelInd = 'N' ");
            SQL.AppendLine("		And Status = 'A' ");
            SQL.AppendLine("		And ReconditionDt <= @EndDt ");
            SQL.AppendLine("		Group By AssetCode ");
            SQL.AppendLine("	)T4 On T1.AssetCode = T4.AssetCode ");
            SQL.AppendLine(")F On A.AssetCode = F.AssetCode ");
            SQL.AppendLine("Left Join TblDepreciationAssetHdr G On A.AssetCode = G.AssetCode ");
            SQL.AppendLine("	And G.CancelInd = 'N'  ");
            SQL.AppendLine("	And G.DocNo = (Select DocNo From TblDepreciationAssetHdr Where AssetCode = G.AssetCode And CancelInd = 'N' Order By CreateDt Desc Limit 1) ");
            SQL.AppendLine("Left Join TblCOA H On A.AcNo = H.AcNo ");
            SQL.AppendLine("Left Join TblCOA I On A.AcNo2 = I.AcNo ");
            SQL.AppendLine("Left JOIN TblOption J On J.OptCat = 'AssetClassification' And J.OptCode = A.Classification  ");
            SQL.AppendLine("Left Join TblOption K On K.OptCat = 'AssetSubClassification' And K.OptCode = A.SubClassification  ");
            SQL.AppendLine("Left Join TblOption L On L.OptCat = 'AssetType' And L.OptCode = A.Type ");
            SQL.AppendLine("Left Join TblOption M On M.OptCat = 'AssetSubType' And M.OptCode = A.SubType ");
            SQL.AppendLine("Left Join TblSite N On A.SiteCode = N.SiteCode ");
            SQL.AppendLine("Where If(A.AssetSource = '1', A.AssetDt, IfNull(G.DepreciationDt, A.OpeningBalanceDt)) <= @EndDt ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 55;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Asset Code",
                    "Asset Name",
                    "Asset's Category",
                    "Date of Purchase",
                    "Asset's Value",

                    //6-10
                    "Date"+Environment.NewLine+"of Recondition",
                    "Recondition Reason",
                    "Recondition"+Environment.NewLine+"Amount",
                    "Total"+Environment.NewLine+"Asset's Value",
                    "Residual"+Environment.NewLine+"Value",

                    //11-15
                    "Asset's Acc.Depr."+Environment.NewLine+"on Recondition Date",
                    "Asset Depreciable"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Date",
                    "Economic Life"+Environment.NewLine+"(Year)",
                    "Economic Life"+Environment.NewLine+"(Month)",

                    //16-20
                    "Economic Life (Year)"+Environment.NewLine+"on Recondition Date",
                    "Economic Life (Month)"+Environment.NewLine+"on Recondition Date",
                    "Remaining Economic Life"+Environment.NewLine+"on Start Date",
                    "Remaining Economic Life"+Environment.NewLine+"on End Date",
                    "Depreciation Method",

                    //21-25
                    "Annual Depreciation"+Environment.NewLine+"Rate",
                    "Depreciation Value"+Environment.NewLine+"(Month)",
                    "Acc.Depr. "+Environment.NewLine+"on Start Date",
                    "NBV"+Environment.NewLine+"on Start Date",
                    "Asset Sold/Retirement", 

                    //26-30
                    "Depreciation Value"+Environment.NewLine+"to End Date",
                    "Acc.Depr. on End Date",
                    "NBV on End Date",
                    "Asset Value"+Environment.NewLine+"on End Date",
                    "COA Account#",

                    //31-35
                    "COA Description",
                    "COA Account#"+Environment.NewLine+"(Depreciation)",
                    "COA Account Description"+Environment.NewLine+"(Depreciation)",
                    "Classification",
                    "Sub Classification", 

                    //36-40
                    "Type",
                    "Sub Type",
                    "Site",
                    "Short Code",
                    "Status", 

                    //41-45
                    "Reference",
                    "Parent",
                    "Display Name",
                    "Active",
                    "Work Center",

                    //46-50
                    "Cost Center's"+Environment.NewLine+"Code",
                    "Cost Center's"+Environment.NewLine+"Name",
                    "Insurance"+Environment.NewLine+"Asset",
                    "Rented"+Environment.NewLine+"Asset",
                    "Sold"+Environment.NewLine+"Asset",

                    //51-54
                    "Fiskal",
                    "Profit Center's"+Environment.NewLine+"Code",
                    "Profit Center's"+Environment.NewLine+"Name",
                    "DeprDt Tmp"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    130, 250, 200, 120, 150, 

                    //6-10
                    120, 200, 150, 150, 150,

                    //11-15
                    150, 150, 100, 100, 100,

                    //16-20
                    150, 150, 150, 150, 150,

                    //21-25
                    150, 150, 150, 150, 150,

                    //26-30
                    150, 150, 150, 150, 150, 

                    //31-35
                    200, 150, 200, 150, 150, 

                    //36-40
                    150, 150, 150, 150, 150,

                    //41-45
                    0, 150, 150, 100, 100,  

                    //46-50
                    100, 250, 100, 100, 100, 

                    //51-54
                    100, 100, 200, 120

                }
            );
            Sm.GrdColButton(Grd1, new int[] { 41 });
            Sm.GrdColCheck(Grd1, new int[] { 44, 45, 48, 49, 50, 51 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 6, 13, 54 });
            Sm.GrdColInvisible(Grd1, new int[] { 11, 22, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53 }, !ChkHideInfoInGrd.Checked);
        }

        /* Old Code ShowData() by IBL 14/09/2022
        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Yr") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date")
                ) return;

            var SQL = new StringBuilder();
            var l1 = new List<DepreciationAssetDtl>();
            mQueryProfitCenter = string.Empty;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                if (mIsRptAssetBookValueUseMultiProfitCenterFilter) SetProfitCenter();

                var cm = new MySqlCommand();

                if (mIsRptAssetBookValueUseMultiProfitCenterFilter)
                {
                    mQueryProfitCenter = "    And A.CCCode Is Not Null ";
                    mQueryProfitCenter += "    And A.CCCode In ( ";
                    mQueryProfitCenter += "        Select Distinct CCCode ";
                    mQueryProfitCenter += "        From TblCostCenter ";
                    mQueryProfitCenter += "        Where ActInd='Y' ";
                    mQueryProfitCenter += "        And ProfitCenterCode Is Not Null ";

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            mQueryProfitCenter += "    And 1=0 ";
                        else
                            mQueryProfitCenter += "    And (" + Filter2 + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode2.Checked)
                        {
                            mQueryProfitCenter += "        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            if (ChkProfitCenterCode2.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter += "        And ProfitCenterCode In ( ";
                            mQueryProfitCenter += "            Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                            mQueryProfitCenter += "            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter += "    ) ";
                        }
                    }
                    mQueryProfitCenter += "    ) ";

                    if (!ChkProfitCenterCode2.Checked)
                    {
                        mQueryProfitCenter = "    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ";
                        mQueryProfitCenter += "        Select Distinct CCCode From TblGroupCostCenter ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "        ))) ";
                    }
                }

                var DaysInMonthStart = DateTime.DaysInMonth(DteStartDt.DateTime.Year, DteStartDt.DateTime.Month);
                var LastDayStart = Sm.FormatDate(new DateTime(DteStartDt.DateTime.Year, DteStartDt.DateTime.Month, DaysInMonthStart));

                var DaysInMonthEnd = DateTime.DaysInMonth(DteEndDt.DateTime.Year, DteEndDt.DateTime.Month);
                var LastDayEnd = Sm.FormatDate(new DateTime(DteEndDt.DateTime.Year, DteEndDt.DateTime.Month, DaysInMonthEnd));

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                //Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt) != LastDayStart ? Sm.FormatDate(DteStartDt.DateTime.AddMonths(-1)) : Sm.GetDte(DteStartDt));
                //Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt) != LastDayEnd ? Sm.FormatDate(DteEndDt.DateTime.AddMonths(-1)) : Sm.GetDte(DteEndDt));
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCtCode), "A.AssetCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetName.Text, new string[] { "A.AssetCode", "A.AssetName", "A.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProfitCenterCode), "B.ProfitCenterCode", true);

                SetSQL(mQueryProfitCenter);

                SQL.AppendLine("Select A.AssetCode, A.AssetName, B.AssetCategoryName, If(A.AssetSource = '1', A.AssetDt, A.OpeningBalanceDt) As PurchaseDt, ");
                SQL.AppendLine("IfNull(A.AssetValue, 0.00) - IfNull(B.TotReconditionValue, 0.00) As AssetValue, B.ReconditionDt, B.ReconditionReason, ");
                SQL.AppendLine("IfNull(B.ReconditionValue, 0.00) As ReconditionValue, IfNull(A.AssetValue, 0.00) - IfNull(B.TotReconditionValue, 0.00) + IfNull(B.ReconditionValue, 0.00) As TotalAssetValue, ");
                SQL.AppendLine("C.ResidualValue, B.AssetValue2 AccDeprRoADt, -- Asset Depreciable Value = Total Asset Value - AccDeprRoADt - Residual Value ");
                SQL.AppendLine("C.DepreciationDt, IfNull(C.EcoLifeYr, 0.00) EcoLifeYr, IfNull(C.EcoLife, 0.00) EcoLifeMth, IfNull(B.EcoLife3, 0.00) EcoLifeRoA, ");
                SQL.AppendLine("IfNull(B.EcoLifeRoAYr, 0.00) EcoLifeRoAYr ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.AssetCode;",
                        new string[]
                        {
                            //0
                            "AssetCode",

                            //1-5
                            "AssetName", "AssetCategoryName", "PurchaseDt", "AssetValue", "ReconditionDt", 

                            //6-10
                            "ReconditionReason", "ReconditionValue", "TotalAssetValue", "ResidualValue", "AccDeprRoADt", 

                            //11-15
                            "DepreciationDt", "EcoLifeYr", "EcoLifeMth", "EcoLifeRoAYr", "EcoLifeRoA", "RemainingEcoStartDt", 
                            
                            //16-20
                            "RemainingEcoEndDt", "DepreciationMethod", "AnnualDepreciationRate", "DepreciationValue", "AccDeprValueStartDt", 

                            //21-25
                            "NBVStartDt", "AssetSold", "AccDeprValueEndDt", "NBVEndDt", "AssetValueEndDt", 

                            //26-30
                            "AcNo", "AcDesc", "AcNoDepreciation", "AcDescDepreciation", "Classification", 

                            //31-35
                            "SubClassification", "AssetType", "SubType", "SiteName", "ShortCode",

                            //36-40
                            "Parent", "DisplayName", "ActiveInd", "WorkCenter", "CCCode", 

                            //41-45
                            "CCName", "LeasingInd", "RentedInd", "SoldInd", "FiskalInd", 
                            
                            //46-50
                            "ProfitCenterCode", "ProfitCenterName", "TotalAssetValue", "AssetDepreciableValue", "DepreciationValueEnd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 5) + Sm.GetGrdDec(Grd1, Row, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10) - 0; //Sm.GetGrdDec(Grd1, Row, 11) --> karena Asset's Acc.Depr. on Recondition Date di hide dan diberi nilai 0
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Grd1.Cells[Row, 24].Value = Sm.GetGrdDec(Grd1, Row, 5) - Sm.GetGrdDec(Grd1, Row, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 50);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 33);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 37);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 44, 38);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 45, 39);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 41);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 48, 42);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 49, 43);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 50, 44);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 51, 45);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 52, 46);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 53, 47);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 5, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
        */

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsFilterByDateInvalid()
                ) return;

            var SQL = new StringBuilder();
            var l1 = new List<DepreciationAssetDtl>();
            mQueryProfitCenter = string.Empty;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                if (mIsRptAssetBookValueUseMultiProfitCenterFilter) SetProfitCenter();

                var cm = new MySqlCommand();

                if (mIsRptAssetBookValueUseMultiProfitCenterFilter)
                {
                    mQueryProfitCenter = "    And A.CCCode Is Not Null ";
                    mQueryProfitCenter += "    And A.CCCode In ( ";
                    mQueryProfitCenter += "        Select Distinct CCCode ";
                    mQueryProfitCenter += "        From TblCostCenter ";
                    mQueryProfitCenter += "        Where ActInd='Y' ";
                    mQueryProfitCenter += "        And ProfitCenterCode Is Not Null ";

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            mQueryProfitCenter += "    And 1=0 ";
                        else
                            mQueryProfitCenter += "    And (" + Filter2 + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode2.Checked)
                        {
                            mQueryProfitCenter += "        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            if (ChkProfitCenterCode2.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter += "        And ProfitCenterCode In ( ";
                            mQueryProfitCenter += "            Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                            mQueryProfitCenter += "            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter += "    ) ";
                        }
                    }
                    mQueryProfitCenter += "    ) ";

                    if (!ChkProfitCenterCode2.Checked)
                    {
                        mQueryProfitCenter = "    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ";
                        mQueryProfitCenter += "        Select Distinct CCCode From TblGroupCostCenter ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "        ))) ";
                    }
                }

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCtCode), "A.AssetCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetName.Text, new string[] { "A.AssetCode", "A.AssetName", "A.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProfitCenterCode), "C.ProfitCenterCode", true);

                SetSQL(mQueryProfitCenter);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.AssetCode;",
                        new string[]
                        {
                            //0
                            "AssetCode",

                            //1-5
                            "AssetName", "AssetCategoryName", "PurchaseDt", "AssetValue", "ReconditionDt", 

                            //6-10
                            "ReconditionReason", "ReconditionValue", "ResidualValue", "AccDeprRoADt", "DepreciationDt",

                            //11-15
                            "EcoLifeYr", "EcoLifeMth", "EcoLifeRoAYr", "EcoLifeRoA", "DepMethod",

                            //16-20
                            "PercentageAnnualDepreciation", "AssetValueEndDt", "AcNo", "AcDesc", "AcNoDepreciation",

                            //21-25
                            "AcDescDepreciation", "Classification", "SubClassification", "AssetType", "SubType",

                            //26-30
                            "SiteName", "ShortCode", "Parent", "DisplayName", "ActiveInd",

                            //31-35
                            "WorkCenter", "CCCode", "CCName", "LeasingInd", "RentedInd",

                            //36-40
                            "SoldInd", "FiskalInd", "ProfitCenterCode", "ProfitCenterName", "DeprDt",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 5) + Sm.GetGrdDec(Grd1, Row, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Grd1.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10) - 0; //Sm.GetGrdDec(Grd1, Row, 11) --> karena Asset's Acc.Depr. on Recondition Date di hide dan diberi nilai 0
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 29);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 44, 30);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 45, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 33);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 48, 34);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 49, 35);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 50, 36);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 51, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 52, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 53, 39);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 54, 40);
                            Grd1.Cells[Row, 18].Value = 0m;
                            Grd1.Cells[Row, 19].Value = 0m;
                            Grd1.Cells[Row, 22].Value = 0m;
                            Grd1.Cells[Row, 23].Value = 0m;
                            Grd1.Cells[Row, 24].Value = 0m;
                            Grd1.Cells[Row, 25].Value = 0m;
                            Grd1.Cells[Row, 26].Value = 0m;
                            Grd1.Cells[Row, 27].Value = 0m;
                            Grd1.Cells[Row, 28].Value = 0m;
                        }, true, false, false, false
                    );
                ProcessData(ref l1);
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessData(ref List<DepreciationAssetDtl> l1)
        {
            var DaysInMonthStart = DateTime.DaysInMonth(DteStartDt.DateTime.Year, DteStartDt.DateTime.Month);
            var LastDayStart = Sm.FormatDate(new DateTime(DteStartDt.DateTime.Year, DteStartDt.DateTime.Month, DaysInMonthStart));

            var DaysInMonthEnd = DateTime.DaysInMonth(DteEndDt.DateTime.Year, DteEndDt.DateTime.Month);
            var LastDayEnd = Sm.FormatDate(new DateTime(DteEndDt.DateTime.Year, DteEndDt.DateTime.Month, DaysInMonthEnd));

            string StartDt = Sm.GetDte(DteStartDt) != LastDayStart ? Sm.FormatDate(DteStartDt.DateTime.AddMonths(-1)) : Sm.GetDte(DteStartDt);
            string EndDt = Sm.GetDte(DteEndDt) != LastDayEnd ? Sm.FormatDate(DteEndDt.DateTime.AddMonths(-1)) : Sm.GetDte(DteEndDt);

            PrepData(ref l1, StartDt);
            ProcessData1(ref l1, StartDt, EndDt, LastDayStart);
            ProcessData2(ref l1, EndDt);
        }

        private void PrepData(ref List<DepreciationAssetDtl> l1, string StartDt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AssetCode", GetAssetCode());
            Sm.CmParamDt(ref cm, "@StartDt", StartDt);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            SQL.AppendLine("Select Cast(replace(T2.MthPurchase, ',', '') As decimal(10, 2)) As MthPurchase, T2.AssetCode, T2.Mth, T2.Yr, ");
            SQL.AppendLine("T2.DepreciationValue, If(T.AssetSource = '2', T.AccDepr, 0.00) As AccDeprVal0, T2.AccDeprValue, ");
            SQL.AppendLine("IfNull(T2.EcoLife, 0.00) As EcoLife0, IfNull(T2.EcoLife, 0.00) - IfNull(T2.MthPurchase, 0.00) As EcoLife ");
            SQL.AppendLine("From TblAsset T ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("	Select B.MthPurchase, A.AssetCode, A.CancelInd, A.CancelByInd, B.Mth, B.Yr, ");
            SQL.AppendLine("	B.DepreciationValue, B.AccDeprValue, A.AssetValue, A.EcoLife ");
            SQL.AppendLine("	From TblDepreciationAssetHdr A ");
            SQL.AppendLine("	Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	Where A.CancelInd = 'Y' ");
            SQL.AppendLine("	And A.CancelByInd = '2' ");
            SQL.AppendLine("	And B.JournalDocNo Is Not Null ");
            SQL.AppendLine("	And Concat(B.Yr, B.Mth) Between Substring(@StartDt, 1, 6) And Substring(@EndDt, 1, 6) ");
            SQL.AppendLine("	And Find_In_Set(A.AssetCode, @AssetCode) ");
            SQL.AppendLine("	Union All ");
            SQL.AppendLine("	Select B.MthPurchase, A.AssetCode, A.CancelInd, A.CancelByInd, B.Mth, B.Yr, ");
            SQL.AppendLine("	B.DepreciationValue, B.AccDeprValue, A.AssetValue, A.EcoLife ");
            SQL.AppendLine("	From TblDepreciationAssetHdr A ");
            SQL.AppendLine("	Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	Where A.CancelInd = 'N' ");
            SQL.AppendLine("	And B.JournalDocNo Is Not Null ");
            SQL.AppendLine("	And Concat(B.Yr, B.Mth) Between Substring(@StartDt, 1, 6) And Substring(@EndDt, 1, 6) ");
            SQL.AppendLine("	And Find_In_Set(A.AssetCode, @AssetCode) ");
            SQL.AppendLine(")T2 On T.AssetCode = T2.AssetCode ");
            SQL.AppendLine("Where Find_In_Set(T.AssetCode, @AssetCode) ");
            SQL.AppendLine("And T.ActiveInd = 'Y' ");
            SQL.AppendLine("Order By T2.AssetCode, T2.Yr, T2.Mth; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr,
                    new string[]
                    {
                        //0
                        "MthPurchase",

                        //1-5
                        "AssetCode", "Mth", "Yr", "DepreciationValue", "AccDeprVal0",
                        
                        //6-8
                        "AccDeprValue", "EcoLife0", "EcoLife"
                    }
                );

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new DepreciationAssetDtl()
                        {
                            MthPurchase = Sm.DrDec(dr, c[0]),
                            AssetCode = Sm.DrStr(dr, c[1]),
                            Mth = Sm.DrStr(dr, c[2]),
                            Yr = Sm.DrStr(dr, c[3]),
                            DepreciationValue = Sm.DrDec(dr, c[4]),
                            AccDeprVal0 = Sm.DrDec(dr, c[5]),
                            AccDeprValue = Sm.DrDec(dr, c[6]),
                            EcoLife0 = Sm.DrDec(dr, c[7]),
                            EcoLife = Sm.DrDec(dr, c[8]),
                        });
                    }
                }
            }
        }

        private void ProcessData1(ref List<DepreciationAssetDtl> l1, string StartDt, string EndDt, string LastDayStart)
        {
            decimal mStartDt = Convert.ToDecimal(Sm.Left(StartDt, 6)),
                    mEndDt = Convert.ToDecimal(Sm.Left(EndDt, 6)),
                    mDteStartDt = Convert.ToDecimal(Sm.GetDte(DteStartDt).Substring(6, 2)),
                    mLastDayStart = Convert.ToDecimal(LastDayStart.Substring(6, 2));

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                for (int j = 0; j < l1.Count; j++)
                {
                    if (l1[j].AssetCode == Sm.GetGrdStr(Grd1, i, 1))
                    {
                        string test1 = Sm.GetGrdDate(Grd1, i, 13);
                        string test2 = Sm.GetDte(DteStartDt);

                        if (Sm.Left(Sm.GetDte(DteStartDt), 6) == Sm.Left(Sm.GetGrdDate(Grd1, i, 54), 6) && mDteStartDt < mLastDayStart)
                        {
                            Grd1.Cells[i, 18].Value = l1[j].EcoLife0;
                            Grd1.Cells[i, 23].Value = l1[j].AccDeprVal0;
                            break;
                        }

                        if (mStartDt < Convert.ToDecimal(Sm.Left(Sm.GetGrdDate(Grd1, i, 54), 6)))
                        {
                            Grd1.Cells[i, 18].Value = l1[j].EcoLife0;
                            Grd1.Cells[i, 23].Value = l1[j].AccDeprValue - l1[j].DepreciationValue;
                            break;
                        }

                        if (String.Concat(l1[j].Yr, l1[j].Mth) == Sm.Left(StartDt, 6))
                        {
                            Grd1.Cells[i, 18].Value = l1[j].EcoLife;
                            Grd1.Cells[i, 23].Value = l1[j].AccDeprValue;
                            break;
                        }
                    }
                }

                for (int j = 0; j < l1.Count; j++)
                {
                    if (l1[j].AssetCode == Sm.GetGrdStr(Grd1, i, 1))
                    {
                        if (mEndDt < Convert.ToDecimal(Sm.Left(Sm.GetGrdDate(Grd1, i, 54), 6)))
                        {
                            Grd1.Cells[i, 19].Value = l1[j].EcoLife0;
                            break;
                        }

                        if (String.Concat(l1[j].Yr, l1[j].Mth) == Sm.Left(EndDt, 6))
                        {
                            Grd1.Cells[i, 19].Value = l1[j].EcoLife;
                            break;
                        }
                    }
                }

                Grd1.Cells[i, 24].Value = Sm.GetGrdDec(Grd1, i, 5) - Sm.GetGrdDec(Grd1, i, 23);
            }
        }

        private void ProcessData2(ref List<DepreciationAssetDtl> l1, string EndDt)
        {
            decimal DeprValToEndDt = 0m;
            string StartDt = Sm.GetDte(DteStartDt);

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                foreach (var x in l1.Where(w => Decimal.Parse(w.Yr + w.Mth) >= Decimal.Parse(Sm.Left(StartDt, 6)))
                        .Where(w => Decimal.Parse(w.Yr + w.Mth) <= Decimal.Parse(Sm.Left(EndDt, 6))))
                {
                    if (x.AssetCode == Sm.GetGrdStr(Grd1, i, 1))
                        DeprValToEndDt += x.DepreciationValue;
                }

                Grd1.Cells[i, 26].Value = DeprValToEndDt;
                Grd1.Cells[i, 27].Value = Sm.GetGrdDec(Grd1, i, 23) + DeprValToEndDt;
                Grd1.Cells[i, 28].Value = Sm.GetGrdDec(Grd1, i, 9) - Sm.GetGrdDec(Grd1, i, 27);
                DeprValToEndDt = 0m;
            }
        }

        #region Additional Method

        private string GetAssetCode()
        {
            string AssetCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (AssetCode.Length > 0) AssetCode += ",";
                AssetCode += Sm.GetGrdStr(Grd1, i, 1);
            }

            return AssetCode;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode2.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode2.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (mIsRptAssetBookValueUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    Where ProfitCenterCode In ( ");
                SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode2);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode2).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode2.Focus();
                return true;
            }

            return false;
        }

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteStartDt);
            var DocDt2 = Sm.GetDte(DteEndDt);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAssetName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                if (Sm.GetDte(DteStartDt).Length > 0) Sm.SetDte(DteStartDt, Sm.GetLue(LueYr) + Sm.GetDte(DteStartDt).Substring(4, 4));
                if (Sm.GetDte(DteEndDt).Length > 0) Sm.SetDte(DteEndDt, Sm.GetLue(LueYr) + Sm.GetDte(DteEndDt).Substring(4, 4));
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.GetDte(DteStartDt).Length > 0 && 
                Sm.GetDte(DteStartDt).Substring(0, 4) != Sm.GetLue(LueYr))
            {
                Sm.StdMsg(mMsgType.Warning, "start date must be in " + Sm.GetLue(LueYr));
                Sm.SetDte(DteStartDt, Sm.GetLue(LueYr) + Sm.GetDte(DteStartDt).Substring(4, 4));
            }
            if (Sm.GetDte(DteEndDt).Length == 0) DteEndDt.EditValue = DteEndDt.EditValue;
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.GetDte(DteEndDt).Length > 0 &&
                Sm.GetDte(DteEndDt).Substring(0, 4) != Sm.GetLue(LueYr))
            {
                Sm.StdMsg(mMsgType.Warning, "end date must be in " + Sm.GetLue(LueYr));
                Sm.SetDte(DteEndDt, Sm.GetLue(LueYr) + Sm.GetDte(DteEndDt).Substring(4, 4));
            }
            if (Sm.GetDte(DteStartDt).Length == 0) DteStartDt.EditValue = DteEndDt.EditValue;
        }

        private void LueAssetCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's Category");
        }

        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void CcbProfitCenterCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Profit Center");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        class DepreciationAssetDtl
        {
            public decimal Row { get; set; }
            public decimal MthPurchase { get; set; }
            public string AssetCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal EcoLife0 { get; set; }
            public decimal EcoLife { get; set; }
            public decimal DepreciationValue { get; set; }
            public decimal AccDeprVal0 { get; set; }
            public decimal AccDeprValue { get; set; }
        }

        #endregion
    }
}
