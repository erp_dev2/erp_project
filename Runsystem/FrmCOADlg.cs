﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCOADlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCOA mFrmParent;

        #endregion

        #region Constructor

        public FrmCOADlg(FrmCOA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

    }
}
