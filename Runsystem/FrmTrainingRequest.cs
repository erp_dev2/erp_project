﻿#region Update
/*
    03/01/2018 [HAR] hapus informasi competence
    29/01/2018 [HAR] form main tambh informasi internal external, trainer yang dipilih hnya yg sesuai training type nya (internal external) dan kode training
    31/03/2018 [TKG] tambah status
    16/08/2018 [HAR] tambah inputan class, default time ambil dari schdule bisa diedit
    28/08/2018 [HAR] Tambah Inputan Site
    19/09/2018 [TKG] difilter otorisasi berdasarkan site di grup
    08/04/2019 [WED] Approval lihat site dari master Training. Parameter baru IsTrainingRequestApprovalNeedDept
    28/07/2020 [ICA/SIER] Training req dapat menarik keseluruhan master training walaupun sitenya spesifik dengan parameter IsTrainingUseAllSite
    16/02/2022 [RIS/GSS] Menambah field PIC dengan menggunakan param IsTrainingRequestUsePIC
    06/04/2022 [RIS/SIER] BUG : Di menu Training Request di kolom Class saat klik muncul warning tsb
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmTrainingRequestFind FrmFind;
        private int mCol = -1;
        iGCell fCell;
        bool fAccept;
        internal bool
           mIsFilterBySiteHR = false,
           mIsFilterBySite = false,
           mIsTrainingUseAllSite = false
           ;
        private bool 
            mIsTrainingRequestApprovalNeedDept = false,
            mIsTrainingRequestUsePIC = false;
        private string mTrainingSiteCode = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Training Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                LueTrainingSchedule.Visible = false;
                Sl.SetLueUserCode(ref LuePIC);
                if (!mIsTrainingRequestUsePIC)
                {
                    label4.Visible = false;
                    LuePIC.Visible = false;
                    TxtTrainingType.Location = new Point(122,52);
                    label3.Location = new Point(10, 54);
                    MeeRemark.Location = new Point(122, 73);
                    label10.Location = new Point(68, 76);
                }

                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Trainer Code",
                        "Trainer",
                        "Type", 
                        "Employee / Vendor",

                        //6-7
                        "Hour",
                        "Remark",
                        
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        20, 80, 180, 100, 200,
                        
                        //6-10
                        80, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee Code",
                        "Employee",
                        "DeptCode",
                        "Department",
                        //6-10 
                        "Position",
                        "SiteCode",
                        "Site",
                        "Gender",
                        "TrainingDno",
                        //11-14
                        "Class",
                        "Time",
                        "Remark",
                        "Approval"+Environment.NewLine+"Status"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        20, 80, 180, 80, 180,
                        //6-10
                        180, 80, 120, 100, 20, 
                        //11-14
                        100, 80, 200, 100
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 4, 7, 10 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 0, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, TxtTrainingCode, TxtTrainingName, 
                        TxtTrainingType, MeeCancelReason, ChkCancelInd, LueSiteCode, MeeRemark, LuePIC }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
                    TxtDocNo.Focus();
                    BtnTrainingCode.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueSiteCode,  MeeRemark, LuePIC }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 7 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 11, 12, 13 });
                    DteDocDt.Focus();
                    BtnTrainingCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason
                    }, false);
                    DteDocDt.Focus();
                    ChkCancelInd.Enabled = true;
                    break;
            }
        }

        private void ClearData()
        {
            mTrainingSiteCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtTrainingCode, 
                TxtTrainingName, TxtTrainingType, LueSiteCode, MeeRemark, LuePIC
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearData2()
        {
            mTrainingSiteCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtTrainingCode, TxtTrainingName, TxtTrainingType, LuePIC });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTrainingCode, "Training", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTrainingRequestDlg2(this, TxtTrainingCode.Text, Sm.GetValue("Select optCode From tblOption Where OptDesc = '" + TxtTrainingType.Text + "'")));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTrainingCode, "Training", false))
                Sm.FormShowDialog(new FrmTrainingRequestDlg2(this, TxtTrainingCode.Text, Sm.GetValue("Select optCode From tblOption Where OptDesc = '" + TxtTrainingType.Text + "'")));

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTrainingCode, "Training", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTrainingRequestDlg3(this));
            }

            if (Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length > 0 && e.ColIndex == 11)
            {
                LueRequestEdit(Grd2, LueTrainingSchedule, ref fCell, ref fAccept, e);
                if (TxtTrainingCode.Text.Length > 0)
                    SetLueTrainingClass(ref LueTrainingSchedule, TxtTrainingCode.Text);
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTrainingCode, "Training", false))
                Sm.FormShowDialog(new FrmTrainingRequestDlg3(this));
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            var Value = string.Empty;
            if (e.ColIndex == 12 && TxtDocNo.Text.Length == 0)
            {
                Value = Sm.GetGrdStr(Grd2, e.RowIndex, 12);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime2(Grd2, e.RowIndex, 12, 12);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                    {
                        Grd2.Cells[e.RowIndex, 12].Value = string.Empty;
                    }
                }
                Sm.FocusGrd(Grd2, (e.RowIndex+1), 12);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainingRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TrainingRequest", "TblTrainingRequestHdr");
            mTrainingSiteCode = Sm.GetValue("Select SiteCode From TblTraining Where TrainingCode = @Param; ", TxtTrainingCode.Text);

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveTrainingRequestHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveTrainingRequestDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveTrainingRequestDtl2(DocNo, Row, mTrainingSiteCode));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsTrainingRequestUsePIC && Sm.IsLueEmpty(LuePIC, "PIC")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 trainer.");
                return true;
            }

            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 equipment.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested trainer data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTrainingRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, TrainingCode, CancelInd, CancelReason, SiteCode, Remark, CreateBy, CreateDt ");
            if (mIsTrainingRequestUsePIC)
                SQL.AppendLine(", PIC) ");
            else
                SQL.AppendLine(") ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @TrainingCode, 'N', Null,  @SiteCode, @Remark, @UserCode, CurrentDateTime() ");
            if (mIsTrainingRequestUsePIC)
                SQL.AppendLine(", @PIC); ");
            else
                SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveTrainingRequestDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingRequestDtl");
            SQL.AppendLine("(DocNo, DNo, TrainerCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @TrainerCode, @Remark, @CreateBy, CurrentDateTime()); ");
            


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainerCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTrainingRequestDtl2(string DocNo, int Row, string TrainingSiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingRequestDtl2");
            SQL.AppendLine("(DocNo, DNo, EmpCode, TrainingCode, TrainingDno, Tm, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @TrainingCode, @TrainingDno, @Tm, 'O', @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.SiteCode=@SiteCode ");
            if (mIsTrainingRequestApprovalNeedDept) SQL.AppendLine("    And T.DeptCode=@DeptCode ");
            else SQL.AppendLine("    And T.DeptCode Is Null ");
            SQL.AppendLine("And T.DocType='TrainingRequest'; ");

            SQL.AppendLine("Update TblTrainingRequestDtl2 Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Dno=@DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='TrainingRequest' And DocNo=@DocNo And Dno=@DNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mIsTrainingRequestApprovalNeedDept) Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@SiteCode", TrainingSiteCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
            Sm.CmParam<String>(ref cm, "@TrainingDno", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@Tm", Sm.GetGrdStr(Grd2, Row, 12).Replace(":", ""));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTrainingRequest());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this training request.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblTrainingRequestHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand CancelTrainingRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTrainingRequestHdr(DocNo);
                ShowTrainingRequestDtl(DocNo);
                ShowTrainingRequestDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "TrainingRequest", ref Grd3);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTrainingRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TrainingCode, B.TrainingName, C.OptDesc As TrainingType, ");
            SQL.AppendLine("A.CancelInd, A.CancelReason, A.SiteCode, A.Remark ");
            if (mIsTrainingRequestUsePIC)
                SQL.AppendLine(", A.PIC As PIC ");
            else
                SQL.AppendLine(", Null As PIC ");
            SQL.AppendLine("From TblTrainingRequestHdr A ");
            SQL.AppendLine("Inner Join TblTraining B On A.TrainingCode = B.TrainingCode ");
            SQL.AppendLine("Inner Join TblOption C On B.TrainingType = C.OptCode And C.OptCat='TrainerType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        "DocNo", 
                        "DocDt", "TrainingCode", "TrainingName", "TrainingType", "CancelInd",
                        "CancelReason", "SiteCode", "Remark", "PIC"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtTrainingCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtTrainingName.EditValue = Sm.DrStr(dr, c[3]);
                        TxtTrainingType.EditValue = Sm.DrStr(dr, c[4]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[6]); 
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[5]) == "Y");
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[7]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[9]));
                    }, true
                );
        }

        private void ShowTrainingRequestDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.Dno, A.TrainerCode, B.TrainerName, if(B.TrainerType = 'I', 'Internal', 'External') As Type, ifnull(C.EmpName, D.VdName) As Trainer, ");
                SQL.AppendLine("B.NoOfHr, A.Remark ");
                SQL.AppendLine("From TblTrainingRequestDtl A ");
                SQL.AppendLine("Inner Join TblTrainer B On A.TrainerCode = B.TrainerCode ");
                SQL.AppendLine("Left Join TblEmployee C On B.EmpCode = C.EmpCode ");
                SQL.AppendLine("Left Join TblVendor D On B.VdCode = D.VdCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "TrainerCode", "TrainerName", "Type", "Trainer", "NoOfHr", 
                        //6-7
                        "Remark",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTrainingRequestDtl2(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.Dno, A.EmpCode, B.EmpName, B.DeptCode,  ");
                SQL.AppendLine("D.DeptName, C.PosName, B.SiteCode, G.Sitename, E.OptDesc As Gender,  A.TrainingDno, F.Class, Concat(Left(A.Tm, 2), ':', Right(A.Tm, 2)) As Tm, A.Remark, ");
                SQL.AppendLine("Case When A.Status = 'O' Then 'Outstanding' When A.Status = 'A' Then 'Approved' When A.Status = 'C' Then 'Cancelled' End As ApprovalStatus ");
                SQL.AppendLine("From TblTrainingRequestDtl2 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblPosition C On B.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQL.AppendLine("Inner Join TblOption E On B.gender=E.OptCode And E.Optcat = 'Gender' ");
                SQL.AppendLine("Left Join TbltrainingDtl F on A.TrainingCode = F.TrainingCode And A.TrainingDno = F.Dno ");
                SQL.AppendLine("Left Join TblSite G On B.SiteCode = G.SiteCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

                Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 
                        //1-5
                        "EmpCode", "EmpName", "DeptCode", "DeptName", "PosName",  
                        //6-10
                        "SiteCode", "SiteName", "Gender", "TrainingDno", "Class", 
                        //11-12
                        "Tm", "Remark", "ApprovalStatus"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd2, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsTrainingRequestApprovalNeedDept = Sm.GetParameterBoo("IsTrainingRequestApprovalNeedDept");
            mIsTrainingUseAllSite = Sm.GetParameterBoo("IsTrainingUseAllSite");
            mIsTrainingRequestUsePIC = Sm.GetParameterBoo("IsTrainingRequestUsePIC");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, mCol));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedTrainerCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedEmployeeCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }


        public void SetLueTrainingClass(ref LookUpEdit Lue, string TrainingCode)
        {
            if (TxtTrainingCode.Text.Length > 0)
            {
                Sm.SetLue3(ref Lue,
                    "Select Dno As Col1, Class As Col2, ifnull(Concat(Left(Tm, 2), ':', Right(Tm, 2)), '') As Col3 " +
                    "From TblTrainingDtl Where TrainingCode = '" + TrainingCode + "' ; ",
                    0, 35, 35, false, true, true, "Code", "Name", "Time", "Col2", "Col1");
            }
        }

        internal string SetTime(string value)
        {
            var Tm = string.Empty;
            if (value.Length == 4 && Convert.ToDecimal(value) <= 2359 && Convert.ToDecimal(Sm.Right(value, 2)) <= 59)
            {
                Tm = String.Concat(Sm.Left(value, 2), ":", Sm.Right(value, 2));
            }
            if (value.Length == 3 && Convert.ToDecimal(value) <= 2359 && Convert.ToDecimal(Sm.Right(value, 2)) <= 59)
            {
                Tm = String.Concat("0", Sm.Left(value, 1), ":", Sm.Right(value, 2));
            }
            return Tm;
        }

        public void SetTime2(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            try
            {
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat("0", Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnTrainingCode_Click(object sender, EventArgs e)
        {
            ClearData2();
            Sm.FormShowDialog(new FrmTrainingRequestDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueTrainingSchedule_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueTrainingSchedule_Leave(object sender, EventArgs e)
        {
            if (LueTrainingSchedule.Visible && fAccept && fCell.ColIndex == 11)
            {
                if (Sm.GetLue(LueTrainingSchedule).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 10].Value = null;
                    Grd2.Cells[fCell.RowIndex, 11].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 10].Value = Sm.GetLue(LueTrainingSchedule);
                    Grd2.Cells[fCell.RowIndex, 11].Value = LueTrainingSchedule.GetColumnValue("Col2");
                    Grd2.Cells[fCell.RowIndex, 12].Value = LueTrainingSchedule.GetColumnValue("Col3");
                }
                LueTrainingSchedule.Visible = false;
            }
        }

        private void LueTrainingSchedule_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingSchedule, new Sm.RefreshLue2(SetLueTrainingClass), TxtTrainingCode.Text);
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearData2();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        #endregion

        #endregion
       
    }
}
