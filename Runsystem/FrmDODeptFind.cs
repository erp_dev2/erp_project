﻿#region Update
/*
    04/04/2017 [WED] Tambah kolom DO Journal# dan Cost Category
    19/09/2017 [TKG] Tambah informasi remark
    22/12/2017 [TKG] Filter department berdasarkan group
    23/05/2018 [TKG] tambah local document#
    07/01/2020 [DITA/SIER] Tambah parameter IsFilterByItCt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDODeptFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDODept mFrmParent;
        private string mSQL = string.Empty;
        internal bool
            mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmDODeptFind(FrmDODept FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                mFrmParent.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsDODeptFilterByAuthorization ? "Y" : "N");
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, C.WhsName, A.DORequestDeptDocNo, F.LocalDocNo As DORequestDeptLocalDocNo, F.WODocNo, E.DeptName, A.JournalDocNo, I.CCtName, ");
            SQL.AppendLine("B.CancelInd, B.ItCode, D.ItName, D.ForeignName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, ");
            SQL.AppendLine("A.Remark As RemarkH, B.Remark As RemarkD, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, IfNull(A.LastUpBy, B.LastUpBy) As LastUpBy, IfNull(A.LastUpDt, B.LastUpDt) As LastUpDt, D.ItGrpCode, G.AssetName, G.DisplayName AS DisplayAssetName ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblDepartment E On A.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblDORequestDeptHdr F On A.DORequestDeptDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblAsset G On B.AssetCode = G.AssetCode ");
            SQL.AppendLine("Left Join TblItemCostCategory H On B.ItCode = H.ItCode And F.CCCode = H.CCCode ");
            SQL.AppendLine("Left Join TblCostCategory I On H.CCtCode = I.CCtCode And H.CCCode = I.CCCode ");
            SQL.AppendLine("Where A.DORequestDeptDocNo Is Not Null ");
            SQL.AppendLine("And A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsDODeptFilterByAuthorization)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DO#", 
                        "Date",
                        "Local#",
                        "Warehouse",
                        "DO Request#",
                        
                        //6-10
                        "Requested Local#",
                        "WO#",
                        "Department",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",

                        //11-15
                        "Foreign Name",
                        "Group",       
                        "Cancel",
                        "Batch#",
                        "Source",
                        
                        //16-20
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //21-25
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Asset Name",
                        "Asset's"+Environment.NewLine+"Display Name",
                        
                        //26-30
                        "Cost Category",
                        "DO's Journal#",
                        "Remark",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        
                        //31-35
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 130, 200, 130, 
                        
                        //6-10
                        130, 130, 200, 80, 200, 

                        //11-15
                        150, 150, 60, 180, 180,  

                        //16-20
                        60, 60, 100, 80, 100, 

                        //21-25
                        80, 100, 80, 180, 180, 

                        //26-30
                        180, 150, 300, 300, 100, 

                        //31-35
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 22 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 31, 34 });
            Sm.GrdFormatTime(Grd1, new int[] { 32, 35 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 11, 15, 30, 31, 32, 33, 34, 35 }, false);
            if(mFrmParent.mIsShowForeignName) Grd1.Cols[11].Visible = true;
            if (mFrmParent.mIsItGrpCodeShow) Grd1.Cols[12].Visible = true;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 11, 15, 30, 31, 32, 33, 34, 35 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23 }, true);
        }


        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
             
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDORequestDocNo.Text, new string[] { "A.DORequestDeptDocNo", "F.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtWODocNo.Text, "F.WODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "B.Source", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "WhsName", "DORequestDeptDocNo", "DORequestDeptLocalDocNo", 
                            
                            //6-10
                            "WODocNo", "DeptName", "ItCode", "ItName", "ForeignName", 
                            
                            //11-15
                            "ItGrpCode", "CancelInd", "BatchNo", "Source", "Lot", 
                            
                            //16-20
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //21-25
                            "Qty3", "InventoryUomCode3", "AssetName", "DisplayAssetName", "CCtName", 
                            
                            //26-30
                            "JournalDocNo", "RemarkH", "RemarkD", "CreateBy", "CreateDt", 
                            
                            //31-32
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 35, 32);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDORequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDORequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO Request#");
        }

        private void TxtWODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WO#");
        }
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(mFrmParent.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsDODeptFilterByAuthorization ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source#");
        }

        #endregion

        #endregion
    }
}
