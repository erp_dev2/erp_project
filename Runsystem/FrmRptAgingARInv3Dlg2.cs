﻿#region update

    /*
     * 18/07/2022 [RDA/PRODUCT] new apps based on FrmrptAgingARInvDlg2
     */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInv3Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptAgingARInv3 mFrmParent;
        internal string
            SalesInvoiceDocNo = string.Empty;
        internal bool
            mVoucherAccessInd = false;

        #endregion

        #region Constructor

        public FrmRptAgingARInv3Dlg2(FrmRptAgingARInv3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "Invoice#",
                        "Reference",
                        "Date Voucher",
                        "Voucher",
                        "",

                        //6
                        "Amount",
                    },
                   new int[] 
                    {
                        //0
                        30,
 
                        //1-5
                        160, 160, 100, 100, 20,

                        //6
                        100
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });

            mVoucherAccessInd = GetAccessInd("FrmVoucher");
            if (!mVoucherAccessInd)
                Sm.GrdColInvisible(Grd1, new int[] { 5 });

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.SLIDocNo, T.Reference, T.VcDt, T.VcDocNo, T.Amt FROM ( ");
            SQL.AppendLine("SELECT D.InvoiceDocNo SLIDocNo, C.DocNo Reference, A.DocDt VcDt, A.DocNo VcDocNo, A.Amt Amt ");
            SQL.AppendLine("FROM tblvoucherhdr A ");
            SQL.AppendLine("INNER JOIN tblvoucherrequesthdr B ON A.VoucherRequestDocNo = B.DocNo AND A.DocNo = B.VoucherDocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymenthdr C ON B.DocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("INNER JOIN tblincomingpaymentdtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("SELECT E.SalesInvoiceDocNo SLIDocNo, E.DocNo Reference, NULL AS VcDocNo, NULL AS VcDt, E.Amt Amt ");
            SQL.AppendLine("FROM tblarshdr E");
            SQL.AppendLine(") T WHERE T.SLIDocNo = @SLIDocNo;");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;
                Sm.CmParam<string>(ref cm, "@SLIDocNo", SalesInvoiceDocNo);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter,
                        new string[] 
                        { 
                            //0
                            "SLIDocNo",
 
                            //1-4
                            "Reference",
                            "VcDt",
                            "VcDocNo",
                            "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, false
                    );
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length > 0)
            {
                var f = new FrmVoucher(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
