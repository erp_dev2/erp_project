﻿#region Update
/*
    27/09/2017 [WED] tambah informasi nomor DO To Department
    09/10/2017 [TKG] tambah filter berdasarkan otorisasi group thd cost center
    23/05/2018 [TKG] tambah local document#
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDORequestDeptFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDORequestDept mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDORequestDeptFind(FrmDORequestDept FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                mFrmParent.SetLueDeptCode(ref LueDeptCode, string.Empty);
                mFrmParent.SetLueWhsCode(ref LueWhsCode, string.Empty);
                mFrmParent.mStateIndicator = 1;
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, D.CancelInd, B.DeptName, C.WhsName, F.DODeptDocNo, ");
            SQL.AppendLine("D.Itcode, E.ItName, E.ForeignName, D.Qty, E.InventoryUomCode, D.Qty2, E.InventoryUomCode2, D.Qty3, E.InventoryUomCode3, A.Remark, ");  
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, E.ItGrpCode ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            if (mFrmParent.mIsFilterByDept && mFrmParent.mStateIndicator == 1)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=B.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            else
            {
                if (mFrmParent.mIsDODeptFilterByAuthorization)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=B.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            if (mFrmParent.mIsDODeptFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=C.WhsCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblDORequestDeptDtl D On A.DocNo = D.DocNo "); 
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("  Select Group_Concat(T1.DocNo Separator ', ') As DODeptDocNo, T2.DocNo As DORequestDeptDocNo, ");
            SQL.AppendLine("  T3.DORequestDeptDNo ");
            SQL.AppendLine("  From TblDODeptHdr T1 ");
            SQL.AppendLine("  Inner Join TblDORequestDeptHdr T2 On T1.DORequestDeptDocNo = T2.DocNo ");
            SQL.AppendLine("  Inner Join TblDODeptDtl2 T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("  Where T1.DORequestDeptDocNo Is Not Null ");
            SQL.AppendLine("  Group By T2.DocNo, T3.DORequestDeptDNo ");
            SQL.AppendLine(") F On A.DocNo = F.DORequestDeptDocNo And D.DNo = F.DORequestDeptDNo ");

            if (mFrmParent.mIsFilterByCC)
            {
                SQL.AppendLine("Inner Join TblCostCenter G On A.CCCode=G.CCCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=G.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("Where A.WODOcNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Local"+Environment.NewLine+"Document#",
                        "Department",
                        
                        //6-10
                        "Warehouse",
                        "DO To"+Environment.NewLine+"Department#",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Foreign Name",
                        
                        //11-15
                        "Group",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        
                        //16-20
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        
                        //21-24
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        130, 80, 60, 130, 200, 
                        
                        //6-10
                        200, 250, 100, 250, 200, 
                        
                        //11-15
                        200, 100, 100, 100, 100, 

                        //16-20
                        100, 100, 300, 100, 100, 

                        //21-25
                        100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And D.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "E.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "DeptName", "WhsName", 
                            
                            //6-10
                            "DODeptDocNo", "ItCode", "ItName", "ForeignName", "ItGrpCode", 
                            
                            //11-15
                            "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 
                            
                            //16-20
                            "InventoryUomCode3", "Remark", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //21
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(mFrmParent.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(mFrmParent.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
