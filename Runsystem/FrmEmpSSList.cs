﻿#region Update
/*
    21/05/2017 [TKG] tambah validasi apabila ada card# yg sama untuk kode karyawan yg sama.
    29/05/2017 [TKG] tambah validasi nomor kartu tidak boleh sama berdasarkan kode employee, nomor kartu, dan jenis social security
    27/09/2017 [TKG] menggunakan employee salary for social security berdasarkan parameter SSSalaryInd
    09/10/2017 [TKG] merubah perhitungan maksimum salary untuk HIN.
    10/10/2017 [TKG] menggunakan parameter IsFilterBySiteHR untuk filter site berdasarkan group
    19/12/2017 [TKG] tambah filter dept+site berdasarkan group
    13/04/2018 [HAR] rounding value untuk nilai employee, employer, dan total amount
    16/05/2018 [TKG] tambah entity
    08/06/2018 [HAR] tambah notifikasi jika employee masih kedaftar di document lain
    03/09/2018 [TKG] Berdasarkan parameter IsPayrollAmtRounded, semua amount yang berhubungan dengan payroll akan dirounding atau tidak.
    20/09/2018 [TKG] tambah filter employment status
    30/09/2018 [TKG] bilangkan angka di belakang koma.
    23/11/2018 [HAR] tambah parameter buat nentuin tipe roundingnya
    06/01/2019 [TKG] Untuk KMI, komponen allowance deduction berdasarkan start date dan end date-nya yg dibandingkan dengan cut off date
    23/10/2019 [TKG/SRN] SSForRetiring bisa diisi lebih dari 1 code
    01/11/2019 [TKG/SRN] perhitungan divalidasi minimum salary
    03/09/2020 [TKG/SRN] Parameter SSProgramForEmployment bisa diisi lbh dari 1 kode ss
    21/09/2020 [TKG/HIN] tambah 4 angka di belakang koma utk persentase
    13/01/2020 [TKG/PHT] Berdasarkan parameter IsEmpSSListExclPassAwalEmp, karyawan yg sudah meninggal tidak akan diproses datanya.
    24/06/2021 [TKG/IMS] department mengambil dari emp ss list
    06/01/2022 [TKG/PHT] Kasih remark pada query save
    22/03/2022 [VIN/ALL] BUG: Export to excel pakai Sm. 
    21/11/2022 [BRI/PHT] BUG: nilai total rounding
    30/11/2022 [TYO/PHT] membuat validasi ketika cancel cek apakah payrun yg dipakai sudah diproses sampai VR Payroll
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSSList : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mSSForRetiring = string.Empty;
        private string 
            mSSSalaryInd = string.Empty, 
            mSSAllowance = string.Empty,
            mSSProgramForHealth = string.Empty,
            mSSProgramForEmployment = string.Empty,
            mSSProgramForPension = string.Empty;
        internal FrmEmpSSListFind FrmFind;
        private decimal mSSRetiredMaxAge = 0m, mMaxLimitSalaryForSS = 0m;
        internal bool 
            mIsSiteMandatory = false, 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false,
            mIsEmpSSBasedOnEntity = false;
        private bool 
            mIsPayrollAmtRounded = false,
            mIsEmpSSListExclPassAwalEmp = false;
        private string mPayrollAmtRoundedType = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpSSList(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Employee's Social Security";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sl.SetLueSSPCode(ref LueSSPCode);
                if (mIsSiteMandatory && !mIsEmpSSBasedOnEntity)
                    LblSiteCode.ForeColor = Color.Red;
                if (mIsEmpSSBasedOnEntity)
                    LblEntCode.ForeColor = Color.Red;
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, true);
                Sl.SetLueEntCode(ref LueEntCode);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo, TxtEmployer, TxtEmployee, TxtTotal }, true);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Participants",
                        "Old"+Environment.NewLine+"Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        "Identity#",
                        "Birth Date",
                        
                        //11-15
                        "Age",
                        "Social Security Code",
                        "Social Security"+Environment.NewLine+"Name",
                        "Card#",
                        "Start"+Environment.NewLine+"Date",

                        //16-20
                        "End"+Environment.NewLine+"Date",
                        "Problem",
                        "Remark",
                        "Status",
                        "Salary",

                        //21-25
                        "Employer"+Environment.NewLine+"(%)",
                        "Employer"+Environment.NewLine+"(Amount)",
                        "Employee"+Environment.NewLine+"(%)",
                        "Employee"+Environment.NewLine+"(Amount)",
                        "Total"+Environment.NewLine+"(%)",
                        
                        //26-28
                        "Total"+Environment.NewLine+"(Amount)",
                        "Payrun Code",
                        "Employment Status"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 200, 80, 150, 
                        
                        //6-10
                        150, 100, 100, 150, 100,  
                        
                        //11-15
                        80, 0, 150, 100, 100,
                        
                        //16-20
                        100, 150, 200, 200, 100, 
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-28
                        100, 130, 150
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 20, 22, 24, 26 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 21, 23, 25 }, 4);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8, 10, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 7, 10, 12, 20, 27 }, false);
            Grd1.Cols[28].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 7, 10, 27 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueYr, LueMth, LueSSPCode, 
                        LueSiteCode, DteCutOffDt, MeeRemark, LueEmploymentStatus, ChkEmploymentStatus
                    }, true);
                    if (mIsEmpSSBasedOnEntity)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, true);
                    BtnProcess.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueMth, LueSSPCode, LueSiteCode, 
                        DteCutOffDt, MeeRemark, LueEmploymentStatus, ChkEmploymentStatus
                    }, false);
                    if (mIsEmpSSBasedOnEntity)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, false);
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, LueMth, LueSSPCode, 
                LueSiteCode, LueEntCode, DteCutOffDt, MeeRemark, LueEmploymentStatus
            });
            ChkCancelInd.Checked = false;
            ChkEmploymentStatus.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtEmployer, TxtEmployee, TxtTotal }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 20, 21, 22, 23, 24, 25, 26 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            if (BtnSave.Enabled)
            {
                TxtEmployer.EditValue = Sm.FormatNum(0m, 0);
                TxtEmployee.EditValue = Sm.FormatNum(0m, 0);
                TxtTotal.EditValue = Sm.FormatNum(0m, 0);
                ClearGrd();
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpSSListFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteCutOffDt.DateTime = DteDocDt.DateTime;
                
                var DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);
                Sm.SetLue(LueYr, Sm.Left(DocDt, 4));
                Sm.SetLue(LueMth, DocDt.Substring(4, 2));
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnExcelClick(object sender, EventArgs e)
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 9].Value = "'" + Sm.GetGrdStr(Grd1, Row, 9);
                Grd1.Cells[Row, 14].Value = "'" + Sm.GetGrdStr(Grd1, Row, 14);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                //Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                //Grd1.Cells[Row, 9].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 9), Sm.GetGrdStr(Grd1, Row, 9).Length - 1);
                //Grd1.Cells[Row, 14].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 14), Sm.GetGrdStr(Grd1, Row, 14).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpSSList", "TblEmpSSListHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpSSListDtl(DocNo));
            cml.Add(SaveEmpSSListHdr(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueSSPCode, "Social security program") ||
                IsGrdEmpty() ||
                //IsSSPeriodNotValid() ||
                IsEmpCardNoNotValid() ||
                (mIsSiteMandatory && !mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueEntCode, "Registered social security entity"))||
                (IsEmpAlreadyExistOnSSList())
                ) 
                return true;
            return false;
        }

        private bool IsEmpCardNoNotValid()
        {
            string EmpCode = string.Empty, SSCode = string.Empty, CardNo = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, i, 1);
                SSCode = Sm.GetGrdStr(Grd1, i, 12);
                CardNo = Sm.GetGrdStr(Grd1, i, 14);
                if (EmpCode.Length > 0 && SSCode.Length>0 && CardNo.Length>0)
                {
                    for (int j = i+1; j < Grd1.Rows.Count; j++)
                    { 
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd1, j, 1)) && 
                            Sm.CompareStr(SSCode, Sm.GetGrdStr(Grd1, j, 12)) &&
                            Sm.CompareStr(CardNo, Sm.GetGrdStr(Grd1, j, 14)))
                        {
                            Sm.StdMsg(mMsgType.Warning, 
                                "Employee Code : " + EmpCode + Environment.NewLine +
                                "Participants : " + Sm.GetGrdStr(Grd1, j, 3) + Environment.NewLine +
                                "Social Security : " + Sm.GetGrdStr(Grd1, j, 13) + Environment.NewLine +
                                "Card# : " + CardNo + Environment.NewLine + Environment.NewLine +
                                "Duplicate card#.");
                            return true;
                        }
                    }        
                }
            }
            return false;
        }

        private bool IsEmpAlreadyExistOnSSList()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblEmpSSListHdr A ");
                SQL.AppendLine("Inner Join TblEmpSSListDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Where A.CancelInd = 'N' ");
                SQL.AppendLine("And Yr='" + Sm.GetLue(LueYr) + "' ");
                SQL.AppendLine("And Mth='" + Sm.GetLue(LueMth) + "' ");
                SQL.AppendLine("And SSPCode='" + Sm.GetLue(LueSSPCode) + "' ");
                //SQL.AppendLine("And CutOffDt>='" + Sm.GetDte(DteCutOffDt).Substring(0, 8) + "' ");
                SQL.AppendLine("And B.EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' ");
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
               
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Year : " + Sm.GetLue(LueYr) + Environment.NewLine +
                        "Month : " + Sm.GetLue(LueMth) + Environment.NewLine +
                        "Social Security Program : " + LueSSPCode.GetColumnValue("Col2") + Environment.NewLine +
                        (Sm.GetLue(LueSiteCode).Length > 0 ?
                        "Site : " + LueSiteCode.GetColumnValue("Col2") + Environment.NewLine
                        : string.Empty) +
                        Environment.NewLine +
                        "Employee : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "active on other document social security. "+Sm.GetValue(cm)+" ");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsSSPeriodNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpSSListHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And SSPCode=@SSPCode ");
            SQL.AppendLine("And CancelInd='N' ");
            if (Sm.GetLue(LueSiteCode).Length>0)
                SQL.AppendLine("And SiteCode=@SiteCode ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Year : " + Sm.GetLue(LueYr) + Environment.NewLine +
                    "Month : " + Sm.GetLue(LueMth) + Environment.NewLine +
                    "Social Security Program : " + LueSSPCode.GetColumnValue("Col2") + Environment.NewLine +
                    (Sm.GetLue(LueSiteCode).Length>0?
                    "Site : " + LueSiteCode.GetColumnValue("Col2") + Environment.NewLine 
                    :string.Empty) +
                    Environment.NewLine +
                    "This document already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpSSListHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Employee SS List Hdr */ ");

            SQL.AppendLine("Insert Into TblEmpSSListHdr(DocNo, DocDt, CancelInd, Yr, Mth, SSPCode, SiteCode, EntCode, CutOffDt, Employer, Employee, Total, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, 'N', @Yr, @Mth, @SSPCode, @SiteCode, @EntCode, @CutOffDt, ");
            SQL.AppendLine("Sum(EmployerAmt) As Employer, Sum(EmployeeAmt) As Employee, Sum(TotalAmt) As Total, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblEmpSSListDtl ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParamDt(ref cm, "@CutOffDt", Sm.GetDte(DteCutOffDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveEmpSSListDtl(string DocNo)
        {
            decimal Salary = 0m;

            if (mSSSalaryInd == "1")
                Salary = Sm.GetParameterDec("SSSal");
            
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Employee SS List Dtl */ ");

            SQL.AppendLine("Insert Into TblEmpSSListDtl ");
            SQL.AppendLine("(DocNo, SSCode, EmpCode, CardNo, EmpInd, ");
            SQL.AppendLine("PName, JoinDt, ResignDt, Gender, BirthPlace, BirthDt, Age, Village, RTRW, SubDistrict, ");
            SQL.AppendLine("CityCode, DeptCode, MaritalStatus, IDNo, EducationLevel, Mother, StartDt, EndDt, Problem, Remark, ");
            SQL.AppendLine("FamilyStatus, HealthFacility, Salary, EmployerPerc, EmployeePerc,  TotalPerc,  ");
            SQL.AppendLine("EmployerAmt, EmployeeAmt, TotalAmt,  ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("    Select @DocNo, SSCode, EmpCode, CardNo, EmpInd, ");
            SQL.AppendLine("    PName, JoinDt, ResignDt, Gender, BirthPlace, BirthDt, Age, Village, RTRW, SubDistrict, ");
            SQL.AppendLine("    CityCode, DeptCode, MaritalStatus, IDNo, EducationLevel, Mother, StartDt, EndDt, ");
            SQL.AppendLine("    Problem, Remark, ");
            SQL.AppendLine("    FamilyStatus, HealthFacility, ");
            SQL.AppendLine("    Salary, EmployerPerc, EmployeePerc, TotalPerc, ");
            if (mIsPayrollAmtRounded)
            {
                if (mPayrollAmtRoundedType == "1")
                {
                    SQL.AppendLine("Floor(EmployerAmt) EmployerAmt, Floor(EmployeeAmt) EmployeeAmt, Floor(TotalAmt) TotalAmt,  ");
                }
                else
                {
                    SQL.AppendLine("Round(EmployerAmt, 0) EmployerAmt, Round(EmployeeAmt, 0) EmployeeAmt, Round(TotalAmt, 0) TotalAmt,  ");
                }
            }
            else
            {
                SQL.AppendLine("EmployerAmt, EmployeeAmt, TotalAmt,  ");
            }
            SQL.AppendLine("    @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    Select '1' As RecType, A.SSCode, A.EmpCode, A.CardNo, 'Y' As EmpInd, ");
            SQL.AppendLine("    C.EmpName As PName, C.JoinDt, C.ResignDt, C.Gender, C.BirthPlace, C.BirthDt, ");
            SQL.AppendLine("    Case When C.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(C.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    C.Village, C.RTRW, C.SubDistrict, C.CityCode, C.DeptCode, C.MaritalStatus, C.IDNumber As IDNo, D.Level As EducationLevel,");
            SQL.AppendLine("    C.Mother, A.StartDt, A.EndDt, ");
            SQL.AppendLine("    A.Problem, E.OptDesc As ProblemDesc, A.Remark, ");
            SQL.AppendLine("    Null As FamilyStatus, A.HealthFacility, ");

            if (mSSSalaryInd == "1")
            {
                if (Sm.CompareStr(Sm.GetLue(LueSSPCode), mSSProgramForHealth))
                {
                    SQL.AppendLine("Case When IfNull(F.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.HealthSSSalary, 0) End As Salary, ");
                    SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*Case When IfNull(F.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.HealthSSSalary, 0) End  As EmployerAmt, ");
                    SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*Case When IfNull(F.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.HealthSSSalary, 0) End  As EmployeeAmt, ");
                    SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*Case When IfNull(F.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.HealthSSSalary, 0) End  As TotalAmt ");
                }
                else
                {
                    //if (Sm.CompareStr(Sm.GetLue(LueSSPCode), mSSProgramForEmployment))
                    if (IsSSPCodeValid(Sm.GetLue(LueSSPCode), mSSProgramForEmployment))
                    {
                        SQL.AppendLine("Case When IfNull(F.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.EmploymentSSSalary, 0) End As Salary, ");
                        SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*Case When IfNull(F.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.EmploymentSSSalary, 0) End  As EmployerAmt, ");
                        SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*Case When IfNull(F.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.EmploymentSSSalary, 0) End  As EmployeeAmt, ");
                        SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*Case When IfNull(F.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(F.EmploymentSSSalary, 0) End  As TotalAmt ");
                    }
                    else
                    {
                        SQL.AppendLine("@Salary As Salary, ");
                        SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*@Salary As EmployerAmt, ");
                        SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*@Salary As EmployeeAmt, ");
                        SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*@Salary As TotalAmt ");
                    }
                }
            }

            if (mSSSalaryInd == "2" || mSSSalaryInd == "3")
            {
                SQL.AppendLine("Case When IfNull(B.SalaryMaxLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("    Case When IfNull(B.SalaryMinLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("        IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(F.Salary, 0)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As Salary, ");

                SQL.AppendLine("B.EmployerPerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(F.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployerAmt, ");

                SQL.AppendLine("B.EmployeePerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(F.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployeeAmt, ");

                SQL.AppendLine("B.TotalPerc, ");

                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("        Floor(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                    else
                        SQL.AppendLine("        Round(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                }
                else
                    SQL.AppendLine("        B.TotalPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("            Floor(B.EmployerPerc*0.01*B.SalaryMinLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                    else
                        SQL.AppendLine("            Round(B.EmployerPerc*0.01*B.SalaryMinLimit) + Round(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                }
                else
                    SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("            Floor(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                    else
                        SQL.AppendLine("            Round(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                }
                else
                    SQL.AppendLine("            B.TotalPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(F.Salary, 0.00)>B.SalaryMaxLimit Then ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("        Floor(B.EmployerPerc*0.01*B.SalaryMaxLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMaxLimit) ");
                    else
                        SQL.AppendLine("        Round(B.EmployerPerc*0.01*B.SalaryMaxLimit) + Round(B.EmployeePerc*0.01*B.SalaryMaxLimit) ");
                }
                else
                    SQL.AppendLine("        B.TotalPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(F.Salary, 0)<B.SalaryMinLimit Then ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("            Floor(B.EmployerPerc*0.01*B.SalaryMinLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                    else
                        SQL.AppendLine("            Round(B.EmployerPerc*0.01*B.SalaryMinLimit) + Round(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                }
                else
                    SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                        SQL.AppendLine("            Floor(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                    else
                        SQL.AppendLine("            Round(B.EmployerPerc*0.01*IfNull(F.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(F.Salary, 0.00)) ");
                }
                else
                    SQL.AppendLine("            B.TotalPerc*0.01*IfNull(F.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As TotalAmt ");                
            }
            SQL.AppendLine("    From TblEmployeeSS A ");
            SQL.AppendLine("    Inner Join TblSS B On A.SSCode=B.SSCode And B.SSPCode=@SSPCode ");
            SQL.AppendLine("    Inner Join TblEmployee C ");
            SQL.AppendLine("        On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("        And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>Left(@CutOffDt, 8))) ");
            SQL.AppendLine("        And C.EmpCode Not In (");
            SQL.AppendLine("            Select T2.EmpCode ");
            SQL.AppendLine("            From TblEmpSSListHdr T1 ");
            SQL.AppendLine("            Inner Join TblEmpSSListDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            Where T1.CancelInd = 'N' ");
            SQL.AppendLine("            And T1.Yr=@Yr ");
            SQL.AppendLine("            And T1.Mth=@Mth ");
            SQL.AppendLine("            And T1.SSPCode=@SSPCode ");
            SQL.AppendLine("        ) ");
            if (ChkEmploymentStatus.Checked)
                SQL.AppendLine("        And C.EmploymentStatus=@EmploymentStatus ");
            if (Sm.GetLue(LueSiteCode).Length>0)
                SQL.AppendLine("        And C.SiteCode=@SiteCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And C.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And C.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            if (mIsEmpSSListExclPassAwalEmp)
            {
                SQL.AppendLine("And C.EmpCode Not In ( ");
                SQL.AppendLine("    Select Distinct A.EmpCode  ");
                SQL.AppendLine("    From TblPPS A, TblParameter B ");
                SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A'  ");
                SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Level ");
            SQL.AppendLine("        From TblEmployeeEducation T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select T2a.EmpCode, Max(T2a.DNo) As DNo ");
            SQL.AppendLine("            From TblEmployeeEducation T2a ");
            SQL.AppendLine("            Join TblEmployee T2b ");
            SQL.AppendLine("                On T2a.EmpCode=T2b.EmpCode ");
            SQL.AppendLine("                And (T2b.ResignDt Is Null Or (T2b.ResignDt Is Not Null And T2b.ResignDt>Left(@CutOffDt, 8))) ");
            SQL.AppendLine("            Where T2a.HighestInd='Y' ");
            SQL.AppendLine("            Group By T2a.EmpCode ");
            SQL.AppendLine("        ) T2 On T1.EmpCode=T2.EmpCode And T1.DNo=T2.DNo ");
            SQL.AppendLine("        Where T1.HighestInd='Y' ");
            SQL.AppendLine("    ) D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("    Left Join TblOption E On E.OptCat='EmployeeSSProblem' And A.Problem=E.OptCode ");

            if (mSSSalaryInd == "1")
            {
                SQL.AppendLine("Left Join TblGradeLevelHdr F On C.GrdLvlCode=F.GrdLvlCode "); 
            }

            if (mSSSalaryInd == "2")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.StartDt, T1.Amt+IfNull(T3.Amt, 0) As Salary ");
                SQL.AppendLine("    From TblEmployeeSalary T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalary ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("        ) And StartDt<Left(@CutOffDt, 8) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select EmpCode, Sum(Amt) As Amt ");
                SQL.AppendLine("        From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("        Where Position(ADCode In @SSAllowance) ");
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Null) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Null And StartDt<=@CutOffDt) Or ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Not Null And @CutOffDt<=EndDt) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@CutOffDt And @CutOffDt<=EndDt) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode ");
                SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            }

            if (mSSSalaryInd == "3")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.Amt As Salary ");
                SQL.AppendLine("    From TblEmployeeSalarySS T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalarySS ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        And Left(@CutOffDt, 8)>=StartDt ");
                SQL.AppendLine("        And Left(@CutOffDt, 8)<=IfNull(EndDt, '99990101') ");
                SQL.AppendLine("        And SSPCode=@SSPCode ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("    Where T1.SSPCode=@SSPCode ");
                SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            }
            SQL.AppendLine("Where 1=1 ");
            if (mIsEmpSSBasedOnEntity)
            {
                SQL.AppendLine("And A.EntCode Is Not Null ");
                SQL.AppendLine("And A.EntCode=@EntCode ");
            }
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As RecType, A.SSCode, A.EmpCode, A.CardNo, 'N' As EmpInd, ");
            SQL.AppendLine("    A.FamilyName As PName, Null As JoinDt, Null As ResignDt, Null As Gender, Null As BirthPlace, A.BirthDt, ");
            SQL.AppendLine("    Case When A.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(A.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("    Null As Village, Null As RTRW, Null As SubDistrict, Null As CityCode, Null As DeptCode, Null As MaritalStatus, A.IDNo, Null As EducationLevel,");
            SQL.AppendLine("    Null As Mother, A.StartDt, A.EndDt, A.Problem, D.OptDesc As ProblemDesc, A.Remark, ");
            SQL.AppendLine("    A.Status As FamilyStatus, A.HealthFacility, ");
            SQL.AppendLine("    0.00 As Salary, ");
            SQL.AppendLine("    0.00 As EmployerPerc, 0.00 As EmployerAmt, ");
            SQL.AppendLine("    0.00 As EmployeePerc, 0.00 As EmployeeAmt, ");
            SQL.AppendLine("    0.00 As TotalPerc, 0.00 As TotalAmt ");
            SQL.AppendLine("    From TblEmployeeFamilySS A ");
            SQL.AppendLine("    Inner Join TblEmployee B ");
            SQL.AppendLine("        On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("        And (B.ResignDt Is Null Or (B.ResignDt Is Not Null And B.ResignDt>Left(@CutOffDt, 8))) ");
            SQL.AppendLine("        And B.EmpCode Not In (");
            SQL.AppendLine("            Select T2.EmpCode ");
            SQL.AppendLine("            From TblEmpSSListHdr T1 ");
            SQL.AppendLine("            Inner Join TblEmpSSListDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            Where T1.CancelInd = 'N' ");
            SQL.AppendLine("            And T1.Yr=@Yr ");
            SQL.AppendLine("            And T1.Mth=@Mth ");
            SQL.AppendLine("            And T1.SSPCode=@SSPCode ");
            SQL.AppendLine("        ) ");
            if (ChkEmploymentStatus.Checked)
                SQL.AppendLine("        And B.EmploymentStatus=@EmploymentStatus ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("        And B.SiteCode=@SiteCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And B.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And B.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=B.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsEmpSSListExclPassAwalEmp)
            {
                SQL.AppendLine("And B.EmpCode Not In ( ");
                SQL.AppendLine("    Select Distinct A.EmpCode  ");
                SQL.AppendLine("    From TblPPS A, TblParameter B ");
                SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A'  ");
                SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TblSS C On A.SSCode=C.SSCode And C.SSPCode=@SSPCode ");
            SQL.AppendLine("    Left Join TblOption D On D.OptCat='EmployeeSSProblem' And A.Problem=D.OptCode ");
            if (mIsEmpSSBasedOnEntity)
            {
                SQL.AppendLine("Where A.EntCode Is Not Null ");
                SQL.AppendLine("And A.EntCode=@EntCode ");
            }
            SQL.AppendLine(") T ");
            //SQL.AppendLine("Where (T.SSCode<>@SSForRetiring ");

            SQL.AppendLine("Where (Not Find_in_set(T.SSCode, IfNull(( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("    ), '')) ");

            SQL.AppendLine("Or ( ");
            //SQL.AppendLine("    T.SSCode=@SSForRetiring ");
            SQL.AppendLine("    Find_in_set(T.SSCode, IfNull(( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("    ), '')) ");

            SQL.AppendLine("    And T.BirthDt Is Not Null ");
            SQL.AppendLine("    And TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))<@SSRetiredMaxAge ");
            SQL.AppendLine("    And Not ( ");
            SQL.AppendLine("        TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))=@SSRetiredMaxAge-1 ");
            SQL.AppendLine("        And (Substring(T.BirthDt, 5, 2)=@RetiringMth) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")) ");
            SQL.AppendLine("And T.EndDt Is Null ");
            SQL.AppendLine("And ");
            SQL.AppendLine("(");
            SQL.AppendLine("    T.StartDt Is Null ");
            SQL.AppendLine("    Or ( T.StartDt Is Not Null And T.StartDt<=Left(@CutOffDt, 8))");
            SQL.AppendLine(");");
                
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            if (mSSSalaryInd == "1")
                Sm.CmParam<Decimal>(ref cm, "@Salary", Salary);

            if (mSSSalaryInd == "2")
                Sm.CmParam<String>(ref cm, "@SSAllowance", mSSAllowance);

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEmploymentStatus.Checked) 
                Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@SSForRetiring", mSSForRetiring);
            Sm.CmParam<Decimal>(ref cm, "@SSRetiredMaxAge", mSSRetiredMaxAge);
            Sm.CmParam<Decimal>(ref cm, "@MaxLimitSalaryForSS", mMaxLimitSalaryForSS);
            Sm.CmParam<String>(ref cm, "@DtForAge", Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01");
            Sm.CmParamDt(ref cm, "@CutOffDt", Sm.GetDte(DteCutOffDt).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@RetiringMth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmpSSListHdr(TxtDocNo.Text));
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                IsEmpSSListNotCancelled() || 
                IsEmpSSListCancelledAlready() ||
                IsEmpSSListProcessedAlready() ||
                IsPayrunCodeAlreadyProcessedToVoucherReq();
        }

        private bool IsEmpSSListNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsEmpSSListCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpSSListHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsEmpSSListProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpSSListHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And ProcessInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);


            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed.");
                return true;
            }

            return false;
        }

        private bool IsPayrunCodeAlreadyProcessedToVoucherReq()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT A.DocNo EmpSSDocNo, B.DocNo VRPayrolDocNo, A.PayrunCode ");
            SQL.AppendLine("FROM TblEmpSSListDtl A ");
            SQL.AppendLine("INNER JOIN TblVoucherRequestPayrollDtl2 B ON A.PayrunCode = B.PayrunCode  ");
            SQL.AppendLine("INNER JOIN TblVoucherRequestPayrollHdr C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("WHERE C.CancelInd = 'N' AND  C.`Status` <> 'C' ");
            SQL.AppendLine("AND A.DocNo = @DocNo; ");
            //SQL.AppendLine("SELECT 1 ");
            //SQL.AppendLine("FROM TblVoucherRequestPayrollDtl2 A ");
            //SQL.AppendLine("INNER JOIN TblVoucherRequestPayrollHdr B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
            //SQL.AppendLine("WHERE A.PayrunCode IN ( SELECT PayrunCode FROM TblEmpSSListDtl WHERE DocNo = @DocNo ) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to voucher request payroll.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateEmpSSListHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpSSListHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpSSListHdr(DocNo);
                ShowEmpSSListDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpSSListHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, Yr, Mth, SSPCode, SiteCode, EntCode, CutOffDt, Employer, Employee, Total, Remark " +
                    "From TblEmpSSListHdr Where DocNo=@DocNo;",
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "Yr", "Mth", "SSPCode",
 
                        //6-10
                        "SiteCode", "EntCode", "CutOffDt", "Employer", "Employee", 
                        
                        //11-12
                        "Total", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueSSPCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[6]), string.Empty);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[7]));
                        Sm.SetDte(DteCutOffDt, Sm.DrStr(dr, c[8]));
                        TxtEmployer.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtEmployee.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowEmpSSListDtl(string DocNo)
        {
            int SeqNo = 0;
            string EmpInd = string.Empty;
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpInd, A.EmpCode, A.PName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.JoinDt, A.ResignDt, A.IDNo, A.BirthDt, ");
            SQL.AppendLine("A.Age, A.SSCode, E.SSName, A.CardNo, A.StartDt, A.EndDt, ");
            SQL.AppendLine("G.OptDesc As ProblemDesc, A.Remark, ");
            SQL.AppendLine("F.OptDesc As FamilyStatus, ");
            SQL.AppendLine("H.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("A.Salary, A.EmployerPerc, A.EmployerAmt, A.EmployeePerc, A.EmployeeAmt, A.TotalPerc, A.TotalAmt, A.PayrunCode ");
            SQL.AppendLine("From TblEmpSSListDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSS E On A.SSCode=E.SSCode ");
            SQL.AppendLine("Left Join TblOption F On F.OptCat='FamilyStatus' And A.FamilyStatus=F.OptCode ");
            SQL.AppendLine("Left Join TblOption G On G.OptCat='EmployeeSSProblem' And A.Problem=G.OptCode ");
            SQL.AppendLine("Left Join TblOption H On H.OptCat='EmploymentStatus' And B.EmploymentStatus=H.OptCode ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By D.DeptName, A.EmpCode, B.EmpName, A.EmpInd Desc;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "EmpInd",

                           //1-5
                           "EmpCode", "PName", "EmpCodeOld", "PosName", "DeptName", 
                           
                           //6-10
                           "JoinDt", "ResignDt", "IDNo", "BirthDt", "Age", 
                           
                           //11-15
                           "SSCode", "SSName", "CardNo", "StartDt", "EndDt",  
                           
                           //16-20
                           "ProblemDesc", "Remark", "FamilyStatus", "Salary", "EmployerPerc", 
                           
                           //21-25
                           "EmployerAmt", "EmployeePerc", "EmployeeAmt", "TotalPerc", "TotalAmt",
 
                           //26-27
                           "PayrunCode", "EmploymentStatusDesc"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        SeqNo += 1;
                        Grd.Cells[Row, 0].Value = SeqNo;
                        EmpInd = Sm.DrStr(dr, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        if (EmpInd == "Y")
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        }
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        if (EmpInd == "Y")
                            Grd.Cells[Row, 19].Value = "Peserta";
                        else
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 20, 21, 22, 23, 24, 25, 26 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mSSForRetiring = Sm.GetParameter("SSForRetiring");
            if (mSSForRetiring.Length == 0) mSSForRetiring = "XXX";
            mSSRetiredMaxAge = Sm.GetParameterDec("SSRetiredMaxAge");
            mSSSalaryInd = Sm.GetParameter("SSSalaryInd");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mSSAllowance = Sm.GetParameter("SSAllowance");
            mMaxLimitSalaryForSS = Sm.GetParameterDec("MaxLimitSalaryForSS");
            mSSProgramForHealth = Sm.GetParameter("SSProgramForHealth");
            mSSProgramForEmployment = Sm.GetParameter("SSProgramForEmployment");
            mSSProgramForPension = Sm.GetParameter("SSProgramForPension");
            mIsEmpSSBasedOnEntity = Sm.GetParameterBoo("IsEmpSSBasedOnEntity");
            mIsPayrollAmtRounded = Sm.GetParameterBoo("IsPayrollAmtRounded");
            mPayrollAmtRoundedType = Sm.GetParameter("PayrollAmtRoundedType");
            mIsEmpSSListExclPassAwalEmp = Sm.GetParameterBoo("IsEmpSSListExclPassAwalEmp");
        }

        private void ShowSSPInfo()
        {
            try
            {
                int SeqNo = 0;
                decimal Salary = 0m, Employer = 0m, Employee = 0m, Total = 0m;

                if (mSSSalaryInd == "1")
                    Salary = Sm.GetParameterDec("SSSal");
                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.RecType, T.EmpCode, T.EmpName, T.EmpCodeOld, T.PosName, T.DeptName, ");
                SQL.AppendLine("T.JoinDt, T.ResignDt, T.IDNo, T.BirthDt, ");
                SQL.AppendLine("T.Age, T.SSCode, T.SSName, T.CardNo, T.StartDt, T.EndDt, ");
                SQL.AppendLine("T.Problem, ProblemDesc, T.Remark, T.EmploymentStatusDesc, ");
                SQL.AppendLine("T.Status, T.Salary, ");
                SQL.AppendLine("T.EmployerPerc,  ");
                SQL.AppendLine("T.EmployeePerc,  ");
                SQL.AppendLine("T.TotalPerc, ");
                if (mIsPayrollAmtRounded)
                {
                    if (mPayrollAmtRoundedType == "1")
                    {
                        SQL.AppendLine("Floor(T.EmployerAmt) As EmployerAmt, Floor(T.EmployeeAmt) As EmployeeAmt, Floor(T.TotalAmt) As TotalAmt");
                    }
                    else
                    {
                        SQL.AppendLine("Round(T.EmployerAmt, 0) As EmployerAmt, Round(T.EmployeeAmt, 0) As EmployeeAmt, Round(T.TotalAmt, 0) As TotalAmt");
                    }
                }
                else
                {
                    SQL.AppendLine("T.EmployerAmt, T.EmployeeAmt, T.TotalAmt ");
                }
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select '1' As RecType, A.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, E.DeptName, ");
                SQL.AppendLine("C.JoinDt, C.ResignDt, C.IDNumber As IDNo, C.BirthDt, ");
                SQL.AppendLine("Case When C.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(C.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d')) End As Age, ");
                SQL.AppendLine("A.SSCode, B.SSName, A.CardNo, A.StartDt, A.EndDt, A.Problem, F.OptDesc As ProblemDesc, A.Remark, H.OptDesc As EmploymentStatusDesc, ");
                SQL.AppendLine("'Peserta' As Status, ");

                if (mSSSalaryInd == "1")
                {
                    if (Sm.CompareStr(Sm.GetLue(LueSSPCode), mSSProgramForHealth))
                    {
                        SQL.AppendLine("Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End As Salary, ");
                        SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As EmployerAmt, ");
                        SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As EmployeeAmt, ");
                        SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As TotalAmt ");
                    }
                    else
                    {
                        //if (Sm.CompareStr(Sm.GetLue(LueSSPCode), mSSProgramForEmployment))
                        if (IsSSPCodeValid(Sm.GetLue(LueSSPCode), mSSProgramForEmployment))
                        {
                            SQL.AppendLine("Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End As Salary, ");
                            SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As EmployerAmt, ");
                            SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As EmployeeAmt, ");
                            SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*Case When IfNull(G.EmploymentSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.EmploymentSSSalary, 0) End  As TotalAmt ");
                        }
                        else
                        {
                            SQL.AppendLine("@Salary As Salary, ");
                            SQL.AppendLine("B.EmployerPerc, B.EmployerPerc/100*@Salary As EmployerAmt, ");
                            SQL.AppendLine("B.EmployeePerc, B.EmployeePerc/100*@Salary As EmployeeAmt, ");
                            SQL.AppendLine("B.TotalPerc, B.TotalPerc/100*@Salary As TotalAmt ");
                        }
                    }
                }
                if (mSSSalaryInd == "2" || mSSSalaryInd == "3")
                {
                    SQL.AppendLine("Case When IfNull(B.SalaryMaxLimit, 0.00)=0.00 Then ");
                    SQL.AppendLine("    Case When IfNull(B.SalaryMinLimit, 0.00)=0.00 Then ");
                    SQL.AppendLine("        IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("Else ");
                    SQL.AppendLine("    Case When IfNull(G.Salary, 0)>B.SalaryMaxLimit Then ");
                    SQL.AppendLine("        B.SalaryMaxLimit ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("End As Salary, ");

                    SQL.AppendLine("B.EmployerPerc, ");

                    SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                    SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                    SQL.AppendLine("        B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("Else ");
                    SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                    SQL.AppendLine("        B.EmployerPerc*0.01*B.SalaryMaxLimit ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("End As EmployerAmt, ");

                    SQL.AppendLine("B.EmployeePerc, ");

                    SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                    SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                    SQL.AppendLine("        B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("Else ");
                    SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                    SQL.AppendLine("        B.EmployeePerc*0.01*B.SalaryMaxLimit ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("End As EmployeeAmt, ");

                    SQL.AppendLine("B.TotalPerc, ");

                    SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                    SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("        Floor(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                        else
                            SQL.AppendLine("        Round(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                    }
                    else
                        SQL.AppendLine("        B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("            Floor(B.EmployerPerc*0.01*B.SalaryMinLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                        else
                            SQL.AppendLine("            Round(B.EmployerPerc*0.01*B.SalaryMinLimit) + Round(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                    }
                    else
                        SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("            Floor(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                        else
                            SQL.AppendLine("            Round(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                    }
                    else
                        SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("Else ");
                    SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("        Floor(B.EmployerPerc*0.01*B.SalaryMaxLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMaxLimit) ");
                        else
                            SQL.AppendLine("        Round(B.EmployerPerc*0.01*B.SalaryMaxLimit) + Round(B.EmployeePerc*0.01*B.SalaryMaxLimit) ");
                    }
                    else
                        SQL.AppendLine("        B.TotalPerc*0.01*B.SalaryMaxLimit ");
                    SQL.AppendLine("    Else ");
                    SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("            Floor(B.EmployerPerc*0.01*B.SalaryMinLimit) + Floor(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                        else
                            SQL.AppendLine("            Round(B.EmployerPerc*0.01*B.SalaryMinLimit) + Round(B.EmployeePerc*0.01*B.SalaryMinLimit) ");
                    }
                    else
                        SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                    SQL.AppendLine("        Else ");
                    if (mIsPayrollAmtRounded)
                    {
                        if (mPayrollAmtRoundedType == "1")
                            SQL.AppendLine("            Floor(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Floor(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                        else
                            SQL.AppendLine("            Round(B.EmployerPerc*0.01*IfNull(G.Salary, 0.00)) + Round(B.EmployeePerc*0.01*IfNull(G.Salary, 0.00)) ");
                    }
                    else
                        SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                    SQL.AppendLine("        End ");
                    SQL.AppendLine("    End ");
                    SQL.AppendLine("End As TotalAmt ");
                }
                
                SQL.AppendLine("From TblEmployeeSS A ");
                SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode And B.SSPCode=@SSPCode ");
                SQL.AppendLine("Inner Join TblEmployee C ");
                SQL.AppendLine("    On A.EmpCode=C.EmpCode ");
                SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>Left(@CutOffDt, 8))) ");

                SQL.AppendLine("And C.EmpCode Not In (");
                SQL.AppendLine("    Select T2.EmpCode ");
                SQL.AppendLine("    From TblEmpSSListHdr T1 ");
                SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    Where T1.CancelInd = 'N' ");
                SQL.AppendLine("    And T1.Yr=@Yr ");
                SQL.AppendLine("    And T1.Mth=@Mth ");
                SQL.AppendLine("    And T1.SSPCode=@SSPCode ");
                SQL.AppendLine("    ) ");

                if (mIsEmpSSListExclPassAwalEmp)
                {
                    SQL.AppendLine("And C.EmpCode Not In ( ");
                    SQL.AppendLine("    Select Distinct A.EmpCode  ");
                    SQL.AppendLine("    From TblPPS A, TblParameter B ");
                    SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                    SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                    SQL.AppendLine("    And A.CancelInd='N' ");
                    SQL.AppendLine("    And A.Status='A'  ");
                    SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                    SQL.AppendLine("    ) ");
                }

                if (ChkEmploymentStatus.Checked)
                    SQL.AppendLine("        And C.EmploymentStatus=@EmploymentStatus ");

                if (Sm.GetLue(LueSiteCode).Length > 0)
                    SQL.AppendLine("    And C.SiteCode=@SiteCode ");

                if (mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And C.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(C.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And C.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
                SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblOption F On F.OptCat='EmployeeSSProblem' And A.Problem=F.OptCode ");

                if (mSSSalaryInd == "1")
                {
                    SQL.AppendLine("Left Join TblGradeLevelHdr G On C.GrdLvlCode=G.GrdLvlCode ");
                }

                if (mSSSalaryInd == "2")
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.EmpCode, T1.StartDt, IfNull(T1.Amt, 0.00)+IfNull(T3.Amt, 0) As Salary ");
	                SQL.AppendLine("    From TblEmployeeSalary T1 ");
	                SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
		            SQL.AppendLine("        From TblEmployeeSalary ");
		            SQL.AppendLine("        Where EmpCode In (");
                    SQL.AppendLine("            Select EmpCode From TblEmployee ");
                    SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                    SQL.AppendLine("        ) And StartDt<Left(@CutOffDt, 8) ");
		            SQL.AppendLine("        Group By EmpCode ");
	                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select EmpCode, Sum(Amt) As Amt ");
                    SQL.AppendLine("        From TblEmployeeAllowanceDeduction ");
                    SQL.AppendLine("        Where Position(ADCode In @SSAllowance) ");

                    SQL.AppendLine("        And ( ");
                    SQL.AppendLine("        (StartDt Is Null And EndDt Is Null) Or ");
                    SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Null And StartDt<=@CutOffDt) Or ");
                    SQL.AppendLine("        (StartDt Is Null And EndDt Is Not Null And @CutOffDt<=EndDt) Or ");
                    SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@CutOffDt And @CutOffDt<=EndDt) ");
                    SQL.AppendLine("        ) ");


                    SQL.AppendLine("        Group By EmpCode ");
                    SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode ");
                    SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
                }

                if (mSSSalaryInd == "3")
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.EmpCode, T1.Amt As Salary ");
                    SQL.AppendLine("    From TblEmployeeSalarySS T1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                    SQL.AppendLine("        From TblEmployeeSalarySS ");
                    SQL.AppendLine("        Where EmpCode In (");
                    SQL.AppendLine("            Select EmpCode From TblEmployee ");
                    SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        And Left(@CutOffDt, 8)>=StartDt ");
                    SQL.AppendLine("        And Left(@CutOffDt, 8)<=IfNull(EndDt, '99990101') ");
                    SQL.AppendLine("        And SSPCode=@SSPCode ");
                    SQL.AppendLine("        Group By EmpCode ");
                    SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                    SQL.AppendLine("    Where T1.SSPCode=@SSPCode ");
                    SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
                }
                SQL.AppendLine("Left Join TblOption H On H.OptCat='EmploymentStatus' And C.EmploymentStatus=H.OptCode ");
                if (mIsEmpSSBasedOnEntity)
                {
                    SQL.AppendLine("Where A.EntCode Is Not Null ");
                    SQL.AppendLine("And A.EntCode=@EntCode ");
                }

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select '2' As RecType, A.EmpCode, A.FamilyName As EmpName, Null As EmpCodeOld, Null As PosName, E.DeptName As DeptName, ");
                SQL.AppendLine("Null As JoinDt, Null As ResignDt, A.IDNo, A.BirthDt, ");
                SQL.AppendLine("Case When A.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(A.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d')) End As Age, ");
                SQL.AppendLine("A.SSCode, B.SSName, A.CardNo, A.StartDt, A.EndDt, A.Problem, F.OptDesc As ProblemDesc, A.Remark, G.OptDesc As EmploymentStatusDesc, ");
                SQL.AppendLine("C.OptDesc As Status, 0.00 As Salary, ");
                SQL.AppendLine("0 As EmployerPerc, 0.00 As EmployerAmt, ");
                SQL.AppendLine("0 As EmployeePerc, 0.00 As EmployeeAmt, ");
                SQL.AppendLine("0 As TotalPerc, 0.00 As TotalAmt ");
                SQL.AppendLine("From TblEmployeeFamilySS A ");
                SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode And B.SSPCode=@SSPCode ");
                SQL.AppendLine("Left Join TblOption C On C.OptCat='FamilyStatus' And A.Status=C.OptCode ");
                SQL.AppendLine("Inner Join TblEmployee D ");
                SQL.AppendLine("    On A.EmpCode=D.EmpCode ");
                SQL.AppendLine("    And (D.ResignDt Is Null Or (D.ResignDt Is Not Null And D.ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("    And D.EmpCode Not In (");
                SQL.AppendLine("        Select T2.EmpCode ");
                SQL.AppendLine("        From TblEmpSSListHdr T1 ");
                SQL.AppendLine("        Inner Join TblEmpSSListDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        Where T1.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.Yr=@Yr ");
                SQL.AppendLine("        And T1.Mth=@Mth ");
                SQL.AppendLine("        And T1.SSPCode=@SSPCode ");
                SQL.AppendLine("    ) ");

                if (mIsEmpSSListExclPassAwalEmp)
                {
                    SQL.AppendLine("And D.EmpCode Not In ( ");
                    SQL.AppendLine("    Select Distinct A.EmpCode  ");
                    SQL.AppendLine("    From TblPPS A, TblParameter B ");
                    SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                    SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                    SQL.AppendLine("    And A.CancelInd='N' "); 
                    SQL.AppendLine("    And A.Status='A'  ");
                    SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                    SQL.AppendLine("    ) ");
                }

                if (ChkEmploymentStatus.Checked)
                    SQL.AppendLine("        And D.EmploymentStatus=@EmploymentStatus ");
                if (Sm.GetLue(LueSiteCode).Length > 0)
                    SQL.AppendLine("    And D.SiteCode=@SiteCode ");
                if (mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And D.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(D.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And D.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=D.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Inner Join TblDepartment E On D.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblOption F On F.OptCat='EmployeeSSProblem' And A.Problem=F.OptCode ");
                SQL.AppendLine("Left Join TblOption G On G.OptCat='EmploymentStatus' And D.EmploymentStatus=G.OptCode ");

                if (mIsEmpSSBasedOnEntity)
                {
                    SQL.AppendLine("Where A.EntCode Is Not Null ");
                    SQL.AppendLine("And A.EntCode=@EntCode ");
                }

                SQL.AppendLine(") T ");
                //SQL.AppendLine("Where (T.SSCode<>@SSForRetiring ");
                SQL.AppendLine("Where (Not Find_in_set(T.SSCode, IfNull(( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
                SQL.AppendLine("    ), '')) ");

                SQL.AppendLine("Or ( ");
                //SQL.AppendLine("    T.SSCode=@SSForRetiring ");
                SQL.AppendLine("    Find_in_set(T.SSCode, IfNull(( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='SSForRetiring' And ParValue is Not Null ");
                SQL.AppendLine("    ), '')) ");
                SQL.AppendLine("    And T.BirthDt Is Not Null ");
                SQL.AppendLine("    And TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))<@SSRetiredMaxAge ");
                SQL.AppendLine("    And Not ( ");
                SQL.AppendLine("        TIMESTAMPDIFF(YEAR, STR_TO_DATE(T.BirthDt, '%Y%m%d'), STR_TO_DATE(@DtForAge, '%Y%m%d'))=@SSRetiredMaxAge-1 ");
                SQL.AppendLine("        And (Substring(T.BirthDt, 5, 2)=@RetiringMth) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
                SQL.AppendLine("And T.EndDt Is Null ");
                SQL.AppendLine("And ");
                SQL.AppendLine("(");
                SQL.AppendLine("    T.StartDt Is Null ");
                SQL.AppendLine("    Or ( T.StartDt Is Not Null And T.StartDt<=Left(@CutOffDt, 8))");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.DeptName, T.EmpCode, T.EmpName, T.RecType;");

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                if (mSSSalaryInd == "1")
                    Sm.CmParam<Decimal>(ref cm, "@Salary", Salary);
                
                if (mSSSalaryInd == "2")
                    Sm.CmParam<String>(ref cm, "@SSAllowance", mSSAllowance);
                if (ChkEmploymentStatus.Checked)
                    Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
                Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@SSForRetiring", mSSForRetiring);
                Sm.CmParam<Decimal>(ref cm, "@SSRetiredMaxAge", mSSRetiredMaxAge);
                Sm.CmParam<Decimal>(ref cm, "@MaxLimitSalaryForSS", mMaxLimitSalaryForSS);
                Sm.CmParam<String>(ref cm, "@DtForAge", Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01");
                Sm.CmParamDt(ref cm, "@CutOffDt", Sm.GetDte(DteCutOffDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@RetiringMth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "RecType",  

                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName",  
                    
                    //6-10
                    "JoinDt", "ResignDt", "IDNo", "BirthDt", "Age", 
                    
                    //11-15
                    "SSCode", "SSName", "CardNo", "StartDt", "EndDt",  
                    
                    //16-20
                    "ProblemDesc", "Remark", "Status", "Salary", "EmployerPerc", 
                    
                    //21-25
                    "EmployerAmt", "EmployeePerc", "EmployeeAmt", "TotalPerc", "TotalAmt",

                    //26
                    "EmploymentStatusDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    { 
                        SeqNo += 1;
                        Grd.Cells[Row, 0].Value = SeqNo;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        if (Sm.DrStr(dr, 0) == "1")
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        if (Sm.DrStr(dr, 0) == "1")
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                            Employer += Sm.GetGrdDec(Grd, Row, 22);
                            Employee += Sm.GetGrdDec(Grd, Row, 24);
                            Total += Sm.GetGrdDec(Grd, Row, 26);
                        }
                        else
                        {
                            for (int Col = 20; Col <= 26; Col++)
                                Grd.Cells[Row, Col].Value = 0m;
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                    }, true, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 20, 21, 22, 23, 24, 25, 26 });
                Sm.FocusGrd(Grd1, 0, 0);

                TxtEmployer.EditValue = Sm.FormatNum(Employer, 0);
                TxtEmployee.EditValue = Sm.FormatNum(Employee, 0);
                TxtTotal.EditValue = Sm.FormatNum(Total, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsSSPCodeValid(string SSPCode1, string SSPCode2)
        {
            foreach (string x in SSPCode2.Trim().Split(new string[] { "," }, StringSplitOptions.None))
                if (x == SSPCode1.Trim()) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
                Sm.FilterLueSetCheckEdit(this, sender);
                ClearData2();
            }
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FilterSetLookUpEdit(this, sender, "Employment status");
                ClearData2();
            }
        }

        private void LueSSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSSPCode, new Sm.RefreshLue1(Sl.SetLueSSPCode));
                ClearData2();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
                ClearData2();
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
                ClearData2();
            }
        }

        private void DteCutOffDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearData2();
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ClearData2();

            if (Sm.IsLueEmpty(LueSSPCode, "Social security program")) return;
            if (mIsSiteMandatory && !mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueSiteCode, "Site")) return;    
            if (mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueEntCode, "Entity")) return;
            if (Sm.IsDteEmpty(DteCutOffDt, "Cut-off date")) return;
                
            ShowSSPInfo();
        }

        #endregion

        #region Grid Event

        private void Grd1_BeforeContentsSorted(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count > 1)
                Grd1.Rows.RemoveAt(Grd1.Rows.Count - 1);
        }

        private void Grd1_AfterContentsSorted(object sender, EventArgs e)
        {
            Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 19, 20, 21, 22, 23, 24, 25, 26 });
        }

        #endregion

        #endregion
    }
}
