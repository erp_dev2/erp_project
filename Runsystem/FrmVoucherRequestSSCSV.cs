﻿#region Update
/*
    23/05/2017 [TKG] proses eksport data ke excel untuk KMI.
    03/08/2022 [BRI/IOK] tambah format eksport berdasarkan param IsExportVoucherRequestToCSVUseBank
    15/08/2022 [BRI/IOK] perubahan format untuk bank mandiri
    16/08/2022 [BRI/IOK] merubah source remark
*/
#endregion

#region Namespace

using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;



#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestSSCSV : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mBankAcNo = string.Empty;
        private string mReportTitle1 = string.Empty;
        private decimal mVoucherRequestSSMaxNoOfRecord = 0;
        private bool mIsNotFilterByAuthorization = false,
            mIsExportVoucherRequestToCSVUseBank = false;
        private string mSalaryInd = string.Empty,
            mBankAccountForExportVRToCSV = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmVoucherRequestSSCSV(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                LueBankCode.Visible = false;
                LblBank.Visible = mIsExportVoucherRequestToCSVUseBank;
                LueBank.Visible = mIsExportVoucherRequestToCSVUseBank;
                SetLueBank(ref LueBank);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mReportTitle1 = Sm.GetParameter("ReportTitle1");
            mVoucherRequestSSMaxNoOfRecord = Sm.GetParameterDec("VoucherRequestSSMaxNoOfRecord");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsExportVoucherRequestToCSVUseBank = Sm.GetParameterBoo("IsExportVoucherRequestToCSVUseBank");
            mBankAccountForExportVRToCSV = Sm.GetParameter("BankAccountForExportVRToCSV");
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.BankName, B.BankAcNo, A.EmpCode, B.EmpName, D.DeptName, 'Period' As Info, E.CurCode, ");
            SQL.AppendLine("Concat(DATE_FORMAT(STR_TO_DATE(G.StartDt, '%Y%m%d'), '%d/%m/%Y'), '-', DATE_FORMAT(STR_TO_DATE(G.EndDt, '%Y%m%d'), '%d/%m/%Y')) As Periode, ");
            SQL.AppendLine("A.Amt As Total, F.Remark ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Inner Join TblBank C On B.BankCode=C.BankCode ");
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestPayrollHdr E On A.VoucherRequestPayrollDocNo=E.DocNo And E.DocNo=@DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestPayrollDtl F On E.DocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblPayrun G On A.PayrunCode=G.PayrunCode And G.VoucherRequestPayrollInd In ('P', 'F') ");
            SQL.AppendLine("Where A.VoucherRequestPayrollDocNo Is Not Null ");
            SQL.AppendLine("And A.VoucherRequestPayrollDocNo=@DocNo ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By B.EmpName; ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Bank Name",
                        "",
                        "Bank Account",
                        "Employee Code",
                        "Employee",

                        //6-10
                        "Department",
                        "Currency",
                        "Periode",
                        "Date",
                        "Amount",

                        //11
                        "Remark",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 20, 150, 100, 200, 
                        
                        //6-10
                        150, 80, 180, 100, 150,

                        //11
                        0,
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 11 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtPayrunCode, TxtPayrunName, TxtDeptCode, 
                        LueBankCode, DteStartDt, DteEndDt, LueBank
                    }, true);
                    BtnSave.Visible = false;
                    BtnEdit.Visible = false;
                    BtnFind.Visible = false;
                    BtnPrint.Visible = false;
                    BtnVoucher.Enabled = false;
                    BtnPayrunCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueBankCode, DteDocDt, LueBank
                    }, false);
                    BtnCancel.Visible = true;
                    BtnPrint.Visible = true;
                    BtnPrint.Enabled = true;
                    BtnPrint.Text = " CSV";
                    BtnVoucher.Focus();
                    BtnVoucher.Enabled = true;
                    BtnPayrunCode.Enabled = true;
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mBankAcNo = string.Empty;
            ClearGrd();
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtPayrunCode, TxtPayrunName, TxtDeptCode, DteStartDt, DteEndDt,LueBankCode, LueBank
            });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if ((mIsExportVoucherRequestToCSVUseBank && Sm.IsLueEmpty(LueBank, "Bank")) || Sm.StdMsgYN("Process", "") == DialogResult.No) return;
                var Bank = Sm.GetLue(LueBank);
                if (mSalaryInd == "1" && Bank == "008") PrintDataMandiri();
                if (mSalaryInd == "1" && Bank != "008") PrintData();
                if (mSalaryInd == "2") PrintData2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            string CurrentDateTime = Sm.ServerCurrentDateTime();
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex) && !Sm.IsTxtEmpty(TxtPayrunCode, "Payrun Code", false))
            {
                LueRequestEdit(Grd1, LueBankCode, ref fCell, ref fAccept, e);
                SetLueBankAcNo(ref LueBankCode);
            }
        }

        #endregion

        internal void ShowData()
        {
            try
            {
                SetSQL();

                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL,
                        new string[]
                        {
                            //0
                            "BankName", 

                            //1-5
                            "BankAcNo", "EmpCode", "EmpName", "Info", "CurCode", 
                            
                            //6-8
                            "Periode", "Total", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Grd1.Cells[Row, 9].Value = Sm.GetDte(DteDocDt).Substring(0, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            if (Row > 0) Grd1.Rows[Row].ReadOnly = iGBool.True;
                        }, true, false, false, false
                    );
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private static void SetLueBank(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select A.BankCode As Col1, A.BankName  As Col2 " +
                "From TblBank A " +
                "Inner Join TblParameter B On B.ParCode='BankAccountForExportVRToCSV' And Find_In_Set(A.BankCode,B.ParValue) " +
                "Order By A.BankCode;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void PrintData()
        {
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            sf.FileName = "VoucherRequest";
            var FileName = string.Empty;
            string Dt = Sm.GetDte(DteDocDt).Substring(0, 8);
            int FileNo = 1;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                var lCSVData = new List<CSVData>();
                int RowCount = 0;
                decimal Total = 0m;
                string Period = Sm.GetGrdStr(Grd1, 0, 8);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    RowCount += 1;
                    Total += Sm.GetGrdDec(Grd1, Row, 10);
                    lCSVData.Add(new CSVData()
                    {
                        BankAcNo = Sm.GetGrdStr(Grd1, Row, 2),
                        EmpName = Sm.GetGrdStr(Grd1, Row, 5),
                        CurCode = Sm.GetGrdStr(Grd1, Row, 7),
                        Total = Sm.GetGrdDec(Grd1, Row, 10),
                        Period = Sm.GetGrdStr(Grd1, Row, 8),
                        NumberOfRow = string.Empty,
                        DocDt = Dt
                    });
                    if (RowCount % mVoucherRequestSSMaxNoOfRecord == 0)
                    {
                        if (sf.FileName.IndexOf(".csv") > 0)
                            FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                        else
                            FileName = sf.FileName + FileNo.ToString();
                        CreateCSVFile(ref lCSVData, FileName, Total, Period, Dt);
                        FileNo += 1;
                        Total = 0m;
                    }
                }
                if (lCSVData.Count > 0)
                {
                    if (sf.FileName.IndexOf(".csv") > 0)
                        FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                    else
                        FileName = sf.FileName + FileNo.ToString();
                    CreateCSVFile(ref lCSVData, FileName, Total, Period, Dt);
                }
            }
        }

        private void CreateCSVFile(ref List<CSVData> lCSVData, string FileName, decimal Total, string Period, string Dt)
        {
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));
            sw.Write(
                ((mBankAcNo.Replace(" ", string.Empty)) + " ").Trim() + "," + mReportTitle1 + ",IDR," + (Total.ToString()).Replace( ",", ".") + "," +
                Period + "," + lCSVData.Count + "," + Dt
                );

            foreach (var x in lCSVData)
            {
                sw.Write(sw.NewLine);
                sw.Write(
                    x.BankAcNo + "," + x.EmpName + "," + x.CurCode + "," + (x.Total.ToString()).Replace(",", ".") + "," +
                    Period + ",," + Dt
                );

            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
            lCSVData.Clear();
        }

        private void PrintData2()
        {
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            sf.FileName = "VoucherRequest";
            var FileName = string.Empty;
            string Dt = Sm.GetDte(DteDocDt).Substring(0, 8);
            int FileNo = 1;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                var lCSVData = new List<CSVData2>();
                int RowCount = 0;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    RowCount += 1;
                    lCSVData.Add(new CSVData2()
                    {
                        BankAcNo = String.Concat("=" + "\"\"" + "&" + Sm.GetGrdStr(Grd1, Row, 2)),
                        EmpName = Sm.GetGrdStr(Grd1, Row, 5),
                        Total = Sm.GetGrdDec(Grd1, Row, 10),
                        NumberOfRow = RowCount.ToString()
                    });
                    if (RowCount % mVoucherRequestSSMaxNoOfRecord == 0)
                    {
                        if (sf.FileName.IndexOf(".csv") > 0)
                            FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                        else
                            FileName = sf.FileName + FileNo.ToString();
                        CreateCSVFile2(ref lCSVData, FileName);
                        FileNo += 1;
                    }
                }
                if (lCSVData.Count > 0)
                {
                    if (sf.FileName.IndexOf(".csv") > 0)
                        FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                    else
                        FileName = sf.FileName + FileNo.ToString();
                    CreateCSVFile2(ref lCSVData, FileName);
                }
            }
        }

        private void CreateCSVFile2(ref List<CSVData2> lCSVData, string FileName)
        {
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));

            sw.Write("BANK,REKENING KARYAWAN,PLUS,NOMINAL,CD,NO,NAMA,KETERANGAN,REKENING PERUSAHAAN");
   
            foreach (var x in lCSVData)
            {
                sw.Write(sw.NewLine);
                sw.Write(
                    "," + x.BankAcNo + ",+," + 
                    (x.Total.ToString()).Replace(",", ".") + ",C," +
                    x.NumberOfRow + "," + x.EmpName +",,"
                );
            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
            lCSVData.Clear();
        }

        private void PrintDataMandiri()
        {
            var sf = new SaveFileDialog();
            sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            sf.FileName = "VoucherRequest";
            var FileName = string.Empty;
            string Dt = Sm.GetDte(DteDocDt).Substring(0, 8);
            int FileNo = 1;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                var lCSVData = new List<CSVDataMandiri>();
                int RowCount = 0;
                decimal Total = 0m;
                string Period = Sm.GetGrdStr(Grd1, 0, 8);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    RowCount += 1;
                    Total += Sm.GetGrdDec(Grd1, Row, 10);
                    lCSVData.Add(new CSVDataMandiri()
                    {
                        BankAcNo = Sm.GetGrdStr(Grd1, Row, 2),
                        EmpName = Sm.GetGrdStr(Grd1, Row, 5),
                        CurCode = Sm.GetGrdStr(Grd1, Row, 7),
                        Total = Sm.GetGrdDec(Grd1, Row, 10),
                        Remark = Sm.GetGrdStr(Grd1, Row, 11),
                        Bank = Sm.GetValue("Select BankShNm From TblBank Where BankCode=@Param;",Sm.GetLue(LueBank)),
                    });
                    if (RowCount % mVoucherRequestSSMaxNoOfRecord == 0)
                    {
                        if (sf.FileName.IndexOf(".csv") > 0)
                            FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                        else
                            FileName = sf.FileName + FileNo.ToString();
                        CreateCSVFileMandiri(ref lCSVData, FileName, Total, Period, Dt);
                        FileNo += 1;
                    }
                }
                if (lCSVData.Count > 0)
                {
                    if (sf.FileName.IndexOf(".csv") > 0)
                        FileName = sf.FileName.Replace(".csv", FileNo.ToString() + ".csv");
                    else
                        FileName = sf.FileName + FileNo.ToString();
                    CreateCSVFileMandiri(ref lCSVData, FileName, Total, Period, Dt);
                }
            }
        }

        private void CreateCSVFileMandiri(ref List<CSVDataMandiri> lCSVData, string FileName, decimal Total, string Period, string Dt)
        {
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));
            sw.Write(
                "P," + Dt + "," + mBankAcNo.Replace("-", string.Empty) + "," + lCSVData.Count + "," + (Total.ToString()).Replace(",", ".") + "," +
                ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"
                );

            foreach (var x in lCSVData)
            {
                sw.Write(sw.NewLine);
                sw.Write(
                    x.BankAcNo + "," + x.EmpName + ",,,," + x.CurCode + "," + (x.Total.ToString()).Replace(",", ".") + "," +
                    x.Remark + ",,IBU,," + x.Bank + ",Kutoarjo,,,,N,,,,,,,,,,,,,,,,,,,,,,OUR,1,E,,,"
                );

            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
            lCSVData.Clear();
        }


        #endregion

        #endregion

        #region Additional Method

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueBankAcNo(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select A.BankAcNo As Col1, " +
                "Concat( " +
                "Case When B.BankName Is Not Null Then Concat(B.BankName, ' : ') Else '' End, " +
                "Case When A.BankAcNo Is Not Null  " +
                "Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') " +
                "Else IfNull(A.BankAcNm, '') End " +
                ") As Col2 " +
                "From TblBankAccount A " +
                "Left Join TblBank B On A.BankCode=B.BankCode " +
                "Order By A.Sequence",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestSSCSVDlg(this));
        }

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            var f = new FrmPayrun(mMenuCode);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mPayrunCode = TxtPayrunCode.Text;
            f.ShowDialog();
        }

        private void LueBank_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBank, new Sm.RefreshLue1(SetLueBank));
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(SetLueBankAcNo));
        }

        private void LueBankCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBankCode_Leave(object sender, EventArgs e)
        {
            if (LueBankCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueBankCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueBankCode);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueBankCode.GetColumnValue("Col2");
                }
                LueBankCode.Visible = false;
            }
        }

        #endregion

        #region Class

        private class CSVData
        {
            public string BankAcNo { get; set; }
            public string EmpName { get; set; }
            public string CurCode { get; set; }
            public decimal Total { get; set; }
            public string Period { get; set; }
            public string NumberOfRow { get; set; }
            public string DocDt { get; set; }
        }

        private class CSVData2
        {
            public string BankAcNo { get; set; }
            public string EmpName { get; set; }
            public decimal Total { get; set; }
            public string NumberOfRow { get; set; }
        }

        private class CSVDataMandiri
        {
            public string BankAcNo { get; set; }
            public string EmpName { get; set; }
            public string CurCode { get; set; }
            public decimal Total { get; set; }
            public string Remark { get; set; }
            public string Bank { get; set; }
        }

        #endregion
    }
}
