﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvProductionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvProduction mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty;
        private bool mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmRecvProductionDlg(FrmRecvProduction FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.DNo, E.DocName, B.ItCode, C.ItName, B.BatchNo, ");
            SQL.AppendLine("(B.Qty-IfNull(RecvQty, 0)) As Qty, (B.Qty2-IfNull(RecvQty2, 0)) As Qty2, ");
            SQL.AppendLine("C.PlanningUomCode, C.PlanningUomCode2, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3 ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And IfNull(B.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T1.QtyPlanning) As RecvQty, Sum(T1.QtyPlanning2) As RecvQty2 ");
            SQL.AppendLine("    From TblRecvProductionDtl T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T2 ");
            SQL.AppendLine("        On T1.ShopFloorControlDocNo=T2.DocNo ");
            SQL.AppendLine("        And T1.ShopFloorControlDNo=T2.DNo ");
            SQL.AppendLine("        And IfNull(T2.ProcessInd, 'O')<>'F' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") D On B.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr E On A.WorkCenterDocNo=E.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.WhsCode=@WhsCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#", 
                        "", 
                        "DNo",
                        "Date",
                        
                        //6-10
                        "Work Center",
                        "Item's Code", 
                        "", 
                        "Item's Name", 
                        "Batch#", 
                        
                        //11-15
                        "Outstanding" + Environment.NewLine + "Quantity",
                        "Planning" + Environment.NewLine + "UoM",
                        "Outstanding" + Environment.NewLine + "Quantity 2",
                        "Planning" + Environment.NewLine + "Uom 2",
                        "UoM",

                        //16-17
                        "Uom 2",
                        "Uom 3"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 130, 20, 0, 80, 
                        
                        //6-10
                        200, 80, 20, 250, 250,  
                        
                        //11-15
                        80, 80, 80, 80, 0, 
                        
                        //16-17
                        0, 0                    
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[]{ 0, 2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7, 8, 13, 14, 15, 16, 17 }, false);
            ShowPlanningUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowPlanningUomCode()
        {
            if (mFrmParent.mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItCodeInternal", "C.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);

                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DNo", "DocDt", "DocName", "ItCode", "ItName", 
                            
                            //6-10
                            "BatchNo", "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", 
                            
                            //11-13
                            "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            mFrmParent.Grd1.BeginUpdate();
            mFrmParent.Grd2.BeginUpdate();
            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 10);
                        
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 14);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 22, 25, 28 });

                        if (Sm.CompareGrdStr(Grd1, Row2, 12, Grd1, Row2, 15))
                        {
                            Sm.CopyGrdValue(
                                mFrmParent.Grd1, Row1, 22,
                                mFrmParent.Grd1, Row1, 17);
                        }

                        if (Sm.CompareGrdStr(Grd1, Row2, 14, Grd1, Row2, 16))
                        {
                            Sm.CopyGrdValue(
                                mFrmParent.Grd1, Row1, 25,
                                mFrmParent.Grd1, Row1, 19);
                        }

                        if (Sm.CompareGrdStr(Grd1, Row2, 14, Grd1, Row2, 17))
                        {
                            Sm.CopyGrdValue(
                                mFrmParent.Grd1, Row1, 28,
                                mFrmParent.Grd1, Row1, 19);
                        }

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 17);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 21, 24, 27 });
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 17, 19, 21, 22, 24, 25, 27, 28 });

                        if (!IsSFCAlreadyChosen(Row))
                        {
                            Row1 = mFrmParent.Grd2.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 0, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 10, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 12);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 15, Grd1, Row2, 14);

                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 9, 13 });

                            mFrmParent.Grd2.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 12, 13, 14 });
                        }

                        mFrmParent.ReComputeSummary();
                    }
                }
            }

            mFrmParent.Grd1.EndUpdate();
            mFrmParent.Grd2.EndUpdate();

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsSFCAlreadyChosen(int Row)
        {
            var Key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 4);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(
                        Key, 
                        Sm.GetGrdStr(mFrmParent.Grd2, Index, 0) +
                        Sm.GetGrdStr(mFrmParent.Grd2, Index, 2)))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmShopFloorControl2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        #endregion

        #endregion
    }
}
