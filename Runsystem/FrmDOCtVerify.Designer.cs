﻿namespace RunSystem
{
    partial class FrmDOCtVerify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDOCtVerify));
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnDOCt2DocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDOCt2DocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDOCt2DocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtWhsCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnJournalDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnJournalDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DteDeliveryDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.DteReceivedDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtLPPBNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCt2DocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteReceivedDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteReceivedDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLPPBNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(669, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtLPPBNo);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.DteReceivedDt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.DteDeliveryDt);
            this.panel2.Controls.Add(this.BtnJournalDocNo2);
            this.panel2.Controls.Add(this.BtnJournalDocNo);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.TxtJournalDocNo2);
            this.panel2.Controls.Add(this.TxtJournalDocNo);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtWhsCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtCtCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.BtnDOCt2DocNo2);
            this.panel2.Controls.Add(this.BtnDOCt2DocNo);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtDOCt2DocNo);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Size = new System.Drawing.Size(669, 289);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 289);
            this.panel3.Size = new System.Drawing.Size(669, 184);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(669, 184);
            this.Grd1.TabIndex = 41;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(123, 9);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(45, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(85, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(123, 30);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(508, 50);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 18;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(33, 54);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 14);
            this.label27.TabIndex = 16;
            this.label27.Text = "Cancel Reason";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(123, 51);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 250;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(377, 20);
            this.MeeCancelReason.TabIndex = 17;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // BtnDOCt2DocNo2
            // 
            this.BtnDOCt2DocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDOCt2DocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDOCt2DocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDOCt2DocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDOCt2DocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDOCt2DocNo2.Appearance.Options.UseFont = true;
            this.BtnDOCt2DocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDOCt2DocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDOCt2DocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDOCt2DocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDOCt2DocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDOCt2DocNo2.Image")));
            this.BtnDOCt2DocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDOCt2DocNo2.Location = new System.Drawing.Point(392, 73);
            this.BtnDOCt2DocNo2.Name = "BtnDOCt2DocNo2";
            this.BtnDOCt2DocNo2.Size = new System.Drawing.Size(17, 17);
            this.BtnDOCt2DocNo2.TabIndex = 22;
            this.BtnDOCt2DocNo2.ToolTip = "Show DO To Customer Based On Delivery Request Information";
            this.BtnDOCt2DocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDOCt2DocNo2.ToolTipTitle = "Run System";
            this.BtnDOCt2DocNo2.Click += new System.EventHandler(this.BtnDOCt2DocNo2_Click);
            // 
            // BtnDOCt2DocNo
            // 
            this.BtnDOCt2DocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDOCt2DocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDOCt2DocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDOCt2DocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDOCt2DocNo.Appearance.Options.UseBackColor = true;
            this.BtnDOCt2DocNo.Appearance.Options.UseFont = true;
            this.BtnDOCt2DocNo.Appearance.Options.UseForeColor = true;
            this.BtnDOCt2DocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDOCt2DocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDOCt2DocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDOCt2DocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDOCt2DocNo.Image")));
            this.BtnDOCt2DocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDOCt2DocNo.Location = new System.Drawing.Point(368, 73);
            this.BtnDOCt2DocNo.Name = "BtnDOCt2DocNo";
            this.BtnDOCt2DocNo.Size = new System.Drawing.Size(17, 17);
            this.BtnDOCt2DocNo.TabIndex = 21;
            this.BtnDOCt2DocNo.ToolTip = "Find DO To Customer Based On Delivery Request Document";
            this.BtnDOCt2DocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDOCt2DocNo.ToolTipTitle = "Run System";
            this.BtnDOCt2DocNo.Click += new System.EventHandler(this.BtnDOCt2DocNo_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(10, 75);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 14);
            this.label17.TabIndex = 19;
            this.label17.Tag = "DO To Customer Based On Request Delivery";
            this.label17.Text = "DO To Customer#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDOCt2DocNo
            // 
            this.TxtDOCt2DocNo.EnterMoveNextControl = true;
            this.TxtDOCt2DocNo.Location = new System.Drawing.Point(123, 72);
            this.TxtDOCt2DocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDOCt2DocNo.Name = "TxtDOCt2DocNo";
            this.TxtDOCt2DocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDOCt2DocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOCt2DocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDOCt2DocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDOCt2DocNo.Properties.MaxLength = 30;
            this.TxtDOCt2DocNo.Properties.ReadOnly = true;
            this.TxtDOCt2DocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDOCt2DocNo.TabIndex = 20;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(123, 177);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 250;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(443, 20);
            this.TxtCtCode.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(59, 181);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 31;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWhsCode
            // 
            this.TxtWhsCode.EnterMoveNextControl = true;
            this.TxtWhsCode.Location = new System.Drawing.Point(123, 198);
            this.TxtWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWhsCode.Name = "TxtWhsCode";
            this.TxtWhsCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWhsCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWhsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWhsCode.Properties.MaxLength = 250;
            this.TxtWhsCode.Properties.ReadOnly = true;
            this.TxtWhsCode.Size = new System.Drawing.Size(443, 20);
            this.TxtWhsCode.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(49, 202);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 14);
            this.label4.TabIndex = 33;
            this.label4.Text = "Warehouse";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(123, 135);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 100;
            this.TxtLocalDocNo.Properties.ReadOnly = true;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtLocalDocNo.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(23, 140);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Local Document";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(123, 219);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(443, 20);
            this.MeeRemark.TabIndex = 36;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(71, 222);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 35;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnJournalDocNo2
            // 
            this.BtnJournalDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo2.Location = new System.Drawing.Point(357, 259);
            this.BtnJournalDocNo2.Name = "BtnJournalDocNo2";
            this.BtnJournalDocNo2.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo2.TabIndex = 41;
            this.BtnJournalDocNo2.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo2.ToolTipTitle = "Run System";
            this.BtnJournalDocNo2.Click += new System.EventHandler(this.BtnJournalDocNo2_Click);
            // 
            // BtnJournalDocNo
            // 
            this.BtnJournalDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnJournalDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnJournalDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnJournalDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnJournalDocNo.Appearance.Options.UseBackColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseFont = true;
            this.BtnJournalDocNo.Appearance.Options.UseForeColor = true;
            this.BtnJournalDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnJournalDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnJournalDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnJournalDocNo.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnJournalDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnJournalDocNo.Location = new System.Drawing.Point(357, 238);
            this.BtnJournalDocNo.Name = "BtnJournalDocNo";
            this.BtnJournalDocNo.Size = new System.Drawing.Size(24, 20);
            this.BtnJournalDocNo.TabIndex = 39;
            this.BtnJournalDocNo.ToolTip = "Show Journal Information";
            this.BtnJournalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnJournalDocNo.ToolTipTitle = "Run System";
            this.BtnJournalDocNo.Click += new System.EventHandler(this.BtnJournalDocNo_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(385, 263);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 14);
            this.label21.TabIndex = 42;
            this.label21.Text = "(Cancel Data)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(124, 261);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo2.TabIndex = 40;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(124, 240);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(229, 20);
            this.TxtJournalDocNo.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(64, 243);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 14);
            this.label22.TabIndex = 37;
            this.label22.Text = "Journal#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(39, 96);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Delivery Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDeliveryDt
            // 
            this.DteDeliveryDt.EditValue = null;
            this.DteDeliveryDt.EnterMoveNextControl = true;
            this.DteDeliveryDt.Location = new System.Drawing.Point(123, 93);
            this.DteDeliveryDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeliveryDt.Name = "DteDeliveryDt";
            this.DteDeliveryDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.Appearance.Options.UseFont = true;
            this.DteDeliveryDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeliveryDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeliveryDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeliveryDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeliveryDt.Size = new System.Drawing.Size(109, 20);
            this.DteDeliveryDt.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(32, 117);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Received Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteReceivedDt
            // 
            this.DteReceivedDt.EditValue = null;
            this.DteReceivedDt.EnterMoveNextControl = true;
            this.DteReceivedDt.Location = new System.Drawing.Point(123, 114);
            this.DteReceivedDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteReceivedDt.Name = "DteReceivedDt";
            this.DteReceivedDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteReceivedDt.Properties.Appearance.Options.UseFont = true;
            this.DteReceivedDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteReceivedDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteReceivedDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteReceivedDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteReceivedDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteReceivedDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteReceivedDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteReceivedDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteReceivedDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteReceivedDt.Size = new System.Drawing.Size(109, 20);
            this.DteReceivedDt.TabIndex = 26;
            // 
            // TxtLPPBNo
            // 
            this.TxtLPPBNo.EnterMoveNextControl = true;
            this.TxtLPPBNo.Location = new System.Drawing.Point(123, 156);
            this.TxtLPPBNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLPPBNo.Name = "TxtLPPBNo";
            this.TxtLPPBNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLPPBNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLPPBNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLPPBNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLPPBNo.Properties.MaxLength = 100;
            this.TxtLPPBNo.Properties.ReadOnly = true;
            this.TxtLPPBNo.Size = new System.Drawing.Size(242, 20);
            this.TxtLPPBNo.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(69, 160);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "LPPB#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDOCtVerify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 473);
            this.Name = "FrmDOCtVerify";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCt2DocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteReceivedDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteReceivedDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLPPBNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        public DevExpress.XtraEditors.SimpleButton BtnDOCt2DocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDOCt2DocNo;
        private System.Windows.Forms.Label label17;
        protected internal DevExpress.XtraEditors.TextEdit TxtDOCt2DocNo;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtWhsCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnJournalDocNo;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.DateEdit DteDeliveryDt;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.DateEdit DteReceivedDt;
        internal DevExpress.XtraEditors.TextEdit TxtLPPBNo;
        private System.Windows.Forms.Label label9;
    }
}