﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSoQuot : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mCtCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmSoQuotFind FrmFind;

        #endregion

        #region Constructor

        public FrmSoQuot(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Customer Quotation";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            Sl.SetLueCtCode(ref LueCtCode);
            Sl.SetLueRingCode(ref LueRingCode);
            Sl.SetLueSPCode(ref LueSPCode);
            Sl.SetLueCurCode(ref LueCurCode);
            Sl.SetLueShpMCode(ref LueShpMCode);
            Sl.SetLuePtCode(ref LuePtCode);
            Sl.SetLueAgingAP(ref LueAgingAP);

            SetGrd();

            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }

            if (mCtCode.Length != 0)
            {
                BtnInsertClick();
                Sm.SetLue(LueCtCode, mCtCode);
                Sm.SetControlReadOnly(LueCtCode, true);
                SetLueCtPersonCode(ref LueCtContactPersonName, mCtCode);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth
            (
                Grd1,
                new string[]
                {
                    //0
                    "",
 
                    //1-4
                    "City Code", "City Name", "Province", "Country" 
                },
                new int[]{ 20, 0, 200, 200, 150 }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 }, true);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColButton(Grd1, new int[] { 0 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 16;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Item Code",
                        "",
                        "Item Name",
                        "UoM",
                        "Item's Price"+Environment.NewLine+"Document Number",
                        
                        //6-10
                        "Item's Price"+Environment.NewLine+"DNo",
                        "Currency",
                        "Price",
                        "Discount"+Environment.NewLine+"Rate 1",
                        "Discount"+Environment.NewLine+"Rate 2", 

                        //11-15                                                                                   
                        "Discount"+Environment.NewLine+"Rate 3",
                        "Discount"+Environment.NewLine+"Rate 4",
                        "Discount"+Environment.NewLine+"Rate 5",
                        "Price"+Environment.NewLine+"After Discount",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        100, 20, 150, 80, 130,
                        
                        //6-10
                        0, 80, 100, 80, 80,
                        
                        //11-15
                        80, 80, 80, 100, 300
                    }
                );
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColButton(Grd2, new int[] { 0, 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9, 10, 11, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 5, 6 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkActInd, DteDocDt, TxtPreviousDocNo, LueCtCode,
                        LueCtContactPersonName, LueRingCode, LueSPCode, DteQtStartDt, TxtQtMth,
                        TxtSalesTarget, LuePtCode, LueAgingAP, LueCurCode, TxtCreditLimit, 
                        TxtDepositAmt, DteDepositExpDt, LueShpMCode, TxtMinDelivery, TxtGoodsReturnDay, 
                        MeeWhsCapacity, ChkPrintSignatureInd, MeeRemark
                    }, true);
                    BtnCtContactPersonName.Enabled = false;
                    Grd1.ReadOnly = Grd2.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueRingCode, LueSPCode, 
                        DteQtStartDt, TxtQtMth, TxtSalesTarget, LuePtCode, LueAgingAP, 
                        LueCurCode, TxtCreditLimit, LueShpMCode, TxtMinDelivery, TxtGoodsReturnDay, 
                        MeeWhsCapacity, ChkPrintSignatureInd, MeeRemark
                    }, false);
                    BtnCtContactPersonName.Enabled = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtPreviousDocNo, LueCtCode, LueCtContactPersonName, 
                LueRingCode, LueSPCode, DteQtStartDt, LuePtCode, LueAgingAP, 
                LueCurCode, DteDepositExpDt, LueShpMCode, MeeWhsCapacity, MeeRemark
            });
            
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtQtMth,
                TxtSalesTarget, TxtCreditLimit, TxtDepositAmt, TxtMinDelivery, TxtGoodsReturnDay,
            }, 0);

            ChkActInd.Checked = ChkPrintSignatureInd.Checked = false;
           

            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            ClearGrd2();
            Sm.FocusGrd(Grd2, 0, 2);
        }

        private void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 11, 12, 13, 14 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSoQuotFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            BtnInsertClick();
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date")||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCtContactPersonName, "Customer Contact Person") ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsDteEmpty(DteQtStartDt, "Quotation Start Date") ||
                Sm.IsTxtEmpty(TxtQtMth, "Quotation's month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Term Of Payment") ||
                Sm.IsLueEmpty(LueCurCode, "Curency") ||
                Sm.IsLueEmpty(LueShpMCode, "Shipping Method") ||
                IsGrdEmpty1() ||
                IsGrdValueNotValid1() ||
                IsGrdExceedMaxRecords1() ||
                IsGrdEmpty2() ||
                IsGrdValueNotValid2() ||
                IsGrdExceedMaxRecords2() ||
                IsDocDtNotValid();
        }

        private bool IsGrdEmpty1()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords1()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "City entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid1()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "City is empty.")) return true;
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords2()
        {
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Item entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd2, Row, 14, true, "Price after discount should be greater than 0."))
                    return true;
            return false;
        }

        private bool IsDocDtNotValid()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =
                    "Select DocDt From TblSoQuot " +
                    "Where ActInd='Y' And CtCode=@CtCode And DocDt>@DocDt Limit 1"
            };
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt).Substring(0,8));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "New document date should not be earlier than customer's last quotation date.");
                return true;
            }
            return false;
        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOQuot", "TblSOQuot");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOQuot(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSOQuotCity(DocNo, Row));
            
            for (int Row = 0; Row < Grd2.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveSOQuotItem(DocNo, Row));
            
            Sm.ExecCommands(cml);
            
            ShowData(DocNo);
        }

        private MySqlCommand SaveSOQuot(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOQuot Set ");
            SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where ActInd='Y' And CtCode=@CtCode; ");

            SQL.AppendLine("Insert Into TblSOQuot ");
            SQL.AppendLine("(DocNo, DocDt, ActInd, CtCode, CtContactPersonName, RingCode, SPCode, QtStartDt, QtMth, ");
            SQL.AppendLine("SalesTarget, PtCode, AgingAP, CurCode, CreditLimit, ShpMCode, MinDelivery, GoodsReturnDay, ");
            SQL.AppendLine("WhsCapacity, PrintSignatureInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'Y', @CtCode, @CtContactPersonName, @RingCode, @SPCode, @QtStartDt, @QtMth, ");
            SQL.AppendLine("@SalesTarget, @PtCode, @AgingAP, @CurCode, @CreditLimit, @ShpMCode, @MinDelivery, @GoodsReturnDay, ");
            SQL.AppendLine("@WhsCapacity, @PrintSignatureInd, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteQtStartDt));
            Sm.CmParam<Decimal>(ref cm, "@QtMth", Decimal.Parse(TxtQtMth.Text));
            Sm.CmParam<Decimal>(ref cm, "@SalesTarget", Decimal.Parse(TxtSalesTarget.Text));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAP", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<Decimal>(ref cm, "@MinDelivery", Decimal.Parse(TxtMinDelivery.Text));
            Sm.CmParam<Decimal>(ref cm, "@GoodsReturnDay", Decimal.Parse(TxtGoodsReturnDay.Text));
            Sm.CmParam<String>(ref cm, "@WhsCapacity", MeeWhsCapacity.Text);
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSOQuotCity(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOQuotCity(DocNo, DNo, CityCode, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @CityCode, @CreateBy, CurrentDateTime())"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
           
            return cm;
        }

        private MySqlCommand SaveSOQuotItem(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOQuotItem(DocNo, DNo, ItCode, ItemPriceDocNo, ItemPriceDNo, DiscRt1, DiscRt2, DiscRt3, DiscRt4, DiscRt5, UPrice, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @ItCode, @ItemPriceDocNo, @ItemPriceDNo, @DiscRt1, @DiscRt2, @DiscRt3, @DiscRt4, @DiscRt5, @UPrice, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItemPriceDocNo", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@ItemPriceDNo", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt1", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt2", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt3", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt4", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@DiscRt5", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 14));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSOQuot());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return IsDataEditedAlready();
        }

        private bool IsDataEditedAlready()
        {
            var ActiveInd = ChkActInd.Checked ? "Y" : "N";
            var cm = new MySqlCommand() 
            { 
                CommandText = "Select DocNo From TblSoQuot Where ActInd=@ActInd And DocNo=@DocNo;" 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActiveInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is " + (Sm.CompareStr(ActiveInd, "Y") ? "" : "not") + " active already.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSOQuot()
        {
            var cm = new MySqlCommand() 
            {
                CommandText = 
                    "Update TblSOQuot Set ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOQuot(DocNo);
                ShowSOQuotCity(DocNo);
                ShowSOQuotItem(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOQuot(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm,
                "Select T.*, (" +
                "   Select DocNo From TblSOQuot " +
                "   Where CtCode=T.CtCode And ActInd='N' " +
                "   Order By DocDt Desc, DocNo Desc Limit 1 " +
                "   ) As PreviousDocNo " +
                "From TblSOQuot T Where T.DocNo=@DocNo ",
                new string[] 
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "ActInd", "CtCode", "CtContactPersonName", "RingCode",
 
                    //6-10
                    "SPCode", "QtStartDt", "QtMth", "SalesTarget", "PtCode", 
                    
                    //11-15
                    "AgingAP", "CurCode", "CreditLimit", "ShpMCode", "MinDelivery", 
                    
                    //16-20
                    "GoodsReturnDay", "WhsCapacity", "PrintSignatureInd", "Remark", "PreviousDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                    LueCtContactPersonName.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[6]));
                    Sm.SetDte(DteQtStartDt, Sm.DrStr(dr, c[7]));
                    TxtQtMth.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                    TxtSalesTarget.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[9]), 0);
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueAgingAP, Sm.DrStr(dr, c[11]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[12]));
                    TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                    Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[14]));
                    TxtMinDelivery.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[15]), 0);
                    TxtGoodsReturnDay.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[16]), 0); ;
                    MeeWhsCapacity.EditValue = Sm.DrStr(dr, c[17]);
                    ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[18]), "Y");
                    MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                    TxtPreviousDocNo.EditValue = Sm.DrStr(dr, c[20]);
                }, true
            );
        }

        private void ShowSOQuotCity(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CityCode, B.CityName, C.ProvName, D.CntName ");
            SQL.AppendLine("From TblSOQuotCity A ");
            SQL.AppendLine("Inner Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Inner Join TblProvince C On B.ProvCode=C.ProvCode ");
            SQL.AppendLine("Inner Join TblCountry D On C.CntCode=D.CntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.CntName, C.ProvName, B.CityName");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "CityCode", 

                    //1-3
                    "CityName", "ProvName", "CntName" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowSOQuotItem(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, B.SalesUomCode, A.ItemPriceDocNo, A.ItemPriceDNo, C.CurCode, D.UPrice, ");
            SQL.AppendLine("A.DiscRt1, A.DiscRt2, A.DiscRt3, A.DiscRt4, A.DiscRt5, A.UPrice As PriceAfterDiscount, A.Remark ");
            SQL.AppendLine("From TblSOQuotItem A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblItemPriceHdr C On A.ItemPriceDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemPriceDtl D On A.ItemPriceDocNo=D.DocNo And A.ItemPriceDNo=D.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
               {
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "SalesUomCode", "ItemPriceDocNo", "ItemPriceDNo", "CurCode", 
                    
                    //6-10
                     "UPrice", "DiscRt1", "DiscRt2", "DiscRt3", "DiscRt4",  
                    
                    //11-13
                    "DiscRt5", "PriceAfterDiscount", "Remark" 
               },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12, 13, 14 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        internal void BtnInsertClick()
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            ChkActInd.Checked = true;
            Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
        }

        private void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        internal string GetSelectedCity()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputePriceAfterDiscount(int Row)
        {
            decimal
                PriceAfterDiscount = 0m,
                UnitPrice = Sm.GetGrdDec(Grd2, Row, 8),
                DiscRt1 = Sm.GetGrdDec(Grd2, Row, 9),
                DiscRt2 = Sm.GetGrdDec(Grd2, Row, 10),
                DiscRt3 = Sm.GetGrdDec(Grd2, Row, 11),
                DiscRt4 = Sm.GetGrdDec(Grd2, Row, 12),
                DiscRt5 = Sm.GetGrdDec(Grd2, Row, 13);

            PriceAfterDiscount = UnitPrice;
            if (DiscRt1 != 0) PriceAfterDiscount = PriceAfterDiscount * ((100 - DiscRt1) / 100);
            if (DiscRt2 != 0) PriceAfterDiscount = PriceAfterDiscount * ((100 - DiscRt2) / 100);
            if (DiscRt3 != 0) PriceAfterDiscount = PriceAfterDiscount * ((100 - DiscRt3) / 100);
            if (DiscRt4 != 0) PriceAfterDiscount = PriceAfterDiscount * ((100 - DiscRt4) / 100);
            if (DiscRt5 != 0) PriceAfterDiscount = PriceAfterDiscount * ((100 - DiscRt5) / 100);

            Grd2.Cells[Row, 14].Value = PriceAfterDiscount;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnCtContactPersonName_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                var f = new FrmCustomer(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mMenuCode = "RSYYNNN";
                f.mCtCode = Sm.GetLue(LueCtCode);
                f.ShowDialog();
                SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd2();
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

            if (Sm.GetLue(LueCtCode).Length==0)
            {
                LueCtContactPersonName.EditValue = null;
                Sm.SetControlReadOnly(LueCtContactPersonName, true);   
            }
            else
            {
                SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode));
                Sm.SetControlReadOnly(LueCtContactPersonName, false);
            }
        }

        private void LueCtContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));
        }

        private void LueRingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(Sl.SetLueSPCode));
        }

        private void TxtQtMth_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQtMth, 0);
        }

        private void TxtSalesTarget_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtSalesTarget, 0);
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueAgingAP_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAgingAP, new Sm.RefreshLue1(Sl.SetLueAgingAP));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            ClearGrd2();
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCreditLimit, 0);
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(Sl.SetLueShpMCode));
        }

        private void TxtMinDelivery_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMinDelivery, 0);
        }

        private void TxtGoodsReturnDay_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtGoodsReturnDay, 0);
        }

        private void LueCtContactPersonName_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            //(LueCtContactPersonName.Properties.DataSource as List<Lue1>).Add
            //( 
            //    new Lue1 { Col1 = e.DisplayValue.ToString() }
            //);
            //e.Handled = true;
        }

        #endregion

        #region Grid Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmSoQuotDlg(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSoQuotDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmSoQuotDlg2(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueCurCode)));
                }

                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 9, 10, 11, 12, 13, 14, 15 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }


        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) 
                Sm.FormShowDialog(new FrmSoQuotDlg2(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueCurCode)));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 9, 10, 11, 12, 13, 14 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 15 }, e);

            if (Sm.IsGrdColSelected(new int[] { 9, 10, 11, 12, 13 }, e.ColIndex))
                ComputePriceAfterDiscount(e.RowIndex);
        }

        #endregion        

        #endregion

    }
}
