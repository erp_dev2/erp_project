﻿#region Update
/*
 16/07/2019 [DITA] fitur baru untuk generate barcode
 * */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmItemBarcode2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        //internal Frm FrmFind;
        private bool mIsItemBarcode = false;


        #endregion

        #region Constructor

        public FrmItemBarcode2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
      
        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            //Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnFind.Visible = false;
            BtnInsert.Visible = false;
            BtnEdit.Visible = false;
            BtnDelete.Visible = false;
            BtnSave.Visible = false;
            BtnCancel.Visible = false;
            Sl.SetLueWhsCode(ref LueWhsCode);
            Sl.SetLueBin(ref LueBin);
            SetLueSeller(ref LueSellerCode);
            SetFormControl(mState.View);
        }

        
        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCode, TxtProdDesc, LueBin, LueSellerCode, LueWhsCode
                    }, false);
                    TxtProdCode.Focus();
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Button Method
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }
     
        #endregion
        #endregion

        public static void SetLueSeller(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SellerCode As Col1, SellerName As Col2 From TblImmSeller Order By SellerName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #region Additinal Method
        private void GetParameter()
        {
            mIsItemBarcode = Sm.GetParameter("IsItemBarcodeUseDefault") == "Y";
        }

        private void ParPrint()
        {
            if ( Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<ProdBarcode>();

            string[] TableName = { "ProdBarcode" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ProdCode, ");
            if (mIsItemBarcode)
            {
                SQL.AppendLine(" A.Source, ");
            }
            else
            {
                SQL.AppendLine(" Replace(A.Source,'-','')As Source, ");
            }
            SQL.AppendLine("substring_index(B.ProdDesc, '-',1)As ProdDesc, C.OptDesc As Color, D.SellerCode, A.Bin, A.WhsCode ");
            SQL.AppendLine("From TblImmStockSummary A ");
            SQL.AppendLine("Inner Join TblImmProduct B On A.ProdCode = B.ProdCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='ItemInformation2' And C.OptCode=B.Color ");
            SQL.AppendLine("Inner Join TblImmSeller D On A.SellerCode = D.SellerCode ");
            SQL.AppendLine("Inner Join TblBin E On A.Bin = E.Bin ");
            SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
            SQL.AppendLine("Where 0 = 0 ");
            if (TxtProdCode.Text.Length > 0)
                SQL.AppendLine("And A.ProdCode Like @ProdCode ");
            if (TxtProdDesc.Text.Length > 0)
                SQL.AppendLine("And B.ProdDesc Like @ProdDesc ");
            if (Sm.GetLue(LueSellerCode).Length > 0)
                SQL.AppendLine("And D.SellerCode = @SellerCode ");
            if (Sm.GetLue(LueBin).Length > 0)
                SQL.AppendLine("And A.Bin = @Bin ");
            if (Sm.GetLue(LueWhsCode).Length > 0)
                SQL.AppendLine("And A.WhsCode = @WhsCode ");

            string Filter = " " ;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                if (TxtProdCode.Text.Length > 0) Sm.CmParam<String>(ref cm, "@ProdCode", string.Concat("%", TxtProdCode.Text, "%"));
                if (TxtProdDesc.Text.Length > 0) Sm.CmParam<String>(ref cm, "@ProdDesc", string.Concat("%", TxtProdDesc.Text, "%"));
                if (Sm.GetLue(LueSellerCode).Length > 0) Sm.CmParam<String>(ref cm, "@SellerCode", Sm.GetLue(LueSellerCode));
                if (Sm.GetLue(LueBin).Length > 0) Sm.CmParam<String>(ref cm, "@Bin", Sm.GetLue(LueBin));
                if (Sm.GetLue(LueWhsCode).Length > 0) Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "ProdCode",

                         //1-3
                         "Source",
                         "ProdDesc",
                         "Color",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProdBarcode()
                        {
                            ProdCode = Sm.DrStr(dr, c[0]),

                            Source = Sm.DrStr(dr, c[1]),
                            ProdDesc = Sm.DrStr(dr, c[2]),
                            Color = Sm.DrStr(dr, c[3]),
                           
                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            if (l.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty); return;
            }

            Sm.PrintReport("ItemBarcode2", myLists, TableName, false);
        }

        #endregion
        #region Event

        #region Misc control Event
        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProdDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product Code");
        }

        private void ChkProdDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product Description");
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bin");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }
        private void ChkSellerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Seller");
        }
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(Sl.SetLueBin));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void LueSellerCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueSeller));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        #endregion

        #region Report Class

        class ProdBarcode
        {
            
            public string ProdCode { get; set; }
            public string ProdDesc { get; set; }
            public string Color { get; set; }
            public string Source { get; set; }
        }



        #endregion



    }
}
