﻿#region Update

/*
    31/05/2022 [SET/PRODUCT] Menu dialog baru
    15/06/2022 [SET/PRODUCT] Penyesuaian
    27/06/2022 [SET/PRODUCT] Menyesuaikan Choose data untuk field Investment Category terisi dengan nama Investment Category
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecuritiesDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAcquisitionDebtSecurities mFrmParent;
        string mSQL = string.Empty;
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecuritiesDlg2(FrmAcquisitionDebtSecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.PortofolioId InvestmentCode, B.PortofolioName InvestmentName, C.InvestmentCtCode, C.InvestmentCtName, A.UomCode, ");
            SQL.AppendLine("B.CurCode, A.InvestmentDebtCode, A.InterestFreq, A.ListingDt, A.MaturityDt, D.OptDesc AnnualDays, A.LastCouponDt, ");
            SQL.AppendLine("A.NextCouponDt, A.InterestRateAmt ");
            SQL.AppendLine("FROM tblinvestmentitemDebt A ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio B ON A.PortofolioId = B.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory C ON B.InvestmentCtCode = C.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tbloption D ON D.OptCat = 'AnnualDaysAssumption' AND A.AnnualDays = D.OptCode ");
            SQL.AppendLine("WHERE A.ActInd = 'Y' ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment Code",
                        "Investment Name",
                        "InvestmentCtCode",
                        "Category",
                        "UoM",

                        //6-10
                        "CurCode",
                        "InvestmentDebtCode",
                        "Interest Frequency",
                        "Listing Date",
                        "Maturity Date",

                        //11-14
                        "Annual Days",
                        "Last Coupon Date",
                        "Next Coupon Date",
                        "Interest Rate"

                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 150, 100, 100, 100,

                        //6-10
                        25, 100, 100, 100, 100,

                        //11-12
                        100, 100, 100, 100
                    }
                );
            //Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10 });
            //Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtInvestment.Text, new string[] { "PortofolioName", "A.PortofolioId" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt;",
                        new string[]
                        {
                            //0
                            "InvestmentCode", 

                            //1-5
                            "InvestmentName", "InvestmentCtCode", "InvestmentCtName", "UOMCode", "CurCode",

                            //6-10
                            "InvestmentDebtCode", "InterestFreq", "ListingDt", "MaturityDt", "AnnualDays", 

                            //11-13
                            "LastCouponDt", "NextCouponDt", "InterestRateAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                //mFrmParent.Grd1.Cells[mRow, 1].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                //mFrmParent.Grd1.Cells[mRow, 2].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                //mFrmParent.Grd1.Cells[mRow, 5].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                //mFrmParent.Grd1.Cells[mRow, 6].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                //mFrmParent.Grd1.Cells[mRow, 8].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                //mFrmParent.Grd1.Cells[mRow, 13].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.mInvestmentDebtCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.mInvestmentCategory = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtInvestmentCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtInvestmentName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtCtInvestment.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtAnnualDays.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.TxtInterestFrequency.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                Sm.SetDte(mFrmParent.DteListingDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 9));
                Sm.SetDte(mFrmParent.DteMaturityDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 10));
                Sm.SetDte(mFrmParent.DteLastCouponDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 12));
                Sm.SetDte(mFrmParent.DteNextCouponDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 13));
                mFrmParent.TxtCouponInterestRate.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);
                mFrmParent.TxtCurCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);

                mFrmParent.CheckCouponDate();
                this.Close();
            }
        }

        #endregion

        #region Event

        private void ChkInvestment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void TxtInvestment_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

    }
}
