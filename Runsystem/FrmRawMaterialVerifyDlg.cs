﻿#region Update
    /*
     13/05/2022 [DITA/IOK] tambah RAK 16-25 untuk verifikasi
     */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRawMaterialVerifyDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRawMaterialVerify mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRawMaterialVerifyDlg(FrmRawMaterialVerify FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -14);
            Sl.SetLueVdCode(ref LueVdCode);
            ShowData();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.VdName, A.QueueNo ");
            SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.ProcessInd1='P' And A.ProcessInd2='P' ");
            SQL.AppendLine("And A.DocNo Not In ( ");

            #region Query 1

            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, ");
            SQL.AppendLine("        Case T3.SectionNo ");
            SQL.AppendLine("            When '1' Then T2.Shelf1 ");
            SQL.AppendLine("            When '2' Then T2.Shelf2 ");
            SQL.AppendLine("            When '3' Then T2.Shelf3 ");
            SQL.AppendLine("            When '4' Then T2.Shelf4 ");
            SQL.AppendLine("            When '5' Then T2.Shelf5 ");
            SQL.AppendLine("            When '6' Then T2.Shelf6 ");
            SQL.AppendLine("            When '7' Then T2.Shelf7 ");
            SQL.AppendLine("            When '8' Then T2.Shelf8 ");
            SQL.AppendLine("            When '9' Then T2.Shelf9 ");
            SQL.AppendLine("            When '10' Then T2.Shelf10 ");
            SQL.AppendLine("            When '11' Then T2.Shelf11 ");
            SQL.AppendLine("            When '12' Then T2.Shelf12 ");
            SQL.AppendLine("            When '13' Then T2.Shelf13 ");
            SQL.AppendLine("            When '14' Then T2.Shelf14 ");
            SQL.AppendLine("            When '15' Then T2.Shelf15 ");
            SQL.AppendLine("            When '16' Then T2.Shelf16 ");
            SQL.AppendLine("            When '17' Then T2.Shelf17 ");
            SQL.AppendLine("            When '18' Then T2.Shelf18 ");
            SQL.AppendLine("            When '19' Then T2.Shelf19 ");
            SQL.AppendLine("            When '20' Then T2.Shelf20 ");
            SQL.AppendLine("            When '21' Then T2.Shelf21 ");
            SQL.AppendLine("            When '22' Then T2.Shelf22 ");
            SQL.AppendLine("            When '23' Then T2.Shelf23 ");
            SQL.AppendLine("            When '24' Then T2.Shelf24 ");
            SQL.AppendLine("            When '25' Then T2.Shelf25 ");
            SQL.AppendLine("        End As Bin, ");
            SQL.AppendLine("        Sum(T3.Qty) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(Z3.Qty) Qty2 ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr Z1 ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr Z2 On Z1.DocNo=Z2.LegalDocVerifyDocNo And Z2.CancelInd='N' And Z2.DocType='1' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl Z3 On Z2.DocNo=Z3.DocNo ");
            SQL.AppendLine("            Where Z1.CancelInd='N' And Z1.ProcessInd1='P' And Z1.ProcessInd2='P' ");
            SQL.AppendLine("            And Z1.DocNo=T1.DocNo ");
            SQL.AppendLine("            And Bin=Z3.Shelf ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("        From TblLegalDocVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialHdr T2 On T1.DocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' And T2.DocType='1' ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialDtl2 T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.ProcessInd1='P' And T1.ProcessInd2='P' ");
            SQL.AppendLine("        Group By T1.DocNo, Bin ");
            SQL.AppendLine("        Having IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo From ( ");
            SQL.AppendLine("        Select T.DocNo, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("    From TblLegalDocVerifyHdr T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.ProcessInd1='P' And T.ProcessInd2='P' ");
            SQL.AppendLine("    ) X Where IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");

            #region Old Code

            //SQL.AppendLine("    Select Distinct Tbl.DocNo From ( ");

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' ");
            //SQL.AppendLine("        And T.ProcessInd1='P' ");
            //SQL.AppendLine("        And T.ProcessInd2='P' ");

            //SQL.AppendLine("    ) Tbl Where IfNull(Tbl.Qty1, 0)<>IfNull(Tbl.Qty2, 0) ");

            #endregion

            #endregion

            SQL.AppendLine("    Union All ");

            #region Query 2

            //SQL.AppendLine("    Select Distinct Tbl1.DocNo From ( ");

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='2' ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='2' ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' ");
            //SQL.AppendLine("        And T.ProcessInd1='P' ");
            //SQL.AppendLine("        And T.ProcessInd2='P' ");
            //SQL.AppendLine("    ) Tbl1 ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("            Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            //SQL.AppendLine("            From TblParameter Where ParCode='BalokVerifyPercentage' ");
            //SQL.AppendLine("        ) Tbl2 On 0=0 ");
            //SQL.AppendLine("        Where ( ");
            //SQL.AppendLine("            Qty2<(Qty1*(100-BalokInterval)*0.01) ");
            //SQL.AppendLine("            Or Qty2>(Qty1*(100+BalokInterval)*0.01) ");
            //SQL.AppendLine("        ) ");

            SQL.AppendLine("        Select Distinct DocNo From ( ");
            SQL.AppendLine("            Select T1.DocNo ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select A.DocNo, ");
            SQL.AppendLine("                        Case C.SectionNo ");
            SQL.AppendLine("                            When '1' Then B.Shelf1 ");
            SQL.AppendLine("                            When '2' Then B.Shelf2 ");
            SQL.AppendLine("                            When '3' Then B.Shelf3 ");
            SQL.AppendLine("                            When '4' Then B.Shelf4 ");
            SQL.AppendLine("                            When '5' Then B.Shelf5 ");
            SQL.AppendLine("                            When '6' Then B.Shelf6 ");
            SQL.AppendLine("                            When '7' Then B.Shelf7 ");
            SQL.AppendLine("                            When '8' Then B.Shelf8 ");
            SQL.AppendLine("                            When '9' Then B.Shelf9 ");
            SQL.AppendLine("                            When '10' Then B.Shelf10 ");
            SQL.AppendLine("                            When '11' Then B.Shelf11 ");
            SQL.AppendLine("                            When '12' Then B.Shelf12 ");
            SQL.AppendLine("                            When '13' Then B.Shelf13 ");
            SQL.AppendLine("                            When '14' Then B.Shelf14 ");
            SQL.AppendLine("                            When '15' Then B.Shelf15 ");
            SQL.AppendLine("                            When '16' Then B.Shelf16 ");
            SQL.AppendLine("                            When '17' Then B.Shelf17 ");
            SQL.AppendLine("                            When '18' Then B.Shelf18 ");
            SQL.AppendLine("                            When '19' Then B.Shelf19 ");
            SQL.AppendLine("                            When '20' Then B.Shelf20 ");
            SQL.AppendLine("                            When '21' Then B.Shelf21 ");
            SQL.AppendLine("                            When '22' Then B.Shelf22 ");
            SQL.AppendLine("                            When '23' Then B.Shelf23 ");
            SQL.AppendLine("                            When '24' Then B.Shelf24 ");
            SQL.AppendLine("                            When '25' Then B.Shelf25 ");
            SQL.AppendLine("                    End As Bin, C.Qty ");
            SQL.AppendLine("                    From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("                            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                            And B.CancelInd='N' ");
            SQL.AppendLine("                            And B.DocType='2' ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                    Where A.CancelInd='N' ");
            SQL.AppendLine("                    And A.ProcessInd1='P' ");
            SQL.AppendLine("                    And A.ProcessInd2='P' ");
            SQL.AppendLine("                ) T Group By DocNo, Bin ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("                From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                    On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                    And B.CancelInd='N' ");
            SQL.AppendLine("                    And B.DocType='2' ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                Where A.CancelInd='N' ");
            SQL.AppendLine("                And A.ProcessInd1='P' ");
            SQL.AppendLine("                And A.ProcessInd2='P' ");
            SQL.AppendLine("            ) T Group By DocNo, Bin ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("            From TblParameter Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("        ) T3 On 0=0 ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("            IfNull(T2.Qty, 0)<(IfNull(T1.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("            Or IfNull(T2.Qty, 0)>(IfNull(T1.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo From ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                And B.CancelInd='N' ");
            SQL.AppendLine("                And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, ");
            SQL.AppendLine("                Case C.SectionNo ");
            SQL.AppendLine("                    When '1' Then B.Shelf1 ");
            SQL.AppendLine("                    When '2' Then B.Shelf2 ");
            SQL.AppendLine("                    When '3' Then B.Shelf3 ");
            SQL.AppendLine("                    When '4' Then B.Shelf4 ");
            SQL.AppendLine("                    When '5' Then B.Shelf5 ");
            SQL.AppendLine("                    When '6' Then B.Shelf6 ");
            SQL.AppendLine("                    When '7' Then B.Shelf7 ");
            SQL.AppendLine("                    When '8' Then B.Shelf8 ");
            SQL.AppendLine("                    When '9' Then B.Shelf9 ");
            SQL.AppendLine("                    When '10' Then B.Shelf10 ");
            SQL.AppendLine("                    When '11' Then B.Shelf11 ");
            SQL.AppendLine("                    When '12' Then B.Shelf12 ");
            SQL.AppendLine("                    When '13' Then B.Shelf13 ");
            SQL.AppendLine("                    When '14' Then B.Shelf14 ");
            SQL.AppendLine("                    When '15' Then B.Shelf15 ");
            SQL.AppendLine("                    When '16' Then B.Shelf16 ");
            SQL.AppendLine("                    When '17' Then B.Shelf17 ");
            SQL.AppendLine("                    When '18' Then B.Shelf18 ");
            SQL.AppendLine("                    When '19' Then B.Shelf19 ");
            SQL.AppendLine("                    When '20' Then B.Shelf20 ");
            SQL.AppendLine("                    When '21' Then B.Shelf21 ");
            SQL.AppendLine("                    When '22' Then B.Shelf22 ");
            SQL.AppendLine("                    When '23' Then B.Shelf23 ");
            SQL.AppendLine("                    When '24' Then B.Shelf24 ");
            SQL.AppendLine("                    When '25' Then B.Shelf25 ");
            SQL.AppendLine("                End As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("        From TblParameter ");
            SQL.AppendLine("        Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("    ) T3 On 0=0 ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("        IfNull(T1.Qty, 0)<(IfNull(T2.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        Or IfNull(T1.Qty, 0)>(IfNull(T2.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl ");

            #endregion

            SQL.AppendLine(") ");

            mSQL = SQL.ToString();

            #region Old Code 1

            //SQL.AppendLine("Select A.DocNo, A.DocDt, B.VdName, A.QueueNo ");
            //SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            //SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            //SQL.AppendLine("Where A.CancelInd='N' And A.ProcessInd1='P' And A.ProcessInd2='P' ");
            //SQL.AppendLine("And A.DocNo Not In ( ");

            //#region Query 1-1

            //SQL.AppendLine("    Select Distinct Tbl1.LegalDocVerifyDocNo From ( ");

            //#region Query 1-2

            //SQL.AppendLine("        Select T.LegalDocVerifyDocNo, T.DocType, T.Shelf, Sum(T.Qty) As Qty1, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) Qty ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A ");
            //SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("            Where A.CancelInd='N' ");
            //SQL.AppendLine("            And IfNull(A.ProcessInd, 'O')='O' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.LegalDocVerifyDocNo ");
            //SQL.AppendLine("            And B.Shelf=T.Shelf ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From ( ");

            //#region Query 1-3

            //for (int Index = 1; Index <= 10; Index++)
            //{
            //    if (Index != 1) SQL.AppendLine("            Union All ");
            //    SQL.AppendLine("            Select T1.LegalDocVerifyDocNo, T1.DocType, T1.Shelf" + Index.ToString() + " As Shelf, T2.Qty ");
            //    SQL.AppendLine("            From TblRecvRawMaterialHdr T1 ");
            //    SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 T2 On T1.DocNo=T2.DocNo And T2.SectionNo='" + Index.ToString() + "' ");
            //    SQL.AppendLine("            Where T1.CancelInd='N' And IfNull(T1.ProcessInd, 'O')='O' ");
            //}

            //#endregion

            //SQL.AppendLine("        ) T Group By T.LegalDocVerifyDocNo, T.DocType, T.Shelf ");

            //#endregion

            //SQL.AppendLine("    ) Tbl1 ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            //SQL.AppendLine("        From TblParameter Where ParCode='BalokVerifyPercentage' ");
            //SQL.AppendLine("    ) Tbl2 On 0=0 ");
            //SQL.AppendLine("    Where ( ");
            //SQL.AppendLine("        (DocType='1' And IfNull(Qty1, 0)<>IfNull(Qty2, 0)) ");
            //SQL.AppendLine("        Or (DocType='2' ");
            //SQL.AppendLine("            And ( ");
            //SQL.AppendLine("            IfNull(Qty2, 0)<(IfNull(Qty1, 0)*(100-BalokInterval)*0.01) ");
            //SQL.AppendLine("            Or IfNull(Qty2, 0)>(IfNull(Qty1, 0)*(100+BalokInterval)*0.01) ");
            //SQL.AppendLine("            )");
            //SQL.AppendLine("        )");
            //SQL.AppendLine("    )");

            //#endregion

            //SQL.AppendLine(") ");

            //SQL.AppendLine("And A.DocNo In ( ");

            //#region Query 2-1

            //SQL.AppendLine("    Select Distinct Tbl.DocNo From ( ");

            //#region Query 2-2

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' ");
            //SQL.AppendLine("        And T.ProcessInd1='P' ");
            //SQL.AppendLine("        And T.ProcessInd2='P' ");

            //#endregion

            //SQL.AppendLine("    ) Tbl Where IfNull(Tbl.Qty1, 0)=IfNull(Tbl.Qty2, 0) ");

            //#endregion

            //SQL.AppendLine(") ");

            #endregion

            #region Old Code 2

            //SQL.AppendLine("Select A.DocNo, A.DocDt, B.VdName, A.QueueNo ");
            //SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            //SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            //SQL.AppendLine("Where A.CancelInd='N' And A.ProcessInd1='P' And A.ProcessInd2='P' ");
            //SQL.AppendLine("And A.DocNo Not In ( ");

            //#region Query 1-1

            //SQL.AppendLine("    Select Distinct Tbl.LegalDocVerifyDocNo From ( ");

            //#region Query 1-2

            //SQL.AppendLine("        Select T.LegalDocVerifyDocNo, T.Shelf, Sum(T.Qty) As Qty1, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) Qty ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A ");
            //SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("            Where A.CancelInd='N' ");
            //SQL.AppendLine("            And IfNull(A.ProcessInd, 'O')='O' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.LegalDocVerifyDocNo ");
            //SQL.AppendLine("            And B.Shelf=T.Shelf ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From ( ");

            //#region Query 1-3

            //for (int Index = 1; Index <= 10; Index++)
            //{
            //    if (Index != 1) SQL.AppendLine("            Union All ");
            //    SQL.AppendLine("            Select T1.LegalDocVerifyDocNo, T1.Shelf" + Index.ToString() + " As Shelf, T2.Qty ");
            //    SQL.AppendLine("            From TblRecvRawMaterialHdr T1 ");
            //    SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 T2 On T1.DocNo=T2.DocNo And T2.SectionNo='" + Index.ToString() + "' ");
            //    SQL.AppendLine("            Where T1.CancelInd='N' And IfNull(T1.ProcessInd, 'O')='O' ");
            //}

            //#endregion

            //SQL.AppendLine("        ) T Group By T.LegalDocVerifyDocNo, T.Shelf ");

            //#endregion

            //SQL.AppendLine("    ) Tbl Where IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");

            //#endregion

            //SQL.AppendLine(") ");

            //SQL.AppendLine("And A.DocNo In ( ");

            //#region Query 2-1

            //SQL.AppendLine("    Select Distinct Tbl.DocNo From ( ");

            //#region Query 2-2

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo And A.CancelInd='N' And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo And A.CancelInd='N' And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' And T.ProcessInd1='P' And T.ProcessInd2='P' ");

            //#endregion

            //SQL.AppendLine("    ) Tbl Where IfNull(Tbl.Qty1, 0)=IfNull(Tbl.Qty2, 0) ");

            //#endregion

            //SQL.AppendLine(") ");

            #endregion
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Nomor Dokumen", 
                        "",
                        "Tanggal"+Environment.NewLine+"Dokumen",
                        "Vendor",
                        "Nomor"+Environment.NewLine+"Antrian"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 100, 180, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-3
                            "DocDt", "VdName", "QueueNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 0))
                {
                    mFrmParent.TxtLegalDocVerifyDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtVdCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtQueueNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                    mFrmParent.ShowRecvRawMaterial();
                    mFrmParent.ShowRawMaterialOpname();
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmLegalDocVerifyUpdate(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmLegalDocVerifyUpdate(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event
        
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
