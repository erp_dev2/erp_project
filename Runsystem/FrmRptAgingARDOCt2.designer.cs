﻿namespace RunSystem
{
    partial class FrmRptAgingARDOCt2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.ChkDocDt = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkCtCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueCtCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDODt1 = new DevExpress.XtraEditors.DateEdit();
            this.DteDODt2 = new DevExpress.XtraEditors.DateEdit();
            this.ChkDODt = new DevExpress.XtraEditors.CheckEdit();
            this.LblDODt = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DteBalAsOfDt = new DevExpress.XtraEditors.DateEdit();
            this.LblBalAsOf = new System.Windows.Forms.Label();
            this.ChkCtItCode = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueCtItCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkBalAsOfDt = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDODt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalAsOfDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalAsOfDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBalAsOfDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkBalAsOfDt);
            this.panel2.Controls.Add(this.ChkCtItCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueCtItCode);
            this.panel2.Controls.Add(this.DteBalAsOfDt);
            this.panel2.Controls.Add(this.LblBalAsOf);
            this.panel2.Controls.Add(this.DteDODt1);
            this.panel2.Controls.Add(this.DteDODt2);
            this.panel2.Controls.Add(this.ChkDODt);
            this.panel2.Controls.Add(this.LblDODt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.ChkCtCtCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueCtCtCode);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.ChkDocDt);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Size = new System.Drawing.Size(772, 80);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 393);
            this.Grd1.TabIndex = 19;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 80);
            this.panel3.Size = new System.Drawing.Size(772, 393);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(87, 25);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 14;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(205, 25);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 16;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // ChkDocDt
            // 
            this.ChkDocDt.Location = new System.Drawing.Point(310, 24);
            this.ChkDocDt.Name = "ChkDocDt";
            this.ChkDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocDt.Properties.Caption = " ";
            this.ChkDocDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocDt.Size = new System.Drawing.Size(19, 22);
            this.ChkDocDt.TabIndex = 17;
            this.ChkDocDt.ToolTip = "Remove filter";
            this.ChkDocDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocDt.ToolTipTitle = "Run System";
            this.ChkDocDt.CheckedChanged += new System.EventHandler(this.ChkDocDt_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Due Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(190, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "-";
            // 
            // ChkCtCode
            // 
            this.ChkCtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkCtCode.Location = new System.Drawing.Point(737, 5);
            this.ChkCtCode.Name = "ChkCtCode";
            this.ChkCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCtCode.Properties.Caption = " ";
            this.ChkCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkCtCode.TabIndex = 22;
            this.ChkCtCode.ToolTip = "Remove filter";
            this.ChkCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCtCode.ToolTipTitle = "Run System";
            this.ChkCtCode.CheckedChanged += new System.EventHandler(this.ChkCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(453, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 14);
            this.label4.TabIndex = 20;
            this.label4.Text = "Customer";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(514, 5);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(219, 20);
            this.LueCtCode.TabIndex = 21;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // ChkCtCtCode
            // 
            this.ChkCtCtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkCtCtCode.Location = new System.Drawing.Point(737, 26);
            this.ChkCtCtCode.Name = "ChkCtCtCode";
            this.ChkCtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCtCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCtCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCtCtCode.Properties.Caption = " ";
            this.ChkCtCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkCtCtCode.TabIndex = 25;
            this.ChkCtCtCode.ToolTip = "Remove filter";
            this.ChkCtCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCtCtCode.ToolTipTitle = "Run System";
            this.ChkCtCtCode.CheckedChanged += new System.EventHandler(this.ChkCtCtCode_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(400, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 14);
            this.label2.TabIndex = 23;
            this.label2.Text = "Customer Category";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCtCode
            // 
            this.LueCtCtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueCtCtCode.EnterMoveNextControl = true;
            this.LueCtCtCode.Location = new System.Drawing.Point(514, 26);
            this.LueCtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCtCode.Name = "LueCtCtCode";
            this.LueCtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCtCode.Properties.DropDownRows = 30;
            this.LueCtCtCode.Properties.NullText = "[Empty]";
            this.LueCtCtCode.Properties.PopupWidth = 300;
            this.LueCtCtCode.Size = new System.Drawing.Size(219, 20);
            this.LueCtCtCode.TabIndex = 24;
            this.LueCtCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCtCode.EditValueChanged += new System.EventHandler(this.LueCtCtCode_EditValueChanged);
            // 
            // DteDODt1
            // 
            this.DteDODt1.EditValue = null;
            this.DteDODt1.EnterMoveNextControl = true;
            this.DteDODt1.Location = new System.Drawing.Point(87, 4);
            this.DteDODt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDODt1.Name = "DteDODt1";
            this.DteDODt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDODt1.Properties.Appearance.Options.UseFont = true;
            this.DteDODt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDODt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDODt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDODt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDODt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDODt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDODt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDODt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDODt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDODt1.Size = new System.Drawing.Size(101, 20);
            this.DteDODt1.TabIndex = 9;
            this.DteDODt1.EditValueChanged += new System.EventHandler(this.DteDODt1_EditValueChanged);
            this.DteDODt1.Validated += new System.EventHandler(this.DteDODt1_Validated);
            // 
            // DteDODt2
            // 
            this.DteDODt2.EditValue = null;
            this.DteDODt2.EnterMoveNextControl = true;
            this.DteDODt2.Location = new System.Drawing.Point(205, 4);
            this.DteDODt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDODt2.Name = "DteDODt2";
            this.DteDODt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDODt2.Properties.Appearance.Options.UseFont = true;
            this.DteDODt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDODt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDODt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDODt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDODt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDODt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDODt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDODt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDODt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDODt2.Size = new System.Drawing.Size(101, 20);
            this.DteDODt2.TabIndex = 11;
            this.DteDODt2.EditValueChanged += new System.EventHandler(this.DteDODt2_EditValueChanged);
            this.DteDODt2.Validated += new System.EventHandler(this.DteDODt2_Validated);
            // 
            // ChkDODt
            // 
            this.ChkDODt.Location = new System.Drawing.Point(310, 3);
            this.ChkDODt.Name = "ChkDODt";
            this.ChkDODt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDODt.Properties.Appearance.Options.UseFont = true;
            this.ChkDODt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDODt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDODt.Properties.Caption = " ";
            this.ChkDODt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDODt.Size = new System.Drawing.Size(19, 22);
            this.ChkDODt.TabIndex = 12;
            this.ChkDODt.ToolTip = "Remove filter";
            this.ChkDODt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDODt.ToolTipTitle = "Run System";
            this.ChkDODt.CheckedChanged += new System.EventHandler(this.ChkDODt_CheckedChanged);
            // 
            // LblDODt
            // 
            this.LblDODt.AutoSize = true;
            this.LblDODt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDODt.ForeColor = System.Drawing.Color.Red;
            this.LblDODt.Location = new System.Drawing.Point(31, 6);
            this.LblDODt.Name = "LblDODt";
            this.LblDODt.Size = new System.Drawing.Size(54, 14);
            this.LblDODt.TabIndex = 8;
            this.LblDODt.Text = "DO Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(190, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "-";
            // 
            // DteBalAsOfDt
            // 
            this.DteBalAsOfDt.EditValue = null;
            this.DteBalAsOfDt.EnterMoveNextControl = true;
            this.DteBalAsOfDt.Location = new System.Drawing.Point(87, 46);
            this.DteBalAsOfDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBalAsOfDt.Name = "DteBalAsOfDt";
            this.DteBalAsOfDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBalAsOfDt.Properties.Appearance.Options.UseFont = true;
            this.DteBalAsOfDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBalAsOfDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBalAsOfDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBalAsOfDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBalAsOfDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBalAsOfDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBalAsOfDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBalAsOfDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBalAsOfDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBalAsOfDt.Size = new System.Drawing.Size(101, 20);
            this.DteBalAsOfDt.TabIndex = 19;
            this.DteBalAsOfDt.EditValueChanged += new System.EventHandler(this.DteBalAsOfDt_EditValueChanged);
            this.DteBalAsOfDt.Validated += new System.EventHandler(this.DteBalAsOfDt_Validated);
            // 
            // LblBalAsOf
            // 
            this.LblBalAsOf.AutoSize = true;
            this.LblBalAsOf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBalAsOf.ForeColor = System.Drawing.Color.Red;
            this.LblBalAsOf.Location = new System.Drawing.Point(3, 48);
            this.LblBalAsOf.Name = "LblBalAsOf";
            this.LblBalAsOf.Size = new System.Drawing.Size(82, 14);
            this.LblBalAsOf.TabIndex = 18;
            this.LblBalAsOf.Text = "Balance As Of";
            // 
            // ChkCtItCode
            // 
            this.ChkCtItCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkCtItCode.Location = new System.Drawing.Point(737, 47);
            this.ChkCtItCode.Name = "ChkCtItCode";
            this.ChkCtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCtItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCtItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCtItCode.Properties.Caption = " ";
            this.ChkCtItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtItCode.Size = new System.Drawing.Size(19, 22);
            this.ChkCtItCode.TabIndex = 28;
            this.ChkCtItCode.ToolTip = "Remove filter";
            this.ChkCtItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCtItCode.ToolTipTitle = "Run System";
            this.ChkCtItCode.EditValueChanged += new System.EventHandler(this.ChkCtItem_EditValueChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(418, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 14);
            this.label8.TabIndex = 26;
            this.label8.Text = "Item\'s Category";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtItCode
            // 
            this.LueCtItCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueCtItCode.EnterMoveNextControl = true;
            this.LueCtItCode.Location = new System.Drawing.Point(514, 47);
            this.LueCtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtItCode.Name = "LueCtItCode";
            this.LueCtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtItCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtItCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtItCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtItCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtItCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtItCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtItCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtItCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtItCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtItCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtItCode.Properties.DropDownRows = 30;
            this.LueCtItCode.Properties.NullText = "[Empty]";
            this.LueCtItCode.Properties.PopupWidth = 300;
            this.LueCtItCode.Size = new System.Drawing.Size(219, 20);
            this.LueCtItCode.TabIndex = 27;
            this.LueCtItCode.ToolTip = "F4 : Show/hide list";
            this.LueCtItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtItCode.EditValueChanged += new System.EventHandler(this.LueCtItem_EditValueChanged);
            // 
            // ChkBalAsOfDt
            // 
            this.ChkBalAsOfDt.Location = new System.Drawing.Point(189, 46);
            this.ChkBalAsOfDt.Name = "ChkBalAsOfDt";
            this.ChkBalAsOfDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBalAsOfDt.Properties.Appearance.Options.UseFont = true;
            this.ChkBalAsOfDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBalAsOfDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBalAsOfDt.Properties.Caption = " ";
            this.ChkBalAsOfDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBalAsOfDt.Size = new System.Drawing.Size(19, 22);
            this.ChkBalAsOfDt.TabIndex = 29;
            this.ChkBalAsOfDt.ToolTip = "Remove filter";
            this.ChkBalAsOfDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBalAsOfDt.ToolTipTitle = "Run System";
            this.ChkBalAsOfDt.EditValueChanged += new System.EventHandler(this.ChkBalAsOfDt_EditValueChanged);
            // 
            // FrmRptAgingARDOCt2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptAgingARDOCt2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDODt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDODt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalAsOfDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBalAsOfDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBalAsOfDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        private DevExpress.XtraEditors.CheckEdit ChkDocDt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkCtCtCode;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueCtCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDODt1;
        internal DevExpress.XtraEditors.DateEdit DteDODt2;
        private DevExpress.XtraEditors.CheckEdit ChkDODt;
        private System.Windows.Forms.Label LblDODt;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkCtItCode;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueCtItCode;
        internal DevExpress.XtraEditors.DateEdit DteBalAsOfDt;
        private System.Windows.Forms.Label LblBalAsOf;
        private DevExpress.XtraEditors.CheckEdit ChkBalAsOfDt;
    }
}