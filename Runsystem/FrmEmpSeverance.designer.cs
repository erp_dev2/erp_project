﻿namespace RunSystem
{
    partial class FrmEmpSeverance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpSeverance));
            this.Tpg = new System.Windows.Forms.TabControl();
            this.TpgSalary = new System.Windows.Forms.TabPage();
            this.TxtDtlSalary = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtDtlPension = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDtlNonPension = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TpgBenefit = new System.Windows.Forms.TabPage();
            this.TxtDtlRetirementCapital = new DevExpress.XtraEditors.TextEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.TxtDtlPensionType = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtDtlEmprFee = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtDtlEmpFee = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtDtlRetirement = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TpgSeverance = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtDtlSevPay5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSevPay3 = new DevExpress.XtraEditors.TextEdit();
            this.label58 = new System.Windows.Forms.Label();
            this.TxtDtlResidual = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSevPay4 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.TxtDtlSevLongService = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtDtlProsentase = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSalary3 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtDtlDivider = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDtlSevPay2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSevPay1 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtDtlSalary2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSalary1 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtDtlSevGrp2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtDtlSevGrp1 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LueDtlKoefisien2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueDtlKoefisien1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgAccrued = new System.Windows.Forms.TabPage();
            this.TxtDtlRetirementCapital2 = new DevExpress.XtraEditors.TextEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtDtlAccruedExp = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.TxtDtlCorporateFinancial = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtDtlEmprFee2 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtDtlSevPay6 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TpgRecvPay = new System.Windows.Forms.TabPage();
            this.TxtDtlRetirementCapital3 = new DevExpress.XtraEditors.TextEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.TxtDtlReceivedPay = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.TxtDtlAccruedExp2 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.TxtDtlCorporateDonation = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.TxtDtlCorporateFinancial2 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.TxtDtlEmprFee3 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TxtDtlEmpFee2 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.TpgPayment = new System.Windows.Forms.TabPage();
            this.TxtDtlAmtAfterTax = new DevExpress.XtraEditors.TextEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtDtlTax2 = new DevExpress.XtraEditors.TextEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.TxtDtlTax = new DevExpress.XtraEditors.TextEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.TxtDtlAmtBeforeTax = new DevExpress.XtraEditors.TextEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtDtlCorporateDonation2 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtDtlCorporateFinancial3 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.TxtDtlAccruedExp3 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TpgDeduction = new System.Windows.Forms.TabPage();
            this.MeeDeductionRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.TxtDeductionAmt = new DevExpress.XtraEditors.TextEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtVRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.DteRetiredDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnRecvDirect = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtHdrRecvDirect = new DevExpress.XtraEditors.TextEdit();
            this.DteLeaveStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.BtnRecvPay = new DevExpress.XtraEditors.SimpleButton();
            this.TxtHdrReceivedPay = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtHdrSalary = new DevExpress.XtraEditors.TextEdit();
            this.TxtYearsWorked = new DevExpress.XtraEditors.TextEdit();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnBenefit = new DevExpress.XtraEditors.SimpleButton();
            this.TxtHdrSeverance = new DevExpress.XtraEditors.TextEdit();
            this.BtnSalary = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.btnAccExp = new DevExpress.XtraEditors.SimpleButton();
            this.TxtHdrAccruedExp = new DevExpress.XtraEditors.TextEdit();
            this.TxtHdrRetirement = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.BtnSevPay = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSiteName = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBirthPlace = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtIDNumber = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtProcess = new DevExpress.XtraEditors.TextEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.TpgAttachment = new System.Windows.Forms.TabPage();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label70 = new System.Windows.Forms.Label();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label71 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Tpg.SuspendLayout();
            this.TpgSalary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlPension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlNonPension.Properties)).BeginInit();
            this.TpgBenefit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlPensionType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmpFee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirement.Properties)).BeginInit();
            this.TpgSeverance.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlResidual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevLongService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlProsentase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlDivider.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevGrp2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevGrp1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDtlKoefisien2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDtlKoefisien1.Properties)).BeginInit();
            this.TpgAccrued.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay6.Properties)).BeginInit();
            this.TpgRecvPay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlReceivedPay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateDonation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmpFee2.Properties)).BeginInit();
            this.TpgPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAmtAfterTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlTax2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAmtBeforeTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateDonation2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp3.Properties)).BeginInit();
            this.TpgDeduction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDeductionRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeductionAmt.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRetiredDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRetiredDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrRecvDirect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrReceivedPay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYearsWorked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrSeverance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrAccruedExp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrRetirement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIDNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcess.Properties)).BeginInit();
            this.TpgAttachment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(929, 0);
            this.panel1.Size = new System.Drawing.Size(70, 580);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtProcess);
            this.panel2.Controls.Add(this.label67);
            this.panel2.Controls.Add(this.TxtBirthPlace);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.TxtIDNumber);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.DteBirthDt);
            this.panel2.Controls.Add(this.TxtPosition);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDeptName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtSiteName);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.BtnEmpCode2);
            this.panel2.Controls.Add(this.TxtEmpName);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.BtnEmpCode);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.Tpg);
            this.panel2.Size = new System.Drawing.Size(929, 580);
            // 
            // Tpg
            // 
            this.Tpg.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tpg.Controls.Add(this.TpgSalary);
            this.Tpg.Controls.Add(this.TpgBenefit);
            this.Tpg.Controls.Add(this.TpgSeverance);
            this.Tpg.Controls.Add(this.TpgAccrued);
            this.Tpg.Controls.Add(this.TpgRecvPay);
            this.Tpg.Controls.Add(this.TpgPayment);
            this.Tpg.Controls.Add(this.TpgDeduction);
            this.Tpg.Controls.Add(this.TpgAttachment);
            this.Tpg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Tpg.Location = new System.Drawing.Point(0, 270);
            this.Tpg.Multiline = true;
            this.Tpg.Name = "Tpg";
            this.Tpg.SelectedIndex = 0;
            this.Tpg.ShowToolTips = true;
            this.Tpg.Size = new System.Drawing.Size(929, 310);
            this.Tpg.TabIndex = 143;
            // 
            // TpgSalary
            // 
            this.TpgSalary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgSalary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgSalary.Controls.Add(this.TxtDtlSalary);
            this.TpgSalary.Controls.Add(this.label55);
            this.TpgSalary.Controls.Add(this.TxtDtlPension);
            this.TpgSalary.Controls.Add(this.label16);
            this.TpgSalary.Controls.Add(this.TxtDtlNonPension);
            this.TpgSalary.Controls.Add(this.label17);
            this.TpgSalary.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgSalary.Location = new System.Drawing.Point(4, 26);
            this.TpgSalary.Name = "TpgSalary";
            this.TpgSalary.Size = new System.Drawing.Size(921, 280);
            this.TpgSalary.TabIndex = 0;
            this.TpgSalary.Text = "Basic Salary";
            this.TpgSalary.UseVisualStyleBackColor = true;
            // 
            // TxtDtlSalary
            // 
            this.TxtDtlSalary.EnterMoveNextControl = true;
            this.TxtDtlSalary.Location = new System.Drawing.Point(149, 53);
            this.TxtDtlSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSalary.Name = "TxtDtlSalary";
            this.TxtDtlSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSalary.Properties.MaxLength = 12;
            this.TxtDtlSalary.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSalary.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSalary.TabIndex = 73;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(106, 55);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(38, 14);
            this.label55.TabIndex = 72;
            this.label55.Text = "Salary";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlPension
            // 
            this.TxtDtlPension.EnterMoveNextControl = true;
            this.TxtDtlPension.Location = new System.Drawing.Point(149, 11);
            this.TxtDtlPension.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlPension.Name = "TxtDtlPension";
            this.TxtDtlPension.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlPension.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlPension.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlPension.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlPension.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlPension.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlPension.Properties.MaxLength = 12;
            this.TxtDtlPension.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlPension.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlPension.TabIndex = 69;
            this.TxtDtlPension.Validated += new System.EventHandler(this.TxtPension_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(30, 13);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 14);
            this.label16.TabIndex = 67;
            this.label16.Text = "Pension Basic Salary";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlNonPension
            // 
            this.TxtDtlNonPension.EnterMoveNextControl = true;
            this.TxtDtlNonPension.Location = new System.Drawing.Point(149, 32);
            this.TxtDtlNonPension.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlNonPension.Name = "TxtDtlNonPension";
            this.TxtDtlNonPension.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlNonPension.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlNonPension.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlNonPension.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlNonPension.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlNonPension.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlNonPension.Properties.MaxLength = 12;
            this.TxtDtlNonPension.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlNonPension.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlNonPension.TabIndex = 71;
            this.TxtDtlNonPension.Validated += new System.EventHandler(this.TxtNonPension_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(4, 35);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(140, 14);
            this.label17.TabIndex = 70;
            this.label17.Text = "Non Pension Basic Salary";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgBenefit
            // 
            this.TpgBenefit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgBenefit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgBenefit.Controls.Add(this.TxtDtlRetirementCapital);
            this.TpgBenefit.Controls.Add(this.label63);
            this.TpgBenefit.Controls.Add(this.TxtDtlPensionType);
            this.TpgBenefit.Controls.Add(this.label56);
            this.TpgBenefit.Controls.Add(this.TxtDtlEmprFee);
            this.TpgBenefit.Controls.Add(this.label20);
            this.TpgBenefit.Controls.Add(this.TxtDtlEmpFee);
            this.TpgBenefit.Controls.Add(this.label19);
            this.TpgBenefit.Controls.Add(this.TxtDtlRetirement);
            this.TpgBenefit.Controls.Add(this.label18);
            this.TpgBenefit.Location = new System.Drawing.Point(4, 26);
            this.TpgBenefit.Name = "TpgBenefit";
            this.TpgBenefit.Size = new System.Drawing.Size(921, 280);
            this.TpgBenefit.TabIndex = 1;
            this.TpgBenefit.Text = "Retirement Benefit";
            this.TpgBenefit.UseVisualStyleBackColor = true;
            // 
            // TxtDtlRetirementCapital
            // 
            this.TxtDtlRetirementCapital.EnterMoveNextControl = true;
            this.TxtDtlRetirementCapital.Location = new System.Drawing.Point(133, 99);
            this.TxtDtlRetirementCapital.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlRetirementCapital.Name = "TxtDtlRetirementCapital";
            this.TxtDtlRetirementCapital.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlRetirementCapital.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlRetirementCapital.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlRetirementCapital.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlRetirementCapital.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlRetirementCapital.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlRetirementCapital.Properties.MaxLength = 12;
            this.TxtDtlRetirementCapital.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlRetirementCapital.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlRetirementCapital.TabIndex = 83;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(19, 102);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(107, 14);
            this.label63.TabIndex = 82;
            this.label63.Text = "Retirement Capital";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlPensionType
            // 
            this.TxtDtlPensionType.EnterMoveNextControl = true;
            this.TxtDtlPensionType.Location = new System.Drawing.Point(133, 15);
            this.TxtDtlPensionType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlPensionType.Name = "TxtDtlPensionType";
            this.TxtDtlPensionType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlPensionType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlPensionType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlPensionType.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlPensionType.Properties.MaxLength = 16;
            this.TxtDtlPensionType.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlPensionType.TabIndex = 75;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(49, 18);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(77, 14);
            this.label56.TabIndex = 74;
            this.label56.Text = "PensionType";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlEmprFee
            // 
            this.TxtDtlEmprFee.EnterMoveNextControl = true;
            this.TxtDtlEmprFee.Location = new System.Drawing.Point(133, 78);
            this.TxtDtlEmprFee.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlEmprFee.Name = "TxtDtlEmprFee";
            this.TxtDtlEmprFee.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlEmprFee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlEmprFee.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlEmprFee.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlEmprFee.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlEmprFee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlEmprFee.Properties.MaxLength = 12;
            this.TxtDtlEmprFee.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlEmprFee.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlEmprFee.TabIndex = 81;
            this.TxtDtlEmprFee.Validated += new System.EventHandler(this.TxtDtlEmprFee_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(45, 80);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 14);
            this.label20.TabIndex = 80;
            this.label20.Text = "Employer Fee";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlEmpFee
            // 
            this.TxtDtlEmpFee.EnterMoveNextControl = true;
            this.TxtDtlEmpFee.Location = new System.Drawing.Point(133, 57);
            this.TxtDtlEmpFee.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlEmpFee.Name = "TxtDtlEmpFee";
            this.TxtDtlEmpFee.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlEmpFee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlEmpFee.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlEmpFee.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlEmpFee.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlEmpFee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlEmpFee.Properties.MaxLength = 12;
            this.TxtDtlEmpFee.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlEmpFee.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlEmpFee.TabIndex = 79;
            this.TxtDtlEmpFee.Validated += new System.EventHandler(this.TxtDtlEmpFee_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(42, 59);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 14);
            this.label19.TabIndex = 78;
            this.label19.Text = "Employee Fee";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlRetirement
            // 
            this.TxtDtlRetirement.EnterMoveNextControl = true;
            this.TxtDtlRetirement.Location = new System.Drawing.Point(133, 36);
            this.TxtDtlRetirement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlRetirement.Name = "TxtDtlRetirement";
            this.TxtDtlRetirement.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlRetirement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlRetirement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlRetirement.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlRetirement.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlRetirement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlRetirement.Properties.MaxLength = 12;
            this.TxtDtlRetirement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlRetirement.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlRetirement.TabIndex = 77;
            this.TxtDtlRetirement.EditValueChanged += new System.EventHandler(this.TxtDtlRetirement_EditValueChanged);
            this.TxtDtlRetirement.Validated += new System.EventHandler(this.TxtDtlRetirement_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(15, 38);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 14);
            this.label18.TabIndex = 76;
            this.label18.Text = "Retirement Benefit";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgSeverance
            // 
            this.TpgSeverance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgSeverance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgSeverance.Controls.Add(this.panel5);
            this.TpgSeverance.Controls.Add(this.panel3);
            this.TpgSeverance.Location = new System.Drawing.Point(4, 26);
            this.TpgSeverance.Name = "TpgSeverance";
            this.TpgSeverance.Padding = new System.Windows.Forms.Padding(3);
            this.TpgSeverance.Size = new System.Drawing.Size(921, 280);
            this.TpgSeverance.TabIndex = 2;
            this.TpgSeverance.Text = "Severance & Compensation";
            this.TpgSeverance.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.TxtDtlSevPay5);
            this.panel5.Controls.Add(this.TxtDtlSevPay3);
            this.panel5.Controls.Add(this.label58);
            this.panel5.Controls.Add(this.TxtDtlResidual);
            this.panel5.Controls.Add(this.TxtDtlSevPay4);
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.TxtDtlSevLongService);
            this.panel5.Controls.Add(this.label33);
            this.panel5.Controls.Add(this.label37);
            this.panel5.Controls.Add(this.label34);
            this.panel5.Controls.Add(this.TxtDtlProsentase);
            this.panel5.Controls.Add(this.TxtDtlSalary3);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.TxtDtlDivider);
            this.panel5.Controls.Add(this.label35);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(458, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(456, 270);
            this.panel5.TabIndex = 1;
            // 
            // TxtDtlSevPay5
            // 
            this.TxtDtlSevPay5.EnterMoveNextControl = true;
            this.TxtDtlSevPay5.Location = new System.Drawing.Point(197, 171);
            this.TxtDtlSevPay5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay5.Name = "TxtDtlSevPay5";
            this.TxtDtlSevPay5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay5.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay5.Properties.MaxLength = 12;
            this.TxtDtlSevPay5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay5.Size = new System.Drawing.Size(173, 20);
            this.TxtDtlSevPay5.TabIndex = 115;
            // 
            // TxtDtlSevPay3
            // 
            this.TxtDtlSevPay3.EnterMoveNextControl = true;
            this.TxtDtlSevPay3.Location = new System.Drawing.Point(197, 52);
            this.TxtDtlSevPay3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay3.Name = "TxtDtlSevPay3";
            this.TxtDtlSevPay3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay3.Properties.MaxLength = 12;
            this.TxtDtlSevPay3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay3.Size = new System.Drawing.Size(178, 20);
            this.TxtDtlSevPay3.TabIndex = 107;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(82, 173);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(112, 14);
            this.label58.TabIndex = 114;
            this.label58.Text = "Severance Amount";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlResidual
            // 
            this.TxtDtlResidual.EnterMoveNextControl = true;
            this.TxtDtlResidual.Location = new System.Drawing.Point(197, 9);
            this.TxtDtlResidual.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlResidual.Name = "TxtDtlResidual";
            this.TxtDtlResidual.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlResidual.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlResidual.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlResidual.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlResidual.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlResidual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlResidual.Properties.MaxLength = 12;
            this.TxtDtlResidual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlResidual.Size = new System.Drawing.Size(73, 20);
            this.TxtDtlResidual.TabIndex = 101;
            this.TxtDtlResidual.Validated += new System.EventHandler(this.TxtDtlResidual_Validated);
            // 
            // TxtDtlSevPay4
            // 
            this.TxtDtlSevPay4.EnterMoveNextControl = true;
            this.TxtDtlSevPay4.Location = new System.Drawing.Point(197, 150);
            this.TxtDtlSevPay4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay4.Name = "TxtDtlSevPay4";
            this.TxtDtlSevPay4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay4.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay4.Properties.MaxLength = 12;
            this.TxtDtlSevPay4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay4.Size = new System.Drawing.Size(173, 20);
            this.TxtDtlSevPay4.TabIndex = 113;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(50, 152);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(144, 14);
            this.label39.TabIndex = 112;
            this.label39.Text = "Compensation Rights pay";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(96, 56);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(98, 14);
            this.label57.TabIndex = 106;
            this.label57.Text = "Residual Amount";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlSevLongService
            // 
            this.TxtDtlSevLongService.EnterMoveNextControl = true;
            this.TxtDtlSevLongService.Location = new System.Drawing.Point(197, 108);
            this.TxtDtlSevLongService.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevLongService.Name = "TxtDtlSevLongService";
            this.TxtDtlSevLongService.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevLongService.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevLongService.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevLongService.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevLongService.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevLongService.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevLongService.Properties.MaxLength = 12;
            this.TxtDtlSevLongService.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevLongService.Size = new System.Drawing.Size(173, 20);
            this.TxtDtlSevLongService.TabIndex = 109;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(108, 13);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(86, 14);
            this.label33.TabIndex = 100;
            this.label33.Text = "Residual Leave";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(3, 111);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(191, 14);
            this.label37.TabIndex = 108;
            this.label37.Text = "Severance And Long Service  Pay";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(274, 11);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(43, 14);
            this.label34.TabIndex = 102;
            this.label34.Text = "Divider";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlProsentase
            // 
            this.TxtDtlProsentase.EnterMoveNextControl = true;
            this.TxtDtlProsentase.Location = new System.Drawing.Point(197, 129);
            this.TxtDtlProsentase.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlProsentase.Name = "TxtDtlProsentase";
            this.TxtDtlProsentase.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlProsentase.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlProsentase.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlProsentase.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlProsentase.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlProsentase.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlProsentase.Properties.MaxLength = 12;
            this.TxtDtlProsentase.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlProsentase.Size = new System.Drawing.Size(42, 20);
            this.TxtDtlProsentase.TabIndex = 110;
            // 
            // TxtDtlSalary3
            // 
            this.TxtDtlSalary3.EnterMoveNextControl = true;
            this.TxtDtlSalary3.Location = new System.Drawing.Point(197, 31);
            this.TxtDtlSalary3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSalary3.Name = "TxtDtlSalary3";
            this.TxtDtlSalary3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSalary3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSalary3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSalary3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSalary3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSalary3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSalary3.Properties.MaxLength = 12;
            this.TxtDtlSalary3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSalary3.Size = new System.Drawing.Size(178, 20);
            this.TxtDtlSalary3.TabIndex = 105;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(245, 131);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 14);
            this.label38.TabIndex = 111;
            this.label38.Text = "%";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlDivider
            // 
            this.TxtDtlDivider.EnterMoveNextControl = true;
            this.TxtDtlDivider.Location = new System.Drawing.Point(321, 9);
            this.TxtDtlDivider.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlDivider.Name = "TxtDtlDivider";
            this.TxtDtlDivider.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlDivider.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlDivider.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlDivider.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlDivider.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlDivider.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlDivider.Properties.MaxLength = 12;
            this.TxtDtlDivider.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlDivider.Size = new System.Drawing.Size(54, 20);
            this.TxtDtlDivider.TabIndex = 103;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(126, 34);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 14);
            this.label35.TabIndex = 104;
            this.label35.Text = "Basic Salary";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtDtlSevPay2);
            this.panel3.Controls.Add(this.TxtDtlSevPay1);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.TxtDtlSalary2);
            this.panel3.Controls.Add(this.TxtDtlSalary1);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.TxtDtlSevGrp2);
            this.panel3.Controls.Add(this.TxtDtlSevGrp1);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.LueDtlKoefisien2);
            this.panel3.Controls.Add(this.LueDtlKoefisien1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(455, 270);
            this.panel3.TabIndex = 0;
            // 
            // TxtDtlSevPay2
            // 
            this.TxtDtlSevPay2.EnterMoveNextControl = true;
            this.TxtDtlSevPay2.Location = new System.Drawing.Point(115, 175);
            this.TxtDtlSevPay2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay2.Name = "TxtDtlSevPay2";
            this.TxtDtlSevPay2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay2.Properties.MaxLength = 12;
            this.TxtDtlSevPay2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSevPay2.TabIndex = 99;
            // 
            // TxtDtlSevPay1
            // 
            this.TxtDtlSevPay1.EnterMoveNextControl = true;
            this.TxtDtlSevPay1.Location = new System.Drawing.Point(115, 69);
            this.TxtDtlSevPay1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay1.Name = "TxtDtlSevPay1";
            this.TxtDtlSevPay1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay1.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay1.Properties.MaxLength = 12;
            this.TxtDtlSevPay1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay1.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSevPay1.TabIndex = 91;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(9, 178);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 14);
            this.label25.TabIndex = 98;
            this.label25.Text = "Long Service Pay";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(22, 72);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 14);
            this.label24.TabIndex = 90;
            this.label24.Text = "Severance Pay";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlSalary2
            // 
            this.TxtDtlSalary2.EnterMoveNextControl = true;
            this.TxtDtlSalary2.Location = new System.Drawing.Point(115, 154);
            this.TxtDtlSalary2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSalary2.Name = "TxtDtlSalary2";
            this.TxtDtlSalary2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSalary2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSalary2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSalary2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSalary2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSalary2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSalary2.Properties.MaxLength = 12;
            this.TxtDtlSalary2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSalary2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSalary2.TabIndex = 97;
            // 
            // TxtDtlSalary1
            // 
            this.TxtDtlSalary1.EnterMoveNextControl = true;
            this.TxtDtlSalary1.Location = new System.Drawing.Point(115, 48);
            this.TxtDtlSalary1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSalary1.Name = "TxtDtlSalary1";
            this.TxtDtlSalary1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSalary1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSalary1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSalary1.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSalary1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSalary1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSalary1.Properties.MaxLength = 12;
            this.TxtDtlSalary1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSalary1.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSalary1.TabIndex = 89;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(41, 157);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 14);
            this.label26.TabIndex = 96;
            this.label26.Text = "Basic Salary";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(41, 51);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 14);
            this.label23.TabIndex = 88;
            this.label23.Text = "Basic Salary";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlSevGrp2
            // 
            this.TxtDtlSevGrp2.EnterMoveNextControl = true;
            this.TxtDtlSevGrp2.Location = new System.Drawing.Point(115, 133);
            this.TxtDtlSevGrp2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevGrp2.Name = "TxtDtlSevGrp2";
            this.TxtDtlSevGrp2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevGrp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevGrp2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevGrp2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevGrp2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevGrp2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevGrp2.Properties.MaxLength = 12;
            this.TxtDtlSevGrp2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevGrp2.Size = new System.Drawing.Size(73, 20);
            this.TxtDtlSevGrp2.TabIndex = 95;
            // 
            // TxtDtlSevGrp1
            // 
            this.TxtDtlSevGrp1.EnterMoveNextControl = true;
            this.TxtDtlSevGrp1.Location = new System.Drawing.Point(115, 27);
            this.TxtDtlSevGrp1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevGrp1.Name = "TxtDtlSevGrp1";
            this.TxtDtlSevGrp1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevGrp1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevGrp1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevGrp1.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevGrp1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevGrp1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevGrp1.Properties.MaxLength = 12;
            this.TxtDtlSevGrp1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevGrp1.Size = new System.Drawing.Size(73, 20);
            this.TxtDtlSevGrp1.TabIndex = 87;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(8, 135);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 14);
            this.label27.TabIndex = 94;
            this.label27.Text = "Severance Group";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(8, 29);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 14);
            this.label22.TabIndex = 86;
            this.label22.Text = "Severance Group";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(44, 115);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 14);
            this.label32.TabIndex = 92;
            this.label32.Text = "Coefficient";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(44, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 14);
            this.label21.TabIndex = 84;
            this.label21.Text = "Coefficient";
            // 
            // LueDtlKoefisien2
            // 
            this.LueDtlKoefisien2.EnterMoveNextControl = true;
            this.LueDtlKoefisien2.Location = new System.Drawing.Point(115, 112);
            this.LueDtlKoefisien2.Margin = new System.Windows.Forms.Padding(5);
            this.LueDtlKoefisien2.Name = "LueDtlKoefisien2";
            this.LueDtlKoefisien2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien2.Properties.Appearance.Options.UseFont = true;
            this.LueDtlKoefisien2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDtlKoefisien2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDtlKoefisien2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDtlKoefisien2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDtlKoefisien2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDtlKoefisien2.Properties.DropDownRows = 12;
            this.LueDtlKoefisien2.Properties.MaxLength = 16;
            this.LueDtlKoefisien2.Properties.NullText = "[Empty]";
            this.LueDtlKoefisien2.Properties.PopupWidth = 500;
            this.LueDtlKoefisien2.Size = new System.Drawing.Size(72, 20);
            this.LueDtlKoefisien2.TabIndex = 93;
            this.LueDtlKoefisien2.ToolTip = "F4 : Show/hide list";
            this.LueDtlKoefisien2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDtlKoefisien2.EditValueChanged += new System.EventHandler(this.LueDtlKoefisien2_EditValueChanged);
            // 
            // LueDtlKoefisien1
            // 
            this.LueDtlKoefisien1.EnterMoveNextControl = true;
            this.LueDtlKoefisien1.Location = new System.Drawing.Point(115, 6);
            this.LueDtlKoefisien1.Margin = new System.Windows.Forms.Padding(5);
            this.LueDtlKoefisien1.Name = "LueDtlKoefisien1";
            this.LueDtlKoefisien1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien1.Properties.Appearance.Options.UseFont = true;
            this.LueDtlKoefisien1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDtlKoefisien1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDtlKoefisien1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDtlKoefisien1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDtlKoefisien1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDtlKoefisien1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDtlKoefisien1.Properties.DropDownRows = 12;
            this.LueDtlKoefisien1.Properties.MaxLength = 16;
            this.LueDtlKoefisien1.Properties.NullText = "[Empty]";
            this.LueDtlKoefisien1.Properties.PopupWidth = 500;
            this.LueDtlKoefisien1.Size = new System.Drawing.Size(72, 20);
            this.LueDtlKoefisien1.TabIndex = 85;
            this.LueDtlKoefisien1.ToolTip = "F4 : Show/hide list";
            this.LueDtlKoefisien1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDtlKoefisien1.EditValueChanged += new System.EventHandler(this.LueDtlKoefisien1_EditValueChanged);
            // 
            // TpgAccrued
            // 
            this.TpgAccrued.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgAccrued.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgAccrued.Controls.Add(this.TxtDtlRetirementCapital2);
            this.TpgAccrued.Controls.Add(this.label65);
            this.TpgAccrued.Controls.Add(this.TxtDtlAccruedExp);
            this.TpgAccrued.Controls.Add(this.label43);
            this.TpgAccrued.Controls.Add(this.TxtDtlCorporateFinancial);
            this.TpgAccrued.Controls.Add(this.label42);
            this.TpgAccrued.Controls.Add(this.TxtDtlEmprFee2);
            this.TpgAccrued.Controls.Add(this.label41);
            this.TpgAccrued.Controls.Add(this.TxtDtlSevPay6);
            this.TpgAccrued.Controls.Add(this.label40);
            this.TpgAccrued.Location = new System.Drawing.Point(4, 26);
            this.TpgAccrued.Name = "TpgAccrued";
            this.TpgAccrued.Padding = new System.Windows.Forms.Padding(3);
            this.TpgAccrued.Size = new System.Drawing.Size(921, 280);
            this.TpgAccrued.TabIndex = 3;
            this.TpgAccrued.Text = "Accrued Expenses";
            this.TpgAccrued.UseVisualStyleBackColor = true;
            // 
            // TxtDtlRetirementCapital2
            // 
            this.TxtDtlRetirementCapital2.EnterMoveNextControl = true;
            this.TxtDtlRetirementCapital2.Location = new System.Drawing.Point(181, 72);
            this.TxtDtlRetirementCapital2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlRetirementCapital2.Name = "TxtDtlRetirementCapital2";
            this.TxtDtlRetirementCapital2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlRetirementCapital2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlRetirementCapital2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlRetirementCapital2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlRetirementCapital2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlRetirementCapital2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlRetirementCapital2.Properties.MaxLength = 12;
            this.TxtDtlRetirementCapital2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlRetirementCapital2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlRetirementCapital2.TabIndex = 122;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(67, 74);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(107, 14);
            this.label65.TabIndex = 121;
            this.label65.Text = "Retirement Capital";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlAccruedExp
            // 
            this.TxtDtlAccruedExp.EnterMoveNextControl = true;
            this.TxtDtlAccruedExp.Location = new System.Drawing.Point(181, 142);
            this.TxtDtlAccruedExp.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlAccruedExp.Name = "TxtDtlAccruedExp";
            this.TxtDtlAccruedExp.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlAccruedExp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlAccruedExp.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlAccruedExp.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlAccruedExp.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlAccruedExp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlAccruedExp.Properties.MaxLength = 12;
            this.TxtDtlAccruedExp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlAccruedExp.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlAccruedExp.TabIndex = 124;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(67, 145);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(107, 14);
            this.label43.TabIndex = 123;
            this.label43.Text = "Accrued Expenses";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlCorporateFinancial
            // 
            this.TxtDtlCorporateFinancial.EnterMoveNextControl = true;
            this.TxtDtlCorporateFinancial.Location = new System.Drawing.Point(181, 50);
            this.TxtDtlCorporateFinancial.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlCorporateFinancial.Name = "TxtDtlCorporateFinancial";
            this.TxtDtlCorporateFinancial.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlCorporateFinancial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlCorporateFinancial.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlCorporateFinancial.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlCorporateFinancial.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlCorporateFinancial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlCorporateFinancial.Properties.MaxLength = 12;
            this.TxtDtlCorporateFinancial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlCorporateFinancial.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlCorporateFinancial.TabIndex = 120;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(5, 51);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(169, 14);
            this.label42.TabIndex = 119;
            this.label42.Text = "Corporate Financial Assistance";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlEmprFee2
            // 
            this.TxtDtlEmprFee2.EnterMoveNextControl = true;
            this.TxtDtlEmprFee2.Location = new System.Drawing.Point(181, 29);
            this.TxtDtlEmprFee2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlEmprFee2.Name = "TxtDtlEmprFee2";
            this.TxtDtlEmprFee2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlEmprFee2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlEmprFee2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlEmprFee2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlEmprFee2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlEmprFee2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlEmprFee2.Properties.MaxLength = 12;
            this.TxtDtlEmprFee2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlEmprFee2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlEmprFee2.TabIndex = 119;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(93, 32);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 14);
            this.label41.TabIndex = 118;
            this.label41.Text = "Employer Fee";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlSevPay6
            // 
            this.TxtDtlSevPay6.EnterMoveNextControl = true;
            this.TxtDtlSevPay6.Location = new System.Drawing.Point(181, 8);
            this.TxtDtlSevPay6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlSevPay6.Name = "TxtDtlSevPay6";
            this.TxtDtlSevPay6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlSevPay6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlSevPay6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlSevPay6.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlSevPay6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlSevPay6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlSevPay6.Properties.MaxLength = 12;
            this.TxtDtlSevPay6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlSevPay6.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlSevPay6.TabIndex = 117;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(87, 10);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(87, 14);
            this.label40.TabIndex = 116;
            this.label40.Text = "Severance Pay";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgRecvPay
            // 
            this.TpgRecvPay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgRecvPay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgRecvPay.Controls.Add(this.TxtDtlRetirementCapital3);
            this.TpgRecvPay.Controls.Add(this.label64);
            this.TpgRecvPay.Controls.Add(this.TxtDtlReceivedPay);
            this.TpgRecvPay.Controls.Add(this.label49);
            this.TpgRecvPay.Controls.Add(this.TxtDtlAccruedExp2);
            this.TpgRecvPay.Controls.Add(this.label48);
            this.TpgRecvPay.Controls.Add(this.TxtDtlCorporateDonation);
            this.TpgRecvPay.Controls.Add(this.label47);
            this.TpgRecvPay.Controls.Add(this.TxtDtlCorporateFinancial2);
            this.TpgRecvPay.Controls.Add(this.label46);
            this.TpgRecvPay.Controls.Add(this.TxtDtlEmprFee3);
            this.TpgRecvPay.Controls.Add(this.label45);
            this.TpgRecvPay.Controls.Add(this.TxtDtlEmpFee2);
            this.TpgRecvPay.Controls.Add(this.label44);
            this.TpgRecvPay.Location = new System.Drawing.Point(4, 26);
            this.TpgRecvPay.Name = "TpgRecvPay";
            this.TpgRecvPay.Padding = new System.Windows.Forms.Padding(3);
            this.TpgRecvPay.Size = new System.Drawing.Size(921, 280);
            this.TpgRecvPay.TabIndex = 4;
            this.TpgRecvPay.Text = "Received Pay";
            this.TpgRecvPay.UseVisualStyleBackColor = true;
            // 
            // TxtDtlRetirementCapital3
            // 
            this.TxtDtlRetirementCapital3.EnterMoveNextControl = true;
            this.TxtDtlRetirementCapital3.Location = new System.Drawing.Point(190, 113);
            this.TxtDtlRetirementCapital3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlRetirementCapital3.Name = "TxtDtlRetirementCapital3";
            this.TxtDtlRetirementCapital3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlRetirementCapital3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlRetirementCapital3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlRetirementCapital3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlRetirementCapital3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlRetirementCapital3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlRetirementCapital3.Properties.MaxLength = 12;
            this.TxtDtlRetirementCapital3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlRetirementCapital3.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlRetirementCapital3.TabIndex = 136;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(76, 116);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(107, 14);
            this.label64.TabIndex = 135;
            this.label64.Text = "Retirement Capital";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlReceivedPay
            // 
            this.TxtDtlReceivedPay.EnterMoveNextControl = true;
            this.TxtDtlReceivedPay.Location = new System.Drawing.Point(190, 134);
            this.TxtDtlReceivedPay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlReceivedPay.Name = "TxtDtlReceivedPay";
            this.TxtDtlReceivedPay.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlReceivedPay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlReceivedPay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlReceivedPay.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlReceivedPay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlReceivedPay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlReceivedPay.Properties.MaxLength = 12;
            this.TxtDtlReceivedPay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlReceivedPay.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlReceivedPay.TabIndex = 138;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(104, 135);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(79, 14);
            this.label49.TabIndex = 137;
            this.label49.Text = "Received Pay";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlAccruedExp2
            // 
            this.TxtDtlAccruedExp2.EnterMoveNextControl = true;
            this.TxtDtlAccruedExp2.Location = new System.Drawing.Point(190, 92);
            this.TxtDtlAccruedExp2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlAccruedExp2.Name = "TxtDtlAccruedExp2";
            this.TxtDtlAccruedExp2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlAccruedExp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlAccruedExp2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlAccruedExp2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlAccruedExp2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlAccruedExp2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlAccruedExp2.Properties.MaxLength = 12;
            this.TxtDtlAccruedExp2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlAccruedExp2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlAccruedExp2.TabIndex = 134;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(76, 93);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(107, 14);
            this.label48.TabIndex = 133;
            this.label48.Text = "Accrued Expenses";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlCorporateDonation
            // 
            this.TxtDtlCorporateDonation.EnterMoveNextControl = true;
            this.TxtDtlCorporateDonation.Location = new System.Drawing.Point(190, 71);
            this.TxtDtlCorporateDonation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlCorporateDonation.Name = "TxtDtlCorporateDonation";
            this.TxtDtlCorporateDonation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlCorporateDonation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlCorporateDonation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlCorporateDonation.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlCorporateDonation.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlCorporateDonation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlCorporateDonation.Properties.MaxLength = 12;
            this.TxtDtlCorporateDonation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlCorporateDonation.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlCorporateDonation.TabIndex = 132;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(69, 72);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(114, 14);
            this.label47.TabIndex = 131;
            this.label47.Text = "Corporate Donation";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlCorporateFinancial2
            // 
            this.TxtDtlCorporateFinancial2.EnterMoveNextControl = true;
            this.TxtDtlCorporateFinancial2.Location = new System.Drawing.Point(190, 50);
            this.TxtDtlCorporateFinancial2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlCorporateFinancial2.Name = "TxtDtlCorporateFinancial2";
            this.TxtDtlCorporateFinancial2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlCorporateFinancial2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlCorporateFinancial2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlCorporateFinancial2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlCorporateFinancial2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlCorporateFinancial2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlCorporateFinancial2.Properties.MaxLength = 12;
            this.TxtDtlCorporateFinancial2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlCorporateFinancial2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlCorporateFinancial2.TabIndex = 130;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(14, 51);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(169, 14);
            this.label46.TabIndex = 129;
            this.label46.Text = "Corporate Financial Assistance";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlEmprFee3
            // 
            this.TxtDtlEmprFee3.EnterMoveNextControl = true;
            this.TxtDtlEmprFee3.Location = new System.Drawing.Point(190, 29);
            this.TxtDtlEmprFee3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlEmprFee3.Name = "TxtDtlEmprFee3";
            this.TxtDtlEmprFee3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlEmprFee3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlEmprFee3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlEmprFee3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlEmprFee3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlEmprFee3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlEmprFee3.Properties.MaxLength = 12;
            this.TxtDtlEmprFee3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlEmprFee3.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlEmprFee3.TabIndex = 128;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(102, 31);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(81, 14);
            this.label45.TabIndex = 127;
            this.label45.Text = "Employer Fee";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlEmpFee2
            // 
            this.TxtDtlEmpFee2.EnterMoveNextControl = true;
            this.TxtDtlEmpFee2.Location = new System.Drawing.Point(190, 8);
            this.TxtDtlEmpFee2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlEmpFee2.Name = "TxtDtlEmpFee2";
            this.TxtDtlEmpFee2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlEmpFee2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlEmpFee2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlEmpFee2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlEmpFee2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlEmpFee2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlEmpFee2.Properties.MaxLength = 12;
            this.TxtDtlEmpFee2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlEmpFee2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlEmpFee2.TabIndex = 126;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(99, 10);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(84, 14);
            this.label44.TabIndex = 125;
            this.label44.Text = "Employee Fee";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgPayment
            // 
            this.TpgPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgPayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgPayment.Controls.Add(this.TxtDtlAmtAfterTax);
            this.TpgPayment.Controls.Add(this.label62);
            this.TpgPayment.Controls.Add(this.TxtDtlTax2);
            this.TpgPayment.Controls.Add(this.label61);
            this.TpgPayment.Controls.Add(this.TxtDtlTax);
            this.TpgPayment.Controls.Add(this.label60);
            this.TpgPayment.Controls.Add(this.TxtDtlAmtBeforeTax);
            this.TpgPayment.Controls.Add(this.label59);
            this.TpgPayment.Controls.Add(this.TxtDtlCorporateDonation2);
            this.TpgPayment.Controls.Add(this.label54);
            this.TpgPayment.Controls.Add(this.TxtDtlCorporateFinancial3);
            this.TpgPayment.Controls.Add(this.label53);
            this.TpgPayment.Controls.Add(this.TxtDtlAccruedExp3);
            this.TpgPayment.Controls.Add(this.label52);
            this.TpgPayment.Location = new System.Drawing.Point(4, 26);
            this.TpgPayment.Name = "TpgPayment";
            this.TpgPayment.Padding = new System.Windows.Forms.Padding(3);
            this.TpgPayment.Size = new System.Drawing.Size(921, 280);
            this.TpgPayment.TabIndex = 5;
            this.TpgPayment.Text = "Payment Received Directly";
            this.TpgPayment.UseVisualStyleBackColor = true;
            // 
            // TxtDtlAmtAfterTax
            // 
            this.TxtDtlAmtAfterTax.EnterMoveNextControl = true;
            this.TxtDtlAmtAfterTax.Location = new System.Drawing.Point(195, 169);
            this.TxtDtlAmtAfterTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlAmtAfterTax.Name = "TxtDtlAmtAfterTax";
            this.TxtDtlAmtAfterTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlAmtAfterTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlAmtAfterTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlAmtAfterTax.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlAmtAfterTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlAmtAfterTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlAmtAfterTax.Properties.MaxLength = 12;
            this.TxtDtlAmtAfterTax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlAmtAfterTax.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlAmtAfterTax.TabIndex = 153;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(79, 171);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(107, 14);
            this.label62.TabIndex = 152;
            this.label62.Text = "Amount After Tax";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlTax2
            // 
            this.TxtDtlTax2.EnterMoveNextControl = true;
            this.TxtDtlTax2.Location = new System.Drawing.Point(195, 124);
            this.TxtDtlTax2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlTax2.Name = "TxtDtlTax2";
            this.TxtDtlTax2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlTax2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlTax2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlTax2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlTax2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlTax2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlTax2.Properties.MaxLength = 12;
            this.TxtDtlTax2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlTax2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlTax2.TabIndex = 150;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(9, 125);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(177, 28);
            this.label61.TabIndex = 149;
            this.label61.Text = "PPH 21 On Corporate Financial \r\nAssistance And Donation";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlTax
            // 
            this.TxtDtlTax.EnterMoveNextControl = true;
            this.TxtDtlTax.Location = new System.Drawing.Point(195, 103);
            this.TxtDtlTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlTax.Name = "TxtDtlTax";
            this.TxtDtlTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlTax.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlTax.Properties.MaxLength = 12;
            this.TxtDtlTax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlTax.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlTax.TabIndex = 148;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(35, 105);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(151, 14);
            this.label60.TabIndex = 147;
            this.label60.Text = "PPH 21 On Severance Pay";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlAmtBeforeTax
            // 
            this.TxtDtlAmtBeforeTax.EnterMoveNextControl = true;
            this.TxtDtlAmtBeforeTax.Location = new System.Drawing.Point(195, 68);
            this.TxtDtlAmtBeforeTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlAmtBeforeTax.Name = "TxtDtlAmtBeforeTax";
            this.TxtDtlAmtBeforeTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlAmtBeforeTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlAmtBeforeTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlAmtBeforeTax.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlAmtBeforeTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlAmtBeforeTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlAmtBeforeTax.Properties.MaxLength = 12;
            this.TxtDtlAmtBeforeTax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlAmtBeforeTax.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlAmtBeforeTax.TabIndex = 146;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(71, 70);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(115, 14);
            this.label59.TabIndex = 145;
            this.label59.Text = "Amount Before Tax";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlCorporateDonation2
            // 
            this.TxtDtlCorporateDonation2.EnterMoveNextControl = true;
            this.TxtDtlCorporateDonation2.Location = new System.Drawing.Point(195, 47);
            this.TxtDtlCorporateDonation2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlCorporateDonation2.Name = "TxtDtlCorporateDonation2";
            this.TxtDtlCorporateDonation2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlCorporateDonation2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlCorporateDonation2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlCorporateDonation2.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlCorporateDonation2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlCorporateDonation2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlCorporateDonation2.Properties.MaxLength = 12;
            this.TxtDtlCorporateDonation2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlCorporateDonation2.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlCorporateDonation2.TabIndex = 144;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(72, 48);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(114, 14);
            this.label54.TabIndex = 143;
            this.label54.Text = "Corporate Donation";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlCorporateFinancial3
            // 
            this.TxtDtlCorporateFinancial3.EnterMoveNextControl = true;
            this.TxtDtlCorporateFinancial3.Location = new System.Drawing.Point(195, 26);
            this.TxtDtlCorporateFinancial3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlCorporateFinancial3.Name = "TxtDtlCorporateFinancial3";
            this.TxtDtlCorporateFinancial3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlCorporateFinancial3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlCorporateFinancial3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlCorporateFinancial3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlCorporateFinancial3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlCorporateFinancial3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlCorporateFinancial3.Properties.MaxLength = 12;
            this.TxtDtlCorporateFinancial3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlCorporateFinancial3.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlCorporateFinancial3.TabIndex = 142;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(17, 27);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(169, 14);
            this.label53.TabIndex = 141;
            this.label53.Text = "Corporate Financial Assistance";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDtlAccruedExp3
            // 
            this.TxtDtlAccruedExp3.EnterMoveNextControl = true;
            this.TxtDtlAccruedExp3.Location = new System.Drawing.Point(195, 5);
            this.TxtDtlAccruedExp3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDtlAccruedExp3.Name = "TxtDtlAccruedExp3";
            this.TxtDtlAccruedExp3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDtlAccruedExp3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDtlAccruedExp3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDtlAccruedExp3.Properties.Appearance.Options.UseFont = true;
            this.TxtDtlAccruedExp3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDtlAccruedExp3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDtlAccruedExp3.Properties.MaxLength = 12;
            this.TxtDtlAccruedExp3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDtlAccruedExp3.Size = new System.Drawing.Size(164, 20);
            this.TxtDtlAccruedExp3.TabIndex = 140;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(79, 6);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(107, 14);
            this.label52.TabIndex = 139;
            this.label52.Text = "Accrued Expenses";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgDeduction
            // 
            this.TpgDeduction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgDeduction.Controls.Add(this.MeeDeductionRemark);
            this.TpgDeduction.Controls.Add(this.label69);
            this.TpgDeduction.Controls.Add(this.TxtDeductionAmt);
            this.TpgDeduction.Controls.Add(this.label68);
            this.TpgDeduction.Location = new System.Drawing.Point(4, 26);
            this.TpgDeduction.Name = "TpgDeduction";
            this.TpgDeduction.Size = new System.Drawing.Size(921, 280);
            this.TpgDeduction.TabIndex = 6;
            this.TpgDeduction.Text = "Deduction";
            this.TpgDeduction.UseVisualStyleBackColor = true;
            // 
            // MeeDeductionRemark
            // 
            this.MeeDeductionRemark.EnterMoveNextControl = true;
            this.MeeDeductionRemark.Location = new System.Drawing.Point(135, 31);
            this.MeeDeductionRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDeductionRemark.Name = "MeeDeductionRemark";
            this.MeeDeductionRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDeductionRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeDeductionRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDeductionRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDeductionRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDeductionRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDeductionRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDeductionRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDeductionRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDeductionRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDeductionRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDeductionRemark.Properties.MaxLength = 400;
            this.MeeDeductionRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeDeductionRemark.Properties.ShowIcon = false;
            this.MeeDeductionRemark.Size = new System.Drawing.Size(355, 20);
            this.MeeDeductionRemark.TabIndex = 144;
            this.MeeDeductionRemark.ToolTip = "F4 : Show/hide text";
            this.MeeDeductionRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDeductionRemark.ToolTipTitle = "Run System";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(82, 34);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(47, 14);
            this.label69.TabIndex = 143;
            this.label69.Text = "Remark";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeductionAmt
            // 
            this.TxtDeductionAmt.EnterMoveNextControl = true;
            this.TxtDeductionAmt.Location = new System.Drawing.Point(135, 10);
            this.TxtDeductionAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeductionAmt.Name = "TxtDeductionAmt";
            this.TxtDeductionAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeductionAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeductionAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeductionAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtDeductionAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDeductionAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDeductionAmt.Properties.MaxLength = 12;
            this.TxtDeductionAmt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtDeductionAmt.Size = new System.Drawing.Size(164, 20);
            this.TxtDeductionAmt.TabIndex = 142;
            this.TxtDeductionAmt.Validated += new System.EventHandler(this.TxtDeductionAmt_Validated);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(18, 11);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(111, 14);
            this.label68.TabIndex = 141;
            this.label68.Text = "Deduction Amount";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtVRDocNo);
            this.panel4.Controls.Add(this.label66);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.DteRetiredDt);
            this.panel4.Controls.Add(this.BtnRecvDirect);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtHdrRecvDirect);
            this.panel4.Controls.Add(this.DteLeaveStartDt);
            this.panel4.Controls.Add(this.label51);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.BtnRecvPay);
            this.panel4.Controls.Add(this.TxtHdrReceivedPay);
            this.panel4.Controls.Add(this.label50);
            this.panel4.Controls.Add(this.TxtHdrSalary);
            this.panel4.Controls.Add(this.TxtYearsWorked);
            this.panel4.Controls.Add(this.DteJoinDt);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.BtnBenefit);
            this.panel4.Controls.Add(this.TxtHdrSeverance);
            this.panel4.Controls.Add(this.BtnSalary);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.btnAccExp);
            this.panel4.Controls.Add(this.TxtHdrAccruedExp);
            this.panel4.Controls.Add(this.TxtHdrRetirement);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.BtnSevPay);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(512, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(417, 270);
            this.panel4.TabIndex = 144;
            // 
            // TxtVRDocNo
            // 
            this.TxtVRDocNo.EnterMoveNextControl = true;
            this.TxtVRDocNo.Location = new System.Drawing.Point(180, 220);
            this.TxtVRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVRDocNo.Name = "TxtVRDocNo";
            this.TxtVRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVRDocNo.Properties.MaxLength = 16;
            this.TxtVRDocNo.Size = new System.Drawing.Size(196, 20);
            this.TxtVRDocNo.TabIndex = 64;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(72, 223);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(102, 14);
            this.label66.TabIndex = 63;
            this.label66.Text = "Voucher Request";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(180, 242);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(228, 20);
            this.MeeRemark.TabIndex = 66;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(127, 245);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 14);
            this.label13.TabIndex = 65;
            this.label13.Text = "Remark";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteRetiredDt
            // 
            this.DteRetiredDt.EditValue = null;
            this.DteRetiredDt.EnterMoveNextControl = true;
            this.DteRetiredDt.Location = new System.Drawing.Point(180, 49);
            this.DteRetiredDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRetiredDt.Name = "DteRetiredDt";
            this.DteRetiredDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRetiredDt.Properties.Appearance.Options.UseFont = true;
            this.DteRetiredDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRetiredDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRetiredDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRetiredDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRetiredDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRetiredDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRetiredDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRetiredDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRetiredDt.Properties.MaxLength = 16;
            this.DteRetiredDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRetiredDt.Size = new System.Drawing.Size(105, 20);
            this.DteRetiredDt.TabIndex = 42;
            // 
            // BtnRecvDirect
            // 
            this.BtnRecvDirect.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvDirect.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvDirect.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvDirect.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvDirect.Appearance.Options.UseBackColor = true;
            this.BtnRecvDirect.Appearance.Options.UseFont = true;
            this.BtnRecvDirect.Appearance.Options.UseForeColor = true;
            this.BtnRecvDirect.Appearance.Options.UseTextOptions = true;
            this.BtnRecvDirect.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvDirect.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvDirect.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvDirect.Image")));
            this.BtnRecvDirect.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvDirect.Location = new System.Drawing.Point(384, 198);
            this.BtnRecvDirect.Name = "BtnRecvDirect";
            this.BtnRecvDirect.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvDirect.TabIndex = 62;
            this.BtnRecvDirect.ToolTip = "Find Employee";
            this.BtnRecvDirect.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvDirect.ToolTipTitle = "Run System";
            this.BtnRecvDirect.Click += new System.EventHandler(this.BtnRecvDirect_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(50, 51);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 14);
            this.label7.TabIndex = 41;
            this.label7.Text = "Retired / Resign Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHdrRecvDirect
            // 
            this.TxtHdrRecvDirect.EnterMoveNextControl = true;
            this.TxtHdrRecvDirect.Location = new System.Drawing.Point(180, 198);
            this.TxtHdrRecvDirect.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrRecvDirect.Name = "TxtHdrRecvDirect";
            this.TxtHdrRecvDirect.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrRecvDirect.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrRecvDirect.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrRecvDirect.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrRecvDirect.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrRecvDirect.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrRecvDirect.Properties.MaxLength = 12;
            this.TxtHdrRecvDirect.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrRecvDirect.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrRecvDirect.TabIndex = 61;
            // 
            // DteLeaveStartDt
            // 
            this.DteLeaveStartDt.EditValue = null;
            this.DteLeaveStartDt.EnterMoveNextControl = true;
            this.DteLeaveStartDt.Location = new System.Drawing.Point(180, 27);
            this.DteLeaveStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt.Name = "DteLeaveStartDt";
            this.DteLeaveStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.MaxLength = 16;
            this.DteLeaveStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt.Size = new System.Drawing.Size(105, 20);
            this.DteLeaveStartDt.TabIndex = 40;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(5, 200);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(169, 14);
            this.label51.TabIndex = 60;
            this.label51.Text = "Payment Received Directly";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(65, 30);
            this.label30.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(109, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Leave\'s Initial Date";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRecvPay
            // 
            this.BtnRecvPay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecvPay.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecvPay.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecvPay.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecvPay.Appearance.Options.UseBackColor = true;
            this.BtnRecvPay.Appearance.Options.UseFont = true;
            this.BtnRecvPay.Appearance.Options.UseForeColor = true;
            this.BtnRecvPay.Appearance.Options.UseTextOptions = true;
            this.BtnRecvPay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecvPay.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecvPay.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecvPay.Image")));
            this.BtnRecvPay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecvPay.Location = new System.Drawing.Point(384, 177);
            this.BtnRecvPay.Name = "BtnRecvPay";
            this.BtnRecvPay.Size = new System.Drawing.Size(24, 21);
            this.BtnRecvPay.TabIndex = 59;
            this.BtnRecvPay.ToolTip = "Find Employee";
            this.BtnRecvPay.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecvPay.ToolTipTitle = "Run System";
            this.BtnRecvPay.Click += new System.EventHandler(this.BtnRecvPay_Click);
            // 
            // TxtHdrReceivedPay
            // 
            this.TxtHdrReceivedPay.EnterMoveNextControl = true;
            this.TxtHdrReceivedPay.Location = new System.Drawing.Point(180, 177);
            this.TxtHdrReceivedPay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrReceivedPay.Name = "TxtHdrReceivedPay";
            this.TxtHdrReceivedPay.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrReceivedPay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrReceivedPay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrReceivedPay.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrReceivedPay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrReceivedPay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrReceivedPay.Properties.MaxLength = 12;
            this.TxtHdrReceivedPay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrReceivedPay.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrReceivedPay.TabIndex = 58;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(87, 180);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(87, 14);
            this.label50.TabIndex = 57;
            this.label50.Text = "Received Pay";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHdrSalary
            // 
            this.TxtHdrSalary.EnterMoveNextControl = true;
            this.TxtHdrSalary.Location = new System.Drawing.Point(180, 92);
            this.TxtHdrSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrSalary.Name = "TxtHdrSalary";
            this.TxtHdrSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrSalary.Properties.MaxLength = 12;
            this.TxtHdrSalary.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrSalary.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrSalary.TabIndex = 46;
            this.TxtHdrSalary.Validated += new System.EventHandler(this.TxtSalary_Validated);
            // 
            // TxtYearsWorked
            // 
            this.TxtYearsWorked.EnterMoveNextControl = true;
            this.TxtYearsWorked.Location = new System.Drawing.Point(180, 71);
            this.TxtYearsWorked.Margin = new System.Windows.Forms.Padding(5);
            this.TxtYearsWorked.Name = "TxtYearsWorked";
            this.TxtYearsWorked.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtYearsWorked.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYearsWorked.Properties.Appearance.Options.UseBackColor = true;
            this.TxtYearsWorked.Properties.Appearance.Options.UseFont = true;
            this.TxtYearsWorked.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtYearsWorked.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtYearsWorked.Properties.MaxLength = 12;
            this.TxtYearsWorked.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtYearsWorked.Size = new System.Drawing.Size(105, 20);
            this.TxtYearsWorked.TabIndex = 44;
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(180, 5);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.MaxLength = 16;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(105, 20);
            this.DteJoinDt.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(116, 8);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 14);
            this.label8.TabIndex = 37;
            this.label8.Text = "Join Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(26, 74);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 14);
            this.label9.TabIndex = 43;
            this.label9.Text = "Number Of Years Worked";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(130, 95);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 14);
            this.label10.TabIndex = 45;
            this.label10.Text = "Salary";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(79, 137);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 14);
            this.label12.TabIndex = 51;
            this.label12.Text = "Severance Pay";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnBenefit
            // 
            this.BtnBenefit.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBenefit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBenefit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBenefit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBenefit.Appearance.Options.UseBackColor = true;
            this.BtnBenefit.Appearance.Options.UseFont = true;
            this.BtnBenefit.Appearance.Options.UseForeColor = true;
            this.BtnBenefit.Appearance.Options.UseTextOptions = true;
            this.BtnBenefit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBenefit.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBenefit.Image = ((System.Drawing.Image)(resources.GetObject("BtnBenefit.Image")));
            this.BtnBenefit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBenefit.Location = new System.Drawing.Point(384, 114);
            this.BtnBenefit.Name = "BtnBenefit";
            this.BtnBenefit.Size = new System.Drawing.Size(24, 21);
            this.BtnBenefit.TabIndex = 50;
            this.BtnBenefit.ToolTip = "Find Employee";
            this.BtnBenefit.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBenefit.ToolTipTitle = "Run System";
            this.BtnBenefit.Click += new System.EventHandler(this.BtnBenefit_Click);
            // 
            // TxtHdrSeverance
            // 
            this.TxtHdrSeverance.EnterMoveNextControl = true;
            this.TxtHdrSeverance.Location = new System.Drawing.Point(180, 134);
            this.TxtHdrSeverance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrSeverance.Name = "TxtHdrSeverance";
            this.TxtHdrSeverance.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrSeverance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrSeverance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrSeverance.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrSeverance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrSeverance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrSeverance.Properties.MaxLength = 12;
            this.TxtHdrSeverance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrSeverance.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrSeverance.TabIndex = 52;
            // 
            // BtnSalary
            // 
            this.BtnSalary.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalary.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSalary.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalary.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSalary.Appearance.Options.UseBackColor = true;
            this.BtnSalary.Appearance.Options.UseFont = true;
            this.BtnSalary.Appearance.Options.UseForeColor = true;
            this.BtnSalary.Appearance.Options.UseTextOptions = true;
            this.BtnSalary.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSalary.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSalary.Image = ((System.Drawing.Image)(resources.GetObject("BtnSalary.Image")));
            this.BtnSalary.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSalary.Location = new System.Drawing.Point(384, 92);
            this.BtnSalary.Name = "BtnSalary";
            this.BtnSalary.Size = new System.Drawing.Size(24, 21);
            this.BtnSalary.TabIndex = 47;
            this.BtnSalary.ToolTip = "Find Employee";
            this.BtnSalary.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSalary.ToolTipTitle = "Run System";
            this.BtnSalary.Click += new System.EventHandler(this.BtnSalary_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(58, 158);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 14);
            this.label11.TabIndex = 54;
            this.label11.Text = "Accrued Expenses";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAccExp
            // 
            this.btnAccExp.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnAccExp.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.btnAccExp.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccExp.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnAccExp.Appearance.Options.UseBackColor = true;
            this.btnAccExp.Appearance.Options.UseFont = true;
            this.btnAccExp.Appearance.Options.UseForeColor = true;
            this.btnAccExp.Appearance.Options.UseTextOptions = true;
            this.btnAccExp.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnAccExp.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnAccExp.Image = ((System.Drawing.Image)(resources.GetObject("btnAccExp.Image")));
            this.btnAccExp.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAccExp.Location = new System.Drawing.Point(384, 155);
            this.btnAccExp.Name = "btnAccExp";
            this.btnAccExp.Size = new System.Drawing.Size(24, 21);
            this.btnAccExp.TabIndex = 56;
            this.btnAccExp.ToolTip = "Find Employee";
            this.btnAccExp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.btnAccExp.ToolTipTitle = "Run System";
            this.btnAccExp.Click += new System.EventHandler(this.btnAccExp_Click);
            // 
            // TxtHdrAccruedExp
            // 
            this.TxtHdrAccruedExp.EnterMoveNextControl = true;
            this.TxtHdrAccruedExp.Location = new System.Drawing.Point(180, 155);
            this.TxtHdrAccruedExp.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrAccruedExp.Name = "TxtHdrAccruedExp";
            this.TxtHdrAccruedExp.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrAccruedExp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrAccruedExp.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrAccruedExp.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrAccruedExp.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrAccruedExp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrAccruedExp.Properties.MaxLength = 12;
            this.TxtHdrAccruedExp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrAccruedExp.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrAccruedExp.TabIndex = 55;
            // 
            // TxtHdrRetirement
            // 
            this.TxtHdrRetirement.EnterMoveNextControl = true;
            this.TxtHdrRetirement.Location = new System.Drawing.Point(180, 113);
            this.TxtHdrRetirement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHdrRetirement.Name = "TxtHdrRetirement";
            this.TxtHdrRetirement.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHdrRetirement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHdrRetirement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHdrRetirement.Properties.Appearance.Options.UseFont = true;
            this.TxtHdrRetirement.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHdrRetirement.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHdrRetirement.Properties.MaxLength = 12;
            this.TxtHdrRetirement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtHdrRetirement.Size = new System.Drawing.Size(196, 20);
            this.TxtHdrRetirement.TabIndex = 49;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(44, 117);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(130, 14);
            this.label36.TabIndex = 48;
            this.label36.Text = "Retirement Benefits";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSevPay
            // 
            this.BtnSevPay.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSevPay.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSevPay.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSevPay.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSevPay.Appearance.Options.UseBackColor = true;
            this.BtnSevPay.Appearance.Options.UseFont = true;
            this.BtnSevPay.Appearance.Options.UseForeColor = true;
            this.BtnSevPay.Appearance.Options.UseTextOptions = true;
            this.BtnSevPay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSevPay.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSevPay.Image = ((System.Drawing.Image)(resources.GetObject("BtnSevPay.Image")));
            this.BtnSevPay.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSevPay.Location = new System.Drawing.Point(384, 134);
            this.BtnSevPay.Name = "BtnSevPay";
            this.BtnSevPay.Size = new System.Drawing.Size(24, 21);
            this.BtnSevPay.TabIndex = 53;
            this.BtnSevPay.ToolTip = "Find Employee";
            this.BtnSevPay.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSevPay.ToolTipTitle = "Run System";
            this.BtnSevPay.Click += new System.EventHandler(this.BtnSevPay_Click);
            // 
            // TxtSiteName
            // 
            this.TxtSiteName.EnterMoveNextControl = true;
            this.TxtSiteName.Location = new System.Drawing.Point(163, 157);
            this.TxtSiteName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteName.Name = "TxtSiteName";
            this.TxtSiteName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSiteName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteName.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteName.Properties.MaxLength = 16;
            this.TxtSiteName.Size = new System.Drawing.Size(261, 20);
            this.TxtSiteName.TabIndex = 27;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(128, 160);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 14);
            this.label28.TabIndex = 26;
            this.label28.Text = "Site";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(163, 73);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(261, 20);
            this.MeeCancelReason.TabIndex = 16;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 76);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 14);
            this.label6.TabIndex = 15;
            this.label6.Text = "Reason For Cancellation";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(433, 74);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 17;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(163, 51);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(166, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(114, 54);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 13;
            this.label14.Text = "Status";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(163, 115);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 16;
            this.TxtEmpName.Size = new System.Drawing.Size(331, 20);
            this.TxtEmpName.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(61, 118);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 14);
            this.label15.TabIndex = 22;
            this.label15.Text = "Employee Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(163, 94);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Size = new System.Drawing.Size(261, 20);
            this.TxtEmpCode.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(64, 97);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Employee Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(163, 29);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(62, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Document Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(163, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(45, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document Number";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBirthPlace
            // 
            this.TxtBirthPlace.EnterMoveNextControl = true;
            this.TxtBirthPlace.Location = new System.Drawing.Point(163, 241);
            this.TxtBirthPlace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBirthPlace.Name = "TxtBirthPlace";
            this.TxtBirthPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlace.Properties.MaxLength = 80;
            this.TxtBirthPlace.Size = new System.Drawing.Size(223, 20);
            this.TxtBirthPlace.TabIndex = 35;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(38, 243);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(118, 14);
            this.label31.TabIndex = 34;
            this.label31.Text = "Birth Place and Date";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIDNumber
            // 
            this.TxtIDNumber.EnterMoveNextControl = true;
            this.TxtIDNumber.Location = new System.Drawing.Point(163, 178);
            this.TxtIDNumber.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIDNumber.Name = "TxtIDNumber";
            this.TxtIDNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIDNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIDNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIDNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtIDNumber.Properties.MaxLength = 16;
            this.TxtIDNumber.Size = new System.Drawing.Size(261, 20);
            this.TxtIDNumber.TabIndex = 29;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(106, 179);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(50, 14);
            this.label29.TabIndex = 28;
            this.label29.Text = "NIB/NIP";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(388, 241);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(105, 20);
            this.DteBirthDt.TabIndex = 36;
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(163, 220);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 16;
            this.TxtPosition.Size = new System.Drawing.Size(331, 20);
            this.TxtPosition.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(107, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(163, 199);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 16;
            this.TxtDeptName.Size = new System.Drawing.Size(331, 20);
            this.TxtDeptName.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(83, 202);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 30;
            this.label3.Text = "Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProcess
            // 
            this.TxtProcess.EnterMoveNextControl = true;
            this.TxtProcess.Location = new System.Drawing.Point(163, 136);
            this.TxtProcess.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProcess.Name = "TxtProcess";
            this.TxtProcess.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProcess.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProcess.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProcess.Properties.Appearance.Options.UseFont = true;
            this.TxtProcess.Properties.MaxLength = 16;
            this.TxtProcess.Properties.ReadOnly = true;
            this.TxtProcess.Size = new System.Drawing.Size(166, 20);
            this.TxtProcess.TabIndex = 25;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(92, 140);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(64, 14);
            this.label67.TabIndex = 24;
            this.label67.Text = "Process to";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(467, 94);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode2.TabIndex = 21;
            this.BtnEmpCode2.ToolTip = "Show Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click_1);
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(437, 94);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 20;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click_1);
            // 
            // TpgAttachment
            // 
            this.TpgAttachment.Controls.Add(this.panel6);
            this.TpgAttachment.Location = new System.Drawing.Point(4, 26);
            this.TpgAttachment.Name = "TpgAttachment";
            this.TpgAttachment.Size = new System.Drawing.Size(921, 280);
            this.TpgAttachment.TabIndex = 7;
            this.TpgAttachment.Text = "Attachment";
            this.TpgAttachment.UseVisualStyleBackColor = true;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(135, 31);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 400;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(355, 20);
            this.memoExEdit1.TabIndex = 144;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(82, 34);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(47, 14);
            this.label70.TabIndex = 143;
            this.label70.Text = "Remark";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(135, 10);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit1.Properties.MaxLength = 12;
            this.textEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textEdit1.Size = new System.Drawing.Size(164, 20);
            this.textEdit1.TabIndex = 142;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(18, 11);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(111, 14);
            this.label71.TabIndex = 141;
            this.label71.Text = "Deduction Amount";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.PbUpload);
            this.panel6.Controls.Add(this.TxtFile);
            this.panel6.Controls.Add(this.label72);
            this.panel6.Controls.Add(this.BtnDownload);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(921, 280);
            this.panel6.TabIndex = 145;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(35, 31);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(442, 23);
            this.PbUpload.TabIndex = 84;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(35, 9);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 82;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(8, 11);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(24, 14);
            this.label72.TabIndex = 81;
            this.label72.Text = "File";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(449, 7);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 83;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // FrmEmpSeverance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 580);
            this.Name = "FrmEmpSeverance";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Tpg.ResumeLayout(false);
            this.TpgSalary.ResumeLayout(false);
            this.TpgSalary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlPension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlNonPension.Properties)).EndInit();
            this.TpgBenefit.ResumeLayout(false);
            this.TpgBenefit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlPensionType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmpFee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirement.Properties)).EndInit();
            this.TpgSeverance.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlResidual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevLongService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlProsentase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlDivider.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSalary1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevGrp2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevGrp1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDtlKoefisien2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDtlKoefisien1.Properties)).EndInit();
            this.TpgAccrued.ResumeLayout(false);
            this.TpgAccrued.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlSevPay6.Properties)).EndInit();
            this.TpgRecvPay.ResumeLayout(false);
            this.TpgRecvPay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlRetirementCapital3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlReceivedPay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateDonation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmprFee3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlEmpFee2.Properties)).EndInit();
            this.TpgPayment.ResumeLayout(false);
            this.TpgPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAmtAfterTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlTax2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAmtBeforeTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateDonation2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlCorporateFinancial3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDtlAccruedExp3.Properties)).EndInit();
            this.TpgDeduction.ResumeLayout(false);
            this.TpgDeduction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDeductionRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeductionAmt.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRetiredDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRetiredDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrRecvDirect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrReceivedPay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYearsWorked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrSeverance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrAccruedExp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHdrRetirement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIDNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProcess.Properties)).EndInit();
            this.TpgAttachment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tpg;
        private System.Windows.Forms.TabPage TpgSalary;
        private System.Windows.Forms.TabPage TpgBenefit;
        protected System.Windows.Forms.Panel panel4;
        public DevExpress.XtraEditors.TextEdit TxtSiteName;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        public DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlace;
        private System.Windows.Forms.Label label31;
        public DevExpress.XtraEditors.TextEdit TxtIDNumber;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtYearsWorked;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        public DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtDeptName;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtHdrSalary;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.SimpleButton BtnBenefit;
        internal DevExpress.XtraEditors.TextEdit TxtHdrSeverance;
        public DevExpress.XtraEditors.SimpleButton BtnSalary;
        public DevExpress.XtraEditors.SimpleButton btnAccExp;
        internal DevExpress.XtraEditors.TextEdit TxtHdrAccruedExp;
        internal DevExpress.XtraEditors.TextEdit TxtHdrRetirement;
        private System.Windows.Forms.Label label36;
        public DevExpress.XtraEditors.SimpleButton BtnSevPay;
        private System.Windows.Forms.TabPage TpgSeverance;
        private System.Windows.Forms.TabPage TpgAccrued;
        private System.Windows.Forms.TabPage TpgRecvPay;
        private System.Windows.Forms.TabPage TpgPayment;
        internal DevExpress.XtraEditors.TextEdit TxtDtlPension;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtDtlNonPension;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDtlRetirement;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtDtlEmprFee;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtDtlEmpFee;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay1;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSalary1;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevGrp1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueDtlKoefisien1;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay2;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSalary2;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevGrp2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraEditors.LookUpEdit LueDtlKoefisien2;
        internal DevExpress.XtraEditors.TextEdit TxtDtlResidual;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSalary3;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtDtlDivider;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtDtlProsentase;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay4;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevLongService;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtDtlCorporateFinancial;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtDtlEmprFee2;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay6;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtDtlAccruedExp;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtDtlEmprFee3;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtDtlEmpFee2;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtDtlCorporateFinancial2;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtDtlAccruedExp2;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit TxtDtlCorporateDonation;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.TextEdit TxtDtlReceivedPay;
        private System.Windows.Forms.Label label49;
        public DevExpress.XtraEditors.SimpleButton BtnRecvPay;
        internal DevExpress.XtraEditors.TextEdit TxtHdrReceivedPay;
        private System.Windows.Forms.Label label50;
        public DevExpress.XtraEditors.SimpleButton BtnRecvDirect;
        internal DevExpress.XtraEditors.TextEdit TxtHdrRecvDirect;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.TextEdit TxtDtlAccruedExp3;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.TextEdit TxtDtlCorporateFinancial3;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.TextEdit TxtDtlCorporateDonation2;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSalary;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        public DevExpress.XtraEditors.TextEdit TxtDtlPensionType;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay3;
        private System.Windows.Forms.Label label57;
        internal DevExpress.XtraEditors.TextEdit TxtDtlSevPay5;
        private System.Windows.Forms.Label label58;
        internal DevExpress.XtraEditors.DateEdit DteRetiredDt;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtDtlAmtBeforeTax;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtDtlTax;
        private System.Windows.Forms.Label label60;
        internal DevExpress.XtraEditors.TextEdit TxtDtlAmtAfterTax;
        private System.Windows.Forms.Label label62;
        internal DevExpress.XtraEditors.TextEdit TxtDtlTax2;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.TextEdit TxtDtlRetirementCapital;
        private System.Windows.Forms.Label label63;
        internal DevExpress.XtraEditors.TextEdit TxtDtlRetirementCapital2;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.TextEdit TxtDtlRetirementCapital3;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.TextEdit TxtVRDocNo;
        private System.Windows.Forms.Label label66;
        internal DevExpress.XtraEditors.TextEdit TxtProcess;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TabPage TpgDeduction;
        private DevExpress.XtraEditors.MemoExEdit MeeDeductionRemark;
        private System.Windows.Forms.Label label69;
        internal DevExpress.XtraEditors.TextEdit TxtDeductionAmt;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TabPage TpgAttachment;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label70;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label71;
        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label72;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD;
    }
}