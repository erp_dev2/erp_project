﻿namespace RunSystem
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TmAutoLogOff = new System.Windows.Forms.Timer(this.components);
            this.TmShowHide = new System.Windows.Forms.Timer(this.components);
            this.PnlBase = new System.Windows.Forms.Panel();
            this.PnlSide = new System.Windows.Forms.Panel();
            this.BtnShowHide = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PnlNews = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LblMeritIncreaseNotification = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.PnlSelfOnBoarding = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.PnlWidget = new System.Windows.Forms.Panel();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.PnlWidget2 = new System.Windows.Forms.Panel();
            this.RtbWidget2 = new System.Windows.Forms.RichTextBox();
            this.LblWidget2 = new System.Windows.Forms.Label();
            this.PnlWidget1 = new System.Windows.Forms.Panel();
            this.RtbWidget1 = new System.Windows.Forms.RichTextBox();
            this.LblWidget1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LogoSelf = new System.Windows.Forms.PictureBox();
            this.statusStrip1.SuspendLayout();
            this.PnlBase.SuspendLayout();
            this.PnlSide.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.PnlNews.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.PnlSelfOnBoarding.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.PnlWidget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            this.PnlWidget2.SuspendLayout();
            this.PnlWidget1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoSelf)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 727);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(988, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // TmAutoLogOff
            // 
            this.TmAutoLogOff.Tick += new System.EventHandler(this.TmAutoLogOff_Tick);
            // 
            // TmShowHide
            // 
            this.TmShowHide.Interval = 20;
            this.TmShowHide.Tick += new System.EventHandler(this.TmShowHide_Tick);
            // 
            // PnlBase
            // 
            this.PnlBase.BackColor = System.Drawing.Color.White;
            this.PnlBase.Controls.Add(this.PnlSide);
            this.PnlBase.Controls.Add(this.panel1);
            this.PnlBase.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlBase.Location = new System.Drawing.Point(573, 0);
            this.PnlBase.Name = "PnlBase";
            this.PnlBase.Size = new System.Drawing.Size(415, 749);
            this.PnlBase.TabIndex = 64;
            // 
            // PnlSide
            // 
            this.PnlSide.BackColor = System.Drawing.Color.SteelBlue;
            this.PnlSide.Controls.Add(this.BtnShowHide);
            this.PnlSide.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlSide.Location = new System.Drawing.Point(396, 0);
            this.PnlSide.Name = "PnlSide";
            this.PnlSide.Size = new System.Drawing.Size(19, 749);
            this.PnlSide.TabIndex = 4;
            // 
            // BtnShowHide
            // 
            this.BtnShowHide.FlatAppearance.BorderSize = 0;
            this.BtnShowHide.Location = new System.Drawing.Point(0, 0);
            this.BtnShowHide.Name = "BtnShowHide";
            this.BtnShowHide.Size = new System.Drawing.Size(19, 75);
            this.BtnShowHide.TabIndex = 7;
            this.BtnShowHide.Text = "HIDE";
            this.BtnShowHide.UseVisualStyleBackColor = true;
            this.BtnShowHide.Click += new System.EventHandler(this.BtnShowHide_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(397, 749);
            this.panel1.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PnlNews);
            this.panel3.Controls.Add(this.PnlSelfOnBoarding);
            this.panel3.Controls.Add(this.PnlWidget);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(397, 674);
            this.panel3.TabIndex = 59;
            // 
            // PnlNews
            // 
            this.PnlNews.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PnlNews.BackColor = System.Drawing.Color.White;
            this.PnlNews.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlNews.Controls.Add(this.panel5);
            this.PnlNews.Controls.Add(this.panel4);
            this.PnlNews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlNews.Location = new System.Drawing.Point(0, 185);
            this.PnlNews.Name = "PnlNews";
            this.PnlNews.Size = new System.Drawing.Size(397, 304);
            this.PnlNews.TabIndex = 60;
            // 
            // panel5
            // 
            this.panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 135);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(395, 167);
            this.panel5.TabIndex = 62;
            // 
            // panel8
            // 
            this.panel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.LblMeritIncreaseNotification);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 42);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(393, 39);
            this.panel8.TabIndex = 63;
            // 
            // LblMeritIncreaseNotification
            // 
            this.LblMeritIncreaseNotification.AutoSize = true;
            this.LblMeritIncreaseNotification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LblMeritIncreaseNotification.ForeColor = System.Drawing.Color.Red;
            this.LblMeritIncreaseNotification.Location = new System.Drawing.Point(0, 0);
            this.LblMeritIncreaseNotification.Name = "LblMeritIncreaseNotification";
            this.LblMeritIncreaseNotification.Size = new System.Drawing.Size(0, 16);
            this.LblMeritIncreaseNotification.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.Grd4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 81);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(393, 84);
            this.panel7.TabIndex = 64;
            // 
            // Grd4
            // 
            this.Grd4.AutoHeightRowMode = TenTec.Windows.iGridLib.iGAutoHeightRowMode.Cells;
            this.Grd4.AutoWidthColMode = TenTec.Windows.iGridLib.iGAutoWidthColMode.Cells;
            this.Grd4.BorderColor = System.Drawing.Color.Transparent;
            this.Grd4.CellCtrlBackColor = System.Drawing.Color.Transparent;
            this.Grd4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Grd4.DefaultAutoGroupRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd4.DefaultCol.AllowGrouping = false;
            this.Grd4.DefaultCol.AllowMoving = false;
            this.Grd4.DefaultCol.AllowSizing = false;
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Selectable = false;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.DefaultRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.FocusRect = false;
            this.Grd4.FocusRectColor1 = System.Drawing.Color.Transparent;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.HorizontalLastRow = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd4.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.VerticalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.Header.AllowPress = false;
            this.Grd4.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.Header.AutoHeightFlags = ((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags)((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnAddCol | TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnContentsChange)));
            this.Grd4.Header.DrawSystem = false;
            this.Grd4.Header.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.Header.SeparatingLine = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.Header.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.Header.Visible = false;
            this.Grd4.HighlightSelCells = false;
            this.Grd4.ImmediateColResizing = false;
            this.Grd4.ImmediateRowResizing = false;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.PrefixGroupValues = false;
            this.Grd4.PressedMouseMoveMode = TenTec.Windows.iGridLib.iGPressedMouseMoveMode.Normal;
            this.Grd4.ProcessEnter = false;
            this.Grd4.RowHeader.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.RowHeader.DrawSystem = false;
            this.Grd4.RowHeader.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.RowHeader.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd4.RowHeader.Width = 0;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd4.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.None;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(391, 82);
            this.Grd4.TabIndex = 68;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.Color.Transparent;
            this.Grd4.TreeLines.ShowRootLines = false;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            // 
            // panel6
            // 
            this.panel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.Grd3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(393, 42);
            this.panel6.TabIndex = 62;
            // 
            // Grd3
            // 
            this.Grd3.AutoHeightRowMode = TenTec.Windows.iGridLib.iGAutoHeightRowMode.Cells;
            this.Grd3.AutoWidthColMode = TenTec.Windows.iGridLib.iGAutoWidthColMode.Cells;
            this.Grd3.BorderColor = System.Drawing.Color.Transparent;
            this.Grd3.CellCtrlBackColor = System.Drawing.Color.Transparent;
            this.Grd3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Grd3.DefaultAutoGroupRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd3.DefaultCol.AllowGrouping = false;
            this.Grd3.DefaultCol.AllowMoving = false;
            this.Grd3.DefaultCol.AllowSizing = false;
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Selectable = false;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.DefaultRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.FocusRect = false;
            this.Grd3.FocusRectColor1 = System.Drawing.Color.Transparent;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.HorizontalLastRow = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd3.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.VerticalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.Header.AllowPress = false;
            this.Grd3.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd3.Header.AutoHeightFlags = ((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags)((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnAddCol | TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnContentsChange)));
            this.Grd3.Header.DrawSystem = false;
            this.Grd3.Header.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.Header.SeparatingLine = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.Header.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.Header.Visible = false;
            this.Grd3.HighlightSelCells = false;
            this.Grd3.ImmediateColResizing = false;
            this.Grd3.ImmediateRowResizing = false;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.PrefixGroupValues = false;
            this.Grd3.PressedMouseMoveMode = TenTec.Windows.iGridLib.iGPressedMouseMoveMode.Normal;
            this.Grd3.ProcessEnter = false;
            this.Grd3.RowHeader.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd3.RowHeader.DrawSystem = false;
            this.Grd3.RowHeader.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.RowHeader.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd3.RowHeader.Width = 0;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd3.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.None;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(391, 40);
            this.Grd3.TabIndex = 66;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.Color.Transparent;
            this.Grd3.TreeLines.ShowRootLines = false;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.Grd2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(395, 135);
            this.panel4.TabIndex = 61;
            // 
            // Grd2
            // 
            this.Grd2.AutoHeightRowMode = TenTec.Windows.iGridLib.iGAutoHeightRowMode.Cells;
            this.Grd2.AutoWidthColMode = TenTec.Windows.iGridLib.iGAutoWidthColMode.Cells;
            this.Grd2.BorderColor = System.Drawing.Color.Transparent;
            this.Grd2.CellCtrlBackColor = System.Drawing.Color.Transparent;
            this.Grd2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Grd2.DefaultAutoGroupRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd2.DefaultCol.AllowGrouping = false;
            this.Grd2.DefaultCol.AllowMoving = false;
            this.Grd2.DefaultCol.AllowSizing = false;
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Selectable = false;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.DefaultRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.FocusRect = false;
            this.Grd2.FocusRectColor1 = System.Drawing.Color.Transparent;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.HorizontalLastRow = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.VerticalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.Header.AllowPress = false;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.AutoHeightFlags = ((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags)((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnAddCol | TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnContentsChange)));
            this.Grd2.Header.DrawSystem = false;
            this.Grd2.Header.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.Header.SeparatingLine = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.Header.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.Header.Visible = false;
            this.Grd2.HighlightSelCells = false;
            this.Grd2.ImmediateColResizing = false;
            this.Grd2.ImmediateRowResizing = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.PrefixGroupValues = false;
            this.Grd2.PressedMouseMoveMode = TenTec.Windows.iGridLib.iGPressedMouseMoveMode.Normal;
            this.Grd2.ProcessEnter = false;
            this.Grd2.RowHeader.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.RowHeader.DrawSystem = false;
            this.Grd2.RowHeader.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.RowHeader.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd2.RowHeader.Width = 0;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd2.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.None;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(393, 133);
            this.Grd2.TabIndex = 58;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.Color.Transparent;
            this.Grd2.TreeLines.ShowRootLines = false;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            // 
            // PnlSelfOnBoarding
            // 
            this.PnlSelfOnBoarding.BackColor = System.Drawing.Color.White;
            this.PnlSelfOnBoarding.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlSelfOnBoarding.Controls.Add(this.Grd1);
            this.PnlSelfOnBoarding.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlSelfOnBoarding.Location = new System.Drawing.Point(0, 0);
            this.PnlSelfOnBoarding.Name = "PnlSelfOnBoarding";
            this.PnlSelfOnBoarding.Size = new System.Drawing.Size(397, 185);
            this.PnlSelfOnBoarding.TabIndex = 60;
            // 
            // Grd1
            // 
            this.Grd1.AutoHeightRowMode = TenTec.Windows.iGridLib.iGAutoHeightRowMode.Cells;
            this.Grd1.AutoWidthColMode = TenTec.Windows.iGridLib.iGAutoWidthColMode.Cells;
            this.Grd1.BorderColor = System.Drawing.Color.Transparent;
            this.Grd1.CellCtrlBackColor = System.Drawing.Color.Transparent;
            this.Grd1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Grd1.DefaultAutoGroupRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd1.DefaultCol.AllowGrouping = false;
            this.Grd1.DefaultCol.AllowMoving = false;
            this.Grd1.DefaultCol.AllowSizing = false;
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Selectable = false;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.DefaultRow.TreeButton = TenTec.Windows.iGridLib.iGTreeButtonState.Hidden;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.FocusRect = false;
            this.Grd1.FocusRectColor1 = System.Drawing.Color.Transparent;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.HorizontalLastRow = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.VerticalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.AllowPress = false;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.AutoHeightFlags = ((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags)((TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnAddCol | TenTec.Windows.iGridLib.iGHdrAutoHeightFlags.OnContentsChange)));
            this.Grd1.Header.DrawSystem = false;
            this.Grd1.Header.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.SeparatingLine = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.Visible = false;
            this.Grd1.HighlightSelCells = false;
            this.Grd1.ImmediateColResizing = false;
            this.Grd1.ImmediateRowResizing = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.PrefixGroupValues = false;
            this.Grd1.PressedMouseMoveMode = TenTec.Windows.iGridLib.iGPressedMouseMoveMode.Normal;
            this.Grd1.ProcessEnter = false;
            this.Grd1.RowHeader.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.RowHeader.DrawSystem = false;
            this.Grd1.RowHeader.HGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.RowHeader.VGridLinesStyle = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.RowHeader.Width = 0;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd1.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.None;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(395, 183);
            this.Grd1.TabIndex = 57;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.Color.Transparent;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            // 
            // PnlWidget
            // 
            this.PnlWidget.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PnlWidget.BackColor = System.Drawing.Color.White;
            this.PnlWidget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlWidget.Controls.Add(this.LueFontSize);
            this.PnlWidget.Controls.Add(this.PnlWidget2);
            this.PnlWidget.Controls.Add(this.PnlWidget1);
            this.PnlWidget.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlWidget.Location = new System.Drawing.Point(0, 489);
            this.PnlWidget.Name = "PnlWidget";
            this.PnlWidget.Size = new System.Drawing.Size(397, 185);
            this.PnlWidget.TabIndex = 59;
            // 
            // LueFontSize
            // 
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(320, 152);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 13;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // PnlWidget2
            // 
            this.PnlWidget2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlWidget2.Controls.Add(this.RtbWidget2);
            this.PnlWidget2.Controls.Add(this.LblWidget2);
            this.PnlWidget2.Location = new System.Drawing.Point(9, 97);
            this.PnlWidget2.Name = "PnlWidget2";
            this.PnlWidget2.Size = new System.Drawing.Size(308, 75);
            this.PnlWidget2.TabIndex = 12;
            // 
            // RtbWidget2
            // 
            this.RtbWidget2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RtbWidget2.Dock = System.Windows.Forms.DockStyle.Right;
            this.RtbWidget2.Location = new System.Drawing.Point(123, 0);
            this.RtbWidget2.Name = "RtbWidget2";
            this.RtbWidget2.ReadOnly = true;
            this.RtbWidget2.Size = new System.Drawing.Size(183, 73);
            this.RtbWidget2.TabIndex = 62;
            this.RtbWidget2.Text = "";
            // 
            // LblWidget2
            // 
            this.LblWidget2.AutoSize = true;
            this.LblWidget2.Font = new System.Drawing.Font("Tahoma", 27.75F);
            this.LblWidget2.Location = new System.Drawing.Point(2, 14);
            this.LblWidget2.Name = "LblWidget2";
            this.LblWidget2.Size = new System.Drawing.Size(0, 45);
            this.LblWidget2.TabIndex = 60;
            // 
            // PnlWidget1
            // 
            this.PnlWidget1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlWidget1.Controls.Add(this.RtbWidget1);
            this.PnlWidget1.Controls.Add(this.LblWidget1);
            this.PnlWidget1.Location = new System.Drawing.Point(9, 10);
            this.PnlWidget1.Name = "PnlWidget1";
            this.PnlWidget1.Size = new System.Drawing.Size(308, 75);
            this.PnlWidget1.TabIndex = 11;
            // 
            // RtbWidget1
            // 
            this.RtbWidget1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RtbWidget1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RtbWidget1.Location = new System.Drawing.Point(123, 0);
            this.RtbWidget1.Name = "RtbWidget1";
            this.RtbWidget1.ReadOnly = true;
            this.RtbWidget1.Size = new System.Drawing.Size(183, 73);
            this.RtbWidget1.TabIndex = 61;
            this.RtbWidget1.Text = "";
            // 
            // LblWidget1
            // 
            this.LblWidget1.AutoSize = true;
            this.LblWidget1.Font = new System.Drawing.Font("Tahoma", 27.75F);
            this.LblWidget1.Location = new System.Drawing.Point(2, 14);
            this.LblWidget1.Name = "LblWidget1";
            this.LblWidget1.Size = new System.Drawing.Size(0, 45);
            this.LblWidget1.TabIndex = 59;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.LogoSelf);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(397, 75);
            this.panel2.TabIndex = 58;
            // 
            // LogoSelf
            // 
            this.LogoSelf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LogoSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogoSelf.Location = new System.Drawing.Point(0, 0);
            this.LogoSelf.Name = "LogoSelf";
            this.LogoSelf.Size = new System.Drawing.Size(395, 73);
            this.LogoSelf.TabIndex = 0;
            this.LogoSelf.TabStop = false;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(988, 749);
            this.Controls.Add(this.PnlBase);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Run System Version ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.PnlBase.ResumeLayout(false);
            this.PnlSide.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.PnlNews.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.PnlSelfOnBoarding.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.PnlWidget.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            this.PnlWidget2.ResumeLayout(false);
            this.PnlWidget2.PerformLayout();
            this.PnlWidget1.ResumeLayout(false);
            this.PnlWidget1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogoSelf)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Timer TmAutoLogOff;
        private System.Windows.Forms.Timer TmShowHide;
        private System.Windows.Forms.Panel PnlBase;
        private System.Windows.Forms.Panel PnlSide;
        private System.Windows.Forms.Button BtnShowHide;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel PnlNews;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel PnlSelfOnBoarding;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel PnlWidget;
        private System.Windows.Forms.Panel PnlWidget2;
        private System.Windows.Forms.Label LblWidget2;
        private System.Windows.Forms.Panel PnlWidget1;
        private System.Windows.Forms.Label LblWidget1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox LogoSelf;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        private System.Windows.Forms.RichTextBox RtbWidget2;
        private System.Windows.Forms.RichTextBox RtbWidget1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label LblMeritIncreaseNotification;





    }
}

