﻿#region Update 
/*
 * 19/08/2021 [VIN/IMS] feedback PO servise ditampilkan 
   18/11/2021 [DITA/TWC] bug saat refresh karna kolom alias not uniq
   31/01/2022 [ISD/PHT] tambah kolom amount after revision dengan parameter IsPOListByVendorUsePORevision
   14/04/2022 [RIS/PHT] Merubah parameter IsPOListByVendorUsePORevision menjadi IsRptProcurementUsePORevision
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVdPO : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false, mIsFilterBySite = false, mIsRptProcurementUsePORevision = false;

        #endregion

        #region Constructor

        public FrmRptVdPO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                //SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsRptProcurementUsePORevision = Sm.GetParameter("IsRptProcurementUsePORevision") == "Y";
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select Distinct C.VdName, A.VdCode, ifnull(E.DocNo, A.DocNo) DocNo, A.DocDt, A.CurCode, ");
            if (mIsSiteMandatory)
                SQL.AppendLine("F.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("SUM(IFNULL(H1.AmtOld, I.TotalBeforeTax)) +(IfNull(H1.AmtOld, I.TotalBeforeTax)*(IfNull(J1.TaxRate, 0)/100))+(IfNull(H1.AmtOld, I.TotalBeforeTax)*(IfNull(J2.TaxRate, 0)/100))+(IfNull(H1.AmtOld, 0)*(IfNull(J3.TaxRate, 0)/100))+A.CustomsTaxAmt-A.DiscountAmt AS Amt, ");
                SQL.AppendLine("SUM(IFNULL(H2.Amt, I.TotalBeforeTax)) +(IfNull(H2.Amt, I.TotalBeforeTax)*(IfNull(J1.TaxRate, 0)/100))+(IfNull(H2.Amt, I.TotalBeforeTax)*(IfNull(J2.TaxRate, 0)/100))+(IfNull(H2.Amt, 0)*(IfNull(J3.TaxRate, 0)/100))+A.CustomsTaxAmt-A.DiscountAmt AS AmtRev ");
            }
            else
                SQL.AppendLine("A.Amt, Null As AmtRev ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("INNER JOIN tblpodtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("INNER Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("INNER JOIN tblporequestdtl D ON B.PORequestDocNo=D.DocNo AND B.PORequestDNo=D.DNo ");
            SQL.AppendLine("LEFT JOIN tblmaterialrequestdtl E ON D.MaterialRequestDocNo=E.DocNo AND D.MaterialRequestDNo=E.DNo AND E.MaterialRequestServiceDocNo IS NOT NULL  ");
            if (mIsSiteMandatory)
                SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("Left Join TblQtDtl G On D.QtDocNo = G.DocNo And D.QtDNo = G.DNo ");
                SQL.AppendLine("Left Join(  ");
                SQL.AppendLine("	SELECT X1.PODocNo, X1.PODNo, X1.AmtOld ");
                SQL.AppendLine("	FROM tblporevision X1  ");
                SQL.AppendLine("	Inner Join tblqtdtl X2 ON X1.QtDocNoOld = X2.DocNo AND X1.QtDNoOld = X2.DNo  ");
                SQL.AppendLine("	Inner Join tblpohdr X3 On X1.PODocNo=X3.DocNo " + Filter.Replace("X.", "X3."));
                SQL.AppendLine("	WHERE X1.DocNo = (SELECT MIN(DocNo) FROM tblporevision WHERE PODocNo = X1.PODocNo)  ");
                SQL.AppendLine(")H1 ON B.DocNo = H1.PODocNo AND B.DNo = H1.PODNo ");
                SQL.AppendLine("Left Join(  ");
                SQL.AppendLine("	SELECT X1.PODocNo, X1.PODNo, X1.Amt ");
                SQL.AppendLine("	FROM tblporevision X1  ");
                SQL.AppendLine("	Inner Join tblqtdtl X2 ON X1.QtDocNo = X2.DocNo AND X1.QtDNo = X2.DNo  ");
                SQL.AppendLine("	Inner Join tblpohdr X3 On X1.PODocNo=X3.DocNo " + Filter.Replace("X.", "X3."));
                SQL.AppendLine("	WHERE X1.DocNo = (SELECT MAX(DocNo) FROM tblporevision WHERE PODocNo = X1.PODocNo)  ");
                SQL.AppendLine(")H2 ON B.DocNo = H2.PODocNo AND B.DNo = H2.PODNo   ");
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("	SELECT X1.DocNo AS PODocNo, X1.DNo as PODNo, (((100 - X1.Discount) / 100) * (X1.Qty * X3.UPrice) - X1.DiscountAmt + X1.RoundingValue) AS TotalBeforeTax ");
                SQL.AppendLine("	FROM tblpodtl X1 ");
                SQL.AppendLine("	INNER JOIN tblporequestdtl X2 ON X1.PORequestDocNo=X2.DocNo AND X1.PORequestDNo=X2.DNo  ");
                SQL.AppendLine("	Inner Join tblqtdtl X3 ON X2.QtDocNo = X3.DocNo AND X2.QtDNo = X3.DNo  ");
                SQL.AppendLine(")I ON B.DocNo = I.PODocNo AND B.DNo = I.PODNo ");
                SQL.AppendLine("Left Join TblTax J1 On A.TaxCode1=J1.TaxCode ");
                SQL.AppendLine("Left Join TblTax J2 On A.TaxCode2=J2.TaxCode ");
                SQL.AppendLine("Left Join TblTax J3 On A.TaxCode3=J3.TaxCode ");
            }
            SQL.AppendLine("Where A.Amt>0 ");
            if (mIsSiteMandatory && mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mIsRptProcurementUsePORevision)
                SQL.AppendLine("GROUP BY A.DocNo, C.VdName, A.VdCode ");
            SQL.AppendLine(")X ");
            if(TxtDocNo.Text.Length > 0)
                SQL.AppendLine("Where X.DocNo Like '%"+TxtDocNo.Text+"%' ");
            else
                SQL.AppendLine("Where 0=0 ");

            return SQL.ToString();
            
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor", 
                        "Document#", 
                        "",
                        "Date",
                        "Site",
                        
                        //6-7
                        "Currency",
                        "Amount"+Environment.NewLine+"(Net)",
                        "Amount (Net)"+Environment.NewLine+"After Revision"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 130, 20, 80, 150,
                        
                        //6-8
                        60, 150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            if (!mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            if (!mIsRptProcurementUsePORevision) Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "X.VdCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter) + Filter + " Order By X.VdName, X.DocDt, X.DocNo;",
                        new string[]
                        {
                            //0
                            "VdName",  

                            //1-5
                            "DocNo", "DocDt", "SiteName", "CurCode", "Amt", 
                            
                            //6
                            "AmtRev"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Sm.GetGrdStr(Grd1, e.RowIndex, 2).Contains("POS"))
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmPO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
            else
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmMaterialRequest4(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Sm.GetGrdStr(Grd1, e.RowIndex, 2).Contains("POS"))
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f = new FrmPO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                {
                    var f = new FrmMaterialRequest4(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (Grd1.GroupObject.Count != 0)
            {
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
            }
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
