﻿#region Update
/*
    16/06/2022 [BRI/IOK] tambah rak 16 - 25
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRawMaterialCompare2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mItCtRMPActual = string.Empty,
            mItCtRMPActual2 = string.Empty,
            mPosCodeGrader = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRawMaterialCompare2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteUnloadingDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteUnloadingDt2.DateTime = Sm.ConvertDate(CurrentDate);

                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mItCtRMPActual = Sm.GetParameter("ItCtRMPActual");
            mItCtRMPActual2 = Sm.GetParameter("ItCtRMPActual2");
            mPosCodeGrader = Sm.GetParameter("PosCodeGrader");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Received"+Environment.NewLine+"Date",
                        "Queue#",
                        "Vendor", 
                        "Grader",
                        "Opname",
                        
                        //6-10
                        "Admin",
                        "Bin",
                        "Item",
                        "Inspection"+Environment.NewLine+"Date",
                        "Inspection"+Environment.NewLine+"Batang",

                        //11-13
                        "Received"+Environment.NewLine+"Batang",
                        "Difference"+Environment.NewLine+"Batang",
                        "Label"+Environment.NewLine+"Quantity"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 80, 200, 200, 200, 
                        
                        //6-10
                        120, 60, 150, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13 }, 2);
            Sm.SetGrdProperty(Grd1, false);
        }

        private bool IsFilterByDateInvalid()
        {
            var Dt1 = Sm.GetDte(DteUnloadingDt1);
            var Dt2 = Sm.GetDte(DteUnloadingDt2);

            if (Decimal.Parse(Dt1) > Decimal.Parse(Dt2))
            {
                Sm.StdMsg(mMsgType.Warning, "Loading Date : " + Environment.NewLine + "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        override protected void ShowData()
        {
            var lLoadingQueue = new List<LoadingQueue>();
            var lRawMaterial = new List<RawMaterial>();
            var lGrader = new List<Grader>();

            try
            {
                Sm.ClearGrd(Grd1, false);
                if (
                    Sm.IsDteEmpty(DteUnloadingDt1, "Unloading's start date") ||
                    Sm.IsDteEmpty(DteUnloadingDt2, "Unloading's end date") ||
                    IsFilterByDateInvalid()
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lLoadingQueue);
                if (lLoadingQueue.Count > 0)
                {
                    Process2(ref lRawMaterial, ref lLoadingQueue);
                    Process3(ref lRawMaterial);
                    Process4(ref lGrader);
                    Process5(ref lLoadingQueue, ref lGrader);
                    Process6(ref lRawMaterial, ref lLoadingQueue);
                    Process7(ref lRawMaterial);
                    Process8(ref lRawMaterial);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lLoadingQueue.Clear();
                lRawMaterial.Clear();
                lGrader.Clear(); 
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<LoadingQueue> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter1 = " ", Filter2 = " ";

            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteUnloadingDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteUnloadingDt2));
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteInspectionDt1), Sm.GetDte(DteInspectionDt2), "D.DocDt");
            Sm.FilterStr(ref Filter1, ref cm, TxtQueueNo.Text, "A.DocNo", false);
            
            SQL.AppendLine("Select B.QueueNo, E.VdName, ");
            SQL.AppendLine("A.CreateDt As UnloadDt, C.DocDt As RecvDt, D.DocDt As OpnameDt, ");
            SQL.AppendLine("C.CreateBy As Admin, F.EmpName As Opname1, G.EmpName As Opname2 ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo ");
            SQL.AppendLine("Left Join TblRecvRawMaterialHdr C On B.DocNo=C.LegalDocVerifyDocNo And C.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblRecvRawMaterial2Hdr D On B.QueueNo=D.QueueNo And D.CancelInd='N'" + Filter2);
            SQL.AppendLine("Left Join TblVendor E On B.VdCode=E.VdCode ");
            SQL.AppendLine("Left Join TblEmployee F On D.EmpCode1=F.EmpCode ");
            SQL.AppendLine("Left Join TblEmployee G On D.EmpCode2=G.EmpCode ");
            SQL.AppendLine(" Where (Left(A.CreateDt, 8) Between @Dt1 And @Dt2) ");
            SQL.AppendLine(Filter1 + " Order By B.QueueNo; ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "QueueNo", 
                    "VdName", "UnloadDt", "RecvDt", "OpnameDt", "Admin", 
                    "Opname1", "Opname2" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LoadingQueue()
                        {
                            QueueNo = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            UnloadDt = Sm.DrStr(dr, c[2]),
                            RecvDt = Sm.DrStr(dr, c[3]),
                            OpnameDt = Sm.DrStr(dr, c[4]),
                            Admin = Sm.DrStr(dr, c[5]),
                            Opname = Sm.DrStr(dr, c[6]) + 
                                (Sm.DrStr(dr, c[7]).Length==0?string.Empty: ", " + Sm.DrStr(dr, c[7])),
                            Grader = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<RawMaterial> l, ref List<LoadingQueue> l2)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (l2.Count >= 1)
            {
                int No = 1;
                
                for (var i = 0; i < l2.Count; i++)
                {    
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DocNo=@QueueNo" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@QueueNo" + No.ToString(), l2[i].QueueNo);
                    No += 1;
                }
                if (Filter.Length>0) Filter = " Where (" + Filter + ") ";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.QueueNo, T1.Bin, T1.ItCode, T1.Qty1, T1.Qty2, T2.ItName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.QueueNo, T.Bin, T.ItCode, ");
            SQL.AppendLine("    Sum(T.Qty1) As Qty1, ");
            SQL.AppendLine("    Sum(T.Qty2) As Qty2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select B.QueueNo, E.ItCode, "); 
            SQL.AppendLine("        Case D.SectionNo "); 
	        SQL.AppendLine("            When 1 Then Shelf1 ");
	        SQL.AppendLine("            When 2 Then Shelf2 ");
	        SQL.AppendLine("            When 3 Then Shelf3 ");
	        SQL.AppendLine("            When 4 Then Shelf4 ");
	        SQL.AppendLine("            When 5 Then Shelf5 ");
	        SQL.AppendLine("            When 6 Then Shelf6 ");
	        SQL.AppendLine("            When 7 Then Shelf7 ");
	        SQL.AppendLine("            When 8 Then Shelf8 ");
	        SQL.AppendLine("            When 9 Then Shelf9 ");
	        SQL.AppendLine("            When 10 Then Shelf10 ");
	        SQL.AppendLine("            When 11 Then Shelf11 ");
	        SQL.AppendLine("            When 12 Then Shelf12 ");
	        SQL.AppendLine("            When 13 Then Shelf13 ");
	        SQL.AppendLine("            When 14 Then Shelf14 ");
	        SQL.AppendLine("            When 15 Then Shelf15 ");
            SQL.AppendLine("            When 16 Then Shelf16 ");
            SQL.AppendLine("            When 17 Then Shelf17 ");
            SQL.AppendLine("            When 18 Then Shelf18 ");
            SQL.AppendLine("            When 19 Then Shelf19 ");
            SQL.AppendLine("            When 20 Then Shelf20 ");
            SQL.AppendLine("            When 21 Then Shelf21 ");
            SQL.AppendLine("            When 22 Then Shelf22 ");
            SQL.AppendLine("            When 23 Then Shelf23 ");
            SQL.AppendLine("            When 24 Then Shelf24 ");
            SQL.AppendLine("            When 25 Then Shelf25 ");
            SQL.AppendLine("        End As Bin, ");
            SQL.AppendLine("        D.Qty As Qty1, 0 As Qty2 ");
            SQL.AppendLine("        From TblLoadingQueue A ");
            SQL.AppendLine("        Inner Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo "); 
            SQL.AppendLine("        Inner Join TblRecvRawMaterialHdr C On B.DocNo=C.LegalDocVerifyDocNo And C.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterialDtl2 D On C.DocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode " + Filter);
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.QueueNo, D.ItCode, "); 
            SQL.AppendLine("        Case C.SectionNo "); 
	        SQL.AppendLine("            When 1 Then Shelf1 ");
	        SQL.AppendLine("            When 2 Then Shelf2 ");
	        SQL.AppendLine("            When 3 Then Shelf3 ");
	        SQL.AppendLine("            When 4 Then Shelf4 ");
	        SQL.AppendLine("            When 5 Then Shelf5 ");
	        SQL.AppendLine("            When 6 Then Shelf6 ");
	        SQL.AppendLine("            When 7 Then Shelf7 ");
	        SQL.AppendLine("            When 8 Then Shelf8 ");
	        SQL.AppendLine("            When 9 Then Shelf9 ");
	        SQL.AppendLine("            When 10 Then Shelf10 ");
	        SQL.AppendLine("            When 11 Then Shelf11 ");
	        SQL.AppendLine("            When 12 Then Shelf12 ");
	        SQL.AppendLine("            When 13 Then Shelf13 ");
	        SQL.AppendLine("            When 14 Then Shelf14 ");
	        SQL.AppendLine("            When 15 Then Shelf15 ");
            SQL.AppendLine("            End As Bin, ");
            SQL.AppendLine("        0, C.Qty As Qty2 "); 
            SQL.AppendLine("        From TblLoadingQueue A ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterial2Hdr B On A.DocNo=B.QueueNo And B.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvRawMaterial2Dtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode " + Filter);
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.QueueNo, T.Bin, T.ItCode ");
            SQL.AppendLine(") T1, TblItem T2 Where T1.ItCode=T2.ItCode ");
            SQL.AppendLine("Order By T1.QueueNo, T1.Bin, T1.ItCode; ");

            Sm.CmParam<String>(ref cm, "@ItCtRMPActual", Sm.GetParameter("ItCtRMPActual"));
            Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", Sm.GetParameter("ItCtRMPActual2"));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "QueueNo", 
                    
                    //1-5
                    "Bin", "ItCode", "Qty1", "Qty2", "ItName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RawMaterial()
                        {
                            QueueNo = Sm.DrStr(dr, c[0]),
                            Bin = Sm.DrStr(dr, c[1]),
                            ItCode = Sm.DrStr(dr, c[2]),
                            Qty1 = Sm.DrDec(dr, c[3]),
                            Qty2 = Sm.DrDec(dr, c[4]),
                            ItName = Sm.DrStr(dr, c[5]),
                            LabelQty = 0m,
                            VdName = string.Empty,
                            UnloadDt = string.Empty,
                            RecvDt = string.Empty,
                            OpnameDt = string.Empty,
                            Grader = string.Empty,
                            Admin = string.Empty,
                            Opname = string.Empty,
                            IsShow = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<RawMaterial> l)
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (l.Count >= 1)
            {
                int No = 1;

                for (var i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.QueueNo=@QueueNo" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@QueueNo" + No.ToString(), l[i].QueueNo);
                    No += 1;
                }
                if (Filter.Length > 0) Filter = " Where A.CancelInd='N' And (" + Filter + ") ";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select QueueNo, ItCode, Bin, LabelQty From ( ");
            SQL.AppendLine("    Select A.QueueNo, B.ItCode, ");
            SQL.AppendLine("    Case B.SectionNo ");
            SQL.AppendLine("    When 1 Then Shelf1 ");
            SQL.AppendLine("    When 2 Then Shelf2 ");
            SQL.AppendLine("    When 3 Then Shelf3 ");
            SQL.AppendLine("    When 4 Then Shelf4 ");
            SQL.AppendLine("    When 5 Then Shelf5 ");
            SQL.AppendLine("    When 6 Then Shelf6 ");
            SQL.AppendLine("    When 7 Then Shelf7 ");
            SQL.AppendLine("    When 8 Then Shelf8 ");
            SQL.AppendLine("    When 9 Then Shelf9 ");
            SQL.AppendLine("    When 10 Then Shelf10 ");
            SQL.AppendLine("    When 11 Then Shelf11 ");
            SQL.AppendLine("    When 12 Then Shelf12 ");
            SQL.AppendLine("    When 13 Then Shelf13 ");
            SQL.AppendLine("    When 14 Then Shelf14 ");
            SQL.AppendLine("    When 15 Then Shelf15 ");
            SQL.AppendLine("    End As Bin, ");
            SQL.AppendLine("    Case B.SectionNo ");
            SQL.AppendLine("    When 1 Then A.LabelQty1 ");
            SQL.AppendLine("    When 2 Then A.LabelQty2 ");
            SQL.AppendLine("    When 3 Then A.LabelQty3 ");
            SQL.AppendLine("    When 4 Then A.LabelQty4 ");
            SQL.AppendLine("    When 5 Then A.LabelQty5 ");
            SQL.AppendLine("    When 6 Then A.LabelQty6 ");
            SQL.AppendLine("    When 7 Then A.LabelQty7 ");
            SQL.AppendLine("    When 8 Then A.LabelQty8 ");
            SQL.AppendLine("    When 9 Then A.LabelQty9 ");
            SQL.AppendLine("    When 10 Then A.LabelQty10 ");
            SQL.AppendLine("    When 11 Then A.LabelQty11 ");
            SQL.AppendLine("    When 12 Then A.LabelQty12 ");
            SQL.AppendLine("    When 13 Then A.LabelQty13 ");
            SQL.AppendLine("    When 14 Then A.LabelQty14 ");
            SQL.AppendLine("    When 15 Then A.LabelQty15 ");
            SQL.AppendLine("    End As LabelQty ");
            SQL.AppendLine("    From TblRecvRawMaterial2Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvRawMaterial2Dtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    Inner Join TblRecvRawMaterial2Dtl2 C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T Order By QueueNo, Bin, ItCode; ");

            string
                QueueNo = string.Empty,
                Bin = string.Empty,
                ItCode = string.Empty;
            int Temp = 0;
            decimal LabelQty = 0m;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "QueueNo", "Bin", "ItCode", "LabelQty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        QueueNo = Sm.DrStr(dr, c[0]);
                        Bin = Sm.DrStr(dr, c[1]);
                        ItCode = Sm.DrStr(dr, c[2]);
                        LabelQty = Sm.DrDec(dr, c[3]);
                        for (var i = Temp; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].QueueNo, QueueNo) == 0 &&
                                string.Compare(l[i].Bin, Bin) == 0 &&
                                string.Compare(l[i].ItCode, ItCode) == 0 
                                )
                            {
                                l[i].LabelQty = LabelQty;
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Grader> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";
            int i = -1;
            string QueueNo = string.Empty;

            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteUnloadingDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteUnloadingDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, "A.DocNo", false);

            SQL.AppendLine("Select A.DocNo As QueueNo, E.EmpName ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo ");
            SQL.AppendLine("Inner Join TblRecvRawMaterialHdr C On B.DocNo=C.LegalDocVerifyDocNo And C.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblRecvRawMaterialDtl D On C.DocNo=D.DocNo And D.EmpType='1' ");
            SQL.AppendLine("Inner Join TblEmployee E On D.EmpCode=E.EmpCode ");
            SQL.AppendLine(" Where (Left(A.CreateDt, 8) Between @Dt1 And @Dt2) ");
            SQL.AppendLine(Filter + " Order By B.QueueNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "QueueNo", "EmpName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (string.Compare(Sm.DrStr(dr, c[0]), QueueNo) == 0)
                            l[i].EmpName = l[i].EmpName + " , " + Sm.DrStr(dr, c[1]);
                        else
                        {
                            l.Add(new Grader()
                            {
                                QueueNo = Sm.DrStr(dr, c[0]),
                                EmpName = Sm.DrStr(dr, c[1])
                            });
                            i++;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process5(ref List<LoadingQueue> l1, ref List<Grader> l2)
        {
            string QueueNo = string.Empty, EmpName = string.Empty;
            for (var i = 0; i < l1.Count; i++)
            {
                if (string.Compare(l1[i].QueueNo, QueueNo) == 0)
                    l1[i].Grader = EmpName;
                else
                {
                    QueueNo = l1[i].QueueNo;
                    foreach (var index in l2.Where(x =>
                        string.Compare(x.QueueNo, QueueNo) == 0))
                        l1[i].Grader = index.EmpName;
                    EmpName = l1[i].Grader;
                }
            }
        }

        private void Process6(ref List<RawMaterial> l1, ref List<LoadingQueue> l2)
        {
            string 
                QueueNo = string.Empty,
                VdName = string.Empty,
                UnloadDt = string.Empty,
                RecvDt = string.Empty,
                OpnameDt = string.Empty,
                Grader = string.Empty,
                Admin = string.Empty,
                Opname = string.Empty;
            
            for (var i = 0; i < l1.Count; i++)
            {
                if (string.Compare(l1[i].QueueNo, QueueNo) == 0)
                {
                    l1[i].VdName = VdName;
                    l1[i].UnloadDt = UnloadDt;
                    l1[i].RecvDt = RecvDt;
                    l1[i].OpnameDt = OpnameDt;
                    l1[i].Grader = Grader;
                    l1[i].Admin = Admin;
                    l1[i].Opname = Opname;
                }
                else
                {
                    QueueNo = l1[i].QueueNo;
                    foreach (var index in l2.Where(x => string.Compare(x.QueueNo, QueueNo) == 0))
                    {
                        l1[i].VdName = index.VdName;
                        l1[i].UnloadDt = index.UnloadDt;
                        l1[i].RecvDt = index.RecvDt;
                        l1[i].OpnameDt = index.OpnameDt;
                        l1[i].Grader = index.Grader;
                        l1[i].Admin = index.Admin;
                        l1[i].Opname = index.Opname;
                    }
                    VdName = l1[i].VdName;
                    UnloadDt = l1[i].UnloadDt;
                    RecvDt = l1[i].RecvDt;
                    OpnameDt = l1[i].OpnameDt;
                    Grader = l1[i].Grader;
                    Admin = l1[i].Admin;
                    Opname = l1[i].Opname;
                }
            }
        }

        private void Process7(ref List<RawMaterial> l)
        {
            var l2 = new List<QueueNoBin>();
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].Qty2 > 0)
                {
                    l2.Add(new QueueNoBin()
                    {
                        QueueNo = l[i].QueueNo,
                        Bin = l[i].Bin
                    });
                }
            }
            for (var i = 0; i < l.Count; i++)
            {
                for (var j = 0; j < l2.Count; j++)
                {
                    if (
                        string.CompareOrdinal(l[i].QueueNo, l2[j].QueueNo) == 0 &&
                        string.CompareOrdinal(l[i].Bin, l2[j].Bin) == 0
                        )
                    {
                        l[i].IsShow = true;
                        break;
                    }
                }
            }
        }

        private void Process8(ref List<RawMaterial> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].IsShow)
                {
                    r = Grd1.Rows.Add();
                    r.Cells[0].Value = i + 1;
                    if (l[i].RecvDt.Length > 0) r.Cells[1].Value = Sm.ConvertDate(l[i].RecvDt);
                    r.Cells[2].Value = l[i].QueueNo;
                    r.Cells[3].Value = l[i].VdName;
                    r.Cells[4].Value = l[i].Grader;
                    r.Cells[5].Value = l[i].Opname;
                    r.Cells[6].Value = l[i].Admin;
                    r.Cells[7].Value = l[i].Bin;
                    r.Cells[8].Value = string.Concat(l[i].ItCode, '-', l[i].ItName);
                    if (l[i].OpnameDt.Length > 0) r.Cells[9].Value = Sm.ConvertDate(l[i].OpnameDt);
                    r.Cells[10].Value = l[i].Qty1;
                    r.Cells[11].Value = l[i].Qty2;
                    r.Cells[12].Value = l[i].Qty1 - l[i].Qty2;
                    r.Cells[13].Value = l[i].LabelQty;
                }
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12 });
            Grd1.EndUpdate();
            Process9(ref l);
        }

        private void Process9(ref List<RawMaterial> l)
        {
            string QueueNo = string.Empty, Bin = string.Empty;
            decimal Label = 0m;
            foreach (var i in l.Where(x=>x.IsShow).OrderBy(x => x.QueueNo).ThenBy(x => x.Bin))
            {
                if (!(Sm.CompareStr(QueueNo, i.QueueNo) && Sm.CompareStr(Bin, i.Bin)))
                    Label += i.LabelQty;
                QueueNo = i.QueueNo;
                Bin = i.Bin;
            }
            Grd1.Cells[Grd1.Rows.Count - 1, 13].Value = Label;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteUnloadingDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteUnloadingDt2).Length == 0)
                DteUnloadingDt2.EditValue = DteUnloadingDt1.EditValue;
        }

        private void DteInspectionDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteInspectionDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkInspectionDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Inspection Date");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Queue#");
        }

        #endregion

        #endregion

        #region Class

        private class LoadingQueue
        {
            public string QueueNo { get; set; }
            public string VdName { get; set; }
            public string UnloadDt { get; set; }
            public string RecvDt { get; set; }
            public string OpnameDt { get; set; }
            public string Grader { get; set; }
            public string Admin { get; set; }
            public string Opname { get; set; }
        }

        private class RawMaterial
        {
            public string QueueNo { get; set; }
            public string Dt { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal LabelQty { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public string VdName { get; set; }
            public string UnloadDt { get; set; }
            public string RecvDt { get; set; }
            public string OpnameDt { get; set; }
            public string Grader { get; set; }
            public string Admin { get; set; }
            public string Opname { get; set; }
            public bool IsShow { get; set; }
        }

        private class Grader
        {
            public string QueueNo { get; set; }
            public string EmpName { get; set; }
        }

        private class QueueNoBin
        {
            public string QueueNo { get; set; }
            public string Bin { get; set; }
        }

        #endregion
    }
}
