﻿#region Update
/*  
   31/12/2021 [VIN/IOK] RFC : dno detail jadi 4 karakter
   16/04/2022 [TKG/IOK] mempercepat proses save
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPWG : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal int mNumberOfPlanningUomCode = 1;
        private decimal mHolidayWagesIndex = 1, mNotHolidayWagesIndex = 1, mWorkingHr = 0m;
        private bool mIsCalcPerPersonProcessData = false;
        internal FrmPWGFind FrmFind;

        #endregion

        #region Constructor

        public FrmPWG(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Employee's Production Wages";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetParameter();

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueProductionShiftCode(ref LueProductionShiftCode);
                Sl.SetLueWagesFormulationCode(ref LueWagesFormulationCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Work Center#",
                        "",
                        "Work Center Name",
                        "SFC#",
                        
                        //6-8
                        "",
                        "Date",
                        "Shift"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 130, 20, 300, 130,  

                        //6-8
                        20, 80, 190
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 7, 8 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 16;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Work Center#",
                        
                        //1-5
                        "Work Center Name",
                        "SFC#",
                        "DNo",
                        "",
                        "Item Code",

                        //6-10
                        "",
                        "Item Name",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Processed"+Environment.NewLine+"Quantity",
                        "Balance",
                        
                        //11-15
                        "UoM",
                        "Outstanding"+Environment.NewLine+"Quantity 2",
                        "Processed"+Environment.NewLine+"Quantity 2",
                        "Balance 2",
                        "Uom 2"
                    },
                     new int[] 
                    {
                        //0
                        130, 

                        //1-5
                        200, 130, 0, 20, 80,   

                        //6-10
                        20, 250, 100, 100, 100,

                        //11-15
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 4, 6 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 3, 4, 5, 6, 12, 13, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9, 10, 12, 13, 14 }, 0);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 14;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo", 

                        //1-5
                        "Item Code",
                        "",
                        "Item Name",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Processed"+Environment.NewLine+"Quantity",
                        
                        //6-10
                        "Balance",
                        "UoM",
                        "Outstanding"+Environment.NewLine+"Quantity 2",
                        "Processed"+Environment.NewLine+"Quantity 2",
                        "Balance 2",
                        
                        //11-13
                        "Uom 2",
                        "Index",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 20, 250, 100, 100, 

                        //6-10
                        100, 100, 100, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 2 });
            Sm.GrdColInvisible(Grd3, new int[] { 1, 2, 8, 9, 10, 11 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5, 6, 8, 9, 10, 12, 13 }, 0);
            Grd3.Cols[12].Move(4);

            #endregion

            #region Grd4

            Grd4.Cols.Count = 4;
            Grd4.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-3
                        "Value",
                        "Wages/Unit",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-3
                        150, 150, 180                    
                    }
                );
            Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3 });
            Sm.GrdFormatDec(Grd4, new int[] { 1, 2, 3 }, 0);

            #endregion

            #region Grd5

            Grd5.Cols.Count = 12;
            Grd5.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Grade Level",

                        //6-10
                        "Index",
                        "Quantity",
                        "Wages",
                        "Holiday Index",
                        "Total",

                        //11
                        "Holiday"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,
                    
                        //6-10
                        100, 100, 150, 100, 150,

                        //11
                        80
                    }
                );
            Sm.GrdColCheck(Grd5, new int[] { 11 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1, 3, 4, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd5, new int[] { 6, 7, 8, 9, 10 }, 0);
            
            #endregion

            #region Grd6

            Grd6.Cols.Count = 12;
            Grd6.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Grade Level",

                        //6-10
                        "Index",
                        "Wages",
                        "Holiday Index",
                        "Total",
                        "Holiday",

                        //11
                        "Coordinator"+Environment.NewLine+"Index"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,
                    
                        //6-10
                        80, 100, 130, 130, 80,

                        //11
                        130
                    }
                );
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 3, 4, 5, 11 }, false);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd6, new int[] { 6, 7, 8, 9, 11 }, 2);
            Sm.GrdColCheck(Grd6, new int[] { 10 });

            #endregion

            #region Grd7

            Grd7.Cols.Count = 14;
            Grd7.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Item Code",

                        //6-10
                        "",
                        "Item Name",
                        "Quantity",
                        "UoM",
                        "Quantity 2",

                        //11-13
                        "Uom 2",
                        "Index",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 100,
                    
                        //6-10
                        20, 250, 100, 100, 100,

                        //11-14
                        100, 100, 100
                    }
                );
            Sm.GrdColButton(Grd7, new int[] { 6 });
            Sm.GrdColInvisible(Grd7, new int[] { 0, 1, 3, 4, 5, 6, 10, 11 }, false);
            Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd7, new int[] { 8, 10, 12, 13 }, 0);

            #endregion

            #region Grd8

            Grd8.Cols.Count = 10;
            Grd8.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Value From",
                        
                        //6-9
                        "Value To",
                        "Value",
                        "Wages/Unit",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 100, 
                        
                        //6-9
                        100, 100, 130, 130                    
                    }
                );
            Sm.GrdColInvisible(Grd8, new int[] { 0, 1, 3, 4, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdFormatDec(Grd8, new int[] { 5, 6, 7, 8, 9 }, 0);

            #endregion

            ShowPlanningUomCode();
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 1, 3, 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 1, 3, 4, 5, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd7, new int[] { 1, 3, 4, 5, 6, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd8, new int[] { 1, 3, 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
            {
                Sm.GrdColInvisible(Grd2, new int[] { 12, 13, 14, 15 }, true);
                Sm.GrdColInvisible(Grd3, new int[] { 8, 9, 10, 11 }, true);
                Sm.GrdColInvisible(Grd7, new int[] { 10, 11 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueWorkCenterDocNo, LueProductionShiftCode, 
                        LueWagesFormulationCode, TxtUomCode, LueCurCode, MeeRemark, ChkCalcPerPerson
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });
                    //Sm.GrdColReadOnly(true, true, Grd5, new int[] { 11 });
                    //Sm.GrdColReadOnly(true, true, Grd6, new int[] { 10 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 8, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWorkCenterDocNo, LueProductionShiftCode, LueCurCode, MeeRemark, 
                        ChkCalcPerPerson
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    //Sm.GrdColReadOnly(false, true, Grd5, new int[] { 11 });
                    //Sm.GrdColReadOnly(false, true, Grd6, new int[] { 10 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 8, 10 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueWorkCenterDocNo, LueProductionShiftCode, LueWagesFormulationCode, 
                 TxtUomCode, LueCurCode, MeeRemark
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstanding, TxtProcessed, TxtBalance, 
                TxtOutstanding2, TxtProcessed2, TxtBalance2,
                TxtIndex, TxtValue, TxtAmt
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtEmpCount }, 1);

            ChkCancelInd.Checked = false;
            ChkCalcPerPerson.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
            ClearGrd8();
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 12, 13, 14 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5, 6, 8, 9, 10, 12, 13 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 1, 2, 3 });
        }

        private void ClearGrd5()
        {
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd5, 0, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 6, 7, 8, 9, 10 });
        }

        private void ClearGrd6()
        {
            Grd6.Rows.Clear();
            Grd6.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd6, 0, new int[] { 10 });
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 6, 7, 8, 9, 11 });
        }

        private void ClearGrd7()
        {
            Grd7.Rows.Clear();
            Grd7.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 8, 10, 12, 13 });
        }

        private void ClearGrd8()
        {
            Grd8.Rows.Clear();
            Grd8.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd8, 0, new int[] { 5, 6, 7, 8, 9 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPWGFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                mIsCalcPerPersonProcessData = true;
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PWG", "TblPWGHdr");
            
            var cml = new List<MySqlCommand>();

            cml.Add(SavePWGHdr(DocNo));
            cml.Add(SavePWGDtl(DocNo));
            cml.Add(SavePWGDtl2(DocNo));
            cml.Add(SavePWGDtl3(DocNo));
            if (!ChkCalcPerPerson.Checked)
                cml.Add(SavePWGDtl4(DocNo));
            cml.Add(SavePWGDtl5(DocNo));
            if (ChkCalcPerPerson.Checked)
            {
                cml.Add(SavePWGDtl6(DocNo));
                cml.Add(SavePWGDtl7(DocNo));
            }
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0) 
            //        cml.Add(SavePWGDtl(DocNo, Row));

            //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SavePWGDtl2(DocNo, Row));

            //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SavePWGDtl3(DocNo, Row));

            //if (!ChkCalcPerPerson.Checked)
            //{
            //    for (int Row = 0; Row < Grd4.Rows.Count; Row++)
            //        cml.Add(SavePWGDtl4(DocNo, Row));
            //}

            //int DNo = 1;
            //for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0)
            //    {
            //        cml.Add(SavePWGDtl5a(DocNo, Sm.Right("000" + DNo, 4), Row));
            //        DNo += 1;
            //    }
            //}

            //for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
            //    {
            //        cml.Add(SavePWGDtl5b(DocNo, Sm.Right("000" + DNo, 4), Row));
            //        DNo += 1;
            //    }
            //}

            //    if (ChkCalcPerPerson.Checked)
            //{
            //    for (int Row = 0; Row < Grd7.Rows.Count; Row++)
            //        if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0) cml.Add(SavePWGDtl6(DocNo, Row));

            //    for (int Row = 0; Row < Grd8.Rows.Count; Row++)
            //        if (Sm.GetGrdStr(Grd8, Row, 2).Length > 0) cml.Add(SavePWGDtl7(DocNo, Row));
            //}

            cml.Add(UpdateSFCProcessInfo(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty(ref Grd1, "You need to input at least 1 work center.") ||
                IsGrdExceedMaxRecords(ref Grd1, "Work center data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).") ||
                IsGrd1ValueNotValid() ||
                IsItemIndexNotValid() ||
                IsSFCAlreadyCancelled() ||
                IsSFCAlreadyFulfilled() ||
                IsGrd7ValueNotValid() ||
                IsSFCBalanceNotValid();
        }

        private bool IsItemIndexNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdDec(Grd3, Row, 12) == 0m)
                {
                    //RecomputeItemIndex(Row, false);
                    if (Sm.GetGrdDec(Grd3, Row, 12) == 0m)
                    {
                        if (Sm.StdMsgYN("Question",
                            "Item's Code : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine + Environment.NewLine +
                            "This item has no index."+ Environment.NewLine +
                            "Do you want to continue to save this data ?"
                            ) == DialogResult.No)
                        return true;
                    }
                }
            }
            return false;    
        }

        private void RecomputeItemIndex(int Row, bool IsReComputeHoliday)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = "Select ItIndex From TblItemIndex Where ItCode=@Param;" 
            };
            Sm.CmParam<String>(ref cm, "@Param", Sm.GetGrdStr(Grd3, Row, 1));
            
            decimal ItIndex = 0m;
            var Value = Sm.GetValue(cm);
            if (Value.Length != 0) ItIndex = decimal.Parse(Value);

            Grd3.Cells[Row, 12].Value = ItIndex;

            string UomCode = TxtUomCode.Text.Length == 0 ? "XXX" : TxtUomCode.Text;

            if (ChkCalcPerPerson.Checked)
            {
                string ItCode = Sm.GetGrdStr(Grd3, Row, 1);

                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd3, Row, 7)))
                    Grd3.Cells[Row, 13].Value = ItIndex * Sm.GetGrdDec(Grd3, Row, 5);

                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd3, Row, 11)))
                    Grd3.Cells[Row, 13].Value = ItIndex * Sm.GetGrdDec(Grd3, Row, 9);

                for (int Index = 0; Index < Grd7.Rows.Count - 1; Index++)
                {
                    if (Sm.GetGrdStr(Grd7, Index, 5).Length > 0 && 
                        Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd7, Index, 5)))
                    {
                        Grd7.Cells[Index, 12].Value = ItIndex;
                        if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd7, Index, 9)))
                            Grd7.Cells[Index, 13].Value = ItIndex * Sm.GetGrdDec(Grd7, Index, 8);

                        if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd7, Row, 11)))
                            Grd7.Cells[Index, 13].Value = ItIndex * Sm.GetGrdDec(Grd7, Index, 10);

                        UpdateWagesCalculatedPerPersonInfo();
                        ComputeWagesBasedOnFormulaForEachPerson(Sm.GetGrdStr(Grd7, Index, 1));
                        ComputeNonCoordinatorWagesForEachPerson(Sm.GetGrdStr(Grd7, Index, 1));
                    }
                }
            }
            else
            {
                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd3, Row, 7)))
                    Grd3.Cells[Row, 13].Value = ItIndex * Sm.GetGrdDec(Grd3, Row, 5);

                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd3, Row, 11)))
                    Grd3.Cells[Row, 13].Value = ItIndex * Sm.GetGrdDec(Grd3, Row, 9);

                ClearGrd4();
                ClearGrd5();
                ClearGrd6();

                ComputeIndex();
                ComputeValue();

                ComputeWagesBasedOnFormula();
                ComputeAmt();

                ComputeNonCoordinatorWages();
                ComputeNonCoordinatorHolidayWages();

                ComputeNumberOfEmployee();

                ComputeCoordinatorWages();
                ComputeCoordinatorHolidayWages();
            }
        }

        private bool IsGrdEmpty(ref iGrid Grd, string Warning)
        {
            if (Grd.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords(ref iGrid Grd, string Warning)
        {
            if (Grd.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Work center is empty.")) return true;
            return false;
        }

        private bool IsGrd7ValueNotValid()
        {
            if (!ChkCalcPerPerson.Checked) return false;

            bool IsHadQty = false;
            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd7, Row, 8) > 0 ||
                        Sm.GetGrdDec(Grd7, Row, 10) > 0)
                        return false;
                }
            }
            if (!IsHadQty)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to set quantity for each person.");
                return true;
            }
            return false;
        }

        private bool IsSFCAlreadyCancelled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblShopFloorControlHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;",
                GetSelectedSFC(), ref DocNo,
                "SFC# : " + DocNo + Environment.NewLine + "This document already cancelled.");
        }

        private bool IsSFCAlreadyFulfilled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblShopFloorControlHdr " +
                "Where ProcessInd='F' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;",
                GetSelectedSFC(), ref DocNo,
                "SFC# : " + DocNo + Environment.NewLine + "This document already fulfilled.");
        }

        private bool IsSFCBalanceNotValid()
        {
            //RecomputeOutstanding();

            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                Msg =
                    "Work Center : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                    "SFC# : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine;
                    
                if (Sm.GetGrdDec(Grd2, Row, 10) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        Msg + 
                        "Balance : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 10), 0) + Environment.NewLine +
                        "Balance should not be less than 0.");
                    return true;
                }

                if (Grd2.Cols[14].Visible)
                {
                    if (Sm.GetGrdDec(Grd2, Row, 14) < 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            Msg + 
                            "Balance (2) : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 14), 0) + Environment.NewLine +
                            "Balance (2) should not be less than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SavePWGHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("/* Production Wages (Hdr) */ ");

            SQL.AppendLine("Insert Into TblPWGHdr(DocNo, DocDt, CancelInd, WorkCenterDocNo, ProductionShiftCode, HolidayWagesIndex, WagesFormulationCode, UomCode, CurCode, CalcPerPersonInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @WorkCenterDocNo, @ProductionShiftCode, @HolidayWagesIndex, @WagesFormulationCode, @UomCode, @CurCode, @CalcPerPersonInd, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand()
            {
                CommandText =SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", decimal.Parse(TxtHolidayWagesIndex.Text));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWorkCenterDocNo));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetLue(LueProductionShiftCode));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CalcPerPersonInd", ChkCalcPerPerson.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePWGDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl(DocNo, DNo, ShopFloorControlDocNo, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ShopFloorControlDocNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl(DocNo, DNo, ShopFloorControlDocNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl2(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, Qty, Qty2, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ShopFloorControlDocNo_" + r.ToString() +
                        ", @ShopFloorControlDNo_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 13));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl2(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl2(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, Qty, Qty2, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @ShopFloorControlDNo, @Qty, @Qty2, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd2, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 13));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl3) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl3(DocNo, DNo, ItCode, Qty, Qty2, ItIndex, Value, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @ItIndex_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@ItIndex_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 13));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl3(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl3(DocNo, DNo, ItCode, Qty, Qty2, ItIndex, Value, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @Qty2, @ItIndex, @Value, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd3, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd3, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd3, Row, 13));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl4) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 2).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd4, r, 2) != 0m)
                    {
                        if (IsFirstOrExisted)
                        {
                            SQL.AppendLine("Insert Into TblPWGDtl4(DocNo, DNo, Value, Wages, Amt, CreateBy, CreateDt) ");
                            SQL.AppendLine("Values ");
                            IsFirstOrExisted = false;
                        }
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine(
                            " (@DocNo, @DNo_" + r.ToString() +
                            ", @Value_" + r.ToString() +
                            ", @Wages_" + r.ToString() +
                            ", @Amt_" + r.ToString() +
                            ", @UserCode, @Dt) ");

                        Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                        Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Wages_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 2));
                        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 3));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl4(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl4(DocNo, DNo, Value, Wages, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @Value, @Wages, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd4, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd4, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl5(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true, IsFirstOrExisted2 = true;
            int DNo = 0;
            SQL.AppendLine("/* Production Wages (Dtl5) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd5.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd5, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, GrdLvlIndex, Qty, Wages, HolInd, HolidayWagesIndex, Wages2, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    DNo += 1;
                    SQL.AppendLine(
                        " (@DocNo, @DNo_1_" + r.ToString() +
                        ", 'O', 'N', @EmpCode_1_" + r.ToString() +
                        ", @GrdLvlIndex_1_" + r.ToString() +
                        ", @Qty_1_" + r.ToString() +
                        ", @Wages_1_" + r.ToString() +
                        ", @HolInd_1_" + r.ToString() +
                        ", @HolidayWagesIndex_1_" + r.ToString() +
                        ", @Wages2_1_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_1_" + r.ToString(), Sm.Right("0000" + DNo.ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@EmpCode_1_" + r.ToString(), Sm.GetGrdStr(Grd5, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex_1_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_1_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Wages_1_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 8));
                    Sm.CmParam<String>(ref cm, "@HolInd_1_" + r.ToString(), Sm.GetGrdBool(Grd5, r, 11) ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex_1_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@Wages2_1_" + r.ToString(), Sm.GetGrdDec(Grd5, r, 10));
                }
            }

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted2)
                    {
                        SQL2.AppendLine("Insert Into TblPWGDtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, GrdLvlIndex, Qty, Wages, HolInd, HolidayWagesIndex, Wages2, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Values ");
                        IsFirstOrExisted2 = false;
                    }
                    else
                        SQL2.AppendLine(", ");

                    DNo += 1;
                    SQL2.AppendLine(
                        " (@DocNo, @DNo_2_" + r.ToString() +
                        ", 'O', 'Y', @EmpCode_2_" + r.ToString() +
                        ", @GrdLvlIndex_2_" + r.ToString() +
                        ", @Qty_2_" + r.ToString() +
                        ", @Wages_2_" + r.ToString() +
                        ", @HolInd_2_" + r.ToString() +
                        ", @HolidayWagesIndex_2_" + r.ToString() +
                        ", @Wages2_2_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_2_" + r.ToString(), Sm.Right("0000" + DNo.ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@EmpCode_2_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex_2_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_2_" + r.ToString(), 0m);
                    Sm.CmParam<Decimal>(ref cm, "@Wages_2_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 7));
                    Sm.CmParam<String>(ref cm, "@HolInd_2_" + r.ToString(), Sm.GetGrdBool(Grd6, r, 10) ? "Y" : "N");
                    Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex_2_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Wages2_2_" + r.ToString(), Sm.GetGrdDec(Grd6, r, 9));
                }
            }

            if (!IsFirstOrExisted)
                SQL.AppendLine("; ");

            if (!IsFirstOrExisted2)
                SQL2.AppendLine("; ");             

            cm.CommandText = SQL.ToString()+SQL2.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl5a(string DocNo, string DNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, GrdLvlIndex, Qty, Wages, HolInd, HolidayWagesIndex, Wages2, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'N', @EmpCode, @GrdLvlIndex, @Qty, @Wages, @HolInd, @HolidayWagesIndex, @Wages2, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", DNo);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd5, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd5, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd5, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd5, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@HolInd", Sm.GetGrdBool(Grd5, Row, 11) ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", Sm.GetGrdDec(Grd5, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@Wages2", Sm.GetGrdDec(Grd5, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SavePWGDtl5b(string DocNo, string DNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, GrdLvlIndex, Qty, Wages, HolInd, HolidayWagesIndex, Wages2, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'Y', @EmpCode, @GrdLvlIndex, @Qty, @Wages, @HolInd, @HolidayWagesIndex, @Wages2, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", DNo);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd6, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd6, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", 0m);
        //    Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd6, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@HolInd", Sm.GetGrdBool(Grd6, Row, 10) ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", Sm.GetGrdDec(Grd6, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Wages2", Sm.GetGrdDec(Grd6, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl6(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl6) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd7.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd7, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl6(DocNo, DNo, EmpCode, ItCode, Qty, Qty2, ItIndex, Value, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @EmpCode_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @ItIndex_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 1));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd7, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd7, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@ItIndex_" + r.ToString(), Sm.GetGrdDec(Grd7, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd7, r, 13));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SavePWGDtl6(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl6(DocNo, DNo, EmpCode, ItCode, Qty, Qty2, ItIndex, Value, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @ItCode, @Qty, @Qty2, @ItIndex, @Value, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd7, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd7, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd7, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd7, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd7, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd7, Row, 13));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePWGDtl7(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Wages (Dtl7) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd8.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd8, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPWGDtl7(DocNo, DNo, EmpCode, ValueFrom, ValueTo, Value, Wages, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @EmpCode_" + r.ToString() +
                        ", @ValueFrom_" + r.ToString() +
                        ", @ValueTo_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @Wages_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("0000" + (r + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@ValueFrom_" + r.ToString(), Sm.GetGrdDec(Grd8, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@ValueTo_" + r.ToString(), Sm.GetGrdDec(Grd8, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd8, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Wages_" + r.ToString(), Sm.GetGrdDec(Grd8, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd8, r, 9));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        //private MySqlCommand SavePWGDtl7(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPWGDtl7(DocNo, DNo, EmpCode, ValueFrom, ValueTo, Value, Wages, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @ValueFrom, @ValueTo, @Value, @Wages, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 4));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd8, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@ValueFrom", Sm.GetGrdDec(Grd8, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@ValueTo", Sm.GetGrdDec(Grd8, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd8, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd8, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd8, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateSFCProcessInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlDtl T1 ");
            SQL.AppendLine("Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.ShopFloorControlDocNo And T1.DNo=T2.ShopFloorControlDNo And T2.DocNo=@DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select B.ShopFloorControlDocNo As DocNo, B.ShopFloorControlDNo As DNo, Sum(B.Qty) Qty, Sum(B.Qty2) Qty2 ");
            SQL.AppendLine("    From TblPWGHdr A ");
            SQL.AppendLine("    Inner Join TblPWGDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct ShopFloorControlDocNo, ShopFloorControlDNo From TblPWGDtl2 Where DocNo=@DocNo ");
            SQL.AppendLine("    ) C On B.ShopFloorControlDocNo=C.ShopFloorControlDocNo And B.ShopFloorControlDNo=C.ShopFloorControlDNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    Group By B.ShopFloorControlDocNo, B.ShopFloorControlDNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo And T1.DNo=T3.DNo ");
            SQL.AppendLine("Set T1.ProcessInd3=");
            SQL.AppendLine("    If(T1.Qty+T1.Qty2<>0 And IfNull(T3.Qty, 0)+IfNull(T3.Qty2, 0)=0, 'O', ");
            SQL.AppendLine("        If(T1.Qty+T1.Qty2-IfNull(T3.Qty, 0)-IfNull(T3.Qty2, 0)=0, 'F', 'P') ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Update TblShopFloorControlHdr T ");
            SQL.AppendLine("Set T.ProcessInd=");
            SQL.AppendLine("    Case When Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd3<>'O' Limit 1) Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If( ");
            SQL.AppendLine("        Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd3<>'F' Limit 1), ");
            SQL.AppendLine("            'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select ShopFloorControlDocNo From TblPWGDtl Where DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPWGHdr());
            cml.Add(UpdateSFCProcessInfo(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToPPR() ||
                IsMinWagesIncentiveExisted();
        }

        private bool IsMinWagesIncentiveExisted()
        {
            return 
                Sm.IsDataExist(
                    "Select DocNo From TblInsentifHdr Where PWGDocNo=@Param And CancelInd='N';",
                    TxtDocNo.Text,
                    "Minimum wages document for this data already existed."
                );
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }


        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPWGHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToPPR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPWGDtl5 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to production payrun.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPWGHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPWGHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                mIsCalcPerPersonProcessData = false;

                ClearData();
                ShowPWGHdr(DocNo);
                ShowPWGDtl(DocNo);
                ShowPWGDtl2(DocNo);
                ShowPWGDtl3(DocNo);
                if (!ChkCalcPerPerson.Checked) ShowPWGDtl4(DocNo);
                ShowPWGDtl5a(DocNo);
                ShowPWGDtl5b(DocNo);
                if (ChkCalcPerPerson.Checked)
                {
                    ShowPWGDtl6(DocNo);
                    ShowPWGDtl7(DocNo);
                }

                ComputeOutstanding();
                ComputeProcessed();
                ComputeBalance();

                ComputeOutstanding2();
                ComputeProcessed2();
                ComputeBalance2();

                ComputeIndex();
                ComputeValue();

                if (ChkCalcPerPerson.Checked)
                    ComputeAmtForEachPerson();
                else
                    ComputeAmt();
                
                ComputeNumberOfEmployee();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsCalcPerPersonProcessData = true;
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPWGHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, WorkCenterDocNo, ProductionShiftCode, HolidayWagesIndex, WagesFormulationCode, " +
                    "UomCode, CurCode, CalcPerPersonInd, Remark " +
                    "From TblPWGHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "WorkCenterDocNo", "ProductionShiftCode", "HolidayWagesIndex", 
                        
                        //6-10
                        "WagesFormulationCode", "UomCode", "CurCode", "CalcPerPersonInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[2]) == "Y");
                        Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueProductionShiftCode, Sm.DrStr(dr, c[4]));
                        TxtHolidayWagesIndex.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        Sm.SetLue(LueWagesFormulationCode, Sm.DrStr(dr, c[6]));
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                        ChkCalcPerPerson.Checked = (Sm.DrStr(dr, c[9]) == "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ShowPWGDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, B.WorkCenterDocNo, C.DocName, A.ShopFloorControlDocNo, B.DocDt, D.ProductionShiftName ");
            SQL.AppendLine("From TblPWGDtl A ");    
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblProductionShift D On B.ProductionShiftCode=D.ProductionShiftCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "WorkCenterDocNo", "DocName", "ShopFloorControlDocNo", "DocDt", "ProductionShiftName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPWGDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.WorkCenterDocNo, D.DocName, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, ");
            SQL.AppendLine("C.ItCode, E.ItName, A.Qty, E.PlanningUomCode, A.Qty2, E.PlanningUomCode2, ");
            SQL.AppendLine("0 As Outstanding, ");
            SQL.AppendLine("0 As Outstanding2 ");
            //SQL.AppendLine("C.Qty-IfNull(F.Qty, 0)+If(G.CancelInd='N', A.Qty, 0) As Outstanding, ");
            //SQL.AppendLine("C.Qty2-IfNull(F.Qty2, 0)+If(G.CancelInd='N', A.Qty2, 0)  As Outstanding2 ");
            SQL.AppendLine("From TblPWGDtl2 A ");    
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl C On A.ShopFloorControlDocNo=C.DocNo And A.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr D On B.WorkCenterDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblItem E On C.ItCode=E.ItCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, "); 
            //SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            //SQL.AppendLine("    From TblPWGHdr T1  ");
            //SQL.AppendLine("    Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblPWGDtl2 T3 On T3.DocNo=@DocNo And T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            ////SQL.AppendLine("    And Exists(Select DocNo From TblPWGDtl2 Where DocNo=@DocNo And ShopFloorControlDocNo=T2.ShopFloorControlDocNo And ShopFloorControlDNo=T2.ShopFloorControlDNo) ");
            //SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            //SQL.AppendLine(") F On A.ShopFloorControlDocNo=F.DocNo And A.ShopFloorControlDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblPWGHdr G On A.DocNo=G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "WorkCenterDocNo", 

                    //1-5
                    "DocName", "ShopFloorControlDocNo", "ShopFloorControlDNo", "ItCode", "ItName", 
                    
                    //6-10
                    "Outstanding", "Qty", "PlanningUomCode", "Outstanding2", "Qty2", 

                    //11
                    "PlanningUomCode2", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = Sm.GetGrdDec(Grd, Row, 8)-Sm.GetGrdDec(Grd, Row, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 9)-Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 12, 13, 14 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowPWGDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, A.Qty, B.PlanningUomCode, A.Qty2, B.PlanningUomCode2, A.ItIndex, A.Value, ");
            //SQL.AppendLine("C.Qty+If(D.CancelInd='N', A.Qty, 0) As Outstanding, ");
            //SQL.AppendLine("C.Qty2+If(D.CancelInd='N', A.Qty2, 0)  As Outstanding2 ");
            SQL.AppendLine("0 As Outstanding, ");
            SQL.AppendLine("0 As Outstanding2 ");
            SQL.AppendLine("From TblPWGDtl3 A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select C2.ItCode, "); 
            //SQL.AppendLine("    Sum(C2.Qty-IfNull(C4.Qty, 0)) As Qty, "); 
            //SQL.AppendLine("    Sum(C2.Qty2-IfNull(C4.Qty2, 0)) As Qty2 ");
            //SQL.AppendLine("    From TblShopFloorControlHdr C1 ");
            //SQL.AppendLine("    Inner Join TblShopFloorControlDtl C2 On C1.DocNo=C2.DocNo ");
            //SQL.AppendLine("    Inner Join TblPWGDtl2 C3 On C2.DocNo=C3.ShopFloorControlDocNo And C2.DNo=C3.ShopFloorControlDNo And C3.DocNo=@DocNo ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, "); 
            //SQL.AppendLine("        Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            //SQL.AppendLine("        From TblPWGHdr T1  ");
            //SQL.AppendLine("        Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        Inner Join TblPWGDtl2 T3 On T3.DocNo=@DocNo And T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            ////SQL.AppendLine("            And Exists(Select DocNo From TblPWGDtl2 Where DocNo=@DocNo And ShopFloorControlDocNo=T2.ShopFloorControlDocNo And ShopFloorControlDNo=T2.ShopFloorControlDNo) ");
            //SQL.AppendLine("        Where T1.CancelInd='N' ");
            //SQL.AppendLine("        Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            //SQL.AppendLine("    ) C4 On C2.DocNo=C4.DocNo And C2.DNo=C4.DNo ");
            //SQL.AppendLine("    Where C1.CancelInd='N' ");
            //SQL.AppendLine("    Group By C2.ItCode ");
            //SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblPWGHdr D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-5
                    "ItCode", "ItName", "Outstanding", "Qty", "PlanningUomCode", 
                    
                    //6-10
                    "Outstanding2", "Qty2", "PlanningUomCode2", "ItIndex", "Value"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Grd.Cells[Row, 6].Value = Sm.GetGrdDec(Grd, Row, 4) - Sm.GetGrdDec(Grd, Row, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Grd.Cells[Row, 10].Value = Sm.GetGrdDec(Grd, Row, 8) - Sm.GetGrdDec(Grd, Row, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 5, 6, 8, 9, 10 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowPWGDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, Value, Wages, Amt ");
            SQL.AppendLine("From TblPWGDtl4 ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-3
                    "Value", "Wages", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 1, 2, 3 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowPWGDtl5a(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.GrdLvlName, ");
            SQL.AppendLine("A.Qty, A.GrdLvlIndex, A.Wages, A.HolidayWagesIndex, A.Wages2, A.HolInd ");
            SQL.AppendLine("From TblPWGDtl5 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CoordinatorInd='N' Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "GrdLvlName", 

                    //6-10
                    "GrdLvlIndex", "Qty", "Wages", "HolidayWagesIndex", "Wages2",
                    
                    //11
                    "HolInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 11);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd5, Grd5.Rows.Count - 1, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowPWGDtl5b(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.GrdLvlName, ");
            SQL.AppendLine("A.GrdLvlIndex, A.Wages, A.HolidayWagesIndex, A.Wages2, A.HolInd, A.CoordinatorIndex ");
            SQL.AppendLine("From TblPWGDtl5 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CoordinatorInd='Y' Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "GrdLvlName", 

                    //6-10
                    "GrdLvlIndex", "Wages", "HolidayWagesIndex", "Wages2", "HolInd",

                    //11
                    "CoordinatorIndex"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);

                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd6, Grd6.Rows.Count - 1, new int[] { 10 });
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 7, 8, 9, });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowPWGDtl6(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.ItCode, E.ItName, A.Qty, E.PlanningUomCode, A.Qty2, E.PlanningUomCode2, A.ItIndex, A.Value ");
            SQL.AppendLine("From TblPWGDtl6 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblItem E On A.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "ItCode", 

                    //6-10
                    "ItName", "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", 

                    //11-12
                    "ItIndex", "Value"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 8, 10, 12, 13 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowPWGDtl7(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.ValueFrom, A.ValueTo, A.Value, A.Wages, A.Amt ");
            SQL.AppendLine("From TblPWGDtl7 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd8, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo",  

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "ValueFrom", 

                    //6-9
                    "ValueTo", "Value", "Wages", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd8, Grd8.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9 });
            Sm.FocusGrd(Grd8, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetParameter()
        {
            var Param = string.Empty;
            
            Param = Sm.GetParameter("NumberOfPlanningUomCode");
            if (Param.Length!=0) mNumberOfPlanningUomCode = int.Parse(Param);

            //Param = Sm.GetParameter("HolidayIndex");
            //if (Param.Length != 0) mHolidayWagesIndex = Decimal.Parse(Param)/10m;

            mHolidayWagesIndex = Sm.GetParameterDec("HolidayWagesIndex");
            mWorkingHr = Sm.GetParameterDec("WorkingHr");
            //Param = Sm.GetParameter("NotHolidayWagesIndex");
            //if (Param.Length != 0) mNotHolidayWagesIndex = Decimal.Parse(Param);
        }

        private void SetHolidayWagesIndex()
        {
            try
            {
                TxtHolName.EditValue = null;
                TxtHolidayWagesIndex.EditValue = mNotHolidayWagesIndex;

                var DocDt = Sm.GetDte(DteDocDt);

                if (DocDt.Length != 0)
                {
                    var cm = new MySqlCommand()
                    { CommandText = "Select HolName From TblHoliday Where HolDt=@HolDt Limit 1;" };
                    Sm.CmParamDt(ref cm, "@HolDt", DocDt);
                    var HolName=Sm.GetValue(cm);
                    if (HolName.Length!=0)
                    {
                        TxtHolName.Text = HolName;
                        TxtHolidayWagesIndex.EditValue = mHolidayWagesIndex;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowWagesFormulationUom()
        {
            try
            {
                TxtUomCode.EditValue = null;
                var WagesFormulationCode = Sm.GetLue(LueWagesFormulationCode);

                if (WagesFormulationCode.Length != 0)
                {
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select UomCode From TblWagesFormulationHdr " +
                            "Where WagesFormulationCode=@WagesFormulationCode;"
                    };
                    Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
                    TxtUomCode.EditValue = Sm.GetValue(cm);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedDocNo()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedSFC()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSelectedSFC2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                        SQL += 
                            "##" + 
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            Sm.GetGrdStr(Grd2, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowDocInfo()
        {
            if (!mIsCalcPerPersonProcessData) return;
            try
            {
                ClearGrd2();
                ClearGrd3();
                ClearGrd4();
                ClearGrd5();
                ClearGrd6();
                ClearGrd7();
                ClearGrd8();

                if (ChkCalcPerPerson.Checked)
                    ShowDocInfo2();
                else
                    ShowDocInfo1();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowDocInfo1()
        {
            bool IsExisted = IsProcessedSFCExisted();

            ShowSCFItem(1, IsExisted);
            ShowItem(1, IsExisted);

            ComputeOutstanding();
            ComputeProcessed();
            ComputeBalance();

            ComputeOutstanding2();
            ComputeProcessed2();
            ComputeBalance2();

            ComputeIndex();
            ComputeValue();

            ComputeWagesBasedOnFormula();
            ComputeAmt();

            ComputeNonCoordinatorWages();
            ComputeNonCoordinatorHolidayWages();

            ComputeNumberOfEmployee();
            
            ComputeCoordinatorWages();
            ComputeCoordinatorHolidayWages();
        }

        private void ShowDocInfo2()
        {
            bool IsExisted = IsProcessedSFCExisted();

            ShowSCFItem(2, IsExisted);
            ShowItem(2, IsExisted);

            ShowEmpItem();

            ComputeOutstanding();
            ComputeProcessed();
            ComputeBalance();

            ComputeOutstanding2();
            ComputeProcessed2();
            ComputeBalance2();

            ComputeIndex();
            ComputeValue();
            ComputeAmt();
            
            ShowWagesFormulaForEachPerson();

            ShowNonCoordinatorForEachPerson();
            ComputeNonCoordinatorHolidayWages();

            ComputeNumberOfEmployee();

            ComputeCoordinatorWages();
            ComputeCoordinatorHolidayWages();

        }

        private void ShowSCFItem(Byte DocType, bool IsExisted)
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 5));
                        No += 1;
                    }
                }
            }

            if (Filter.Length == 0) Filter = " 0=0 ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.WorkCenterDocNo, D.DocName As WorkCenterDocName, ");
            SQL.AppendLine("B.ItCode, E.ItName, E.PlanningUomCode, E.PlanningUomCode2, ");
            if (IsExisted)
            {
                SQL.AppendLine("B.Qty-IfNull(C.Qty, 0) As Qty, ");
                SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0) As Qty2 ");
            }
            else
            {
                SQL.AppendLine("B.Qty, B.Qty2 ");
            }
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo And B.ProcessInd3<>'F' ");
            if (IsExisted)
            {
                SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
                //SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
                //SQL.AppendLine("    From TblPWGHdr T1  ");
                //SQL.AppendLine("    Inner Join TblPWGDtl2 T2 ");
                //SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        And Exists( ");
                //SQL.AppendLine("            Select T.DocNo From TblShopFloorControlDtl T ");
                //SQL.AppendLine("            Where T.ProcessInd3<>'F' ");
                //SQL.AppendLine("            And T.DocNo=T2.ShopFloorControlDocNo ");
                //SQL.AppendLine("            And T.DNo=T2.ShopFloorControlDNo ");
                //SQL.AppendLine("            And ( " + Filter.Replace("A.", "T.") + ")");
                //SQL.AppendLine("        ) ");
                //SQL.AppendLine("    Where T1.CancelInd='N' ");
                //SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");

                SQL.AppendLine("    Select T1.ShopFloorControlDocNo As DocNo, T1.ShopFloorControlDNo As DNo, ");
                SQL.AppendLine("    Sum(T1.Qty) As Qty, Sum(T1.Qty2) As Qty2 ");
                SQL.AppendLine("    From TblPWGDtl2 T1  ");
                SQL.AppendLine("    Inner Join TblPWGHdr T2 ");
                SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("            Select T.DocNo From TblShopFloorControlDtl T ");
                SQL.AppendLine("            Where T.ProcessInd3<>'F' ");
                SQL.AppendLine("            And T.DocNo=T1.ShopFloorControlDocNo ");
                SQL.AppendLine("            And T.DNo=T1.ShopFloorControlDNo ");
                SQL.AppendLine("            And ( " + Filter.Replace("A.", "T.") + ")");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    Group By T1.ShopFloorControlDocNo, T1.ShopFloorControlDNo ");
                SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            }
            SQL.AppendLine("Inner Join TblWorkCenterHdr D On A.WorkCenterDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode");
            SQL.AppendLine("Where A.CancelInd='N' ");


            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString() + 
                " And (" + Filter + ") " + 
                " Order By A.DocNo;", // SQL.ToString(),
                new string[] 
                { 
                    //0
                    "WorkCenterDocNo", 

                    //1-5
                    "WorkCenterDocName", "DocNo", "DNo", "ItCode", "ItName", 
                    
                    //6-9
                    "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    if (DocType == 1)
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Grd.Cells[Row, 10].Value = 0m;
                    }
                    else
                    {
                        Grd.Cells[Row, 9].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                    }
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    if (DocType == 1)
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                        Grd.Cells[Row, 14].Value = 0m;
                    }
                    else
                    {
                        Grd.Cells[Row, 13].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 8);
                    }
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 12, 13, 14 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowItem(Byte DocType, bool IsExisted)
        {
            var cm = new MySqlCommand();

            var Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 5));
                        No += 1;
                    }
                }
            }

            if (Filter.Length == 0) Filter = " 0=0 ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.ItCode, Tbl2.ItName, Tbl2.PlanningUomCode, Tbl2.PlanningUomCode2, ");
            SQL.AppendLine("Tbl1.Qty, Tbl1.Qty2, IfNull(Tbl3.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("Case @UomCode ");
            SQL.AppendLine("When Tbl2.PlanningUomCode Then (Tbl1.Qty*IfNull(Tbl3.ItIndex, 0)) ");
            SQL.AppendLine("When Tbl2.PlanningUomCode2 Then (Tbl1.Qty2*IfNull(Tbl3.ItIndex, 0)) ");
            SQL.AppendLine("Else 0 End As Value ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select B.ItCode, ");
            if (IsExisted)
            {
                SQL.AppendLine("    Sum(B.Qty-IfNull(C.Qty, 0)) As Qty, ");
                SQL.AppendLine("    Sum(B.Qty2-IfNull(C.Qty2, 0)) As Qty2 ");
            }
            else
            {
                SQL.AppendLine("    Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2 ");
            }
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo And B.ProcessInd3<>'F' ");
            if (IsExisted)
            {
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
                SQL.AppendLine("        Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
                SQL.AppendLine("        From TblPWGHdr T1  ");
                SQL.AppendLine("        Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("        Inner Join TblShopFloorControlDtl T3 ");
                //SQL.AppendLine("            On T2.ShopFloorControlDocNo=T3.DocNo ");
                //SQL.AppendLine("            And T2.ShopFloorControlDNo=T3.DNo ");
                //SQL.AppendLine("            And T3.ProcessInd3<>'F' ");
                //SQL.AppendLine("            And ( " + Filter.Replace("A.", "T3.") + ")");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("        Select T.DocNo From TblShopFloorControlDtl T ");
                SQL.AppendLine("        Where T.ProcessInd3<>'F' ");
                SQL.AppendLine("        And T.DocNo=T2.ShopFloorControlDocNo ");
                SQL.AppendLine("        And T.DNo=T2.ShopFloorControlDNo ");
                SQL.AppendLine("            And ( " + Filter.Replace("A.", "T.") + ")");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        Where T1.CancelInd='N' ");
                SQL.AppendLine("        Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
                SQL.AppendLine("    ) C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            }
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And (" + Filter + ")  ");
            //SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Group By B.ItCode ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblItem Tbl2 On Tbl1.ItCode=Tbl2.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex Tbl3 On Tbl1.ItCode=Tbl3.ItCode");
            SQL.AppendLine("Order By Tbl2.ItName, Tbl1.ItCode;");

            // Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.CmParam<String>(ref cm, "@UomCode", (TxtUomCode.Text.Length == 0 ? "XXX" : TxtUomCode.Text));
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2",

                    //6-7
                    "ItIndex", "Value"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                    if (DocType == 1)
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                        Grd.Cells[Row, 6].Value = 0m;
                    }
                    else
                    {
                        Grd.Cells[Row, 5].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 2);
                    }
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    if (DocType == 1)
                    {
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 4);
                        Grd.Cells[Row, 10].Value = 0m;
                    }
                    else
                    {
                        Grd.Cells[Row, 9].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 4);
                    }
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 7);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd5, Grd5.Rows.Count - 1, new int[] { 11 });
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 5, 6, 8, 9, 10, 12, 13 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private bool IsProcessedSFCExisted()
        {
            return ChkCalcPerPerson.Checked;

            //var cm = new MySqlCommand();
            //var Filter = string.Empty;

            //if (Grd1.Rows.Count != 1)
            //{
            //    int No = 1;
            //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
            //        {
            //            if (Filter.Length > 0) Filter += " Or ";
            //            Filter += "(T.DocNo=@DocNo" + No + ") ";
            //            Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 5));
            //            No += 1;
            //        }
            //    }
            //}
            //if (Filter.Length == 0) Filter = " 0=0 ";

            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select T1.DocNo ");
            //SQL.AppendLine("From TblPWGHdr T1  ");
            //SQL.AppendLine("Inner Join TblPWGDtl2 T2 ");
            //SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And Exists( ");
            //SQL.AppendLine("            Select T.DocNo From TblShopFloorControlDtl T ");
            //SQL.AppendLine("            Where T.ProcessInd3<>'F' ");
            //SQL.AppendLine("            And T.DocNo=T2.ShopFloorControlDocNo ");
            //SQL.AppendLine("            And T.DNo=T2.ShopFloorControlDNo ");
            //SQL.AppendLine("            And ( " + Filter + ") ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("Limit 1;");

            //cm.CommandText = SQL.ToString();
            //return Sm.IsDataExist(cm);
        }

        private decimal ComputeAmtGrd3(int Col)
        {
            decimal Amt = 0m;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, Col).Length != 0)
                        Amt += Sm.GetGrdDec(Grd3, Row, Col);       
            }
            return Amt;
        }

        private decimal ComputeAmtGrd4(int Col)
        {
            decimal Amt = 0m;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, Col).Length != 0)
                        Amt += Sm.GetGrdDec(Grd4, Row, Col);
            }
            return Amt;
        }

        private void ComputeOutstanding() 
        {
            TxtOutstanding.EditValue = Sm.FormatNum(ComputeAmtGrd3(4), 0);
        }

        private void ComputeProcessed()
        {
            TxtProcessed.EditValue = Sm.FormatNum(ComputeAmtGrd3(5), 0);
        }

        private void ComputeBalance()
        {
            TxtBalance.EditValue = Sm.FormatNum(ComputeAmtGrd3(6), 0);
        }

        private void ComputeOutstanding2()
        {
            TxtOutstanding2.EditValue = Sm.FormatNum(ComputeAmtGrd3(8), 0);
        }

        private void ComputeProcessed2()
        {
            TxtProcessed2.EditValue = Sm.FormatNum(ComputeAmtGrd3(9), 0);
        }

        private void ComputeBalance2()
        {
            TxtBalance2.EditValue = Sm.FormatNum(ComputeAmtGrd3(10), 0);
        }

        private void ComputeIndex()
        {
            TxtIndex.EditValue = Sm.FormatNum(ComputeAmtGrd3(12), 0);
        }

        private void ComputeValue()
        {
            TxtValue.EditValue = Sm.FormatNum(ComputeAmtGrd3(13), 0);
        }

        private void ComputeAmt()
        {
            TxtAmt.EditValue = Sm.FormatNum(ComputeAmtGrd4(3), 0);
        }

        private void ComputeWagesBasedOnFormula()
        {
            decimal Value = 0m, From = 0m, To = 0m, Wages = 0m, Temp = 0m;

            if (TxtValue.Text.Length != 0)
                Value = decimal.Parse(TxtValue.Text);

            if (Value <= 0) return;

            Temp = Value;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ValueFrom, ValueTo, Wages ");
            SQL.AppendLine("From TblWagesFormulationDtl ");
            SQL.AppendLine("Where WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Order By ValueFrom;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "ValueFrom", "ValueTo", "Wages" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd4.BeginUpdate();
                        while (dr.Read())
                        {
                            From = Sm.DrDec(dr, 0);
                            To = Sm.DrDec(dr, 1);
                            Wages = Sm.DrDec(dr, 2);

                            Grd4.Rows.Add();
                            if (Value >= From)
                            {
                                if (Temp <= To)
                                {
                                    Grd4.Cells[Row, 1].Value = Temp;
                                    Grd4.Cells[Row, 2].Value = Wages;
                                    Grd4.Cells[Row, 3].Value = Temp * Wages;
                                    Temp = 0m;
                                }
                                else
                                {
                                    Grd4.Cells[Row, 1].Value = To;
                                    Grd4.Cells[Row, 2].Value = Wages;
                                    Grd4.Cells[Row, 3].Value = To * Wages;
                                    Temp -= To;
                                }
                            }
                            else
                                Temp = 0m;
                            if (Temp == 0m) break;
                            Row++;
                        }
                    }
                    Grd4.EndUpdate();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 1, 2, 3 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ComputeNonCoordinatorWages()
        {
            decimal Amt = 0m;

            if (TxtAmt.Text.Length != 0)
                Amt = decimal.Parse(TxtAmt.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("E.GrdLvlName, IfNull(F.GrdLvlIndexW, 1) As GrdLvlIndex, A.Qty, ");
            SQL.AppendLine("Case When G.TotalQty=0 Then 0 ");
            SQL.AppendLine("Else (A.Qty/G.TotalQty)*@Amt*IfNull(F.GrdLvlIndexW, 1) ");
            SQL.AppendLine("End As Wages ");
            SQL.AppendLine("From ( " );
            SQL.AppendLine("    Select BomCode As EmpCode, Sum(Qty) As Qty ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine("    Group By BomCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            //SQL.AppendLine("    And B.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 F ");
            SQL.AppendLine("    On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("    And F.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Sum(Qty) As TotalQty ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine(") G On 0=0 ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);

            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-5
                               "EmpName", "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", 

                               //6-7
                               "Qty", "Wages",
                               
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ComputeNonCoordinatorHolidayWages()
        {
            //decimal HolidayWagesIndex = 1m;

            //if (TxtHolidayWagesIndex.Text.Length != 0)
            //    HolidayWagesIndex = decimal.Parse(TxtHolidayWagesIndex.Text);

            if (Grd5.Rows.Count > 1)
            {
                SetNonCoordinatorHoliday();
                Grd5.BeginUpdate();
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    //Grd5.Cells[Row, 9].Value = HolidayWagesIndex;
                    Grd5.Cells[Row, 10].Value = Sm.GetGrdDec(Grd5, Row, 9) * Sm.GetGrdDec(Grd5, Row, 8);
                    //Grd5.Cells[Row, 11].Value = TxtHolName.Text.Length>0;
                }
                Grd5.EndUpdate();
            }
        }

        private void SetNonCoordinatorHoliday()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode= string.Empty;

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0)
                {
                    Grd5.Cells[Row, 9].Value = mNotHolidayWagesIndex;
                    Grd5.Cells[Row, 11].Value = false;
                }
            }

            int No = 1;
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd5, Row, 1));
                    No += 1;
                }
            }

            if (Filter.Length==0)
                Filter = " And 0=1 ";
            else
                Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.EmpCode, T2.HolidayInd ");
            SQL.AppendLine("From TblEmpWorkSchedule T1 ");
            SQL.AppendLine("Inner Join TblWorkSchedule T2 On T1.WSCode=T2.WSCode ");
            SQL.AppendLine("Where Dt=@Dt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteDocDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        if (Sm.DrStr(dr, c[1])=="Y")
                        {
                            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd5, Row, 1), EmpCode))
                                {
                                    Grd5.Cells[Row, 9].Value = mHolidayWagesIndex;
                                    Grd5.Cells[Row, 11].Value = true;
                                }
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ComputeNumberOfEmployee()
        {
            int EmpCount = 0;

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0) EmpCount += 1;
            }
            TxtEmpCount.EditValue = Sm.FormatNum(EmpCount, 1);
        }

        private void ComputeCoordinatorWages()
        {
            decimal Amt = 0m, EmpCount = 0m;

            if (TxtAmt.Text.Length != 0)
                Amt = decimal.Parse(TxtAmt.Text);

            if (TxtEmpCount.Text.Length != 0)
                EmpCount = decimal.Parse(TxtEmpCount.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("E.GrdLvlName, IfNull(F.GrdLvlIndexW, 1) As GrdLvlIndex, ");
            SQL.AppendLine("Case When @WorkingHr=0 Then 0 ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When G.TotalQty=0 Then 0 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When (G.TotalQty/G.TotalRecord)>=@WorkingHr Then 1 ");
            SQL.AppendLine("        Else ((G.TotalQty/G.TotalRecord)/@WorkingHr) End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("End As CoordinatorIndex, ");
            SQL.AppendLine("Case When @EmpCount=0 Then 0 ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When @WorkingHr=0 Then 0 ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When G.TotalQty=0 Then ");
            SQL.AppendLine("            ((1/@EmpCount)*@Amt*IfNull(F.GrdLvlIndexW, 1)) ");
            SQL.AppendLine("        Else ");
            SQL.AppendLine("            Case When (G.TotalQty/G.TotalRecord)>@WorkingHr Then 1 ");
            SQL.AppendLine("            Else ((G.TotalQty/G.TotalRecord)/@WorkingHr) End ");
            SQL.AppendLine("                * ((1/@EmpCount)*@Amt*IfNull(F.GrdLvlIndexW, 1)) ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("End As Wages ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='Y' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            //SQL.AppendLine("    And B.PosCode=IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 F ");
            SQL.AppendLine("    On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("    And F.WagesFormulationCode=@WagesFormulationCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select BomCode As EmpCode, Count(BomCode) As TotalRecord, Sum(Qty) As TotalQty ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='Y' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Group By BomCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);
            Sm.CmParam<Decimal>(ref cm, "@EmpCount", EmpCount);
            Sm.CmParam<Decimal>(ref cm, "@WorkingHr", mWorkingHr);

            Sm.ShowDataInGrid(
                    ref Grd6, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-5
                               "EmpName", "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", 

                               //6-7
                               "CoordinatorIndex", "Wages"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd6, Grd6.Rows.Count - 1, new int[] { 10 });
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 7, 8, 9, 11 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ComputeCoordinatorHolidayWages()
        {
            //decimal HolidayWagesIndex = 1m;

            //if (TxtHolidayWagesIndex.Text.Length != 0)
            //    HolidayWagesIndex = decimal.Parse(TxtHolidayWagesIndex.Text);

            if (Grd6.Rows.Count > 1)
            {
                SetCoordinatorHoliday();
                Grd6.BeginUpdate();
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    //Grd6.Cells[Row, 8].Value = HolidayWagesIndex;
                    Grd6.Cells[Row, 9].Value = Sm.GetGrdDec(Grd6, Row, 8) * Sm.GetGrdDec(Grd6, Row, 7);
                    //Grd6.Cells[Row, 10].Value = TxtHolName.Text.Length > 0;
                }
                Grd6.EndUpdate();
            }
        }

        private void SetCoordinatorHoliday()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                {
                    Grd6.Cells[Row, 8].Value = mNotHolidayWagesIndex;
                    Grd6.Cells[Row, 10].Value = false;
                }
            }

            int No = 1;
            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T1.EmpCode=@EmpCode" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), Sm.GetGrdStr(Grd6, Row, 1));
                    No += 1;
                }
            }

            if (Filter.Length == 0)
                Filter = " And 0=1 ";
            else
                Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select T1.EmpCode, T2.HolidayInd ");
            SQL.AppendLine("From TblEmpWorkSchedule T1 ");
            SQL.AppendLine("Inner Join TblWorkSchedule T2 On T1.WSCode=T2.WSCode ");
            SQL.AppendLine("Where Dt=@Dt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteDocDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "HolidayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        if (Sm.DrStr(dr, c[1]) == "Y")
                        {
                            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd6, Row, 1), EmpCode))
                                {
                                    Grd6.Cells[Row, 8].Value = mHolidayWagesIndex;
                                    Grd6.Cells[Row, 10].Value = true;
                                }
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ShowEmpItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.EmpCode, Tbl2.EmpName, Tbl3.PosName, Tbl4.DeptName, ");
            SQL.AppendLine("Tbl5.ItCode, Tbl6.ItName, Tbl6.PlanningUomCode, Tbl6.PlanningUomCode2, Tbl7.ItIndex ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblEmployee Tbl2 ");
            SQL.AppendLine("    On Tbl1.EmpCode=Tbl2.EmpCode ");
            SQL.AppendLine("    And Tbl2.ResignDt Is Null ");
            //SQL.AppendLine("    And Tbl2.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Left Join TblPosition Tbl3 On Tbl2.PosCode=Tbl3.PosCode ");
            SQL.AppendLine("Left Join TblDepartment Tbl4 On Tbl2.DeptCode=Tbl4.DeptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Distinct B.ItCode ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo And B.ProcessInd3<>'F' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") Tbl5 On 0=0 ");
            SQL.AppendLine("Left Join TblItem Tbl6 On Tbl5.ItCode=Tbl6.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex Tbl7 On Tbl5.ItCode=Tbl7.ItCode");
            SQL.AppendLine("Order By Tbl4.DeptName, Tbl2.EmpName, Tbl1.EmpCode, Tbl6.ItName;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());

            Sm.ShowDataInGrid(
                    ref Grd7, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-5
                               "EmpName", "PosName", "DeptName", "ItCode", "ItName", 
                               
                               //6-8
                               "PlanningUomCode", "PlanningUomCode2", "ItIndex"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 8, 10, 13 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 8, 10, 12, 13 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowWagesFormulaForEachPerson()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, ");
            SQL.AppendLine("E.ValueFrom, E.ValueTo, E.Wages ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            //SQL.AppendLine("    And B.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl E On E.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode, E.ValueFrom;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));

            Sm.ShowDataInGrid(
                   ref Grd8, ref cm, SQL.ToString(),
                   new string[] 
                   { 
                       //0
                       "EmpCode",

                       //1-5
                       "EmpName", "PosName", "DeptName", "ValueFrom", "ValueTo", 
                       
                       //6
                       "Wages"
                   },
                   (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 7, 9 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd8, Grd8.Rows.Count - 1, new int[] { 5, 6, 7, 8 });
            Sm.FocusGrd(Grd8, 0, 1);
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        private void ComputeValue(int Row)
        {
            string UomCode = TxtUomCode.Text;

            Decimal Qty = 0m, ItemIndex = 0m;

            if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd7, Row, 9)))
            {
                if (Sm.GetGrdStr(Grd7, Row, 8).Length > 0) Qty = Sm.GetGrdDec(Grd7, Row, 8);
            }
            else
            {
                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd7, Row, 11)))
                {
                    if (Sm.GetGrdStr(Grd7, Row, 10).Length > 0) Qty = Sm.GetGrdDec(Grd7, Row, 10);
                }
            }

            if (Sm.GetGrdStr(Grd7, Row, 12).Length > 0) ItemIndex = Sm.GetGrdDec(Grd7, Row, 12);

            Grd7.Cells[Row, 13].Value = Qty * ItemIndex;
        }

        private void UpdateWagesCalculatedPerPersonInfo()
        {
            ComputeItemProcessed();
            ComputeSFCProcessed();
        }

        private void ComputeItemProcessed()
        {
            decimal Qty = 0m, Qty2 = 0m;
            if (Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (Sm.GetGrdStr(Grd3, Row3, 1).Length != 0)
                    {
                        Qty = 0m;
                        Qty2 = 0m;
                        for (int Row7 = 0; Row7 < Grd7.Rows.Count; Row7++)
                        {
                            if (
                                Sm.GetGrdStr(Grd7, Row7, 5).Length != 0 &&
                                Sm.CompareGrdStr(Grd3, Row3, 1, Grd7, Row7, 5) 
                                )
                            {
                                Qty +=Sm.GetGrdDec(Grd7, Row7, 8);
                                Qty2 += Sm.GetGrdDec(Grd7, Row7, 10);
                            }
                        }
                        Grd3.Cells[Row3, 5].Value = Qty;
                        Grd3.Cells[Row3, 6].Value = Sm.GetGrdDec(Grd3, Row3, 4)-Qty;
                        Grd3.Cells[Row3, 9].Value = Qty2;
                        Grd3.Cells[Row3, 10].Value = Sm.GetGrdDec(Grd3, Row3, 8) - Qty2;
                    }
                }
            }

            ComputeProcessed();
            ComputeBalance();
            ComputeProcessed2();
            ComputeBalance2();
        }

        private void ComputeSFCProcessed()
        {
            decimal Qty = 0m, Qty2 = 0m;
            int Row = -1;
            if (Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (Sm.GetGrdStr(Grd3, Row3, 1).Length != 0)
                    {
                        Qty = Sm.GetGrdDec(Grd3, Row3, 5);
                        Qty2 = Sm.GetGrdDec(Grd3, Row3, 9);

                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row2, 5).Length != 0 &&
                                Sm.CompareGrdStr(Grd3, Row3, 1, Grd2, Row2, 5)
                                )
                            {
                                // Compute Qty 1
                                if (Sm.GetGrdDec(Grd2, Row2, 8) >= Qty)
                                {
                                    Grd2.Cells[Row2, 9].Value = Qty;
                                }
                                else
                                {
                                    Grd2.Cells[Row2, 9].Value = Sm.GetGrdDec(Grd2, Row2, 8);
                                }
                                Grd2.Cells[Row2, 10].Value = Sm.GetGrdDec(Grd2, Row2, 8) - Sm.GetGrdDec(Grd2, Row2, 9);
                                Qty -= Sm.GetGrdDec(Grd2, Row2, 9);


                                // Compute Qty 2

                                if (Sm.GetGrdDec(Grd2, Row2, 12) >= Qty2)
                                {
                                    Grd2.Cells[Row2, 13].Value = Qty2;
                                }
                                else
                                {
                                    Grd2.Cells[Row2, 13].Value = Sm.GetGrdDec(Grd2, Row2, 12);
                                }
                                Grd2.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd2, Row2, 12) - Sm.GetGrdDec(Grd2, Row2, 13);
                                Qty2 -= Sm.GetGrdDec(Grd2, Row2, 13);

                                Row = Row2;
                            }
                        }

                        //Jika masih sisa
                        if ((Qty > 0 || Qty2 > 0) && Row >= 0)
                        {
                            Grd2.Cells[Row, 9].Value = Sm.GetGrdDec(Grd2, Row, 9)+Qty;
                            Grd2.Cells[Row, 10].Value = Sm.GetGrdDec(Grd2, Row, 8) - Sm.GetGrdDec(Grd2, Row, 9);
                            Grd2.Cells[Row, 13].Value = Sm.GetGrdDec(Grd2, Row, 13)+Qty2;
                            Grd2.Cells[Row, 14].Value = Sm.GetGrdDec(Grd2, Row, 12) - Sm.GetGrdDec(Grd2, Row, 13);
                        }
                    }
                }
            }
        }

        private void ComputeWagesBasedOnFormulaForEachPerson(string EmpCode)
        {
            decimal Value = 0m;
            if (Grd7.Rows.Count > 1)
            {
                for (int Row7 = 0; Row7 < Grd7.Rows.Count; Row7++)
                    if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd7, Row7, 1)))
                        Value += Sm.GetGrdDec(Grd7, Row7, 13);
            }

            int Row = -1;
            if (Grd8.Rows.Count > 1)
            {
                //mendapatkan baris untuk empcode
                for (int Row8 = 0; Row8 < Grd8.Rows.Count; Row8++)
                {
                    if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd8, Row8, 1)))
                    {    
                        Row = Row8;
                        break;
                    }
                }

                if (Row >= 0)
                {
                    //set 0 utk value dan amount 
                    for (int Row8 = Row; Row8 < Grd8.Rows.Count; Row8++)
                    {
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd8, Row8, 1)))
                        {
                            Grd8.Cells[Row8, 7].Value = 0m;
                            Grd8.Cells[Row8, 9].Value = 0m;
                        }
                        else
                            break;
                    }

                    decimal Temp = Value, To = 0m;

                    for (int Row8 = Row; Row8 < Grd8.Rows.Count; Row8++)
                    {
                        if (Row8 == Row && Value < Sm.GetGrdDec(Grd8, Row8, 5))
                        {
                            Temp = 0m;
                            break;
                        }

                        To = Sm.GetGrdDec(Grd8, Row8, 6);

                        if (Temp <= To)
                        {
                            Grd8.Cells[Row8, 7].Value = Temp;
                            Grd8.Cells[Row8, 9].Value = Temp * Sm.GetGrdDec(Grd8, Row8, 8);
                            Temp = 0m;
                        }
                        else
                        {
                            Grd8.Cells[Row8, 7].Value = To;
                            Grd8.Cells[Row8, 9].Value = To * Sm.GetGrdDec(Grd8, Row8, 8);
                            Temp -= To;
                        }

                        if (Temp == 0m) break;
                    }
                }
            }
        }

        private void ShowNonCoordinatorForEachPerson()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.GrdLvlName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct BomCode As EmpCode ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where BomType='3' ");
            SQL.AppendLine("    And CoordinatorInd='N' ");
            SQL.AppendLine("    And Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            //SQL.AppendLine("    And B.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName, A.EmpCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-4
                               "EmpName", "PosName", "DeptName", "GrdLvlName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 6, 7, 8, 9, 10 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ComputeNonCoordinatorWagesForEachPerson(string EmpCode)
        {
            int Row = -1;
            if (Grd5.Rows.Count > 1)
            {
                //mendapatkan baris untuk empcode
                for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                {
                    if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd5, Row5, 1)))
                    {
                        Row = Row5;
                        break;
                    }
                }
            }

            decimal Wages = 0m;
            if (Row >= 0)
            { 
                if (Grd8.Rows.Count > 1)
                {
                    for (int Row8 = 0; Row8 < Grd8.Rows.Count; Row8++)
                        if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(Grd8, Row8, 1)))
                            Wages += Sm.GetGrdDec(Grd8, Row8, 9);
                }
                Grd5.Cells[Row, 8].Value = Wages;
                Grd5.Cells[Row, 10].Value = Wages * Sm.GetGrdDec(Grd5, Row, 9);
            }
            ComputeAmtForEachPerson();
            ComputeCoordinatorWagesForEachPerson();
        }

        private void ComputeAmtForEachPerson()
        {
            decimal Amt = 0m;
            
            if (Grd5.Rows.Count > 1)
            {
                for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                    if (Sm.GetGrdStr(Grd5, Row5, 8).Length>0)
                        Amt += Sm.GetGrdDec(Grd5, Row5, 8);
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ComputeCoordinatorWagesForEachPerson()
        {
            decimal Amt = 0m, EmpCount = 0m;

            if (TxtAmt.Text.Length != 0)
                Amt = decimal.Parse(TxtAmt.Text);

            if (TxtEmpCount.Text.Length != 0)
                EmpCount = decimal.Parse(TxtEmpCount.Text);

            if (Grd6.Rows.Count > 1)
            {
                for (int Row6 = 0; Row6 < Grd6.Rows.Count; Row6++)
                {
                    if (Sm.GetGrdStr(Grd6, Row6, 1).Length>0)
                    {
                        if (EmpCount == 0m)
                            Grd6.Cells[Row6, 7].Value = 0m;
                        else
                        {
                            if (Sm.GetGrdDec(Grd6, Row6, 11)==0)
                                Grd6.Cells[Row6, 7].Value = (1 / EmpCount) * Amt * Sm.GetGrdDec(Grd6, Row6, 6);
                            else
                                Grd6.Cells[Row6, 7].Value = Sm.GetGrdDec(Grd6, Row6, 11)*((1 / EmpCount) * Amt * Sm.GetGrdDec(Grd6, Row6, 6));
                        }
                        Grd6.Cells[Row6, 9].Value = Sm.GetGrdDec(Grd6, Row6, 7) * Sm.GetGrdDec(Grd6, Row6, 8);
                    }
                }
            }
        }

        //private void RecomputeOutstanding()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select A.DocNo, B.DNo, ");
        //    SQL.AppendLine("B.Qty-IfNull(C.Qty, 0) As Qty, ");
        //    SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0) As Qty2 ");
        //    SQL.AppendLine("From TblShopFloorControlHdr A ");
        //    SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Left Join ( ");
        //    SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
        //    SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
        //    SQL.AppendLine("    From TblPWGHdr T1  ");
        //    SQL.AppendLine("    Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.DocNo ");
        //    SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
        //    SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
        //    SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
        //    SQL.AppendLine("        And T3.ProcessInd3<>'F' ");
        //    SQL.AppendLine("    Where T1.CancelInd='N' ");
        //    SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
        //    SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
        //    SQL.AppendLine("Where Locate(Concat('##', A.DocNo, B.DNo, '##'), @GetSelectedSFC)>0 ");
        //    SQL.AppendLine("Order By A.DocNo, B.DNo;");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand()
        //        {
        //            Connection = cn,
        //            CommandText = SQL.ToString()
        //        };
        //        Sm.CmParam<String>(ref cm, "@SelectedSFC", GetSelectedSFC2());
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty", "Qty2" });

        //        if (dr.HasRows) 
        //        {
        //            Grd2.BeginUpdate();
        //            while (dr.Read())
        //            {
        //                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
        //                {
        //                    if (Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 2), Sm.DrStr(dr, 0)) &&
        //                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 3), Sm.DrStr(dr, 1)))
        //                    {
        //                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 2);
        //                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 12, 3);

        //                        Grd2.Cells[Row, 10].Value = Sm.GetGrdDec(Grd2, Row, 8)-Sm.GetGrdDec(Grd2, Row, 9);
        //                        Grd2.Cells[Row, 14].Value = Sm.GetGrdDec(Grd2, Row, 12) - Sm.GetGrdDec(Grd2, Row, 13);
        //                        break;
        //                    }
        //                }
        //            }
        //            Grd2.EndUpdate();
        //        }
        //        dr.Close();
        //    }
        //}

        private void SetWagesFormulationCode()
        {
            LueWagesFormulationCode.EditValue = null;

            try
            {
                string 
                    DocDt = Sm.GetDte(DteDocDt),
                    WorkCenterDocNo=Sm.GetLue(LueWorkCenterDocNo);

                if (DocDt.Length > 0 && WorkCenterDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select WagesFormulationCode ");
                    SQL.AppendLine("From TblWorkCenterWagesFormulation ");
                    SQL.AppendLine("Where Date<=@Date ");
                    SQL.AppendLine("And WorkCenterCode=@WorkCenterDocNo ");
                    SQL.AppendLine("Order By Date Desc Limit 1;");

                    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@Date", DocDt.Substring(0, 8));
                    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", WorkCenterDocNo);

                    string WagesFormulationCode = Sm.GetValue(cm);
                    if (WagesFormulationCode.Length > 0)
                        Sm.SetLue(LueWagesFormulationCode, WagesFormulationCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_Validated(object sender, EventArgs e)
        {

        }

        private void LueWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
           
        }

        private void LueProductionShiftCode_Validated(object sender, EventArgs e)
        {
            
        }

        private void LueWagesFormulationCode_Validated(object sender, EventArgs e)
        {
           
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            SetHolidayWagesIndex();
            SetWagesFormulationCode();
            //ComputeNonCoordinatorHolidayWages();
            //ComputeCoordinatorHolidayWages();

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstanding, TxtProcessed, TxtBalance, 
                TxtOutstanding2, TxtProcessed2, TxtBalance2,
                TxtIndex, TxtValue, TxtAmt
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmpCount }, 1);

            ChkCalcPerPerson.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
            ClearGrd8();
        }

        private void LueWorkCenterDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWorkCenterDocNo, new Sm.RefreshLue2(Sl.SetLueWorkCenterDocNo), string.Empty);
            SetWagesFormulationCode();

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstanding, TxtProcessed, TxtBalance, 
                TxtOutstanding2, TxtProcessed2, TxtBalance2,
                TxtIndex, TxtValue, TxtAmt
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmpCount }, 1);

            ChkCalcPerPerson.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
            ClearGrd8();
        }

        private void LueProductionShiftCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionShiftCode, new Sm.RefreshLue1(Sl.SetLueProductionShiftCode));

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstanding, TxtProcessed, TxtBalance, 
                TxtOutstanding2, TxtProcessed2, TxtBalance2,
                TxtIndex, TxtValue, TxtAmt
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmpCount }, 1);

            ChkCalcPerPerson.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
            ClearGrd8();
        }

        private void LueWagesFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWagesFormulationCode, new Sm.RefreshLue1(Sl.SetLueWagesFormulationCode));
            ShowWagesFormulationUom();

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtOutstanding, TxtProcessed, TxtBalance, 
                TxtOutstanding2, TxtProcessed2, TxtBalance2,
                TxtIndex, TxtValue, TxtAmt
            }, 0);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmpCount }, 1);

            ChkCalcPerPerson.Checked = false;
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
            ClearGrd8();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkCalcPerPerson_CheckedChanged(object sender, EventArgs e)
        {
            ShowDocInfo();
        }

        #endregion

        #region Grid Event

        #region Grd1 Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (
                BtnSave.Enabled && 
                TxtDocNo.Text.Length == 0 && 
                e.ColIndex == 1 && 
                !Sm.IsDteEmpty(DteDocDt, "Date") &&
                !Sm.IsLueEmpty(LueProductionShiftCode, "Shift") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") 
                )
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) 
                    Sm.FormShowDialog(new FrmPWGDlg(
                        this, 
                        Sm.GetDte(DteDocDt), 
                        Sm.GetLue(LueWorkCenterDocNo),
                        Sm.GetLue(LueProductionShiftCode)
                        ));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete) ShowDocInfo();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && 
                TxtDocNo.Text.Length == 0 && 
                e.ColIndex == 1 && 
                !Sm.IsDteEmpty(DteDocDt, "Date") &&
                !Sm.IsLueEmpty(LueProductionShiftCode, "Shift") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") 
                )
                Sm.FormShowDialog(
                        new FrmPWGDlg(this,
                        Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueWorkCenterDocNo),
                        Sm.GetLue(LueProductionShiftCode)
                        ));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grd5 Event

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (!(e.ColIndex == 11 && TxtHolName.Text.Length > 0))
                    e.DoDefault = false;
            }
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 11 && TxtHolName.Text.Length > 0)
            {
                Grd5.Cells[e.RowIndex, 9].Value = 
                    Sm.GetGrdBool(Grd5, e.RowIndex, 11) ?
                        mHolidayWagesIndex : mNotHolidayWagesIndex;

                Grd5.Cells[e.RowIndex, 10].Value = Sm.GetGrdDec(Grd5, e.RowIndex, 8) * Sm.GetGrdDec(Grd5, e.RowIndex, 9);
            }
        }

        #endregion

        #region Grd6 Event

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (!(e.ColIndex == 10 && TxtHolName.Text.Length > 0))
                    e.DoDefault = false;
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 10 && TxtHolName.Text.Length > 0)
            {
                Grd6.Cells[e.RowIndex, 8].Value =
                    Sm.GetGrdBool(Grd6, e.RowIndex, 10) ?
                        mHolidayWagesIndex : mNotHolidayWagesIndex;

                Grd6.Cells[e.RowIndex, 9].Value = Sm.GetGrdDec(Grd6, e.RowIndex, 7) * Sm.GetGrdDec(Grd6, e.RowIndex, 8);
            }
        }

        #endregion

        #region Grd7 Event

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(
                BtnSave.Enabled && 
                TxtDocNo.Text.Length == 0 && 
                ChkCalcPerPerson.Checked &&
                Sm.IsGrdColSelected(new int[] { 8, 10 }, e.ColIndex) &&
                Sm.GetGrdStr(Grd7, e.RowIndex, 5).Length!=0
                ))
                e.DoDefault = false;
        }

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd7, new int[] { 8, 10 }, e);

                if (e.ColIndex == 8)
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd7, e.RowIndex, 5, 8, 10, 9, 11);

                if (e.ColIndex == 10)
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd7, e.RowIndex, 5, 10, 8, 11, 9);

                if (e.ColIndex == 8 || e.ColIndex == 10)
                {
                    ComputeValue(e.RowIndex);
                    UpdateWagesCalculatedPerPersonInfo();
                    ComputeWagesBasedOnFormulaForEachPerson(Sm.GetGrdStr(Grd7, e.RowIndex, 1));
                    ComputeNonCoordinatorWagesForEachPerson(Sm.GetGrdStr(Grd7, e.RowIndex, 1));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion
    }
}
