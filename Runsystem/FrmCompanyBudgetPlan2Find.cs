﻿#region Update
/*
    26/11/2020 [DITA/PHT] new apps
    27/01/2021 [WED/PHT] salah query alias
    18/06/2021 [TKG/PHT] cost center divalidasi berdasarkan group thd profit center, cost center yg ditampilkan adalah cost center yg tidak menjadi parent cost center yg lain
    04/10/2021 [WED/PHT] data yang muncul hanya CBP asli (bukan revisi)
    21/10/2021 [IBL/PHT] CBP Revisi dumunculkan juga
    20/03/2023 [RDA/PHT] fix dropdown Yr belum di set saat frmload
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmCompanyBudgetPlan2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan2Find(FrmCompanyBudgetPlan2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                mFrmParent.SetLueCCCode(ref LueCCCode, string.Empty);
                Sl.SetLueYr(LueYr, string.Empty);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.CancelInd, B.CCName, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.CCCode=B.CCCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.DocType=@DocType ");
            //SQL.AppendLine("And A.RevNo = 0 ");
            SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
            SQL.AppendLine("    Select CCCode ");
            SQL.AppendLine("    From TblCostCenter ");
            SQL.AppendLine("    Where ProfitCenterCode Is Not Null ");
            SQL.AppendLine("    And ProfitCenterCode In (");
            SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    )))) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Year",
                    "Cancel",
                    "Cost Center",
                    
                    //6-10
                    "Remark",
                    "Created By", 
                    "Created Date",
                    "Created Time",
                    "Last Updated By",

                    //11-12
                    "Last Updated Date",
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 100, 100, 80, 200, 
                    
                    //6-10
                    200, 130, 130, 130, 130,

                    //11-12
                    130, 130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if(mFrmParent.mIsCompanyBudgetPlanProfitLoss) Sm.CmParam<String>(ref cm, "@DocType", "2");
                else Sm.CmParam<String>(ref cm, "@DocType", "3");
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "A.CCCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "Yr", "CancelInd", "CCName", "Remark", 
                        
                        //6-9
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            string mYr = Sm.GetLue(LueYr);
            if (mYr.Length == 0) Sl.SetLueYr(LueYr, string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mFrmParent.mIsFilterByCC ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }


        #endregion

        #endregion
    }
}
