﻿#region Update
/* 
    09/04/2017 [TKG] Tambah informasi replaced items
    12/06/2017 [TKG] bug fixing saat insert tidak mengupdate stock movement
    11/07/2017 [WED] EntCode save ke journalDtl
    11/07/2017 [WED] Remark Journal Hdr ambil dari remark dokumen transaksi
    17/07/2018 [TKG] tambah cost center saat journal
    09/08/2018 [TKG] tambah filter status do
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    27/10/2019 [TKG/IOK] merubah proses menggunakan replaced item (DOCt3DocNo tidak digunakan lagi)
    01/11/2019 [WED/IMS] tambah DR+SO Contract
    27/11/2019 [WED/IMS] tambah parameter IsBOMShowSpecifications untuk nampilkan project code, project name, customer po# di dialog nya
    30/01/2020 [TKG/IMS] munculkan kolom ProjectCode, ProjectName, Customer PO#
    12/05/2020 [IBL/IMS] Penambahan Kolom untuk memasukan No. SJR (SJR#) pada header berdasarkan parameter IsSJRNoMandatory
    13/05/2020 [IBL/IMS] Attachment dokumen pendukung untuk proses input system bisa dilakukan
    27/05/2020 [VIN/IMS] penyesuaian printout
    10/06/2020 [VIN/IMS] Tambah Kolom item local code pada kedua detail
    16/06/2020 [VIN/IMS] tambah kolom customer, dan notes bisa diedit  
    08/07/2020 [HAR/IMS] remark so contract ambil dari detail item
    13/07/2020 [IBL/KSM] menyambungkan dengan DO to customer based on DRSC
    24/09/2020 [TKG/IMS] Ubah query journal saat cancel
    02/10/2020 [ICA/IMS] printOut Receiving from Customer
    16/10/2020 [IBL/IMS] Feedback printOut: No NCR dan No SJN belum muncul
    11/11/2020 [VIN/IMS] Penambahan kolom No di detail 
    13/11/2020 [TKG/IMS] bisa edit remark
    06/12/2020 [WED/IMS] bisa tarik No berdasarkan parameter IsDOCtUseSOContract
    07/12/2020 [WED/IMS] logic print uot masih salah (ambil No nya masih salah)
 *  19/07/2021 [ICA/IMS] hanya menampilkan DOCtDR berdasarkan parameter IsRecvCtBasedOnDOCtDROnly
 *  16/08/2021 [SET/ALL] Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong (baik dari Menu Master Data maupun System's Parameter) sehingga terhindar dari jurnal yang tidak seimbang.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmRecvCt : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmRecvCtFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        //private bool mIsRecvCtEditable = false;
        internal bool mIsRecvCtShowPriceInfo = false, 
            mIsBOMShowSpecifications = false, 
            mIsSJRNoMandatory = false, 
            mIsRecvCtAllowToUploadFile = false,
            mIsSalesContractEnabled = false,
            mIsSalesTransactionShowSOContractRemark,
            mIsSalesTransactionUseItemNotes,
            mIsDetailShowColumnNumber,
            mIsDOCtUseSOContract = false,
            mIsRecvCtBasedOnDOCtDROnly = false
            ;
        private bool 
            mIsAutoJournalActived = false,
            mIsRecvCtRemarkEditable = false,
            mIsCheckCOAJournalNotExists = false,
            mIsItemCategoryUseCOAAPAR = false;
        private string mAcNoForCOGS = string.Empty, mEntCode = string.Empty;
        internal string
            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmRecvCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Customer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
                if (mIsSJRNoMandatory)
                    LblSJRNo.ForeColor = Color.Red;
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "DO To Customer#",
                        "DNo",

                        //6-10
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //21-25
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Unit Price",
                        "Total",

                        //26-30
                        "Remark",
                        "DO Type",
                        "Price",
                        "Project's Code",
                        "Project's Name",

                        //31-35
                        "Customer's PO#", 
                        "Item's"+Environment.NewLine+" Local Code",
                        "Remark SO",
                        "Notes",
                        "Site Code",

                        //36
                        "No."


                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        50, 50, 20, 150, 50, 
                        
                        //6-10
                        20, 200, 20, 150, 130, 
                        
                        //11-15
                        180, 100, 80, 100, 80,
                        
                        //16-20
                        80, 100, 80, 80, 100, 
                        
                        //21-25
                        80, 80, 70, 150, 150,
                        
                        //26-30
                        300, 50, 0, 120, 200,

                        //31-35
                        120, 120, 150, 150, 0,

                        //36
                        50
                        
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 28 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 6, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 7, 8, 11, 12, 13, 17, 18, 19, 20, 21, 22, 27, 28, 35 }, false);
            Grd1.Cols[32].Move(10);
            if (!mIsRecvCtShowPriceInfo)
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 25 }, false);
            if(!mIsSalesTransactionShowSOContractRemark)
                Sm.GrdColInvisible(Grd1, new int[] { 33 }, false);
            if (!mIsSalesTransactionUseItemNotes)
                Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            if (!mIsDetailShowColumnNumber)
                Sm.GrdColInvisible(Grd1, new int[] { 36 }, false);
            Grd1.Cols[36].Move(8);
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 14;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Received",
                        "Replaced",
                        "UoM",

                        //6-10
                        "Received",
                        "Replaced",
                        "UoM",
                        "Received",
                        "Replaced",

                        //11-12
                        "UoM",
                        "DO#",
                        "Item's"+Environment.NewLine+" Local Code"

                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        120, 200, 100, 100, 80, 
                        
                        //6-10
                        100, 100, 80, 100, 100, 
                        
                        //11-13
                        80, 130, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4, 6, 7, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 6, 7, 8, 9, 10, 11 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Grd2.Cols[13].Move(3);
            
            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7, 8 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7, 8, 9, 10, 11 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueCtCode, MeeRemark,
                        ChkReplaceInd, TxtCtDONo, TxtCtReturnNo, TxtSJRNo, TxtFile,
                        TxtFile2, TxtFile3
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 });
                    Sm.GrdColInvisible(Grd1, new int[] { 14, 17, 20 }, false);
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvCtAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvCtAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvCtAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, MeeRemark, ChkReplaceInd,
                        TxtCtDONo, TxtCtReturnNo, TxtSJRNo
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 24, 26, 34, 36});
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 33, 35 });

                    Grd1.Cols[14].Visible = true;
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 17 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 17, 20 }, true);
                    Grd1.Cols[14].Text = "Remaining" + Environment.NewLine + "DO Qty 1";
                    Grd1.Cols[17].Text = "Remaining" + Environment.NewLine + "DO Qty 2";
                    Grd1.Cols[20].Text = "Remaining" + Environment.NewLine + "DO Qty 3";
                    if (mIsRecvCtAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Grd1.Cols[14].Visible = true;
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 17 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 17, 20 }, true);
                    Grd1.Cols[14].Text = "Stock" + Environment.NewLine + "1";
                    Grd1.Cols[17].Text = "Stock" + Environment.NewLine + "2";
                    Grd1.Cols[20].Text = "Stock" + Environment.NewLine + "3";
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueCtCode, MeeRemark,
                TxtCtDONo, TxtCtReturnNo, TxtSJRNo, TxtFile, TxtFile2,
                TxtFile3
            });
            mEntCode = string.Empty;
            ChkReplaceInd.Checked = false;
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 24, 25 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 4, 6, 7, 9, 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvCtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
                SetFormControl(mState.Edit);
                ComputeStock();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue(
                            "Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode=@Param limit 1;", Sm.GetLue(LueWhsCode));
                
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "RecvCtHdr", "RecvCtDtl", "Sign1" };

            var l = new List<RecvCtHdr>();
            var ldtl = new List<RecvCtDtl>();
            var ldtl2 = new List<Sign1>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y') As DocDt,B.WhsName,C.CtName,A.Remark ");
            SQL.AppendLine(", F.PONo, F.ProjectName, F.ProjectCode, ");
            SQL.AppendLine(" G.EmpName, H.DeptName, I.PosName, GROUP_CONCAT(Distinct F.SOCRemark separator '\n') as SOCRemark");
            SQL.AppendLine("From TblRecvCtHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("left JOIN tblrecvctdtl D ON A.Docno = D.DocNo ");
            SQL.AppendLine("Left Join TblDOCt2Hdr E On D.DOCtDocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblDOCt2Dtl E1 On E1.DocNo = E.DocNo ");
            SQL.AppendLine("LEFT JOIN  ");

            SQL.AppendLine("( ");
			SQL.AppendLine("	Select X1.DocNo, Group_Concat(Distinct X5.ProjectCode) ProjectCode,  ");
            SQL.AppendLine("    Group_Concat(Distinct X5.ProjectName) ProjectName,  ");
            SQL.AppendLine("    Group_Concat(DISTINCT X2.PONo) PONo, GROUP_CONCAT(DISTINCT X.Remark) SOCRemark, X.ItCode  ");
            SQL.AppendLine("    From TblDRDtl X1  ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo  ");
            SQL.AppendLine("    Inner Join tblsocontractdtl X ON X2.Docno = X.DocNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo  ");
            SQL.AppendLine("    Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo  ");
            SQL.AppendLine("    Left Join TblProjectGroup X5 On X4.PGCode = X5.PGCode  ");
            SQL.AppendLine("    Where X1.DocNo In (  ");
            SQL.AppendLine("        Select T3.DRDocNo  ");
            SQL.AppendLine("        From TblRecvCtHdr T1  ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T2 On T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo Is Not Null  ");
            SQL.AppendLine("        Where T1.DocNo=@DocNo  ");
            SQL.AppendLine("    )  ");
            SQL.AppendLine("    Group By X1.DocNo, X.ItCode  ");
            SQL.AppendLine("	) F On E.DRDocNo = F.DocNo AND F.ItCode=E1.ItCode ");

            SQL.AppendLine("Inner Join TBLEmployee G ");
            SQL.AppendLine("INNER JOIN tbldepartment H ON G.DeptCode= H.DeptCode ");
            SQL.AppendLine("INNER JOIN tblposition I ON G.PosCode=I.PosCode ");
            SQL.AppendLine("INNER JOIN  tblparameter J ON G.EmpCode=J.ParValue WHERE J.ParCode='RecvCtPrintOutSignName' ");

            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Group By A.DocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         //6-10
                         "CtName",
                         "Remark",
                         "CompanyLogo",
                         "PONo",
                         "ProjectCode",

                         //11-15
                         "ProjectName", 
                         "EmpName",
                         "DeptName",
                         "PosName", 
                         "SOCRemark"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvCtHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),

                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),

                            CtName = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            CompanyLogo = Sm.DrStr(dr, c[8]),
                            SJRNo = TxtSJRNo.Text,
                            PONo = Sm.DrStr(dr, c[9]),

                            ProjectCode = Sm.DrStr(dr, c[10]),
                            ProjectName = Sm.DrStr(dr, c[11]),
                            EmpName = Sm.DrStr(dr, c[12]),
                            DeptName = Sm.DrStr(dr, c[13]),
                            PosName = Sm.DrStr(dr, c[14]),
                            SOCRemark = Sm.DrStr(dr, c[15]),

                            NCRNo = TxtCtReturnNo.Text,
                            SJNNo = TxtCtDONo.Text,

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);
#endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select T.DocNo, T.DOCtDocNo, T.DocDt, T.ItCode, T.ItName, T.BatchNo, T.Lot, T.Bin, T.Qty, T.Qty2, T.Qty3, T.InventoryUOMCode, T.InventoryUOMCode2, T.InventoryUOMCode3, T.Remark, T.ItCodeInternal, ");
                SQLDtl.AppendLine("T.SOCRemark, T.SOCNo From (");
                SQLDtl.AppendLine("Select A.DocNo, A.DOCtDocNo,Date_Format(D.DocDt,'%d %M %Y') As DocDt,B.ItCode,C.ItName,B.BatchNo,B.Lot,B.Bin,A.Qty,A.Qty2,A.Qty3,C.InventoryUOMCode,C.InventoryUOMCode2,C.InventoryUOMCode3,A.Remark, C.ItCodeInternal, ");
                SQLDtl.AppendLine("E.Remark AS SOCRemark, E.No AS SOCNo ");
                SQLDtl.AppendLine("From TblRecvCtDtl A ");
                SQLDtl.AppendLine("Inner Join TblDOCt2Dtl B On Concat(A.DOCtDocNo,A.DOCtDNo)=Concat(B.DocNo,B.DNo) ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode= C.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDOCt2Hdr D On A.DOCtDocNo = D.DocNo ");
                SQLDtl.AppendLine("Left Join TblSOContractDtl E On B.SOContractDocNo = E.DocNo And B.SOContractDNo = E.DNo ");
                #region old code by wed
                //SQLDtl.AppendLine("LEFT JOIN ( ");
                //SQLDtl.AppendLine("     SELECT T1.DocNo, GROUP_CONCAT(DISTINCT T3.`No`) No, GROUP_CONCAT(DISTINCT T3.Remark) Remark, T3.ItCode  ");
                //SQLDtl.AppendLine("     FROM tblDOCt2Hdr T1  ");
                //SQLDtl.AppendLine("     INNER JOIN tbldrdtl T2 ON T2.DocNo = T1.DRDocno");
                //SQLDtl.AppendLine("     INNER JOIN tblsocontractdtl T3 ON T2.SODocNo = T3.DocNo ");
                //SQLDtl.AppendLine("         AND T2.SODNo = T3.DNo ");
                //SQLDtl.AppendLine("     GROUP BY T1.DocNo, T3.ItCode ");
                //SQLDtl.AppendLine(")E ON E.DocNo=D.DocNo AND E.ItCode = C.ItCode ");
                #endregion
                SQLDtl.AppendLine("Where A.DOType ='2' ");
                SQLDtl.AppendLine("Union All ");
                SQLDtl.AppendLine("Select A.DocNo, A.DOCtDocNo,Date_Format(D.DocDt,'%d %M %Y') As DocDt,B.ItCode,C.ItName,B.BatchNo,B.Lot,B.Bin,A.Qty,A.Qty2,A.Qty3,C.InventoryUOMCode,C.InventoryUOMCode2,C.InventoryUOMCode3,A.Remark, C.ItCodeInternal, ");
                SQLDtl.AppendLine("NULL AS SOCRemark, NULL AS SOCNo ");
                SQLDtl.AppendLine("From TblRecvCtDtl A ");
                SQLDtl.AppendLine("Inner Join TblDOCtDtl B On Concat(A.DOCtDocNo,A.DOCtDNo)=Concat(B.DocNo,B.DNo) ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode= C.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDOCtHdr D On A.DOCtDocNo= D.DocNo ");
                SQLDtl.AppendLine("Where A.DOType ='1' ");
                SQLDtl.AppendLine(")T ");
                SQLDtl.AppendLine("Where T.DocNo =@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DOCtDocNo",
                         //1-5
                         "ItCode",
                         "ItName",
                         "BatchNo",
                         "Lot",
                         "Bin",
                         //6-10
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",
                         "InventoryUOMCode2",
                         //11-14
                         "InventoryUOMCode3",
                         "DocDt",
                         "Remark",
                         "ItCodeInternal",
                         "SOCNo",
                         //16
                         "SOCRemark",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RecvCtDtl()
                        {
                            DOCtDocNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            DocDt = Sm.DrStr(drDtl, cDtl[12]),
                            Remark = Sm.DrStr(drDtl, cDtl[13]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[14]),
                            SOCNo = Sm.DrStr(drDtl, cDtl[15]),
                            SOCRemark = Sm.DrStr(drDtl, cDtl[16]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("SELECT B.UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.Posname ");
                SQLDtl2.AppendLine("from tblrecvcthdr A  ");
                SQLDtl2.AppendLine("inner join TblUser B  ON A.CreateBy=B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature'  ");
                SQLDtl2.AppendLine("Left Join tblemployee D ON B.UserCode =D.UserCode  ");
                SQLDtl2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
                SQLDtl2.AppendLine("INNER JOIN tbllog F ON F.UserCode=B.UserCode  ");

                SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "UserCode" ,

                         //1-2
                         "UserName",
                         "Posname"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new Sign1()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            int a = int.Parse(ParValue);
            if (Sm.GetParameter("DocTitle") == "IMS")
                Sm.PrintReport("RecvCtIMS", myLists, TableName, false);
            else
            {
                if (a == 1)
                {
                    Sm.PrintReport("RecvCt1", myLists, TableName, false);
                }
                else if (a == 2)
                {
                    Sm.PrintReport("RecvCt2", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport("RecvCt3", myLists, TableName, false);
                }
            }

        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 26 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 27) == "1")
                {
                    e.DoDefault = false;
                    var f = new FrmDOCt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 27) == "2")
                {
                    e.DoDefault = false;
                    if (mIsSalesContractEnabled && Sm.GetGrdStr(Grd1, e.RowIndex, 35).Length > 0)
                    {
                        var f = new FrmDOCt7(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmDOCt9(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.ShowDialog();
                    }
                }
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueCtCode, "Customer") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 27) == "1")
                {
                    var f = new FrmDOCt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 27) == "2")
                {
                    if (mIsSalesContractEnabled && Sm.GetGrdStr(Grd1, e.RowIndex, 35).Length > 0)
                    {
                        var f = new FrmDOCt7(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.ShowDialog();
                    }
                    else
                    {
                        var f = new FrmDOCt9(mMenuCode);
                        f.Tag = mMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                        f.ShowDialog();
                    }
                }
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 26 }, e);

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 15, 18, 16, 19);
                ComputeTotal(e.RowIndex);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 18, 15, 19, 16);
                ComputeTotal(e.RowIndex);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);


            if (e.ColIndex == 15) ComputeTotal(e.RowIndex);

            if (e.ColIndex == 24)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 27) == "2" )
                {
                    decimal val = Sm.GetGrdDec(Grd1, e.RowIndex, 28);
                    Sm.StdMsg(mMsgType.Warning, "You can't change unit price");
                    Grd1.Cells[e.RowIndex, 24].Value = val;
                }
                else
                {
                    ComputeTotal(e.RowIndex);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvCt", "TblRecvCtHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvCtHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvCtDtl(DocNo, Row));

            if (ChkReplaceInd.Checked) cml.Add(SaveRecvCtDtl2(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    cml.Add(SaveStockMovement(DocNo, r));
                    cml.Add(SaveStockSummary(r));
                }
            }
            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            bool result;
            ReComputeRemainingQty();

            return result =
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                (mIsSJRNoMandatory && Sm.IsTxtEmpty(TxtSJRNo, "SJR#", false)) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt))||
                (mIsSalesContractEnabled && IsGrdHasDifferentSite()) ||
                IsJournalSettingInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "DO to customer's document# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 15, true, "Quantity (1) is empty.") ||
                    (Sm.GetGrdStr(Grd1, Row, 19).Length > 0 && mNumberOfInventoryUomCode >= 2 && Sm.IsGrdValueEmpty(Grd1, Row, 18, true, "Quantity (2) is empty.")) ||
                    (Sm.GetGrdStr(Grd1, Row, 22).Length > 0 && mNumberOfInventoryUomCode == 3 && Sm.IsGrdValueEmpty(Grd1, Row, 21, true, "Quantity (3) is empty."))
                    ) return true;
                if (IsExceedMaxChar(Sm.GetGrdStr(Grd1, Row, 36)))
                {
                    Sm.StdMsg(mMsgType.Warning, "No in Received Item exceed maximum length character (4).");
                    return true;
                }
                if (((Sm.GetGrdStr(Grd1, Row, 15).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 15).Length != 0 && Sm.GetGrdDec(Grd1, Row, 15) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 18).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 18).Length != 0 && Sm.GetGrdDec(Grd1, Row, 18) == 0m)) &&
                    ((Sm.GetGrdStr(Grd1, Row, 21).Length == 0) || (Sm.GetGrdStr(Grd1, Row, 21).Length != 0 && Sm.GetGrdDec(Grd1, Row, 21) == 0m)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.");
                    return true;
                }

                if (IsRecvQtyBiggerThanRemainingQty(Row)) return true;
            }
            return false;
        }

        private bool IsGrdHasDifferentSite()
        {
            string SiteCode = Sm.GetValue("SELECT D.SiteCode From TblDOCt2Hdr A " +
                                          "Inner Join TblDRDtl B On A.DRDocNo = B.DocNo " +
                                          "Inner Join TblSalesContract C On B.SCDocNo = C.DocNo " +
                                          "Inner Join TblSalesMemoHdr D On C.SalesMemoDocNo = D.DocNo " +
                                          "Where A.DocNo = @Param Limit 1; ", Sm.GetGrdStr(Grd1, 0, 4));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 35) != SiteCode)
                {
                    Sm.StdMsg(mMsgType.Warning,
                    "DO To Customer# :" + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code :" + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name :" + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                    "This document has different site.");
                    return true;
                }
            }
            return false;
        }

        private bool IsRecvQtyBiggerThanRemainingQty(int Row)
        {
            decimal
                Remaining1 = Sm.GetGrdDec(Grd1, Row, 14),
                Recv1 = Sm.GetGrdDec(Grd1, Row, 15),
                Remaining2 = Sm.GetGrdDec(Grd1, Row, 17),
                Recv2 = Sm.GetGrdDec(Grd1, Row, 18),
                Remaining3 = Sm.GetGrdDec(Grd1, Row, 20),
                Recv3 = Sm.GetGrdDec(Grd1, Row, 21);

            if (Recv1 > Remaining1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity (" + Sm.FormatNum(Recv1, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining1, 0) + ")."
                    );
                return true;
            }

            if (Recv2 > Remaining2)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity (" + Sm.FormatNum(Recv2, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining2, 0) + ")."
                    );
                return true;
            }

            if (Recv3 > Remaining3)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "Received quantity (" + Sm.FormatNum(Recv3, 0) + ") is greater than remaining quantity (" + Sm.FormatNum(Remaining3, 0) + ")."
                    );
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            var SQL = new StringBuilder();
            string mAcNoForCOGS = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForCOGS'");

            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists)
            {
                if (mAcNoForCOGS.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }
            }
            else
            {
                if (IsJournalSettingInvalid_Item(Msg, "AcNo")) return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Item(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 7);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    r=+2;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (Stock) (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRecvCtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvCtHdr(DocNo, DocDt, WhsCode, CtCode, Remark, ");
            SQL.AppendLine("SalesReturnInvoiceInd, DOCt3Ind, ReplaceInd, CtDONo, CtReturnNo, SJRNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @CtCode, @Remark, ");
            SQL.AppendLine("'O', 'O', @ReplaceInd, @CtDONo, @CtReturnNo, @SJRNo, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ReplaceInd", ChkReplaceInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CtDONo", TxtCtDONo.Text);
            Sm.CmParam<String>(ref cm, "@CtReturnNo", TxtCtReturnNo.Text);
            Sm.CmParam<String>(ref cm, "@SJRNo", TxtSJRNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRecvCtDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvCtDtl(DocNo, DNo, No, CancelInd, DOCtDocNo, DOCtDNo, Qty, Qty2, Qty3, UPrice, DOType, Remark, Notes, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @No, 'N', @DOCtDocNo, @DOCtDNo, @Qty, @Qty2, @Qty3, @UPrice, @DOType, @Remark, @Notes, @CreateBy, CurrentDateTime()) " 

                    };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@DOType", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 34));

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRecvCtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblRecvCtDtl2 ");
            SQL.AppendLine("(DocNo, DNo, ItCode, ");
            SQL.AppendLine("RecvCtQty, RecvCtQty2, RecvCtQty3, DOCt3Qty, DOCt3Qty2, DOCt3Qty3, ");
            SQL.AppendLine("DOCt3DocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3, ");
            SQL.AppendLine("0, 0, 0, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T2.ItCode, T1.Qty, T1.Qty2, T1.Qty3 ");
            SQL.AppendLine("    From TblRecvCtDtl T1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DOCtDocNo=T2.DocNo And T1.DOCtDNo=T2.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    And T1.DOType='1' ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.ItCode, T1.Qty, T1.Qty2, T1.Qty3 ");
            SQL.AppendLine("    From TblRecvCtDtl T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DOCtDocNo=T2.DocNo And T1.DOCtDNo=T2.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    And T1.DOType='2' ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By ItCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocType, @DocNo, @DNo, 'N', @DocDt, @WhsCode, @Lot, @Bin, @ItCode, @BatchNo, @Source, @Qty, @Qty2, @Qty3, @Remark, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", "08");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, r, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, r, 21));
            var RemarkH = MeeRemark.Text;
            var RemarkD = Sm.GetGrdStr(Grd1, r, 26);
            if (RemarkH.Length > 0)
            {
                if (RemarkD.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Remark", string.Concat(RemarkH, " ( ", RemarkD, " )"));
                else
                    Sm.CmParam<String>(ref cm, "@Remark", RemarkH);
            }
            else
            {
                if (RemarkD.Length > 0)
                    Sm.CmParam<String>(ref cm, "@Remark", RemarkD);
                else
                    Sm.CmParam<String>(ref cm, "@Remark", string.Empty);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, Source, ItCode, BatchNo, PropCode, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, @Lot, @Bin, @Source, @ItCode, @BatchNo, '-', 0, 0, 0, UserCode, CreateDt ");
            SQL.AppendLine("From (Select @UserCode UserCode, CurrentDateTime() As CreateDt) T ");
            SQL.AppendLine("Where Not Exists(");
            SQL.AppendLine("    Select 1 From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode=@WhsCode ");
            SQL.AppendLine("    And Lot=@Lot ");
            SQL.AppendLine("    And Bin=@Bin ");
            SQL.AppendLine("    And Source=@Source ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblRecvCtHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblRecvCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine("        Select F.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(G.ExcRate, 0)*IfNull(G.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='1' ");
            SQL.AppendLine("        Inner join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblStockPrice G On D.Source=G.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.PLDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='1' ");
            SQL.AppendLine("        Inner join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.PLDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        INNER JOIN ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT T1.DocNo, T2.DNo, T6.ItCode, T6.UPrice ");
            SQL.AppendLine("            FROM TblRecvCtHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblRecvCtDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Hdr T3 ON T2.DOCtDocNo = T3.DocNo AND T3.DRDocNo IS NOT null ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Dtl2 T4 ON T3.DOcNo = T4.DocNo ");
            SQL.AppendLine("            INNER JOIN TblDRDtl T5 ON T3.DRDocNo = T5.DocNo AND T4.DRDNo = T5.DNo AND T5.SCDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblSalesMemoDtl T6 ON T5.SODocNo = T6.DocNo AND T5.SODNo = T6.DNo ");
            SQL.AppendLine("        ) E ON A.DocNO = E.DocNo AND B.DNo = E.DNo AND D.ItCode = E.ItCode ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        INNER JOIN ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT T1.DocNo, T2.DNo, T6.ItCode, T6.UPrice ");
            SQL.AppendLine("            FROM TblRecvCtHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblRecvCtDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Hdr T3 ON T2.DOCtDocNo = T3.DocNo AND T3.DRDocNo IS NOT null ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Dtl2 T4 ON T3.DOcNo = T4.DocNo ");
            SQL.AppendLine("            INNER JOIN TblDRDtl T5 ON T3.DRDocNo = T5.DocNo AND T4.DRDNo = T5.DNo AND T5.SCDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblSalesMemoDtl T6 ON T5.SODocNo = T6.DocNo AND T5.SODNo = T6.DNo ");
            SQL.AppendLine("        ) E ON A.DocNO = E.DocNo AND B.DNo = E.DNo AND D.ItCode = E.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsRecvCtRemarkEditable)
                cml.Add(EditRecvCtHdr());

            for (int r= 0;r< Grd1.Rows.Count;r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2))
                    cml.Add(EditRecvCtDtl(r));
            
            if (ChkReplaceInd.Checked) cml.Add(EditRecvCtForReplacedItem());
            if (mIsAutoJournalActived && IsJournalDataExisted()) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            UpdateCancelledItem();
            ComputeStock();
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (!mIsRecvCtRemarkEditable && IsCancelledItemNotExisted()) || 
                IsCancelledQtyNotValid() || 
                IsDataAlreadyProcessedToSalesReturnInvoice() ||
                IsDataAlreadyProcessedToDOCt3();
        }

        private void ComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select DNo From TblRecvCtDtl Where DocNo=@DocNo And CancelInd='Y' Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo" });
                if (dr.HasRows)
                {
                    var DNo = string.Empty;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DNo = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), DNo))
                            {
                                Grd1.Cells[Row, 1].Value = true;
                                Grd1.Cells[Row, 2].Value = true;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledItemNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                    return false;
            Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
            return true;
        }

        private bool IsCancelledQtyNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                            "Cancelled quantity [1] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 15), 0) + Environment.NewLine +
                            "Stock [1] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0) + Environment.NewLine + Environment.NewLine +
                            "Cancelled quantity is greater than stock."
                        );
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                            "Cancelled quantity [2] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + Environment.NewLine +
                            "Stock [2] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 17), 0) + Environment.NewLine + Environment.NewLine +
                            "Cancelled quantity is greater than stock."
                        );
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                            "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                            "Cancelled quantity [3] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 21), 0) + Environment.NewLine +
                            "Stock [3] : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 20), 0) + Environment.NewLine + Environment.NewLine + 
                            "Cancelled quantity is greater than stock."
                        );
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToSalesReturnInvoice()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0 && 
                    Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (IsDataAlreadyProcessedToSalesReturnInvoice(Row))
                        return true;
                }
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToSalesReturnInvoice(int Row)
        { 
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSalesReturnInvoiceHdr A, TblSalesReturnInvoiceDtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.RecvCtDocNo=@DocNo ");
            SQL.AppendLine("And B.RecvCtDNo=@DNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine +
                    "This data already processed to sales return invoice."
                );
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToDOCt3()
        {
            if (!ChkReplaceInd.Checked) return false;

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0 && 
                    Sm.GetGrdBool(Grd1, r, 1) && 
                    !Sm.GetGrdBool(Grd1, r, 2) && 
                    IsDataAlreadyProcessedToDOCt3(r)) 
                    return true;
            return false;
        }

        private bool IsDataAlreadyProcessedToDOCt3(int r)
        {
            if (Sm.IsDataExist("Select 1 From TblRecvCtDtl2 Where DocNo=@Param1 And ItCode=@Param2 And RecvCtQty<>0.00 And DOCt3Qty<>0.00;",
                TxtDocNo.Text, Sm.GetGrdStr(Grd1, r, 7), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO# : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine + Environment.NewLine +
                    "This data already processed to DO to customer for replaced items."
                );
                return true;
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            return Sm.IsDataExist("Select 1 From TblRecvCtHdr Where JournalDocNo Is Not Null And DocNo=@Param Limit 1;", TxtDocNo.Text);
        }

        private MySqlCommand EditRecvCtHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtHdr Set ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditRecvCtDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N';");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Qty*-1, Qty2*-1, Qty3*-1, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N' And DocType=@DocType;");
            
            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, ");
            SQL.AppendLine("    Qty2=Qty2-@Qty2, ");
            SQL.AppendLine("    Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", "08");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditRecvCtForReplacedItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtDtl2 A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T.ItCode, Sum(T.Qty) Qty, Sum(T.Qty2) Qty2, Sum(T.Qty3) Qty3 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.ItCode, T1.Qty, T1.Qty2, T1.Qty3 ");
            SQL.AppendLine("        From TblRecvCtDtl T1 ");
            SQL.AppendLine("        Inner Join TblDOCtDtl T2 On T1.DOCtDocNo=T2.DocNo And T1.DOCtDNo=T2.DNo ");
            SQL.AppendLine("        Where T1.DocNo=@DocNo And T1.DOType='1' And T1.CancelInd='N' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.ItCode, T1.Qty, T1.Qty2, T1.Qty3 ");
            SQL.AppendLine("        From TblRecvCtDtl T1 ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl T2 On T1.DOCtDocNo=T2.DocNo And T1.DOCtDNo=T2.DNo ");
            SQL.AppendLine("        Where T1.DocNo=@DocNo And T1.DOType='2' And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.ItCode Having Sum(T.Qty)<>0.00 ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.RecvCtQty=IfNull(B.Qty, 0.00), ");
            SQL.AppendLine("    A.RecvCtQty2=IfNull(B.Qty2, 0.00), ");
            SQL.AppendLine("    A.RecvCtQty3=IfNull(B.Qty3, 0.00) ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='O' Where DocNo=@DocNo And ReplaceInd='Y'; ");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='F'");
            SQL.AppendLine("Where DocNo=@DocNo And ReplaceInd='Y' ");
            SQL.AppendLine("And Not Exists(Select 1 From TblRecvCtDtl2 Where DocNo=@DocNo And RecvCtQty<>DOCt3Qty Limit 1);");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='P'");
            SQL.AppendLine("Where DocNo=@DocNo And ReplaceInd='Y' ");
            SQL.AppendLine("And Exists(Select 1 From TblRecvCtDtl2 Where DocNo=@DocNo And RecvCtQty<>0.00 And RecvCtQty<>DOCt3Qty Limit 1);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var cm = new MySqlCommand(); 
            string Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            if (Grd1.Rows.Count >= 1)
            {
                string DNo = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    DNo = Sm.GetGrdStr(Grd1, Row, 0);
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && DNo.Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.DNo=@DNo" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DNo" + Row.ToString(), DNo);
                    }
                }
            }

            if (Filter.Length != 0) 
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And (1=0) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine(Filter.Replace("B.", string.Empty));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Receiving Item From Customer : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblRecvCtHdr Where DocNo=@DocNo); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select F.AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(G.ExcRate, 0)*IfNull(G.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='1' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblStockPrice G On D.Source=G.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.PLDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='1' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCtHdr C On B.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("        Inner join TblDOCtDtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        IfNull(E.ExcRate, 0)*IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.PLDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice E On D.Source=E.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select G.AcNo As AcNo, ");
            SQL.AppendLine("        0 As DAmt, IfNull(E.UPrice, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        INNER JOIN ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT T1.DocNo, T2.DNo, T6.ItCode, T6.UPrice ");
            SQL.AppendLine("            FROM TblRecvCtHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblRecvCtDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Hdr T3 ON T2.DOCtDocNo = T3.DocNo AND T3.DRDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Dtl2 T4 ON T3.DOcNo = T4.DocNo ");
            SQL.AppendLine("            INNER JOIN TblDRDtl T5 ON T3.DRDocNo = T5.DocNo AND T4.DRDNo = T5.DNo AND T5.SCDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblSalesMemoDtl T6 ON T5.SODocNo = T6.DocNo AND T5.SODNo = T6.DNo ");
            SQL.AppendLine("        ) E ON A.DocNO = E.DocNo AND B.DNo = E.DNo AND D.ItCode = E.ItCode ");
            SQL.AppendLine("        Inner join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        IfNull(E.UPrice, 0)*B.Qty As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvCtHdr A ");
            SQL.AppendLine("        Inner join TblRecvCtDtl B On A.DocNo=B.DocNo And B.DOType='2' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DrDocNo Is Not Null ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("        INNER JOIN ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT T1.DocNo, T2.DNo, T6.ItCode, T6.UPrice ");
            SQL.AppendLine("            FROM TblRecvCtHdr T1 ");
            SQL.AppendLine("            INNER JOIN TblRecvCtDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Hdr T3 ON T2.DOCtDocNo = T3.DocNo AND T3.DRDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblDOCt2Dtl2 T4 ON T3.DOcNo = T4.DocNo ");
            SQL.AppendLine("            INNER JOIN TblDRDtl T5 ON T3.DRDocNo = T5.DocNo AND T4.DRDNo = T5.DNo AND T5.SCDocNo IS NOT NULL ");
            SQL.AppendLine("            INNER JOIN TblSalesMemoDtl T6 ON T5.SODocNo = T6.DocNo AND T5.SODNo = T6.DNo ");
            SQL.AppendLine("        ) E ON A.DocNO = E.DocNo AND B.DNo = E.DNo AND D.ItCode = E.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRecvCtHdr(DocNo);
                ShowRecvCtDtl(DocNo);
                if (ChkReplaceInd.Checked) ShowRecvCtDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvCtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, CtCode, Remark, ReplaceInd, CtDONo, CtReturnNo, SJRNo, FileName, FileName2, FileName3 " +
                    "From TblRecvCtHdr " +
                    "Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "CtCode", "Remark", "ReplaceInd",
                        "CtDONo", "CtReturnNo", "SJRNo", "FileName", "FileName2",
                        "FileName3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        ChkReplaceInd.Checked = Sm.DrStr(dr, c[5])=="Y";
                        TxtCtDONo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtCtReturnNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSJRNo.EditValue = Sm.DrStr(dr, c[8]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[9]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[10]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowRecvCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.No, A.CancelInd, A.DOCtDocNo, A.DOCtDNo,  ");
            SQL.AppendLine("IfNull(C.ItCode, F.ItCode) As ItCode,  K.ItCodeInternal, ");
            SQL.AppendLine("IfNull(D.ItName, G.ItName) As ItName, ");
            SQL.AppendLine("IfNull(C.BatchNo, F.BatchNo) As BatchNo, ");
            SQL.AppendLine("IfNull(C.Source, F.Source) As Source, ");
            SQL.AppendLine("IfNull(C.Lot, F.Lot) As Lot, ");
            SQL.AppendLine("IfNull(C.Bin, F.Bin) As Bin, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, ");
            SQL.AppendLine("IfNull(D.InventoryUomCode, G.InventoryUomCode) As InventoryUomCode, ");
            SQL.AppendLine("IfNull(D.InventoryUomCode2, G.InventoryUomCode2) As InventoryUomCode2, ");
            SQL.AppendLine("IfNull(D.InventoryUomCode3, G.InventoryUomCode3) As InventoryUomCode3, ");
            SQL.AppendLine("IfNull(B.CurCode, ifnull(H.CurCode, I.CurCode)) As CurCode, ");
            SQL.AppendLine("A.Uprice, ");
            SQL.AppendLine("(A.Qty*A.Uprice) As Amt, ");
            SQL.AppendLine("A.DOType, A.Remark,  ");
            SQL.AppendLine("J.ProjectCode, J.ProjectName, J.PONo, A.Notes, J.RemarkSO ");
            SQL.AppendLine("From TblRecvCtDtl A ");
            SQL.AppendLine("Left Join TblDOCtHdr B On A.DOCtDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDOCtDtl C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
            SQL.AppendLine("Left Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblDOCt2Hdr E On A.DOCtDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblDOCt2Dtl F On A.DOCtDocNo=F.DocNo And A.DOCtDNo=F.DNo ");
            SQL.AppendLine("Left Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join TblItem K On G.ItCode=K.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("            Select Distinct A.DocNo, C.CurCode   ");
            SQL.AppendLine("            From TblDrHdr A  ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
            SQL.AppendLine("            Union All ");
            SQL.AppendLine("            Select Distinct A.DocNo, D.CurCode   ");
            SQL.AppendLine("            From TblDRHdr A ");
            SQL.AppendLine("            Inner Join TblDRDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSalesContract C On B.SCDocNo = C.DocNo ");
            SQL.AppendLine("            Inner Join TblSalesMemoHdr D On C.SalesMemoDocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblSalesMemoDtl E On D.DocNo = E.DocNo And B.SODNo = E.DNo ");
            SQL.AppendLine(")H On E.DrDocno = H.DocNo And E.DrDocNo Is Not null ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("            Select Distinct A.DocNo, C.CurCode ");
            SQL.AppendLine("            From TblDrhdr A  ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine(")H1 On E.DrDocno = H1.DocNo And E.DrDocNo is not null ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("            Select Distinct A.DocNo, C.CurCode   ");
            SQL.AppendLine("            From TblPLhdr A ");
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
            SQL.AppendLine(")I On E.PLDocNo = I.DocNo And E.PLDocNo is not null ");

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Group_Concat(Distinct X.DOcNo) SOCDocNo, X1.DocNo, Group_Concat(Distinct X5.ProjectCode) ProjectCode, ");
            SQL.AppendLine("    Group_Concat(Distinct X5.ProjectName) ProjectName, ");
            SQL.AppendLine("    Group_Concat(Distinct X2.PONo) PONo,  Group_Concat(Distinct x.Remark) as remarkSO, X.ItCode ");
            SQL.AppendLine("    From TblDRDtl X1 ");
            SQL.AppendLine("    Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo ");
            SQL.AppendLine("    Inner Join tblSoContractdtl X On X1.SODocNO = X.Docno And X1.SODNO = X.Dno ");
            SQL.AppendLine("    Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup X5 On X4.PGCode = X5.PGCode ");
            SQL.AppendLine("    Where X1.DocNo In ( ");
            SQL.AppendLine("        Select T3.DRDocNo ");
            SQL.AppendLine("        From TblRecvCtHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvCtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo Is Not Null ");
            SQL.AppendLine("        Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By X1.DocNo, X.ItCode ");
            SQL.AppendLine(") J On E.DRDocNo = J.DocNo And J.ItCode = F.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "DOCtDocNo", "DOCtDNo", "ItCode", "ItName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "Qty", 
                        
                        //11-15
                        "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                        
                        //16-20
                        "CurCode", "UPrice", "Amt", "DOtype", "Remark",

                        //21-25
                        "ProjectCode", "ProjectName", "PONo", "ItCodeInternal", "RemarkSO",
                        
                        //26-27
                        "Notes", "No"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                    Grd.Cells[Row, 17].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                    Grd.Cells[Row, 20].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 27);


                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowRecvCtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItCodeInternal, B.ItName, ");
            SQL.AppendLine("A.RecvCtQty, A.DOCt3Qty, B.InventoryUomCode, ");
            SQL.AppendLine("A.RecvCtQty2, A.DOCt3Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("A.RecvCtQty3, A.DOCt3Qty3, B.InventoryUomCode3, ");
            SQL.AppendLine("A.DOCt3DocNo ");
            SQL.AppendLine("From TblRecvCtDtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "RecvCtQty", "DOCt3Qty", "InventoryUomCode", 

                        //6-10
                        "RecvCtQty2", "DOCt3Qty2", "InventoryUomCode2", "RecvCtQty3", "DOCt3Qty3", 
                        
                        //11-13
                        "InventoryUomCode3", "DOCt3DocNo", "ItCodeInternal"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);

                }, false, false, false, false
            );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4, 6, 7, 9, 10 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsRecvCtShowPriceInfo = Sm.GetParameter("IsRecvCtShowPriceInfo") == "Y";
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mAcNoForCOGS = Sm.GetParameter("AcNoForCOGS");
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0)
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsSJRNoMandatory = Sm.GetParameterBoo("IsSJRNoMandatory");
            mIsRecvCtAllowToUploadFile = Sm.GetParameterBoo("IsRecvCtAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsSalesTransactionShowSOContractRemark = Sm.GetParameterBoo("IsSalesTransactionShowSOContractRemark");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsSalesContractEnabled = Sm.GetParameterBoo("IsSalesContractEnabled");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
            mIsRecvCtRemarkEditable = Sm.GetParameterBoo("IsRecvCtRemarkEditable");
            mIsDOCtUseSOContract = Sm.GetParameterBoo("IsDOCtUseSOContract");
            mIsRecvCtBasedOnDOCtDROnly = Sm.GetParameterBoo("IsRecvCtBasedOnDOCtDROnly");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
            mIsItemCategoryUseCOAAPAR = Sm.GetParameterBoo("IsItemCategoryUseCOAAPAR");
        }

        private bool IsExceedMaxChar(string Data)
        {
            return Data.Length > 4;
        }

        internal string GetSelectedDOCt()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeRemainingQty()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T1.DocNo, T1.DNo, ");
                SQL.AppendLine("T1.Qty-IfNull(T2.Qty, 0) As OutstandingQty1, T1.Qty2-IfNull(T2.Qty2, 0) As OutstandingQty2, T1.Qty3-IfNull(T2.Qty3, 0) As OutstandingQty3 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select B.DocNo, B.DNo, B.Qty, B.Qty2, B.Qty3 ");
                SQL.AppendLine("    From TblDOCtHdr A, TblDOCtDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo And A.Status='A' And B.CancelInd='N' And A.WhsCode=@WhsCode And A.CtCode=@CtCode ");
                SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @SelectedDOCt)>1 ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select B.DocNo, B.DNo, B.Qty, B.Qty2, B.Qty3 ");
                SQL.AppendLine("    From TblDOCt2Hdr A, TblDOCt2Dtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo And B.CancelInd='N' And A.WhsCode=@WhsCode And A.CtCode=@CtCode ");
                SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @SelectedDOCt)>1 ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select B.DOCtDocNo, B.DOCtDNo, Sum(B.Qty) As Qty, Sum(B.Qty2) As Qty2, Sum(B.Qty3) As Qty3 ");
                SQL.AppendLine("    From TblRecvCtHdr A, TblRecvCtDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And A.WhsCode=@WhsCode ");
                SQL.AppendLine("    And A.CtCode=@CtCode ");
                SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @SelectedDOCt)>1 ");
                SQL.AppendLine("    Group By B.DOCtDocNo, B.DOCtDNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DOCtDocNo And  T1.DNo=T2.DOCtDNo ");

                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                Sm.CmParam<String>(ref cm, "@SelectedDOCt", GetSelectedDOCt());
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "DocNo", 
                        "DNo", "OutstandingQty1", "OutstandingQty2", "OutstandingQty3"
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 4);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        public void ComputeTotal(int row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, row, 15).Length != 0) Qty = Sm.GetGrdDec(Grd1, row, 15);
            if (Sm.GetGrdStr(Grd1, row, 24).Length != 0) UPrice = Sm.GetGrdDec(Grd1, row, 24);

            Grd1.Cells[row, 25].Value = Sm.FormatNum(Qty * UPrice, 0);
        }

        private MySqlCommand UpdateRecvCtFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateRecvCtFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateRecvCtFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvCtFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvCtFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvCtFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsRecvCtAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        private void TxtCtDONo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtDONo);
        }

        private void TxtCtReturnNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtReturnNo);
        }

        private void TxtSJRNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSJRNo);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        #endregion

        #endregion

        #region Class

        private class RecvCtHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string CtName { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string SJRNo { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string SOCRemark { get; set; }
            public string NCRNo { get; set; }
            public string SJNNo { get; set; }

        }

        private class RecvCtDtl
        {
            public string DOCtDocNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string ItCodeInternal { get; set; }
            public string SOCNo { get; set; }
            public string SOCRemark { get; set; }

        }

        private class Sign1
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }

        }

        #endregion
    }
}
