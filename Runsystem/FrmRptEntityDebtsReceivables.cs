﻿#region Update
/* 
    26/07/2017 [TKG] Reporting Debts And Receivables Between Entity
    30/07/2017 [TKG] hilangkan filter entity (2), tambah informasi entity.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEntityDebtsReceivables : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptEntityDebtsReceivables(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();

                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));

                Sl.SetLueEntCode(ref LueEntCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.Cat, T1.DocDt, T2.EntName, T1.JournalDocNo, T1.DocNo, ");
            SQL.AppendLine("T1.DocType, T1.ItCode, T1.ItName, T1.Amt ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("Select '1' As Cat, F.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, G.OptDesc As DocType, ");
            SQL.AppendLine("H.ItCode, I.ItName, H.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D  ");
	        SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode  ");
	        SQL.AppendLine("    And D.EntCode=@EntCode ");
            SQL.AppendLine("Inner join TblCostCenter E On A.CCCode=E.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter F  ");
	        SQL.AppendLine("    On E.ProfitCenterCode=F.ProfitCenterCode ");
	        SQL.AppendLine("    And F.EntCode<>@EntCode ");
            SQL.AppendLine("Inner Join TblOption G On G.OptCat='InventoryTransType' And G.OptCode='05' ");
            SQL.AppendLine("Inner Join TblDODeptDtl H On A.DocNo=H.DocNo And H.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode=I.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice J On H.Source=J.Source ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");
            
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '1' As Cat, D.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
	        SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
	        SQL.AppendLine("    And D.EntCode<>@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
	        SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
	        SQL.AppendLine("    And G.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On J.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '1' As Cat, D.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs2Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode<>@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs2Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' And I.Status='A' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On I.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '1' As Cat, D.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode<>@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On I.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '1' As Cat, D.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode<>@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On J.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Cat, D.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, G.OptDesc As DocType, ");
            SQL.AppendLine("H.ItCode, I.ItName, H.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D  ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode  ");
            SQL.AppendLine("    And D.EntCode<>@EntCode ");
            SQL.AppendLine("Inner join TblCostCenter E On A.CCCode=E.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter F  ");
            SQL.AppendLine("    On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("    And F.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblOption G On G.OptCat='InventoryTransType' And G.OptCode='05' ");
            SQL.AppendLine("Inner Join TblDODeptDtl H On A.DocNo=H.DocNo And H.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode=I.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice J On H.Source=J.Source ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Cat, G.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode=@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode<>@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On J.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Cat, G.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs2Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode=@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode<>@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs2Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' And I.Status='A' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On I.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Cat, G.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs3Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode=@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode<>@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs3Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On I.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Cat, G.EntCode, A.DocDt, A.JournalDocNo, A.DocNo, ");
            SQL.AppendLine("H.OptDesc As DocType, ");
            SQL.AppendLine("K.ItCode, K.ItName, I.Qty*J.UPrice*J.Excrate As Amt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner join TblWarehouse B On A.WhsCode2=B.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter D ");
            SQL.AppendLine("    On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    And D.EntCode=@EntCode ");
            SQL.AppendLine("Inner join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("Inner join TblCostCenter F On E.CCCode=F.CCCode ");
            SQL.AppendLine("Inner join TblProfitCenter G ");
            SQL.AppendLine("    On F.ProfitCenterCode=G.ProfitCenterCode ");
            SQL.AppendLine("    And G.EntCode<>@EntCode ");
            SQL.AppendLine("Inner Join TblOption H On H.OptCat='InventoryTransType' And H.OptCode='10' ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl I On A.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblStockPrice J On I.Source=J.Source ");
            SQL.AppendLine("Inner Join TblItem K On J.ItCode=K.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");

            SQL.AppendLine(") T1, TblEntity T2 Where T1.EntCode=T2.EntCode ");
            SQL.AppendLine("Order By T1.Cat, T2.EntName, T1.DocType, T1.DocDt, T1.DocNo;");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Category",
                        "Entity",
                        "Date",
                        "Journal#",
                        "Document#",
                        
                        //6-9
                        "Type",
                        "Item's Code",
                        "Item's Name",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 200, 80, 140, 150, 
                        
                        //6-9
                        230, 100, 300, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
        }

        override protected void HideInfoInGrd()
        {
           Sm.GrdColInvisible(Grd1, new int[] { 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueMth, "Month")) return;
            if (Sm.IsLueEmpty(LueEntCode, "Entity")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr),Sm.GetLue(LueMth)));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            "Cat", 
                            "EntName", "DocDt", "JournalDocNo", "DocNo", "DocType", "ItCode", 
                            "ItName", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = dr.GetString(0)=="1"?"Piutang Ke Entity":"Hutang Ke Entity";
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.Group();
                Sm.SetGrdAlwaysShowSubTotal(Grd1);
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #endregion
    }
}
