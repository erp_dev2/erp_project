﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestRevision : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, DNoPOR = string.Empty;
        internal bool 
            mIsGroupPaymentTermActived = false,
            mIsFilterBySite = false;
        internal FrmPORequestRevisionFind FrmFind;

        #endregion

        #region Constructor

        public FrmPORequestRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "PO Request Revision";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtVdCode, TxtQtyNew, TxtQtyOld,
                        TxtItCode, TxtPORDocNo, MeeRemark
                    }, true);
                    BtnPORDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtQtyNew, MeeRemark 
                    }, false);
                    BtnPORDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, TxtVdCode,
               TxtPORDocNo, TxtItCode, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtQtyNew, TxtQtyOld
            }, 0);
        }

        private void GetParameter()
        {
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPORequestRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PORequestRevision", "TblPORequestRevision");

            var cml = new List<MySqlCommand>();
            cml.Add(SavePORequestRevision(DocNo));
            cml.Add(UpdatePORequest());
            Sm.ExecCommands(cml);
           
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtPORDocNo, "PO Request Number", false)||
                IsQtyNotValid();

        }

        private bool IsQtyNotValid()
        {
            decimal QtyOld = Decimal.Parse(TxtQtyOld.Text);
            decimal QtyNew = Decimal.Parse(TxtQtyNew.Text);
            if (QtyNew > QtyOld)
            {
                Sm.StdMsg(mMsgType.Warning,
                  "Revision quantity is bigger than outstanding quantity.");
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                    TxtQtyNew
                }, 0);
                return true;
            }
            return false;
        }

        private MySqlCommand SavePORequestRevision(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblPORequestRevision(DocNo, DocDt, PORequestDocNo, PORequestDNo, QtyOld, Qty, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @PORequestDocNo, @PORequestDNo, @QtyOld, @Qty, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PORequestDocNo", TxtPORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PORequestDNo", DNoPOR);
            Sm.CmParam<Decimal>(ref cm, "@QtyOld", Decimal.Parse(TxtQtyOld.Text));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQtyNew.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdatePORequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPORequestDtl SET Qty=@Qty, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtPORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNoPOR);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQtyOld.Text) - Decimal.Parse(TxtQtyNew.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPORequestRevision(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPORequestRevision(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt,  ");
            SQL.AppendLine("A.PORequestDocNo, A.PORequestDNo, E.ItCode, E.ItCode, I.ItName, H.VdName, F.UPrice, A.QtyOld, A.Qty, A.Remark ");
            SQL.AppendLine("From TblPORequestRevision A ");
            SQL.AppendLine("Inner Join TblPORequestHdr B On A.PORequestDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On A.PORequestDocNo=D.DocNo And A.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo=E.DocNo And D.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem I On E.ItCode = I.ItCode ");
            SQL.AppendLine("Inner Join TblQtDtl F On F.DocNo = D.QtDocNo And D.QtDNo = F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On G.DocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblVendor H On G.VdCode= H.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "PORequestDocNo",  "PORequestDNo", "ItCode", "ItName",  
                        
                        //6-10
                        "VdName", "QtyOld", "Qty", "Remark"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtPORDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    DNoPOR = Sm.DrStr(dr, c[3]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtVdCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtQtyOld.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtQtyNew.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                }, true
        );

        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnPORDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPORequestRevisionDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void TxtQtyNew_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQtyNew, 0);
        }

        #endregion

        #endregion
    }
}
