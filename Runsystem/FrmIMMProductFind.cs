﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIMMProductFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmIMMProduct mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMProductFind(FrmIMMProduct FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueIMMProdCtCode(ref LueProdCtCode);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Code", 
                    "Description",
                    "Color",
                    "Category",
                    "Created By",   
                    
                    //6-10
                    "Created Date", 
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time"
                },
                new int[]
                {
                    //0
                    60,

                    //1-5
                    100, 200, 120, 200, 100,

                    //6-10
                    100, 100, 100, 100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "A.ProdCode", "A.ProdDesc" });
                Sm.FilterStr(ref Filter, ref cm, TxtColor.Text, "A.Color", false); 
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProdCtCode), "A.ProdCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ProdCode, A.ProdDesc, A.Color, B.ProdCtName, " +
                    "A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                    "From TblIMMProduct A " +
                    "Left join TblIMMProductCategory B On A.ProdCtCode=B.ProdCtCode " +
                    Filter + " Order By A.ProdDesc;",
                    new string[]
                    {
                        //0
                        "ProdCode", 
                            
                        //1-5
                        "ProdDesc", "Color", "ProdCtName", "CreateBy", "CreateDt", 
                        
                        //6-7
                        "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        private void TxtColor_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkColor_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Color");
        }

        private void LueProdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProdCtCode, new Sm.RefreshLue1(Sl.SetLueIMMProdCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProdCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Product category");
        }

        #endregion

        #endregion
    }
}
