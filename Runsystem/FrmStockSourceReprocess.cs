﻿#region Update
/*
    13/05/2020 [TKG/ASA] New application
    07/07/2020 [TKG/ASA] reverted date tidak mandatory
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockSourceReprocess : RunSystem.FrmBase15
    {
        #region Field

        private string mMenuCode = string.Empty, mDocTitle = string.Empty, mMainCurCode = string.Empty;
        private int mRow = -1;
        
        #endregion

        #region Constructor

        public FrmStockSourceReprocess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mRow = -1;
            if (IsNotValid()) return;

            var l = new List<Process>();
            try
            {
                string
                    Dt1 = string.Empty,
                    Dt2 = string.Empty,
                    MthYr1 = string.Empty,
                    MthYr2 = string.Empty;

                
                Dt1 = Sm.GetDte(DteDt1);
                MthYr1 = string.Concat(Dt1.Substring(4, 2), "/", Dt1.Substring(2, 2));

                if (ChkDt2.Checked)
                {
                    Dt2 = Sm.GetDte(DteDt2);
                    MthYr2 = string.Concat(Dt2.Substring(4, 2), "/", Dt2.Substring(2, 2));

                    if (Sm.CompareStr(MthYr1, MthYr2))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Updating start date and reverted date should not be in the same month.");
                        return;
                    }
                }

                Process3(ref l, MthYr1, MthYr2);
                Process4(ref l, Dt1, Dt2);
                
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                if (mRow == -1)
                    Sm.ShowErrorMsg(Exc);
                else
                {
                    Grd1.Cells[mRow, 2].Value = Exc.ToString();
                    Sm.StdMsg(mMsgType.Warning,
                        "Warehouse's Code : " + Sm.GetGrdStr(Grd1, mRow, 3) + Environment.NewLine +
                        "Source : " + Sm.GetGrdStr(Grd1, mRow, 4) + Environment.NewLine +
                        "Price : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, mRow, 5), 0) + Environment.NewLine + Environment.NewLine +
                        "Error : " + Exc.ToString());
                    Sm.FocusGrd(Grd1, mRow, 2);
                }
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Process",
                        "Warning",
                        "Warehouse",
                        "Source",
                        "New Price"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 200, 150, 180, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 }, false);
        }

        private void Process1(ref List<CSV> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string WhsCodeTemp = string.Empty, SourceTemp = string.Empty;
            var PriceTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            WhsCodeTemp = arr[0].Trim();
                            SourceTemp = arr[1].Trim();
                            if (arr[2].Trim().Length > 0)
                                PriceTemp = decimal.Parse(arr[2].Trim());
                            else
                                PriceTemp = 0m;
                            l.Add(new CSV()
                            {
                                WhsCode = WhsCodeTemp,
                                Source = SourceTemp,
                                Price = PriceTemp
                            });
                        }
                    }
                }
            }
        }

        private void Process2(ref List<CSV> l)
        { 
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i+1;
                r.Cells[1].Value = false;
                r.Cells[2].Value = string.Empty;
                r.Cells[3].Value = l[i].WhsCode;
                r.Cells[4].Value = l[i].Source;
                r.Cells[5].Value = l[i].Price;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Grd1.EndUpdate();   
        }

        private void Process3(ref List<Process> l, string MthYr1, string MthYr2)
        {
            var NoOld = Sm.GetValueDec("Select Max(Convert(Left(DocNo, 4), Decimal)) From TblMutationHdr Where Right(DocNo, 5)=@Param;", MthYr1);
            var NoNew = 0m;
            if (MthYr2.Length>0) NoNew = Sm.GetValueDec("Select Max(Convert(Left(DocNo, 4), Decimal)) From TblMutationHdr Where Right(DocNo, 5)=@Param;", MthYr2);
            string DocNoOld = string.Empty, DocNoNew = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (!Sm.GetGrdBool(Grd1, r, 1) &&
                    Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                {
                    NoOld += 1;
                    DocNoOld = string.Concat(Sm.Right(string.Concat("0000", NoOld.ToString()), 4), "/", mDocTitle, "/MT/", MthYr1);
                    if (MthYr2.Length > 0)
                    {
                        NoNew += 1;
                        DocNoNew = string.Concat(Sm.Right(string.Concat("0000", NoNew.ToString()), 4), "/", mDocTitle, "/MT/", MthYr2);
                    }
                    l.Add(new Process()
                    {
                        row = r,
                        WhsCode = Sm.GetGrdStr(Grd1, r, 3),
                        SourceOld = Sm.GetGrdStr(Grd1, r, 4),
                        Price = Sm.GetGrdDec(Grd1, r, 5),
                        SourceNew = String.Concat("12*", DocNoOld, "*001"),
                        MutationDocNo1 = DocNoOld,
                        MutationDocNo2 = DocNoNew
                    });
                }
            }
        }

        private void Process4(ref List<Process> l, string Dt1, string Dt2)
        {
            foreach (var i in l.OrderBy(o => o.row))
            {
                mRow = i.row;
                Process5(i, Dt1, Dt2);
                Grd1.Cells[i.row, 1].Value = true;
            }
        }

        private void Process5(Process i, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblMutationHdr (DocNo, DocDt, WhsCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo1, @Dt1, @WhsCode, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblMutationDtl ");
            SQL.AppendLine("(DocNo, DNo, CancelInd, Lot, Bin, ");
            SQL.AppendLine("ItCodeFrom, PropCodeFrom, BatchNoFrom, SourceFrom, QtyFrom, Qty2From, Qty3From, ");
            SQL.AppendLine("ItCodeTo, PropCodeTo, BatchNoTo, SourceTo, QtyTo, Qty2To, Qty3To, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo1, '001', 'N', '-', '-', ");
            SQL.AppendLine("ItCode, '-', BatchNo, @SourceFrom, Qty, Qty2, Qty3, ");
            SQL.AppendLine("ItCode, '-', BatchNo, @SourceTo, Qty, Qty2, Qty3, ");
            SQL.AppendLine("@UserCode, @Dt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, BatchNo, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where WhsCode=@WhsCode ");
            SQL.AppendLine("    And Source=@SourceFrom ");
            SQL.AppendLine("    And DocDt<=@Dt1 ");
            SQL.AppendLine("    Group By ItCode, BatchNo ");
            SQL.AppendLine(") T; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeFrom, B.BatchNoFrom, B.SourceFrom, -1*B.QtyFrom, -1*B.Qty2From, -1*B.Qty3From, ");
            SQL.AppendLine("Null As Remark, @UserCode, @Dt ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo1; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, B.QtyTo, B.Qty2To, B.Qty3To, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblMutationHdr A, TblMutationDtl B ");
            SQL.AppendLine("Where A.DocNo=@DocNo1 And A.DocNo=B.DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, B.QtyTo, B.Qty2To, B.Qty3To, ");
            SQL.AppendLine("Null As Remark, @UserCode, @Dt ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo1; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select ItCodeTo, BatchNoTo, SourceTo, ");
            SQL.AppendLine("@CurCode As CurCode, ");
            SQL.AppendLine("@Price As UPrice, ");
            SQL.AppendLine("1.00 As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblMutationDtl ");
            SQL.AppendLine("Where DocNo=@DocNo1; ");

            SQL.AppendLine("Update TblStockMovement Set  ");
            SQL.AppendLine("    Source=@SourceTo ");
            SQL.AppendLine("Where Source=@SourceFrom ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            SQL.AppendLine("And DocDt>@Dt1 ");
            if (Dt2.Length > 0)
                SQL.AppendLine("And DocDt<=@Dt2; ");
            else
                SQL.AppendLine("; ");

            if (Dt2.Length > 0)
            {
                SQL.AppendLine("Insert Into TblMutationHdr (DocNo, DocDt, WhsCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo2, @Dt2, @WhsCode, @UserCode, @Dt); ");

                SQL.AppendLine("Insert Into TblMutationDtl ");
                SQL.AppendLine("(DocNo, DNo, CancelInd, Lot, Bin, ");
                SQL.AppendLine("ItCodeFrom, PropCodeFrom, BatchNoFrom, SourceFrom, QtyFrom, Qty2From, Qty3From, ");
                SQL.AppendLine("ItCodeTo, PropCodeTo, BatchNoTo, SourceTo, QtyTo, Qty2To, Qty3To, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocNo2, '001', 'N', '-', '-', ");
                SQL.AppendLine("ItCode, '-', BatchNo, @SourceTo, Qty, Qty2, Qty3, ");
                SQL.AppendLine("ItCode, '-', BatchNo, @SourceFrom, Qty, Qty2, Qty3, ");
                SQL.AppendLine("@UserCode, @Dt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select ItCode, BatchNo, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@SourceTo ");
                SQL.AppendLine("    And DocDt<=@Dt2 ");
                SQL.AppendLine("    Group By ItCode, BatchNo ");
                SQL.AppendLine(") T; ");

                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeFrom, B.BatchNoFrom, B.SourceFrom, -1*B.QtyFrom, -1*B.Qty2From, -1*B.Qty3From, ");
                SQL.AppendLine("Null As Remark, @UserCode, @Dt ");
                SQL.AppendLine("From TblMutationHdr A ");
                SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo2; ");

                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, B.QtyTo, B.Qty2To, B.Qty3To, ");
                SQL.AppendLine("Null As Remark, @UserCode, @Dt ");
                SQL.AppendLine("From TblMutationHdr A ");
                SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo2; ");
            }

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select WhsCode, Source, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where WhsCode=@WhsCode And Source In (@SourceFrom, @SourceTo) ");
            SQL.AppendLine("    Group By WhsCode, Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set T1.Qty=T2.Qty, T1.Qty2=T2.Qty2, T1.Qty3=T2.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=@Dt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo1", i.MutationDocNo1);
            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            if (Dt2.Length > 0)
            {
                Sm.CmParam<String>(ref cm, "@DocNo2", i.MutationDocNo2);
                Sm.CmParamDt(ref cm, "@Dt2", Dt2);
            }
            Sm.CmParam<String>(ref cm, "@WhsCode", i.WhsCode);
            Sm.CmParam<String>(ref cm, "@SourceFrom", i.SourceOld);
            Sm.CmParam<String>(ref cm, "@SourceTo", i.SourceNew);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<Decimal>(ref cm, "@Price", i.Price);
            Sm.CmParam<String>(ref cm, "@DocType1", "11");
            Sm.CmParam<String>(ref cm, "@DocType2", "12");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private bool IsNotValid()
        {
            if (Sm.IsDteEmpty(DteDt1, "Updating start date") || 
                (ChkDt2.Checked && Sm.IsDteEmpty(DteDt2, "Reverted date")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()) 
                return true;
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 4, false, "Source is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 5, true, "Price should be bigger than 0.00.")) return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var lCSV = new List<CSV>();

            Sm.ClearGrd(Grd1, false);
            try
            {
                Process1(ref lCSV);
                if (lCSV.Count > 0)
                    Process2(ref lCSV);
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lCSV.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void ChkDt2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkDt2.Checked == false) DteDt2.EditValue = null;
        }

        private void DteDt2_Validated(object sender, EventArgs e)
        {
            ChkDt2.Checked = Sm.GetDte(DteDt2).Length > 0;
        }

        #endregion

        #endregion
    }

    #region Class

    internal class CSV
    {
        public string WhsCode { get; set; }
        public string Source { get; set; }
        public decimal Price { get; set; }
    }

    internal class Process
    {
        public int row { get; set; }
        public string WhsCode { get; set; }
        public string SourceOld { get; set; }
        public decimal Price { get; set; }
        public string SourceNew { get; set; }
        public string MutationDocNo1 { get; set; }
        public string MutationDocNo2 { get; set; }
    }

    #endregion
}
