﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARLedgerPaidItemDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptAgingARLedgerPaidItem mFrmParent;
        private string mSQL = string.Empty,  mDocNo = string.Empty, mCtName = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARLedgerPaidItemDlg(FrmRptAgingARLedgerPaidItem FrmParent, string DocNo, string CtName)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mCtName = CtName;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); ShowData();
            TxtDocNo.EditValue = mDocNo;
            TxtCtName.EditValue = mCtName;
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtName, TxtDocNo, 
                    }, true);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCtName, TxtDocNo, 
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "SO#",

                        //1-4
                        "Date",
                        "Amount", 
                        "",
                        "DocType"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("Select Distinct A.DocNo, If(A.DocType='1', E1.DocDt, E2.DocDt) As DocDt, A.DocType, ");
                SQL.AppendLine("If(A.DocType='1', D1.SODocNo, D2.SODocNo) As SODocNo,  ");
                SQL.AppendLine("If(A.DocType='1', E1.Amt, E2.Amt) As Amt ");
                SQL.AppendLine("From TblSalesInvoiceDtl A  ");
                SQL.AppendLine("Inner Join TblSalesInvoicehdr A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo  ");
                SQL.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo  ");
                SQL.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo  ");
                SQL.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo  ");
                SQL.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo  ");
                SQL.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo  ");
                SQL.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo  ");
                SQL.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo  ");
                SQL.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo  ");
                SQL.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo  ");
                SQL.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo  ");
                SQL.AppendLine("Union all ");
                SQL.AppendLine("Select Distinct A.DocNo, E1.DocDt, '3', A2.SODocNo, E1.Amt ");
                SQL.AppendLine("From TblSalesInvoiceDtl A  ");
                SQL.AppendLine("Inner Join TblSalesInvoicehdr A2 On A.DocNo = A2.DocNo ");
                SQL.AppendLine("Inner Join TblSOHdr E1 On A2.SODocNo=E1.DocNo  ");
                SQL.AppendLine("Left Join TblSODtl F1 On A.DoCtDocNo=F1.DocNo And A.DOCtDNo=F1.DNo  ");
                SQL.AppendLine("Where A2.SoDocNo is not null ");
                SQL.AppendLine(")X ");
                
                mSQL = SQL.ToString();

                string Filter = "Where X.DocNo = '" + mDocNo + "'  ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "SoDocNo",
                            //1-5
                            "DocDt", "Amt", "DocType"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                           }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmSO2("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }
        #endregion
        
    }
}
