﻿#region Update
/*
    26/11/2019 [TKG/IMS] New reporting
    18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovementMovingAvg : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsFilterByItCt = false;
        internal bool mIsBOMShowSpecifications = false;

        #endregion

        #region Constructor

        public FrmRptStockMovementMovingAvg(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.OptDesc, A.DocNo, A.DocDt, C.WhsName, A.ItCode, D.ItName, D.ItCodeInternal, E.ItCtName, ");
            SQL.AppendLine("A.Qty, D.InventoryUomCode, A.MovingAvgCurCode, A.MovingAvgPrice, D.Specification ");
            SQL.AppendLine("From TblStockMovement A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='InventoryTransType' ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCode=D.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.DocDt, D.ItCodeInternal, A.ItCode, A.Qty; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",
                        "Document#",
                        "Date",
                        "Warehouse",
                        "Item's Code",
                        
                        //6-10
                        "Item's Name",
                        "Local Code",
                        "Category",
                        "Quantity", 
                        "UoM",   
                  
                        //11-14
                        "Currency", 
                        "Price", 
                        "Total",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 150, 80, 200, 100, 
                        
                        //6-10
                        200, 100, 180, 100, 80,   
                        
                        //11-14
                        60, 130, 150, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 8, 11 });
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 14 });
            Grd1.Cols[14].Move(7);
            Grd1.Cols[7].Move(5);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 8, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName", "D.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL(Filter),
                new string[]
                    {
                        //0
                        "OptDesc", 
                        //1-5
                        "DocNo", "DocDt", "WhsName", "ItCode", "ItName", 
                        //6-10
                        "ItCodeInternal", "ItCtName", "Qty", "InventoryUomCode", "MovingAvgCurCode", 
                        //11-12
                        "MovingAvgPrice", "Specification"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 9) * Sm.GetGrdDec(Grd, Row, 12);
                    }, true, false, false, false
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{9, 13});
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion


        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
