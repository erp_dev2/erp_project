﻿#region Update
/*
 * 25/07/2022 [HPH/SIER] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAdvanceCharge : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmAdvanceChargeFind FrmFind;


        //internal Frm FrmFind;

        #endregion

        #region Constructor

        public FrmAdvanceCharge(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
            Sl.SetLueAcNo(ref LueAcNo);
            SetLueAcType(ref LueAcType);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtACCode, TxtACName, TxtLocalName, TxtValue, LueAcType, LueAcNo, ChkValue, ChkActInd
                    }, true);
                    BtnAcNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       TxtACName,  TxtACName, TxtLocalName, TxtValue, LueAcType, LueAcNo, ChkValue
                    }, false);    
                    ChkActInd.Checked = true;
                    BtnAcNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        LueAcNo, ChkActInd
                    }, true);
                    TxtACName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      TxtACCode, TxtACName, TxtACName, TxtLocalName, TxtValue, LueAcType, LueAcNo, ChkValue 
                    }, true);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Checked = false;
                    BtnAcNo.Enabled = false;
                    break;
                default:
                    break;
            }
        }
        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtACCode, TxtACName, TxtLocalName, TxtValue, LueAcType, LueAcNo
            });
            ChkActInd.Checked = false;
            ChkValue.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtValue
            }, 0);
        }

        #endregion

        #region Button method


        override protected void BtnFindClick(object sender, EventArgs e)
        {
            {
                ClearData();
                if (FrmFind == null) FrmFind = new FrmAdvanceChargeFind(this);
                Sm.SetFormFindSetting(this, FrmFind);

            }
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtACCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);

        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {

            try
            {
                var cm = new MySqlCommand();

                Sm.ExecCommand(cm);


                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {        
              if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;
                 
                Cursor.Current = Cursors.WaitCursor;
                if (TxtACCode.Text.Length == 0)
                {
                    TxtACCode.Text = GenerateCodeOtomatis();
                }
                var cml = new List<MySqlCommand>();
                cml.Add(SaveAdvanceCharge(TxtACCode.Text));
                Sm.ExecCommands(cml);
                ShowData(TxtACCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data
        public void ShowData(string ACCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                Sl.SetLueAcNo(ref LueAcNo);
                ShowAdvaceCharge(ACCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }
        private void SetLueAcType(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'C' As Col1, 'Credit' As Col2 Union All " +
                "Select 'D' As Col1, 'Debit' As Col2; ",
                0, 25, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void ShowAdvaceCharge(string ACCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT AdvanceChargeCode, AdvanceChargeName, ActInd, LocalName, PercentageInd, Value, AcType, AcNo  ");             
                SQL.AppendLine("FROM tbladvancecharge  ");
                SQL.AppendLine("WHERE AdvanceChargeCode = @AdvanceChargeCode; ");

                Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", ACCode);

                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "AdvanceChargeCode", 

                        //1-5
                        "AdvanceChargeName", "ActInd", "LocalName", "PercentageInd",  "Value",

                        //6-7
                        "AcType", "AcNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtACCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtACName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");                                               
                        TxtLocalName.EditValue = Sm.DrStr(dr, c[3]);
                        ChkValue.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        TxtValue.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueAcNo, Sm.DrStr(dr, c[7]));
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Save Data
        private bool IsDataNotValid()
        {
            return
            IsCodeAlreadyCancelled() ||
            Sm.IsTxtEmpty(TxtACName, " Name", false) ||
            Sm.IsLueEmpty(LueAcType, "Debet/Credit") ||
            Sm.IsLueEmpty(LueAcNo, "COA");
            
        }

        private string GenerateCodeOtomatis()
        {

            int lenCode = 4;
            var SQL = new StringBuilder();
            var MaxCode = Sm.GetValue("SELECT MAX(LENGTH(A.AdvanceChargeCode)) FROM tblAdvanceCharge A");
            if (MaxCode.Length > 0)
                lenCode = Convert.ToInt32(MaxCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            SQL.AppendLine("    Select Cast(IfNull(Max(A.AdvanceChargeCode), '0000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From TblAdvanceCharge A ");
            SQL.AppendLine("    WHERE LENGTH(A.AdvanceChargeCode) = " + lenCode + " ");
            SQL.AppendLine("    Group By A.AdvanceChargeCode ");
            SQL.AppendLine("    Order By A.AdvanceChargeCode Desc Limit 1");
            SQL.AppendLine("), 1)), " + lenCode + ") As Code");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveAdvanceCharge(string ACCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert into tbladvancecharge (AdvanceChargeCode, AdvanceChargeName, ActInd, LocalName, PercentageInd, Value, AcType, AcNo, CreateBy, CreateDt)");
            SQL.AppendLine("Values (@AdvanceChargeCode, @AdvanceChargeName, @ActInd, @LocalName, @PercentageInd, @Value, @AcType, @AcNo, @CreateBy, CurrentDateTime())");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update AdvanceChargeCode=@AdvanceChargeCode, AdvanceChargeName=@AdvanceChargeName, ActInd=@ActInd, LocalName=@LocalName, PercentageInd=@PercentageInd, Value=@Value, AcType=@AcType, AcNo=@AcNo,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AdvanceChargeCode", ACCode);                    
            Sm.CmParam<String>(ref cm, "@AdvanceChargeName", TxtACName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LocalName", TxtLocalName.Text);
            Sm.CmParam<String>(ref cm, "@PercentageInd", ChkValue.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Value", Decimal.Parse(TxtValue.Text));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueAcNo));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }


        private bool IsCodeAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select AdvanceChargeCode From TblAdvanceCharge " +
                "Where AdvanceChargeCode=@Param AND ActInd = 'N';",
                TxtACCode.Text,
                "This asset already not active.");
        }

        #region Insert Data


        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event


        private void LueMasterCOA_EditValueChanged(object sender, EventArgs e)
            {
                if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcNo, new Sm.RefreshLue1(Sl.SetLueAcNo));
            }
        private void ChkActiveInd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void TxtLocalName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalName);
        }

        private void TxtMasterName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtValue, 0);
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(SetLueAcType));
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {                
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmAdvanceChargeDlg(this));
        }
        #endregion

        #endregion

       

    }
} 
