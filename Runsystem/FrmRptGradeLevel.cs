﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using GroupRowCountManager_CS;



#endregion

namespace RunSystem
{
    public partial class FrmRptGradeLevel : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private iGGroupRowCountManager GRCM = new iGGroupRowCountManager();
        

        #endregion

        #region Constructor

        public FrmRptGradeLevel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode);
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (Select C.GrdLvlName,A.EmpCode,A.EmpName,A.JoinDt,B.DeptName,D.GrdLvlGrpName,A.DeptCode,A.GrdLvlCode ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode  ");
            SQL.AppendLine("Inner Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode  ");
            SQL.AppendLine("Inner Join TblGradeLevelGroup D On C.GrdLvlGrpCode = D.GrdLvlGrpCode  ");
            SQL.AppendLine("Where A.ResignDt >= Left(CurrentDateTime(),8)or IFNULL(A.ResignDt,0)=0 ");
            SQL.AppendLine(") As T1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            GRCM.Attach(Grd1);

            Grd1.GroupBox.Visible = true;
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Grade Level Name", 
                        "Employee "+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Department", 
                        "Grade Level Group",
                        //6-8
                        "Join Date",
                        "Working Period",
                        "Total",
                      
                        
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, false);
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            int NowYear = int.Parse(Sm.GetValue("Select Left(CurrentDateTime(),4 ) "));
            int NowMonth = int.Parse(Sm.GetValue("Select SubString(CurrentDateTime(),5,2 ) "));
            int NowDay = int.Parse(Sm.GetValue("Select SubString(CurrentDateTime(),7,2 ) "));
            int Now= int.Parse(Sm.GetValue("Select Left(CurrentDateTime(),6 ) "));
           
            string LastMonthDate=Sm.GetValue("Select Date_Add(Str_To_Date('01,"+NowMonth+","+NowYear+"' ,'%d,%m,%Y'), INTERVAL - 1 DAY)");
            int jml = 0;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "";

                var cm = new MySqlCommand();
                int i = 0;

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T1.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtGrdLvlCode.Text, new string[] { "T1.GrdLvlCode", "T1.GrdLvlName" });

                Sm.ShowDataInGridGrdLvl2Grp(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T1.EmpCode",
                        new string[]
                        {
                            //0
                            "GrdLvlName",
                            //1-5
                            "EmpCode","EmpName","DeptName","GrdLvlGrpName","JoinDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            i += 1;
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);

                            int JoinDtYear = int.Parse(Sm.GetGrdDate(Grd1, Row, 6).Substring(0, 4));
                            int JoinDtMonth = int.Parse(Sm.GetGrdDate(Grd1, Row, 6).Substring(4, 2));
                            int JoinDtDay = int.Parse(Sm.GetGrdDate(Grd1, Row, 6).Substring(6, 2));
                            
                            int WorkingPeriodYear = 0;
                            int WorkingPeriodMonth = 0;
                            int WorkingPeriodDay = 0;
                            string yr = "";
                            string mth = "";
                            string day = "";

                            int LastDay = int.Parse(LastMonthDate.Substring(3, 2));
                            //Sm.StdMsg(mMsgType.Warning, LastDay.ToString());
                            if (NowYear == JoinDtYear) {
                                WorkingPeriodYear = 0;
                                
                                    if (NowDay >= JoinDtDay)
                                    {
                                        WorkingPeriodMonth = NowMonth - JoinDtMonth;
                                        WorkingPeriodDay = NowDay - JoinDtDay;

                                    }
                                    else {
                                        WorkingPeriodMonth = (NowMonth - JoinDtMonth)-1;
                                        WorkingPeriodDay = (LastDay - JoinDtDay) + NowDay;
                                    }
                                
                            
                            }
                            else if (JoinDtYear < NowYear) { 
                              // int month= NowMonth + ((NowYear - JoinDtYear) * 12) - JoinDtMonth;
                               int year = NowYear - JoinDtYear;
                               if (NowMonth >= JoinDtMonth)
                               {
                                   WorkingPeriodYear = year;
                                  
                                   if (NowDay >= JoinDtDay)
                                   {
                                       WorkingPeriodMonth = NowMonth - JoinDtMonth;
                                       WorkingPeriodDay = NowDay - JoinDtDay;

                                   }
                                   else
                                   {
                                       WorkingPeriodMonth = (NowMonth - JoinDtMonth)-1;
                                       WorkingPeriodDay = (LastDay - JoinDtDay) + NowDay;
                                   }


                               }
                               else {
                                   WorkingPeriodYear = year-1;
                                   if (NowDay >= JoinDtDay)
                                   {
                                       WorkingPeriodMonth = 12 - JoinDtMonth + NowMonth;
                                       WorkingPeriodDay = NowDay - JoinDtDay;

                                   }
                                   else
                                   {
                                       WorkingPeriodMonth = (12 - JoinDtMonth + NowMonth)-1;
                                       WorkingPeriodDay = (LastDay - JoinDtDay) + NowDay;
                                   }
                               }

                               
                            }





                            if (WorkingPeriodYear == 1 )
                            {
                                 yr = "Year";

                            }
                            if (WorkingPeriodMonth == 1) {
                                mth = "Month";
                            }
                            if (WorkingPeriodDay == 1)
                            {
                                day = "Day";
                            }
                            if (WorkingPeriodYear > 1)
                            {
                                yr = "Years";
                            }
                            if (WorkingPeriodMonth > 1)
                            {
                                mth = "Months";
                            }
                            if (WorkingPeriodDay > 1)
                            {
                                day = "Days";
                            }
                            if (WorkingPeriodYear == 0)
                            {
                                if (WorkingPeriodMonth == 0)
                                {
                                    if (WorkingPeriodDay == 0)
                                    {
                                        Grd1.Cells[Row, 7].Value = "0";

                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodDay +" "+ day;
                                    }
                                }
                                else
                                {
                                    
                                    if (WorkingPeriodDay == 0)
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodMonth + " " + mth;

                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodMonth + " " + mth + ", " + WorkingPeriodDay + " " + day;
                                    }
                                }
                            }
                            else
                            {
                                if (WorkingPeriodMonth == 0)
                                {
                                    if (WorkingPeriodDay == 0)
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodYear + " " + yr;

                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodYear + " " + yr + ", " + WorkingPeriodDay + " " + day;
                                    }
                                }
                                else
                                {
                                    if (WorkingPeriodDay == 0)
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodYear + " " + yr + ", " + WorkingPeriodMonth + " " + mth;

                                    }
                                    else
                                    {
                                        Grd1.Cells[Row, 7].Value = WorkingPeriodYear + " " + yr + ", " + WorkingPeriodMonth + " " + mth + ", " + WorkingPeriodDay + " " + day;
                                    }

                                }

                            }
                        }, true, true, false, true
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.SetGrdProperty(Grd1, true);
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion


        #endregion

        

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtGrdLvlCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGrdLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Grade Level");
        }

        #endregion




        #endregion

    }
}
