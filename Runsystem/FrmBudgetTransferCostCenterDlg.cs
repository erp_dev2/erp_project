﻿#region Update
/*
    07/05/2021 [WED/PHT] new apps
    18/06/2021 [TKG/PHT] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    27/10/2021 [ICA/PHT] memunculkan cost center yg tidak menjadi parent di cost center lain (ga cuma dari parentInd)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransferCostCenterDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetTransferCostCenter mFrmParent;
        private string mSQL = string.Empty, mProfitCenterCode = string.Empty, mLabelForm = string.Empty;
        private byte mDocType = 0;

        #endregion

        #region Constructor

        public FrmBudgetTransferCostCenterDlg(FrmBudgetTransferCostCenter FrmParent, byte DocType, string ProfitCenterCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
            mProfitCenterCode = ProfitCenterCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {

                if (mDocType == 1 || mDocType == 2) mLabelForm = "Profit Center";
                else mLabelForm = "Cost Center";

                this.Text = "List of " + mLabelForm;
                LblCode.Text = mLabelForm;

                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-2
                    mLabelForm + " Code", 
                    mLabelForm + " Name"

                },
                new int[] 
                {
                    //0
                    50,

                    //1-2
                    150, 300
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.Code, T.Name From ( ");

            if (mDocType == 1 || mDocType == 2)
            {
                SQL.AppendLine("Select ProfitCenterCode As Code, ProfitCenterName As Name ");
                SQL.AppendLine("From TblProfitCenter ");
                SQL.AppendLine("Where ProfitCenterCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct B.ProfitCenterCode ");
                SQL.AppendLine("    From TblCompanyBudgetPlanHdr A, TblCostCenter B, TblGroupProfitCenter C, TblUser D ");
                SQL.AppendLine("    Where A.CCCode Is Not Null ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And A.CompletedInd = 'Y' ");
                SQL.AppendLine("    And A.CCCode=B.CCCode ");
                SQL.AppendLine("    And B.ActInd = 'Y' ");
                SQL.AppendLine("    And B.CBPInd = 'Y' ");
                SQL.AppendLine("    And B.NotParentInd='Y' ");
                SQL.AppendLine("    And B.CCCode Not In (Select Parent From TblCostCenter Where Parent Is Not Null)");
                SQL.AppendLine("    And B.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And B.ProfitCenterCode=C.ProfitCenterCode ");
                SQL.AppendLine("    And C.GrpCode=D.GrpCode ");
                SQL.AppendLine("    And D.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
            
                //SQL.AppendLine("    Select Distinct B.ProfitCenterCode ");
                //SQL.AppendLine("    From ");
                //SQL.AppendLine("    ( ");
                //SQL.AppendLine("        Select Distinct CCCode ");
                //SQL.AppendLine("        From TblCompanyBudgetPlanHdr ");
                //SQL.AppendLine("        Where CCCode Is Not Null ");
                //SQL.AppendLine("        And CancelInd = 'N' ");
                //SQL.AppendLine("        And CompletedInd = 'Y' ");
                //SQL.AppendLine("    ) A ");
                //SQL.AppendLine("    Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
                //SQL.AppendLine("        And B.ActInd = 'Y' ");
                //SQL.AppendLine("        And B.CBPInd = 'Y' ");
                //SQL.AppendLine("        And B.NotParentInd='Y' ");
                ////SQL.AppendLine("        And B.CCCode Not In (Select Parent From TblCostCenter Where Parent Is Not Null) ");
                
            }
            else
            {
                SQL.AppendLine("Select CCCode As Code, CCName As Name ");
                SQL.AppendLine("From TblCostCenter ");
                SQL.AppendLine("Where ActInd = 'Y' ");
                SQL.AppendLine("And CBPInd = 'Y' ");
                SQL.AppendLine("And NotParentInd='Y' ");
                SQL.AppendLine("And ProfitCenterCode = @ProfitCenterCode ");
                SQL.AppendLine("And CCCode Not In (Select Parent From TblCostCenter Where Parent Is Not Null) ");
            }

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", mProfitCenterCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtCode.Text, new string[] { "T.Code", "T.Name" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter + " Order By T.Name;",
                    new string[] { "Code", "Name" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                string Code = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                string Name = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);

                if (mDocType == 1)
                {
                    mFrmParent.TxtProfitCenterCode.EditValue = Code;
                    mFrmParent.TxtProfitCenterName.EditValue = Name;
                }

                if (mDocType == 2)
                {
                    mFrmParent.TxtProfitCenterCode2.EditValue = Code;
                    mFrmParent.TxtProfitCenterName2.EditValue = Name;
                }

                if (mDocType == 3)
                {
                    mFrmParent.TxtCCCode.EditValue = Code;
                    mFrmParent.TxtCCName.EditValue = Name;
                }

                if (mDocType == 4)
                {
                    mFrmParent.TxtCCCode2.EditValue = Code;
                    mFrmParent.TxtCCName2.EditValue = Name;
                }

                mFrmParent.ClearGrd();
                this.Close();
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, mLabelForm);
        }

        #endregion

        #endregion
    }
}
