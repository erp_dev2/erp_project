﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPRDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPPR mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPPRDlg(FrmPPR FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocType, T1.DocDesc, T1.DocNo, T1.DocDt, T1.DNo, T1.EmpCode, T2.EmpName, T3.PosName, T4.DeptName, T1.Amt, T6.DocName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As DocType, 'Wages' As DocDesc, A.DocNo, A.DocDt, A.ShopFloorControlDocNo, B.DNo, B.EmpCode, B.Wages As Amt ");
            SQL.AppendLine("    From TblProductionWagesHdr A ");
            SQL.AppendLine("    Inner Join TblProductionWagesDtl3 B On A.DocNo=B.DocNo And B.ProcessInd='O' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, 'Wages' As DocDesc, A.DocNo, A.DocDt, A.ShopFloorControlDocNo, B.DNo, B.EmpCode, B.Wages As Amt ");
            SQL.AppendLine("    From TblProductionWagesHdr A ");
            SQL.AppendLine("    Inner Join TblProductionWagesDtl4 B On A.DocNo=B.DocNo And B.ProcessInd='O' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '3' As DocType, 'Penalty' As DocDesc, A.DocNo, A.DocDt, A.ShopFloorControlDocNo, B.DNo, B.EmpCode, -1*B.Penalty As Amt ");
            SQL.AppendLine("    From TblProductionPenaltyHdr A ");
            SQL.AppendLine("    Inner Join TblProductionPenaltyDtl3 B On A.DocNo=B.DocNo And B.ProcessInd='O' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '4' As DocType, 'Penalty' As DocDesc, A.DocNo, A.DocDt, A.ShopFloorControlDocNo, B.DNo, B.EmpCode, -1*B.Penalty As Amt ");
            SQL.AppendLine("    From TblProductionPenaltyHdr A ");
            SQL.AppendLine("    Inner Join TblProductionPenaltyDtl4 B On A.ShopFloorControlDocNo=B.DocNo And B.ProcessInd='O' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            SQL.AppendLine("Left Join TblDepartment T4 On T2.DeptCode=T4.DeptCode ");
            SQL.AppendLine("Left Join TblShopFloorControlHdr T5 On T1.ShopFloorControlDocNo=T5.DocNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr T6 On T5.WorkCenterDocNo=T6.DocNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        
                        //6-10
                        "Document#",
                        "DNo",
                        "Type",
                        "Type",
                        "Date",

                        //11-12
                        "Amount",
                        "Work Center"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        50, 80, 200, 150, 150, 
                        
                        //6-10
                        150, 0, 0, 100, 100, 
                        
                        //11-12
                        100, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 7, 8, 10 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where Locate(Concat('##', T1.DocType, T1.DocNo, T1.DNo, '##'), @SelectedDocNo)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedDocNo", mFrmParent.GetSelectedDocNo());
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T1.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T2.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T2.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, "T6.DocName", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL +
                        Filter + " Order By T2.EmpName, T1.EmpCode, T1.DocType;",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "PosName", "DeptName", "DocNo", "DNo",
                            
                            //6-10
                            "DocType", "DocDesc", "DocDt", "Amt", "DocName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        mFrmParent.Grd1.Cells[Row1, 11].Value = "O";

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10 });
                    }
                }
            }
            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
            else
                mFrmParent.SetSummary();
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 8) +
                Sm.GetGrdStr(Grd1, Row, 6) +
                Sm.GetGrdStr(Grd1, Row, 7);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key,
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 6) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 7) 
                    ))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
