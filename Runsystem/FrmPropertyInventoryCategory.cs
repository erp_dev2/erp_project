﻿#region Update
/*
    30/11/2022 [IBL/BBT] New Apps
    05/01/2023 [MAU/BBT] ketika edit data dapat merubah checkbox
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mPropertyCode = string.Empty; //if this application is called from other application;
        internal bool mIsCOAUseAlias = false;
        internal FrmPropertyInventoryCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmPropertyInventoryCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mPropertyCode.Length != 0)
                {
                    ShowData(mPropertyCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { TxtPropCtCode, TxtPropCtName }, true);

                    ChkActiveInd.Properties.ReadOnly = true;
                    ChkRentCategoryInd.Properties.ReadOnly = true;
                    ChkSalesCategoryInd.Properties.ReadOnly = true;
                    ChkInitialCategoryInd.Properties.ReadOnly = true;
                    ChkMandatoryCertificateInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    BtnAcNo3.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { TxtPropCtCode, TxtPropCtName }, false);
                    ChkRentCategoryInd.Properties.ReadOnly = false;
                    ChkSalesCategoryInd.Properties.ReadOnly = false;
                    ChkInitialCategoryInd.Properties.ReadOnly = false;
                    ChkMandatoryCertificateInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    TxtPropCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPropCtName }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkRentCategoryInd.Properties.ReadOnly = false;
                    ChkSalesCategoryInd.Properties.ReadOnly = false;
                    ChkInitialCategoryInd.Properties.ReadOnly = false;
                    ChkMandatoryCertificateInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    TxtPropCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtPropCtCode, TxtPropCtName,
                TxtAcNo, TxtAcDesc,
                TxtAcNo2, TxtAcDesc2,
                TxtAcNo3, TxtAcDesc3
            });
            ChkActiveInd.Checked = false;
            ChkRentCategoryInd.Checked = false;
            ChkSalesCategoryInd.Checked = false;
            ChkInitialCategoryInd.Checked = false;
            ChkMandatoryCertificateInd.Checked = false;
        }
        #endregion

        #region Button

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPropertyInventoryCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActiveInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPropCtCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPropCtCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPropertyInventoryCategory Where PropCtCode=@PropCtCode" };
                Sm.CmParam<String>(ref cm, "@PropCtCode", TxtPropCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPropertyInventoryCategory(PropertyCategoryCode, PropertyCategoryName, ActInd, RentCategoryInd, SalesCategoryInd, InitialCategoryInd, MandatoryCertificateInd, AcNo1, AcNo2, AcNo3, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PropCtCode, @PropCtName, @ActInd, @RentCategoryInd, @SalesCategoryInd, @InitialCategoryInd, @MandatoryCertificateInd, @AcNo1, @AcNo2, @AcNo3, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PropertyCategoryName=@PropCtName, ActInd=@ActInd, RentCategoryInd=@RentCategoryInd, SalesCategoryInd=@SalesCategoryInd, InitialCategoryInd=@InitialCategoryInd, MandatoryCertificateInd=@MandatoryCertificateInd, ");
                SQL.AppendLine("   AcNo1=@AcNo1, AcNo2=@AcNo2, AcNo3=@AcNo3, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PropCtCode", TxtPropCtCode.Text);
                Sm.CmParam<String>(ref cm, "@PropCtName", TxtPropCtName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@RentCategoryInd", ChkRentCategoryInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@SalesCategoryInd", ChkSalesCategoryInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@InitialCategoryInd", ChkInitialCategoryInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@MandatoryCertificateInd", ChkMandatoryCertificateInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
                Sm.CmParam<String>(ref cm, "@AcNo3", TxtAcNo3.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPropCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PropCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCategoryCode, A.PropertyCategoryName, A.ActInd, A.RentCategoryInd, A.SalesCategoryInd, ");
            SQL.AppendLine("InitialCategoryInd, MandatoryCertificateInd, A.AcNo1, A.AcNo2, A.AcNo3, ");
            if (mIsCOAUseAlias)
            {
                SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc1, ");
                SQL.AppendLine("Concat(C.AcDesc, Case When C.Alias Is Null Then '' Else Concat(' [', C.Alias, ']') End) As AcDesc2, ");
                SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc3 ");
            }
            else
            {
                SQL.AppendLine("B.AcDesc As AcDesc1, C.AcDesc As AcDesc2, D.AcDesc As AcDesc3 ");
            }
            SQL.AppendLine("From TblPropertyInventoryCategory A ");
            SQL.AppendLine("Left Join TblCoa B On A.AcNo1=B.AcNo ");
            SQL.AppendLine("Left Join TblCoa C On A.AcNo2=C.AcNo ");
            SQL.AppendLine("Left Join TblCoa D On A.AcNo3=D.AcNo ");
            SQL.AppendLine("Where A.PropertyCategoryCode=@PropCtCode;");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PropCtCode", PropCtCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "PropertyCategoryCode",
                        //1-5
                        "PropertyCategoryName", "ActInd", "RentCategoryInd", "SalesCategoryInd", "InitialCategoryInd",
                        //6-10
                        "MandatoryCertificateInd", "AcNo1", "AcNo2", "AcNo3", "AcDesc1",
                        //11-12
                        "AcDesc2", "AcDesc3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPropCtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPropCtName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        ChkRentCategoryInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        ChkSalesCategoryInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        ChkInitialCategoryInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                        ChkMandatoryCertificateInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                        TxtAcNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAcNo2.EditValue = Sm.DrStr(dr, c[8]);
                        TxtAcNo3.EditValue = Sm.DrStr(dr, c[9]);
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[10]);
                        TxtAcDesc2.EditValue = Sm.DrStr(dr, c[11]);
                        TxtAcDesc3.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPropCtCode, "Property category code", false) ||
                Sm.IsTxtEmpty(TxtPropCtName, "Property category name", false) ||
                Sm.IsTxtEmpty(TxtAcNo, "COA# for Inventory", false) ||
                ((ChkRentCategoryInd.Checked || ChkSalesCategoryInd.Checked) && Sm.IsTxtEmpty(TxtAcNo2, "COA# for Sales", false)) ||
                (ChkSalesCategoryInd.Checked && Sm.IsTxtEmpty(TxtAcNo3, "COA# for COGS", false)) ||
                IsItCtCodeExisted();
        }

        private bool IsItCtCodeExisted()
        {
            if (!TxtPropCtCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select PropertyCategoryCode From TblPropertyInventoryCategory Where PropertyCategoryCode=@Param;", TxtPropCtCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Property inventory category code ( " + TxtPropCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ('IsCOAUseAlias'");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCOAUseAlias": mIsCOAUseAlias = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPropCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPropCtCode);
        }

        private void TxtPropCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPropCtName);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPropertyInventoryCategoryDlg(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPropertyInventoryCategoryDlg(this, 2));
        }

        private void BtnAcNo3_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPropertyInventoryCategoryDlg(this, 3));
        }

        private void ChkRentCategoryInd_CheckedChanged(object sender, EventArgs e)
        {

            if (ChkRentCategoryInd.Checked)
            {
                if (ChkSalesCategoryInd.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "You cannot tick Rent Category and Sales Category at the same time.");
                    ChkRentCategoryInd.Checked = false;
                }
            }
        }

        private void ChkSalesCategoryInd_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkSalesCategoryInd.Checked)
            {
                if (ChkRentCategoryInd.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "You cannot tick Rent Category and Sales Category at the same time.");
                    ChkSalesCategoryInd.Checked = false;
                }
            }
        }


        #endregion

        #endregion
    }
}
