﻿#region Update
/*
    10/09/2017 [TKG] New application
    27/09/2018 [TKG] tambah currency, region per ad
    23/07/2020 [WED/SIER] tambah ActInd, berdasarkan parameter IsPositionStatusUseActInd
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPositionStatus : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mRegionCode = string.Empty;
        internal FrmPositionStatusFind FrmFind;
        internal bool mIsPositionStatusUseActInd = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPositionStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Position Status";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (!mIsPositionStatusUseActInd) ChkActInd.Visible = false;
                BtnPrint.Visible = false;
                SetGrd();
                Sl.SetLueRegionCode(ref LueRegionCode);
                Sl.SetLueCurCode2(ref LueCurCode);
                DteStartDt.Visible = false;
                DteEndDt.Visible = false;
                LueCurCode.Visible = false;
                LueRegionCode.Visible = false;
                SetFormControl(mState.View);
                if (mRegionCode.Length != 0)
                {
                    ShowData(mRegionCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Allowance/Deduction"+Environment.NewLine+"Code",
                        "Allowance/Deduction"+Environment.NewLine+"Name",
                        "",
                        "Start Date",
                        "End Date",

                        //6-9
                        "Currency",
                        "Amount",
                        "Region Code",
                        "Region"
                    },
                     new int[] 
                    {
                        20, 
                        120, 250, 20, 100, 100, 
                        80, 130, 0, 200
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 8 });
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPositionStatusCode, TxtPositionStatusName, TxtDisplayName, LueRegionCode, MeeRemark, ChkActInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 4, 5, 6, 7, 9 });
                    TxtPositionStatusCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPositionStatusCode, TxtPositionStatusName, TxtDisplayName, LueRegionCode, MeeRemark
                    }, false);
                    ChkActInd.Checked = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 4, 5, 6, 7, 9 });
                    TxtPositionStatusCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPositionStatusName, TxtDisplayName, LueRegionCode, MeeRemark
                    }, false);
                    if (mIsPositionStatusUseActInd) ChkActInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 4, 5, 6, 7, 9 });
                    TxtPositionStatusName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtPositionStatusCode, TxtPositionStatusName, TxtDisplayName, MeeRemark,
                LueCurCode, LueRegionCode 
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPositionStatusFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPositionStatusCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                var cml = new List<MySqlCommand>();
                cml.Add(SavePositionStatus());
                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
                            cml.Add(SavePositionStatusAllowanceDeduction(Row));
                }
                Sm.ExecCommands(cml);
                ShowData(TxtPositionStatusCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPositionStatusDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 4, 5, 6, 9 }, e.ColIndex))
                {
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd1, DteEndDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 6) LueRequestEdit(ref Grd1, ref LueCurCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 9) LueRequestEdit(ref Grd1, ref LueRegionCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7 });
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 7 }, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmPositionStatusDlg(this));
            
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmAllowanceDeduction(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mADCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                if (Sm.GetGrdDate(Grd1, 0, 4).Length != 0)
                {
                    var StartDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 4));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 4].Value = StartDt;
                }
            }
            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdDate(Grd1, 0, 5).Length != 0)
                {
                    var EndDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 5));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 5].Value = EndDt;
                }
            }
            if (e.ColIndex == 6)
            {
                if (Sm.GetGrdStr(Grd1, 0, 6).Length != 0)
                {
                    var CurCode = Sm.GetGrdStr(Grd1, 0, 6);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 6].Value = CurCode;
                }
            }
            if (e.ColIndex == 9)
            {
                if (Sm.GetGrdStr(Grd1, 0, 9).Length != 0)
                {
                    string 
                        RegionCode = Sm.GetGrdStr(Grd1, 0, 8),
                        RegionName = Sm.GetGrdStr(Grd1, 0, 9)
                        ;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        {
                            Grd1.Cells[Row, 8].Value = RegionCode;
                            Grd1.Cells[Row, 9].Value = RegionName;
                        }
                }
            }

        }

        #endregion

        #region Show Data

        public void ShowData(string PositionStatusCode)
        {
            try
            {
                ClearData();
                ShowPositionStatus(PositionStatusCode);
                ShowPositionStatusAllowanceDeduction(PositionStatusCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPositionStatus(string PositionStatusCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", PositionStatusCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select PositionStatusCode, PositionStatusName, DisplayName, Remark, ActInd " +
                  "From TblPositionStatus Where PositionStatusCode=@PositionStatusCode; ",
                 new string[] 
                   {
                      //0
                      "PositionStatusCode",

                      //1-4
                      "PositionStatusName", "DisplayName", "Remark", "ActInd"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtPositionStatusCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtPositionStatusName.EditValue = Sm.DrStr(dr, c[1]);
                     TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                     ChkActInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                 }, true
             );
        }

        private void ShowPositionStatusAllowanceDeduction(string PositionStatusCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ADCode, B.ADName, A.StartDt, A.EndDt, A.CurCode, A.Amt, A.RegionCode, C.RegionName ");
            SQL.AppendLine("From TblPositionstatusAllowanceDeduction A  ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Left Join TblRegion C On A.RegionCode=C.RegionCode ");
            SQL.AppendLine("Where A.PositionStatusCode=@PositionStatusCode Order By B.ADName;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@PositionStatusCode", PositionStatusCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "ADCode",

                           //1-5
                           "ADName", "StartDt", "EndDt", "CurCode", "Amt",

                           //6-7
                           "RegionCode", "RegionName"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count-1, new int[] { 7 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPositionStatusCode, "Position status code", false) ||
                Sm.IsTxtEmpty(TxtPositionStatusName, "Position status name", false) ||
                IsDataInactive() ||
                IsCodeExisted() ||
                IsGrdValueNotValid();
        }

        private bool IsDataInactive()
        {
            if (!mIsPositionStatusUseActInd) return false;
            if (!TxtPositionStatusCode.Properties.ReadOnly) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblPositionStatus ");
            SQL.AppendLine("Where PositionStatusCode = @Param ");
            SQL.AppendLine("And ActInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPositionStatusCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already inactive.");
                ChkActInd.Focus();
                return true;
            }

            return false;
        }

        private bool IsCodeExisted()
        { 
            if (TxtPositionStatusCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select 1 From TblPositionStatus Where PositionStatusCode=@Param",
                TxtPositionStatusCode.Text,
                "This position status code already existed.");
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                string StartDt = string.Empty, EndDt = string.Empty, StartDt2 = string.Empty, EndDt2 = string.Empty;

                for (int r=0; r<Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Allowance/Deduction is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 4, false,
                        "Allowance/Deduction : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                        "Started date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 5, false,
                        "Allowance/Deduction : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                        "End date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 6, false, "Currency is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 7, true, "Amount should not be zero.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 9, false, "Region is empty.")) return true;

                    StartDt = Sm.GetGrdDate(Grd1, r, 4).Substring(0, 8);
                    EndDt = Sm.GetGrdDate(Grd1, r, 5).Substring(0, 8);

                    if (Sm.CompareDtTm(EndDt, StartDt) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Allowance/Deduction : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Region : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                            "End date should not be earlier than start date.");
                        return true;
                    }

                    for (int j = r; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (r != j)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), Sm.GetGrdStr(Grd1, j, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), Sm.GetGrdStr(Grd1, j, 8)))
                            {
                                StartDt2 = Sm.GetGrdDate(Grd1, j, 4);
                                EndDt2 = Sm.GetGrdDate(Grd1, j, 5);

                                if (StartDt2.Length > 0 && EndDt2.Length > 0)
                                {
                                    StartDt2 = StartDt2.Substring(0, 8);
                                    EndDt2 = EndDt2.Substring(0, 8);
                                    if (
                                        (Sm.CompareDtTm(StartDt2, StartDt) <= 0 && Sm.CompareDtTm(StartDt, EndDt2) <= 0) ||
                                        (Sm.CompareDtTm(StartDt, StartDt2) <= 0 && Sm.CompareDtTm(StartDt2, EndDt) <= 0)
                                        )
                                    {
                                        Sm.StdMsg(mMsgType.Warning,
                                            "Allowance/Deduction : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                                            "Region : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                                            "Invalid period.");
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SavePositionStatus()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPositionStatus ");
            SQL.AppendLine("(PositionStatusCode, PositionStatusName, DisplayName, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PositionStatusCode, @PositionStatusName, @DisplayName, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    ActInd = @ActInd, PositionStatusName=@PositionStatusName, DisplayName=@DisplayName, Remark=@Remark, "); 
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblPositionStatusAllowanceDeduction Where PositionStatusCode=@PositionStatusCode;");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", TxtPositionStatusCode.Text);
            Sm.CmParam<String>(ref cm, "@PositionStatusName", TxtPositionStatusName.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePositionStatusAllowanceDeduction(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPositionStatusAllowanceDeduction(PositionStatusCode, ADCode, StartDt, EndDt, CurCode, Amt, RegionCode, CreateBy, CreateDt) " +
                    "Values (@PositionStatusCode, @ADCode, @StartDt, @EndDt, @CurCode, @Amt, @RegionCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", TxtPositionStatusCode.Text);
            Sm.CmParam<String>(ref cm, "ADCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParamDt(ref cm, "StartDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "EndDt", Sm.GetGrdDate(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "CurCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "RegionCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsPositionStatusUseActInd = Sm.GetParameterBoo("IsPositionStatusUseActInd");
        }

        private void LueRequestEdit(
           ref iGrid Grd, ref LookUpEdit Lue,
           ref iGCell fCell, ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPositionStatusCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPositionStatusCode);
        }

        private void TxtPositionStatusName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPositionStatusName);
        }

        private void TxtDisplayName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtDisplayName);
        }

        private void LueRegionCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueRegionCode, new Sm.RefreshLue1(Sl.SetLueRegionCode));
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueCurCode);
                LueCurCode.Visible = false;
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode2));
        }

        private void LueRegionCode_Leave(object sender, EventArgs e)
        {
            if (LueRegionCode.Visible && fAccept)
            {
                if (Sm.GetLue(LueRegionCode).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = null;
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueRegionCode);
                    Grd1.Cells[fCell.RowIndex, 9].Value = LueRegionCode.GetColumnValue("Col2");
                }
                LueRegionCode.Visible = false;
            }
        }

        private void LueRegionCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #endregion
    }
}
