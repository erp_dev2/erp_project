﻿#region Update
/*
    16/10/2019 [WED/IMS] filter COA alias
    27/02/2020 [WED/KBN] COA bisa dipilih berkali kali berdasarkan parameter IsJournalUseDuplicateCOA
    19/05/2020 [DITA/YK] Filter COA berdasarkan group --> param = IsCOAFilteredByGroup
    15/10/2021 [MYA/ALL] Memasang Parameter agar Journal Transaction tidak bisa menarik akun-akun KAS/BANK
    29/07/2022 [IBL/PHT] Bug: GetParameter() blm dipanggil di FrmLoad
    02/08/2022 [IBL/PHT] Bug: data tidak muncul ketika pake COA Group
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmJournalDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmJournal mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsCOAFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmJournalDlg(FrmJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            SetGrd();
            SetSQL();
            SetLueAcCode(ref LueAcCode);
        }

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Account#", 
                    "Alias",
                    "Account"+Environment.NewLine+"Description", 
                    "Account"+Environment.NewLine+"Type"
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5 });
            Grd1.Cols[3].Visible = mFrmParent.mIsCOAUseAlias;
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.Alias, A.AcDesc, B.OptDesc As AcType ");
            SQL.AppendLine("From TblCoa A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("where A.AcNo Not In (Select parent From TblCoa Where parent is not null) ");
            SQL.AppendLine("And A.ActInd='Y' ");
            if (mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = A.AcNo ");
                SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }
            if (mFrmParent.mJournalCashBankAcNo.Length > 0)
            {
                string[] AcNo = {};
                AcNo = mFrmParent.mJournalCashBankAcNo.Split(',');

                for (int i = 0; i < AcNo.Length; i++)
                {
                    SQL.AppendLine("    AND A.AcNo NOT IN ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT DISTINCT(AcNo) FROM TblCoa ");
                    SQL.AppendLine("        WHERE AcNo LIKE '" + AcNo[i].Trim() + "%' ");
                    SQL.AppendLine("    ) ");
                }
            }
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.Parent Like '" + Sm.GetLue(LueAcCode) + "%'  ";
                if (!mFrmParent.mIsJournalUseDuplicateCOA)
                    Filter += " And A.AcNo Not In (" + mFrmParent.GetSelectedJournal() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.AcDesc" });
                Sm.FilterStr(ref Filter, ref cm, TxtAlias.Text, "A.Alias", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By A.AcNo, A.AcDesc; ",
                    new string[] 
                    { 
                        //0
                        "AcNo",

                        //1-3
                        "Alias", "AcDesc", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                     }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        mFrmParent.Grd1.Cells[Row1, 6].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 7].Value = 0;
                       
                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 6].Value = 0;
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 7].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.mIsJournalUseDuplicateCOA) return false;

            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        private void SetLueAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo As Col1, A.AcDesc As Col2 ");
            SQL.AppendLine("From TblCOA A Where A.Level = 1  ");

            if (mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = A.AcNo ");
                SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
             ref Lue, ref cm,
             0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void GetParameter()
        {
            mIsCOAFilteredByGroup = Sm.GetParameterBoo("IsCOAFilteredByGroup");
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void ChkAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account's Group");
        }

        private void LueAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCode, new Sm.RefreshLue1(SetLueAcCode));
        }

        private void TxtAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Alias");
        }

        #endregion
      
        #endregion

    }
}
