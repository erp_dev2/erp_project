﻿#region Update
/*
   03/10/2022 [HAR/GSS] Menu baru 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmImportData3 : RunSystem.FrmBase12
    {
        #region Field, Property

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mDocTitle = string.Empty,
            mCustomerAcNoDownPayment = string.Empty,
            mCustomerAcDescDownPayment = string.Empty,
            mAcNoForGiroAR = string.Empty,
            mAcDescForGiroAR = string.Empty,
            mCustomerAcNoAR = string.Empty,
            mCustomerAcNoNonInvoice = string.Empty,
            mCustomerAcDescAR = string.Empty,
            mCustomerAcDescNonInvoice = string.Empty,
            mEmployeeAcNo = string.Empty,
            mEmployeeAcDesc = string.Empty,
            mAcNoForAdvancePayment = string.Empty,
            mAcDescForAdvancePayment = string.Empty,
            mAcNoForForeignExchange = string.Empty,
            mMainCurCode = string.Empty,
            mDocType = "17",
            mVendorAcNoDownPayment = string.Empty,
            mAcNoForInitialStockOpeningBalance = string.Empty,
            mEntCode = string.Empty,
            mDeptCodeForImportPI = string.Empty,
            mSiteCodeForImportPI = string.Empty,
            mCurCodeForImportPI = string.Empty,
            mWhsCodeForImportPI = string.Empty,
            mPTCodeForImportPI = string.Empty,
            mItCodeForImportPI = string.Empty,
            mCurCodeForImportSLI = string.Empty,
            mWhsCodeForImportSLI = string.Empty,
            mItCodeForImportSLI = string.Empty,
            mDocNoFormat = string.Empty,
            mGenerateEmpCodeFormat = string.Empty,
            mItCodeSeqNo = string.Empty,
            mItemPriceDNoLength = string.Empty
            ;
        internal string
            mGenerateCustomerCOAFormat = string.Empty,
            mItCodeFormat = string.Empty,
            mLengthAssetCode = "12";
        internal bool
            mIsEmpSSBasedOnEntity = false,
            mIsEmployeeManagementRepresentative = false,
            mIsEmpContractDocExtValidationEnabled = false;

        private bool
          mIsEmployeeCOAInfoMandatory = false,
          mIsPPSActive = false,
          mIsAnnualLeaveUseStartDt = false,
          mIsEmployeeAutoCreatePPS = false,
          mIsAutoJournalActived = false,
          mIsStockOpnameShowWarningIfChanged = false,
          mIsBatchNoUseDocDtIfEmpty = false,
          mIsEmpCodeUseContractDt = false,
          mIsItCodeUseItSeqNo = false,
          mIsAssetCodeBasedOnCategoryEnabled = false,
          mIsEmpContractDtMandatory = false,
          mIsVdCodeUseVdCt = false,
          mIsCustomerCOAUseCustomerCategory = false,
          mIsBudgetCategoryUseCostCategory = false,
          mIsBudgetCategoryUseCostCenter = false,
          mIsBudgetCategoryUseCOA = false,
          mIsAssetShowAdditionalInformation = false;

        #endregion

        #region Constructor

        public FrmImportData3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                string validuser = Sm.GetValue("Select GrpCode from tbluser Where Usercode = '"+Gv.CurrentUserCode+"'");
                if (validuser == "SysAdm")
                {
                    mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                    SetLueImportCode(ref LueImportCode);
                    GetParameter();
                    Sm.SetControlReadOnly(new List<BaseEdit> { TxtFileName }, true);
                    LueImportCode.Focus();
                    base.FrmLoad(sender, e);
                }
                else
                {
                    MessageBox.Show("Under Construction");
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit>
            {
               TxtFileName, LueImportCode
            });
            LueImportCode.Focus();
            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);

        }

        #endregion

        #region Button Methods

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueImportCode, "Import") ||
                Sm.IsTxtEmpty(TxtFileName, "File (CSV)", false) ||
                Sm.StdMsgYN("Process", "") == DialogResult.No)
                return;

            try
            {
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Process Data

        private void ProcessData()
        {
            GetCSVData(TxtFileName.Text, Sm.GetLue(LueImportCode));
        }

        private void ProcessCOA(ref List<COA> lCOA)
        {
            if (IsInsertedCOAInvalid(ref lCOA)) { ClearData(); return; }

            try
            {
                lCOA.ForEach(i =>
                {
                    InsertCOA(i);
                });
                Sm.StdMsg(mMsgType.Info, lCOA.Count() + " COA data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                lCOA.Clear();
            }
        }

        private void ProcessEmpSalary(ref List<EmpSalary> lEmpSalary)
        {
            if (IsInsertedEmpSalaryInvalid(ref lEmpSalary)) { ClearData(); return; }

            try
            {
                var lEmpSalaryExt = new List<EmpSalaryExt>();
                PrepareEmpSalaryExists(ref lEmpSalaryExt);
                AssignDNo(ref lEmpSalary, ref lEmpSalaryExt);
                lEmpSalary.ForEach(i =>
                {
                    InsertEmpSalary(i);
                });
                Sm.StdMsg(mMsgType.Info, lEmpSalary.Count() + " Employee salary data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                lEmpSalary.Clear();
            }
        }

        private void ProcessEmployeeSalarySS(ref List<EmployeeSalarySS> l)
        {
            var l2 = new List<EmployeeSalarySS2>();
            PrepareEmployeeSalarySS(ref l2);

            if (IsInsertedEmployeeSalarySSInvalid(ref l, ref l2))
            {
                ClearData();
                return;
            }

            try
            {
                l.ForEach(i =>
                {
                    InsertEmployeeSalarySS(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Employee salary social security data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
                l2.Clear();
            }
        }

        private void ProcessEmployeeAllowanceDeduction(ref List<EmployeeAllowanceDeduction> l)
        {
            var l2 = new List<EmployeeAllowanceDeduction2>();
            PrepareEmployeeAllowanceDeduction(ref l, ref l2);

            if (IsInsertedEmployeeAllowanceDeductionInvalid(ref l, ref l2))
            {
                ClearData();
                return;
            }

            try
            {
                l.ForEach(i =>
                {
                    InsertEmployeeAllowanceDeduction(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Employee allowance/deduction data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
                l2.Clear();
            }
        }

        private void ProcessAsset(ref List<Asset> l)
        {
            if (IsInsertedAssetInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertAsset(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Asset data has been inserted (Master Asset & Asset Summary).");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessVendor(ref List<Vendor> l)
        {
            if (IsInsertedVendorInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertVendor(i);
                });

                Sm.StdMsg(mMsgType.Info, l.Count() + " Vendor data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessCustomer(ref List<Customer> l)
        {
            if (IsInsertedCustomerInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertCustomer(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Customer data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            if (IsInsertedEmployeeInvalid(ref l)) { ClearData(); return; }

            try
            {
                var IsAutoCreatePPS = true;
                if (!mIsEmployeeAutoCreatePPS)
                {
                    if (Sm.StdMsgYN("Question", "Do you want to create PPS document for this new employee ?") == DialogResult.No)
                        IsAutoCreatePPS = false;
                }
                l.ForEach(i =>
                {
                    InsertEmployee(i, IsAutoCreatePPS);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Employee data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessEmployeeSS(ref List<EmployeeSS> l, ref List<EmployeeSSHdr> l2, ref List<EmployeeSSDtl> l3, ref List<EmployeeSSDtl2> l4)
        {
            if (IsInsertedEmployeeSSInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();

                if (mIsEmpSSBasedOnEntity)
                {
                    l2.ForEach(i =>
                    {
                        cml.Add(InsertEmployeeSSHdr(i));
                    });
                }
                l3.ForEach(i =>
                {
                    cml.Add(InsertEmployeeSSDtl(i));
                });
                l4.ForEach(i =>
                {
                    cml.Add(InsertEmployeeSSDtl2(i));
                });

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l.Count() + " Employee Social Security data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
        }

        private void ProcessItem(ref List<Item> l)
        {
            if (IsInsertedItemInvalid(ref l))
            {
                l.Clear();
                return;
            }

            try
            {
                l.ForEach(i => { InsertItem(i); });
                Sm.StdMsg(mMsgType.Info, l.Count() + " data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessItemKSM(ref List<ItemKSM> l)
        {
            if (IsInsertedItemInvalid(ref l)) return;

            try
            {
                InsertItem(l);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessCustomerDeposit(ref List<CustomerDeposit> l)
        {
            try
            {
                var cml = new List<MySqlCommand>();
                l.ForEach(i =>
                {
                    var lR = new List<Rate>();
                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();
                    decimal DP = 0m;

                    SQL.AppendLine("Select CtCode, CurCode, ExcRate, Amt ");
                    SQL.AppendLine("From TblCustomerDepositSummary2 ");
                    SQL.AppendLine("Where CtCode = @CtCode ");
                    SQL.AppendLine("And CurCode = @CurCode1 ");
                    SQL.AppendLine("And Amt > 0 Order By CreateDt Asc; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@CtCode", i.CtCode);
                        Sm.CmParam<String>(ref cm, "@CurCode1", i.CurCode1);
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CtCode",

                         //1-3
                         "CurCode",
                         "ExcRate",
                         "Amt"
                        });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lR.Add(new Rate()
                                {
                                    CtCode = Sm.DrStr(dr, c[0]),
                                    CurCode = Sm.DrStr(dr, c[1]),
                                    ExcRate = Sm.DrDec(dr, c[2]),
                                    Amt = Sm.DrDec(dr, c[3])
                                });
                            }
                        }
                        dr.Close();
                    }

                    if (lR.Count > 0)
                    {
                        DP = i.Amt1;
                        for (int j = 0; j < lR.Count; j++)
                        {
                            if (DP > 0)
                            {
                                cml.Add(ReduceAmtSummaryCt2(ref lR, j, DP));
                                DP = DP - lR[j].Amt;
                            }
                        }

                        // save ke tabel summary2 untuk nilai switch nya

                        if (i.CurCode1 != mMainCurCode && i.CurCode2 != mMainCurCode) // switch from non-MainCurCode to non-MainCurCode
                        {
                            decimal rate = 0m;
                            for (int x = 0; x < lR.Count; x++)
                            {
                                rate += lR[x].ExcRate / i.Amt1;
                            }

                            rate = rate / lR.Count;

                            cml.Add(InsertExcRateOtherMainCurCode(i, rate));
                        }
                        else // swicth from MainCurCode or to MainCurCode
                        {
                            cml.Add(InsertExcRateMainCurCode(i));
                        }

                    }
                    else
                    {
                        // 07/06/2018 [TKG] untuk IDR to Non IDR
                        if (!Sm.CompareStr(i.CurCode2, mMainCurCode))
                            cml.Add(InsertCustomerDepositSummary2(i));
                    }

                    cml.Add(InsertCustomerDepositCurSwitch(i));

                    if (mIsAutoJournalActived)
                    {
                        decimal Amt1 = 0m, Amt2 = 0m;
                        PrepareJournal(i, ref Amt1, ref Amt2);
                        if (Amt1 != Amt2)
                        {
                            if (Amt1 < Amt2)
                                cml.Add(InsertJournal(i, 1, Amt2 - Amt1));
                            else
                                cml.Add(InsertJournal(i, 2, Amt1 - Amt2));
                        }
                    }


                });
                Sm.ExecCommands(cml);
                Sm.StdMsg(mMsgType.Info, l.Count() + " Customer Deposit data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessStockOpname(ref List<StockOpname> l, ref List<StockOpnameHdr> l2, ref List<StockOpnameDtl> l3)
        {
            if (IsInsertedStockOpnameInvalid(ref l)) { ClearData(); return; }

            try
            {
                ReComputeStock(ref l3);

                var cml = new List<MySqlCommand>();

                l.ForEach(i =>
                {
                    mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + i.WhsCode + "' limit 1;");


                });
                l2.ForEach(i =>
                {
                    cml.Add(InsertStockOpnameHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(InsertStockOpnameDtl(i));
                });

                l2.ForEach(i =>
                {
                    if (!IsApprovalExist(i))
                    {
                        cml.Add(InsertStockMovement(i));
                        cml.Add(InsertStockSummary(i));
                        if (mIsAutoJournalActived) cml.Add(InsertJournal(i));
                    }
                });

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l2.Count() + " Stock Opname data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear();
            }
        }

        private void ProcessVendorDeposit(ref List<VendorDeposit> l)
        {
            try
            {
                l.ForEach(i =>
                {
                    InsertVendorDeposit(i);
                });

                Sm.StdMsg(mMsgType.Info, l.Count() + " Vendor Deposit data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessStockInitial(ref List<StockInitial> l, ref List<StockInitialHdr> l2, ref List<StockInitialDtl> l3)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                mEntCode = Sm.GetValue("Select C.EntCode " +
                              "From TblWarehouse A " +
                              "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                              "Inner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                              "Where A.WhsCode = '" + l[i].WhsCode + "' limit 1");
            }
            if (IsInsertedStockInitialInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();

                l2.ForEach(i =>
                {
                    cml.Add(InsertStockInitialHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(InsertStockInitialDtl(i));
                });
                l2.ForEach(i =>
                {
                    cml.Add(SaveStock(i));

                    if (mIsAutoJournalActived)
                        cml.Add(SaveJournalStockInitial(i));
                });

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l2.Count() + " Stock Intial data has been inserted.");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear();
            }
        }

        private void ProcessCostCategory(ref List<CostCategory> l)
        {
            if (IsInsertedCostCategoryInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i => { InsertCostCategory(i); });
                Sm.StdMsg(mMsgType.Info, l.Count() + " cost category data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessItemLocal(ref List<ItemLocal> l)
        {
            if (IsInsertedItemLocalInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i => { UpdateItemLocal(i); });
                Sm.StdMsg(mMsgType.Info, l.Count() + "Item local data has been updated.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessEmpContractDoc(ref List<EmpContractDoc> l)
        {
            if (IsInsertedEmpContractDocInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertEmpContractDoc(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + "Employee Contract Document data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessMRToPI(ref List<MRToPI> l)
        {
            if (IsInsertedMRToPIInvalid(ref l)) { ClearData(); return; }

            try
            {
                int mIndex = 1;
                l.ForEach(i =>
                {
                    mEntCode = Sm.GetValue("Select C.EntCode From TblWarehouse A Inner Join TblCostCenter B ON A.CCCode = B.CCCode And A.WhsCode = @Param Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode; ", mWhsCodeForImportPI);

                    i.MRDocNo = GenerateDocNo(mIndex, i.DocDt, "MaterialRequest", "TblMaterialRequestHdr");
                    i.PORDocNo = GenerateDocNo(mIndex, i.DocDt, "PORequest", "TblPORequestHdr");
                    i.QtDocNo = GenerateDocNo(mIndex, i.DocDt, "Qt", "TblQtHdr");
                    i.PODocNo = GenerateDocNo(mIndex, i.DocDt, "PO", "TblPOHdr");

                    if (mDocNoFormat == "1")
                        i.RecvVdDocNo = GenerateDocNo(mIndex, i.DocDt, "RecvVd2", "TblRecvVdHdr");
                    else
                        i.RecvVdDocNo = Sm.GenerateDocNo3(i.DocDt, "RecvVd2", "TblRecvVdHdr", mEntCode, mIndex.ToString());

                    if (mDocNoFormat == "1")
                        i.PIDocNo = GenerateDocNo(mIndex, i.DocDt, "PurchaseInvoice", "TblPurchaseInvoiceHdr");
                    else
                        i.PIDocNo = Sm.GenerateDocNo3(i.DocDt, "PurchaseInvoice", "TblPurchaseInvoiceHdr", mEntCode, mIndex.ToString());

                    InsertMRToPI(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " document data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessDOCtToSLI(ref List<DOCtToSLI> l)
        {
            if (IsInsertedDOCtToSLIInvalid(ref l)) { ClearData(); return; }

            try
            {
                int mIndex = 1;
                l.ForEach(i =>
                {
                    mEntCode = Sm.GetValue("Select C.EntCode From TblWarehouse A Inner Join TblCostCenter B ON A.CCCode = B.CCCode And A.WhsCode = @Param Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode; ", mWhsCodeForImportSLI);

                    if (mDocNoFormat == "1")
                        i.DOCtDocNo = GenerateDocNo(mIndex, i.DocDt, "DOCt", "TblDOCtHdr");
                    else
                        i.DOCtDocNo = Sm.GenerateDocNo3(i.DocDt, "DOCt", "TblDOCtHdr", mEntCode, mIndex.ToString());

                    if (mDocNoFormat == "1")
                        i.SLIDocNo = GenerateDocNo(mIndex, i.DocDt, "SalesInvoice", "TblSalesInvoiceHdr");
                    else
                        i.SLIDocNo = Sm.GenerateDocNo3(i.DocDt, "SalesInvoice", "TblSalesInvoiceHdr", mEntCode, mIndex.ToString());

                    InsertDOCtToSLI(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " document data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessJournalTransaction(ref List<Journal> l, ref List<JournalHdr> l2, ref List<JournalDtl> l3)
        {
            if (IsJournalTransactionInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();
                l2.ForEach(i =>
                {
                    cml.Add(InsertJournalHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(InsertJournalDtl(i));
                });

                Sm.ExecCommands(cml);
                Sm.StdMsg(mMsgType.Info, l.Count() + " Journal Transaction data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear();
            }
        }

        private void ProcessJournal(ref List<Journal> l, string MthYr)
        {
            //Harus dalam 1 bulan yg sama
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            decimal i = 0m;
            var Remark = string.Empty;
            int DNo = 0;
            int DocNoLength = 0;
            var JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo");
            if (JournalDocSeqNo.Length == 0)
                DocNoLength = 4;
            else
                DocNoLength = int.Parse(JournalDocSeqNo);

            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal' And DocAbbr Is Not Null; ");
            var j = Sm.GetValueDec(
                "Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNoTemp " +
                "From TblJournalHdr " +
                "Where Right(DocNo, 5)=@Param " +
                "Order By DocNo Desc Limit 1;", MthYr
                );
            var DocNo = string.Concat("/", mDocTitle, "/", DocAbbr, "/", MthYr);

            try
            {
                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
                SQL.AppendLine("Set @MenuDesc:=(Select MenuDesc From TblMenu Where MenuCode=@MenuCode); ");

                foreach (var x in l.OrderBy(o1 => o1.DocDt).ThenBy(o2 => o2.Remark).ThenByDescending(o3 => o3.JnDesc))
                {
                    if (Sm.CompareStr(Remark, x.Remark))
                        DNo += 1;
                    else
                    {
                        DNo = 1;
                        j++;
                        Remark = x.Remark;

                        //SQL.Append("Set @DocNo" + j.ToString() + ":=Concat( ");

                        //SQL.Append("IfNull(( ");
                        //SQL.Append("   Select Right(Concat( ");
                        //SQL.Append("   Repeat('0', " + JournalDocSeqNo + "), ");
                        //SQL.Append("   Convert(DocNoTemp+" + j.ToString() + ", Char) ");
                        //SQL.Append("   ), " + JournalDocSeqNo + ") ");
                        //SQL.Append("   From ( ");
                        //SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNoTemp ");
                        //SQL.Append("       From TblJournalHdr ");
                        //SQL.Append("       Where Right(DocNo, 5)='" + MthYr + "'");
                        //SQL.Append("       Order By DocNo Desc ");
                        //SQL.Append("       Limit 1 ");
                        //SQL.Append("    ) As Temp ");
                        //SQL.Append("   ), ");
                        //SQL.Append("   Right(Concat( ");
                        //SQL.Append("        Repeat('0', " + JournalDocSeqNo + "), '" + j.ToString() + "'), " + JournalDocSeqNo + ")) ");

                        //SQL.Append(", '/', @DocTitle, '/', @DocAbbr, '/', '" + MthYr + "'");
                        //SQL.Append("); ");

                        Sm.CmParam<String>(ref cm, "@DocNo" + j.ToString(),
                            string.Concat(Sm.Right(string.Concat(new String('0', DocNoLength), j.ToString()), DocNoLength),
                            DocNo));

                        SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, CancelInd, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values(@DocNo" + j.ToString() + ", @DocDt" + i.ToString() + ", 'N', @JnDesc" + i.ToString() + ", @MainCurCode, @MenuCode, @MenuDesc, @CCCode" + i.ToString() + ", @Remark" + i.ToString() + ", @UserCode, @Dt); ");
                    }

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo" + j.ToString() + ", @DNo" + i.ToString() + ", @AcNo" + i.ToString() + ", @DAmt" + i.ToString() + ", @CAmt" + i.ToString() + ", @RemarkD" + i.ToString() + ", @UserCode, @Dt); ");

                    Sm.CmParam<String>(ref cm, "@DNo" + i.ToString(), Sm.Right(string.Concat("00", DNo.ToString()), 3));
                    Sm.CmParam<String>(ref cm, "@RemarkD" + i.ToString(), x.RemarkD);
                    Sm.CmParam<String>(ref cm, "@CCCode" + i.ToString(), x.CCCode);
                    Sm.CmParam<String>(ref cm, "@Remark" + i.ToString(), x.Remark);
                    Sm.CmParam<String>(ref cm, "@DocDt" + i.ToString(), x.DocDt);
                    Sm.CmParam<String>(ref cm, "@JnDesc" + i.ToString(), x.JnDesc);
                    Sm.CmParam<String>(ref cm, "@AcNo" + i.ToString(), x.AcNo);
                    Sm.CmParam<Decimal>(ref cm, "@DAmt" + i.ToString(), x.DAmt);
                    Sm.CmParam<Decimal>(ref cm, "@CAmt" + i.ToString(), x.CAmt);

                    i++;
                }

                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                cm.CommandText = SQL.ToString();
                cml.Add(cm);
                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l.Count() + " journal data has been processed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessItemCostCategory(ref List<ItemCostCategory> l)
        {
            if (IsInsertedItemCostCategoryInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertItemCostCategory(i);
                });

                Sm.StdMsg(mMsgType.Info, l.Count() + " Item Cost Category data has been inserted.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessBudgetCategory(ref List<BudgetCategory> l)
        {
            if (IsInsertedBudgetCategoryInvalid(ref l)) { ClearData(); return; }

            try
            {
                l.ForEach(i =>
                {
                    InsertBudgetCategory(i);
                });
                Sm.StdMsg(mMsgType.Info, l.Count() + " Budget Category data has been inserted.");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessItemPrice(ref List<ItemPrice> l, ref List<ItemPriceHdr> l2, ref List<ItemPriceDtl> l3)
        {
            if (IsInsertedItemPriceInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();

                cml.Add(InsertItemPriceHdr(l2));
                cml.Add(InsertItemPriceDtl(l3));

                //l2.ForEach(i =>
                //{
                //    cml.Add(InsertItemPriceHdr(i));
                //});
                //l3.ForEach(i =>
                //{
                //    cml.Add(InsertItemPriceDtl(i));
                //});

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l2.Count() + " Item's Price data has been inserted.");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear();
            }
        }

        private void ProcessDOCt(ref List<DOToCustomer> l, ref List<DOToCustomerHdr> l2, ref List<DOToCustomerDtl> l3)
        {
            if (IsInsertedDOCtInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();

                l2.ForEach(i =>
                {
                    cml.Add(InsertDOCtHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(InsertDOCtDtl(i));
                });


                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l2.Count() + " DO To Customer data has been inserted.");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear(); l2.Clear(); l3.Clear();
            }
        }

        private void ProcessPersonalInformation(ref List<PersonalInformation> l, ref List<PersonalInformationHdr> lh,
            ref List<PersonalInformationDtl2> l2, ref List<PersonalInformationDtl3> l3, ref List<PersonalInformationDtl4> l4, ref List<PersonalInformationDtl5> l5,
            ref List<PersonalInformationDtl6> l6, ref List<PersonalInformationDtl7> l7, ref List<PersonalInformationDtl8> l8
            )
        {
            if (IsInsertedPersonalInformationInvalid(ref l, ref l2, ref l3, ref l4, ref l6))
            {
                ClearData(); return;
            }

            try
            {
                var cml = new List<MySqlCommand>();

                cml.Add(SaveEmployeeRecruitment(lh));
                if (l8.Count() > 0)
                    cml.Add(UpdateEmployeeReq(l8));
                if (l2.Count() > 0)
                    cml.Add(SaveFamilyEmployeeRecruitment(l2));
                if (l3.Count() > 0)
                    cml.Add(SaveEmployeeWorkExpRecruitment(l3));
                if (l4.Count() > 0)
                    cml.Add(SaveEmployeeEducationRecruitment(l4));
                if (l5.Count() > 0)
                    cml.Add(SaveEmployeeCompetenceRecruitment(l5));
                if (l6.Count() > 0)
                    cml.Add(SaveEmployeeTrainingRecruitment(l6));
                if (l7.Count() > 0)
                    cml.Add(SaveEmployeePersonalReferenceRecruitment(l7));

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l2.Count() + " PersonalInformation data has been inserted.");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
                lh.Clear();
                l2.Clear();
                l3.Clear();
                l4.Clear();
                l5.Clear();
                l6.Clear();
                l7.Clear();
            }
        }


        private void ProcessSahamMovement(ref List<SahamMovement> l)
        {
            if (IsInsertedSahamMovementInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();
                string DocDt = Sm.GetValue("SELECT left(CurrentDateTime(), 8)");
                string DocNo = Sm.GenerateDocNo(DocDt, "SahamMovement", "TblSahamMovement");
                int Row = 1;

                l.ForEach(i =>
                {
                    i.DocNo = DocNo;
                    i.DNo = Sm.Right(string.Concat("00000000", Row), 8);
                    cml.Add(InsertSahamMovement(i));
                    cml.Add(InsertInvestor(i));
                    Row += 1;
                });

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, l.Count() + " Saham movement data has been inserted.");

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }

        private void ProcessInvestor(ref List<SahamMovement> l)
        {
            if (IsInsertedSahamMovementInvalid(ref l)) { ClearData(); return; }

            try
            {
                var cml = new List<MySqlCommand>();
                string DocDt = Sm.GetValue("SELECT left(CurrentDateTime(), 8)");
                int Row = 1;

                l.ForEach(i =>
                {
                    if(i.Account.Length>0)
                        cml.Add(InsertInvestor(i));
                    Row += 1;
                });

                Sm.ExecCommands(cml);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                l.Clear();
            }
        }


        #endregion

        #endregion

        #region Additional Method

        private bool IsParameterEmpty(string ImportCode)
        {
            string mParCode = string.Empty;

            if (ImportCode == "ImportMRToPI")
            {
                mParCode = GetEmptyParameter(new string[,]
                {
                    { mDeptCodeForImportPI, "DeptCodeForImportPI" }, { mSiteCodeForImportPI, "SiteCodeForImportPI" },
                    { mCurCodeForImportPI, "CurCodeForImportPI" }, { mWhsCodeForImportPI, "WhsCodeForImportPI" },
                    { mPTCodeForImportPI, "PTCodeForImportPI" }, { mItCodeForImportPI, "ItCodeForImportPI" }
                });
            }

            if (ImportCode == "ImportDOCtToSLI")
            {
                mParCode = GetEmptyParameter(new string[,]
                {
                    { mWhsCodeForImportSLI, "WhsCodeForImportSLI" }, { mCurCodeForImportSLI, "CurCodeForImportSLI" },
                    { mItCodeForImportSLI, "ItCodeForImportSLI" }
                });
            }

            if (mParCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "There is one or more parameter that should be filled, such as : " + mParCode + "." + Environment.NewLine + Environment.NewLine + "Please contact the application admin for support.");
                return true;
            }

            return false;
        }

        private string GetEmptyParameter(string[,] ParCode)
        {
            string mParCode = string.Empty;

            for (int i = 0; i < (ParCode.Length / 2); ++i)
            {
                if (ParCode[i, 0].Length == 0)
                {
                    if (mParCode.Length > 0) mParCode += ", ";
                    mParCode += ParCode[i, 1];
                }
            }

            return mParCode;
        }

        private void SetLueImportCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ImportCode As Col1, ImportDesc As Col2 From TblImportData Where ActInd = 'Y' Order By ImportDesc; ",
                0, 35, false, true, "Code", "Description", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mCustomerAcNoDownPayment = Sm.GetParameter("CustomerAcNoDownPayment");
            mCustomerAcDescDownPayment = Sm.GetParameter("CustomerAcDescDownPayment");
            mAcNoForGiroAR = Sm.GetParameter("AcNoForGiroAR");
            mAcDescForGiroAR = Sm.GetParameter("AcDescForGiroAR");
            mCustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");
            mCustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice");
            mCustomerAcDescAR = Sm.GetParameter("CustomerAcDescAR");
            mCustomerAcDescNonInvoice = Sm.GetParameter("CustomerAcDescNonInvoice");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mIsPPSActive = Sm.GetParameterBoo("IsPPSActive");
            mIsEmployeeCOAInfoMandatory = Sm.GetParameterBoo("IsEmployeeCOAInfoMandatory");
            mEmployeeAcNo = Sm.GetParameter("EmployeeAcNo");
            mEmployeeAcDesc = Sm.GetParameter("EmployeeAcDesc");
            mAcNoForAdvancePayment = Sm.GetParameter("AcNoForAdvancePayment");
            mAcDescForAdvancePayment = Sm.GetParameter("AcDescForAdvancePayment");
            mIsAnnualLeaveUseStartDt = Sm.GetParameterBoo("IsAnnualLeaveUseStartDt");
            mIsEmployeeAutoCreatePPS = Sm.GetParameterBoo("IsEmployeeAutoCreatePPS");
            mIsEmpSSBasedOnEntity = Sm.GetParameterBoo("IsEmpSSBasedOnEntity");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mAcNoForForeignExchange = Sm.GetParameter("AcNoForForeignExchange");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsStockOpnameShowWarningIfChanged = Sm.GetParameter("IsStockOpnameShowWarningIfChanged") == "Y";
            mVendorAcNoDownPayment = Sm.GetParameter("VendorAcNoDownPayment");
            mIsBatchNoUseDocDtIfEmpty = Sm.GetParameterBoo("IsBatchNoUseDocDtIfEmpty");
            mAcNoForInitialStockOpeningBalance = Sm.GetParameter("AcNoForInitialStockOpeningBalance");
            mIsEmpCodeUseContractDt = Sm.GetParameterBoo("IsEmpCodeUseContractDt");
            mIsEmployeeManagementRepresentative = Sm.GetParameterBoo("IsEmployeeManagementRepresentative");
            mIsEmpContractDocExtValidationEnabled = Sm.GetParameterBoo("IsEmpContractDocExtValidationEnabled");
            mDeptCodeForImportPI = Sm.GetParameter("DeptCodeForImportPI");
            mSiteCodeForImportPI = Sm.GetParameter("SiteCodeForImportPI");
            mCurCodeForImportPI = Sm.GetParameter("CurCodeForImportPI");
            mWhsCodeForImportPI = Sm.GetParameter("WhsCodeForImportPI");
            mPTCodeForImportPI = Sm.GetParameter("PTCodeForImportPI");
            mItCodeForImportPI = Sm.GetParameter("ItCodeForImportPI");
            mCurCodeForImportSLI = Sm.GetParameter("CurCodeForImportSLI");
            mWhsCodeForImportSLI = Sm.GetParameter("WhsCodeForImportSLI");
            mItCodeForImportSLI = Sm.GetParameter("ItCodeForImportSLI");
            mGenerateEmpCodeFormat = Sm.GetParameter("GenerateEmpCodeFormat");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            if (mDocNoFormat.Length == 0) mDocNoFormat = "1";
            mIsItCodeUseItSeqNo = Sm.GetParameterBoo("IsItCodeUseItSeqNo");
            mItCodeSeqNo = Sm.GetParameter("ItCodeSeqNo");
            if (mItCodeSeqNo.Length == 0) mItCodeSeqNo = "5";
            mIsAssetCodeBasedOnCategoryEnabled = Sm.GetParameterBoo("IsAssetCodeBasedOnCategoryEnabled");
            mDocTitle = Sm.GetParameter("DocTitle");
            mLengthAssetCode = Sm.GetParameter("LengthAssetCode");
            if (mLengthAssetCode.Length == 0) mLengthAssetCode = "4";
            mIsEmpContractDtMandatory = Sm.GetParameterBoo("IsEmpContractDtMandatory");
            mIsVdCodeUseVdCt = Sm.GetParameterBoo("IsVdCodeUseVdCt");
            mIsCustomerCOAUseCustomerCategory = Sm.GetParameterBoo("IsCustomerCOAUseCustomerCategory");
            mIsBudgetCategoryUseCostCategory = Sm.GetParameterBoo("IsBudgetCategoryUseCostCategory");
            mIsBudgetCategoryUseCostCenter = Sm.GetParameterBoo("IsBudgetCategoryUseCostCenter");
            mIsBudgetCategoryUseCOA = Sm.GetParameterBoo("IsBudgetCategoryUseCOA");
            mIsAssetShowAdditionalInformation = Sm.GetParameterBoo("IsAssetShowAdditionalInformation");
            mItCodeFormat = Sm.GetParameter("ItCodeFormat");
            mItemPriceDNoLength = Sm.GetParameter("ItemPriceDNoLength");
            if (mItCodeFormat.Length == 0) mItCodeFormat = "1";
        }

        private string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("   ), '0001' ");
            SQL.Append("), '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateBCCode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            SQL.AppendLine("    Select Convert(BCCode, Decimal)+1 As Value ");
            SQL.AppendLine("    From TblBudgetCategory Order By BCCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), 5) As BCCode");

            return Sm.GetValue(SQL.ToString());
        }

        public string GenerateDocNo(int Index, string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled"),
                 IsDocNoFormatUseFullYear = Sm.GetParameterBoo("IsDocNoFormatUseFullYear");

            string Yr = string.Empty,
                //Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";

            if (IsDocNoFormatUseFullYear)
                Yr = Sm.Left(DocDt, 4);
            else
                Yr = DocDt.Substring(2, 2);

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + Index + ", Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+ " + Index + ", Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Index + "'), " + DocSeqNo + ") ");
            //SQL.Append("   ), Right(Concat('000', " + Index + "), 4) ");
            SQL.Append("), '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());

        }

        public static string GenerateDocNoJournal(string DocDt, int Index)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo")
                ;
            var SQL = new StringBuilder();

            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + JournalDocSeqNo + "), Convert(DocNo+" + Index + ", Char)), " + JournalDocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblJournalHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By DocNo Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + JournalDocSeqNo + "), '" + Index + "'), " + JournalDocSeqNo + ")) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        #region COA

        private bool IsInsertedCOAInvalid(ref List<COA> lCOA)
        {
            return
                IsAcNoEmpty(ref lCOA) ||
                IsAcDescEmpty(ref lCOA) ||
                IsAcTypeEmpty(ref lCOA) ||
                IsAcNoDuplicated(ref lCOA) ||
                IsAcNoAlreadyExists(ref lCOA) ||
                IsAcTypeInvalid(ref lCOA);
        }

        private bool IsAcNoEmpty(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                if (lCOA[i].AcNo.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Account number is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAcDescEmpty(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                if (lCOA[i].AcDesc.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Account# " + lCOA[i].AcNo + " has no Account Description yet.");
                    return true;
                }
            }

            return false;
        }

        private bool IsAcTypeEmpty(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                if (lCOA[i].AcType.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Account# " + lCOA[i].AcNo + " has no Account Type yet.");
                    return true;
                }
            }

            return false;
        }

        private bool IsAcNoDuplicated(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                for (int j = (i + 1); j < lCOA.Count(); j++)
                {
                    if (lCOA[i].AcNo == lCOA[j].AcNo)
                    {
                        Sm.StdMsg(mMsgType.Info, "Account# " + lCOA[i].AcNo + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsAcNoAlreadyExists(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                var cm = new MySqlCommand() { CommandText = "Select AcNo From TblCOA Where AcNo =@AcNo Limit 1; " };

                Sm.CmParam<String>(ref cm, "@AcNo", lCOA[i].AcNo);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Account number ( " + lCOA[i].AcNo + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private bool IsAcTypeInvalid(ref List<COA> lCOA)
        {
            for (int i = 0; i < lCOA.Count(); i++)
            {
                if (Sm.Left(lCOA[i].AcType, 1) != "D" && Sm.Left(lCOA[i].AcType, 1) != "C" && Sm.Left(lCOA[i].AcType, 1) != "K")
                {
                    Sm.StdMsg(mMsgType.Info, "Account# " + lCOA[i].AcNo + " has invalid Account Type (" + lCOA[i].AcType + ").");
                    return true;
                }
            }

            return false;
        }

        private void InsertCOA(COA x)
        {
            string AcType = x.AcType;

            if (Sm.Left(AcType, 1).ToUpper() == "D" || Sm.Left(AcType, 1).ToUpper() == "C") // debit atau credit
            {
                AcType = Sm.Left(AcType, 1).ToUpper();
            }
            else if (Sm.Left(AcType, 1).ToUpper() == "K") // kredit
            {
                AcType = "C";
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, ActInd, Level, AcType, Alias, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AcNo, @AcDesc, 'Y', @Level, @AcType, @Alias,@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblCOA ");
            SQL.AppendLine("  Set Parent = null ");
            SQL.AppendLine("Where AcNo = @AcNo And Level = 1; ");

            SQL.AppendLine("Update TblCOA ");
            SQL.AppendLine("  Set Parent = Substring_Index(AcNo, '.', Level-1) ");
            SQL.AppendLine("Where AcNo = @AcNo And Level > 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
            Sm.CmParam<String>(ref cm, "@AcDesc", x.AcDesc);
            Sm.CmParam<String>(ref cm, "@Level", x.Level.ToString());
            Sm.CmParam<String>(ref cm, "@AcType", AcType);
            Sm.CmParam<String>(ref cm, "@Alias", x.Alias);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Employee Salary

        private bool IsInsertedEmpSalaryInvalid(ref List<EmpSalary> lEmpSalary)
        {
            return
                IsStartDtInvalid(ref lEmpSalary) ||
                IsEmpSalaryDuplicate(ref lEmpSalary) ||
                IsEmpSalaryDuplicate2(ref lEmpSalary) ||
                IsAmtNegative(ref lEmpSalary);
        }

        private bool IsStartDtInvalid(ref List<EmpSalary> lEmpSalary)
        {
            for (int i = 0; i < lEmpSalary.Count; i++)
            {
                if (lEmpSalary[i].StartDt.Length != 8)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + lEmpSalary[i].EmpCode);
                    mW.AppendLine("Start Date : " + lEmpSalary[i].StartDt);
                    mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
                else
                {
                    DateTime mDt;
                    bool mValid = DateTime.TryParseExact(lEmpSalary[i].StartDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out mDt);

                    if (!mValid)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (i + 1));
                        mW.AppendLine("Employee Code : " + lEmpSalary[i].EmpCode);
                        mW.AppendLine("Start Date : " + lEmpSalary[i].StartDt);
                        mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmpSalaryDuplicate(ref List<EmpSalary> lEmpSalary)
        {
            for (int i = 0; i < lEmpSalary.Count - 1; i++)
            {
                for (int j = i + 1; j < lEmpSalary.Count; j++)
                {
                    if (lEmpSalary[i].EmpCode == lEmpSalary[j].EmpCode &&
                        lEmpSalary[i].StartDt == lEmpSalary[j].StartDt
                        )
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + lEmpSalary[j].EmpCode);
                        mW.AppendLine("Start Date : " + lEmpSalary[j].StartDt);
                        mW.AppendLine("Duplicate employee salary found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }

                    if (lEmpSalary[i].EmpCode == lEmpSalary[j].EmpCode)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + lEmpSalary[j].EmpCode);
                        mW.AppendLine("Duplicate employee code found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmpSalaryDuplicate2(ref List<EmpSalary> lEmpSalary)
        {
            var lEmpSalaryExt2 = new List<EmpSalaryExt2>();
            PrepareEmpSalaryExists2(ref lEmpSalaryExt2);

            if (lEmpSalaryExt2.Count > 0)
            {
                for (int i = 0; i < lEmpSalary.Count; i++)
                {
                    for (int j = 0; j < lEmpSalaryExt2.Count; j++)
                    {
                        if (lEmpSalary[i].EmpCode == lEmpSalaryExt2[j].EmpCode &&
                            lEmpSalary[i].StartDt == lEmpSalaryExt2[j].StartDt
                            )
                        {
                            var mW = new StringBuilder();

                            mW.AppendLine("Row #" + (i + 1));
                            mW.AppendLine("Employee Code : " + lEmpSalary[i].EmpCode);
                            mW.AppendLine("Start Date : " + lEmpSalary[i].StartDt);
                            mW.AppendLine("Duplicate employee salary found compared to existing data.");

                            Sm.StdMsg(mMsgType.Warning, mW.ToString());
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsAmtNegative(ref List<EmpSalary> lEmpSalary)
        {
            for (int i = 0; i < lEmpSalary.Count; i++)
            {
                if (lEmpSalary[i].Amt < 0)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + lEmpSalary[i].EmpCode);
                    mW.AppendLine("Monthly Amount : " + lEmpSalary[i].Amt);
                    mW.AppendLine("Monthly Amount should not be negative.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }

                if (lEmpSalary[i].Amt2 < 0)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + lEmpSalary[i].EmpCode);
                    mW.AppendLine("Daily Amount : " + lEmpSalary[i].Amt2);
                    mW.AppendLine("Daily Amount should not be negative.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
            }

            return false;
        }

        private void PrepareEmpSalaryExists(ref List<EmpSalaryExt> lEmpSalaryExt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.EmpCode, Max(T.DNo) DNo ");
            SQL.AppendLine("From TblEmployeeSalary T ");
            SQL.AppendLine("Where T.ActInd = 'Y' ");
            SQL.AppendLine("Group By T.EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpSalaryExt.Add(new EmpSalaryExt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepareEmpSalaryExists2(ref List<EmpSalaryExt2> lEmpSalaryExt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.EmpCode, StartDt ");
            SQL.AppendLine("From TblEmployeeSalary T ");
            SQL.AppendLine("Where T.ActInd = 'Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpSalaryExt2.Add(new EmpSalaryExt2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void AssignDNo(ref List<EmpSalary> lEmpSalary, ref List<EmpSalaryExt> lEmpSalaryExt)
        {
            for (int j = 0; j < lEmpSalary.Count; j++)
            {
                lEmpSalary[j].DNo = "001";
            }

            if (lEmpSalaryExt.Count > 0)
            {
                for (int i = 0; i < lEmpSalaryExt.Count; i++)
                {
                    for (int j = 0; j < lEmpSalary.Count; j++)
                    {
                        if (lEmpSalary[j].EmpCode == lEmpSalaryExt[i].EmpCode)
                        {
                            lEmpSalary[j].DNo = Sm.Right(string.Concat("000", (Decimal.Parse(lEmpSalaryExt[i].DNo) + 1)), 3);
                            break;
                        }
                    }
                }
            }
        }

        private void InsertEmpSalary(EmpSalary x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeSalary(EmpCode, DNo, StartDt, ActInd, Amt, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @StartDt, 'Y', @Amt, @Amt2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@StartDt", x.StartDt);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", x.Amt2);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Employee Salary SS

        private bool IsInsertedEmployeeSalarySSInvalid(ref List<EmployeeSalarySS> l, ref List<EmployeeSalarySS2> l2)
        {
            return
                IsDtInvalid(ref l) ||
                IsEmployeeSalarySSDuplicate(ref l) ||
                IsEmployeeSalarySSDuplicate2(ref l, ref l2) ||
                IsAmtNegative(ref l);
        }

        private bool IsDtInvalid(ref List<EmployeeSalarySS> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].StartDt.Length != 8)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                    mW.AppendLine("Start Date : " + l[i].StartDt);
                    mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
                else
                {
                    DateTime mDt;
                    bool mValid = DateTime.TryParseExact(l[i].StartDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out mDt);

                    if (!mValid)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (i + 1));
                        mW.AppendLine("Employee Code : " + l[i].EmpCode);
                        mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                        mW.AppendLine("Start Date : " + l[i].StartDt);
                        mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }

                if (l[i].EndDt.Length != 8)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                    mW.AppendLine("End Date : " + l[i].EndDt);
                    mW.AppendLine("End Date format should be yyyymmdd, for example : 20180101.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
                else
                {
                    DateTime mDt;
                    bool mValid = DateTime.TryParseExact(l[i].EndDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out mDt);

                    if (!mValid)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (i + 1));
                        mW.AppendLine("Employee Code : " + l[i].EmpCode);
                        mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                        mW.AppendLine("End Date : " + l[i].EndDt);
                        mW.AppendLine("End Date format should be yyyymmdd, for example : 20180101.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmployeeSalarySSDuplicate(ref List<EmployeeSalarySS> l)
        {
            for (int i = 0; i < l.Count - 1; i++)
            {
                for (int j = i + 1; j < l.Count; j++)
                {
                    if (l[i].EmpCode == l[j].EmpCode &&
                        l[i].SSPCode == l[j].SSPCode &&
                        l[i].StartDt == l[j].StartDt
                        )
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + l[j].EmpCode);
                        mW.AppendLine("Social Security Program Code : " + l[j].SSPCode);
                        mW.AppendLine("Start Date : " + l[j].StartDt);
                        mW.AppendLine("Duplicate employee salary social security found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }

                    if (l[i].EmpCode == l[j].EmpCode && l[i].SSPCode == l[j].SSPCode)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + l[j].EmpCode);
                        mW.AppendLine("Social Security Program Code : " + l[j].SSPCode);
                        mW.AppendLine("Duplicate employee code and social security program found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmployeeSalarySSDuplicate2(ref List<EmployeeSalarySS> l, ref List<EmployeeSalarySS2> l2)
        {
            if (l2.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < l2.Count; j++)
                    {
                        if (l[i].EmpCode == l2[j].EmpCode &&
                            l[i].SSPCode == l2[j].SSPCode &&
                            l[i].StartDt == l2[j].StartDt
                            )
                        {
                            var mW = new StringBuilder();

                            mW.AppendLine("Row #" + (i + 1));
                            mW.AppendLine("Employee Code : " + l[i].EmpCode);
                            mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                            mW.AppendLine("Start Date : " + l[i].StartDt);
                            mW.AppendLine("Duplicate employee salary social security found compared to existing data.");

                            Sm.StdMsg(mMsgType.Warning, mW.ToString());
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsAmtNegative(ref List<EmployeeSalarySS> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Amt < 0)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Social Security Program Code : " + l[i].SSPCode);
                    mW.AppendLine("Amount : " + l[i].Amt);
                    mW.AppendLine("Amount should not be negative.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
            }
            return false;
        }

        private void PrepareEmployeeSalarySS(ref List<EmployeeSalarySS2> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.EmpCode, SSPCode, StartDt ");
            SQL.AppendLine("From TblEmployeeSalarySS T; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "SSPCode", "StartDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            SSPCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void InsertEmployeeSalarySS(EmployeeSalarySS x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EmpCode, SSPCode, Max(EndDt) EndDt ");
            SQL.AppendLine("    From TblEmployeeSalarySS ");
            SQL.AppendLine("    Where EmpCode=@EmpCode ");
            SQL.AppendLine("    And SSPCode=@SSPCode ");
            SQL.AppendLine("    And StartDt Is Not Null ");
            SQL.AppendLine("    And EndDt Is Not Null ");
            SQL.AppendLine("    Group By EmpCode, SSPCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode And A.SSPCode=B.SSPCode And A.EndDt=B.EndDt ");
            SQL.AppendLine("Set A.EndDt=@PrevEndDt, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.SSPCode=@SSPCode ");
            SQL.AppendLine("And A.StartDt Is Not Null ");
            SQL.AppendLine("And A.EndDt Is Not Null; ");

            SQL.AppendLine("Select @Row:=Cast(IfNull(Max(CAST(DNo AS UNSIGNED)), '000') As UNSIGNED) From TblEmployeeSalarySS Where EmpCode=@EmpCode; ");
            SQL.AppendLine("Insert Into TblEmployeeSalarySS(EmpCode, DNo, SSPCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), @SSPCode, @StartDt, @EndDt, @Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@SSPCode", x.SSPCode);
            Sm.CmParamDt(ref cm, "@StartDt", x.StartDt);
            Sm.CmParamDt(ref cm, "@EndDt", x.EndDt);
            Sm.CmParamDt(ref cm, "@PrevEndDt", x.PrevEndDt);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Employee Allowance/Deduction

        private bool IsInsertedEmployeeAllowanceDeductionInvalid(ref List<EmployeeAllowanceDeduction> l, ref List<EmployeeAllowanceDeduction2> l2)
        {
            return
                IsDtInvalid(ref l) ||
                IsEmployeeAllowanceDeductionDuplicate(ref l) ||
                IsEmployeeAllowanceDeductionDuplicate2(ref l, ref l2) ||
                IsAmtNegative(ref l);
        }

        private bool IsDtInvalid(ref List<EmployeeAllowanceDeduction> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].StartDt.Length != 8)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                    mW.AppendLine("Start Date : " + l[i].StartDt);
                    mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
                else
                {
                    DateTime mDt;
                    bool mValid = DateTime.TryParseExact(l[i].StartDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out mDt);

                    if (!mValid)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (i + 1));
                        mW.AppendLine("Employee Code : " + l[i].EmpCode);
                        mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                        mW.AppendLine("Start Date : " + l[i].StartDt);
                        mW.AppendLine("Start Date format should be yyyymmdd, for example : 20180101.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }

                if (l[i].EndDt.Length != 8)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                    mW.AppendLine("End Date : " + l[i].EndDt);
                    mW.AppendLine("End Date format should be yyyymmdd, for example : 20180101.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
                else
                {
                    DateTime mDt;
                    bool mValid = DateTime.TryParseExact(l[i].EndDt, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out mDt);

                    if (!mValid)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (i + 1));
                        mW.AppendLine("Employee Code : " + l[i].EmpCode);
                        mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                        mW.AppendLine("End Date : " + l[i].EndDt);
                        mW.AppendLine("End Date format should be yyyymmdd, for example : 20180101.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmployeeAllowanceDeductionDuplicate(ref List<EmployeeAllowanceDeduction> l)
        {
            for (int i = 0; i < l.Count - 1; i++)
            {
                for (int j = i + 1; j < l.Count; j++)
                {
                    if (l[i].EmpCode == l[j].EmpCode &&
                        l[i].ADCode == l[j].ADCode &&
                        l[i].StartDt == l[j].StartDt
                        )
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + l[j].EmpCode);
                        mW.AppendLine("Allowance/Deduction Code : " + l[j].ADCode);
                        mW.AppendLine("Start Date : " + l[j].StartDt);
                        mW.AppendLine("Duplicate employee allowance/deduction found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }

                    if (l[i].EmpCode == l[j].EmpCode && l[i].ADCode == l[j].ADCode)
                    {
                        var mW = new StringBuilder();

                        mW.AppendLine("Row #" + (j + 1));
                        mW.AppendLine("Employee Code : " + l[j].EmpCode);
                        mW.AppendLine("Allowance/Deduction Code : " + l[j].ADCode);
                        mW.AppendLine("Duplicate employee code and employee allowance/deduction found.");

                        Sm.StdMsg(mMsgType.Warning, mW.ToString());
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmployeeAllowanceDeductionDuplicate2(ref List<EmployeeAllowanceDeduction> l, ref List<EmployeeAllowanceDeduction2> l2)
        {
            if (l2.Count > 0)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < l2.Count; j++)
                    {
                        if (l[i].EmpCode == l2[j].EmpCode &&
                            l[i].ADCode == l2[j].ADCode &&
                            l[i].StartDt == l2[j].StartDt
                            )
                        {
                            var mW = new StringBuilder();

                            mW.AppendLine("Row #" + (i + 1));
                            mW.AppendLine("Employee Code : " + l[i].EmpCode);
                            mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                            mW.AppendLine("Start Date : " + l[i].StartDt);
                            mW.AppendLine("Duplicate employee's allowance/deduction found compared to existing data.");

                            Sm.StdMsg(mMsgType.Warning, mW.ToString());
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsAmtNegative(ref List<EmployeeAllowanceDeduction> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Amt < 0)
                {
                    var mW = new StringBuilder();

                    mW.AppendLine("Row #" + (i + 1));
                    mW.AppendLine("Employee Code : " + l[i].EmpCode);
                    mW.AppendLine("Allowance/Deduction Code : " + l[i].ADCode);
                    mW.AppendLine("Amount : " + l[i].Amt);
                    mW.AppendLine("Amount should not be negative.");

                    Sm.StdMsg(mMsgType.Warning, mW.ToString());
                    return true;
                }
            }
            return false;
        }

        private void PrepareEmployeeAllowanceDeduction(ref List<EmployeeAllowanceDeduction> l1, ref List<EmployeeAllowanceDeduction2> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            foreach (var x in l1.Select(m => new { m.EmpCode, m.ADCode }).Distinct().ToList())
            {
                if (x.EmpCode.Length != 0 && x.ADCode.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T.EmpCode=@EmpCode0" + i.ToString() + " And T.ADCode=@ADCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), x.EmpCode);
                    Sm.CmParam<String>(ref cm, "@ADCode0" + i.ToString(), x.ADCode);
                }
                i++;
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select T.EmpCode, ADCode, StartDt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction T ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "StartDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new EmployeeAllowanceDeduction2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void InsertEmployeeAllowanceDeduction(EmployeeAllowanceDeduction x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Update TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("Set EndDt=@PrevEndDt, LastUpBy=@UserCode, LastUpDt=@EndDt ");
            SQL.AppendLine("Where EmpCode=@EmpCode ");
            SQL.AppendLine("And ADCode=@ADCode ");
            SQL.AppendLine("And StartDt Is Not Null ");
            SQL.AppendLine("And EndDt Is Not Null ");
            SQL.AppendLine("And EndDt In ( ");
            SQL.AppendLine("    Select EndDt From (");
            SQL.AppendLine("    Select Max(EndDt) EndDt From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("    Where EmpCode=@EmpCode ");
            SQL.AppendLine("    And ADCode=@ADCode ");
            SQL.AppendLine("    And StartDt Is Not Null ");
            SQL.AppendLine("    And EndDt Is Not Null ");
            SQL.AppendLine(") T); ");

            SQL.AppendLine("Select @Row:=Cast(IfNull(Max(DNo), '000') As UNSIGNED) From TblEmployeeAllowanceDeduction Where EmpCode=@EmpCode; ");

            SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction(EmpCode, DNo, ADCode, StartDt, EndDt, Amt, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), @ADCode, @StartDt, @EndDt, @Amt, Null, @UserCode, @Dt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@ADCode", x.ADCode);
            Sm.CmParamDt(ref cm, "@StartDt", x.StartDt);
            Sm.CmParamDt(ref cm, "@EndDt", x.EndDt);
            Sm.CmParamDt(ref cm, "@PrevEndDt", x.PrevEndDt);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Asset

        private bool IsInsertedAssetInvalid(ref List<Asset> lAsset)
        {
            return
                IsAssetNameEmpty(ref lAsset) ||
                IsItCodeEmpty(ref lAsset) ||
                IsAssetDtEmpty(ref lAsset) ||
                IsAssetDtFormatInvalid(ref lAsset) ||
                IsAssetCodeFormatInvalid(ref lAsset) ||
                IsDepreciationMethodEmpty(ref lAsset) ||
                IsCCCodeEmpty(ref lAsset) ||
                IsAssetCodeDuplicated(ref lAsset) ||
                IsAssetCodeAlreadyExists(ref lAsset);
        }

        private bool IsAssetNameEmpty(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Asset name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAssetDtEmpty(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Asset date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItCodeEmpty(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAssetDtFormatInvalid(ref List<Asset> lAsset)
        {
            for (int i = 1; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetDt.Contains("/") || lAsset[i].AssetDt.Contains("-") || lAsset[i].AssetDt.Contains(".") || lAsset[i].AssetDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAssetCodeFormatInvalid(ref List<Asset> lAsset)
        {
            for (int i = 1; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetCode.Length > 0)
                {
                    if (Sm.Right(lAsset[i].AssetCode, 4) != Sm.Left(lAsset[i].AssetDt, 4))
                    {
                        Sm.StdMsg(mMsgType.Info, "Asset Code and Asset Date (year) is not match at row #" + (i + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDepreciationMethodEmpty(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].DepreciationCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Depreciation code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCCCodeEmpty(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].CCCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost center code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAssetCodeDuplicated(ref List<Asset> lAsset)
        {
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetCode.Length > 0)
                {
                    for (int j = (i + 1); j < lAsset.Count(); j++)
                    {
                        if (Sm.CompareStr(lAsset[i].AssetCode, lAsset[j].AssetCode))
                        {
                            Sm.StdMsg(mMsgType.Info, "Asset code " + lAsset[i].AssetCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsAssetCodeAlreadyExists(ref List<Asset> lAsset)
        {
            string mAssetCode = string.Empty, mAssetExisted = string.Empty;
            for (int i = 0; i < lAsset.Count(); i++)
            {
                if (lAsset[i].AssetCode.Length > 0)
                {
                    if (mAssetCode.Length > 0) mAssetCode += ",";
                    mAssetCode += lAsset[i].AssetCode;
                }
            }

            if (mAssetCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct AssetCode Separator '\r\n') AssetCode From TblAsset Where Find_In_Set(AssetCode, @Param); ");
                mAssetExisted = Sm.GetValue(SQL.ToString(), mAssetCode);

                if (mAssetExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Asset code ( " + Sm.GetValue(SQL.ToString(), mAssetCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private void InsertAsset(Asset x)
        {
            var SQL = new StringBuilder();

            if (x.AssetCode.Length == 0)
            {
                if (mIsAssetCodeBasedOnCategoryEnabled)
                {
                    SQL.Append("Set @AssetCode:=Concat(");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('00000', Convert(AssetCode+1, Char)), 5) From ( ");
                    SQL.Append("       Select Convert(Left(AssetCode, 5), Decimal) As AssetCode From TblAsset ");
                    SQL.Append("       Where Left(AssetDt, 6)=Left(@AssetDt, 6) ");
                    SQL.Append("       And AssetCategoryCode=@AssetCategoryCode ");
                    SQL.Append("       Order By Left(AssetCode, 5) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '00001') ");
                    SQL.Append(", '/', @AssetCategoryCode, '/', '" + mDocTitle + "', '/', Substring(@AssetDt, 5, 2), '/', Left(@AssetDt, 4) ");
                    SQL.Append("); ");
                }
                else
                {
                    SQL.Append("Set @AssetCode:=(Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("    Select Right(Concat(Repeat('0', " + mLengthAssetCode + "), Convert(Code+1, Char)), " + mLengthAssetCode + ") From ( ");
                    SQL.Append("        Select Convert(Left(AssetCode, " + mLengthAssetCode + "), Decimal) As Code From TblAsset ");
                    SQL.Append("        Where Left(AssetDt, 4)=Left(@AssetDt, 4) ");
                    SQL.Append("        Order By Left(AssetCode, " + mLengthAssetCode + ") Desc Limit 1 ");
                    SQL.Append("    ) As Temp ");
                    SQL.Append("    ), Right(Concat(Repeat('0', " + mLengthAssetCode + "), '1'), " + mLengthAssetCode + ")), ");
                    SQL.Append("'/AS/', Left(@AssetDt, 4) ");
                    SQL.Append(")); ");
                }
            }

            SQL.AppendLine("Insert Into TblAsset(AssetCode, ItCode, AssetName, DisplayName, ActiveInd, AssetType, ");
            SQL.AppendLine("LeasingInd, RentedInd, SoldInd, AssetDt, AssetValue, EcoLifeYr, EcoLife, ");
            SQL.AppendLine("DepreciationCode, PercentageAnnualDepreciation, CCCode, AcNo, AcNo2, AssetCategoryCode, ");
            SQL.AppendLine("Length, Height, Width, Volume, Diameter, Wide, ");
            SQL.AppendLine("LengthUomCode, HeightUomCode, WidthUomCode, VolumeUomCode, DiameterUomCode, WideUomCode, Parent, ShortCode, FileName, Location,  ");
            SQL.AppendLine("Classification, SubClassification, Type, SubType, Location2, SubLocation, Location3, SiteCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AssetCode, @ItCode, @AssetName, @DisplayName, 'Y', @AssetType, ");
            SQL.AppendLine("@LeasingInd, @RentedInd, @SoldInd, @AssetDt, @AssetValue, @EcoLifeYr, @EcoLife, ");
            SQL.AppendLine("@DepreciationCode, @PercentageAnnualDepreciation, @CCCode, @AcNo, @AcNo2, @AssetCategoryCode, ");
            SQL.AppendLine("@Length, @Height, @Width, @Volume, @Diameter, @Wide, ");
            SQL.AppendLine("@LengthUomCode, @HeightUomCode, @WidthUomCode, @VolumeUomCode, @DiameterUomCode, @WideUomCode, @Parent, @ShortCode, @FileName, @Location, ");
            SQL.AppendLine("@Classification, @SubClassification, @Type, @SubType, @Location2, @SubLocation, @Location3, @SiteCode,  ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AssetCode, @CCCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("ON Duplicate Key ");
            SQL.AppendLine("    Update CCCode=@CCCode, LastUpBy=@LastUpBy, LastUpDt=CurrentDateTime() ; ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString(),
                CommandTimeout = 1800
            };

            if (x.AssetCode.Length > 0) Sm.CmParam<String>(ref cm, "@AssetCode", x.AssetCode);
            Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            Sm.CmParam<String>(ref cm, "@AssetName", x.AssetName);
            Sm.CmParam<String>(ref cm, "@DisplayName", x.DisplayName);
            Sm.CmParam<String>(ref cm, "@AssetType", x.AssetType);
            Sm.CmParam<String>(ref cm, "@LeasingInd", x.LeasingInd);
            Sm.CmParam<String>(ref cm, "@RentedInd", x.RentedInd);
            Sm.CmParam<String>(ref cm, "@SoldInd", x.SoldInd);
            Sm.CmParam<String>(ref cm, "@AssetDt", x.AssetDt);
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", x.AssetValue);
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", x.EcoLifeYr);
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", x.EcoLife);
            Sm.CmParam<String>(ref cm, "@DepreciationCode", x.DepreciationCode);
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", x.PercentageAnnualDepreciation);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);
            Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
            Sm.CmParam<String>(ref cm, "@AcNo2", x.AcNo2);
            Sm.CmParam<String>(ref cm, "@AssetCategoryCode", x.AssetCategoryCode);
            Sm.CmParam<Decimal>(ref cm, "@Length", x.Length);
            Sm.CmParam<Decimal>(ref cm, "@Height", x.Height);
            Sm.CmParam<Decimal>(ref cm, "@Width", x.Width);
            Sm.CmParam<Decimal>(ref cm, "@Volume", x.Volume);
            Sm.CmParam<Decimal>(ref cm, "@Diameter", x.Diameter);
            Sm.CmParam<Decimal>(ref cm, "@Wide", x.Wide);
            Sm.CmParam<String>(ref cm, "@LengthUomCode", x.LengthUomCode);
            Sm.CmParam<String>(ref cm, "@HeightUomCode", x.HeightUomCode);
            Sm.CmParam<String>(ref cm, "@WidthUomCode", x.WidthUomCode);
            Sm.CmParam<String>(ref cm, "@VolumeUomCode", x.VolumeUomCode);
            Sm.CmParam<String>(ref cm, "@DiameterUomCode", x.DiameterUomCode);
            Sm.CmParam<String>(ref cm, "@WideUomCode", x.WideUomCode);
            Sm.CmParam<String>(ref cm, "@Parent", x.Parent);
            Sm.CmParam<String>(ref cm, "@ShortCode", x.ShortCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@FileName", x.FileName);
            Sm.CmParam<String>(ref cm, "@Location", x.Location);
            if (mIsAssetShowAdditionalInformation)
            {
                Sm.CmParam<String>(ref cm, "@Classification", x.Classification);
                Sm.CmParam<String>(ref cm, "@SubClassification", x.SubClassification);
                Sm.CmParam<String>(ref cm, "@Type", x.Type);
                Sm.CmParam<String>(ref cm, "@SubType", x.SubType);
                Sm.CmParam<String>(ref cm, "@Location2", x.Location2);
                Sm.CmParam<String>(ref cm, "@SubLocation", x.SubLocation);
                Sm.CmParam<String>(ref cm, "@Location3", x.Location3);
                Sm.CmParam<String>(ref cm, "@SiteCode", x.SiteCode);
            }

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Vendor

        private bool IsInsertedVendorInvalid(ref List<Vendor> lVendor)
        {
            return
                IsVendorNameEmpty(ref lVendor) ||
                IsShortNameEmpty(ref lVendor) ||
                IsCityCodeEmpty(ref lVendor) ||
                IsVendorCodeDuplicated(ref lVendor) ||
                IsVendorCodeAlreadyExists(ref lVendor);
        }

        private bool IsVendorNameEmpty(ref List<Vendor> lVendor)
        {
            for (int i = 0; i < lVendor.Count(); i++)
            {
                if (lVendor[i].VdName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Vendor name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsShortNameEmpty(ref List<Vendor> lVendor)
        {
            for (int i = 0; i < lVendor.Count(); i++)
            {
                if (lVendor[i].ShortName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Short name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCityCodeEmpty(ref List<Vendor> lVendor)
        {
            for (int i = 0; i < lVendor.Count(); i++)
            {
                if (lVendor[i].CityCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "City Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsVendorCodeDuplicated(ref List<Vendor> lVendor)
        {
            for (int i = 0; i < lVendor.Count(); i++)
            {
                for (int j = (i + 1); j < lVendor.Count(); j++)
                {
                    if (lVendor[i].VdCode == lVendor[j].VdCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Vendor code " + lVendor[i].VdCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsVendorCodeAlreadyExists(ref List<Vendor> lVendor)
        {
            string mVdCode = string.Empty, mVdExisted = string.Empty;
            for (int i = 0; i < lVendor.Count(); i++)
            {
                if (mVdCode.Length > 0) mVdCode += ",";
                mVdCode += lVendor[i].VdCode;
            }

            if (mVdCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct VdCode Separator '\r\n') VdCode From TblVendor Where Find_In_Set(VdCode, @Param); ");
                mVdExisted = Sm.GetValue(SQL.ToString(), mVdCode);

                if (mVdExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Vendor code ( " + Sm.GetValue(SQL.ToString(), mVdCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private void InsertVendor(Vendor x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVendor(VdCode, VdName, ActInd, VendorRegisterInd, ShortName, VdCtCode, Address, CityCode,  ");
            SQL.AppendLine("SDCode, VilCode, PostalCd, Website, Parent, EstablishedYr,  ");
            SQL.AppendLine("IdentityNo, TIN, TaxInd, Phone, Fax, Email, Mobile, ");
            SQL.AppendLine("CreditLimit, NIB, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VdCode, @VdName, 'Y', 'N', @ShortName, @VdCtCode, @Address, ");
            SQL.AppendLine("@CityCode, @SDCode, @VilCode, @PostalCd, @Website, @Parent, @EstablishedYr, ");
            SQL.AppendLine("@IdentityNo, @TIN, @TaxInd, @Phone, @Fax, @Email, @Mobile,  ");
            SQL.AppendLine("@CreditLimit, @NIB, @Remark,@CreateBy, CurrentDateTime()); ");

            COAVendor(ref SQL, "VendorAcNoDownPayment", "VendorAcDescDownPayment", "D");
            COAVendor(ref SQL, "VendorAcNoAP", "VendorAcDescAP", "C");
            COAVendor(ref SQL, "VendorAcNoUnInvoiceAP", "VendorAcDescUnInvoiceAP", "C");
            COAVendor(ref SQL, "AcNoForGiroAP", "AcDescForGiroAP", "C");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm, "@VdName", x.VdName);
            Sm.CmParam<String>(ref cm, "@ShortName", x.ShortName);
            Sm.CmParam<String>(ref cm, "@VdCtCode", x.VdCtCode);
            Sm.CmParam<String>(ref cm, "@Address", x.Address);
            Sm.CmParam<String>(ref cm, "@CityCode", x.CityCode);
            Sm.CmParam<String>(ref cm, "@SDCode", x.SDCode);
            Sm.CmParam<String>(ref cm, "@VilCode", x.VilCode);
            Sm.CmParam<String>(ref cm, "@PostalCd", x.PostalCd);
            Sm.CmParam<String>(ref cm, "@Website", x.Website);
            Sm.CmParam<String>(ref cm, "@Parent", x.Parent);
            Sm.CmParam<String>(ref cm, "@EstablishedYr", x.EstablishedYr);
            Sm.CmParam<String>(ref cm, "@IdentityNo", x.IdentityNo);
            Sm.CmParam<String>(ref cm, "@TIN", x.TIN);
            Sm.CmParam<String>(ref cm, "@TaxInd", x.TaxInd);
            Sm.CmParam<String>(ref cm, "@Phone", x.Phone);
            Sm.CmParam<String>(ref cm, "@Fax", x.Fax);
            Sm.CmParam<String>(ref cm, "@Email", x.Email);
            Sm.CmParam<String>(ref cm, "@Mobile", x.Mobile);
            Sm.CmParam<String>(ref cm, "@NIB", x.NIB);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", x.CreditLimit);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ClearData();
        }

        private void COAVendor(ref StringBuilder SQL, string AcNo, string AcDesc, string AcType)
        {
            SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
            SQL.AppendLine("Select ");
            SQL.AppendLine("(Select Concat(ParValue, @VdCode) From TblParameter Where ParCode='" + AcNo + "') As AcNo, ");
            SQL.AppendLine("(Select Concat(ParValue, ' ', @VdName) From TblParameter Where ParCode='" + AcDesc + "') As AcDesc, ");
            if (mIsVdCodeUseVdCt)
            {
                SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @VdCode), '.', Length(Concat(ParValue, @VdCode))-Length(Replace(Concat(ParValue, @VdCode), '.', ''))) From Tblparameter Where parcode = '" + AcNo + "') As Parent, ");
                SQL.AppendLine("(Select Length(Concat(ParValue, @VdCode))-Length(Replace(Concat(ParValue, @VdCode), '.', ''))+1 From TblParameter Where ParCode = '" + AcNo + "') As Level, ");
            }
            else
            {
                SQL.AppendLine("(Select Left(ParValue, Length(ParValue)-1) From TblParameter Where ParCode = '" + AcNo + "') As Parent, ");
                SQL.AppendLine("(Select Length(ParValue)-Length(Replace(ParValue, '.', ''))+1 From TblParameter Where ParCode = '" + AcNo + "') As Level, ");
            }

            SQL.AppendLine("'" + AcType + "', @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVendor ");
            SQL.AppendLine("Where VdCode=@VdCode  ");
            SQL.AppendLine("And Exists(Select ParCode From TblParameter Where ParCode='" + AcNo + "' And IfNull(ParValue, '')<>'') ");
            SQL.AppendLine("And Exists(Select ParCode From TblParameter Where ParCode='" + AcDesc + "' and IfNull(ParValue, '')<>''); ");
        }

        #endregion

        #region Customer

        private bool IsInsertedCustomerInvalid(ref List<Customer> lCustomer)
        {
            return
                IsCustomerNameEmpty(ref lCustomer) ||
                IsCustomerCodeDuplicated(ref lCustomer) ||
                IsCustomerCodeAlreadyExists(ref lCustomer);
        }

        private bool IsCustomerNameEmpty(ref List<Customer> lCustomer)
        {
            for (int i = 0; i < lCustomer.Count(); i++)
            {
                if (lCustomer[i].CtName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Customer name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private bool IsCustomerCodeDuplicated(ref List<Customer> lCustomer)
        {
            for (int i = 0; i < lCustomer.Count(); i++)
            {
                for (int j = (i + 1); j < lCustomer.Count(); j++)
                {
                    if (lCustomer[i].CtCode == lCustomer[j].CtCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Customer code " + lCustomer[i].CtCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsCustomerCodeAlreadyExists(ref List<Customer> lCustomer)
        {
            string mCtCode = string.Empty, mCustomerExisted = string.Empty;
            for (int i = 0; i < lCustomer.Count(); i++)
            {
                if (mCtCode.Length > 0) mCtCode += ",";
                mCtCode += lCustomer[i].CtCode;
            }

            if (mCtCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct CtCode) CtCode From TblCustomer Where Find_In_Set(CtCode, @Param); ");
                mCustomerExisted = Sm.GetValue(SQL.ToString(), mCtCode);

                if (mCustomerExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Customer code ( " + Sm.GetValue(SQL.ToString(), mCtCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private void InsertCustomer(Customer x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomer(CtCode, CtName, ActInd, AgentInd, CtCtCode, CtGrpCode, CtShortCode, CntCode, Address, CityCode, NPWP,  PostalCd, ");
            SQL.AppendLine("Phone, Fax, Mobile, NIB, ShippingMark, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @CtName, 'Y', @AgentInd, @CtCtCode, @CtGrpCode, @CtShortCode, @CntCode, ");
            SQL.AppendLine("@Address, @CityCode, @NPWP, @PostalCd, @Phone, @Fax, @Mobile, @NIB, @ShippingMark, @Remark, @UserCode, CurrentDateTime()); ");

            if (mGenerateCustomerCOAFormat == "1")
            {
                if (mCustomerAcNoDownPayment.Length > 0)
                {
                    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, ");
                    SQL.AppendLine("Trim(Concat(@CustomerAcDescDownPayment, ' ', @CtName)) As AcDesc, ");
                    if (mIsCustomerCOAUseCustomerCategory)
                    {
                        SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoDownPayment') As Parent, ");
                        SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoDownPayment') As Level, ");
                    }
                    else
                    {
                        SQL.AppendLine("If(Length(@CustomerAcNoDownPayment)<=0, '', substring(@CustomerAcNoDownPayment, 1, (Length(@CustomerAcNoDownPayment)-1)))  As Parent, ");
                        SQL.AppendLine("If(Length(@CustomerAcNoDownPayment)<=0, '', Length(@CustomerAcNoDownPayment)-Length(Replace(@CustomerAcNoDownPayment, '.', ''))+1) As Level, ");
                    }
                    SQL.AppendLine("'C', @UserCode, CurrentDateTime(); ");
                }

                if (mCustomerAcNoAR.Length > 0)
                {
                    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select Concat(@CustomerAcNoAR, @CtCode) As AcNo, ");
                    SQL.AppendLine("Trim(Concat(@CustomerAcDescAR, ' ', @CtName)) As AcDesc, ");
                    if (mIsCustomerCOAUseCustomerCategory)
                    {
                        SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoAR') As Parent, ");
                        SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoAR') As Level, ");
                    }
                    else
                    {
                        SQL.AppendLine("If(Length(@CustomerAcNoAR)<=0, '', substring(@CustomerAcNoAR, 1, (Length(@CustomerAcNoAR)-1))) As Parent, ");
                        SQL.AppendLine("If(Length(@CustomerAcNoAR)<=0, '', Length(@CustomerAcNoAR)-Length(Replace(@CustomerAcNoAR, '.', ''))+1) As Level, ");
                    }
                    SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                }

                if (mCustomerAcNoNonInvoice.Length > 0)
                {
                    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select Concat(@CustomerAcNoNonInvoice, @CtCode) As AcNo, ");
                    SQL.AppendLine("Trim(Concat(@CustomerAcDescNonInvoice, ' ', @CtName)) As AcDesc, ");
                    if (mIsCustomerCOAUseCustomerCategory)
                    {
                        SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoNonInvoice') As Parent, ");
                        SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoNonInvoice') As Level, ");
                    }
                    else
                    {
                        SQL.AppendLine("If(Length(@CustomerAcNoNonInvoice)<=0, '', substring(@CustomerAcNoNonInvoice, 1, (Length(@CustomerAcNoNonInvoice)-1))) As Parent, ");
                        SQL.AppendLine("If(Length(@CustomerAcNoNonInvoice)<=0, '', Length(@CustomerAcNoNonInvoice)-Length(Replace(@CustomerAcNoNonInvoice, '.', ''))+1) As Level, ");
                    }
                    SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                }

                if (mAcNoForGiroAR.Length > 0)
                {
                    SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select Concat(@AcNoForGiroAR, @CtCode) As AcNo, ");
                    SQL.AppendLine("Trim(Concat(@AcDescForGiroAR, ' ', @CtName)) As AcDesc, ");
                    if (mIsCustomerCOAUseCustomerCategory)
                    {
                        SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'AcNoForGiroAR') As Parent, ");
                        SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'AcNoForGiroAR') As Level, ");
                    }
                    else
                    {
                        SQL.AppendLine("If(Length(@AcNoForGiroAR)<=0, '', substring(@AcNoForGiroAR, 1, (Length(@AcNoForGiroAR)-1))) As Parent, ");
                        SQL.AppendLine("If(Length(@AcNoForGiroAR)<=0, '', Length(@AcNoForGiroAR)-Length(Replace(@AcNoForGiroAR, '.', ''))+1) As Level, ");
                    }
                    SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", x.CtCode);
            Sm.CmParam<String>(ref cm, "@CtName", x.CtName);
            Sm.CmParam<String>(ref cm, "@AgentInd", x.AgentInd);
            Sm.CmParam<String>(ref cm, "@CtCtCode", x.CtCtCode);
            Sm.CmParam<String>(ref cm, "@CtGrpCode", x.CtGrpCode);
            Sm.CmParam<String>(ref cm, "@CntCode", x.CntCode);
            Sm.CmParam<String>(ref cm, "@Address", x.Address);
            Sm.CmParam<String>(ref cm, "@CityCode", x.CityCode);
            Sm.CmParam<String>(ref cm, "@PostalCd", x.PostalCd);
            Sm.CmParam<String>(ref cm, "@Phone", x.Phone);
            Sm.CmParam<String>(ref cm, "@Fax", x.Fax);
            Sm.CmParam<String>(ref cm, "@Mobile", x.Mobile);
            Sm.CmParam<String>(ref cm, "@CtShortCode", x.CtShortCode);
            Sm.CmParam<String>(ref cm, "@NPWP", x.NPWP);
            Sm.CmParam<String>(ref cm, "@NIB", x.NIB);
            Sm.CmParam<String>(ref cm, "@ShippingMark", x.ShippingMark);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            if (mCustomerAcNoAR.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoAR", mCustomerAcNoAR);
            if (mCustomerAcDescAR.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescAR", mCustomerAcDescAR);
            if (mCustomerAcNoNonInvoice.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoNonInvoice", mCustomerAcNoNonInvoice);
            if (mCustomerAcDescNonInvoice.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescNonInvoice", mCustomerAcDescNonInvoice);
            if (mCustomerAcNoDownPayment.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoDownPayment", mCustomerAcNoDownPayment);
            if (mCustomerAcDescDownPayment.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescDownPayment", mCustomerAcDescDownPayment);
            if (mAcNoForGiroAR.Length > 0) Sm.CmParam<String>(ref cm, "@AcNoForGiroAR", mAcNoForGiroAR);
            if (mAcDescForGiroAR.Length > 0) Sm.CmParam<String>(ref cm, "@AcDescForGiroAR", mAcDescForGiroAR);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Employee

        private bool IsInsertedEmployeeInvalid(ref List<Employee> lEmployee)
        {
            return
                IsEmployeeNameEmpty(ref lEmployee) ||
                IsJoinDtEmpty(ref lEmployee) ||
                IsJoinDtFormatInvalid(ref lEmployee) ||
                (mIsEmpCodeUseContractDt && IsContractDtEmpty(ref lEmployee)) ||
                (mIsEmpCodeUseContractDt && IsContractDtFormatInvalid(ref lEmployee)) ||
                //IsEmployeeCodeDuplicated(ref lEmployee) ||
                //IsEmployeeCodeAlreadyExists(ref lEmployee) ||
                IsResignDtFormatInvalid(ref lEmployee) ||
                IsTGDtFormatInvalid(ref lEmployee) ||
                IsWeddingDtFormatInvalid(ref lEmployee) ||
                IsBirthDtFormatInvalid(ref lEmployee) ||
                IsEmploymentStatusInvalid(ref lEmployee)
                ;
        }

        private bool IsEmploymentStatusInvalid(ref List<Employee> lEmployee)
        {
            if (mGenerateEmpCodeFormat == "1") return false;
            int Row = 0;
            foreach (var x in lEmployee)
            {
                if (x.EmploymentStatus.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Employment status is empty at row #" + (Row + 1));
                    return true;
                }
                Row += 1;
            }

            return false;
        }

        private bool IsEmployeeNameEmpty(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].EmpName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Employee name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private bool IsEmployeeCodeDuplicated(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                for (int j = (i + 1); j < lEmployee.Count(); j++)
                {
                    if (lEmployee[i].EmpCode == lEmployee[j].EmpCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Employee code " + lEmployee[i].EmpCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsEmployeeCodeAlreadyExists(ref List<Employee> lEmployee)
        {
            string mEmpCode = string.Empty, mEmployeeExisted = string.Empty;
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (mEmpCode.Length > 0) mEmpCode += ",";
                mEmpCode += lEmployee[i].EmpCode;
            }

            if (mEmpCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct EmpCode)EmpCode From TblEmployee Where Find_In_Set(EmpCode, @Param); ");
                mEmployeeExisted = Sm.GetValue(SQL.ToString(), mEmpCode);

                if (mEmployeeExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Employee code ( " + Sm.GetValue(SQL.ToString(), mEmpCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private bool IsJoinDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].JoinDt.Contains("/") || lEmployee[i].JoinDt.Contains("-") || lEmployee[i].JoinDt.Contains(".") || lEmployee[i].JoinDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsContractDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].ContractDt.Contains("/") || lEmployee[i].ContractDt.Contains("-") || lEmployee[i].ContractDt.Contains(".") || lEmployee[i].ContractDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Contract date's format should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsResignDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].ResignDt.Contains("/") || lEmployee[i].ResignDt.Contains("-") || lEmployee[i].ResignDt.Contains(".") || lEmployee[i].ResignDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsTGDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].TGDt.Contains("/") || lEmployee[i].TGDt.Contains("-") || lEmployee[i].TGDt.Contains(".") || lEmployee[i].TGDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsWeddingDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].WeddingDt.Contains("/") || lEmployee[i].WeddingDt.Contains("-") || lEmployee[i].WeddingDt.Contains(".") || lEmployee[i].WeddingDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBirthDtFormatInvalid(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].BirthDt.Contains("/") || lEmployee[i].BirthDt.Contains("-") || lEmployee[i].BirthDt.Contains(".") || lEmployee[i].BirthDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsJoinDtEmpty(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].JoinDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Join date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsContractDtEmpty(ref List<Employee> lEmployee)
        {
            for (int i = 0; i < lEmployee.Count(); i++)
            {
                if (lEmployee[i].ContractDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Contract date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private void InsertEmployee(Employee x, bool IsAutoCreatePPS)
        {
            var SQL = new StringBuilder();
            string YYMM = string.Empty;

            if (mGenerateEmpCodeFormat == "1")
            {
                if (!mIsEmpContractDtMandatory)
                {
                    YYMM = x.JoinDt.Substring(2, 4);

                    SQL.AppendLine("Set @EmpCode := (Select Concat(@YYMM, ");
                    SQL.AppendLine("IfNull((Select Right(Concat('0000', Convert(EmpCodeTemp+1, Char)), 4) From ( ");
                    SQL.AppendLine("    Select Convert(Right(EmpCode, 5), Decimal) As EmpCodeTemp ");
                    SQL.AppendLine("    From TblEmployee Where Left(EmpCode, 4)=@YYMM Order By Right(EmpCode, 4) Desc Limit 1 ");
                    SQL.AppendLine(") As TblEmployeeTemp), '0001')) As EmpCode); ");
                }
                else
                {
                    YYMM = x.ContractDt.Substring(2, 4);

                    #region Alt
                    //string EmpStat = Sm.Right(string.Concat("00", x.EmploymentStatus), 2);
                    //string LastEmp = Sm.GetValue("Select ifnull( " +
                    //                                "(Select Right(Concat('00000', Convert(EmpCodeTemp+1, Char)), 5) From ( " +
                    //                                "    Select Convert(Right(EmpCode, 5), Decimal) As EmpCodeTemp  " +
                    //                                "    From TblEmployee  " +
                    //                                "    Where Substring(EmpCode, 3, 2) = '" + Sm.Left(YYMM, 2) + "' And EmploymentStatus = '" + x.EmploymentStatus + "' And Length(EmpCode) = 9  order by Right(EmpCode, 5) Desc Limit 1 " +
                    //                                ")As TblEmployeeTemp) " +
                    //                                ", '00001') ");
                    #endregion

                    SQL.AppendLine("Set @EmpCode := (SELECT CONCAT ( RIGHT(CONCAT('00', @EmploymentStatus), 2), left(@YYMM, 2), (Select ifnull( ");
                    SQL.AppendLine("(Select Right(Concat('00000', Convert(EmpCodeTemp+1, Char)), 5) From ( ");
                    SQL.AppendLine("    Select Convert(Right(EmpCode, 5), Decimal) As EmpCodeTemp ");
                    SQL.AppendLine("    From TblEmployee ");
                    SQL.AppendLine("    Where Substring(EmpCode, 3, 2) = left(@YYMM, 2) And EmploymentStatus = @EmploymentStatus ");
                    SQL.AppendLine("     And Length(EmpCode) = 9  order by Right(EmpCode, 5) Desc Limit 1 ");
                    SQL.AppendLine(")As TblEmployeeTemp) ");
                    SQL.AppendLine(", '00001')))As EmpCode); ");
                }
            }
            else
            {
                #region Old Code
                //SQL.AppendLine("Set @EmpCode := (Select concat(CONCAT( ");
                //SQL.AppendLine(" @EmploymentStatus, "); 
                //SQL.AppendLine("    IfNull( "); 
                //SQL.AppendLine("        (  ");
                //SQL.AppendLine("        Select Right(Concat('00', Convert(EmpCodeTemp + 1, Char)), 2)  ");
                //SQL.AppendLine("        From  ");
                //SQL.AppendLine("        (  ");
                //SQL.AppendLine("            Select Convert(Right(EmpCode, 2), Decimal) As EmpCodeTemp  ");
                //SQL.AppendLine("            From TblEmployee  ");
                //SQL.AppendLine("            Where Left(EmpCode, 8) = @EmploymentStatus  ");
                //SQL.AppendLine("            Order By Right(EmpCode, 2) Desc  ");
                //SQL.AppendLine("            Limit 1  ");
                //SQL.AppendLine("        ) T  ");
                //SQL.AppendLine("        )  ");
                //SQL.AppendLine("    , '01') ");
                //SQL.AppendLine("    ), LEFT(@JoinDt, 6), @EmploymentStatus) AS EmpCode); ");
                #endregion

                YYMM = Sm.Left(x.JoinDt, 6);

                SQL.AppendLine("Set @EmpCode := (Select Concat(@Param, ");
                SQL.AppendLine("    IfNull( ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("        Select Right(Concat('00', Convert(EmpCodeTemp + 1, Char)), 2) ");
                SQL.AppendLine("        From ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Convert(Right(EmpCode, 2), Decimal) As EmpCodeTemp ");
                SQL.AppendLine("            From TblEmployee ");
                SQL.AppendLine("            Where Left(EmpCode, 8) = @Param ");
                SQL.AppendLine("            Order By Right(EmpCode, 2) Desc ");
                SQL.AppendLine("            Limit 1 ");
                SQL.AppendLine("        ) T ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    , '01')) As EmpCode); ");
            }

            SQL.AppendLine("Insert Into TblEmployee(EmpCode, EmpName, DisplayName, UserCode, EmpCodeOld, ShortCode, DeptCode, PosCode, PositionStatusCode, ");
            SQL.AppendLine("JoinDt, ResignDt, ContractDt, TGDt, IdNumber, MaritalStatus, WeddingDt, Gender, Religion, Mother, BirthPlace, BirthDt, Address, ");
            SQL.AppendLine("CityCode, SubDistrict, Village, RTRW, PostalCode, EntCode, SiteCode, SectionCode, WorkGroupCode, Domicile, BloodType, Phone, Mobile, Email,GrdLvlCode,NPWP, SystemType, PayrollType, ");
            SQL.AppendLine("PTKP,BankCode,BankAcName,BankAcNo,BankBranch, ");
            SQL.AppendLine("PayrunPeriod, EmploymentStatus, DivisionCode, PGCode, POH, LeaveStartDt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @EmpName, @DisplayName, @UsCode, @EmpCodeOld, @ShortCode, @DeptCode, @PosCode, @PositionStatusCode, ");
            SQL.AppendLine("@JoinDt, @ResignDt, @ContractDt, @TGDt, @IdNumber, @MaritalStatus, @WeddingDt, @Gender, @Religion, @Mother, @BirthPlace, @BirthDt, @Address, ");
            SQL.AppendLine("@CityCode, @SubDistrict, @Village, @RTRW, @PostalCode, @EntCode, @SiteCode, @SectionCode, @WorkGroupCode, @Domicile, @BloodType, @Phone, @Mobile, @Email, @GrdLvlCode, @NPWP, @SystemType, @PayrollType,@PTKP,@BankCode,@BankAcName,@BankAcNo,@BankBranch, ");
            SQL.AppendLine("@PayrunPeriod, @EmploymentStatus, @DivisionCode, @PGCode, @POH, ");
            if (mIsAnnualLeaveUseStartDt)
                SQL.AppendLine("@LeaveStartDt, ");
            else
                SQL.AppendLine("Null, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            if (mIsEmployeeCOAInfoMandatory)
            {
                SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                SQL.AppendLine("Select Concat(@EmployeeAcNo, EmpCode) As AcNo, ");
                SQL.AppendLine("Trim(Concat(@EmployeeAcDesc, ' ', EmpCode, ' - ', EmpName)) As AcDesc, ");
                SQL.AppendLine("If(Length(@EmployeeAcNo)<=0, '', Left(@EmployeeAcNo, Length(@EmployeeAcNo)-1)) As Parent, ");
                SQL.AppendLine("If(Length(@EmployeeAcNo)<=0, '', Length(@EmployeeAcNo)-Length(Replace(@EmployeeAcNo, '.', ''))+1) As Level, ");
                SQL.AppendLine("'C', @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblEmployee ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And Concat(@EmployeeAcNo, EmpCode) Not In (Select AcNo from TblCOA); ");
            }

            if (mAcNoForAdvancePayment.Length > 0 && mAcDescForAdvancePayment.Length > 0)
            {
                SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                SQL.AppendLine("Select Concat(@AcNoForAdvancePayment, @EmpCode) As AcNo, ");
                SQL.AppendLine("Trim(Concat(@AcDescForAdvancePayment, ' ', @EmpCode)) As AcDesc, ");
                SQL.AppendLine("If(Length(@AcNoForAdvancePayment)<=0, '', Left(@AcNoForAdvancePayment, Length(@AcNoForAdvancePayment)-1)) As Parent, ");
                SQL.AppendLine("If(Length(@AcNoForAdvancePayment)<=0, '', Length(@AcNoForAdvancePayment)-Length(Replace(@AcNoForAdvancePayment, '.', ''))+2) As Level, ");
                SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
            }

            if (mIsPPSActive && IsAutoCreatePPS)
            {
                //SQL.AppendLine("Insert Into TblPPS ");
                //SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, Jobtransfer, ");
                //SQL.AppendLine("StartDt, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, DeptCodeOld, ");
                //SQL.AppendLine("PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, ");
                //SQL.AppendLine("PGCodeOld, SiteCodeOld, ResignDtOld, DeptCodeNew, PosCodeNew, PositionStatusCodeNew, ");
                //SQL.AppendLine("GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, ");
                //SQL.AppendLine("SiteCodeNew, ResignDtNew, UpdLeaveStartDtInd, Remark, CreateBy, ");
                //SQL.AppendLine("CreateDt) ");
                //SQL.AppendLine("Values ");
                //SQL.AppendLine("(@DocNo, @DocDt, 'N', 'A', 'N', ");
                //SQL.AppendLine("@JoinDt, @EmpCode, Null, Null, Null, ");
                //SQL.AppendLine("Null, Null, Null, Null, Null, ");
                //SQL.AppendLine("Null, Null, Null, Null, @DeptCode, @PosCode, @PositionStatusCode, ");
                //SQL.AppendLine("@GrdLvlCode, @EmploymentStatus, @SystemType, @PayrunPeriod, @PGCode, ");
                //SQL.AppendLine("@SiteCode, @ResignDt, 'N', Null, @UserCode, ");
                //SQL.AppendLine("CurrentDateTime()); ");

                SQL.AppendLine("Insert Into TblEmployeePPS ");
                SQL.AppendLine("(EmpCode, StartDt, EndDt, DeptCode, PosCode, ");
                SQL.AppendLine("GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ");
                SQL.AppendLine("ProcessInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Select EmpCode, JoinDt, Null, DeptCode, PosCode, ");
                SQL.AppendLine("GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, @PGCode, @SiteCode, ");
                SQL.AppendLine("'Y', CreateBy, CreateDt ");
                SQL.AppendLine("From TblEmployee Where EmpCode=@EmpCode;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            //Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@Param", string.Concat(YYMM, x.EmploymentStatus));
            Sm.CmParam<String>(ref cm, "@YYMM", YYMM);
            Sm.CmParam<String>(ref cm, "@EmpName", x.EmpName);
            Sm.CmParam<String>(ref cm, "@DisplayName", x.DisplayName);
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", x.EmploymentStatus);
            Sm.CmParam<String>(ref cm, "@UsCode", x.UserCode);
            Sm.CmParam<String>(ref cm, "@EmpCodeOld", x.EmpCodeOld);
            Sm.CmParam<String>(ref cm, "@ShortCode", x.ShortCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@PosCode", x.PosCode);
            Sm.CmParam<String>(ref cm, "@PositionStatusCode", x.PositionStatusCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", x.SiteCode);
            Sm.CmParam<String>(ref cm, "@SectionCode", x.SiteCode);
            Sm.CmParam<String>(ref cm, "@WorkGroupCode", x.WorkGroupCode);
            Sm.CmParam<String>(ref cm, "@Domicile", x.Domicile);
            Sm.CmParam<String>(ref cm, "@BloodType", x.BloodType);
            Sm.CmParam<String>(ref cm, "@JoinDt", x.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", x.ResignDt);
            Sm.CmParam<String>(ref cm, "@ContractDt", x.ContractDt);
            Sm.CmParam<String>(ref cm, "@TGDt", x.TGDt);
            Sm.CmParam<String>(ref cm, "@IdNumber", x.IDNumber);
            Sm.CmParam<String>(ref cm, "@MaritalStatus", x.MaritalStatus);
            Sm.CmParam<String>(ref cm, "@WeddingDt", x.WeddingDt);
            Sm.CmParam<String>(ref cm, "@Gender", x.Gender);
            Sm.CmParam<String>(ref cm, "@Religion", x.Religion);
            Sm.CmParam<String>(ref cm, "@Mother", x.Mother);
            Sm.CmParam<String>(ref cm, "@BirthPlace", x.BirthPlace);
            Sm.CmParam<String>(ref cm, "@BirthDt", x.BirthDt);
            Sm.CmParam<String>(ref cm, "@Address", x.Address);
            Sm.CmParam<String>(ref cm, "@CityCode", x.CityCode);
            Sm.CmParam<String>(ref cm, "@SubDistrict", x.SubDistrict);
            Sm.CmParam<String>(ref cm, "@Village", x.Village);
            Sm.CmParam<String>(ref cm, "@RTRW", x.RTRW);
            Sm.CmParam<String>(ref cm, "@PostalCode", x.PostalCode);
            Sm.CmParam<String>(ref cm, "@Phone", x.Phone);
            Sm.CmParam<String>(ref cm, "@Mobile", x.Mobile);
            Sm.CmParam<String>(ref cm, "@Email", x.Email);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", x.GrdLvlCode);
            Sm.CmParam<String>(ref cm, "@NPWP", x.NPWP);
            Sm.CmParam<String>(ref cm, "@SystemType", x.SystemType);
            Sm.CmParam<String>(ref cm, "@PayrollType", x.PayrollType);
            Sm.CmParam<String>(ref cm, "@PTKP", x.PTKP);
            Sm.CmParam<String>(ref cm, "@BankCode", x.BankCode);
            Sm.CmParam<String>(ref cm, "@BankBranch", x.BankBranch);
            Sm.CmParam<String>(ref cm, "@BankAcName", x.BankAcName);
            Sm.CmParam<String>(ref cm, "@BankAcNo", x.BankAcNo);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", x.PayrunPeriod);
            if (mIsEmployeeCOAInfoMandatory)
            {
                Sm.CmParam<String>(ref cm, "@EmployeeAcNo", mEmployeeAcNo);
                Sm.CmParam<String>(ref cm, "@EmployeeAcDesc", mEmployeeAcDesc);
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", x.DivisionCode);
            Sm.CmParam<String>(ref cm, "@PGCode", x.PGCode);
            Sm.CmParam<String>(ref cm, "@POH", x.POH);

            if (mIsAnnualLeaveUseStartDt) Sm.CmParamDt(ref cm, "@LeaveStartDt", x.LeaveStartDt);
            if (mAcNoForAdvancePayment.Length > 0) Sm.CmParam<String>(ref cm, "@AcNoForAdvancePayment", mAcNoForAdvancePayment);
            if (mAcDescForAdvancePayment.Length > 0) Sm.CmParam<String>(ref cm, "@AcDescForAdvancePayment", mAcDescForAdvancePayment);

            if (mIsPPSActive && IsAutoCreatePPS)
            {
                var DocDt = Sm.Left(Sm.ServerCurrentDateTime(), 8);
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GenerateDocNo(DocDt, "PPS", "TblPPS"));
                Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
            }

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Item

        private bool IsInsertedItemInvalid(ref List<Item> lItem)
        {
            return
                IsItemNameEmpty(ref lItem) ||
                IsCategoryEmpty(ref lItem) ||
                IsPurchaseUomCodeEmpty(ref lItem) ||
                IsInventoryUomCodeEmpty(ref lItem) ||
                IsInventoryUomCode2Empty(ref lItem) ||
                IsInventoryUomCode3Empty(ref lItem) ||
                IsMinStockEmpty(ref lItem) ||
                IsMaxStockEmpty(ref lItem) ||
                IsReOrderStockEmpty(ref lItem) ||
                IsSalesUomCodeEmpty(ref lItem) ||
                IsSalesUomCode2Empty(ref lItem) ||
                IsSalesConvertUom1Empty(ref lItem) ||
                IsSalesConvertUom2Empty(ref lItem) ||
                IsPlanningUomCodeEmpty(ref lItem) ||
                IsPlanningUomCode2Empty(ref lItem) ||
                IsPlanningUomCodeConvert12Empty(ref lItem) ||
                IsPlanningUomCodeConvert21Empty(ref lItem) ||
                IsItemCodeAlreadyExists(ref lItem) ||
                IsItemCodeDuplicated(ref lItem);
        }

        private bool IsItemNameEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].ItName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCategoryEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].ItCtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item Category is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPurchaseUomCodeEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].PurchaseUOMCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Purchase UOM Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsInventoryUomCodeEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].InventoryUOMCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Inventory UOM Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsInventoryUomCode2Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].InventoryUOMCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Inventory UOM Code 2 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsInventoryUomCode3Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].InventoryUOMCode3.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Inventory UOM Code 3 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsMinStockEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].MinStock <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Min Stock is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsMaxStockEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].MaxStock <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Max Stock is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsReOrderStockEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].ReOrderStock <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "ReOrder Stock is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsSalesUomCodeEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].SalesUOMCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Sales UOM Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsSalesUomCode2Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].SalesUOMCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Sales UOM Code 2 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsSalesConvertUom1Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].SalesUomCodeConvert12 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Sales UOM Code Convert12  is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsSalesConvertUom2Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].SalesUomCodeConvert21 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Sales UOM Code Convert21  is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPlanningUomCodeEmpty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].PlanningUomCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Planning Uom Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPlanningUomCode2Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].PlanningUomCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Planning Uom Code 2 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPlanningUomCodeConvert12Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].PlanningUomCodeConvert12 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Planning Uom Code Convert12 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPlanningUomCodeConvert21Empty(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (lItem[i].PlanningUomCodeConvert21 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Planning Uom Code Convert21 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCodeDuplicated(ref List<Item> lItem)
        {
            for (int i = 0; i < lItem.Count(); i++)
            {
                for (int j = (i + 1); j < lItem.Count(); j++)
                {
                    if (lItem[i].ItCode == lItem[j].VdCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Item code " + lItem[i].ItCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsItemCodeAlreadyExists(ref List<Item> lItem)
        {
            string mItCode = string.Empty, mItExisted = string.Empty;
            for (int i = 0; i < lItem.Count(); i++)
            {
                if (mItCode.Length > 0) mItCode += ",";
                mItCode += lItem[i].VdCode;
            }

            if (mItCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct ItCode Separator '\r\n') ItCode From TblItem Where Find_In_Set(ItCode, @Param); ");
                mItExisted = Sm.GetValue(SQL.ToString(), mItCode);

                if (mItExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Item code ( " + Sm.GetValue(SQL.ToString(), mItCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private void InsertItem(Item x)
        {
            var SQL = new StringBuilder();

            if (mItCodeFormat == "2")
            {
                if (mIsItCodeUseItSeqNo)
                {
                    SQL.AppendLine("Set @ItCode := (Select Concat(@Param1,'-', ");
                    SQL.AppendLine("IfNull((Select Right(Concat(Repeat('0', @ItCodeSeqNo), Convert(V+1, Char)), @ItCodeSeqNo ) From ( ");
                    SQL.AppendLine("Select ItSeqNo As V ");
                    SQL.AppendLine("From TblItem ");
                    SQL.AppendLine("Where Left(ItCode, @Param2)=Concat(@Param1, '-') ");
                    SQL.AppendLine("Order By ItSeqNo Desc Limit 1 ");
                    SQL.AppendLine(") As Tbl), Right(Concat(Repeat('0', @ItCodeSeqNo ), '1'), @ItCodeSeqNo ) ");
                    SQL.AppendLine(")) As ItCode);  ");
                }
                else
                {
                    SQL.AppendLine("Set @ItCode := (Select Concat(@Param1,'-', ");
                    SQL.AppendLine("IfNull((Select Right(Concat('00000', Convert(ItCode+1, Char)), 5) From ( ");
                    SQL.AppendLine("Select Convert(Right(ItCode, 5), Decimal) As ItCode  ");
                    SQL.AppendLine("From TblItem ");
                    SQL.AppendLine("Where Left(ItCode, @Param2)=Concat(@Param1, '-') ");
                    SQL.AppendLine("Order By Right(ItCode, 5) Desc Limit 1 ");
                    SQL.AppendLine(") As TblItemTemp), '00001')) As ItCode); ");

                }
            }

            SQL.AppendLine("Insert Into TblItem ");
            SQL.AppendLine("(ItCode, ItName, ActInd, ItSeqNo, HSCode, ItCodeInternal, ForeignName, InventoryItemInd, SalesItemInd,  ");
            SQL.AppendLine("PurchaseItemInd, ServiceItemInd, FixedItemInd, PlanningItemInd, ItCtCode, ItScCode,  ");
            SQL.AppendLine("ItBrCode, ItGrpCode, ItemRequestDocNo, Specification, Remark, TaxLiableInd, VdCode, PurchaseUOMCode, ");
            SQL.AppendLine("PGCode, MinOrder, TaxCode1, TaxCode2, TaxCode3, Length, Height, Width, Volume, Diameter, LengthUomCode, HeightUomCode, WidthUomCode, VolumeUomCode, DiameterUomCode, ");
            SQL.AppendLine("SalesUOMCode, SalesUOMCode2, SalesUomCodeConvert12, SalesUomCodeConvert21, InventoryUOMCode, InventoryUOMCode2, InventoryUOMCode3, InventoryUomCodeConvert12, InventoryUomCodeConvert13, ");
            SQL.AppendLine("InventoryUomCodeConvert21, InventoryUomCodeConvert23, InventoryUomCodeConvert31, InventoryUomCodeConvert32, MinStock, MaxStock, ReOrderStock, ControlByDeptCode, PlanningUomCode, PlanningUomCode2, PlanningUomCodeConvert12,  PlanningUomCodeConvert21, ");
            SQL.AppendLine("Information1, Information2, Information3, Information4, Information5, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ItCode, @ItName, 'Y', @ItSeqNo, @HSCode, @ItCodeInternal, @ForeignName, 'Y', @SalesItemInd,  ");
            SQL.AppendLine("'Y', @ServiceItemInd, @FixedItemInd, @PlanningItemInd, @ItCtCode, @ItScCode, ");
            SQL.AppendLine("@ItBrCode, @ItGrpCode, @ItemRequestDocNo, @Spesification, @Remark, @TaxLiableInd, @VdCode, @PurchaseUOMCode,  ");
            SQL.AppendLine("@PGCode, @MinOrder, @TaxCode1, @TaxCode2, @TaxCOde3, @Length, @Height, @Width, @Volume, @Diameter, @LengthUomCode, @HeightUomCode, @WidthUomCode, @VolumeUomCode, @DiameterUomCode, ");
            SQL.AppendLine("@SalesUOMCode, @SalesUOMCode2, @SalesUomCodeConvert12, @SalesUomCodeConvert21, @InventoryUOMCode, @InventoryUOMCode2, @InventoryUOMCode3, @InventoryUomCodeConvert12, @InventoryUomCodeConvert13, ");
            SQL.AppendLine("@InventoryUomCodeConvert21, @InventoryUomCodeConvert23, @InventoryUomCodeConvert31, @InventoryUomCodeConvert32, @MinStock, @MaxStock, @ReOrderStock, @ControlByDeptCode, @PlanningUomCode, @PlanningUomCode2, @PlanningUomCodeConvert12,  @PlanningUomCodeConvert21, ");
            SQL.AppendLine("@Information1, @Information2,@Information3, @Information4, @Information5, @CreateBy, CurrentDateTime()); ");

            if (x.PackagingUomCode.Length > 0)
            {
                SQL.AppendLine("Insert Into TblItemPackagingUnit(ItCode, UomCode, Qty, Qty2, NW, GW, Length, Width, Height, Volume, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItCode, @PackagingUomCode, @Qty, @Qty2, @NetWeight, @GrossWeight, @PackagingLength, @PackagingWidth, @PackagingHeight, @PackagingVolume, @CreateBy, CurrentDateTime());");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };
            if (mItCodeFormat == "1")
                Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            else
            {
                Sm.CmParam<String>(ref cm, "@Param1", x.ItCtCode);
                Sm.CmParam<String>(ref cm, "@Param2", (x.ItCtCode.Length + 1).ToString());
            }
            Sm.CmParam<String>(ref cm, "@ItName", x.ItName);
            if (mIsItCodeUseItSeqNo)
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", decimal.Parse(Sm.Right(x.ItCode, int.Parse(mItCodeSeqNo))));
            else
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", 0m);
            Sm.CmParam<String>(ref cm, "@HSCode", x.HSCode);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", x.ItCodeInternal);
            Sm.CmParam<String>(ref cm, "@ForeignName", x.ForeignName);
            Sm.CmParam<String>(ref cm, "@SalesItemInd", x.SalesItemInd);
            Sm.CmParam<String>(ref cm, "@ServiceItemInd", x.ServiceItemInd);
            Sm.CmParam<String>(ref cm, "@FixedItemInd", x.FixedItemInd);
            Sm.CmParam<String>(ref cm, "@PlanningItemInd", x.PlanningItemInd);
            Sm.CmParam<String>(ref cm, "@ItCtCode", x.ItCtCode);
            Sm.CmParam<String>(ref cm, "@ItScCode", x.ItScCode);
            Sm.CmParam<String>(ref cm, "@ItBrCode", x.ItBrCode);
            Sm.CmParam<String>(ref cm, "@ItGrpCode", x.ItGrpCode);
            Sm.CmParam<String>(ref cm, "@ItemRequestDocNo", x.ItemRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Specification", x.Specification);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@TaxLiableInd", x.TaxLiableInd);
            Sm.CmParam<String>(ref cm, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm, "@PurchaseUOMCode", x.PurchaseUOMCode);
            Sm.CmParam<String>(ref cm, "@PGCode", x.PGCode);
            Sm.CmParam<Decimal>(ref cm, "@MinOrder", x.MinOrder);
            Sm.CmParam<String>(ref cm, "@TaxCode1", x.TaxCode1);
            Sm.CmParam<String>(ref cm, "@TaxCode2", x.TaxCode2);
            Sm.CmParam<String>(ref cm, "@TaxCode3", x.TaxCode3);
            Sm.CmParam<Decimal>(ref cm, "@Length", x.Length);
            Sm.CmParam<Decimal>(ref cm, "@Height", x.Height);
            Sm.CmParam<Decimal>(ref cm, "@Width", x.Width);
            Sm.CmParam<Decimal>(ref cm, "@Volume", x.Volume);
            Sm.CmParam<Decimal>(ref cm, "@Diameter", x.Diameter);
            Sm.CmParam<String>(ref cm, "@LengthUomCode", x.LengthUomCode);
            Sm.CmParam<String>(ref cm, "@HeightUomCode", x.HeightUomCode);
            Sm.CmParam<String>(ref cm, "@WidthUomCode", x.WidthUomCode);
            Sm.CmParam<String>(ref cm, "@VolumeUomCode", x.VolumeUomCode);
            Sm.CmParam<String>(ref cm, "@DiameterUomCode", x.DiameterUomCode);
            Sm.CmParam<String>(ref cm, "@SalesUOMCode", x.SalesUOMCode);
            Sm.CmParam<String>(ref cm, "@SalesUOMCode2", x.SalesUOMCode2);
            Sm.CmParam<Decimal>(ref cm, "@SalesUomCodeConvert12", x.SalesUomCodeConvert12);
            Sm.CmParam<Decimal>(ref cm, "@SalesUomCodeConvert21", x.SalesUomCodeConvert21);
            Sm.CmParam<String>(ref cm, "@InventoryUOMCode", x.InventoryUOMCode);
            Sm.CmParam<String>(ref cm, "@InventoryUOMCode2", x.InventoryUOMCode2);
            Sm.CmParam<String>(ref cm, "@InventoryUOMCode3", x.InventoryUOMCode3);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert12", x.InventoryUomCodeConvert12);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert13", x.InventoryUomCodeConvert13);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert21", x.InventoryUomCodeConvert21);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert23", x.InventoryUomCodeConvert23);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert31", x.InventoryUomCodeConvert31);
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert32", x.InventoryUomCodeConvert32);
            Sm.CmParam<Decimal>(ref cm, "@MinStock", x.MinStock);
            Sm.CmParam<Decimal>(ref cm, "@MaxStock", x.MaxStock);
            Sm.CmParam<Decimal>(ref cm, "@ReOrderStock", x.ReOrderStock);
            Sm.CmParam<String>(ref cm, "@ControlByDeptCode", x.ControlByDeptCode);
            Sm.CmParam<String>(ref cm, "@PlanningUomCode", x.PlanningUomCode);
            Sm.CmParam<String>(ref cm, "@PlanningUomCode2", x.PlanningUomCode2);
            Sm.CmParam<Decimal>(ref cm, "@PlanningUomCodeConvert12", x.PlanningUomCodeConvert12);
            Sm.CmParam<Decimal>(ref cm, "@PlanningUomCodeConvert21", x.PlanningUomCodeConvert21);
            Sm.CmParam<String>(ref cm, "@Information1", x.Information1);
            Sm.CmParam<String>(ref cm, "@Information2", x.Information2);
            Sm.CmParam<String>(ref cm, "@Information3", x.Information3);
            Sm.CmParam<String>(ref cm, "@Information4", x.Information4);
            Sm.CmParam<String>(ref cm, "@Information5", x.Information5);
            Sm.CmParam<String>(ref cm, "@PackagingUomCode", x.PackagingUomCode);
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.ConversionRate1);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", x.ConversionRate2);
            Sm.CmParam<Decimal>(ref cm, "@NetWeight", x.NetWeight);
            Sm.CmParam<Decimal>(ref cm, "@GrossWeight", x.GrossWeight);
            Sm.CmParam<Decimal>(ref cm, "@PackagingLength", x.PackagingLength);
            Sm.CmParam<Decimal>(ref cm, "@PackagingWidth", x.PackagingWidth);
            Sm.CmParam<Decimal>(ref cm, "@PackagingHeight", x.PackagingHeight);
            Sm.CmParam<Decimal>(ref cm, "@PackagingVolume", x.PackagingLength * x.PackagingWidth * x.PackagingHeight);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Item (Custom for KSM)

        private bool IsInsertedItemInvalid(ref List<ItemKSM> l)
        {
            return IsItemInfoInvalid(ref l);
        }

        private bool IsItemInfoInvalid(ref List<ItemKSM> l)
        {
            var ItCtCode = string.Empty;
            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].ItName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item name is empty at row " + (i + 1) + ".");
                    return true;
                }
                if (l[i].ItCtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item's category is empty at row " + (i + 1) + ".");
                    return true;
                }
                if (l[i].InventoryUomCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Uom is empty at row " + (i + 1) + ".");
                    return true;
                }
                if (i != 0 && !Sm.CompareStr(ItCtCode, l[i].ItCtCode))
                {
                    Sm.StdMsg(mMsgType.Info,
                        "Category (1) : " + ItCtCode + Environment.NewLine +
                        "Category (2) : " + l[i].ItCtCode + Environment.NewLine +
                        "You need to process data with the same item's category.");
                    return true;
                }
                ItCtCode = l[i].ItCtCode;
            }
            return false;
        }

        private void InsertItem(List<ItemKSM> l)
        {
            var cml = new List<MySqlCommand>();
            decimal n = 1m;

            string SeqNo = Sm.GetValue("Select Right(ItCode, 5) From TblItem Where ItCtCode=@Param Order By Right(ItCode, 5) Desc;", l[0].ItCtCode);
            if (SeqNo.Length > 0) n = decimal.Parse(SeqNo) + 1;

            l.ForEach(i =>
            {
                cml.Add(InsertItem(i, n));
                n++;
            });
            Sm.ExecCommands(cml);
            Sm.StdMsg(mMsgType.Info, l.Count() + " item(s) has been inserted.");
        }

        private MySqlCommand InsertItem(ItemKSM x, decimal n)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItem ");
            SQL.AppendLine("(ItCode, ItName, ActInd, ItSeqNo, ItCodeOld, ");
            SQL.AppendLine("ItCodeInternal, ForeignName, InventoryItemInd, ItCtCode, ItScCode,  ");
            SQL.AppendLine("ItGrpCode, Specification, Remark, PurchaseUOMCode, SalesUOMCode, ");
            SQL.AppendLine("SalesUOMCode2, SalesUomCodeConvert12, SalesUomCodeConvert21, InventoryUOMCode, InventoryUOMCode2, ");
            SQL.AppendLine("InventoryUOMCode3, InventoryUomCodeConvert12, InventoryUomCodeConvert13, InventoryUomCodeConvert21, InventoryUomCodeConvert23, ");
            SQL.AppendLine("InventoryUomCodeConvert31, InventoryUomCodeConvert32, PlanningUomCode, PlanningUomCode2, PlanningUomCodeConvert12,  ");
            SQL.AppendLine("PlanningUomCodeConvert21, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@ItCode, @ItName, 'Y', @ItSeqNo, @ItCodeOld, ");
            SQL.AppendLine("@ItCodeInternal, @ForeignName, 'Y', @ItCtCode, @ItScCode,  ");
            SQL.AppendLine("@ItGrpCode, @Specification, @Remark, @UomCode, @UomCode, ");
            SQL.AppendLine("@UomCode, 1.00, 1.00, @UomCode, @UomCode2, ");
            SQL.AppendLine("@UomCode3, 1.00, 1.00, 1.00, 1.00, ");
            SQL.AppendLine("1.00, 1.00, @UomCode, @UomCode, 1.00,  ");
            SQL.AppendLine("1.00, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ItCode", string.Concat(x.ItCtCode, "-", Sm.Right(string.Concat("00000", n.ToString()), 5)));
            Sm.CmParam<String>(ref cm, "@ItName", x.ItName);
            Sm.CmParam<String>(ref cm, "@ItCodeOld", x.ItCodeOld);
            Sm.CmParam<decimal>(ref cm, "@ItSeqNo", n);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", x.ItCodeInternal);
            Sm.CmParam<String>(ref cm, "@ForeignName", x.ForeignName);
            Sm.CmParam<String>(ref cm, "@ItCtCode", x.ItCtCode);
            Sm.CmParam<String>(ref cm, "@ItScCode", x.ItScCode);
            Sm.CmParam<String>(ref cm, "@ItGrpCode", x.ItGrpCode);
            Sm.CmParam<String>(ref cm, "@Specification", x.Specification);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@UomCode", x.InventoryUomCode);
            Sm.CmParam<String>(ref cm, "@UomCode2", x.InventoryUomCode2);
            Sm.CmParam<String>(ref cm, "@UomCode3", x.InventoryUomCode3);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return (cm);
        }

        #endregion

        #region Employee SS

        private bool IsInsertedEmployeeSSInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            return
                IsDocDtEmpty(ref lEmployeeSS) ||
                IsEmpCodeEmpty(ref lEmployeeSS) ||
                IsDocDtFormatInvalid(ref lEmployeeSS) ||
                IsStartDtFormatInvalid(ref lEmployeeSS) ||
                IsEndDtFormatInvalid(ref lEmployeeSS) ||
                IsStartDt2FormatInvalid(ref lEmployeeSS) ||
                IsEndDt2FormatInvalid(ref lEmployeeSS) ||
                IsBirthDtFormatInvalid(ref lEmployeeSS);
        }

        private bool IsEmpCodeEmpty(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].EmpCode == "00000000")
                {
                    Sm.StdMsg(mMsgType.Info, "Employee Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtEmpty(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].DocDt.Contains("/") || lEmployeeSS[i].DocDt.Contains("-") || lEmployeeSS[i].DocDt.Contains(".") || lEmployeeSS[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDtFormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].StartDt.Contains("/") || lEmployeeSS[i].StartDt.Contains("-") || lEmployeeSS[i].StartDt.Contains(".") || lEmployeeSS[i].StartDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEndDtFormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].EndDt.Contains("/") || lEmployeeSS[i].EndDt.Contains("-") || lEmployeeSS[i].EndDt.Contains(".") || lEmployeeSS[i].EndDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDt2FormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].StartDt2.Contains("/") || lEmployeeSS[i].StartDt2.Contains("-") || lEmployeeSS[i].StartDt2.Contains(".") || lEmployeeSS[i].StartDt2.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEndDt2FormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].EndDt2.Contains("/") || lEmployeeSS[i].EndDt2.Contains("-") || lEmployeeSS[i].EndDt2.Contains(".") || lEmployeeSS[i].EndDt2.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBirthDtFormatInvalid(ref List<EmployeeSS> lEmployeeSS)
        {
            for (int i = 0; i < lEmployeeSS.Count(); i++)
            {
                if (lEmployeeSS[i].BirthDt.Contains("/") || lEmployeeSS[i].BirthDt.Contains("-") || lEmployeeSS[i].BirthDt.Contains(".") || lEmployeeSS[i].BirthDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private void SetIndex(ref List<EmployeeSS> lEmployeeSS)
        {
            string mEmpCode = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lEmployeeSS.Count; i++)
            {
                lEmployeeSS[i].Mth = (lEmployeeSS[i].DocDt).Substring(4, 2);
                lEmployeeSS[i].Yr = (lEmployeeSS[i].DocDt).Substring(0, 4);

                if (lEmployeeSS[i].EmpCode != mEmpCode || lEmployeeSS[i].DocDt != mDocDt)
                {
                    if (lEmployeeSS[i].Mth != mMth || lEmployeeSS[i].Yr != mYr)
                    {
                        mIndex = 1;
                        mEmpCode = lEmployeeSS[i].EmpCode;
                        mDocDt = lEmployeeSS[i].DocDt;
                        mYr = (lEmployeeSS[i].DocDt).Substring(0, 4);
                        mMth = (lEmployeeSS[i].DocDt).Substring(4, 2);
                    }
                    else mIndex++;

                }
                lEmployeeSS[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<EmployeeSS> lEmployeeSS)
        {
            string mDocNo = string.Empty, mEmpCode = string.Empty, mDocDt = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lEmployeeSS.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    lEmployeeSS[i].DocNo = GenerateDocNo(lEmployeeSS[i].Index, lEmployeeSS[i].DocDt, "EmpSS", "TblEmpSSHdr");
                    mDocNo = lEmployeeSS[i].DocNo;
                    mEmpCode = lEmployeeSS[i].EmpCode;
                    mDocDt = lEmployeeSS[i].DocDt;

                }
                else
                {
                    if (mEmpCode != lEmployeeSS[i].EmpCode || mDocDt != lEmployeeSS[i].DocDt)
                    {
                        lEmployeeSS[i].DocNo = GenerateDocNo(lEmployeeSS[i].Index, lEmployeeSS[i].DocDt, "EmpSS", "TblEmpSSHdr");
                        mDocNo = lEmployeeSS[i].DocNo;
                        mEmpCode = lEmployeeSS[i].EmpCode;
                        mDocDt = lEmployeeSS[i].DocDt;
                    }
                    else
                    {
                        lEmployeeSS[i].DocNo = mDocNo;
                    }
                }
                lEmployeeSS[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<EmployeeSS> l, ref List<EmployeeSSHdr> l2, ref List<EmployeeSSDtl> l3, ref List<EmployeeSSDtl2> l4)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l
                .Select(x => new { x.DocNo, x.DocDt, x.EmpCode, x.EntCode, x.Remark })
                .OrderBy(y => y.DocNo)
                )

                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new EmployeeSSHdr()
                    {
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        EmpCode = m.EmpCode,
                        EntCode = m.EntCode,
                        Remark = m.Remark
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new EmployeeSSHdr()
                        {
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            EmpCode = m.EmpCode,
                            EntCode = m.EntCode,
                            Remark = m.Remark
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new { x.DocNo, x.EmpCode, x.DNo, x.SSCode, x.CardNo, x.HealthFacility, x.StartDt, x.EndDt, x.Problem, x.RemarkD1, x.EntCode })
                .OrderBy(o => o.DocNo)
               )
                if (n.SSCode.Trim().Length > 0 || n.CardNo.Trim().Length > 0 || n.HealthFacility.Trim().Length > 0 || n.StartDt.Trim().Length > 0
                    || n.EndDt.Trim().Length > 0 || n.Problem.Trim().Length > 0 || n.RemarkD1.Trim().Length > 0)
                {
                    l3.Add(new EmployeeSSDtl()
                    {
                        DocNo = n.DocNo,
                        EmpCode = n.EmpCode,
                        DNo = n.DNo,
                        SSCode = n.SSCode,
                        CardNo = n.CardNo,
                        HealthFacility = n.HealthFacility,
                        StartDt = n.StartDt,
                        EndDt = n.EndDt,
                        Problem = n.Problem,
                        RemarkD1 = n.RemarkD1,
                        EntCode = n.EntCode

                    });
                }

            foreach (var o in l.Select(x => new
            {
                x.DocNo,
                x.EmpCode,
                x.DNo2,
                x.FamilyName,
                x.Status,
                x.Gender,
                x.IDNo,
                x.NIN,
                x.SSCode2,
                x.CardNo2,
                x.HealthFacility2,
                x.StartDt2,
                x.EndDt2,
                x.BirthDt,
                x.Problem2,
                x.RemarkD2,
                x.EntCode
            }).OrderBy(o => o.DocNo)
               )
                if (o.FamilyName.Trim().Length > 0 || o.Status.Trim().Length > 0 || o.Gender.Trim().Length > 0 || o.IDNo.Trim().Length > 0 || o.NIN.Trim().Length > 0 || o.SSCode2.Trim().Length > 0 || o.CardNo2.Trim().Length > 0 ||
                     o.HealthFacility2.Trim().Length > 0 || o.StartDt2.Trim().Length > 0 || o.EndDt2.Trim().Length > 0 || o.BirthDt.Trim().Length > 0 || o.Problem2.Trim().Length > 0 || o.RemarkD2.Trim().Length > 0)
                {
                    l4.Add(new EmployeeSSDtl2()
                    {
                        DocNo = o.DocNo,
                        EmpCode = o.EmpCode,
                        DNo2 = o.DNo2,
                        FamilyName = o.FamilyName,
                        Status = o.Status,
                        Gender = o.Gender,
                        IDNo = o.IDNo,
                        NIN = o.NIN,
                        SSCode2 = o.SSCode2,
                        CardNo2 = o.CardNo2,
                        HealthFacility2 = o.HealthFacility2,
                        StartDt2 = o.StartDt2,
                        EndDt2 = o.EndDt2,
                        BirthDt = o.BirthDt,
                        Problem2 = o.Problem2,
                        RemarkD2 = o.RemarkD2,
                        EntCode = o.EntCode

                    });
                }
        }

        private void ProcessDtl1(ref List<EmployeeSSDtl> lEmployeeSSDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lEmployeeSSDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = lEmployeeSSDtl[i].DocNo;
                    lEmployeeSSDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lEmployeeSSDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = lEmployeeSSDtl[i].DocNo;

                    }

                    lEmployeeSSDtl[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl2(ref List<EmployeeSSDtl2> lEmployeeSSDtl2)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lEmployeeSSDtl2.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = lEmployeeSSDtl2[i].DocNo;
                    lEmployeeSSDtl2[i].DNo2 = mDNo;
                }
                else
                {
                    if (mDocNo == lEmployeeSSDtl2[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = lEmployeeSSDtl2[i].DocNo;

                    }

                    lEmployeeSSDtl2[i].DNo2 = mDNo;
                }
            }
        }

        private MySqlCommand InsertEmployeeSSHdr(EmployeeSSHdr x)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Into TblEmpSSHdr(DocNo, DocDt, EmpCode, EntCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@DocNo, @DocDt, @EmpCode, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblEmployee Set RegEntCode=@EntCode Where EmpCode=@EmpCode;");

            //SQL.AppendLine("Delete From TblEmployeeSS Where EmpCode=@EmpCode;");

            //SQL.AppendLine("Delete From TblEmployeeFamilySS Where EmpCode=@EmpCode;");


            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };
            //Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            //Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            //Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            //Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertEmployeeSSDtl(EmployeeSSDtl y)
        {
            var SQL = new StringBuilder();


            //SQL.AppendLine("Insert Into TblEmpSSDtl(DocNo, DNo, SSCode, CardNo, HealthFacility, StartDt, EndDt, Problem, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@DocNo, @DNo, @SSCode, @CardNo, @HealthFacility, @StartDt, @EndDt, @Problem, @Remark, @CreateBy, CurrentDateTime()); ");

            //SQL.AppendLine("Insert Into TblEmployeeSS(EmpCode, DNo, SSCode, CardNo, HealthFacility, StartDt, EndDt, Problem, EntCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@EmpCode,@DNo, @SSCode, @CardNo, @HealthFacility, @StartDt, @EndDt, @Problem, @EntCode, @Remark, @CreateBy, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("Update SSCode = @SSCode, CardNo= @CardNo, HealthFacility= @HealthFacility, StartDt= @StartDt, EndDt= @EndDt, Problem= @Problem, EntCode= @EntCode, Remark = @Remark; ");

            SQL.AppendLine("Set @DNo:=");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(DNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(DNo, Decimal) As DNo From TblEmployeeSS ");
            SQL.Append("       Where EmpCode=@EmpCode ");
            SQL.Append("       Order By DNo Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001');");

            SQL.AppendLine("Insert Into TblEmployeeSS(EmpCode, DNo, SSCode, CardNo, HealthFacility, StartDt, EndDt, Problem, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode,@DNo, @SSCode, @CardNo, @HealthFacility, @StartDt, @EndDt, @Problem, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("Update SSCode = @SSCode, CardNo= @CardNo, HealthFacility= @HealthFacility, StartDt= @StartDt, EndDt= @EndDt, Problem= @Problem, EntCode= @EntCode, Remark = @Remark; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", y.EmpCode);
            //Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@SSCode", y.SSCode);
            Sm.CmParam<String>(ref cm, "@CardNo", y.CardNo);
            Sm.CmParam<String>(ref cm, "@HealthFacility", y.HealthFacility);
            Sm.CmParam<String>(ref cm, "@StartDt", y.StartDt);
            Sm.CmParam<String>(ref cm, "@EndDt", y.EndDt);
            Sm.CmParam<String>(ref cm, "@Problem", y.Problem);
            Sm.CmParam<String>(ref cm, "@Remark", y.RemarkD1);
            Sm.CmParam<String>(ref cm, "@EntCode", y.EntCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand InsertEmployeeSSDtl2(EmployeeSSDtl2 z)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Insert Into TblEmpSSDtl2(DocNo, DNo, FamilyName, Status, Gender, IDNo, NIN, SSCode, CardNo, HealthFacility, StartDt, EndDt, BirthDt, Problem, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@DocNo, @DNo, @FamilyName, @Status, @Gender, @IDNo, @NIN, @SSCode2, @CardNo2, @HealthFacility2, @StartDt2, @EndDt2, @BirthDt, @Problem2, @Remark, @CreateBy, CurrentDateTime());");

            //SQL.AppendLine("Insert Into TblEmployeeFamilySS(EmpCode, DNo, FamilyName, Status, Gender, IDNo, NIN, SSCode, CardNo, HealthFacility, StartDt, EndDt, BirthDt, Problem, EntCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @IDNo, @NIN, @SSCode2, @CardNo2, @HealthFacility2, @StartDt2, @EndDt2, @BirthDt, @Problem2, @EntCode, @Remark, @CreateBy, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("Update FamilyName = @FamilyName , Status= @Status , Gender= @Gender , IDNo= @IDNo , NIN= @NIN , SSCode= @SSCode2 , CardNo= @CardNo2 , HealthFacility= @HealthFacility2 , StartDt= @StartDt2 , EndDt= @EndDt2 , BirthDt= @BirthDt , Problem= @Problem2 , EntCode= @EntCode , Remark= @Remark ; ");

            SQL.AppendLine("Set @DNo:=");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(DNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(DNo, Decimal) As DNo From TblEmployeeFamilySS ");
            SQL.Append("       Where EmpCode=@EmpCode ");
            SQL.Append("       Order By DNo Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001');");

            SQL.AppendLine("Insert Into TblEmployeeFamilySS(EmpCode, DNo, FamilyName, Status, Gender, IDNo, NIN, SSCode, CardNo, HealthFacility, StartDt, EndDt, BirthDt, Problem, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @FamilyName, @Status, @Gender, @IDNo, @NIN, @SSCode2, @CardNo2, @HealthFacility2, @StartDt2, @EndDt2, @BirthDt, @Problem2, @EntCode, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@DocNo", z.DocNo);
            //Sm.CmParam<String>(ref cm, "@DNo", z.DNo2);
            Sm.CmParam<String>(ref cm, "@EmpCode", z.EmpCode);
            Sm.CmParam<String>(ref cm, "@FamilyName", z.FamilyName);
            Sm.CmParam<String>(ref cm, "@Status", z.Status);
            Sm.CmParam<String>(ref cm, "@Gender", z.Gender);
            Sm.CmParam<String>(ref cm, "@IDNo", z.IDNo);
            Sm.CmParam<String>(ref cm, "@NIN", z.NIN);
            Sm.CmParam<String>(ref cm, "@SSCode2", z.SSCode2);
            Sm.CmParam<String>(ref cm, "@CardNo2", z.CardNo2);
            Sm.CmParam<String>(ref cm, "@HealthFacility2", z.HealthFacility2);
            Sm.CmParam<String>(ref cm, "@StartDt2", z.StartDt2);
            Sm.CmParam<String>(ref cm, "@EndDt2", z.EndDt2);
            Sm.CmParam<String>(ref cm, "@BirthDt", z.BirthDt);
            Sm.CmParam<String>(ref cm, "@Problem2", z.Problem2);
            Sm.CmParam<String>(ref cm, "@Remark", z.RemarkD2);
            Sm.CmParam<String>(ref cm, "@EntCode", z.EntCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        #endregion

        #region CustomerDeposit

        private bool IsInsertedCustomerDepositInvalid(ref List<CustomerDeposit> lCtDeposit)
        {
            return
                IsDocDtEmpty(ref lCtDeposit) ||
                IsCtCodeEmpty(ref lCtDeposit) ||
                IsCurCode1Empty(ref lCtDeposit) ||
                IsAmt1Empty(ref lCtDeposit) ||
                IsRateAmt1Empty(ref lCtDeposit) ||
                IsCurCode2Empty(ref lCtDeposit) ||
                IsDocDtFormatInvalid(ref lCtDeposit) ||
                IsCurCodeNotDifferent(ref lCtDeposit) ||
                IsClosingJournalInvalid(ref lCtDeposit);
        }

        private bool IsCurCodeNotDifferent(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].CurCode1 == lCtDeposit[i].CurCode2)
                {
                    Sm.StdMsg(mMsgType.Info, "Switched Currency should be different.");
                    return true;
                }
            }

            return false;
        }

        private bool IsClosingJournalInvalid(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (Sm.IsClosingJournalInvalid(lCtDeposit[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsAmountNotValid(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].Amt1 > lCtDeposit[i].ExistingAmt)
                {
                    Sm.StdMsg(mMsgType.Info, "Switched Amount is bigger than Existing Amount.");
                    return true;
                }
            }

            return false;
        }
        private bool IsCtCodeEmpty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].CtCode == "0000")
                {
                    Sm.StdMsg(mMsgType.Info, "Customer Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtEmpty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCurCode1Empty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].CurCode1.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "First Currency code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private bool IsCurCode2Empty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].CurCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Second Currency code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAmt1Empty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].Amt1.ToString().Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "First Currency code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        private bool IsRateAmt1Empty(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].RateAmt1.ToString().Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "First Currency code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<CustomerDeposit> lCtDeposit)
        {
            for (int i = 0; i < lCtDeposit.Count(); i++)
            {
                if (lCtDeposit[i].DocDt.Contains("/") || lCtDeposit[i].DocDt.Contains("-") || lCtDeposit[i].DocDt.Contains(".") || lCtDeposit[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private void SetDocNo(ref List<CustomerDeposit> lCtDeposit)
        {
            int mIndex = 1;
            for (int i = 0; i < lCtDeposit.Count; i++)
            {
                lCtDeposit[i].DocNo = GenerateDocNo(mIndex, lCtDeposit[i].DocDt, "CustomerDepositCurSwitch", "TblCustomerDepositCurSwitch");
                mIndex++;
            }
        }


        private void SetJournalDocNo(ref List<CustomerDeposit> lCtDeposit)
        {
            int mIndex = 1;
            for (int i = 0; i < lCtDeposit.Count; i++)
            {
                lCtDeposit[i].JournalDocNo = GenerateDocNoJournal(lCtDeposit[i].DocDt, mIndex);
                mIndex++;
            }
        }


        private void ExistingAmt(ref List<CustomerDeposit> lCtDeposit)
        {
            try
            {
                for (int i = 0; i < lCtDeposit.Count; i++)
                {
                    lCtDeposit[i].ExistingAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblCustomerDepositSummary Where CtCode = '" + lCtDeposit[i].CtCode + "' And CurCode = '" + lCtDeposit[i].CurCode1 + "'; "));

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeAmt(ref List<CustomerDeposit> lCtDeposit)
        {
            try
            {
                for (int i = 0; i < lCtDeposit.Count; i++)
                {
                    if (lCtDeposit[i].CurCode2.Length != 0 && lCtDeposit[i].RateAmt1.ToString().Length != 0 && lCtDeposit[i].Amt1.ToString().Length != 0 && lCtDeposit[i].DocNo.Length == 0)
                    {
                        lCtDeposit[i].Amt2 = lCtDeposit[i].Amt1 * lCtDeposit[i].RateAmt1;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PrepareJournal(CustomerDeposit x, ref decimal Amt1, ref decimal Amt2)
        {
            if (x.Amt1.ToString().Length > 0)
                Amt1 = x.Amt1;

            if (!Sm.CompareStr(x.CurCode1, mMainCurCode))
                Amt1 *= GetCurRate(x, x.CurCode1, mMainCurCode);

            if (x.Amt2.ToString().Length > 0)
                Amt2 = x.Amt2;

            if (!Sm.CompareStr(x.CurCode2, mMainCurCode))
                Amt2 *= GetCurRate(x, x.CurCode2, mMainCurCode);

        }

        private decimal GetCurRate(CustomerDeposit x, string CurCode1, string CurCode2)
        {
            var SQL = new StringBuilder();
            var Value = string.Empty;

            SQL.AppendLine("Select Amt From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1=@CurCode1 ");
            SQL.AppendLine("And CurCode2=@CurCode2 ");
            SQL.AppendLine("And RateDt<=@DocDt ");
            SQL.AppendLine("Order By RateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode1", CurCode1);
            Sm.CmParam<String>(ref cm, "@CurCode2", CurCode2);
            Value = Sm.GetValue(cm);

            if (Value.Length == 0)
                return 0m;
            else
                return decimal.Parse(Value);
        }


        private MySqlCommand InsertCustomerDepositCurSwitch(CustomerDeposit x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerDepositCurSwitch (DocNo, DocDt, CtCode, CurCode1, Amt1, RateAmt1, CurCode2, Amt2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @CtCode, @CurCode1, @Amt1, @RateAmt1, @CurCode2, @Amt2, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("values (@DocNo, @DocType1, @DocDt, @CtCode, @CurCode1, @AmtMin, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("values (@DocNo, @DocType2, @DocDt, @CtCode, @CurCode2, @AmtPlus, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@CtCode, @CurCode1, @Amt1, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt-@Amt1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@CtCode, @CurCode2, @Amt2, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt+@Amt2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtCode);
            Sm.CmParam<String>(ref cm, "@CurCode1", x.CurCode1);
            Sm.CmParam<Decimal>(ref cm, "@Amt1", x.Amt1);
            Sm.CmParam<Decimal>(ref cm, "@AmtMin", x.Amt1 * -1);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt1", x.RateAmt1);
            Sm.CmParam<String>(ref cm, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm, "@AmtPlus", x.Amt2);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@DocType1", "05");
            Sm.CmParam<String>(ref cm, "@DocType2", "06");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertCustomerDepositSummary2(CustomerDeposit x)
        {
            var SQL = new StringBuilder();
            decimal RateAmt = x.RateAmt1, ExcRate = 0m;

            if (RateAmt != 0) ExcRate = 1m / RateAmt;

            SQL.AppendLine("Insert Into TblCustomerDepositSummary2 ");
            SQL.AppendLine("(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @CurCode, @ExcRate, @Amt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        Amt=Amt+@Amt, ");
            SQL.AppendLine("        LastUpBy=@UserCode, ");
            SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", x.CtCode);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", x.RateAmt1);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertJournal(CustomerDeposit x, byte Type, decimal Amt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblCustomerDepositCurSwitch Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Customer Deposit (Currency Switch) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblCustomerDepositCurSwitch Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");

            if (Type == 1)
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.JournalDocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForForeignExchange", mAcNoForForeignExchange);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoDownPayment", mCustomerAcNoDownPayment);
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);

            return cm;
        }
        private MySqlCommand ReduceAmtSummaryCt2(ref List<Rate> lR, int i, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=" + x + "; ");

            SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand InsertExcRateMainCurCode(CustomerDeposit x)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @CtCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", x.CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", (x.CurCode2 == mMainCurCode) ? 1 : (1 / x.RateAmt1));
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand InsertExcRateOtherMainCurCode(CustomerDeposit x, decimal rate)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblCustomerDepositSummary2(CtCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @CtCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@CtCode", x.CtCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", rate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }


        #endregion

        #region Stock Opname

        private bool IsInsertedStockOpnameInvalid(ref List<StockOpname> lStockOpname)
        {
            return
                IsDocDtEmpty(ref lStockOpname) ||
                IsWhsCodeEmpty(ref lStockOpname) ||
                IsDocDtFormatInvalid(ref lStockOpname) ||
                IsSourceInvalid(ref lStockOpname) ||
                IsClosingJournalInvalid(ref lStockOpname) ||
                IsDocDtNotValid(ref lStockOpname) ||
                IsDataChanged(ref lStockOpname);
        }

        private bool IsWhsCodeEmpty(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (lStockOpname[i].WhsCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Warehouse code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtEmpty(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (lStockOpname[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (lStockOpname[i].DocDt.Contains("/") || lStockOpname[i].DocDt.Contains("-") || lStockOpname[i].DocDt.Contains(".") || lStockOpname[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsSourceInvalid(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (!Sm.IsDataExist(
               "Select Source From TblStockSummary Where WhsCode=@Param1 " +
               "And Source = @Param2 limit 1; ",
               lStockOpname[i].WhsCode, lStockOpname[i].Source, string.Empty
               ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Source is not available in Stock Summary at row #" + (i + 1));
                    return true;
                }

            }

            return false;
        }

        private bool IsClosingJournalInvalid(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (Sm.IsClosingJournalInvalid(lStockOpname[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDocDtNotValid(ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (Sm.IsDocDtNotValid(Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"), lStockOpname[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDataChanged(ref List<StockOpname> lStockOpname)
        {
            if (!mIsStockOpnameShowWarningIfChanged) return false;

            var l = new List<Stock>();
            GetOpname(ref l, ref lStockOpname);
            if (l.Count > 0)
            {
                GetStock(ref l);
                if (IsDataChanged(ref l)) return true;
            }
            l.Clear();
            return false;
        }

        private void GetOpname(ref List<Stock> l, ref List<StockOpname> lStockOpname)
        {
            for (int i = 0; i < lStockOpname.Count(); i++)
            {
                if (lStockOpname[i].ItCode.Length > 0)
                    l.Add(new Stock()
                    {
                        Lot = lStockOpname[i].Lot,
                        Bin = lStockOpname[i].Bin,
                        ItCode = lStockOpname[i].ItCode,
                        ItName = Sm.GetValue("Select ItName From tblitem where ItCode = " + lStockOpname[i].ItCode + " "),
                        BatchNo = lStockOpname[i].BatchNo,
                        Source = lStockOpname[i].Source,
                        BQty = lStockOpname[i].QtyActual - lStockOpname[i].Qty,
                        BQty2 = lStockOpname[i].QtyActual2 - lStockOpname[i].Qty2,
                        BQty3 = lStockOpname[i].QtyActual3 - lStockOpname[i].Qty3,
                        AQty = 0m,
                        AQty2 = 0m,
                        AQty3 = 0m,
                        WhsCode = lStockOpname[i].WhsCode
                    });
            }
        }

        private void GetStock(ref List<Stock> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string
                Filter = string.Empty,
                Source = string.Empty,
                Lot = string.Empty,
                Bin = string.Empty;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter +=
                    " (WhsCode=@WhsCode" + i.ToString() + " And Source=@Source" + i.ToString() + " And Lot=@Lot" + i.ToString() + " And Bin=@Bin" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), l[i].Source);
                Sm.CmParam<String>(ref cm, "@Lot" + i.ToString(), l[i].Lot);
                Sm.CmParam<String>(ref cm, "@Bin" + i.ToString(), l[i].Bin);
                Sm.CmParam<String>(ref cm, "@WhsCode" + i.ToString(), l[i].WhsCode);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ")";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where 0 = 0 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
            {
                    "Source",
                    "Lot", "Bin", "Qty", "Qty2", "Qty3"
            });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, c[0]);
                        Lot = Sm.DrStr(dr, c[1]);
                        Bin = Sm.DrStr(dr, c[2]);
                        Qty = Sm.DrDec(dr, c[3]);
                        Qty2 = Sm.DrDec(dr, c[4]);
                        Qty3 = Sm.DrDec(dr, c[5]);

                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].Source, Source) &&
                                Sm.CompareStr(l[i].Lot, Lot) &&
                                Sm.CompareStr(l[i].Bin, Bin))
                            {
                                l[i].AQty = Qty;
                                l[i].AQty2 = Qty2;
                                l[i].AQty3 = Qty3;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private bool IsDataChanged(ref List<Stock> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].BQty != l[i].AQty ||
                    l[i].BQty2 != l[i].AQty2 ||
                    l[i].BQty3 != l[i].AQty3)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + l[i].ItCode + Environment.NewLine +
                        "Item's Name : " + l[i].ItName + Environment.NewLine +
                        "Batch# : " + l[i].BatchNo + Environment.NewLine +
                        "Source : " + l[i].Source + Environment.NewLine +
                        "Lot : " + l[i].Lot + Environment.NewLine +
                        "Bin : " + l[i].Bin + Environment.NewLine +
                        "Quantity (Opname) : " + Sm.FormatNum(l[i].BQty, 0) + Environment.NewLine +
                        "Quantity (Current) : " + Sm.FormatNum(l[i].AQty, 0) + Environment.NewLine + Environment.NewLine +
                        "Stock already changed." + Environment.NewLine +
                        "You can't save this document."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsApprovalExist(StockOpnameHdr lStockOpnameHdr)
        {
            //for (int i = 0; i < lStockOpname.Count; i++)
            //{
            string DocApproval = Sm.GetValue(
                "Select UserCode From TblDocApprovalSetting " +
                "Where DocType='StockOpname' And WhsCode = '" + lStockOpnameHdr.WhsCode + "' limit 1; ");

            if (DocApproval.Length > 0) return true;
            //}

            return false;
        }

        private void SetIndex(ref List<StockOpname> lStockOpname)
        {
            string mWhsCode = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lStockOpname.Count; i++)
            {
                lStockOpname[i].Mth = (lStockOpname[i].DocDt).Substring(4, 2);
                lStockOpname[i].Yr = (lStockOpname[i].DocDt).Substring(0, 4);

                if (lStockOpname[i].WhsCode != mWhsCode || lStockOpname[i].DocDt != mDocDt)
                {
                    if (lStockOpname[i].Mth != mMth || lStockOpname[i].Yr != mYr)
                    {
                        mIndex = 1;
                        mWhsCode = lStockOpname[i].WhsCode;
                        mDocDt = lStockOpname[i].DocDt;
                        mYr = (lStockOpname[i].DocDt).Substring(0, 4);
                        mMth = (lStockOpname[i].DocDt).Substring(4, 2);
                    }
                    else mIndex++;

                }
                lStockOpname[i].Index = mIndex;
            }
        }


        private void SetDocNo(ref List<StockOpname> lStockOpname)
        {
            string mDocNo = string.Empty, mWhsCode = string.Empty, mDocDt = string.Empty, mJournalDocNo = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lStockOpname.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    lStockOpname[i].DocNo = GenerateDocNo(lStockOpname[i].Index, lStockOpname[i].DocDt, "StockOpname", "TblStockOpnameHdr");
                    lStockOpname[i].JournalDocNo = GenerateDocNoJournal(lStockOpname[i].DocDt, lStockOpname[i].Index);
                    mDocNo = lStockOpname[i].DocNo;
                    mWhsCode = lStockOpname[i].WhsCode;
                    mDocDt = lStockOpname[i].DocDt;
                    mJournalDocNo = lStockOpname[i].JournalDocNo;
                }
                else
                {
                    if (mWhsCode != lStockOpname[i].WhsCode || mDocDt != lStockOpname[i].DocDt)
                    {
                        lStockOpname[i].DocNo = GenerateDocNo(lStockOpname[i].Index, lStockOpname[i].DocDt, "StockOpname", "TblStockOpnameHdr");
                        lStockOpname[i].JournalDocNo = GenerateDocNoJournal(lStockOpname[i].DocDt, lStockOpname[i].Index);
                        mDocNo = lStockOpname[i].DocNo;
                        mWhsCode = lStockOpname[i].WhsCode;
                        mDocDt = lStockOpname[i].DocDt;
                        mJournalDocNo = lStockOpname[i].JournalDocNo;
                    }
                    else
                    {
                        lStockOpname[i].DocNo = mDocNo;
                        lStockOpname[i].JournalDocNo = mJournalDocNo;
                    }
                }
                lStockOpname[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<StockOpname> l, ref List<StockOpnameHdr> l2, ref List<StockOpnameDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l
                .Select(x => new { x.DocNo, x.DocDt, x.Status, x.WhsCode, x.Remark, x.JournalDocNo })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new StockOpnameHdr()
                    {
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        WhsCode = m.WhsCode,
                        Status = m.Status,
                        Remark = m.Remark,
                        JournalDocNo = m.JournalDocNo
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new StockOpnameHdr()
                        {
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            WhsCode = m.WhsCode,
                            Status = m.Status,
                            Remark = m.Remark,
                            JournalDocNo = m.JournalDocNo
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            #region Old Code
            //string mDocNo = string.Empty;
            //if (l.Count > 0) l.OrderBy(o => o.DocNo);
            //for (int i = 0; i < l.Count; i++)
            //{
            //    if (mDocNo.Length <= 0)
            //    {
            //        l2.Add(new StockOpnameHdr()
            //        {
            //            DocNo = l[i].DocNo,
            //            DocDt = l[i].DocDt,
            //            WhsCode = l[i].WhsCode,
            //            Status = l[i].Status,
            //            Remark = l[i].Remark,
            //            JournalDocNo = l[i].JournalDocNo
            //        });
            //    }
            //    else
            //    {
            //        if (mDocNo != l[i].DocNo)
            //        {
            //            l2.Add(new StockOpnameHdr()
            //            {
            //                DocNo = l[i].DocNo,
            //                DocDt = l[i].DocDt,
            //                WhsCode = l[i].WhsCode,
            //                Status = l[i].Status,
            //                Remark = l[i].Remark,
            //                JournalDocNo = l[i].JournalDocNo
            //            });

            //        }

            //    }
            //    mDocNo = l[i].DocNo;
            //}
            #endregion

            foreach (var n in l.Select(x => new { x.DocNo, x.WhsCode, x.DNo, x.ItCode, x.BatchNo, x.Source, x.Lot, x.Bin, x.QtyActual, x.QtyActual2, x.QtyActual3, x.Qty, x.Qty2, x.Qty3, x.Remark2 })
               .OrderBy(o => o.DocNo))
                if (n.ItCode.Trim().Length > 0 || n.BatchNo.Trim().Length > 0 || n.Source.Trim().Length > 0 || n.Lot.Trim().Length > 0
                    || n.Bin.Trim().Length > 0 || n.QtyActual.ToString().Trim().Length > 0 || n.QtyActual2.ToString().Trim().Length > 0 || n.QtyActual3.ToString().Trim().Length > 0 || n.Qty.ToString().Trim().Length > 0 || n.Qty2.ToString().Trim().Length > 0 || n.Qty3.ToString().Trim().Length > 0 || n.Remark2.Trim().Length > 0)
                {
                    l3.Add(new StockOpnameDtl()
                    {
                        DocNo = n.DocNo,
                        WhsCode = n.WhsCode,
                        ItCode = n.ItCode,
                        DNo = n.DNo,
                        BatchNo = n.BatchNo,
                        Source = n.Source,
                        Lot = n.Lot,
                        Bin = n.Bin,
                        QtyActual = n.QtyActual,
                        QtyActual2 = n.QtyActual2,
                        QtyActual3 = n.QtyActual3,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                        Remark2 = n.Remark2,
                        QtySystem = 0m,
                        QtySystem2 = 0m,
                        QtySystem3 = 0m
                    });
                }
        }


        private void ProcessDtl1(ref List<StockOpnameDtl> lStockOpnameDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lStockOpnameDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = lStockOpnameDtl[i].DocNo;
                    lStockOpnameDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lStockOpnameDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = lStockOpnameDtl[i].DocNo;

                    }

                    lStockOpnameDtl[i].DNo = mDNo;
                }
            }
        }

        private void ReComputeStock(ref List<StockOpnameDtl> lStockOpnameDtl)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where 0 = 0 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                for (int i = 0; i < lStockOpnameDtl.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter +=
                        " (WhsCode=@WhsCode" + i.ToString() + " And Source=@Source" + i.ToString() + " And Lot=@Lot" + i.ToString() + " And Bin=@Bin" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@Source" + i.ToString(), lStockOpnameDtl[i].Source);
                    Sm.CmParam<String>(ref cm, "@Lot" + i.ToString(), lStockOpnameDtl[i].Lot);
                    Sm.CmParam<String>(ref cm, "@Bin" + i.ToString(), lStockOpnameDtl[i].Bin);
                    Sm.CmParam<String>(ref cm, "@WhsCode" + i.ToString(), lStockOpnameDtl[i].WhsCode);
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ")";
                else
                    Filter = " And 1=0 ";

                cm.CommandText = SQL.ToString() + Filter;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                   { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3"
                   });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < lStockOpnameDtl.Count; row++)
                        {
                            if (
                                Sm.CompareStr(lStockOpnameDtl[row].Source, Source) &&
                                Sm.CompareStr(lStockOpnameDtl[row].Lot, Lot) &&
                                Sm.CompareStr(lStockOpnameDtl[row].Bin, Bin)
                                )
                            {
                                lStockOpnameDtl[row].QtySystem = Sm.DrDec(dr, 3);
                                lStockOpnameDtl[row].QtySystem2 = Sm.DrDec(dr, 4);
                                lStockOpnameDtl[row].QtySystem3 = Sm.DrDec(dr, 5);

                                lStockOpnameDtl[row].Qty = lStockOpnameDtl[row].QtyActual - lStockOpnameDtl[row].QtySystem;
                                lStockOpnameDtl[row].Qty2 = lStockOpnameDtl[row].QtyActual2 - lStockOpnameDtl[row].QtySystem2;
                                lStockOpnameDtl[row].Qty3 = lStockOpnameDtl[row].QtyActual3 - lStockOpnameDtl[row].QtySystem3;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private MySqlCommand InsertStockOpnameHdr(StockOpnameHdr y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockOpnameHdr(DocNo, DocDt, Status, WhsCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Status, @WhsCode, @Remark, @CreateBy, CurrentDateTime()); ");

            if (IsApprovalExist(y) == true)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='StockOpname' And WhsCode = '" + y.WhsCode + "'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", y.DocDt);
            Sm.CmParam<String>(ref cm, "@Status", (IsApprovalExist(y)) ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@WhsCode", y.WhsCode);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertStockOpnameDtl(StockOpnameDtl x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockOpnameDtl(DocNo, DNo, ItCode, BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("QtyActual, QtyActual2, QtyActual3, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @BatchNo, @Source, @Lot, @Bin, ");
            SQL.AppendLine("@QtyActual, @QtyActual2, @QtyActual3, @Qty, @Qty2, @Qty3, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", x.BatchNo);
            Sm.CmParam<String>(ref cm, "@Source", x.Source);
            Sm.CmParam<String>(ref cm, "@Lot", x.Lot);
            Sm.CmParam<String>(ref cm, "@Bin", x.Bin);
            Sm.CmParam<Decimal>(ref cm, "@QtyActual", x.QtyActual);
            Sm.CmParam<Decimal>(ref cm, "@QtyActual2", x.QtyActual2);
            Sm.CmParam<Decimal>(ref cm, "@QtyActual3", x.QtyActual3);
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", x.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", x.Qty3);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark2);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand InsertStockMovement(StockOpnameHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpnameHdr A ");
            SQL.AppendLine("Inner Join TblStockOpnameDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertStockSummary(StockOpnameHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("   Select X1.WhsCode, X2.Qty, X2.Qty2, X2.Qty3, X2.Lot, X2.Bin, X2.Source ");
            SQL.AppendLine("   From TblStockOpnameHdr X1 ");
            SQL.AppendLine("   Inner Join TblStockOpnameDtl X2 On X1.DocNo = X2.DocNo And X1.DocNo = @DocNo ");
            SQL.AppendLine(") B On A.WhsCode = B.WhsCode And A.Lot = B.Lot And A.Bin = B.Bin And A.Source = B.Source ");
            SQL.AppendLine("Set A.Qty = A.Qty+B.Qty, A.Qty2 = A.Qty2+B.Qty2, A.Qty3 = A.Qty3+B.Qty3; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);

            return cm;
        }

        private MySqlCommand InsertJournal(StockOpnameHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockOpnameHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Opname : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockOpnameHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            //Debit
            SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From (");

            SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt ");
            SQL.AppendLine("        From TblStockOpnameHdr A ");
            SQL.AppendLine("        Inner Join TblStockOpnameDtl B On A.DocNo=B.DocNo And B.Qty>0 ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As DAmt ");
            SQL.AppendLine("        From TblStockOpnameHdr A ");
            SQL.AppendLine("        Inner Join TblStockOpnameDtl B On A.DocNo=B.DocNo And B.Qty<0 ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
            SQL.AppendLine("    Group By T.AcNo ");

            //Credit
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T.AcNo, 0.00 As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("        From TblStockOpnameHdr A ");
            SQL.AppendLine("        Inner Join TblStockOpnameDtl B On A.DocNo=B.DocNo And B.Qty>0 ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("        From TblStockOpnameHdr A ");
            SQL.AppendLine("        Inner Join TblStockOpnameDtl B On A.DocNo=B.DocNo And B.Qty<0 ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
            SQL.AppendLine("    Group By T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.JournalDocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            return cm;
        }

        #endregion

        #region Vendor Deposit

        private bool IsInsertedVendorDepositInvalid(ref List<VendorDeposit> lVendorDeposit)
        {
            return
                IsDocDtEmpty(ref lVendorDeposit) ||
                IsVdCodeEmpty(ref lVendorDeposit) ||
                IsAmt1Empty(ref lVendorDeposit) ||
                IsCurCodeNotDifferent(ref lVendorDeposit) ||
                IsCurCode2Empty(ref lVendorDeposit) ||
                IsRateAmt1Empty(ref lVendorDeposit) ||
                IsClosingJournalInvalid(ref lVendorDeposit) ||
                IsAmt2Empty(ref lVendorDeposit);
        }

        private bool IsDocDtEmpty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsVdCodeEmpty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].VdCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Vendor Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsClosingJournalInvalid(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (Sm.IsClosingJournalInvalid(lVendorDeposit[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsCurCodeNotDifferent(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].CurCode2 == lVendorDeposit[i].CurCode1)
                {
                    Sm.StdMsg(mMsgType.Info, "CurCode1 should be different from CurCode2");
                    return true;
                }
            }

            return false;
        }
        private bool IsAmt1Empty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].Amt1 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Amt1 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCurCode2Empty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].CurCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "CurCode1 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsRateAmt1Empty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].RateAmt1 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "RateAmt1 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        private bool IsAmt2Empty(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].Amt2 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Amt2 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        private bool IsAmountNotValid(ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                if (lVendorDeposit[i].Amt1 > lVendorDeposit[i].ExistingAmt)
                {
                    Sm.StdMsg(mMsgType.Info, "Amt1 is bigger than Existing Amount");
                    return true;
                }
            }
            return false;
        }
        private void SetExistingAmt(ref List<VendorDeposit> lVendorDeposit, bool IsInclSwitchedAmt)
        {
            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                var SQL = new StringBuilder();
                SQL.AppendLine("Select A.Amt-IfNull(B.Amt, 0) As Amt ");
                SQL.AppendLine("From TblVendorDepositSummary A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.VdCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, ");
                SQL.AppendLine("    Sum(T1.Amt) As Amt ");
                SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
                SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
                SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
                SQL.AppendLine("        And T2.CancelInd='N' ");
                SQL.AppendLine("        And T2.Status<>'C' ");
                SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
                SQL.AppendLine("    Where T1.Status<>'C' ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.VdCode=@VdCode ");
                SQL.AppendLine("    And IfNull(T1.EntCode, '')=IfNull(@EntCode, '') ");
                SQL.AppendLine("    And T1.CurCode=@CurCode ");
                SQL.AppendLine("    Group By T1.VdCode, IfNull(T1.EntCode, ''), T1.CurCode ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On A.VdCode=B.VdCode ");
                SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(B.EntCode, '') ");
                SQL.AppendLine("    And A.CurCode=B.CurCode ");
                SQL.AppendLine("Where A.VdCode=@VdCode ");
                SQL.AppendLine("And IfNull(A.EntCode, '')=IfNull(@EntCode, '') ");
                SQL.AppendLine("And A.CurCode=@CurCode;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@VdCode", lVendorDeposit[i].VdCode);
                Sm.CmParam<String>(ref cm, "@EntCode", lVendorDeposit[i].EntCode);
                Sm.CmParam<String>(ref cm, "@CurCode", lVendorDeposit[i].CurCode1);

                decimal Amt = 0m;
                string Value = Sm.GetValue(cm);
                if (Value.Length != 0) Amt = decimal.Parse(Value);
                if (IsInclSwitchedAmt && lVendorDeposit[i].Amt1 > 0) Amt += lVendorDeposit[i].Amt1;
                lVendorDeposit[i].ExistingAmt = Amt;
            }
        }

        private void SetAmt2(ref List<VendorDeposit> lVendorDeposit)
        {

            for (int i = 0; i < lVendorDeposit.Count(); i++)
            {
                string
             CurCode1 = lVendorDeposit[i].CurCode1,
             CurCode2 = lVendorDeposit[i].CurCode2,
             DocDt = lVendorDeposit[i].DocDt;
                decimal
                    Amt1 = 0m,
                    Amt2 = 0m,
                    RateAmt = 0m;

                if (CurCode1.Length == 0 || CurCode2.Length == 0)
                {
                    RateAmt = 0m;
                    Amt2 = 0m;
                    return;
                }

                if (DocDt.Length == 0)
                {
                    RateAmt = 0m;
                    Amt2 = 0m;
                    return;
                }
                else
                {
                    try
                    {
                        DocDt = Sm.Left(DocDt, 8);
                        if (Sm.CompareStr(CurCode1, CurCode2))
                            RateAmt = 1m;
                        else
                        {
                            if (CurCode2 == mMainCurCode)
                                RateAmt = GetCurRate(CurCode1, CurCode2, DocDt);
                            else
                                RateAmt = GetCurRate(CurCode2, CurCode1, DocDt);
                        }
                        if (RateAmt != 0) RateAmt = 1m / RateAmt;
                        if (lVendorDeposit[i].Amt1 > 0) Amt1 = lVendorDeposit[i].Amt1;
                        Amt2 = Amt1 * RateAmt;
                        lVendorDeposit[i].RateAmt1 = decimal.Parse(String.Format("{0:#,##0.00######}", RateAmt));
                        lVendorDeposit[i].Amt2 = Amt2;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
            }
        }

        private void SetDocNo(ref List<VendorDeposit> lVendorDeposit)
        {
            int mIndex = 1;
            for (int i = 0; i < lVendorDeposit.Count; i++)
            {
                lVendorDeposit[i].DocNo = GenerateDocNo(mIndex, lVendorDeposit[i].DocDt, "VendorDepositCurSwitch", "TblVendorDepositCurSwitch");
                mIndex++;
            }
        }
        private void GetVendorDepositSummary2(ref List<Rate> l, ref List<VendorDeposit> lVendorDeposit)
        {
            for (int i = 0; i < lVendorDeposit.Count; i++)
            {
                if (Sm.CompareStr(lVendorDeposit[i].CurCode1, mMainCurCode)) return;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select VdCode, CurCode, ExcRate, Amt ");
                SQL.AppendLine("From TblVendorDepositSummary2 ");
                SQL.AppendLine("Where VdCode=@VdCode ");
                SQL.AppendLine("And CurCode=@CurCode1 ");
                SQL.AppendLine("And Amt>0 ");
                SQL.AppendLine("Order By CreateDt Asc; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@VdCode", lVendorDeposit[i].VdCode);
                    Sm.CmParam<String>(ref cm, "@CurCode1", lVendorDeposit[i].CurCode1);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "VdCode",

                    //1-3
                    "CurCode",
                    "ExcRate",
                    "Amt"
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Rate()
                            {
                                VdCode = Sm.DrStr(dr, c[0]),
                                CurCode = Sm.DrStr(dr, c[1]),
                                ExcRate = Sm.DrDec(dr, c[2]),
                                Amt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }
        }


        private MySqlCommand ReduceAmtSummaryVd2(ref List<Rate> lR, int i, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=" + x + "; ");

            SQL1.AppendLine("Update TblVendorDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where VdCode = @VdCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", lR[i].VdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }
        private MySqlCommand SaveExcRateOtherMainCurCode(decimal rate, VendorDeposit x)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @VdCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", rate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateMainCurCode(VendorDeposit x)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @VdCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", (x.CurCode2 == mMainCurCode) ? 1 : (1 / x.RateAmt1));
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }
        private MySqlCommand SaveVendorDepositSummary2(VendorDeposit x)
        {
            var SQL = new StringBuilder();
            decimal RateAmt = x.RateAmt1, ExcRate = 0m;

            if (RateAmt != 0) ExcRate = 1m / RateAmt;

            SQL.AppendLine("Insert Into TblVendorDepositSummary2 ");
            SQL.AppendLine("(VdCode, CurCode, EntCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VdCode, @CurCode, IfNull(@EntCode, ''), @ExcRate, @Amt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        Amt=Amt+@Amt, ");
            SQL.AppendLine("        LastUpBy=@UserCode, ");
            SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", x.RateAmt1);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveVendorDepositCurSwitch(VendorDeposit x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVendorDepositCurSwitch (DocNo, DocDt, VdCode, EntCode, CurCode1, Amt1, RateAmt1, CurCode2, Amt2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @VdCode, @EntCode, @CurCode1, @Amt1, @RateAmt1, @CurCode2, @Amt2, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values (@DocNo, @DocType1, @DocDt, @VdCode, @EntCode, @CurCode1, @AmtMin, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values (@DocNo, @DocType2, @DocDt, @VdCode, @EntCode, @CurCode2, @AmtPlus, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) Values (@VdCode, IfNull(@EntCode, ''), @CurCode1, @Amt1, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt-@Amt1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) Values (@VdCode, IfNull(@EntCode, ''), @CurCode2, @Amt2, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt+@Amt2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@VdCode", x.VdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            Sm.CmParam<String>(ref cm, "@CurCode1", x.CurCode1);
            Sm.CmParam<Decimal>(ref cm, "@Amt1", x.Amt1);
            Sm.CmParam<Decimal>(ref cm, "@AmtMin", x.Amt1 * -1);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt1", x.RateAmt1);
            Sm.CmParam<String>(ref cm, "@CurCode2", x.CurCode2);
            Sm.CmParam<Decimal>(ref cm, "@Amt2", x.Amt2);
            Sm.CmParam<Decimal>(ref cm, "@AmtPlus", x.Amt2);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@DocType1", "05");
            Sm.CmParam<String>(ref cm, "@DocType2", "06");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        private void PrepareJournal(ref decimal Amt1, ref decimal Amt2, VendorDeposit x)
        {
            string
                CurCode1 = x.CurCode1,
                CurCode2 = x.CurCode2,
                DocDt = x.DocDt;

            if (x.Amt1 > 0)
                Amt1 = x.Amt1;

            if (!Sm.CompareStr(CurCode1, mMainCurCode))
                Amt1 *= GetCurRate(CurCode1, mMainCurCode, DocDt);

            if (x.Amt2 > 0)
                Amt2 = x.Amt2;

            if (!Sm.CompareStr(CurCode2, mMainCurCode))
                Amt2 *= GetCurRate(CurCode2, mMainCurCode, DocDt);
        }
        private decimal GetCurRate(string CurCode1, string CurCode2, string DocDt)
        {
            var SQL = new StringBuilder();
            var Value = string.Empty;

            SQL.AppendLine("Select Amt From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1=@CurCode1 ");
            SQL.AppendLine("And CurCode2=@CurCode2 ");
            SQL.AppendLine("And RateDt<=@DocDt ");
            SQL.AppendLine("Order By RateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode1", CurCode1);
            Sm.CmParam<String>(ref cm, "@CurCode2", CurCode2);
            Value = Sm.GetValue(cm);

            if (Value.Length == 0)
                return 0m;
            else
                return decimal.Parse(Value);
        }
        private MySqlCommand SaveJournal(byte Type, decimal Amt, VendorDeposit x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblVendorDepositCurSwitch Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Vendor Deposit (Currency Switch) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVendorDepositCurSwitch Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");

            if (Type == 1)
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("Concat(@VendorAcNoDownPayment, @VdCode) As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("Concat(@VendorAcNoDownPayment, @VdCode) As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(x.DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForForeignExchange", mAcNoForForeignExchange);
            Sm.CmParam<String>(ref cm, "@VendorAcNoDownPayment", mVendorAcNoDownPayment);
            Sm.CmParam<String>(ref cm, "@VdCode", x.VdCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);

            return cm;
        }
        private void InsertVendorDeposit(VendorDeposit x)
        {
            var lR = new List<Rate>();
            var VD = new List<VendorDeposit>();
            var cml = new List<MySqlCommand>();
            decimal DP = 0m;
            GetVendorDepositSummary2(ref lR, ref VD);

            if (lR.Count > 0)
            {
                DP = x.Amt1;
                for (int j = 0; j < lR.Count; j++)
                {
                    if (DP > 0)
                    {
                        cml.Add(ReduceAmtSummaryVd2(ref lR, j, DP));
                        DP = DP - lR[j].Amt;
                    }
                }

                if (x.CurCode2 != mMainCurCode && x.CurCode1 != mMainCurCode) // switch from non-MainCurCode to non-MainCurCode
                {
                    decimal rate = 0m;
                    for (int y = 0; y < lR.Count; y++)
                    {
                        rate += lR[y].ExcRate / x.RateAmt1;
                    }

                    rate = rate / lR.Count;

                    cml.Add(SaveExcRateOtherMainCurCode(rate, x));
                }
                else // swicth from MainCurCode or to MainCurCode
                {
                    cml.Add(SaveExcRateMainCurCode(x));
                }
            }
            else
            {
                // 07/06/2018 [TKG] untuk IDR to Non IDR
                if (!Sm.CompareStr(x.CurCode2, mMainCurCode))
                    cml.Add(SaveVendorDepositSummary2(x));
            }

            cml.Add(SaveVendorDepositCurSwitch(x));

            if (mIsAutoJournalActived)
            {
                decimal Amt1 = 0m, Amt2 = 0m;
                PrepareJournal(ref Amt1, ref Amt2, x);
                if (Amt1 != Amt2)
                {
                    if (Amt1 < Amt2)
                        cml.Add(SaveJournal(1, Amt2 - Amt1, x));
                    else
                        cml.Add(SaveJournal(2, Amt1 - Amt2, x));
                }
            }
            Sm.ExecCommands(cml);

            ClearData();
        }
        #endregion

        #region Stock Initial 

        private bool IsInsertedStockInitialInvalid(ref List<StockInitial> lStockInitial)
        {
            return
                IsDocDtEmpty(ref lStockInitial) ||
                IsDocDtFormatInvalid(ref lStockInitial) ||
                IsWhsCodeEmpty(ref lStockInitial) ||
                IsExcRateEmpty(ref lStockInitial) ||
                IsClosingJournalInvalid(ref lStockInitial) ||
                IsDocDtInvalid(ref lStockInitial);
        }

        private bool IsDocDtEmpty(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (lStockInitial[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsWhsCodeEmpty(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (lStockInitial[i].WhsCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Warehouse Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsExcRateEmpty(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (lStockInitial[i].ExcRate.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Exchange Rate is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsClosingJournalInvalid(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (Sm.IsClosingJournalInvalid(lStockInitial[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDocDtInvalid(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (Sm.IsDocDtNotValid(Sm.CompareStr(
                    Sm.GetParameter("InventoryDocDtValidInd"), "Y"), lStockInitial[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<StockInitial> lStockInitial)
        {
            for (int i = 0; i < lStockInitial.Count(); i++)
            {
                if (lStockInitial[i].DocDt.Contains("/") || lStockInitial[i].DocDt.Contains("-") || lStockInitial[i].DocDt.Contains(".") || lStockInitial[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private void SetIndex(ref List<StockInitial> lStockInitial)
        {
            string mWhsCode = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lStockInitial.Count; i++)
            {
                lStockInitial[i].Mth = (lStockInitial[i].DocDt).Substring(4, 2);
                lStockInitial[i].Yr = (lStockInitial[i].DocDt).Substring(0, 4);
                if (lStockInitial[i].WhsCode != mWhsCode || lStockInitial[i].DocDt != mDocDt)
                {
                    if (lStockInitial[i].Mth != mMth || lStockInitial[i].Yr != mYr)
                    {
                        mIndex = 1;
                        mWhsCode = lStockInitial[i].WhsCode;
                        mDocDt = lStockInitial[i].DocDt;
                        mYr = (lStockInitial[i].DocDt).Substring(0, 4);
                        mMth = (lStockInitial[i].DocDt).Substring(4, 2);
                    }
                    else mIndex++;
                }
                lStockInitial[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<StockInitial> lStockInitial)
        {
            string mDocNo = string.Empty, mWhsCode = string.Empty, mDocDt = string.Empty, mJournalDocNo = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lStockInitial.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    lStockInitial[i].DocNo = GenerateDocNo(lStockInitial[i].Index, lStockInitial[i].DocDt, "StockInitial", "TblStockInitialHdr");
                    lStockInitial[i].JournalDocNo = Sm.GenerateDocNoJournal(lStockInitial[i].DocDt, lStockInitial[i].Index);
                    mDocNo = lStockInitial[i].DocNo;
                    mWhsCode = lStockInitial[i].WhsCode;
                    mDocDt = lStockInitial[i].DocDt;
                    mJournalDocNo = lStockInitial[i].JournalDocNo;
                }
                else
                {
                    if (mWhsCode != lStockInitial[i].WhsCode || mDocDt != lStockInitial[i].DocDt)
                    {
                        lStockInitial[i].DocNo = GenerateDocNo(lStockInitial[i].Index, lStockInitial[i].DocDt, "StockInitial", "TblStockInitialHdr");
                        lStockInitial[i].JournalDocNo = Sm.GenerateDocNoJournal(lStockInitial[i].DocDt, lStockInitial[i].Index);
                        mDocNo = lStockInitial[i].DocNo;
                        mWhsCode = lStockInitial[i].WhsCode;
                        mDocDt = lStockInitial[i].DocDt;
                        mJournalDocNo = lStockInitial[i].JournalDocNo;
                    }
                    else
                    {
                        lStockInitial[i].DocNo = mDocNo;
                        lStockInitial[i].JournalDocNo = mJournalDocNo;
                    }
                }
                lStockInitial[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<StockInitial> l, ref List<StockInitialHdr> l2, ref List<StockInitialDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l.Select(x => new { x.Index, x.DocNo, x.DocDt, x.WhsCode, x.CurCode, x.ExcRate, x.JournalDocNo, x.Remark })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new StockInitialHdr()
                    {
                        Index = m.Index,
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        WhsCode = m.WhsCode,
                        CurCode = m.CurCode,
                        ExcRate = m.ExcRate,
                        JournalDocNo = m.JournalDocNo,
                        Remark = m.Remark
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new StockInitialHdr()
                        {
                            Index = m.Index,
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            WhsCode = m.WhsCode,
                            CurCode = m.CurCode,
                            ExcRate = m.ExcRate,
                            JournalDocNo = m.JournalDocNo,
                            Remark = m.Remark
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new
            {
                x.DocNo,
                x.DNo,
                x.ItCode,
                x.BatchNo,
                x.Source,
                x.Lot,
                x.Bin,
                x.Qty1,
                x.Uom1,
                x.Qty2,
                x.Uom2,
                x.Qty3,
                x.Uom3,
                x.Price,
                x.Remark1
            })
                .OrderBy(o => o.DocNo)
               )
                if (n.ItCode.Trim().Length > 0 || n.BatchNo.Trim().Length > 0 || n.Source.Trim().Length > 0
                    || n.Lot.Trim().Length > 0 || n.Bin.Trim().Length > 0 || n.Qty1.ToString().Trim().Length > 0 || n.Uom1.Trim().Length > 0 || n.Qty2.ToString().Trim().Length > 0
                    || n.Uom2.Trim().Length > 0 || n.Qty3.ToString().Trim().Length > 0 || n.Uom3.Trim().Length > 0 || n.Price.ToString().Trim().Length > 0 || n.Remark1.Trim().Length > 0)
                {
                    l3.Add(new StockInitialDtl()
                    {
                        DocNo = n.DocNo,
                        DNo = n.DNo,
                        ItCode = n.ItCode,
                        BatchNo = n.BatchNo,
                        Source = n.Source,
                        Lot = n.Lot,
                        Bin = n.Bin,
                        Qty1 = n.Qty1,
                        Uom1 = n.Uom1,
                        Qty2 = n.Qty2,
                        Uom2 = n.Uom2,
                        Qty3 = n.Qty3,
                        Uom3 = n.Uom3,
                        Price = n.Price,
                        Remark1 = n.Remark1,
                    });
                }
        }

        private void ProcessDtl(ref List<StockInitialDtl> lStockInitialDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lStockInitialDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "00001";
                    mDocNo = lStockInitialDtl[i].DocNo;
                    lStockInitialDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lStockInitialDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("00000", (Int32.Parse(mDNo) + 1)), 5);
                    }
                    else
                    {
                        mDNo = "00001";
                        mDocNo = lStockInitialDtl[i].DocNo;
                    }
                    lStockInitialDtl[i].DNo = mDNo;
                }
            }
        }

        private MySqlCommand InsertStockInitialHdr(StockInitialHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialHdr(DocNo, DocDt, WhsCode, CurCode, ExcRate, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @CurCode, @ExcRate, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@ExcRate", x.ExcRate);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertStockInitialDtl(StockInitialDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialDtl(DocNo, DNo,CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, ");
            SQL.AppendLine("IfNull(@BatchNo, ");
            SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
            SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", y.ItCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", y.BatchNo);
            Sm.CmParam<String>(ref cm, "@Lot", y.Lot);
            Sm.CmParam<String>(ref cm, "@Bin", y.Bin);
            Sm.CmParam<String>(ref cm, "@DocType", "04");
            Sm.CmParam<Decimal>(ref cm, "@Qty", y.Qty1);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", y.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", y.Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", y.Price);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark1);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournalStockInitial(StockInitialHdr z)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockInitialHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Initial : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, Remark As RemarkJournal From (");
            SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt, B.Remark ");
            SQL.AppendLine("        From TblStockInitialHdr A ");
            SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
            SQL.AppendLine("        From TblStockInitialHdr A ");
            SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", z.DocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(z.DocDt, z.Index));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", z.JournalDocNo);
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", z.DocNo);
            return cm;
        }

        private MySqlCommand SaveStock(StockInitialHdr lStockInitial)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, A.ExcRate, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", lStockInitial.DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "04");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Cost Category

        private bool IsInsertedCostCategoryInvalid(ref List<CostCategory> lCCt)
        {
            return
                IsCostCategoryNameEmpty(ref lCCt) ||
                IsCostCenterCodeEmpty(ref lCCt) ||
                IsAcNoEmpty(ref lCCt) ||
                IsAcNoCodeDuplicated(ref lCCt) ||
                IsAccountCCtCodeExisted(ref lCCt);
        }

        private bool IsCostCategoryNameEmpty(ref List<CostCategory> lCCt)
        {
            for (int i = 0; i < lCCt.Count(); i++)
            {
                if (lCCt[i].CCtName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost category name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAcNoEmpty(ref List<CostCategory> lCCt)
        {
            for (int i = 0; i < lCCt.Count(); i++)
            {
                if (lCCt[i].AcNo.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Account# is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCostCenterCodeEmpty(ref List<CostCategory> lCCt)
        {
            for (int i = 0; i < lCCt.Count(); i++)
            {
                if (lCCt[i].CCCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost center is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsAcNoCodeDuplicated(ref List<CostCategory> lCCt)
        {
            if (Sm.GetParameterBoo("IsOneAccountForOneCostCategory") == true)
            {
                for (int i = 0; i < lCCt.Count(); i++)
                {
                    for (int j = (i + 1); j < lCCt.Count(); j++)
                    {
                        if (lCCt[i].AcNo == lCCt[j].AcNo)
                        {
                            Sm.StdMsg(mMsgType.Info, "Account# " + lCCt[i].AcNo + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                            return true;
                        }
                    }
                }
            }

            return false;

        }

        private bool IsAccountCCtCodeExisted(ref List<CostCategory> lCCt)
        {
            string mAcNo = string.Empty, mAcNoExisted = string.Empty;
            if (Sm.GetParameterBoo("IsOneAccountForOneCostCategory") == true)
            {
                for (int i = 0; i < lCCt.Count(); i++)
                {
                    if (mAcNo.Length > 0) mAcNo += ",";
                    mAcNo += lCCt[i].AcNo;
                }

                if (mAcNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Group_Concat(Distinct AcNo)AcNo From TblCostCategory Where Find_In_Set(AcNo, @Param); ");
                    mAcNoExisted = Sm.GetValue(SQL.ToString(), mAcNo);

                    if (mAcNoExisted.Length > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Account# ( " + Sm.GetValue(SQL.ToString(), mAcNo) + " ) already existed.");
                        return true;
                    }
                }
            }

            return false;
        }

        private void InsertCostCategory(CostCategory x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CCtCode:=Right(Concat('00000', Value), 5) ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select IfNull(Max(Cast(Trim(CCtCode) As Decimal)), 0)+1 As Value ");
            SQL.AppendLine("    From TblCostCategory ");
            SQL.AppendLine(") T;");

            SQL.AppendLine("Insert Into TblCostCategory(CCtCode, CCtName, CCCode, AcNo, CCGrpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CCtCode, @CCtName, @CCCode, @AcNo, @CCGrpCode, @UserCode, CurrentDateTime()); ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("   Update CCtName=@CCtName, CCCode=@CCCode, AcNo=@AcNo, CCGrpCode=@CCGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@CCtName", x.CCtName);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);
            Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
            Sm.CmParam<String>(ref cm, "@CCGrpCode", x.CCGrpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Employee Contract Document 

        private bool IsInsertedEmpContractDocInvalid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            return
                IsDocDtEmpty(ref lEmpContractDoc) ||
                IsDocDtFormatNotValid(ref lEmpContractDoc) ||
                IsEmpCodeEmpty(ref lEmpContractDoc) ||
                IsContractExtentionEmpty(ref lEmpContractDoc) ||
                IsStartDtEmpty(ref lEmpContractDoc) ||
                IsStartDtFormatNotValid(ref lEmpContractDoc) ||
                IsEndDtEmpty(ref lEmpContractDoc) ||
                IsEndDtFormatNotValid(ref lEmpContractDoc) ||
                IsEndDtNotValid(ref lEmpContractDoc) ||
                IsRenewalEmpty(ref lEmpContractDoc) ||
                (mIsEmployeeManagementRepresentative && IsEmpCode2Empty(ref lEmpContractDoc)) ||
                IsContractExtInvalid(ref lEmpContractDoc);

        }

        private bool IsContractExtInvalid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            if (!mIsEmpContractDocExtValidationEnabled) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblEmpContractDoc ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("And ContractExt>=@ContractExt ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                Sm.CmParam<String>(ref cm, "@EmpCode", lEmpContractDoc[i].EmpCode);
                Sm.CmParam<Decimal>(ref cm, "@ContractExt", lEmpContractDoc[i].ContractExtention);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Info, "Invalid contract extension at row #" + (i + 1));
                    return true;
                }
            }

            return false;

        }

        private bool IsDocDtEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEmpCodeEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].EmpCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Employee Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEmpCode2Empty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].EmpCode2.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Employee Code 2 is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsContractExtentionEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].ContractExtention.ToString().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Contract Extention is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsRenewalEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].Renewal.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Renewal is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDtEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].StartDate.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Start Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEndDtEmpty(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].EndDate.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "End Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEndDtNotValid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (Convert.ToInt32(lEmpContractDoc[i].EndDate) < Convert.ToInt32(lEmpContractDoc[i].StartDate))
                {
                    Sm.StdMsg(mMsgType.Info, "End Date is not valid at row #" + (i + 1));
                    return true;
                }
            }


            return false;
        }

        private bool IsDocDtFormatNotValid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].DocDt.Contains("/") || lEmpContractDoc[i].DocDt.Contains("-") || lEmpContractDoc[i].DocDt.Contains(".") || lEmpContractDoc[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDtFormatNotValid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].StartDate.Contains("/") || lEmpContractDoc[i].StartDate.Contains("-") || lEmpContractDoc[i].StartDate.Contains(".") || lEmpContractDoc[i].StartDate.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEndDtFormatNotValid(ref List<EmpContractDoc> lEmpContractDoc)
        {
            for (int i = 0; i < lEmpContractDoc.Count(); i++)
            {
                if (lEmpContractDoc[i].EndDate.Contains("/") || lEmpContractDoc[i].EndDate.Contains("-") || lEmpContractDoc[i].EndDate.Contains(".") || lEmpContractDoc[i].EndDate.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private void SetDocNo(ref List<EmpContractDoc> lEmpContractDoc)
        {
            string mYr = string.Empty, mMth = string.Empty;
            int mIndex = 0;

            lEmpContractDoc.OrderBy(x => x.DocDt);
            for (int i = 0; i < lEmpContractDoc.Count; i++)
            {
                lEmpContractDoc[i].Mth = (lEmpContractDoc[i].DocDt).Substring(4, 2);
                lEmpContractDoc[i].Yr = (lEmpContractDoc[i].DocDt).Substring(0, 4);

                if (lEmpContractDoc[i].Mth == mMth || lEmpContractDoc[i].Yr == mYr)
                {
                    mIndex++;
                    lEmpContractDoc[i].DocNo = GenerateDocNo(mIndex, lEmpContractDoc[i].DocDt, "EmpContractDoc", "TblEmpContractDoc");
                }
                else
                {
                    string mMthDocNo = lEmpContractDoc[i].Mth;
                    string mYrDocNo = Sm.Right(lEmpContractDoc[i].Yr, 2);
                    string indexFromQuery = Sm.GetValue("Select Convert(Left(DocNo, 4), Decimal) As DocNo " +
                                                "From tblempcontractdoc " +
                                                "Where Right(DocNo, 5)=Concat('" + mMthDocNo + "','/', '" + mYrDocNo + "')  " +
                                                "Order By Left(DocNo, 4) Desc Limit 1 ;");

                    if (indexFromQuery.Length > 0)
                        mIndex = Convert.ToInt32(indexFromQuery);
                    else
                        mIndex = 1;
                    lEmpContractDoc[i].DocNo = GenerateDocNo(mIndex, lEmpContractDoc[i].DocDt, "EmpContractDoc", "TblEmpContractDoc");
                }

                mMth = lEmpContractDoc[i].Mth;
                mYr = lEmpContractDoc[i].Yr;

            }
        }

        private void InsertEmpContractDoc(EmpContractDoc x)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Insert Into TblEmpContractDoc ");
            SQL.AppendLine("(DocNo, DocDt, ActInd, LocalDocNo, EmpCode, EmpCode2, ");
            SQL.AppendLine("ContractExt, StartDt, EndDt, RenewalInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'Y', @LocalDocNo, @EmpCode, @EmpCode2, ");
            SQL.AppendLine("@ContractExt, @StartDt, @EndDt, @RenewalInd, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", x.LocalDocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", x.EmpCode);
            Sm.CmParam<String>(ref cm, "@EmpCode2", x.EmpCode2);
            Sm.CmParam<Decimal>(ref cm, "@ContractExt", x.ContractExtention);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@StartDt", x.StartDate);
            Sm.CmParam<String>(ref cm, "@EndDt", x.EndDate);
            Sm.CmParam<String>(ref cm, "@RenewalInd", x.Renewal);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion 

        #region MR To PI

        private bool IsInsertedMRToPIInvalid(ref List<MRToPI> l)
        {
            return
                IsParameterEmpty(Sm.GetLue(LueImportCode)) ||
                IsDocDtEmpty(ref l) ||
                IsDocDtFormatNotValid(ref l) ||
                IsVdCodeEmpty(ref l) ||
                IsVdCodeNotFound(ref l) ||
                IsAmountZero(ref l) ||
                IsDataDuplicated(ref l);
        }

        private bool IsVdCodeNotFound(ref List<MRToPI> l)
        {
            string mVdCode = string.Empty, mVdCode2 = string.Empty, mVdCode3 = string.Empty;

            foreach (var x in l)
            {
                if (mVdCode.Length > 0) mVdCode += ",";
                mVdCode += x.VdCode;
            }

            if (mVdCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct VdCode) ");
                SQL.AppendLine("From TblVendor ");
                SQL.AppendLine("Where Find_In_Set(VdCode, @Param); ");

                mVdCode2 = Sm.GetValue(SQL.ToString(), mVdCode);

                if (mVdCode2.Length > 0)
                {
                    string[] mArrVdCode = mVdCode.Split(',');
                    string[] mArrVdCode2 = mVdCode2.Split(',');

                    foreach (var x in mArrVdCode)
                    {
                        bool IsFlag = false;
                        foreach (var y in mArrVdCode2)
                        {
                            if (x == y)
                            {
                                IsFlag = true;
                                break;
                            }
                        }

                        if (!IsFlag)
                        {
                            if (mVdCode3.Length > 0) mVdCode3 += ",";
                            mVdCode3 += x;
                        }
                    }
                }
                else
                    mVdCode3 = mVdCode;

                if (mVdCode3.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This vendor data is not exists on the system : " + mVdCode3);
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtEmpty(ref List<MRToPI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Date is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDocDtFormatNotValid(ref List<MRToPI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].DocDt.Contains("/") || l[i].DocDt.Contains("-") || l[i].DocDt.Contains(".") || l[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsVdCodeEmpty(ref List<MRToPI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].VdCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Vendor Code is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsAmountZero(ref List<MRToPI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].Amt == 0m)
                {
                    Sm.StdMsg(mMsgType.Info, "Amount is zero at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDataDuplicated(ref List<MRToPI> l)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = (i + 1); j < l.Count; ++j)
                {
                    if (l[i].DocDt == l[j].DocDt && l[i].VdCode == l[j].VdCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Could not process duplicated date and vendor code at row #" + (j + 1) + ". Duplicated from row #" + (i + 1));
                        return true;
                    }
                }
            }
            return false;
        }

        private void InsertMRToPI(MRToPI y)
        {
            var SQL = new StringBuilder();

            // Purchase Invoice
            SQL.AppendLine("Insert Into TblPurchaseInvoiceHdr(DocNo, DocDt, CancelInd, ProcessInd, ");
            SQL.AppendLine("VdCode, DueDt, CurCode, Amt, DeptCode, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PIDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("@VdCode, @DocDt, @CurCode, @Amt, @DeptCode, @SiteCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblPurchaseInvoiceDtl(DocNo, DNo, RecvVdDocNo, RecvVdDNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PIDocNo, '001', @RecvVdDocNo, '001', @Remark, @CreateBy, CurrentDateTime()); ");

            // Receiving Item From Vendor
            SQL.AppendLine("Insert Into TblRecvVdHdr(DocNo, DocDt, POInd, WhsCode, VdCode, DeptCode, ");
            SQL.AppendLine("SiteCode, PtCode, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@RecvVdDocNo, @DocDt, 'N', @WhsCode, @VdCode, @DeptCode, ");
            SQL.AppendLine("@SiteCode, @PtCode, @CurCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, ");
            SQL.AppendLine("PropCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, UPrice, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@RecvVdDocNo, '001', 'N', 'A', @PODocNo, '001', @ItCode, ");
            SQL.AppendLine("'-', '-', '-', '-', '-', 1.00, 1.00, 1.00, 1.00, @Amt, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            // Vendor Quotation
            SQL.AppendLine("Update TblQtDtl As T1  ");
            SQL.AppendLine("Inner Join TblQtHdr T2 On T1.DocNo=T2.DocNo And T2.VdCode=@VdCode And T2.PtCode=@PtCode ");
            SQL.AppendLine("Set T1.ActInd='N', T1.LastUpBy=@CreateBy, T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where T1.ActInd='Y' ");
            SQL.AppendLine("And T1.ItCode In (");
            SQL.AppendLine("    Select Distinct B.ItCode ");
            SQL.AppendLine("    From TblRecvVdHdr A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocNo=@RecvVdDocNo And A.POInd='N' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblQtHdr(DocNo, DocDt, SystemNo, VdCode, PtCode, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNo, DocDt, Concat(Left(DocDt, 6), Left(@QtDocNo, 4)), VdCode, PtCode, CurCode, Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@RecvVdDocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblQtDtl(DocNo, DNo, ItCode, ActInd, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNo, B.DNo, ");
            SQL.AppendLine("B.ItCode, 'Y', B.UPrice, @Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@RecvVdDocNo And A.POInd='N'; ");

            // Material Request
            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, DocDt, CancelInd, Status, DeptCode, SiteCode, ReqType, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @MaterialRequestDocNo, DocDt, 'N', 'A', DeptCode, @SiteCode, '2', ");
            SQL.AppendLine("Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@RecvVdDocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, Status, ProcessInd, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Select @MaterialRequestDocNo, B.DNo, ");
            SQL.AppendLine("'N', 'A', 'F', B.ItCode, B.QtyPurchase, A.DocDt, ");
            SQL.AppendLine("@QtDocNo, (Select Max(DNo) As DNo From TblQtDtl Where DocNo=@QtDocNo And ItCode=B.ItCode And UPrice=B.UPrice) As QtDNo, ");
            SQL.AppendLine("B.UPrice, @Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@RecvVdDocNo And A.POInd='N'; ");

            // PO Request
            SQL.AppendLine("Insert Into TblPORequestHdr(DocNo, DocDt, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PORequestDocNo, DocDt, @SiteCode, Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@RecvVdDocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblPORequestDtl");
            SQL.AppendLine("(DocNo, DNo, CancelInd, Status, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, CreditLimit, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PORequestDocNo, DNo, 'N', 'A', DocNo, DNo, Qty, QtDocNo, QtDNo, 0, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblMaterialRequestDtl Where DocNo=@MaterialRequestDocNo And CancelInd='N'; ");

            // Purchase Order
            SQL.AppendLine("Insert Into TblPOHdr(DocNo, DocDt, Status, DocNoSource, RevNo, VdCode, SiteCode, VdContactPerson, ShipTo, BillTo, CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, DownPayment, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PODocNo, DocDt, 'A', @PODocNo, 0, VdCode, @SiteCode, Null, Null, Null, @CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, 0, DiscountAmt, Amt, 0, Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@RecvVdDocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, ProcessInd, PORequestDocNo, PORequestDNo, Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PODocNo, DNo, 'N', 'F', A.DocNo, B.DNo, B.Qty, ");
            SQL.AppendLine("(Select Discount From TblRecvVdDtl Where DocNo=@RecvVdDocNo And DNo=B.DNo) As Discount, ");
            SQL.AppendLine("0 As DiscountAmt, ");
            SQL.AppendLine("(Select RoundingValue From TblRecvVdDtl Where DocNo=@RecvVdDocNo And DNo=B.DNo) As RoundingValue, ");
            SQL.AppendLine("A.DocDt, A.Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @PORequestDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            var cml = new List<MySqlCommand>();

            Sm.CmParam<String>(ref cm, "@PIDocNo", y.PIDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", y.DocDt);
            Sm.CmParam<String>(ref cm, "@QtDocNo", y.QtDocNo);
            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", y.RecvVdDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", y.MRDocNo);
            Sm.CmParam<String>(ref cm, "@PORequestDocNo", y.PORDocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo", y.PODocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", y.VdCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", y.Amt);
            Sm.CmParam<String>(ref cm, "@ItCode", mItCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@CurCode", mCurCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@PtCode", mPTCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForImportPI);
            Sm.CmParam<String>(ref cm, "@Remark", "Auto generated by Import MR to PI");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cml.Add(cm);

            Sm.ExecCommands(cml);

            ClearData();
        }

        #endregion

        #region DOCt to SLI

        private bool IsInsertedDOCtToSLIInvalid(ref List<DOCtToSLI> l)
        {
            return
                IsParameterEmpty(Sm.GetLue(LueImportCode)) ||
                IsDocDtEmpty(ref l) ||
                IsDocDtFormatNotValid(ref l) ||
                IsCtCodeEmpty(ref l) ||
                IsCtCodeNotFound(ref l) ||
                IsAmountZero(ref l) ||
                IsDataDuplicated(ref l) ||
                IsCtCityCodeEmpty(ref l);
        }

        private bool IsCtCodeNotFound(ref List<DOCtToSLI> l)
        {
            string mCtCode = string.Empty, mCtCode2 = string.Empty, mCtCode3 = string.Empty;

            foreach (var x in l)
            {
                if (mCtCode.Length > 0) mCtCode += ",";
                mCtCode += x.CtCode;
            }

            if (mCtCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct CtCode) ");
                SQL.AppendLine("From TblCustomer ");
                SQL.AppendLine("Where Find_In_Set(CtCode, @Param); ");

                mCtCode2 = Sm.GetValue(SQL.ToString(), mCtCode);

                if (mCtCode2.Length > 0)
                {
                    string[] mArrCtCode = mCtCode.Split(',');
                    string[] mArrCtCode2 = mCtCode2.Split(',');

                    foreach (var x in mArrCtCode)
                    {
                        bool IsFlag = false;
                        foreach (var y in mArrCtCode2)
                        {
                            if (x == y)
                            {
                                IsFlag = true;
                                break;
                            }
                        }

                        if (!IsFlag)
                        {
                            if (mCtCode3.Length > 0) mCtCode3 += ",";
                            mCtCode3 += x;
                        }
                    }
                }
                else
                    mCtCode3 = mCtCode;

                if (mCtCode3.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This customer data is not exists on the system : " + mCtCode3);
                    return true;
                }
            }

            return false;
        }

        private bool IsCtCityCodeEmpty(ref List<DOCtToSLI> l)
        {
            string mCtCode = string.Empty, mCtCode2 = string.Empty;

            foreach (var x in l)
            {
                if (mCtCode.Length > 0) mCtCode += ",";
                mCtCode += x.CtCode;
            }

            if (mCtCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(CtCode Separator ', ') ");
                SQL.AppendLine("From TblCustomer ");
                SQL.AppendLine("Where Find_In_Set(CtCode, @Param) ");
                SQL.AppendLine("And CityCode Is Null; ");

                mCtCode2 = Sm.GetValue(SQL.ToString(), mCtCode);

                if (mCtCode2.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "City in this customer data should be filled : " + mCtCode2);
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtEmpty(ref List<DOCtToSLI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Date is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDocDtFormatNotValid(ref List<DOCtToSLI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].DocDt.Contains("/") || l[i].DocDt.Contains("-") || l[i].DocDt.Contains(".") || l[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCtCodeEmpty(ref List<DOCtToSLI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].CtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Customer Code is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsAmountZero(ref List<DOCtToSLI> l)
        {
            for (int i = 0; i < l.Count(); ++i)
            {
                if (l[i].Amt == 0m)
                {
                    Sm.StdMsg(mMsgType.Info, "Amount is zero at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDataDuplicated(ref List<DOCtToSLI> l)
        {
            for (int i = 0; i < l.Count; ++i)
            {
                for (int j = (i + 1); j < l.Count; ++j)
                {
                    if (l[i].DocDt == l[j].DocDt && l[i].CtCode == l[j].CtCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Could not process duplicated date and customer code at row #" + (j + 1) + ". Duplicated from row #" + (i + 1));
                        return true;
                    }
                }
            }
            return false;
        }

        private void InsertDOCtToSLI(DOCtToSLI y)
        {
            var SQL = new StringBuilder();

            string
                SAName = string.Empty,
                SAAddress = string.Empty,
                SACityCode = string.Empty,
                SACntCode = string.Empty,
                SAPostalCd = string.Empty,
                SAPhone = string.Empty,
                SAFax = string.Empty,
                SAEmail = string.Empty,
                SAMobile = string.Empty,
                PackagingUnitUomCode = string.Empty;

            // DO To Customer
            SQL.AppendLine("Insert Into TblDOCtHdr(DocNo, DocDt, Status, WhsCode, CtCode, ");
            SQL.AppendLine("CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DOCtDocNo, @DocDt, 'A', @WhsCode, @CtCode, ");
            SQL.AppendLine("@CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, ");
            SQL.AppendLine("BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, ");
            SQL.AppendLine("PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DOCtDocNo, '001', 'N', @ItCode, '-', ");
            SQL.AppendLine("'-', 'F', '-', '-', '-', 1.00, ");
            SQL.AppendLine("@PackagingUnitUomCode, 1.00, 1.00, 1.00, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            // Sales Invoice based on DO To Customer
            SQL.AppendLine("Insert Into TblSalesInvoiceHdr(DocNo, ReceiptNo, DocDt, CancelInd, ProcessInd, ");
            SQL.AppendLine("CBDInd, CtCode, DueDt, CurCode, TotalAmt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SLIDocNo, @ReceiptNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("'N', @CtCode, @DocDt, @CurCode, @Amt, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ");
            SQL.AppendLine("ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, UPriceAfterTax, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SLIDocNo, '001', '3', @DOCtDocNo, '001', 'O', ");
            SQL.AppendLine("@ItCode, 1.00, 1.00, @Amt, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            var cml = new List<MySqlCommand>();

            Sm.CmParam<String>(ref cm, "@DOCtDocNo", y.DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", y.DocDt);
            Sm.CmParam<String>(ref cm, "@SLIDocNo", y.SLIDocNo);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", GenerateReceipt(y.DocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", y.CtCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", y.Amt);
            Sm.CmParam<String>(ref cm, "@CurCode", mCurCodeForImportSLI);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForImportSLI);
            Sm.CmParam<String>(ref cm, "@ItCode", mItCodeForImportSLI);

            string sSQL = string.Empty, sSQL2 = string.Empty;
            var cm2 = new MySqlCommand();

            sSQL += "Select IfNull(B.Name, A.CtName) SAName, IfNull(B.Address, A.Address) SAAddress, A.CityCode SACityCode, IfNull(B.CntCode, A.CntCode) SACntCode, ";
            sSQL += "IfNull(B.PostalCd, A.PostalCd) SAPostalCd, IfNull(B.Phone, A.Phone) SAPhone, IfNull(B.Fax, A.Fax) SAFax, IfNull(B.Email, A.Email) SAEmail, IfNull(B.Mobile, A.Mobile) SAMobile ";
            sSQL += "From TblCustomer A ";
            sSQL += "Left Join TblCustomerShipAddress B On A.CtCode = B.CtCode ";
            sSQL += "    And B.DNo In (Select Min(DNo) From TblCustomerShipAddress Where CtCode = @Param) ";
            sSQL += "Where A.CtCode = @Param; ";

            sSQL2 += "Select UomCode ";
            sSQL2 += "From TblItemPackagingUnit ";
            sSQL2 += "Where ItCode = @Param ";
            sSQL2 += "Limit 1; ";

            PackagingUnitUomCode = Sm.GetValue(sSQL2, mItCodeForImportSLI);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm2.Connection = cn;
                cm2.CommandText = sSQL;
                Sm.CmParam<String>(ref cm2, "@Param", y.CtCode);
                var dr = cm2.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    "SAName",
                    "SAAddress", "SACityCode", "SACntCode", "SAPostalCd", "SAPhone",
                    "SAFax", "SAEmail", "SAMobile"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SAName = Sm.DrStr(dr, c[0]);
                        SAAddress = Sm.DrStr(dr, c[1]);
                        SACityCode = Sm.DrStr(dr, c[2]);
                        SACntCode = Sm.DrStr(dr, c[3]);
                        SAPostalCd = Sm.DrStr(dr, c[4]);
                        SAPhone = Sm.DrStr(dr, c[5]);
                        SAFax = Sm.DrStr(dr, c[6]);
                        SAEmail = Sm.DrStr(dr, c[7]);
                        SAMobile = Sm.DrStr(dr, c[8]);
                    }
                }
                dr.Close();
            }

            Sm.CmParam<String>(ref cm, "@SAName", SAName);
            Sm.CmParam<String>(ref cm, "@SAAddress", SAAddress);
            Sm.CmParam<String>(ref cm, "@SACityCode", SACityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", SACntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", SAPostalCd);
            Sm.CmParam<String>(ref cm, "@SAPhone", SAPhone);
            Sm.CmParam<String>(ref cm, "@SAFax", SAFax);
            Sm.CmParam<String>(ref cm, "@SAEmail", SAEmail);
            Sm.CmParam<String>(ref cm, "@SAMobile", SAMobile);
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", PackagingUnitUomCode);

            Sm.CmParam<String>(ref cm, "@Remark", "Auto generated by Import DOCt to SLI");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            cml.Add(cm);

            Sm.ExecCommands(cml);

            ClearData();
        }

        #endregion

        #region Item Local

        private bool IsInsertedItemLocalInvalid(ref List<ItemLocal> lItemLocal)
        {
            return
                IsItemLocalNameEmpty(ref lItemLocal) ||
                IsItemCodeEmpty(ref lItemLocal) ||
                IsItCodeInternalDuplicated(ref lItemLocal) ||
                IsItCodeDuplicated(ref lItemLocal) ||
                IsItCodeInternalCodeExisted(ref lItemLocal);
        }

        private bool IsItemLocalNameEmpty(ref List<ItemLocal> lItemLocal)
        {
            for (int i = 0; i < lItemLocal.Count(); i++)
            {
                if (lItemLocal[i].ItCodeInternal.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item local name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCodeEmpty(ref List<ItemLocal> lItemLocal)
        {
            for (int i = 0; i < lItemLocal.Count(); i++)
            {
                if (lItemLocal[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItCodeInternalDuplicated(ref List<ItemLocal> lItemLocal)
        {
            for (int i = 0; i < lItemLocal.Count(); i++)
            {
                for (int j = (i + 1); j < lItemLocal.Count(); j++)
                {
                    if (lItemLocal[i].ItCodeInternal == lItemLocal[j].ItCodeInternal)
                    {
                        Sm.StdMsg(mMsgType.Info, "Item local# " + lItemLocal[i].ItCodeInternal + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;

        }

        private bool IsItCodeDuplicated(ref List<ItemLocal> lItemLocal)
        {
            for (int i = 0; i < lItemLocal.Count(); i++)
            {
                for (int j = (i + 1); j < lItemLocal.Count(); j++)
                {
                    if (lItemLocal[i].ItCode == lItemLocal[j].ItCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Item code# " + lItemLocal[i].ItCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;

        }

        private bool IsItCodeInternalCodeExisted(ref List<ItemLocal> lItemLocal)
        {
            string mItCode = string.Empty, mAcNoExisted = string.Empty;
            for (int i = 0; i < lItemLocal.Count(); i++)
            {
                if (mItCode.Length > 0) mItCode += ",";
                mItCode += lItemLocal[i].ItCode;
            }

            if (mItCode.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ItCodeInternal From TblItem Where Find_In_Set(ItCode, @Param) And ItCodeinternal is not null ; ");
                mAcNoExisted = Sm.GetValue(SQL.ToString(), mItCode);

                if (mAcNoExisted.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Item Local# ( " + Sm.GetValue(SQL.ToString(), mItCode) + " ) already existed.");
                    return true;
                }
            }

            return false;
        }

        private void UpdateItemLocal(ItemLocal x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update Tblitem ");
            SQL.AppendLine("SET ItCodeInternal=@ItCodeInternal, ");
            SQL.AppendLine("LastUpBy = @UserCode, ");
            SQL.AppendLine("LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where ItCode = @ItCode ; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", x.ItCodeInternal);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Journal Transaction 

        private bool IsJournalTransactionInvalid(ref List<Journal> lJournal)
        {
            return
                IsDocDtEmpty(ref lJournal) ||
                IsDocDtFormatNotValid(ref lJournal) ||
                IsPeriodEmpty(ref lJournal) ||
                IsPeriodFormatNotValid(ref lJournal) ||
                IsRemarkEmpty(ref lJournal) ||
                IsCOAEmpty(ref lJournal) ||
                IsDAmtEmpty(ref lJournal) ||
                IsCAmtEmpty(ref lJournal) ||
                IsJnDescEmpty(ref lJournal) ||
                IsEntityEmpty(ref lJournal) ||
                IsBalancedNotValid(ref lJournal);
        }

        private bool IsEntityEmpty(ref List<Journal> lJournal)
        {

            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].Entity.Length <= 0)
                {
                    if (mDocNoFormat != "1")
                    {
                        Sm.StdMsg(mMsgType.Info, "Entity Code is empty at row #" + (i + 1));
                        return true;
                    }
                }
            }

            return false;

        }

        private bool IsDocDtEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); ++i)
            {
                if (lJournal[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDocDtFormatNotValid(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].DocDt.Contains("/") || lJournal[i].DocDt.Contains("-") || lJournal[i].DocDt.Contains(".") || lJournal[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsJnDescEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].JnDesc.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Journal Description is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPeriodEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].Period.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Period is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPeriodFormatNotValid(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].Period.Contains("/") || lJournal[i].Period.Contains("-") || lJournal[i].Period.Contains(".") || lJournal[i].Period.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Period should be 'yyyymm' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsRemarkEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].Remark.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Remark is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsCOAEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].AcNo.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "COA is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsDAmtEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].DAmt.ToString().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Debit Amount is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCAmtEmpty(ref List<Journal> lJournal)
        {
            for (int i = 0; i < lJournal.Count(); i++)
            {
                if (lJournal[i].CAmt.ToString().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Credit Amount is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBalancedNotValid(ref List<Journal> lJournal)
        {
            decimal TotalDebit = 0m, TotalCredit = 0m;
            string mDocNo = string.Empty;

            foreach (var x in lJournal)
            {
                if (mDocNo.Length == 0) mDocNo = x.DocNo;

                if (mDocNo == x.DocNo)
                {
                    TotalDebit += x.DAmt;
                    TotalCredit += x.CAmt;
                }
                else
                {
                    if (TotalCredit != TotalDebit)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Debit and credit is not balanced on data : " + x.Remark);
                        return true;
                    }

                    mDocNo = x.DocNo;
                    TotalDebit = x.DAmt;
                    TotalCredit = x.CAmt;
                }
            }

            return false;

            //string mRemark = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            //for (int i = 0; i < lJournal.Count(); ++i)
            //{
            //    mMth = (lJournal[i].DocDt).Substring(4, 2);
            //    mYr = (lJournal[i].DocDt).Substring(0, 4);
            //    mRemark = lJournal[i].Remark;


            //    if (lJournal[i].DAmt.ToString().Length != 0) Debit = lJournal[i].DAmt;
            //    if (lJournal[i].CAmt.ToString().Length != 0) Credit = lJournal[i].CAmt;
            //    if (lJournal[i].Mth == mMth || lJournal[i].Yr == mYr || lJournal[i].Remark == mRemark)
            //    {
            //        if (Debit != Credit)
            //        {
            //            Sm.StdMsg(mMsgType.Warning, "Debit and credit is not balanced");
            //            return true;
            //        }

            //    }
            //}
            //return false;
        }

        private void SetIndex(ref List<Journal> lJournal)
        {
            string mRemark = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            int mIndex = 0;

            for (int i = 0; i < lJournal.Count; i++)
            {
                lJournal[i].Mth = (lJournal[i].DocDt).Substring(4, 2);
                lJournal[i].Yr = (lJournal[i].DocDt).Substring(0, 4);

                if (lJournal[i].Remark != mRemark || lJournal[i].DocDt != mDocDt)
                {
                    if (lJournal[i].Mth != mMth || lJournal[i].Yr != mYr)
                    {
                        mIndex = 1;
                        mRemark = lJournal[i].Remark;
                        mDocDt = lJournal[i].DocDt;
                        mYr = (lJournal[i].DocDt).Substring(0, 4);
                        mMth = (lJournal[i].DocDt).Substring(4, 2);
                    }
                    else mIndex++;

                }
                lJournal[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<Journal> lJournal)
        {
            string mDocNo = string.Empty, mRemark = string.Empty, mDocDt = string.Empty,
                mJournalDocNo = string.Empty;
            int mIndex = 1;

            for (int i = 0; i < lJournal.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    if (mDocNoFormat == "1")
                        lJournal[i].DocNo = GenerateDocNoJournal(lJournal[i].DocDt, lJournal[i].Index);
                    else
                        lJournal[i].DocNo = Sm.GenerateDocNo3(lJournal[i].DocDt, "Journal", "TblJournalHdr", lJournal[i].Entity, lJournal[i].Index.ToString());

                    mDocNo = lJournal[i].DocNo;
                    mRemark = lJournal[i].Remark;
                    mDocDt = lJournal[i].DocDt;
                }
                else
                {
                    if (mRemark != lJournal[i].Remark || mDocDt != lJournal[i].DocDt)
                    {
                        if (mDocNoFormat == "1")
                            lJournal[i].DocNo = GenerateDocNoJournal(lJournal[i].DocDt, lJournal[i].Index);
                        else
                            lJournal[i].DocNo = Sm.GenerateDocNo3(lJournal[i].DocDt, "Journal", "TblJournalHdr", lJournal[i].Entity, lJournal[i].Index.ToString());

                        mDocNo = lJournal[i].DocNo;
                        mRemark = lJournal[i].Remark;
                        mDocDt = lJournal[i].DocDt;

                    }
                    else
                    {
                        lJournal[i].DocNo = mDocNo;

                    }
                }
                lJournal[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<Journal> l, ref List<JournalHdr> l2, ref List<JournalDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l
                .Select(x => new { x.DocNo, x.DocDt, x.JnDesc, x.Period })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new JournalHdr()
                    {
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        JnDesc = m.JnDesc,
                        Period = m.Period

                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new JournalHdr()
                        {
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            JnDesc = m.JnDesc,
                            Period = m.Period
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }


            foreach (var n in l.Select(x => new { x.DocNo, x.DNo, x.AcNo, x.DAmt, x.CAmt, x.Remark, x.Entity })
               .OrderBy(o => o.DocNo))
                if (n.DocNo.Trim().Length > 0 || n.DNo.Trim().Length > 0 || n.AcNo.Trim().Length > 0 || n.DAmt.ToString().Trim().Length > 0 | n.CAmt.ToString().Trim().Length > 0
                    || n.Remark.Trim().Length > 0 || n.Entity.Trim().Length > 0)
                {
                    l3.Add(new JournalDtl()
                    {
                        DocNo = n.DocNo,
                        DNo = n.DNo,
                        AcNo = n.AcNo,
                        DAmt = n.DAmt,
                        CAmt = n.CAmt,
                        Remark = n.Remark,
                        Entity = n.Entity

                    });
                }
        }

        private void ProcessDtl1(ref List<JournalDtl> lJournalDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lJournalDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = lJournalDtl[i].DocNo;
                    lJournalDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lJournalDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = lJournalDtl[i].DocNo;

                    }

                    lJournalDtl[i].DNo = mDNo;

                }
            }
        }

        private MySqlCommand InsertJournalHdr(JournalHdr x)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("INSERT INTO tbljournalhdr (DocNo, DocDt, CancelInd, JnDesc, CurCode, ");
            SQL.AppendLine("MenuCode, MenuDesc, Period, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @JnDesc, ");
            SQL.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='Maincurcode'), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Period, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@JnDesc", x.JnDesc);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@Period", x.Period);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return (cm);
        }

        private MySqlCommand InsertJournalDtl(JournalDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO tbljournaldtl (DocNo, DNo, AcNo, EntCode, DAmt, CAmt,  ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @AcNo, @EntCode, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime());  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@AcNo", y.AcNo);
            Sm.CmParam<String>(ref cm, "@EntCode", y.Entity);
            Sm.CmParam<Decimal>(ref cm, "@DAmt", y.DAmt);
            Sm.CmParam<Decimal>(ref cm, "@CAmt", y.CAmt);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return (cm);

        }

        #endregion 

        #region Updating Item's Cost Category

        private bool IsInsertedItemCostCategoryInvalid(ref List<ItemCostCategory> lItemCostCategory)
        {
            return
                IsItemCodeEmpty(ref lItemCostCategory) ||
                IsCostCategoryCodeEmpty(ref lItemCostCategory) ||
                IsCostCustomerCodeEmpty(ref lItemCostCategory) ||
                IsItemCodeDuplicated(ref lItemCostCategory);
        }

        private bool IsItemCodeEmpty(ref List<ItemCostCategory> lItemCostCategory)
        {
            for (int i = 0; i < lItemCostCategory.Count(); i++)
            {
                if (lItemCostCategory[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCostCategoryCodeEmpty(ref List<ItemCostCategory> lItemCostCategory)
        {
            for (int i = 0; i < lItemCostCategory.Count(); i++)
            {
                if (lItemCostCategory[i].CCCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost Center Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCostCustomerCodeEmpty(ref List<ItemCostCategory> lItemCostCategory)
        {
            for (int i = 0; i < lItemCostCategory.Count(); i++)
            {
                if (lItemCostCategory[i].CCtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost Category Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItemCodeDuplicated(ref List<ItemCostCategory> lItemCostCategory)
        {
            for (int i = 0; i < lItemCostCategory.Count(); i++)
            {
                for (int j = (i + 1); j < lItemCostCategory.Count(); j++)
                {
                    if (lItemCostCategory[i].ItCode == lItemCostCategory[j].ItCode && lItemCostCategory[i].CCCode == lItemCostCategory[j].CCCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Item code " + lItemCostCategory[i].ItCode + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private void InsertItemCostCategory(ItemCostCategory x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemCostCategory(ItCode, CCCode, CCtCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ItCode, @CCCode, @CCtCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("ON Duplicate Key Update CCtCode = @CCtCode,LastUpBy=@UserCode, LastUpDt=CurrentDateTime();  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", x.CCtCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Budget Category

        private bool IsInsertedBudgetCategoryInvalid(ref List<BudgetCategory> lBudgetCategory)
        {
            return
                IsBCNameEmpty(ref lBudgetCategory) ||
                IsBCDeptCodeEmpty(ref lBudgetCategory) ||
                IsBCBudgetTypeEmpty(ref lBudgetCategory) ||
                (mIsBudgetCategoryUseCostCategory && IsBCCostCategoryEmpty(ref lBudgetCategory)) ||
                (mIsBudgetCategoryUseCostCenter && IsBCCostCenterEmpty(ref lBudgetCategory)) ||
                (mIsBudgetCategoryUseCOA && IsBCCOAEmpty(ref lBudgetCategory)) ||
                IsBCCodeDuplicated(ref lBudgetCategory);
        }

        private bool IsBCNameEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].BCName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Budget Category Name is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCDeptCodeEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].DeptCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Department Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCBudgetTypeEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].BudgetType.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Budget Type Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCCostCategoryEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].CCtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost Category Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCCostCenterEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].CCCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Cost Center Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCCOAEmpty(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                if (lBudgetCategory[i].AcNo.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "COA's Account is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsBCCodeDuplicated(ref List<BudgetCategory> lBudgetCategory)
        {
            for (int i = 0; i < lBudgetCategory.Count(); i++)
            {
                for (int j = (i + 1); j < lBudgetCategory.Count(); j++)
                {
                    if (lBudgetCategory[i].BCName == lBudgetCategory[j].BCName
                        && lBudgetCategory[i].DeptCode == lBudgetCategory[j].DeptCode)
                    {
                        Sm.StdMsg(mMsgType.Info, "Budget category name " + lBudgetCategory[i].BCName + " is duplicated. Row #" + (i + 1) + " and row #" + (j + 1));
                        return true;
                    }
                }
            }

            return false;
        }

        private void InsertBudgetCategory(BudgetCategory x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetCategory(BCCode, LocalCode, BCName, DeptCode, BudgetType, CCtCode, CCCode, AcNo, ActInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@BCCode, @LocalCode, @BCName, @DeptCode, @BudgetType, @CCtCode, @CCCode, @AcNo, 'Y', @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update LocalCode=@LocalCode, BCName=@BCName, DeptCode=@DeptCode, BudgetType=@BudgetType, CCtCode=@CCtCode, CCCode=@CCCode, AcNo=@AcNo, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString(), CommandTimeout = 1800 };

            Sm.CmParam<String>(ref cm, "@BCCode", GenerateBCCode());
            Sm.CmParam<String>(ref cm, "@LocalCode", x.LocalCode);
            Sm.CmParam<String>(ref cm, "@BCName", x.BCName);
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParam<String>(ref cm, "@BudgetType", x.BudgetType);
            Sm.CmParam<String>(ref cm, "@CCtCode", x.CCtCode);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);
            Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ClearData();
        }

        #endregion

        #region Item's Price

        private bool IsInsertedItemPriceInvalid(ref List<ItemPrice> lItemPrice)
        {
            return
                IsDocDtEmpty(ref lItemPrice) ||
                IsDocDtFormatInvalid(ref lItemPrice) ||
                IsStartDtEmpty(ref lItemPrice) ||
                IsStartDtFormatInvalid(ref lItemPrice) ||
                IsCurCodeEmpty(ref lItemPrice) ||
                IsPriceUOMCodeEmpty(ref lItemPrice) ||
                IsItCodeEmpty(ref lItemPrice) ||
                IsUPriceEmpty(ref lItemPrice);
        }

        private bool IsDocDtEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].DocDt.Contains("/") || lItemPrice[i].DocDt.Contains("-") || lItemPrice[i].DocDt.Contains(".") || lItemPrice[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDtEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].StartDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "StartDt Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsStartDtFormatInvalid(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].StartDt.Contains("/") || lItemPrice[i].StartDt.Contains("-") || lItemPrice[i].StartDt.Contains(".") || lItemPrice[i].StartDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private bool IsCurCodeEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].CurCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Currency Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsPriceUOMCodeEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].PriceUomCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Price UOM Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItCodeEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsUPriceEmpty(ref List<ItemPrice> lItemPrice)
        {
            for (int i = 0; i < lItemPrice.Count(); i++)
            {
                if (lItemPrice[i].UPrice <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Unit Price is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }



        private void SetIndex(ref List<ItemPrice> lItemPrice)
        {
            string mCurCode = string.Empty, mPriceUOMCode = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lItemPrice.Count; i++)
            {
                if (mCurCode.Length < 0)
                {
                    mIndex = 1;
                    mPriceUOMCode = lItemPrice[i].PriceUomCode;
                    mCurCode = lItemPrice[i].CurCode;

                }
                else
                {
                    if (lItemPrice[i].CurCode != mCurCode || lItemPrice[i].PriceUomCode != mPriceUOMCode)
                    {
                        mIndex++;
                        mPriceUOMCode = lItemPrice[i].PriceUomCode;
                        mCurCode = lItemPrice[i].CurCode;
                    }
                }


                lItemPrice[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<ItemPrice> lItemPrice)
        {
            string mDocNo = string.Empty, mCurCode = string.Empty, mPriceUOMCode = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lItemPrice.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    lItemPrice[i].DocNo = GenerateDocNo(lItemPrice[i].Index, lItemPrice[i].DocDt, "ItemPrice", "TblItemPriceHdr");
                    mDocNo = lItemPrice[i].DocNo;
                    mCurCode = lItemPrice[i].CurCode;
                    mPriceUOMCode = lItemPrice[i].PriceUomCode;
                }
                else
                {
                    if (mCurCode != lItemPrice[i].CurCode || mPriceUOMCode != lItemPrice[i].PriceUomCode)
                    {
                        lItemPrice[i].DocNo = GenerateDocNo(lItemPrice[i].Index, lItemPrice[i].DocDt, "ItemPrice", "TblItemPriceHdr");
                        mDocNo = lItemPrice[i].DocNo;
                        mCurCode = lItemPrice[i].CurCode;
                        mPriceUOMCode = lItemPrice[i].PriceUomCode;
                    }
                    else
                    {
                        lItemPrice[i].DocNo = mDocNo;
                    }
                }
                lItemPrice[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<ItemPrice> l, ref List<ItemPriceHdr> l2, ref List<ItemPriceDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l.Select(x => new { x.Index, x.DocNo, x.DocDt, x.StartDt, x.CurCode, x.PriceUomCode, x.Remark })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new ItemPriceHdr()
                    {
                        Index = m.Index,
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        StartDt = m.StartDt,
                        CurCode = m.CurCode,
                        PriceUomCode = m.PriceUomCode,
                        Remark = m.Remark
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new ItemPriceHdr()
                        {
                            Index = m.Index,
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            StartDt = m.StartDt,
                            CurCode = m.CurCode,
                            PriceUomCode = m.PriceUomCode,
                            Remark = m.Remark
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new
            {
                x.DocNo,
                x.DNo,
                x.ItCode,
                x.UPrice,
                x.RemarkDtl
            })
                .OrderBy(o => o.DocNo)
               )
                if (n.ItCode.Trim().Length > 0 || n.UPrice.ToString().Trim().Length > 0 || n.RemarkDtl.Trim().Length > 0)
                {
                    l3.Add(new ItemPriceDtl()
                    {
                        DocNo = n.DocNo,
                        DNo = n.DNo,
                        ItCode = n.ItCode,
                        UPrice = n.UPrice,
                        RemarkDtl = n.RemarkDtl,
                    });
                }
        }

        private void ProcessDtl(ref List<ItemPriceDtl> lItemPriceDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lItemPriceDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), int.Parse(mItemPriceDNoLength));
                    mDocNo = lItemPriceDtl[i].DocNo;
                    lItemPriceDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lItemPriceDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), int.Parse(mItemPriceDNoLength));
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), int.Parse(mItemPriceDNoLength));
                        mDocNo = lItemPriceDtl[i].DocNo;
                    }
                    lItemPriceDtl[i].DNo = mDNo;
                }
            }
        }

        //private MySqlCommand InsertItemPriceHdr(ItemPriceHdr x)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblItemPriceHdr(DocNo, DocDt, ActInd, StartDt, CurCode, PriceUomCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values (@DocNo, @DocDt, 'Y', @StartDt, @CurCode, @PriceUomCode, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
        //    Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
        //    Sm.CmParam<String>(ref cm, "@StartDt", x.StartDt);
        //    Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
        //    Sm.CmParam<String>(ref cm, "@PriceUomCode", x.PriceUomCode);
        //    Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand InsertItemPriceDtl(ItemPriceDtl y)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblItemPriceDtl(DocNo,DNo,ItCode,UPrice,Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values (@DocNo,@DNo,@ItCode,@UPrice,@Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", y.ItCode);
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", y.UPrice);
        //    Sm.CmParam<String>(ref cm, "@Remark", y.RemarkDtl);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        private MySqlCommand InsertItemPriceHdr(List<ItemPriceHdr> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblItemPriceHdr(DocNo, DocDt, ActInd, StartDt, CurCode, PriceUomCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int i = 0;

            foreach (var x in l2)
            {
                if (i != 0) SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo__" + i.ToString() + ", @DocDt__" + i.ToString() + ", 'Y', @StartDt__" + i.ToString() + ", @CurCode__" + i.ToString() + ", @PriceUomCode__" + i.ToString() + ", @Remark__" + i.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DocNo__" + i.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "@DocDt__" + i.ToString(), x.DocDt);
                Sm.CmParam<String>(ref cm, "@StartDt__" + i.ToString(), x.StartDt);
                Sm.CmParam<String>(ref cm, "@CurCode__" + i.ToString(), x.CurCode);
                Sm.CmParam<String>(ref cm, "@PriceUomCode__" + i.ToString(), x.PriceUomCode);
                Sm.CmParam<String>(ref cm, "@Remark__" + i.ToString(), x.Remark);

                i++;
            }

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertItemPriceDtl(List<ItemPriceDtl> l3)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblItemPriceDtl(DocNo,DNo,ItCode,UPrice,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int i = 0;

            foreach (var x in l3)
            {
                if (i != 0) SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo__" + i.ToString() + ", @DNo__" + i.ToString() + ", @ItCode__" + i.ToString() + ", @UPrice__" + i.ToString() + ", @Remark__" + i.ToString() + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DocNo__" + i.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + i.ToString(), x.DNo);
                Sm.CmParam<String>(ref cm, "@ItCode__" + i.ToString(), x.ItCode);
                Sm.CmParam<Decimal>(ref cm, "@UPrice__" + i.ToString(), x.UPrice);
                Sm.CmParam<String>(ref cm, "@Remark__" + i.ToString(), x.RemarkDtl);

                i++;
            }

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region DO To Customer

        private bool IsInsertedDOCtInvalid(ref List<DOToCustomer> lDOCt)
        {
            return
                IsDocDtEmpty(ref lDOCt) ||
                IsDocDtFormatInvalid(ref lDOCt) ||
                IsWhsCodeEmpty(ref lDOCt) ||
                IsCtCodeEmpty(ref lDOCt) ||
                IsItCodeEmpty(ref lDOCt) ||
                IsQtyEmpty(ref lDOCt) ||
                IsQty2Empty(ref lDOCt) ||
                IsQty3Empty(ref lDOCt) ||
                IsUPriceEmpty(ref lDOCt) ||
                IsClosingJournalInvalid(ref lDOCt) ||
                IsDocDtInvalid(ref lDOCt);
        }

        private bool IsDocDtEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsWhsCodeEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].WhsCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Warehouse Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsCtCodeEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].CtCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Customer Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsItCodeEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].ItCode.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Item Code is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsClosingJournalInvalid(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (Sm.IsClosingJournalInvalid(lDOCt[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDocDtInvalid(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (Sm.IsDocDtNotValid(Sm.CompareStr(
                    Sm.GetParameter("InventoryDocDtValidInd"), "Y"), lDOCt[i].DocDt))
                    return true;
            }
            return false;
        }

        private bool IsDocDtFormatInvalid(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].DocDt.Contains("/") || lDOCt[i].DocDt.Contains("-") || lDOCt[i].DocDt.Contains(".") || lDOCt[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsQtyEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].Qty <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Qty is empty(0) at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsQty2Empty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].Qty2 <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Qty2 is empty(0) at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsQty3Empty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].Qty <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Qty is empty(0) at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsUPriceEmpty(ref List<DOToCustomer> lDOCt)
        {
            for (int i = 0; i < lDOCt.Count(); i++)
            {
                if (lDOCt[i].UPrice <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "UPrice is empty(0) at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }


        private void SetIndex(ref List<DOToCustomer> lDOCt)
        {
            string mWhsCode = string.Empty, mCtCode = string.Empty, mDocDt = string.Empty, mMth = string.Empty, mYr = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lDOCt.Count; i++)
            {
                lDOCt[i].Mth = (lDOCt[i].DocDt).Substring(4, 2);
                lDOCt[i].Yr = (lDOCt[i].DocDt).Substring(0, 4);
                if (lDOCt[i].WhsCode != mWhsCode || lDOCt[i].DocDt != mDocDt || lDOCt[i].CtCode != mCtCode)
                {
                    if (lDOCt[i].Mth != mMth || lDOCt[i].Yr != mYr)
                    {
                        mIndex = 1;
                        mWhsCode = lDOCt[i].WhsCode;
                        mCtCode = lDOCt[i].CtCode;
                        mDocDt = lDOCt[i].DocDt;
                        mYr = (lDOCt[i].DocDt).Substring(0, 4);
                        mMth = (lDOCt[i].DocDt).Substring(4, 2);
                    }
                    else mIndex++;
                }
                lDOCt[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<DOToCustomer> lDOCt)
        {
            string mDocNo = string.Empty, mWhsCode = string.Empty, mCtCode = string.Empty, mDocDt = string.Empty, mJournalDocNo = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < lDOCt.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    lDOCt[i].DocNo = GenerateDocNo(lDOCt[i].Index, lDOCt[i].DocDt, "DOCt", "TblDOCtHdr");
                    mDocNo = lDOCt[i].DocNo;
                    mWhsCode = lDOCt[i].WhsCode;
                    mDocDt = lDOCt[i].DocDt;
                    mCtCode = lDOCt[i].CtCode;
                }
                else
                {
                    if (mWhsCode != lDOCt[i].WhsCode || mDocDt != lDOCt[i].DocDt || mCtCode != lDOCt[i].CtCode)
                    {
                        lDOCt[i].DocNo = GenerateDocNo(lDOCt[i].Index, lDOCt[i].DocDt, "DOCt", "TblDOCtHdr");
                        mDocNo = lDOCt[i].DocNo;
                        mWhsCode = lDOCt[i].WhsCode;
                        mDocDt = lDOCt[i].DocDt;
                        mCtCode = lDOCt[i].CtCode;
                    }
                    else
                    {
                        lDOCt[i].DocNo = mDocNo;
                    }
                }
                lDOCt[i].Index = mIndex;
            }
        }

        private void DataBreakDown(ref List<DOToCustomer> l, ref List<DOToCustomerHdr> l2, ref List<DOToCustomerDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l.Select(x => new { x.Index, x.DocNo, x.DocDt, x.WhsCode, x.CtCode, x.DocNoInternal, x.SAName, x.SAAddress, x.SACityCode, x.SACntCode, x.SAPostalCd, x.SAPhone, x.SAFax, x.SAEmail, x.SAMobile, x.CurCode, x.Driver, x.Expedition, x.VehicleRegNo, x.Remark })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new DOToCustomerHdr()
                    {
                        Index = m.Index,
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        WhsCode = m.WhsCode,
                        CtCode = m.CtCode,
                        DocNoInternal = m.DocNoInternal,
                        SAName = m.SAName,
                        SAAddress = m.SAAddress,
                        SACityCode = m.SACityCode,
                        SACntCode = m.SACntCode,
                        SAPostalCd = m.SAPostalCd,
                        SAPhone = m.SAPhone,
                        SAFax = m.SAFax,
                        SAEmail = m.SAEmail,
                        SAMobile = m.SAMobile,
                        CurCode = m.CurCode,
                        Driver = m.Driver,
                        Expedition = m.Expedition,
                        VehicleRegNo = m.VehicleRegNo,
                        Remark = m.Remark
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new DOToCustomerHdr()
                        {
                            Index = m.Index,
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            WhsCode = m.WhsCode,
                            CtCode = m.CtCode,
                            DocNoInternal = m.DocNoInternal,
                            SAName = m.SAName,
                            SAAddress = m.SAAddress,
                            SACityCode = m.SACityCode,
                            SACntCode = m.SACntCode,
                            SAPostalCd = m.SAPostalCd,
                            SAPhone = m.SAPhone,
                            SAFax = m.SAFax,
                            SAEmail = m.SAEmail,
                            SAMobile = m.SAMobile,
                            CurCode = m.CurCode,
                            Driver = m.Driver,
                            Expedition = m.Expedition,
                            VehicleRegNo = m.VehicleRegNo,
                            Remark = m.Remark
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new
            {
                x.DocNo,
                x.DNo,
                x.ItCode,
                x.PropCode,
                x.BatchNo,
                x.Source,
                x.Lot,
                x.Bin,
                x.Qty,
                x.Qty2,
                x.Qty3,
                x.UPrice,
                x.RemarkDtl
            })
                .OrderBy(o => o.DocNo)
               )
                if (n.ItCode.Trim().Length > 0 || n.PropCode.Trim().Length > 0 || n.BatchNo.Trim().Length > 0 || n.Source.Trim().Length > 0
                    || n.Lot.Trim().Length > 0 || n.Bin.Trim().Length > 0 || n.Qty.ToString().Trim().Length > 0 || n.Qty2.ToString().Trim().Length > 0
                    || n.Qty3.ToString().Trim().Length > 0 || n.UPrice.ToString().Trim().Length > 0 || n.RemarkDtl.Trim().Length > 0)
                {
                    l3.Add(new DOToCustomerDtl()
                    {
                        DocNo = n.DocNo,
                        DNo = n.DNo,
                        ItCode = n.ItCode,
                        PropCode = n.PropCode,
                        BatchNo = n.BatchNo,
                        Source = n.Source,
                        Lot = n.Lot,
                        Bin = n.Bin,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                        UPrice = n.UPrice,
                        RemarkDtl = n.RemarkDtl,
                    });
                }
        }

        private void ProcessDtl(ref List<DOToCustomerDtl> lDOCtDtl)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < lDOCtDtl.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "0000000001";
                    mDocNo = lDOCtDtl[i].DocNo;
                    lDOCtDtl[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == lDOCtDtl[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 10);
                    }
                    else
                    {
                        mDNo = "0000000001";
                        mDocNo = lDOCtDtl[i].DocNo;
                    }
                    lDOCtDtl[i].DNo = mDNo;
                }
            }
        }

        private MySqlCommand InsertDOCtHdr(DOToCustomerHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCtHdr(DocNo, DocDt, WhsCode, CtCode, DocNoInternal, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, CurCode, Driver, Expedition, VehicleRegNo, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @CtCode, @DocNoInternal, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, @CurCode, @Driver, @Expedition, @VehicleRegNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@CtCode", x.CtCode);
            Sm.CmParam<String>(ref cm, "@DocNoInternal", x.DocNoInternal);
            Sm.CmParam<String>(ref cm, "@SAName", x.SAName);
            Sm.CmParam<String>(ref cm, "@SAAddress", x.SAAddress);
            Sm.CmParam<String>(ref cm, "@SACityCode", x.SACityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", x.SACntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", x.SAPostalCd);
            Sm.CmParam<String>(ref cm, "@SAPhone", x.SAPhone);
            Sm.CmParam<String>(ref cm, "@SAFax", x.SAFax);
            Sm.CmParam<String>(ref cm, "@SAEmail", x.SAEmail);
            Sm.CmParam<String>(ref cm, "@SAMobile", x.SAMobile);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@Driver", x.Driver);
            Sm.CmParam<String>(ref cm, "@Expedition", x.Expedition);
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", x.VehicleRegNo);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDOCtDtl(DOToCustomerDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo,CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, IfNull(@PropCode, '-'), ");
            SQL.AppendLine("IfNull(@BatchNo, ");
            SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
            SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", y.ItCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", y.BatchNo);
            Sm.CmParam<String>(ref cm, "@PropCode", y.PropCode);
            Sm.CmParam<String>(ref cm, "@Lot", y.Lot);
            Sm.CmParam<String>(ref cm, "@Bin", y.Bin);
            Sm.CmParam<String>(ref cm, "@DocType", "07");
            Sm.CmParam<Decimal>(ref cm, "@Qty", y.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", y.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", y.Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", y.UPrice);
            Sm.CmParam<String>(ref cm, "@Remark", y.RemarkDtl);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Personal Information 

        private bool IsInsertedPersonalInformationInvalid(ref List<PersonalInformation> PIH, ref List<PersonalInformationDtl2> PIdtl2,
            ref List<PersonalInformationDtl3> PIDtl3, ref List<PersonalInformationDtl4> PIDtl4, ref List<PersonalInformationDtl6> PIDtl6)
        {
            return
                IsDateFormatInvalid(ref PIH) ||
                IsEmpNameInvalid(ref PIH) ||
                IsFamilyReferenceInvalid(ref PIdtl2) ||
                IsWorkExperienceInvalid(ref PIDtl3) ||
                IsEducationInvalid(ref PIDtl4) ||
                isTrainingInvalid(ref PIDtl6) ||
                IsPersonalRefDt(ref PIH);
        }

        private bool IsPersonalRefDt(ref List<PersonalInformation> PIH)
        {
            if (PIH.Count() > 0)
            {
                for (int i = 0; i < PIH.Count(); i++)
                {
                    if (PIH[i].InterviewDt.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Interview Date is empty at row #" + (i + 1));
                        return true;
                    }

                    if (PIH[i].StartWorkDt.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Start Work Date is empty at row #" + (i + 1));
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDocDtEmpty(ref List<PersonalInformationHdr> PIH)
        {
            for (int i = 0; i < PIH.Count(); i++)
            {
                if (PIH
                    [i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsDateFormatInvalid(ref List<PersonalInformation> PIH)
        {
            for (int i = 0; i < PIH.Count(); i++)
            {
                if (PIH[i].DocDt.Contains("/") || PIH[i].DocDt.Contains("-") ||
                    PIH[i].DocDt.Contains(".") || PIH[i].DocDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Document date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
                if (PIH[i].JoinDate.Contains("/") || PIH[i].JoinDate.Contains("-") ||
                    PIH[i].JoinDate.Contains(".") || PIH[i].JoinDate.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Join date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }

                if (PIH[i].ResignDt.Contains("/") || PIH[i].ResignDt.Contains("-") ||
                    PIH[i].ResignDt.Contains(".") || PIH[i].ResignDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Resign date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }

                if (PIH[i].WedingDt.Contains("/") || PIH[i].WedingDt.Contains("-") ||
                    PIH[i].WedingDt.Contains(".") || PIH[i].WedingDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Wedding date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }

                if (PIH[i].BirthDt.Contains("/") || PIH[i].BirthDt.Contains("-") ||
                    PIH[i].BirthDt.Contains(".") || PIH[i].BirthDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format Birth date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        private bool IsJoinDtFormatInvalid(ref List<PersonalInformation> PersonalInformation)
        {
            for (int i = 0; i < PersonalInformation.Count(); i++)
            {
                if (PersonalInformation[i].JoinDate.Contains("/") || PersonalInformation[i].JoinDate.Contains("-") ||
                    PersonalInformation[i].JoinDate.Contains(".") || PersonalInformation[i].JoinDate.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        private bool IsEmpNameInvalid(ref List<PersonalInformation> PIH)
        {
            for (int i = 0; i < PIH.Count(); i++)
            {
                if (PIH[i].EmpName.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Employee Name is empty at row #" + (i + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFamilyReferenceInvalid(ref List<PersonalInformationDtl2> PIDtl2)
        {
            if (PIDtl2.Count() > 0)
            {
                for (int i = 0; i < PIDtl2.Count(); i++)
                {
                    if (PIDtl2[i].FamilyName.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Family Name is empty at row #" + (i + 1));
                        return true;
                    }

                    if (PIDtl2[i].FamilyGender.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Gender is empty at row #" + (i + 1));
                        return true;
                    }

                    if (PIDtl2[i].FamilyBirtDt.Contains("/") || PIDtl2[i].FamilyBirtDt.Contains("-") ||
                        PIDtl2[i].FamilyBirtDt.Contains(".") || PIDtl2[i].FamilyBirtDt.Contains(" "))
                    {
                        Sm.StdMsg(mMsgType.Info, "Format Family Birth date should be 'yyyymmdd' at row #" + (i + 1));
                        return true;
                    }

                }
            }
            return false;
        }

        private bool IsWorkExperienceInvalid(ref List<PersonalInformationDtl3> PIDtl3)
        {
            if (PIDtl3.Count() > 0)
            {
                for (int i = 0; i < PIDtl3.Count(); i++)
                {
                    if (PIDtl3[i].Company.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Company is empty at row #" + (i + 1));
                        return true;
                    }


                }
            }
            return false;
        }

        private bool IsEducationInvalid(ref List<PersonalInformationDtl4> PIDtl4)
        {
            if (PIDtl4.Count() > 0)
            {
                for (int i = 0; i < PIDtl4.Count(); i++)
                {
                    if (PIDtl4[i].School.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "School is empty at row #" + (i + 1));
                        return true;
                    }

                    if (PIDtl4[i].Level.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Level is empty at row #" + (i + 1));
                        return true;
                    }
                }
            }
            return false;
        }

        private bool isTrainingInvalid(ref List<PersonalInformationDtl6> PIDtl6)
        {
            if (PIDtl6.Count() > 0)
            {
                for (int i = 0; i < PIDtl6.Count(); i++)
                {
                    if (PIDtl6[i].SpecTraining.Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Info, "Specialization is empty at row #" + (i + 1));
                        return true;
                    }
                }
            }
            return false;
        }


        private void SetIndex(ref List<PersonalInformation> PersonalInformation)
        {
            string mDocDt = string.Empty, mEmpName = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < PersonalInformation.Count; i++)
            {
                if (mDocDt.Length < 0)
                {
                    mIndex = 1;
                    mDocDt = PersonalInformation[i].DocDt;
                    mEmpName = PersonalInformation[i].EmpName;

                }
                else
                {
                    if (PersonalInformation[i].DocDt != mDocDt || PersonalInformation[i].EmpName != mEmpName)
                    {
                        mIndex++;
                        mEmpName = PersonalInformation[i].EmpName;
                        mDocDt = PersonalInformation[i].DocDt;
                    }
                }


                PersonalInformation[i].Index = mIndex;
            }
        }

        private void SetDocNo(ref List<PersonalInformation> PersonalInformation)
        {
            string mDocNo = string.Empty, mDocDt = string.Empty, mEmpName = string.Empty;
            int mIndex = 0;
            for (int i = 0; i < PersonalInformation.Count; i++)
            {
                if (mDocNo.Length <= 0)
                {
                    PersonalInformation[i].DocNo = GenerateDocNo(PersonalInformation[i].Index, PersonalInformation[i].DocDt, "EmployeeRecruitment", "TblEmployeeRecruitment");
                    //PersonalInformation[i].DocNo = Sm.GenerateDocNo(PersonalInformation[i].DocDt, "EmployeeRecruitment", "TblEmployeeRecruitment");
                    mDocNo = PersonalInformation[i].DocNo;
                    mDocDt = PersonalInformation[i].DocDt;
                    mEmpName = PersonalInformation[i].EmpName;
                }
                else
                {
                    if (mDocDt != PersonalInformation[i].DocDt || mEmpName != PersonalInformation[i].EmpName)
                    {
                        PersonalInformation[i].DocNo = GenerateDocNo(PersonalInformation[i].Index, PersonalInformation[i].DocDt, "EmployeeRecruitment", "TblEmployeeRecruitment");
                        //PersonalInformation[i].DocNo = Sm.GenerateDocNo(PersonalInformation[i].DocDt, "EmployeeRecruitment", "TblEmployeeRecruitment");
                        mDocNo = PersonalInformation[i].DocNo;
                        mDocDt = PersonalInformation[i].DocDt;
                        mEmpName = PersonalInformation[i].EmpName;

                    }
                    else
                    {
                        PersonalInformation[i].DocNo = mDocNo;
                    }
                }
                PersonalInformation[i].Index = mIndex;

            }
        }

        private void DataBreakDown(ref List<PersonalInformation> l, ref List<PersonalInformationHdr> lh,
            ref List<PersonalInformationDtl2> l2, ref List<PersonalInformationDtl3> l3, ref List<PersonalInformationDtl4> l4, ref List<PersonalInformationDtl5> l5,
            ref List<PersonalInformationDtl6> l6, ref List<PersonalInformationDtl7> l7)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l.Select(x => new
            {
                x.Index,
                x.DocNo,
                x.EmpRequestNo,
                x.DocDt,
                x.EmpName,
                x.DisplayName,
                x.DNo,
                x.UserCode,
                x.OldCode,
                x.ShortCode,
                x.Barcode,
                x.EmploymentSts,
                x.Site,
                x.Divition,
                x.Department,
                x.Position,
                x.SectionCode,
                x.Workinggroup,
                x.POH,
                x.JoinDate,
                x.ResignDt,
                x.Adreess,
                x.city,
                x.SubDistrict,
                x.Vilage,
                x.RTRW,
                x.PostalCode,
                x.Domicily,
                x.Identity,
                x.MaritalSts,
                x.WedingDt,
                x.Gender,
                x.Religion,
                x.BirthDt,
                x.BirthPlace,
                x.BlodType,
                x.Mother,
                x.SystemType,
                x.GradeLevel,
                x.PayrollGroup,
                x.PayrollType,
                x.PayrunPeriod,
                x.NPWP,
                x.PTKP,
                x.BankName,
                x.BankBranch,
                x.AccountName,
                x.AccountNo,
                x.Phone,
                x.Mobile,
                x.Email,
                x.InterviewBy,
                x.InterviewDt,
                x.Place,
                x.GrossSalary,
                x.StartWorkDt,
                x.Placement,
                x.CompanyDept,
                x.Probation,
                x.NonIncomingTax
            })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    lh.Add(new PersonalInformationHdr()
                    {
                        Index = m.Index,
                        DocNo = m.DocNo,
                        EmpRequestNo = m.EmpRequestNo,
                        DocDt = m.DocDt,
                        EmpName = m.EmpName,
                        DisplayName = m.DisplayName,
                        UserCode = m.UserCode,
                        OldCode = m.OldCode,
                        ShortCode = m.ShortCode,
                        Barcode = m.Barcode,
                        EmploymentSts = m.EmploymentSts,
                        Site = m.Site,
                        Divition = m.Divition,
                        Department = m.Department,
                        Position = m.Position,
                        SectionCode = m.SectionCode,
                        Workinggroup = m.Workinggroup,
                        POH = m.POH,
                        JoinDate = m.JoinDate,
                        ResignDt = m.ResignDt,
                        Adreess = m.Adreess,
                        city = m.city,
                        SubDistrict = m.SubDistrict,
                        Vilage = m.Vilage,
                        RTRW = m.RTRW,
                        PostalCode = m.PostalCode,
                        Domicily = m.Domicily,
                        Identity = m.Identity,
                        MaritalSts = m.MaritalSts,
                        WedingDt = m.WedingDt,
                        Gender = m.Gender,
                        Religion = m.Religion,
                        BirthDt = m.BirthDt,
                        BirthPlace = m.BirthPlace,
                        BlodType = m.BlodType,
                        Mother = m.Mother,
                        SystemType = m.SystemType,
                        GradeLevel = m.GradeLevel,
                        PayrollGroup = m.PayrollGroup,
                        PayrollType = m.PayrollType,
                        PayrunPeriod = m.PayrunPeriod,
                        NPWP = m.NPWP,
                        NonIncomingTax = m.NonIncomingTax,
                        BankName = m.BankName,
                        BankBranch = m.BankBranch,
                        AccountName = m.AccountName,
                        AccountNo = m.AccountNo,
                        Phone = m.Phone,
                        Mobile = m.Mobile,
                        Email = m.Email,
                        InterviewBy = m.InterviewBy,
                        InterviewDt = m.InterviewDt,
                        Place = m.Place,
                        GrossSalary = m.GrossSalary,
                        StartWorkDt = m.StartWorkDt,
                        Placement = m.Placement,
                        CompanyDept = m.CompanyDept,
                        Probation = m.Probation

                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        lh.Add(new PersonalInformationHdr()
                        {
                            Index = m.Index,
                            DocNo = m.DocNo,
                            EmpRequestNo = m.EmpRequestNo,
                            DocDt = m.DocDt,
                            EmpName = m.EmpName,
                            DisplayName = m.DisplayName,
                            UserCode = m.UserCode,
                            OldCode = m.OldCode,
                            ShortCode = m.ShortCode,
                            Barcode = m.Barcode,
                            EmploymentSts = m.EmploymentSts,
                            Site = m.Site,
                            Divition = m.Divition,
                            Department = m.Department,
                            Position = m.Position,
                            SectionCode = m.SectionCode,
                            Workinggroup = m.Workinggroup,
                            POH = m.POH,
                            JoinDate = m.JoinDate,
                            ResignDt = m.ResignDt,
                            Adreess = m.Adreess,
                            city = m.city,
                            SubDistrict = m.SubDistrict,
                            Vilage = m.Vilage,
                            RTRW = m.RTRW,
                            PostalCode = m.PostalCode,
                            Domicily = m.Domicily,
                            Identity = m.Identity,
                            MaritalSts = m.MaritalSts,
                            WedingDt = m.WedingDt,
                            Gender = m.Gender,
                            Religion = m.Religion,
                            BirthDt = m.BirthDt,
                            BirthPlace = m.BirthPlace,
                            BlodType = m.BlodType,
                            Mother = m.Mother,
                            SystemType = m.SystemType,
                            GradeLevel = m.GradeLevel,
                            PayrollGroup = m.PayrollGroup,
                            PayrollType = m.PayrollType,
                            PayrunPeriod = m.PayrunPeriod,
                            NPWP = m.NPWP,
                            NonIncomingTax = m.NonIncomingTax,
                            BankName = m.BankName,
                            BankBranch = m.BankBranch,
                            AccountName = m.AccountName,
                            AccountNo = m.AccountNo,
                            Phone = m.Phone,
                            Mobile = m.Mobile,
                            Email = m.Email,
                            InterviewBy = m.InterviewBy,
                            InterviewDt = m.InterviewDt,
                            Place = m.Place,
                            GrossSalary = m.GrossSalary,
                            StartWorkDt = m.StartWorkDt,
                            Placement = m.Placement,
                            CompanyDept = m.CompanyDept,
                            Probation = m.Probation
                        });
                        mDocNoHdr = m.DocNo;
                    }
                }

            // breakdown dtl family 
            foreach (var dtl2 in l.Select(x2 => new
            {
                x2.DocNo,
                x2.DNo,
                x2.FamilyName,
                x2.FamilyBirtDt,
                x2.IDCard,
                x2.FamilyGender,
                x2.FamilySts,
                x2.FamilyRemark,
                x2.NIN
            })
            .OrderBy(o => o.DocNo)
            )
                if (dtl2.FamilyName.Length > 0 && dtl2.FamilyGender.Length > 0)
                {
                    l2.Add(new PersonalInformationDtl2()
                    {
                        DocNo = dtl2.DocNo,
                        DNo = dtl2.DNo,
                        FamilyName = dtl2.FamilyName,
                        FamilyGender = dtl2.FamilyGender,
                        FamilySts = dtl2.FamilySts,
                        FamilyBirtDt = dtl2.FamilyBirtDt,
                        IDCard = dtl2.IDCard,
                        NIN = dtl2.NIN,
                        FamilyRemark = dtl2.FamilyRemark
                    });
                }

            //breakdown dtl of Work Experiences
            foreach (var dtl3 in l.Select(x3 => new
            {
                x3.DocNo,
                x3.DNo,
                x3.Company,
                x3.WEPosition,
                x3.Period,
                x3.WERemark
            }).OrderBy(o => o.DocNo)
            )
                if (dtl3.Company.Length > 0)
                {
                    l3.Add(new PersonalInformationDtl3()
                    {
                        DocNo = dtl3.DocNo,
                        DNo = dtl3.DNo,
                        Company = dtl3.Company,
                        WEPosition = dtl3.WEPosition,
                        Period = dtl3.Period,
                        WERemark = dtl3.WERemark
                    });
                }

            // breakdown dtl of Education 
            foreach (var dtl4 in l.Select(x4 => new
            {
                x4.DocNo,
                x4.DNo,
                x4.School,
                x4.Level,
                x4.Major,
                x4.HighestInd,
                x4.EdcRemark
            }).OrderBy(o => o.DocNo)
            )
                if (dtl4.School.Length > 0 && dtl4.Level.Length > 0)
                {
                    l4.Add(new PersonalInformationDtl4()
                    {
                        DocNo = dtl4.DocNo,
                        DNo = dtl4.DNo,
                        School = dtl4.School,
                        Major = dtl4.Major,
                        Level = dtl4.Level,
                        HighestInd = dtl4.HighestInd,
                        EdcRemark = dtl4.EdcRemark
                    });
                }
            // Breakdown dtl of Competence 
            foreach (var dtl5 in l.Select(x5 => new
            {
                x5.DocNo,
                x5.DNo,
                x5.CompetenceName,
                x5.Description,
                x5.Score
            }).OrderBy(o => o.DocNo)
            )
                if (dtl5.CompetenceName.Length > 0 || dtl5.Description.Length > 0 || dtl5.Score > 0)
                {
                    l5.Add(new PersonalInformationDtl5()
                    {
                        DocNo = dtl5.DocNo,
                        DNo = dtl5.DNo,
                        CompetenceName = dtl5.CompetenceName,
                        Description = dtl5.Description,
                        Score = dtl5.Score
                    });
                }
            // Breakdown dtl of Training 
            foreach (var dtl6 in l.Select(x6 => new
            {
                x6.DocNo,
                x6.DNo,
                x6.SpecTraining,
                x6.TrainingPlace,
                x6.TrainingPeriod,
                x6.TrainingYear,
                x6.TrainingRemark
            }).OrderBy(o => o.DocNo)
            )
                if (dtl6.SpecTraining.Length > 0)
                {
                    l6.Add(new PersonalInformationDtl6()
                    {
                        DocNo = dtl6.DocNo,
                        DNo = dtl6.DNo,
                        SpecTraining = dtl6.SpecTraining,
                        TrainingPlace = dtl6.TrainingPlace,
                        TrainingPeriod = dtl6.TrainingPeriod,
                        TrainingYear = dtl6.TrainingYear,
                        TrainingRemark = dtl6.TrainingRemark
                    });
                }
            // Breakdown dtl of Reference
            foreach (var dtl7 in l.Select(x7 => new
            {
                x7.DocNo,
                x7.DNo,
                x7.PRName,
                x7.PRPhone,
                x7.PRAddress,
                x7.PRRemark
            }).OrderBy(o => o.DocNo)
            )
                if (dtl7.PRName.Length > 0 || dtl7.PRPhone.Length > 0 || dtl7.PRAddress.Length > 0 || dtl7.PRRemark.Length > 0)
                {
                    l7.Add(new PersonalInformationDtl7()
                    {
                        DocNo = dtl7.DocNo,
                        DNo = dtl7.DNo,
                        PRName = dtl7.PRName,
                        PRPhone = dtl7.PRPhone,
                        PRAddress = dtl7.PRAddress,
                        PRRemark = dtl7.PRRemark
                    });
                }


        }

        private void ProcessDtl2(ref List<PersonalInformationDtl2> PIdtl2)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl2.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl2[i].DocNo;
                    PIdtl2[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl2[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl2[i].DocNo;
                    }
                    PIdtl2[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl3(ref List<PersonalInformationDtl3> PIdtl3)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl3.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl3[i].DocNo;
                    PIdtl3[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl3[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl3[i].DocNo;
                    }
                    PIdtl3[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl4(ref List<PersonalInformationDtl4> PIdtl4)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl4.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl4[i].DocNo;
                    PIdtl4[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl4[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl4[i].DocNo;
                    }
                    PIdtl4[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl5(ref List<PersonalInformationDtl5> PIdtl5)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl5.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl5[i].DocNo;
                    PIdtl5[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl5[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl5[i].DocNo;
                    }
                    PIdtl5[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl6(ref List<PersonalInformationDtl6> PIdtl6)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl6.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl6[i].DocNo;
                    PIdtl6[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl6[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl6[i].DocNo;
                    }
                    PIdtl6[i].DNo = mDNo;
                }
            }
        }

        private void ProcessDtl7(ref List<PersonalInformationDtl7> PIdtl7)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < PIdtl7.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                    mDocNo = PIdtl7[i].DocNo;
                    PIdtl7[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == PIdtl7[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = Sm.Right(string.Concat("0000000000", 1), 3);
                        mDocNo = PIdtl7[i].DocNo;
                    }
                    PIdtl7[i].DNo = mDNo;
                }
            }
        }

        private void ProcessEmpReq(ref List<PersonalInformation> PIH, ref List<PersonalInformationDtl8> PI8)
        {
            string EmpReqno = string.Empty;

            for (int i = 0; i < PIH.Count; i++)
            {
                if (EmpReqno.Length <= 0)
                {
                    EmpReqno = PIH[i].EmpRequestNo;
                    PI8.Add(new PersonalInformationDtl8()
                    {
                        EmpRequestNo = EmpReqno
                    });

                }
                else
                {
                    if (EmpReqno != PIH[i].EmpRequestNo)
                    {
                        EmpReqno = PIH[i].EmpRequestNo;
                        PI8.Add(new PersonalInformationDtl8()
                        {
                            EmpRequestNo = EmpReqno
                        });
                    }
                }
            }
        }

        private MySqlCommand SaveEmployeeRecruitment(List<PersonalInformationHdr> lh)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeeRecruitment(DocNo, DocDt, EmpName, DisplayName, UserCode, EmpCodeOld, ShortCode, DeptCode, PosCode, ");
            SQL.AppendLine("JoinDt, ResignDt, IdNumber, MaritalStatus, WeddingDt, Gender, Religion, Mother, BirthPlace, BirthDt, Address, ");
            SQL.AppendLine("CityCode, SubDistrict, Village, RTRW, PostalCode, SiteCode, SectionCode, WorkGroupCode, Domicile, BloodType, Phone, Mobile, Email,GrdLvlCode,NPWP, SystemType, PayrollType, ");
            SQL.AppendLine("PTKP,BankCode,BankAcName,BankAcNo,BankBranch, ");
            SQL.AppendLine("PayrunPeriod, EmploymentStatus, DivisionCode, PGCode, POH, InterviewerBy, Place, InterviewDt, GrossSalary, StartWorkDt, Placement, DeptCode2, Probation, EmployeeRequestDocNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int i = 0;

            foreach (var x in lh)
            {
                if (i != 0) SQL.AppendLine(", ");
                string index = i.ToString();
                SQL.AppendLine("( @DocNo__" + index + ", @DocDt__" + index + ", @EmpName__" + index + ", @DisplayName__" + index);
                SQL.AppendLine(", @UsCode__" + index + ", @EmpCodeOld__" + index + ",@ShortCode__" + index + ", @DeptCode__" + index);
                SQL.AppendLine(", @PosCode__" + index + ", @JoinDt__" + index + ", @ResignDt__" + index + ", @IdNumber__" + index + ", @MaritalStatus__" + index);
                SQL.AppendLine(", @WeddingDt__" + index + ", @Gender__" + index + ", @Religion__" + index + ", @Mother__" + index + ", @BirthPlace__" + index + ", @BirthDt__" + index);
                SQL.AppendLine(", @Address__" + index + ",@CityCode__" + index + ",@SubDistrict__" + index + ", @Village__" + index + ", @RTRW__" + index + ", @PostalCode__" + index + ", @SiteCode__" + index);
                SQL.AppendLine(", @SectionCode__" + index + ", @WorkGroupCode__" + index + ", @Domicile__" + index + ", @BloodType__" + index + ", @Phone__" + index + ", @Mobile__" + index + ", @Email__" + index);
                SQL.AppendLine(", @GrdLvlCode" + index + ", @NPWP__" + index + ", @SystemType__" + index + ", @PayrollType__" + index + ",@PTKP__" + index + ",@BankCode__" + index + ",@BankAcName__" + index);
                SQL.AppendLine(", @BankAcNo__" + index + ",@BankBranch__" + index + ", @PayrunPeriod__" + index + ", @EmploymentStatus__" + index + ", @DivisionCode__" + index);
                SQL.AppendLine(", @PGCode__" + index + ", @POH__" + index + ", @InterviewerBy__" + index + ", @Place__" + index + ",  @InterviewDt__" + index + ", @GrossSalary__" + index);
                SQL.AppendLine(", @StartWorkDt__" + index + ", @Placement__" + index + ", @DeptCode2__" + index + ", @Probation__" + index + ", @EmployeeRequestDocNo__" + index);
                SQL.AppendLine(", @CreateBy , CurrentDateTime()) ");


                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DocDt__" + index, x.DocDt);
                Sm.CmParam<String>(ref cm, "@EmpName__" + index, x.EmpName);
                Sm.CmParam<String>(ref cm, "@DisplayName__" + index, x.DisplayName);
                Sm.CmParam<String>(ref cm, "@@UsCode__" + index, x.UserCode);
                Sm.CmParam<String>(ref cm, "@EmpCodeOld__" + index, x.OldCode);
                Sm.CmParam<String>(ref cm, "@ShortCode__" + index, x.ShortCode);
                Sm.CmParam<String>(ref cm, "@DeptCode__" + index, x.Department);
                Sm.CmParam<String>(ref cm, "@PosCode__" + index, x.Position);
                Sm.CmParam<String>(ref cm, "@JoinDt__" + index, x.JoinDate);
                Sm.CmParam<String>(ref cm, "@ResignDt__" + index, x.ResignDt);
                Sm.CmParam<String>(ref cm, "@IdNumber__" + index, x.Identity);
                Sm.CmParam<String>(ref cm, "@MaritalStatus__" + index, x.MaritalSts);
                Sm.CmParam<String>(ref cm, "@WeddingDt__" + index, x.WedingDt);
                Sm.CmParam<String>(ref cm, "@Gender__" + index, x.Gender);
                Sm.CmParam<String>(ref cm, "@Religion__" + index, x.Religion);
                Sm.CmParam<String>(ref cm, "@Mother__" + index, x.Mother);
                Sm.CmParam<String>(ref cm, "@BirthPlace__" + index, x.BirthPlace);
                Sm.CmParam<String>(ref cm, "@BirthDt__" + index, x.BirthDt);
                Sm.CmParam<String>(ref cm, "@Address__" + index, x.Adreess);
                Sm.CmParam<String>(ref cm, "@CityCode__" + index, x.city);
                Sm.CmParam<String>(ref cm, "@SubDistrict__" + index, x.SubDistrict);
                Sm.CmParam<String>(ref cm, "@Village__" + index, x.Vilage);
                Sm.CmParam<String>(ref cm, "@RTRW__" + index, x.RTRW);
                Sm.CmParam<String>(ref cm, "@PostalCode__" + index, x.PostalCode);
                Sm.CmParam<String>(ref cm, "@SiteCode__" + index, x.Site);
                Sm.CmParam<String>(ref cm, "@SectionCode__" + index, x.SectionCode);
                Sm.CmParam<String>(ref cm, "@WorkGroupCode__" + index, x.Workinggroup);
                Sm.CmParam<String>(ref cm, "@Domicile__" + index, x.Domicily);
                Sm.CmParam<String>(ref cm, "@BloodType__" + index, x.BlodType);
                Sm.CmParam<String>(ref cm, "@Phone__" + index, x.Phone);
                Sm.CmParam<String>(ref cm, "@Mobile__" + index, x.Mobile);
                Sm.CmParam<String>(ref cm, "@Email__" + index, x.Email);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode" + index, x.GradeLevel);
                Sm.CmParam<String>(ref cm, "@NPWP__" + index, x.NPWP);
                Sm.CmParam<String>(ref cm, "@SystemType__" + index, x.SystemType);
                Sm.CmParam<String>(ref cm, "@PayrollType__" + index, x.PayrollType);
                Sm.CmParam<String>(ref cm, "@PTKP__" + index, x.NonIncomingTax);
                Sm.CmParam<String>(ref cm, "@BankCode__" + index, x.BankName);
                Sm.CmParam<String>(ref cm, "@BankAcName__" + index, x.AccountName);
                Sm.CmParam<String>(ref cm, "@BankAcNo__" + index, x.AccountNo);
                Sm.CmParam<String>(ref cm, "@BankBranch__" + index, x.BankBranch);
                Sm.CmParam<String>(ref cm, "@PayrunPeriod__" + index, x.PayrunPeriod);
                Sm.CmParam<String>(ref cm, "@EmploymentStatus__" + index, x.EmploymentSts);
                Sm.CmParam<String>(ref cm, "@DivisionCode__" + index, x.Divition);
                Sm.CmParam<String>(ref cm, "@PGCode__" + index, x.PayrollGroup);
                Sm.CmParam<String>(ref cm, "@POH__" + index, x.POH);
                Sm.CmParam<String>(ref cm, "@InterviewerBy__" + index, x.InterviewBy);
                Sm.CmParam<String>(ref cm, "@Place__" + index, x.Place);
                Sm.CmParam<String>(ref cm, "@InterviewDt__" + index, x.InterviewDt);
                Sm.CmParam<String>(ref cm, "@GrossSalary__" + index, x.GrossSalary);
                Sm.CmParam<String>(ref cm, "@StartWorkDt__" + index, x.StartWorkDt);
                Sm.CmParam<String>(ref cm, "@Placement__" + index, x.Placement);
                Sm.CmParam<String>(ref cm, "@DeptCode2__" + index, x.CompanyDept);
                Sm.CmParam<String>(ref cm, "@Probation__" + index, x.Probation);
                Sm.CmParam<String>(ref cm, "@EmployeeRequestDocNo__" + index, x.EmpRequestNo);

                i++;
            }

            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    EmpName=Values(EmpName), DisplayName=Values(DisplayName), UserCode=Values(UserCode), EmpCodeOld=Values(EmpCodeOld), ShortCode=Values(ShortCode), DeptCode=Values(DeptCode), PosCode=Values(PosCode), ");
            SQL.AppendLine("    JoinDt=Values(JoinDt), ResignDt=Values(ResignDt), IdNumber=Values(IdNumber), MaritalStatus=Values(MaritalStatus), WeddingDt=Values(WeddingDt), Gender=Values(Gender), ");
            SQL.AppendLine("    Religion=Values(Religion), Mother=Values(Mother), BirthPlace=Values(BirthPlace), BirthDt=Values(BirthDt), Address=Values(Address), ");
            SQL.AppendLine("    CityCode=Values(CityCode), SubDistrict=Values(SubDistrict), Village=Values(Village), RTRW=Values(RTRW), PostalCode=Values(PostalCode), SiteCode=Values(SiteCode), SectionCode=Values(SectionCode), WorkGroupCode=Values(WorkGroupCode), Domicile=Values(Domicile), BloodType=Values(BloodType), Phone=Values(Phone), Mobile=Values(Mobile),Email=Values(Email), ");
            SQL.AppendLine("    GrdLvlCode=Values(GrdLvlCode), NPWP=Values(NPWP), SystemType=Values(SystemType), PayrollType=Values(PayrollType), PTKP=Values(PTKP), BankCode=Values(BankCode),BankAcName=Values(BankAcName),BankAcNo=Values(BankAcNo),BankBranch=Values(BankBranch), ");
            SQL.AppendLine("    PayrunPeriod=Values(PayrunPeriod), EmploymentStatus=Values(EmploymentStatus), DivisionCode=Values(DivisionCode), PGCode=Values(PGCode), POH=Values(POH), InterviewerBy=Values(InterviewerBy), Place=Values(Place), InterviewDt=Values(InterviewDt), GrossSalary=Values(GrossSalary), StartWorkDt=Values(StartWorkDt), Placement=Values(Placement), DeptCode2=Values(DeptCode2), Probation=Values(Probation), ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt=CurrentDateTime()");

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployeeReq(List<PersonalInformationDtl8> l8)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            int i = 0;
            foreach (var x in l8)
            {
                string index = i.ToString();
                SQL.AppendLine("Update TblEmployeeRequest A ");
                SQL.AppendLine("SET A.ProcessInd = (Select Case  ");
                SQL.AppendLine("When Count(*) = 0 Then 'O'  ");
                SQL.AppendLine("When Count(*) = A.Amount Then 'F' ");
                SQL.AppendLine("Else 'P' ");
                SQL.AppendLine("End As processInd ");
                SQL.AppendLine("from TblEmployeeRecruitment Where EmployeeRequestDocNo=@EmployeeRequestDocNo__" + index + "  ) ");
                SQL.AppendLine("Where A.DocNo = @EmployeeRequestDocNo__" + index + " ;");

                Sm.CmParam<String>(ref cm, "@EmployeeRequestDocNo__" + index, x.EmpRequestNo);

                i++;
            }
            cm.CommandText = SQL.ToString();
            return cm;

        }

        private MySqlCommand SaveFamilyEmployeeRecruitment(List<PersonalInformationDtl2> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeeFamilyRecruitment(DocNo, DNo, FamilyName, Status, Gender, BirthDt, IDNo, NIN, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values");

            int i = 0;

            foreach (var x in l2)
            {
                if (i != 0) SQL.AppendLine(", ");
                string index = i.ToString();
                SQL.AppendLine("(@DocNo__" + index + ", @DNo__" + index + ", @FamilyName__" + index + ", @Status__" + index +
                    ", @Gender__" + index + ", @BirthDt__" + index + ", @IDNo__" + index + ", @NIN__" + index + ", @Remark__" + index +
                    ", @CreateBy , CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@FamilyName__" + index, x.FamilyName);
                Sm.CmParam<String>(ref cm, "@Status__" + index, x.FamilySts);
                Sm.CmParam<String>(ref cm, "@Gender__" + index, x.FamilyGender);
                Sm.CmParam<String>(ref cm, "@BirthDt__" + index, x.FamilyBirtDt);
                Sm.CmParam<String>(ref cm, "@IDNo__" + index, x.IDCard);
                Sm.CmParam<String>(ref cm, "@NIN__" + index, x.NIN);
                Sm.CmParam<String>(ref cm, "@Remark__" + index, x.FamilyRemark);


                i++;
            }

            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update FamilyName=Values(FamilyName), Status=Values(Status), Gender=Values(Gender), BirthDt=Values(BirthDt), IDNo=Values(IDNo), NIN=Values(NIN), Remark=Values(Remark), LastUpBy=@CreateBy, LastUpDt=CurrentDateTime()");

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;

        }

        private MySqlCommand SaveEmployeeWorkExpRecruitment(List<PersonalInformationDtl3> PI3)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeeWorkExpRecruitment(DocNo, DNo, Company, Position, Period, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("values");
            int i = 0;

            foreach (var x in PI3)
            {
                if (i != 0) SQL.AppendLine(", ");
                string index = i.ToString();
                SQL.AppendLine("( @DocNo__" + index + ", @DNo__" + index + ", @Company__" + index + ", @Position__" + index + ", @Period__" + index);
                SQL.AppendLine(", @Remark__" + index + ", @CreateBy, CurrentDateTime()) ");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@Company__" + index, x.Company);
                Sm.CmParam<String>(ref cm, "@Position__" + index, x.WEPosition);
                Sm.CmParam<String>(ref cm, "@Period__" + index, x.Period);
                Sm.CmParam<String>(ref cm, "@Remark__" + index, x.WERemark);
                i++;
            }

            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("Company=Values(Company),  Position=Values(Position),  Period=Values(Period),  ");
            SQL.AppendLine("Remark=Values(Remark),  LastUpBy=@CreateBy, LastUpDt=CurrentDateTime()");
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeEducationRecruitment(List<PersonalInformationDtl4> PI4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeeEducationRecruitment(DocNo, DNo, School, Level, Major, HighestInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("values");
            int i = 0;
            foreach (var x in PI4)
            {
                if (i != 0)
                    SQL.AppendLine(",");
                string index = i.ToString();

                SQL.AppendLine("( @DocNo__" + index + ", @DNo__" + index + ", @School__" + index + ", @Level__" + index + ", @Major__" + index + ", @HighestInd__" + index);
                SQL.AppendLine(", @Remark__" + index + ", @CreateBy, CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@School__" + index, x.School);
                Sm.CmParam<String>(ref cm, "@Level__" + index, x.Level);
                Sm.CmParam<String>(ref cm, "@Major__" + index, x.Major);
                Sm.CmParam<String>(ref cm, "@HighestInd__" + index, x.HighestInd != "" ? x.HighestInd : "N");
                Sm.CmParam<String>(ref cm, "@Remark__" + index, x.EdcRemark);


                i++;
            }

            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    School=Values(School),  Level=Values(Level),  Major=Values(Major),  HighestInd=Values(HighestInd),  ");
            SQL.AppendLine("    Remark=Values(Remark),  LastUpBy=@CreateBy, LastUpDt=CurrentDateTime()");

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveEmployeeCompetenceRecruitment(List<PersonalInformationDtl5> PI5)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeeCompetenceRecruitment ");
            SQL.AppendLine("(DocNo, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
            SQL.AppendLine("values");

            int i = 0;
            foreach (var x in PI5)
            {
                if (i != 0)
                    SQL.AppendLine(",");
                string index = i.ToString();
                SQL.AppendLine("( @DocNo__" + index + ", @DNo__" + index + ", @CompetenceCode__" + index + ", @Description__" + index + ", @Score__" + index);
                SQL.AppendLine(", @CreateBy, CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@Description__" + index, x.Description);
                Sm.CmParam<String>(ref cm, "@CompetenceCode__" + index, x.CompetenceName);
                Sm.CmParam<Decimal>(ref cm, "@Score__" + index, x.Score);
                i++;
            }

            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;

        }

        private MySqlCommand SaveEmployeeTrainingRecruitment(List<PersonalInformationDtl6> PI6)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into tblemployeeTrainingRecruitment ");
            SQL.AppendLine("(DocNo, DNo, SpecializationTraining, Place, Period, Yr, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            int i = 0;

            foreach (var x in PI6)
            {
                if (i != 0)
                    SQL.AppendLine(",");
                string index = i.ToString();

                SQL.AppendLine("( @DocNo__" + index + ", @DNo__" + index + ", @SpecializationTraining__" + index + ", @Place__" + index + ", @Period__" + index);
                SQL.AppendLine(", @Yr__" + index + ", @Remark__" + index);
                SQL.AppendLine(", @CreateBy, CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@SpecializationTraining__" + index, x.SpecTraining);
                Sm.CmParam<String>(ref cm, "@Place__" + index, x.TrainingPlace);
                Sm.CmParam<String>(ref cm, "@Period__" + index, x.TrainingPeriod);
                Sm.CmParam<String>(ref cm, "@Yr__" + index, x.TrainingYear);
                Sm.CmParam<String>(ref cm, "@Remark__" + index, x.TrainingRemark);
                i++;
            }
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    SpecializationTraining=values(SpecializationTraining),  Place=values(Place),  Period=values(Period),  Yr=values(Yr), Remark=values(Remark), LastUpBy=@CreateBy, LastUpDt=CurrentDateTime()");
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeePersonalReferenceRecruitment(List<PersonalInformationDtl7> PI7)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblEmployeePersonalReferenceRecruitment(DocNo, DNo, Name, Phone, Address, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            int i = 0;

            foreach (var x in PI7)
            {
                if (i != 0)
                    SQL.AppendLine(",");
                string index = i.ToString();
                SQL.AppendLine("( @DocNo__" + index + ", @DNo__" + index + ", @Name__" + index + ", @Phone__" + index + ", @Address__" + index);
                SQL.AppendLine(", @Remark__" + index + ", @CreateBy, CurrentDateTime())");

                Sm.CmParam<String>(ref cm, "@DocNo__" + index, x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo__" + index, x.DNo);
                Sm.CmParam<String>(ref cm, "@Name__" + index, x.PRName);
                Sm.CmParam<String>(ref cm, "@Phone__" + index, x.PRPhone);
                Sm.CmParam<String>(ref cm, "@Address__" + index, x.PRAddress);
                Sm.CmParam<String>(ref cm, "@Remark__" + index, x.PRRemark);

                i++;

            }

            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("Name=Values(Name), Phone=Values(Phone), Address=Values(Address),  ");
            SQL.AppendLine("Remark=Values(Remark),  LastUpBy=@CreateBy, LastUpDt=CurrentDateTime();");
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Saham Movement
        private MySqlCommand InsertSahamMovement(SahamMovement y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSahamMovement(DocNo, DNo,DocDt, Account, SID, Name, NIP, Country, Category, Sekuritas, Amt, StatusBalance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @Account, @SID, @Name, @NIP, @Country, @Category, @Sekuritas, @Amt, @StatusBalance, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@DocDt", y.DocDt);
            Sm.CmParam<String>(ref cm, "@Account", y.Account);
            Sm.CmParam<String>(ref cm, "@SID", y.SID);
            Sm.CmParam<String>(ref cm, "@Name", y.Name);
            Sm.CmParam<String>(ref cm, "@NIP", y.NIP);
            Sm.CmParam<String>(ref cm, "@Country", y.Country);
            Sm.CmParam<String>(ref cm, "@Category", y.Category);
            Sm.CmParam<String>(ref cm, "@Sekuritas", y.Sekuritas);
            Sm.CmParam<Decimal>(ref cm, "@Amt", y.Amt);
            Sm.CmParam<String>(ref cm, "@StatusBalance", y.StatusBalance);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand InsertInvestor(SahamMovement x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert into TblInvestor(account, SID, name, createby, createdt) ");
            SQL.AppendLine("values (@Account, @SID, @Name, @CreateBy, @createdt) ");
            SQL.AppendLine("    ON Duplicate Key ");
            SQL.AppendLine("    update SID=@SID, Name=@Name ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@Account", x.Account);
            Sm.CmParam<string>(ref cm, "@SID", x.Account);
            Sm.CmParam<string>(ref cm, "@Name", x.Name);
            return cm;

        }

        private bool IsInsertedSahamMovementInvalid(ref List<SahamMovement> lSahamMovement)
        {
            return
                IsDocDtEmpty(ref lSahamMovement);
        }

        private bool IsDocDtEmpty(ref List<SahamMovement> lSahamMovement)
        {
            for (int i = 0; i < lSahamMovement.Count(); i++)
            {
                if (lSahamMovement[i].DocDt.Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Info, "Document Date is empty at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }
        #endregion

        private void GetCSVData(string FileName, string ImportCode)
        {
            bool IsFirst = true;

            if (ImportCode == "ImportJournalTransaction")
            {
                # region Journal Transaction

                if (Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod"))
                    GetCSVDataJournal(ref FileName, ref IsFirst);
                else
                    GetCSVDataJournal2(ref FileName, ref IsFirst);

                #endregion
            }
            else
            {
                using (var rd = new StreamReader(FileName))
                {
                    #region COA

                    if (ImportCode == "ImportCOA")
                    {
                        var lCOA = new List<COA>();
                        int Lvl = 1;
                        string AcType = string.Empty;

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 4)
                            {
                                if (splits.Count() > 4)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Account number " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 4)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Account number " + splits[0].Trim() + " have no Account Description and/or Account Type.");
                                }
                                lCOA.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                var countLvl = splits[0].Split('.');
                                Lvl = countLvl.Count();

                                var arr = splits.ToArray();

                                if (arr[0].Trim().Length > 0)
                                {
                                    lCOA.Add(new COA()
                                    {
                                        AcNo = splits[0].Trim(),
                                        AcDesc = splits[1].TrimStart(),
                                        AcType = splits[2].TrimStart(),
                                        Level = Lvl,
                                        Alias = splits[3].TrimStart(),
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    ClearData();
                                    return;
                                }
                            }
                        }

                        if (lCOA.Count > 0) ProcessCOA(ref lCOA);
                        else lCOA.Clear();

                    }

                    #endregion

                    #region Employee Salary

                    if (ImportCode == "ImportEmployeeSalary")
                    {
                        var lEmpSalary = new List<EmpSalary>();
                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() == 4)
                            {
                                var arr = splits.ToArray();
                                if (arr[0].Trim().Length > 0)
                                {
                                    lEmpSalary.Add(new EmpSalary()
                                    {
                                        //EmpCode = Sm.Right(string.Concat("00000000", splits[0].Trim()), 8),
                                        EmpCode = mGenerateEmpCodeFormat != "1" ?
                                               Sm.Right(string.Concat("00000000", splits[0].Trim()), 10) :
                                               mIsEmpCodeUseContractDt == false ?
                                                   Sm.Right(string.Concat("00000000", splits[0].Trim()), 8) :
                                                   Sm.Right(string.Concat("00000000", splits[0].Trim()), 9),
                                        StartDt = splits[1].Trim(),
                                        Amt = Decimal.Parse(splits[2].Trim().Length > 0 ? splits[2].Trim() : "0"),
                                        Amt2 = Decimal.Parse(splits[3].Trim().Length > 0 ? splits[3].Trim() : "0"),
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    ClearData();
                                    return;
                                }
                            }
                        }

                        if (lEmpSalary.Count > 0)
                            ProcessEmpSalary(ref lEmpSalary);
                        else lEmpSalary.Clear();
                    }

                    #endregion

                    #region Employee Salary SS

                    if (ImportCode == "ImportEmployeeSalarySS")
                    {
                        var lEmployeeSalarySS = new List<EmployeeSalarySS>();
                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() == 5)
                            {
                                var arr = splits.ToArray();
                                if (arr[0].Trim().Length > 0)
                                {
                                    lEmployeeSalarySS.Add(new EmployeeSalarySS()
                                    {
                                        //EmpCode = Sm.Right(string.Concat("00000000", splits[0].Trim()), 8),
                                        EmpCode = mGenerateEmpCodeFormat != "1" ?
                                               Sm.Right(string.Concat("00000000", splits[0].Trim()), 10) :
                                               mIsEmpCodeUseContractDt == false ?
                                                   Sm.Right(string.Concat("00000000", splits[0].Trim()), 8) :
                                                   Sm.Right(string.Concat("00000000", splits[0].Trim()), 9),
                                        DNo = "001",
                                        SSPCode = splits[1].Trim(),
                                        StartDt = splits[2].Trim(),
                                        EndDt = splits[3].Trim(),
                                        PrevEndDt = string.Empty,
                                        Amt = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0")
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                                    ClearData();
                                    return;
                                }
                            }
                        }

                        if (lEmployeeSalarySS.Count > 0)
                        {
                            var Dt = new DateTime(1900, 1, 1, 0, 0, 0);
                            foreach (var i in lEmployeeSalarySS)
                            {
                                if (i.StartDt.Length > 0)
                                {
                                    Dt = new DateTime(
                                      Int32.Parse(i.StartDt.Substring(0, 4)),
                                      Int32.Parse(i.StartDt.Substring(4, 2)),
                                      Int32.Parse(i.StartDt.Substring(6, 2)),
                                      0, 0, 0
                                      );
                                    Dt = Dt.AddDays(-1);
                                    i.PrevEndDt =
                                        Dt.Year.ToString() +
                                        ("00" + Dt.Month.ToString()).Substring(("00" + Dt.Month.ToString()).Length - 2, 2) +
                                        ("00" + Dt.Day.ToString()).Substring(("00" + Dt.Day.ToString()).Length - 2, 2);
                                }
                            }
                            ProcessEmployeeSalarySS(ref lEmployeeSalarySS);
                        }
                        else
                            lEmployeeSalarySS.Clear();
                    }

                    #endregion

                    #region Employee Allowance/Deduction

                    if (ImportCode == "ImportEmployeeAD")
                    {
                        var lEmployeeAllowanceDeduction = new List<EmployeeAllowanceDeduction>();
                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() == 5)
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lEmployeeAllowanceDeduction.Add(new EmployeeAllowanceDeduction()
                                        {
                                            //EmpCode = Sm.Right(string.Concat("00000000", splits[0].Trim()), 8),
                                            EmpCode = mGenerateEmpCodeFormat != "1" ?
                                                Sm.Right(string.Concat("00000000", splits[0].Trim()), 10) :
                                                mIsEmpCodeUseContractDt == false ?
                                                    Sm.Right(string.Concat("00000000", splits[0].Trim()), 8) :
                                                    Sm.Right(string.Concat("00000000", splits[0].Trim()), 9),
                                            DNo = "001",
                                            //ADCode = Sm.Right(string.Concat("000", splits[1].Trim()), 3),
                                            ADCode = splits[1].Trim(),
                                            StartDt = splits[2].Trim(),
                                            EndDt = splits[3].Trim(),
                                            PrevEndDt = string.Empty,
                                            Amt = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0")
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lEmployeeAllowanceDeduction.Count > 0)
                        {
                            var Dt = new DateTime(1900, 1, 1, 0, 0, 0);
                            foreach (var i in lEmployeeAllowanceDeduction)
                            {
                                if (i.StartDt.Length > 0)
                                {
                                    Dt = new DateTime(
                                      Int32.Parse(i.StartDt.Substring(0, 4)),
                                      Int32.Parse(i.StartDt.Substring(4, 2)),
                                      Int32.Parse(i.StartDt.Substring(6, 2)),
                                      0, 0, 0
                                      );
                                    Dt = Dt.AddDays(-1);
                                    i.PrevEndDt =
                                        Dt.Year.ToString() +
                                        ("00" + Dt.Month.ToString()).Substring(("00" + Dt.Month.ToString()).Length - 2, 2) +
                                        ("00" + Dt.Day.ToString()).Substring(("00" + Dt.Day.ToString()).Length - 2, 2);
                                }
                            }
                            ProcessEmployeeAllowanceDeduction(ref lEmployeeAllowanceDeduction);
                        }
                        else
                            lEmployeeAllowanceDeduction.Clear();
                    }

                    #endregion

                    #region Asset

                    if (ImportCode == "ImportAsset")
                    {
                        var lAsset = new List<Asset>();
                        var AssetColCount = 34;

                        if (mIsAssetShowAdditionalInformation) AssetColCount = 42;

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != AssetColCount)
                            {
                                if (splits.Count() > AssetColCount)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[1].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < AssetColCount)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this asset code " + splits[1].Trim() + " (34) have a correct column count data.");
                                }

                                lAsset.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    var mCount = Sm.GetValue("Select Max(Length(CCCode)) CCCode From TblCostCenter Where ActInd = 'Y'; ");

                                    if (arr[2].Trim().Length > 0)
                                    {
                                        string CCCodeTemp = string.Empty, AssetCodeTemp = string.Empty;
                                        if (splits[16].Trim().Length > 0)
                                        {
                                            CCCodeTemp = (mCount.Length > 0) ? Sm.Right(string.Concat("00000000", splits[16].Trim()), Int32.Parse(mCount)) : splits[16].Trim();
                                        }
                                        if (splits[0].Trim().Length == 0)
                                            AssetCodeTemp = string.Empty;
                                        else
                                            AssetCodeTemp = splits[0].Trim();
                                        if (mIsAssetShowAdditionalInformation)
                                        {
                                            lAsset.Add(new Asset()
                                            {
                                                AssetCode = AssetCodeTemp,
                                                ItCode = splits[1].Trim(),
                                                AssetName = splits[2].Trim(),
                                                DisplayName = splits[3].Trim(),
                                                Parent = splits[4].Trim(),
                                                ShortCode = splits[5].Trim(),
                                                AssetType = splits[6].Trim().Length > 0 ? splits[6].Trim() : "N",
                                                LeasingInd = splits[7].Trim().Length > 0 ? splits[7].Trim() : "N",
                                                RentedInd = splits[8].Trim().Length > 0 ? splits[8].Trim() : "N",
                                                SoldInd = splits[9].Trim().Length > 0 ? splits[9].Trim() : "N",
                                                AssetDt = splits[10].Trim(),
                                                AssetValue = splits[11].Trim().Length > 0 ? Decimal.Parse(splits[11].Trim()) : 0m,
                                                EcoLifeYr = splits[12].Trim().Length > 0 ? Decimal.Parse(splits[12].Trim()) : 0m,
                                                EcoLife = splits[13].Trim().Length > 0 ? Decimal.Parse(splits[13].Trim()) : 0m,
                                                DepreciationCode = splits[14].Trim(),
                                                PercentageAnnualDepreciation = splits[15].Trim().Length > 0 ? Decimal.Parse(splits[15].Trim()) : 0m,
                                                CCCode = CCCodeTemp,
                                                AcNo = splits[17].Trim(),
                                                AcNo2 = splits[18].Trim(),
                                                AssetCategoryCode = splits[19].Trim(),
                                                Length = splits[20].Trim().Length > 0 ? Decimal.Parse(splits[20].Trim()) : 0m,
                                                Height = splits[21].Trim().Length > 0 ? Decimal.Parse(splits[21].Trim()) : 0m,
                                                Width = splits[22].Trim().Length > 0 ? Decimal.Parse(splits[22].Trim()) : 0m,
                                                Volume = splits[23].Trim().Length > 0 ? Decimal.Parse(splits[23].Trim()) : 0m,
                                                Diameter = splits[24].Trim().Length > 0 ? Decimal.Parse(splits[24].Trim()) : 0m,
                                                Wide = splits[25].Trim().Length > 0 ? Decimal.Parse(splits[25].Trim()) : 0m,
                                                LengthUomCode = splits[26].Trim(),
                                                HeightUomCode = splits[27].Trim(),
                                                WidthUomCode = splits[28].Trim(),
                                                VolumeUomCode = splits[29].Trim(),
                                                DiameterUomCode = splits[30].Trim(),
                                                WideUomCode = splits[31].Trim(),
                                                FileName = splits[32].Trim(),
                                                Location = splits[33].Trim(),
                                                Classification = splits[34].Trim(),
                                                SubClassification = splits[35].Trim(),
                                                Type = splits[36].Trim(),
                                                SubType = splits[37].Trim(),
                                                Location2 = splits[38].Trim(),
                                                SubLocation = splits[39].Trim(),
                                                Location3 = splits[40].Trim(),
                                                SiteCode = splits[41].Trim()
                                            });
                                        }
                                        else
                                        {
                                            lAsset.Add(new Asset()
                                            {
                                                AssetCode = AssetCodeTemp,
                                                ItCode = splits[1].Trim(),
                                                AssetName = splits[2].Trim(),
                                                DisplayName = splits[3].Trim(),
                                                Parent = splits[4].Trim(),
                                                ShortCode = splits[5].Trim(),
                                                AssetType = splits[6].Trim().Length > 0 ? splits[6].Trim() : "N",
                                                LeasingInd = splits[7].Trim().Length > 0 ? splits[7].Trim() : "N",
                                                RentedInd = splits[8].Trim().Length > 0 ? splits[8].Trim() : "N",
                                                SoldInd = splits[9].Trim().Length > 0 ? splits[9].Trim() : "N",
                                                AssetDt = splits[10].Trim(),
                                                AssetValue = splits[11].Trim().Length > 0 ? Decimal.Parse(splits[11].Trim()) : 0m,
                                                EcoLifeYr = splits[12].Trim().Length > 0 ? Decimal.Parse(splits[12].Trim()) : 0m,
                                                EcoLife = splits[13].Trim().Length > 0 ? Decimal.Parse(splits[13].Trim()) : 0m,
                                                DepreciationCode = splits[14].Trim(),
                                                PercentageAnnualDepreciation = splits[15].Trim().Length > 0 ? Decimal.Parse(splits[15].Trim()) : 0m,
                                                CCCode = CCCodeTemp,
                                                AcNo = splits[17].Trim(),
                                                AcNo2 = splits[18].Trim(),
                                                AssetCategoryCode = splits[19].Trim(),
                                                Length = splits[20].Trim().Length > 0 ? Decimal.Parse(splits[20].Trim()) : 0m,
                                                Height = splits[21].Trim().Length > 0 ? Decimal.Parse(splits[21].Trim()) : 0m,
                                                Width = splits[22].Trim().Length > 0 ? Decimal.Parse(splits[22].Trim()) : 0m,
                                                Volume = splits[23].Trim().Length > 0 ? Decimal.Parse(splits[23].Trim()) : 0m,
                                                Diameter = splits[24].Trim().Length > 0 ? Decimal.Parse(splits[24].Trim()) : 0m,
                                                Wide = splits[25].Trim().Length > 0 ? Decimal.Parse(splits[25].Trim()) : 0m,
                                                LengthUomCode = splits[26].Trim(),
                                                HeightUomCode = splits[27].Trim(),
                                                WidthUomCode = splits[28].Trim(),
                                                VolumeUomCode = splits[29].Trim(),
                                                DiameterUomCode = splits[30].Trim(),
                                                WideUomCode = splits[31].Trim(),
                                                FileName = splits[32].Trim(),
                                                Location = splits[33].Trim()
                                            });
                                        }
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }
                        if (lAsset.Count > 0)
                            ProcessAsset(ref lAsset);
                        else
                            lAsset.Clear();
                    }

                    #endregion

                    #region Vendor

                    if (ImportCode == "ImportVendor")
                    {
                        var lVendor = new List<Vendor>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 22)
                            {
                                if (splits.Count() > 22)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 22)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this vendor code " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lVendor.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    var mCount = Sm.GetValue("Select Max(Length(VdCode)) VdCode From TblVendor Where ActInd = 'Y'; ");

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lVendor.Add(new Vendor()
                                        {
                                            VdCode = (mCount.Length > 0) ? Sm.Right(string.Concat("000000000000", splits[0].Trim()), Int32.Parse(mCount)) : splits[0].Trim(),
                                            VdName = splits[1].Trim(),
                                            ShortName = splits[2].Trim(),
                                            VdCtCode = splits[3].Trim(),
                                            Address = splits[4].Trim(),
                                            CityCode = splits[5].Trim(),
                                            SDCode = splits[6].Trim(),
                                            VilCode = splits[7].Trim(),
                                            PostalCd = splits[8].Trim(),
                                            Website = splits[9].Trim(),
                                            Parent = splits[10].Trim(),
                                            EstablishedYr = splits[11].Trim(),
                                            IdentityNo = splits[12].Trim(),
                                            TIN = splits[13].Trim(),
                                            TaxInd = splits[14].Trim(),
                                            Phone = splits[15].Trim(),
                                            Fax = splits[16].Trim(),
                                            Email = splits[17].Trim(),
                                            Mobile = splits[18].Trim(),
                                            CreditLimit = splits[19].Trim().Length > 0 ? Decimal.Parse(splits[19].Trim()) : 0m,
                                            NIB = splits[20].Trim(),
                                            Remark = splits[21].Trim(),
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }
                        if (lVendor.Count > 0) ProcessVendor(ref lVendor);
                        else lVendor.Clear();
                    }

                    #endregion

                    #region Customer

                    if (ImportCode == "ImportCustomer")
                    {
                        var lCustomer = new List<Customer>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 17)
                            {
                                if (splits.Count() > 17)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 17)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this customer code " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lCustomer.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    var mCount = Sm.GetValue("Select Max(Length(CtCode)) CtCode From TblCustomer Where ActInd = 'Y'; ");

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lCustomer.Add(new Customer()
                                        {
                                            CtCode = (mCount.Length > 0) ? Sm.Right(string.Concat("000000000000", splits[0].Trim()), Int32.Parse(mCount)) : splits[0].Trim(),
                                            CtName = splits[1].Trim(),
                                            AgentInd = splits[2].Trim().Length > 0 ? splits[2].Trim() : "N",
                                            CtShortCode = splits[3].Trim(),
                                            CtCtCode = splits[4].Trim(),
                                            CtGrpCode = splits[5].Trim(),
                                            CntCode = splits[6].Trim(),
                                            CityCode = splits[7].Trim(),
                                            NPWP = splits[8].Trim(),
                                            ShippingMark = splits[9].Trim(),
                                            Address = splits[10].Trim(),
                                            PostalCd = splits[11].Trim(),
                                            Phone = splits[12].Trim(),
                                            Fax = splits[13].Trim(),
                                            Mobile = splits[14].Trim(),
                                            NIB = splits[15].Trim(),
                                            Remark = splits[16].Trim()
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lCustomer.Count > 0) ProcessCustomer(ref lCustomer);
                        else lCustomer.Clear();
                    }

                    #endregion

                    #region Employee

                    if (ImportCode == "ImportEmployee")
                    {
                        var lEmployee = new List<Employee>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 51)
                            {
                                if (splits.Count() > 51)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 51)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data (51 columns).");
                                }

                                lEmployee.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lEmployee.Add(new Employee()
                                        {
                                            //EmpCode = Sm.Right(string.Concat("00000000", splits[0].Trim()), 8),
                                            EmpName = splits[0].Trim(),
                                            DisplayName = splits[1].Trim(),
                                            EmploymentStatus = splits[2].Trim(),
                                            UserCode = splits[3].Trim(),
                                            EmpCodeOld = splits[4].Trim(),
                                            ShortCode = splits[5].Trim(),
                                            DivisionCode = splits[6].Trim(),
                                            DeptCode = splits[7].Trim(),
                                            PosCode = splits[8].Trim(),
                                            PositionStatusCode = splits[9].Trim(),
                                            SectionCode = splits[10].Trim(),
                                            WorkGroupCode = splits[11].Trim(),
                                            EntCode = splits[12].Trim(),
                                            JoinDt = splits[13].Trim(),
                                            ResignDt = splits[14].Trim(),
                                            LeaveStartDt = splits[15].Trim(),
                                            TGDt = splits[16].Trim(),
                                            IDNumber = splits[17].Trim(),
                                            Gender = splits[18].Trim(),
                                            Religion = splits[19].Trim(),
                                            MaritalStatus = splits[20].Trim(),
                                            WeddingDt = splits[21].Trim(),
                                            BirthPlace = splits[22].Trim(),
                                            BirthDt = splits[23].Trim(),
                                            Address = splits[24].Trim(),
                                            CityCode = splits[25].Trim(),
                                            SubDistrict = splits[26].Trim(),
                                            Village = splits[27].Trim(),
                                            RTRW = splits[28].Trim(),
                                            PostalCode = splits[29].Trim(),
                                            Domicile = splits[30].Trim(),
                                            SiteCode = splits[31].Trim(),
                                            POH = splits[32].Trim(),
                                            BloodType = splits[33].Trim(),
                                            Phone = splits[34].Trim(),
                                            Mobile = splits[35].Trim(),
                                            Email = splits[36].Trim(),
                                            GrdLvlCode = splits[37].Trim(),
                                            PGCode = splits[38].Trim(),
                                            NPWP = splits[39].Trim(),
                                            SystemType = splits[40].Trim(),
                                            PayrollType = splits[41].Trim(),
                                            PTKP = splits[42].Trim(),
                                            BankCode = splits[43].Trim(),
                                            BankBranch = splits[44].Trim(),
                                            BankAcNo = splits[45].Trim(),
                                            BankName = splits[46].Trim(),
                                            BankAcName = splits[47].Trim(),
                                            PayrunPeriod = splits[48].Trim(),
                                            Mother = splits[49].Trim(),
                                            ContractDt = splits[50].Trim()
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lEmployee.Count > 0) ProcessEmployee(ref lEmployee);
                        else lEmployee.Clear();
                    }

                    #endregion

                    #region Item

                    if (ImportCode == "ImportItem")
                    {
                        var lItem = new List<Item>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            decimal ColoumnCount = 0m;
                            if (mItCodeFormat == "1")
                                ColoumnCount = 68;
                            else
                                ColoumnCount = 67;

                            if (splits.Count() != ColoumnCount)
                            {
                                if (splits.Count() > ColoumnCount)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[1].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < ColoumnCount)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this item code " + splits[1].Trim() + " have a correct column count data.");
                                }

                                lItem.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    var mCount = Sm.GetValue("Select Max(Length(ItCode)) ItCode From TblItem Where ActInd = 'Y'; ");
                                    var mCount2 = Sm.GetValue("Select Max(Length(ItCtCode)) ItCtCode From TblItemCategory Where ActInd = 'Y'; ");
                                    string ItCodeTemp = string.Empty, ItCtCodeTemp = string.Empty;

                                    if (mItCodeFormat == "1")
                                    {
                                        if (arr[0].Trim().Length > 0)
                                        {
                                            if (mIsItCodeUseItSeqNo)
                                            {
                                                ItCodeTemp = splits[0].Trim();
                                                ItCtCodeTemp = splits[9].Trim();
                                            }
                                            else
                                            {
                                                ItCodeTemp = (mCount.Length > 0) ?
                                                    Sm.Right(string.Concat("00000000", splits[0].Trim()), Int32.Parse(mCount)) :
                                                    splits[0].Trim();
                                                ItCtCodeTemp = Sm.Right(string.Concat("000000000000", splits[9].Trim()), Int32.Parse(mCount2));
                                            }

                                            lItem.Add(new Item()
                                            {
                                                ItCode = ItCodeTemp,
                                                ItName = splits[1].Trim(),
                                                HSCode = splits[2].Trim(),
                                                ItCodeInternal = splits[3].Trim(),
                                                ForeignName = splits[4].Trim(),
                                                SalesItemInd = splits[5].Trim().Length > 0 ? splits[5].Trim() : "N",
                                                ServiceItemInd = splits[6].Trim().Length > 0 ? splits[6].Trim() : "N",
                                                FixedItemInd = splits[7].Trim().Length > 0 ? splits[7].Trim() : "N",
                                                PlanningItemInd = splits[8].Trim().Length > 0 ? splits[8].Trim() : "N",
                                                ItCtCode = ItCtCodeTemp,
                                                ItScCode = splits[10].Trim(),
                                                ItBrCode = splits[11].Trim(),
                                                ItGrpCode = splits[12].Trim(),
                                                ItemRequestDocNo = splits[13].Trim(),
                                                Specification = splits[14].Trim(),
                                                Remark = splits[15].Trim(),
                                                TaxLiableInd = splits[16].Trim().Length > 0 ? splits[16].Trim() : "N",
                                                VdCode = splits[17].Trim(),
                                                PurchaseUOMCode = splits[18].Trim(),
                                                PGCode = splits[19].Trim(),
                                                MinOrder = splits[20].Trim().Length > 0 ? Decimal.Parse(splits[20].Trim()) : 0m,
                                                TaxCode1 = splits[21].Trim(),
                                                TaxCode2 = splits[22].Trim(),
                                                TaxCode3 = splits[23].Trim(),
                                                Length = splits[24].Trim().Length > 0 ? Decimal.Parse(splits[24].Trim()) : 0m,
                                                Height = splits[25].Trim().Length > 0 ? Decimal.Parse(splits[25].Trim()) : 0m,
                                                Width = splits[26].Trim().Length > 0 ? Decimal.Parse(splits[26].Trim()) : 0m,
                                                Volume = splits[27].Trim().Length > 0 ? Decimal.Parse(splits[27].Trim()) : 0m,
                                                Diameter = splits[28].Trim().Length > 0 ? Decimal.Parse(splits[28].Trim()) : 0m,
                                                LengthUomCode = splits[29].Trim(),
                                                HeightUomCode = splits[30].Trim(),
                                                WidthUomCode = splits[31].Trim(),
                                                VolumeUomCode = splits[32].Trim(),
                                                DiameterUomCode = splits[33].Trim(),
                                                SalesUOMCode = splits[34].Trim(),
                                                SalesUOMCode2 = splits[35].Trim(),
                                                SalesUomCodeConvert12 = splits[36].Trim().Length > 0 ? Decimal.Parse(splits[36].Trim()) : 0m,
                                                SalesUomCodeConvert21 = splits[37].Trim().Length > 0 ? Decimal.Parse(splits[37].Trim()) : 0m,
                                                InventoryUOMCode = splits[38].Trim(),
                                                InventoryUOMCode2 = splits[39].Trim(),
                                                InventoryUOMCode3 = splits[40].Trim(),
                                                InventoryUomCodeConvert12 = splits[41].Trim().Length > 0 ? Decimal.Parse(splits[41].Trim()) : 0m,
                                                InventoryUomCodeConvert13 = splits[42].Trim().Length > 0 ? Decimal.Parse(splits[42].Trim()) : 0m,
                                                InventoryUomCodeConvert21 = splits[43].Trim().Length > 0 ? Decimal.Parse(splits[43].Trim()) : 0m,
                                                InventoryUomCodeConvert23 = splits[44].Trim().Length > 0 ? Decimal.Parse(splits[44].Trim()) : 0m,
                                                InventoryUomCodeConvert31 = splits[45].Trim().Length > 0 ? Decimal.Parse(splits[45].Trim()) : 0m,
                                                InventoryUomCodeConvert32 = splits[46].Trim().Length > 0 ? Decimal.Parse(splits[46].Trim()) : 0m,
                                                MinStock = splits[47].Trim().Length > 0 ? Decimal.Parse(splits[47].Trim()) : 0m,
                                                MaxStock = splits[48].Trim().Length > 0 ? Decimal.Parse(splits[48].Trim()) : 0m,
                                                ReOrderStock = splits[49].Trim().Length > 0 ? Decimal.Parse(splits[49].Trim()) : 0m,
                                                ControlByDeptCode = splits[50].Trim(),
                                                PlanningUomCode = splits[51].Trim(),
                                                PlanningUomCode2 = splits[52].Trim(),
                                                PlanningUomCodeConvert12 = splits[53].Trim().Length > 0 ? Decimal.Parse(splits[53].Trim()) : 0m,
                                                PlanningUomCodeConvert21 = splits[54].Trim().Length > 0 ? Decimal.Parse(splits[54].Trim()) : 0m,
                                                Information1 = splits[55].Trim(),
                                                Information2 = splits[56].Trim(),
                                                Information3 = splits[57].Trim(),
                                                Information4 = splits[58].Trim(),
                                                Information5 = splits[59].Trim(),
                                                PackagingUomCode = splits[60].Trim(),
                                                ConversionRate1 = splits[61].Trim().Length > 0 ? Decimal.Parse(splits[61].Trim()) : 0m,
                                                ConversionRate2 = splits[62].Trim().Length > 0 ? Decimal.Parse(splits[62].Trim()) : 0m,
                                                GrossWeight = splits[63].Trim().Length > 0 ? Decimal.Parse(splits[63].Trim()) : 0m,
                                                NetWeight = splits[64].Trim().Length > 0 ? Decimal.Parse(splits[64].Trim()) : 0m,
                                                PackagingLength = splits[65].Trim().Length > 0 ? Decimal.Parse(splits[65].Trim()) : 0m,
                                                PackagingWidth = splits[66].Trim().Length > 0 ? Decimal.Parse(splits[66].Trim()) : 0m,
                                                PackagingHeight = splits[67].Trim().Length > 0 ? Decimal.Parse(splits[67].Trim()) : 0m,
                                            });
                                        }
                                    }
                                    else if (mItCodeFormat == "2")
                                    {
                                        if (arr[0].Trim().Length > 0)
                                        {
                                            lItem.Add(new Item()
                                            {
                                                //ItCode = ItCodeTemp,
                                                ItName = splits[0].Trim(),
                                                HSCode = splits[1].Trim(),
                                                ItCodeInternal = splits[2].Trim(),
                                                ForeignName = splits[3].Trim(),
                                                SalesItemInd = splits[4].Trim().Length > 0 ? splits[4].Trim() : "N",
                                                ServiceItemInd = splits[5].Trim().Length > 0 ? splits[5].Trim() : "N",
                                                FixedItemInd = splits[6].Trim().Length > 0 ? splits[6].Trim() : "N",
                                                PlanningItemInd = splits[7].Trim().Length > 0 ? splits[7].Trim() : "N",
                                                ItCtCode = splits[8].Trim(),
                                                ItScCode = splits[9].Trim(),
                                                ItBrCode = splits[10].Trim(),
                                                ItGrpCode = splits[11].Trim(),
                                                ItemRequestDocNo = splits[12].Trim(),
                                                Specification = splits[13].Trim(),
                                                Remark = splits[14].Trim(),
                                                TaxLiableInd = splits[15].Trim().Length > 0 ? splits[15].Trim() : "N",
                                                VdCode = splits[16].Trim(),
                                                PurchaseUOMCode = splits[17].Trim(),
                                                PGCode = splits[18].Trim(),
                                                MinOrder = splits[19].Trim().Length > 0 ? Decimal.Parse(splits[19].Trim()) : 0m,
                                                TaxCode1 = splits[20].Trim(),
                                                TaxCode2 = splits[21].Trim(),
                                                TaxCode3 = splits[22].Trim(),
                                                Length = splits[23].Trim().Length > 0 ? Decimal.Parse(splits[23].Trim()) : 0m,
                                                Height = splits[24].Trim().Length > 0 ? Decimal.Parse(splits[24].Trim()) : 0m,
                                                Width = splits[25].Trim().Length > 0 ? Decimal.Parse(splits[25].Trim()) : 0m,
                                                Volume = splits[26].Trim().Length > 0 ? Decimal.Parse(splits[26].Trim()) : 0m,
                                                Diameter = splits[27].Trim().Length > 0 ? Decimal.Parse(splits[27].Trim()) : 0m,
                                                LengthUomCode = splits[28].Trim(),
                                                HeightUomCode = splits[29].Trim(),
                                                WidthUomCode = splits[30].Trim(),
                                                VolumeUomCode = splits[31].Trim(),
                                                DiameterUomCode = splits[32].Trim(),
                                                SalesUOMCode = splits[33].Trim(),
                                                SalesUOMCode2 = splits[34].Trim(),
                                                SalesUomCodeConvert12 = splits[35].Trim().Length > 0 ? Decimal.Parse(splits[35].Trim()) : 0m,
                                                SalesUomCodeConvert21 = splits[36].Trim().Length > 0 ? Decimal.Parse(splits[36].Trim()) : 0m,
                                                InventoryUOMCode = splits[37].Trim(),
                                                InventoryUOMCode2 = splits[38].Trim(),
                                                InventoryUOMCode3 = splits[39].Trim(),
                                                InventoryUomCodeConvert12 = splits[40].Trim().Length > 0 ? Decimal.Parse(splits[40].Trim()) : 0m,
                                                InventoryUomCodeConvert13 = splits[41].Trim().Length > 0 ? Decimal.Parse(splits[41].Trim()) : 0m,
                                                InventoryUomCodeConvert21 = splits[42].Trim().Length > 0 ? Decimal.Parse(splits[42].Trim()) : 0m,
                                                InventoryUomCodeConvert23 = splits[43].Trim().Length > 0 ? Decimal.Parse(splits[43].Trim()) : 0m,
                                                InventoryUomCodeConvert31 = splits[44].Trim().Length > 0 ? Decimal.Parse(splits[44].Trim()) : 0m,
                                                InventoryUomCodeConvert32 = splits[45].Trim().Length > 0 ? Decimal.Parse(splits[45].Trim()) : 0m,
                                                MinStock = splits[46].Trim().Length > 0 ? Decimal.Parse(splits[46].Trim()) : 0m,
                                                MaxStock = splits[47].Trim().Length > 0 ? Decimal.Parse(splits[47].Trim()) : 0m,
                                                ReOrderStock = splits[48].Trim().Length > 0 ? Decimal.Parse(splits[48].Trim()) : 0m,
                                                ControlByDeptCode = splits[49].Trim(),
                                                PlanningUomCode = splits[50].Trim(),
                                                PlanningUomCode2 = splits[51].Trim(),
                                                PlanningUomCodeConvert12 = splits[52].Trim().Length > 0 ? Decimal.Parse(splits[52].Trim()) : 0m,
                                                PlanningUomCodeConvert21 = splits[53].Trim().Length > 0 ? Decimal.Parse(splits[53].Trim()) : 0m,
                                                Information1 = splits[54].Trim(),
                                                Information2 = splits[55].Trim(),
                                                Information3 = splits[56].Trim(),
                                                Information4 = splits[57].Trim(),
                                                Information5 = splits[58].Trim(),
                                                PackagingUomCode = splits[59].Trim(),
                                                ConversionRate1 = splits[60].Trim().Length > 0 ? Decimal.Parse(splits[60].Trim()) : 0m,
                                                ConversionRate2 = splits[61].Trim().Length > 0 ? Decimal.Parse(splits[61].Trim()) : 0m,
                                                GrossWeight = splits[62].Trim().Length > 0 ? Decimal.Parse(splits[62].Trim()) : 0m,
                                                NetWeight = splits[63].Trim().Length > 0 ? Decimal.Parse(splits[63].Trim()) : 0m,
                                                PackagingLength = splits[64].Trim().Length > 0 ? Decimal.Parse(splits[64].Trim()) : 0m,
                                                PackagingWidth = splits[65].Trim().Length > 0 ? Decimal.Parse(splits[65].Trim()) : 0m,
                                                PackagingHeight = splits[66].Trim().Length > 0 ? Decimal.Parse(splits[66].Trim()) : 0m,
                                            });
                                        }
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lItem.Count > 0)
                            ProcessItem(ref lItem);
                        else
                            lItem.Clear();
                    }

                    #endregion

                    #region Item (Custom for KSM)

                    if (ImportCode == "KSM_ImportItem")
                    {
                        var lItemKSM = new List<ItemKSM>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 12)
                            {
                                if (splits.Count() > 12)
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                else if (splits.Count() < 12)
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this item code " + splits[0].Trim() + " have a correct column count data.");

                                lItemKSM.Clear();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lItemKSM.Add(new ItemKSM()
                                        {
                                            ItCodeOld = splits[0].Trim(),
                                            ItName = splits[1].Trim(),
                                            ItCodeInternal = splits[2].Trim(),
                                            ForeignName = splits[3].Trim(),
                                            ItCtCode = splits[4].Trim(),
                                            ItScCode = splits[5].Trim(),
                                            ItGrpCode = splits[6].Trim(),
                                            InventoryUomCode = splits[7].Trim(),
                                            InventoryUomCode2 = splits[8].Trim(),
                                            InventoryUomCode3 = splits[9].Trim(),
                                            Specification = splits[10].Trim(),
                                            Remark = splits[11].Trim(),
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                                        return;
                                    }
                                }
                            }
                        }
                        if (lItemKSM.Count > 0) ProcessItemKSM(ref lItemKSM);
                        lItemKSM.Clear();
                    }

                    #endregion

                    #region Employee SS

                    if (ImportCode == "ImportEmployeeSS")
                    {
                        var lEmployeeSS = new List<EmployeeSS>();
                        var lEmployeeSSHdr = new List<EmployeeSSHdr>();
                        var lEmployeeSSDtl = new List<EmployeeSSDtl>();
                        var lEmployeeSSDtl2 = new List<EmployeeSSDtl2>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 24)
                            {
                                if (splits.Count() > 24)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 24)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this employee code " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lEmployeeSS.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lEmployeeSS.Add(new EmployeeSS()
                                        {
                                            Mth = string.Empty,
                                            Yr = string.Empty,
                                            DocNo = string.Empty,
                                            Index = 0,
                                            DocDt = splits[0].Trim(),
                                            EmpCode = mGenerateEmpCodeFormat != "1" ?
                                                Sm.Right(string.Concat("00000000", splits[1].Trim()), 10) :
                                                mIsEmpCodeUseContractDt == false ?
                                                    Sm.Right(string.Concat("00000000", splits[1].Trim()), 8) :
                                                    Sm.Right(string.Concat("00000000", splits[1].Trim()), 9),
                                            EntCode = Sm.Right(string.Concat("000", splits[2].Trim()), 3),
                                            Remark = splits[3].Trim(),
                                            DNo = "001",
                                            SSCode = splits[4].Trim(),
                                            CardNo = splits[5].Trim(),
                                            HealthFacility = splits[6].Trim(),
                                            StartDt = splits[7].Trim(),
                                            EndDt = splits[8].Trim(),
                                            Problem = splits[9].Trim(),
                                            RemarkD1 = splits[10].Trim(),
                                            DNo2 = "001",
                                            FamilyName = splits[11].Trim(),
                                            Status = splits[12].Trim(),
                                            Gender = splits[13].Trim(),
                                            IDNo = splits[14].Trim(),
                                            NIN = splits[15].Trim(),
                                            SSCode2 = splits[16].Trim(),
                                            CardNo2 = splits[17].Trim(),
                                            HealthFacility2 = splits[18].Trim(),
                                            StartDt2 = splits[19].Trim(),
                                            EndDt2 = splits[20].Trim(),
                                            BirthDt = splits[21].Trim(),
                                            Problem2 = splits[22].Trim(),
                                            RemarkD2 = splits[23].Trim()
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lEmployeeSS.Count > 0)
                        {
                            lEmployeeSS.OrderBy(o => o.DocDt).ThenBy(o => o.EmpCode);
                            SetIndex(ref lEmployeeSS);
                            SetDocNo(ref lEmployeeSS);
                            DataBreakDown(ref lEmployeeSS, ref lEmployeeSSHdr, ref lEmployeeSSDtl, ref lEmployeeSSDtl2);
                            ProcessDtl1(ref lEmployeeSSDtl);
                            ProcessDtl2(ref lEmployeeSSDtl2);
                            ProcessEmployeeSS(ref lEmployeeSS, ref lEmployeeSSHdr, ref lEmployeeSSDtl, ref lEmployeeSSDtl2);
                        }
                        else
                            lEmployeeSS.Clear();

                    }

                    #endregion

                    #region CustomerDeposit

                    if (ImportCode == "ImportCustomerDeposit")
                    {
                        var lCtDeposit = new List<CustomerDeposit>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 7)
                            {
                                if (splits.Count() > 7)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 7)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this customer code " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lCtDeposit.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                var arr = splits.ToArray();

                                if (arr[0].Trim().Length > 0)
                                {

                                    lCtDeposit.Add(new CustomerDeposit()
                                    {

                                        Mth = string.Empty,
                                        Yr = string.Empty,
                                        DocNo = string.Empty,
                                        DocDt = splits[0].Trim(),
                                        CtCode = Sm.Right(string.Concat("00000", splits[1].Trim()), 5),
                                        CurCode1 = splits[2].Trim(),
                                        Amt1 = splits[3].Trim().Length > 0 ? Decimal.Parse(splits[3].Trim()) : 0m,
                                        RateAmt1 = splits[4].Trim().Length > 0 ? Decimal.Parse(splits[4].Trim()) : 0m,
                                        CurCode2 = splits[5].Trim(),
                                        Amt2 = 0m,
                                        Remark = splits[6].Trim(),
                                        ExistingAmt = 0m,
                                        JournalDocNo = string.Empty


                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    ClearData();
                                    return;
                                }
                            }
                        }
                        if (lCtDeposit.Count > 0)
                        {
                            if (IsInsertedCustomerDepositInvalid(ref lCtDeposit)) { ClearData(); return; }
                            ExistingAmt(ref lCtDeposit);
                            ComputeAmt(ref lCtDeposit);
                            if (IsAmountNotValid(ref lCtDeposit)) { ClearData(); return; }
                            SetDocNo(ref lCtDeposit);
                            SetJournalDocNo(ref lCtDeposit);
                            ProcessCustomerDeposit(ref lCtDeposit);
                        }
                        else lCtDeposit.Clear();

                        if (lCtDeposit.Count > 0) ProcessCustomerDeposit(ref lCtDeposit);
                        else lCtDeposit.Clear();
                    }

                    #endregion

                    #region Stock Opname

                    if (ImportCode == "ImportStockOpname")
                    {
                        var lStockOpname = new List<StockOpname>();
                        var lStockOpnameHdr = new List<StockOpnameHdr>();
                        var lStockOpnameDtl = new List<StockOpnameDtl>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 15)
                            {
                                if (splits.Count() > 15)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 15)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this warehouse code " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lStockOpname.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                var arr = splits.ToArray();

                                var mCount = Sm.GetValue("Select Max(Length(ItCode)) ItCode From TblItem Where ActInd = 'Y'; ");

                                if (arr[0].Trim().Length > 0)
                                {
                                    lStockOpname.Add(new StockOpname()
                                    {
                                        Mth = string.Empty,
                                        Yr = string.Empty,
                                        DocNo = string.Empty,
                                        Status = string.Empty,
                                        Index = 0,
                                        DocDt = splits[0].Trim(),
                                        WhsCode = splits[1].Trim(),
                                        Remark = splits[2].Trim(),
                                        DNo = "001",
                                        ItCode = (mCount.Length > 0) ? Sm.Right(string.Concat("00000000", splits[3].Trim()), Int32.Parse(mCount)) : splits[3].Trim(),
                                        BatchNo = splits[4].Trim(),
                                        Source = splits[5].Trim(),
                                        Lot = splits[6].Trim(),
                                        Bin = splits[7].Trim(),
                                        QtyActual = splits[8].Trim().Length > 0 ? Decimal.Parse(splits[8].Trim()) : 0m,
                                        QtyActual2 = splits[9].Trim().Length > 0 ? Decimal.Parse(splits[9].Trim()) : 0m,
                                        QtyActual3 = splits[10].Trim().Length > 0 ? Decimal.Parse(splits[10].Trim()) : 0m,
                                        Qty = splits[11].Trim().Length > 0 ? Decimal.Parse(splits[11].Trim()) : 0m,
                                        Qty2 = splits[12].Trim().Length > 0 ? Decimal.Parse(splits[12].Trim()) : 0m,
                                        Qty3 = splits[13].Trim().Length > 0 ? Decimal.Parse(splits[13].Trim()) : 0m,
                                        Remark2 = splits[14].Trim(),
                                        JournalDocNo = string.Empty
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    ClearData();
                                    return;
                                }
                            }
                        }

                        if (lStockOpname.Count > 0)
                        {
                            lStockOpname.OrderBy(o => o.DocDt);
                            SetIndex(ref lStockOpname);
                            SetDocNo(ref lStockOpname);
                            DataBreakDown(ref lStockOpname, ref lStockOpnameHdr, ref lStockOpnameDtl);
                            ProcessDtl1(ref lStockOpnameDtl);
                            ProcessStockOpname(ref lStockOpname, ref lStockOpnameHdr, ref lStockOpnameDtl);
                        }
                        else lStockOpname.Clear();


                    }

                    #endregion

                    #region Vendor Deposit

                    if (ImportCode == "ImportVendorDeposit")
                    {
                        var lVendorDeposit = new List<VendorDeposit>();
                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 9)
                            {
                                if (splits.Count() > 9)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 9)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this vendor code " + splits[1].Trim() + " have a correct column count data.");
                                }

                                lVendorDeposit.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                var arr = splits.ToArray();
                                var mCount = Sm.GetValue("Select Max(Length(VdCode)) VdCode From TblVendor Where ActInd = 'Y'; ");

                                if (arr[0].Trim().Length > 0)
                                {
                                    lVendorDeposit.Add(new VendorDeposit()
                                    {
                                        DocNo = string.Empty,
                                        DocDt = splits[0].Trim(),
                                        VdCode = (mCount.Length > 0) ? Sm.Right(string.Concat("000000000000", splits[1].Trim()), Int32.Parse(mCount)) : splits[1].Trim(),
                                        EntCode = splits[2].Trim(),
                                        CurCode1 = splits[3].Trim(),
                                        Amt1 = splits[4].Trim().Length > 0 ? Decimal.Parse(splits[4].Trim()) : 0m,
                                        RateAmt1 = splits[5].Trim().Length > 0 ? Decimal.Parse(splits[5].Trim()) : 0m,
                                        CurCode2 = splits[6].Trim(),
                                        Amt2 = splits[7].Trim().Length > 0 ? Decimal.Parse(splits[7].Trim()) : 0m,
                                        Remark = splits[8].Trim(),
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    ClearData();
                                    return;
                                }
                            }
                        }

                        if (lVendorDeposit.Count > 0)
                        {
                            if (IsInsertedVendorDepositInvalid(ref lVendorDeposit)) { ClearData(); return; }
                            SetExistingAmt(ref lVendorDeposit, true);
                            SetAmt2(ref lVendorDeposit);
                            if (IsAmountNotValid(ref lVendorDeposit)) { ClearData(); return; }
                            SetDocNo(ref lVendorDeposit);
                            ProcessVendorDeposit(ref lVendorDeposit);
                        }
                        else lVendorDeposit.Clear();
                    }
                    #endregion

                    #region Stock Initial

                    if (ImportCode == "ImportStockInitial")
                    {
                        var lStockInitial = new List<StockInitial>();
                        var lStockInitialHdr = new List<StockInitialHdr>();
                        var lStockInitialDtl = new List<StockInitialDtl>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 17)
                            {
                                if (splits.Count() > 17)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 17)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Warehouse code " + splits[1].Trim() + " have a correct column count data.");
                                }

                                lStockInitial.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lStockInitial.Add(new StockInitial()
                                        {
                                            Mth = string.Empty,
                                            Yr = string.Empty,
                                            DocNo = string.Empty,
                                            Index = 0,
                                            DocDt = splits[0].Trim(),
                                            WhsCode = splits[1].Trim(),
                                            CurCode = splits[2].Trim(),
                                            ExcRate = splits[3].Trim(),
                                            Remark = splits[4].Trim(),
                                            DNo = "00001",
                                            ItCode = splits[5].Trim(),
                                            BatchNo = splits[6].Trim(),
                                            Lot = splits[7].Trim(),
                                            Bin = splits[8].Trim(),
                                            Qty1 = splits[9].Trim().Length > 0 ? Decimal.Parse(splits[9].Trim()) : 0m,
                                            Uom1 = splits[10].Trim(),
                                            Qty2 = splits[11].Trim().Length > 0 ? Decimal.Parse(splits[11].Trim()) : 0m,
                                            Uom2 = splits[12].Trim(),
                                            Qty3 = splits[13].Trim().Length > 0 ? Decimal.Parse(splits[13].Trim()) : 0m,
                                            Uom3 = splits[14].Trim(),
                                            Price = splits[15].Trim().Length > 0 ? Decimal.Parse(splits[15].Trim()) : 0m,
                                            Remark1 = splits[16].Trim(),
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lStockInitial.Count > 0)
                        {
                            lStockInitial.OrderBy(o => o.DocDt).ThenBy(o => o.WhsCode);
                            SetIndex(ref lStockInitial);
                            SetDocNo(ref lStockInitial);
                            DataBreakDown(ref lStockInitial, ref lStockInitialHdr, ref lStockInitialDtl);
                            ProcessDtl(ref lStockInitialDtl);
                            ProcessStockInitial(ref lStockInitial, ref lStockInitialHdr, ref lStockInitialDtl);
                        }
                        else lStockInitial.Clear();
                    }

                    #endregion

                    #region Cost Category

                    if (ImportCode == "ImportCostCategory")
                    {
                        var lCCt = new List<CostCategory>();
                        var mCount = Sm.GetValue("Select Max(Length(OptCode)) OptCode From TblOption Where OptCat = 'CostCenterGroup'; ");
                        var mCount2 = Sm.GetValue("Select Max(Length(CCCode)) CCCode From TblCostCenter Where ActInd = 'Y'; ");

                        if (mCount.Length <= 0) mCount = "0";
                        if (mCount2.Length <= 0) mCount2 = "5";

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 4)
                            {
                                if (splits.Count() > 4)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 4)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this cost category " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lCCt.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lCCt.Add(new CostCategory()
                                        {
                                            CCCode = Sm.Right(string.Concat("000000000000", splits[0].Trim()), Int32.Parse(mCount2)),
                                            CCtName = splits[1].Trim(),
                                            AcNo = splits[2].Trim(),
                                            CCGrpCode =
                                                splits[3].Trim().Length == 0 ?
                                                string.Empty :
                                                Sm.Right(string.Concat("000000000000", splits[3].Trim()), Int32.Parse(mCount))
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lCCt.Count > 0)
                            ProcessCostCategory(ref lCCt);
                        else
                            lCCt.Clear();
                    }

                    #endregion

                    #region Employee Contract Document

                    if (ImportCode == "ImportEmployeeContractDocument")
                    {
                        var lEmpContractDoc = new List<EmpContractDoc>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 10)
                            {
                                if (splits.Count() > 10)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 10)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this employee " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lEmpContractDoc.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lEmpContractDoc.Add(new EmpContractDoc()
                                        {
                                            Mth = string.Empty,
                                            Yr = string.Empty,
                                            DocNo = string.Empty,
                                            DocDt = splits[0].Trim(),
                                            EmpCode = splits[1].Trim(),
                                            LocalDocNo = splits[2].Trim(),
                                            ContractExtention = decimal.Parse(splits[3].Trim()),
                                            StartDate = splits[4].Trim(),
                                            EndDate = splits[5].Trim(),
                                            Remark = splits[6].Trim(),
                                            Renewal = splits[7].Trim(),
                                            ManagementRepresentatif = splits[8].Trim(),
                                            EmpCode2 = splits[9].Trim()
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lEmpContractDoc.Count > 0)
                        {
                            lEmpContractDoc.OrderBy(o => o.DocDt);
                            SetDocNo(ref lEmpContractDoc);
                            ProcessEmpContractDoc(ref lEmpContractDoc);
                        }
                        else
                            lEmpContractDoc.Clear();
                    }
                    #endregion

                    #region MR to PI

                    if (ImportCode == "ImportMRToPI")
                    {
                        var lMRToPI = new List<MRToPI>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 3)
                            {
                                if (splits.Count() > 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lMRToPI.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lMRToPI.Add(new MRToPI()
                                        {
                                            DocDt = splits[0].Trim(),
                                            VdCode = splits[1].Trim(),
                                            Amt = Decimal.Parse(splits[2].Trim()),
                                            MRDocNo = string.Empty,
                                            PORDocNo = string.Empty,
                                            QtDocNo = string.Empty,
                                            PODocNo = string.Empty,
                                            RecvVdDocNo = string.Empty,
                                            PIDocNo = string.Empty
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lMRToPI.Count > 0) ProcessMRToPI(ref lMRToPI);
                        else lMRToPI.Clear();
                    }

                    #endregion

                    #region DOCt to SLI

                    if (ImportCode == "ImportDOCtToSLI")
                    {
                        var lDOCtToSLI = new List<DOCtToSLI>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 3)
                            {
                                if (splits.Count() > 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lDOCtToSLI.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lDOCtToSLI.Add(new DOCtToSLI()
                                        {
                                            DocDt = splits[0].Trim(),
                                            CtCode = splits[1].Trim(),
                                            Amt = Decimal.Parse(splits[2].Trim()),
                                            DOCtDocNo = string.Empty,
                                            SLIDocNo = string.Empty
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lDOCtToSLI.Count > 0) ProcessDOCtToSLI(ref lDOCtToSLI);
                        else lDOCtToSLI.Clear();
                    }

                    #endregion

                    #region Item Local POS SRN

                    if (ImportCode == "ImportItemLocal")
                    {
                        var lItLocal = new List<ItemLocal>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 2)
                            {
                                if (splits.Count() > 2)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 2)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this item local " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lItLocal.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lItLocal.Add(new ItemLocal()
                                        {
                                            ItCode = splits[0].Trim(),
                                            ItCodeInternal = splits[1].Trim(),
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lItLocal.Count > 0)
                            ProcessItemLocal(ref lItLocal);
                        else
                            lItLocal.Clear();
                    }

                    #endregion

                    #region ItemCostCategory

                    if (ImportCode == "ImportItemCostCategory")
                    {
                        var lItemCostCategory = new List<ItemCostCategory>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 3)
                            {
                                if (splits.Count() > 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 3)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Item Cost Category " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lItemCostCategory.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lItemCostCategory.Add(new ItemCostCategory()
                                        {
                                            ItCode = splits[0].Trim(),
                                            CCCode = splits[1].Trim(),
                                            CCtCode = splits[2].Trim(),
                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }
                        if (lItemCostCategory.Count > 0) ProcessItemCostCategory(ref lItemCostCategory);
                        else lItemCostCategory.Clear();
                    }

                    #endregion

                    #region Budget Category

                    if (ImportCode == "ImportBudgetCategory")
                    {
                        var lBudgetCategory = new List<BudgetCategory>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 7)
                            {
                                if (splits.Count() > 7)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 7)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Budget Category " + splits[0].Trim() + " have a correct column count data.");
                                }

                                lBudgetCategory.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();

                                    lBudgetCategory.Add(new BudgetCategory()
                                    {
                                        BCName = splits[0].Trim(),
                                        LocalCode = splits[1].Trim(),
                                        DeptCode = splits[2].Trim(),
                                        CCtCode = splits[3].Trim(),
                                        CCCode = splits[4].Trim(),
                                        AcNo = splits[5].Trim(),
                                        BudgetType = splits[6].Trim()
                                    });
                                }
                            }
                        }

                        if (lBudgetCategory.Count == 0)
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            ClearData();
                            return;
                        }
                        if (lBudgetCategory.Count > 0) ProcessBudgetCategory(ref lBudgetCategory);
                        else lBudgetCategory.Clear();
                    }

                    #endregion

                    #region Item Price

                    if (ImportCode == "ImportItemPrice")
                    {
                        var lItemPrice = new List<ItemPrice>();
                        var lItemPriceHdr = new List<ItemPriceHdr>();
                        var lItemPriceDtl = new List<ItemPriceDtl>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 8)
                            {
                                if (splits.Count() > 8)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 8)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Currency code " + splits[2].Trim() + " and UOM " + splits[3].Trim() + " have a correct column count data.");
                                }

                                lItemPrice.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lItemPrice.Add(new ItemPrice()
                                        {
                                            DocNo = string.Empty,
                                            Index = 0,
                                            DocDt = splits[0].Trim(),
                                            StartDt = splits[1].Trim(),
                                            CurCode = splits[2].Trim(),
                                            PriceUomCode = splits[3].Trim(),
                                            Remark = splits[4].Trim(),
                                            DNo = Sm.Right(string.Concat("0000000000", 1), int.Parse(mItemPriceDNoLength)),
                                            ItCode = splits[5].Trim(),
                                            UPrice = splits[6].Trim().Length > 0 ? Decimal.Parse(splits[6].Trim()) : 0m,
                                            RemarkDtl = splits[7].Trim(),

                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lItemPrice.Count > 0)
                        {
                            lItemPrice.OrderBy(o => o.CurCode).ThenBy(o => o.PriceUomCode);
                            SetIndex(ref lItemPrice);
                            SetDocNo(ref lItemPrice);
                            DataBreakDown(ref lItemPrice, ref lItemPriceHdr, ref lItemPriceDtl);
                            ProcessDtl(ref lItemPriceDtl);
                            ProcessItemPrice(ref lItemPrice, ref lItemPriceHdr, ref lItemPriceDtl);
                        }
                        else lItemPrice.Clear();
                    }

                    #endregion

                    #region DO To Customer

                    if (ImportCode == "ImportDOToCustomer")
                    {
                        var lDOCt = new List<DOToCustomer>();
                        var lDOCtHdr = new List<DOToCustomerHdr>();
                        var lDOCtDtl = new List<DOToCustomerDtl>();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 29)
                            {
                                if (splits.Count() > 29)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 29)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Warehouse code " + splits[1].Trim() + " and Customer Code " + splits[2].Trim() + " have a correct column count data.");
                                }

                                lDOCt.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lDOCt.Add(new DOToCustomer()
                                        {
                                            Mth = string.Empty,
                                            Yr = string.Empty,
                                            DocNo = string.Empty,
                                            Index = 0,
                                            DocDt = splits[0].Trim(),
                                            WhsCode = splits[1].Trim(),
                                            CtCode = splits[2].Trim(),
                                            DocNoInternal = splits[3].Trim(),
                                            SAName = splits[4].Trim(),
                                            SAAddress = splits[5].Trim(),
                                            SACityCode = splits[6].Trim(),
                                            SACntCode = splits[7].Trim(),
                                            SAPostalCd = splits[8].Trim(),
                                            SAPhone = splits[9].Trim(),
                                            SAFax = splits[10].Trim(),
                                            SAEmail = splits[11].Trim(),
                                            SAMobile = splits[12].Trim(),
                                            CurCode = splits[13].Trim(),
                                            Driver = splits[14].Trim(),
                                            Expedition = splits[15].Trim(),
                                            VehicleRegNo = splits[16].Trim(),
                                            Remark = splits[17].Trim(),
                                            DNo = Sm.Right(string.Concat("0000000000", 1), 10),
                                            ItCode = splits[18].Trim(),
                                            PropCode = splits[19].Trim(),
                                            BatchNo = splits[20].Trim(),
                                            Source = splits[21].Trim(),
                                            Lot = splits[22].Trim(),
                                            Bin = splits[23].Trim(),
                                            Qty = splits[24].Trim().Length > 0 ? Decimal.Parse(splits[24].Trim()) : 0m,
                                            Qty2 = splits[25].Trim().Length > 0 ? Decimal.Parse(splits[25].Trim()) : 0m,
                                            Qty3 = splits[26].Trim().Length > 0 ? Decimal.Parse(splits[26].Trim()) : 0m,
                                            UPrice = splits[27].Trim().Length > 0 ? Decimal.Parse(splits[27].Trim()) : 0m,
                                            RemarkDtl = splits[28].Trim(),


                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (lDOCt.Count > 0)
                        {
                            lDOCt.OrderBy(o => o.DocDt).ThenBy(o => o.WhsCode);
                            SetIndex(ref lDOCt);
                            SetDocNo(ref lDOCt);
                            DataBreakDown(ref lDOCt, ref lDOCtHdr, ref lDOCtDtl);
                            ProcessDtl(ref lDOCtDtl);
                            ProcessDOCt(ref lDOCt, ref lDOCtHdr, ref lDOCtDtl);

                        }
                        else lDOCt.Clear();
                    }

                    #endregion

                    #region Personal Information

                    if (ImportCode == "ImportPersonalInformation")
                    {
                        var PersonalInformation = new List<PersonalInformation>();
                        var PersonalInformationHdr = new List<PersonalInformationHdr>();
                        var PersonalInformationDtl2 = new List<PersonalInformationDtl2>();
                        var PersonalInformationDtl3 = new List<PersonalInformationDtl3>();
                        var PersonalInformationDtl4 = new List<PersonalInformationDtl4>();
                        var PersonalInformationDtl5 = new List<PersonalInformationDtl5>();
                        var PersonalInformationDtl6 = new List<PersonalInformationDtl6>();
                        var PersonalInformationDtl7 = new List<PersonalInformationDtl7>();
                        var PersonalInformationDtl8 = new List<PersonalInformationDtl8>();


                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (splits.Count() != 84)
                            {
                                if (splits.Count() > 84)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                                }
                                else if (splits.Count() < 84)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "Make sure this Currency code " + splits[2].Trim() + " and UOM " + splits[3].Trim() + " have a correct column count data.");
                                }

                                PersonalInformation.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        PersonalInformation.Add(new PersonalInformation()
                                        {
                                            DocNo = string.Empty,
                                            DNo = string.Empty,
                                            Index = 0,
                                            EmpRequestNo = splits[0].Trim(),
                                            DocDt = splits[1].Trim(),
                                            EmpName = splits[2].Trim(),
                                            DisplayName = splits[3].Trim(),
                                            UserCode = splits[4].Trim(),
                                            OldCode = splits[5].Trim(),
                                            ShortCode = splits[6].Trim(),
                                            Barcode = splits[7].Trim(),
                                            EmploymentSts = splits[8].Trim(),
                                            Site = splits[9].Trim(),
                                            Divition = splits[10].Trim(),
                                            Department = splits[11].Trim(),
                                            Position = splits[12].Trim(),
                                            SectionCode = splits[13].Trim(),
                                            Workinggroup = splits[14].Trim(),
                                            POH = splits[15].Trim(),
                                            JoinDate = splits[16].Trim(),
                                            ResignDt = splits[17].Trim(),
                                            Adreess = splits[18].Trim(),
                                            city = splits[19].Trim(),
                                            SubDistrict = splits[20].Trim(),
                                            Vilage = splits[21].Trim(),
                                            RTRW = splits[22].Trim(),
                                            PostalCode = splits[23].Trim(),
                                            Domicily = splits[24].Trim(),
                                            Identity = splits[25].Trim(),
                                            MaritalSts = splits[26].Trim(),
                                            WedingDt = splits[27].Trim(),
                                            Gender = splits[28].Trim(),
                                            Religion = splits[29].Trim(),
                                            BirthPlace = splits[30].Trim(),
                                            BirthDt = splits[31].Trim(),
                                            BlodType = splits[32].Trim(),
                                            Mother = splits[33].Trim(),
                                            SystemType = splits[34].Trim(),
                                            GradeLevel = splits[35].Trim(),
                                            PayrollGroup = splits[36].Trim(),
                                            PayrollType = splits[37].Trim(),
                                            PayrunPeriod = splits[38].Trim(),
                                            NPWP = splits[39].Trim(),
                                            NonIncomingTax = splits[40].Trim(),
                                            BankName = splits[41].Trim(),
                                            BankBranch = splits[42].Trim(),
                                            AccountName = splits[43].Trim(),
                                            AccountNo = splits[44].Trim(),
                                            Phone = splits[45].Trim(),
                                            Mobile = splits[46].Trim(),
                                            Email = splits[47].Trim(),
                                            FamilyName = splits[48].Trim(),
                                            FamilySts = splits[49].Trim(),
                                            FamilyGender = splits[50].Trim(),
                                            FamilyBirtDt = splits[51].Trim(),
                                            IDCard = splits[52].Trim(),
                                            NIN = splits[53].Trim(),
                                            FamilyRemark = splits[54].Trim(),
                                            PRName = splits[55].Trim(),
                                            PRPhone = splits[56].Trim(),
                                            PRAddress = splits[57].Trim(),
                                            PRRemark = splits[58].Trim(),
                                            Company = splits[59].Trim(),
                                            WEPosition = splits[60].Trim(),
                                            Period = splits[61].Trim(),
                                            WERemark = splits[62].Trim(),
                                            School = splits[63].Trim(),
                                            Level = splits[64].Trim(),
                                            Major = splits[65].Trim(),
                                            HighestInd = splits[66].Trim(),
                                            EdcRemark = splits[67].Trim(),
                                            CompetenceName = splits[68].Trim(),
                                            Description = splits[69].Trim(),
                                            Score = splits[70].Trim().Length > 0 ? Decimal.Parse(splits[53].Trim()) : 0,
                                            SpecTraining = splits[71].Trim(),
                                            TrainingPlace = splits[72].Trim(),
                                            TrainingPeriod = splits[73].Trim(),
                                            TrainingYear = splits[74].Trim(),
                                            TrainingRemark = splits[75].Trim(),
                                            InterviewBy = splits[76].Trim(),
                                            Place = splits[77].Trim(),
                                            InterviewDt = splits[78].Trim(),
                                            GrossSalary = splits[79].Trim(),
                                            StartWorkDt = splits[80].Trim(),
                                            Placement = splits[81].Trim(),
                                            CompanyDept = splits[82].Trim(),
                                            Probation = splits[83].Trim(),


                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }

                        if (PersonalInformation.Count > 0)
                        {
                            //PersonalInformation.OrderBy(o => o.CurCode).ThenBy(o => o.PriceUomCode);
                            SetIndex(ref PersonalInformation);
                            SetDocNo(ref PersonalInformation);
                            DataBreakDown(ref PersonalInformation, ref PersonalInformationHdr,
                                ref PersonalInformationDtl2, ref PersonalInformationDtl3, ref PersonalInformationDtl4, ref PersonalInformationDtl5,
                                ref PersonalInformationDtl6, ref PersonalInformationDtl7
                                );
                            ProcessDtl2(ref PersonalInformationDtl2);
                            ProcessDtl3(ref PersonalInformationDtl3);
                            ProcessDtl4(ref PersonalInformationDtl4);
                            ProcessDtl5(ref PersonalInformationDtl5);
                            ProcessDtl6(ref PersonalInformationDtl6);
                            ProcessDtl7(ref PersonalInformationDtl7);
                            ProcessEmpReq(ref PersonalInformation, ref PersonalInformationDtl8);
                            ProcessPersonalInformation(ref PersonalInformation, ref PersonalInformationHdr, ref PersonalInformationDtl2,
                                ref PersonalInformationDtl3, ref PersonalInformationDtl4, ref PersonalInformationDtl5, ref PersonalInformationDtl6,
                                ref PersonalInformationDtl7, ref PersonalInformationDtl8);
                        }
                        else PersonalInformation.Clear();
                    }
                    #endregion

                    #region investor

                    if (ImportCode == "ImportSaham")
                    {
                        var lSahamMovement = new List<SahamMovement>();
                        var lInvestor = new List<Investor>();
                        int ChecklistGrd2 = 0;
                        int DataMaster = Grd1.Rows.Count;
                        List<int> lColCheck = new List<int>();
                        for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                        {
                            if (Sm.GetGrdBool(Grd2, i, 1))
                            {
                                ChecklistGrd2 += 1;
                                lColCheck.Add(i);
                            }
                        }
                        int[] ColCheck = lColCheck.ToArray();

                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            if (ChecklistGrd2 != DataMaster)
                            {
                                Sm.StdMsg(mMsgType.Warning, "CSV column chceklist (" + ChecklistGrd2.ToString() + ") doesn't same with Form Input (" + (Grd1.Rows.Count).ToString() + ").");

                                lSahamMovement.Clear();
                                ClearData();
                                return;
                            }
                            else
                            {
                                if (IsFirst)
                                    IsFirst = false;
                                else
                                {
                                    var arr = splits.ToArray();
                                    if (arr[0].Trim().Length > 0)
                                    {
                                        lSahamMovement.Add(new SahamMovement()
                                        {
                                            DocNo = string.Empty,
                                            DocDt = splits[ColCheck[0]].Trim(),
                                            Account = splits[ColCheck[1]].Trim(),
                                            SID = splits[ColCheck[2]].Trim(),
                                            Name = splits[ColCheck[3]].Trim(),
                                            NIP = splits[ColCheck[4]].Trim(),
                                            Country = splits[ColCheck[5]].Trim(),
                                            Category = splits[ColCheck[6]].Trim(),
                                            Sekuritas = splits[ColCheck[7]].Trim(),
                                            Amt = splits[ColCheck[8]].Trim().Length > 0 ? Decimal.Parse(splits[ColCheck[8]].Trim()) : 0m,
                                            StatusBalance = splits[ColCheck[9]].Trim(),

                                        });
                                    }
                                    else
                                    {
                                        Sm.StdMsg(mMsgType.NoData, "");
                                        ClearData();
                                        return;
                                    }
                                }
                            }
                        }



                        if (lSahamMovement.Count > 0)
                        {
                            lSahamMovement.OrderBy(o => o.DocDt);
                            ProcessSahamMovement(ref lSahamMovement);
                        }
                        else
                            lSahamMovement.Clear();

                        #endregion
                    }
                }
            }
        }

        private void GetCSVDataJournal(ref string FileName, ref bool IsFirst)
        {
            var lJournal = new List<Journal>();
            var lJournalHdr = new List<JournalHdr>();
            var lJournalDtl = new List<JournalDtl>();

            using (var rd = new StreamReader(FileName))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 8)
                    {
                        if (splits.Count() > 8)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[1].Trim() + " could not have any comma(s) in its description.");
                        }
                        else if (splits.Count() < 8)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this journal transaction " + splits[1].Trim() + " have a correct column count data.");
                        }

                        lJournal.Clear();
                        ClearData();
                        return;
                    }
                    else
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            var arr = splits.ToArray();

                            if (arr[0].Trim().Length > 0)
                            {
                                lJournal.Add(new Journal()
                                {
                                    Mth = string.Empty,
                                    Yr = string.Empty,
                                    DocNo = string.Empty,
                                    DNo = string.Empty,
                                    DocDt = splits[0].Trim(),
                                    Period = splits[1].Trim(),
                                    JnDesc = splits[2].Trim(),
                                    Entity = splits[3].Trim(),
                                    Remark = splits[4].Trim(),
                                    AcNo = splits[5].Trim(),
                                    DAmt = decimal.Parse(splits[6].Trim()),
                                    CAmt = decimal.Parse(splits[7].Trim())
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.NoData, "");
                                ClearData();
                                return;
                            }
                        }
                    }
                }

                if (lJournal.Count > 0)
                {
                    lJournal.OrderBy(o => o.DocDt).ThenBy(p => p.Remark);
                    SetIndex(ref lJournal);
                    SetDocNo(ref lJournal);
                    DataBreakDown(ref lJournal, ref lJournalHdr, ref lJournalDtl);
                    ProcessDtl1(ref lJournalDtl);
                    ProcessJournalTransaction(ref lJournal, ref lJournalHdr, ref lJournalDtl);
                }
                else
                    lJournal.Clear();
            }
        }

        private void GetCSVDataJournal2(ref string FileName, ref bool IsFirst)
        {
            var l = new List<Journal>();
            string DocDtTemp = string.Empty, RemarkTemp = string.Empty;

            using (var rd = new StreamReader(FileName))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 8)
                    {
                        if (splits.Count() > 8)
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                        else if (splits.Count() < 8)
                            Sm.StdMsg(mMsgType.Warning, "Make sure this journal transaction " + splits[0].Trim() + " have a correct column count data.");
                        l.Clear();
                        ClearData();
                        return;
                    }
                    else
                    {
                        if (IsFirst)
                            IsFirst = false;
                        else
                        {
                            var arr = splits.ToArray();
                            RemarkTemp = splits[0].Trim();
                            DocDtTemp = splits[1].Trim();
                            if (RemarkTemp.Length > 0)
                            {
                                l.Add(new Journal()
                                {
                                    Remark = RemarkTemp,
                                    DocDt = DocDtTemp,
                                    JnDesc = splits[2].Trim(),
                                    AcNo = splits[3].Trim(),
                                    DAmt = decimal.Parse(splits[4].Trim()),
                                    CAmt = decimal.Parse(splits[5].Trim()),
                                    CCCode = splits[6].Trim(),
                                    RemarkD = splits[7].Trim()
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.NoData, string.Empty);
                                ClearData();
                                return;
                            }
                        }
                    }
                }

                if (l.Count > 0)
                    ProcessJournal(ref l, string.Concat(DocDtTemp.Substring(4, 2), "/", DocDtTemp.Substring(2, 2)));
                else
                    l.Clear();
            }
        }

        private void ProcessColumnCsv(string ImportCode, bool IsFirst, StreamReader rd)
        {
            SetGrd(ImportCode);

            string[] ArrayColName = new string[] { "Date", "Account", "SID", "Name", "NIP", "Country", "Category", "Sekuritas", "Amt", "Statusbalance" };
            for (int i = 0; i < ArrayColName.Length; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[i, 0].Value = i + 1;
                Grd1.Cells[i, 1].Value = ArrayColName[i];
            }

            while (!rd.EndOfStream)
            {
                var splits = rd.ReadLine().Split(',');

                if (IsFirst)
                {
                    var arr = splits.ToArray();
                    for (int Row = 0; Row < arr.Count(); Row++)
                    {
                        Grd2.Rows.Add();
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Grd2.Cells[Row, 2].Value = arr[Row].Trim();
                    }
                    IsFirst = false;
                }
            }
        }
        private void SetGrd(string importCode)
        {
            if (importCode == "ImportSaham")
            {

                #region Grd1

                Grd1.Cols.Count = 2;
                Grd1.FrozenArea.ColCount = 2;

                Sm.GrdHdrWithColWidth(
                        Grd1,
                        new string[]
                        {
                        //0
                        "No",

                        //1-2
                        "Column",
                        
                        },
                         new int[]
                        {
                        //0
                        40, 200
                        }
                    );
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });

                #endregion

                #region Grd2

                Grd2.Cols.Count = 3;
                Grd2.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd2,
                        new string[]
                        {
                        //0
                        "No",

                        //1-2
                        "",
                        "Column",

                        },
                         new int[]
                        {
                        //0
                        40,

                        //1-5
                        20, 200
                        }
                    );
                Sm.GrdColCheck(Grd2, new int[] { 1 });
                Sm.GrdColReadOnly(Grd2, new int[] { 0, 2 });
                #endregion
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueImportCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueImportCode, new Sm.RefreshLue1(SetLueImportCode));
        }

        #endregion

        #region Button Event

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OD.InitialDirectory = "c:";
                OD.Filter = "CSV files (*.csv)|*.CSV";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFileName.Text = OD.FileName;
                Sm.ClearGrd(Grd2, false);
                Sm.ClearGrd(Grd1, false);
                var rd = new StreamReader(TxtFileName.Text);
                ProcessColumnCsv(Sm.GetLue(LueImportCode), true, rd);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public string Alias { get; set; }
            public int Level { get; set; }
        }

        private class EmpSalary
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
        }

        private class EmpSalaryExt
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
        }

        private class EmpSalaryExt2
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string SSPCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
            public string PrevEndDt { get; set; }
        }

        private class EmployeeSalarySS2
        {
            public string EmpCode { get; set; }
            public string SSPCode { get; set; }
            public string StartDt { get; set; }
        }

        private class EmployeeAllowanceDeduction
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
            public string PrevEndDt { get; set; }
        }

        private class EmployeeAllowanceDeduction2
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
        }

        private class Asset
        {
            public string AssetCode { get; set; }
            public string ItCode { get; set; }
            public string AssetName { get; set; }
            public string DisplayName { get; set; }
            public string AssetType { get; set; }
            public string LeasingInd { get; set; }
            public string RentedInd { get; set; }
            public string SoldInd { get; set; }
            public string AssetDt { get; set; }
            public decimal AssetValue { get; set; }
            public decimal EcoLifeYr { get; set; }
            public decimal EcoLife { get; set; }
            public string DepreciationCode { get; set; }
            public decimal PercentageAnnualDepreciation { get; set; }
            public string CCCode { get; set; }
            public string AcNo { get; set; }
            public string AcNo2 { get; set; }
            public string AssetCategoryCode { get; set; }
            public decimal Length { get; set; }
            public decimal Height { get; set; }
            public decimal Width { get; set; }
            public decimal Volume { get; set; }
            public decimal Diameter { get; set; }
            public decimal Wide { get; set; }
            public string LengthUomCode { get; set; }
            public string HeightUomCode { get; set; }
            public string WidthUomCode { get; set; }
            public string VolumeUomCode { get; set; }
            public string DiameterUomCode { get; set; }
            public string WideUomCode { get; set; }
            public string Parent { get; set; }
            public string ShortCode { get; set; }
            public string FileName { get; set; }
            public string Location { get; set; }
            public string Classification { get; set; }
            public string SubClassification { get; set; }
            public string Type { get; set; }
            public string SubType { get; set; }
            public string Location2 { get; set; }
            public string SubLocation { get; set; }
            public string Location3 { get; set; }
            public string SiteCode { get; set; }
        }

        private class Vendor
        {
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string ShortName { get; set; }
            public string VdCtCode { get; set; }
            public string Address { get; set; }
            public string CityCode { get; set; }
            public string SDCode { get; set; }
            public string VilCode { get; set; }
            public string PostalCd { get; set; }
            public string Website { get; set; }
            public string Parent { get; set; }
            public string EstablishedYr { get; set; }
            public string IdentityNo { get; set; }
            public string TIN { get; set; }
            public string TaxInd { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public decimal CreditLimit { get; set; }
            public string NIB { get; set; }
            public string Remark { get; set; }
        }

        private class Customer
        {
            public string CtCode { get; set; }
            public string CtName { get; set; }
            public string AgentInd { get; set; }
            public string CtShortCode { get; set; }
            public string CtCtCode { get; set; }
            public string CtGrpCode { get; set; }
            public string CntCode { get; set; }
            public string CityCode { get; set; }
            public string NPWP { get; set; }
            public string ShippingMark { get; set; }
            public string Address { get; set; }
            public string PostalCd { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Mobile { get; set; }
            public string NIB { get; set; }
            public string Remark { get; set; }

        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DisplayName { get; set; }
            public string EmploymentStatus { get; set; }
            public string UserCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string ShortCode { get; set; }
            public string DivisionCode { get; set; }
            public string DeptCode { get; set; }
            public string PosCode { get; set; }
            public string PositionStatusCode { get; set; }
            public string SectionCode { get; set; }
            public string WorkGroupCode { get; set; }
            public string EntCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LeaveStartDt { get; set; }
            public string TGDt { get; set; }
            public string IDNumber { get; set; }
            public string Gender { get; set; }
            public string Religion { get; set; }
            public string MaritalStatus { get; set; }
            public string WeddingDt { get; set; }
            public string BirthPlace { get; set; }
            public string BirthDt { get; set; }
            public string Address { get; set; }
            public string CityCode { get; set; }
            public string SubDistrict { get; set; }
            public string Village { get; set; }
            public string RTRW { get; set; }
            public string PostalCode { get; set; }
            public string Domicile { get; set; }
            public string SiteCode { get; set; }
            public string POH { get; set; }
            public string BloodType { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string GrdLvlCode { get; set; }
            public string PGCode { get; set; }
            public string NPWP { get; set; }
            public string SystemType { get; set; }
            public string PayrollType { get; set; }
            public string PTKP { get; set; }
            public string BankCode { get; set; }
            public string BankBranch { get; set; }
            public string BankAcNo { get; set; }
            public string BankName { get; set; }
            public string BankAcName { get; set; }
            public string PayrunPeriod { get; set; }
            public string Mother { get; set; }
            public string ContractDt { get; set; }
        }

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string HSCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string ForeignName { get; set; }
            public string SalesItemInd { get; set; }
            public string ServiceItemInd { get; set; }
            public string FixedItemInd { get; set; }
            public string PlanningItemInd { get; set; }
            public string ItCtCode { get; set; }
            public string ItScCode { get; set; }
            public string ItBrCode { get; set; }
            public string ItGrpCode { get; set; }
            public string ItemRequestDocNo { get; set; }
            public string Specification { get; set; }
            public string Remark { get; set; }
            public string TaxLiableInd { get; set; }
            public string VdCode { get; set; }
            public string PurchaseUOMCode { get; set; }
            public string PGCode { get; set; }
            public decimal MinOrder { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal Length { get; set; }
            public decimal Height { get; set; }
            public decimal Width { get; set; }
            public decimal Volume { get; set; }
            public decimal Diameter { get; set; }
            public string LengthUomCode { get; set; }
            public string HeightUomCode { get; set; }
            public string WidthUomCode { get; set; }
            public string VolumeUomCode { get; set; }
            public string DiameterUomCode { get; set; }
            public string SalesUOMCode { get; set; }
            public string SalesUOMCode2 { get; set; }
            public decimal SalesUomCodeConvert12 { get; set; }
            public decimal SalesUomCodeConvert21 { get; set; }
            public string InventoryUOMCode { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public decimal InventoryUomCodeConvert12 { get; set; }
            public decimal InventoryUomCodeConvert13 { get; set; }
            public decimal InventoryUomCodeConvert21 { get; set; }
            public decimal InventoryUomCodeConvert23 { get; set; }
            public decimal InventoryUomCodeConvert31 { get; set; }
            public decimal InventoryUomCodeConvert32 { get; set; }
            public decimal MinStock { get; set; }
            public decimal MaxStock { get; set; }
            public decimal ReOrderStock { get; set; }
            public string ControlByDeptCode { get; set; }
            public string PlanningUomCode { get; set; }
            public string PlanningUomCode2 { get; set; }
            public decimal PlanningUomCodeConvert12 { get; set; }
            public decimal PlanningUomCodeConvert21 { get; set; }
            public string Information1 { get; set; }
            public string Information2 { get; set; }
            public string Information3 { get; set; }
            public string Information4 { get; set; }
            public string Information5 { get; set; }
            public string PackagingUomCode { get; set; }
            public decimal ConversionRate1 { get; set; }
            public decimal ConversionRate2 { get; set; }
            public decimal GrossWeight { get; set; }
            public decimal NetWeight { get; set; }
            public decimal PackagingLength { get; set; }
            public decimal PackagingWidth { get; set; }
            public decimal PackagingHeight { get; set; }
        }

        private class ItemKSM
        {
            public string ItCodeOld { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string ForeignName { get; set; }
            public string ItCtCode { get; set; }
            public string ItScCode { get; set; }
            public string ItGrpCode { get; set; }
            public string InventoryUomCode { get; set; }
            public string Specification { get; set; }
            public string Remark { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
        }

        #region EmployeeSS

        private class EmployeeSS
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string EntCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string SSCode { get; set; }
            public string CardNo { get; set; }
            public string HealthFacility { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string Problem { get; set; }
            public string RemarkD1 { get; set; }
            public string DNo2 { get; set; }
            public string FamilyName { get; set; }
            public string Status { get; set; }
            public string Gender { get; set; }
            public string IDNo { get; set; }
            public string NIN { get; set; }
            public string SSCode2 { get; set; }
            public string CardNo2 { get; set; }
            public string HealthFacility2 { get; set; }
            public string StartDt2 { get; set; }
            public string EndDt2 { get; set; }
            public string BirthDt { get; set; }
            public string Problem2 { get; set; }
            public string RemarkD2 { get; set; }

        }
        private class EmployeeSSHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string EntCode { get; set; }
            public string Remark { get; set; }
        }

        private class EmployeeSSDtl
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string EntCode { get; set; }
            public string DNo { get; set; }
            public string SSCode { get; set; }
            public string CardNo { get; set; }
            public string HealthFacility { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string Problem { get; set; }
            public string RemarkD1 { get; set; }

        }

        private class EmployeeSSDtl2
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string DNo2 { get; set; }
            public string FamilyName { get; set; }
            public string Status { get; set; }
            public string Gender { get; set; }
            public string IDNo { get; set; }
            public string NIN { get; set; }
            public string SSCode2 { get; set; }
            public string CardNo2 { get; set; }
            public string HealthFacility2 { get; set; }
            public string StartDt2 { get; set; }
            public string EndDt2 { get; set; }
            public string BirthDt { get; set; }
            public string Problem2 { get; set; }
            public string EntCode { get; set; }
            public string RemarkD2 { get; set; }

        }

        #endregion

        private class CustomerDeposit
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtCode { get; set; }
            public string CurCode1 { get; set; }
            public decimal Amt1 { get; set; }
            public decimal RateAmt1 { get; set; }
            public string CurCode2 { get; set; }
            public decimal Amt2 { get; set; }
            public string Remark { get; set; }
            public decimal ExistingAmt { get; set; }
            public string JournalDocNo { get; set; }

        }

        #region Stock Opname

        private class StockOpname
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string Status { get; set; }
            public string WhsCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal QtyActual { get; set; }
            public decimal QtyActual2 { get; set; }
            public decimal QtyActual3 { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark2 { get; set; }
            public string JournalDocNo { get; set; }


        }

        private class StockOpnameHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Status { get; set; }
            public string WhsCode { get; set; }
            public string Remark { get; set; }
            public string JournalDocNo { get; set; }

        }

        private class StockOpnameDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal QtyActual { get; set; }
            public decimal QtyActual2 { get; set; }
            public decimal QtyActual3 { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal QtySystem { get; set; }
            public decimal QtySystem2 { get; set; }
            public decimal QtySystem3 { get; set; }
            public string Remark2 { get; set; }

        }

        private class Stock
        {
            public string WhsCode { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal BQty { get; set; }
            public decimal BQty2 { get; set; }
            public decimal BQty3 { get; set; }
            public decimal AQty { get; set; }
            public decimal AQty2 { get; set; }
            public decimal AQty3 { get; set; }
        }

        #endregion

        #region Vendor Deposit

        private class VendorDeposit
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public string EntCode { get; set; }
            public string CurCode1 { get; set; }
            public decimal ExistingAmt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal RateAmt1 { get; set; }
            public string CurCode2 { get; set; }
            public decimal Amt2 { get; set; }
            public string JournalDocNo { get; set; }
            public string Remark { get; set; }
        }

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string CtCode { get; set; }
            public string VdCode { get; set; }
        }

        #endregion

        #region Stock Initial

        private class StockInitial
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CurCode { get; set; }
            public string ExcRate { get; set; }
            public string JournalDocNo { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty1 { get; set; }
            public string Uom1 { get; set; }
            public decimal Qty2 { get; set; }
            public string Uom2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Uom3 { get; set; }
            public decimal Price { get; set; }
            public string Remark1 { get; set; }
        }

        private class StockInitialHdr
        {
            public int Index { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CurCode { get; set; }
            public string ExcRate { get; set; }
            public string JournalDocNo { get; set; }
            public string Remark { get; set; }

        }

        private class StockInitialDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty1 { get; set; }
            public string Uom1 { get; set; }
            public decimal Qty2 { get; set; }
            public string Uom2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Uom3 { get; set; }
            public decimal Price { get; set; }
            public string Remark1 { get; set; }
        }

        #endregion

        private class CostCategory
        {
            public string CCCode { get; set; }
            public string CCtName { get; set; }
            public string AcNo { get; set; }
            public string CCGrpCode { get; set; }
        }

        #region Employee Contract Document

        private class EmpContractDoc
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string LocalDocNo { get; set; }
            public decimal ContractExtention { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string Remark { get; set; }
            public string Renewal { get; set; }
            public string ManagementRepresentatif { get; set; }
            public string EmpCode2 { get; set; }

        }

        #endregion

        private class MRToPI
        {
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public decimal Amt { get; set; }

            public string MRDocNo { get; set; }
            public string PORDocNo { get; set; }
            public string QtDocNo { get; set; }
            public string PODocNo { get; set; }
            public string RecvVdDocNo { get; set; }
            public string PIDocNo { get; set; }
        }

        private class DOCtToSLI
        {
            public string DocDt { get; set; }
            public string CtCode { get; set; }
            public decimal Amt { get; set; }

            public string DOCtDocNo { get; set; }
            public string SLIDocNo { get; set; }
        }

        private class ItemLocal
        {
            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
        }

        #region Journal Transaction

        private class Journal
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public int Index { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string Period { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
            public string Entity { get; set; }
            public string CCCode { get; set; }
            public string RemarkD { get; set; }

        }
        private class JournalHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string Period { get; set; }

        }
        private class JournalDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
            public string Entity { get; set; }
        }
        #endregion

        #region Updating Item's Cost Cagory

        private class ItemCostCategory
        {
            public string ItCode { get; set; }
            public string CCCode { get; set; }
            public string CCtCode { get; set; }
        }

        #endregion

        #region Insert Budget Category

        private class BudgetCategory
        {
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public string LocalCode { get; set; }
            public string DeptCode { get; set; }
            public string CCtCode { get; set; }
            public string CCCode { get; set; }
            public string AcNo { get; set; }
            public string BudgetType { get; set; }
        }

        #endregion

        #region Item's Price

        private class ItemPrice
        {
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string StartDt { get; set; }
            public string CurCode { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public decimal UPrice { get; set; }
            public string RemarkDtl { get; set; }
        }

        private class ItemPriceHdr
        {
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string StartDt { get; set; }
            public string CurCode { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }

        }

        private class ItemPriceDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public decimal UPrice { get; set; }
            public string RemarkDtl { get; set; }
        }

        #endregion

        #region DO To Customer

        private class DOToCustomer
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CtCode { get; set; }
            public string DocNoInternal { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string SACityCode { get; set; }
            public string SACntCode { get; set; }
            public string SAPostalCd { get; set; }
            public string SAPhone { get; set; }
            public string SAFax { get; set; }
            public string SAEmail { get; set; }
            public string SAMobile { get; set; }
            public string CurCode { get; set; }
            public string Driver { get; set; }
            public string Expedition { get; set; }
            public string VehicleRegNo { get; set; }
            public string Remark { get; set; }

            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string RemarkDtl { get; set; }
        }

        private class DOToCustomerHdr
        {
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CtCode { get; set; }
            public string DocNoInternal { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string SACityCode { get; set; }
            public string SACntCode { get; set; }
            public string SAPostalCd { get; set; }
            public string SAPhone { get; set; }
            public string SAFax { get; set; }
            public string SAEmail { get; set; }
            public string SAMobile { get; set; }
            public string CurCode { get; set; }
            public string Driver { get; set; }
            public string Expedition { get; set; }
            public string VehicleRegNo { get; set; }
            public string Remark { get; set; }

        }

        private class DOToCustomerDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string RemarkDtl { get; set; }
        }

        #endregion

        #region Personal Information

        private class PersonalInformation
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public int Index { get; set; }
            //header
            public string DocDt { get; set; }
            public string EmpName { get; set; }
            public string DisplayName { get; set; }
            public string EmpRequestNo { get; set; }
            //general
            public string UserCode { get; set; }
            public string OldCode { get; set; }
            public string ShortCode { get; set; }
            public string Barcode { get; set; }
            public string EmploymentSts { get; set; }
            public string Site { get; set; }
            public string Divition { get; set; }
            public string Department { get; set; }
            public string Position { get; set; }
            public string Workinggroup { get; set; }
            public string JoinDate { get; set; }
            public string Adreess { get; set; }
            public string city { get; set; }
            public string Domicily { get; set; }
            public string Identity { get; set; }
            public string SystemType { get; set; }
            public string GradeLevel { get; set; }
            public string PayrollGroup { get; set; }
            public string PayrollType { get; set; }
            public string PayrunPeriod { get; set; }
            public string NPWP { get; set; }
            public string NonIncomingTax { get; set; }
            public string BankName { get; set; }
            public string AccountName { get; set; }
            public string AccountNo { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }
            public string ResignDt { get; set; }
            public string MaritalSts { get; set; }
            public string WedingDt { get; set; }
            public string Gender { get; set; }
            public string Religion { get; set; }
            public string Mother { get; set; }
            public string BirthPlace { get; set; }
            public string BirthDt { get; set; }
            public string SubDistrict { get; set; }
            public string Vilage { get; set; }
            public string PostalCode { get; set; }
            public string SectionCode { get; set; }
            public string BlodType { get; set; }
            public string Email { get; set; }
            public string PTKP { get; set; }
            public string BankCode { get; set; }
            public string BankAcNo { get; set; }
            public string BankBranch { get; set; }
            public string PGCode { get; set; }
            public string POH { get; set; }
            public string Department2 { get; set; }
            public string RTRW { get; set; }

            //family name
            public string FamilyName { get; set; }
            public string FamilySts { get; set; }
            public string FamilyGender { get; set; }
            public string IDCard { get; set; }
            public string NIN { get; set; }
            public string Remark { get; set; }
            public string FamilyBirtDt { get; set; }
            public string FamilyRemark { get; set; }

            //Personal Reference
            public string PRName { get; set; }
            public string PRPhone { get; set; }
            public string PRAddress { get; set; }
            public string PRRemark { get; set; }

            //Work Experience
            public string Company { get; set; }
            public string WEPosition { get; set; }
            public string Period { get; set; }
            public string WERemark { get; set; }

            //Education
            public string School { get; set; }
            public string Level { get; set; }
            public string Major { get; set; }
            public string HighestInd { get; set; }
            public string EdcRemark { get; set; }

            //competence
            public string CompetenceName { get; set; }
            public string Description { get; set; }
            public Decimal Score { get; set; }

            // training 
            public string SpecTraining { get; set; }
            public string TrainingPlace { get; set; }
            public string TrainingPeriod { get; set; }
            public string TrainingYear { get; set; }
            public string TrainingRemark { get; set; }

            //company 
            public string InterviewBy { get; set; }
            public string Place { get; set; }
            public string InterviewDt { get; set; }
            public string GrossSalary { get; set; }
            public string StartWorkDt { get; set; }
            public string Placement { get; set; }
            public string CompanyDept { get; set; }
            public string Probation { get; set; }

        }

        public class PersonalInformationHdr
        {
            //Header
            public string DocNo { get; set; }
            public int Index { get; set; }
            public string DocDt { get; set; }
            public string EmpName { get; set; }
            public string DisplayName { get; set; }
            public string EmpRequestNo { get; set; }
            // General
            public string DNo { get; set; }
            public string UserCode { get; set; }
            public string OldCode { get; set; }
            public string ShortCode { get; set; }
            public string Barcode { get; set; }
            public string EmploymentSts { get; set; }
            public string Site { get; set; }
            public string Divition { get; set; }
            public string Department { get; set; }
            public string Position { get; set; }
            public string Workinggroup { get; set; }
            public string JoinDate { get; set; }
            public string Adreess { get; set; }
            public string city { get; set; }
            public string Domicily { get; set; }
            public string Identity { get; set; }
            public string SystemType { get; set; }
            public string GradeLevel { get; set; }
            public string PayrollGroup { get; set; }
            public string PayrollType { get; set; }
            public string PayrunPeriod { get; set; }
            public string NPWP { get; set; }
            public string NonIncomingTax { get; set; }
            public string BankName { get; set; }
            public string AccountName { get; set; }
            public string AccountNo { get; set; }
            public string Phone { get; set; }
            public string Mobile { get; set; }

            public string ResignDt { get; set; }
            public string MaritalSts { get; set; }
            public string WedingDt { get; set; }
            public string Gender { get; set; }
            public string Religion { get; set; }
            public string Mother { get; set; }
            public string BirthPlace { get; set; }
            public string BirthDt { get; set; }
            public string SubDistrict { get; set; }
            public string Vilage { get; set; }
            public string PostalCode { get; set; }
            public string SectionCode { get; set; }
            public string BlodType { get; set; }
            public string Email { get; set; }
            public string PTKP { get; set; }
            public string BankCode { get; set; }
            public string BankAcNo { get; set; }
            public string BankBranch { get; set; }
            public string PGCode { get; set; }
            public string POH { get; set; }
            public string Department2 { get; set; }
            public string RTRW { get; set; }

            //company 
            public string InterviewBy { get; set; }
            public string Place { get; set; }
            public string InterviewDt { get; set; }
            public string GrossSalary { get; set; }
            public string StartWorkDt { get; set; }
            public string Placement { get; set; }
            public string CompanyDept { get; set; }
            public string Probation { get; set; }
        }

        // Family
        public class PersonalInformationDtl2
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string FamilyName { get; set; }
            public string FamilySts { get; set; }
            public string FamilyGender { get; set; }
            public string FamilyBirtDt { get; set; }
            public string IDCard { get; set; }
            public string NIN { get; set; }
            public string FamilyRemark { get; set; }
            public string BirthDate { get; set; }

        }

        // Work Experience
        public class PersonalInformationDtl3
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string Company { get; set; }
            public string WEPosition { get; set; }
            public string Period { get; set; }
            public string WERemark { get; set; }
        }

        //Education
        public class PersonalInformationDtl4
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string School { get; set; }
            public string Level { get; set; }
            public string Major { get; set; }
            public string HighestInd { get; set; }
            public string EdcRemark { get; set; }
        }

        //Compentence
        public class PersonalInformationDtl5
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string CompetenceName { get; set; }
            public string Description { get; set; }
            public Decimal Score { get; set; }
        }
        // Training
        public class PersonalInformationDtl6
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string SpecTraining { get; set; }
            public string TrainingPlace { get; set; }
            public string TrainingPeriod { get; set; }
            public string TrainingYear { get; set; }
            public string TrainingRemark { get; set; }
        }
        // personal reference
        public class PersonalInformationDtl7
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string PRName { get; set; }
            public string PRPhone { get; set; }
            public string PRAddress { get; set; }
            public string PRRemark { get; set; }
        }

        public class PersonalInformationDtl8
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string EmpRequestNo { get; set; }
        }
        #endregion

        private class SahamMovement
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string Account { get; set; }
            public string SID { get; set; }
            public string Name { get; set; }
            public string NIP { get; set; }
            public string Country { get; set; }
            public string Category { get; set; }
            public string Sekuritas { get; set; }
            public decimal Amt { get; set; }
            public string StatusBalance { get; set; }

        }

        private class Investor
        {
            public string Account { get; set; }
            public string SID { get; set; }
            public string Name { get; set; }
        }
        #endregion
    }
}
