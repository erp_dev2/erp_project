﻿#region Update
/*
    18/05/2018 [TKG] tambah remark
    31/08/2018 [TKG] tambah info return
    16/02/2022 [BRI/IOK] tambah rak 16 - 25
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvRawMaterialFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvRawMaterial mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvRawMaterialFind(FrmRecvRawMaterial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("    Select 1 As Type, A.DocNo, A.DocDt, A.CancelInd, B.VdCode, C.VdName, B.QueueNo, A.LegalDocVerifyDocNo, ");
            SQL.AppendLine("    Case A.DocType When '1' Then 'Log' When '2' Then 'Balok' End As DocTypeDesc, ");
            SQL.AppendLine("    Case SectionNo ");
            SQL.AppendLine("        When '1' Then A.Shelf1 ");
            SQL.AppendLine("        When '2' Then A.Shelf2 ");
            SQL.AppendLine("        When '3' Then A.Shelf3 ");
            SQL.AppendLine("        When '4' Then A.Shelf4 ");
            SQL.AppendLine("        When '5' Then A.Shelf5 ");
            SQL.AppendLine("        When '6' Then A.Shelf6 ");
            SQL.AppendLine("        When '7' Then A.Shelf7 ");
            SQL.AppendLine("        When '8' Then A.Shelf8 ");
            SQL.AppendLine("        When '9' Then A.Shelf9 ");
            SQL.AppendLine("        When '10' Then A.Shelf10 ");
            SQL.AppendLine("        When '11' Then A.Shelf11 ");
            SQL.AppendLine("        When '12' Then A.Shelf12 ");
            SQL.AppendLine("        When '13' Then A.Shelf13 ");
            SQL.AppendLine("        When '14' Then A.Shelf14 ");
            SQL.AppendLine("        When '15' Then A.Shelf15 ");
            SQL.AppendLine("        When '16' Then A.Shelf16 ");
            SQL.AppendLine("        When '17' Then A.Shelf17 ");
            SQL.AppendLine("        When '18' Then A.Shelf18 ");
            SQL.AppendLine("        When '19' Then A.Shelf19 ");
            SQL.AppendLine("        When '20' Then A.Shelf20 ");
            SQL.AppendLine("        When '21' Then A.Shelf21 ");
            SQL.AppendLine("        When '22' Then A.Shelf22 ");
            SQL.AppendLine("        When '23' Then A.Shelf23 ");
            SQL.AppendLine("        When '24' Then A.Shelf24 ");
            SQL.AppendLine("        When '25' Then A.Shelf25 ");
            SQL.AppendLine("    End As Shelf, ");
            SQL.AppendLine("    Case A.DocType ");
            SQL.AppendLine("        When '1' Then Concat(E.ItName, ' (P:', Convert(Format(E.Length, 2) Using utf8), ', D:', Convert(Format(E.Diameter, 2) Using utf8), ')') ");
            SQL.AppendLine("        When '2' Then Concat(E.ItName, ' (P:', Convert(Format(E.Length, 2) Using utf8), ', T:', Convert(Format(E.Height, 2) Using utf8), ', L:', Convert(Format(E.Width, 2) Using utf8), ')') ");
            SQL.AppendLine("    End As ItName, ");
            SQL.AppendLine("    D.ItCode, D.Qty, ");
            SQL.AppendLine("    0.00 As ReturnLength, 0.00 As ReturnQty, Null As ReturnRemark, ");
            SQL.AppendLine("    A.Remark, A.CreateBy, A.CreateDt, ");
            SQL.AppendLine("    IfNull(D.LastUpBy, Case When A.CreateDt=D.CreateDt Then Null Else D.CreateBy End) As LastUpBy, ");
            SQL.AppendLine("    IfNull(D.LastUpDt, Case When A.CreateDt=D.CreateDt Then Null Else D.CreateDt End) As LastUpDt ");
            SQL.AppendLine("    From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("    Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("    Inner Join TblRecvRawMaterialDtl2 D On A.DocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("    Where A.DocType=@DocType ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 2 As Type, A.DocNo, A.DocDt, A.CancelInd, B.VdCode, C.VdName, B.QueueNo, A.LegalDocVerifyDocNo, ");
            SQL.AppendLine("    Case A.DocType When '1' Then 'Log' When '2' Then 'Balok' End As DocTypeDesc, ");
            SQL.AppendLine("    Null As Shelf, ");
            SQL.AppendLine("    Null As ItName, ");
            SQL.AppendLine("    Null As ItCode, ");
            SQL.AppendLine("    0.00 As Qty, ");
            SQL.AppendLine("    D.Length As ReturnLength, D.Qty As ReturnQty, D.Remark As ReturnRemark, ");
            SQL.AppendLine("    A.Remark, ");
            SQL.AppendLine("    A.CreateBy, A.CreateDt, ");
            SQL.AppendLine("    IfNull(D.LastUpBy, Case When A.CreateDt=D.CreateDt Then Null Else D.CreateBy End) As LastUpBy, ");
            SQL.AppendLine("    IfNull(D.LastUpDt, Case When A.CreateDt=D.CreateDt Then Null Else D.CreateDt End) As LastUpDt ");
            SQL.AppendLine("    From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("    Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("    Inner Join TblRecvRawMaterialDtl4 D On A.DocNo=D.DocNo ");
            SQL.AppendLine("    Where A.DocType=@DocType ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T  ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Dokumen#", 
                        "Tanggal",
                        "Batal",
                        "Tipe",
                        "Vendor",

                        //6-10
                        "Antrian#",
                        "Verifikasi"+Environment.NewLine+"Dokumen Legalitas#",
                        "",
                        "Rak",
                        "Kode Item",
                        
                        //11-15
                        "Nama Item",
                        "Jumlah",
                        "Return"+Environment.NewLine+"Panjang",
                        "Return"+Environment.NewLine+"Jumlah",
                        "Return"+Environment.NewLine+"Keterangan",

                        //16-20
                        "Keterangan",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        
                        //21-22
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 100, 150,  
                        
                        //6-10
                        100, 0, 20, 100, 100, 
                        
                        //11-15
                        300, 100, 130, 130, 180, 
                        
                        //16-20
                        250, 100, 100, 100, 100, 
                        
                        //21-22
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 17, 18, 19, 20, 21, 22 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocType", mFrmParent.mType.ToString());
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLoadingQueue.Text, "QueueNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc, DocNo, Type;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "DocTypeDesc", "VdName", "QueueNo", 
                            
                            //6-10
                            "LegalDocVerifyDocNo", "Shelf", "ItCode", "ItName", "Qty", 
                            
                            //11-15
                            "ReturnLength", "ReturnQty", "ReturnRemark", "Remark", "CreateBy", 
                            
                            //16-17
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 18);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmLegalDocVerifyUpdate(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmLegalDocVerifyUpdate(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkLoadingQueue_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Antrian");
        }

        private void TxtLoadingQueue_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
