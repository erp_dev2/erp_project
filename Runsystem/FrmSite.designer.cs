﻿namespace RunSystem
{
    partial class FrmSite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSite));
            this.label8 = new System.Windows.Forms.Label();
            this.TxtSiteName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkHOInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueProfitCenterCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.LblShortCode = new System.Windows.Forms.Label();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.LueSiteGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.LblCityCode = new System.Windows.Forms.Label();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.LueBranchCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHOInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBranchCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(855, 0);
            this.panel1.Size = new System.Drawing.Size(70, 328);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(855, 328);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(71, 99);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteName
            // 
            this.TxtSiteName.EnterMoveNextControl = true;
            this.TxtSiteName.Location = new System.Drawing.Point(126, 55);
            this.TxtSiteName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteName.Name = "TxtSiteName";
            this.TxtSiteName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSiteName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteName.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TxtSiteName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteName.Properties.AppearanceFocused.Options.UseFont = true;
            this.TxtSiteName.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteName.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtSiteName.Properties.MaxLength = 100;
            this.TxtSiteName.Size = new System.Drawing.Size(661, 20);
            this.TxtSiteName.TabIndex = 12;
            this.TxtSiteName.Validated += new System.EventHandler(this.TxtSiteName_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(58, 57);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 14);
            this.label15.TabIndex = 11;
            this.label15.Text = "Site Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(126, 34);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TxtSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.TxtSiteCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtSiteCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 16;
            this.TxtSiteCode.Size = new System.Drawing.Size(218, 20);
            this.TxtSiteCode.TabIndex = 10;
            this.TxtSiteCode.Validated += new System.EventHandler(this.TxtSiteCode_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(61, 36);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Site Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(74, 141);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 19;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(348, 33);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(59, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(126, 97);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 1000;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(950, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(661, 20);
            this.MeeAddress.TabIndex = 16;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(126, 139);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(950, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(661, 20);
            this.MeeRemark.TabIndex = 20;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // ChkHOInd
            // 
            this.ChkHOInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkHOInd.Location = new System.Drawing.Point(410, 33);
            this.ChkHOInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkHOInd.Name = "ChkHOInd";
            this.ChkHOInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkHOInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHOInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkHOInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkHOInd.Properties.Appearance.Options.UseFont = true;
            this.ChkHOInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHOInd.Properties.Caption = "Head Office";
            this.ChkHOInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHOInd.Size = new System.Drawing.Size(101, 22);
            this.ChkHOInd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(44, 120);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Profit Center";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProfitCenterCode
            // 
            this.LueProfitCenterCode.EnterMoveNextControl = true;
            this.LueProfitCenterCode.Location = new System.Drawing.Point(126, 118);
            this.LueProfitCenterCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfitCenterCode.Name = "LueProfitCenterCode";
            this.LueProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfitCenterCode.Properties.DropDownRows = 30;
            this.LueProfitCenterCode.Properties.MaxLength = 40;
            this.LueProfitCenterCode.Properties.NullText = "[Empty]";
            this.LueProfitCenterCode.Properties.PopupWidth = 950;
            this.LueProfitCenterCode.Size = new System.Drawing.Size(661, 20);
            this.LueProfitCenterCode.TabIndex = 18;
            this.LueProfitCenterCode.ToolTip = "F4 : Show/hide list";
            this.LueProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfitCenterCode.EditValueChanged += new System.EventHandler(this.LueProfitCenterCode_EditValueChanged);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(855, 328);
            this.Tc1.TabIndex = 8;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp4,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel4);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(849, 300);
            this.Tp1.Text = "General";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtShortCode);
            this.panel4.Controls.Add(this.LblShortCode);
            this.panel4.Controls.Add(this.TxtSiteCode);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.LueProfitCenterCode);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.ChkHOInd);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.TxtSiteName);
            this.panel4.Controls.Add(this.MeeAddress);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.ChkActInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(849, 300);
            this.panel4.TabIndex = 22;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(126, 76);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtShortCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TxtShortCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtShortCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.TxtShortCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtShortCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 16;
            this.TxtShortCode.Size = new System.Drawing.Size(155, 20);
            this.TxtShortCode.TabIndex = 14;
            // 
            // LblShortCode
            // 
            this.LblShortCode.AutoSize = true;
            this.LblShortCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShortCode.ForeColor = System.Drawing.Color.Black;
            this.LblShortCode.Location = new System.Drawing.Point(52, 79);
            this.LblShortCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblShortCode.Name = "LblShortCode";
            this.LblShortCode.Size = new System.Drawing.Size(69, 14);
            this.LblShortCode.TabIndex = 13;
            this.LblShortCode.Text = "Short Code";
            this.LblShortCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.LueSiteGroup);
            this.Tp4.Controls.Add(this.label1);
            this.Tp4.Controls.Add(this.BtnAcNo);
            this.Tp4.Controls.Add(this.LueCityCode);
            this.Tp4.Controls.Add(this.TxtAcDesc);
            this.Tp4.Controls.Add(this.LblCityCode);
            this.Tp4.Controls.Add(this.TxtAcNo);
            this.Tp4.Controls.Add(this.LblAcNo);
            this.Tp4.Margin = new System.Windows.Forms.Padding(2);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(849, 300);
            this.Tp4.Text = "Customized";
            // 
            // LueSiteGroup
            // 
            this.LueSiteGroup.EnterMoveNextControl = true;
            this.LueSiteGroup.Location = new System.Drawing.Point(101, 27);
            this.LueSiteGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteGroup.Name = "LueSiteGroup";
            this.LueSiteGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteGroup.Properties.Appearance.Options.UseFont = true;
            this.LueSiteGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteGroup.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueSiteGroup.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueSiteGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteGroup.Properties.DropDownRows = 30;
            this.LueSiteGroup.Properties.MaxLength = 40;
            this.LueSiteGroup.Properties.NullText = "[Empty]";
            this.LueSiteGroup.Properties.PopupWidth = 920;
            this.LueSiteGroup.Size = new System.Drawing.Size(638, 20);
            this.LueSiteGroup.TabIndex = 10;
            this.LueSiteGroup.ToolTip = "F4 : Show/hide list";
            this.LueSiteGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteGroup.EditValueChanged += new System.EventHandler(this.LueSiteGroup_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(22, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Site\'s Group";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(408, 70);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(13, 16);
            this.BtnAcNo.TabIndex = 15;
            this.BtnAcNo.ToolTip = "List of POS\'s COA Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(101, 48);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueCityCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.MaxLength = 40;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 920;
            this.LueCityCode.Size = new System.Drawing.Size(638, 20);
            this.LueCityCode.TabIndex = 12;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(101, 92);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TxtAcDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.TxtAcDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 16;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(638, 20);
            this.TxtAcDesc.TabIndex = 16;
            // 
            // LblCityCode
            // 
            this.LblCityCode.AutoSize = true;
            this.LblCityCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCityCode.ForeColor = System.Drawing.Color.Black;
            this.LblCityCode.Location = new System.Drawing.Point(67, 51);
            this.LblCityCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCityCode.Name = "LblCityCode";
            this.LblCityCode.Size = new System.Drawing.Size(27, 14);
            this.LblCityCode.TabIndex = 11;
            this.LblCityCode.Text = "City";
            this.LblCityCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(101, 70);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TxtAcNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.TxtAcNo.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.TxtAcNo.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 100;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(300, 20);
            this.TxtAcNo.TabIndex = 14;
            // 
            // LblAcNo
            // 
            this.LblAcNo.AutoSize = true;
            this.LblAcNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo.Location = new System.Drawing.Point(27, 71);
            this.LblAcNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo.Name = "LblAcNo";
            this.LblAcNo.Size = new System.Drawing.Size(67, 14);
            this.LblAcNo.TabIndex = 13;
            this.LblAcNo.Text = "POS COA#";
            this.LblAcNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel5);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(849, 300);
            this.Tp2.Text = "List of Site\'s Division";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Grd1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(849, 300);
            this.panel5.TabIndex = 22;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(849, 300);
            this.Grd1.TabIndex = 9;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.LueBranchCode);
            this.Tp3.Controls.Add(this.Grd2);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(849, 300);
            this.Tp3.Text = "List of Branch\'s POS COA";
            // 
            // LueBranchCode
            // 
            this.LueBranchCode.EnterMoveNextControl = true;
            this.LueBranchCode.Location = new System.Drawing.Point(93, 21);
            this.LueBranchCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBranchCode.Name = "LueBranchCode";
            this.LueBranchCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchCode.Properties.Appearance.Options.UseFont = true;
            this.LueBranchCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBranchCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBranchCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBranchCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBranchCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBranchCode.Properties.DropDownRows = 30;
            this.LueBranchCode.Properties.MaxLength = 40;
            this.LueBranchCode.Properties.NullText = "[Empty]";
            this.LueBranchCode.Properties.PopupWidth = 300;
            this.LueBranchCode.Size = new System.Drawing.Size(291, 20);
            this.LueBranchCode.TabIndex = 19;
            this.LueBranchCode.ToolTip = "F4 : Show/hide list";
            this.LueBranchCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBranchCode.EditValueChanged += new System.EventHandler(this.LueBranchCode_EditValueChanged);
            this.LueBranchCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBranchCode_KeyDown);
            this.LueBranchCode.Leave += new System.EventHandler(this.LueBranchCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(849, 300);
            this.Grd2.TabIndex = 10;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // FrmSite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 328);
            this.Name = "FrmSite";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHOInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            this.Tp4.ResumeLayout(false);
            this.Tp4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBranchCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.TextEdit TxtSiteName;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private DevExpress.XtraEditors.CheckEdit ChkHOInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueProfitCenterCode;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Label LblAcNo;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label LblShortCode;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.LookUpEdit LueBranchCode;
        private System.Windows.Forms.Label LblCityCode;
        internal DevExpress.XtraEditors.LookUpEdit LueCityCode;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteGroup;
        private System.Windows.Forms.Label label1;
    }
}