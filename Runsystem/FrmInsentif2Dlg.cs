﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInsentif2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInsentif2 mFrmParent;
        private string mSQL = string.Empty, mDt = string.Empty;
        
        #endregion

        #region Constructor

        public FrmInsentif2Dlg(FrmInsentif2 FrmParent, string Dt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDt = Dt;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code", 
                        "Department",
 
                        //6-10
                        "Position",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        "Identity#",
                        "Gender",
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");

            SQL.AppendLine("Select A.EmpCode,A.EmpName,B.UserName, A.EmpCodeOld, A.DeptCode, C.DeptName,D.PosName,A.JoinDt,A.ResignDt,A.IdNumber,E.OptDesc as Gender,F.OptDesc as Religion, ");
            SQL.AppendLine("A.BirthPlace,A.BirthDt,A.Address,G.CityName,A.PostalCode,A.Phone,A.Mobile,A.Email,A.NPWP,H.OptDesc as PayrollType,A.PTKP, ");
            SQL.AppendLine("I.BankName As BankName,J.BankAcNm As BankAcNm,A.BankBranch, ");
            SQL.AppendLine("A.CreateBy,A.CreateDt,A.LastUpBy,A.LastUpDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join tbluser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("left Join tbldepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join tblposition D On A.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join tbloption E On A.Gender=E.OptCode and E.OptCat='Gender' ");
            SQL.AppendLine("Left Join tbloption F On A.Religion=F.OptCode and F.OptCat='Religion' ");
            SQL.AppendLine("Left Join tblcity G On A.CityCode=G.CityCode ");
            SQL.AppendLine("Left Join tblOption H On A.PayrollType=H.OptCode and H.OptCat='EmployeePayrollType' ");
            SQL.AppendLine("Left Join tblBank I On A.BankCode=I.BankCode ");
            SQL.AppendLine("Left Join tblBankAccount J On A.BankAcNo=J.BankAcNo ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or ( ");
            SQL.AppendLine("    A.ResignDt Is Not Null And A.ResignDt>=@Dt ");
            SQL.AppendLine(")) ");
            if (!ChkEmp.Checked)
            {
                SQL.AppendLine("And A.PosCode In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter Where ParCode='CoordinatorPosCode' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(")X ");
           
            mSQL = SQL.ToString();            
        }

       

        override protected void ShowData()
        {
            try
            {
                SetSQL();
               
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@Dt", mDt);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "X.EmpCode", "X.EmpCodeOld", "X.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By X.EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName","EmpCodeOld","DeptName","PosName","JoinDt",
                            //6-10
                            "ResignDt","IdNumber","Gender",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;


             if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 10 });
                    }
                    mFrmParent.ShowEmpAmt();
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Event


        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
