﻿#region Update
/*
    30/05/2017 [HAR] tambah informasi hours meter
    14/09/2017 [HAR] perubahan input (bisa input asset lbh dari 1), bug fixing waktu save WOR
    20/09/2017 [HAR] bug fixing saat save ke WOR dan delete row
    26/09/2017 [HAR] bug fixing saat delete
    04/10/2017 [HAR] WO template
    12/10/2017 [HAR] WO template tidak ambil dari source
    16/10/2017 [TKG] tambah filter berdasarkan technical object
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmMaintenanceSchedule : RunSystem.FrmBase7
    {
        #region Field

        private string
            mMenuCode = string.Empty, mSQL = string.Empty,
            mMaintenanceScheduleWORSymptomProblem = string.Empty,
            mMaintenanceScheduleWORMaintenanceStatus = string.Empty,
            mMaintenanceScheduleWOBreakdownStatus = string.Empty,
            mMaintenanceScheduleWOActivity = string.Empty;
        private int mCol = -1;
        iGCell fCell;
        bool fAccept;
        private bool mIsWORApprovalBySiteMandatory = false;
        
        #endregion

        #region Constructor

        public FrmMaintenanceSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                DteDocDt1.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                SetLueLocCode(ref LueLocCode);
                LueDocNo.Visible = false;

                SetSQL();
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMaintenanceScheduleWORSymptomProblem = Sm.GetParameter("MaintenanceScheduleWORSymptomProblem");
            mMaintenanceScheduleWORMaintenanceStatus = Sm.GetParameter("MaintenanceScheduleWORMaintenanceStatus");
            mIsWORApprovalBySiteMandatory = Sm.GetParameter("IsWORApprovalBySiteMandatory") == "Y";
            mMaintenanceScheduleWOActivity = Sm.GetParameter("MaintenanceScheduleWOActivity");
            mMaintenanceScheduleWOBreakdownStatus = Sm.GetParameter("MaintenanceScheduleWOBreakdownStatus");
        }

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.AGName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.AGCode, T1.AGName, T2.EmpCode ");
            SQL.AppendLine("    From TblAttendanceGrpHdr T1 ");
            SQL.AppendLine("    Inner Join TblAttendanceGrpDtl T2 On T1.AGCode=T2.AGCode ");
            SQL.AppendLine("    Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And T3.DeptCode=@DeptCode ");
            SQL.AppendLine(Filter1.Replace("A.", "T3."));
            SQL.AppendLine("    Where T1.ActInd='Y' ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine(" Where 0=0 ");
            SQL.AppendLine(Filter1);
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        //1-5
                        "Dno",
                        "Asset Code",
                        "",
                        "Asset",
                        "Equipment",
                        //6-7
                        "Equipment",
                        "Hours Meter"
                    },
                     new int[] 
                    {
                        //0
                        20, 
                        //1-5
                        40, 100, 20, 200, 80, 
                        //6-7
                        200, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                ClearData();
                Grd1.Cols.Count = 8;

                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") || 
                    Sm.IsLueEmpty(LueLocCode, "Location")
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                ShowAssetFromMaintenanceSchedule();
                ShowDate();
                
                var l = new List<MaintenanceSchedule>();

                GetData(ref l);
                ShowData(ref l);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ClearData()
        {
            LueDocNo.EditValue = null;
            Grd1.Cols.Count = 8;
            Sm.ClearGrd(Grd1, true);
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsSaveDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var l = new List<MaintenanceSchedule>();
                var l2 = new List<MaintenanceSchedule>();

                GetData(ref l2);
                PrepareData(ref l);

                var cml = new List<MySqlCommand>();

                l.ForEach(i => { cml.Add(SaveMaintenanceSchedule(ref i)); });

                Sm.ExecCommands(cml);

                if (Sm.StdMsgYN("Question", "Do you want to automatic create WO request ?") == DialogResult.No)
                {
                    ClearData();
                    return;
                }
                else
                {
                    GenerateWORequest(ref l, ref l2);
                    Sm.StdMsg(mMsgType.Info, "Process completed.");
                }
                ClearData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void GenerateWORequest(ref List<MaintenanceSchedule> l, ref List<MaintenanceSchedule> l2)
        {
            for (var i = 0; i < l.Count; i++)
            {
                if (l[i].DocNo.Length > 0)
                {
                    l[i].IsGenerateWOR = true;

                    #region Old 
                    //for (var j = 0; j < l2.Count; j++)
                    //{
                    //    if (Sm.CompareStr(l[i].AssetCode, l2[j].AssetCode) &&
                    //        Sm.CompareStr(l[i].EquipCode, l2[j].EquipCode) &&
                    //        Sm.CompareStr(l[i].Dt, l2[j].Dt)&&
                    //        Sm.CompareStr(l[i].Dno, l2[j].Dno)
                    //        )
                    //    {
                    //        if (Sm.CompareStr(l[i].DocNo, l2[j].DocNo))
                    //        {
                    //            l[i].IsGenerateWOR = false;
                    //            break;
                    //        }
                    //        else
                    //            break;
                    //    }
                    //}
                    #endregion
                }
            }

            foreach (MaintenanceSchedule x in l.Where(x => x.IsGenerateWOR).OrderBy(x=>x.Dt))
            {
                GenerateWORequest2(x);
            }
        }

        private void GenerateWORequest2(MaintenanceSchedule ms) 
        {
            string DocNo = Sm.GenerateDocNo(ms.Dt, "WOR", "TblWOR");
            string WODocNo = Sm.GenerateDocNo(ms.Dt, "WO", "TblWOHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveWOR(DocNo, ms, WODocNo));
            
            Sm.ExecCommands(cml);
        }

        private MySqlCommand SaveWOR(string DocNo, MaintenanceSchedule ms, string WODocNo)
        {
            var lEmpBOM = new List<EmpBOM>();
            ProcessGenerateEmp(ref lEmpBOM, ms.DocNo);

            string DocApprovalWOR = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWOR ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, WOStatus, MtcStatus, MtcType, SymProblem, ToCode, Description, DownDt, DownTm, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', 'O', ");
            SQL.AppendLine("@MtcStatus, ");
            SQL.AppendLine("(Select MtcType From TblBOMMaintenanceHdr Where DocNo=@BOMMaintenanceDocNo), ");
            SQL.AppendLine("@SymProblem, @TOCode, ");
            SQL.AppendLine("@Description, @DownDt, ");
            SQL.AppendLine("(Select DownTm From TblBOMMaintenanceHdr Where DocNo=@BOMMaintenanceDocNo), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='WORequest' ");
            if (mIsWORApprovalBySiteMandatory)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblWOR A");
                SQL.AppendLine("    Inner Join TblTOHdr B On A.TOCOde=B.AssetCode ");
                SQL.AppendLine("    Inner Join TblLocation C On B.LocCode=C.LocCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And C.SiteCode=T.SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblWOR Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists(  ");
            SQL.AppendLine("    Select DocNo From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='WORequest' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine("    );  ");

            String DataExists = Sm.GetValue("Select UserCode from tblDocApprovalSetting Where DocType = 'WORequest'; ");

            if (DataExists.Length == 0)
            {
                SQL.AppendLine("Insert Into TblWOHdr ");
                SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, WORDocNo,  Remark, FixedInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @WODocNo, @DocDt, Null, 'N', @DocNo, 'From maintenance schedule', 'N', @CreateBy, CurrentDateTime(); ");

                SQL.AppendLine("Insert Into TblWODtl(DocNo, DNo, WOActivity, BreakdownStatus, Dt1, Tm1, Dt2, Tm2, Tm3, Remark, CreateBy, CreateDt)  ");
                SQL.AppendLine("Select @WODocNo, '001', @WOActivity, @BreakdownStatus, @Dt1, @Tm1, @Dt2, @Tm2, @Tm3, @Remark, @CreateBy, CurrentDateTime(); ");


                if (lEmpBOM.Count > 0)
                {
                    for (var j = 0; j < lEmpBOM.Count; j++)
                    {
                        SQL.AppendLine("Insert Into TblWODtl2(DocNo, DNo, DNo2, MechanicCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select @WODocNo, '001', '" + lEmpBOM[j].BOMDno + "', '" + lEmpBOM[j].EmpCode + "', @CreateBy, CurrentDateTime() ; ");
                    }
                }
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WODocNo", WODocNo);
            Sm.CmParamDt(ref cm, "@DocDt", ms.Dt);
            Sm.CmParam<String>(ref cm, "@BOMMaintenanceDocNo", ms.DocNo);
            Sm.CmParam<String>(ref cm, "@Dt1", ms.Dt);
            Sm.CmParam<String>(ref cm, "@Tm1", Sm.GetValue("Select DownTm From TblBOMMaintenanceHdr Where DocNo = '" + ms.DocNo + "' "));
            Sm.CmParam<String>(ref cm, "@Dt2", ms.Dt);
            Sm.CmParam<String>(ref cm, "@Tm2", Sm.GetValue("Select DownTm From TblBOMMaintenanceHdr Where DocNo = '" + ms.DocNo + "' "));
            Sm.CmParam<String>(ref cm, "@Tm3", "0000");
            Sm.CmParam<String>(ref cm, "@TOCode", ms.AssetCode);
            Sm.CmParam<String>(ref cm, "@MtcStatus", mMaintenanceScheduleWORMaintenanceStatus);
            Sm.CmParam<String>(ref cm, "@SymProblem", mMaintenanceScheduleWORSymptomProblem);
            Sm.CmParam<String>(ref cm, "@WOActivity", mMaintenanceScheduleWOActivity);
            Sm.CmParam<String>(ref cm, "@BreakdownStatus", mMaintenanceScheduleWOBreakdownStatus); 
            Sm.CmParam<String>(ref cm, "@Description", "Source : Maintenance Schedule");
            Sm.CmParamDt(ref cm, "@DownDt", ms.Dt);
            Sm.CmParam<String>(ref cm, "@Remark", "Routine");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private void ProcessGenerateEmp(ref List<EmpBOM> lEmpBOM, string BOMMainDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, Dno, EmpCode From TblBommaintenanceDtl2 Where DocNO=@BOMMaintenanceDocNo ");

            Sm.CmParam<String>(ref cm, "@BOMMaintenanceDocNo", BOMMainDocNo);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Dno", "EmpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEmpBOM.Add(new EmpBOM()
                        {
                            BOMMaintenanceDocNo = Sm.DrStr(dr, c[0]),
                            BOMDno = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private bool IsSaveDataNotValid()
        {
            return IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No asset selected.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveMaintenanceSchedule(ref MaintenanceSchedule i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaintenanceSchedule(DNo, AssetCode, EquipmentCode, Dt, DocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DNo, @AssetCode, @EquipmentCode, @Dt, @DocNo, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocNo=@DocNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DNo", i.Dno);
            Sm.CmParam<String>(ref cm, "@AssetCode", i.AssetCode);
            Sm.CmParam<String>(ref cm, "@EquipmentCode", i.EquipCode);
            Sm.CmParam<String>(ref cm, "@Dt", i.Dt);
            Sm.CmParam<String>(ref cm, "@DocNo", i.DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void PrepareData(ref List<MaintenanceSchedule> l)
        {
            int Col = 8;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    Grd1.Cols.Count > 8)
                {
                    Col = 8;
                    while (Col < Grd1.Cols.Count)
                    {
                        l.Add(new MaintenanceSchedule()
                        {
                            Dno = Sm.Right("00" + (Row + 1).ToString(), 3),
                            AssetCode = Sm.GetGrdStr(Grd1, Row, 2),
                            EquipCode = Sm.GetGrdStr(Grd1, Row, 5),
                            Dt = Grd1.Header.Cells[0, Col].Value.ToString(),
                            DocNo = Sm.GetGrdStr(Grd1, Row, Col),
                            IsGenerateWOR = false
                        });
                        Col += 2;
                    }
                }
            }
        }

        #endregion

        #region Additional Method

        private void SetLueLocCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select LocCode As Col1, LocName As Col2 ");
                SQL.AppendLine("From TblLocation ");
                SQL.AppendLine("Where LocCode In (Select Distinct LocCode From TblTOHdr) ");
                SQL.AppendLine("Order By LocName;");

                cm.CommandText = SQL.ToString();

                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDocNo(ref DXE.LookUpEdit Lue, string AssetCode, string EquipCode, string DocNo)
        {
            try
            {
                var SQL = new StringBuilder();
                var subSQL = new StringBuilder();
                var f = false;

                SQL.AppendLine("Select DocNo As Col1, BomName As Col2 ");
                SQL.AppendLine("From TblBOMMaintenanceHdr ");

                if (DocNo.Length > 0)
                {
                    subSQL.AppendLine("DocNo=@DocNo ");
                    f = true;
                }
                if (AssetCode.Length > 0 && EquipCode.Length > 0)
                {
                    if (f) subSQL.AppendLine(" Or ");
                    subSQL.AppendLine("(ActInd='Y' ");
                    subSQL.AppendLine("And AssetCode=@AssetCode ");
                    subSQL.AppendLine("And EquipmentCode=@EquipmentCode) ");
                    f = true;
                }

                //tidak menampilkan data
                if (f)
                    SQL.AppendLine(" Where (" + subSQL.ToString() + ") ");
                else
                    SQL.AppendLine(" Where  1=0 ");

                SQL.AppendLine("Order By BomName;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (AssetCode.Length > 0 && EquipCode.Length > 0)
                {
                    Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
                    Sm.CmParam<String>(ref cm, "@EquipmentCode", EquipCode);
                }
                if (DocNo.Length > 0)
                {
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                }
                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowAssetFromMaintenanceSchedule()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = " ";

            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetLue(LueLocCode));
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtTOCode.Text, "X.AssetName", false);

            SQL.AppendLine("Select A.Dno, A.AssetCode, B.AssetName, A.EquipmentCode, F.EquipmentName, ifnull(E.HoursMeter, 0) As HoursMeter ");
            SQL.AppendLine("From TblmaintenanceSchedule A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode " + Filter.Replace("X.", "B."));
            SQL.AppendLine("Inner Join TblTOhdr C On A.AssetCode = C.AssetCode ");
            SQL.AppendLine("Inner Join TblTODtl D On C.AssetCode = D.AssetCode And A.EquipmentCode = D.EquipmentCode ");
            SQL.AppendLine("Left Join TblHoursMeter E On C.HmDOcNo = E.DocNo  ");
            SQL.AppendLine("Left Join TblEquipment F On D.EquipmentCode=F.EquipmentCode ");
            SQL.AppendLine("Where C.LocCode = @LocCode And A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("Group by A.AssetCode, A.EquipmentCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select Distinct 0 Dno, A.AssetCode, C.AssetName, B.EquipmentCode, D.EquipmentName, ifnull(E.HoursMeter, 0) As HoursMeter ");
            SQL.AppendLine("From TblTOHdr A ");
            SQL.AppendLine("Inner Join TblTODtl B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Inner Join TblAsset C On A.AssetCode=C.AssetCode " + Filter.Replace("X.", "C."));
            SQL.AppendLine("Inner Join TblEquipment D On B.EquipmentCode=D.EquipmentCode ");
            SQL.AppendLine("Left Join TblHoursMeter E On A.HmDOcNo = E.DocNo ");
            SQL.AppendLine("Where A.LocCode=@LocCode ");
            SQL.AppendLine("And A.AssetCode Not In (Select AssetCode From tblMaintenanceSchedule Where Dt Between @Dt1 And @Dt2) ");
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString()+" Order by Dno;",
                    new string[] 
                    { 
                        "DNo",  
                        "AssetCode", "AssetName", "EquipmentCode", "EquipmentName", "HoursMeter" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    }, true, false, false, false
                );
            if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0) Grd1.Rows.Add();
        }

        private void ShowDate()
        {
            var DtMin = Sm.GetDte(DteDocDt1);
            var DtMax = Sm.GetDte(DteDocDt2);

            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 0; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                Grd1.Cols.Count = 8 + ((TotalDays + 1) * 2);
                int Col = 8;

                Grd1.BeginUpdate();
                s.ForEach(i =>
                {
                    Grd1.Header.Cells[0, Col].Value = i;
                    Grd1.Cols[Col].Width = 100;
                    Grd1.Cols[Col].Visible = false;
                    Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.MiddleCenter;

                    Grd1.Header.Cells[0, Col + 1].Value =
                        Sm.Right(i, 2) + "-" +
                        CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(
                            int.Parse(i.Substring(4, 2))
                            ) + "-" +
                        Sm.Left(i, 4) + Environment.NewLine +
                        TempDt.DayOfWeek.ToString();
                    Grd1.Cols[Col+1].Width = 100;
                    Grd1.Header.Cells[0, Col+1].TextAlign = iGContentAlignment.MiddleCenter;

                    //if ((int)TempDt.DayOfWeek==0)
                    //{
                    //    for (int r = 0; r < Grd1.Rows.Count; r++)
                    //        Grd1.Cells[r, Col + 1].BackColor = Color.LightSalmon;
                    //}

                    TempDt = TempDt.AddDays(1);
                    Col += 2;
                });
                Grd1.EndUpdate();
            }
        }

        //dapetin data asset dan equip yang sdh muncul di grid
        private void GetData(ref List<MaintenanceSchedule> l)
        {
            string Filter = string.Empty, AssetCode = string.Empty, EquipCode = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Grd1.Rows.Count != 1)
            {
                for (int r = 0; r < Grd1.Rows.Count-1; r++)
                {
                    AssetCode = Sm.GetGrdStr(Grd1, r, 2);
                    EquipCode = Sm.GetGrdStr(Grd1, r, 5);
                    if (AssetCode.Length != 0 && EquipCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.AssetCode=@AssetCode0" + r.ToString() + " And A.EquipmentCode=@EquipCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@AssetCode0" + r.ToString(), AssetCode);
                        Sm.CmParam<String>(ref cm, "@EquipCode0" + r.ToString(), EquipCode);
                    }
                }
            }

            if (Filter.Length != 0)
                Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select A.Dno, A.AssetCode, A.Dno, A.EquipmentCode, A.Dt, A.DocNo, B.BomName ");
            SQL.AppendLine("From TblMaintenanceSchedule A, TblBomMaintenanceHdr B ");
            SQL.AppendLine("Where A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.AssetCode, A.EquipmentCode, A.Dt;");

            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Dno", "AssetCode", "EquipmentCode", "Dt", "DocNo", "BomName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MaintenanceSchedule()
                        {
                            Dno = Sm.DrStr(dr, c[0]),
                            AssetCode = Sm.DrStr(dr, c[1]),
                            EquipCode = Sm.DrStr(dr, c[2]),
                            Dt = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            BomName = Sm.DrStr(dr, c[5]),
                            IsGenerateWOR = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowData(ref List<MaintenanceSchedule> l)
        {
            if (Grd1.Cols.Count <= 8) return;
            string AssetCode = string.Empty, EquipCode = string.Empty, Dno = string.Empty;
            int r = 0;
            int c = 8;
            Grd1.BeginUpdate();
            l.ForEach(i =>
            {
                if (!(Sm.CompareStr(AssetCode, i.AssetCode) && Sm.CompareStr(EquipCode, i.EquipCode) && Sm.CompareStr(Dno, i.Dno)))
                {
                    AssetCode = i.AssetCode;
                    EquipCode = i.EquipCode;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2), i.AssetCode) && 
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), i.EquipCode) 
                            //&& Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), i.Dno)
                            )
                        {
                            r = Row;
                            break;
                        }
                    }
                }
                if (Grd1.Rows.Count > 0)
                {
                    c = 8;
                }
                else
                    c = 7;

                while (c < Grd1.Cols.Count)
                {
                    if (Sm.CompareStr(i.Dt, Grd1.Header.Cells[0, c].Value.ToString().Substring(0, 8)))
                    {
                        Grd1.Cells[r, c].Value = i.DocNo;
                        Grd1.Cells[r, c + 1].Value = i.BomName;
                        break;
                    }
                    c = c + 2;
                }
            });
            Grd1.EndUpdate();
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, mCol-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedAssetCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            DteDocDt2.EditValue = DteDocDt1.EditValue;
            ClearData();
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            ClearData();
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            ClearData();
        }

        private void LueDocNo_EditValueChanged(object sender, EventArgs e)
        {
            string AssetCode = Sm.GetGrdStr(Grd1, fCell.RowIndex, 2);
            string EquipCode = Sm.GetGrdStr(Grd1, fCell.RowIndex, 5);
            string DocNo = Sm.GetGrdStr(Grd1, fCell.RowIndex, mCol-1);

            Sm.RefreshLookUpEdit(LueDocNo, new Sm.RefreshLue4(SetLueDocNo), AssetCode, EquipCode, DocNo);
        }

        private void LueDocNo_Leave(object sender, EventArgs e)
        {
            if (LueDocNo.Visible && fAccept)
            {
                //Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Black;
                if (Sm.GetLue(LueDocNo).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, mCol - 1].Value = null;
                    Grd1.Cells[fCell.RowIndex, mCol].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, mCol - 1].Value = Sm.GetLue(LueDocNo);
                    Grd1.Cells[fCell.RowIndex, mCol].Value = LueDocNo.GetColumnValue("Col2");
                    //if (LueWSCode.GetColumnValue("Col3").ToString() == "Y") Grd1.Cells[fCell.RowIndex, mCol].ForeColor = Color.Red;
                }
                LueDocNo.Visible = false;
            }
        }

        private void LueDocNo_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void TxtTOCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTOCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Technical Object");
        }

        #endregion

        #region Grid Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length >0)
            {
                var f = new FrmTO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmMaintenanceScheduleDlg(this, Sm.GetLue(LueLocCode)));

           
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmMaintenanceScheduleDlg(this, Sm.GetLue(LueLocCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                var f = new FrmTO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0 && e.ColIndex > 7)
            {
                mCol = e.ColIndex;
                LueRequestEdit(Grd1, LueDocNo, ref fCell, ref fAccept, e);
                Sm.SetLue(LueDocNo, "<Refresh>");
                string AssetCode = Sm.GetGrdStr(Grd1, fCell.RowIndex, 2);
                string EquipCode = Sm.GetGrdStr(Grd1, fCell.RowIndex, 5);
                string DocNo = Sm.GetGrdStr(Grd1, fCell.RowIndex, mCol - 1);
                Sm.RefreshLookUpEdit(LueDocNo, new Sm.RefreshLue4(SetLueDocNo), AssetCode, EquipCode, DocNo);
                Sm.SetLue(LueDocNo, DocNo);
            }
            else
                e.DoDefault = false;
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Grd1.CurCell != null && 
                Grd1.CurCell.RowIndex != Grd1.Rows.Count - 1)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.FocusGrd(Grd1, 0, 7);           
        }

        #endregion

        #region Button Event

        private void BtnClearData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to clear all schedule ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    for (int Col = 8; Col < Grd1.Cols.Count; Col++)
                    {
                        Grd1.Cells[Row, Col].Value = null;
                        Grd1.Cells[Row, Col].ForeColor = Color.Black;
                    }
                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy data ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1 || Sm.GetGrdStr(Grd1, 0, 0).Length==0) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 1; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        for (int Col = 8; Col < Grd1.Cols.Count; Col++)
                        {
                            Grd1.Cells[Row, Col].Value = Grd1.Cells[0, Col].Value;
                            //Grd1.Cells[Row, Col].ForeColor = Grd1.Cells[0, Col].ForeColor;
                        }
                    }
                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }

        }

        private void BtnCopyPattern_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to copy pattern ?") == DialogResult.No) return;

            if (Grd1.Rows.Count <= 1) return;

            try
            {
                Grd1.BeginUpdate();

                for (int Row = 0; Row <= Grd1.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) CopyPattern(Row);
                
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void CopyPattern(int Row)
        {
            int TempCol = -1;
            for (int Col = 8; Col < Grd1.Cols.Count; Col++)
                if (Sm.GetGrdStr(Grd1, Row, Col).Length != 0) TempCol = Col;

            if (TempCol == -1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern existed.");
                return;
            }

            if (TempCol == Grd1.Cols.Count - 1)
            {
                //Sm.StdMsg(mMsgType.Warning, "No pattern copied.");
                return;
            }

            var l = new List<MaintenaceSchedulePattern>();

            for (int Col = 8; Col <= TempCol; Col++)
                l.Add(new MaintenaceSchedulePattern()
                {
                    Pattern = Sm.GetGrdStr(Grd1, Row, Col),
                    //PatternColor = Grd1.Cells[Row, Col].ForeColor
                });

            var PatternCount = l.Count;
            int PatternIndex = 0;
            for (int Col = TempCol + 1; Col < Grd1.Cols.Count; Col++)
            {
                Grd1.Cells[Row, Col].Value = l[PatternIndex].Pattern;
                //Grd1.Cells[Row, Col].ForeColor = l[PatternIndex].PatternColor;
                if (PatternIndex == PatternCount - 1)
                    PatternIndex = 0;
                else
                    PatternIndex += 1;
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }


        #endregion

        #endregion

        #region Class

        private class MaintenanceSchedule
        {
            public string Dno { get; set; }
            public string AssetCode { get; set; }
            public string EquipCode { get; set; }
            public string Dt { get; set; }
            public string DocNo { get; set; }
            public string BomName { get; set; }
            public bool IsGenerateWOR { get; set; }
        }

        private class MaintenaceSchedulePattern
        {
            public string Pattern { get; set; }
            //public Color PatternColor { get; set; }
        }

        private class EmpBOM
        {
            public string BOMMaintenanceDocNo { get; set; }
            public string BOMDno { get; set; }
            public string EmpCode { get; set; }
        }

        private class DtlWO
        {
            public string WODocNo { get; set; }
            public string WODno { get; set; }
        }

        #endregion
    }
}
