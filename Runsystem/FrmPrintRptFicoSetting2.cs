﻿#region Update
/*
    02/03/2020 [WED/KBN] new apps
    01/04/2020 [WED/KBN] perbaikan performance
    02/04/2020 [WED/YK] tambah bisa pilih COA
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting2 : RunSystem.FrmBase5
    {
        #region Field

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mCOALevelFicoSettingJournalToCBP = string.Empty;
        private bool mIsEntityMandatory = false;
        iGCell fCell;
        bool fAccept;
        internal List<COA> l = null;
        internal List<ColumnHeader> l2 = null;
        internal List<COAColumnAmount> l3 = null;
        internal List<COARow> l4 = null;
        private int mMaxLevelCOA = 0;
        
        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnRefresh.Visible = BtnSave.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                GetParameter();

                SetGrd();
                Grd1.Rows.Add();
                Sl.SetLueOption(ref LueType, "FicoSetting2Type");
                SetLueEntCode(ref LueEntCode);
                if (!mIsEntityMandatory) LblEntCode.ForeColor = Color.Black;
                LueType.Visible = false;

                l4 = new List<COARow>();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mCOALevelFicoSettingJournalToCBP = Sm.GetParameter("COALevelFicoSettingJournalToCBP");
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Column Name",
                    "Type Code", 
                    "Type",
                    "Start Date",
                    "End Date",

                    //6-9
                    "",
                    "List of COA#",
                    "",
                    "Formula"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 0, 130, 100, 100, 

                    //6-9
                    20, 200, 20, 500
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 6, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9 }, false);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 7, 9 }, e.ColIndex))
            {
                if (e.ColIndex == 3) LueRequestEdit(Grd1, LueType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 4) Sm.DteRequestEdit(Grd1, DteStartDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 5) Sm.DteRequestEdit(Grd1, DteEndDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                GenerateGrdNo();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                Sm.FormShowDialog(new FrmPrintRptFicoSetting2Dlg2(this, e.RowIndex));
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length > 0)
            {
                Sm.FormShowDialog(new FrmPrintRptFicoSetting2Dlg3(this, e.RowIndex));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            int Row = Grd1.SelectedRows.Count;
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnProcess);
            GenerateGrdNo();
            if ((Grd1.Rows.Count - 1) != l4.Count()) l4.RemoveAt(Row);
        }

        private void Grd1_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                e.Text = "Select COA";
            }
            
            if (e.ColIndex == 8)
            {
                e.Text = "Show Selected COA";
                e.GetType();
            }
        }

        #endregion

        #region Additional Methods

        internal void GetCOAList(int Row, string AcNo)
        {
            if (l4.Count == 0) l4.Add(new COARow() { AcNo = AcNo });
            else
            {
                if (l4.Count() <= Row) l4.Add(new COARow() { AcNo = AcNo });
                else l4[Row].AcNo = AcNo;
            }

            Grd1.Cells[Row, 7].Value = "[Collection]";
        }

        private void SetLueEntCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y';");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GenerateGrdNo()
        {
            if(Grd1.Rows.Count > 1)
            {
                int No = 1;
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    Grd1.Cells[i, 0].Value = No;
                    No += 1;
                }
            }
        }

        private bool IsProcessedDataInvalid()
        {
            return
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 column formula.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.IsGrdValueEmpty(Grd1, i, 1, false, "Column name is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, i, 3, false, "Type is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, i, 4, false, "Start Date is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, i, 5, false, "End Date is empty.")) return true;
                if (Decimal.Parse(Convert.ToDateTime(Sm.GetGrdStr(Grd1, i, 4)).ToString("yyyyMMdd")) > Decimal.Parse(Convert.ToDateTime(Sm.GetGrdStr(Grd1, i, 5)).ToString("yyyyMMdd")))
                {
                    Sm.StdMsg(mMsgType.Warning, "Start date should not greater than end date.");
                    Sm.FocusGrd(Grd1, i, 4);
                    return true;
                }
                if (Sm.IsGrdValueEmpty(Grd1, i, 7, false, "COA is empty.")) return true;
            }

            return false;
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void Process1(ref List<COA> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string sSQL = string.Empty, sSQL2 = string.Empty, AcNoLists = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (AcNoLists.Length > 0) AcNoLists += ",";
                AcNoLists += l4[i].AcNo; //Sm.GetGrdStr(Grd1, i, 7);
            }

            sSQL += "Select A.AcNo, A.AcDesc, A.Alias, A.Parent, A.Level, If(A.Parent Is Null, 'Y', 'N') HasChild ";

            sSQL2 += "Select Max(A.Level) ";
            
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Where Find_In_Set(A.AcNo, '"+AcNoLists+"') ");
            SQL.AppendLine("And A.ActInd = 'Y' ");

            mMaxLevelCOA = Int32.Parse(Sm.GetValue(string.Concat(sSQL2, SQL.ToString())));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = sSQL + SQL.ToString() + " Order By A.AcNo; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Alias", "Parent", "Level", "HasChild" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Alias = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrInt(dr, c[4]),
                            HasChild = Sm.DrStr(dr, c[5]) == "Y" ? true : false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ColumnHeader> l2, string No, string ColumnName)
        {
            l2.Add(new ColumnHeader()
            {
                No = No, ColumnName = ColumnName
            });
        }

        #region Journal

        private void ProcessJournal(ref List<COAColumnAmount> l3, ref List<COA> l, string No, string StartDt, string EndDt, string AcNo)
        {
            var lj = new List<Journal>();
            var ljc = new List<JournalCOA>();

            PrepJournal(ref lj, No, AcNo);
            PrepJournalCOA(ref ljc, StartDt, EndDt);
            if (ljc.Count > 0)
            {
                ProcessJournalCOA(ref ljc);
                ljc.RemoveAll(t => t.Amt == 0m);

                foreach (var x in lj.OrderBy(y => y.AcNo))
                {
                    foreach (var z in ljc.Where(b => b.AcNo == x.AcNo))
                    {
                        x.Amt = z.Amt;
                        z.IsDone = true;
                        break;
                    }
                }

                lj.RemoveAll(t => t.Amt == 0m);

                foreach (var x in lj.OrderBy(y => y.AcNo))
                {
                    l3.Add(new COAColumnAmount()
                    {
                        No = x.No,
                        AcNo = x.AcNo,
                        Amt = x.Amt
                    });
                }
            }

            lj.Clear(); ljc.Clear();
        }

        private void PrepJournal(ref List<Journal> lj, string No, string AcNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Parent, Level ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, @AcNo); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent", "Level" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lj.Add(new Journal()
                        {
                            No = No,
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            Amt = 0m,
                            IsDone = false
                        });
                    }
                }
                dr.Close();
            }          
        }

        private void PrepJournalCOA(ref List<JournalCOA> ljc, string StartDt, string EndDt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T1.AcNo, T1.Parent, T1.Level, IfNull(T2.Amt, 0.00) Amt ");
            SQL.AppendLine("From (Select AcNo, Parent, Level From TblCOA Where ActInd = 'Y') T1 ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.AcNo, Sum(IfNull(If(C.AcType = 'C', B.CAmt, B.DAmt), 0.00)) Amt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.DocDt Between '" + StartDt + "' And '" + EndDt + "' ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And B.DAmt != 0 Or B.CAmt != 0 ");
            SQL.AppendLine("    Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("        And C.ActInd = 'Y' ");
            SQL.AppendLine("    Group By B.AcNo ");
            SQL.AppendLine("    HAVING Sum(IfNull(If(C.AcType = 'C', B.CAmt, B.DAmt), 0.00)) != 0.00 ");
            SQL.AppendLine(") T2 On T1.AcNo = T2.AcNo ");
            SQL.AppendLine("Order By T1.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent", "Level", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ljc.Add(new JournalCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrInt(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            IsDone = false
                        });
                    }
                    //int index = 0;
                    //while (dr.Read())
                    //{
                    //    for (int i = index; i < lj.Count(); ++i)
                    //    {
                    //        if (Sm.DrStr(dr, c[0]) == lj[i].AcNo)
                    //        {
                    //            lj[i].Amt = Sm.DrDec(dr, c[1]);
                    //            index = i;
                    //            break;
                    //        }
                    //    }
                    //}
                }
                dr.Close();
            }
        }

        private void ProcessJournalCOA(ref List<JournalCOA> ljc)
        {
            string mParent = string.Empty;
            decimal mAmt = 0m;
            mMaxLevelCOA = Int32.Parse(Sm.GetValue("Select IfNull(Max(Level), 0) From TblCOA Where ActInd = 'Y'; "));

            for (int i = mMaxLevelCOA; i > 1; --i)
            {
                mParent = string.Empty; mAmt = 0m;

                foreach (var o in ljc.OrderBy(p => p.AcNo).Where(q => q.Level == i).Where(q => q.IsDone == false))
                {
                    if (mParent.Length == 0) mParent = o.Parent;

                    if (mParent == o.Parent)
                    {
                        if (o.Amt != 0m) mAmt += o.Amt;
                        o.IsDone = true;
                    }
                    else
                    {
                        if (mAmt != 0m)
                        {
                            foreach (var q in ljc.Where(s => s.AcNo == mParent))
                            {
                                q.Amt = mAmt;
                                break;
                            }
                        }

                        mAmt = o.Amt;
                        mParent = o.Parent;
                    }
                }
            }
        }

        #endregion

        #region RKAP

        private void ProcessRKAP(ref List<COAColumnAmount> l, string No, string StartDt, string EndDt, string AcNo)
        {
            int mYr = Int32.Parse(Sm.Left(StartDt, 4)), mMth = Int32.Parse(StartDt.Substring(4, 2));

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.No, T.AcNo, Sum(T.Amt) Amt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            while (Decimal.Parse(string.Concat(mYr.ToString(), Sm.Right(string.Concat("00", mMth.ToString()), 2))) <= Decimal.Parse(string.Concat(Sm.Left(EndDt, 4), EndDt.Substring(4, 2))))
            {
                SQL.AppendLine("Select '" + No + "' As `No`, B.AcNo, B.Amt" + Sm.Right(string.Concat("00", mMth.ToString()), 2) + " As Amt ");
                SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
                SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
                SQL.AppendLine("    And B.Amt" + Sm.Right(string.Concat("00", mMth.ToString()), 2) + " != 0.00 ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode).ToUpper() != "CONSOLIDATE")
                    SQL.AppendLine("    And A.EntCode = @EntCode ");
                SQL.AppendLine("    And A.Yr = '" + mYr.ToString() + "' ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");

                if (mMth == 12)
                {
                    mMth = 1; mYr += 1;
                }
                else
                {
                    mMth += 1;
                }

                if (Decimal.Parse(string.Concat(mYr.ToString(), Sm.Right(string.Concat("00", mMth.ToString()), 2))) <= Decimal.Parse(string.Concat(Sm.Left(EndDt, 4), EndDt.Substring(4, 2))))
                    SQL.AppendLine("Union All ");
            }
            
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.No, T.AcNo ");
            SQL.AppendLine("Having Sum(T.Amt) != 0.00; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "No", "AcNo", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAColumnAmount()
                        {
                            No = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if(IsProcessedDataInvalid()) return;

            l = new List<COA>();
            l2 = new List<ColumnHeader>();
            l3 = new List<COAColumnAmount>();

            string mNo = string.Empty, mColumnName = string.Empty, mType = string.Empty, mStartDt = string.Empty, mEndDt = string.Empty, mAcNo = string.Empty;

            Process1(ref l);
            if (l.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    mNo = Sm.GetGrdStr(Grd1, i, 0);
                    mColumnName = Sm.GetGrdStr(Grd1, i, 1);
                    mType = Sm.GetGrdStr(Grd1, i , 2);
                    mStartDt = Sm.Left(Sm.GetGrdDate(Grd1, i, 4), 8);
                    mEndDt = Sm.Left(Sm.GetGrdDate(Grd1, i, 5), 8);
                    mAcNo = l4[i].AcNo; //Sm.GetGrdStr(Grd1, i , 7);

                    Process2(ref l2, mNo, mColumnName);

                    if (mType == "1") // Journal
                    {
                        ProcessJournal(ref l3, ref l, mNo, mStartDt, mEndDt, mAcNo);
                    }
                    else if (mType == "2") // RKAP
                    {
                        ProcessRKAP(ref l3, mNo, mStartDt, mEndDt, mAcNo);
                    }
                }

                Sm.FormShowDialog(new FrmPrintRptFicoSetting2Dlg(this));
            }
            else
                Sm.StdMsg(mMsgType.NoData, string.Empty);

            l.Clear(); l2.Clear(); l3.Clear();
        }

        private void BtnCopyCOA_Click(object sender, EventArgs e)
        {
            if (Grd1.Rows.Count > 1)
            {
                if (l4.Count > 1)
                {
                    l4.RemoveRange(1, l4.Count - 1);
                }

                string mAcNo = Sm.GetGrdStr(Grd1, 0, 7);
                for (int i = 1; i < Grd1.Rows.Count - 1; ++i)
                {
                    l4.Add(new COARow() { AcNo = l4[0].AcNo });
                    Grd1.Cells[i, 7].Value = mAcNo;
                }
            }
        }

        #endregion

        #region Misc Control Events

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "FicoSetting2Type");
        }

        private void LueType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueType);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueType.GetColumnValue("Col2");
                }
                LueType.Visible = false;
            }
        }

        #endregion

        #endregion

        #region Class

        internal class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Alias { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
        }

        internal class ColumnHeader
        {
            public string No { get; set; }
            public string ColumnName { get; set; }
        }

        internal class COAColumnAmount
        {
            public string No { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class Journal
        {
            public string No { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
            public int Level { get; set; }
            public string Parent { get; set; }
            public bool IsDone { get; set; }
        }

        private class JournalCOA
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
            public int Level { get; set; }
            public string Parent { get; set; }
            public bool IsDone { get; set; }
        }

        internal class COARow
        {
            public string AcNo { get; set; }
        }

        #endregion

    }
}
