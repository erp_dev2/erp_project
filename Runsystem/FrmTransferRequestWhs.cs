﻿#region Update
/*
    18/05/2017 [TKG] stock yg bisa diambil adalah aktual stok dikurangi tr yg belum di-do dikurangi tr yg sudah di-do tapi belum di-received
    18/05/2017 [ARI] jika belum di approve, tidak bisa print.
    23/05/2018 [TKG] tambah local document#
    17/01/2019 [TKG] konversi quantity berdasarkan formula secara otomatis utk uom3
    04/11/2019 [DITA/IMS] tambah informasi Specifications & ItCodeInternal
    13/12/2019 [VIN+DITA/IMS] Printout Transfer Request
    07/02/2020 [TKG/IMS] tambah informasi project
    30/03/2020 [IBL/IMS] tambah informasi spesifikasi barang
    08/05/2020 [IBL/IMS] tambah informasi SO Contract#
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestWhs : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty, mAccessInd = string.Empty,
           mDocNo = string.Empty, //if this application is called from other application;
           mPGCode = string.Empty; 
        internal FrmTransferRequestWhsFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsSystemUseCostCenter = false;
        internal bool 
            mIsTransferRequestWhsProjectEnabled = false,
            mIsItGrpCodeShow = false,
            mIsBOMShowSpecifications= false;
        #endregion

        #region Constructor

        public FrmTransferRequestWhs(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Warehouse's Transfer Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Tp2.PageVisible = mIsTransferRequestWhsProjectEnabled;
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "",
                        "Item's Code",
                        "Item's Name",
                        "Stock",

                        //6-10
                        "Requested",
                        "UoM",
                        "Stock",
                        "Requested",
                        "UoM",

                        //11-15
                        "Stock",
                        "Requested",
                        "UoM",
                        "Remark",
                        "Cost Category",

                        //16-18
                        "Group",
                        "Local Code",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 20, 100, 200, 100, 

                        //6-10
                        100, 80, 100, 100, 80, 

                        //11-15
                        100, 100, 80, 200, 150, 
 
                        //16-18
                        150, 180, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 8, 9, 11, 12 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 9, 10, 11, 12, 13, 17 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3, 4, 5, 7, 8, 10, 11, 13, 15, 16, 17, 18 });

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(5);
            }
            ShowInventoryUomCode();
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 18 });
            Grd1.Cols[17].Move(4);
            Grd1.Cols[18].Move(6);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 17 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length == 0))
            {
                if (mNumberOfInventoryUomCode == 2)
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                if (mNumberOfInventoryUomCode == 3)
                    Sm.GrdColInvisible(Grd1, new int[] { 13, 15 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueWhsCode2, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9, 12, 14 });
                    Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 11 }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnPGCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode, LueWhsCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {  6, 9, 12, 14 });
                    Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 8, 11 }, true);
                    BtnPGCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueWhsCode2, 
                TxtStatus, MeeRemark, TxtProjectCode, TxtProjectName, TxtSOCDocNo
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 8, 9, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTransferRequestWhsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

         override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked==false)
            {
                string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

                if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || (Sm.GetParameter("DocTitle") == "MSI" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                string[] TableName = { "TransferReqHdr", "TransferReqHdr2", "TransferReqHdr3", "TransferReqDtl" };
                string mDocTitle = Sm.GetParameter("DocTitle");
                var l = new List<TransferReqHdr>();
                var l1 = new List<TransferReqHdr2>();
                var l2 = new List<TransferReqHdr3>();
                var ldtl = new List<TransferReqDtl>();

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
                SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName,  B1.WhsName As WhsName2, F.UserName As DoReqCreateBy, F1.UserName As DoToDeptCreateby, ");
                SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') As EmpPict, ");
                SQL.AppendLine("Concat(IfNull(G.ParValue, ''), F1.UserCode, '.JPG') As EmpPict2, A.Remark, NULL AS ProjectCode, H.ProjectName AS ProjectName, NULL AS Workshop  ");
                SQL.AppendLine("From TblTransferRequestWhsHdr A ");
                SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode  ");
                SQL.AppendLine("Inner Join TblWarehouse B1 On A.WhsCode2=B1.WhsCode ");
                SQL.AppendLine("Inner Join tbluser F On A.CreateBy=F.UserCode  ");
                SQL.AppendLine("Inner Join tbluser F1 On A.CreateBy=F1.UserCode ");
                SQL.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("Left Join TblProjectGroup H On A.PGCode=H.PGCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "Company",
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",

                         //6-10
                         "WhsName",
                         "WhsName2",
                         "DoReqCreateBy",
                         "DoToDeptCreateby",
                         "EmpPict",

                         //11-14
                         "EmpPict2",
                         "Remark",
                         "ProjectCode",
                         "ProjectName",
                         "WorkShop"
                         
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new TransferReqHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                WhsName = Sm.DrStr(dr, c[6]),
                                WhsName2 = Sm.DrStr(dr, c[7]),
                                DoReqCreateBy = Sm.DrStr(dr, c[8]),
                                DoToDeptCreateby = Sm.DrStr(dr, c[9]),
                                EmpPict = Sm.DrStr(dr, c[10]),

                                EmpPict2 = Sm.DrStr(dr, c[11]),
                                Remark = Sm.DrStr(dr, c[12]),
                                ProjectCode = Sm.DrStr(dr, c[13]),
                                ProjectName = Sm.DrStr(dr, c[14]),
                                Workshop = Sm.DrStr(dr, c[15]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                PrintDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }

                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Header2
                var cm1 = new MySqlCommand();
                var SQL1 = new StringBuilder();

                SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
                SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL1.AppendLine("from TblDocApproval A ");
                SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
                SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL1.AppendLine("Where DocType = 'TransferRequestWhs' ");
                SQL1.AppendLine("And DocNo =@DocNo ");
                SQL1.AppendLine("Group by ApprovalDno limit 1");

                using (var cn1 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn1.Open();
                    cm1.Connection = cn1;
                    cm1.CommandText = SQL1.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                    var dr1 = cm1.ExecuteReader();
                    var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"

                        
                        });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            l1.Add(new TransferReqHdr2()
                            {
                                ApprovalDno = Sm.DrStr(dr1, c1[0]),
                                UserCode = Sm.DrStr(dr1, c1[1]),
                                UserName = Sm.DrStr(dr1, c1[2]),
                                EmpPict = Sm.DrStr(dr1, c1[3]),
                            });
                        }
                    }

                    dr1.Close();
                }
                myLists.Add(l1);
                #endregion

                #region Header3
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();

                SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
                SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL2.AppendLine("from TblDocApproval A ");
                SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
                SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL2.AppendLine("Where DocType = 'TransferRequestWhs' ");
                SQL2.AppendLine("And DocNo =@DocNo ");
                SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-3
                         "UserCode",
                         "UserName",
                         "EmpPict"

                        
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new TransferReqHdr3()
                            {
                                ApprovalDno = Sm.DrStr(dr2, c2[0]),
                                UserCode = Sm.DrStr(dr2, c2[1]),
                                UserName = Sm.DrStr(dr2, c2[2]),
                                EmpPict = Sm.DrStr(dr2, c2[3]),
                            });
                        }
                    }

                    dr2.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.ItCode, B.ItName,B.ItCodeInternal, B.Specification, B.ItGrpCode, A.Qty, A.Qty2, A.Qty3, B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, A.Remark, NULL As ProjectCode ");
                    SQLDtl.AppendLine("From TblTransferRequestWhsDtl A ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo");
                    SQLDtl.AppendLine("ORDER BY A.DNo");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                             //0
                             "ItCode",
                             
                             //1-5
                             "ItName",
                             "ItGrpCode",
                             "Qty",
                             "Qty2",
                             "Qty3",

                             //6-10
                             "InventoryUOMCode",
                             "InventoryUOMCode2",
                             "InventoryUOMCode3",
                             "Remark",
                             "ProjectCode",

                             //11-12
                             "ItCodeInternal",
                             "Specification"
                             
                        });
                    int nomor = 0;
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            nomor = nomor + 1;
                            ldtl.Add(new TransferReqDtl()
                            {
                                No = nomor,
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),

                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                ItGrpCode = Sm.DrStr(drDtl, cDtl[2]),
                                Qty1 = Sm.DrDec(drDtl, cDtl[3]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[4]),
                                Qty3 = Sm.DrDec(drDtl, cDtl[5]),

                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[6]),
                                InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[7]),
                                InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[8]),
                                Remark = Sm.DrStr(drDtl, cDtl[9]),
                                ProjectCode = Sm.DrStr(drDtl, cDtl[10]),

                                ItCodeInternal = Sm.DrStr(drDtl, cDtl[11]),
                                Specification = Sm.DrStr(drDtl, cDtl[12]),


                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                int a = int.Parse(ParValue);
                if (mDocTitle == "IMS")
                {
                    Sm.PrintReport("TransferRequestIMS", myLists, TableName, false);
                }
                else
                {
                    if (a == 1)
                    {
                        Sm.PrintReport("TransferRequest1", myLists, TableName, false);
                    }
                    else if (a == 2)
                    {
                        Sm.PrintReport("TransferRequest2", myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport("TransferRequest3", myLists, TableName, false);
                    }
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmTransferRequestWhsDlg(this, Sm.GetLue(LueWhsCode)));
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmTransferRequestWhsDlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 9, 12 }, e);

            if (e.ColIndex == 6)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 3, 6, 9, 12, 7, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 3, 6, 12, 9, 7, 13, 10);
            }

            if (e.ColIndex == 9)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 3, 9, 6, 12, 10, 7, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 3, 9, 12, 6, 10, 13, 7);
            }

            if (e.ColIndex == 12)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 3, 12, 6, 9, 13, 7, 10);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 3, 12, 9, 6, 13, 10, 7);
            }

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 10)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 9 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 9);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TransferRequestWhs", "TblTransferRequestWhsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTransferRequestWhsHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    cml.Add(SaveTransferRequestWhsDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }


        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse from") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse to") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            
            ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.")) return true;
              
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 6) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 6) > Sm.GetGrdDec(Grd1, Row, 5))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[9].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 9) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 9) > Sm.GetGrdDec(Grd1, Row, 8))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[11].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, ItCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
    
            int No = 1;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(X.ItCode=@ItCode" + No + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd1, Row, 3));
                    No += 1;
                }
            }
       
            SQL.AppendLine("Select A.ItCode, ");
            SQL.AppendLine("IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0) As Qty, ");
            SQL.AppendLine("IfNull(A.StockQty2, 0)-IfNull(B.TransferredQty2, 0)-IfNull(C.DOQty2, 0) As Qty2, ");
            SQL.AppendLine("IfNull(A.StockQty3, 0)-IfNull(B.TransferredQty3, 0)-IfNull(C.DOQty3, 0) As Qty3 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, ");
            SQL.AppendLine("    Sum(Qty) As StockQty, Sum(Qty2) As StockQty2, Sum(Qty3) As StockQty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Qty<>0 ");
            SQL.AppendLine("    And WhsCode=@WhsCode And (" + Filter.Replace("X.", "") + ") ");
            SQL.AppendLine("    Group By ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, ");
            SQL.AppendLine("    Sum(T2.Qty) As TransferredQty, ");
            SQL.AppendLine("    Sum(T2.Qty2) As TransferredQty2, ");
            SQL.AppendLine("    Sum(T2.Qty3) As TransferredQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd ='O' ");
            SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T4.ItCode, ");
            SQL.AppendLine("    Sum(T4.Qty) As DOQty, ");
            SQL.AppendLine("    Sum(T4.Qty2) As DOQty2, ");
            SQL.AppendLine("    Sum(T4.Qty3) As DOQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' ");
            SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblDOWhsHdr T3 ");
            SQL.AppendLine("        On T3.TransferRequestWhsDocNo Is Not Null ");
            SQL.AppendLine("        And T2.DocNo=IfNull(T3.TransferRequestWhsDocNo, '') ");
            SQL.AppendLine("        And T3.CancelInd ='N' ");
            SQL.AppendLine("        And T3.Status In ('A', 'O') ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T4 ");
            SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        And T2.ItCode=T4.ItCode ");
            SQL.AppendLine("        And T4.ProcessInd='O' ");
            SQL.AppendLine("        And T4.CancelInd='N' ");
            SQL.AppendLine("        And (" + Filter.Replace("X.", "T4.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')='A' ");
            SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            SQL.AppendLine("    Group By T4.ItCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { "ItCode", "Qty", "Qty2", "Qty3" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 3), ItCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 3);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int Col)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(X.ItCode=@ItCode" + No + ") ";
            Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd, Row, Col));
        }

        private MySqlCommand SaveTransferRequestWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsHdr(DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, PGCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @WhsCode2, @PGCode, @SOContractDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='TransferRequestWhs' And WhsCode = '"+Sm.GetLue(LueWhsCode2)+"'; ");

            SQL.AppendLine("Update TblTransferRequestWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='TransferRequestWhs' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            return cm;
        }

        private MySqlCommand SaveTransferRequestWhsDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsDtl(DocNo, DNo, ItCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTransferRequestWhsHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyDO();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return 
                Sm.IsDataExist(
                    "Select DocNo From TblTransferRequestWhsHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled."
                );
        }

        private bool IsDataAlreadyDO()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblDOWhsHdr " +
                    "Where TransferRequestWhsDocNo Is Not Null " +
                    "And TransferRequestWhsDocNo=@Param " +
                    "And CancelInd='N' " +
                    "And Status In ('O', 'A') Limit 1;",
                    TxtDocNo.Text,
                    "This document already processed to DO."
                );
        }

        private MySqlCommand CancelTransferRequestWhsHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTransferRequestWhsHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTransferRequestWhsHdr(DocNo);
                ShowTransferRequestWhsDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTransferRequestWhsHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End as StatusInd, ");
            SQL.AppendLine("A.LocalDocNo, A.WhsCode, A.WhsCode2, A.Remark, B.PGCode, B.ProjectCode, B.ProjectName, A.SOContractDocNo ");
            SQL.AppendLine("From TblTransferRequestWhsHdr A ");
            SQL.AppendLine("Left Join TblProjectGroup B On A.PGCode=B.PGCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
 
                        //1-5
                        "DocDt", "CancelInd", "StatusInd", "LocalDocNo", "WhsCode", 
                        
                        //6-10
                        "WhsCode2", "Remark", "PGCode", "ProjectCode", "ProjectName",
 
                        //11
                        "SOContractDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        mPGCode = Sm.DrStr(dr, c[8]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[10]);
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowTransferRequestWhsDtl(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, ");
                SQL.AppendLine("A.ItCode, B.ItName, ");
                SQL.AppendLine("A.Qty, B.InventoryUOMCode, ");
                SQL.AppendLine("A.Qty2, B.InventoryUOMCode2, ");
                SQL.AppendLine("A.Qty3, B.InventoryUOMCode3, ");
                SQL.AppendLine("A.Remark, E.CCtName, B.ItGrpCode, B.ItCodeInternal, B.Specification ");
                SQL.AppendLine("From TblTransferRequestWhsDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory D On A.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "Qty",  "InventoryUomCode", "Qty2", 
                        
                        //6-10
                        "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark",  "CCtName",   
                        
                        //11-13
                        "ItGrpCode", "ItCodeInternal", "Specification"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                       
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsTransferRequestWhsProjectEnabled = Sm.GetParameterBoo("IsTransferRequestWhsProjectEnabled");
        }

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public static void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.CCCode As Col1, T.CCName As Col2 " +
                "From TblCostCenter T " +
                "Where Not Exists(Select Parent From TblCostCenter Where CCCode=T.CCCode And Parent Is Not Null) " +
                "Order By T.CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblTransferRequestWhsHdr " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtSOCDocNo.EditValue = f.mSOCDocNo;
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtSOCDocNo.EditValue = null;
            }
            f.Close();
        }

        #endregion
    }

    #region Report Class

    class TransferReqHdr
    {
        public string Company { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string WhsName { get; set; }
        public string WhsName2 { get; set; }
        public string Remark { get; set; }
        public string CompanyLogo { get; set; }
        public string PrintBy { get; set; }
        public string DoReqCreateBy { get; set; }
        public string DoToDeptCreateby { get; set; }
        public string EmpPict { get; set; }
        public string EmpPict2 { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string Workshop { get; set; }
        public string PrintDt { get; set; }
        public string CCName { get; set; }
    }

    class TransferReqHdr2
    {
        public string ApprovalDno { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string EmpPict { get; set; }
    }

    class TransferReqHdr3
    {
        public string ApprovalDno { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string EmpPict { get; set; }
    }

    class TransferReqDtl
    {
        public int No { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string ItGrpCode { get; set; }
        public decimal Qty1 { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }
        public string Remark { get; set; }
        public string ItCodeInternal { get; set; }
        public string Specification { get; set; }
        public string ProjectCode { get; set; }

    }

    #endregion
}
