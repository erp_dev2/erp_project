﻿#region Update
/*
    27/10/2020 [WED/PHT] new apps
    23/12/2020 [VIN/PHT] tambah kolom COA, actual voucher, dan budget vs voucher
    15/01/2021 [DITA/PHT] mengganti filter multi entity menjadi multi cost center berdasarkan param : IsFicoUseMultiCostCenterFilter
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBudgetCashFlow2 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mReqTypeForNonBudget = string.Empty
            ;
        private bool
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMRShowEstimatedPrice = false,
            mIsEntityMandatory = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsReportingFilterByEntity = false,
            mIsFicoUseMultiCostCenterFilter = false,
            mIsCostCenterMandatory = false,
            mIsReportingFilterByCostCenter = false
            ;
        private string
            mMainCurCode = string.Empty,
            mBudgetBasedOn = string.Empty; // 1 = Department, 2 = Site

        #endregion

        #region Constructor

        public FrmRptBudgetCashFlow2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                if (mIsCostCenterMandatory) LblMultiCostCenter.ForeColor = Color.Red;
                SetCcbCCCode(ref CcbCCCode, string.Empty);

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                Sl.SetLueMth(LueMth2);
                Sm.SetLue(LueMth2, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr2, "");
                Sm.SetLue(LueYr2, CurrentDateTime.Substring(0, 4));

                if (mBudgetBasedOn == "1")
                {
                    SetLueDeptCode(ref LueDeptCode);
                }
                if (mBudgetBasedOn == "2")
                {
                    SetLueSiteCode(ref LueDeptCode);
                    LblDeptCode.Text = "Site";
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode, T.DeptName, T.BCCode, T.BCName, T.AcNo, T.BudgetAmt, T.TotalAmtPO, T.TotalAmtMR, If(T.UsedAmt = 0, 0, ((T.UsedAmt / T.BudgetAmt) * 100)) UsagePercentage, ");
            SQL.AppendLine("T.VCAmt, (T.BudgetAmt-T.VCAmt*-1) BudgetVSVoucher ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Z2.DeptCode, Z3.DeptName, Z3_2.BCCode, Z3_3.BCName, IFNULL(Z3_4.AcNo, '') AcNo, IfNull(Sum(Z3_1.Amt), 0.00) BudgetAmt, ");
            SQL.AppendLine("    IfNull(Z6.Amt, 0.00) TotalAmtPO, ");
            SQL.AppendLine("    IfNull(Z4.Amt, 0.00) - IfNull(Z5.Amt, 0.00) As TotalAmtMR, ");
            SQL.AppendLine("    (IfNull(Z7.Amt, 0.00) + IfNull(Z8.Amt, 0.00) + IfNull(Z9.Amt, 0.00) + IfNull(Z10.Amt, 0.00)) As UsedAmt, ");
            SQL.AppendLine("    ifnull(Z11.Amt, 0.00) VCAmt ");
            SQL.AppendLine("    From TblBudgetHdr Z1 ");
            SQL.AppendLine("    Inner Join TblBudgetRequestHdr Z2 On Z1.BudgetRequestDocNo = Z2.DocNo ");
            SQL.AppendLine("        And Z1.`Status` = 'A' ");
            SQL.AppendLine("        And Z2.CancelInd = 'N' ");
            SQL.AppendLine("        And Concat(Z2.Yr, Z2.Mth) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");

            #region filter by entity (comment by DITA 15/01/2021)
            //if (CcbCCCode.Text.Trim().Length > 0 && !GetCcbCode().Contains("Consolidate"))
            //{
            //    if (ChkCCCode.Checked)
            //    {
            //        SQL.AppendLine("And Find_In_Set(Z2.EntCode, @EntCode) ");
            //    }
            //}
            //if (mIsReportingFilterByEntity)
            //{
            //    SQL.AppendLine("And Z2.EntCode Is Not Null ");
            //    SQL.AppendLine("And Exists( ");
            //    SQL.AppendLine("    Select 1 From TblGroupEntity ");
            //    SQL.AppendLine("    Where EntCode=IfNull(Z2.EntCode, '') ");
            //    SQL.AppendLine("    And GrpCode In ( ");
            //    SQL.AppendLine("        Select GrpCode From TblUser ");
            //    SQL.AppendLine("        Where UserCode=@UserCode ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine(") ");
            //}

            #endregion

            SQL.AppendLine("    Inner Join TblDepartment Z3 On Z2.DeptCode = Z3.DeptCode ");
            if (mBudgetBasedOn == "1" && mIsFilterByDept)
            {
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupDepartment ");
                SQL.AppendLine("            Where DeptCode=Z2.DeptCode ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("    Inner Join TblBudgetDtl Z3_1 On Z1.DocNo = Z3_1.DocNo ");
            SQL.AppendLine("    Inner Join TblBudgetRequestDtl Z3_2 On Z2.DocNo = Z3_2.DocNo And Z3_1.BudgetRequestDNo = Z3_2.DNo ");
            SQL.AppendLine("    Inner Join TblBudgetCategory Z3_3 On Z3_2.BCCode = Z3_3.BCCode ");
            SQL.AppendLine("    LEFT JOIN tblcostcategory Z3_4 ON Z3_3.CCtCode = Z3_4.CCtCode ");
            SQL.AppendLine("    LEFT JOIN tblcoa Z3_5 ON Z3_4.AcNo = Z3_5.AcNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.DeptCode, A.BCCode, Sum(B.UPrice*D.Qty) As Amt ");
            SQL.AppendLine("        From TblMaterialRequestHdr A ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL.AppendLine("        Where A.Status='A' ");
            SQL.AppendLine("        And A.Reqtype='1' ");
            SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("        Group By A.DeptCode, A.BCCode ");
            SQL.AppendLine("    ) Z4 On Z2.DeptCode=Z4.DeptCode And Z3_2.BCCode = Z4.BCCode ");

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select A.DeptCode, A.BCCode, Sum(B.UPrice*F.Qty) As Amt ");
            SQL.AppendLine("        From TblMaterialRequestHdr A ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL.AppendLine("        Inner Join TblPOQtyCancel F On D.DocNo=F.PODocNo And D.DNo=F.PODNo And F.CancelInd='N' ");
            SQL.AppendLine("        Where A.Status='A' ");
            SQL.AppendLine("        And A.Reqtype='1' ");
            SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("        Group By A.DeptCode, A.BCCode ");
            SQL.AppendLine("    ) Z5 On Z2.DeptCode=Z5.DeptCode And Z3_2.BCCode = Z5.BCCode ");

            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X2.DeptCode, X2.BCCode, Sum( ");
            SQL.AppendLine("        X2.Amt - ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("          IfNull( X1.DiscountAmt * ");
            SQL.AppendLine("              Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
            SQL.AppendLine("              IfNull(( ");
            SQL.AppendLine("                  Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                  Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                  Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ), 0.00) End ");
            SQL.AppendLine("          , 0.00) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        ) As Amt ");
            SQL.AppendLine("        From TblPOHdr X1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select A.DeptCode, E.DocNo, A.BCCode, Sum( ");
            SQL.AppendLine("            ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue) ");
            SQL.AppendLine("            * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("            IfNull(( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ), 0.00) End ");
            SQL.AppendLine("            ) As Amt ");
            SQL.AppendLine("            From TblMaterialRequestHdr A ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl B ");
            SQL.AppendLine("                On A.DocNo=B.DocNo ");
            SQL.AppendLine("                And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("                And B.CancelInd='N' ");
            SQL.AppendLine("                And B.Status='A' ");
            SQL.AppendLine("                And A.Status='A' ");
            SQL.AppendLine("                And A.Reqtype='1' ");
            SQL.AppendLine("            Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL.AppendLine("            Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL.AppendLine("            Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            SQL.AppendLine("            Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            SQL.AppendLine("            Group By A.DeptCode, E.DocNo, A.BCCode ");
            SQL.AppendLine("        ) X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        Group By X2.DeptCode, X2.BCCode ");
            SQL.AppendLine("    ) Z6 On Z2.DeptCode=Z6.DeptCode And Z3_2.BCCode = Z6.BCCode ");

            //SQL.AppendLine("-- used amount ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DeptCode, T1.BCCode, ");
            if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                SQL.AppendLine("    Sum(T2.Qty * T2.EstPrice) As Amt ");
            else
                SQL.AppendLine("    Sum(T2.Qty * T2.UPrice) As Amt ");
            SQL.AppendLine("        From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And T2.CancelInd = 'N' ");
            SQL.AppendLine("            And T2.`Status` In ('O', 'A') ");
            SQL.AppendLine("        Group By T1.DeptCode, T1.BCCode ");
            SQL.AppendLine("    ) Z7 On Z2.DeptCode = Z7.DeptCode And Z3_2.BCCode = Z7.BCCode ");

            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.DeptCode, T1.BCCode, Sum(T1.Amt) Amt ");
            SQL.AppendLine("        From TblVoucherRequestHdr T1 ");
            SQL.AppendLine("        Where Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("        And T1.ReqType Is Not Null ");
            SQL.AppendLine("        And T1.ReqType <> @ReqTypeForNonBudget ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        And Find_In_Set(T1.DocType,  ");
    	    SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
    	    SQL.AppendLine("        Group By T1.DeptCode, T1.BCCode ");
            SQL.AppendLine("    ) Z8 On Z2.DeptCode = Z8.DeptCode And Z3_2.BCCode = Z8.BCCode ");

            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T3.DeptCode, T2.BCCode, Sum(T2.Amt1+T2.Amt2+T2.Amt3+T2.Amt4+T2.Amt5+T2.Amt6+T2.Detasering) Amt ");
            SQL.AppendLine("        From TblTravelRequestHdr T1 ");
            SQL.AppendLine("        Inner Join TblTravelRequestDtl7 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And T1.CancelInd = 'N' ");
            SQL.AppendLine("            And T1.`Status` In ('O', 'A') ");
            SQL.AppendLine("        Inner Join TblEmployee T3 On T2.PICCode = T3.EmpCode ");
            SQL.AppendLine("        Group By T3.DeptCode, T2.BCCode ");
            SQL.AppendLine("    ) Z9 On Z2.DeptCode = Z9.DeptCode And Z3_2.BCCode = Z9.BCCode ");

            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T.DeptCode, T.BCCode, SUM(T.Amt) As Amt From ( ");
            SQL.AppendLine("            Select D.DeptCode, D.BCCode, ");
            SQL.AppendLine("            Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt ");
            SQL.AppendLine("            From tblvoucherhdr A ");
            SQL.AppendLine("            Inner Join ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            SQL.AppendLine("                From tblvoucherhdr X1 ");
            SQL.AppendLine("                INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            SQL.AppendLine("                    AND X1.DocType = '58' ");
            SQL.AppendLine("                    AND X1.CancelInd = 'N' ");
            SQL.AppendLine("                    AND X2.Status In ('O', 'A') ");
            SQL.AppendLine("            ) B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            SQL.AppendLine("            Inner Join ");
            SQL.AppendLine("            ( ");
            SQL.AppendLine("                SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            SQL.AppendLine("                From tblvoucherhdr X1 ");
            SQL.AppendLine("                Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            SQL.AppendLine("                INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            SQL.AppendLine("                    AND X1.DocType = '56' ");
            SQL.AppendLine("                    AND X1.CancelInd = 'N' ");
            SQL.AppendLine("                    AND X3.Status In ('O', 'A') ");
            SQL.AppendLine("            ) D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("            Where Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("        Group By T.DeptCode, T.BCCode ");
            SQL.AppendLine("    ) Z10 On Z2.DeptCode = Z10.DeptCode And Z3_2.BCCode = Z10.BCCode ");

            SQL.AppendLine("    LEFT JOIN ( ");
    	    SQL.AppendLine("    SELECT C.BCCode, D.AcNo, B.AcType A, E.AcType, A.DeptCode, ");
		    SQL.AppendLine("    ifnull(SUM(  ");
		    SQL.AppendLine("    case  ");
            SQL.AppendLine("        when B.AcType != E.AcType then B.Amt ");
            SQL.AppendLine("        ELSE (B.Amt)*-1 ");
		    SQL.AppendLine("    END), 0.00) Amt ");
		    SQL.AppendLine("    FROM tblvoucherrequesthdr A ");
		    SQL.AppendLine("    INNER JOIN tblvoucherhdr B ON A.DocNo=B.VoucherRequestDocNo ");
		    SQL.AppendLine("    INNER JOIN tblbudgetcategory C ON A.BCCode=C.BCCode ");
		    SQL.AppendLine("    LEFT JOIN tblcostcategory D ON C.CCtCode=D.CCtCode ");
		    SQL.AppendLine("    LEFT JOIN tblcoa E ON D.AcNo=E.AcNo ");
		    SQL.AppendLine("    WHERE Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
		    SQL.AppendLine("    GROUP BY A.BCCode, A.DeptCode ");
            SQL.AppendLine("    ) Z11 ON Z11.BCCode=Z3_2.BCCode AND Z11.DeptCode=Z2.DeptCode AND Z11.AcNo=Z3_5.AcNo ");

            if (CcbCCCode.Text.Trim().Length > 0 && !GetCcbCode().Contains("Consolidate"))
            {
                if (ChkCCCode.Checked)
                {
                    SQL.AppendLine("Inner Join TblCostCenter Z12 ON Z2.DeptCode = Z12.DeptCode ");
                    SQL.AppendLine("And Find_In_Set(Z12.CCCode, @CCCode) ");
                }
            }
            if (mIsReportingFilterByCostCenter)
            {
                SQL.AppendLine("And Z12.CCCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=IfNull(Z12.CCCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }


            SQL.AppendLine("    Group By Z2.DeptCode, Z3.DeptName, Z3_2.BCCode, Z3_3.BCName ");
            SQL.AppendLine(") T ");

            #region without Category

            //SQL.AppendLine("Select T.DeptCode, T.DeptName, Null As BCCode, Null As BCName, T.BudgetAmt, T.TotalAmtPO, T.TotalAmtMR, If(T.UsedAmt = 0, 0, ((T.UsedAmt / T.BudgetAmt) * 100)) UsagePercentage ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select Z2.DeptCode, Z3.DeptName, IfNull(Sum(Z1.Amt), 0.00) BudgetAmt, ");
            //SQL.AppendLine("    IfNull(Z6.Amt, 0.00) TotalAmtPO, ");
            //SQL.AppendLine("    IfNull(Z4.Amt, 0.00) - IfNull(Z5.Amt, 0.00) As TotalAmtMR, ");
            //SQL.AppendLine("    (IfNull(Z7.Amt, 0.00) + IfNull(Z8.Amt, 0.00) + IfNull(Z9.Amt, 0.00) + IfNull(Z10.Amt, 0.00)) As UsedAmt ");
            //SQL.AppendLine("    From TblBudgetHdr Z1 ");
            //SQL.AppendLine("    Inner Join TblBudgetRequestHdr Z2 On Z1.BudgetRequestDocNo = Z2.DocNo ");
            //SQL.AppendLine("        And Z1.`Status` = 'A' ");
            //SQL.AppendLine("        And Z2.CancelInd = 'N' ");
            //SQL.AppendLine("        And Concat(Z2.Yr, Z2.Mth) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //if (CcbEntCode.Text.Trim().Length > 0 && !GetCcbCode().Contains("Consolidate"))
            //{
            //    if (ChkEntCode.Checked)
            //    {
            //        SQL.AppendLine("And Find_In_Set(Z2.EntCode, @EntCode) ");
            //    }
            //}
            //if (mIsReportingFilterByEntity)
            //{
            //    SQL.AppendLine("And Z2.EntCode Is Not Null ");
            //    SQL.AppendLine("And Exists( ");
            //    SQL.AppendLine("    Select 1 From TblGroupEntity ");
            //    SQL.AppendLine("    Where EntCode=IfNull(Z2.EntCode, '') ");
            //    SQL.AppendLine("    And GrpCode In ( ");
            //    SQL.AppendLine("        Select GrpCode From TblUser ");
            //    SQL.AppendLine("        Where UserCode=@UserCode ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine(") ");
            //}
            //SQL.AppendLine("    Inner Join TblDepartment Z3 On Z2.DeptCode = Z3.DeptCode ");
            //if (mBudgetBasedOn == "1" && mIsFilterByDept)
            //{
            //    SQL.AppendLine("        And Exists( ");
            //    SQL.AppendLine("            Select 1 From TblGroupDepartment ");
            //    SQL.AppendLine("            Where DeptCode=Z2.DeptCode ");
            //    SQL.AppendLine("            And GrpCode In ( ");
            //    SQL.AppendLine("                Select GrpCode From TblUser ");
            //    SQL.AppendLine("                Where UserCode=@UserCode ");
            //    SQL.AppendLine("            ) ");
            //    SQL.AppendLine("        ) ");
            //}

            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select A.DeptCode, Sum(B.UPrice*D.Qty) As Amt ");
            //SQL.AppendLine("        From TblMaterialRequestHdr A ");
            //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("            On A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And B.CancelInd='N' ");
            //SQL.AppendLine("            And B.Status='A' ");
            //SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("        Where A.Status='A' ");
            //SQL.AppendLine("        And A.Reqtype='1' ");
            //SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("        Group By A.DeptCode ");
            //SQL.AppendLine("    ) Z4 On Z2.DeptCode=Z4.DeptCode ");

            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select A.DeptCode, Sum(B.UPrice*F.Qty) As Amt ");
            //SQL.AppendLine("        From TblMaterialRequestHdr A ");
            //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("            On A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And B.CancelInd='N' ");
            //SQL.AppendLine("            And B.Status='A' ");
            //SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("        Inner Join TblPOQtyCancel F On D.DocNo=F.PODocNo And D.DNo=F.PODNo And F.CancelInd='N' ");
            //SQL.AppendLine("        Where A.Status='A' ");
            //SQL.AppendLine("        And A.Reqtype='1' ");
            //SQL.AppendLine("        And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("        Group By A.DeptCode ");
            //SQL.AppendLine("    ) Z5 On Z2.DeptCode=Z5.DeptCode ");

            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("        Select X2.DeptCode, Sum( ");
            //SQL.AppendLine("        X2.Amt - ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("          IfNull( X1.DiscountAmt * ");
            //SQL.AppendLine("              Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("              IfNull(( ");
            //SQL.AppendLine("                  Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("                  Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("                  Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("            ), 0.00) End ");
            //SQL.AppendLine("          , 0.00) ");
            //SQL.AppendLine("        ) ");
            //SQL.AppendLine("        ) As Amt ");
            //SQL.AppendLine("        From TblPOHdr X1 ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select A.DeptCode, E.DocNo, Sum( ");
            //SQL.AppendLine("            ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue) ");
            //SQL.AppendLine("            * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("            IfNull(( ");
            //SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("                Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("            ), 0.00) End ");
            //SQL.AppendLine("            ) As Amt ");
            //SQL.AppendLine("            From TblMaterialRequestHdr A ");
            //SQL.AppendLine("            Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("                On A.DocNo=B.DocNo ");
            //SQL.AppendLine("                And Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("                And B.CancelInd='N' ");
            //SQL.AppendLine("                And B.Status='A' ");
            //SQL.AppendLine("                And A.Status='A' ");
            //SQL.AppendLine("                And A.Reqtype='1' ");
            //SQL.AppendLine("            Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("            Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("            Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("            Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("            Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            //SQL.AppendLine("            Group By A.DeptCode, E.DocNo ");
            //SQL.AppendLine("        ) X2 On X1.DocNo = X2.DocNo ");
            //SQL.AppendLine("        Group By X2.DeptCode ");
            //SQL.AppendLine("    ) Z6 On Z2.DeptCode=Z6.DeptCode ");

            ////SQL.AppendLine("-- used amount ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T1.DeptCode, ");
            //if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
            //    SQL.AppendLine("    Sum(T2.Qty * T2.EstPrice) As Amt ");
            //else
            //    SQL.AppendLine("    Sum(T2.Qty * T2.UPrice) As Amt ");
            //SQL.AppendLine("        From TblMaterialRequestHdr T1 ");
            //SQL.AppendLine("        Inner Join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            And Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("            And T2.CancelInd = 'N' ");
            //SQL.AppendLine("            And T2.`Status` In ('O', 'A') ");
            //SQL.AppendLine("        Group By T1.DeptCode ");
            //SQL.AppendLine("    ) Z7 On Z2.DeptCode = Z7.DeptCode ");

            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T1.DeptCode, Sum(T1.Amt) Amt ");
            //SQL.AppendLine("        From TblVoucherRequestHdr T1 ");
            //SQL.AppendLine("        Where Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("        And T1.ReqType Is Not Null ");
            //SQL.AppendLine("        And T1.ReqType <> @ReqTypeForNonBudget ");
            //SQL.AppendLine("        And T1.CancelInd = 'N' ");
            //SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            //SQL.AppendLine("        And Find_In_Set(T1.DocType,  ");
            //SQL.AppendLine("         IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
            //SQL.AppendLine("         Group By T1.DeptCode ");
            //SQL.AppendLine("    ) Z8 On Z2.DeptCode = Z8.DeptCode ");

            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T3.DeptCode, Sum(T2.Amt1+T2.Amt2+T2.Amt3+T2.Amt4+T2.Amt5+T2.Amt6+T2.Detasering) Amt ");
            //SQL.AppendLine("        From TblTravelRequestHdr T1 ");
            //SQL.AppendLine("        Inner Join TblTravelRequestDtl7 T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            And Left(T1.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("            And T1.CancelInd = 'N' ");
            //SQL.AppendLine("            And T1.`Status` In ('O', 'A') ");
            //SQL.AppendLine("        Inner Join TblEmployee T3 On T2.PICCode = T3.EmpCode ");
            //SQL.AppendLine("        Group By T3.DeptCode ");
            //SQL.AppendLine("    ) Z9 On Z2.DeptCode = Z9.DeptCode ");

            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T.DeptCode, SUM(T.Amt) As Amt From ( ");
            //SQL.AppendLine("            Select D.DeptCode, ");
            //SQL.AppendLine("            Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt ");
            //SQL.AppendLine("            From tblvoucherhdr A ");
            //SQL.AppendLine("            Inner Join ");
            //SQL.AppendLine("            ( ");
            //SQL.AppendLine("                SELECT X1.DocNo, X1.VoucherRequestDocNo ");
            //SQL.AppendLine("                From tblvoucherhdr X1 ");
            //SQL.AppendLine("                INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
            //SQL.AppendLine("                    AND X1.DocType = '58' ");
            //SQL.AppendLine("                    AND X1.CancelInd = 'N' ");
            //SQL.AppendLine("                    AND X2.Status In ('O', 'A') ");
            //SQL.AppendLine("            ) B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
            //SQL.AppendLine("            Inner Join ");
            //SQL.AppendLine("            ( ");
            //SQL.AppendLine("                SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
            //SQL.AppendLine("                From tblvoucherhdr X1 ");
            //SQL.AppendLine("                Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
            //SQL.AppendLine("                INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
            //SQL.AppendLine("                    AND X1.DocType = '56' ");
            //SQL.AppendLine("                    AND X1.CancelInd = 'N' ");
            //SQL.AppendLine("                    AND X3.Status In ('O', 'A') ");
            //SQL.AppendLine("            ) D ON C.DocNo = D.DocNo ");
            //SQL.AppendLine("            Where Left(A.DocDt, 6) Between Concat(@YrFrom, @MthFrom) And Concat(@Yr, @Mth) ");
            //SQL.AppendLine("        ) T ");
            //SQL.AppendLine("        Group By T.DeptCode ");
            //SQL.AppendLine("    ) Z10 On Z2.DeptCode = Z10.DeptCode ");
            //SQL.AppendLine("    Group By Z2.DeptCode, Z3.DeptName ");
            //SQL.AppendLine(") T ");

            #endregion

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Department",
                    "Budget Category",
                    "Budget",
                    "Amount Actual"+Environment.NewLine+"PO",
                    "Amount Actual"+Environment.NewLine+"MR",

                    //6-10
                    "Budget"+Environment.NewLine+"VS"+Environment.NewLine+"Purchase Order",
                    "Percentage"+Environment.NewLine+"Usage Budget",
                    "COA",
                    "Amount Actual"+Environment.NewLine+"Voucher",
                    "Budget"+Environment.NewLine+"VS"+Environment.NewLine+"Voucher",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 200, 200, 180, 180, 

                    //6-7
                    180, 120, 200, 180, 180
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[8].Move(3);
            Grd1.Cols[9].Move(6);
            Grd1.Cols[10].Move(9);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);

            if (Sm.IsLueEmpty(LueYr, "Year From") ||
                Sm.IsLueEmpty(LueMth, "Month From") ||
                Sm.IsLueEmpty(LueYr2, "Year To") ||
                Sm.IsLueEmpty(LueMth2, "Month To") ||
                IsPeriodFilterInvalid() ||
                IsCostCenterInvalid()
                //IsEntityInvalid()
                ) return;

            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MthFrom", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@YrFrom", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr2));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);

                //string multiEntCode = GetCcbCode();
                //if (ChkCCCode.Checked)
                //    Sm.CmParam<String>(ref cm, "@EntCode", multiEntCode.Contains("Consolidate") ? "" : multiEntCode);

                string multiCCCode = GetCcbCode();
                if (ChkCCCode.Checked)
                    Sm.CmParam<String>(ref cm, "@CCCode", multiCCCode.Contains("Consolidate") ? "" : multiCCCode);

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DeptName; ",
                    new string[]
                    { 
                        //0
                        "DeptName", 

                        //1-5
                        "BCName", "BudgetAmt", "TotalAmtPO", "TotalAmtMr", "UsagePercentage",

                        //6-8
                        "AcNo", "VCAmt", "BudgetVSVoucher"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                        Grd1.Cells[Row, 6].Value = Sm.DrDec(dr, c[2]) - Sm.DrDec(dr, c[3]);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                        Grd1.Cells[Row, 10].Value = Sm.DrDec(dr, c[2]) - Sm.DrDec(dr, c[7]);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
            
        }
        
        #endregion

        #region Additional Method

        private void SetCcbEntCode(ref DXE.CheckedComboBoxEdit Ccb, string EntName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' Col Union All ");
            SQL.AppendLine("Select EntName As Col From TblEntity  ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (EntName.Length > 0)
            {
                SQL.AppendLine("And EntName = @EntName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select T.EntCode From TblGroupEntity T ");
                SQL.AppendLine("    Where T.EntCode = EntCode ");
                SQL.AppendLine("    And T.GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EntName", EntName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private void SetCcbCCCode(ref DXE.CheckedComboBoxEdit Ccb, string CCName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.Col From( ");
            SQL.AppendLine("Select 'Consolidate' Col, '01' Col2 Union All ");
            SQL.AppendLine("Select CCName As Col, '02' Col2  From TblCostCenter  ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (CCName.Length > 0)
            {
                SQL.AppendLine("And CCName = @CCName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select T.CCCode From TblGroupCostCenter T ");
                SQL.AppendLine("    Where T.CCCode = CCCode ");
                SQL.AppendLine("    And T.GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Order By Col2, Col ");
            SQL.AppendLine(")X ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CCName", CCName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbCode()
        {
            string Value = string.Empty;

            Value = Sm.GetCcb(CcbCCCode);
            if (Value.Length > 0)
            {
                Value = GetCCCode(Value);
                Value = Value.Replace(", ", ",");
            }

            return Value;
        }

        private string GetEntCode(string Value)
        {
            if (Value.Length != 0)
            {
                string initValue = Value;
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.EntCode Separator ', ') EntCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select EntCode ");
                SQL.AppendLine("    From TblEntity ");
                SQL.AppendLine("    Where ActInd = 'Y' And Find_In_Set(EntName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
                if (initValue.Contains("Consolidate"))
                    Value = string.Concat("Consolidate, ", Value);
            }

            return Value;
        }

        private string GetCCCode(string Value)
        {
            if (Value.Length != 0)
            {
                string initValue = Value;
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.CCCode Separator ', ') CCCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select CCCode ");
                SQL.AppendLine("    From TblCostCenter ");
                SQL.AppendLine("    Where ActInd = 'Y' And Find_In_Set(CCName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
                if (initValue.Contains("Consolidate"))
                    Value = string.Concat("Consolidate, ", Value);
            }

            return Value;
        }

        private bool IsPeriodFilterInvalid()
        {
            decimal mStart = 0m, mEnd = 0m;

            mStart = Decimal.Parse(string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            mEnd = Decimal.Parse(string.Concat(Sm.GetLue(LueYr2), Sm.GetLue(LueMth2)));

            if (mStart > mEnd)
            {
                Sm.StdMsg(mMsgType.Warning, "Start period should not be bigger than End period.");
                LueYr.Focus();
                return true;
            }

            return false;
        }

        private bool IsEntityInvalid()
        {
            if (!mIsEntityMandatory) return false;

            if (mIsFicoUseMultiEntityFilter)
            {
                if (CcbCCCode.EditValue == null ||
                    CcbCCCode.Text.Trim().Length == 0 ||
                    CcbCCCode.EditValue.ToString().Trim().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Multi Entity is empty.");
                    CcbCCCode.Focus();
                    return true;
                }

                string multiEntCode = GetCcbCode();
                if (multiEntCode.Contains("Consolidate"))
                {
                    string[] split = multiEntCode.Split(',');
                    bool mFlag = false;
                    if (split.Length > 1)
                    {
                        foreach (var x in split)
                        {
                            if (x.Length == 0)
                            {
                                mFlag = true;
                                break;
                            }
                        }

                        if (!mFlag)
                        {
                            Sm.StdMsg(mMsgType.Warning, "You can not choose another entity if already chosen Consolidate.");
                            CcbCCCode.Focus();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsCostCenterInvalid()
        {
            if (!mIsCostCenterMandatory) return false;

            if (mIsFicoUseMultiCostCenterFilter)
            {
                if (CcbCCCode.EditValue == null ||
                    CcbCCCode.Text.Trim().Length == 0 ||
                    CcbCCCode.EditValue.ToString().Trim().Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Multi Cost Center is empty.");
                    CcbCCCode.Focus();
                    return true;
                }

                string multiCCCode = GetCcbCode();
                if (multiCCCode.Contains("Consolidate"))
                {
                    string[] split = multiCCCode.Split(',');
                    bool mFlag = false;
                    if (split.Length > 1)
                    {
                        foreach (var x in split)
                        {
                            if (x.Length == 0)
                            {
                                mFlag = true;
                                break;
                            }
                        }

                        if (!mFlag)
                        {
                            Sm.StdMsg(mMsgType.Warning, "You can not choose another cost center if already chosen Consolidate.");
                            CcbCCCode.Focus();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private void GetParameter()
        {
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsFicoUseMultiEntityFilter = Sm.GetParameterBoo("IsFicoUseMultiEntityFilter");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsFicoUseMultiCostCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiCostCenterFilter");
            mIsCostCenterMandatory = Sm.GetParameterBoo("IsCostCenterMandatory");
            mIsReportingFilterByCostCenter = Sm.GetParameterBoo("IsReportingFilterByCostCenter");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                SQL.AppendLine(") ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mIsFilterBySite) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mBudgetBasedOn == "1")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
            if (mBudgetBasedOn == "2")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueSiteCode));

            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void CcbCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Cost Center");
        }

        #endregion

        #endregion
    }
}
