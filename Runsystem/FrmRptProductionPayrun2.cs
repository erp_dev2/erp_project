﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionPayrun2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mWC = "", mEC = "" ;
        private decimal mDirectLaborDailyWorkHour = 0m;

        #endregion

        #region Constructor

        public FrmRptProductionPayrun2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                TxtAmt.Properties.ReadOnly = TxtAvg.Properties.ReadOnly =
                TxtAmtEmp.Properties.ReadOnly = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            string DirectLaborDailyWorkHour = Sm.GetParameter("DirectLaborDailyWorkHour");
            if (DirectLaborDailyWorkHour.Length > 0)
                mDirectLaborDailyWorkHour = decimal.Parse(DirectLaborDailyWorkHour);
        }


        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Production Payrun 2 */ ");
            SQL.AppendLine("Select * From ( ");
            
            #region wages
            SQL.AppendLine("Select Z2.WorkcenterDocNo, Z2.DocName, Z2.EmpCode, Z2.EmpName, Z2.PosName, Z2.DeptCode, Z2.DeptName, 'Earning' As TypeCode, '1' As CodeName, ");
            SQL.AppendLine("SUM(Z2.01) '01', SUM(Z2.02) '02', SUM(Z2.03) '03', SUM(Z2.04) '04', SUM(Z2.05) '05', SUM(Z2.06) '06', SUM(Z2.07) '07', SUM(Z2.08) '08', SUM(Z2.09) '09', SUM(Z2.10) '10', ");
            SQL.AppendLine("SUM(Z2.11) '11', SUM(Z2.12) '12', SUM(Z2.13) '13', SUM(Z2.14) '14', SUM(Z2.15) '15', SUM(Z2.16) '16', SUM(Z2.17) '17', SUM(Z2.18) '18', SUM(Z2.19) '19', SUM(Z2.20) '20', ");
            SQL.AppendLine("SUM(Z2.21) '21', SUM(Z2.22) '22', SUM(Z2.23) '23', SUM(Z2.24) '24', SUM(Z2.25) '25', SUM(Z2.26) '26', SUM(Z2.27) '27', SUM(Z2.28) '28', SUM(Z2.29) '29', SUM(Z2.30) '30', ");
            SQL.AppendLine("SUM(Z2.31) '31', 'Wages' As Type ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Z.*, ");
            SQL.AppendLine("Case Z.Dt When '01' Then Z.Wages Else 0.0000 End As '01', ");
            SQL.AppendLine("Case Z.Dt When '02' Then Z.Wages Else 0.0000 End As '02', ");
            SQL.AppendLine("Case Z.Dt When '03' Then Z.Wages Else 0.0000 End As '03', ");
            SQL.AppendLine("Case Z.Dt When '04' Then Z.Wages Else 0.0000 End As '04', ");
            SQL.AppendLine("Case Z.Dt When '05' Then Z.Wages Else 0.0000 End As '05', ");
            SQL.AppendLine("Case Z.Dt When '06' Then Z.Wages Else 0.0000 End As '06', ");
            SQL.AppendLine("Case Z.Dt When '07' Then Z.Wages Else 0.0000 End As '07', ");
            SQL.AppendLine("Case Z.Dt When '08' Then Z.Wages Else 0.0000 End As '08', ");
            SQL.AppendLine("Case Z.Dt When '09' Then Z.Wages Else 0.0000 End As '09', ");
            SQL.AppendLine("Case Z.Dt When '10' Then Z.Wages Else 0.0000 End As '10', ");
            SQL.AppendLine("Case Z.Dt When '11' Then Z.Wages Else 0.0000 End As '11', ");
            SQL.AppendLine("Case Z.Dt When '12' Then Z.Wages Else 0.0000 End As '12', ");
            SQL.AppendLine("Case Z.Dt When '13' Then Z.Wages Else 0.0000 End As '13', ");
            SQL.AppendLine("Case Z.Dt When '14' Then Z.Wages Else 0.0000 End As '14', ");
            SQL.AppendLine("Case Z.Dt When '15' Then Z.Wages Else 0.0000 End As '15', ");
            SQL.AppendLine("Case Z.Dt When '16' Then Z.Wages Else 0.0000 End As '16', ");
            SQL.AppendLine("Case Z.Dt When '17' Then Z.Wages Else 0.0000 End As '17', ");
            SQL.AppendLine("Case Z.Dt When '18' Then Z.Wages Else 0.0000 End As '18', ");
            SQL.AppendLine("Case Z.Dt When '19' Then Z.Wages Else 0.0000 End As '19', "); 
            SQL.AppendLine("Case Z.Dt When '20' Then Z.Wages Else 0.0000 End As '20', ");
            SQL.AppendLine("Case Z.Dt When '21' Then Z.Wages Else 0.0000 End As '21', ");
            SQL.AppendLine("Case Z.Dt When '22' Then Z.Wages Else 0.0000 End As '22', ");
            SQL.AppendLine("Case Z.Dt When '23' Then Z.Wages Else 0.0000 End As '23', ");
            SQL.AppendLine("Case Z.Dt When '24' Then Z.Wages Else 0.0000 End As '24', ");
            SQL.AppendLine("Case Z.Dt When '25' Then Z.Wages Else 0.0000 End As '25', ");
            SQL.AppendLine("Case Z.Dt When '26' Then Z.Wages Else 0.0000 End As '26', ");
            SQL.AppendLine("Case Z.Dt When '27' Then Z.Wages Else 0.0000 End As '27', ");
            SQL.AppendLine("Case Z.Dt When '28' Then Z.Wages Else 0.0000 End As '28', ");
            SQL.AppendLine("Case Z.Dt When '29' Then Z.Wages Else 0.0000 End As '29', ");
            SQL.AppendLine("Case Z.Dt When '30' Then Z.Wages Else 0.0000 End As '30', ");
            SQL.AppendLine("Case Z.Dt When '31' Then Z.Wages Else 0.0000 End As '31' ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.WorkCenterDocNo, C.DocName, ");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, D.DeptCode, F.DeptName,B.DocDt, B.DateTrx, G.Dt, ");
            SQL.AppendLine("Sum(B.Wages) As Wages, Sum(B.Penalty) As Penalty  ");
            SQL.AppendLine("From (  ");
    		SQL.AppendLine("	Select T1.WorkCenterDocNo, Right(T1.DocDT, 2) As DateTrx, T1.DocDt,  T2.EmpCode, T2.Wages2 As Wages, 0 As Penalty ");
			SQL.AppendLine("	From TblPWGHdr T1 ");
			SQL.AppendLine("	Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='N' ");
			SQL.AppendLine("	Where T1.CancelInd='N'");
			SQL.AppendLine("	Union All ");
			SQL.AppendLine("	Select T1.WorkCenterDocNo, Right(T1.DocDT, 2) As DateTrx, T1.DocDt,  T2.EmpCode, T2.Wages2 As Wages, 0 As Penalty");
			SQL.AppendLine("	From TblPWGHdr T1 ");
			SQL.AppendLine("	Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='Y' ");
			SQL.AppendLine("	Where T1.CancelInd='N' ");
            SQL.AppendLine(") B");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Dt Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All ");
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1) Union All ");
	        SQL.AppendLine("   Select convert('13' using latin1) Union All ");
            SQL.AppendLine("   Select convert('14' using latin1) Union All ");
            SQL.AppendLine("   Select convert('15' using latin1) Union All ");
            SQL.AppendLine("   Select convert('16' using latin1) Union All ");
            SQL.AppendLine("   Select convert('17' using latin1) Union All ");
            SQL.AppendLine("   Select convert('18' using latin1) Union All ");
            SQL.AppendLine("   Select convert('19' using latin1) Union All ");
            SQL.AppendLine("   Select convert('20' using latin1) Union All ");
            SQL.AppendLine("   Select convert('21' using latin1) Union All ");
            SQL.AppendLine("   Select convert('22' using latin1) Union All ");
            SQL.AppendLine("   Select convert('23' using latin1) Union All ");
            SQL.AppendLine("   Select convert('24' using latin1) Union All ");
	        SQL.AppendLine("   Select convert('25' using latin1) Union All ");
            SQL.AppendLine("   Select convert('26' using latin1) Union All ");
            SQL.AppendLine("   Select convert('27' using latin1) Union All ");
            SQL.AppendLine("   Select convert('28' using latin1) Union All ");
            SQL.AppendLine("   Select convert('29' using latin1) Union All ");
            SQL.AppendLine("   Select convert('30' using latin1) Union All ");
            SQL.AppendLine("   Select convert('31' using latin1)  ");
            SQL.AppendLine(") G On G.Dt=B.DateTrx  ");
            SQL.AppendLine("Where Left(B.DocDt, 4) = @Year And substring(B.Docdt, 5, 2)=@Mth ");
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.DateTrx, B.DocDt, B.WorkCenterDocNo, C.DocName, ");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, F.DeptName ");
            SQL.AppendLine("Order By B.WorkCenterDocNo, B.EmpCode, B.DocDt ");
            SQL.AppendLine(")Z ");
            SQL.AppendLine(")Z2 ");
            SQL.AppendLine("Group By  Z2.WorkcenterDocNo, Z2.DocName, Z2.EmpCode, Z2.EmpName, Z2.PosName, Z2.DeptName ");
            #endregion

            #region penalty
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptCode, Z3.DeptName, 'Deduction' As TypeCode, '2' As CodeName, ");
            SQL.AppendLine("SUM(Z3.01)*(-1), SUM(Z3.02)*(-1), SUM(Z3.03)*(-1), SUM(Z3.04)*(-1), SUM(Z3.05)*(-1), SUM(Z3.06)*(-1), SUM(Z3.07)*(-1), SUM(Z3.08)*(-1), SUM(Z3.09)*(-1), SUM(Z3.10)*(-1), ");
            SQL.AppendLine("SUM(Z3.11)*(-1), SUM(Z3.12)*(-1), SUM(Z3.13)*(-1), SUM(Z3.14)*(-1), SUM(Z3.15)*(-1), SUM(Z3.16)*(-1), SUM(Z3.17)*(-1), SUM(Z3.18)*(-1), SUM(Z3.19)*(-1), SUM(Z3.20)*(-1), ");
            SQL.AppendLine("SUM(Z3.21)*(-1), SUM(Z3.22)*(-1), SUM(Z3.23)*(-1), SUM(Z3.24)*(-1), SUM(Z3.25)*(-1), SUM(Z3.26)*(-1), SUM(Z3.27)*(-1), SUM(Z3.28)*(-1), SUM(Z3.29)*(-1), SUM(Z3.30)*(-1), ");
            SQL.AppendLine("SUM(Z3.31)*(-1), 'Penalty' As Type ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Z.*, ");
            SQL.AppendLine("Case Z.Dt When '01' Then Z.Penalty Else 0.0000 End As '01', ");
            SQL.AppendLine("Case Z.Dt When '02' Then Z.Penalty Else 0.0000 End As '02', ");
            SQL.AppendLine("Case Z.Dt When '03' Then Z.Penalty Else 0.0000 End As '03',");
            SQL.AppendLine("Case Z.Dt When '04' Then Z.Penalty Else 0.0000 End As '04',");
            SQL.AppendLine("Case Z.Dt When '05' Then Z.Penalty Else 0.0000 End As '05',");
            SQL.AppendLine("Case Z.Dt When '06' Then Z.Penalty Else 0.0000 End As '06',");
            SQL.AppendLine("Case Z.Dt When '07' Then Z.Penalty Else 0.0000 End As '07',");
            SQL.AppendLine("Case Z.Dt When '08' Then Z.Penalty Else 0.0000 End As '08',");
            SQL.AppendLine("Case Z.Dt When '09' Then Z.Penalty Else 0.0000 End As '09',");
            SQL.AppendLine("Case Z.Dt When '10' Then Z.Penalty Else 0.0000 End As '10',");
            SQL.AppendLine("Case Z.Dt When '11' Then Z.Penalty Else 0.0000 End As '11',");
            SQL.AppendLine("Case Z.Dt When '12' Then Z.Penalty Else 0.0000 End As '12',");
            SQL.AppendLine("Case Z.Dt When '13' Then Z.Penalty Else 0.0000 End As '13',");
            SQL.AppendLine("Case Z.Dt When '14' Then Z.Penalty Else 0.0000 End As '14',");
            SQL.AppendLine("Case Z.Dt When '15' Then Z.Penalty Else 0.0000 End As '15',");
            SQL.AppendLine("Case Z.Dt When '16' Then Z.Penalty Else 0.0000 End As '16',");
            SQL.AppendLine("Case Z.Dt When '17' Then Z.Penalty Else 0.0000 End As '17',");
            SQL.AppendLine("Case Z.Dt When '18' Then Z.Penalty Else 0.0000 End As '18',");
            SQL.AppendLine("Case Z.Dt When '19' Then Z.Penalty Else 0.0000 End As '19',");
            SQL.AppendLine("Case Z.Dt When '20' Then Z.Penalty Else 0.0000 End As '20',");
            SQL.AppendLine("Case Z.Dt When '21' Then Z.Penalty Else 0.0000 End As '21',");
            SQL.AppendLine("Case Z.Dt When '22' Then Z.Penalty Else 0.0000 End As '22',");
            SQL.AppendLine("Case Z.Dt When '23' Then Z.Penalty Else 0.0000 End As '23',");
            SQL.AppendLine("Case Z.Dt When '24' Then Z.Penalty Else 0.0000 End As '24',");
            SQL.AppendLine("Case Z.Dt When '25' Then Z.Penalty Else 0.0000 End As '25',");
            SQL.AppendLine("Case Z.Dt When '26' Then Z.Penalty Else 0.0000 End As '26',");
            SQL.AppendLine("Case Z.Dt When '27' Then Z.Penalty Else 0.0000 End As '27', ");
            SQL.AppendLine("Case Z.Dt When '28' Then Z.Penalty Else 0.0000 End As '28',");
            SQL.AppendLine("Case Z.Dt When '29' Then Z.Penalty Else 0.0000 End As '29',");
            SQL.AppendLine("Case Z.Dt When '30' Then Z.Penalty Else 0.0000 End As '30',");
            SQL.AppendLine("Case Z.Dt When '31' Then Z.Penalty Else 0.0000 End As '31' ");   
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.WorkCenterDocNo, C.DocName, ");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, D.DeptCode, F.DeptName, B.DocDt, B.DateTrx, G.Dt, ");
            SQL.AppendLine("Sum(B.Penalty) As Penalty ");
            SQL.AppendLine("From  (  ");
            SQL.AppendLine("    Select T1.WorkCenterDocNo, Right(T1.DocDt, 2) As DateTrx, T1.DocDt, T2.EmpCode, 0 As Wages, T2.Penalty As Penalty ");
            SQL.AppendLine("    From TblPNT2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.WorkCenterDocNo, Right(T1.DocDt, 2) As DateTrx, T1.DocDt, T2.EmpCode, 0 As Wages, T2.Penalty As Penalty ");
            SQL.AppendLine("    From TblPNTHdr T1 ");
            SQL.AppendLine("    Inner Join TblPNTDtl4 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Dt Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All ");
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1) Union All ");
	        SQL.AppendLine("   Select convert('13' using latin1) Union All ");
            SQL.AppendLine("   Select convert('14' using latin1) Union All ");
            SQL.AppendLine("   Select convert('15' using latin1) Union All ");
            SQL.AppendLine("   Select convert('16' using latin1) Union All ");
            SQL.AppendLine("   Select convert('17' using latin1) Union All ");
            SQL.AppendLine("   Select convert('18' using latin1) Union All ");
            SQL.AppendLine("   Select convert('19' using latin1) Union All ");
            SQL.AppendLine("   Select convert('20' using latin1) Union All ");
            SQL.AppendLine("   Select convert('21' using latin1) Union All ");
            SQL.AppendLine("   Select convert('22' using latin1) Union All ");
            SQL.AppendLine("   Select convert('23' using latin1) Union All ");
            SQL.AppendLine("   Select convert('24' using latin1) Union All ");
	        SQL.AppendLine("   Select convert('25' using latin1) Union All ");
            SQL.AppendLine("   Select convert('26' using latin1) Union All ");
            SQL.AppendLine("   Select convert('27' using latin1) Union All ");
            SQL.AppendLine("   Select convert('28' using latin1) Union All ");
            SQL.AppendLine("   Select convert('29' using latin1) Union All ");
            SQL.AppendLine("   Select convert('30' using latin1) Union All ");
            SQL.AppendLine("   Select convert('31' using latin1)  ");
            SQL.AppendLine(") G On G.Dt=B.DateTrx  ");
            SQL.AppendLine("Where Left(B.DocDt, 4) = @Year And substring(B.Docdt, 5, 2)=@Mth "); 
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.DateTrx, B.DocDt, B.WorkCenterDocNo, C.DocName,");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, F.DeptName");
            SQL.AppendLine("Order By B.WorkCenterDocNo, B.EmpCode, B.DocDt");
            SQL.AppendLine(")Z");
            SQL.AppendLine(")Z3");
            SQL.AppendLine("Group By  Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptName");
            #endregion

            #region leave
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Z4.WorkcenterDocNo, Z4.DocName, Z4.EmpCode, Z4.EmpName, Z4.PosName, Z4.DeptCode, Z4.DeptName, 'Deduction' As TypeCode, '3' As CodeName, ");
            SQL.AppendLine("SUM(Z4.01)*(-1), SUM(Z4.02)*(-1), SUM(Z4.03)*(-1), SUM(Z4.04)*(-1), SUM(Z4.05)*(-1), SUM(Z4.06)*(-1), SUM(Z4.07)*(-1), SUM(Z4.08)*(-1), SUM(Z4.09)*(-1), SUM(Z4.10)*(-1), ");
            SQL.AppendLine("SUM(Z4.11)*(-1), SUM(Z4.12)*(-1), SUM(Z4.13)*(-1), SUM(Z4.14)*(-1), SUM(Z4.15)*(-1), SUM(Z4.16)*(-1), SUM(Z4.17)*(-1), SUM(Z4.18)*(-1), SUM(Z4.19)*(-1), SUM(Z4.20)*(-1), ");
            SQL.AppendLine("SUM(Z4.21)*(-1), SUM(Z4.22)*(-1), SUM(Z4.23)*(-1), SUM(Z4.24)*(-1), SUM(Z4.25)*(-1), SUM(Z4.26)*(-1), SUM(Z4.27)*(-1), SUM(Z4.28)*(-1), SUM(Z4.29)*(-1), SUM(Z4.30)*(-1), ");
            SQL.AppendLine("SUM(Z4.31)*(-1), 'Leave' As type ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Z.*, ");
            SQL.AppendLine("Case Z.Dt When '01' Then Z.PenaltyLeave Else 0.0000 End As '01', ");
            SQL.AppendLine("Case Z.Dt When '02' Then Z.PenaltyLeave Else 0.0000 End As '02', ");
            SQL.AppendLine("Case Z.Dt When '03' Then Z.PenaltyLeave Else 0.0000 End As '03', ");
            SQL.AppendLine("Case Z.Dt When '04' Then Z.PenaltyLeave Else 0.0000 End As '04', ");
            SQL.AppendLine("Case Z.Dt When '05' Then Z.PenaltyLeave Else 0.0000 End As '05',");
            SQL.AppendLine("Case Z.Dt When '06' Then Z.PenaltyLeave Else 0.0000 End As '06',");
            SQL.AppendLine("Case Z.Dt When '07' Then Z.PenaltyLeave Else 0.0000 End As '07',");
            SQL.AppendLine("Case Z.Dt When '08' Then Z.PenaltyLeave Else 0.0000 End As '08',");
            SQL.AppendLine("Case Z.Dt When '09' Then Z.PenaltyLeave Else 0.0000 End As '09',");
            SQL.AppendLine("Case Z.Dt When '10' Then Z.PenaltyLeave Else 0.0000 End As '10',");
            SQL.AppendLine("Case Z.Dt When '11' Then Z.PenaltyLeave Else 0.0000 End As '11',");
            SQL.AppendLine("Case Z.Dt When '12' Then Z.PenaltyLeave Else 0.0000 End As '12',");
            SQL.AppendLine("Case Z.Dt When '13' Then Z.PenaltyLeave Else 0.0000 End As '13',");
            SQL.AppendLine("Case Z.Dt When '14' Then Z.PenaltyLeave Else 0.0000 End As '14',");
            SQL.AppendLine("Case Z.Dt When '15' Then Z.PenaltyLeave Else 0.0000 End As '15',");
            SQL.AppendLine("Case Z.Dt When '16' Then Z.PenaltyLeave Else 0.0000 End As '16',");
            SQL.AppendLine("Case Z.Dt When '17' Then Z.PenaltyLeave Else 0.0000 End As '17',");
            SQL.AppendLine("Case Z.Dt When '18' Then Z.PenaltyLeave Else 0.0000 End As '18',");
            SQL.AppendLine("Case Z.Dt When '19' Then Z.PenaltyLeave Else 0.0000 End As '19',");
            SQL.AppendLine("Case Z.Dt When '20' Then Z.PenaltyLeave Else 0.0000 End As '20',");
            SQL.AppendLine("Case Z.Dt When '21' Then Z.PenaltyLeave Else 0.0000 End As '21',");
            SQL.AppendLine("Case Z.Dt When '22' Then Z.PenaltyLeave Else 0.0000 End As '22',");
            SQL.AppendLine("Case Z.Dt When '23' Then Z.PenaltyLeave Else 0.0000 End As '23',");
            SQL.AppendLine("Case Z.Dt When '24' Then Z.PenaltyLeave Else 0.0000 End As '24',");
            SQL.AppendLine("Case Z.Dt When '25' Then Z.PenaltyLeave Else 0.0000 End As '25',");
            SQL.AppendLine("Case Z.Dt When '26' Then Z.PenaltyLeave Else 0.0000 End As '26',");
            SQL.AppendLine("Case Z.Dt When '27' Then Z.PenaltyLeave Else 0.0000 End As '27', ");
            SQL.AppendLine("Case Z.Dt When '28' Then Z.PenaltyLeave Else 0.0000 End As '28',");
            SQL.AppendLine("Case Z.Dt When '29' Then Z.PenaltyLeave Else 0.0000 End As '29',");
            SQL.AppendLine("Case Z.Dt When '30' Then Z.PenaltyLeave Else 0.0000 End As '30',");
            SQL.AppendLine("Case Z.Dt When '31' Then Z.PenaltyLeave Else 0.0000 End As '31'   "); 
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.WorkCenterDocNo, C.DocName,"); 
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, D.DeptCode, F.DeptName, B.DocDt, B.DateTrx, G.Dt,"); 
            SQL.AppendLine("Sum(B.PenaltyLeave) As PenaltyLeave ");
            SQL.AppendLine("From  (  ");
            SQL.AppendLine("    Select T1.DocDt, Right(T1.DocDt, 2) As DateTrx, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, 0 As Penalty, ");
            SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As PenaltyLeave ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='N' ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocDt, Right(T1.DocDt, 2) As DateTrx, T1.WorkCenterDocNo, T2.EmpCode, 0 As Wages, 0 As Penalty, ");
            SQL.AppendLine("    If(@DirectLaborDailyWorkHour=0, 0, ((T3.DurationHour/@DirectLaborDailyWorkHour)*T2.Wages2)) As PenaltyLeave ");
            SQL.AppendLine("    From TblPWGHdr T1 ");
            SQL.AppendLine("    Inner Join TblPWGDtl5 T2 On T1.DocNo=T2.DocNo And T2.CoordinatorInd='Y' ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select EmpCode, LeaveDt, Sum(DurationHour) As DurationHour ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select X1.EmpCode, X2.LeaveDt, ");
            SQL.AppendLine("            If(X1.DurationHour=0 Or X1.DurationHour>@DirectLaborDailyWorkHour, ");
            SQL.AppendLine("            @DirectLaborDailyWorkHour, X1.DurationHour) As DurationHour ");
            SQL.AppendLine("            From TblEmpLeaveHdr X1 ");
            SQL.AppendLine("            Inner Join TblEmpLeaveDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblLeave X3 On X1.LeaveCode = X3.LeaveCode And X3.PaidInd = 'N' ");
            SQL.AppendLine("            Where X1.CancelInd='N' And IfNull(X1.Status, 'O')='A' ");
            SQL.AppendLine("        ) X Group By X.EmpCode, X.LeaveDt ");
            SQL.AppendLine("    ) T3 On T1.DocDt=T3.LeaveDt And T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Dt Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All ");
            SQL.AppendLine("   Select convert('06' using latin1) Union All ");
            SQL.AppendLine("   Select convert('07' using latin1) Union All ");
            SQL.AppendLine("   Select convert('08' using latin1) Union All ");
            SQL.AppendLine("   Select convert('09' using latin1) Union All ");
            SQL.AppendLine("   Select convert('10' using latin1) Union All ");
            SQL.AppendLine("   Select convert('11' using latin1) Union All ");
            SQL.AppendLine("   Select convert('12' using latin1) Union All ");
            SQL.AppendLine("   Select convert('13' using latin1) Union All ");
            SQL.AppendLine("   Select convert('14' using latin1) Union All ");
            SQL.AppendLine("   Select convert('15' using latin1) Union All ");
            SQL.AppendLine("   Select convert('16' using latin1) Union All ");
            SQL.AppendLine("   Select convert('17' using latin1) Union All ");
            SQL.AppendLine("   Select convert('18' using latin1) Union All ");
            SQL.AppendLine("   Select convert('19' using latin1) Union All ");
            SQL.AppendLine("   Select convert('20' using latin1) Union All ");
            SQL.AppendLine("   Select convert('21' using latin1) Union All ");
            SQL.AppendLine("   Select convert('22' using latin1) Union All ");
            SQL.AppendLine("   Select convert('23' using latin1) Union All ");
            SQL.AppendLine("   Select convert('24' using latin1) Union All ");
            SQL.AppendLine("   Select convert('25' using latin1) Union All ");
            SQL.AppendLine("   Select convert('26' using latin1) Union All ");
            SQL.AppendLine("   Select convert('27' using latin1) Union All ");
            SQL.AppendLine("   Select convert('28' using latin1) Union All ");
            SQL.AppendLine("   Select convert('29' using latin1) Union All ");
            SQL.AppendLine("   Select convert('30' using latin1) Union All ");
            SQL.AppendLine("   Select convert('31' using latin1)  ");
            SQL.AppendLine(") G On G.Dt=B.DateTrx  ");
            SQL.AppendLine("Where Left(B.DocDt, 4) = @Year And substring(B.Docdt, 5, 2)=@Mth  ");
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.DateTrx, B.DocDt, B.WorkCenterDocNo, C.DocName,");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, F.DeptName");
            SQL.AppendLine("Order By B.WorkCenterDocNo, B.EmpCode, B.DocDt");
            SQL.AppendLine(")Z");
            SQL.AppendLine(")Z4");
            SQL.AppendLine("Group By  Z4.WorkcenterDocNo, Z4.DocName, Z4.EmpCode, Z4.EmpName, Z4.PosName, Z4.DeptName");
            #endregion

            #region insentif 2
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptCode, Z3.DeptName, 'Earning' As TypeCode, '5' As CodeName, ");
            SQL.AppendLine("SUM(Z3.01), SUM(Z3.02), SUM(Z3.03), SUM(Z3.04), SUM(Z3.05), SUM(Z3.06), SUM(Z3.07), SUM(Z3.08), SUM(Z3.09), SUM(Z3.10), ");
            SQL.AppendLine("SUM(Z3.11), SUM(Z3.12), SUM(Z3.13), SUM(Z3.14), SUM(Z3.15), SUM(Z3.16), SUM(Z3.17), SUM(Z3.18), SUM(Z3.19), SUM(Z3.20), ");
            SQL.AppendLine("SUM(Z3.21), SUM(Z3.22), SUM(Z3.23), SUM(Z3.24), SUM(Z3.25), SUM(Z3.26), SUM(Z3.27), SUM(Z3.28), SUM(Z3.29), SUM(Z3.30), ");
            SQL.AppendLine("SUM(Z3.31), 'Insentif' As Type ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Z.*, ");
            SQL.AppendLine("Case Z.Dt When '01' Then Z.Insentif Else 0.0000 End As '01', ");
            SQL.AppendLine("Case Z.Dt When '02' Then Z.Insentif Else 0.0000 End As '02', ");
            SQL.AppendLine("Case Z.Dt When '03' Then Z.Insentif Else 0.0000 End As '03',");
            SQL.AppendLine("Case Z.Dt When '04' Then Z.Insentif Else 0.0000 End As '04',");
            SQL.AppendLine("Case Z.Dt When '05' Then Z.Insentif Else 0.0000 End As '05',");
            SQL.AppendLine("Case Z.Dt When '06' Then Z.Insentif Else 0.0000 End As '06',");
            SQL.AppendLine("Case Z.Dt When '07' Then Z.Insentif Else 0.0000 End As '07',");
            SQL.AppendLine("Case Z.Dt When '08' Then Z.Insentif Else 0.0000 End As '08',");
            SQL.AppendLine("Case Z.Dt When '09' Then Z.Insentif Else 0.0000 End As '09',");
            SQL.AppendLine("Case Z.Dt When '10' Then Z.Insentif Else 0.0000 End As '10',");
            SQL.AppendLine("Case Z.Dt When '11' Then Z.Insentif Else 0.0000 End As '11',");
            SQL.AppendLine("Case Z.Dt When '12' Then Z.Insentif Else 0.0000 End As '12',");
            SQL.AppendLine("Case Z.Dt When '13' Then Z.Insentif Else 0.0000 End As '13',");
            SQL.AppendLine("Case Z.Dt When '14' Then Z.Insentif Else 0.0000 End As '14',");
            SQL.AppendLine("Case Z.Dt When '15' Then Z.Insentif Else 0.0000 End As '15',");
            SQL.AppendLine("Case Z.Dt When '16' Then Z.Insentif Else 0.0000 End As '16',");
            SQL.AppendLine("Case Z.Dt When '17' Then Z.Insentif Else 0.0000 End As '17',");
            SQL.AppendLine("Case Z.Dt When '18' Then Z.Insentif Else 0.0000 End As '18',");
            SQL.AppendLine("Case Z.Dt When '19' Then Z.Insentif Else 0.0000 End As '19',");
            SQL.AppendLine("Case Z.Dt When '20' Then Z.Insentif Else 0.0000 End As '20',");
            SQL.AppendLine("Case Z.Dt When '21' Then Z.Insentif Else 0.0000 End As '21',");
            SQL.AppendLine("Case Z.Dt When '22' Then Z.Insentif Else 0.0000 End As '22',");
            SQL.AppendLine("Case Z.Dt When '23' Then Z.Insentif Else 0.0000 End As '23',");
            SQL.AppendLine("Case Z.Dt When '24' Then Z.Insentif Else 0.0000 End As '24',");
            SQL.AppendLine("Case Z.Dt When '25' Then Z.Insentif Else 0.0000 End As '25',");
            SQL.AppendLine("Case Z.Dt When '26' Then Z.Insentif Else 0.0000 End As '26',");
            SQL.AppendLine("Case Z.Dt When '27' Then Z.Insentif Else 0.0000 End As '27', ");
            SQL.AppendLine("Case Z.Dt When '28' Then Z.Insentif Else 0.0000 End As '28',");
            SQL.AppendLine("Case Z.Dt When '29' Then Z.Insentif Else 0.0000 End As '29',");
            SQL.AppendLine("Case Z.Dt When '30' Then Z.Insentif Else 0.0000 End As '30',");
            SQL.AppendLine("Case Z.Dt When '31' Then Z.Insentif Else 0.0000 End As '31' ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.WorkCenterDocNo, 'Work Center Group' As DocName, ");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, D.DeptCode, F.DeptName, B.DocDt, B.DateTrx, G.Dt, ");
            SQL.AppendLine("Sum(B.Insentif) As Insentif ");
            SQL.AppendLine("From  (  ");
            SQL.AppendLine("    Select '' As WorkCenterDocNo, Right(T2.EndDt, 2) As DateTrx,  T2.EndDt As DocDt,  T3.EmpCode, ");
            SQL.AppendLine("    0 As wages, 0 As penalty, 0 As Penaltyleave, 0 As InsentifAmt, T3.InsentifAmt As Insentif ");
            SQL.AppendLine("    From TblInsentif2hdr T2   ");
            SQL.AppendLine("    Inner Join TblInsentif2Dtl T3 On T2.DocNo = T3.DocNo And T2.cancelInd ='N' ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Dt Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All");
            SQL.AppendLine("   Select convert('06' using latin1) Union All");
            SQL.AppendLine("   Select convert('07' using latin1) Union All");
            SQL.AppendLine("   Select convert('08' using latin1) Union All");
            SQL.AppendLine("   Select convert('09' using latin1) Union All");
            SQL.AppendLine("   Select convert('10' using latin1) Union All");
            SQL.AppendLine("   Select convert('11' using latin1) Union All");
            SQL.AppendLine("   Select convert('12' using latin1) Union All ");
            SQL.AppendLine("   Select convert('13' using latin1) Union All ");
            SQL.AppendLine("   Select convert('14' using latin1) Union All ");
            SQL.AppendLine("   Select convert('15' using latin1) Union All ");
            SQL.AppendLine("   Select convert('16' using latin1) Union All ");
            SQL.AppendLine("   Select convert('17' using latin1) Union All ");
            SQL.AppendLine("   Select convert('18' using latin1) Union All ");
            SQL.AppendLine("   Select convert('19' using latin1) Union All ");
            SQL.AppendLine("   Select convert('20' using latin1) Union All ");
            SQL.AppendLine("   Select convert('21' using latin1) Union All ");
            SQL.AppendLine("   Select convert('22' using latin1) Union All ");
            SQL.AppendLine("   Select convert('23' using latin1) Union All ");
            SQL.AppendLine("   Select convert('24' using latin1) Union All ");
            SQL.AppendLine("   Select convert('25' using latin1) Union All ");
            SQL.AppendLine("   Select convert('26' using latin1) Union All ");
            SQL.AppendLine("   Select convert('27' using latin1) Union All ");
            SQL.AppendLine("   Select convert('28' using latin1) Union All ");
            SQL.AppendLine("   Select convert('29' using latin1) Union All ");
            SQL.AppendLine("   Select convert('30' using latin1) Union All ");
            SQL.AppendLine("   Select convert('31' using latin1)  ");
            SQL.AppendLine(") G On G.Dt=B.DateTrx  ");
            SQL.AppendLine("Where Left(B.DocDt, 4) = @Year And substring(B.Docdt, 5, 2)=@Mth  ");
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.DateTrx, B.DocDt, B.WorkCenterDocNo, DocName,");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, F.DeptName");
            SQL.AppendLine("Order By B.WorkCenterDocNo, B.EmpCode, B.DocDt");
            SQL.AppendLine(")Z");
            SQL.AppendLine(")Z3");
            SQL.AppendLine("Group By  Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptName");
            #endregion

            #region insentif
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptCode, Z3.DeptName, 'Earning' As TypeCode, '4' As CodeName, ");
            SQL.AppendLine("SUM(Z3.01), SUM(Z3.02), SUM(Z3.03), SUM(Z3.04), SUM(Z3.05), SUM(Z3.06), SUM(Z3.07), SUM(Z3.08), SUM(Z3.09), SUM(Z3.10), ");
            SQL.AppendLine("SUM(Z3.11), SUM(Z3.12), SUM(Z3.13), SUM(Z3.14), SUM(Z3.15), SUM(Z3.16), SUM(Z3.17), SUM(Z3.18), SUM(Z3.19), SUM(Z3.20), ");
            SQL.AppendLine("SUM(Z3.21), SUM(Z3.22), SUM(Z3.23), SUM(Z3.24), SUM(Z3.25), SUM(Z3.26), SUM(Z3.27), SUM(Z3.28), SUM(Z3.29), SUM(Z3.30), ");
            SQL.AppendLine("SUM(Z3.31), 'Minimum Wages Insentif' As Type ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Z.*, ");
            SQL.AppendLine("Case Z.Dt When '01' Then Z.Insentif Else 0.0000 End As '01', ");
            SQL.AppendLine("Case Z.Dt When '02' Then Z.Insentif Else 0.0000 End As '02', ");
            SQL.AppendLine("Case Z.Dt When '03' Then Z.Insentif Else 0.0000 End As '03',");
            SQL.AppendLine("Case Z.Dt When '04' Then Z.Insentif Else 0.0000 End As '04',");
            SQL.AppendLine("Case Z.Dt When '05' Then Z.Insentif Else 0.0000 End As '05',");
            SQL.AppendLine("Case Z.Dt When '06' Then Z.Insentif Else 0.0000 End As '06',");
            SQL.AppendLine("Case Z.Dt When '07' Then Z.Insentif Else 0.0000 End As '07',");
            SQL.AppendLine("Case Z.Dt When '08' Then Z.Insentif Else 0.0000 End As '08',");
            SQL.AppendLine("Case Z.Dt When '09' Then Z.Insentif Else 0.0000 End As '09',");
            SQL.AppendLine("Case Z.Dt When '10' Then Z.Insentif Else 0.0000 End As '10',");
            SQL.AppendLine("Case Z.Dt When '11' Then Z.Insentif Else 0.0000 End As '11',");
            SQL.AppendLine("Case Z.Dt When '12' Then Z.Insentif Else 0.0000 End As '12',");
            SQL.AppendLine("Case Z.Dt When '13' Then Z.Insentif Else 0.0000 End As '13',");
            SQL.AppendLine("Case Z.Dt When '14' Then Z.Insentif Else 0.0000 End As '14',");
            SQL.AppendLine("Case Z.Dt When '15' Then Z.Insentif Else 0.0000 End As '15',");
            SQL.AppendLine("Case Z.Dt When '16' Then Z.Insentif Else 0.0000 End As '16',");
            SQL.AppendLine("Case Z.Dt When '17' Then Z.Insentif Else 0.0000 End As '17',");
            SQL.AppendLine("Case Z.Dt When '18' Then Z.Insentif Else 0.0000 End As '18',");
            SQL.AppendLine("Case Z.Dt When '19' Then Z.Insentif Else 0.0000 End As '19',");
            SQL.AppendLine("Case Z.Dt When '20' Then Z.Insentif Else 0.0000 End As '20',");
            SQL.AppendLine("Case Z.Dt When '21' Then Z.Insentif Else 0.0000 End As '21',");
            SQL.AppendLine("Case Z.Dt When '22' Then Z.Insentif Else 0.0000 End As '22',");
            SQL.AppendLine("Case Z.Dt When '23' Then Z.Insentif Else 0.0000 End As '23',");
            SQL.AppendLine("Case Z.Dt When '24' Then Z.Insentif Else 0.0000 End As '24',");
            SQL.AppendLine("Case Z.Dt When '25' Then Z.Insentif Else 0.0000 End As '25',");
            SQL.AppendLine("Case Z.Dt When '26' Then Z.Insentif Else 0.0000 End As '26',");
            SQL.AppendLine("Case Z.Dt When '27' Then Z.Insentif Else 0.0000 End As '27', ");
            SQL.AppendLine("Case Z.Dt When '28' Then Z.Insentif Else 0.0000 End As '28',");
            SQL.AppendLine("Case Z.Dt When '29' Then Z.Insentif Else 0.0000 End As '29',");
            SQL.AppendLine("Case Z.Dt When '30' Then Z.Insentif Else 0.0000 End As '30',");
            SQL.AppendLine("Case Z.Dt When '31' Then Z.Insentif Else 0.0000 End As '31' ");   
            SQL.AppendLine("From( ");
            SQL.AppendLine("Select B.WorkCenterDocNo, C.DocName, ");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, D.DeptCode, F.DeptName, B.DocDt, B.DateTrx, G.Dt, ");
            SQL.AppendLine("Sum(B.InsentifAmt) As Insentif ");
            SQL.AppendLine("From  (  ");
            SQL.AppendLine("    Select T1.WorkCenterDocNo, Right(T1.DocDt, 2) As DateTrx, T1.DocDt,  T3.EmpCode,");
	        SQL.AppendLine("     0 As wages, 0 As penalty, 0 As Penaltyleave, T3.InsentifAmt");
	        SQL.AppendLine("     From TblPwghdr T1");
	        SQL.AppendLine("     Inner join TblInsentifhdr T2 On T1.DocNo = T2.PwgDocNo And T2.cancelInd ='N' ");
	        SQL.AppendLine("     Inner Join TblInsentifDtl T3 On T2.DocNo = T3.DocNo ");
	        SQL.AppendLine("     Where T1.cancelInd = 'N'");
	        SQL.AppendLine("     Group BY T1.DocDt, T1.WorkCenterDocNo, T3.EmpCode");
            SQL.AppendLine(") B ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee D On B.EmpCode=D.EmpCode  ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("   Select convert('01' using latin1) As Dt Union All ");
            SQL.AppendLine("   Select convert('02' using latin1) Union All ");
            SQL.AppendLine("   Select convert('03' using latin1) Union All ");
            SQL.AppendLine("   Select convert('04' using latin1) Union All ");
            SQL.AppendLine("   Select convert('05' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('06' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('07' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('08' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('09' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('10' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('11' using latin1) Union All"); 
            SQL.AppendLine("   Select convert('12' using latin1) Union All ");
            SQL.AppendLine("   Select convert('13' using latin1) Union All ");
            SQL.AppendLine("   Select convert('14' using latin1) Union All ");
            SQL.AppendLine("   Select convert('15' using latin1) Union All ");
            SQL.AppendLine("   Select convert('16' using latin1) Union All ");
            SQL.AppendLine("   Select convert('17' using latin1) Union All ");
            SQL.AppendLine("   Select convert('18' using latin1) Union All ");
            SQL.AppendLine("   Select convert('19' using latin1) Union All ");
            SQL.AppendLine("   Select convert('20' using latin1) Union All ");
            SQL.AppendLine("   Select convert('21' using latin1) Union All ");
            SQL.AppendLine("   Select convert('22' using latin1) Union All ");
            SQL.AppendLine("   Select convert('23' using latin1) Union All ");
            SQL.AppendLine("   Select convert('24' using latin1) Union All ");
            SQL.AppendLine("   Select convert('25' using latin1) Union All ");
            SQL.AppendLine("   Select convert('26' using latin1) Union All ");
            SQL.AppendLine("   Select convert('27' using latin1) Union All ");
            SQL.AppendLine("   Select convert('28' using latin1) Union All ");
            SQL.AppendLine("   Select convert('29' using latin1) Union All ");
            SQL.AppendLine("   Select convert('30' using latin1) Union All ");
            SQL.AppendLine("   Select convert('31' using latin1)  ");
            SQL.AppendLine(") G On G.Dt=B.DateTrx  ");
            SQL.AppendLine("Where Left(B.DocDt, 4) = @Year And substring(B.Docdt, 5, 2)=@Mth  ");
            SQL.AppendLine("Group By ");
            SQL.AppendLine("B.DateTrx, B.DocDt, B.WorkCenterDocNo, C.DocName,");
            SQL.AppendLine("B.EmpCode, D.EmpName, E.PosName, F.DeptName");
            SQL.AppendLine("Order By B.WorkCenterDocNo, B.EmpCode, B.DocDt");
            SQL.AppendLine(")Z");
            SQL.AppendLine(")Z3");
            SQL.AppendLine("Group By  Z3.WorkcenterDocNo, Z3.DocName, Z3.EmpCode, Z3.EmpName, Z3.PosName, Z3.DeptName");
            #endregion

            SQL.AppendLine(")Tbl ");
            //SQL.AppendLine("Group By  Tbl.WorkcenterDocNo, Tbl.DocName, Tbl.EmpCode, Tbl.EmpName, Tbl.PosName, Tbl.DeptName, TypeCode");

            return SQL.ToString();
        }

        private void ClearData()
        {
            Sm.FormatNumTxt(TxtAmt, 0);
            Sm.FormatNumTxt(TxtAmtEmp, 0);
            Sm.FormatNumTxt(TxtAvg, 0);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Work Center",
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department", 
                        //6-10
                        "Type",
                        "Amount",
                        "01",
                        "02",
                        "03",
                        //11-15
                        "04",
                        "05",
                        "06",
                        "07",
                        "08",
                        //16-20
                        "09",
                        "10",
                        "11",
                        "12",
                        "13",
                        //21-25
                        "14",
                        "15",
                        "16",
                        "17",
                        "18",
                        //26-30
                        "19",
                        "20",
                        "21",
                        "22",
                        "23",
                        //31-35
                        "24",
                        "25",
                        "26",
                        "27",
                        "28",
                        //36-39
                        "29",
                        "30",
                        "31",
                        "Transaction",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 150, 150, 150, 
                        //6-10
                        80, 150, 80, 80, 80, 
                        //11-15
                        80, 80, 80, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 80,
                        //21-25
                        80, 80, 80, 80, 80,
                        //26-30
                        80, 80, 80, 80, 80,
                        //31-35
                        80, 80, 80, 80, 80,
                        //36
                        80, 80, 80, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
                22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            ClearData();
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);

            if (Year == "")
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                ClearData();
            }
            else if (Month == "")
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                ClearData();
            }

            else
            {
                try
                {
                    ClearData();
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " Where 0=0 ";

                    var cm = new MySqlCommand();

                    Sm.CmParam<Decimal>(ref cm, "@DirectLaborDailyWorkHour", mDirectLaborDailyWorkHour);
                    Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));

                    string Filter2 = Filter;
                    Sm.FilterStr(ref Filter2, ref cm, TxtWorkCenterDocNo.Text, "Tbl.DocName", false);
                    Sm.FilterStr(ref Filter2, ref cm, TxtEmpCode.Text, new string[] { "Tbl.EmpCode", "Tbl.EmpName" });
                    Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueDeptCode), "Tbl.DeptCode", true);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            GetSQL(Filter) + Filter2 +
                            " Group By Tbl.WorkcenterDocNo, Tbl.DocName, Tbl.EmpCode, Tbl.EmpName, Tbl.PosName, Tbl.DeptName, Tbl.TypeCode, Tbl.Type  " +
                            " Order By Tbl.EmpCode, Tbl.WorkcenterDocNo, Tbl.DocName,  Tbl.CodeName, Tbl.Type ;",
                            new string[]
                        {
                            //0
                            "WorkCenterDocNo", 

                            //1-5
                            "DocName", "EmpCode", "EmpName", "PosName", "DeptName", 
                            
                            //6
                            "TypeCode", "01", "02", "03", "04",

                            //
                            "05", "06", "07", "08", "09",

                            //
                            "10", "11", "12", "13", "14",

                            //
                            "15", "16", "17", "18", "19",

                            //
                            "20", "21", "22", "23", "24",

                            //
                            "25", "26", "27", "28", "29",

                            //
                            "30", "31", "Type"

                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);


                                Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 29);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 31);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 32);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 35);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 36);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 37);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                                //if (Sm.GetGrdStr(Grd1, Row, 6) == "Deduction")
                                //{
                                    decimal Amt = 0;
                                    for (int col = 8; col < 38; col++)
                                    {
                                        Amt = Amt + Sm.GetGrdDec(Grd1, Row, col);
                                    }
                                    Grd1.Cells[Row, 7].Value = Amt;
                                //}
                                //else
                                //{
                                //    decimal Amt = 0;
                                //    for (int col = 8; col < 38; col++)
                                //    {
                                //        Amt = Amt + Sm.GetGrdDec(Grd1, Row, col);
                                //    }
                                //    Grd1.Cells[Row, 7].Value = Amt;
                                //}
                            }, true, false, false, false
                        );
                    ComputeEmployee();
                    ComputeTotal();
                    Grd1.GroupObject.Add(1);
                    Grd1.GroupObject.Add(6);
                    Grd1.GroupObject.Add(39);
                    Grd1.Group();
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 
                    30, 31, 32, 33, 34, 35, 36, 37, 38});
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 0);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void ComputeTotal()
        {
            decimal amt = 0;
            decimal emp = 0;
            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        amt += Sm.GetGrdDec(Grd1, Row, 7);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(amt, 0);
            emp = Decimal.Parse(TxtAmtEmp.Text);
            if (emp != 0)
            {
                TxtAvg.EditValue = Sm.FormatNum(amt / emp, 0);
            }
            else
            {
                TxtAvg.EditValue = 0m;
            }
        }

        private void ComputeEmployee()
        {
            decimal EmpAmt = 0;
            string EmpCode = "";
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2) == EmpCode)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    }
                    else
                    {
                        EmpAmt = EmpAmt + 1;
                        EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
                    }
                }
            }
            else
            {
                EmpAmt = 1;
            }
            TxtAmtEmp.EditValue = Sm.FormatNum(EmpAmt, 0);
        }

        #endregion

        #region Event
        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Workcenter");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void TxAmtEmp_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmtEmp, 0);
        }

        private void TxtAvg_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAvg, 0);
        }
        #endregion
    }
}
