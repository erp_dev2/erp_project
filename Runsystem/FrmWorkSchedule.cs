﻿#region Update
/*
    06/11/2017 [TKG] tambah site
    08/02/2020 [TKG/IMS] tambah break 2
    21/02/2020 [HAR/IMS] bug Show Data
    04/12/2020 [TKG/IMS] tambah deduction rounding
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmWorkSchedule : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mWSCode = string.Empty;
        internal FrmWorkScheduleFind FrmFind;
        private bool 
            mIsWorkScheduleBasedOnSite = false,
            mIsWorkScheduleDeductionRoundValidationEnabled = false;

        #endregion

        #region Constructor

        public FrmWorkSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Work Schedule";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueOption(ref LueDeductionRoundCode, "DeductionRound");
                if (mIsWorkScheduleBasedOnSite) LblSiteCode.ForeColor = Color.Red;
                SetFormControl(mState.View);
                if (mWSCode.Length != 0)
                {
                    ShowData(mWSCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();

        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSCode, TxtWSName, ChkActInd, ChkHolidayInd, ChkDoubleShiftInd, 
                        TmeIn1, TmeOut1, TmeIn2, TmeOut2, TmeIn3, 
                        TmeOut3, MeeRemark, DurIn1, DurIn2, DurOut1, 
                        DurOut2, DurBreakIn1, DurBreakIn2, DurBreakOut1, DurBreakOut2, 
                        DurOTIn1, DurOTIn2, DurOTOut1, DurOTOut2, LueSiteCode,
                        TmeIn4, TmeOut4, DurBreakIn3, DurBreakIn4, DurBreakOut3, 
                        DurBreakOut4, LueDeductionRoundCode 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtWSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSCode, TxtWSName, ChkHolidayInd, ChkDoubleShiftInd, TmeIn1, 
                        TmeOut1, TmeIn2, TmeOut2, TmeIn3, TmeOut3, 
                        MeeRemark, DurIn1, DurIn2, DurOut1, DurOut2, 
                        DurBreakIn1, DurBreakIn2, DurBreakOut1, DurBreakOut2, DurOTIn1, 
                        DurOTIn2, DurOTOut1, DurOTOut2, LueSiteCode, TmeIn4, 
                        TmeOut4, DurBreakIn3, DurBreakIn4, DurBreakOut3, DurBreakOut4,
                        LueDeductionRoundCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtWSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWSName, ChkActInd, ChkHolidayInd, ChkDoubleShiftInd, TmeIn1, 
                        TmeOut1, TmeIn2, TmeOut2, TmeIn3, TmeOut3, 
                        MeeRemark, DurIn1, DurIn2, DurOut1, DurOut2, 
                        DurBreakIn1, DurBreakIn2, DurBreakOut1, DurBreakOut2, DurOTIn1, 
                        DurOTIn2,DurOTOut1, DurOTOut2, LueSiteCode, TmeIn4, 
                        TmeOut4, DurBreakIn3, DurBreakIn4, DurBreakOut3, DurBreakOut4,
                        LueDeductionRoundCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtWSName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtWSCode, TxtWSName, TmeIn1, TmeOut1, TmeIn2, 
                TmeOut2, TmeIn3, TmeOut3, MeeRemark, DurIn1,
                DurIn2, DurOut1, DurOut2, DurBreakIn1, DurBreakIn2,
                DurBreakOut1, DurBreakOut2, DurOTIn1, DurOTIn2, DurOTOut1,
                DurOTOut2, LueSiteCode, TmeIn4, TmeOut4, DurBreakIn3, 
                DurBreakIn4, DurBreakOut3, DurBreakOut4, LueDeductionRoundCode 
            });
            ChkActInd.Checked = false;
            ChkHolidayInd.Checked = false;
            ChkDoubleShiftInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] { "", "Code", "Name", "Type" },
                    new int[] { 20, 80, 250, 80 }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWorkScheduleFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                SetLueSiteCode(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWSCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWSCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWorkSchedule Where WSCode=@WSCode" };
                Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsInsert = !TxtWSCode.Properties.ReadOnly;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveWorkSchedule());
                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SaveWorkScheduleAD(Row));
                }
                Sm.ExecCommands(cml);

                if (IsInsert)
                    BtnInsertClick(sender, e);
                else
                    ShowData(TxtWSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string WSCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowWorkSchedule(WSCode);
                ShowWorkScheduleAD(WSCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWorkSchedule(string WSCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WSCode, WSName, ActInd, HolidayInd, DoubleShiftInd, ");
            SQL.AppendLine("In1, bIn1, aIn1, Out1, bOut1, aOut1, ");
            SQL.AppendLine("In2, bIn2, aIn2, Out2, bOut2, aOut2, ");
            SQL.AppendLine("In3, bIn3, aIn3, Out3, bOut3, aOut3, ");
            SQL.AppendLine("In4, bIn4, aIn4, Out4, bOut4, aOut4, ");
            SQL.AppendLine("SiteCode, DeductionRoundCode, Remark ");
            SQL.AppendLine("From TblWorkSchedule Where WSCode=@WSCode;");

            Sm.CmParam<String>(ref cm, "@WSCode", WSCode);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "WSCode", 
                            
                            //1-5
                            "WSName", "ActInd", "HolidayInd", "DoubleShiftInd", "In1", 
                            
                            //6-10
                            "bIn1", "aIn1", "Out1", "bOut1", "aOut1", 
                            
                            //11-15
                            "In2", "bIn2", "aIn2", "Out2", "bOut2", 
                            
                            //16-20
                            "aOut2", "In3", "bIn3", "aIn3", "Out3", 
                            
                            //21-25
                            "bOut3", "aOut3", "In4", "bIn4", "aIn4", 
                            
                            //26-30
                            "Out4", "bOut4", "aOut4", "SiteCode", "DeductionRoundCode", 
                            
                            //31
                            "Remark"
                           
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtWSCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtWSName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        ChkHolidayInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        ChkDoubleShiftInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        Sm.SetTme(TmeIn1, Sm.DrStr(dr, c[5]));
                        Sm.SetTme(DurIn1, Sm.DrStr(dr, c[6]));
                        Sm.SetTme(DurIn2, Sm.DrStr(dr, c[7]));
                        Sm.SetTme(TmeOut1, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(DurOut1, Sm.DrStr(dr, c[9]));
                        Sm.SetTme(DurOut2, Sm.DrStr(dr, c[10]));
                        Sm.SetTme(TmeIn2, Sm.DrStr(dr, c[11]));
                        Sm.SetTme(DurBreakIn1, Sm.DrStr(dr, c[12]));
                        Sm.SetTme(DurBreakIn2, Sm.DrStr(dr, c[13]));
                        Sm.SetTme(TmeOut2, Sm.DrStr(dr, c[14]));
                        Sm.SetTme(DurBreakOut1, Sm.DrStr(dr, c[15]));
                        Sm.SetTme(DurBreakOut2, Sm.DrStr(dr, c[16]));
                        Sm.SetTme(TmeIn3, Sm.DrStr(dr, c[17]));
                        Sm.SetTme(DurOTIn1, Sm.DrStr(dr, c[18]));
                        Sm.SetTme(DurOTIn2, Sm.DrStr(dr, c[19]));
                        Sm.SetTme(TmeOut3, Sm.DrStr(dr, c[20]));
                        Sm.SetTme(DurOTOut1, Sm.DrStr(dr, c[21]));
                        Sm.SetTme(DurOTOut2, Sm.DrStr(dr, c[22]));
                        Sm.SetTme(TmeIn4, Sm.DrStr(dr, c[23]));
                        Sm.SetTme(DurBreakIn3, Sm.DrStr(dr, c[24]));
                        Sm.SetTme(DurBreakIn4, Sm.DrStr(dr, c[25]));
                        Sm.SetTme(TmeOut4, Sm.DrStr(dr, c[26]));
                        Sm.SetTme(DurBreakOut3, Sm.DrStr(dr, c[27]));
                        Sm.SetTme(DurBreakOut4, Sm.DrStr(dr, c[28]));
                        SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueDeductionRoundCode, Sm.DrStr(dr, c[30]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[31]);
                    }, true
                );
        }

        private void ShowWorkScheduleAD(string WSCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WSCode", WSCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ADCode, B.ADName, C.OptDesc As ADTypeDesc ");
            SQL.AppendLine("From TblWorkScheduleAD A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Inner Join TblOption C On B.ADType=C.OptCode And C.OptCat='ADType' "); 
            SQL.AppendLine("Where A.WSCode=@WSCode ");
            SQL.AppendLine("Order By B.ADName;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "ADCode", "ADName", "ADTypeDesc" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWSCode, "Work schedule's code", false) ||
                Sm.IsTxtEmpty(TxtWSName, "Work schedule's name", false) ||
                IsEmpty() ||
                IsWSCodeExisted() ||
                (mIsWorkScheduleBasedOnSite && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                (mIsWorkScheduleDeductionRoundValidationEnabled && !ChkHolidayInd.Checked && Sm.IsLueEmpty(LueDeductionRoundCode, "Deduction rounding")) ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Allowance/deduction is empty.")) return true;
            }
            return false;
        }

        private bool IsEmpty()
        {
            if (!ChkHolidayInd.Checked && TmeIn1.EditValue != null)
            {
                if (DurIn1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Check in (before) is empty");
                    return true;
                }
                if (DurIn2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Check in (after) is empty");
                    return true;
                }
            }

            if (!ChkHolidayInd.Checked && TmeOut1.EditValue != null)
            {
                if (DurOut1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Check out (before) is empty");
                    return true;
                }
                if (DurOut2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Check out (after) is empty");
                    return true;
                }
            }

            if (!ChkHolidayInd.Checked && TmeIn2.EditValue != null)
            {
                if (DurBreakIn1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Break in (before) is empty");
                    return true;
                }
                if (DurBreakIn2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Break in (after) is empty");
                    return true;
                }
            }

            if (!ChkHolidayInd.Checked && TmeOut2.EditValue != null)
            {
                if (DurBreakOut1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Break out(before) is empty");
                    return true;
                }
                if (DurBreakOut2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Break out(after) is empty");
                    return true;
                }
            }

            if (!ChkHolidayInd.Checked && TmeIn3.EditValue != null)
            {
                if (DurOTIn1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Routine OT in (before) is empty");
                    return true;
                }
                if (DurOTIn2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Routine OT in (after) is empty");
                    return true;
                }
            }

            if (!ChkHolidayInd.Checked && TmeOut3.EditValue != null)
            {
                if (DurOTOut1.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Routine OT out (after) is empty");
                    return true;
                }
                if (DurOTOut2.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Routine OT out (after) is empty");
                    return true;
                }
            }
            return false;
        }

        private bool IsWSCodeExisted()
        {
            if (!TxtWSCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select 1 From TblWorkSchedule Where WSCode=@WSCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Work schedule code ( " + TxtWSCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveWorkSchedule()
        {
            var SQL = new StringBuilder();

            if (TxtWSCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblWorkScheduleAD Where WSCode=@WSCode;");

            SQL.AppendLine("Insert Into TblWorkSchedule(WSCode, WSName, ActInd, HolidayInd, DoubleShiftInd, ");
            SQL.AppendLine("In1, bIn1, aIn1, Out1, bOut1, aOut1, In2, bIn2, aIn2, Out2, bOut2, aOut2, In3, bIn3, aIn3, Out3, bOut3, aOut3, In4, bIn4, aIn4, Out4, bOut4, aOut4, ");
            SQL.AppendLine("SiteCode, DeductionRoundCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WSCode, @WSName, @ActInd, @HolidayInd, @DoubleShiftInd, ");
            SQL.AppendLine("@In1,  @bIn1, @aIn1, @Out1, @bOut1, @aOut1, @In2, @bIn2, @aIn2, @Out2, @bOut2, @aOut2, @In3, @bIn3, @aIn3, @aOut3, @bOut3, @aOut3, @In4, @bIn4, @aIn4, @Out4, @bOut4, @aOut4, ");
            SQL.AppendLine("@SiteCode, @DeductionRoundCode, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update WSName=@WSName, ActInd=@ActInd, HolidayInd=@HolidayInd, DoubleShiftInd=@DoubleShiftInd, ");
            SQL.AppendLine("   In1=@In1, bIn1=@bin1, aIn1=@aIn1, ");
            SQL.AppendLine("   Out1=@Out1, bOut1=@bOut1, aOut1=@aOut1, In2=@In2, bIn2=@bIn2, aIn2=@aIn2, Out2=@Out2, bOut2=@bOut2, aOut2=@aOut2, In3=@In3, bIn3=@bIn3, aIn3=@aIn3, Out3=@Out3, bOut3=@bOut3, aOut3=@aOut3, In4=@In4, bIn4=@bIn4, aIn4=@aIn4, Out4=@Out4, bOut4=@bOut4, aOut4=@aOut4, ");
            SQL.AppendLine("   SiteCode=@SiteCode, DeductionRoundCode=@DeductionRoundCode, Remark=@Remark, ");
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);
            Sm.CmParam<String>(ref cm, "@WSName", TxtWSName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@HolidayInd", ChkHolidayInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DoubleShiftInd", ChkDoubleShiftInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@In1", Sm.GetTme(TmeIn1));
            Sm.CmParam<String>(ref cm, "@bIn1", Sm.GetTme(DurIn1));
            Sm.CmParam<String>(ref cm, "@aIn1", Sm.GetTme(DurIn2));
            Sm.CmParam<String>(ref cm, "@Out1", Sm.GetTme(TmeOut1));
            Sm.CmParam<String>(ref cm, "@bOut1", Sm.GetTme(DurOut1));
            Sm.CmParam<String>(ref cm, "@aOut1", Sm.GetTme(DurOut2));
            Sm.CmParam<String>(ref cm, "@In2", Sm.GetTme(TmeIn2));
            Sm.CmParam<String>(ref cm, "@bIn2", Sm.GetTme(DurBreakIn1));
            Sm.CmParam<String>(ref cm, "@aIn2", Sm.GetTme(DurBreakIn2));
            Sm.CmParam<String>(ref cm, "@Out2", Sm.GetTme(TmeOut2));
            Sm.CmParam<String>(ref cm, "@bOut2", Sm.GetTme(DurBreakOut1));
            Sm.CmParam<String>(ref cm, "@aOut2", Sm.GetTme(DurBreakOut2));
            Sm.CmParam<String>(ref cm, "@In3", Sm.GetTme(TmeIn3));
            Sm.CmParam<String>(ref cm, "@bIn3", Sm.GetTme(DurOTIn1));
            Sm.CmParam<String>(ref cm, "@aIn3", Sm.GetTme(DurOTIn2));
            Sm.CmParam<String>(ref cm, "@Out3", Sm.GetTme(TmeOut3));
            Sm.CmParam<String>(ref cm, "@bOut3", Sm.GetTme(DurOTOut1));
            Sm.CmParam<String>(ref cm, "@aOut3", Sm.GetTme(DurOTOut2));
            Sm.CmParam<String>(ref cm, "@In4", Sm.GetTme(TmeIn4));
            Sm.CmParam<String>(ref cm, "@bIn4", Sm.GetTme(DurBreakIn3));
            Sm.CmParam<String>(ref cm, "@aIn4", Sm.GetTme(DurBreakIn4));
            Sm.CmParam<String>(ref cm, "@Out4", Sm.GetTme(TmeOut4));
            Sm.CmParam<String>(ref cm, "@bOut4", Sm.GetTme(DurBreakOut3));
            Sm.CmParam<String>(ref cm, "@aOut4", Sm.GetTme(DurBreakOut4));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeductionRoundCode", Sm.GetLue(LueDeductionRoundCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWorkScheduleAD(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWorkScheduleAD(WSCode, ADCode, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select WSCode, @ADCode, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblWorkSchedule Where WSCode=@WSCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WSCode", TxtWSCode.Text);
            Sm.CmParam<String>(ref cm, "@ADCode", Sm.GetGrdStr(Grd1, Row, 1));
            
            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsWorkScheduleBasedOnSite = Sm.GetParameterBoo("IsWorkScheduleBasedOnSite");
            mIsWorkScheduleDeductionRoundValidationEnabled = Sm.GetParameterBoo("IsWorkScheduleDeductionRoundValidationEnabled");
        }

        internal string GetSelectedADCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.SiteCode=@Code Or T.ActInd='Y') ");
                SQL.AppendLine("Order By T.SiteName;");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                SQL.AppendLine("Order By T.SiteName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWSCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWSCode);
        }

        private void TxtWSName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtWSName);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(SetLueSiteCode), string.Empty);
        }

        private void LueDeductionRoundCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeductionRoundCode, new Sm.RefreshLue2(Sl.SetLueOption), "DeductionRound");
        }

        #endregion

        #region Grid Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWorkScheduleDlg(this, Grd1));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmWorkScheduleDlg(this, Grd1));
        }

        #endregion

        #endregion
    }
}
