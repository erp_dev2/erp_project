﻿#region Update
/* 
    03/05/2017 [TKG] menampilkan data dengan qty > 0 untuk filter back date
    18/10/2017 [TKG] tambah filter batch#
    14/01/2018 [TKG] tambah filter item
    06/02/2018 [TKG] tambah filter dan informasi entity
    10/07/2018 [WED] tambah filter multi warehouse
    16/05/2019 [WED] tambah fitur tampilkan 0 stock, berdasarkan parameter IsStockSummaryShowZeroStock
    22/05/2019 [WED] IsStockSummaryShowZeroStock juga menampilkan selain 0
    06/08/2019 [TKG] bug proses multi warehouse
    29/04/2020 [IBL/IMS] Penambahan field Nama Project dan Nomer PO Customer berdasarkan parameter IsRptStockSumary3ShowProjectInfo
    28/12/2020 [VIN/SRN] mengganti label kolom, qty --> qty1, uom --> uom1
    19/01/2021 [ICA/IMS] menambah kolom specification based on parameter IsBOMShowSpecifications
    11/03/2021 [TKG/IMS] menambah heat number
    22/04/2021 [VIN/IMS] bug: GetSQL2, tambah select BatcNo
    05/07/2021 [TKG/IMS] utk item category yg menggunakan moving average, currency, price, dan rate-nya menggunakan data moving avg
    06/07/2021 [VIN/IMS] Bug moving avg GetSQL1 kurang ifnull
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockSummary3 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mMainCurCode = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool 
            IsReCompute = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsStockSummaryShowZeroStock = false,
            mIsRptStockSummary3ShowProjectInfo = false,
            mIsBOMShowSpecifications = false,
            mIsStockSummary3HeatNumberEnabled = false,
            mIsMovingAvgEnabled = false
            ;
        #endregion

        #region Constructor

        public FrmRptStockSummary3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsStockSummaryShowZeroStock)
                {
                    LblZeroStock.Visible = false;
                    ChkZeroStock.Visible = false;
                }
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueEntCode(ref LueEntCode);

                SetCcbWhsName(ref CcbWhsName, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL1(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 3-1 */ ");

            SQL.AppendLine("Select B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, C.Specification, A.BatchNo, A.Source, D.ItCtName, ");
            SQL.AppendLine("A.Qty, C.InventoryUomCode, A.Qty2, C.InventoryUOMCode2, A.Qty3, C.InventoryUOMCode3, ");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then IfNull(M.MovingAvgCurCode, 0.00) Else E.CurCode End As CurCode, ");
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then IfNull(M.MovingAvgPrice, 0.00) Else E.Uprice End As Uprice, ");
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then 1.00 Else E.ExcRate End As ExcRate, ");
            }
            else
                SQL.AppendLine("E.CurCode, E.Uprice, E.ExcRate, ");
            SQL.AppendLine("C.ItGrpCode, F.ItGrpName, G.ItScName, J.EntName ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine(", K.ProjectName, L.Value1 As PONo ");
            else
                SQL.AppendLine(", Null As ProjectName, Null As PONo ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine(", L.Value2 As HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On A.Source=E.Source ");
            SQL.AppendLine("Left Join TblItemGroup F On C.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory G On C.ItScCode = G.ItScCode ");
            SQL.AppendLine("Left Join TblCostCenter H On B.CCCode=H.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter I On H.ProfitCenterCode=I.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity J On I.EntCode=J.EntCode ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine("Left Join TblProjectGroup K On A.BatchNo=K.ProjectCode ");
            if (mIsRptStockSummary3ShowProjectInfo || mIsStockSummary3HeatNumberEnabled)
                SQL.AppendLine("Left Join TblSourceInfo L On A.Source=L.Source ");
            if (mIsMovingAvgEnabled)
                SQL.AppendLine("Left Join TblItemMovingAvg M On A.ItCode=M.ItCode ");
            SQL.AppendLine("Where 1=1 ");

            if (!ChkZeroStock.Checked) SQL.AppendLine("And A.Qty>0 ");
            else SQL.AppendLine("And (A.Qty = 0 Or A.Qty > 0) ");

            SQL.AppendLine(Filter);
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By B.WhsName, C.ItName; ");

            return SQL.ToString();
        }

        private string GetSQL2(string DocDt, string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 3-2 */ ");
            SQL.AppendLine("Select B.WhsName, A.Lot, A.Bin, A.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, C.Specification, E.BatchNo, A.Source, D.ItCtName, ");
            SQL.AppendLine("A.Qty, C.InventoryUomCode, A.Qty2, C.InventoryUOMCode2, A.Qty3, C.InventoryUOMCode3, ");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then IfNull(M.MovingAvgCurCode, 0.00)  Else E.CurCode End As CurCode, ");
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then IfNull(M.MovingAvgPrice, 0.00) Else E.Uprice End As Uprice, ");
                SQL.AppendLine("Case When D.MovingAvgInd='Y' Then 1.00 Else E.ExcRate End As ExcRate, ");
            }
            else
                SQL.AppendLine("E.CurCode, E.Uprice, E.ExcRate, ");
            SQL.AppendLine("C.ItGrpCode, F.ItGrpName, G.ItScName, J.EntName ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine(", K.ProjectName, L.Value1 As PONo ");
            else
                SQL.AppendLine(", Null As ProjectName, Null As PONo ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine(", L.Value2 As HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, ItCode, Source, A.BatchNo, ");
            SQL.AppendLine("    Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Where A.DocDt<=@DocDt " + Filter);
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=A.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Group By WhsCode, Lot, Bin, ItCode, Source ");
            if (!ChkZeroStock.Checked) SQL.AppendLine("    Having Sum(A.Qty)<>0.00 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On A.Source=E.Source ");
            SQL.AppendLine("Left Join TblItemGroup F On C.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory G On C.ItScCode = G.ItScCode ");
            SQL.AppendLine("Left Join TblCostCenter H On B.CCCode=H.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter I On H.ProfitCenterCode=I.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity J On I.EntCode=J.EntCode ");
            if (mIsRptStockSummary3ShowProjectInfo)
                SQL.AppendLine("Left Join TblProjectGroup K On A.BatchNo=K.ProjectCode ");
            if (mIsRptStockSummary3ShowProjectInfo || mIsStockSummary3HeatNumberEnabled)
                SQL.AppendLine("Left Join TblSourceInfo L On A.Source=L.Source ");
            if (mIsMovingAvgEnabled)
                SQL.AppendLine("Left Join TblItemMovingAvg M On A.ItCode=M.ItCode ");
            SQL.AppendLine("Where 1=1 ");
            if (!ChkZeroStock.Checked) SQL.AppendLine("And A.Qty>0.00 ");
            else SQL.AppendLine("And (A.Qty = 0.00 Or A.Qty > 0.00) ");
            SQL.AppendLine("Order By B.WhsName, A.ItCode; ");

            return SQL.ToString();
        }

        private void GetParameter()
        {
            panel4.Visible = Sm.GetParameterBoo("IsRptStockSummaryShowFilterByDocDt");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsStockSummaryShowZeroStock = Sm.GetParameterBoo("IsStockSummaryShowZeroStock");
            mIsRptStockSummary3ShowProjectInfo = Sm.GetParameterBoo("IsRptStockSummary3ShowProjectInfo");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsStockSummary3HeatNumberEnabled = Sm.GetParameterBoo("IsStockSummary3HeatNumberEnabled");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Lot",
                        "Bin",   
                        "Item's Code", 
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Foreign Name",
                        "Batch#",
                        "Source",

                        //11-15
                        "Category", 
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        
                        //16-20
                        "Quantity", 
                        "UoM",
                        "Currency",
                        "Price",
                        "Rate",

                        //21-25
                        "Total",
                        "Item Group"+Environment.NewLine+"Code",
                        "Item Group"+Environment.NewLine+"Name",
                        "Sub-Category",
                        "Entity",

                        //26-29
                        "Project's Name",
                        "Customer's PO#",
                        "Specification",
                        "Heat Number"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 50, 50, 80, 20, 
                        
                        //6-10
                        80, 200, 230, 200, 180,

                        //11-15
                        150, 130, 70, 130, 70,

                        //16-20
                        130, 70, 60, 100, 80,
                        
                        //21-25
                        200, 100, 150, 150, 200,

                        //26-29
                        200, 130, 200, 200
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 19, 20, 21 }, 0);
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 10, 11, 14, 15, 16, 17, 20, 24 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 8, 10, 11, 14, 15, 16, 17, 20, 24 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 29 });
            if (mIsItGrpCode)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23 }, false);
            }
            Grd1.Cols[22].Move(9);
            Grd1.Cols[23].Move(10);
            Grd1.Cols[24].Move(11);
            if (!mIsRptStockSummary3ShowProjectInfo) Sm.GrdColInvisible(Grd1, new int[] { 26, 27 });
            if (mIsBOMShowSpecifications)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 6, 28 }, true);
                Grd1.Cols[28].Move(8);
            }
            if (mIsStockSummary3HeatNumberEnabled)
                Grd1.Cols[29].Move(15);
            else
                Grd1.Cols[29].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 10, 11, 20, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        override protected void ShowData()
        {
            if (ChkWhsName.Checked && ChkWhsCode.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Please just use Multi Warehouse or Warehouse filter, instead of using both of them.");
                CcbWhsName.Focus();
                return;
            }

            if (Sm.IsDteEmpty(DteDocDt, "Filtering data by date is empty.")) return;

            Cursor.Current = Cursors.WaitCursor;

            string
                DocDt = Sm.GetDte(DteDocDt).Substring(0, 8),
                Filter = " ";

            try
            {
                var cm = new MySqlCommand();
                IsReCompute = false;

                Sm.CmParamDt(ref cm, "@DocDt", DocDt);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                if (ChkWhsName.Checked)
                    FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "A.WhsCode", "_2");
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                
                if (ChkItCtCode.Checked || ChkItName.Checked)
                {
                    Filter += " And A.ItCode In (";
                    Filter += "     Select ItCode From TblItem ";
                    Filter += "     Where  1=1 ";
                    if (ChkItCtCode.Checked)
                        Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "ItCtCode", true);
                    if (ChkItName.Checked)
                        Sm.FilterStr(ref Filter, ref cm, TxtItName.Text, new string[] { "ItCode", "ItName" });
                    Filter += " ) ";
                }

                if (ChkEntCode.Checked)
                {
                    Filter += " And A.WhsCode In (";
                    Filter += "     Select T1.WhsCode ";
                    Filter += "     From TblWarehouse T1 ";
                    Filter += "     Inner Join TblCostCenter T2 On T1.CCCode=T2.CCCode And T2.ProfitCenterCode Is Not Null ";
                    Filter += "     Inner Join TblProfitCenter T3 On T2.ProfitCenterCode=T3.ProfitCenterCode And T3.EntCode Is Not Null And T3.EntCode=@EntCode ";
                    Filter += "     Where T1.CCCode Is Not Null ";                    
                    Filter += " ) ";
                    Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                }

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                (decimal.Parse(DocDt) >= decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                GetSQL1(Filter) : GetSQL2(DocDt, Filter),
                new string[]
                    {
                        //0
                        "WhsName", 

                        //1-5
                        "Lot", "Bin", "ItCode", "ItCodeInternal", "ItName", 
                        
                        //6-10
                        "ForeignName", "BatchNo", "Source","ItCtName", "Qty",
                        
                        //11-15
                        "InventoryUomCode", "Qty2", "InventoryUOMCode2", "Qty3", "InventoryUOMCode3",

                        //16-20
                        "CurCode", "UPrice", "Excrate", "ItGrpCode", "ItGrpName",

                        //21-25
                        "ItScName", "EntName", "ProjectName", "PONo", "Specification",

                        //26
                        "HeatNumber"
                    },
                 (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                 {
                     Grd.Cells[Row, 0].Value = Row + 1;
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 2, 1);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 3, 2);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 3);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 7, 5);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 9, 7);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 10, 8);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 11, 9);
                     Sm.SetGrdVal("N", Grd, dr, c, Row, 12, 10);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 13, 11);
                     Sm.SetGrdVal("N", Grd, dr, c, Row, 14, 12);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 15, 13);
                     Sm.SetGrdVal("N", Grd, dr, c, Row, 16, 14);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 17, 15);
                     Sm.SetGrdVal("S", Grd, dr, c, Row, 18, 16);
                     Sm.SetGrdVal("N", Grd, dr, c, Row, 19, 17);
                     Sm.SetGrdVal("N", Grd, dr, c, Row, 20, 18);
                     Grd.Cells[Row, 21].Value = dr.GetDecimal(c[10]) * dr.GetDecimal(c[17]) * dr.GetDecimal(c[18]);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                 }, true, false, false, false
                 );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 16, 21 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.WhsName = @WhsName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select WhsCode From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode ");
                SQL.AppendLine("    From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (IsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbWhsName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Warehouse");
        }

        #endregion

        #endregion
    }
}
