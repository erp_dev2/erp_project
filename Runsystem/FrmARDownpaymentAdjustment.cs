﻿#region Update 
/*
    [ICA/YK] New App
    02/03/2021 [ICA/YK] tambah Checkbox For Journal di grid 
    12/03/2021 [WED/YK] Checkbox nya ganti label jadi As Addition
                        Validasi nilai Debit dan Credit harus imbang itu dihilangkan, karena semua detail nya akan masuk di journal nya
                        Kalau Checkbox nya di tick, maka nilai nya itu akan jadi penambah. kalau gak di tick, akan jadi pengurang
    16/03/2021 [WED/YK] gajadi pakai checkbox nya, trus validasi debit & credit harus balance gak jadi dihilangkan
                        COA yang muncul hanya COA milik project tersebut berdasarkan parameter tertentu (baru)
                        yang dihitung sebagai adjustment nya hanya COA PJK
                        Nilai adjustment nya gajadi yg (DP + Total Tax) + Adjustment, tapi hanya DP + Adjustment
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpaymentAdjustment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private string
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty;

        internal string 
            mCtCode = string.Empty,
            mProjectCode2 = string.Empty,
            mCustomerAcNoAR = string.Empty,
            mPPHAcNoForARDownpaymentAdjustment = string.Empty,
            mCostAcNoForARDownpaymentAdjustment = string.Empty
            ;
        internal FrmARDownpaymentAdjustmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmARDownpaymentAdjustment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                SetLueCtCode(ref LueCtCode);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
           #region Grid COA

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "As"+Environment.NewLine+"Addition",
                        "Debit",
                        "Credit",

                        //6
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 50, 100, 100, 
                        
                        //6
                        400
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 3 });
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, TxtARDPDocNo, TxtDownpayment, TxtDownpaymentAftTax, LuePaymentType, LueBankCode, LueBankAcCode, 
                        MeeCancelReason, ChkCancelInd, TxtCOAAmt, TxtAmt, TxtVoucherDocNo, TxtVoucherRequestDocNo, 
                        TxtGiroNo, DteDueDt
                    }, true);
                    BtnARDPDocNo.Enabled = false;
                    Sm.SetControlReadOnly(ChkCancelInd, true);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LuePaymentType, LueBankCode, LueBankAcCode
                    }, false);
                    BtnARDPDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mProjectCode2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCtCode, TxtARDPDocNo, TxtDownpayment, TxtDownpaymentAftTax, LuePaymentType, LueBankCode, LueBankAcCode, 
                MeeCancelReason, ChkCancelInd, TxtCOAAmt, TxtAmt, TxtVoucherDocNo, TxtVoucherRequestDocNo, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDownpayment, TxtDownpaymentAftTax, TxtAmt, TxtCOAAmt }, 0);
            ChkCancelInd.Checked = false;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmARDownpaymentAdjustmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetLueCtCode(ref LueCtCode);
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                //ParPrint();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CtCode, A.ARDPDocNo, B.Amt as DownpaymentAmt, A.PaymentType, A.BankCode, A.BankAcCode, A.GiroNo, A.DueDt, ");
                SQL.AppendLine("A.CancelReason, A.CancelInd, A.Amt, A.COAAmt, A.VoucherRequestDocNo, A.VoucherDocNo, ");
                SQL.AppendLine("Case IfNull(A.Status, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'A' Then 'Approved' ");
                SQL.AppendLine("    When 'C' Then 'Cancel' End ");
                SQL.AppendLine("As StatusDesc, ");
                SQL.AppendLine("(B.Amt+B.TaxAmt+B.TaxAmt2+B.TaxAmt3) As DownpaymentAftTax, C.ProjectCode2 ");
                SQL.AppendLine("From TblARDownpaymentAdjustmentHdr A ");
                SQL.AppendLine("Inner Join TblARDownpayment B On A.ARDPDocNo = B.DocNo ");
                SQL.AppendLine("Left JOin TblSOContractHdr C On B.SODocNo = C.DocNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CtCode", "ARDPDocNo", "DownpaymentAmt", "PaymentType", 
                            
                            //6-10
                            "BankCode", "BankAcCode", "GiroNo", "DueDt", "CancelReason",  
                            
                            //11-15
                            "CancelInd", "COAAmt", "Amt", "VoucherRequestDocNo",  "VoucherDocNo",

                            //16-18
                            "DownpaymentAftTax", "StatusDesc", "ProjectCode2"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[2]));
                            TxtARDPDocNo.EditValue = Sm.DrStr(dr, c[3]);
                            TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[5]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[6]));
                            Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[7]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[8]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[9]));
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[10]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[11]) == "Y";
                            TxtCOAAmt.EditValue = Sm.FormatNum(Decimal.Parse(Sm.DrStr(dr, c[12])), 0);
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]);
                            TxtDownpaymentAftTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                            TxtStatus.EditValue = Sm.DrStr(dr, c[17]);
                            //TxtAmt.EditValue = Sm.FormatNum(Decimal.Parse(Sm.DrStr(dr, c[16])) + Decimal.Parse(Sm.DrStr(dr, c[12])), 0);
                            mProjectCode2 = Sm.DrStr(dr, c[18]);
                        }, true
                    );
                ShowARDPAdjustmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowARDPAdjustmentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.ForJournalInd ");
            SQL.AppendLine("From TblARDownpaymentAdjustmentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "ForJournalInd", "DAmt", "CAmt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ARDownpaymentAdjustment", "TblARDownpaymentAdjustmentHdr");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveARDPAdjustment(DocNo, TxtVoucherRequestDocNo.Text));

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveARDPAdjustmentDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return 
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsTxtEmpty(TxtARDPDocNo, "AR Downpayment#", false) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankCode, "Bank Name") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCOAAmtNotBalanced()
                ;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, r, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd2, r, 4) == 0m && Sm.GetGrdDec(Grd2, r, 5) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd2, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd2, r, 2) + Environment.NewLine + Environment.NewLine +
                            "Debit and credit value should not be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd2, r, 4) != 0m && Sm.GetGrdDec(Grd2, r, 5) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA's Account List" + Environment.NewLine +
                            "Account# : " + Sm.GetGrdStr(Grd2, r, 1) + Environment.NewLine +
                            "Description : " + Sm.GetGrdStr(Grd2, r, 2) + Environment.NewLine + Environment.NewLine +
                            "You have to fill in either debit or credit value.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsCOAAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 4).Length > 0) Debit += Sm.GetGrdDec(Grd2, r, 4);
                if (Sm.GetGrdStr(Grd2, r, 5).Length > 0) Credit += Sm.GetGrdDec(Grd2, r, 5);
            }
            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "COA's Account List" + Environment.NewLine +
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }

            return false;
        }

        private MySqlCommand SaveARDPAdjustment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            #region save AR

            SQL.AppendLine("Insert Into TblARDownpaymentAdjustmentHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, CtCode, ARDPDocNo, PaymentType, BankCode, ");
            SQL.AppendLine("BankAcCode, GiroNo, DueDt, COAAmt, Amt, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'A', @CtCode, @ARDPDocNo, @PaymentType, @BankCode, ");
            SQL.AppendLine("@BankAcCode, @GiroNo, @DueDt, @COAAmt, @Amt, @VoucherRequestDocNo, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ARDownpaymentAdjustment' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblARDownpaymentAdjustmentHdr A ");
            SQL.AppendLine("    Inner Join TblARDownpayment B On A.ARDPDocNo = B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) C On B.CurCode=C.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0.00)); ");

            SQL.AppendLine("Update TblARDownpaymentAdjustmentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ARDownpaymentAdjustment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            #region Save VR

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A', Amt=@Amt, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestDtl Set Amt=@Amt, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@ARDPDocNo", TxtARDPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<Decimal>(ref cm, "@COAAmt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveARDPAdjustmentDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblARDownpaymentAdjustmentDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, ForJournalInd, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @ForJournalInd, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3)); 
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@ForJournalInd", Sm.GetGrdBool(Grd2, Row, 3) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditARDPAdjustment());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblARDownpaymentAdjustmentHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditARDPAdjustment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARDownpaymentAdjustmentHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, Status = 'C', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Amt=( ");
            SQL.AppendLine("    Select (Amt+TaxAmt+TaxAmt2+TaxAmt3) as Amt From TblARDownpayment Where VoucherRequestDocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("), ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestDtl A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Status<>'C' ");
            SQL.AppendLine("Set A.Amt=( ");
            SQL.AppendLine("    Select (Amt+TaxAmt+TaxAmt2+TaxAmt3) as Amt From TblARDownpayment Where VoucherRequestDocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("), ");
            SQL.AppendLine("A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mCustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");
            mPPHAcNoForARDownpaymentAdjustment = Sm.GetParameter("PPHAcNoForARDownpaymentAdjustment");
            mCostAcNoForARDownpaymentAdjustment = Sm.GetParameter("CostAcNoForARDownpaymentAdjustment");
        }

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CtCode As Col1, CtName As Col2 From TblCustomer ");
            SQL.AppendLine("Where CtCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.CtCode From TblARDownpayment A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr B ON A.VoucherRequestDocNo = B.DocNo AND B.VoucherDocNo IS NULL ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' ");
            SQL.AppendLine("    And Not Exists ( ");
            SQL.AppendLine("        Select 1 from TblARDownpaymentAdjustmentHdr Where CancelInd = 'N' And ARDPDocNo = A.DocNo ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By CtName; ");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ComputeCOAAmt()
        {
            try
            {
                decimal Amt = 0m;

                if (mCustomerAcNoAR.Length > 0)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        #region Old Code
                        //string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = '" + Sm.GetGrdStr(Grd2, Row, 1) + "'");
                        //if (Sm.GetGrdBool(Grd2, Row, 3))
                        //{
                        //    if (Sm.GetGrdDec(Grd2, Row, 4) != 0)
                        //    {
                        //        if (AcType == "D")
                        //            Amt += Sm.GetGrdDec(Grd2, Row, 4);
                        //        else
                        //            Amt -= Sm.GetGrdDec(Grd2, Row, 4);
                        //    }
                        //    if (Sm.GetGrdDec(Grd2, Row, 5) != 0)
                        //    {
                        //        if (AcType == "C")
                        //            Amt += Sm.GetGrdDec(Grd2, Row, 5);
                        //        else
                        //            Amt -= Sm.GetGrdDec(Grd2, Row, 5);
                        //    }
                        //}
                        #endregion

                        #region Old Code 2
                        //if (Sm.GetGrdBool(Grd2, Row, 3))
                        //    Amt += (Sm.GetGrdDec(Grd2, Row, 4) + Sm.GetGrdDec(Grd2, Row, 5));
                        //else
                        //    Amt -= (Sm.GetGrdDec(Grd2, Row, 4) + Sm.GetGrdDec(Grd2, Row, 5));
                        #endregion

                        string SelectedAcNo = Sm.GetGrdStr(Grd2, Row, 1);
                        if (SelectedAcNo == string.Concat(mCustomerAcNoAR, mProjectCode2))
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param; ", SelectedAcNo);

                            // Debit
                            if (Sm.GetGrdDec(Grd2, Row, 4) != 0m)
                            {
                                if (AcType == "D") Amt += Sm.GetGrdDec(Grd2, Row, 4);
                                else Amt -= Sm.GetGrdDec(Grd2, Row, 4);
                            }

                            // Credit
                            if (Sm.GetGrdDec(Grd2, Row, 5) != 0m)
                            {
                                if (AcType == "C") Amt += Sm.GetGrdDec(Grd2, Row, 5);
                                else Amt -= Sm.GetGrdDec(Grd2, Row, 5);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(Amt, 0);
                //TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtDownpaymentAftTax.Text) + Amt), 0);
                TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtDownpayment.Text) + Amt), 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmARDownpaymentAdjustmentDlg2(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmARDownpaymentAdjustmentDlg2(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0) ComputeCOAAmt();
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 5].Value = 0;
                ComputeCOAAmt();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 4].Value = 0;
                ComputeCOAAmt();
            }
        }

        #endregion

        #region Event

        #region Button Event

        private void BtnARDPDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer is Empty"))
            {
                mCtCode = Sm.GetLue(LueCtCode);
                Sm.FormShowDialog(new FrmARDownpaymentAdjustmentDlg(this, Sm.GetLue(LueCtCode)));
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }
        #endregion

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }


        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        #endregion

        #endregion
    }
}
