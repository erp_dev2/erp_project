﻿namespace RunSystem
{
    partial class FrmDocApprovalSettingFormula
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDocApprovalSettingFormula));
            this.label7 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLevel = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTypeApproval = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.BtnDocApprovalSetting = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDASLevelCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtDASSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtDASDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.LueApprovalType = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnProcess = new DevExpress.XtraEditors.SimpleButton();
            this.ChkSiteCodeFilter = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDeptCodeFilter = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueSiteCodeFilter = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCodeFilter = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.ChkApprovalType = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTypeApproval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASDeptCode.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueApprovalType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCodeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCodeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCodeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkApprovalType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(585, 0);
            this.panel1.Size = new System.Drawing.Size(70, 261);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(585, 261);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(92, 144);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 14);
            this.label7.TabIndex = 21;
            this.label7.Text = "Approver\'s Site";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(188, 141);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 25;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Size = new System.Drawing.Size(329, 20);
            this.LueSiteCode.TabIndex = 22;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(71, 166);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 14);
            this.label17.TabIndex = 23;
            this.label17.Text = "Approver\'s Position";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(188, 162);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 25;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 250;
            this.LuePosCode.Size = new System.Drawing.Size(329, 20);
            this.LuePosCode.TabIndex = 24;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(40, 123);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Approver\'s Departement";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(188, 120);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(329, 20);
            this.LueDeptCode.TabIndex = 20;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtLevel
            // 
            this.TxtLevel.EnterMoveNextControl = true;
            this.TxtLevel.Location = new System.Drawing.Point(188, 36);
            this.TxtLevel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel.Name = "TxtLevel";
            this.TxtLevel.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLevel.Size = new System.Drawing.Size(66, 20);
            this.TxtLevel.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(95, 39);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 14);
            this.label3.TabIndex = 11;
            this.label3.Text = "Setting\'s Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTypeApproval
            // 
            this.TxtTypeApproval.EnterMoveNextControl = true;
            this.TxtTypeApproval.Location = new System.Drawing.Point(188, 15);
            this.TxtTypeApproval.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTypeApproval.Name = "TxtTypeApproval";
            this.TxtTypeApproval.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTypeApproval.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTypeApproval.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTypeApproval.Properties.Appearance.Options.UseFont = true;
            this.TxtTypeApproval.Properties.MaxLength = 30;
            this.TxtTypeApproval.Size = new System.Drawing.Size(329, 20);
            this.TxtTypeApproval.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(96, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Approval Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(585, 261);
            this.Tc1.TabIndex = 8;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.BtnDocApprovalSetting);
            this.Tp1.Controls.Add(this.TxtDASLevelCode);
            this.Tp1.Controls.Add(this.TxtDASSiteCode);
            this.Tp1.Controls.Add(this.TxtDASDeptCode);
            this.Tp1.Controls.Add(this.label10);
            this.Tp1.Controls.Add(this.label11);
            this.Tp1.Controls.Add(this.label12);
            this.Tp1.Controls.Add(this.TxtTypeApproval);
            this.Tp1.Controls.Add(this.label1);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.TxtLevel);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.LueDeptCode);
            this.Tp1.Controls.Add(this.LueSiteCode);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.label17);
            this.Tp1.Controls.Add(this.LuePosCode);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(579, 233);
            this.Tp1.Text = "General";
            // 
            // BtnDocApprovalSetting
            // 
            this.BtnDocApprovalSetting.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDocApprovalSetting.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDocApprovalSetting.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDocApprovalSetting.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDocApprovalSetting.Appearance.Options.UseBackColor = true;
            this.BtnDocApprovalSetting.Appearance.Options.UseFont = true;
            this.BtnDocApprovalSetting.Appearance.Options.UseForeColor = true;
            this.BtnDocApprovalSetting.Appearance.Options.UseTextOptions = true;
            this.BtnDocApprovalSetting.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDocApprovalSetting.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDocApprovalSetting.Image = ((System.Drawing.Image)(resources.GetObject("BtnDocApprovalSetting.Image")));
            this.BtnDocApprovalSetting.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDocApprovalSetting.Location = new System.Drawing.Point(519, 15);
            this.BtnDocApprovalSetting.Name = "BtnDocApprovalSetting";
            this.BtnDocApprovalSetting.Size = new System.Drawing.Size(24, 21);
            this.BtnDocApprovalSetting.TabIndex = 30;
            this.BtnDocApprovalSetting.ToolTip = "Choose document approval setting";
            this.BtnDocApprovalSetting.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDocApprovalSetting.ToolTipTitle = "Run System";
            this.BtnDocApprovalSetting.Click += new System.EventHandler(this.BtnDocApprovalSetting_Click);
            // 
            // TxtDASLevelCode
            // 
            this.TxtDASLevelCode.EnterMoveNextControl = true;
            this.TxtDASLevelCode.Location = new System.Drawing.Point(188, 99);
            this.TxtDASLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDASLevelCode.Name = "TxtDASLevelCode";
            this.TxtDASLevelCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDASLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDASLevelCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDASLevelCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDASLevelCode.Properties.MaxLength = 30;
            this.TxtDASLevelCode.Size = new System.Drawing.Size(329, 20);
            this.TxtDASLevelCode.TabIndex = 29;
            // 
            // TxtDASSiteCode
            // 
            this.TxtDASSiteCode.EnterMoveNextControl = true;
            this.TxtDASSiteCode.Location = new System.Drawing.Point(188, 78);
            this.TxtDASSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDASSiteCode.Name = "TxtDASSiteCode";
            this.TxtDASSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDASSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDASSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDASSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDASSiteCode.Properties.MaxLength = 30;
            this.TxtDASSiteCode.Size = new System.Drawing.Size(329, 20);
            this.TxtDASSiteCode.TabIndex = 28;
            // 
            // TxtDASDeptCode
            // 
            this.TxtDASDeptCode.EnterMoveNextControl = true;
            this.TxtDASDeptCode.Location = new System.Drawing.Point(188, 57);
            this.TxtDASDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDASDeptCode.Name = "TxtDASDeptCode";
            this.TxtDASDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDASDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDASDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDASDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDASDeptCode.Properties.MaxLength = 30;
            this.TxtDASDeptCode.Size = new System.Drawing.Size(329, 20);
            this.TxtDASDeptCode.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(38, 102);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 14);
            this.label10.TabIndex = 17;
            this.label10.Text = "Setting\'s Employee Level";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(102, 81);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 14);
            this.label11.TabIndex = 15;
            this.label11.Text = "Setting\'s Site";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(50, 60);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 14);
            this.label12.TabIndex = 13;
            this.label12.Text = "Setting\'s Departement";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.LueApprovalType);
            this.Tp2.Controls.Add(this.BtnProcess);
            this.Tp2.Controls.Add(this.ChkSiteCodeFilter);
            this.Tp2.Controls.Add(this.ChkDeptCodeFilter);
            this.Tp2.Controls.Add(this.label2);
            this.Tp2.Controls.Add(this.LueSiteCodeFilter);
            this.Tp2.Controls.Add(this.label4);
            this.Tp2.Controls.Add(this.LueDeptCodeFilter);
            this.Tp2.Controls.Add(this.label6);
            this.Tp2.Controls.Add(this.ChkApprovalType);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 205);
            this.Tp2.Text = "Updating Process";
            // 
            // LueApprovalType
            // 
            this.LueApprovalType.EnterMoveNextControl = true;
            this.LueApprovalType.Location = new System.Drawing.Point(141, 13);
            this.LueApprovalType.Margin = new System.Windows.Forms.Padding(5);
            this.LueApprovalType.Name = "LueApprovalType";
            this.LueApprovalType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueApprovalType.Properties.Appearance.Options.UseFont = true;
            this.LueApprovalType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueApprovalType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueApprovalType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueApprovalType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueApprovalType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueApprovalType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueApprovalType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueApprovalType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueApprovalType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueApprovalType.Properties.DropDownRows = 30;
            this.LueApprovalType.Properties.NullText = "[Empty]";
            this.LueApprovalType.Properties.PopupWidth = 300;
            this.LueApprovalType.Size = new System.Drawing.Size(330, 20);
            this.LueApprovalType.TabIndex = 10;
            this.LueApprovalType.ToolTip = "F4 : Show/hide list";
            this.LueApprovalType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueApprovalType.EditValueChanged += new System.EventHandler(this.LueApprovalType_EditValueChanged);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProcess.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProcess.Location = new System.Drawing.Point(389, 82);
            this.BtnProcess.Name = "BtnProcess";
            this.BtnProcess.Size = new System.Drawing.Size(82, 21);
            this.BtnProcess.TabIndex = 18;
            this.BtnProcess.Text = "Process Data";
            this.BtnProcess.ToolTip = "Process Data";
            this.BtnProcess.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProcess.ToolTipTitle = "Run System";
            this.BtnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // ChkSiteCodeFilter
            // 
            this.ChkSiteCodeFilter.Location = new System.Drawing.Point(475, 58);
            this.ChkSiteCodeFilter.Name = "ChkSiteCodeFilter";
            this.ChkSiteCodeFilter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCodeFilter.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCodeFilter.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCodeFilter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCodeFilter.Properties.Caption = " ";
            this.ChkSiteCodeFilter.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCodeFilter.Size = new System.Drawing.Size(31, 22);
            this.ChkSiteCodeFilter.TabIndex = 17;
            this.ChkSiteCodeFilter.ToolTip = "Remove filter";
            this.ChkSiteCodeFilter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCodeFilter.ToolTipTitle = "Run System";
            this.ChkSiteCodeFilter.CheckedChanged += new System.EventHandler(this.ChkSiteCodeFilter_CheckedChanged);
            // 
            // ChkDeptCodeFilter
            // 
            this.ChkDeptCodeFilter.Location = new System.Drawing.Point(475, 35);
            this.ChkDeptCodeFilter.Name = "ChkDeptCodeFilter";
            this.ChkDeptCodeFilter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCodeFilter.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCodeFilter.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCodeFilter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCodeFilter.Properties.Caption = " ";
            this.ChkDeptCodeFilter.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCodeFilter.Size = new System.Drawing.Size(31, 22);
            this.ChkDeptCodeFilter.TabIndex = 14;
            this.ChkDeptCodeFilter.ToolTip = "Remove filter";
            this.ChkDeptCodeFilter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCodeFilter.ToolTipTitle = "Run System";
            this.ChkDeptCodeFilter.CheckedChanged += new System.EventHandler(this.ChkDeptCodeFilter_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(58, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Setting\'s Site";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCodeFilter
            // 
            this.LueSiteCodeFilter.EnterMoveNextControl = true;
            this.LueSiteCodeFilter.Location = new System.Drawing.Point(141, 59);
            this.LueSiteCodeFilter.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCodeFilter.Name = "LueSiteCodeFilter";
            this.LueSiteCodeFilter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFilter.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCodeFilter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFilter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCodeFilter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFilter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCodeFilter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFilter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCodeFilter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCodeFilter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCodeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCodeFilter.Properties.DropDownRows = 25;
            this.LueSiteCodeFilter.Properties.MaxLength = 16;
            this.LueSiteCodeFilter.Properties.NullText = "[Empty]";
            this.LueSiteCodeFilter.Properties.PopupWidth = 500;
            this.LueSiteCodeFilter.Size = new System.Drawing.Size(330, 20);
            this.LueSiteCodeFilter.TabIndex = 16;
            this.LueSiteCodeFilter.ToolTip = "F4 : Show/hide list";
            this.LueSiteCodeFilter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCodeFilter.EditValueChanged += new System.EventHandler(this.LueSiteCodeFilter_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 39);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Setting\'s Departement";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCodeFilter
            // 
            this.LueDeptCodeFilter.EnterMoveNextControl = true;
            this.LueDeptCodeFilter.Location = new System.Drawing.Point(141, 36);
            this.LueDeptCodeFilter.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCodeFilter.Name = "LueDeptCodeFilter";
            this.LueDeptCodeFilter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCodeFilter.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCodeFilter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCodeFilter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCodeFilter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCodeFilter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCodeFilter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCodeFilter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCodeFilter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCodeFilter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCodeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCodeFilter.Properties.DropDownRows = 25;
            this.LueDeptCodeFilter.Properties.MaxLength = 16;
            this.LueDeptCodeFilter.Properties.NullText = "[Empty]";
            this.LueDeptCodeFilter.Properties.PopupWidth = 500;
            this.LueDeptCodeFilter.Size = new System.Drawing.Size(330, 20);
            this.LueDeptCodeFilter.TabIndex = 13;
            this.LueDeptCodeFilter.ToolTip = "F4 : Show/hide list";
            this.LueDeptCodeFilter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCodeFilter.EditValueChanged += new System.EventHandler(this.LueDeptCodeFilter_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(51, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Setting\'s Type";
            // 
            // ChkApprovalType
            // 
            this.ChkApprovalType.Location = new System.Drawing.Point(475, 11);
            this.ChkApprovalType.Name = "ChkApprovalType";
            this.ChkApprovalType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkApprovalType.Properties.Appearance.Options.UseFont = true;
            this.ChkApprovalType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkApprovalType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkApprovalType.Properties.Caption = " ";
            this.ChkApprovalType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkApprovalType.Size = new System.Drawing.Size(31, 22);
            this.ChkApprovalType.TabIndex = 11;
            this.ChkApprovalType.ToolTip = "Remove filter";
            this.ChkApprovalType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkApprovalType.ToolTipTitle = "Run System";
            this.ChkApprovalType.CheckedChanged += new System.EventHandler(this.ChkApprovalType_CheckedChanged);
            // 
            // FrmDocApprovalSettingFormula
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 261);
            this.Name = "FrmDocApprovalSettingFormula";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTypeApproval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDASDeptCode.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueApprovalType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCodeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCodeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCodeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCodeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkApprovalType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtTypeApproval;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCodeFilter;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCodeFilter;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCodeFilter;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCodeFilter;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkApprovalType;
        public DevExpress.XtraEditors.SimpleButton BtnProcess;
        private DevExpress.XtraEditors.LookUpEdit LueApprovalType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDASLevelCode;
        internal DevExpress.XtraEditors.TextEdit TxtDASSiteCode;
        internal DevExpress.XtraEditors.TextEdit TxtDASDeptCode;
        public DevExpress.XtraEditors.SimpleButton BtnDocApprovalSetting;
        internal DevExpress.XtraEditors.TextEdit TxtLevel;
    }
}