﻿#region Update
/*
    10/09/2017 [TKG] tambah filter dan informasi site
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    27/02/2018 [TKG] tambah info+filter employment status
 *  09/01/2019 [HAR/SRN] distinct data untuk data2 log yang duplicate
 *  14/05/2020 [HAR/Iprobe] tambah infromasi remark dan city
 *  07/09/2020 [ICA/IMS] Tambah Kolom Longitude dan Latitude dengan parameter IsAttendanceLogUseLatLngTable
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAttendanceLog : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false;
        private bool 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false,
            mIsAttendanceLogUseLatLngTable = false;


        #endregion

        #region Constructor

        public FrmRptAttendanceLog(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsAttendanceLogUseLatLngTable = Sm.GetParameterBoo("IsAttendanceLogUseLatLngTable");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.EmpCode,  B.EmpName, B.EmpCodeOld, A.Dt As Date, B.BarCodecode, A.Latitude, A.Longitude, ");
            SQL.AppendLine("C.DeptName, D.PosName, E.SiteName, F.OptDesc As EmploymentStatusDesc, A.Tm As Time, A.Machine, A.City, A.Remark ");
            if (mIsAttendanceLogUseLatLngTable)
                SQL.AppendLine(", G.Position as City2 ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCOde = B.EmpCOde ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsAttendanceLogUseLatLngTable)
            {
                SQL.AppendLine("Left Join TblLatLngPos G On Round(A.Latitude, 3) = G.Latitude ");
                SQL.AppendLine("And Round(A.Longitude, 3) = G.Longitude ");
                SQL.AppendLine("And A.Latitude Is Not Null And A.Longitude Is Not Null ");
            }
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblOption F On B.EmploymentStatus=F.OptCode And F.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Where A.Dt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "",
                        "Employee's Name",
                        "Old Code", 
                        "Date",
                        
                        //6-10
                        "Barcode",
                        "Department",
                        "Position",
                        "Site",
                        "Employment Status",
                        
                        //11-15
                        "Machine",
                        "Time",
                        "City", 
                        "Remark",
                        "Latitude",

                        //16
                        "Longitude",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 200, 80, 100, 
                        
                        //6-10
                        130, 200, 150, 150, 150, 
                        
                        //11-15
                        150, 80, 120, 200, 150, 

                        //16
                        150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 6, 8, 10, 15, 16 }, false);
            Sm.GrdFormatTime(Grd1, new int[] { 12 });
            Grd1.Cols[15].Move(13);
            Grd1.Cols[16].Move(14);
        }

        override protected void HideInfoInGrd()
        {
            if (mIsAttendanceLogUseLatLngTable)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 6, 8, 10, 15, 16 }, !ChkHideInfoInGrd.Checked);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 6, 8, 10 }, !ChkHideInfoInGrd.Checked);
            }
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpCodeOld", "B.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEmploymentStatus), "B.EmploymentStatus", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By B.EmpName, A.Dt, A.Tm;",
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName","EmpCodeOld","Date","BarcodeCode","DeptName",
                            //6-10
                            "PosName", "SiteName", "EmploymentStatusDesc", "Machine", "Time",
                            //11-14
                            (mIsAttendanceLogUseLatLngTable) ? "City2" : "City", "Remark", "Latitude", "Longitude", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

       
        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion

        #endregion

    }
}
