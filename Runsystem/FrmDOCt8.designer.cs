﻿namespace RunSystem
{
    partial class FrmDOCt8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDOCt8));
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSAName = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProcessInd = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtVehicleRegNo = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtExpedition = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtDriver = new DevExpress.XtraEditors.TextEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCountry = new DevExpress.XtraEditors.TextEdit();
            this.TxtCity = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MeeSAAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtDocNoInternal = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LuePackagingUnitUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtResiNo = new DevExpress.XtraEditors.TextEdit();
            this.lb = new System.Windows.Forms.Label();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.BtnCustomerShipAddress = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSAName = new DevExpress.XtraEditors.SimpleButton();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProductionWorkGroup = new System.Windows.Forms.Label();
            this.BtnCopyDOCt = new DevExpress.XtraEditors.SimpleButton();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BtnPEB = new DevExpress.XtraEditors.SimpleButton();
            this.TxtKBSubmissionNo = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.DteKBRegistrationDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.DteKBPLDt = new DevExpress.XtraEditors.DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.DteKBContractDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnKBContractNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtKBRegistrationNo = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtKBPLNo = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtKBContractNo = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ChkProgressInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkServiceInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVehicleRegNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSAAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePackagingUnitUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResiNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProgressInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(797, 0);
            this.panel1.Size = new System.Drawing.Size(70, 509);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(797, 287);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LuePackagingUnitUomCode);
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 287);
            this.panel3.Size = new System.Drawing.Size(797, 222);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LuePackagingUnitUomCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(797, 166);
            this.Grd1.TabIndex = 60;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(36, 93);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(99, 90);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 350;
            this.LueCtCode.Size = new System.Drawing.Size(305, 20);
            this.LueCtCode.TabIndex = 22;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(26, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "Warehouse";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(99, 69);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 350;
            this.LueWhsCode.Size = new System.Drawing.Size(305, 20);
            this.LueWhsCode.TabIndex = 20;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(94, 216);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(259, 20);
            this.MeeRemark.TabIndex = 59;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(42, 219);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 58;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(99, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(135, 20);
            this.DteDocDt.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(62, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(99, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(305, 20);
            this.TxtDocNo.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 166);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(797, 56);
            this.Grd2.TabIndex = 62;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(224, 132);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(129, 20);
            this.TxtAmt.TabIndex = 51;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(6, 135);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 14);
            this.label9.TabIndex = 49;
            this.label9.Text = "Total Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(94, 132);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 150;
            this.LueCurCode.Size = new System.Drawing.Size(127, 20);
            this.LueCurCode.TabIndex = 50;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblSAName
            // 
            this.LblSAName.AutoSize = true;
            this.LblSAName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSAName.ForeColor = System.Drawing.Color.Black;
            this.LblSAName.Location = new System.Drawing.Point(7, 156);
            this.LblSAName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSAName.Name = "LblSAName";
            this.LblSAName.Size = new System.Drawing.Size(88, 14);
            this.LblSAName.TabIndex = 28;
            this.LblSAName.Text = "Shipping Name";
            this.LblSAName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.LueProcessInd);
            this.panel4.Controls.Add(this.LblProcessInd);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.TxtVehicleRegNo);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.TxtExpedition);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.TxtDriver);
            this.panel4.Controls.Add(this.TxtMobile);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtEmail);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtFax);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtAmt);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtPhone);
            this.panel4.Controls.Add(this.TxtPostalCd);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.LueCurCode);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(432, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(359, 259);
            this.panel4.TabIndex = 38;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(94, 6);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 30;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 350;
            this.LueProcessInd.Size = new System.Drawing.Size(259, 20);
            this.LueProcessInd.TabIndex = 40;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // LblProcessInd
            // 
            this.LblProcessInd.AutoSize = true;
            this.LblProcessInd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProcessInd.ForeColor = System.Drawing.Color.Black;
            this.LblProcessInd.Location = new System.Drawing.Point(41, 9);
            this.LblProcessInd.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProcessInd.Name = "LblProcessInd";
            this.LblProcessInd.Size = new System.Drawing.Size(48, 14);
            this.LblProcessInd.TabIndex = 39;
            this.LblProcessInd.Text = "Process";
            this.LblProcessInd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(9, 199);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 14);
            this.label25.TabIndex = 56;
            this.label25.Text = "Vehicle Req#";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVehicleRegNo
            // 
            this.TxtVehicleRegNo.EnterMoveNextControl = true;
            this.TxtVehicleRegNo.Location = new System.Drawing.Point(94, 195);
            this.TxtVehicleRegNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVehicleRegNo.Name = "TxtVehicleRegNo";
            this.TxtVehicleRegNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVehicleRegNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVehicleRegNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVehicleRegNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVehicleRegNo.Properties.MaxLength = 100;
            this.TxtVehicleRegNo.Size = new System.Drawing.Size(259, 20);
            this.TxtVehicleRegNo.TabIndex = 57;
            this.TxtVehicleRegNo.Validated += new System.EventHandler(this.TxtVehicleRegNo_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(25, 178);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 14);
            this.label24.TabIndex = 54;
            this.label24.Text = "Expedition";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExpedition
            // 
            this.TxtExpedition.EnterMoveNextControl = true;
            this.TxtExpedition.Location = new System.Drawing.Point(94, 174);
            this.TxtExpedition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExpedition.Name = "TxtExpedition";
            this.TxtExpedition.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExpedition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExpedition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExpedition.Properties.Appearance.Options.UseFont = true;
            this.TxtExpedition.Properties.MaxLength = 100;
            this.TxtExpedition.Size = new System.Drawing.Size(259, 20);
            this.TxtExpedition.TabIndex = 55;
            this.TxtExpedition.Validated += new System.EventHandler(this.TxtExpedition_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(51, 156);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 14);
            this.label22.TabIndex = 52;
            this.label22.Text = "Driver";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDriver
            // 
            this.TxtDriver.EnterMoveNextControl = true;
            this.TxtDriver.Location = new System.Drawing.Point(94, 153);
            this.TxtDriver.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDriver.Name = "TxtDriver";
            this.TxtDriver.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDriver.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDriver.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDriver.Properties.Appearance.Options.UseFont = true;
            this.TxtDriver.Properties.MaxLength = 100;
            this.TxtDriver.Size = new System.Drawing.Size(259, 20);
            this.TxtDriver.TabIndex = 53;
            this.TxtDriver.Validated += new System.EventHandler(this.TxtDriver_Validated);
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(94, 111);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(259, 20);
            this.TxtMobile.TabIndex = 48;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(48, 114);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(94, 90);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Properties.ReadOnly = true;
            this.TxtEmail.Size = new System.Drawing.Size(259, 20);
            this.TxtEmail.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(55, 93);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 45;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(94, 69);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Properties.ReadOnly = true;
            this.TxtFax.Size = new System.Drawing.Size(259, 20);
            this.TxtFax.TabIndex = 44;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(64, 72);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 14);
            this.label11.TabIndex = 43;
            this.label11.Text = "Fax";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(94, 48);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(259, 20);
            this.TxtPhone.TabIndex = 42;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(94, 27);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Properties.ReadOnly = true;
            this.TxtPostalCd.Size = new System.Drawing.Size(143, 20);
            this.TxtPostalCd.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(18, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 14);
            this.label14.TabIndex = 39;
            this.label14.Text = "Postal Code";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(47, 51);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 41;
            this.label12.Text = "Phone";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(45, 219);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 36;
            this.label6.Text = "Country";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCountry
            // 
            this.TxtCountry.EnterMoveNextControl = true;
            this.TxtCountry.Location = new System.Drawing.Point(99, 216);
            this.TxtCountry.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCountry.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCountry.Properties.Appearance.Options.UseFont = true;
            this.TxtCountry.Properties.MaxLength = 20;
            this.TxtCountry.Properties.ReadOnly = true;
            this.TxtCountry.Size = new System.Drawing.Size(305, 20);
            this.TxtCountry.TabIndex = 37;
            // 
            // TxtCity
            // 
            this.TxtCity.EnterMoveNextControl = true;
            this.TxtCity.Location = new System.Drawing.Point(99, 195);
            this.TxtCity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCity.Properties.Appearance.Options.UseFont = true;
            this.TxtCity.Properties.MaxLength = 20;
            this.TxtCity.Properties.ReadOnly = true;
            this.TxtCity.Size = new System.Drawing.Size(305, 20);
            this.TxtCity.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(68, 198);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 14);
            this.label13.TabIndex = 34;
            this.label13.Text = "City";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(45, 177);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 32;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeSAAddress
            // 
            this.MeeSAAddress.EnterMoveNextControl = true;
            this.MeeSAAddress.Location = new System.Drawing.Point(99, 174);
            this.MeeSAAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSAAddress.Name = "MeeSAAddress";
            this.MeeSAAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeSAAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.Appearance.Options.UseBackColor = true;
            this.MeeSAAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSAAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSAAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSAAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSAAddress.Properties.MaxLength = 400;
            this.MeeSAAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSAAddress.Properties.ReadOnly = true;
            this.MeeSAAddress.Properties.ShowIcon = false;
            this.MeeSAAddress.Size = new System.Drawing.Size(305, 20);
            this.MeeSAAddress.TabIndex = 33;
            this.MeeSAAddress.ToolTip = "F4 : Show/hide text";
            this.MeeSAAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSAAddress.ToolTipTitle = "Run System";
            // 
            // TxtDocNoInternal
            // 
            this.TxtDocNoInternal.EnterMoveNextControl = true;
            this.TxtDocNoInternal.Location = new System.Drawing.Point(99, 111);
            this.TxtDocNoInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNoInternal.Name = "TxtDocNoInternal";
            this.TxtDocNoInternal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNoInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNoInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoInternal.Properties.MaxLength = 80;
            this.TxtDocNoInternal.Size = new System.Drawing.Size(305, 20);
            this.TxtDocNoInternal.TabIndex = 25;
            this.TxtDocNoInternal.Validated += new System.EventHandler(this.TxtDocNoInternal_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(23, 114);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "Local Doc.#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePackagingUnitUomCode
            // 
            this.LuePackagingUnitUomCode.EnterMoveNextControl = true;
            this.LuePackagingUnitUomCode.Location = new System.Drawing.Point(372, 21);
            this.LuePackagingUnitUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePackagingUnitUomCode.Name = "LuePackagingUnitUomCode";
            this.LuePackagingUnitUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackagingUnitUomCode.Properties.Appearance.Options.UseFont = true;
            this.LuePackagingUnitUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackagingUnitUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePackagingUnitUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackagingUnitUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePackagingUnitUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackagingUnitUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePackagingUnitUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePackagingUnitUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePackagingUnitUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePackagingUnitUomCode.Properties.DropDownRows = 25;
            this.LuePackagingUnitUomCode.Properties.NullText = "[Empty]";
            this.LuePackagingUnitUomCode.Properties.PopupWidth = 220;
            this.LuePackagingUnitUomCode.Size = new System.Drawing.Size(220, 20);
            this.LuePackagingUnitUomCode.TabIndex = 61;
            this.LuePackagingUnitUomCode.ToolTip = "F4 : Show/hide list";
            this.LuePackagingUnitUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePackagingUnitUomCode.EditValueChanged += new System.EventHandler(this.LuePackagingUnitUomCode_EditValueChanged);
            this.LuePackagingUnitUomCode.Leave += new System.EventHandler(this.LuePackagingUnitUomCode_Leave);
            this.LuePackagingUnitUomCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePackagingUnitUomCode_KeyDown);
            // 
            // TxtResiNo
            // 
            this.TxtResiNo.EnterMoveNextControl = true;
            this.TxtResiNo.Location = new System.Drawing.Point(99, 132);
            this.TxtResiNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtResiNo.Name = "TxtResiNo";
            this.TxtResiNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtResiNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtResiNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtResiNo.Properties.Appearance.Options.UseFont = true;
            this.TxtResiNo.Properties.MaxLength = 250;
            this.TxtResiNo.Size = new System.Drawing.Size(305, 20);
            this.TxtResiNo.TabIndex = 27;
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.ForeColor = System.Drawing.Color.Black;
            this.lb.Location = new System.Drawing.Point(38, 135);
            this.lb.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(57, 14);
            this.lb.TabIndex = 26;
            this.lb.Text = "Receipt#";
            this.lb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(99, 153);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 20;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(253, 20);
            this.TxtSAName.TabIndex = 29;
            // 
            // BtnCustomerShipAddress
            // 
            this.BtnCustomerShipAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCustomerShipAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCustomerShipAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCustomerShipAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCustomerShipAddress.Appearance.Options.UseBackColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseFont = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseForeColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCustomerShipAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCustomerShipAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCustomerShipAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCustomerShipAddress.Image")));
            this.BtnCustomerShipAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCustomerShipAddress.Location = new System.Drawing.Point(355, 153);
            this.BtnCustomerShipAddress.Name = "BtnCustomerShipAddress";
            this.BtnCustomerShipAddress.Size = new System.Drawing.Size(24, 21);
            this.BtnCustomerShipAddress.TabIndex = 30;
            this.BtnCustomerShipAddress.ToolTip = "Find Customer\'s Shipping Information";
            this.BtnCustomerShipAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCustomerShipAddress.ToolTipTitle = "Run System";
            this.BtnCustomerShipAddress.Click += new System.EventHandler(this.BtnCustomerShipAddress_Click);
            // 
            // BtnSAName
            // 
            this.BtnSAName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSAName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSAName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSAName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSAName.Appearance.Options.UseBackColor = true;
            this.BtnSAName.Appearance.Options.UseFont = true;
            this.BtnSAName.Appearance.Options.UseForeColor = true;
            this.BtnSAName.Appearance.Options.UseTextOptions = true;
            this.BtnSAName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSAName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSAName.Image = ((System.Drawing.Image)(resources.GetObject("BtnSAName.Image")));
            this.BtnSAName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSAName.Location = new System.Drawing.Point(380, 152);
            this.BtnSAName.Name = "BtnSAName";
            this.BtnSAName.Size = new System.Drawing.Size(24, 21);
            this.BtnSAName.TabIndex = 31;
            this.BtnSAName.ToolTip = "Add Customer Shipping Address";
            this.BtnSAName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSAName.ToolTipTitle = "Run System";
            this.BtnSAName.Click += new System.EventHandler(this.BtnCtShippingAddress_Click);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(99, 48);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(135, 20);
            this.TxtStatus.TabIndex = 18;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(53, 50);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 14);
            this.label27.TabIndex = 17;
            this.label27.Text = "Status";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(797, 287);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(791, 259);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.LueProductionWorkGroup);
            this.panel5.Controls.Add(this.LblProductionWorkGroup);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.BtnCopyDOCt);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.TxtCountry);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.TxtStatus);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.label27);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.TxtCity);
            this.panel5.Controls.Add(this.TxtSAName);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.BtnCustomerShipAddress);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.TxtResiNo);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.lb);
            this.panel5.Controls.Add(this.LueCtCode);
            this.panel5.Controls.Add(this.TxtDocNoInternal);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LblSAName);
            this.panel5.Controls.Add(this.MeeSAAddress);
            this.panel5.Controls.Add(this.BtnSAName);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(791, 259);
            this.panel5.TabIndex = 12;
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(99, 237);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 30;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 350;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(305, 20);
            this.LueProductionWorkGroup.TabIndex = 40;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWorkGroup.EditValueChanged += new System.EventHandler(this.LueProductionWorkGroup_EditValueChanged);
            // 
            // LblProductionWorkGroup
            // 
            this.LblProductionWorkGroup.AutoSize = true;
            this.LblProductionWorkGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProductionWorkGroup.ForeColor = System.Drawing.Color.Red;
            this.LblProductionWorkGroup.Location = new System.Drawing.Point(55, 240);
            this.LblProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProductionWorkGroup.Name = "LblProductionWorkGroup";
            this.LblProductionWorkGroup.Size = new System.Drawing.Size(40, 14);
            this.LblProductionWorkGroup.TabIndex = 39;
            this.LblProductionWorkGroup.Text = "Group";
            this.LblProductionWorkGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCopyDOCt
            // 
            this.BtnCopyDOCt.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyDOCt.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyDOCt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyDOCt.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyDOCt.Appearance.Options.UseBackColor = true;
            this.BtnCopyDOCt.Appearance.Options.UseFont = true;
            this.BtnCopyDOCt.Appearance.Options.UseForeColor = true;
            this.BtnCopyDOCt.Appearance.Options.UseTextOptions = true;
            this.BtnCopyDOCt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyDOCt.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyDOCt.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyDOCt.Image")));
            this.BtnCopyDOCt.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyDOCt.Location = new System.Drawing.Point(405, 90);
            this.BtnCopyDOCt.Name = "BtnCopyDOCt";
            this.BtnCopyDOCt.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyDOCt.TabIndex = 23;
            this.BtnCopyDOCt.ToolTip = "Add Customer Shipping Address";
            this.BtnCopyDOCt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyDOCt.ToolTipTitle = "Run System";
            this.BtnCopyDOCt.Click += new System.EventHandler(this.BtnCopyDOCt_Click);
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 259);
            this.Tp2.Text = "Kawasan Berikat";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.BtnPEB);
            this.panel6.Controls.Add(this.TxtKBSubmissionNo);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.DteKBRegistrationDt);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.DteKBPLDt);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.DteKBContractDt);
            this.panel6.Controls.Add(this.BtnKBContractNo);
            this.panel6.Controls.Add(this.TxtKBRegistrationNo);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.TxtKBPLNo);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.TxtKBContractNo);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 259);
            this.panel6.TabIndex = 12;
            // 
            // BtnPEB
            // 
            this.BtnPEB.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPEB.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPEB.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPEB.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPEB.Appearance.Options.UseBackColor = true;
            this.BtnPEB.Appearance.Options.UseFont = true;
            this.BtnPEB.Appearance.Options.UseForeColor = true;
            this.BtnPEB.Appearance.Options.UseTextOptions = true;
            this.BtnPEB.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPEB.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPEB.Image = ((System.Drawing.Image)(resources.GetObject("BtnPEB.Image")));
            this.BtnPEB.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPEB.Location = new System.Drawing.Point(369, 11);
            this.BtnPEB.Name = "BtnPEB";
            this.BtnPEB.Size = new System.Drawing.Size(24, 21);
            this.BtnPEB.TabIndex = 16;
            this.BtnPEB.ToolTip = "Show PEB Document";
            this.BtnPEB.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPEB.ToolTipTitle = "Run System";
            this.BtnPEB.Click += new System.EventHandler(this.BtnPEB_Click);
            // 
            // TxtKBSubmissionNo
            // 
            this.TxtKBSubmissionNo.EnterMoveNextControl = true;
            this.TxtKBSubmissionNo.Location = new System.Drawing.Point(137, 137);
            this.TxtKBSubmissionNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBSubmissionNo.Name = "TxtKBSubmissionNo";
            this.TxtKBSubmissionNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtKBSubmissionNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBSubmissionNo.Properties.MaxLength = 30;
            this.TxtKBSubmissionNo.Properties.ReadOnly = true;
            this.TxtKBSubmissionNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBSubmissionNo.TabIndex = 28;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(58, 139);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 14);
            this.label23.TabIndex = 27;
            this.label23.Text = "Submission#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(31, 119);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(101, 14);
            this.label16.TabIndex = 25;
            this.label16.Text = "Registration Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBRegistrationDt
            // 
            this.DteKBRegistrationDt.EditValue = null;
            this.DteKBRegistrationDt.EnterMoveNextControl = true;
            this.DteKBRegistrationDt.Location = new System.Drawing.Point(137, 116);
            this.DteKBRegistrationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBRegistrationDt.Name = "DteKBRegistrationDt";
            this.DteKBRegistrationDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteKBRegistrationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.ReadOnly = true;
            this.DteKBRegistrationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBRegistrationDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBRegistrationDt.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(32, 78);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 14);
            this.label17.TabIndex = 21;
            this.label17.Text = "Packing List Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBPLDt
            // 
            this.DteKBPLDt.EditValue = null;
            this.DteKBPLDt.EnterMoveNextControl = true;
            this.DteKBPLDt.Location = new System.Drawing.Point(137, 74);
            this.DteKBPLDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBPLDt.Name = "DteKBPLDt";
            this.DteKBPLDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteKBPLDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBPLDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBPLDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBPLDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBPLDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBPLDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.ReadOnly = true;
            this.DteKBPLDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBPLDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBPLDt.TabIndex = 22;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(48, 35);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 14);
            this.label18.TabIndex = 17;
            this.label18.Text = "Contract Date";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBContractDt
            // 
            this.DteKBContractDt.EditValue = null;
            this.DteKBContractDt.EnterMoveNextControl = true;
            this.DteKBContractDt.Location = new System.Drawing.Point(137, 32);
            this.DteKBContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBContractDt.Name = "DteKBContractDt";
            this.DteKBContractDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteKBContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.ReadOnly = true;
            this.DteKBContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBContractDt.Size = new System.Drawing.Size(140, 20);
            this.DteKBContractDt.TabIndex = 18;
            // 
            // BtnKBContractNo
            // 
            this.BtnKBContractNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnKBContractNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnKBContractNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKBContractNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnKBContractNo.Appearance.Options.UseBackColor = true;
            this.BtnKBContractNo.Appearance.Options.UseFont = true;
            this.BtnKBContractNo.Appearance.Options.UseForeColor = true;
            this.BtnKBContractNo.Appearance.Options.UseTextOptions = true;
            this.BtnKBContractNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnKBContractNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnKBContractNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnKBContractNo.Image")));
            this.BtnKBContractNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnKBContractNo.Location = new System.Drawing.Point(345, 11);
            this.BtnKBContractNo.Name = "BtnKBContractNo";
            this.BtnKBContractNo.Size = new System.Drawing.Size(24, 21);
            this.BtnKBContractNo.TabIndex = 15;
            this.BtnKBContractNo.ToolTip = "Show TPB  Document";
            this.BtnKBContractNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnKBContractNo.ToolTipTitle = "Run System";
            this.BtnKBContractNo.Click += new System.EventHandler(this.BtnKBContractNo_Click);
            // 
            // TxtKBRegistrationNo
            // 
            this.TxtKBRegistrationNo.EnterMoveNextControl = true;
            this.TxtKBRegistrationNo.Location = new System.Drawing.Point(137, 95);
            this.TxtKBRegistrationNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBRegistrationNo.Name = "TxtKBRegistrationNo";
            this.TxtKBRegistrationNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtKBRegistrationNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBRegistrationNo.Properties.MaxLength = 80;
            this.TxtKBRegistrationNo.Properties.ReadOnly = true;
            this.TxtKBRegistrationNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBRegistrationNo.TabIndex = 24;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(52, 99);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 14);
            this.label19.TabIndex = 23;
            this.label19.Text = "Registration#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPLNo
            // 
            this.TxtKBPLNo.EnterMoveNextControl = true;
            this.TxtKBPLNo.Location = new System.Drawing.Point(137, 53);
            this.TxtKBPLNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPLNo.Name = "TxtKBPLNo";
            this.TxtKBPLNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtKBPLNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPLNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPLNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPLNo.Properties.MaxLength = 80;
            this.TxtKBPLNo.Properties.ReadOnly = true;
            this.TxtKBPLNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBPLNo.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(53, 57);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 14);
            this.label20.TabIndex = 19;
            this.label20.Text = "Packing List#";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBContractNo
            // 
            this.TxtKBContractNo.EnterMoveNextControl = true;
            this.TxtKBContractNo.Location = new System.Drawing.Point(137, 11);
            this.TxtKBContractNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBContractNo.Name = "TxtKBContractNo";
            this.TxtKBContractNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtKBContractNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBContractNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBContractNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBContractNo.Properties.MaxLength = 80;
            this.TxtKBContractNo.Properties.ReadOnly = true;
            this.TxtKBContractNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBContractNo.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(69, 14);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 14);
            this.label21.TabIndex = 13;
            this.label21.Text = "Contract#";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.panel7);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(791, 259);
            this.Tp3.Text = "Project";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.ChkProgressInd);
            this.panel7.Controls.Add(this.ChkServiceInd);
            this.panel7.Controls.Add(this.TxtProjectName);
            this.panel7.Controls.Add(this.label30);
            this.panel7.Controls.Add(this.BtnSOContractDocNo2);
            this.panel7.Controls.Add(this.BtnSOContractDocNo);
            this.panel7.Controls.Add(this.TxtPONo);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.TxtProjectCode);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.TxtSOContractDocNo);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(791, 259);
            this.panel7.TabIndex = 0;
            // 
            // ChkProgressInd
            // 
            this.ChkProgressInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkProgressInd.Location = new System.Drawing.Point(432, 6);
            this.ChkProgressInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkProgressInd.Name = "ChkProgressInd";
            this.ChkProgressInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkProgressInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProgressInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkProgressInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkProgressInd.Properties.Appearance.Options.UseFont = true;
            this.ChkProgressInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkProgressInd.Properties.Caption = "Progress";
            this.ChkProgressInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProgressInd.ShowToolTips = false;
            this.ChkProgressInd.Size = new System.Drawing.Size(73, 22);
            this.ChkProgressInd.TabIndex = 48;
            this.ChkProgressInd.CheckedChanged += new System.EventHandler(this.ChkProgressInd_CheckedChanged);
            // 
            // ChkServiceInd
            // 
            this.ChkServiceInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkServiceInd.Location = new System.Drawing.Point(361, 6);
            this.ChkServiceInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkServiceInd.Name = "ChkServiceInd";
            this.ChkServiceInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkServiceInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkServiceInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkServiceInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkServiceInd.Properties.Appearance.Options.UseFont = true;
            this.ChkServiceInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkServiceInd.Properties.Caption = "Service";
            this.ChkServiceInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkServiceInd.ShowToolTips = false;
            this.ChkServiceInd.Size = new System.Drawing.Size(73, 22);
            this.ChkServiceInd.TabIndex = 47;
            this.ChkServiceInd.CheckedChanged += new System.EventHandler(this.ChkServiceInd_CheckedChanged);
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(106, 49);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 80;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(206, 20);
            this.TxtProjectName.TabIndex = 44;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(24, 51);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(81, 14);
            this.label30.TabIndex = 43;
            this.label30.Text = "Project Name";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(337, 9);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(17, 16);
            this.BtnSOContractDocNo2.TabIndex = 40;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract Information";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(315, 9);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(17, 16);
            this.BtnSOContractDocNo.TabIndex = 39;
            this.BtnSOContractDocNo.ToolTip = "Show SO Contract Document";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(106, 70);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 80;
            this.TxtPONo.Properties.ReadOnly = true;
            this.TxtPONo.Size = new System.Drawing.Size(206, 20);
            this.TxtPONo.TabIndex = 46;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(17, 72);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 14);
            this.label29.TabIndex = 45;
            this.label29.Text = "Customer PO#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(106, 28);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 80;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(206, 20);
            this.TxtProjectCode.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(27, 30);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Project Code";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(106, 7);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 80;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(206, 20);
            this.TxtSOContractDocNo.TabIndex = 38;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(22, 10);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 37;
            this.label26.Text = "SO Contract#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDOCt8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 509);
            this.Name = "FrmDOCt8";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVehicleRegNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSAAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePackagingUnitUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResiNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProgressInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label LblSAName;
        public DevExpress.XtraEditors.SimpleButton BtnSAName;
        private System.Windows.Forms.Label label13;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtDocNoInternal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LuePackagingUnitUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtResiNo;
        private System.Windows.Forms.Label lb;
        public DevExpress.XtraEditors.SimpleButton BtnCustomerShipAddress;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        internal DevExpress.XtraEditors.TextEdit TxtFax;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        internal DevExpress.XtraEditors.TextEdit TxtCountry;
        internal DevExpress.XtraEditors.TextEdit TxtCity;
        internal DevExpress.XtraEditors.MemoExEdit MeeSAAddress;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        public DevExpress.XtraEditors.SimpleButton BtnCopyDOCt;
        private System.Windows.Forms.Panel panel6;
        public DevExpress.XtraEditors.SimpleButton BtnPEB;
        internal DevExpress.XtraEditors.TextEdit TxtKBSubmissionNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteKBRegistrationDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.DateEdit DteKBPLDt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.DateEdit DteKBContractDt;
        public DevExpress.XtraEditors.SimpleButton BtnKBContractNo;
        internal DevExpress.XtraEditors.TextEdit TxtKBRegistrationNo;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtKBPLNo;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtKBContractNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtDriver;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtVehicleRegNo;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtExpedition;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private System.Windows.Forms.Label LblProcessInd;
        private DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private System.Windows.Forms.Label LblProductionWorkGroup;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtPONo;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label26;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        protected internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        protected internal DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label30;
        protected internal DevExpress.XtraEditors.CheckEdit ChkServiceInd;
        protected internal DevExpress.XtraEditors.CheckEdit ChkProgressInd;
    }
}