﻿#region Update
// 2018/04/13 [HAR] tambah text dan prubahan warna
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpMatrix2 : RunSystem.FrmBase8
    {
        #region Field

        //private Frm mFrmParent;

        #endregion

        #region Constructor

        public FrmEmpMatrix2(string MenuCode, int HL, int HM, int HH, int ML, int MM, int MH, int LL, int LM, int LH) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            //mFrmParent = FrmParent;
           
            BtnHL.Text = HL.ToString();
            BtnHM.Text = HM.ToString();
            BtnHH.Text = HH.ToString();

            BtnML.Text = ML.ToString();
            BtnMM.Text = MM.ToString();
            BtnMH.Text = MH.ToString();

            BtnLL.Text = LL.ToString();
            BtnLM.Text = LM.ToString();
            BtnLH.Text = LH.ToString();
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            //mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            //BtnFind.Visible = BtnCancel.Visible = BtnDelete.Visible = BtnEdit.Visible = BtnInsert.Visible = BtnSave.Visible = BtnPrint.Visible = false;
            BtnAdd.Visible = false;
        }
    }
}
