﻿namespace RunSystem
{
    partial class FrmDocApprovalPORequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDocApprovalPORequest));
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.TxtUPrice = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtPurchaseUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPtCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPtCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtVdCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtCurCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtUPrice2 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DteQtDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.DteQtDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.DteMaterialRequestDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnMaterialRequest = new DevExpress.XtraEditors.SimpleButton();
            this.TxtInventoryUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtStock = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtMinStock = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtMaxStock = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnItem = new DevExpress.XtraEditors.SimpleButton();
            this.TxtMaterialRequestQty = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtQty2 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtMth01 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth03 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth06 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth09 = new DevExpress.XtraEditors.TextEdit();
            this.TxtMth12 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtMinOrder = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.PicItem = new System.Windows.Forms.PictureBox();
            this.MeeItemRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaterialRequestDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaterialRequestDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaterialRequestQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth01.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth03.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth06.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth09.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeItemRemark.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeItemRemark);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.PicItem);
            this.panel2.Controls.Add(this.TxtMinOrder);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.TxtMth12);
            this.panel2.Controls.Add(this.TxtMth09);
            this.panel2.Controls.Add(this.TxtMth06);
            this.panel2.Controls.Add(this.TxtMth03);
            this.panel2.Controls.Add(this.TxtMth01);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.TxtQty2);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.TxtMaterialRequestQty);
            this.panel2.Controls.Add(this.BtnItem);
            this.panel2.Controls.Add(this.TxtMaxStock);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.TxtMinStock);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.TxtInventoryUomCode);
            this.panel2.Controls.Add(this.TxtStock);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BtnMaterialRequest);
            this.panel2.Controls.Add(this.DteMaterialRequestDocDt);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.DteQtDocDt2);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.DteQtDocDt);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtPtCode2);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtVdCode2);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtCurCode2);
            this.panel2.Controls.Add(this.TxtUPrice2);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.DteUsageDt);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtPtCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtCurCode);
            this.panel2.Controls.Add(this.TxtPurchaseUomCode);
            this.panel2.Controls.Add(this.TxtUPrice);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtDeptCode);
            this.panel2.Controls.Add(this.TxtQty);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(842, 319);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd2);
            this.panel3.Location = new System.Drawing.Point(0, 319);
            this.panel3.Size = new System.Drawing.Size(842, 204);
            this.panel3.Controls.SetChildIndex(this.Grd2, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 118);
            this.Grd1.TabIndex = 65;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(133, 114);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Properties.ReadOnly = true;
            this.TxtQty.Size = new System.Drawing.Size(59, 20);
            this.TxtQty.TabIndex = 19;
            this.TxtQty.ToolTip = "PO Request Quantity";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(10, 117);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 14);
            this.label11.TabIndex = 18;
            this.label11.Text = "Requested Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(132, 291);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(700, 20);
            this.MeeRemark.Properties.ReadOnly = true;
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(700, 22);
            this.MeeRemark.TabIndex = 64;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(81, 294);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 63;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(54, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 5;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(326, 4);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.ReadOnly = true;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(128, 20);
            this.DteDocDt.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(289, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(133, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(155, 20);
            this.TxtDocNo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(54, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(133, 26);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 16;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(343, 20);
            this.TxtDeptCode.TabIndex = 6;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(133, 48);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(86, 20);
            this.TxtItCode.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(31, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "Requested Item";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(221, 48);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(255, 20);
            this.TxtItName.TabIndex = 10;
            // 
            // TxtUPrice
            // 
            this.TxtUPrice.EnterMoveNextControl = true;
            this.TxtUPrice.Location = new System.Drawing.Point(179, 246);
            this.TxtUPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice.Name = "TxtUPrice";
            this.TxtUPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtUPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice.Properties.ReadOnly = true;
            this.TxtUPrice.Size = new System.Drawing.Size(101, 20);
            this.TxtUPrice.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Green;
            this.label7.Location = new System.Drawing.Point(94, 249);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 14);
            this.label7.TabIndex = 47;
            this.label7.Text = "Price";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPurchaseUomCode
            // 
            this.TxtPurchaseUomCode.EnterMoveNextControl = true;
            this.TxtPurchaseUomCode.Location = new System.Drawing.Point(276, 114);
            this.TxtPurchaseUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPurchaseUomCode.Name = "TxtPurchaseUomCode";
            this.TxtPurchaseUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtPurchaseUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPurchaseUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPurchaseUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPurchaseUomCode.Properties.MaxLength = 16;
            this.TxtPurchaseUomCode.Properties.ReadOnly = true;
            this.TxtPurchaseUomCode.Size = new System.Drawing.Size(130, 20);
            this.TxtPurchaseUomCode.TabIndex = 22;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(132, 246);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 16;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(44, 20);
            this.TxtCurCode.TabIndex = 48;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(132, 202);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 16;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(275, 20);
            this.TxtVdCode.TabIndex = 44;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Green;
            this.label8.Location = new System.Drawing.Point(80, 205);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 43;
            this.label8.Text = "Vendor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPtCode
            // 
            this.TxtPtCode.EnterMoveNextControl = true;
            this.TxtPtCode.Location = new System.Drawing.Point(132, 224);
            this.TxtPtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPtCode.Name = "TxtPtCode";
            this.TxtPtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtPtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPtCode.Properties.MaxLength = 16;
            this.TxtPtCode.Properties.ReadOnly = true;
            this.TxtPtCode.Size = new System.Drawing.Size(275, 20);
            this.TxtPtCode.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Green;
            this.label9.Location = new System.Drawing.Point(24, 227);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 14);
            this.label9.TabIndex = 45;
            this.label9.Text = "Term of Payment";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(284, 92);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.ReadOnly = true;
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(122, 20);
            this.DteUsageDt.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(274, 97);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 14);
            this.label10.TabIndex = 16;
            this.label10.Text = "/";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPtCode2
            // 
            this.TxtPtCode2.EnterMoveNextControl = true;
            this.TxtPtCode2.Location = new System.Drawing.Point(592, 224);
            this.TxtPtCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPtCode2.Name = "TxtPtCode2";
            this.TxtPtCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtPtCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPtCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPtCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtPtCode2.Properties.MaxLength = 16;
            this.TxtPtCode2.Properties.ReadOnly = true;
            this.TxtPtCode2.Size = new System.Drawing.Size(242, 20);
            this.TxtPtCode2.TabIndex = 55;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label12.Location = new System.Drawing.Point(485, 227);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 14);
            this.label12.TabIndex = 54;
            this.label12.Text = "Term of Payment";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode2
            // 
            this.TxtVdCode2.EnterMoveNextControl = true;
            this.TxtVdCode2.Location = new System.Drawing.Point(592, 202);
            this.TxtVdCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode2.Name = "TxtVdCode2";
            this.TxtVdCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtVdCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode2.Properties.MaxLength = 16;
            this.TxtVdCode2.Properties.ReadOnly = true;
            this.TxtVdCode2.Size = new System.Drawing.Size(242, 20);
            this.TxtVdCode2.TabIndex = 53;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label13.Location = new System.Drawing.Point(437, 206);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 14);
            this.label13.TabIndex = 52;
            this.label13.Text = "Latest Order From Vendor";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode2
            // 
            this.TxtCurCode2.EnterMoveNextControl = true;
            this.TxtCurCode2.Location = new System.Drawing.Point(592, 246);
            this.TxtCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode2.Name = "TxtCurCode2";
            this.TxtCurCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode2.Properties.MaxLength = 16;
            this.TxtCurCode2.Properties.ReadOnly = true;
            this.TxtCurCode2.Size = new System.Drawing.Size(41, 20);
            this.TxtCurCode2.TabIndex = 57;
            // 
            // TxtUPrice2
            // 
            this.TxtUPrice2.EnterMoveNextControl = true;
            this.TxtUPrice2.Location = new System.Drawing.Point(635, 246);
            this.TxtUPrice2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUPrice2.Name = "TxtUPrice2";
            this.TxtUPrice2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtUPrice2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUPrice2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUPrice2.Properties.Appearance.Options.UseFont = true;
            this.TxtUPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUPrice2.Properties.ReadOnly = true;
            this.TxtUPrice2.Size = new System.Drawing.Size(101, 20);
            this.TxtUPrice2.TabIndex = 58;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(555, 248);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 14);
            this.label14.TabIndex = 56;
            this.label14.Text = "Price";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteQtDocDt
            // 
            this.DteQtDocDt.EditValue = null;
            this.DteQtDocDt.EnterMoveNextControl = true;
            this.DteQtDocDt.Location = new System.Drawing.Point(132, 268);
            this.DteQtDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtDocDt.Name = "DteQtDocDt";
            this.DteQtDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteQtDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteQtDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtDocDt.Properties.ReadOnly = true;
            this.DteQtDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteQtDocDt.TabIndex = 51;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Green;
            this.label15.Location = new System.Drawing.Point(35, 270);
            this.label15.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 14);
            this.label15.TabIndex = 50;
            this.label15.Text = "Quotation Date";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteQtDocDt2
            // 
            this.DteQtDocDt2.EditValue = null;
            this.DteQtDocDt2.EnterMoveNextControl = true;
            this.DteQtDocDt2.Location = new System.Drawing.Point(592, 268);
            this.DteQtDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtDocDt2.Name = "DteQtDocDt2";
            this.DteQtDocDt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteQtDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtDocDt2.Properties.Appearance.Options.UseBackColor = true;
            this.DteQtDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteQtDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtDocDt2.Properties.ReadOnly = true;
            this.DteQtDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtDocDt2.Size = new System.Drawing.Size(109, 20);
            this.DteQtDocDt2.TabIndex = 62;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(496, 270);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 14);
            this.label16.TabIndex = 61;
            this.label16.Text = "Quotation Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteMaterialRequestDocDt
            // 
            this.DteMaterialRequestDocDt.EditValue = null;
            this.DteMaterialRequestDocDt.EnterMoveNextControl = true;
            this.DteMaterialRequestDocDt.Location = new System.Drawing.Point(133, 92);
            this.DteMaterialRequestDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteMaterialRequestDocDt.Name = "DteMaterialRequestDocDt";
            this.DteMaterialRequestDocDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DteMaterialRequestDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaterialRequestDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteMaterialRequestDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteMaterialRequestDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaterialRequestDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteMaterialRequestDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteMaterialRequestDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteMaterialRequestDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaterialRequestDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteMaterialRequestDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaterialRequestDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteMaterialRequestDocDt.Properties.ReadOnly = true;
            this.DteMaterialRequestDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteMaterialRequestDocDt.Size = new System.Drawing.Size(140, 20);
            this.DteMaterialRequestDocDt.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(6, 95);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Requested/Usage Dt";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 118);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(842, 86);
            this.Grd2.TabIndex = 66;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnMaterialRequest
            // 
            this.BtnMaterialRequest.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMaterialRequest.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMaterialRequest.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMaterialRequest.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMaterialRequest.Appearance.Options.UseBackColor = true;
            this.BtnMaterialRequest.Appearance.Options.UseFont = true;
            this.BtnMaterialRequest.Appearance.Options.UseForeColor = true;
            this.BtnMaterialRequest.Appearance.Options.UseTextOptions = true;
            this.BtnMaterialRequest.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMaterialRequest.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMaterialRequest.Image = ((System.Drawing.Image)(resources.GetObject("BtnMaterialRequest.Image")));
            this.BtnMaterialRequest.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMaterialRequest.Location = new System.Drawing.Point(478, 25);
            this.BtnMaterialRequest.Name = "BtnMaterialRequest";
            this.BtnMaterialRequest.Size = new System.Drawing.Size(24, 21);
            this.BtnMaterialRequest.TabIndex = 7;
            this.BtnMaterialRequest.ToolTip = "Material Request Information";
            this.BtnMaterialRequest.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMaterialRequest.ToolTipTitle = "Run System";
            this.BtnMaterialRequest.Click += new System.EventHandler(this.BtnMaterialRequest_Click);
            // 
            // TxtInventoryUomCode
            // 
            this.TxtInventoryUomCode.EnterMoveNextControl = true;
            this.TxtInventoryUomCode.Location = new System.Drawing.Point(245, 158);
            this.TxtInventoryUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode.Name = "TxtInventoryUomCode";
            this.TxtInventoryUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtInventoryUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode.Properties.MaxLength = 16;
            this.TxtInventoryUomCode.Properties.ReadOnly = true;
            this.TxtInventoryUomCode.Size = new System.Drawing.Size(130, 20);
            this.TxtInventoryUomCode.TabIndex = 27;
            // 
            // TxtStock
            // 
            this.TxtStock.EnterMoveNextControl = true;
            this.TxtStock.Location = new System.Drawing.Point(133, 158);
            this.TxtStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStock.Name = "TxtStock";
            this.TxtStock.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStock.Properties.Appearance.Options.UseFont = true;
            this.TxtStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtStock.Properties.ReadOnly = true;
            this.TxtStock.Size = new System.Drawing.Size(109, 20);
            this.TxtStock.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(91, 161);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 14);
            this.label6.TabIndex = 25;
            this.label6.Text = "Stock";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinStock
            // 
            this.TxtMinStock.EnterMoveNextControl = true;
            this.TxtMinStock.Location = new System.Drawing.Point(133, 180);
            this.TxtMinStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinStock.Name = "TxtMinStock";
            this.TxtMinStock.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMinStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinStock.Properties.Appearance.Options.UseFont = true;
            this.TxtMinStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinStock.Properties.ReadOnly = true;
            this.TxtMinStock.Size = new System.Drawing.Size(109, 20);
            this.TxtMinStock.TabIndex = 29;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(40, 183);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 14);
            this.label18.TabIndex = 28;
            this.label18.Text = "Min/Max Stock";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMaxStock
            // 
            this.TxtMaxStock.EnterMoveNextControl = true;
            this.TxtMaxStock.Location = new System.Drawing.Point(266, 180);
            this.TxtMaxStock.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMaxStock.Name = "TxtMaxStock";
            this.TxtMaxStock.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMaxStock.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMaxStock.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMaxStock.Properties.Appearance.Options.UseFont = true;
            this.TxtMaxStock.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMaxStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMaxStock.Properties.ReadOnly = true;
            this.TxtMaxStock.Size = new System.Drawing.Size(109, 20);
            this.TxtMaxStock.TabIndex = 31;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(245, 183);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 14);
            this.label19.TabIndex = 30;
            this.label19.Text = "/";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnItem
            // 
            this.BtnItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItem.Appearance.Options.UseBackColor = true;
            this.BtnItem.Appearance.Options.UseFont = true;
            this.BtnItem.Appearance.Options.UseForeColor = true;
            this.BtnItem.Appearance.Options.UseTextOptions = true;
            this.BtnItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItem.Location = new System.Drawing.Point(478, 48);
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Size = new System.Drawing.Size(24, 21);
            this.BtnItem.TabIndex = 11;
            this.BtnItem.ToolTip = "Item Information";
            this.BtnItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItem.ToolTipTitle = "Run System";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // TxtMaterialRequestQty
            // 
            this.TxtMaterialRequestQty.EnterMoveNextControl = true;
            this.TxtMaterialRequestQty.Location = new System.Drawing.Point(214, 114);
            this.TxtMaterialRequestQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMaterialRequestQty.Name = "TxtMaterialRequestQty";
            this.TxtMaterialRequestQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMaterialRequestQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMaterialRequestQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMaterialRequestQty.Properties.Appearance.Options.UseFont = true;
            this.TxtMaterialRequestQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMaterialRequestQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMaterialRequestQty.Properties.ReadOnly = true;
            this.TxtMaterialRequestQty.Size = new System.Drawing.Size(59, 20);
            this.TxtMaterialRequestQty.TabIndex = 21;
            this.TxtMaterialRequestQty.ToolTip = "Outstanding Material Request Quantity";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(194, 117);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 14);
            this.label20.TabIndex = 20;
            this.label20.Text = "of";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQty2
            // 
            this.TxtQty2.EnterMoveNextControl = true;
            this.TxtQty2.Location = new System.Drawing.Point(771, 246);
            this.TxtQty2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty2.Name = "TxtQty2";
            this.TxtQty2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtQty2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty2.Properties.Appearance.Options.UseFont = true;
            this.TxtQty2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty2.Properties.ReadOnly = true;
            this.TxtQty2.Size = new System.Drawing.Size(63, 20);
            this.TxtQty2.TabIndex = 60;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(740, 249);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 14);
            this.label21.TabIndex = 59;
            this.label21.Text = "Qty";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Blue;
            this.label22.Location = new System.Drawing.Point(534, 8);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 14);
            this.label22.TabIndex = 32;
            this.label22.Text = "Avg Consumption :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMth01
            // 
            this.TxtMth01.EnterMoveNextControl = true;
            this.TxtMth01.Location = new System.Drawing.Point(544, 26);
            this.TxtMth01.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth01.Name = "TxtMth01";
            this.TxtMth01.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth01.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth01.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth01.Properties.Appearance.Options.UseFont = true;
            this.TxtMth01.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth01.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth01.Properties.ReadOnly = true;
            this.TxtMth01.Size = new System.Drawing.Size(101, 20);
            this.TxtMth01.TabIndex = 34;
            // 
            // TxtMth03
            // 
            this.TxtMth03.EnterMoveNextControl = true;
            this.TxtMth03.Location = new System.Drawing.Point(544, 48);
            this.TxtMth03.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth03.Name = "TxtMth03";
            this.TxtMth03.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth03.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth03.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth03.Properties.Appearance.Options.UseFont = true;
            this.TxtMth03.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth03.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth03.Properties.ReadOnly = true;
            this.TxtMth03.Size = new System.Drawing.Size(101, 20);
            this.TxtMth03.TabIndex = 36;
            // 
            // TxtMth06
            // 
            this.TxtMth06.EnterMoveNextControl = true;
            this.TxtMth06.Location = new System.Drawing.Point(544, 70);
            this.TxtMth06.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth06.Name = "TxtMth06";
            this.TxtMth06.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth06.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth06.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth06.Properties.Appearance.Options.UseFont = true;
            this.TxtMth06.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth06.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth06.Properties.ReadOnly = true;
            this.TxtMth06.Size = new System.Drawing.Size(101, 20);
            this.TxtMth06.TabIndex = 38;
            // 
            // TxtMth09
            // 
            this.TxtMth09.EnterMoveNextControl = true;
            this.TxtMth09.Location = new System.Drawing.Point(544, 92);
            this.TxtMth09.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth09.Name = "TxtMth09";
            this.TxtMth09.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth09.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth09.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth09.Properties.Appearance.Options.UseFont = true;
            this.TxtMth09.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth09.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth09.Properties.ReadOnly = true;
            this.TxtMth09.Size = new System.Drawing.Size(101, 20);
            this.TxtMth09.TabIndex = 40;
            // 
            // TxtMth12
            // 
            this.TxtMth12.EnterMoveNextControl = true;
            this.TxtMth12.Location = new System.Drawing.Point(545, 114);
            this.TxtMth12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMth12.Name = "TxtMth12";
            this.TxtMth12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMth12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMth12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMth12.Properties.Appearance.Options.UseFont = true;
            this.TxtMth12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMth12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMth12.Properties.ReadOnly = true;
            this.TxtMth12.Size = new System.Drawing.Size(101, 20);
            this.TxtMth12.TabIndex = 42;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(526, 29);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 14);
            this.label23.TabIndex = 33;
            this.label23.Text = "1";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(526, 51);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 14);
            this.label24.TabIndex = 35;
            this.label24.Text = "3";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(526, 73);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 14);
            this.label25.TabIndex = 37;
            this.label25.Text = "6";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(526, 95);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 14);
            this.label26.TabIndex = 39;
            this.label26.Text = "9";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(519, 117);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 14);
            this.label27.TabIndex = 41;
            this.label27.Text = "12";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinOrder
            // 
            this.TxtMinOrder.EnterMoveNextControl = true;
            this.TxtMinOrder.Location = new System.Drawing.Point(133, 136);
            this.TxtMinOrder.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinOrder.Name = "TxtMinOrder";
            this.TxtMinOrder.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TxtMinOrder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinOrder.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinOrder.Properties.Appearance.Options.UseFont = true;
            this.TxtMinOrder.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinOrder.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinOrder.Properties.ReadOnly = true;
            this.TxtMinOrder.Size = new System.Drawing.Size(140, 20);
            this.TxtMinOrder.TabIndex = 24;
            this.TxtMinOrder.ToolTip = "PO Request Quantity";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(63, 139);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 14);
            this.label28.TabIndex = 23;
            this.label28.Text = "Min. Order";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PicItem
            // 
            this.PicItem.Location = new System.Drawing.Point(657, 9);
            this.PicItem.Name = "PicItem";
            this.PicItem.Size = new System.Drawing.Size(179, 188);
            this.PicItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicItem.TabIndex = 63;
            this.PicItem.TabStop = false;
            // 
            // MeeItemRemark
            // 
            this.MeeItemRemark.EnterMoveNextControl = true;
            this.MeeItemRemark.Location = new System.Drawing.Point(133, 70);
            this.MeeItemRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeItemRemark.Name = "MeeItemRemark";
            this.MeeItemRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MeeItemRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeItemRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeItemRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeItemRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeItemRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeItemRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeItemRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeItemRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeItemRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeItemRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeItemRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeItemRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeItemRemark.Properties.MaxLength = 400;
            this.MeeItemRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeItemRemark.Properties.ReadOnly = true;
            this.MeeItemRemark.Properties.ShowIcon = false;
            this.MeeItemRemark.Size = new System.Drawing.Size(343, 20);
            this.MeeItemRemark.TabIndex = 13;
            this.MeeItemRemark.ToolTip = "F4 : Show/hide text";
            this.MeeItemRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeItemRemark.ToolTipTitle = "Run System";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(22, 73);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(105, 14);
            this.label29.TabIndex = 12;
            this.label29.Text = "Item\'s Description";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDocApprovalPORequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 523);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmDocApprovalPORequest";
            this.Text = "PO Request Information";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPurchaseUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPtCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaterialRequestDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaterialRequestDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaxStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaterialRequestQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth01.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth03.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth06.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth09.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMth12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeItemRemark.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        internal DevExpress.XtraEditors.TextEdit TxtPtCode2;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode2;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode2;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice2;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtPtCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtPurchaseUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtUPrice;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        internal DevExpress.XtraEditors.DateEdit DteQtDocDt2;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteQtDocDt;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.DateEdit DteMaterialRequestDocDt;
        private System.Windows.Forms.Label label17;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        public DevExpress.XtraEditors.SimpleButton BtnMaterialRequest;
        internal DevExpress.XtraEditors.TextEdit TxtMaxStock;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtMinStock;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtStock;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnItem;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtMaterialRequestQty;
        internal DevExpress.XtraEditors.TextEdit TxtQty2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtMth12;
        internal DevExpress.XtraEditors.TextEdit TxtMth09;
        internal DevExpress.XtraEditors.TextEdit TxtMth06;
        internal DevExpress.XtraEditors.TextEdit TxtMth03;
        internal DevExpress.XtraEditors.TextEdit TxtMth01;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtMinOrder;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox PicItem;
        private DevExpress.XtraEditors.MemoExEdit MeeItemRemark;
        private System.Windows.Forms.Label label29;
    }
}