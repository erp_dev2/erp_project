﻿#region Update
/*
    10/03/2021 [IBL/SIER] New Apps
    16/04/2021 [IBL/SIER] BUG: tidak bisa save DOCt yg warehousenya ga punya CostCenter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt11 : RunSystem.FrmBase7
    {
        #region Field

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty;

        private string
            mSQL = string.Empty,
            mDocType = "07",
            mDocNoFormat = string.Empty,
            mMainCurCode = string.Empty;

        private bool
            mIsDOCtApprovalActived = false,
            mIsKawasanBerikatEnabled = false,
            mIsAutoJournalActived = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtAmtRounded = false,
            mIsCheckCOAJournalNotExists = false;
        
        #endregion

        #region Constructor

        public FrmDOCt11(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                GetParameter();
                SetGrd();
                SetSQL();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Finalization",
                    "DO#",
                    "",
                    "Date",
                    "Customer",

                    //6-10
                    "Warehouse",
                    "Total Amount",
                    "Remark",
                    "Create By",
                    "Create Time",

                    //11-14
                    "Create Date",
                    "Last Updated By",
                    "Last Updated Time",
                    "Last Updated Date"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    100, 150, 20, 100, 200,
                    //6-10
                    200, 130, 200, 100, 100,
                    //11-14
                    100, 100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtName, C.WhsName, IfNull(D.TotalAmt, 0) As TotalAmt, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select DocNo, Sum(Qty*UPrice) As TotalAmt ");
	        SQL.AppendLine("    From TblDOCtDtl ");
	        SQL.AppendLine("    Where CancelInd = 'N' ");
	        SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            SQL.AppendLine("Where A.ProcessInd = 'D' ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            Sm.ClearGrd(Grd1, false);

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL,
                new string[]
                {
                    //0
                    "DocNo",  
                    //1-5
                    "DocDt", "CtName", "WhsName", "TotalAmt", "Remark", 
                    //6-8
                    "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 9);
                }, true, false, false, false
            );
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Rows.Count = 0;
        }

        #endregion

        #region Button Method

        protected override void BtnSaveClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;
            SaveData();
        }

        #endregion

        #region SaveData

        protected override void SaveData()
        {
            var l = new List<PrepDOCtHdr>();
            var l2 = new List<PrepDOCtDtl>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1))
                {
                    PrepData(ref l, Row);
                    PrepData2(Sm.GetGrdStr(Grd1, Row, 2), ref l2);
                }
            SavingData(ref l, ref l2);
        }

        private void PrepData(ref List<PrepDOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CurCode, A.Remark, D.EntCode ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-4
                         "DocDt",
                         "CurCode", 
                         "Remark",
                         "EntCode"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PrepDOCtHdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            CurCode = Sm.DrStr(dr, c[2]),
                            Remark = Sm.DrStr(dr, c[3]),
                            EntCode = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepData2(string DocNo, ref List<PrepDOCtDtl> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, CancelInd, DNo, ItCode ");
            SQL.AppendLine("From TblDOCtDtl ");
            SQL.AppendLine("Where DocNo = @DocNo Order By DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-2
                         "CancelInd",
                         "DNo", 
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new PrepDOCtDtl()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            CancelInd = (Sm.DrStr(dr, c[1]) == "Y" ? true : false),
                            CancelIndOld = (Sm.DrStr(dr, c[1]) == "Y" ? true : false),
                            DNo = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SavingData(ref List<PrepDOCtHdr> l, ref List<PrepDOCtDtl> l2)
        {
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();

            if(l.Count > 0)
            {
                foreach (var x in l)
                    if (IsEditedDataNotValid(x, ref l2)) return;

                foreach (var x in l)
                {
                    cml.Add(UpdateCancelledItem(x));
                    cml.Add(EditDOCtHdr(x));
                    cml.Add(SaveStock(x));

                    if (mIsAutoJournalActived && !mIsItemCategoryUseCOAAPAR) cml.Add(SaveJournal(x));
                    if (mIsAutoJournalActived && !mIsItemCategoryUseCOAAPAR) cml.Add(SaveJournal2(x, ref l2));
                }
                Sm.StdMsg(mMsgType.Info, l.Count() + (l.Count > 1 ? " Documents" : " Document") + " has been finalized.");
                ClearGrd();
            }
            else
                IsGridDataInvalid();

            Sm.ExecCommands(cml);
        }

        private bool IsEditedDataNotValid(PrepDOCtHdr x, ref List<PrepDOCtDtl> l2)
        {
            return
                IsClosingJournalInvalid(mIsAutoJournalActived, x.DocDt, x.DocNo) ||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalEditNotValid(x.DocNo, ref l2));
        }

        private bool IsGridDataInvalid()
        {
            bool IsChoose = false;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && IsChoose == false) IsChoose = true;

            if (!IsChoose)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 document.");
                return true;
            }

            return false;
        }

        private bool IsClosingJournalInvalid(bool IsAutoJournalActived, string Dt, string DocNo)
        {
            if (!IsAutoJournalActived) return false;
            if (Sm.IsClosingJournalUseCurrentDt(Dt))
            {
                Sm.StdMsg(mMsgType.Info,
                    "Monthly closing already been done." + Environment.NewLine +
                    "Your transaction will be processed in today's journal." + Environment.NewLine +
                    "Document# : " + DocNo);
                return true;
            }

            return false;
        }

        private bool IsCOAJournalEditNotValid(string DocNo, ref List<PrepDOCtDtl> l2)
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            foreach (var y in l2)
            {
                if (y.DocNo == DocNo)
                {
                    if (y.CancelInd && !y.CancelIndOld && y.ItCode.Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (B.DNo=" + y.DNo + ") ";
                    }
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";


            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");


            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("Select E.AcNo5 As AcNo ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("Select D.ParValue As AcNo ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("Inner Join TblParameter D On D.ParCode='AcNoForCOGS'  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select E.AcNo ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
            SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(X2.ParValue, X3.CtCode) As AcNo ");
            SQL.AppendLine("from TblParameter X2  ");
            SQL.AppendLine("Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("where X2.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("Select X.AcNo ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select E.AcNo4 As AcNo ");
                SQL.AppendLine("    From TblDOCtHdr A ");
                SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(") X ");
                SQL.AppendLine("Group By X.AcNo ");
            }
            else
            {

                SQL.AppendLine("Select Parvalue As AcNo ");
                SQL.AppendLine("from TblParameter where ParCode='AcNoForSaleOfFinishedGoods' ");

            }

            SQL.AppendLine(")T ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateCancelledItem(PrepDOCtHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set CancelInd = 'N', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'Y'; ");            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtHdr(PrepDOCtHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set Remark=@Remark, ProcessInd = 'F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            if (mIsDOCtApprovalActived)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt' ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(PrepDOCtHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And B.CancelInd = 'N'; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(PrepDOCtHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            if (mDocNoFormat == "1")
                SQL.AppendLine(Sm.GetNewJournalDocNo(x.DocDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo3(x.DocDt, x.EntCode, "1"));
            //else
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                else
                    SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Group By E.AcNo5 ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
            else
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
            else
                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }

            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(PrepDOCtHdr x, ref List<PrepDOCtDtl> l2)
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(x.DocDt);

            foreach (var y in l2)
            {
                if (y.DocNo == x.DocNo)
                {
                    if (y.CancelInd && !y.CancelIndOld && y.ItCode.Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (B.DNo=" + y.DNo + ") ";
                    }
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            if (Filter.Length > 0)
            {
                SQL.AppendLine("Update TblDOCtDtl Set JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
                SQL.AppendLine("And Exists(Select 1 From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null) ");
                SQL.AppendLine("And JournalDocNo Is Null; ");

                SQL.AppendLine("Insert Into TblJournalHdr ");
                SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, ");
                if (IsClosingJournalUseCurrentDt)
                    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                else
                    SQL.AppendLine("DocDt, ");
                SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
                SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblJournalHdr ");
                SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select D.ParValue As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                else
                    SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
                SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
                SQL.AppendLine("        Union All ");

                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("        Select X.AcNo, ");
                    SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("            Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                    else
                        SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X ");
                    SQL.AppendLine("        Group By X.AcNo ");
                }
                else
                {
                    SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                    SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                    else
                        SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X1 ");
                    SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                    SQL.AppendLine("        Group By X2.ParValue ");
                }
                SQL.AppendLine("    ) Tbl Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select 1; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", x.EntCode, "1"));
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(x.DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(x.DocDt, "Journal", "TblJournalHdr", x.EntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", x.CurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsDOCtApprovalActived = Sm.GetParameterBoo("IsDOCtApprovalActived");
            mIsKawasanBerikatEnabled = Sm.GetParameterBoo("IsKawasanBerikatEnabled");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsItemCategoryUseCOAAPAR = Sm.GetParameterBoo("IsItemCategoryUseCOAAPAR");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #region Grid Event

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (Grd1.Rows.Count > 0)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
                }
            }
        }

        #endregion

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Class
        private class PrepDOCtHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public string EntCode { get; set; }
            public string Remark { get; set; }
        }

        private class PrepDOCtDtl
        {
            public string DocNo { get; set; }
            public bool CancelInd { get; set; }
            public bool CancelIndOld { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
        }
        class COA
        {
            public string AcNo { set; get; }
        }
        #endregion
    }
}
