﻿#region Update
/*
    30/09/2020 [WED/IMS] new apps
    26/10/2020 [WED/IMS] hanya bisa edit nilai Received nya, lalu bikin dokumen Received from Other Warehouse
    04/11/2020 [ICA/IMS] Memunculkan preview setelah save
    12/11/2020 [TKG/IMS] tambah item name di printout
    30/11/2020 [IBL/IMS] tambah unit/bagian (ambil dari costcenter warehouse) di printout
    09/12/2020 [WED/IMS] ubah tarikan journal
    15/12/2020 [WED/IMS] tambah approval di header
    11/03/2021 [TKG/IMS] menambah heat number
    13/03/2021 [TKG/IMS] merubah proses GetParameter
    13/03/2021 [TKG/IMS] menambah heat# saat print
    20/04/2021 [TKG/IMS] Transfer Request Warehouse# dan DOW disembunyikan
    30/04/2021 [TKG/IMS] ubah proses RecomputeStock
    15/06/2021 [BRI/IMS] tambah cancel berdasarkan parameter IsDOWhs4Cancellable
    13/07/2021 [ICA/IMS] menambah DO To Other Warehouse For Project DocNo di Journal Description
    26/07/2021 [TKG/IMS] ubah journal berdasarkan MovingAvgInd di TblWarehouse
    08/05/2021 [TKG/IMS] ubah proses journal
    10/06/2022 [VIN/IMS] tambah validasi IsQtyInvalid2 (received tidak boleh lebih dari stock whs asal)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs4 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty, mAccessInd = string.Empty,
           mDocNo = string.Empty, //if this application is called from other application;
           mPGCode = string.Empty; 
        internal FrmDOWhs4Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsSystemUseCostCenter = false;
        internal bool 
            mIsTransferRequestWhsProjectEnabled = false,
            mIsItGrpCodeShow = false,
            mIsBOMShowSpecifications= false,
            mIsRecvWhs4ProcessDOWhsPartialEnabled = false,
            mIsRecvWhs4ProcessTo1Journal = false,
            mIsAutoJournalActived = false,
            mIsDOWhs4HeatNumberEnabled = false,
            mIsDOWhs4Cancellable = false;
        private string mDocType = "27", mDocType2 = "26";
        private bool mIsDOWhs4ApprovalBasedOnWhs = false;

        #endregion

        #region Constructor

        public FrmDOWhs4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "DO To Warehouse (Transfer Request) for Project";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                //BtnEdit.Visible = false;
                if (!mIsDOWhs4Cancellable)
                {
                    label12.Visible = false;
                    MeeCancelReason.Visible = false;
                    ChkCancelInd.Visible = false;
                }
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
                Tp2.PageVisible = mIsTransferRequestWhsProjectEnabled;
                SetGrd();
                Tc1.SelectedTabPage = Tp2;
                SetLueItCode(ref LueItCode, string.Empty);
                Tc1.SelectedTabPage = Tp1;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "",
                    "",
                    "Item's Code",
                    "Item's Name",
                    "Stock",

                    //6-10
                    "Requested",
                    "UoM",
                    "Stock",
                    "Requested",
                    "UoM",

                    //11-15
                    "Stock",
                    "Requested",
                    "UoM",
                    "Remark",
                    "Cost Category",

                    //16-20
                    "Group",
                    "Local Code",
                    "Specification",
                    "Warehouse Code",
                    "Warehouse from",

                     //21-25
                    "Transfer Request Warehouse#",
                    "Transfer Request Warehouse D#",
                    "DOW#",
                    "DOW D#",
                    "TRPDNo",

                    //26-30
                    "Source",
                    "Batch#",
                    "PropCode",
                    "Lot",
                    "Bin",

                    //31-35
                    "InitQty", 
                    "Cancelled",
                    "Received",
                    "IsReceived",
                    "Outstanding",

                    //36
                    "Heat Number"
                },
                 new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 20, 100, 200, 100, 

                    //6-10
                    100, 80, 100, 100, 80, 

                    //11-15
                    100, 100, 80, 200, 150, 

                    //16-20
                    150, 180, 200, 0, 200,

                    //21-25
                    0, 0, 0, 0, 0, 

                    //26-30
                    200, 200, 100, 100, 100, 

                    //31-35
                    0, 60, 100, 0, 0,

                    //36
                    200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 8, 9, 11, 12, 31, 33, 35 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 2 });
            Sm.GrdColCheck(Grd1, new int[] { 32, 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 9, 10, 11, 12, 13, 17, 19, 25, 26, 27, 28, 29, 30, 31, 32 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3, 4, 5, 7, 8, 10, 11, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 34, 35, 36 });

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(5);
            }
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 18 });
            Grd1.Cols[17].Move(4);
            Grd1.Cols[18].Move(7);
            Grd1.Cols[20].Move(6);

            if (mIsDOWhs4HeatNumberEnabled)
                Grd1.Cols[36].Move(28);
            else
                Grd1.Cols[36].Visible = false;
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 17, 26, 27, 28, 29, 30 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length == 0))
            {
                if (mNumberOfInventoryUomCode == 2)
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                if (mNumberOfInventoryUomCode == 3)
                    Sm.GrdColInvisible(Grd1, new int[] { 13, 15 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode2, MeeRemark, LueItCode, MeeCancelReason
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9, 12, 14, 33 });
                    Sm.GrdColInvisible(Grd1, new int[] { 8, 11 }, false);
                    BtnPGCode.Enabled = false;
                    ChkCancelInd.Enabled = false;
                    BtnTransferRequestProjectDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {  6, 9, 12, 14 });
                    Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 8, 11 }, true);
                    DteDocDt.Focus();
                    BtnTransferRequestProjectDocNo.Enabled = true;
                    break;
                case mState.Edit:
                    if (mIsDOWhs4Cancellable)
                    {
                        ChkCancelInd.Enabled = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    }
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 33 });
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode2, 
                MeeRemark, TxtProjectCode, TxtProjectName, TxtSOCDocNo, 
                LueItCode, TxtTransferRequestProjectDocNo, TxtStatus, MeeCancelReason
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 8, 9, 11, 12, 33, 35 });
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 32 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOWhs4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;
            SetFormControl(mState.Edit);
            Grd1Format();
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                {
                    if (MeeCancelReason.Text.Length > 0)
                        EditData(sender, e);
                    else
                        CreateRecvWhs();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { // "DOWhs2Hdr", "DOWhs2Hdr2", "DOWhs2Hdr3", 
                                     "DOWhs2Hdr4", 
                                     // "DOWhs2Dtl", 
                                     "DOWhs2Dtl2" };


            var l3 = new List<DOWhs2Hdr4>();
            var ldtl2 = new List<DOWhs2Dtl2>();
            decimal Numb = 0;

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header 4
            var cm4 = new MySqlCommand();
            var SQLHdr4 = new StringBuilder();

            SQLHdr4.AppendLine("SELECT @CompanyLogo As CompanyLogo, ");
            SQLHdr4.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, ");
            SQLHdr4.AppendLine("B.WhsName, A.TransferRequestProjectDocNo, C.UserName, D.CCName ");
            SQLHdr4.AppendLine("FROM TblDOWhs4Hdr A ");
            SQLHdr4.AppendLine("Inner Join TblWarehouse B On A.WhsCode2=B.WhsCode  ");
            SQLHdr4.AppendLine("INNER JOIN TblUser C ON A.CreateBy=C.UserCode ");
            SQLHdr4.AppendLine("INNER JOIN TblCostCenter D ON B.CCCode = D.CCCode ");
            SQLHdr4.AppendLine("Where A.DocNo=@DocNo");
            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQLHdr4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm4, "@CompanyLogo", @Sm.CompanyLogo());
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                {
                    //0
                    "DocNo",

                    //1-5
                    "CompanyLogo",
                    "DocDt",
                    "WhsName",
                    "TransferRequestProjectDocNo",
                    "UserName",

                    //6
                    "CCName"
                });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l3.Add(new DOWhs2Hdr4()
                        {
                            DocNo = Sm.DrStr(dr4, c4[0]),
                            CompanyLogo = Sm.DrStr(dr4, c4[1]),
                            DocDt = Sm.DrStr(dr4, c4[2]),
                            WhsName = Sm.DrStr(dr4, c4[3]),
                            TransferRequestWhsDocNo = Sm.DrStr(dr4, c4[4]),
                            UserName = Sm.DrStr(dr4, c4[5]),
                            CCName = Sm.DrStr(dr4, c4[6]),
                            PrintDt = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }

                dr4.Close();
            }
            myLists.Add(l3);

            #endregion

            #region Detail 2
            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select E.ItCodeInternal, ");
                if (mIsDOWhs4HeatNumberEnabled)
                    SQLDtl2.AppendLine("Case When I.Value2 Is Null Then E.ItName Else Concat(E.ItName, ' (Heat#:', I.Value2, ')') End As ItName, ");
                else
                    SQLDtl2.AppendLine("E.ItName, ");
                SQLDtl2.AppendLine("E.Specification, D.Qty, E.InventoryUomCode, H.ProjectName, A.Remark, ");
                SQLDtl2.AppendLine("H.ProjectName AS ProjectName2, C.WhsName ");
                SQLDtl2.AppendLine("From TblDOWhs4Dtl A ");
                SQLDtl2.AppendLine("Inner Join TblTransferRequestWhsHdr B On A.TransferRequestWhsDocNo=B.DocNo And B.CancelInd='N' ");
                SQLDtl2.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
                SQLDtl2.AppendLine("Inner Join TblDOWhsDtl D On A.DOWhsDocNo=D.DocNo And A.DOWhsDNo=D.DNo ");
                SQLDtl2.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQLDtl2.AppendLine("Inner Join TblDOWhs4Hdr F On A.DocNo=F.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblTransferRequestProjectHdr G On F.TransferRequestProjectDocNo=G.DocNo ");
                SQLDtl2.AppendLine("Left Join TblProjectGroup H On G.PGCode=H.PGCode ");
                if (mIsDOWhs4HeatNumberEnabled)
                    SQLDtl2.AppendLine("Left Join TblSourceInfo I On D.Source=I.Source ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo; ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                         //0
                         "ItCodeInternal",
                         
                         //1-5
                         "ItName",
                         "Specification",
                         "Qty",
                         "InventoryUomCode",
                         "ProjectName",
                         
                         //6-8
                         "ProjectName2",
                         "Remark",
                         "WhsName"    
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        Numb = Numb + 1;
                        ldtl2.Add(new DOWhs2Dtl2()
                        {
                            Number = Numb,
                            ItCodeInternal = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItName = Sm.DrStr(drDtl2, cDtl2[1]),
                            Specification = Sm.DrStr(drDtl2, cDtl2[2]),
                            Qty = Sm.DrDec(drDtl2, cDtl2[3]),
                            InventoryUomCode = Sm.DrStr(drDtl2, cDtl2[4]),
                            ProjectName = Sm.DrStr(drDtl2, cDtl2[5]),
                            ProjectName2 = Sm.DrStr(drDtl2, cDtl2[6]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[7]),
                            WhsName = Sm.DrStr(drDtl2, cDtl2[8])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            Sm.PrintReport("DOWhs4IMS", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTransferRequestProjectDocNo, "Transfer Request#", false))
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmDOWhs4Dlg(this));
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtTransferRequestProjectDocNo, "Transfer Request#", false))
                Sm.FormShowDialog(new FrmDOWhs4Dlg(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 9, 12 }, e);

            if (e.ColIndex == 6)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 3, 6, 9, 12, 7, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 3, 6, 12, 9, 7, 13, 10);
            }

            if (e.ColIndex == 9)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 3, 9, 6, 12, 10, 7, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 3, 9, 12, 6, 10, 13, 7);
            }

            if (e.ColIndex == 12)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 3, 12, 6, 9, 13, 7, 10);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 3, 12, 9, 6, 13, 10, 7);
            }

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 10)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 9 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 9);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOWhs4", "TblDOWhs4Hdr");

            ProcessDataTransferRequestWhs();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOWhs4Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    cml.Add(SaveDOWhs4Dtl(DocNo, Row));

            cml.Add(UpdateTransferRequestProjectProcessInd(TxtTransferRequestProjectDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }


        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtTransferRequestProjectDocNo, "Transfer Request#", false) ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse to") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsItemExceedInitialQty() ||
                IsItemTransferRequestProjectCancelled()
                ;
        }

        private bool IsItemTransferRequestProjectCancelled()
        {
            string DNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (DNo.Length > 0) DNo += ",";
                DNo += Sm.GetGrdStr(Grd1, i, 25);
            }

            if (DNo.Length > 0)
            {
                var SQL = new StringBuilder();
                string DNos = string.Empty;
                string[] arrDNos = { };
                string CancelledDNo = string.Empty;

                SQL.AppendLine("Select Group_Concat(DNo) DNo ");
                SQL.AppendLine("From TblTransferRequestProjectDtl ");
                SQL.AppendLine("Where DocNo = @Param1 ");
                SQL.AppendLine("And Find_In_Set(DNo, @Param2) ");
                SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C'); ");

                DNos = Sm.GetValue(SQL.ToString(), TxtTransferRequestProjectDocNo.Text, DNo, string.Empty);

                if (DNos.Trim().Length > 0)
                {
                    arrDNos = DNos.Split(',');
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        foreach(var x in arrDNos.Where(w => w == Sm.GetGrdStr(Grd1, i, 25)))
                        {
                            if (CancelledDNo.Length > 0) CancelledDNo += ",";
                            CancelledDNo += (i + 1).ToString();
                        }
                    }
                    Sm.StdMsg(mMsgType.Warning, "One or more item(s) in the list is already cancelled or inactive. Row : #" + CancelledDNo);
                    return true;
                }
            }

            return false;
        }

        private bool IsItemExceedInitialQty()
        {
            RecomputeInitialQty();

            string TRPDNo = string.Empty;
            decimal TotalQty = 0m, InitQty = 0m;
            int temp = 0;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                for (int j = i; j < Grd1.Rows.Count - 1; ++j)
                {
                    if (TRPDNo.Length == 0) TRPDNo = Sm.GetGrdStr(Grd1, j, 25);
                    if (TRPDNo == Sm.GetGrdStr(Grd1, j, 25)) 
                    {
                        TotalQty += Sm.GetGrdDec(Grd1, j, 6);
                        InitQty = Sm.GetGrdDec(Grd1, j, 31);
                        temp = j;
                    }
                }

                if (TotalQty > InitQty)
                {
                    Sm.StdMsg(mMsgType.Warning, "Your total requested quantity (" + Sm.FormatNum(TotalQty, 0) + ") is exceeding outstanding quantity (" + Sm.FormatNum(InitQty, 0) + ").");
                    Sm.FocusGrd(Grd1, temp, 6);
                    return true;
                }

                TRPDNo = string.Empty;
                TotalQty = 0m;
                InitQty = 0m;
                temp = i;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.")) return true;
              
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 6) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 6) > Sm.GetGrdDec(Grd1, Row, 5))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[9].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 9) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 9) > Sm.GetGrdDec(Grd1, Row, 8))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[11].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        //private void ReComputeStock1()
        //{
        //    string Filter = string.Empty, ItCode = string.Empty, WhsCode = string.Empty;
        //    var cm = new MySqlCommand();
        //    var SQL = new StringBuilder();
    
        //    int No = 1;
        //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
        //        {
        //            if (Filter.Length > 0) Filter += " Or ";
        //            Filter += " Concat(X.ItCode, Y.WhsCode) = Concat(@ItCode" + No + ", @WhsCode" + No + ") ";
        //            Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd1, Row, 3));
        //            Sm.CmParam<String>(ref cm, "@WhsCode" + No, Sm.GetGrdStr(Grd1, Row, 19));
        //            No += 1;
        //        }
        //    }
       
        //    SQL.AppendLine("Select A.ItCode, A.WhsCode, ");
        //    SQL.AppendLine("IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0) As Qty, ");
        //    SQL.AppendLine("IfNull(A.StockQty2, 0)-IfNull(B.TransferredQty2, 0)-IfNull(C.DOQty2, 0) As Qty2, ");
        //    SQL.AppendLine("IfNull(A.StockQty3, 0)-IfNull(B.TransferredQty3, 0)-IfNull(C.DOQty3, 0) As Qty3 ");
        //    SQL.AppendLine("From ( ");
        //    SQL.AppendLine("    Select ItCode, WhsCode, ");
        //    SQL.AppendLine("    Sum(Qty) As StockQty, Sum(Qty2) As StockQty2, Sum(Qty3) As StockQty3 ");
        //    SQL.AppendLine("    From TblStockSummary ");
        //    SQL.AppendLine("    Where Qty<>0 ");
        //    SQL.AppendLine("    And (" + Filter.Replace("X.", "").Replace("Y.", "") + ") ");
        //    SQL.AppendLine("    Group By ItCode, WhsCode ");
        //    SQL.AppendLine(") A ");
        //    SQL.AppendLine("Left join ( ");
        //    SQL.AppendLine("    Select T2.ItCode, T1.WhsCode, ");
        //    SQL.AppendLine("    Sum(T2.Qty) As TransferredQty, ");
        //    SQL.AppendLine("    Sum(T2.Qty2) As TransferredQty2, ");
        //    SQL.AppendLine("    Sum(T2.Qty3) As TransferredQty3 ");
        //    SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
        //    SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd ='O' ");
        //    SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.").Replace("Y.", "T1.") + ") ");
        //    SQL.AppendLine("    Where T1.CancelInd='N' ");
        //    SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
        //    SQL.AppendLine("    Group By T2.ItCode, T1.WhsCode ");
        //    SQL.AppendLine(") B On A.ItCode=B.ItCode And A.WhsCode = B.WhsCode ");
        //    SQL.AppendLine("Left join ( ");
        //    SQL.AppendLine("    Select T4.ItCode, T1.WhsCode, ");
        //    SQL.AppendLine("    Sum(T4.Qty) As DOQty, ");
        //    SQL.AppendLine("    Sum(T4.Qty2) As DOQty2, ");
        //    SQL.AppendLine("    Sum(T4.Qty3) As DOQty3 ");
        //    SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
        //    SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' ");
        //    SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.").Replace("Y.", "T1.") + ") ");
        //    SQL.AppendLine("    Inner Join TblDOWhsHdr T3 ");
        //    SQL.AppendLine("        On T3.TransferRequestWhsDocNo Is Not Null ");
        //    SQL.AppendLine("        And T2.DocNo=IfNull(T3.TransferRequestWhsDocNo, '') ");
        //    SQL.AppendLine("        And T3.CancelInd ='N' ");
        //    SQL.AppendLine("        And T3.Status In ('A', 'O') ");
        //    SQL.AppendLine("    Inner Join TblDOWhsDtl T4 ");
        //    SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
        //    SQL.AppendLine("        And T2.ItCode=T4.ItCode ");
        //    SQL.AppendLine("        And T4.ProcessInd='O' ");
        //    SQL.AppendLine("        And T4.CancelInd='N' ");
        //    SQL.AppendLine("        And (" + Filter.Replace("X.", "T4.").Replace("Y.", "T3.") + ") ");
        //    SQL.AppendLine("    Where T1.CancelInd='N' ");
        //    SQL.AppendLine("    And IfNull(T1.Status, 'O')='A' ");
        //    SQL.AppendLine("    Group By T4.ItCode, T1.WhsCode ");
        //    SQL.AppendLine(") C On A.ItCode=C.ItCode; ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "WhsCode", "Qty", "Qty2", "Qty3" });
        //        if (dr.HasRows)
        //        {
        //            Grd1.BeginUpdate();
        //            while (dr.Read())
        //            {
        //                ItCode = Sm.DrStr(dr, 0);
        //                WhsCode = Sm.DrStr(dr, 1);
        //                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //                {
        //                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 3), ItCode) && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), WhsCode))
        //                    {
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 2);
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 3);
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 4);
        //                        break;
        //                    }
        //                }
        //            }
        //            Grd1.EndUpdate();
        //        }
        //        dr.Close();
        //    }
        //}

        private void ReComputeStock()
        {
            string 
                Filter = string.Empty, 
                Source = string.Empty, 
                WhsCode = string.Empty,
                Lot = string.Empty,
                Bin = string.Empty;
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (int r=0; r<Grd1.Rows.Count - 1;r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 25).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (C.WhsCode=@WhsCode_"+r.ToString()+" And C.Lot=@Lot_"+r.ToString()+" And C.Bin=@Bin_"+r.ToString()+" And C.Source=@Source_"+r.ToString()+") ";
                    
                    Sm.CmParam<String>(ref cm, "@WhsCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 19));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 29));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 30));
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 25));
                }
            }

            if (Filter.Length > 0)
                Filter = " And " + Filter;
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select C.WhsCode, C.Lot, C.Bin, C.Source, ");
            SQL.AppendLine("C.Qty-IfNull(D.Qty, 0.00) As Qty,  ");
            SQL.AppendLine("C.Qty2-IfNull(D.Qty2, 0.00) As Qty2, ");
            SQL.AppendLine("C.Qty3-IfNull(D.Qty3, 0.00) As Qty3 ");
            SQL.AppendLine("From TblTransferRequestProjectHdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestProjectDtl B  ");
            SQL.AppendLine("    On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Status In ('O', 'A')  ");
            SQL.AppendLine("    And B.WhsCode Is Null ");
            SQL.AppendLine("Inner Join TblStockSummary C On B.ItCode=C.ItCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X5.WhsCode, X6.Lot, X6.Bin, X6.Source,  ");
            SQL.AppendLine("    Sum(X6.Qty) As Qty, Sum(X6.Qty2) As Qty2, Sum(X6.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblTransferRequestProjectHdr X1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl X2  ");
            SQL.AppendLine("        On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("        And X2.CancelInd='N'  ");
            SQL.AppendLine("        And X2.Status In ('O', 'A') ");
            SQL.AppendLine("        And X2.WhsCode Is Null ");
            SQL.AppendLine("    Inner Join TblDOWhs4Hdr X3 On X1.DocNo=X3.TransferRequestProjectDocNo And X3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblDOWhs4Dtl X4 On X3.DocNo=X4.DocNo And X2.DocNo=X4.TransferRequestProjectDocNo And X2.DNo=X4.TransferRequestProjectDNo ");
            SQL.AppendLine("    Inner Join TblDOWhsHdr X5 On X4.DOWhsDocNo=X5.DocNo And X5.CancelInd='N' And X5.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl X6 On X5.DocNo=X6.DocNo And X4.DOWhsDocNo=X6.DocNo And X4.DOWhsDNo=X6.DNo And X6.CancelInd='N'  ");
            SQL.AppendLine("    Where X1.CancelInd='N' ");
            SQL.AppendLine("    And X1.Status In ('O', 'A') ");
            SQL.AppendLine("    And X6.Qty-X4.RecvQty>0.00 ");
            SQL.AppendLine("    And X1.DocNo=@TransferRequestProjectDocNo ");
            SQL.AppendLine(") D  ");
            SQL.AppendLine("    On C.WhsCode=D.WhsCode ");
            SQL.AppendLine("    And C.Lot=D.Lot ");
            SQL.AppendLine("    And C.Bin=D.Bin ");
            SQL.AppendLine("    And C.Source=D.Source ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.Status In ('O', 'A') ");
            //SQL.AppendLine("And C.Qty-IfNull(D.Qty, 0.00)>0.00 ");
            SQL.AppendLine("And A.DocNo=@TransferRequestProjectDocNo ");

            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", TxtTransferRequestProjectDocNo.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "WhsCode", 
                    "Lot", "Bin", "Source", "Qty", "Qty2", 
                    "Qty3" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        WhsCode = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        Source = Sm.DrStr(dr, 3);
                        Qty = Sm.DrDec(dr, 4);
                        Qty2 = Sm.DrDec(dr, 5);
                        Qty3 = Sm.DrDec(dr, 6);
                        for (int r= 0; r< Grd1.Rows.Count - 1;r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 19), WhsCode) && 
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 29), Lot) && 
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 30), Bin) && 
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 25), Source))
                            {
                                Grd1.Cells[r, 5].Value = Qty;
                                Grd1.Cells[r, 8].Value = Qty2;
                                Grd1.Cells[r, 11].Value = Qty3;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void RecomputeInitialQty()
        {
            string TRPDoc = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (TRPDoc.Length > 0) TRPDoc += ",";
                TRPDoc += string.Concat(TxtTransferRequestProjectDocNo.Text, Sm.GetGrdStr(Grd1, i, 25));
            }

            if(TRPDoc.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                string TRPDNo = string.Empty;

                SQL.AppendLine("Select A.DNo, (A.Qty - IfNull(B.UsedQty, 0.00)) InitQty ");
                SQL.AppendLine("From TblTransferRequestProjectDtl A ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.TransferRequestProjectDocNo, T1.TransferRequestProjectDNo, Sum(T3.Qty) UsedQty ");
                SQL.AppendLine("    From TblDOWhs4Dtl T1 ");
                SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl T2 On T1.TransferRequestProjectDocNo = T2.DocNo And T1.TransferRequestProjectDNo = T2.DNo ");
                SQL.AppendLine("        And Find_In_Set(Concat(T2.DocNo, T2.DNo), @TRPDoc) ");
                SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T3 On T1.TransferRequestWhsDocNo = T3.DocNo And T1.TransferRequestWhsDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T4 On T3.DocNo = T4.DocNo And T4.CancelInd = 'N' And T4.Status In ('A', 'O') ");
                SQL.AppendLine("    Group By T1.TransferRequestProjectDocNo, T1.TransferRequestProjectDNo ");
                SQL.AppendLine(") B On A.DocNo = B.TransferRequestProjectDocNo And A.DNo = B.TransferRequestProjectDNo ");
                SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @TRPDoc); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    Sm.CmParam<String>(ref cm, "@TRPDoc", TRPDoc);
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DNo", "InitQty" });
                    if (dr.HasRows)
                    {
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            TRPDNo = Sm.DrStr(dr, 0);
                            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 25), TRPDNo))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 31, 1);
                                }
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                }
            }
        }

        void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int Col)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(X.ItCode=@ItCode" + No + ") ";
            Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd, Row, Col));
        }

        private MySqlCommand SaveDOWhs4Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhs4Hdr(DocNo, DocDt, CancelReason, CancelInd, Status, LocalDocNo, WhsCode2, TransferRequestProjectDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', @LocalDocNo, @WhsCode2, @TransferRequestProjectDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='DOWhs4' ");
            if (mIsDOWhs4ApprovalBasedOnWhs)
                SQL.AppendLine("And WhsCode = @WhsCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblDOWhs4Hdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", TxtTransferRequestProjectDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveDOWhs4Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhs4Dtl(DocNo, DNo, TransferRequestProjectDocNo, TransferRequestProjectDNo, TransferRequestWhsDocNo, TransferRequestWhsDNo, DOWhsDocNo, DOWhsDNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @TransferRequestProjectDocNo, @TransferRequestProjectDNo, @TransferRequestWhsDocNo, @TransferRequestWhsDNo, @DOWhsDocNo, @DOWhsDNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", TxtTransferRequestProjectDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDNo", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDocNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDNo", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@DOWhsDocNo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@DOWhsDNo", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateTransferRequestProjectProcessInd(string TransferRequestProjectDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTransferRequestProjectDtl A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.TransferRequestProjectDocNo, T1.TransferRequestProjectDNo, Sum(T3.Qty) Qty ");
            SQL.AppendLine("    From TblDOWhs4Dtl T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl T2 On T1.TransferRequestProjectDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.TransferRequestProjectDNo = T2.DNo ");
            SQL.AppendLine("        And T2.DocNo = @TransferRequestProjectDocNo ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T3 On T1.TransferRequestWhsDocNo = T3.DocNo ");
            SQL.AppendLine("        And T1.TransferRequestWhsDNo = T3.DNo ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T4 On T3.DocNo = T4.DocNo And T4.CancelInd = 'N' And T4.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblDOWhs4Hdr T5 On T1.DocNo = T5.DocNo And T5.Status = 'A' And T5.CancelInd = 'N' ");
            SQL.AppendLine("    Group By T1.TransferRequestProjectDocNo, T1.TransferRequestProjectDNo ");
            SQL.AppendLine(") B On A.DocNo = B.TransferRequestProjectDocNo And A.DNo = B.TransferRequestProjectDNo ");
            SQL.AppendLine("Set A.ProcessInd = Case ");
            SQL.AppendLine("    When A.Qty <= B.Qty Then 'F' ");
            SQL.AppendLine("    When A.Qty > B.Qty And B.Qty Is Not Null And B.Qty != 0 Then 'P' ");
            SQL.AppendLine("    Else 'O' ");
            SQL.AppendLine("End, A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", TransferRequestProjectDocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTransferRequestWhsHdr(TransferReqProjectHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsHdr(DocNo, DocDt, LocalDocNo, CancelInd, WhsCode, WhsCode2, PGCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, 'N', @WhsCode, @WhsCode2, @PGCode, @SOContractDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='DOWhs4' ");
            if (mIsDOWhs4ApprovalBasedOnWhs)
                SQL.AppendLine("And WhsCode = @WhsCode2 ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblTransferRequestWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='DOWhs4' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt",x.DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", x.LocalDocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@WhsCode2", x.WhsCode2);
            Sm.CmParam<String>(ref cm, "@PGCode", x.PGCode);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", x.SOContractDocNo);
            return cm;
        }

        private MySqlCommand SaveTransferRequestWhsDtl(TransferReqProjectDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsDtl(DocNo, DNo, ProcessInd, ItCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'F', @ItCode, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", y.ItCode);
            Sm.CmParam<Decimal>(ref cm, "@Qty", y.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", y.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", y.Qty3);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark2);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOWhs2Hdr(DOWhsHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsHdr(DocNo, DocDt, CancelInd, Status, LocalDocNo, WhsCode, WhsCode2, TransferRequestWhsDocNo, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @LocalDocNo, @WhsCode, (Select WhsCode2 From TblTransferRequestWhsHdr Where DocNo=@TRWhsDocNo), @TRWhsDocNo, ");
            SQL.AppendLine("@KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='DOWhs4' ");
            if (mIsDOWhs4ApprovalBasedOnWhs)
                SQL.AppendLine("And WhsCode = @WhsCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblDOWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='DOWhs4' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", x.DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", x.LocalDocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@TRWhsDocNo", x.TransferRequestWhsDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOWhs2Dtl(DOWhsDtl x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) Values ");
            SQL.AppendLine("(@DocNo, @DNo, 'N', @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", x.ItCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", x.BatchNo);
            Sm.CmParam<String>(ref cm, "@Source", x.Source);
            Sm.CmParam<String>(ref cm, "@Lot", x.Lot);
            Sm.CmParam<String>(ref cm, "@Bin", x.Bin);
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", x.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", x.Qty3);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOWhs2Dtl2(DOWhsDtl2 x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsDtl2(DocNo, DNo, TransferRequestWhsDNo, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @TransferRequestWhsDNo, @Qty, @Qty2, @Qty3, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<Decimal>(ref cm, "@Qty", x.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", x.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", x.Qty3);
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDNo", x.TransferRequestWhsDNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = TxtDocNo.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOWhs4Hdr(DocNo));
            cml.Add(UpdateTransferRequestProjectProcessInd(TxtTransferRequestProjectDocNo.Text));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDOWhs4Hdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdDec(Grd1, Row, 33) > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Data already processed into Receiving Item.");
                    return true;
                }
            return false;
        } 

        private MySqlCommand CancelDOWhs4Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhs4Hdr Set CancelReason=@CancelReason, CancelInd=@CancelInd, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblTransferRequestWhsHdr Set CancelInd=@CancelInd, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select TransferRequestWhsDocNo From TblDOWhs4Dtl Where DocNo=@DocNo); ");

            SQL.AppendLine("Update TblDOWhsHdr Set CancelInd=@CancelInd, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select DOWhsDocNo From TblDOWhs4Dtl Where DocNo=@DocNo); ");

            SQL.AppendLine("Update TblDOWhsDtl Set CancelInd=@CancelInd, ");
            SQL.AppendLine("LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select DOWhsDocNo From TblDOWhs4Dtl Where DocNo=@DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Create Received Warehouse

        private void CreateRecvWhs()
        {
            CheckIsReceived();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataInvalid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            int Index = 0;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 34))
                {
                    Index++;
                    cml.Add(SaveRecvWhs4(i, Index));
                    //cml.Add(SaveRecvWhs4(i));
                    //cml.Add(UpdateDOWhs4Dtl(i));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataInvalid()
        {
            return
                IsDocumentNotApproved() ||
                IsQtyInvalid() ||
                IsQtyInvalid2 ()
                ;
        }

        private bool IsDocumentNotApproved()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDOWhs4Hdr ");
            SQL.AppendLine("Where DocNO = @Param ");
            SQL.AppendLine("And Status != 'A' ");
            SQL.AppendLine("; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document need to be approved.");
                return true;
            }

            return false;
        }

        private bool IsQtyInvalid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 34))
                {
                    if (Sm.GetGrdDec(Grd1, i, 35) < Sm.GetGrdDec(Grd1, i, 33))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Your received quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 33), 0) + ") exceeds outstanding received quantity (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 35), 0) + ").");
                        Sm.FocusGrd(Grd1, i, 33);
                        return true;
                    }
                }
            }

            return false;
        }
        private bool IsQtyInvalid2()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 33) > Sm.GetGrdDec(Grd1, Row, 5))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Received quantity should not be bigger than available stock.");
                    return true;
                }
            }
            return false;
        }
        private MySqlCommand SaveRecvWhs4(int Row, int Index)
        {
            var SQL = new StringBuilder();
            string mDocDt = Sm.Left(Sm.GetValue("Select CurrentDateTime() "), 8);

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Set @RecvWhsDocNo := ");
            SQL.AppendLine(GenerateRecvWhs4DocNo(mDocDt, "RecvWhs4", "TblRecvWhs4Hdr"));
            SQL.AppendLine("; ");

            #region Header

            SQL.AppendLine("Insert Into TblRecvWhs4Hdr(DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RecvWhsDocNo, Left(@Dt, 8), @LocalDocNo, @WhsCode, @WhsCode2, @Remark, @CreateBy, @Dt ");
            SQL.AppendLine("From TblDOWhsDtl ");
            SQL.AppendLine("Where DocNo=@DOWhsDocNo And DNo=@DOWhsDNo; ");

            #endregion

            #region Detail

            SQL.AppendLine("Insert Into TblRecvWhs4Dtl(DocNo, DNo, CancelInd, DOWhsDocNo, DOwhsDNo, BatchNo, Source, Qty, Qty2, Qty3, BatchNoOld, SourceOld, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RecvWhsDocNo, '001', 'N', DocNo, DNo, BatchNo, Source, @Qty, @Qty2, @Qty3, BatchNo, Source, Remark, @CreateBy, @Dt ");
            SQL.AppendLine("From TblDOWhsDtl ");
            SQL.AppendLine("Where DocNo = @DOWhsDocNo And DNo = @DOWhsDNo; ");

            #endregion

            #region Stock

            #region movement batch old

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo And B.BatchNo=B.BatchNoOld; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, C.DocNo, C.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("D.DocDt, A.WhsCode2, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblDOWhsHdr D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo; ");

            #endregion

            #region movement batch new jika batch tidak sama

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo And B.BatchNo<>B.BatchNoOld; ");

            #endregion

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, C.Source, 0, 0, 0, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblStockSummary Where WhsCode = A.WhsCode And Lot=C.Lot And Bin=C.Bin And Source = C.Source); ");

            #region update stock summary

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@CreateBy, LastUpDt=@Dt ");
            SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And Source=@SourceOld; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@CreateBy, LastUpDt=@Dt ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And Source=@SourceOld; ");

            #endregion

            #endregion

            #region Update DOWhs

            if (mIsRecvWhs4ProcessDOWhsPartialEnabled)
            {
                SQL.AppendLine("Update TblDOWhsDtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo, Sum(Qty) As Qty ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A, TblRecvWhs4Dtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblRecvWhs4Dtl ");
                SQL.AppendLine("        Where DocNo=@RecvWhsDocNo  ");
                SQL.AppendLine("        And DOWhsDocNo=B.DOWhsDocNo ");
                SQL.AppendLine("        And DOWhsDNo=B.DOWhsDNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By B.DOWhsDocNo, B.DOWhsDNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    T1.ProcessInd= ");
                SQL.AppendLine("        Case When T1.Qty<=IfNull(T2.Qty, 0) Then 'F' ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When IfNull(T2.Qty, 0)=0.00 Then 'O' Else 'P' End ");
                SQL.AppendLine("        End, ");
                SQL.AppendLine("    T1.LastUpBy=@CreateBy, T1.LastUpDt=@Dt ");
                SQL.AppendLine("Where T1.CancelInd='N'; ");
            }
            else
            {
                SQL.AppendLine("Update TblDOWhsDtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@RecvWhsDocNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
                SQL.AppendLine("Set T1.ProcessInd='F', T1.LastUpBy=@CreateBy, T1.LastUpDt=@Dt ");
                SQL.AppendLine("Where T1.CancelInd='N'; ");
            }

            #endregion

            #region Update DOWhs4

            SQL.AppendLine("Update TblDOWhs4Dtl Set ");
            SQL.AppendLine("    RecvQty=RecvQty+@Qty ");
            SQL.AppendLine("Where DocNo=@DOWhs4DocNo And DNo=@DOWhs4DNo; ");

            #endregion

            #region Journal

            if (mIsRecvWhs4ProcessTo1Journal) //IMS
            {
                if (mIsAutoJournalActived)
                {
                    SQL.AppendLine("Set @JournalDocNo := ");
                    SQL.AppendLine(Sm.GetNewJournalDocNo(mDocDt, Index));
                    SQL.AppendLine("; ");

                    SQL.AppendLine("Update TblRecvWhs4Hdr Set ");
                    SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
                    SQL.AppendLine("Where DocNo=@RecvWhsDocNo ");
                    SQL.AppendLine("And WhsCode In (Select WhsCode From TblWarehouse Where MovingAvgInd='N'); ");

                    SQL.AppendLine("Insert Into TblJournalHdr ");
                    SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
                    SQL.AppendLine("Concat('Receiving Item From Warehouse (DO With Transfer Request) : ', A.DocNo, ', Do To Other Warehouse for Project : ', @DOWhs4DocNo) As JnDesc, ");
                    SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
                    SQL.AppendLine("@CCCodeWhs2, Concat('SO Contract# : ', B.SOContractDocNo), A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblRecvWhs4Hdr A ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.DocNo, ");
                    SQL.AppendLine("    Group_Concat(Distinct T5.SOContractDocNo Separator ', ') SOContractDocNo ");
                    SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
                    SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("    Inner Join TblDOWhsDtl T3 On T2.DOWhsDocno = T3.DocNo And T2.DOWhsDNo = T3.DNo ");
                    SQL.AppendLine("    Inner Join TblDOWhsHdr T4 On T3.DocNo = T4.DocNo ");
                    SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T5 ");
                    SQL.AppendLine("        On T4.TransferRequestWhsDocNo = T5.DocNo ");
                    SQL.AppendLine("        And T4.TransferRequestWhsDocNo Is Not Null ");
                    SQL.AppendLine("        And T5.SOContractDocNo Is Not Null ");
                    SQL.AppendLine("    Where T1.DocNo=@RecvWhsDocNo ");
                    SQL.AppendLine("    Group By T1.DocNo ");
                    SQL.AppendLine(") B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo ");
                    SQL.AppendLine("And A.JournalDocNo Is Not Null ");
                    SQL.AppendLine("And A.JournalDocNo=@JournalDocNo; ");

                    SQL.AppendLine("Set @Row:=0; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, @SOContractDocNo, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");

                    SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
                    SQL.AppendLine("    From ( ");

                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo, ");
                    SQL.AppendLine("    0.00 AS DAmt, (B.Qty*C.UPrice*C.ExcRate) As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'N' ");
                    SQL.AppendLine("    Where A.DocNo=@RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.Parvalue AcNo, ");
                    SQL.AppendLine("    0.00 AS DAmt, (B.Qty*C.UPrice*C.ExcRate) As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
                    SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
                    SQL.AppendLine("    Where A.DocNo=@RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
                    SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo2 As AcNo, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' And E.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo, ");
                    SQL.AppendLine("    0.00 AS DAmt, ");
                    SQL.AppendLine("    B.Qty*IFNULL(F.MovingAvgPrice, 0.00) CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'Y' ");
                    SQL.AppendLine("    INNER JOIN TblItemMovingAvg F On D.ItCode = F.ItCode ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
                    SQL.AppendLine("    0.00 AS DAmt, ");
                    SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
                    SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
                    SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
                    SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
                    SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
                    SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo2 AS AcNo, ");
                    SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
                    SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
                    SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
                    SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' And E.AcNo2 Is Not Null ");
                    SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
                    SQL.AppendLine("    Where A.DocNo = @RecvWhsDocNo ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    Group By T.EntCode, T.AcNo ");
                    SQL.AppendLine(") B On 1=1 ");
                    SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                    SQL.AppendLine("And A.DocNo In (Select JournalDocNo From TblRecvWhs4Hdr Where DocNo=@RecvWhsDocNo And JournalDocNo Is Not Null); ");
                }
            }

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DOWhs4DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DOWhs4DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where param = 'FrmRecvWhs4' Limit 1; "));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetGrdStr(Grd1, Row, 19)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetGrdStr(Grd1, Row, 19)));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@DOWhsDocNo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@DOWhsDNo", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 33));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", GetConversionQty(Sm.GetGrdDec(Grd1, Row, 33), Sm.GetGrdStr(Grd1, Row, 3), "12"));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", GetConversionQty(Sm.GetGrdDec(Grd1, Row, 33), Sm.GetGrdStr(Grd1, Row, 3), "13"));
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDOWhs4Dtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhs4Dtl ");
            SQL.AppendLine("Set RecvQty = RecvQty + @Qty ");
            SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 33));

            return cm;
        }

        //private MySqlCommand SaveRecvWhs4(int Row)
        //{
        //    var SQL = new StringBuilder();
        //    string mDocDt = Sm.Left(Sm.GetValue("Select CurrentDateTime() "), 8);

        //    SQL.AppendLine("Set @RecvWhsDocNo := ");
        //    SQL.AppendLine(GenerateRecvWhs4DocNo(mDocDt, "RecvWhs4", "TblRecvWhs4Hdr"));
        //    SQL.AppendLine("; ");

        //    SQL.AppendLine("Set @JournalDocNo := ");
        //    SQL.AppendLine(Sm.GetNewJournalDocNo(mDocDt, 1));
        //    SQL.AppendLine("; ");

        //    #region Header

        //    SQL.AppendLine("Insert Into TblRecvWhs4Hdr(DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @RecvWhsDocNo ");
        //    SQL.AppendLine(", Left(CurrentDateTime(), 8), @LocalDocNo, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOWhsDtl ");
        //    SQL.AppendLine("Where DocNo = @DOWhsDocNo And DNo = @DOWhsDNo; ");

        //    #endregion

        //    #region Detail

        //    SQL.AppendLine("Insert Into TblRecvWhs4Dtl(DocNo, DNo, CancelInd, DOWhsDocNo, DOwhsDNo, BatchNo, Source, Qty, Qty2, Qty3, BatchNoOld, SourceOld, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @RecvWhsDocNo, '001', 'N', DocNo, DNo, BatchNo, Source, @Qty, @Qty2, @Qty3, BatchNo, Source, Remark, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDOWhsDtl ");
        //    SQL.AppendLine("Where DocNo = @DOWhsDocNo And DNo = @DOWhsDNo; ");

        //    #endregion

        //    #region Stock

        //    #region movement batch old

        //    SQL.AppendLine("Insert Into TblStockMovement ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
        //    SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, C.Source, 'N', '', ");
        //    SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
        //    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblRecvWhs4Hdr A ");
        //    SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
        //    SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo And B.BatchNo=B.BatchNoOld; ");

        //    SQL.AppendLine("Insert Into TblStockMovement ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
        //    SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType2, C.DocNo, C.DNo, C.Source, 'N', '', ");
        //    SQL.AppendLine("D.DocDt, A.WhsCode2, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
        //    SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblRecvWhs4Hdr A ");
        //    SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
        //    SQL.AppendLine("Inner Join TblDOWhsHdr D On C.DocNo=D.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo; ");

        //    #endregion

        //    #region movement batch new jika batch tidak sama

        //    SQL.AppendLine("Insert Into TblStockMovement ");
        //    SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
        //    SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
        //    SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
        //    SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, B.BatchNo, ");
        //    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblRecvWhs4Hdr A ");
        //    SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
        //    SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo And B.BatchNo<>B.BatchNoOld; ");

        //    #endregion

        //    SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, C.Source, 0, 0, 0, Null, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblRecvWhs4Hdr A ");
        //    SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
        //    SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo ");
        //    SQL.AppendLine("And Not Exists(Select 1 From TblStockSummary Where WhsCode = A.WhsCode And Lot=C.Lot And Bin=C.Bin And Source = C.Source); ");

        //    #region update stock summary

        //    SQL.AppendLine("Update TblStockSummary Set ");
        //    SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
        //    SQL.AppendLine("    LastUpBy=@CreateBy, LastUpDt=CurrentDateTime() ");
        //    SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin ");
        //    SQL.AppendLine("And Source=@SourceOld; ");

        //    SQL.AppendLine("Update TblStockSummary Set ");
        //    SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
        //    SQL.AppendLine("    LastUpBy=@CreateBy, LastUpDt=CurrentDateTime() ");
        //    SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
        //    SQL.AppendLine("And Source=@SourceOld; ");

        //    #endregion

        //    #endregion

        //    #region Update DO

        //    if (mIsRecvWhs4ProcessDOWhsPartialEnabled)
        //    {
        //        SQL.AppendLine("Update TblDOWhsDtl T1 ");
        //        SQL.AppendLine("Inner Join ( ");
        //        SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo, Sum(Qty) As Qty ");
        //        SQL.AppendLine("    From TblRecvWhs4Hdr A, TblRecvWhs4Dtl B ");
        //        SQL.AppendLine("    Where A.DocNo=B.DocNo ");
        //        SQL.AppendLine("    And B.CancelInd='N' ");
        //        SQL.AppendLine("    And Exists( ");
        //        SQL.AppendLine("        Select 1 ");
        //        SQL.AppendLine("        From TblRecvWhs4Dtl ");
        //        SQL.AppendLine("        Where DocNo=@RecvWhsDocNo  ");
        //        SQL.AppendLine("        And DOWhsDocNo=B.DOWhsDocNo ");
        //        SQL.AppendLine("        And DOWhsDNo=B.DOWhsDNo ");
        //        SQL.AppendLine("    ) ");
        //        SQL.AppendLine("    Group By B.DOWhsDocNo, B.DOWhsDNo ");
        //        SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
        //        SQL.AppendLine("Set ");
        //        SQL.AppendLine("    T1.ProcessInd= ");
        //        SQL.AppendLine("        Case When T1.Qty<=IfNull(T2.Qty, 0) Then 'F' ");
        //        SQL.AppendLine("        Else ");
        //        SQL.AppendLine("            Case When IfNull(T2.Qty, 0)=0.00 Then 'O' Else 'P' End ");
        //        SQL.AppendLine("        End, ");
        //        SQL.AppendLine("    T1.LastUpBy=@CreateBy, T1.LastUpDt=CurrentDateTime()  ");
        //        SQL.AppendLine("Where T1.CancelInd='N'; ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Update TblDOWhsDtl T1 ");
        //        SQL.AppendLine("Inner Join ( ");
        //        SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo ");
        //        SQL.AppendLine("    From TblRecvWhs4Hdr A ");
        //        SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
        //        SQL.AppendLine("    Where A.DocNo=@RecvWhsDocNo ");
        //        SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
        //        SQL.AppendLine("Set T1.ProcessInd='F', T1.LastUpBy=@CreateBy, T1.LastUpDt=CurrentDateTime()  ");
        //        SQL.AppendLine("Where T1.CancelInd='N'; ");
        //    }

        //    #endregion

        //    #region Journal

        //    if (mIsRecvWhs4ProcessTo1Journal) //IMS
        //    {
        //        if (mIsAutoJournalActived)
        //        {
        //            SQL.AppendLine("Update TblRecvWhs4Hdr Set ");
        //            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
        //            SQL.AppendLine("Where DocNo=@RecvWhsDocNo ");
        //            SQL.AppendLine("And WhsCode In (Select WhsCode From TblWarehouse Where MovingAvgInd='N'); ");

        //            SQL.AppendLine("Insert Into TblJournalHdr ");
        //            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.JournalDocNo, ");
        //            SQL.AppendLine("A.DocDt, ");
        //            SQL.AppendLine("Concat('Receiving Item From Warehouse (DO With Transfer Request) : ', A.DocNo, ', Do To Other Warehouse for Project : ', @DOWhs4DocNo) As JnDesc, ");
        //            SQL.AppendLine("@MenuCode As MenuCode, ");
        //            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
        //            SQL.AppendLine("@CCCodeWhs2, Concat('SO Contract# : ', B.SOContractDocNo), A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("Inner Join ");
        //            SQL.AppendLine("( ");
        //            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T5.SOContractDocNo Separator ', ') SOContractDocNo ");
        //            SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
        //            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo = T2.DocNo And T1.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    Inner Join TblDOWhsDtl T3 On T2.DOWhsDocno = T3.DocNo And T2.DOWhsDNo = T3.DNo ");
        //            SQL.AppendLine("    Inner Join TblDOWhsHdr T4 On T3.DocNo = T4.DocNo ");
        //            SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T5 On T4.TransferRequestWhsDocNo = T5.DocNo ");
        //            SQL.AppendLine("        AND T4.TransferRequestWhsDocNo IS NOT NULL ");
        //            SQL.AppendLine("        AND T5.SOContractDocNo IS NOT NULL ");
        //            SQL.AppendLine("    Group By T1.DocNo ");
        //            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
        //            SQL.AppendLine("Where A.DocNo=@RecvWhsDocNo ");
        //            SQL.AppendLine("And A.JournalDocNo Is Not Null; ");

        //            SQL.AppendLine("Set @Row:=0; ");

        //            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, @SOContractDocNo, A.CreateBy, A.CreateDt ");
        //            SQL.AppendLine("From TblJournalHdr A ");
        //            SQL.AppendLine("Inner Join ( ");

        //            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
        //            SQL.AppendLine("    From ( ");

        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    E.AcNo, 0.00 AS DAmt, ");
        //            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'N' ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    F.Parvalue AcNo, 0.00 AS DAmt, ");
        //            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
        //            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    F.Parvalue AcNo, ");
        //            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
        //            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    E.AcNo2 As AcNo, ");
        //            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' And E.AcNo2 Is Not Null ");
        //            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfRawMaterial' AND F.ParValue IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");

        //            SQL.AppendLine("    UNION ALL ");

        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    E.AcNo, 0.00 AS DAmt, ");
        //            SQL.AppendLine("    B.Qty*IFNULL(F.MovingAvgPrice, 0.00) CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'Y' ");
        //            SQL.AppendLine("    INNER JOIN TblItemMovingAvg F On D.ItCode = F.ItCode ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    F.Parvalue AcNo, 0.00 AS DAmt, ");
        //            SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
        //            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
        //            SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    F.Parvalue AcNo, ");
        //            SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
        //            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
        //            SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
        //            SQL.AppendLine("    UNION ALL ");
        //            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, ");
        //            SQL.AppendLine("    E.AcNo2 AS AcNo, ");
        //            SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
        //            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
        //            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
        //            SQL.AppendLine("        AND A.DocNo = @RecvWhsDocNo ");
        //            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
        //            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
        //            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' And E.AcNo2 Is Not Null ");
        //            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfRawMaterial' AND F.ParValue IS NOT NULL ");
        //            SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
        //            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
        //            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
        //            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
        //            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
        //            //SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");

        //            SQL.AppendLine("    ) T ");
        //            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

        //            SQL.AppendLine(") B On 1=1 ");
        //            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
        //            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs4Hdr Where DocNo=@RecvWhsDocNo And JournalDocNo Is Not Null); ");
        //        }
        //    }

        //    #endregion

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DOWhs4DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
        //    Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where param = 'FrmRecvWhs4' Limit 1; "));
        //    Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
        //    Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetGrdStr(Grd1, Row, 19)));
        //    Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));
        //    Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetGrdStr(Grd1, Row, 19)));
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode2));
        //    Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetGrdStr(Grd1, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@DOWhsDocNo", Sm.GetGrdStr(Grd1, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@DOWhsDNo", Sm.GetGrdStr(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 33));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", GetConversionQty(Sm.GetGrdDec(Grd1, Row, 33), Sm.GetGrdStr(Grd1, Row, 3), "12"));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", GetConversionQty(Sm.GetGrdDec(Grd1, Row, 33), Sm.GetGrdStr(Grd1, Row, 3), "13"));
        //    Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 26));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 29));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 30));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdateDOWhs4Dtl(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblDOWhs4Dtl ");
        //    SQL.AppendLine("Set RecvQty = RecvQty + @Qty ");
        //    SQL.AppendLine("Where DocNo = @DocNo And DNo = @DNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 33));

        //    return cm;
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowDOWhs4Hdr(DocNo);
                ShowDOWhs4Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOWhs4Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TransferRequestProjectDocNo, A.LocalDocNo, A.WhsCode2, A.Remark, ");
            SQL.AppendLine("C.ProjectCode, C.ProjectName, B.SOContractDocNo, B.ItCode, B.PGCode, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("A.CancelReason, A.CancelInd ");
            SQL.AppendLine("From TblDOWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblTransferRequestProjectHdr B On A.TransferRequestProjectDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup C On B.PGCode = C.PGCode; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "TransferRequestProjectDocNo", "LocalDocNo",  "WhsCode2", "Remark",
                    
                    //6-10
                    "ProjectCode", "ProjectName", "SOContractDocNo", "ItCode", "PGCode",

                    //11 - 13
                    "StatusDesc", "CancelReason", "CancelInd"    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtTransferRequestProjectDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[4]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[6]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    SetLueItCode(ref LueItCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueItCode, Sm.DrStr(dr, c[9]));
                    mPGCode = Sm.DrStr(dr, c[10]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[11]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[12]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[13]) == "Y";
                }, true
            );
        }

        private void ShowDOWhs4Dtl(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DNo, C.ItCode, D.ItName, G.Qty StockQty, C.Qty, D.InventoryUOMCode, ");
                SQL.AppendLine("G.Qty2 StockQty2, C.Qty2, D.InventoryUomCode2, G.Qty3 StockQty3, C.Qty3, D.InventoryUomCode3,  ");
                SQL.AppendLine("B.Remark, J.CCtName, K.ItGrpName, D.ItCodeInternal, D.Specification, E.WhsCode, F.WhsName, ");
                SQL.AppendLine("B.TransferRequestWhsDocNo, B.TransferRequestWhsDNo, B.DOWhsDocNo, B.DOWhsDNo, ");
                SQL.AppendLine("B.TransferRequestProjectDNo, C.Source, C.BatchNo, C.PropCode, C.Lot, C.Bin, E.CancelInd, ");
                SQL.AppendLine("B.RecvQty, (C.Qty - IfNull(L.Qty, 0.00)) OutstandingRecvQty, ");
                SQL.AppendLine("Case When (C.Qty - IfNull(L.Qty, 0.00)) <= 0.00 Then 'Y' Else 'N' End As IsReceived ");
                if (mIsDOWhs4HeatNumberEnabled)
                    SQL.AppendLine(", M.Value2 As HeatNumber ");
                else
                    SQL.AppendLine(", Null As HeatNumber ");
                SQL.AppendLine("From TblDOWhs4Hdr A ");
                SQL.AppendLine("Inner Join TblDOWhs4Dtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo = C.DocNo And B.DOWhsDNo = C.DNo ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
                SQL.AppendLine("Inner Join TblTransferRequestWhsHdr E On B.TransferRequestWhsDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblWarehouse F On E.WhsCode = F.WhsCode ");
                SQL.AppendLine("Inner Join TblStockSummary G ");
                SQL.AppendLine("    On C.Source = G.Source ");
                SQL.AppendLine("    And E.WhsCode = G.WhsCode ");
                SQL.AppendLine("    And C.Lot = G.Lot ");
                SQL.AppendLine("    And C.Bin = G.Bin ");
                SQL.AppendLine("Inner Join TblItemCategory H On D.ItCtCode = H.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory I On C.ItCode = I.ItCode And I.CCCode = @CCCode ");
                SQL.AppendLine("Left Join TblCostCategory J On I.CCtCode = J.CCtCode ");
                SQL.AppendLine("Left Join TblItemGroup K On D.ItGrpCode = K.ItGrpCode ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T2.WhsCode2, T2.WhsCode, T1.DOWhsDocNo, T1.DOWhsDNo, Sum(T1.Qty) Qty ");
                SQL.AppendLine("    From TblRecvWhs4Dtl T1 ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Hdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd = 'N' ");
                SQL.AppendLine("    Inner Join TblDOWhs4Dtl T3 On T3.DocNo = @DocNo ");
                SQL.AppendLine("        And T1.DOWhsDocNo = T3.DOWhsDocNo And T1.DOWhsDNo = T3.DOWhsDNo ");
                SQL.AppendLine("    Inner Join TblDOWhsHdr T4 On T4.DocNo = T3.DOWhsDocNo ");
                SQL.AppendLine("        And T4.WhsCode = T2.WhsCode2 And T4.WhsCode2 = T2.WhsCode ");
                SQL.AppendLine("    Group By T2.WhsCode2, T2.WhsCode, T1.DOWhsDocNo, T1.DOWhsDNo ");
                SQL.AppendLine(") L On F.WhsCode = L.WhsCode2 And C.DocNo = L.DOWhsDocNo And C.DNo = L.DOWhsDNo ");
                if (mIsDOWhs4HeatNumberEnabled)
                    SQL.AppendLine("Left Join TblSourceInfo M On C.Source=M.Source ");
                SQL.AppendLine("; ");

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "StockQty", "Qty", "InventoryUOMCode", 
                        
                        //6-10
                        "StockQty2", "Qty2", "InventoryUomCode2", "StockQty3", "Qty3",   
                        
                        //11-15
                        "InventoryUomCode3", "Remark", "CCtName", "ItGrpName", "ItCodeInternal", 

                        //16-20
                        "Specification", "WhsCode", "WhsName", "TransferRequestWhsDocNo", "TransferRequestWhsDNo", 

                        //21-25
                        "DOWhsDocNo", "DOWhsDNo", "TransferRequestProjectDNo", "Source", "BatchNo", 

                        //26-30
                        "PropCode", "Lot", "Bin", "CancelInd", "RecvQty",

                        //31-33
                        "IsReceived", "OutstandingRecvQty", "HeatNumber"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                        Grd.Cells[Row, 31].Value = 0m;
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 32, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                    }, false, false, true, false
                );

                Sm.FocusGrd(Grd1, 0, 1);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12, 31, 33, 35 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private decimal GetConversionQty(decimal Qty1, string ItCode, string ConvertType)
        {
            decimal Convert = Sm.GetValueDec("Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@Param;", ItCode);

            if (Convert != 0m)
            {
                return Convert * Qty1;
            }

            return 0m;
        }

        private string GetCostCenterWarehouse(string WhsCode)
        {
            return Sm.GetValue("Select CCCode From TblWarehouse Where WhsCode=@Param;", WhsCode);
        }

        private string GenerateRecvWhs4DocNo(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            SQL.Append("( ");

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") ");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(")");
            }

            SQL.Append(") ");

            return SQL.ToString();
        }

        private void CheckIsReceived()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mData = string.Empty;
            string mDOWhsDocNo = string.Empty;
            string mDOWhsDNo = string.Empty;
            string mWhsCodeFrom = string.Empty;
            string mWhsCodeTo = string.Empty;

            string DOWhsDocNo = string.Empty, DOWhsDNo = string.Empty, WhsCodeFrom = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                mDOWhsDocNo = Sm.GetGrdStr(Grd1, i, 23);
                mDOWhsDNo = Sm.GetGrdStr(Grd1, i, 24);
                mWhsCodeFrom = Sm.GetGrdStr(Grd1, i, 19);
                mWhsCodeTo = Sm.GetLue(LueWhsCode2);

                if (mData.Length > 0) mData += ",";
                mData += string.Concat(mWhsCodeFrom, mWhsCodeTo, mDOWhsDocNo, mDOWhsDNo);
            }

            if (mData.Length > 0)
            {
                SQL.AppendLine("Select T2.WhsCode, T2.WhsCode2, T1.DocNo DOWhsDocNo, T1.DNo DOWhsDNo, (T1.Qty - IfNull(T3.Qty, 0.00)) Qty ");
                SQL.AppendLine("From TblDOWhsDtl T1 ");
                SQL.AppendLine("Inner Join TblDOWhsHdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    And Find_In_set(Concat(T2.WhsCode, T2.WhsCode2, T1.DocNo, T1.DNo), @Param) ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select B.WhsCode2, B.WhsCode, A.DOWhsDocNo, A.DOWhsDNo, Sum(A.Qty) Qty ");
                SQL.AppendLine("    From TblRecvWhs4Dtl A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Hdr B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine("    And Find_In_set(Concat(B.WhsCode2, B.WhsCode, A.DOWhsDocNo, A.DOWhsDNo), @Param) ");
                SQL.AppendLine("    Group By B.WhsCode2, B.WhsCode, A.DOWhsDocNo, A.DOWhsDNo ");
                SQL.AppendLine(") T3 On T1.DocNo = T3.DOWhsDocNo And T1.DNo = T3.DOWhsDNo And T2.WhsCode = T3.WhsCode2 And T2.WhsCode2 = T3.WhsCode ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "WhsCode", "WhsCode2", "DOWhsDocNo", "DOWhsDNo", "Qty" });
                    if (dr.HasRows)
                    {
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            WhsCodeFrom = Sm.DrStr(dr, c[0]);
                            DOWhsDocNo = Sm.DrStr(dr, c[2]);
                            DOWhsDNo = Sm.DrStr(dr, c[3]);

                            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), WhsCodeFrom) && 
                                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 23), DOWhsDocNo) &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 24), DOWhsDNo)
                                    )
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 35, 4);
                                    break;
                                }
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                }
            }

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdDec(Grd1, i, 35) <= 0)
                {
                    Grd1.Cells[i, 34].Value = true;
                }
            }
        }

        private void Grd1Format()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd1, i, 34))
                {
                    Grd1.Cells[i, 33].ReadOnly = iGBool.True;
                }
            }
        }

        private void ProcessDataTransferRequestWhs()
        {
            var l = new List<TransferReqProject>();
            var l2 = new List<TransferReqProjectHdr>();
            var l3 = new List<TransferReqProjectDtl>();
            var l4 = new List<DOWhsHdr>();
            var l5 = new List<DOWhsDtl>();
            var l6 = new List<DOWhsDtl2>();
            Process1(ref l);
            l.OrderBy(o => o.WhsCode);
            SetDocNo(ref l);
            Process2(ref l, ref l2, ref l3, ref l4, ref l5, ref l6);
            Process3(ref l3, ref l5, ref l6);
            Process4(ref l, ref l2, ref l3, ref l4, ref l5, ref l6);
            Process5(ref l2, ref l3);

            l.Clear(); l2.Clear(); l3.Clear();
            l4.Clear(); l5.Clear(); l6.Clear();
        }

        public string GenerateDocNo(int Index, string DocDt, string DocType, string Tbl)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+ " + Index + ", Char)), 4) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat('000', " + Index + "), 4)) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());

        }

        private void SetDocNo(ref List<TransferReqProject> l)
        {
            string mDNo = string.Empty, mWhsCode =string.Empty;
            int mIndex = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (mWhsCode.Length == 0)
                {
                    mIndex = 1;
                }
                else if (mWhsCode != l[i].WhsCode)
                {
                    mIndex++;
                }

                l[i].Index = mIndex;
                l[i].DocNo = GenerateDocNo(l[i].Index, l[i].DocDt, "TransferRequestWhs", "TblTransferRequestWhsHdr");
                l[i].DOWhsDocNo = GenerateDocNo(l[i].Index, l[i].DocDt, "DOWhs2", "TblDOWhsHdr");
                mWhsCode = l[i].WhsCode;
            }
        }

        private void Process1(ref List<TransferReqProject> l)
        {
            string mWhsCode = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                l.Add(new TransferReqProject()
                {
                    DocDt = Sm.GetDte(DteDocDt),
                    LocalDocNo = TxtLocalDocNo.Text,
                    WhsCode = Sm.GetGrdStr(Grd1, Row, 19),
                    WhsCode2 = Sm.GetLue(LueWhsCode2),
                    PGCode = mPGCode,
                    SOContractDocNo = TxtSOCDocNo.Text,
                    Remark = MeeRemark.Text,
                    ItCode = Sm.GetGrdStr(Grd1, Row, 3),
                    Qty = Sm.GetGrdDec(Grd1, Row, 6),
                    Qty2 = Sm.GetGrdDec(Grd1, Row, 9),
                    Qty3 = Sm.GetGrdDec(Grd1, Row, 12),
                    Remark2 = Sm.GetGrdStr(Grd1, Row, 14),
                    Source = Sm.GetGrdStr(Grd1, Row, 26),
                    BatchNo = Sm.GetGrdStr(Grd1, Row, 27),
                    PropCode = Sm.GetGrdStr(Grd1, Row, 28),
                    Lot = Sm.GetGrdStr(Grd1, Row, 29),
                    Bin = Sm.GetGrdStr(Grd1, Row, 30),
                });
            }
        }

        private void Process2(
            ref List<TransferReqProject> l, 
            ref List<TransferReqProjectHdr> l2, 
            ref List<TransferReqProjectDtl> l3, 
            ref List<DOWhsHdr> l4, 
            ref List<DOWhsDtl> l5, 
            ref List<DOWhsDtl2> l6
            )
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l
                .Select(x => new { x.DocNo, x.DOWhsDocNo, x.DocDt, x.LocalDocNo ,x.WhsCode, x.WhsCode2, x.SOContractDocNo, x.PGCode, x.Remark })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new TransferReqProjectHdr()
                    {
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        LocalDocNo = m.LocalDocNo,
                        WhsCode = m.WhsCode,
                        WhsCode2 = m.WhsCode2,
                        SOContractDocNo = m.SOContractDocNo,
                        PGCode = m.PGCode,
                        Remark = m.Remark,
                    });

                    l4.Add(new DOWhsHdr()
                    {
                        DocNo = m.DOWhsDocNo,
                        DocDt = m.DocDt,
                        LocalDocNo = m.LocalDocNo,
                        WhsCode = m.WhsCode,
                        WhsCode2 = m.WhsCode2,
                        TransferRequestWhsDocNo = m.DocNo,
                        Remark = m.Remark,
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new TransferReqProjectHdr()
                        {
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            LocalDocNo = m.LocalDocNo,
                            WhsCode = m.WhsCode,
                            WhsCode2 = m.WhsCode2,
                            SOContractDocNo = m.SOContractDocNo,
                            PGCode = m.PGCode,
                            Remark = m.Remark,
                        });

                        l4.Add(new DOWhsHdr()
                        {
                            DocNo = m.DOWhsDocNo,
                            DocDt = m.DocDt,
                            LocalDocNo = m.LocalDocNo,
                            WhsCode = m.WhsCode,
                            WhsCode2 = m.WhsCode2,
                            TransferRequestWhsDocNo = m.DocNo,
                            Remark = m.Remark,
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new { x.DocNo, x.DOWhsDocNo, x.DNo, x.ItCode, x.Source, x.BatchNo, x.PropCode, x.Lot, x.Bin, x.Qty, x.Qty2, x.Qty3, x.Remark2 })
               .OrderBy(o => o.DocNo))
                if (n.ItCode.Trim().Length > 0 || n.Remark2.Trim().Length > 0)
                {
                    l3.Add(new TransferReqProjectDtl()
                    {
                        DocNo = n.DocNo,
                        DOWhsDocNo = n.DOWhsDocNo,
                        ItCode = n.ItCode,
                        DNo = n.DNo,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                        Remark2 = n.Remark2,
                    });

                    l5.Add(new DOWhsDtl()
                    {
                        DocNo = n.DOWhsDocNo,
                        DNo = n.DNo,
                        ItCode = n.ItCode,
                        Source = n.Source,
                        BatchNo = n.BatchNo,
                        PropCode = n.PropCode,
                        Lot = n.Lot,
                        Bin = n.Bin,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                        Remark = n.Remark2
                    });

                    l6.Add(new DOWhsDtl2()
                    {
                        DocNo = n.DOWhsDocNo,
                        DNo = n.DNo,
                        TransferRequestWhsDNo = n.DNo,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                    });
                }
        }

        private void Process3(ref List<TransferReqProjectDtl> l3, ref List<DOWhsDtl> l5, ref List<DOWhsDtl2> l6)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < l3.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = l3[i].DocNo;
                    l3[i].DNo = mDNo;
                    l5[i].DNo = mDNo;
                    l6[i].DNo = mDNo;
                    l6[i].TransferRequestWhsDNo = mDNo;
                }
                else
                {
                    if (mDocNo == l3[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = l3[i].DocNo;

                    }

                    l3[i].DNo = mDNo;
                    l5[i].DNo = mDNo;
                    l6[i].DNo = mDNo;
                    l6[i].TransferRequestWhsDNo = mDNo;
                }
            }
        }

        private void Process4(
            ref List<TransferReqProject> l,
            ref List<TransferReqProjectHdr> l2,
            ref List<TransferReqProjectDtl> l3,
            ref List<DOWhsHdr> l4,
            ref List<DOWhsDtl> l5,
            ref List<DOWhsDtl2> l6
            )
        {
            try
            {
                var cml = new List<MySqlCommand>();
                l2.ForEach(i =>
                {
                    cml.Add(SaveTransferRequestWhsHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(SaveTransferRequestWhsDtl(i));
                });

                l4.ForEach(i =>
                {
                    cml.Add(SaveDOWhs2Hdr(i));
                });

                l5.ForEach(i =>
                {
                    cml.Add(SaveDOWhs2Dtl(i));
                });

                l6.ForEach(i =>
                {
                    cml.Add(SaveDOWhs2Dtl2(i));
                });

                Sm.ExecCommands(cml);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process5(ref List<TransferReqProjectHdr> l2, ref List<TransferReqProjectDtl> l3)
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                foreach (var x in l2.Where(p => p.WhsCode == Sm.GetGrdStr(Grd1, i, 19)))
                {
                    foreach (var y in l3.Where(p => p.ItCode == Sm.GetGrdStr(Grd1, i, 3)))
                    {
                        if (x.DocNo == y.DocNo)
                        {
                            Grd1.Cells[i, 21].Value = y.DocNo;
                            Grd1.Cells[i, 22].Value = y.DNo;
                            Grd1.Cells[i, 23].Value = y.DOWhsDocNo;
                            Grd1.Cells[i, 24].Value = y.DNo;
                        }
                    }
                }
            }
        }       

        private void GetParameter()
        {
            //string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            //if (NumberOfInventoryUomCode.Length == 0)
            //    mNumberOfInventoryUomCode = 1;
            //else
            //    mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            //mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            //mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            //mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            //mIsTransferRequestWhsProjectEnabled = Sm.GetParameterBoo("IsTransferRequestWhsProjectEnabled");
            //mIsRecvWhs4ProcessDOWhsPartialEnabled = Sm.GetParameterBoo("IsRecvWhs4ProcessDOWhsPartialEnabled");
            //mIsRecvWhs4ProcessTo1Journal = Sm.GetParameterBoo("IsRecvWhs4ProcessTo1Journal");
            //mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            //mIsDOWhs4ApprovalBasedOnWhs = Sm.GetParameterBoo("IsDOWhs4ApprovalBasedOnWhs");
            //mIsDOWhs4HeatNumberEnabled = Sm.GetParameterBoo("IsDOWhs4HeatNumberEnabled");
            mIsDOWhs4Cancellable = Sm.GetParameterBoo("IsDOWhs4Cancellable");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSystemUseCostCenter', 'IsItGrpCodeShow', 'IsBOMShowSpecifications', 'IsTransferRequestWhsProjectEnabled', 'IsRecvWhs4ProcessDOWhsPartialEnabled', ");
            SQL.AppendLine("'IsRecvWhs4ProcessTo1Journal', 'IsAutoJournalActived', 'IsDOWhs4ApprovalBasedOnWhs', 'IsDOWhs4HeatNumberEnabled', 'NumberOfInventoryUomCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSystemUseCostCenter": mIsSystemUseCostCenter = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsTransferRequestWhsProjectEnabled": mIsTransferRequestWhsProjectEnabled = ParValue == "Y"; break;
                            case "IsRecvWhs4ProcessDOWhsPartialEnabled": mIsRecvWhs4ProcessDOWhsPartialEnabled = ParValue == "Y"; break;
                            case "IsRecvWhs4ProcessTo1Journal": mIsRecvWhs4ProcessTo1Journal = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsDOWhs4ApprovalBasedOnWhs": mIsDOWhs4ApprovalBasedOnWhs = ParValue == "Y"; break;
                            case "IsDOWhs4HeatNumberEnabled": mIsDOWhs4HeatNumberEnabled = ParValue == "Y"; break;

                            //integer
                            case "NumberOfInventoryUomCode": if (ParValue.Length > 0) mNumberOfInventoryUomCode = int.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
        }

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public static void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.CCCode As Col1, T.CCName As Col2 " +
                "From TblCostCenter T " +
                "Where Not Exists(Select Parent From TblCostCenter Where CCCode=T.CCCode And Parent Is Not Null) " +
                "Order By T.CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode As Col1, A.ItName As Col2 ");
            SQL.AppendLine("From TblItem A ");
            if(ItCode.Length > 0)
                SQL.AppendLine("Where A.ItCode=@ItCode ");
            SQL.AppendLine("Order By A.ItName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }
        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue2(SetLueItCode), string.Empty);
        }

        #endregion

        #region Button Clicks

        private void BtnTransferRequestProjectDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmDOWhs4Dlg3(this));
            }
        }

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtSOCDocNo.EditValue = f.mSOCDocNo;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, false);
                SetLueItCode(ref LueItCode, string.Empty);
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtSOCDocNo.EditValue = null;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, true);
                LueItCode.EditValue = null;
            }
            f.Close();
        }

        #endregion

        #endregion

        #region Class

        class TransferReqProject
        {
            public int Index { get; set; }
            public string DocNo { get; set; }
            public string DOWhsDocNo { get; set; }
            public string DocDt { get; set; }
            public string LocalDocNo { get; set; }
            public string WhsCode { get; set; }
            public string WhsCode2 { get; set; }
            public string SOContractDocNo { get; set; }
            public string PGCode { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string Source { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string PropCode { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark2 { get; set; }
        }

        class TransferReqProjectHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string LocalDocNo { get; set; }
            public string WhsCode { get; set; }
            public string WhsCode2 { get; set; }
            public string SOContractDocNo { get; set; }
            public string PGCode { get; set; }
            public string Remark { get; set; }
        }

        class TransferReqProjectDtl
        {
            public string DocNo { get; set; }
            public string DOWhsDocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark2 { get; set; }
        }

        class DOWhsHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string TransferRequestWhsDocNo { get; set; }
            public string LocalDocNo { get; set; }
            public string WhsCode { get; set; }
            public string WhsCode2 { get; set; }
            public string Remark { get; set; }
        }

        class DOWhsDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string PropCode { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark { get; set; }
        }

        class DOWhsDtl2
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string TransferRequestWhsDNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
        }

        #endregion

        #region Report Class

        class DOWhs2Hdr4
        {
            public string DocNo { get; set; }
            public string CompanyLogo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string TransferRequestWhsDocNo { get; set; }
            public string UserName { get; set; }
            public string CCName { get; set; }
            public string PrintDt { get; set; }
        }

        class DOWhs2Dtl2
        {
            public decimal Number { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public string ProjectName { get; set; }
            public string ProjectName2 { get; set; }
            public string Remark { get; set; }
            public string WhsName { get; set; }
        }

        #endregion

    }
}
