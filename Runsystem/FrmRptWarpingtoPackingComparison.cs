﻿#region Update
/* 
    22/05/2018 [TKG] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptWarpingtoPackingComparison : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptWarpingtoPackingComparison(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Type, DocName, PPDocNo, DocNo, DocDt, ItCode, ItName, BatchNo, ");
            SQL.AppendLine("Qty, Uom1, Qty2, Uom2, Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As Type, C.DocName, A.PPDocNo, A.DocNo, A.DocDt, D.ItCode, D.ItName, B.BatchNo, ");
            SQL.AppendLine("    B.Qty, D.InventoryUomCode As Uom1, 0.00 As Qty2, Null As Uom2, A.Remark ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblWorkCenterHdr C On A.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Find_In_Set(");
            SQL.AppendLine("    A.WorkCenterDocNo, ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='RptWarpingtoPackingComparisonWC1'), '') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As Type, C.DocName, A.PPDocNo, A.DocNo, A.DocDt, D.ItCode, D.ItName, B.BatchNo, ");
            SQL.AppendLine("    0.00 As Qty, Null As Uom1, B.Qty As Qty2, D.PlanningUomCode As Uom2, A.Remark ");
            SQL.AppendLine("    From TblShopFloorControlHdr A ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblWorkCenterHdr C On A.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And Find_In_Set(");
            SQL.AppendLine("    A.WorkCenterDocNo, ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='RptWarpingtoPackingComparisonWC2'), '') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Work Center",
                        "Planned#",
                        "SFC#", 
                        "Date",
                        "Item's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Item's Name",
                        "Batch#", 
                        "Material"+Environment.NewLine+"Consumption"+Environment.NewLine+"(BOM)",
                        "UoM",
                        "Result"+Environment.NewLine+"(SFC)",

                        //11-12
                        "UoM",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        170, 130, 130, 80, 80, 
                        
                        //6-10
                        200, 200, 80, 80, 80,

                        //11-12
                        80, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtShopFloorControlDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPPDocNo.Text, "T.PPDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocDt Desc, T.Type, T.DocNo, T.ItName, T.BatchNo;",
                        new string[]
                        {
                            //0
                            "DocName", 
                            
                            //1-5
                            "PPDocNo", "DocNo", "DocDt", "ItCode", "ItName", 
                            
                            //6-10
                            "BatchNo", "Qty", "Uom1", "Qty2", "Uom2", 
                            
                            //11
                            "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 8, 10 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true); 
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtShopFloorControlDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkShopFloorControlDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SFC#");
        }

        private void TxtPPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planned#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
