﻿namespace RunSystem
{
    partial class FrmRptIMMStockSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.ChkSource = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.ChkBin = new DevExpress.XtraEditors.CheckEdit();
            this.LueBin = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkWhsCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkLocCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueLocCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtProdCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkProdCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtProdCode);
            this.panel2.Controls.Add(this.ChkProdCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkLocCode);
            this.panel2.Controls.Add(this.LueLocCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtSource);
            this.panel2.Controls.Add(this.ChkSource);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ChkBin);
            this.panel2.Controls.Add(this.LueBin);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ChkWhsCode);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Size = new System.Drawing.Size(772, 119);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 354);
            this.Grd1.TabIndex = 23;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 119);
            this.panel3.Size = new System.Drawing.Size(772, 354);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "Source";
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(84, 68);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Properties.MaxLength = 250;
            this.TxtSource.Size = new System.Drawing.Size(228, 20);
            this.TxtSource.TabIndex = 18;
            this.TxtSource.Validated += new System.EventHandler(this.TxtSource_Validated);
            // 
            // ChkSource
            // 
            this.ChkSource.Location = new System.Drawing.Point(318, 66);
            this.ChkSource.Name = "ChkSource";
            this.ChkSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSource.Properties.Appearance.Options.UseFont = true;
            this.ChkSource.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSource.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSource.Properties.Caption = " ";
            this.ChkSource.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSource.Size = new System.Drawing.Size(19, 22);
            this.ChkSource.TabIndex = 19;
            this.ChkSource.ToolTip = "Remove filter";
            this.ChkSource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSource.ToolTipTitle = "Run System";
            this.ChkSource.CheckedChanged += new System.EventHandler(this.ChkSource_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(57, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Bin";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkBin
            // 
            this.ChkBin.Location = new System.Drawing.Point(318, 46);
            this.ChkBin.Name = "ChkBin";
            this.ChkBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBin.Properties.Appearance.Options.UseFont = true;
            this.ChkBin.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBin.Properties.Caption = " ";
            this.ChkBin.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBin.Size = new System.Drawing.Size(19, 22);
            this.ChkBin.TabIndex = 16;
            this.ChkBin.ToolTip = "Remove filter";
            this.ChkBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBin.ToolTipTitle = "Run System";
            this.ChkBin.CheckedChanged += new System.EventHandler(this.ChkBin_CheckedChanged);
            // 
            // LueBin
            // 
            this.LueBin.EnterMoveNextControl = true;
            this.LueBin.Location = new System.Drawing.Point(84, 47);
            this.LueBin.Margin = new System.Windows.Forms.Padding(5);
            this.LueBin.Name = "LueBin";
            this.LueBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.Appearance.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBin.Properties.DropDownRows = 30;
            this.LueBin.Properties.NullText = "[Empty]";
            this.LueBin.Properties.PopupWidth = 250;
            this.LueBin.Size = new System.Drawing.Size(228, 20);
            this.LueBin.TabIndex = 15;
            this.LueBin.ToolTip = "F4 : Show/hide list";
            this.LueBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBin.EditValueChanged += new System.EventHandler(this.LueBin_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(11, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Warehouse";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkWhsCode
            // 
            this.ChkWhsCode.Location = new System.Drawing.Point(318, 3);
            this.ChkWhsCode.Name = "ChkWhsCode";
            this.ChkWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWhsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkWhsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWhsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWhsCode.Properties.Caption = " ";
            this.ChkWhsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWhsCode.Size = new System.Drawing.Size(19, 22);
            this.ChkWhsCode.TabIndex = 10;
            this.ChkWhsCode.ToolTip = "Remove filter";
            this.ChkWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWhsCode.ToolTipTitle = "Run System";
            this.ChkWhsCode.CheckedChanged += new System.EventHandler(this.ChkWhsCode_CheckedChanged);
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(84, 5);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 250;
            this.LueWhsCode.Size = new System.Drawing.Size(228, 20);
            this.LueWhsCode.TabIndex = 9;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(27, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 14);
            this.label3.TabIndex = 11;
            this.label3.Text = "Location";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkLocCode
            // 
            this.ChkLocCode.Location = new System.Drawing.Point(318, 24);
            this.ChkLocCode.Name = "ChkLocCode";
            this.ChkLocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLocCode.Properties.Appearance.Options.UseFont = true;
            this.ChkLocCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLocCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLocCode.Properties.Caption = " ";
            this.ChkLocCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLocCode.Size = new System.Drawing.Size(19, 22);
            this.ChkLocCode.TabIndex = 13;
            this.ChkLocCode.ToolTip = "Remove filter";
            this.ChkLocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLocCode.ToolTipTitle = "Run System";
            this.ChkLocCode.CheckedChanged += new System.EventHandler(this.ChkLocCode_CheckedChanged);
            // 
            // LueLocCode
            // 
            this.LueLocCode.EnterMoveNextControl = true;
            this.LueLocCode.Location = new System.Drawing.Point(84, 26);
            this.LueLocCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLocCode.Name = "LueLocCode";
            this.LueLocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.Appearance.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLocCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLocCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLocCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLocCode.Properties.DropDownRows = 30;
            this.LueLocCode.Properties.NullText = "[Empty]";
            this.LueLocCode.Properties.PopupWidth = 250;
            this.LueLocCode.Size = new System.Drawing.Size(228, 20);
            this.LueLocCode.TabIndex = 12;
            this.LueLocCode.ToolTip = "F4 : Show/hide list";
            this.LueLocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLocCode.EditValueChanged += new System.EventHandler(this.LueLocCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 14);
            this.label4.TabIndex = 20;
            this.label4.Text = "Product";
            // 
            // TxtProdCode
            // 
            this.TxtProdCode.EnterMoveNextControl = true;
            this.TxtProdCode.Location = new System.Drawing.Point(84, 89);
            this.TxtProdCode.Name = "TxtProdCode";
            this.TxtProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProdCode.Properties.MaxLength = 250;
            this.TxtProdCode.Size = new System.Drawing.Size(228, 20);
            this.TxtProdCode.TabIndex = 21;
            this.TxtProdCode.Validated += new System.EventHandler(this.TxtProdCode_Validated);
            // 
            // ChkProdCode
            // 
            this.ChkProdCode.Location = new System.Drawing.Point(318, 87);
            this.ChkProdCode.Name = "ChkProdCode";
            this.ChkProdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProdCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProdCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProdCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProdCode.Properties.Caption = " ";
            this.ChkProdCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProdCode.Size = new System.Drawing.Size(19, 22);
            this.ChkProdCode.TabIndex = 22;
            this.ChkProdCode.ToolTip = "Remove filter";
            this.ChkProdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProdCode.ToolTipTitle = "Run System";
            this.ChkProdCode.CheckedChanged += new System.EventHandler(this.ChkProdCode_CheckedChanged);
            // 
            // FrmRptIMMStockSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptIMMStockSummary";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProdCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkLocCode;
        private DevExpress.XtraEditors.LookUpEdit LueLocCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtSource;
        private DevExpress.XtraEditors.CheckEdit ChkSource;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.CheckEdit ChkBin;
        private DevExpress.XtraEditors.LookUpEdit LueBin;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkWhsCode;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtProdCode;
        private DevExpress.XtraEditors.CheckEdit ChkProdCode;
    }
}