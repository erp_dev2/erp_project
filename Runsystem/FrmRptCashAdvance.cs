﻿#region Update
/*
    14/04/2020 [IBL] : New reporting
    21/04/2020 [TKG/KBN] : filter site
    18/09/2020 [DITA/YK] : Bug data CAS tidak muncul karena kena validasi entcode ga boleh kosong
    15/12/2020 [TKG/IMS] Bug saat show data di reporting open cash advance.
    22/01/2021 [DITA/IMS] Tambah kolom PIC dan Department
    30/04/2021 [WED/IMS] tambah tipe Voucher Cash Advance Travel Request
    10/03/2023 [WED/KBN] tambah informasi due date berdasarkan parameter IsVoucherRequestCASUseDueDate
    21/03/2023 [MYA/MNET] Penyesuaian reporting Open Cash Advance : perubahan tata letak , menambah voucher date , dan menambah informasi / warning berupa validasi perbedaan warna ketika terdapat keterlambatan CAS
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashAdvance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mBankAccountFormat = "0";
        private bool 
            mIsVRBudgetUseCASBA = false, 
            mIsVoucherRequestCASUseDueDate = false, 
            mIsCASUseVCDroppingPaymentType = false,
            mIsRptOpenCashAdvanceUseVoucherDate = false;

        #endregion

        #region Constructor

        public FrmRptCashAdvance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                LblExcludeUnsettledCA.Visible = ChkExcludeUnsettledCA.Visible = mIsVoucherRequestCASUseDueDate;
                LblVRType.Visible = LueVRType.Visible = ChkVRType.Visible = LblDroppingPayment.Visible = 
                    TxtDroppingPaymentDocNo.Visible = ChkDroppingPayment.Visible = mIsCASUseVCDroppingPaymentType;
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetLueVRType(ref LueVRType);
                SetLueEntCode(ref LueEntCode);
                SetLueSiteCode(ref LueSiteCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, T2.SiteName, T3.EntName, T4.UserName PICName, T5.DeptName, T6.OptDesc As CashAdvanceTypeDesc, ");
            if (mIsVoucherRequestCASUseDueDate) SQL.AppendLine("If(T6.Property1 Is Null, Null, If(T.VCDocDt Is Null, Null, Date_Format(Date_Add(T.VCDocDt, interval T6.Property1 Day), '%Y%m%d'))) As CASDueDt, IfNull(T6.Property1, 0.00) As CASAging ");
            else SQL.AppendLine("Null As CASDueDt, 0.00 As CASAging ");
            SQL.AppendLine(", T7.OptDesc AS VCType, T8.DocNo AS DPDocNo ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    SELECT T1.DocDt DocDtVR, T1.DocNo DocNoVR, T1.LocalDocNo LclDocNoVR, IFNULL(T1.Amt, 0) AmtVR, T1.OptDesc PaymentTypeVR, T1.DueDt DueDtVR, T1.Remark RemarkVR, T1.DeptCode, T2.EntCode, T2.SiteCode, ");
            SQL.AppendLine("    T2.DocNo DocNoVC, T2.VCDocDt, T2.LocalDocNo LclDocNoVC, IFNULL(T2.Amt, 0) AmtVC, T2.OptDesc PaymentTypeVC, T2.BankAcNm BankAcVC, T2.Remark RemarkVC, ");
            SQL.AppendLine("    T3.DocNo DocNoCAS, IFNULL(T3.Amt1, 0) Amt1CAS1, IFNULL(T3.Amt2, 0) Amt1CAS2, IFNULL(T3.Amt3, 0) Amt1CAS3, T3.Remark RemarkCAS, T3.BankAcNm BankAcCAS, T1.PIC, ");
            SQL.AppendLine("    T1.CashAdvanceTypeCode, T1.VRDocType ");
            SQL.AppendLine("    FROM ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT A.DocNo, A.DocDt, A.LocalDocNo, A.Amt, C.OptDesc, A.DueDt, A.DeptCode, ");
            SQL.AppendLine("    	A.Remark, A.DocType, A.VoucherDocNo, A.PIC, A.DocType VRDocType, ");
            if (mIsVoucherRequestCASUseDueDate) SQL.AppendLine("    	A.CashAdvanceTypeCode ");
            else SQL.AppendLine("    	Null As CashAdvanceTypeCode ");
            SQL.AppendLine("        FROM TblVoucherRequestHdr A ");
            SQL.AppendLine("        LEFT JOIN tbloption C ON A.PaymentType = C.OptCode ");
            SQL.AppendLine("            And C.OptCat = 'VoucherPaymentType' ");
            SQL.AppendLine("        Where A.doctype In ('56', '61', '58' ");
            if (mIsVRBudgetUseCASBA)
                SQL.AppendLine("        , '70' ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And A.CancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("        AND A.docdt BETWEEN @docdt1 AND @docdt2 ");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT A.DocNo, A.LocalDocNo, A.Amt, C.OptDesc, B.BankAcNm, A.Remark, B.SiteCode, B.EntCode, A.DocDt VCDocDt ");
            SQL.AppendLine("        FROM tblvoucherhdr A ");
            SQL.AppendLine("        LEFT JOIN ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select X.BankAcCode, X.SiteCode, X.EntCode, ");
                SQL.AppendLine("        TRIM(CONCAT( ");
                SQL.AppendLine("        Case When Y.BankName Is Not Null Then CONCAT(Y.BankName, ' ') Else '' End, ");
                SQL.AppendLine("        Case When X.BankAcNo Is Not Null ");
                SQL.AppendLine("            Then Concat(X.BankAcNo) ");
                SQL.AppendLine("            Else IfNull(X.BankAcNm, '') End, ");
                SQL.AppendLine("        Case When X.Remark Is Not Null Then Concat(' ', '(', X.Remark, ')') Else '' End ");
                SQL.AppendLine("        )) AS BankAcNm ");
                SQL.AppendLine("        From TblBankAccount X ");
                SQL.AppendLine("        Left JOIN TblBank Y On X.BankCode=Y.BankCode ");
                SQL.AppendLine("        Where (X.SiteCode Is Null Or ");
                SQL.AppendLine("        (X.SiteCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupSite ");
                SQL.AppendLine("            Where SiteCode=IfNull(X.SiteCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ))) ");
                SQL.AppendLine("        And (X.EntCode Is Null Or ");
                SQL.AppendLine("        (X.EntCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupEntity ");
                SQL.AppendLine("            Where EntCode=IfNull(X.EntCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ))) ");
                SQL.AppendLine("    )B ON A.BankAcCode = B.BankAcCode ");
            }
            else
            {
                SQL.AppendLine("    ( ");
	            SQL.AppendLine("        Select X.BankAcCode, X.SiteCode, X.EntCode, ");
	            SQL.AppendLine("        Trim(CONCAT( ");
	            SQL.AppendLine("        Case When X.BankAcNo Is Not Null  ");
	            SQL.AppendLine("            Then Concat(X.BankAcNo, ' [', IfNull(X.BankAcNm, ''), ']') ");
	            SQL.AppendLine("            Else IfNull(X.BankAcNm, '') End, ");
	            SQL.AppendLine("        Case When Y.BankName Is Not Null Then Concat(' ', Y.BankName) Else '' End ");
	            SQL.AppendLine("        )) As BankAcNm ");
	            SQL.AppendLine("        From TblBankAccount X ");
	            SQL.AppendLine("        Left Join TblBank Y On X.BankCode=Y.BankCode ");
                SQL.AppendLine("        Where (X.SiteCode Is Null Or ");
                SQL.AppendLine("        (X.SiteCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupSite ");
                SQL.AppendLine("            Where SiteCode=IfNull(X.SiteCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ))) ");
                SQL.AppendLine("        And (X.EntCode Is Null Or ");
                SQL.AppendLine("        (X.EntCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupEntity ");
                SQL.AppendLine("            Where EntCode=IfNull(X.EntCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ))) ");
                SQL.AppendLine("    )B ON A.BankAcCode = B.BankAcCode ");
            }
            SQL.AppendLine("        LEFT JOIN tbloption C ON A.PaymentType = C.OptCode ");
            SQL.AppendLine("            And C.OptCat = 'VoucherPaymentType' ");
            SQL.AppendLine("        Where A.CancelInd = 'N' ");
            SQL.AppendLine("    )T2 ON T1.VoucherDocNo = T2.DocNo ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT DISTINCT(CONCAT(A.DocNo, C.VoucherRequestDocNo)), A.DocNo, C.VoucherRequestDocNo, A.Amt1, A.Amt2, A.Amt3, A.Remark, E.BankAcNm ");
            SQL.AppendLine("        FROM tblcashadvancesettlementhdr A ");
            SQL.AppendLine("        INNER JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            SQL.AppendLine("        INNER JOIN tblvoucherhdr C ON B.VoucherDocNo = C.DocNo ");
            SQL.AppendLine("        INNER JOIN tblvoucherrequesthdr D ON C.DocNo = D.VoucherDocNo ");
            SQL.AppendLine("        LEFT JOIN ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select X.BankAcCode, ");
                SQL.AppendLine("        TRIM(CONCAT( ");
                SQL.AppendLine("        Case When Y.BankName Is Not Null Then CONCAT(Y.BankName, ' ') Else '' End, ");
                SQL.AppendLine("        Case When X.BankAcNo Is Not Null ");
                SQL.AppendLine("          Then Concat(X.BankAcNo) ");
                SQL.AppendLine("          Else IfNull(X.BankAcNm, '') End, ");
                SQL.AppendLine("        Case When X.Remark Is Not Null Then Concat(' ', '(', X.Remark, ')') Else '' End ");
                SQL.AppendLine("        )) AS BankAcNm ");
                SQL.AppendLine("        From TblBankAccount X ");
                SQL.AppendLine("        Left JOIN TblBank Y On X.BankCode=Y.BankCode ");
                SQL.AppendLine("    )E ON A.BankAcCode = E.BankAcCode ");
            }
            else
            {
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select X.BankAcCode, ");
                SQL.AppendLine("        Trim(CONCAT( ");
                SQL.AppendLine("        Case When X.BankAcNo Is Not Null  ");
                SQL.AppendLine("            Then Concat(X.BankAcNo, ' [', IfNull(X.BankAcNm, ''), ']') ");
                SQL.AppendLine("            Else IfNull(X.BankAcNm, '') End, ");
                SQL.AppendLine("        Case When Y.BankName Is Not Null Then Concat(' ', Y.BankName) Else '' End ");
                SQL.AppendLine("        )) As BankAcNm ");
                SQL.AppendLine("        From TblBankAccount X ");
                SQL.AppendLine("        Left Join TblBank Y On X.BankCode=Y.BankCode ");
                SQL.AppendLine("    )E ON A.BankAcCode = E.BankAcCode ");
            }
            SQL.AppendLine("    )T3 ON T1.DocNo = T3.VoucherRequestDocNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblSite T2 On T.SiteCode=T2.SiteCode ");
            SQL.AppendLine("Left Join TblEntity T3 On  T.EntCode=T3.EntCode ");
            SQL.AppendLine("Left Join TblUser T4 On T.PIC=T4.UserCode ");
            SQL.AppendLine("Left Join TblDepartment T5 On T.DeptCode=T5.DeptCode ");
            SQL.AppendLine("Left Join TblOption T6 On T6.OptCat = 'VoucherRequestCASDueDate' And T.CashAdvanceTypeCode = T6.OptCode ");
            SQL.AppendLine("Left Join tbloption T7 ON T7.OptCat = 'VoucherDocType' AND T.VRDocType = T7.OptCode  ");
            SQL.AppendLine("LEFT JOIN tbldroppingpaymenthdr T8 ON T.DocNoVR = T8.VoucherRequestDocNo ");
            SQL.AppendLine("Where (T2.SiteCode Is Null Or ");
            SQL.AppendLine("(T2.SiteCode Is Not Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupSite ");
            SQL.AppendLine("        Where SiteCode=IfNull(T2.SiteCode, '') ");
            SQL.AppendLine("        And GrpCode In ( ");
            SQL.AppendLine("            Select GrpCode From TblUser ");
            SQL.AppendLine("            Where UserCode=@UserCode ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    ))) ");
            SQL.AppendLine("And (T.EntCode Is Null Or ");
            SQL.AppendLine("(T.EntCode Is Not Null ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupEntity ");
            SQL.AppendLine("        Where EntCode=IfNull(T.EntCode, '') ");
            SQL.AppendLine("        And GrpCode In ( ");
            SQL.AppendLine("            Select GrpCode From TblUser ");
            SQL.AppendLine("            Where UserCode=@UserCode ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    ))) ");            

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new String[]
                {
                    //0
                    "No",

                    //1-5
                    "Date",
                    "",
                    "Voucher Request#",
                    "Local VR#",
                    "VR's Amount",

                    //6-10
                    "Payment Type",
                    "Due Date (VR)",
                    "VR's Remark",
                    "",
                    "Voucher#",

                    //11-15
                    "Local VC#",
                    "Amount VC",
                    "Payment Type",
                    "Bank Account",
                    "Voucher's Remark",

                    //16-20
                    "",
                    "Cash Advance" +Environment.NewLine+"Settlement#",
                    "Total Cost",
                    "Total Cash Advance",
                    "Total Details",

                    //21-25
                    "CAS's Remark",
                    "Bank Account",
                    "Site",
                    "Entity",
                    "PIC",

                    //26-30
                    "Department",
                    "Voucher Date",
                    "Cash Advance Type",
                    "Due Date (CAS)",
                    "Aging Cash Advance",

                    //31-32
                    "VR's Type",
                    "Dropping Payment#"
                },
                new int[] 
                {
                    //0
                    25,

                    //1-5
                    100, 20, 150, 130, 150,

                    //6-10
                    100, 100, 200, 20, 170,

                    //11-15
                    100, 150, 100, 250, 250,

                    //16-20
                    20, 170, 150, 150, 150,

                    //21-25
                    250, 250, 200, 200, 180,

                    //26-30
                    200, 100, 100, 100, 100,

                    //31-32
                    150, 150
                }
            );

            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 9, 13, 14, 16, 22 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5, 12, 18, 19, 20, 30 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1, 7, 27, 29 });
            Sm.GrdColButton(Grd1, new int[] { 2, 9, 16 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 });
            //Grd1.Cols[25].Move(2);
            //Grd1.Cols[26].Move(3);

            if (mIsVoucherRequestCASUseDueDate)
                Grd1.Cols[27].Move(18);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29, 30 });

            if (mIsCASUseVCDroppingPaymentType)
            {
                Grd1.Cols[31].Move(4);
                Grd1.Cols[32].Move(5);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 31, 32 });

            if (mIsRptOpenCashAdvanceUseVoucherDate)
                Grd1.Cols[27].Move(13);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 9, 13, 14, 16, 22 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "T.EntCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNoVR.Text, new string[] { "T.DocNoVR" });
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNoVR.Text, new string[] { "T.LclDocNoVR" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNoCAS.Text, new string[] { "T.DocNoCAS" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNoVC.Text, new string[] { "T.DocNoVC" });

                if (ChkExcludeUnsettledCA.Checked)
                {
                    Filter += " And (T.DocNoCAS Is Null Or (T.DocNoCAS Is Not Null And Length(Trim(T.DocNoCAS)) = 0) )";
                }
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By T.DocDtVR;",
                        new string[]
                        { 
                            //0
                            "DocDtVR", 

                            //1-5
                            "DocNoVR", "LclDocNoVR", "AmtVR", "PaymentTypeVR", "DueDtVR",
                            
                            //6-10
                            "RemarkVR", "DocNoVC", "LclDocNoVC", "AmtVC", "PaymentTypeVC",
                            
                            //11-15
                            "BankAcVC", "RemarkVC", "DocNoCAS", "Amt1CAS1", "Amt1CAS2",
                            
                            //16-20
                            "Amt1CAS3", "RemarkCAS", "BankAcCAS", "SiteName", "EntName",

                            //21-25
                            "PICName", "DeptName", "VCDocDt", "CashAdvanceTypeDesc", "CASDueDt", 
                            
                            //26-28
                            "CASAging", "VCType", "DPDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                            if (mIsVoucherRequestCASUseDueDate)
                            {
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 23);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 25);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 26);

                                if (mIsRptOpenCashAdvanceUseVoucherDate)
                                {
                                    string Param = Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'DueDateforCashAdvance'");

                                    if (Sm.DrStr(dr, c[23]).Length > 0 && Param.Length > 0)
                                    {
                                        decimal Date1 = Decimal.Parse(Sm.DrStr(dr, c[23]));
                                        decimal Date2 = Convert.ToInt32(Sm.GetValue("Select Date_Format(CURDATE(),'%Y%m%d')"));
                                        decimal Diff = Convert.ToInt32(Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'DueDateforCashAdvance'"));

                                        if ((Date2 - Date1) > Diff && Sm.DrStr(dr, c[13]).Length == 0)
                                        {
                                            Grd.Rows[Row].BackColor = Color.Red;
                                        }
                                    }
                                }
                                else
                                {
                                    if (Sm.DrStr(dr, c[25]).Length > 0)
                                    {
                                        decimal CurDt = Decimal.Parse(Sm.ServerCurrentDate());
                                        decimal CASDueDt = Decimal.Parse(Sm.DrStr(dr, c[25]));


                                        if (CurDt > CASDueDt && Sm.DrStr(dr, c[13]).Length == 0)
                                        {
                                            Grd.Rows[Row].BackColor = Color.Red;
                                        }

                                    }
                                }
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 12, 18, 19, 20 });
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mIsVRBudgetUseCASBA = Sm.GetParameterBoo("IsVRBudgetUseCASBA");
            mIsVoucherRequestCASUseDueDate = Sm.GetParameterBoo("IsVoucherRequestCASUseDueDate");
            mIsCASUseVCDroppingPaymentType = Sm.GetParameterBoo("IsCASUseVCDroppingPaymentType");
            mIsRptOpenCashAdvanceUseVoucherDate = Sm.GetParameterBoo("IsRptOpenCashAdvanceUseVoucherDate");

        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
            Grd1.EndUpdate();
        }

        private void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupSite ");
            SQL.AppendLine("    Where SiteCode=T.SiteCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By T.SiteName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupEntity ");
            SQL.AppendLine("    Where EntCode=T.EntCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By T.EntName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueVRType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 From TblOption T ");
            SQL.AppendLine("Where T.OptCat='VoucherDocType' And OptCode In ('55', '56') ");
            SQL.AppendLine("Order By T.OptCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNoVR_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "VR (Cash Advance)#");
        }

        private void TxtDocNoVR_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNoVR_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local VR#");
        }

        private void TxtLocalDocNoVR_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTxtDocNoCAS_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cash Advance Settlement#");
        }

        private void TxtTxtDocNoCAS_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTxtDocNoVC_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }

        private void TxtTxtDocNoVC_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #region Grid Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmCashAdvanceSettlement(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

    }
}
