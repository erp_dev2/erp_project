﻿namespace RunSystem
{
    partial class FrmSOQuotPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgAgent = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgItem = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtUom = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LueReason4 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCashBackAmt = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueReason3 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCashBack2 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtCashBack1 = new DevExpress.XtraEditors.TextEdit();
            this.LueReason2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtTargetOmset = new DevExpress.XtraEditors.TextEdit();
            this.LueReason1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueCostumerCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDtEnd = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDtStart = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgAgent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBackAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBack2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBack1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTargetOmset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostumerCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(887, 0);
            this.panel1.Size = new System.Drawing.Size(70, 396);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(887, 396);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgAgent);
            this.tabControl1.Controls.Add(this.TpgItem);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 186);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(887, 210);
            this.tabControl1.TabIndex = 22;
            // 
            // TpgAgent
            // 
            this.TpgAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgAgent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgAgent.Controls.Add(this.Grd1);
            this.TpgAgent.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgAgent.Location = new System.Drawing.Point(4, 26);
            this.TpgAgent.Name = "TpgAgent";
            this.TpgAgent.Size = new System.Drawing.Size(879, 180);
            this.TpgAgent.TabIndex = 0;
            this.TpgAgent.Text = "Agent";
            this.TpgAgent.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(875, 176);
            this.Grd1.TabIndex = 40;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // TpgItem
            // 
            this.TpgItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgItem.Controls.Add(this.Grd2);
            this.TpgItem.Location = new System.Drawing.Point(4, 26);
            this.TpgItem.Name = "TpgItem";
            this.TpgItem.Size = new System.Drawing.Size(879, 180);
            this.TpgItem.TabIndex = 1;
            this.TpgItem.Text = "Item";
            this.TpgItem.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(875, 176);
            this.Grd2.TabIndex = 41;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.TxtUom);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.LueReason4);
            this.panel3.Controls.Add(this.TxtCashBackAmt);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.LueReason3);
            this.panel3.Controls.Add(this.TxtCashBack2);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.TxtCashBack1);
            this.panel3.Controls.Add(this.LueReason2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.TxtTargetOmset);
            this.panel3.Controls.Add(this.LueReason1);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.LueCostumerCode);
            this.panel3.Controls.Add(this.DteDtEnd);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.DteDtStart);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.ChkActiveInd);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(887, 186);
            this.panel3.TabIndex = 21;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(204, 93);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 12;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(135, 20);
            this.LueCurCode.TabIndex = 20;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // TxtUom
            // 
            this.TxtUom.EnterMoveNextControl = true;
            this.TxtUom.Location = new System.Drawing.Point(451, 134);
            this.TxtUom.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUom.Name = "TxtUoM";
            this.TxtUom.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUom.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUom.Properties.Appearance.Options.UseFont = true;
            this.TxtUom.Properties.MaxLength = 16;
            this.TxtUom.Size = new System.Drawing.Size(50, 20);
            this.TxtUom.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(296, 137);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 14);
            this.label14.TabIndex = 25;
            this.label14.Text = "X";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(520, 98);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 14);
            this.label13.TabIndex = 38;
            this.label13.Text = "Reason (4)";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReason4
            // 
            this.LueReason4.EnterMoveNextControl = true;
            this.LueReason4.Location = new System.Drawing.Point(591, 95);
            this.LueReason4.Margin = new System.Windows.Forms.Padding(5);
            this.LueReason4.Name = "LueReason4";
            this.LueReason4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason4.Properties.Appearance.Options.UseFont = true;
            this.LueReason4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReason4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReason4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReason4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReason4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReason4.Properties.DropDownRows = 12;
            this.LueReason4.Properties.NullText = "[Empty]";
            this.LueReason4.Properties.PopupWidth = 500;
            this.LueReason4.Size = new System.Drawing.Size(284, 20);
            this.LueReason4.TabIndex = 39;
            this.LueReason4.ToolTip = "F4 : Show/hide list";
            this.LueReason4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReason4.EditValueChanged += new System.EventHandler(this.LueReason4_EditValueChanged);
            // 
            // TxtCashBackAmt
            // 
            this.TxtCashBackAmt.EnterMoveNextControl = true;
            this.TxtCashBackAmt.Location = new System.Drawing.Point(204, 156);
            this.TxtCashBackAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCashBackAmt.Name = "TxtCashBackAmt";
            this.TxtCashBackAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCashBackAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashBackAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCashBackAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCashBackAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCashBackAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCashBackAmt.Size = new System.Drawing.Size(166, 20);
            this.TxtCashBackAmt.TabIndex = 29;
            this.TxtCashBackAmt.Validated += new System.EventHandler(this.TxtCashBackAmt_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(520, 76);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 14);
            this.label12.TabIndex = 36;
            this.label12.Text = "Reason (3)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(3, 158);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 14);
            this.label7.TabIndex = 28;
            this.label7.Text = "Cash Back  Amount per Shipment";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReason3
            // 
            this.LueReason3.EnterMoveNextControl = true;
            this.LueReason3.Location = new System.Drawing.Point(591, 73);
            this.LueReason3.Margin = new System.Windows.Forms.Padding(5);
            this.LueReason3.Name = "LueReason3";
            this.LueReason3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason3.Properties.Appearance.Options.UseFont = true;
            this.LueReason3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReason3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReason3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReason3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReason3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReason3.Properties.DropDownRows = 12;
            this.LueReason3.Properties.NullText = "[Empty]";
            this.LueReason3.Properties.PopupWidth = 500;
            this.LueReason3.Size = new System.Drawing.Size(284, 20);
            this.LueReason3.TabIndex = 37;
            this.LueReason3.ToolTip = "F4 : Show/hide list";
            this.LueReason3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReason3.EditValueChanged += new System.EventHandler(this.LueReason3_EditValueChanged);
            // 
            // TxtCashBack2
            // 
            this.TxtCashBack2.EnterMoveNextControl = true;
            this.TxtCashBack2.Location = new System.Drawing.Point(317, 135);
            this.TxtCashBack2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCashBack2.Name = "TxtCashBack2";
            this.TxtCashBack2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCashBack2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashBack2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCashBack2.Properties.Appearance.Options.UseFont = true;
            this.TxtCashBack2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCashBack2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCashBack2.Size = new System.Drawing.Size(131, 20);
            this.TxtCashBack2.TabIndex = 26;
            this.TxtCashBack2.Validated += new System.EventHandler(this.TxtCashBack2_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(520, 54);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 14);
            this.label11.TabIndex = 34;
            this.label11.Text = "Reason (2)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCashBack1
            // 
            this.TxtCashBack1.EnterMoveNextControl = true;
            this.TxtCashBack1.Location = new System.Drawing.Point(204, 135);
            this.TxtCashBack1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCashBack1.Name = "TxtCashBack1";
            this.TxtCashBack1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCashBack1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCashBack1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCashBack1.Properties.Appearance.Options.UseFont = true;
            this.TxtCashBack1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCashBack1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCashBack1.Size = new System.Drawing.Size(86, 20);
            this.TxtCashBack1.TabIndex = 24;
            this.TxtCashBack1.Validated += new System.EventHandler(this.TxtCashBack1_Validated);
            // 
            // LueReason2
            // 
            this.LueReason2.EnterMoveNextControl = true;
            this.LueReason2.Location = new System.Drawing.Point(591, 51);
            this.LueReason2.Margin = new System.Windows.Forms.Padding(5);
            this.LueReason2.Name = "LueReason2";
            this.LueReason2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason2.Properties.Appearance.Options.UseFont = true;
            this.LueReason2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReason2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReason2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReason2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReason2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReason2.Properties.DropDownRows = 12;
            this.LueReason2.Properties.NullText = "[Empty]";
            this.LueReason2.Properties.PopupWidth = 500;
            this.LueReason2.Size = new System.Drawing.Size(284, 20);
            this.LueReason2.TabIndex = 35;
            this.LueReason2.ToolTip = "F4 : Show/hide list";
            this.LueReason2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReason2.EditValueChanged += new System.EventHandler(this.LueReason2_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(77, 137);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Cash Back Shipment";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(520, 33);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Reason (1)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTargetOmset
            // 
            this.TxtTargetOmset.EnterMoveNextControl = true;
            this.TxtTargetOmset.Location = new System.Drawing.Point(204, 114);
            this.TxtTargetOmset.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTargetOmset.Name = "TxtTargetOmset";
            this.TxtTargetOmset.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTargetOmset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTargetOmset.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTargetOmset.Properties.Appearance.Options.UseFont = true;
            this.TxtTargetOmset.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTargetOmset.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTargetOmset.Size = new System.Drawing.Size(166, 20);
            this.TxtTargetOmset.TabIndex = 22;
            this.TxtTargetOmset.Validated += new System.EventHandler(this.TxtTargetOmset_Validated);
            // 
            // LueReason1
            // 
            this.LueReason1.EnterMoveNextControl = true;
            this.LueReason1.Location = new System.Drawing.Point(591, 30);
            this.LueReason1.Margin = new System.Windows.Forms.Padding(5);
            this.LueReason1.Name = "LueReason1";
            this.LueReason1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason1.Properties.Appearance.Options.UseFont = true;
            this.LueReason1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReason1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReason1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReason1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReason1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReason1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReason1.Properties.DropDownRows = 12;
            this.LueReason1.Properties.NullText = "[Empty]";
            this.LueReason1.Properties.PopupWidth = 500;
            this.LueReason1.Size = new System.Drawing.Size(284, 20);
            this.LueReason1.TabIndex = 33;
            this.LueReason1.ToolTip = "F4 : Show/hide list";
            this.LueReason1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReason1.EditValueChanged += new System.EventHandler(this.LueReason1_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(53, 117);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Target Omset After Disc";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(528, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 30;
            this.label9.Text = "Customer";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(139, 96);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostumerCode
            // 
            this.LueCostumerCode.EnterMoveNextControl = true;
            this.LueCostumerCode.Location = new System.Drawing.Point(591, 7);
            this.LueCostumerCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostumerCode.Name = "LueCostumerCode";
            this.LueCostumerCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostumerCode.Properties.Appearance.Options.UseFont = true;
            this.LueCostumerCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostumerCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostumerCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostumerCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostumerCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostumerCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostumerCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostumerCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostumerCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostumerCode.Properties.DropDownRows = 12;
            this.LueCostumerCode.Properties.NullText = "[Empty]";
            this.LueCostumerCode.Properties.PopupWidth = 500;
            this.LueCostumerCode.Size = new System.Drawing.Size(284, 20);
            this.LueCostumerCode.TabIndex = 31;
            this.LueCostumerCode.ToolTip = "F4 : Show/hide list";
            this.LueCostumerCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostumerCode.EditValueChanged += new System.EventHandler(this.LueCostumerCode_EditValueChanged);
            // 
            // DteDtEnd
            // 
            this.DteDtEnd.EditValue = null;
            this.DteDtEnd.EnterMoveNextControl = true;
            this.DteDtEnd.Location = new System.Drawing.Point(204, 72);
            this.DteDtEnd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDtEnd.Name = "DteDtEnd";
            this.DteDtEnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDtEnd.Properties.Appearance.Options.UseFont = true;
            this.DteDtEnd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDtEnd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDtEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDtEnd.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDtEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDtEnd.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDtEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDtEnd.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDtEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDtEnd.Size = new System.Drawing.Size(135, 20);
            this.DteDtEnd.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(127, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Promo End";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDtStart
            // 
            this.DteDtStart.EditValue = null;
            this.DteDtStart.EnterMoveNextControl = true;
            this.DteDtStart.Location = new System.Drawing.Point(204, 51);
            this.DteDtStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDtStart.Name = "DteDtStart";
            this.DteDtStart.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDtStart.Properties.Appearance.Options.UseFont = true;
            this.DteDtStart.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDtStart.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDtStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDtStart.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDtStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDtStart.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDtStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDtStart.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDtStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDtStart.Size = new System.Drawing.Size(135, 20);
            this.DteDtStart.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(121, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Promo Start";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(449, 7);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active ";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.Size = new System.Drawing.Size(70, 22);
            this.ChkActiveInd.TabIndex = 12;
            this.ChkActiveInd.CheckedChanged += new System.EventHandler(this.ChkActiveInd_CheckedChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(204, 30);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(135, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(100, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Quotation Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(204, 9);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(238, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(44, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Quotation Promo Number";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSOQuotPromo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 396);
            this.Name = "FrmSOQuotPromo";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgAgent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBackAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBack2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCashBack1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTargetOmset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReason1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostumerCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDtStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgAgent;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtCashBackAmt;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtCashBack2;
        internal DevExpress.XtraEditors.TextEdit TxtCashBack1;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtTargetOmset;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDtEnd;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDtStart;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueReason4;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueReason3;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueReason2;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueReason1;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueCostumerCode;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtUom;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
    }
}