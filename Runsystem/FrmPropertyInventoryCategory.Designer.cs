﻿namespace RunSystem
{
    partial class FrmPropertyInventoryCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPropertyInventoryCategory));
            this.TxtAcDesc3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOACOGS = new System.Windows.Forms.Label();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOASales = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAInventory = new System.Windows.Forms.Label();
            this.TxtPropCtName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPropCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkMandatoryCertificateInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkInitialCategoryInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSalesCategoryInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkRentCategoryInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMandatoryCertificateInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInitialCategoryInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesCategoryInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRentCategoryInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(835, 0);
            this.panel1.Size = new System.Drawing.Size(70, 220);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.TxtAcDesc3);
            this.panel2.Controls.Add(this.BtnAcNo3);
            this.panel2.Controls.Add(this.TxtAcNo3);
            this.panel2.Controls.Add(this.LblCOACOGS);
            this.panel2.Controls.Add(this.TxtAcDesc2);
            this.panel2.Controls.Add(this.BtnAcNo2);
            this.panel2.Controls.Add(this.TxtAcNo2);
            this.panel2.Controls.Add(this.LblCOASales);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblCOAInventory);
            this.panel2.Controls.Add(this.TxtPropCtName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPropCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(835, 220);
            // 
            // TxtAcDesc3
            // 
            this.TxtAcDesc3.EnterMoveNextControl = true;
            this.TxtAcDesc3.Location = new System.Drawing.Point(312, 91);
            this.TxtAcDesc3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc3.Name = "TxtAcDesc3";
            this.TxtAcDesc3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc3.Properties.MaxLength = 80;
            this.TxtAcDesc3.Properties.ReadOnly = true;
            this.TxtAcDesc3.Size = new System.Drawing.Size(323, 20);
            this.TxtAcDesc3.TabIndex = 24;
            // 
            // BtnAcNo3
            // 
            this.BtnAcNo3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo3.Appearance.Options.UseBackColor = true;
            this.BtnAcNo3.Appearance.Options.UseFont = true;
            this.BtnAcNo3.Appearance.Options.UseForeColor = true;
            this.BtnAcNo3.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo3.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo3.Image")));
            this.BtnAcNo3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo3.Location = new System.Drawing.Point(643, 90);
            this.BtnAcNo3.Name = "BtnAcNo3";
            this.BtnAcNo3.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo3.TabIndex = 25;
            this.BtnAcNo3.ToolTip = "Find COA\'s Account";
            this.BtnAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo3.ToolTipTitle = "Run System";
            this.BtnAcNo3.Click += new System.EventHandler(this.BtnAcNo3_Click);
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(155, 91);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.MaxLength = 40;
            this.TxtAcNo3.Properties.ReadOnly = true;
            this.TxtAcNo3.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo3.TabIndex = 23;
            // 
            // LblCOACOGS
            // 
            this.LblCOACOGS.AutoSize = true;
            this.LblCOACOGS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOACOGS.ForeColor = System.Drawing.Color.Black;
            this.LblCOACOGS.Location = new System.Drawing.Point(60, 95);
            this.LblCOACOGS.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOACOGS.Name = "LblCOACOGS";
            this.LblCOACOGS.Size = new System.Drawing.Size(89, 14);
            this.LblCOACOGS.TabIndex = 22;
            this.LblCOACOGS.Text = "COA# (COGS) ";
            this.LblCOACOGS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(312, 70);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 80;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(323, 20);
            this.TxtAcDesc2.TabIndex = 20;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(644, 68);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo2.TabIndex = 21;
            this.BtnAcNo2.ToolTip = "Find COA\'s Account";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(155, 70);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 40;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo2.TabIndex = 19;
            // 
            // LblCOASales
            // 
            this.LblCOASales.AutoSize = true;
            this.LblCOASales.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOASales.ForeColor = System.Drawing.Color.Black;
            this.LblCOASales.Location = new System.Drawing.Point(64, 74);
            this.LblCOASales.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOASales.Name = "LblCOASales";
            this.LblCOASales.Size = new System.Drawing.Size(85, 14);
            this.LblCOASales.TabIndex = 18;
            this.LblCOASales.Text = "COA# (Sales) ";
            this.LblCOASales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(312, 49);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(323, 20);
            this.TxtAcDesc.TabIndex = 16;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(644, 47);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo.TabIndex = 17;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(155, 49);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 40;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo.TabIndex = 15;
            // 
            // LblCOAInventory
            // 
            this.LblCOAInventory.AutoSize = true;
            this.LblCOAInventory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAInventory.ForeColor = System.Drawing.Color.Red;
            this.LblCOAInventory.Location = new System.Drawing.Point(38, 53);
            this.LblCOAInventory.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAInventory.Name = "LblCOAInventory";
            this.LblCOAInventory.Size = new System.Drawing.Size(111, 14);
            this.LblCOAInventory.TabIndex = 14;
            this.LblCOAInventory.Text = "COA# (Inventory) ";
            this.LblCOAInventory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropCtName
            // 
            this.TxtPropCtName.EnterMoveNextControl = true;
            this.TxtPropCtName.Location = new System.Drawing.Point(155, 27);
            this.TxtPropCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropCtName.Name = "TxtPropCtName";
            this.TxtPropCtName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPropCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtPropCtName.Properties.MaxLength = 80;
            this.TxtPropCtName.Size = new System.Drawing.Size(480, 20);
            this.TxtPropCtName.TabIndex = 13;
            this.TxtPropCtName.Validated += new System.EventHandler(this.TxtPropCtName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(7, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Property Category Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPropCtCode
            // 
            this.TxtPropCtCode.EnterMoveNextControl = true;
            this.TxtPropCtCode.Location = new System.Drawing.Point(155, 6);
            this.TxtPropCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPropCtCode.Name = "TxtPropCtCode";
            this.TxtPropCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPropCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPropCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPropCtCode.Properties.MaxLength = 16;
            this.TxtPropCtCode.Size = new System.Drawing.Size(178, 20);
            this.TxtPropCtCode.TabIndex = 10;
            this.TxtPropCtCode.Validated += new System.EventHandler(this.TxtPropCtCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Property Category Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(342, 4);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActiveInd.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.ChkMandatoryCertificateInd);
            this.panel4.Controls.Add(this.ChkInitialCategoryInd);
            this.panel4.Controls.Add(this.ChkSalesCategoryInd);
            this.panel4.Controls.Add(this.ChkRentCategoryInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(676, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(159, 220);
            this.panel4.TabIndex = 26;
            // 
            // ChkMandatoryCertificateInd
            // 
            this.ChkMandatoryCertificateInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkMandatoryCertificateInd.Location = new System.Drawing.Point(5, 71);
            this.ChkMandatoryCertificateInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkMandatoryCertificateInd.Name = "ChkMandatoryCertificateInd";
            this.ChkMandatoryCertificateInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkMandatoryCertificateInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkMandatoryCertificateInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkMandatoryCertificateInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkMandatoryCertificateInd.Properties.Appearance.Options.UseFont = true;
            this.ChkMandatoryCertificateInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkMandatoryCertificateInd.Properties.Caption = "Mandatory Certificate";
            this.ChkMandatoryCertificateInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkMandatoryCertificateInd.Size = new System.Drawing.Size(149, 22);
            this.ChkMandatoryCertificateInd.TabIndex = 30;
            // 
            // ChkInitialCategoryInd
            // 
            this.ChkInitialCategoryInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkInitialCategoryInd.Location = new System.Drawing.Point(5, 49);
            this.ChkInitialCategoryInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkInitialCategoryInd.Name = "ChkInitialCategoryInd";
            this.ChkInitialCategoryInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkInitialCategoryInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInitialCategoryInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkInitialCategoryInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkInitialCategoryInd.Properties.Appearance.Options.UseFont = true;
            this.ChkInitialCategoryInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkInitialCategoryInd.Properties.Caption = "Initial Category";
            this.ChkInitialCategoryInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInitialCategoryInd.Size = new System.Drawing.Size(109, 22);
            this.ChkInitialCategoryInd.TabIndex = 29;
            // 
            // ChkSalesCategoryInd
            // 
            this.ChkSalesCategoryInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSalesCategoryInd.Location = new System.Drawing.Point(5, 27);
            this.ChkSalesCategoryInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSalesCategoryInd.Name = "ChkSalesCategoryInd";
            this.ChkSalesCategoryInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSalesCategoryInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSalesCategoryInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSalesCategoryInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSalesCategoryInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSalesCategoryInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSalesCategoryInd.Properties.Caption = "Sales Category";
            this.ChkSalesCategoryInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSalesCategoryInd.Size = new System.Drawing.Size(109, 22);
            this.ChkSalesCategoryInd.TabIndex = 28;
            this.ChkSalesCategoryInd.CheckedChanged += new System.EventHandler(this.ChkSalesCategoryInd_CheckedChanged);
            // 
            // ChkRentCategoryInd
            // 
            this.ChkRentCategoryInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRentCategoryInd.Location = new System.Drawing.Point(5, 5);
            this.ChkRentCategoryInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRentCategoryInd.Name = "ChkRentCategoryInd";
            this.ChkRentCategoryInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRentCategoryInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRentCategoryInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRentCategoryInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRentCategoryInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRentCategoryInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRentCategoryInd.Properties.Caption = "Rent Category";
            this.ChkRentCategoryInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRentCategoryInd.Size = new System.Drawing.Size(109, 22);
            this.ChkRentCategoryInd.TabIndex = 27;
            this.ChkRentCategoryInd.CheckedChanged += new System.EventHandler(this.ChkRentCategoryInd_CheckedChanged);
            // 
            // FrmPropertyInventoryCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 220);
            this.Name = "FrmPropertyInventoryCategory";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkMandatoryCertificateInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInitialCategoryInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSalesCategoryInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRentCategoryInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtAcDesc3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo3;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        private System.Windows.Forms.Label LblCOACOGS;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label LblCOASales;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label LblCOAInventory;
        private DevExpress.XtraEditors.TextEdit TxtPropCtName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtPropCtCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkMandatoryCertificateInd;
        private DevExpress.XtraEditors.CheckEdit ChkInitialCategoryInd;
        private DevExpress.XtraEditors.CheckEdit ChkSalesCategoryInd;
        private DevExpress.XtraEditors.CheckEdit ChkRentCategoryInd;
    }
}