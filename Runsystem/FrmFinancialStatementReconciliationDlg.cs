﻿#region Update
/*
  12/07/2022 [DITA/PRODUCT] New Apps
  26/07/2022 [DITA/PRODUCT] meng 0 kan amount2 di header sat choose data
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFinancialStatementReconciliationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmFinancialStatementReconciliation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmFinancialStatementReconciliationDlg(FrmFinancialStatementReconciliation FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }


        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Trim().Length == 0) this.Text = "Importing Financial Statement Document";
                base.FrmLoad(sender, e);
                Sl.SetLueEntCode(ref LueEntCode);
                SetGrd();
                SetSQL();   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EntName, A.DocNo, A.DocDt, A.ClosingDt ");
            SQL.AppendLine("From TblFinancialStatementHdr A ");
            SQL.AppendLine("Inner Join TblEntity B On A.EntCode = B.EntCode ");
            SQL.AppendLine("    And A.EntCode = @EntCode ");
            SQL.AppendLine("Where A.CancelInd = 'N'  ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("  Select IFSDocNo From TblFinancialStatementReconciliationHdr Where CancelInd = 'N' And CancelReason Is Null ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-4
                    "Entity",
                    "Document#",
                    "Date",
                    "Closing Date",
                    
                },
                new int[]
                {
                    50,
                    150, 140, 80, 80
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 3,4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueEntCode, "Entity")) return;

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                if (Sm.GetDte(DteClosingDt).Length > 0) Filter += " And A.ClosingDt = @ClosingDt ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (Sm.GetDte(DteClosingDt).Length > 0)
                    Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By B.EntName, A.DocNo;",
                    new string[]
                    {
                        "EntName",
                        "DocNo", "DocDt", "ClosingDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.ClearGrd();
                mFrmParent.TxtEntName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtIFSDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                Sm.SetDte(mFrmParent.DteClosingDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 4));
                mFrmParent.ShowAccount(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { mFrmParent.TxtBalanceSheetAmt, mFrmParent.TxtProfitLossAmt, mFrmParent.TxtSumBalance }, 0);

                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }


        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        private void DteClosingDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkClosingDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Closing Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion
    }
}
