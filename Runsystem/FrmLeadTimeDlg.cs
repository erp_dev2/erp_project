﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLeadTimeDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmLeadTime mFrmParent;
        private string mSQL = string.Empty;
        internal string mMenuCode = string.Empty;
        internal string mItCtCode = string.Empty,
            mItScCode = string.Empty,
            mItGrpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmLeadTimeDlg (FrmLeadTime FrmParent, string ItCtCode, string ItScCode, string ItGrpCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItCtCode = ItCtCode;
            mItScCode = ItScCode;
            mItGrpCode = ItGrpCode;

        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's"+Environment.NewLine+"Name",

                        //6
                        "Item's"+Environment.NewLine+"Category",
                        "Item's"+Environment.NewLine+"Subcategory"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, C.ItScName "); 
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubCategory C On A.ItScCode=C.ItScCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.ItCtCode=@ItCtCode ");
            if (mItScCode.Length > 0)
                SQL.AppendLine("And A.ItScCode=@ItScCode ");
            if (mItGrpCode.Length > 0)
                SQL.AppendLine("And A.ItGrpCode=@ItGrpCode");

            mSQL = SQL.ToString();
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.ItCode Not In (" + mFrmParent.GetSelectedItem() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ItCtCode", mItCtCode);
                Sm.CmParam<String>(ref cm, "@ItScCode", mItScCode);
                Sm.CmParam<String>(ref cm, "@ItGrpCode", mItGrpCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                        
                            //0
                            "ItCode",

                            //1-3
                            "ItCodeInternal", "ItName", "ItCtName", "ItScName"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        mFrmParent.Grd1.Cells[Row1, 5].Value = 0m;

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 5].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareGrdStr(mFrmParent.Grd1, Index, 2, Grd1, Row, 2)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }


        #endregion

        #endregion

    }
}
