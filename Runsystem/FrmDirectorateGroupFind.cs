﻿#region Update
/*
    08/04/2022 [TYO/PHT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDirectorateGroupFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDirectorateGroup mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDirectorateGroupFind(FrmDirectorateGroup FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DirectorateGroupCode, A.DirectorateGroupName, B.DirectorateGroupName AS ParentName, A.NotParentInd, A.ActInd ");
            SQL.AppendLine("FROM tbldirectorategroup A ");
            SQL.AppendLine("LEFT JOIN tbldirectorategroup B ON A.ParentCode = B.DirectorateGroupCode ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Directorate"+Environment.NewLine+"Group Code",
                        "Directorate"+Environment.NewLine+"Group Name",
                        "Directorate"+Environment.NewLine+"Group Parent",
                        "Not Parent",
                        "Active"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 100, 100, 50
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtDirectorateGroup.Text, new string[] { "A.DirectorateGroupCode", "A.DirectorateGroupName" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DirectorateGroupCode ",
                        new string[]
                        {
                             //0
                            "DirectorateGroupCode", 
                                
                            //1-4
                            "DirectorateGroupName", "ParentName", "NotParentInd", "ActInd",
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 5, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion
    }
}
