﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvDeptDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvDept mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty, mDeptCode = string.Empty;
        private bool mIsInventoryShowTotalQty = false, mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmRecvDeptDlg(FrmRecvDept FrmParent, string WhsCode, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno, B.DNo, A.DocDt, ");
            SQL.AppendLine("B.ItCode, D.ItCodeInternal, D.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, B.Remark, E.CCCode, E.CCName, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0) As Qty, B.Qty2-IfNull(C.Qty2, 0) As Qty2, B.Qty3-IfNull(C.Qty3, 0) As Qty3  ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.Source Is Not Null  ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select DODeptDocNo, DODeptDNo, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
	        SQL.AppendLine("    From TblRecvDeptDtl ");
            SQL.AppendLine("    Where CancelInd='N' "); 	
	        SQL.AppendLine("    Group By DODeptDocNo, DODeptDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DODeptDocNo And B.DNo=C.DODeptDNo ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblCostCenter E On A.CCCode=E.CCCode ");
            SQL.AppendLine("Where (B.Qty-IfNull(C.Qty, 0)>0 Or B.Qty2-IfNull(C.Qty2, 0)>0 Or B.Qty3-IfNull(C.Qty3, 0)>0) ");
            SQL.AppendLine("And A.WhsCode=@WhsCode ");	
            SQL.AppendLine("And A.DeptCode=@DeptCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "Document DNo",
                        "Date",
                        "Item's Code", 

                        //6-10
                        "", 
                        "Local Code",
                        "Item's Name", 
                        "Batch Number",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin", 
                        "Available DO" + Environment.NewLine + "Quantity",
                        "UoM",
                        "Available DO" + Environment.NewLine + "Quantity 2",
                        
                        //16-20
                        "UoM",
                        "Available DO" + Environment.NewLine + "Quantity 3",
                        "UoM",
                        "Remark",
                        "Cost Center Code",

                        //21
                        "Cost Center"
                    },
                    new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 130, 50, 80, 80, 
                        
                        //6-10
                        20, 100, 250, 200, 180,
                        
                        //11-15
                        60, 80, 80, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 400, 0,

                        //21
                        200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 6, 7, 10, 11, 12, 15, 16, 17, 18, 20 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedDODept() + ") "; ;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItCodeInternal", "D.ItName" });

                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DNo", "DocDt", "ItCode", "ItCodeInternal", "ItName",  
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty", 
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3",
 
                            //16-18
                            "Remark", "CCCode", "CCName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 21);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 15, 18, 21 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(Grd1, Row, 3)))
                        return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty && mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
