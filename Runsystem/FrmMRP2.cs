﻿#region Update
/*
    21/06/2018 [WED] rename dari MRP ke MRP 2. Find and Replace PPDocNo --> ProductionOrderDocNo
    09/07/2018 [WED] Production Order yang muncul di dialog nya adalah ProdOrder yang sudah diproses di ProdPlanning
    16/06/2020 [VIN/MMM] Parameter QtyFormulaForMRP -> pembulatan dari batch quantity di Production Order * quantity di BOM
    
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMRP2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty; //if this application is called from other application;
        internal FrmMRP2Find FrmFind;
        internal string mQtyFormulaForMRP = string.Empty;

        #endregion

        #region Constructor

        public FrmMRP2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmMRP2");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();

                SetFormControl(mState.View);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtProductionOrderDocNo
                }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6 });

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code", 
                        "Item's Name",
                        "Quantity",

                        //6
                        "UoM"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 20, 100, 250, 100,

                        //6
                        80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 3 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 16;
            Sm.GrdHdr(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "SeqNo",
 
                        //1-5
                        "ProductionOrderDocNo",
                        "ProductionOrderDocNo",
                        "WorkCenterDocNo", 
                        "Sequence",
                        "MaxSequence",

                        //6-10
                        "ResultItCode",
                        "ResultQty",
                        "BomItCode",
                        "BomQty",
                        "RoutingSource",
                        
                        //11-15
                        "BomPercentage",
                        "BomQuantity",
                        "ItCodeInternal",
                        "ItName",
                        "PlanningUomCode"
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 4, 5, 7, 9, 10, 11, 12 }, 0);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, MeeRemark
                    }, true);
                    BtnProductionOrderDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    BtnProductionOrderDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtProductionOrderDocNo, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 2);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 4, 5, 7, 9, 10, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMRP2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 1));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 1));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MRP2", "TblMRP2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMRP2Hdr(DocNo));

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveMRP2Dtl(DocNo, Row));
            }

            cml.Add(UpdateProductionOrderProcessInd(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtProductionOrderDocNo, "Production Order#", false) ||
                IsProductionOrderAlreadyCancelled() ||
                IsProductionOrderAlreadyProcessedToMRP() ||
                IsGrdExceedMaxRecords() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
            ;
        }

        private bool IsProductionOrderAlreadyProcessedToMRP()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select ProductionOrderDocNo " +
                    "From TblMRP2Hdr " +
                    "Where CancelInd='N' " +
                    "And IfNull(ProductionOrderDocNo, 'XXX')=@ProductionOrderDocNo; "
            };
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production Order# : " + TxtProductionOrderDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "Another MRP already used this production order#.");
                return true;
            }
            return false;
        }

        private bool IsProductionOrderAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProductionOrderHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtProductionOrderDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Production Order# : " + TxtProductionOrderDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This Production Order# already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 work center.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveMRP2Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblMRP2Hdr(DocNo, DocDt, CancelInd, ProductionOrderDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @ProductionOrderDocNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMRP2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblMRP2Dtl(DocNo, DNo, ItCode, Qty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ItCode, @Qty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMRP2Dtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblMRP2Dtl2(DocNo, DNo, SeqNo, ProductionOrderDocNo, WorkCenterDocNo, Sequence, MaxSequence, " +
                    "ResultCode, ResultQty, BomItCode, BomQty, RoutingSource, BomPercentage, BomQuantity, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @SeqNo, @ProductionOrderDocNo, @WorkCenterDocNo, @Sequence, @MaxSequence, " +
                    "@ResultCode, @ResultQty, @BomItCode, @BomQty, @RoutingSource, @BomPercentage, @BomQuantity, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@SeqNo", Sm.GetGrdDec(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetGrdStr(Grd2, Row, 3)); 
            Sm.CmParam<Decimal>(ref cm, "@Sequence", Sm.GetGrdDec(Grd2, Row, 4)); 
            Sm.CmParam<Decimal>(ref cm, "@MaxSequence", Sm.GetGrdDec(Grd2, Row, 5)); 
            Sm.CmParam<String>(ref cm, "@ResultCode", Sm.GetGrdStr(Grd2, Row, 6)); 
            Sm.CmParam<Decimal>(ref cm, "@ResultQty", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@BomItCode", Sm.GetGrdStr(Grd2, Row, 8)); 
            Sm.CmParam<Decimal>(ref cm, "@BomQty", Sm.GetGrdDec(Grd2, Row, 9)); 
            Sm.CmParam<Decimal>(ref cm, "@RoutingSource", Sm.GetGrdDec(Grd2, Row, 10)); 
            Sm.CmParam<Decimal>(ref cm, "@BomPercentage", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@BomQuantity", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateProductionOrderProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionOrderHdr Set ProcessInd2='O' ");
            SQL.AppendLine("Where DocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select * From (");
            SQL.AppendLine("        Select DocNo From TblProductionOrderHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And DocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("        And DocNo In ( ");
            SQL.AppendLine("            Select ProductionOrderDocNo As Key1 From TblMRP2Hdr ");
            SQL.AppendLine("            Where CancelInd='N' And ProductionOrderDocNo Is Not Null");
            SQL.AppendLine("        )) T ); ");

            SQL.AppendLine("Update TblProductionOrderHdr Set ProcessInd2='F' ");
            SQL.AppendLine("Where DocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select * From (");
            SQL.AppendLine("        Select DocNo From TblProductionOrderHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And DocNo=@ProductionOrderDocNo ");
            SQL.AppendLine("        And DocNo In ( ");
            SQL.AppendLine("            Select ProductionOrderDocNo As Key1 From TblMRP2Hdr ");
            SQL.AppendLine("            Where CancelInd='N' And ProductionOrderDocNo Is Not Null");
            SQL.AppendLine("        )) T ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMRP2Hdr());

            cml.Add(UpdateProductionOrderProcessInd(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            return IsDataExists(
                "Select DocNo From TblMRP2Hdr " +
                "Where CancelInd='Y' And DocNo=@Param; ",
                TxtDocNo.Text, "This document already cancelled."
                );
        }

        private MySqlCommand CancelMRP2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMRP2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMRP2Hdr(DocNo);
                ShowMRP2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMRP2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, ProductionOrderDocNo, Remark " +
                    "From TblMRP2Hdr Where DocNo=@DocNo;",
                    new string[]{ "DocNo", "DocDt", "CancelInd", "ProductionOrderDocNo", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtProductionOrderDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowMRP2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItCodeInternal, B.ItName, A.Qty, B.PlanningUomCode ");
            SQL.AppendLine("From TblMRP2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "ItCode", "ItCodeInternal", "ItName", "Qty", "PlanningUomCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        internal void GenerateWorkCenterBOM(string ProductionOrderDocNo)
        {
            try
            {
                ClearGrd();

                var lwcb = new List<WCB21>();
                SetWCB(ProductionOrderDocNo, ref lwcb);

                if (lwcb.Count > 0)
                {
                    //SetRoutingSource(ref lwcb);
                    //ComputeBomPercentage(ref lwcb);
                    //ComputeBomQuantity(ref lwcb);
                    //CopyWCBToGrd(ref lwcb);
                    SetWCB2(ref lwcb);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetWCB(string ProductionOrderDocNo, ref List<WCB21> lwcb)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            decimal SeqNo = 0, MaxSequence = 0;

            SQL.AppendLine("Select C.DocNo, E.WorkCenterDocNo, E.Sequence, "); 
            SQL.AppendLine("G.ItCode As ResultItCode, G.Qty As ResultQty, ");
            SQL.AppendLine("H.DocCode As BomItCode, I.ItCodeInternal, I.ItName, H.Qty As BomQty, I.PlanningUomCode, C.Qty As ProductionOrderQty ");
            if(mQtyFormulaForMRP == "1")
                SQL.AppendLine(",  (C.Qty * H.Qty) As MRP2Qty ");
            else
                SQL.AppendLine(",  (round(C.BatchQty) * H.Qty) As MRP2Qty ");
            SQL.AppendLine("From TblProductionOrderHdr C ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo And C.DocNo = @ProductionOrderDocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblBOMHdr F On D.BomDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblBOMDtl2 G On F.DocNo=G.DocNo And G.ItType='1' ");
            SQL.AppendLine("Inner Join TblBOMDtl H On F.DocNo=H.DocNo And H.DocType='1' ");
            SQL.AppendLine("Left Join TblItem I On H.DocCode=I.ItCode ");
            SQL.AppendLine("Order By C.DocDt, C.DocNo, E.Sequence Desc, E.WorkcenterDocNo, H.DocCode; ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", ProductionOrderDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "WorkCenterDocNo", "Sequence", "ResultItCode", "ResultQty", "BomItCode", 
                         
                         //6-10
                         "BomQty", "ItCodeInternal", "ItName", "PlanningUomCode", "ProductionOrderQty",

                         //11
                         "MRP2Qty"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SeqNo += 1;
                        lwcb.Add(new WCB21()
                        {
                            SeqNo = SeqNo,
                            ProductionOrderDocNo = Sm.DrStr(dr, c[0]),
                            WorkCenterDocNo = Sm.DrStr(dr, c[1]),
                            Sequence = Sm.DrDec(dr, c[2]),
                            MaxSequence = Sm.CompareStr(ProductionOrderDocNo, Sm.DrStr(dr, c[0])) ? 
                                MaxSequence : Sm.DrDec(dr, c[2]),
                            ResultItCode = Sm.DrStr(dr, c[3]),
                            ResultQty = Sm.DrDec(dr, c[4]),
                            BomItCode = Sm.DrStr(dr, c[5]),
                            BomQty = Sm.DrDec(dr, c[6]),
                            ItCodeInternal = Sm.DrStr(dr, c[7]),
                            ItName = Sm.DrStr(dr, c[8]),
                            PlanningUomCode = Sm.DrStr(dr, c[9]),
                            ProductionOrderQty = Sm.DrDec(dr, c[10]),
                            MRP2Qty = Sm.DrDec(dr, c[11]),
                            RoutingSource = 0m,
                            BomPercentage = 100m,
                            BomQuantity = 0m
                        });
                        MaxSequence = Sm.CompareStr(ProductionOrderDocNo, Sm.DrStr(dr, c[0])) ?
                            MaxSequence : Sm.DrDec(dr, c[2]);
                        ProductionOrderDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }
        }

        private void SetRoutingSource(ref List<WCB21> lwcb)
        {
            var lwcb2 = lwcb.ToList();
            lwcb.ForEach(i =>
            {
                foreach (var i2 in lwcb2.Where(Index => Index.ProductionOrderDocNo == i.ProductionOrderDocNo)) //Enumerable.Reverse()
                {
                    if (i.Sequence < i2.Sequence && Sm.CompareStr(i.ResultItCode, i2.BomItCode))
                    {
                        i.RoutingSource = i2.Sequence;
                        break;
                    }   
                }
            });
        }

        private void ComputeBomPercentage(ref List<WCB21> lwcb)
        {
            lwcb.ForEach(i =>{ i.BomPercentage = Math.Round(i.BomQty / i.ResultQty, 4); });
        }

        private void ComputeBomQuantity(ref List<WCB21> lwcb)
        {
            List<WCB21> lwcb2 = lwcb.ToList();
            List<WCB21> lwcb3 = null;
            lwcb2.ForEach(i =>
            {
                if (i.RoutingSource != 0)
                {
                    lwcb3=null;
                    lwcb3 = lwcb2.ToList();
                    foreach (var i2 in lwcb3.Where(Index => Index.Sequence == i.RoutingSource && Index.BomItCode == i.ResultItCode)) 
                    {
                        i.BomQuantity = i.BomPercentage * i2.BomQuantity;
                        break;
                    }
                }
                else
                    i.BomQuantity = i.BomQty;
            });
        }

        private void CopyWCBToGrd(ref List<WCB21> lwcb)
        {
            int Row = 0;
            Grd2.ProcessTab = true;
            Grd2.BeginUpdate();
            lwcb.ForEach(i =>
            {
                Grd2.Rows.Add();
                Grd2.Cells[Row, 0].Value = i.SeqNo;
                Grd2.Cells[Row, 1].Value = i.ProductionOrderDocNo; 
                Grd2.Cells[Row, 2].Value = i.ProductionOrderDocNo; 
                Grd2.Cells[Row, 3].Value = i.WorkCenterDocNo;
                Grd2.Cells[Row, 4].Value = i.Sequence; 
                Grd2.Cells[Row, 5].Value = i.MaxSequence;
                Grd2.Cells[Row, 6].Value = i.ResultItCode;
                Grd2.Cells[Row, 7].Value = i.ResultQty;
                Grd2.Cells[Row, 8].Value = i.BomItCode;
                Grd2.Cells[Row, 9].Value = i.BomQty;
                Grd2.Cells[Row, 10].Value = i.RoutingSource;
                Grd2.Cells[Row, 11].Value = i.BomPercentage;
                Grd2.Cells[Row, 12].Value = i.BomQuantity;
                Row++;
            });
            Grd2.Cols.AutoWidth();
            Grd2.EndUpdate();
        }

        private void SetWCB2(ref List<WCB21> lwcb)
        {
            var result = from value in lwcb
                         group value.MRP2Qty by new { 
                             value.BomItCode, 
                             value.ItCodeInternal, 
                             value.ItName, 
                             value.PlanningUomCode 
                         } into grouped
                         select new WCB22()
                         {
                             ItCode = grouped.Key.BomItCode,
                             ItCodeInternal = grouped.Key.ItCodeInternal,
                             ItName = grouped.Key.ItName,
                             PlanningUomCode = grouped.Key.PlanningUomCode,
                             Qty = grouped.Sum()
                         };

            int Row = 0;
            Grd1.ProcessTab = true;
            Grd1.BeginUpdate();
            foreach (WCB22 i in result)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 1].Value = i.ItCode;
                Grd1.Cells[Row, 3].Value = i.ItCodeInternal;
                Grd1.Cells[Row, 4].Value = i.ItName;
                Grd1.Cells[Row, 5].Value = i.Qty;
                Grd1.Cells[Row, 6].Value = i.PlanningUomCode;
                Row++;
            }
            Grd1.EndUpdate();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
        }
        private void GetParameter()
        {
            mQtyFormulaForMRP = Sm.GetParameter("QtyFormulaForMRP");
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnProductionOrderDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMRP2Dlg(this));
        }

        private void BtnProductionOrderDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtProductionOrderDocNo, "Production Order#", false))
            {
                var f1 = new FrmProductionOrder(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtProductionOrderDocNo.Text;
                f1.ShowDialog();
            }
        }
        
        #endregion

        #endregion
    }

    internal class WCB21
    {
        public decimal SeqNo { get; set; }
        public string ProductionOrderDocNo { get; set; }
        public string WorkCenterDocNo { get; set; }
        public decimal Sequence { get; set; }
        public decimal MaxSequence { get; set; }
        public string ResultItCode { get; set; }
        public decimal ResultQty { get; set; }
        public string BomItCode { get; set; }
        public decimal BomQty { get; set; }
        public decimal RoutingSource { get; set; }
        public decimal BomPercentage { get; set; }
        public decimal BomQuantity { get; set; }
        public string ItCodeInternal { get; set; }
        public string ItName { get; set; }
        public string PlanningUomCode { get; set; }
        public decimal ProductionOrderQty { get; set; }
        public decimal MRP2Qty { get; set; }
        public decimal MRP2Qty2 { get; set; }

    }

    internal class WCB22
    {
        public string ItCode { get; set; }
        public string ItCodeInternal { get; set; }
        public string ItName { get; set; }
        public decimal Qty { get; set; }
        public string PlanningUomCode { get; set; }
    }
}
