﻿#region Update
/*   18/04/2017 [WED] Validasi max item menjadi 99.999 item di method Choose Data
     07/08/2017 [WED] tambah kolom foreign name, berdasarkan parameter IsShowForeignName
     18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
     * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentStockOpname2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentStockOpname2 mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty;
        private bool mIsInventoryShowTotalQty = false, mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmInvestmentStockOpname2Dlg(FrmInvestmentStockOpname2 FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                SetGrd();
                SetSQL();
                mFrmParent.SetLueInvestmentCtCode(ref LueItCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.InvestmentCode, B.InvestmentCodeInternal, B.InvestmentName, A.BatchNo, A.Source, C.InvestmentCtName, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, ");
            SQL.AppendLine("D.CurCode, D.UPrice, B.ForeignName, B.Specification ");
            SQL.AppendLine("From TblInvestmentStockSummary A ");
            SQL.AppendLine("Inner Join TblInvestmentItem B On A.InvestmentCode=B.InvestmentCode ");
            SQL.AppendLine("Inner Join TblInvestmentCategory C On B.InvestmentCtCode=C.InvestmentCtCode ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source=D.Source And A.InvestmentCode=D.InvestmentCode And A.BatchNo=D.BatchNo ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Investment's Code", 
                        "", 
                        "Investment's" + Environment.NewLine + "Local Code", 
                        "Investment's Name", 
                        
                        //6-10
                        "Investment's Category",
                        "Batch Number",
                        "Source",
                        "Lot",
                        "Bin", 
                        
                        //11-15
                        "Stock",
                        "UoM",
                        "Stock 2",
                        "Uom 2",
                        "Stock 3",

                        //16-20
                        "Uom 3",
                        "Currency",
                        "Price",
                        "Foreign Name",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 20, 80, 250, 
                        
                        //6-10
                        150, 200, 170, 60, 60, 
                        
                        //11-15
                        80, 80, 80, 80, 80,

                        //16-20
                        80, 60, 120, 200, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 18 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 9, 13, 14, 15, 16, 17 }, false);
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 19 });
            if (!mFrmParent.mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });

            Grd1.Cols[19].Move(7);
            Grd1.Cols[20].Move(6);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 9, 17 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter2 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "A", ref mFrmParent.Grd1, 7, 8, 9);    
                var Filter = (Filter2.Length > 0)?" And (" + Filter2 + ") " : " And 0=0 ";
                
               
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.InvestmentCode", "B.InvestmentCodeInternal", "B.InvestmentName", "B.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.InvestmentCtCode", true);

                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By B.InvestmentName, A.BatchNo, A.Bin;",
                        new string[] 
                        { 
                            //0
                            "InvestmentCode",
 
                            //1-5
                            "InvestmentCodeInternal", "InvestmentName", "InvestmentCtName", "BatchNo", "Source",  
                            
                            //6-10
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //11-15
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",

                            //16-17
                            "ForeignName", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13, 15 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                var ls = GetExistingData();
                mFrmParent.Grd1.ProcessTab = true;
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && 
                        !IsInvestmentCodeAlreadyChosen(
                            ls,
                            Sm.GetGrdStr(Grd1, Row, 2)+
                            Sm.GetGrdStr(Grd1, Row, 7)+
                            Sm.GetGrdStr(Grd1, Row, 8)+
                            Sm.GetGrdStr(Grd1, Row, 9)+
                            Sm.GetGrdStr(Grd1, Row, 10)
                            )
                        )
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 12, 16, 20 });
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }
            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20, 23, 25 });
            mFrmParent.Grd1.EndUpdate();
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
            {
                if (mFrmParent.Grd1.Rows.Count>100000)
                    Sm.StdMsg(mMsgType.Warning,
                        "Number of rows : " + (mFrmParent.Grd1.Rows.Count-1).ToString() + Environment.NewLine +
                        "You've already choose data more than maximum limit ( 99.999 ).");
            }
        }

        private bool IsInvestmentCodeAlreadyChosen(List<string> ls, string Key)
        {
            foreach (string s in ls)
            {
                if (string.Compare(s, Key) == 0) return true;
            }
            return false;
        }

        private List<string> GetExistingData()
        {
            var ls = new List<string>();

            for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 2).Length != 0)
                {
                    ls.Add(
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 2) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 6) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 7) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 8) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 9)
                    );
                }
            }
            return ls;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmInvestmentItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmInvestmentItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mInvestmentCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty && mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Misc Control Event

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueInvestmentCtCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueInvestmentCtCode, new Sm.RefreshLue1(Sl.SetLueInvestmentCtCode));
            //Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInvestmentCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion
    }
}
