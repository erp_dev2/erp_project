﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGroupDlg7 : RunSystem.FrmBase4
    {
        #region Field

        private FrmGroup mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGroupDlg7(FrmGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "List Of Group";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from TblGroup ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Group"+Environment.NewLine+"Code", 
                        "Group"+Environment.NewLine+"Name",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        //6-8
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 150, 100, 100, 100,   
                    
                        //6-8
                        100, 100, 100
                    }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 7 });
            Sm.GrdFormatTime(Grd1, new int[] { 5, 8 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtGroup.Text, new string[] { "GrpCode", "GrpName" });
             
                Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By GrpName ;",
                            new string[]
                        {
                            //0
                            "GrpCode", 

                            //1-5
                            "GrpName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                            
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("T", Grd1, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("T", Grd1, dr, c, Row, 8, 5);
                            }, true, false, false, false
                        );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), 1);
                this.Close();
            }

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
        #region Event
        private void ChkGroup_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Group");
        }

        private void TxtGroup_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
