﻿#region Update
/*
    03/08/2017 [TKG] tambah resign date, Karyawan yg tgl resignnya lebih lama dari tgl skrg tetap bisa minta leave
    22/01/2019 [TKG] validasi minimum OT (dalam menit)
    23/10/2019 [HAR/SIER] tambah parameter mIsOTAuthorizationByLevel validasi waktu save 
    31/10/2019 [HAR/SRN] tambah inputan special OT berdasarkan parameter OTRequestUseSpecialOT
    06/07/2020 [TKG/IMS] 
        validasi saat edit data, 
        apakah dokumen sudah diverifikasi atau belum (berdasarkan parameter IsOTVerificationEnabled)
    02/03/2022 [RIS/SIER] Menyambungkan Doc Approval Group
    15/03/2022 [TKG/GSS] merubah GetParameter dan proses save
    26/07/2022 [IBL/SIER] memunculkan tombol print ketika dipanggil dari docapproval & menu lain
    10/08/2022 [VIN/SIER] Bug approval tidak nyambung ke DAG
    11/08/2022 [SET/SIER] penyesuaian tampilan approval berdasar parameter IsTransactionUseDetailApprovalInformation
    21/09/2022 [MAU/SIER] memperbesar ukuran tab approval 

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOTRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mOTRequestType = "1";
        internal FrmOTRequestFind FrmFind;
        internal bool
            mIsFilterBySiteHR = false, mIsFilterByDeptHR = false,
            mIsSiteMandatory = false,
            mIsApprovalBySiteMandatory = false,
            mIsFilterBySite = false,
            mIsOTAuthorizationByLevel = false,
            mIsOTRequestUseSpecialOT = false,
            mIsTransactionUseDetailApprovalInformation = false;
        private bool mIsOTVerificationEnabled = false;
        private decimal mOTMinimumInMinute = 0m;
        private string mDocTitle = string.Empty;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmOTRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "OT Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo, TxtStatus }, true);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;
                if (mIsOTRequestUseSpecialOT)
                    ChkOTSpecial.Visible = true;
                else
                    ChkOTSpecial.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if (mDocTitle != "SIER") BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee's" + Environment.NewLine + "Code",           
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        
                        //6-9
                        "Start"+ Environment.NewLine + "Time",
                        "End"+ Environment.NewLine + "Time",
                        "Remark",
                        "Resign"+ Environment.NewLine + "Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        20, 100, 200, 100, 150, 
                        
                        //6-9
                        60, 60, 300, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5 }, false);
            Grd1.Cols[9].Move(6);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "No.", "Checked By", "Position", "Status", "Date", "Remark" },
                    new int[] { 100, 200, 300, 80, 80, 300 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 4 });
            Sm.GrdColInvisible(Grd2, new int[] { 4 }, false);
            if (!mIsTransactionUseDetailApprovalInformation)
                Sm.GrdColInvisible(Grd2, new int[] { 0, 2 }, false);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, ChkOTSpecial, LueDeptCode, LueSiteCode, DteDocDt, DteOTDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkOTSpecial, LueDeptCode, LueSiteCode, DteOTDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 6, 7, 8 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    ChkOTSpecial.Properties.ReadOnly = false;
                    ChkOTSpecial.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtStatus, DteDocDt, LueDeptCode, LueSiteCode, 
                DteOTDt, MeeRemark, TmeOTTm
            });
            ChkCancelInd.Checked = false;
            ChkOTSpecial.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOTRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteOTDt.DateTime = DteDocDt.DateTime;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter)
            {
                if (Grd1.CurCell.Col.Index != Grd1.Cols.Count - 1)
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index, Grd1.CurCell.Col.Index + 1);
                else
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index +1, 6);
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1 && 
                    !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                    (!mIsSiteMandatory || (mIsSiteMandatory && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                    )
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmOTRequestDlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));
                }
                if (Sm.IsGrdColSelected(new int[] { 1, 6, 7, 8 }, e.ColIndex))
                {
                    if (e.ColIndex == 6 || e.ColIndex == 7) 
                        TmeRequestEdit(Grd1, TmeOTTm, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && 
                TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                (!mIsSiteMandatory || (mIsSiteMandatory && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                )
                Sm.FormShowDialog(new FrmOTRequestDlg(this, Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode)));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.IsGrdColSelected(new int[] { 6, 7 }, e.ColIndex))
            {
                var Tm = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                for (int Row = 1; Row < Grd1.Rows.Count-1; Row++)
                    Grd1.Cells[Row, e.ColIndex].Value = Tm;
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 8 }, e);
            //if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 8) != string.Empty)
            //    {
            //        try
            //        {
            //            if (Sm.GetGrdStr(Grd1, e.RowIndex, 8).Substring(2, 1) != ":")
            //            {
            //                string awal = Sm.GetGrdStr(Grd1, e.RowIndex, 8).Substring(0, 2);
            //                string akhir = Sm.GetGrdStr(Grd1, e.RowIndex, 8).Substring(2, 2);
            //                if (IsTimeMoreBigger(Convert.ToInt32(awal), Convert.ToInt32(akhir)))
            //                {
            //                    Grd1.Cells[e.RowIndex, 8].Value = "00:00";
            //                }
            //                else
            //                {
            //                    Grd1.Cells[e.RowIndex, 8].Value = string.Empty + awal + ":" + akhir + "";
            //                }
            //            }
            //            else
            //            {
            //                string awal = Sm.GetGrdStr(Grd1, e.RowIndex, 8).Substring(0, 2);
            //                string akhir = Sm.GetGrdStr(Grd1, e.RowIndex, 8).Substring(3, 2);
            //                if (IsTimeMoreBigger(Convert.ToInt32(awal), Convert.ToInt32(akhir)))
            //                {
            //                    Grd1.Cells[e.RowIndex, 8].Value = "00:00";
            //                }
            //                else
            //                {
            //                    Grd1.Cells[e.RowIndex, 8].Value = string.Empty + awal + ":" + akhir + "";
            //                }
            //            }
            //        }
            //        catch (Exception Exc)
            //        {
            //            Sm.StdMsg(mMsgType.Warning, "Time format Not valid");
            //            Grd1.Cells[e.RowIndex, 8].Value = "00:00";
            //        }
            //    }
            //if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 9) != string.Empty)
            //{
            //    try
            //    {
            //        if (Sm.GetGrdStr(Grd1, e.RowIndex, 9).Substring(2, 1) != ":")
            //        {
            //            string awal = Sm.GetGrdStr(Grd1, e.RowIndex, 9).Substring(0, 2);
            //            string akhir = Sm.GetGrdStr(Grd1, e.RowIndex, 9).Substring(2, 2);
            //            if (IsTimeMoreBigger(Convert.ToInt32(awal), Convert.ToInt32(akhir)))
            //            {
            //                Grd1.Cells[e.RowIndex, 9].Value = "00:00";
            //            }
            //            else
            //            {
            //                Grd1.Cells[e.RowIndex, 9].Value = string.Empty + awal + ":" + akhir + "";
            //            }
            //        }
            //        else
            //        {
            //            string awal = Sm.GetGrdStr(Grd1, e.RowIndex, 9).Substring(0, 2);
            //            string akhir = Sm.GetGrdStr(Grd1, e.RowIndex, 9).Substring(3, 2);
            //            if (IsTimeMoreBigger(Convert.ToInt32(awal), Convert.ToInt32(akhir)))
            //            {
            //                Grd1.Cells[e.RowIndex, 9].Value = "00:00";
            //            }
            //            else
            //            {
            //                Grd1.Cells[e.RowIndex, 9].Value = string.Empty + awal + ":" + akhir + "";
            //            }
            //        }
            //    }
            //    catch (Exception Exc)
            //    {
            //        Sm.StdMsg(mMsgType.Warning, "Time format Not valid");
            //        Grd1.Cells[e.RowIndex, 9].Value = "00:00";
            //    }
            //}
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OTRequest", "TblOTRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOTRequest(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveOTRequestDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsDteEmpty(DteOTDt, "Over Time date") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsDtForResingeeInvalid() ||
                IsOTAuthorizationByLevel() ||
                (mOTMinimumInMinute>0m && IsOTMinimumNotValid());
        }

        private bool IsDtForResingeeInvalid()
        {
            string EmpCode = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                if (EmpCode.Length > 0)
                {
                    if (IsDocDtForResingeeInvalid(EmpCode, r)) return true;
                    if (IsOTDtForResingeeInvalid(EmpCode, r)) return true;
                }
            }
            return false;
        }

        private bool IsDocDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 9)) + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsOTDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@OTDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@OTDt", Sm.GetDte(DteOTDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 9)) + Environment.NewLine +
                    "OT Date :" + DteOTDt.Text + Environment.NewLine + Environment.NewLine +
                    "OT date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsOTRequestNotValid(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo From TblOTRequestHdr A ");
            SQL.AppendLine("Inner Join TblOTRequestDtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status<>'C' And A.OTDt=@OTDt Limit 1;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@OTDt", Sm.GetDte(DteOTDt));
            Sm.CmParamDt(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
                    "Duplicate Over Time."
                    );
                return true;
            }
            else
                return false;
        }

        private bool IsOTMinimumNotValid()
        {
            var l1 = new List<OTDuration>();
            var l2 = new List<OTDuration>();
            string 
                EmpCode = string.Empty,
                Dt = Sm.GetDte(DteOTDt).Substring(0, 8),
                Value1 = string.Empty, 
                Value2 = string.Empty;
            double Value = 0; 

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    Value1 = string.Concat(Dt, Sm.GetGrdStr(Grd1, r, 6).Replace(":", ""));
                    Value2 = string.Concat(Dt, Sm.GetGrdStr(Grd1, r, 7).Replace(":", ""));
                    Value = (Sm.ConvertDateTime(Value2) - Sm.ConvertDateTime(Value1)).TotalMinutes;
                    l1.Add(new OTDuration
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 2),
                        EmpName = Sm.GetGrdStr(Grd1, r, 3),
                        Duration = (decimal)Value
                    });
                }
            }
            foreach (var i in l1.OrderBy(o => o.EmpCode))
            {
                if (Sm.CompareStr(EmpCode, i.EmpCode))
                {
                    foreach (var j in l2.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
                        j.Duration += i.Duration;
                }
                else
                {
                    l2.Add(new OTDuration
                    {
                        EmpCode = i.EmpCode,
                        EmpName = i.EmpName,
                        Duration = i.Duration
                    });
                }
                EmpCode = i.EmpCode;
            }

            foreach (var x in l2.Where(w => w.Duration<mOTMinimumInMinute))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + x.EmpCode + Environment.NewLine +
                    "Employee's Name : " + x.EmpName + Environment.NewLine + 
                    "Minimum Overtime : " + Sm.FormatNum(mOTMinimumInMinute, 2) + " minutes" + Environment.NewLine +
                    "Total Overtime : " + Sm.FormatNum(x.Duration, 2) + " minutes" + Environment.NewLine + Environment.NewLine +
                    "Total Overtime is less than minimum overtime."
                    );
                return true;
            }

            l1.Clear();
            l2.Clear();

            return false;
        }

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private bool IsGrdValueNotValid()
        {
            int StartTm = 0, EndTm = 0;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Employee is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Start time is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "End time is empty.")
                    ) 
                    return true;
                
                if (IsOTRequestNotValid(Row)) return true;
            }
            return false;
        }

        private bool IsOTAuthorizationByLevel()
        {
            if (!mIsOTAuthorizationByLevel)
                return false;
            else
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select 1 From tblUser A "+
                        "Inner Join TblEmployee B On A.userCode = B.userCode "+
                        "Inner Join tblgradeLevelhdr C On B.GrdLvlCode = C.GrdLvlCode  "+
                        "Inner Join TblLevelhdr D On C.LevelCode = D.LevelCode " +
                        "Where A.UserCode=@UserCode And D.AllowOTRequestInd = 'Y' ;"
                };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (!Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Authorization OT Request by Level is Actived. " + Environment.NewLine +
                        "You don't have access to create OT Request. ");
                    return true;
                }
                return false;
            }
        }

        private MySqlCommand SaveOTRequest(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* OT Request */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblOTRequestHdr(DocNo, DocDt, Status, CancelInd, OTSpecialInd, DeptCode, SiteCode, OTDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @OTSpecialInd, @DeptCode, @SiteCode, @OTDt, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='OTRequest' ");
            if (mIsApprovalBySiteMandatory)
                SQL.AppendLine("And IfNull(SiteCode, '')=@SiteCode ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And (DAGCode Is Null Or ");
            SQL.AppendLine("(DAGCode Is Not Null ");
            SQL.AppendLine("And DAGCode In ( ");
            SQL.AppendLine("    Select A.DAGCode ");
            SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
            SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
            SQL.AppendLine("    And A.ActInd='Y' ");
            SQL.AppendLine("    And B.EmpCode=(Select EmpCode From TblEmployee Where UserCode = @UserCode) ");
            SQL.AppendLine("))); ");

            SQL.AppendLine("Update TblOTRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='OTRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblOTRequestDtl(DocNo, DNo, EmpCode, OTStartTm, OTEndTm, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @EmpCode_" + r.ToString() +
                        ", @OTStartTm_" + r.ToString() +
                        ", @OTEndTm_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@OTStartTm_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6).Replace(":", ""));
                    Sm.CmParam<String>(ref cm, "@OTEndTm_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7).Replace(":", ""));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");


            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParamDt(ref cm, "@OTDt", Sm.GetDte(DteOTDt));
            Sm.CmParam<String>(ref cm, "@OTSpecialInd", ChkOTSpecial.Checked == true ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveOTRequestDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblOTRequestDtl(DocNo, DNo, EmpCode, OTStartTm, OTEndTm, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @EmpCode, @OTStartTm, @OTEndTm, @Remark, @UserCode, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@OTStartTm", Sm.GetGrdStr(Grd1, Row, 6).Replace(":", ""));
        //    Sm.CmParam<String>(ref cm, "@OTEndTm", Sm.GetGrdStr(Grd1, Row, 7).Replace(":", ""));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}   

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOTRequestDtl());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                IsDataAlreadyCancelled() ||
                (mIsOTVerificationEnabled && IsOTRequestAlreadyVerified());
        }

        private bool IsOTRequestAlreadyVerified()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblOTVerificationHdr A, TblOTVerificationdtl B  ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Status In ('O', 'A') ");
            SQL.AppendLine("And B.OTRequestDocNo=@Param Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text, "This OT document already verified.");
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblOTRequestHdr ");
            SQL.AppendLine("Where DocNo=@Param ");
            SQL.AppendLine("And (CancelInd='Y' Or Status='C') Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text, "This document has been cancelled.");
        }

        private MySqlCommand EditOTRequestDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOTRequestHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, OTSpecialInd=@OTSpecialInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@OTSpecialInd", ChkOTSpecial.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOTRequestHdr(DocNo);
                ShowOTRequestDtl(DocNo);
                Sm.ShowDocApproval(DocNo, "OTRequest", ref Grd2, mIsTransactionUseDetailApprovalInformation);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowOTRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("CancelInd, OTSpecialInd, DeptCode, SiteCode, OTDt, Remark ");
            SQL.AppendLine("From TblOTRequestHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DocNo", 
                       
                       //1-5
                       "DocDt", "StatusDesc", "CancelInd", "OTSpecialInd", "DeptCode",  
                       
                       //6-8
                       "SiteCode", "OtDt", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3])=="Y";
                        ChkOTSpecial.Checked = Sm.DrStr(dr, c[4]) == "Y";
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), string.Empty);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[6]), string.Empty);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteOTDt, Sm.DrStr(dr, c[7]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
            );
        }

        private void ShowOTRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, B.ResignDt, A.OTStartTm, A.OTEndTm, A.Remark ");
            SQL.AppendLine("From TblOTRequestDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B on A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblPosition C On B.PosCode = C.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    
                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "OTStartTm", 
                    
                    //6-8
                    "OTEndTm", "Remark", "ResignDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='OTRequest' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'OTRequestType', 'IsFilterBySite', 'OTMinimumInMinute', 'IsSiteMandatory', 'IsApprovalBySiteMandatory', ");
            SQL.AppendLine("'IsFilterBySiteHR', 'IsFilterByDeptHR', 'IsOTAuthorizationByLevel', 'IsOTRequestUseSpecialOT', 'IsOTVerificationEnabled', ");
            SQL.AppendLine("'DocTitle', 'IsTransactionUseDetailApprovalInformation');");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsOTVerificationEnabled": mIsOTVerificationEnabled = ParValue == "Y"; break;
                            case "IsOTRequestUseSpecialOT'": mIsOTRequestUseSpecialOT = ParValue == "Y"; break;
                            case "IsOTAuthorizationByLevel": mIsOTAuthorizationByLevel = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsApprovalBySiteMandatory": mIsApprovalBySiteMandatory = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;

                            //string
                            case "OTRequestType": mOTRequestType = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;

                            //decimal
                            case "OTMinimumInMinute": if (ParValue.Length>0) mOTMinimumInMinute = decimal.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void TmeRequestEdit(iGrid Grd, DevExpress.XtraEditors.TimeEdit Tme, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Tme.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Tme.EditValue = null;
            else
                Sm.SetTme(Tme, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Replace(":", ""));

            Tme.Visible = true;
            Tme.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void TmeOTTm_Leave(object sender, EventArgs e)
        {
            if (TmeOTTm.Visible && fAccept && 
                Sm.IsGrdColSelected(new int[] { 6, 7 }, fCell.ColIndex)
                )
            {
                var Tm = Sm.GetTme(TmeOTTm);
                if (Tm.Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                {
                    if (Tm.Length == 4) Tm = Sm.Left(Tm, 2) + ":" + Sm.Right(Tm, 2);
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Tm;
                }
                TmeOTTm.Visible = false;
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd();
            }
        }

        #endregion

        #endregion

        #region Event

        private class OTDuration
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public decimal Duration { get; set; }
        }

        #endregion
    }
}
