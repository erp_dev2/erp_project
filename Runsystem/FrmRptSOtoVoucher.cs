﻿#region Update
/*
    07/11/2017 [HAR] bug Fixing jenis trx (CBD indicator), ambil dari DR . 
    06/09/2022 [HPH] kolom paid ambil dari tblvoucherhdr, sebelumnya dari TblsalesInvoiceSummary pada List SI process 4
*/
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSOtoVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptSOtoVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);

                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
               
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 49;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "SO#", 
                        "DNo",
                        "Date",
                        "Type",
                        "Customer",
                        //6-10
                        "Item Code",
                        "",
                        "Local Code",
                        "Item Name",
                        "SO Quantity"+Environment.NewLine+"packaging",
                        //11-15
                        "SO Uom"+Environment.NewLine+"Packaging",
                        "SO Quantity",
                        "SO Uom",
                        "Quotation",
                        "Quotation Dno",
                        //16-20
                        "", 
                        "Unit"+Environment.NewLine+"Price",
                        "Discount",
                        "Discount"+Environment.NewLine+"Amount",
                        "Price"+Environment.NewLine+"After Disc",
                        //21-25
                        "Promo Rate", 
                        "Price Before",
                        "Tax Rate",
                        "Tax Amount",
                        "Price"+Environment.NewLine+"After tax",
                        //26-30
                        "Total Amount",
                        "DR Document",
                        "DR Date",
                        "DR Quantity"+Environment.NewLine+"Packaging",
                        "DR Quantity"+Environment.NewLine+"Sales",
                        //31-35
                        "DR Quantity"+Environment.NewLine+"Inventory",
                        "DO Document",
                        "DO Date",
                        "DO Quantity"+Environment.NewLine+"Packaging",
                        "",
                        //36-40
                        "Sales Invoice",
                        "Sales Invoice Dno",
                        "Sales Invoice"+Environment.NewLine+"Qty Packaging",
                        "Sales Invoice"+Environment.NewLine+"Quantity",
                        "Sales Invoice"+Environment.NewLine+"UPrice Before Tax",
                        //41-45
                        "Sales Invoice"+Environment.NewLine+"taxRate",
                        "Sales Invoice"+Environment.NewLine+"taxAmt",
                        "Sales Invoice"+Environment.NewLine+"UPriceAftertax",
                        "Amount Item",
                        "Additional",
                        //46-48
                        "Paid",
                        "Voucher Request",
                        "Voucher"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        150, 40, 100, 100, 250, 
                        //6-10
                        100, 20, 100, 250, 100, 
                        //11-15
                        100, 100, 100, 130, 100, 
                        //16-20
                        20, 100, 100, 100, 100, 
                        //21-25
                        100, 100, 100, 100, 100,
                        //26-30
                        100, 150, 100, 100, 100, 
                        //31-35
                        100, 150, 100, 100, 20,
                        //36-40
                        150, 100, 100, 100, 100,
                        //41-45
                        100, 100, 100, 100, 100,
                        //46-47
                        100, 150, 150
                        
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 20, 19, 18, 17, 16, 14, 13, 12, 11, 10, 9, 5, 4, 3, 1, 0 });
            Sm.GrdColButton(Grd1, new int[] { 7, 16, 35 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 29, 30, 31, 34, 38,
            39, 40, 41, 42, 43, 44, 45, 46}, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 28, 33  });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 8, 15, 16, 21, 35, 37,   }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            var LSI = new List<L1>();

            try
            {
            if (
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
                Process1(ref LSI);
                if (LSI.Count > 0)
                {
                    Process2(ref LSI);
                    Process3(ref LSI);
                    Process4(ref LSI);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                var f = new FrmCtQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }
        }
        

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        //list SO
        private void Process1(ref List<L1> LP1)
        {
            LP1.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = "And 0=0";

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.SODocNo", false);

            SQL.AppendLine("Select 'SO' TypeTrx, T2.CtName, T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt)*Qty As Total   ");
            SQL.AppendLine("From (   ");
            SQL.AppendLine("   Select A.CtCode, A.DocNo SODocNo, B.Dno SODno, A.DocDt SODocDt, E.ItCode, G.ItName, G.ItCodeInternal, ");
            SQL.AppendLine("   B.QtyPackagingUnit, B.PackagingUnitUomCode, B.Qty, D.PriceUomCode, A.CurCode, A.CtQtDocNo As CtQtDocNo, B.CtQtDNo,  "); 
            SQL.AppendLine("   E.UPrice, C.Discount, ifnull((E.UPrice*C.Discount *0.01), 0) As DiscountAmt,   ");
            SQL.AppendLine("   C.UPrice As UPriceAfterDiscount,   ");
            SQL.AppendLine("   IfNull(F.DiscRate, 0) As PromoRate,    ");
            SQL.AppendLine("   (C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0))) As UPriceBefore,   ");
            SQL.AppendLine("   B.TaxRate,    ");
            SQL.AppendLine("   ((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate) As TaxAmt   ");  
            SQL.AppendLine("   From TblSOHdr A     ");
            SQL.AppendLine("   Inner Join TblSODtl B On A.DocNo=B.DocNo    ");
            SQL.AppendLine("   Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo   ");
            SQL.AppendLine("   Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo   ");
            SQL.AppendLine("   Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo   ");
            SQL.AppendLine("   Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode   "); 
            SQL.AppendLine("   Inner Join TblItem G On E.ItCode=G.ItCode   ");
            SQL.AppendLine("   Where A.cancelInd = 'N' ");
            SQL.AppendLine(") T   ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T.CtCode = T2.CtCode  ");
            SQL.AppendLine("Where T.SODocDt Between @StartDt And @EndDt ");

            //SQL.AppendLine("Union  All  ");
            ////POS
            //SQL.AppendLine("Select 'POS', (Select Ctname From tblCustomer ");
            //SQL.AppendLine("Where CtCode = (Select parvalue From tblparameter Where parcode = 'StoreCtCode')) As Ctname, ");
            //SQL.AppendLine("(Select parvalue From tblparameter Where parcode = 'StoreCtCode') As CtCode,   ");
            //SQL.AppendLine("Concat(A.PosNo, '-', A.TrnNo, '-', A.BsDate) TrnNo, B.DtlNo, A.BsDate, B.ItCode, C.ItName, B.Qty, C.ItCodeInternal, C.SalesUomCode,  ");
            //SQL.AppendLine(" B.Qty, C.SalesUomCode, D.SetValue As Cur, null, null,  B.UPrice, round((B.DiscAmt*-100/B.UPrice), 0) As Disc, B.DiscAmt, (B.UPrice+B.DiscAmt) PriceAfterDisc,  ");
            //SQL.AppendLine("0 promorate,(B.UPrice+B.DiscAmt) pricebeforetax, 0 taxrate,  B.Tax1Amt,  ");
            //SQL.AppendLine("(B.UPrice+B.DiscAmt)+B.Tax1Amt As Priceaftertax, ");
            //SQL.AppendLine("(B.Qty*(B.UPrice+ifnull(B.DiscAmt,0)))+ifnull(B.Tax1Amt,0)+ifnull(B.Tax2Amt,0)+ifnull(B.Tax3Amt,0) As total ");
            //SQL.AppendLine("From TblPosTrnHdr A  ");
            //SQL.AppendLine("Inner Join TblPosTrnDtl B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo  ");
            //SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            //SQL.AppendLine("Left Join TblPosSetting D On A.PosNo=D.PosNo And D.SetCode='LocalCur'  ");
            //SQL.AppendLine("Left Join TblPosSetting E On A.PosNo=E.PosNo And E.SetCode='StoreWhsCode'  ");
            //SQL.AppendLine("Order By A.PosNo, A.BsDate, A.TrnNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString()+ Filter +" Order By T.SODocNo, T.SODno";

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SODOcNo",  
                    //1-5
                    "SODno", "SODocDt", "CtCode", "CtName",  "ItCode",
                    //6-10
                    "ItName", "ItCodeInternal", "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", 
                    //11-15
                    "PriceUomCode", "CurCode", "CtQtDocNo", "CtQtDno", "Uprice", 
                    //16-20
                    "Discount", "DiscountAmt", "UpriceAfterDiscount", "PromoRate", "UPriceBefore", 
                    //21-25
                    "TaxRate", "TaxAmt", "UPriceAfterTax", "Total", "TypeTrx"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LP1.Add(new L1()
                        {
                            SODocNo = Sm.DrStr(dr, c[0]),
                            SODNo = Sm.DrStr(dr, c[1]),
                            SODate = Sm.DrStr(dr, c[2]),
                            Customer = Sm.DrStr(dr, c[4]),
                            ItemCode = Sm.DrStr(dr, c[5]),
                            ItemName = Sm.DrStr(dr, c[6]),
                            LocalCode = Sm.DrStr(dr, c[7]),
                            SOQtyPackaging = Sm.DrDec(dr, c[8]),
                            SOUomPackaging = Sm.DrStr(dr, c[9]),
                            SOQty = Sm.DrDec(dr, c[10]),
                            SOUom= Sm.DrStr(dr, c[11]),
                            Quotation= Sm.DrStr(dr, c[13]),
                            QuotationDno= Sm.DrStr(dr, c[14]),
                            UnitPrice= Sm.DrDec(dr, c[15]),
                            Disc= Sm.DrDec(dr, c[16]),
                            DiscAmt= Sm.DrDec(dr, c[17]),
                            PriceAfDisc= Sm.DrDec(dr, c[18]),
                            PromoRate= Sm.DrDec(dr, c[19]),
                            PriceBefore= Sm.DrDec(dr, c[20]),
                            TaxRate= Sm.DrDec(dr, c[21]),
                            TaxAmount= Sm.DrDec(dr, c[22]),
                            PriceAfTax= Sm.DrDec(dr, c[23]),
                            TotalAmt= Sm.DrDec(dr, c[24]),
                            TypeTrx = Sm.DrStr(dr, c[25]),
                        });
                    }
                }
                dr.Close();
            }
        }

        //List DR
        private void Process2(ref List<L1> LP1)
        {
            int Temp = 0;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select B.SODocNo, B.SoDno, A.DocNo DRDocno, B.Dno DRDNo, A.DocDt As DRDate, B.QtyPackagingUnit, B.Qty, B.QtyInventory, if(A.CBDInd='Y', 'CBD', 'TOP') As TypeTrx ");
            SQL.AppendLine("From TblDrHdr A ");
            SQL.AppendLine("Inner Join TBlDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.cancelInd = 'N' ");
            SQL.AppendLine("Order BY B.SODocNo, B.SODno ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SODocNo",
                    //5-10
                    "SODNo", "DRDocNo", "DRDno", "DRDate", "QtyPackagingUnit",
                    //6-8
                    "Qty", "QtyInventory", "TypeTrx"
                });
                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (string.Compare(LP1[i].SODocNo, dr.GetString(0)) == 0 && string.Compare(LP1[i].SODNo, dr.GetString(1)) == 0)
                            {
                                LP1[i].DRDoc = dr.GetString(c[2]);
                                LP1[i].DRDate = dr.GetString(c[4]);
                                LP1[i].DRQtyPackaging = dr.GetDecimal(c[5]);
                                LP1[i].DRQtySales = dr.GetDecimal(c[6]);
                                LP1[i].DRQtyInv = dr.GetDecimal(c[7]);
                                LP1[i].TypeTrx = dr.GetString(c[8]);

                                LP1[i].SODocNo = LP1[i].SODocNo;
                                LP1[i].SODNo = LP1[i].SODNo;
                                LP1[i].SODate = LP1[i].SODate;
                                LP1[i].Customer = LP1[i].Customer;
                                LP1[i].ItemCode = LP1[i].ItemCode;
                                LP1[i].ItemName = LP1[i].ItemName;
                                LP1[i].LocalCode = LP1[i].LocalCode;
                                LP1[i].SOQtyPackaging = LP1[i].SOQtyPackaging;
                                LP1[i].SOUomPackaging = LP1[i].SOUomPackaging;
                                LP1[i].SOQty = LP1[i].SOQty;
                                LP1[i].SOUom=LP1[i].SOUom;
                                LP1[i].Quotation= LP1[i].Quotation;
                                LP1[i].QuotationDno= LP1[i].QuotationDno;
                                LP1[i].UnitPrice= LP1[i].UnitPrice;
                                LP1[i].Disc= LP1[i].Disc;
                                LP1[i].DiscAmt= LP1[i].DiscAmt;
                                LP1[i].PriceAfDisc= LP1[i].PriceAfDisc;
                                LP1[i].PromoRate = LP1[i].PromoRate;
                                LP1[i].PriceBefore= LP1[i].PriceBefore;
                                LP1[i].TaxRate= LP1[i].TaxRate;
                                LP1[i].TaxAmount= LP1[i].TaxAmount;
                                LP1[i].PriceAfTax= LP1[i].PriceAfTax;
                                LP1[i].TotalAmt= LP1[i].TotalAmt;
                                Temp = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //List DO
        private void Process3(ref List<L1> LP1)
        {
            int Temp = 0;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            SQL.AppendLine("Select X.SODocNO, X.SoDNo, X.DODocNo, X.DODno, ifnull(X.DODate, '-') DODate, X.QtyDO ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As DR, C.SODocNo, C.SODNo, A.DocNo As DODocNo, B.Dno As DODno, A.DocDt DODate,  Sum(B.Qty) As QtyDO ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Left Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("    Group By C.SODocNo, C.SODNo, A.DocNo, B.Dno, A.DocDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As PL, C.SODocNo, C.SODNo, A.DocNo, B.Dno, A.DocDt DODate, Sum(B.Qty) As QtyDO  ");
            SQL.AppendLine("    From TblDOCt2Hdr A  ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PlDno = C.Dno  ");
            SQL.AppendLine("    Left Join TblPlHdr D On C.DocNo = D.Docno   ");
            SQL.AppendLine("    Left Join TblSIHdr E On D.SiDocno = E.DocNo   ");
            SQL.AppendLine("    Left Join TblSP F On E.SpDocNo = F.DocNo And F.Status <> 'C'   ");
            SQL.AppendLine("    Group By C.SODocNo, C.SODNo, A.DocNo, B.Dno, A.DocDt ");
            SQL.AppendLine(") X  ");
            


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + "  ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SODocNo",  

                    //1-5
                    "SODno", "DODocNo", "DODno", "DODate", "QtyDO"
                });
                if (dr.HasRows)
                {
                    int r = 0;

                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (LP1[i].SODocNo == dr.GetString(0) && LP1[i].SODNo == dr.GetString(1))
                            {
                                LP1[i].DODoc = dr.GetString(c[2]);
                                LP1[i].DODate = dr.GetString(c[4]);
                                LP1[i].DOQtyPackaging = dr.GetDecimal(c[5]);

                                LP1[i].DRDoc = LP1[i].DRDoc;
                                LP1[i].DRDate = LP1[i].DRDate;
                                LP1[i].DRQtyPackaging = LP1[i].DRQtyPackaging;
                                LP1[i].DRQtySales = LP1[i].DRQtySales;
                                LP1[i].DRQtyInv = LP1[i].DRQtyInv;
                                LP1[i].SODocNo = LP1[i].SODocNo;
                                LP1[i].SODNo = LP1[i].SODNo;
                                LP1[i].SODate = LP1[i].SODate;
                                LP1[i].Customer = LP1[i].Customer;
                                LP1[i].ItemCode = LP1[i].ItemCode;
                                LP1[i].ItemName = LP1[i].ItemName;
                                LP1[i].LocalCode = LP1[i].LocalCode;
                                LP1[i].SOQtyPackaging = LP1[i].SOQtyPackaging;
                                LP1[i].SOUomPackaging = LP1[i].SOUomPackaging;
                                LP1[i].SOQty = LP1[i].SOQty;
                                LP1[i].SOUom = LP1[i].SOUom;
                                LP1[i].Quotation = LP1[i].Quotation;
                                LP1[i].QuotationDno = LP1[i].QuotationDno;
                                LP1[i].UnitPrice = LP1[i].UnitPrice;
                                LP1[i].Disc = LP1[i].Disc;
                                LP1[i].DiscAmt = LP1[i].DiscAmt;
                                LP1[i].PriceAfDisc = LP1[i].PriceAfDisc;
                                LP1[i].PromoRate = LP1[i].PromoRate;
                                LP1[i].PriceBefore = LP1[i].PriceBefore;
                                LP1[i].TaxRate = LP1[i].TaxRate;
                                LP1[i].TaxAmount = LP1[i].TaxAmount;
                                LP1[i].PriceAfTax = LP1[i].PriceAfTax;
                                LP1[i].TotalAmt = LP1[i].TotalAmt;
                                LP1[i].TypeTrx = LP1[i].TypeTrx;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //List SI
        private void Process4(ref List<L1> LP1)
        {
            int Temp = 0;
            
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = string.Empty;

            SQL.AppendLine("Select X.SoDocNO, X.SODNo, X.TypeTrx, X.SIDocNo, X.SIDno, ");
            SQL.AppendLine("X.QtyPackagingUnit, X.Qty, X.UPriceBeforeTax, X.taxRate, X.taxAmt, X.UPriceAftertax, ");
            SQL.AppendLine("ifnull(X2.Amt1, 0) AmtItem, ifnull(X3.Amt1, 0) As Additional, ");
            SQL.AppendLine("ifnull(X4.Amt, 0) PaidItem, ifnull(X3.Amt2, 0) As PaidAdditional, ifnull(X.VRDocNo, '-') VRDocno, ifnull(X.VCDocno, '-') VCDocno ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select B.DOctDocNo As SoDocNO, B.DocTDno As SODno, 'TOP' As TypeTrx, A.DocNo As SIDocNo, B.Dno As SIDno,  ");
            SQL.AppendLine("    B.QtyPackagingUnit, B.Qty, B.UPriceBeforeTax, B.taxRate, B.taxAmt, B.UPriceAftertax, C.VRDocNo, C.VCDocno ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Inner Join Tblsalesinvoicedtl B On A.DocNo = B.DocnO ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select B.InvoiceDocNo, A.VoucherRequestDocNo As VRDocNo, C.VoucherDocno As VCDocNO ");
	        SQL.AppendLine("        from tblincomingpaymenthdr A ");
	        SQL.AppendLine("        inner Join TblIncomingpaymentDtl B On A.DocNo =B.DocNo ");
	        SQL.AppendLine("        left Join TblVoucherRequesthdr C On A.VoucherRequestDocNo = C.DocNo ");
	        SQL.AppendLine("        Where A.cancelInd = 'N' And C.CancelInd = 'N' ");
            SQL.AppendLine("    )C On A.Docno = C.InvoiceDocNo ");
            SQL.AppendLine("    Where A.CBDInd = 'Y' And A.cancelInd = 'N' And A.SODocNo is not null  ");
            SQL.AppendLine("    Union all ");
            SQL.AppendLine("    Select E.SODocNo, E.SODNo, 'CBD' As TypeTrx, A.DocNo, B.Dno,   ");
            SQL.AppendLine("    B.QtyPackagingUnit, B.Qty, B.UPriceBeforeTax, B.taxRate, B.taxAmt, B.UPriceAftertax, F.VRDocNo, F.VCDocno ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A ");
            SQL.AppendLine("    Inner Join Tblsalesinvoicedtl B On A.DocNo = B.DocnO ");
            SQL.AppendLine("    Inner JOin TblDOCt2Dtl2 C On B.DOCtDocNO = C.DocNO And B.DOCtDno = C.DNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr D On B.DOCtDocNO = D.DocNo   ");
            SQL.AppendLine("    Inner Join TblDrDtl E On D.DRDocNo = E.DocNo And C.DrDno = E.Dno ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select B.InvoiceDocNo, A.VoucherRequestDocNo As VRDocNo, C.VoucherDocno As VCDocNO ");
            SQL.AppendLine("        from tblincomingpaymenthdr A ");
            SQL.AppendLine("        inner Join TblIncomingpaymentDtl B On A.DocNo =B.DocNo ");
            SQL.AppendLine("        left Join TblVoucherRequesthdr C On A.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("        Where A.cancelInd = 'N' And C.CancelInd = 'N' ");
            SQL.AppendLine("    )F On A.Docno = F.InvoiceDocNo ");
            SQL.AppendLine("    Where A.CBDInd = 'N' And A.CancelInd = 'N' And A.SODocNo is null ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Left Join TblsalesInvoiceSummary X2 On X.SiDOcno = X2.DocNo And X.SIDno = X2.Dno And X2.Doctype ='1' ");
            SQL.AppendLine("Left Join TblsalesInvoiceSummary X3 On X.SiDOcno = X3.DocNo And X.SIDno = X3.Dno And X3.Doctype ='2' ");
            SQL.AppendLine("Left Join tblvoucherhdr X4 On X.VCDocno = X4.DocNo  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "SODocNo",  
                    //1-5
                    "SODno", "TypeTrx", "SIDOcno", "SIDno", "QtyPackagingUnit", 
                    //6-10
                    "Qty", "UPriceBeforeTax", "taxRate", "taxAmt", "UPriceAftertax",
                    //11-15
                    "AmtItem", "Additional", "PaidItem", "VRDocNo", "VCDocNo"
                });
                if (dr.HasRows)
                {
                    int r = 0;

                    while (dr.Read())
                    {
                        for (var i = Temp; i < LP1.Count; i++)
                        {
                            if (LP1[i].SODocNo == dr.GetString(0) && LP1[i].SODNo == dr.GetString(1))
                            {
                                r = r + 1;
                                Grd1.Rows.Add();
                                int Row = Grd1.Rows.Count - 1;
                                Grd1.Cells[Row, 0].Value = r + 1;
                                Grd1.Cells[Row, 1].Value = LP1[i].SODocNo;
                                Grd1.Cells[Row, 2].Value = LP1[i].SODNo;
                                Grd1.Cells[Row, 3].Value = Sm.SetGrdDate(LP1[i].SODate);
                                Grd1.Cells[Row, 4].Value = LP1[i].TypeTrx; // dr.GetString(c[2]);
                                Grd1.Cells[Row, 5].Value = LP1[i].Customer;
                       
                                Grd1.Cells[Row, 6].Value = LP1[i].ItemCode;
                                Grd1.Cells[Row, 8].Value = LP1[i].LocalCode;
                                Grd1.Cells[Row, 9].Value = LP1[i].ItemName;
                                Grd1.Cells[Row, 10].Value = LP1[i].SOQtyPackaging;
                       
                                Grd1.Cells[Row, 11].Value = LP1[i].SOUomPackaging;
                                Grd1.Cells[Row, 12].Value = LP1[i].SOQty;
                                Grd1.Cells[Row, 13].Value = LP1[i].SOUom;
                                Grd1.Cells[Row, 14].Value = LP1[i].Quotation;
                                Grd1.Cells[Row, 15].Value = LP1[i].QuotationDno;
                       
                                Grd1.Cells[Row, 17].Value = LP1[i].UnitPrice;
                                Grd1.Cells[Row, 18].Value = LP1[i].Disc;
                                Grd1.Cells[Row, 19].Value = LP1[i].DiscAmt;
                                Grd1.Cells[Row, 20].Value = LP1[i].PriceAfDisc;

                                Grd1.Cells[Row, 21].Value = LP1[i].PromoRate;
                                Grd1.Cells[Row, 22].Value = LP1[i].PriceBefore;
                                Grd1.Cells[Row, 23].Value = LP1[i].TaxRate;
                                Grd1.Cells[Row, 24].Value = LP1[i].TaxAmount;
                                Grd1.Cells[Row, 25].Value = LP1[i].PriceAfTax;
                        
                                Grd1.Cells[Row, 26].Value = LP1[i].TotalAmt;
                                Grd1.Cells[Row, 27].Value = LP1[i].DRDoc;
                                if (LP1[i].DRDate != null)
                                Grd1.Cells[Row, 28].Value = Sm.SetGrdDate(LP1[i].DRDate);
                                Grd1.Cells[Row, 29].Value = LP1[i].DRQtyPackaging;
                                Grd1.Cells[Row, 30].Value = LP1[i].DRQtySales;
                        
                                Grd1.Cells[Row, 31].Value = LP1[i].DRQtyInv;
                                Grd1.Cells[Row, 32].Value = LP1[i].DODoc;
                                if (LP1[i].DODate != null)
                                Grd1.Cells[Row, 33].Value = Sm.SetGrdDate(LP1[i].DODate);
                                Grd1.Cells[Row, 34].Value = LP1[i].DOQtyPackaging;

                               
                                Grd1.Cells[Row, 36].Value = dr.GetString(c[3]);
                                Grd1.Cells[Row, 37].Value = dr.GetString(c[4]);
                                Grd1.Cells[Row, 38].Value = dr.GetDecimal(c[5]);
                                Grd1.Cells[Row, 39].Value = dr.GetDecimal(c[6]);
                                Grd1.Cells[Row, 40].Value = dr.GetDecimal(c[7]);

                                Grd1.Cells[Row, 41].Value = dr.GetDecimal(c[8]);
                                Grd1.Cells[Row, 42].Value = dr.GetDecimal(c[9]);
                                Grd1.Cells[Row, 43].Value = dr.GetDecimal(c[10]);
                                Grd1.Cells[Row, 44].Value = dr.GetDecimal(c[11]);
                                Grd1.Cells[Row, 45].Value = dr.GetDecimal(c[12]);

                                Grd1.Cells[Row, 46].Value = dr.GetDecimal(c[13]);
                                Grd1.Cells[Row, 47].Value = dr.GetString(c[14]);
                                Grd1.Cells[Row, 48].Value = dr.GetString(c[15]);
                                break;
                            }
                        }
                    }
                    Grd1.GroupObject.Add(1);
                    Grd1.Group();
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 29, 30, 31, 34, 38,
            39, 40, 41, 42, 43, 44, 45, 46 });
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion

        #region Class

        private class L1
        {
            #region SO
            public string SODocNo { get; set; }
            public string SODNo{ get; set; }
            public string SODate{ get; set; }
            public string TypeTrx { get; set; } 
            public string Customer{ get; set; }
            public string ItemCode{ get; set; }
            public string LocalCode{ get; set; }
            public string ItemName{ get; set; }
            public decimal SOQtyPackaging{ get; set; }
            public string SOUomPackaging{ get; set; }
            public decimal SOQty{ get; set; }
            public string SOUom{ get; set; }
            public string Quotation{ get; set; }
            public string QuotationDno{ get; set; }
            public decimal UnitPrice{ get; set; }
            public decimal Disc{ get; set; }
            public decimal DiscAmt{ get; set; }
            public decimal PriceAfDisc{ get; set; }
            public decimal PromoRate{ get; set; }
            public decimal PriceBefore{ get; set; }
            public decimal TaxRate{ get; set; }
            public decimal TaxAmount{ get; set; }
            public decimal PriceAfTax{ get; set; }
            public decimal TotalAmt{ get; set; }
            #endregion

            #region DR
            public string DRDoc{ get; set; }
            public string DRDate{ get; set; }
            public decimal DRQtyPackaging{ get; set; }
            public decimal DRQtySales{ get; set; }
            public decimal DRQtyInv{ get; set; }
            #endregion

            #region DO
            public string DODoc { get; set; }
            public string DODate{ get; set; }
            public decimal DOQtyPackaging { get; set; }
            #endregion

            #region SI
            public string SIDocNo{ get; set; }
            public string SIDno{ get; set; }
            public string SIQtyPackagingUnit{ get; set; }
            public decimal SIQty{ get; set; }
            public decimal SIUPriceBeforeTax { get; set; }
            public decimal SItaxRate { get; set; }
            public decimal SItaxAmt { get; set; }
            public decimal SIUPriceAftertax{ get; set; }
            public decimal AmtItem { get; set; }
            public decimal Additional{ get; set; }
            public decimal Paid { get; set; }
            #endregion

            #region VR, VC
            public string VRDocNo { get; set; }
            public string VCDocNo { get; set; }
            #endregion
        }

        #endregion      

    }
}
