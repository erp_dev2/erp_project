﻿#region Update
/*
    30/05/2017 [HAR] query LocationCode, LocationName diubah menjadi LocCode, LocName
    08/08/2017 [ARI] (saat insert location & find) Berdasar kan Setingan Grup SITE
    30/08/2017 [WED] tambah insert ke location summary
    31/01/2018 [WED] tambah field informasi Display Name
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTO : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "" ;
        internal string mAssetCode = string.Empty; 
        internal FrmTOFind FrmFind;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmTO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Technical Object";

            try
            {

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
                SetFormControl(mState.View);
                SetLueStatusCode(ref LueStatus);
                SetLueInfo(ref LueInfo);

                TpgTO.SelectTab(0);
                Sl.SetLueUomCode(ref LueWeightUomCode);

                TpgTO.SelectTab(1);
                SetLueLocationCode(ref LueLocationCode);

                TpgTO.SelectTab(2);
                Sl.SetLueCCCode(ref LueCCCode);

                TpgTO.SelectTab(3);
                Sl.SetLueUomCode(ref LueCapacityUom);

                SetGrd();

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mAssetCode.Length != 0)
                {
                    ShowData(mAssetCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    TpgTO.SelectTab("TpgGeneral");
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {

            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 3;
            
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "No",

                        //1-4
                        "",
                        "Equipment Code",
                        "Description"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-4
                        20, 100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAssetCode, TxtAssetName, TxtAssetCtCode,LueStatus, DteStart, DteEnd, LueInfo,
                        TxtClass, TxtObjectType, TxtAuthorize, TxtWeight, LueWeightUomCode, TxtSize,
                        TxtInventoryNo, DteStartUpDt, TxtShift, TxtAcquisition, DteAcquisitionDt,
                        TxtManufacture, TxtModel,TxtPartNo, TxtSerial, TxtCnt, TxtConstYr, TxtConstMth,
                        LueLocationCode, TxtCompany, TxtArea, TxtAsset, LueCCCode, TxtWbs, TxtPlanningPlant,
                        TxtPlannerGroup, TxtWorkcenter, TxtCatalog, TxtFunctional, MeeDesc, TxtSuperorder,
                        MeeDesc, TxtPosition, MeeDesc2, TxtTechnical, TxtConsType, TxtCapacity, LueCapacityUom,
                        MeeRemark, TxtDisplayName
                    }, true);
                    BtnAsset.Enabled = false;
                    TxtAssetCode.Focus();
                    ChkActInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteStart, DteEnd,
                        TxtClass, TxtObjectType, TxtAuthorize, TxtWeight, LueWeightUomCode, TxtSize,
                        TxtInventoryNo, DteStartUpDt, TxtShift, LueStatus, LueInfo,
                        TxtManufacture, TxtModel,TxtPartNo, TxtSerial, TxtCnt, TxtConstYr, TxtConstMth,
                        LueLocationCode, TxtCompany, TxtArea, TxtAsset, LueCCCode, TxtWbs, TxtPlanningPlant,
                        TxtPlannerGroup, TxtWorkcenter, TxtCatalog, TxtFunctional, MeeDesc, TxtSuperorder,
                        MeeDesc, TxtPosition, MeeDesc2, TxtTechnical, TxtConsType, TxtCapacity, LueCapacityUom,
                        MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtAssetCode.Focus();
                    BtnAsset.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteStart, DteEnd, LueStatus, LueInfo,
                        TxtClass, TxtObjectType, TxtAuthorize, TxtWeight, LueWeightUomCode, TxtSize,
                        TxtInventoryNo, DteStartUpDt, TxtShift, LueStatus,
                        TxtManufacture, TxtModel,TxtPartNo, TxtSerial, TxtCnt, TxtConstYr, TxtConstMth,
                        LueLocationCode, TxtCompany, TxtArea, TxtAsset, LueCCCode, TxtWbs, TxtPlanningPlant,
                        TxtPlannerGroup, TxtWorkcenter, TxtCatalog, TxtFunctional, MeeDesc, TxtSuperorder,
                        MeeDesc, TxtPosition, MeeDesc2, TxtTechnical, TxtConsType, TxtCapacity, LueCapacityUom, MeeRemark     
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteStart.Focus();
                    ChkActInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAssetCode, TxtAssetName,  TxtAssetCtCode, LueStatus, DteStart, DteEnd,
                TxtClass, TxtObjectType, TxtAuthorize,  LueWeightUomCode, LueInfo,
                TxtInventoryNo, DteStartUpDt, TxtShift, TxtAcquisition, DteAcquisitionDt,
                TxtManufacture, TxtModel,TxtPartNo, TxtSerial, TxtCnt, TxtConstYr, TxtConstMth,
                LueLocationCode, TxtCompany, TxtArea, TxtAsset, LueCCCode, TxtWbs, TxtPlanningPlant,
                TxtPlannerGroup, TxtWorkcenter, TxtCatalog, TxtFunctional, MeeDesc, TxtSuperorder,
                MeeDesc, TxtPosition, MeeDesc2, TxtTechnical, TxtConsType, LueCapacityUom, MeeRemark,
                TxtDisplayName
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtWeight,TxtSize,TxtCapacity, 
            }, 0);

            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, false);
            Sm.FocusGrd(Grd1, 0, 1);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                ChkActInd.Checked = true;
                Grd1.Rows.Add();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                DteStart.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteEnd.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteStartUpDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueStatus, "001");
                Sm.SetLue(LueInfo, "LIFE");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAssetCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string AssetCode = TxtAssetCode.Text;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveTOHdr());

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
                        cml.Add(SaveTODtl(Row));

                Sm.ExecCommands(cml);
                ShowData(AssetCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string AssetCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowTOHdr(AssetCode);
                ShowTODtl(AssetCode);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        public void ShowTOHdr(string AssetCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*,  T2.AssetName, T2.AssetValue, T2.AssetDt, T3.AssetcategoryName, T2.DisplayName From TblTOHdr T  ");
            SQL.AppendLine("Inner Join TblAsset T2 On T.AssetCode = T2.AssetCode ");
            SQL.AppendLine("Left Join TblAssetCategory T3 On T2.AssetcategoryCode = T3.AssetCategoryCode ");
            SQL.AppendLine("Where T.AssetCode=@AssetCode ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "AssetCode", 
                        //1-5
                        "StartDt", "EndDt", "Status", "Class", "ObjectType",  
                        //6-10
                        "AuthorizedGrp", "Weight", "WeightUomCode","Size","InventoryNo",
                        //11-15
                        "StartUpDt","ShiftType","ManufactureName","ManufactureNo", "ManufacturePart",
                        //16-20
                        "ManufactureSerial","ManufactureCnt","ManufactureConstYr","ManufactureConstMth", "LocCode",
                        //21-25
                        "EntCode","SiteCode","AssetOrg","CCCode","WBS", 
                        //26-30
                        "PlanningPlant","PlanningGroup","Workcenter","Catalog","Functional",
                        //31-35
                        "Description","SuperorderEquip","Description2", "Position","TechnicalNo",
                        //36-40
                        "ConsType","Capacity","CapacityUOM", "AssetName", "AssetValue", 
                        //41-45
                        "AssetDt", "AssetcategoryName", "ActInd", "Info", "Remark",
                        //46
                        "DisplayName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAssetCode.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteStart, Sm.DrStr(dr, c[1]));
                        Sm.SetDte(DteEnd, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                        TxtClass.EditValue = Sm.DrStr(dr, c[4]);
                        TxtObjectType.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAuthorize.EditValue = Sm.DrStr(dr, c[6]);
                        TxtWeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        Sm.SetLue(LueWeightUomCode, Sm.DrStr(dr, c[8]));
                        TxtSize.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtInventoryNo.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteStartUpDt, Sm.DrStr(dr, c[11]));
                        TxtShift.EditValue = Sm.DrStr(dr, c[12]);
                        TxtManufacture.EditValue = Sm.DrStr(dr, c[13]);
                        TxtModel.EditValue = Sm.DrStr(dr, c[14]);
                        TxtPartNo.EditValue = Sm.DrStr(dr, c[15]);
                        TxtSerial.EditValue = Sm.DrStr(dr, c[16]);
                        TxtCnt.EditValue = Sm.DrStr(dr, c[17]);
                        TxtConstYr.EditValue = Sm.DrStr(dr, c[18]);
                        TxtConstMth.EditValue = Sm.DrStr(dr, c[19]);
                        Sm.SetLue(LueLocationCode, Sm.DrStr(dr, c[20]));
                        TxtCompany.EditValue = Sm.DrStr(dr, c[21]);
                        TxtArea.EditValue = Sm.DrStr(dr, c[22]);
                        TxtAsset.EditValue = Sm.DrStr(dr, c[23]);
                        Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[24]));
                        TxtWbs.EditValue = Sm.DrStr(dr, c[25]);
                        TxtPlanningPlant.EditValue = Sm.DrStr(dr, c[26]);
                        TxtPlannerGroup.EditValue = Sm.DrStr(dr, c[27]);
                        TxtWorkcenter.EditValue = Sm.DrStr(dr, c[28]);
                        TxtCatalog.EditValue = Sm.DrStr(dr, c[29]);
                        TxtFunctional.EditValue = Sm.DrStr(dr, c[30]);
                        MeeDesc.EditValue = Sm.DrStr(dr, c[31]);
                        TxtSuperorder.EditValue = Sm.DrStr(dr, c[32]);
                        MeeDesc2.EditValue = Sm.DrStr(dr, c[33]);
                        TxtPosition.EditValue = Sm.DrStr(dr, c[34]);
                        TxtTechnical.EditValue = Sm.DrStr(dr, c[35]);
                        TxtConsType.EditValue = Sm.DrStr(dr, c[36]);
                        TxtCapacity.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                        Sm.SetLue(LueCapacityUom, Sm.DrStr(dr, c[38]));
                        TxtAssetName.EditValue = Sm.DrStr(dr, c[39]);
                        TxtAcquisition.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                        Sm.SetDte(DteAcquisitionDt, Sm.DrStr(dr, c[41]));
                        TxtAssetCtCode.EditValue = Sm.DrStr(dr, c[42]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[43]), "Y");
                        Sm.SetLue(LueInfo, Sm.DrStr(dr, c[44]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[45]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[46]);
                    }, true
                );
        }

        private void ShowTODtl(string AssetCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.EquipmentCode, B.EquipmentName " +
                    "From TblTODtl A Inner Join TblEquipment B On A.EquipmentCode=B.EquipmentCode Where A.AssetCode=@AssetCode Order By Dno;",
                    new string[]
                    {
                        //0
                        "EquipmentCode", 
                            
                        //1-4
                        "EquipmentName",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    }, false, false, false, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
            Grd1.Rows.Add();
        }

       

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAssetCode, "Asset code", false) || 
                Sm.IsLueEmpty(LueStatus, "Status")||
                Sm.IsDteEmpty(DteStart, "Date valid From")||
                Sm.IsDteEmpty(DteEnd, "Date Valid To") ||
                Sm.IsLueEmpty(LueInfo, "Info")
                ;
        }

        private MySqlCommand SaveTOHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTOHdr(AssetCode, ActInd, StartDt, EndDt, Status, Info, Class, ObjectType, AuthorizedGrp, Weight, "); 
            SQL.AppendLine("    WeightUomCode,Size,InventoryNo,StartUpDt,ShiftType,ManufactureName,ManufactureNo, "); 
            SQL.AppendLine("    ManufacturePart,ManufactureSerial,ManufactureCnt,ManufactureConstYr,ManufactureConstMth, "); 
            SQL.AppendLine("    LocCode,EntCode,SiteCode,AssetOrg,CCCode,WBS,PlanningPlant, ");
            SQL.AppendLine("    PlanningGroup,Workcenter,Catalog,Functional,Description,SuperorderEquip,Description2, "); 
            SQL.AppendLine("    Position,TechnicalNo,ConsType,Capacity,CapacityUOM,Remark,CreateBy,CreateDt) ");
            SQL.AppendLine("    Values(@AssetCode, @ActInd, @StartDt, @EndDt, @Status, @Info, @Class, @ObjectType, @AuthorizedGrp, @Weight,  ");
            SQL.AppendLine("    @WeightUomCode,@Size,@InventoryNo,@StartUpDt,@ShiftType,@ManufactureName,@ManufactureNo, ");
            SQL.AppendLine("    @ManufacturePart,@ManufactureSerial,@ManufactureCnt,@ManufactureConstYr,@ManufactureConstMth, ");
            SQL.AppendLine("    @LocCode,@EntCode,@SiteCode,@AssetOrg,@CCCode,@WBS,@PlanningPlant, ");
            SQL.AppendLine("    @PlanningGroup,@Workcenter,@Catalog,@Functional,@Description,@SuperorderEquip,@Description2, ");
            SQL.AppendLine("    @Position,@TechnicalNo,@ConsType,@Capacity,@CapacityUOM,@Remark, ");
            SQL.AppendLine("    @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    ActInd=@ActInd, StartDt=@StartDt, EndDt=@EndDt, Status=@Status, Info=@Info, Class=@Class, ObjectType=@ObjectType, AuthorizedGrp=@AuthorizedGrp, Weight=@Weight, ");
            SQL.AppendLine("    WeightUomCode=@WeightUomCode,Size=@Size,InventoryNo=@InventoryNo,StartUpDt=@StartUpDt,ShiftType=@ShiftType,ManufactureName=@ManufactureName,ManufactureNo=@ManufactureNo,  ");
            SQL.AppendLine("    ManufacturePart=@ManufacturePart,ManufactureSerial=@ManufactureSerial,ManufactureCnt=@ManufactureCnt,ManufactureConstYr=@ManufactureConstYr,ManufactureConstMth=@ManufactureConstMth,  ");
            SQL.AppendLine("    LocCode=@LocCode,EntCode=@EntCode,SiteCode=@SiteCode,AssetOrg=@AssetOrg,CCCode=@CCCode,WBS=@WBS,PlanningPlant=@PlanningPlant,  ");
            SQL.AppendLine("    PlanningGroup=@PlanningGroup,Workcenter=@Workcenter,Catalog=@Catalog,Functional=@Functional,Description=@Description,SuperorderEquip=@SuperorderEquip,Description2=@Description2, ");
            SQL.AppendLine("    Position=@Position,TechnicalNo=@TechnicalNo,ConsType=@ConsType,Capacity=@Capacity,CapacityUOM=@CapacityUOM,Remark=@Remark,  ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            SQL.AppendLine("Delete From TblToDtl Where AssetCode=@AssetCode; ");

            SQL.AppendLine("Insert Into TblLocationSummary(AssetCode, LocCode, CreateBy, CreateDt) ");
            SQL.AppendLine("    Values (@AssetCode, @LocCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update LocCode = @LocCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStart));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEnd));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@Info", Sm.GetLue(LueInfo));
            Sm.CmParam<String>(ref cm, "@Class", TxtClass.Text);
            Sm.CmParam<String>(ref cm, "@ObjectType", TxtObjectType.Text);
            Sm.CmParam<String>(ref cm, "@AuthorizedGrp", TxtAuthorize.Text);
            Sm.CmParam<Decimal>(ref cm, "@Weight", Decimal.Parse(TxtWeight.Text));
            Sm.CmParam<String>(ref cm, "@WeightUomCode", Sm.GetLue(LueWeightUomCode));
            Sm.CmParam<Decimal>(ref cm, "@Size", Decimal.Parse(TxtSize.Text));
            Sm.CmParam<String>(ref cm, "@InventoryNo", TxtInventoryNo.Text);
            Sm.CmParamDt(ref cm, "@StartUpDt", Sm.GetDte(DteStartUpDt));
            Sm.CmParam<String>(ref cm, "@ShiftType", TxtShift.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureName", TxtManufacture.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureNo", TxtModel.Text);
            Sm.CmParam<String>(ref cm, "@ManufacturePart", TxtPartNo.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureSerial", TxtSerial.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureCnt", TxtCnt.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureConstYr", TxtConstYr.Text);
            Sm.CmParam<String>(ref cm, "@ManufactureConstMth", TxtConstMth.Text);
            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetLue(LueLocationCode));
            Sm.CmParam<String>(ref cm, "@EntCode", TxtCompany.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", TxtArea.Text);
            Sm.CmParam<String>(ref cm, "@AssetOrg", TxtAsset.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@WBS", TxtWbs.Text);
            Sm.CmParam<String>(ref cm, "@PlanningPlant", TxtPlanningPlant.Text);
            Sm.CmParam<String>(ref cm, "@PlanningGroup", TxtPlannerGroup.Text);
            Sm.CmParam<String>(ref cm, "@Workcenter", TxtWorkcenter.Text);
            Sm.CmParam<String>(ref cm, "@Catalog", TxtCatalog.Text);
            Sm.CmParam<String>(ref cm, "@Functional", TxtFunctional.Text);
            Sm.CmParam<String>(ref cm, "@Description", MeeDesc.Text);
            Sm.CmParam<String>(ref cm, "@SuperorderEquip", TxtSuperorder.Text);
            Sm.CmParam<String>(ref cm, "@Description2", MeeDesc2.Text);
            Sm.CmParam<String>(ref cm, "@Position", TxtPosition.Text);
            Sm.CmParam<String>(ref cm, "@TechnicalNo", TxtTechnical.Text);
            Sm.CmParam<String>(ref cm, "@ConsType", TxtConsType.Text);
            Sm.CmParam<Decimal>(ref cm, "@Capacity", Decimal.Parse(TxtCapacity.Text));
            Sm.CmParam<String>(ref cm, "@CapacityUOM", Sm.GetLue(LueCapacityUom));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTODtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblTODtl(AssetCode, Dno, EquipmentCode, CreateBy, CreateDt) " +
                    "Values(@AssetCode, @Dno, @EquipmentCode, @CreateBy, CurrentDateTime()); "

            };
            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EquipmentCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLueInfo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'OFF' As Col1  ");
            SQL.AppendLine("UNION ALL");
            SQL.AppendLine("Select 'LIFE' As Col1  ");
            
            Sm.SetLue1(ref Lue, SQL.ToString(), "Info");
        }

        private void SetLueLocationCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select SiteCode From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueStatusCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2  From TblOption Where Optcat = 'MaintenanceStatus' ");
            SQL.AppendLine("Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        #endregion

        #endregion

        #region Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmTODlg2(this));
                    }
            }
            
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmTODlg2(this));
        }

        private void BtnEquip_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTODlg(this));
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(ref Sl.SetLueCCCode));
        }

        private void LueCapacityUom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCapacityUom, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueWeightUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWeightUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueLocationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocationCode, new Sm.RefreshLue1(SetLueLocationCode));
        }

        private void TxtAcquisition_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAcquisition, 0);
        }

        private void TxtWeight_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtWeight, 0);
        }

        private void TxtSize_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtSize, 0);
        }

        private void TxtCapacity_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCapacity, 0);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatusCode));
        }

        private void LueInfo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInfo, new Sm.RefreshLue1(SetLueInfo));
        }

        #endregion
       
    }
}
