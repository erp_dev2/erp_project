﻿#region Update
/*
    07/05/2018 [HAR] validasi entity berdasarkan entity di group user dan berdasarkan parameter
    14/12/2018 [TKG] bug saat menghitung nilai awal.
    18/12/2019 [HAR/KBN] bug untuk journal ambil tanggal sebelum periode end date
    10/06/2020 [WED/KBN] COA tidak wajib diisi berdasarkan parameter IsPrintGLCOANotMandatory
    15/06/2020 [TKG/KBN] ubah proses data dengan menggunakan GC
    22/06/2020 [HAR/KBN] Clear Logo
    08/12/2021 [BRI/PHT] tambah filter multi profit center dan difilter berdasarkan profit center dengan param IsFicoUseMultiProfitCenterFilter, IsFilterByProfitCenter
    14/03/2022 [SET/PHT] Merubah format hasil printout GL dengan memunculkan saldo awal, mutasi debit kredit sesuai transaksi, dan debit, kredit , serta balance saldo akhir menggunakan param IsPrintOutGeneralLedgerUseMultiCOA
    16/03/2022 [SET/PHT] Merubah perumusan filter COA's Account PrintOut General Ledger dari yang hanya COA Child yang bisa di print, menjadi bisa difilter oleh COA yang lebih parent menggunakan param IsPrintOutGeneralLedgerUseMultiCOA
    23/03/2022 [BRI/PHT] profit center get child tapi hanya menampilkan profit center yang dipilih
    28/03/2022 [BRI/PHT] tambah grand total
    01/04/2022 [MYA/PHT] memindahkan judul profit center di header pada hasil print reporting Print Out General Ledger
    11/04/2022 [BRI/PHT] bug Grand Total dan AcNo
    15/04/2022 [BRI/PHT] bug nilai OB
    19/04/2022 [BRI/VIR] tambah filter AcNo berdasarkan param IsGeneralLedgerUseCOADigit
    18/05/2022 [BRI/VIR] perbaiki posisi filter
    20/05/2022 [BRI/PHT] merubah source remark
    23/05/2022 [BRI/PHT] bug reporting
    31/10/2022 [SET/PHT] Penyesuaian Grand Total printout
    30/11/2022 [SET/PHT] Penyesuaian filter COA parent
    27/03/2023 [WED/PHT] percepat load
                         print out minus pakai simbol kurung buka-tutup
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintGL : RunSystem.FrmBase11
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private decimal mGrandTotal = 0m;
        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocTitle = string.Empty,
            mSQL = string.Empty, 
            startYr = string.Empty,
            mPOPrintOutCompanyLogo = "1",
            mQueryAcNo = string.Empty,
            mQueryAcNo2 = string.Empty,
            mQueryAcNo3 = string.Empty,
            mQueryAcNo4 = string.Empty,
            mAcNo = string.Empty,
            mQueryProfitCenter = string.Empty,
            mQueryProfitCenter2 = string.Empty,
            mQueryProfitCenter3 = string.Empty,
            mQueryProfitCenter4 = string.Empty,
            mProfitCenterCode = string.Empty
            ;
        private bool
           mIsReportingFilterByEntity = false,
           mIsPrintGLCOANotMandatory = false,
           mIsFicoUseMultiProfitCenterFilter = false,
           mIsFilterByProfitCenter = false,
           mIsAllAcNoSelected = false,
           mIsPrintOutGeneralLedgerUseMultiCOA = false,
           mIsAllProfitCenterSelected = false,
           mIsGeneralLedgerUseCOADigit = false
           ;
        private List<String> mlAcNo = null;

        #endregion

        #region Constructor

        public FrmPrintGL(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                if (mIsPrintGLCOANotMandatory)
                {
                    LblCOAAc.ForeColor = Color.Black;
                }
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, 0);
                SetLueAcNo(ref LueCOAAc);
                mlAcNo = new List<String>();
                SetCcbAcNo(ref CcbAcNo );
                if(mIsPrintOutGeneralLedgerUseMultiCOA)
                {
                    LueCOAAc.Visible = false;
                    CcbAcNo.Top = 31;
                    ChkAcNo.Top = 31;
                }
                if (mIsFicoUseMultiProfitCenterFilter)
                {
                    int ypoint = 52;
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                    LblMultiProfitCenterCode.Top = ypoint;
                    CcbProfitCenterCode.Top = ypoint;
                    ChkProfitCenterCode.Top = ypoint;
                }
                else
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                if (!mIsGeneralLedgerUseCOADigit)
                {
                    TxtAcNo1.Visible = TxtAcNo2.Visible = TxtAcNo3.Visible = TxtAcNo4.Visible = TxtAcNo5.Visible = TxtAcNo6.Visible = TxtAcNo7.Visible = false;
                    TxtAcNo8.Visible = TxtAcNo9.Visible = TxtAcNo10.Visible = TxtAcNo11.Visible = TxtAcNo12.Visible = TxtAcNo13.Visible = TxtAcNo14.Visible = false;
                    ChkAcNo2.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                (!mIsPrintGLCOANotMandatory && !mIsPrintOutGeneralLedgerUseMultiCOA && !mIsGeneralLedgerUseCOADigit && Sm.IsLueEmpty(LueCOAAc, "COA's Account")) ||
                IsMultiCOAInvalid() || 
                IsCOAFilterInvalid()
                ) return;

            var separator = new System.Globalization.NumberFormatInfo()
            {
                NumberDecimalDigits = 2,
                NumberGroupSeparator = ","
            };

            mDocTitle = Sm.GetParameter("DocTitle");
            var l1 = new List<Data1>();
            var l2 = new List<Data2>();
            var l3 = new List<Data2>();
            var l = new List<GeneralLedger>();
            var lLogo = new List<Logo>();
            string[] TableName = { "GeneralLedger", "Logo" };
            var myLists = new List<IList>();
            var selectedProfitCenter = new List<SelectedProfitCenter>();
            var selectedCOA = new List<SelectedCOA>();
            string AcNo1 = GetCcbAcNo();
            string[] AcNoList = AcNo1.Split(',');

            int Id1 = GC.GetGeneration(myLists);
            int Id2 = GC.GetGeneration(lLogo);
            int Id3 = GC.GetGeneration(l1);
            int Id4 = GC.GetGeneration(l2);
            int Id5 = GC.GetGeneration(l3);
            int Id6 = GC.GetGeneration(l);

            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;

                SetAcNo();
                SetProfitCenter();
                if (mlProfitCenter.Count > 0) GetProfitCenterData(ref selectedProfitCenter);
                if (AcNo1.Length > 0) GetCOAData(ref selectedCOA, AcNo1);

                Process1(ref l1, AcNo1, AcNoList);
                if (l1.Count > 0)
                {
                    Process2(ref l2, ref l1);
                    Process3(ref l1, ref l2);
                    l1.Clear();
                    l1 = null;
                    Process4(ref l2, ref l3);
                    Process5(ref l3, ref selectedProfitCenter, ref selectedCOA);
                    l2.Clear();
                    l2 = null;
                }
                else
                {
                    l1.Clear();
                    l2.Clear();
                    l3.Clear();
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                for (var i = 0; i < l3.Count; i++)
                {
                    string OBShow = string.Empty, DAmtShow = string.Empty, CAmtShow = string.Empty, BalanceShow = string.Empty;

                    if (l3[i].OpeningBalance < 0m) OBShow = string.Concat("(", (l3[i].OpeningBalance * -1m).ToString("N", separator), ")");
                    else OBShow = l3[i].OpeningBalance.ToString("N", separator);

                    if (l3[i].DAmt < 0m) DAmtShow = string.Concat("(", (l3[i].DAmt * -1m).ToString("N", separator), ")");
                    else DAmtShow = l3[i].DAmt.ToString("N", separator);

                    if (l3[i].CAmt < 0m) CAmtShow = string.Concat("(", (l3[i].CAmt * -1m).ToString("N", separator), ")");
                    else CAmtShow = l3[i].CAmt.ToString("N", separator);

                    if (l3[i].Balance < 0m) BalanceShow = string.Concat("(", (l3[i].Balance * -1m).ToString("N", separator), ")");
                    else BalanceShow = l3[i].Balance.ToString("N", separator);

                    l.Add(new GeneralLedger()
                    {
                        AcNo = l3[i].AcNo,
                        AcDesc = l3[i].AcDesc,
                        OpeningBalance = l3[i].OpeningBalance,
                        OpeningBalanceShow = OBShow,
                        DocNo = l3[i].DocNo,
                        DocDt = l3[i].DocDt,
                        Opening = l3[i].Opening,
                        DAmt = l3[i].DAmt,
                        DAmtShow = DAmtShow,
                        CAmt = l3[i].CAmt,
                        CAmtShow = CAmtShow,
                        Balance = l3[i].Balance,
                        BalanceShow = BalanceShow,
                        Remark = l3[i].Remark,
                        JnDesc = l3[i].JnDesc,
                        StartDt = DteDocDt1.Text,
                        EndDt = DteDocDt2.Text,
                        ProfitCenterCode = l3[i].ProfitCenterCode,
                        ProfitCenterName = l3[i].ProfitCenterName,
                        ProfitCenterCodeGrp = l3[i].ProfitCenterCodeGrp,
                        ProfitCenterNameGrp = l3[i].ProfitCenterNameGrp,
                        OBInd = l3[i].OBInd,
                    });
                }
                l3.Clear();
                l3 = null;

                #region Logo
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName' ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        "CompanyLogo", "CompanyName"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string GrandTotalShow = string.Empty;

                            if (mGrandTotal < 0m) GrandTotalShow = string.Concat("(", (mGrandTotal * -1m).ToString("N", separator), ")");
                            else GrandTotalShow = mGrandTotal.ToString("N", separator);

                            lLogo.Add(new Logo()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                ProfitCenterName = (mIsFicoUseMultiProfitCenterFilter ? GetProfitCenter() : ""),
                                GrandTotal = mGrandTotal,
                                GrandTotalShow = GrandTotalShow
                            });
                            //lLogo.ForEach(x => {
                            //    foreach (var y in l4)
                            //    {
                            //        x.GrandTotal += y.GrandTotal;
                            //    }
                            //});
                        }
                    }
                    dr.Close();
                }

                #endregion

                myLists.Add(l);
                myLists.Add(lLogo);

                if (mDocTitle == "PHT")
                    Sm.PrintReport("GeneralLedgerPHT", myLists, TableName, false);
                else
                    Sm.PrintReport("GeneralLedger", myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                if (myLists != null) myLists.Clear();
                //if (lLogo != null) lLogo.Clear();
                if (l1 != null) l1.Clear();
                if (l2 != null) l2.Clear();
                if (l3 != null) l3.Clear();
                if (l != null) l.Clear();

                myLists = null;
                //lLogo = null;
                l1 = null;
                l2 = null;
                l3 = null;
                l = null;

                GC.Collect(Id1, GCCollectionMode.Forced);
                GC.Collect(Id2, GCCollectionMode.Forced);
                GC.Collect(Id3, GCCollectionMode.Forced);
                GC.Collect(Id4, GCCollectionMode.Forced);
                GC.Collect(Id5, GCCollectionMode.Forced);
                GC.Collect(Id6, GCCollectionMode.Forced);

                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void GetCOAData(ref List<SelectedCOA> selectedCOA, string AcNo1)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, AcDesc ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where Find_In_Set(AcNo, @AcNos) ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AcNos", AcNo1);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        selectedCOA.Add(new SelectedCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetProfitCenterData(ref List<SelectedProfitCenter> selectedProfitCenter)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select ProfitCenterCode, ProfitCenterName ");
            SQL.AppendLine("From TblProfitCenter ");
            SQL.AppendLine("Where Find_In_Set(ProfitCenterCode, @ProfitCenterCodes) ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ProfitCenterCodes", GetCcbProfitCenterCode());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode", "ProfitCenterName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        selectedProfitCenter.Add(new SelectedProfitCenter()
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]),
                            ProfitCenterName = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetParameter()
        {
            mIsPrintOutGeneralLedgerUseMultiCOA = Sm.GetParameterBoo("IsPrintOutGeneralLedgerUseMultiCOA");
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsPrintGLCOANotMandatory = Sm.GetParameterBoo("IsPrintGLCOANotMandatory");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
            mIsGeneralLedgerUseCOADigit = Sm.GetParameterBoo("IsGeneralLedgerUseCOADigit");
        }

        private void Process1(ref List<Data1> l, string AcNo1, string[] AcNoList)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var AcNoTemp = string.Empty;
            var OpeningBalanceTemp = 0m;
            var Dt = Sm.GetDte(DteDocDt1).Substring(0, 8);
            var Yr = Sm.Left(Dt, 4);
            int m = 0, n = 0, o = 0;
            

            if (!mIsAllAcNoSelected)
            {
                var Filter = string.Empty;
                var Filter3 = string.Empty;
                mQueryAcNo = null;
                mQueryAcNo3 = null;
                int a = 0, b = 0, c = 0;
            }
            else
            {
                if (ChkAcNo.Checked)
                {
                    mQueryAcNo += "    And Find_In_Set(T2.AcNo, @AcNo) ";//bug
                    if (ChkAcNo.Checked)
                        Sm.CmParam<String>(ref cm, "@AcNo", AcNo1);
                }
            }

            #region Profit Center filtered
            if (ChkProfitCenterCode.Checked)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (B.ProfitCenterCode=@ProfitCenter1_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter1_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                        mQueryProfitCenter += "    And 1=0 ";
                    else
                        mQueryProfitCenter += "    And (" + Filter_ + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter += "    And Find_In_Set(B.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter += "    And B.ProfitCenterCode In ( ";
                        mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "    ) ";
                    }
                }

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter2 = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (T4.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                        mQueryProfitCenter2 += "    And 1=0 ";
                    else
                        mQueryProfitCenter2 += "    And (" + Filter_ + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter2 += "    And Find_In_Set(T4.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter2 += "    And T4.ProfitCenterCode In ( ";
                        mQueryProfitCenter2 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter2 += "    ) ";
                    }
                }

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter3 = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (T4.ProfitCenterCode=@ProfitCenter3_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter3_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                        mQueryProfitCenter3 += "    And 1=0 ";
                    else
                        mQueryProfitCenter3 += "    And (" + Filter_ + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter3 += "    And Find_In_Set(T4.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter3 += "    And T4.ProfitCenterCode In ( ";
                        mQueryProfitCenter3 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter3 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter3 += "    ) ";
                    }
                }
            }
            #endregion

            SQL.AppendLine("Select A.AcNo, A.AcDesc, ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("F.ProfitCenterCode, F.ProfitCenterName, ");
            else
                SQL.AppendLine("Null As ProfitCenterCode, Null As ProfitCenterName, ");
            SQL.AppendLine("IfNull(C.DAmt - C.CAmt, 0.00)+IfNull(D.DAmt, 0.00)-IfNull(D.CAmt, 0.00) As Amt ");
            //SQL.AppendLine("From TblCOA A "); // ini akan lama banget padahal COA udah di filter [by wed]

            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, AcDesc, ActInd ");
            SQL.AppendLine("    From TblCOA ");
            SQL.AppendLine("    Where Actind = 'Y' ");
            SQL.AppendLine("    And AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");

            if (Sm.GetLue(LueCOAAc).Length > 0)
                SQL.AppendLine("And AcNo=@AcNo ");
            if (mIsGeneralLedgerUseCOADigit && ChkAcNo2.Checked)
                SQL.AppendLine("And Find_In_Set(AcNo,@AcNo2) ");
            if (mIsPrintOutGeneralLedgerUseMultiCOA && AcNo1.Length > 0)
            {
                m = 0;
                foreach (var y in AcNoList)
                {
                    if (m == 0) SQL.AppendLine("     And ( ");
                    else SQL.AppendLine("    OR ");

                    SQL.AppendLine("     AcNo LIKE '" + y + "%'");
                    m++;
                }
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(") A ");

            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("Inner Join TblProfitCenter B On B.ProfitCenterCode " + mQueryProfitCenter + " ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.AcNo, ");
            SQL.AppendLine("    Sum(Case T3.AcType When 'D' Then T2.Amt Else 0.00 End) As DAmt, ");
            SQL.AppendLine("    Sum(Case T3.AcType When 'C' Then T2.Amt Else 0.00 End) As CAmt ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("    , T4.ProfitCenterCode ");
            SQL.AppendLine("    From TblCOAOpeningBalanceHdr T1 ");
            SQL.AppendLine("    Inner Join TblCOAOpeningBalanceDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.Yr=@Yr ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And T2.Amt <> 0.00 ");
            if (Sm.GetLue(LueCOAAc).Length > 0)
                SQL.AppendLine("        And T2.AcNo=@AcNo ");
            if (mIsGeneralLedgerUseCOADigit && ChkAcNo2.Checked)
                SQL.AppendLine("        And Find_In_Set(T2.AcNo,@AcNo2) ");
            if (mIsPrintOutGeneralLedgerUseMultiCOA && AcNo1.Length > 0)
            {
                m = 0;
                foreach (var y in AcNoList)
                {
                    if (m == 0)
                        SQL.AppendLine("     And ( ");
                    else
                        SQL.AppendLine("    OR ");
                    SQL.AppendLine("     T2.AcNo LIKE '" + y + "%'");
                    m++;
                }
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TblCOA T3 On T2.AcNo = T3.AcNo And T3.ActInd = 'Y' And T3.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            if (ChkProfitCenterCode.Checked)
            {
                SQL.AppendLine("    Inner Join TblProfitCenter T4 On T1.ProfitCenterCode = T4.ProfitCenterCode ");
                SQL.AppendLine(mQueryProfitCenter2);
            }
            SQL.AppendLine("    Group By T2.AcNo ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("    , T4.ProfitCenterCode ");
            SQL.AppendLine(") C On A.AcNo = C.AcNo ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("And B.ProfitCenterCode = C.ProfitCenterCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.AcNo, Sum(T2.DAmt) As DAmt, Sum(T2.CAmt) As CAmt ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("    , T4.ProfitCenterCode ");
            SQL.AppendLine("    From TblJournalHdr T1 ");
            SQL.AppendLine("    Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And Left(T1.DocDt, 4) = @Yr ");
            SQL.AppendLine("        And T1.DocDt < @Dt ");
            SQL.AppendLine("        And T2.DAmt + T2.CAmt != 0.00 ");
            if (Sm.GetLue(LueCOAAc).Length > 0)
                SQL.AppendLine("        And T2.AcNo=@AcNo ");
            if (mIsGeneralLedgerUseCOADigit && ChkAcNo2.Checked)
                SQL.AppendLine("        And Find_In_Set(T2.AcNo,@AcNo2) ");
            if (mIsPrintOutGeneralLedgerUseMultiCOA && AcNo1.Length > 0)
            {
                n = 0;
                foreach (var y in AcNoList)
                {
                    if (n == 0)
                        SQL.AppendLine("     And ( ");
                    else
                        SQL.AppendLine("    OR ");
                    SQL.AppendLine("     T2.AcNo LIKE '" + y + "%'");
                    n++;
                }
                SQL.AppendLine("    ) ");
            }
            
            if (ChkProfitCenterCode.Checked)
            {
                SQL.AppendLine("    Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("    Inner Join TblProfitCenter T4 On T3.ProfitCenterCode = T4.ProfitCenterCode ");
                SQL.AppendLine(mQueryProfitCenter3);
            }
            SQL.AppendLine("Inner Join TblCOA T5 On T2.AcNo = T5.AcNo And T5.ActInd = 'Y' And T5.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    Group By T2.AcNo ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("    , T4.ProfitCenterCode ");
            SQL.AppendLine(") D On A.AcNo = D.AcNo ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("And B.ProfitCenterCode = D.ProfitCenterCode ");

            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("Inner Join TblCoaDtl D On A.AcNo = D.Acno ");
                SQL.AppendLine("And D.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(D.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine("Left Join TblProfitCenter F On B.ProfitCenterCode = F.ProfitCenterCode ");
            SQL.AppendLine("Where 0 = 0 ");
            SQL.AppendLine("Order By A.AcNo ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine(", B.ProfitCenterCode ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParamDt(ref cm, "@Dt", Dt);
                Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueCOAAc));
                Sm.CmParam<String>(ref cm, "@AcNo2", GetAcNoLike());
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Amt", "ProfitCenterCode", "ProfitCenterName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data1()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            OpeningBalance = Sm.DrDec(dr, c[2]),
                            OpeningBalanceGrp = 0m,
                            ProfitCenterCode = Sm.DrStr(dr, c[3]),
                            ProfitCenterName = Sm.DrStr(dr, c[4]),
                        });
                    }
                    if (ChkProfitCenterCode.Checked)
                    {
                        var z = l.Last();
                        foreach (var x in l)
                        {
                            if (x.AcNo == AcNoTemp || AcNoTemp.Length == 0)
                            {
                                if (x.OpeningBalance > 0)
                                {
                                    OpeningBalanceTemp = x.OpeningBalance;
                                }
                            }
                            else
                            {
                                if (OpeningBalanceTemp > 0)
                                {
                                    foreach (var y in l)
                                    {
                                        if (y.AcNo == AcNoTemp)
                                            y.OpeningBalanceGrp = OpeningBalanceTemp;
                                    }
                                    OpeningBalanceTemp = 0m;
                                }
                            }
                            if (x.AcNo == z.AcNo && x.ProfitCenterCode == z.ProfitCenterCode && OpeningBalanceTemp > 0)
                            {
                                foreach (var y in l)
                                {
                                    if (y.AcNo == AcNoTemp)
                                        y.OpeningBalanceGrp = OpeningBalanceTemp;
                                }
                                OpeningBalanceTemp = 0m;
                            }
                            AcNoTemp = x.AcNo;
                        }
                    }
                }
                dr.Close();
            }
        }
        private void Process2(ref List<Data2> l, ref List<Data1> lSET)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var AcNoTemp = string.Empty;
            var AcNoPrev = string.Empty;  //date_format(DocDt, '%d %b %Y')
            int i = 0;

            if (!mIsAllAcNoSelected)
            {
                var Filter4 = string.Empty;
                mQueryAcNo4 = null;
                int j = 0;
                foreach (var x in mlAcNo.Distinct())
                {
                    if (Filter4.Length > 0) Filter4 += " Or ";
                    Filter4 += " B.AcNo=@AcNo4_" + j.ToString() + " ";
                    Sm.CmParam<String>(ref cm, "@AcNo4_" + j.ToString(), x);
                    j++;
                }
                if (Filter4.Length == 0)
                {
                    mQueryAcNo4 += "    And 1=0 ";
                }
                else
                {
                    mQueryAcNo4 += "    And (" + Filter4 + ") ";
                }
            }
            else
            {
                if (ChkAcNo.Checked)
                {
                    mQueryAcNo4 += "    And Find_In_Set(B.AcNo, @AcNo) ";
                    if (ChkAcNo.Checked)
                        Sm.CmParam<String>(ref cm, "@AcNo", GetCcbAcNo());
                }
            }

            if (ChkProfitCenterCode.Checked)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter4 = null;
                    int j = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (G.ProfitCenterCode=@ProfitCenter4_" + j.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter4_" + j.ToString(), x);
                        j++;
                    }
                    if (Filter_.Length == 0)
                        mQueryProfitCenter4 += "    And 1=0 ";
                    else
                        mQueryProfitCenter4 += "    And (" + Filter_ + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter4 += "    And Find_In_Set(G.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter4 += "    And G.ProfitCenterCode In ( ";
                        mQueryProfitCenter4 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter4 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter4 += "    ) ";
                    }
                }
            }

            SQL.AppendLine("Select B.AcNo, C.AcDesc, C.AcType, A.DocNo, Date_Format(A.DocDt,'%d %b %Y')as DocDt, B.DAmt, B.CAmt, ");
            SQL.AppendLine("Case When D.JournalDocNo Is Not Null Or E.JournalDocNo Is Not Null Then A.Remark Else A.Remark End As Remark, A.JnDesc ");
            if (ChkProfitCenterCode.Checked)
                SQL.AppendLine(", G.ProfitCenterCode, G.ProfitCenterName ");
            else
                SQL.AppendLine(", Null As ProfitCenterCode, Null As ProfitCenterName ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.DAmt + B.CAmt != 0.00 ");
            if (Sm.GetLue(LueCOAAc).Length > 0)
                SQL.AppendLine("    And B.AcNo = @AcNo ");
            if (mIsGeneralLedgerUseCOADigit && ChkAcNo2.Checked)
                SQL.AppendLine("    And Find_In_Set(B.AcNo,@AcNo2) ");
            if (mIsPrintOutGeneralLedgerUseMultiCOA && ChkAcNo.Checked)
                SQL.AppendLine(mQueryAcNo4);
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("And B.EntCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Left Join TblVoucherJournalHdr D On A.DocNo=D.JournalDocNo And D.JournalDocNo Is Not Null ");
            SQL.AppendLine("Left Join TblVoucherJournalHdr E On A.DocNo=E.JournalDocNo2 And E.JournalDocNo2 Is Not Null ");
            if (ChkProfitCenterCode.Checked)
            {
                SQL.AppendLine("Inner Join TblCostCenter F On A.CCCode = F.CCCode ");
                SQL.AppendLine("Inner Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode ");
                SQL.AppendLine(mQueryProfitCenter4);
            }
            else
                SQL.AppendLine("Left Join TblCostCenter F On A.CCCode = F.CCCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Order By B.AcNo, A.DocDt, A.DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetLue(LueCOAAc));
                Sm.CmParam<String>(ref cm, "@AcNo2", GetAcNoLike());
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    "AcNo", 
                    "AcDesc", "DocNo", "DocDt", "DAmt", "CAmt",
                    "Remark", "JnDesc", "ProfitCenterCode", "ProfitCenterName", "AcType"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        AcNoTemp = Sm.DrStr(dr, c[0]);
                        if (Sm.CompareStr(AcNoTemp, AcNoPrev))
                            i += 1;
                        else
                            i = 1;
                        l.Add(new Data2()
                        {
                            AcNo = AcNoTemp,
                            AcDesc = Sm.DrStr(dr, c[1]),
                            SeqNo=i,
                            DocNo = Sm.DrStr(dr, c[2]),
                            DocDt = Sm.DrStr(dr, c[3]),
                            DAmt = Sm.DrDec(dr, c[4]),
                            CAmt = Sm.DrDec(dr, c[5]),
                            Remark = Sm.DrStr(dr, c[6]),
                            OpeningBalance = 0m,
                            Opening = 0m,
                            Balance = 0m,
                            JnDesc = Sm.DrStr(dr, c[7]),
                            ProfitCenterCode = Sm.DrStr(dr, c[8]),
                            ProfitCenterName = Sm.DrStr(dr, c[9]),
                            AcType = Sm.DrStr(dr, c[10]),
                        });
                        AcNoPrev = AcNoTemp;
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Data1> l1, ref List<Data2> l2)
        {
            mProfitCenterCode = GetCcbProfitCenterCode();
            int x = 0, v = 0;
            bool IsFind = false;
            for (int i = 0; i < l1.Count; i++)
            {
                x = v;
                IsFind = false;
                for (int j = x; j < l2.Count; j++)
                {
                    if (Sm.Find_In_Set(l1[i].AcNo + l1[i].ProfitCenterCode, l2[j].AcNo + l2[j].ProfitCenterCode))
                    {
                        if (ChkProfitCenterCode.Checked)
                            l2[j].OpeningBalance = l1[i].OpeningBalanceGrp;
                        else
                            l2[j].OpeningBalance = l1[i].OpeningBalance;
                        v = j;
                        IsFind = true;
                    }
                    else
                    {
                        if (Sm.CompareStr(l2[j].AcNo, l1[i].AcNo) && l1[i].OpeningBalanceGrp > 0)
                        {
                            l2[j].OpeningBalance = l1[i].OpeningBalanceGrp;
                        }
                        if (IsFind) break;
                    }
                }
                if (l1[i].OpeningBalance > 0 || Sm.Find_In_Set(l1[i].ProfitCenterCode, mProfitCenterCode))
                {
                    l2.Add(new Data2()
                    {
                        AcNo = l1[i].AcNo,
                        AcDesc = l1[i].AcDesc,
                        SeqNo = 0,
                        DocNo = string.Empty,
                        DocDt = string.Empty,
                        DAmt = 0m,
                        CAmt = 0m,
                        OpeningBalance = l1[i].OpeningBalance,
                        Opening = 0m,
                        Balance = 0m,
                        Remark = string.Empty,
                        JnDesc = l1[i].JnDesc,
                        ProfitCenterCode = l1[i].ProfitCenterCode,
                        ProfitCenterName = l1[i].ProfitCenterName,
                    });
                }
            }
        }

        private void Process4(ref List<Data2> l2, ref List<Data2> l3)
        {
            mGrandTotal = 0m;
            var Amt = 0m;
            var OBInd = string.Empty;
            var AcNoTemp = string.Empty;
            var GrandTotalTemp1 = 0m;
            var GrandTotalTemp2 = 0m;
            l2.Count(); l3.Count();
            foreach (var x in l2.OrderBy(o1 => o1.AcNo).ThenBy(o2 => o2.ProfitCenterCode).ThenBy(o2 => o2.SeqNo))
            {
                var z = l2.Last();
                if (x.SeqNo == 0)
                {
                    if (AcNoTemp != x.AcNo)
                    {
                        GrandTotalTemp1 += Amt;
                        AcNoTemp = x.AcNo;
                        OBInd = "Y";
                    }
                    else
                        OBInd = string.Empty;
                    Amt = x.OpeningBalance;
                }
                else
                {
                    OBInd = string.Empty;
                    x.Opening = Amt;
                    if (x.AcType == "D")
                        x.Balance = x.Opening + (x.DAmt - x.CAmt);
                    if (x.AcType == "C")
                        x.Balance = x.Opening + (x.CAmt - x.DAmt);
                    Amt = x.Balance;
                    GrandTotalTemp2 = Amt;
                }
                if (x.AcNo == z.AcNo && x.ProfitCenterCode == z.ProfitCenterCode && (GrandTotalTemp1 == 0 && Amt > 0))
                    GrandTotalTemp1 += Amt;
                l3.Add(new Data2()
                {
                    AcNo = x.AcNo,
                    AcDesc = x.AcDesc,
                    SeqNo = x.SeqNo,
                    DocNo = x.DocNo,
                    DocDt = x.DocDt,
                    OpeningBalance = x.OpeningBalance,
                    Opening = x.Opening,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt,
                    Balance = x.Balance,
                    Remark = x.Remark,
                    JnDesc = x.JnDesc,
                    ProfitCenterCode = x.ProfitCenterCode,
                    ProfitCenterName = x.ProfitCenterName,
                    OBInd = OBInd,
                });
            }
            if (GrandTotalTemp2 == 0)
                mGrandTotal = GrandTotalTemp1;
            else
                mGrandTotal = GrandTotalTemp2;
        }

        private void Process5(ref List<Data2> l3, ref List<SelectedProfitCenter> selectedProfitCenter, ref List<SelectedCOA> selectedCOA)
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            //mAcNo = GetCcbAcNo();
            //string[] mAcNos = mAcNo.Split(',');
            //mProfitCenterCode = GetCcbProfitCenterCode();
            //string[] mProfitCenterCodes = mProfitCenterCode.Split(',');
            var mOpeningBalance = 0m;
            var AcNoTemp = string.Empty;

            if (ChkProfitCenterCode.Checked)
            {
                foreach(var x in selectedProfitCenter) //foreach (var x in mProfitCenterCodes)
                {
                    foreach (var y in l3.Where(w => 
                            w.ProfitCenterCode.Length >= x.ProfitCenterCode.Length && 
                            w.ProfitCenterCode.Substring(0, x.ProfitCenterCode.Length) == x.ProfitCenterCode
                    ))
                    {
                        y.ProfitCenterCodeGrp = x.ProfitCenterCode;
                        y.ProfitCenterNameGrp = x.ProfitCenterName; //Sm.GetValue("Select ProfitCenterName From TblProfitCenter Where ProfitCenterCode = @Param", y.ProfitCenterCodeGrp);
                    }
                    //l3.RemoveAll(w => w.OpeningBalance == 0 && w.DAmt == 0 && w.CAmt == 0 && w.OBInd == "Y" && w.ProfitCenterCode != x);
                }
            }
            if (ChkAcNo.Checked && mDocTitle == "PHT")
            {
                foreach (var y in l3)
                {
                    if (y.OBInd == "Y" && y.AcNo != AcNoTemp)
                        mOpeningBalance += y.OpeningBalance;
                    AcNoTemp = y.AcNo;
                }

                foreach (var x in selectedCOA) //foreach (var x in mAcNos)
                {
                    l3.Insert(0, new Data2()
                    {
                        AcNo = x.AcNo,
                        AcDesc = x.AcDesc, //Sm.GetValue("Select AcDesc From TblCoa Where AcNo = @Param", x),
                        OpeningBalance = mOpeningBalance,
                        OBInd = "Y",
                    });
                }
            }
            l3.RemoveAll(w => w.DocNo == string.Empty && w.DAmt == 0 && w.CAmt == 0 && w.OBInd == string.Empty);
            //l3.GroupBy(g => new { g.AcNo, g.DocNo });
        }

        private string GetAcNoLike()
        {
            string[] Value = {  TxtAcNo1.Text , TxtAcNo2.Text , TxtAcNo3.Text , TxtAcNo4.Text , TxtAcNo5.Text , TxtAcNo6.Text , TxtAcNo7.Text ,
                                TxtAcNo8.Text , TxtAcNo9.Text , TxtAcNo10.Text , TxtAcNo11.Text , TxtAcNo12.Text , TxtAcNo13.Text , TxtAcNo14.Text };
            if (Value.Length == 0) return string.Empty;
            return GetAcNoLike(Value);
        }

        private string GetAcNoLike(string[] Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                GetValue(
                    "Select Group_Concat(T.Code Separator ',') As Code " +
                    "From (Select AcNo As Code From TblCOA Where " +
                    "Substring(AcNo,1,1) Like @Param1 " +
                    "And Substring(AcNo,3,1) Like @Param2 " +
                    "And Substring(AcNo,5,1) Like @Param3 " +
                    "And Substring(AcNo,7,1) Like @Param4 " +
                    "And Substring(AcNo,9,1) Like @Param5 " +
                    "And Substring(AcNo,11,1) Like @Param6 " +
                    "And Substring(AcNo,13,1) Like @Param7 " +
                    "And Substring(AcNo,15,1) Like @Param8 And Substring(AcNo,16,1) Like @Param9 And Substring(AcNo,17,1) Like @Param10 " +
                    "And Substring(AcNo,19,1) Like @Param11 And Substring(AcNo,20,1) Like @Param12 And Substring(AcNo,21,1) Like @Param13 And Substring(AcNo,22,1) Like @Param14) T; ",
                    Value[0], Value[1], Value[2], Value[3], Value[4], Value[5], Value[6], Value[7], Value[8], Value[9], Value[10], Value[11], Value[12], Value[13]);
        }

        private void SetCcbAcNo(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(T.AcNo, ' [', T.AcDesc, ']') As Col ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Where 0 = 0 ");
            //if (mIsCOAFilteredByGroup)
            //{
            //    SQL.AppendLine("    And Exists ");
            //    SQL.AppendLine("    ( ");
            //    SQL.AppendLine("        Select 1 From TblGroupCOA ");
            //    SQL.AppendLine("        Where GrpCode In (");
            //    SQL.AppendLine("            Select GrpCode From TblUser ");
            //    SQL.AppendLine("            Where UserCode=@UserCode ");
            //    SQL.AppendLine("        ) ");
            //    SQL.AppendLine("        And AcNo Like Concat(T.AcNo, '%') ");
            //    SQL.AppendLine("    ) ");
            //}
            SQL.AppendLine("Order By Concat(T.AcNo, ' [', T.AcDesc, ']');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        internal void SetAcNo()
        {
            mIsAllAcNoSelected = false;

            if (!ChkAcNo.Checked) mIsAllAcNoSelected = true;

            if (mIsAllAcNoSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlAcNo.Clear();

            while (!IsCompleted)
                SetAcNo(ref IsFirst, ref IsCompleted);
        }

        private void SetAcNo(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, AcNo = string.Empty;
            bool IsExisted = false;
            int i = 0, j = 0, k = 0, p = 0;
            string AcNo1 = GetCcbAcNo();
            string AcNo2 = GetAcNoParent();
            string[] AcNoList = AcNo1.Split(',');
            string[] AcNoParentList = AcNo2.Split(',');
            //string a = AcNoList;

            if (IsFirst)
            {
                if (ChkAcNo.Checked)
                {
                    SQL.AppendLine("Select distinct AcNo From TblCoa ");
                    //SQL.AppendLine("    Where Find_In_Set(AcNo, @AcNo) ");
                    //SQL.AppendLine("    Where Parent IS NOT NULL ");
                    SQL.AppendLine("    Where ");
                    foreach (var y in AcNoList)
                    {
                        if (j == 0)
                            SQL.AppendLine("     ( ");
                        else
                            SQL.AppendLine("    OR ");
                        SQL.AppendLine("     AcNo LIKE '" + y + "%'");
                        j++;
                    }
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    And Not Find_In_Set(AcNo, @AcNo) ");
                    //SQL.AppendLine("    AND NOT Find_IN_set(acno, ( ");
                    //SQL.AppendLine("        SELECT group_concat(Parent) ");
                    //SQL.AppendLine("        FROM tblcoa ");
                    //SQL.AppendLine("        WHERE ");
                    //foreach (var z in AcNoList)
                    //{
                    //if (AcNoList.Count() > 1 && AcNoList.First() != null )
                    //    if (k == 0)
                    //        SQL.AppendLine("     AcNo LIKE '" + z + "%'");
                    //    else
                    //    {
                    //        SQL.AppendLine("        OR ");
                    //        SQL.AppendLine("     AcNo LIKE '" + z + "%'");
                    //    }
                    //    k++;
                    //}
                    //SQL.AppendLine("    )) ");
                    //SQL.AppendLine("    And ( ");
                    //SQL.AppendLine("     ( ");
                    //foreach (var s in AcNoParentList)
                    //{
                    //    //if (AcNoParentList.Count() > 1)
                    //    if (p == 0)
                    //        SQL.AppendLine("     AcNo != '" + s + "' ");
                    //    else
                    //    {
                    //        SQL.AppendLine("        And ");
                    //        SQL.AppendLine("     AcNo != '" + s + "' ");
                    //    }
                    //    p++;
                    //}
                    //SQL.AppendLine("    ) ");
                    //Sm.CmParam<String>(ref cm, "@AcNo", GetCcbAcNo());
                    Sm.CmParam<String>(ref cm, "@AcNo", GetAcNoParent());
                    //IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("Select distinct AcNo From TblCoa ");
                SQL.AppendLine("    Where Parent Is Not Null ");
                //SQL.AppendLine("    Where 1=1 ");
                foreach (var x in mlAcNo.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " AcNo=@AcNo" + i.ToString() + " ";

                    Sm.CmParam<String>(ref cm, "@AcNo" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                IsCompleted = true;
            }
            SQL.AppendLine("Order By AcNo;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlAcNo.Add(AcNo);
                        else
                        {
                            AcNo = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlAcNo.Where(w => Sm.CompareStr(w, AcNo)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlAcNo.Add(AcNo);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private string GetCcbAcNo()
        {
            string Value = string.Empty;

            Value = Sm.GetCcb(CcbAcNo);
            if (Value.Length > 0)
            {
                Value = GetAcNo(Value);
                Value = Value.Replace(", ", ",");
            }

            return Value;
        }

        private string GetAcNo(string Value)
        {
            if (Value.Length != 0)
            {
                string initValue = Value;

                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(Distinct T.AcNo Separator ',') EntCode ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select AcNo From TblCOA ");
                SQL.AppendLine("    Where Find_In_Set(Concat(AcNo, ' [', AcDesc, ']'), @Param) ");
                SQL.AppendLine(") T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
                if (initValue.Contains("Consolidate"))
                    Value = string.Concat("Consolidate, ", Value);
            }

            return Value;
        }
        
        private string GetAcNoParent()
        {
            string Value = string.Empty;
            var SQL1 = new StringBuilder();
            string AcNo = Sm.GetCcb(CcbAcNo);

            if(AcNo.Length > 0)
            {
                AcNo = GetAcNo(AcNo);
                AcNo = AcNo.Replace(", ", ",");
            }

            if (AcNo.Length > 0)
            {
                //string initValue = Value;

                //Value = Value.Replace(", ", ",");

                SQL1.AppendLine("Select GROUP_CONCAT(distinct parent SEPARATOR ',') Parent ");
                SQL1.AppendLine("From tblcoa ");
                SQL1.AppendLine("Where AcNo Like '" + AcNo + "%'; ");

                Value = Sm.GetValue(SQL1.ToString());
                //if (initValue.Contains("Consolidate"))
                //    Value = string.Concat("Consolidate, ", Value);
            }

            return Value;
        }

        private void SetLueAcNo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo As Col1, Concat(AcNo, ' [ ', AcDesc, ' ]') As Col2 ");
            SQL.AppendLine("From TblCOA Order By AcNo;");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private bool IsMultiCOAInvalid()
        {
            if (mIsPrintOutGeneralLedgerUseMultiCOA && !mIsGeneralLedgerUseCOADigit && Sm.GetCcb(CcbAcNo).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "COA's Account still empty.");
                CcbAcNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsCOAFilterInvalid()
        {
            if (mIsGeneralLedgerUseCOADigit && (Sm.GetCcb(CcbAcNo).Length == 0 && Sm.GetLue(LueCOAAc).Length == 0) &&
                TxtAcNo1.Text.Length == 0 && TxtAcNo2.Text.Length == 0 && TxtAcNo3.Text.Length == 0 && TxtAcNo4.Text.Length == 0 &&
                TxtAcNo5.Text.Length == 0 && TxtAcNo6.Text.Length == 0 && TxtAcNo7.Text.Length == 0 && TxtAcNo8.Text.Length == 0 &&
                TxtAcNo9.Text.Length == 0 && TxtAcNo10.Text.Length == 0 && TxtAcNo11.Text.Length == 0 && TxtAcNo12.Text.Length == 0 &&
                TxtAcNo13.Text.Length == 0 && TxtAcNo14.Text.Length == 0
                )
            {
                Sm.StdMsg(mMsgType.Warning, "COA's Account still empty.");
                if (mIsPrintOutGeneralLedgerUseMultiCOA)
                    CcbAcNo.Focus();
                else
                    LueCOAAc.Focus();
                return true;
            }

            if (mIsGeneralLedgerUseCOADigit && (Sm.GetCcb(CcbAcNo).Length > 0 || Sm.GetLue(LueCOAAc).Length > 0) &&
                (TxtAcNo1.Text.Length > 0 || TxtAcNo2.Text.Length > 0 || TxtAcNo3.Text.Length > 0 || TxtAcNo4.Text.Length > 0 ||
                TxtAcNo5.Text.Length > 0 || TxtAcNo6.Text.Length > 0 || TxtAcNo7.Text.Length > 0 || TxtAcNo8.Text.Length > 0 ||
                TxtAcNo9.Text.Length > 0 || TxtAcNo10.Text.Length > 0 || TxtAcNo11.Text.Length > 0 || TxtAcNo12.Text.Length > 0 ||
                TxtAcNo13.Text.Length > 0 || TxtAcNo14.Text.Length > 0)
                )
            {
                Sm.StdMsg(mMsgType.Warning, "You can only use one COA's Account filter.");
                if (mIsPrintOutGeneralLedgerUseMultiCOA)
                    CcbAcNo.Focus();
                else
                    LueCOAAc.Focus();
                return true;
            }

            return false;
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private string GetProfitCenter()
        {
            string ProfitCenterCode = string.Empty;
            string ProfitCenterName = string.Empty;

            //string test = GetCcbProfitCenterCode();

            string[] ProfitCenterChild = GetCcbProfitCenterCode().Split(',');

            string[] ProfitCenterParent = Sm.GetValue("SELECT GROUP_CONCAT(DISTINCT A.Parent) as Parent " +
                                                      "FROM tblprofitcenter A " +
                                                      "WHERE FIND_IN_SET (A.ProfitCenterCode, '" + GetCcbProfitCenterCode() + "' ) ").Split(',');

            List<string> ProfitCenterChildShow = new List<string>();
            List<string> ProfitCenterShow = new List<string>();

            
            int TotChild = 0;
            int TotParent = 0;

            if(GetCcbProfitCenterCode().Length > 0)
            {
                for (int x = 0; x < ProfitCenterParent.Length; x++)
                {
                    string Parent = Sm.GetValue("SELECT COUNT(A.Parent) AS Jumlah " +
                                                "FROM tblprofitcenter A " +
                                                "WHERE A.Parent = '" + ProfitCenterParent[x] + "' " +
                                                "GROUP BY A.Parent");
                    if(Parent.Length > 0)
                    {
                        TotParent = Convert.ToInt32(Parent);
                    }

                    for (int y = 0; y < ProfitCenterChild.Length; y++)
                    {
                        if (Sm.GetValue("SELECT Parent FROM tblprofitcenter where ProfitCenterCode = '" + ProfitCenterChild[y] + "' ") == ProfitCenterParent[x])
                        {
                            TotChild += 1;
                            ProfitCenterChildShow.Add(ProfitCenterChild[y]);
                        }
                        else if (Sm.GetValue("SELECT Parent FROM tblprofitcenter where ProfitCenterCode = '" + ProfitCenterChild[y] + "' ").Length == 0)
                        {
                            ProfitCenterShow.Add(ProfitCenterChild[y]);
                        }
                    }

                    if (TotChild == TotParent)
                    {
                        ProfitCenterShow.Add(ProfitCenterParent[x]);
                    }
                    else
                    {
                        ProfitCenterShow.AddRange(ProfitCenterChildShow);
                    }
                    TotChild = 0;
                    ProfitCenterChildShow.Clear();
                }

                ProfitCenterCode = string.Join(",", ProfitCenterShow.Distinct().ToArray());
                ProfitCenterName = Sm.GetValue("SELECT GROUP_CONCAT(A.ProfitCenterName SEPARATOR ', ') AS ProfitCenterName " +
                                               "FROM tblprofitcenter A " +
                                               "WHERE FIND_IN_SET(A.ProfitCenterCode, '" + ProfitCenterCode + "')");
            }
            else
            {
                ProfitCenterName = Sm.GetValue("SELECT A.ProfitCenterName " +
                                               "FROM tblprofitcenter A " +
                                               "WHERE A.ProfitCenterCode = '1' ");
            }
            

            return ProfitCenterName;
        }

        private string GetValue(string SQL, string Param1, string Param2, string Param3, string Param4, string Param5, string Param6, string Param7,
            string Param8, string Param9, string Param10, string Param11, string Param12, string Param13, string Param14)
        {
            Object Value;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand() { Connection = cn, CommandText = SQL };
                Sm.CmParam<String>(ref cm, "@Param1", "%"+Param1+"%");
                Sm.CmParam<String>(ref cm, "@Param2", "%"+Param2+"%");
                Sm.CmParam<String>(ref cm, "@Param3", "%"+Param3+"%");
                Sm.CmParam<String>(ref cm, "@Param4", "%"+Param4+"%");
                Sm.CmParam<String>(ref cm, "@Param5", "%"+Param5+"%");
                Sm.CmParam<String>(ref cm, "@Param6", "%"+Param6+"%");
                Sm.CmParam<String>(ref cm, "@Param7", "%"+Param7+"%");
                Sm.CmParam<String>(ref cm, "@Param8", "%"+Param8+"%");
                Sm.CmParam<String>(ref cm, "@Param9", "%"+Param9+"%");
                Sm.CmParam<String>(ref cm, "@Param10", "%"+Param10+"%");
                Sm.CmParam<String>(ref cm, "@Param11", "%"+Param11+"%");
                Sm.CmParam<String>(ref cm, "@Param12", "%"+Param12+"%");
                Sm.CmParam<String>(ref cm, "@Param13", "%"+Param13+"%");
                Sm.CmParam<String>(ref cm, "@Param14", "%"+Param14+"%");
                Value = cm.ExecuteScalar();
            }
            return (Value == null ? string.Empty : Value.ToString());
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        #region COA filter

        private void TxtAcNo1_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo1.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo2_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo2.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo3_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo3.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo4_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo4.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo5_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo5.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo6_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo6.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo7_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo7.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo8_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo8.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo9_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo9.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo10_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo10.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo11_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo11.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo12_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo12.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo13_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo13.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void TxtAcNo14_Validated(object sender, EventArgs e)
        {
            if (TxtAcNo14.Text.Length > 0)
                ChkAcNo2.Checked = true;
        }

        private void ChkAcNo2_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkAcNo2.Checked)
            {
                TxtAcNo1.Text = TxtAcNo2.Text = TxtAcNo3.Text = TxtAcNo4.Text = TxtAcNo5.Text = TxtAcNo6.Text = TxtAcNo7.Text = string.Empty;
                TxtAcNo8.Text = TxtAcNo9.Text = TxtAcNo10.Text = TxtAcNo11.Text = TxtAcNo12.Text = TxtAcNo13.Text = TxtAcNo14.Text = string.Empty;
            }
            if (ChkAcNo2.Checked && TxtAcNo1.Text.Length == 0 && TxtAcNo2.Text.Length == 0 && TxtAcNo3.Text.Length == 0 && TxtAcNo4.Text.Length == 0 &&
                TxtAcNo5.Text.Length == 0 && TxtAcNo6.Text.Length == 0 && TxtAcNo7.Text.Length == 0 && TxtAcNo8.Text.Length == 0 &&
                TxtAcNo9.Text.Length == 0 && TxtAcNo10.Text.Length == 0 && TxtAcNo11.Text.Length == 0 && TxtAcNo12.Text.Length == 0 &&
                TxtAcNo13.Text.Length == 0 && TxtAcNo14.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "COA's Account Digit is empty.");
                ChkAcNo2.Checked = false;
            }
        }

        #endregion

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void CcbAcNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi COA's account");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }
        #endregion

        #endregion

        #region Class

        private class Data1
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal OpeningBalance { get; set; }
            public decimal OpeningBalanceGrp { get; set; }
            public string JnDesc { get; set; }
            public string ProfitCenterCode { get; set; }
            public string ProfitCenterName { get; set; }
        }

        private class Data2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal OpeningBalance { get; set; } //saldo awal
            public int SeqNo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Opening { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
            public string Remark { get; set; }
            public string JnDesc { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string ProfitCenterCode { get; set; }
            public string ProfitCenterName { get; set; }
            public string AcType { get; set; }
            public string ProfitCenterCodeGrp { get; set; }
            public string ProfitCenterNameGrp { get; set; }
            public string OBInd { get; set; }
        }

        private class GeneralLedger
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal OpeningBalance { get; set; }
            public string OpeningBalanceShow { get; set; }
            public int SeqNo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Opening { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Balance { get; set; }
            public string DAmtShow { get; set; }
            public string CAmtShow { get; set; }
            public string BalanceShow { get; set; }
            public string Remark { get; set; }
            public string JnDesc { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string ProfitCenterCode { get; set; }
            public string ProfitCenterName { get; set; }
            public string ProfitCenterCodeGrp { get; set; }
            public string ProfitCenterNameGrp { get; set; }
            public string OBInd { get; set; }
        }

        private class Logo
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string PrintBy { get; set; }
            public string ProfitCenterName { get; set; }
            public decimal GrandTotal { get; set; }
            public string GrandTotalShow { get; set; }
        }

        private class SelectedProfitCenter
        { 
            public string ProfitCenterCode { get; set; }
            public string ProfitCenterName { get; set; }
        }

        private class SelectedCOA
        { 
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        #endregion

    }
}
