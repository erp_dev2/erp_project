﻿#region Update
/*
    17/07/2017 [TKG] tambah journal antar entity
    26/09/2017 [HAR] validasi tidak bisa backdate
    15/04/2018 [TKG] ubah proses journal untuk beda entity
    17/04/2018 [TKG] ubah proses journal untuk beda entity menjadi 2 dokumen
    17/07/2018 [TKG] tambah cost center saat journal
    27/08/2019 [WED] tambah tab kawasan berikat, BC261
    14/10/2019 [WED/MAI] masuk ke stock movement, kolom CustomsDocCode
    23/10/2019 [DITA/MAI] tambah KBContractNo, KBSubmissionNo, KBRegistrationNo saat save
    16/12/2019 [TKG/IMS] journal untuk moving average
    25/08/2020 [ICA/MGI] Tambah Field Group parameter IsUseProductionWorkGroup
    08/09/2020 [DITA/MAI] menambahkan field Nilai Jaminan (Insurance Amount), nomor SKEP (Decree#), tanggal SKEP (Decree Date) di Tab Kawasan Berikat
    10/09/2020 [WED/IOK] KB tidak diinput selain MAI
    01/03/2023 [RDA/MNET] approval RecvWhs based on parameter IsRecvWhsUseApproval
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mKBInsuranceNo = string.Empty,
            mKBInsuranceDt = string.Empty,
            mKBInsuranceDueDt = string.Empty;
        internal FrmRecvWhsFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string 
            mDocType = "10", 
            mDocType2 = "09",
            mRecvWhsCustomsDocCodeManualInput = string.Empty;
        internal bool 
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false;
        private bool 
            mIsStockInfoEditable = false,
            mIsAutoJournalActived = false, 
            mIsEntityMandatory = false,
            mIsKawasanBerikatEnabled = false, 
            mIsUseProductionWorkGroup = false,
            mIsMovingAvgEnabled = false,
            mIsRecvWhsUseApproval = false,
            mIsRecvWhsApprovalBasedOnWhs = false
            ;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvWhs(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Received Item From Other Warehouse";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled) Tp2.PageVisible = true; else Tp2.PageVisible = false;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin, string.Empty, string.Empty);
                LueLot.Visible = false;
                LueBin.Visible = false;

                if (mIsKawasanBerikatEnabled)
                {
                    TcRecvWhs.SelectedTabPage = Tp2;
                    Sl.SetLueOption(ref LueCustomsDocCode, "CustomsDocCode261");
                }

                TcRecvWhs.SelectedTabPage = Tp1;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (!mIsUseProductionWorkGroup) panel4.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        
        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "DO#",
                        "DO#"+Environment.NewLine+"DNo",

                        //6-10
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",
                        
                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Stock",
                        "Stock",

                        //26-30
                        "Stock",
                        "Group",
                        "Foreign Name",
                        "Batch#",
                        "Source",

                        //31-33
                        "Lot",
                        "Bin", 
                        "DocDt"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 150, 0, 
                        
                        //6-10
                        20, 100, 20, 200, 150, 
                        
                        //11-15
                        150, 60, 60, 100, 100,
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-25
                        100, 100, 200, 80, 80,

                        //26-30
                        80, 100, 230, 0, 0,

                        //31-33
                        0, 0, 100
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 6, 8 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 7, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 });
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 5, 7, 8, 11, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 33 }, false);
                Grd1.Cols[28].Move(10);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 5, 7, 8, 11, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, false);
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[27].Move(7);
            }
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, ChkKBNonDocInd, LueCustomsDocCode,
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty, LueProductionWorkGroup,
                        DteKBDecreeDt, TxtKBDecreeNo, TxtKBInsuranceAmt
                    }, true);
                    BtnKBContractNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 15, 18, 21, 23 });
                    if (mIsStockInfoEditable)
                        Sm.GrdColReadOnly(true, true, Grd1, new int[] { 10, 12, 13 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, ChkKBNonDocInd, LueCustomsDocCode, LueProductionWorkGroup }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23 });
                    if (mIsStockInfoEditable)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 10, 12, 13 });
                    DteDocDt.Focus();
                    if (mIsKawasanBerikatEnabled) BtnKBContractNo.Enabled = false;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark,
                TxtJournalDocNo, TxtJournalDocNo2, LueLot, LueBin, LueCustomsDocCode,
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                LueProductionWorkGroup, DteKBDecreeDt, TxtKBDecreeNo, TxtKBInsuranceAmt
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            ChkKBNonDocInd.Checked = false;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvWhsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "RecvWhsHdr", "RecvWhsDtl" };

            var l = new List<RecvWhsHdr>();
            var ldtl = new List<RecvWhsDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y') As DocDt,B.WhsName,C.WhsName as WhsName2,A.Remark, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2=C.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                 //0
                 "Company",
                 //1-5
                 "Address",
                 "Phone",
                 "DocNo",
                 "DocDt",
                 "WhsName",
                 //6-9
                 "WhsName2",
                 "Remark",
                 "CompanyLogo",
                 "IsForeignName",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvWhsHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            WhsName = Sm.DrStr(dr, c[5]),
                            WhsName2 = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            CompanyLogo = Sm.DrStr(dr, c[8]),
                            IsForeignName = Sm.DrStr(dr, c[9]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDate(Sm.ServerCurrentDateTime()))

                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;


                SQLDtl.AppendLine("Select A.DOWhsDocNo,Date_Format(D.DocDt,'%d %M %Y') As DocDt, ");
                SQLDtl.AppendLine("B.ItCode,C.ItName,C.ForeignName,B.BatchNo,B.Lot,B.Bin,A.Qty,A.Qty2,A.Qty3, ");
                SQLDtl.AppendLine("C.InventoryUOMCode,C.InventoryUOMCode2,C.InventoryUOMCode3,A.Remark, C.ItGrpCode ");
                SQLDtl.AppendLine("From TblRecvWhsDtl A ");
                SQLDtl.AppendLine("Inner Join TblDOWhsDtl B On Concat(A.DOWhsDocNo,A.DOWhsDNo)=Concat(B.DocNo,B.DNo) ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Inner Join TblDOWhsHdr D On A.DOWhsDocNo= D.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo;");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DOWhsDocNo",

                         //1-5
                         "ItCode",
                         "ItName",
                         "BatchNo",
                         "Lot",
                         "Bin",

                         //6-10
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",
                         "InventoryUOMCode2",

                         //11-15
                         "InventoryUOMCode3",
                         "DocDt",
                         "Remark",
                         "ItGrpCode",
                         "ForeignName"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RecvWhsDtl()
                        {
                            DOWhsDocNo = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            DocDt = Sm.DrStr(drDtl, cDtl[12]),
                            Remark = Sm.DrStr(drDtl, cDtl[13]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[14]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[15]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            int a = int.Parse(ParValue);
            if (a == 1)
            {
                Sm.PrintReport("RecvWhs1", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("RecvWhs2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("RecvWhs3", myLists, TableName, false);
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from other warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvWhsDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueWhsCode2)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 10, 12, 13, 15, 18, 21, 23 }, e.ColIndex))
                    {
                        if (e.ColIndex == 12) LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                        if (e.ColIndex == 13) LueBinRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOWhs(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from other warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvWhsDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueWhsCode2)));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmDOWhs(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 10, 12, 13, 23 }, e);

            if (Sm.IsGrdColSelected(new int[] { 10, 12, 13 }, e.ColIndex) && 
                Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = "-";

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 7, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 7, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 7, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 7, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvWhs", "TblRecvWhsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvWhsHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvWhsDtl(DocNo, Row, IsDocApprovalSettingNotExisted()));

            if (!mIsRecvWhsUseApproval)
            {
                cml.Add(SaveStockMovement(DocNo, mIsKawasanBerikatEnabled));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0) cml.Add(SaveStockSummary(Row));

                cml.Add(UpdateDOWhsProcessInd(DocNo));

                if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal(DocNo, mIsMovingAvgEnabled));
            }
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock(Sm.GetLue(LueWhsCode2));
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (From)") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (To)") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group"))||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                BackDateValidate()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool BackDateValidate()
        {
            int DateRecv = Convert.ToInt32(Sm.GetDte(DteDocDt).Substring(0, 8));
            int DO = 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                 if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0) 
                 {
                    DO = Convert.ToInt32(Sm.GetGrdStr(Grd1, Row, 33));
                    if (DateRecv < DO)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Received date should not be earlier than DO date (" + Sm.GetGrdStr(Grd1, Row, 4) + ").");
                        return true;
                    }
                 }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "DO# is empty.")) return true;
                if (mIsStockInfoEditable)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 12, false, "Lot is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 13, false, "Bin is empty.")) return true;
                }
                Msg =
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (IsDOWhsAlreadyCancelled(Msg, Row)) return true;
                if (IsDOWhsAlreadyReceived(Msg, Row)) return true;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 14))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than DO quantity.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 24))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should not be bigger than DO quantity (2).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 25))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 20))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should not be bigger than DO quantity (3).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 26))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDOWhsAlreadyCancelled(string Msg, int Row)
        { 
            var cm = new MySqlCommand()
            { CommandText = "Select DocNo From TblDOWhsDtl Where DocNo=@DocNo And DNo=@DNo And CancelInd='Y';" };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 5));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDOWhsAlreadyReceived(string Msg, int Row)
        {
            var cm = new MySqlCommand()
            { CommandText = "Select DocNo From TblDOWhsDtl Where DocNo=@DocNo And DNo=@DNo And ProcessInd='F';" };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 5));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "This data already received.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRecvWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvWhsHdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, ");
            SQL.AppendLine("CustomsDocCode, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, ");
            SQL.AppendLine("KBRegistrationDt, KBSubmissionNo, KBPackaging, KBPackagingQty, KBNonDocInd, ");
            SQL.AppendLine("KBInsuranceNo, KBInsuranceDt, KBInsuranceDueDt, KBInsuranceAmt, KBDecreeNo, KBDecreeDt, ");
            SQL.AppendLine("ProductionWorkGroup, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, ");
            SQL.AppendLine("@CustomsDocCode, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, ");
            SQL.AppendLine("@KBRegistrationDt, @KBSubmissionNo, @KBPackaging, @KBPackagingQty, @KBNonDocInd, ");
            SQL.AppendLine("@KBInsuranceNo, @KBInsuranceDt, @KBInsuranceDueDt, @KBInsuranceAmt, @KBDecreeNo, @KBDecreeDt, ");
            SQL.AppendLine("@ProductionWorkGroup, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", Sm.GetLue(LueCustomsDocCode));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@KBPackaging", TxtKBPackaging.Text);

            if (TxtKBPackagingQty.Text.Length == 0) Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", 0m);
            else Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", decimal.Parse(TxtKBPackagingQty.Text));
            
            Sm.CmParam<String>(ref cm, "@KBNonDocInd", ChkKBNonDocInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            if (mIsKawasanBerikatEnabled)
            {
                Sm.CmParam<String>(ref cm, "@KBInsuranceNo", mKBInsuranceNo);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDt", mKBInsuranceDt);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDueDt", mKBInsuranceDueDt);
                Sm.CmParam<Decimal>(ref cm, "@KBInsuranceAmt", TxtKBInsuranceAmt.Text.Length > 0 ? decimal.Parse(TxtKBInsuranceAmt.Text) : 0m);
                Sm.CmParam<String>(ref cm, "@KBDecreeNo", TxtKBDecreeNo.Text);
                Sm.CmParamDt(ref cm, "@KBDecreeDt", Sm.GetDte(DteKBDecreeDt));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@KBInsuranceNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDt", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDueDt", string.Empty);
                Sm.CmParam<Decimal>(ref cm, "@KBInsuranceAmt", 0m);
                Sm.CmParam<String>(ref cm, "@KBDecreeNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBDecreeDt", string.Empty);
            }
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvWhsDtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvWhsDtl(DocNo, DNo, CancelInd, Status, DOWhsDocNo, DOWhsDNo, ");
            SQL.AppendLine("BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("BatchNoOld, SourceOld, LotOld, BinOld, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @Status, @DOWhsDocNo, @DOWhsDNo, ");
            SQL.AppendLine("@BatchNo, @Source, @Lot, @Bin, ");
            SQL.AppendLine("@BatchNoOld, @SourceOld, @LotOld, @BinOld, ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='RecvWhs' ");
                if (mIsRecvWhsApprovalBasedOnWhs)
                    SQL.AppendLine("And IfNull(T.WhsCode, '')=@WhsCode ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            string DNo = Sm.Right("00" + (Row + 1).ToString(), 3);

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@DOWhsDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DOWhsDNo", Sm.GetGrdStr(Grd1, Row, 5));
            if (
                Sm.GetGrdStr(Grd1, Row, 10) != Sm.GetGrdStr(Grd1, Row, 29) ||
                Sm.GetGrdStr(Grd1, Row, 12) != Sm.GetGrdStr(Grd1, Row, 31) ||
                Sm.GetGrdStr(Grd1, Row, 13) != Sm.GetGrdStr(Grd1, Row, 32) 
                )
            {
                Grd1.Cells[Row, 11].Value = string.Concat(mDocType, "*", DocNo, "*", DNo);
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
                Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
                Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 29));
                Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 31));
                Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 32));
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 30));
            }
            Sm.CmParam<String>(ref cm, "@BatchNoOld", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@LotOld", Sm.GetGrdStr(Grd1, Row, 31));
            Sm.CmParam<String>(ref cm, "@BinOld", Sm.GetGrdStr(Grd1, Row, 32));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

            return cm;
        }

        public static MySqlCommand SaveStockMovement(string DocNo, bool IsKawasanBerikatEnabled)
        {
            var SQL = new StringBuilder();

            //Stock movement untuk gudang yg kirim
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, C.DocNo, C.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("D.DocDt, A.WhsCode2, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblDOWhsHdr D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            //Stock movement untuk gudang yg terima (Yg info stock-nya sama)
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.SourceOld=B.Source ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            //Stock movement untuk gudang yg terima (Yg info stock-nya beda)
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot, B.Bin, C.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (IsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.SourceOld<>B.Source ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            //Stock summary untuk barang yg diterima tapi belum pernah ada info stock di gudang tsb
            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, C.ItCode, B.BatchNo, B.Source, 0, 0, 0, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhsHdr A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select WhsCode From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode = A.WhsCode ");
            SQL.AppendLine("    And Lot=B.Lot ");
            SQL.AppendLine("    And Bin=B.Bin ");
            SQL.AppendLine("    And Source=B.Source ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "10"); // ambil dari tbloption (InventoryTransType) : Receiving Item From Other Warehouse
            Sm.CmParam<String>(ref cm, "@DocType2", "09"); // ambil dari tbloption (InventoryTransType) : DO To Other Warehouse
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And ItCode=@ItCode ");
            SQL.AppendLine("And BatchNo=@BatchNo ");
            SQL.AppendLine("And Source=@Source ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin;");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode2 ");
            SQL.AppendLine("And ItCode=@ItCode ");
            SQL.AppendLine("And BatchNo=@BatchNoOld ");
            SQL.AppendLine("And Source=@SourceOld ");
            SQL.AppendLine("And Lot=@LotOld ");
            SQL.AppendLine("And Bin=@BinOld;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@BatchNoOld", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 30));
            Sm.CmParam<String>(ref cm, "@LotOld", Sm.GetGrdStr(Grd1, Row, 31));
            Sm.CmParam<String>(ref cm, "@BinOld", Sm.GetGrdStr(Grd1, Row, 32));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public static MySqlCommand SaveStockSummaryWithApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On B.DocNo = @DocNo ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And A.BatchNo = B.BatchNo ");
            SQL.AppendLine("And A.Source = B.Source ");
            SQL.AppendLine("And A.Lot = B.Lot ");
            SQL.AppendLine("And A.Bin = B.Bin ");
            SQL.AppendLine("Inner Join TblRecvWhsHdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join tblDOWhsDtl D ON B.DOWhsDocNo=D.DocNo AND B.DOWhsDNo=D.DNo ");
            SQL.AppendLine("And A.ItCode = D.ItCode ");
            SQL.AppendLine("And A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Set A.Qty = A.Qty + B.Qty, A.Qty2 = A.Qty2 + B.Qty2, A.Qty3 = A.Qty3 + B.Qty3, ");
            SQL.AppendLine("A.LastUpBy = B.CreateBy, A.LastUpDt = CurrentDateTime(); ");

            SQL.AppendLine("Update TblStockSummary A ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On B.DocNo = @DocNo ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And A.BatchNo = B.BatchNoOld ");
            SQL.AppendLine("And A.Source = B.SourceOld ");
            SQL.AppendLine("And A.Lot = B.LotOld ");
            SQL.AppendLine("And A.Bin = B.BinOld ");
            SQL.AppendLine("Inner Join TblRecvWhsHdr C On B.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join tblDOWhsDtl D ON B.DOWhsDocNo=D.DocNo AND B.DOWhsDNo=D.DNo ");
            SQL.AppendLine("And A.ItCode = D.ItCode ");
            SQL.AppendLine("And A.WhsCode = C.WhsCode2 ");
            SQL.AppendLine("Set A.Qty = A.Qty - B.Qty, A.Qty2 = A.Qty2 - B.Qty2, A.Qty3 = A.Qty3 - B.Qty3, ");
            SQL.AppendLine("A.LastUpBy = B.CreateBy, A.LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public static MySqlCommand UpdateDOWhsProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhsDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo ");
            SQL.AppendLine("    From TblRecvWhsHdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
            SQL.AppendLine("Set T1.ProcessInd='F', T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where T1.CancelInd='N'; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        public static MySqlCommand SaveJournal(string DocNo, bool IsMovingAvgEnabled)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhsHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo, ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            #region Journal 1

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhsHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (IsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo5 As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(T.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='N' ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo4 As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo6 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo5 As AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(T.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*IfNull(G.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='Y' ");
                SQL.AppendLine("            Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo4 As AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo6 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo5 As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(T.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo4 As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("        D.AcNo6 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo2, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhsHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo2 Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (IsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='N' ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        D.AcNo3 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*IfNull(G.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='Y' ");
                SQL.AppendLine("            Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        D.AcNo3 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        T.AcNo, ");
                SQL.AppendLine("        Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select F.AcNo, ");
                SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("            From TblRecvWhsHdr A ");
                SQL.AppendLine("            Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("            Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("            Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("        D.AcNo3 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvWhsHdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            string DocDt = string.Empty, WhsCode = string.Empty, WhsCode2 = string.Empty;

            DocDt = Sm.GetValue("Select DocDt From TblRecvWhsHdr Where DocNo=@Param", DocNo);
            WhsCode = Sm.GetValue("Select WhsCode From TblRecvWhsHdr Where DocNo=@Param", DocNo);
            WhsCode2 = Sm.GetValue("Select WhsCode2 From TblRecvWhsHdr Where DocNo=@Param", DocNo);

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(DocDt, 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select Menucode From TblMenu Where Param = 'FrmRecvWhs' Limit 1; "));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(WhsCode));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(WhsCode2));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(WhsCode));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(WhsCode2));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRecvWhsHdr(DocNo);
                ShowRecvWhsDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, WhsCode, WhsCode2, Remark, JournalDocNo, JournalDocNo2, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("KBPackaging, KBPackagingQty, KBNonDocInd, KBSubmissionNo, CustomsDocCode, ProductionWorkGroup, ");
            SQL.AppendLine("KBInsuranceAmt, KBDecreeNo, KBDecreeDt ");
            SQL.AppendLine("From TblRecvWhsHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        "DocNo", 
                        "DocDt", "WhsCode", "WhsCode2", "Remark", "JournalDocNo", 
                        "JournalDocNo2", "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt",
                        "KBRegistrationNo", "KBRegistrationDt", "KBPackaging", "KBPackagingQty", "KBNonDocInd",
                        "KBSubmissionNo", "CustomsDocCode", "ProductionWorkGroup", "KBInsuranceAmt", "KBDecreeNo" ,
                        "KBDecreeDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[6]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[8]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[10]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[12]));
                        TxtKBPackaging.EditValue = Sm.DrStr(dr, c[13]);
                        TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        ChkKBNonDocInd.Checked = Sm.DrStr(dr, c[15]) == "Y";
                        TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetLue(LueCustomsDocCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[18]));
                        TxtKBInsuranceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                        TxtKBDecreeNo.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteKBDecreeDt, Sm.DrStr(dr, c[21]));
                    }, true
                );
        }

        private void ShowRecvWhsDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.DOWhsDocNo, A.DOWhsDNo, ");
            SQL.AppendLine("B.ItCode, C.ItName, C.ForeignName, ");
            SQL.AppendLine("IfNull(A.BatchNo, B.BatchNo) As BatchNo, ");
            SQL.AppendLine("IfNull(A.Source, B.Source) As Source, ");
            SQL.AppendLine("IfNull(A.Lot, B.Lot) As Lot, ");
            SQL.AppendLine("IfNull(A.Bin, B.Bin) As Bin, ");
            SQL.AppendLine("B.Qty As DOQty, A.Qty, B.Qty2 As DOQty2, A.Qty2, B.Qty3 As DOQty3, A.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, A.Remark, C.ItGrpCode, ");
            SQL.AppendLine("A.BatchNoOld, A.SourceOld, A.LotOld, A.BinOld ");
            SQL.AppendLine("From TblRecvWhsDtl A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DOWhsDocNo=B.DocNo And A.DOWhsDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "DOWhsDocNo", "DOWhsDNo", "ItCode", "ItName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "DOQty",  
                        
                        //11-15
                        "Qty", "InventoryUomCode", "DOQty2", "Qty2", "InventoryUomCode2", 
                        
                        //16-20
                        "DOQty3", "Qty3", "InventoryUomCode3", "Remark", "ItGrpCode",

                        //21-25
                        "ForeignName", "BatchNoOld", "SourceOld", "LotOld", "BinOld"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 25);
                }, false, false, true, false
        );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocType From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is not Null ");
            SQL.AppendLine("And DocType='RecvWhs' ");
            if (mIsRecvWhsApprovalBasedOnWhs)
            {
                SQL.AppendLine("And WhsCode = @WhsCode ");
            }
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

            if (!Sm.IsDataExist(cm))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }
        private bool IsCustomsDocCodeInputManual()
        {
            if (mRecvWhsCustomsDocCodeManualInput.Length > 0)
            {
                var CustomsDocCode = Sm.GetLue(LueCustomsDocCode);
                if (CustomsDocCode.Length == 0) return false;
                var l = new List<string>(mRecvWhsCustomsDocCodeManualInput.Split(',').Select(s => s));
                foreach (var s in l)
                    if (Sm.CompareStr(CustomsDocCode, s)) return true;
            }
            return false;
        }

        public static string GetCostCenterWarehouse(string WhsCode)
        {
            return Sm.GetValue("Select CCCode From TblWarehouse Where WhsCode=@Param;", WhsCode);
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2));
            return (EntCode1 != EntCode2);
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));
            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void LueBinRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            var Bin = Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex);
            SetLueBin(ref LueBin, Sm.GetGrdStr(Grd, fCell.RowIndex, 12), Bin);
            if (Bin.Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Bin);
            
            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue, string Lot, string Bin)
        {
            var Filter = string.Empty;
            var cm = new MySqlCommand();

            if (Lot.Length > 0)
            {
                Filter = " And Bin In (Select Bin From TblLotDtl Where Lot=@Lot) ";
                Sm.CmParam<String>(ref cm, "@Lot", Lot);
            }
            else
                Filter = " And 1=0 ";

            if (Bin.Length > 0) Sm.CmParam<String>(ref cm, "@Bin", Bin);

            cm.CommandText = 
                "Select Distinct Bin As Col1 from TblBin " +
                "Where ActInd='Y' " + Filter + 
                ((Bin.Length==0)?"":"Union All Select @Bin As Bin ") +
                " Order By Col1;";

            Sm.SetLue1(ref Lue, ref cm, "Bin");
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
            mIsStockInfoEditable = Sm.GetParameter("IsStockInfoEditable") == "Y";
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mRecvWhsCustomsDocCodeManualInput = Sm.GetParameter("RecvWhsCustomsDocCodeManualInput");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            mIsRecvWhsUseApproval = Sm.GetParameterBoo("IsRecvWhsUseApproval");
            mIsRecvWhsApprovalBasedOnWhs = Sm.GetParameterBoo("IsRecvWhsApprovalBasedOnWhs");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedDO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock(string WhsCode)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 30).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 30);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ") ";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 30), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 31), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 32), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 24, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 25, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 26, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Click

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWhsCode2, "From") && !Sm.IsLueEmpty(LueCustomsDocCode, "Customs document code"))
                Sm.FormShowDialog(new FrmRecvWhsDlg2(this, Sm.GetLue(LueCustomsDocCode)));
        }

        #endregion

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
                ClearGrd();
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            var Lot = Sm.GetGrdStr(Grd1, fCell.RowIndex, 12);
            Sm.RefreshLookUpEdit(
                LueBin, 
                new Sm.RefreshLue3(SetLueBin), Lot, string.Empty);
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 13].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueLot.GetColumnValue("Col1");
                }
                Grd1.Cells[fCell.RowIndex, 13].Value = null;
                LueLot.Visible = false;
            }
        }

        private void LueCustomsDocCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCustomsDocCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomsDocCode261");
                if (IsCustomsDocCodeInputManual())
                {
                    BtnKBContractNo.Enabled = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                        TxtKBInsuranceAmt, TxtKBDecreeNo, DteKBDecreeDt
                    }, false);
                }
                else
                {
                    BtnKBContractNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                        TxtKBInsuranceAmt, TxtKBDecreeNo, DteKBDecreeDt
                    }, true);
                }

                if (TxtDocNo.Text.Length == 0)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBDecreeNo, DteKBDecreeDt
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty, TxtKBInsuranceAmt }, 0);
                    ChkKBNonDocInd.Checked = false;
                    ClearGrd();
                }
            }
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
        }

        #endregion

        #endregion

        #region Report Class

        class RecvWhsHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string WhsName2 { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string IsForeignName { get; set; }
            public string PrintBy { get; set; }

        }

        class RecvWhsDtl
        {
            public string DOWhsDocNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string ItGrpCode { get; set; }
        }

        #endregion

    }
}
