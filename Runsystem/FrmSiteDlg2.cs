﻿#region Update
/*
    14/02/2020 [WED/YK] new apps
    15/09/2020 [WED/YK] bisa untuk pilih COA ke detail 2
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSiteDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSite mFrmParent;
        private string mSQL = string.Empty;
        private byte mTarget = 0;
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmSiteDlg2(FrmSite FrmParent, byte Target, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTarget = Target;
            mRow = Row;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of COA";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-3
                    "Account#",
                    "Description",
                    "Type"
                },
                new int[]
                {
                    //0
                    50,

                    //1-4
                    150, 200, 100
                }
            );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcType ");
            SQL.AppendLine("From TblCoa A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("where A.AcNo Not In (Select parent From TblCoa Where parent is not null) ");
            SQL.AppendLine("And A.ActInd='Y' ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.AcDesc" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By A.AcNo; ",
                    new string[] 
                    { 
                        "AcNo", "AcDesc", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                string SelectedAcNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                string SelectedAcDesc = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);

                if (mTarget == 1)
                {
                    mFrmParent.TxtAcNo.EditValue = SelectedAcNo;
                    mFrmParent.TxtAcDesc.EditValue = SelectedAcDesc;
                }
                else
                {
                    mFrmParent.GetAcNoForPOSDetail(SelectedAcNo, SelectedAcDesc, mRow);
                }

                this.Hide();
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        #endregion

        #endregion

    }
}
