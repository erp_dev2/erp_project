﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLegalDocVerify : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmLegalDocVerifyFind FrmFind;
        internal string mVdCode = string.Empty, mVdCode1 = string.Empty, mVdCode2 = string.Empty; 
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmLegalDocVerify(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Verifikasi Legalitas Dokumen";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd(ref Grd1);
                SetGrd(ref Grd2);
                SetFormControl(mState.View);

                tabControl1.SelectTab("tabPage2");
                Sl.SetLueDLCode(ref LueDLCode1);
                LueDLCode1.Visible = false;

                tabControl1.SelectTab("tabPage3");
                Sl.SetLueDLCode(ref LueDLCode2);
                LueDLCode2.Visible = false;

                tabControl1.SelectTab("tabPage1");

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, TxtQueueNo, TxtVehicleRegNo, TxtTTCode,
                        TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode,
                        TxtVdName1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1,
                        TxtVdName2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    BtnQueueNo.Enabled = BtnVdCode.Enabled = BtnVdCode1.Enabled = BtnVdCode2.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt
                    }, false);
                    BtnQueueNo.Enabled = BtnQueueNo.Enabled = BtnVdCode.Enabled = BtnVdCode1.Enabled = BtnVdCode2.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6 });
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtQueueNo, TxtVehicleRegNo, TxtTTCode,
                TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode, 
                TxtVdName1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1, 
                TxtVdName2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2
            });
            mVdCode = mVdCode1 = mVdCode2 = string.Empty;
            ChkCancelInd.Checked = false;
            ClearGrd(ref Grd1);
            ClearGrd(ref Grd2);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetGrd(ref iGrid Grd)
        {
            Grd.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                Grd,
                new string[] { 
                    //0
                    "Code", 
                    
                    //1-5
                    "Document Legalitas", "Nomor Dokumen", "Tgl.Dokumen", "Volume", "Jumlah", 
                    
                    //6
                    "Keterangan" 
                },
                new int[] 
                { 
                    //0
                    0, 

                    //1-5
                    250, 130, 100, 100, 100,
                    
                    //6
                    350 
                }
            );
            Sm.GrdFormatDec(Grd, new int[] { 4, 5 }, 0);
            Sm.GrdFormatDate(Grd, new int[] { 3 });
        }

        private void ClearGrd(ref iGrid Grd)
        {
            Sm.ClearGrd(Grd, true);
            Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 4, 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLegalDocVerifyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);

            tabControl1.SelectTab("tabPage1");
            BtnQueueNo_Click(sender, e);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void GrdRequestEdit(ref iGrid Grd, ref DXE.LookUpEdit LueDLCode, ref DXE.DateEdit DteDLDocDt, object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd, LueDLCode, ref fCell, ref fAccept, e);
                if (e.ColIndex == 3) Sm.DteRequestEdit(Grd, DteDLDocDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 4, 5 });
            }
        }

        private void GrdKeyDown(ref iGrid Grd, object sender, KeyEventArgs e)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select LegalDocVerifyDocNo From TblRawMaterialVerify Where LegalDocVerifyDocNo=@DocNo ",
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already verified.");
            }
            else
            {
                //if (TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd, e, BtnSave);
                Sm.GrdKeyDown(Grd, e, BtnFind, BtnSave);
            }
        }

        private void GrdAfterCommitEdit(ref iGrid Grd, object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd, new int[] { 2, 6 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd, new int[] { 4, 5 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || 
                IsInsertedDataNotValid()) 
                return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LegalDocVerify", "TblLegalDocVerifyHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLegalDocVerifyHdr(DocNo));

            int DNo = 0;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyDtl("1", ref Grd1, ref DNo, DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyDtl("2", ref Grd2, ref DNo, DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtQueueNo, "Nomor antrian", false) ||
                Sm.IsTxtEmpty(TxtVehicleRegNo, "Nomor polisi", false) ||
                Sm.IsTxtEmpty(TxtVdName, "Vendor", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords(ref Grd1) ||
                IsGrdExceedMaxRecords(ref Grd2) ||
                IsGrdValueNotValid(ref Grd1) ||
                IsGrdValueNotValid(ref Grd2) ||
                IsDoubleEntry(ref Grd1) ||
                IsDoubleEntry(ref Grd2) ||
                IsQueueNoNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Kedua data dokumen log dan balok masih kosong.");
                return true;
            }

            if (TxtVdName1.Text.Length!=0 && Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data dokumen log masih kosong.");
                return true;
            }

            if (TxtVdName1.Text.Length == 0 && Grd1.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor log masih kosong.");
                return true;
            }

            if (TxtVdName2.Text.Length != 0 && Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data dokumen balok masih kosong.");
                return true;
            }

            if (TxtVdName2.Text.Length == 0 && Grd2.Rows.Count > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor balok masih kosong.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd, Row, 1, false, "Document is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsDoubleEntry(ref iGrid Grd)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd.Rows.Count - 1; Row1++)
                {
                    if (Sm.GetGrdStr(Grd, Row1, 0).Length != 0)
                    {
                        for (int Row2 = 0; Row2 < Grd.Rows.Count - 1; Row2++)
                        {
                            if (Row1 != Row2 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd, Row1, 0), Sm.GetGrdStr(Grd, Row2, 0)))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Dokumen : " + Sm.GetGrdStr(Grd, Row2, 1) + Environment.NewLine + Environment.NewLine +
                                    "Dokumen dimasukkan lebih dari 1 kali. ");
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }


        private MySqlCommand SaveLegalDocVerifyHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLegalDocVerifyHdr(DocNo, DocDt, CancelInd, ProcessInd1, ProcessInd2, QueueNo, VehicleRegNo, VdCode, VdCode1, VdCode2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', 'O', @QueueNo, @VehicleRegNo, @VdCode, @VdCode1, @VdCode2, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@QueueNo", TxtQueueNo.Text);
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@VdCode1", mVdCode1);
            Sm.CmParam<String>(ref cm, "@VdCode2", mVdCode2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLegalDocVerifyDtl(string DLType, ref iGrid Grd, ref int DNo, string DocNo, int Row)
        {
            DNo += 1;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblLegalDocVerifyDtl(DocNo, DNo, DLType, DLCode, DLDocNo, DLDocDt, Volume, Qty, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DLType, @DLCode, @DLDocNo, @DLDocDt, @Volume, @Qty, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DLType", DLType);
            Sm.CmParam<String>(ref cm, "@DLCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@DLDocNo", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParamDt(ref cm, "@DLDocDt", Sm.GetGrdDate(Grd, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Sm.GetGrdDec(Grd, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if(IsLegalDocAlreadyExist() == true) return;
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelLegalDocVerifyHdr());

            int DNo = 0;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyDtl("1", ref Grd1, ref DNo, TxtDocNo.Text, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveLegalDocVerifyDtl("2", ref Grd2, ref DNo, TxtDocNo.Text, Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords(ref Grd1) ||
                IsGrdExceedMaxRecords(ref Grd2) ||
                IsGrdValueNotValid(ref Grd1) ||
                IsGrdValueNotValid(ref Grd2) ||
                IsDoubleEntry(ref Grd1) ||
                IsDoubleEntry(ref Grd2) ||
                // IsCancelIndNotValid() ||
                IsDataCancelledAlready() ||
                (ChkCancelInd.Checked && IsDataProcessedAlready());
        }

        private bool IsLegalDocAlreadyExist()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblLegalDocVerifyHdr Where (IfNull(ProcessInd1, 'O')='F' Or IfNull(ProcessInd2, 'O')='F') And CancelInd='N' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Nomor antrian telah diproses di dokumen verifikasi.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotValid()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen ini belum akan dibatalkan.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblLegalDocVerifyHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where (IfNull(ProcessInd1, 'O')='F' Or IfNull(ProcessInd2, 'O')='F') " +
                    "And CancelInd='N' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Nomor antrian telah diproses di dokumen verifikasi.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelLegalDocVerifyHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Delete From TblLegalDocVerifyDtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLegalDocVerifyHdr(DocNo);
                ShowVdInfo(mVdCode, TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode);
                ShowVdInfo(mVdCode1, TxtVdName1, TxtIdentityNo1, MeeAddress1, TxtVilCode1, TxtSDCode1, TxtCityCode1, TxtProvCode1);
                ShowVdInfo(mVdCode2, TxtVdName2, TxtIdentityNo2, MeeAddress2, TxtVilCode2, TxtSDCode2, TxtCityCode2, TxtProvCode2);
                ShowLegalDocVerifyDtl("1", ref Grd1, DocNo);
                ShowLegalDocVerifyDtl("2", ref Grd2, DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                tabControl1.SelectTab("tabPage1");
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLegalDocVerifyHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, A.QueueNo, A.VehicleRegNo, A.VdCode, A.VdCode1, A.VdCode2, C.TTName " +
                    "From TblLegalDocVerifyHdr A " +
                    "Left Join TblLoadingQueue B On A.QueueNo=B.DocNo " +
                    "Left Join TblTransportType C On B.TTCode=C.TTCode " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "QueueNo", "VehicleRegNo", "TTName",  
                        
                        //6-8
                        "VdCode", "VdCode1", "VdCode2"  
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtQueueNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[5]);
                        mVdCode = Sm.DrStr(dr, c[6]);
                        mVdCode1 = Sm.DrStr(dr, c[7]);
                        mVdCode2 = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowLegalDocVerifyDtl(string DLType, ref iGrid grd, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DLCode, B.DLName, A.DLDocNo, A.DLDocDt, A.Volume, A.Qty, A.Remark ");
            SQL.AppendLine("From TblLegalDocVerifyDtl A ");
            SQL.AppendLine("Left Join TblDocLegality B On A.DLCode=B.DLCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DLType=@DLType ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DLType", DLType);
            Sm.ShowDataInGrid(
                ref grd, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    "DLCode", 
                    "DLName", "DLDocNo", "DLDocDt", "Volume", "Qty", 
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref grd, grd.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(grd, 0, 1);
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowVdInfo(
            string VdCode, DXE.TextEdit TxtVdName, 
            DXE.TextEdit TxtIdentityNo, DXE.MemoExEdit MeeAddress, DXE.TextEdit TxtVilCode, 
            DXE.TextEdit TxtSDCode, DXE.TextEdit TxtCityCode, DXE.TextEdit TxtProvCode
            )
        {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtVdName, TxtIdentityNo, MeeAddress, TxtVilCode, TxtSDCode, TxtCityCode, TxtProvCode
                });

            if (VdCode.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VdName, A.IdentityNo, A.Address, B.VilName, C.SDName, D.CityName, E.ProvName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Left Join TblVillage B On A.VilCode=B.VilCode ");
            SQL.AppendLine("Left Join TblSubDistrict C On A.SDCode=C.SDCode ");
            SQL.AppendLine("Left Join TblCity D On A.CityCode=D.CityCode ");
            SQL.AppendLine("Left Join TblProvince E On D.ProvCode=E.ProvCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode; ");

            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "VdName",

                            //1-5
                            "IdentityNo", "Address", "VilName", "SDName", "CityName", 
                            
                            //6
                            "ProvName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtVdName.EditValue = Sm.DrStr(dr, c[0]);
                            TxtIdentityNo.EditValue = Sm.DrStr(dr, c[1]);
                            MeeAddress.EditValue = Sm.DrStr(dr, c[2]);
                            TxtVilCode.EditValue = Sm.DrStr(dr, c[3]);
                            TxtSDCode.EditValue = Sm.DrStr(dr, c[4]);
                            TxtCityCode.EditValue = Sm.DrStr(dr, c[5]);
                            TxtProvCode.EditValue = Sm.DrStr(dr, c[6]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsQueueNoNotValid()
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo ");
                SQL.AppendLine("From TblLoadingQueue A ");
                SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.DocNo=B.QueueNo And B.CancelInd='N' ");
                SQL.AppendLine("Where A.DocNo=@DocNo And B.QueueNo Is Null");
                SQL.AppendLine("Limit 1");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtQueueNo.Text);

                if (Sm.IsDataExist(cm)) return false;

                Sm.StdMsg(mMsgType.Warning,
                    "Nomor Antrian : " + TxtQueueNo.Text + Environment.NewLine + Environment.NewLine +
                    "Nomor antrian ini tidak benar."
                    );
                TxtQueueNo.EditValue = null;
                TxtQueueNo.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return true;
        }

        private void SetVehicleRegNo()
        {
            try
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select LicenceNo From TblLoadingQueue Where DocNo=@DocNo Limit 1"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtQueueNo.Text);

                var VehicleRegNo = Sm.GetValue(cm);
                TxtVehicleRegNo.EditValue = VehicleRegNo;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnQueueNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyDlg(this));
        }

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyDlg2(this, 0));
        }

        private void BtnVdCode1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyDlg2(this, 1));
        }

        private void BtnVdCode2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLegalDocVerifyDlg2(this, 2));
        }

        #endregion

        #region Misc Control Event

        private void LueDLCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDLCode1, new Sm.RefreshLue1(Sl.SetLueDLCode));
        }

        private void LueDLCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDLCode2, new Sm.RefreshLue1(Sl.SetLueDLCode));
        }

        private void LueDLCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDLCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueDLCode1_Leave(object sender, EventArgs e)
        {
            if (LueDLCode1.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueDLCode1).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueDLCode1);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueDLCode1.GetColumnValue("Col2");
                }
                LueDLCode1.Visible = false;
            }
        }

        private void DteDLDocDt1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDLDocDt1_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDLDocDt1, ref fCell, ref fAccept);
        }

        private void LueDLCode2_Leave(object sender, EventArgs e)
        {
            if (LueDLCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueDLCode2).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueDLCode2);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueDLCode2.GetColumnValue("Col2");
                }
                LueDLCode2.Visible = false;
            }
        }

        private void DteDLDocDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteDLDocDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDLDocDt2, ref fCell, ref fAccept);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(ref Grd1, ref LueDLCode1, ref DteDLDocDt1, sender, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(ref Grd1, sender, e);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(ref Grd1, sender, e);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(ref Grd2, ref LueDLCode2, ref DteDLDocDt2, sender, e);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdAfterCommitEdit(ref Grd2, sender, e);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            GrdKeyDown(ref Grd2, sender, e);
        }

        #endregion

        #endregion
    }
}
