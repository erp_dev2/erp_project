﻿#region Update
/* 
    14/12/2021 [DITA/PHT] New Apps (Balance sheet with setting)
    22/03/2022 [DITA/PHT] untuk acno sumber nya ttep dari akun 1,2,3 bukan dari balance sheet with setting
    20/04/2022 [DITA/PHT] penyesuaian tampilan laba rugi untuk kolom debit credit (disesuaikan juga jika ada minus plus)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBalanceSheet3 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mBalanceSheetDraft = "0",
            mAcNoForCurrentEarning2 = "9",
            mAcNoForActiva = "1",
            mAcNoForPassiva = "2",
            mAcNoForCapital = "3",
            mCurrentEarningFormulaType = "1",
            mMaxAccountCategory = "9";

        private bool
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsRptBalanceSheetCurrentEarningUseProfitLoss = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsFicoUseCOALevelFilter = false,
            mIsReportingFilterByEntity = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsRptBalanceSheetUseEndDt = false,
            //mIsFicoUseMultiProfitCenterFilter = false
            mIsRptBalanceSheetUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptFinancialNotUseStartFromFilter = false,
            mIsRptAccountingShowJournalMemorial = false;
            
        #endregion

        #region Constructor

        public FrmRptBalanceSheet3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueDt(LueDt);
                Sm.SetLue(LueDt, CurrentDateTime.Substring(6, 2));
                Sl.SetLuePeriod(LuePeriod);
                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                    }
                }
                if (!mIsFicoUseCOALevelFilter)
                {
                    LblLevel.Visible = LblLevel2.Visible = LueLevelFrom.Visible = LueLevelTo.Visible = false;
                }
                else
                {
                    SetLueLevelFromTo(ref LueLevelFrom);
                    SetLueLevelFromTo(ref LueLevelTo);
                }

                if (mIsReportingFilterByEntity) SetLueEntCode(ref LueEntCode);
                if (mIsRptBalanceSheetUseProfitCenter)    
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            mAcNoForCost = Sm.GetParameter("AcNoForCost");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsCOAAssetUseStartYr = Sm.GetParameterBoo("IsCOAAssetUseStartYr");
            mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            mIsRptBalanceSheetCurrentEarningUseProfitLoss = Sm.GetParameterBoo("IsRptBalanceSheetCurrentEarningUseProfitLoss");
            mBalanceSheetDraft = Sm.GetParameter("BalanceSheetDraft");
            mAcNoForCurrentEarning2 = Sm.GetParameter("AcNoForCurrentEarning2");
            mAcNoForActiva = Sm.GetParameter("AcNoForActiva");
            mAcNoForPassiva = Sm.GetParameter("AcNoForPassiva");
            mAcNoForCapital = Sm.GetParameter("AcNoForCapital");
            mCurrentEarningFormulaType = Sm.GetParameter("CurrentEarningFormulaType");
            mMaxAccountCategory = Sm.GetParameter("MaxAccountCategory");
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
            mIsFicoUseMultiEntityFilter = Sm.GetParameterBoo("IsFicoUseMultiEntityFilter");
            mIsFicoUseCOALevelFilter = Sm.GetParameterBoo("IsFicoUseCOALevelFilter");
            mIsRptBalanceSheetUseEndDt = Sm.GetParameterBoo("IsRptBalanceSheetUseEndDt");
            //mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mIsRptBalanceSheetUseProfitCenter = Sm.GetParameterBoo("IsRptBalanceSheetUseProfitCenter");
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Alias";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "Opening Balance";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 2;
            Grd1.Header.Cells[0, 3].Value = "Debit";
            Grd1.Header.Cells[0, 4].Value = "Credit";
            Grd1.Cols[3].Width = 130;
            Grd1.Cols[4].Width = 130;

            Grd1.Header.Cells[1, 5].Value = "Month To Date";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 2;
            Grd1.Header.Cells[0, 5].Value = "Debit";
            Grd1.Header.Cells[0, 6].Value = "Credit";
            Grd1.Cols[5].Width = 130;
            Grd1.Cols[6].Width = 130;

            Grd1.Header.Cells[1, 7].Value = "Current Month";
            Grd1.Header.Cells[1, 7].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 7].SpanCols = 2;
            Grd1.Header.Cells[0, 7].Value = "Debit";
            Grd1.Header.Cells[0, 8].Value = "Credit";
            Grd1.Cols[7].Width = 130;
            Grd1.Cols[8].Width = 130;

            Grd1.Header.Cells[1, 9].Value = "Year To Date";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 2;
            Grd1.Header.Cells[0, 9].Value = "Debit";
            Grd1.Header.Cells[0, 10].Value = "Credit";
            Grd1.Cols[9].Width = 130;
            Grd1.Cols[10].Width = 130;

            Grd1.Header.Cells[0, 11].Value = "Balance";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Cols[11].Width = 150;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 });
            if (mBalanceSheetDraft=="1") Sm.GrdColInvisible(Grd1, new int[] { 11 });
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month") ||
                (mIsRptBalanceSheetUseEndDt && Sm.IsLueEmpty(LueDt, "Date")) ||
                IsLevelFromToInvalid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var Dt = Sm.GetLue(LueDt);
            var StartFrom = Sm.GetLue(LueStartFrom);
            var SelectedCOACurentEarning = string.Empty;
            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var IsFilterByEntity = ChkEntCode.Checked;
                var lCOABalanceSheetSetting = new List<COABalanceSheetSetting>();
                var lCOACurrentEarning = new List<COACurrentEarning>();
                var lCOACurrentEarningDtl = new List<COACurrentEarningDtl>();

                SetProfitCenter();

                Process1(ref lCOA);
                if (IsFilterByEntity) Process8(ref lEntityCOA);
                
                if (lCOA.Count > 0)
                {
                    //GetCOABalanceSheetSetting(ref lCOABalanceSheetSetting);
                    //UpdateCOABSSetting(ref lCOA, ref lCOABalanceSheetSetting);

                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, Dt, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, Dt, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, Dt, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, Dt, string.Empty);
                    }
                    if (mIsRptAccountingShowJournalMemorial)
                        Process4_JournalMemorial(ref lCOA, Yr, Mth, Dt);
                    else
                        Process4(ref lCOA, Yr, Mth, Dt);

                    GetCOACurrentEarning(ref lCOACurrentEarning);
                    GetCOACurrentEarningDtl(ref lCOACurrentEarningDtl);

                    SelectedCOACurentEarning = GetSelectedCOACurrentEarning(ref lCOACurrentEarning);

                    Process5(ref lCOA, ref lCOACurrentEarning, ref lCOACurrentEarningDtl);
                    Process6(ref lCOA, ref lCOACurrentEarning);
                    Process7(ref lCOA);
                    Process9(ref lCOA, ref lCOACurrentEarning, SelectedCOACurentEarning);

                    #region Filtered By Entity
                    if (IsFilterByEntity)
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lCOA.Count > 0)
                            {
                                Grd1.BeginUpdate();
                                Grd1.Rows.Count = 0;

                                iGRow r;

                                r = Grd1.Rows.Add();
                                r.Level = 0;
                                r.TreeButton = iGTreeButtonState.Visible;
                                r.Cells[0].Value = "COA";
                                for (var j = 0; j < lEntityCOA.Count; j++)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (Process10(lCOA[i]) == true)
                                        { 
                                            if ((Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5") && lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                            {
                                                if (mIsFicoUseCOALevelFilter && Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                                {
                                                    if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                                    {
                                                        r = Grd1.Rows.Add();
                                                        r.Level = lCOA[i].Level;
                                                        r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                        r.Cells[0].Value = lCOA[i].AcNo;
                                                        r.Cells[1].Value = lCOA[i].Alias;
                                                        r.Cells[2].Value = lCOA[i].AcDesc;
                                                        for (var c = 3; c < 11; c++)
                                                            r.Cells[c].Value = 0m;
                                                        r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                        r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                        r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                        r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                        r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                        r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                        if (mBalanceSheetDraft == "1")
                                                        {
                                                            if (lCOA[i].AcType == "D")
                                                            {
                                                                r.Cells[9].Value = lCOA[i].Balance;
                                                                r.Cells[10].Value = 0m;
                                                            }
                                                            else
                                                            {
                                                                r.Cells[9].Value = 0m;
                                                                r.Cells[10].Value = lCOA[i].Balance;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                            r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                        }
                                                        r.Cells[11].Value = lCOA[i].Balance;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    r = Grd1.Rows.Add();
                                                    r.Level = lCOA[i].Level;
                                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                    r.Cells[0].Value = lCOA[i].AcNo;
                                                    r.Cells[1].Value = lCOA[i].Alias;
                                                    r.Cells[2].Value = lCOA[i].AcDesc;
                                                    for (var c = 3; c < 11; c++)
                                                        r.Cells[c].Value = 0m;
                                                    r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                    r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                    r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                    r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                    r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                    r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                    if (mBalanceSheetDraft == "1")
                                                    {
                                                        if (lCOA[i].AcType == "D")
                                                        {
                                                            r.Cells[9].Value = lCOA[i].Balance;
                                                            r.Cells[10].Value = 0m;
                                                        }
                                                        else
                                                        {
                                                            r.Cells[9].Value = 0m;
                                                            r.Cells[10].Value = lCOA[i].Balance;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                        r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                    }
                                                    r.Cells[11].Value = lCOA[i].Balance;
                                                    break;
                                                }
                                            }
                                         }
                                    }
                                }
                                Grd1.TreeLines.Visible = true;
                                Grd1.Rows.CollapseAll();

                                decimal
                                    OpeningBalanceDAmt = 0m,
                                    OpeningBalanceCAmt = 0m,
                                    MonthToDateDAmt = 0m,
                                    MonthToDateCAmt = 0m,
                                    CurrentMonthDAmt = 0m,
                                    CurrentMonthCAmt = 0m,
                                    YearToDateDAmt = 0m,
                                    YearToDateCAmt = 0m,
                                    Balance = 0m;

                                for (var i = 0; i < lCOA.Count; i++)
                                {
                                    if (Process10(lCOA[i]) == true)
                                    {
                                        if ((Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5") && lCOA[i].Parent.Length == 0)
                                        {
                                            OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                            OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                            MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                            MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                            CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                            CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                            if (mBalanceSheetDraft == "1")
                                            {
                                                if (lCOA[i].AcType == "D")
                                                    YearToDateDAmt += lCOA[i].Balance;
                                                else
                                                    YearToDateCAmt += lCOA[i].Balance;
                                            }
                                            else
                                            {
                                                YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                                YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                            }
                                            Balance += lCOA[i].Balance;
                                        }
                                    }
                                }

                                r = Grd1.Rows.Add();
                                r.Cells[0].Value = "Total";
                                for (var c = 3; c < 11; c++)
                                    r.Cells[c].Value = 0m;

                                r.Cells[3].Value = OpeningBalanceDAmt;
                                r.Cells[4].Value = OpeningBalanceCAmt;
                                r.Cells[5].Value = MonthToDateDAmt;
                                r.Cells[6].Value = MonthToDateCAmt;
                                r.Cells[7].Value = CurrentMonthDAmt;
                                r.Cells[8].Value = CurrentMonthCAmt;
                                r.Cells[9].Value = YearToDateDAmt;
                                r.Cells[10].Value = YearToDateCAmt;
                                r.Cells[11].Value = Balance;
                                r.BackColor = Color.LightSalmon;

                                Grd1.EndUpdate();
                            }
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Process10(lCOA[i]) == true)
                                {
                                    if (Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5")
                                    {
                                        if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                        {
                                            if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                            {
                                                r = Grd1.Rows.Add();
                                                r.Level = lCOA[i].Level;
                                                r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                r.Cells[0].Value = lCOA[i].AcNo;
                                                r.Cells[1].Value = lCOA[i].Alias;
                                                r.Cells[2].Value = lCOA[i].AcDesc;
                                                for (var c = 3; c < 11; c++)
                                                    r.Cells[c].Value = 0m;
                                                r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                                r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                                r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                                r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                                if (mBalanceSheetDraft == "1")
                                                {
                                                    if (lCOA[i].AcType == "D")
                                                    {
                                                        r.Cells[9].Value = lCOA[i].Balance;
                                                        r.Cells[10].Value = 0m;
                                                    }
                                                    else
                                                    {
                                                        r.Cells[9].Value = 0m;
                                                        r.Cells[10].Value = lCOA[i].Balance;
                                                    }
                                                }
                                                else
                                                {
                                                    r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                    r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                                }
                                                r.Cells[11].Value = lCOA[i].Balance;
                                                if (Sm.Find_In_Set(lCOA[i].AcNo, SelectedCOACurentEarning))
                                                    r.BackColor = Color.LightGreen;
                                            }
                                        }
                                        else
                                        {
                                            r = Grd1.Rows.Add();
                                            r.Level = lCOA[i].Level;
                                            r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                            r.Cells[0].Value = lCOA[i].AcNo;
                                            r.Cells[1].Value = lCOA[i].Alias;
                                            r.Cells[2].Value = lCOA[i].AcDesc;
                                            for (var c = 3; c < 11; c++)
                                                r.Cells[c].Value = 0m;
                                            r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                            r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                            r.Cells[5].Value = lCOA[i].MonthToDateDAmt;
                                            r.Cells[6].Value = lCOA[i].MonthToDateCAmt;
                                            r.Cells[7].Value = lCOA[i].CurrentMonthDAmt;
                                            r.Cells[8].Value = lCOA[i].CurrentMonthCAmt;
                                            if (mBalanceSheetDraft == "1")
                                            {
                                                if (lCOA[i].AcType == "D")
                                                {
                                                    r.Cells[9].Value = lCOA[i].Balance;
                                                    r.Cells[10].Value = 0m;
                                                }
                                                else
                                                {
                                                    r.Cells[9].Value = 0m;
                                                    r.Cells[10].Value = lCOA[i].Balance;
                                                }
                                            }
                                            else
                                            {
                                                r.Cells[9].Value = lCOA[i].YearToDateDAmt;
                                                r.Cells[10].Value = lCOA[i].YearToDateCAmt;
                                            }
                                            r.Cells[11].Value = lCOA[i].Balance;
                                            if (Sm.Find_In_Set(lCOA[i].AcNo, SelectedCOACurentEarning))
                                                r.BackColor = Color.LightGreen;
                                        }
                                    }
                                    }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();

                            decimal
                                OpeningBalanceDAmt = 0m,
                                OpeningBalanceCAmt = 0m,
                                MonthToDateDAmt = 0m,
                                MonthToDateCAmt = 0m,
                                CurrentMonthDAmt = 0m,
                                CurrentMonthCAmt = 0m,
                                YearToDateDAmt = 0m,
                                YearToDateCAmt = 0m,
                                Balance = 0m;

                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Process10(lCOA[i]) == true)
                                {
                                    if ((Sm.Left(lCOA[i].AcNo, 1) != "4" && Sm.Left(lCOA[i].AcNo, 1) != "5") && lCOA[i].Parent.Length == 0)
                                    {
                                        OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                        OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                        MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                        MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                        CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                        CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                        if (mBalanceSheetDraft == "1")
                                        {
                                            if (lCOA[i].AcType == "D")
                                                YearToDateDAmt += lCOA[i].Balance;
                                            else
                                                YearToDateCAmt += lCOA[i].Balance;
                                        }
                                        else
                                        {
                                            YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                            YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                        }
                                        Balance += lCOA[i].Balance;
                                    }
                                }
                            }

                            r = Grd1.Rows.Add();
                            r.Cells[0].Value = "Total";
                            for (var c = 3; c < 11; c++)
                                r.Cells[c].Value = 0m;

                            r.Cells[3].Value = OpeningBalanceDAmt;
                            r.Cells[4].Value = OpeningBalanceCAmt;
                            r.Cells[5].Value = MonthToDateDAmt;
                            r.Cells[6].Value = MonthToDateCAmt;
                            r.Cells[7].Value = CurrentMonthDAmt;
                            r.Cells[8].Value = CurrentMonthCAmt;
                            r.Cells[9].Value = YearToDateDAmt;
                            r.Cells[10].Value = YearToDateCAmt;
                            r.Cells[11].Value = Balance;
                            r.BackColor = Color.LightSalmon;

                            Grd1.EndUpdate();
                        }
                    }
                    #endregion

                }
                lCOA.Clear();
                lEntityCOA.Clear();
                lCOABalanceSheetSetting.Clear();
                lCOACurrentEarning.Clear();
                lCOACurrentEarningDtl.Clear();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptBalanceSheetUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetLueLevelFromTo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Level Col1, Level Col2 ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Order By Level; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsLevelFromToInvalid()
        {
            if (!mIsFicoUseCOALevelFilter) return false;

            string mLevelFrom = string.Empty, mLevelTo = string.Empty;

            mLevelFrom = Sm.GetLue(LueLevelFrom);
            mLevelTo = Sm.GetLue(LueLevelTo);

            if (mLevelTo.Length > 0 || mLevelFrom.Length > 0)
            {
                if (mLevelFrom.Length > 0 && mLevelTo.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level to is empty.");
                    LueLevelTo.Focus();
                    return true;
                }

                if (mLevelFrom.Length == 0 && mLevelTo.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level from is empty.");
                    LueLevelFrom.Focus();
                    return true;
                }

                if (mLevelTo.Length > 0 && mLevelFrom.Length > 0)
                {
                    if (Int32.Parse(mLevelFrom) > Int32.Parse(mLevelTo))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Level from should be smaller than level to.");
                        LueLevelFrom.Focus();
                        return true;
                    }
                }
            }

            return false;
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            //SQL.AppendLine("Select 'Consolidate' As Col1, 'CONSOLIDATE' As Col2 ");
            //SQL.AppendLine("Union All ");
            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode As Col1, B.EntName As Col2 ");
                SQL.AppendLine("    From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.AcNo, A.Alias, A.AcDesc, A.Parent, A.AcType ");
            SQL.AppendLine("From TblCOA A ");
            if (ChkEntCode.Checked) SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
            SQL.AppendLine("Where A.ActInd='Y'  ");
            SQL.AppendLine("Order By A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Alias", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Alias = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[3]).Length==0?1:-1,
                            AcType = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            BSSetting = "N",
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.AcNo Not in (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            //if (mIsRptBalanceSheetUseProfitCenter)
            //{
            //    SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");
            //    if (ChkProfitCenterCode.Checked)
            //        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
            //    else
            //    {
            //        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
            //        SQL.AppendLine("        Select ProfitCenterCode ");
            //        SQL.AppendLine("        From TblCostCenter ");
            //        SQL.AppendLine("        Where ProfitCenterCode Is Not Null ");
            //        SQL.AppendLine("        And ActInd='Y' ");
            //        SQL.AppendLine("        And CCCode In ( ");
            //        SQL.AppendLine("            Select Distinct CCCode From TblGroupCostCenter T ");
            //        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //        SQL.AppendLine("        ) ");
            //        SQL.AppendLine("    ) ");
            //    }
            //}
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        if (ChkProfitCenterCode.Checked) 
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); 
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNoForCurrentEarning", mAcNoForCurrentEarning);
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();

                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string Dt, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo;");

            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                        
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string Dt, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From (");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
            }
            else
            {
                if (StartFrom.Length == 0)
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                else
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (StartFrom.Length > 0)
                Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo;");

            var lJournal = new List<Journal>();
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt From ( ");

            #region Standard

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Status='O' And A.CancelInd='N' ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                if (mIsRptBalanceSheetUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            }
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptBalanceSheetUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo;");

            var lJournal = new List<Journal>();

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA, ref List<COACurrentEarning> lCOACurrentEarning, ref List<COACurrentEarningDtl> lCOACurrentEarningDtl)
        {
            GetCOACurrentEarningDtlAmt(ref lCOA, ref lCOACurrentEarningDtl);
            GetCOACurrentEarningAmt(ref lCOACurrentEarningDtl, ref lCOACurrentEarning);
        }

        private void GetCOACurrentEarningDtlAmt(ref List<COA> lCOA, ref List<COACurrentEarningDtl> lCOACurrentEarningDtl)
        {
            foreach (var a in lCOACurrentEarningDtl.OrderBy(o => o.AcNoSource))
            {
                foreach (var b in lCOA.Where(p => p.AcNo == a.AcNoSource))
                {
                    a.CurrentMonthCAmt = b.CurrentMonthCAmt;
                    a.CurrentMonthDAmt = b.CurrentMonthDAmt;
                    a.MonthToDateCAmt = b.MonthToDateCAmt;
                    a.MonthToDateDAmt = b.MonthToDateDAmt;
                    a.OpeningBalanceCAmt = b.OpeningBalanceCAmt;
                    a.OpeningBalanceDAmt = b.OpeningBalanceDAmt;
                    
                }
            }
        }

        private void GetCOACurrentEarningAmt(ref List<COACurrentEarningDtl> lCOACurrentEarningDtl, ref List<COACurrentEarning> lCOACurrentEarning )
        {
            decimal mAdditionCAmt = 0m, mSubstractionCAmt = 0m, mAdditionDAmt = 0m, mSubstractionDAmt = 0m;

            //foreach (var a in lCOACurrentEarning.OrderBy(o => o.AcNo))
            //{
            //    foreach (var b in lCOACurrentEarningDtl.Where(p => p.AcNo == a.AcNo))
            //    {
            //        if(b.Type == "A")
            //            mAdditionDAmt = mAdditionDAmt + (b.OpeningBalanceDAmt + b.MonthToDateDAmt + b.CurrentMonthDAmt);
            //        else
            //            mSubstractionDAmt = mSubstractionDAmt +(b.OpeningBalanceDAmt + b.MonthToDateDAmt + b.CurrentMonthDAmt);

            //        if (b.Type == "A")
            //            mAdditionCAmt = mAdditionCAmt + (b.OpeningBalanceCAmt + b.MonthToDateCAmt + b.CurrentMonthCAmt);
            //        else
            //            mSubstractionCAmt = mSubstractionCAmt + (b.OpeningBalanceCAmt + b.MonthToDateCAmt + b.CurrentMonthCAmt);
            //    }

            //    a.YearToDateDAmt = mAdditionDAmt - mSubstractionDAmt;
            //    a.YearToDateCAmt = mAdditionCAmt - mSubstractionCAmt;
            //}

           

            foreach (var a in lCOACurrentEarning.OrderBy(o => o.AcNo))
            {
                decimal ABal = 0m, SBal = 0m;
                foreach (var b in lCOACurrentEarningDtl.Where(p => p.AcNo == a.AcNo))
                {
                    if (b.AcType == "D")
                        b.Balance = (b.OpeningBalanceDAmt + b.MonthToDateDAmt + b.CurrentMonthDAmt) - (b.OpeningBalanceCAmt + b.MonthToDateCAmt + b.CurrentMonthCAmt);
                    else
                        b.Balance = (b.OpeningBalanceCAmt + b.MonthToDateCAmt + b.CurrentMonthCAmt) - (b.OpeningBalanceDAmt + b.MonthToDateDAmt + b.CurrentMonthDAmt);

                    if (b.Type == "A")
                        ABal = ABal + b.Balance;
                    else
                        SBal = SBal + b.Balance;
                }

                a.YearToDateBalance = ABal - SBal;
                if (a.AcType == "D")
                {
                    a.YearToDateDAmt = a.YearToDateBalance > 0m ? a.YearToDateBalance : 0m;
                    a.YearToDateCAmt = a.YearToDateBalance < 0m ? a.YearToDateBalance * -1 : 0m;

                }
                else
                {
                    a.YearToDateCAmt = a.YearToDateBalance > 0m ? a.YearToDateBalance : 0m;
                    a.YearToDateDAmt = a.YearToDateBalance < 0m ? a.YearToDateBalance * -1 : 0m;
                }
            }


        }

        private void Process6(ref List<COA> lCOA, ref List<COACurrentEarning> lCOACurrentEarning)
        {
            foreach (var a in lCOA)
            {
                a.YearToDateDAmt =
                    a.OpeningBalanceDAmt +
                    a.MonthToDateDAmt +
                    a.CurrentMonthDAmt;

                a.YearToDateCAmt =
                    a.OpeningBalanceCAmt +
                    a.MonthToDateCAmt +
                    a.CurrentMonthCAmt;

               
                    if (a.AcType == "D")
                        a.Balance = a.YearToDateDAmt - a.YearToDateCAmt;
                    else
                        a.Balance = a.YearToDateCAmt - a.YearToDateDAmt;

                    foreach (var b in lCOACurrentEarning.Where(w => w.AcNo == a.AcNo))
                    {

                    a.YearToDateCAmt = b.YearToDateCAmt;
                    a.YearToDateDAmt = b.YearToDateDAmt;

                    //if (a.AcType == "D")
                    //    a.Balance = a.YearToDateDAmt - a.YearToDateCAmt;
                    //else
                    //    a.Balance = a.YearToDateCAmt - a.YearToDateDAmt;

                    if (b.AcType == "D")
                       a.Balance = b.YearToDateDAmt - b.YearToDateCAmt;
                    else
                       a.Balance = b.YearToDateCAmt - b.YearToDateDAmt;

                }
                  
            }
        }

        private void Process7(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                break;
                            }
                        }
                    }
                }
                for (var j = i+1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA, ref List<COACurrentEarning> lCOACurrentEarning, string SelectedCOACurentEarning)
        {
           
            foreach (var x in lCOACurrentEarning.OrderBy(w => w.AcNo))
            {
                foreach
                    (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, SelectedCOACurentEarning))))
                {
                    if (x.AcNo.Count(y => y == '.') != i.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(i.AcNo))
                    {
                        i.YearToDateDAmt = 0m;
                        i.YearToDateCAmt = 0m;
                        i.Balance = 0m;
                    }

                }

                //foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && Sm.CompareStr(w.AcNo, x.AcNo) && w.HasChild))
                //{
                //    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, SelectedCOACurentEarning))))
                //    {
                //        if (
                //           i.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && i.AcNo.StartsWith(j.AcNo)
                //           )

                //        {
                //            j.YearToDateDAmt += i.YearToDateDAmt;
                //            j.YearToDateCAmt += i.YearToDateCAmt;
                //            j.Balance += i.Balance;
                //            if (string.Compare(j.AcNo, i.AcNo) == 0)
                //                break;
                //        }
                //    }
                //}

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && !w.HasChild))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, SelectedCOACurentEarning))))
                    {
                        if (x.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(j.AcNo))
                        {
                            if (
                                i.AcNo.Count(y => y == '.') == j.AcNo.Count(y => y == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                                i.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                            {
                                j.YearToDateDAmt += i.YearToDateDAmt;
                                j.YearToDateCAmt += i.YearToDateCAmt;
                                j.Balance += i.Balance;
                                if (string.Compare(j.AcNo, i.AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }

                //int mMaxLevel = Int32.Parse(Sm.GetValue("Select Max(Level) From TblCOA Where AcNo Like @Param ", string.Concat(Sm.Left(x.AcNo, 1), "%")));

                ////bukan bontot dan bukan current earning, nilai nya di set 0
                //foreach (var y in lCOA.Where(w =>
                //    Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) &&
                //    w.HasChild &&
                //    !Sm.Find_In_Set(w.AcNo, SelectedCOACurentEarning)
                //))
                //{
                //    y.YearToDateDAmt = 0m;
                //    y.YearToDateCAmt = 0m;
                //    y.Balance = 0m;
                //}

                //for (int lvl = mMaxLevel; lvl > 0; --lvl)
                //{
                //    string Parent = string.Empty;
                //    decimal YTDD = 0m, YTDC = 0m, Bal = 0m;
                //    bool IsFirst = true;

                //    foreach (var i in lCOA.Where(w => w.Level == lvl && Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1)).OrderBy(o => o.AcNo))
                //    {
                //        if (IsFirst)
                //        {
                //            Parent = i.Parent;
                //            IsFirst = false;
                //        }

                //        if (Parent != i.Parent)
                //        {
                //            foreach (var j in lCOA.Where(w => w.AcNo == Parent && !Sm.Find_In_Set(w.AcNo, SelectedCOACurentEarning)))
                //            {
                //                j.YearToDateDAmt = YTDD;
                //                j.YearToDateCAmt = YTDC;
                //                j.Balance = Bal;

                //                break;
                //            }

                //            YTDD = i.YearToDateDAmt;
                //            YTDC = i.YearToDateCAmt;
                //            Bal = i.Balance;

                //            Parent = i.Parent;
                //        }
                //        else
                //        {
                //            YTDD += i.YearToDateDAmt;
                //            YTDC += i.YearToDateCAmt;
                //            Bal += i.Balance;
                //        }
                //    }
                //}
            }
        }

        private bool Process10(COA lCOA)
        {
            if (lCOA.AcNo.Length == 1)
            {
                if (lCOA.AcNo == "1" || lCOA.AcNo == "2" || lCOA.AcNo == "3")
                    return true;
                else
                    return false;
            }
            else
            {
                if (Sm.Left(lCOA.AcNo, 2) == "1." || Sm.Left(lCOA.AcNo, 2) == "2." || Sm.Left(lCOA.AcNo, 2) == "3.")
                    return true;
                else
                    return false;
            }
        }

        private void GetCOABalanceSheetSetting(ref List<COABalanceSheetSetting> lCOABalanceSheetSetting)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    Select X1.AcNo, X2.Level, X2.Parent From ( ");
            SQL.AppendLine("    SELECT B.AcNo ");
            SQL.AppendLine("    FROM TblBalanceSheetSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblBalanceSheetSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT B.AcNo  ");
            SQL.AppendLine("    FROM TblBalanceSheetSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblBalanceSheetSettingDtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT B.AcNo  ");
            SQL.AppendLine("    FROM TblBalanceSheetSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblBalanceSheetSettingDtl3 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' ");
            SQL.AppendLine("    )X1");
            SQL.AppendLine("    Inner Join TblCOA X2 On X1.AcNo = X2.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Level", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOABalanceSheetSetting.Add(new COABalanceSheetSetting()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Level = Sm.DrInt(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }


        private void UpdateCOABSSetting(ref List<COA> lCOA, ref List<COABalanceSheetSetting> lCOABalanceSheetSetting)
        {
            
            foreach (var x in lCOABalanceSheetSetting.OrderBy(o => o.AcNo))
            {
                foreach (var y in lCOA.Where(w => w.AcNo == x.AcNo))
                {
                    y.BSSetting = "Y";
                }
               

                int currLevel = x.Level;
                string currParent = x.Parent;

                for (int i = 0; i < currLevel; i++)
                {
                    if (currParent.Length > 0)
                        {
                            foreach (var y in lCOA.Where(w => w.AcNo == currParent))
                            {
                           
                                    y.BSSetting = "Y";
                                    currParent = y.Parent;
                                    break;
                            
                            }
                        }

                }
            }
        
        }

        private void GetCOACurrentEarning(ref List<COACurrentEarning> lCOACurrentEarning)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.CurrentEarningAcNo, B.AcType ");
            SQL.AppendLine("From TblProfitLossSettingHdr A ");
            SQL.AppendLine("Inner Join TblCOA B On A.CurrentEarningAcNo = B.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' And Yr = @Yr; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CurrentEarningAcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOACurrentEarning.Add(new COACurrentEarning()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            YearToDateCAmt = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateBalance = 0m
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetCOACurrentEarningDtl(ref List<COACurrentEarningDtl> lCOACurrentEarningDtl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select X1.CurrentEarningAcNo, X1.AcNo, X1.Type, X2.AcType ");
            SQL.AppendLine("From(");
            SQL.AppendLine("SELECT A.CurrentEarningAcNo, B.AcNo, 'A' AS Type ");
            SQL.AppendLine("FROM TblProfitLossSettingHdr A ");
            SQL.AppendLine("INNER JOIN TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("WHERE Yr = @Yr AND ActInd = 'Y' ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("SELECT A.CurrentEarningAcNo, B.AcNo, 'S' AS Type ");
            SQL.AppendLine("FROM TblProfitLossSettingHdr A ");
            SQL.AppendLine("INNER JOIN TblProfitLossSettingDtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("WHERE Yr = @Yr AND ActInd = 'Y' ");
            SQL.AppendLine(")X1 ");
            SQL.AppendLine("Inner Join TblCOA X2 On X1.AcNo = X2.AcNo ");
            SQL.AppendLine("Inner Join TblCOA X3 On X1.CurrentEarningAcNo = X3.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CurrentEarningAcNo", "AcNo", "Type", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOACurrentEarningDtl.Add(new COACurrentEarningDtl()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcNoSource = Sm.DrStr(dr, c[1]),
                            Type = Sm.DrStr(dr, c[2]),
                            AcType = Sm.DrStr(dr, c[3]),
                            CurrentMonthCAmt = 0m,
                            CurrentMonthDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            OpeningBalanceDAmt = 0m,
                            Balance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOACurrentEarning(ref List<COACurrentEarning> lCOACurrentEarning)
        {
            string AcNo = string.Empty;

            foreach (var x in lCOACurrentEarning)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        private void SetLueDt(LookUpEdit Lue)
        {
            Sl.SetLookUpEdit(Lue,
                new string[] {
                    null,
                    "01", "02", "03", "04", "05",
                    "06", "07", "08", "09", "10",
                    "11", "12", "13", "14", "15",
                    "16", "17", "18", "19", "20",
                    "21", "22", "23", "24", "25",
                    "26", "27", "28", "29", "30",
                    "31"
                });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }


        private void LueLevelFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelFrom, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LueLevelTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelTo, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public string BSSetting { get; set; }

        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COABalanceSheetSetting
        {
            public string AcNo { get; set; }
            public int Level { get; set; }
            public string Parent { get; set; }

        }

        private class COACurrentEarning
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class COACurrentEarningDtl
        {
            public string AcNo { get; set; }
            public string AcNoSource { get; set; }
            public string AcType { get; set; }
            public string Type { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal Balance { get; set; }

        }


        #endregion
    }
}
