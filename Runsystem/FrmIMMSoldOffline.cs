﻿#region Update
/*
    04/09/2019 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMSoldOffline : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            mDocNo = string.Empty,
            mDocSeqNo = string.Empty;
        internal FrmIMMSoldOfflineFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMSoldOffline(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sold Offline";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    //ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMSoldOffline';");
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Bin",
                        "Location Code",
                        "Location",
                        "Source",
                        "Product's Code",
                        
                        //6-8
                        "Product's Name",
                        "Color",
                        "Seller"
                    },
                     new int[] 
                    {
                        //0
                        20, 
 
                        //1-5
                        150, 0, 150, 150, 100, 
                        
                        //6-8
                        200, 100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeCancelReason, ChkCancelInd, LueWhsCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDocNo = string.Empty;
            mDocSeqNo = string.Empty;
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueWhsCode, MeeRemark
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMSoldOfflineFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIMMSoldOfflineDlg(this, Sm.GetLue(LueWhsCode)));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmIMMSoldOfflineDlg(this, Sm.GetLue(LueWhsCode)));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            decimal DocSeqNo = 0m;

            GenerateDocNo(ref DocNo, ref DocSeqNo);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveIMMSoldOfflineHdr(DocNo, DocSeqNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveIMMSoldOfflineDtl(DocNo, DocSeqNo, r));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void GenerateDocNo(ref string DocNo, ref decimal DocSeqNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMSoldOfflineHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
            DocSeqNo += 1;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 product.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 3, false, "Source is empty.") ||
                    IsStockInvalid1(r) ||
                    IsStockInvalid2(r))
                    return true;
            return false;
        }

        private bool IsStockInvalid1(int r)
        {
            string
                Source = Sm.GetGrdStr(Grd1, r, 4),
                Bin = Sm.GetGrdStr(Grd1, r, 1);

            if (Sm.IsDataExist("Select 1 From TblIMMStockSummary Where WhsCode=@Param1 And Bin=@Param2 And Source=@Param3 And Qty<=0.00;",
                Sm.GetLue(LueWhsCode), Bin, Source))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Source : " + Source + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Bin : " + Bin + Environment.NewLine +
                    "Location : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine + Environment.NewLine +
                    "Stock is empty."
                    );
                return true;
            }
            return false;
        }

        private bool IsStockInvalid2(int r)
        {
            string Source = Sm.GetGrdStr(Grd1, r, 4);

            if (Sm.IsDataExist("Select 1 From TblIMMTransferBinReqDtl Where CancelInd='N' And Source=@Param And TransferBinDocNo Is Null;", Source))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Source : " + Source + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Location : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine + Environment.NewLine +
                    "Invalid stock because outstanding request existed."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveIMMSoldOfflineHdr(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMSoldOfflineHdr ");
            SQL.AppendLine("(DocNo, DocSeqNo, DocDt, CancelReason, CancelInd, WhsCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @DocDt, Null, 'N', @WhsCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIMMSoldOfflineDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMSoldOfflineDtl(DocNo, DocSeqNo, Bin, LocCode, Source, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @Bin, @LocCode, @Source, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values('07', @DocNo, @DocSeqNo, @Source, @DocDt, @WhsCode, @LocCode, @Bin, @ProdCode, -1.00, @UserCode, CurrentDateTime()); ");
            
            SQL.AppendLine("Update TblIMMStockSummary Set ");
            SQL.AppendLine("    Qty=0.00 ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            SQL.AppendLine("And Bin=@Bin; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<String>(ref cm, "@ProdCode", Sm.GetGrdStr(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditIMMSoldOfflineHdr());

            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation");
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblIMMSoldOfflineHdr Where CancelInd='Y' And DocNo=@Param1 And DocSeqNo=@Param2;",
                mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditIMMSoldOfflineHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMSoldOfflineHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '08', DocNo, DocSeqNo, Source, DocDt, WhsCode, Bin, ProdCode, -1.00*Qty, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMStockMovement ");
            SQL.AppendLine("Where DocType='07' And DocNo=@DocNo And DocSeqNo=@DocSeqNo;");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMSoldOfflineHdr B ");
            SQL.AppendLine("    On B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMSoldOfflineDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=C.DocSeqNo ");
            SQL.AppendLine("    And A.Source=C.Source ");
            SQL.AppendLine("Set A.Qty=1.00, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                mDocNo = DocNo;
                mDocSeqNo = DocSeqNo;
                ShowSoldOfflineHdr(DocNo, DocSeqNo);
                ShowSoldOfflineDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSoldOfflineHdr(string DocNo, string DocSeqNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocDt, CancelReason, CancelInd, WhsCode, Remark From TblIMMSoldOfflineHdr Where DocNo=@DocNo And DocSeqNo=@DocSeqNo;",
                    new string[] 
                    { 
                        "DocDt", 
                        "CancelReason", "CancelInd", "WhsCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        MeeCancelReason.Text = Sm.DrStr(dr, c[1]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowSoldOfflineDtl(string DocNo, string DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, A.LocCode, E.LocName, A.Source, B.ProdCode, C.ProdDesc, C.Color, D.SellerName ");
            SQL.AppendLine("From TblIMMSoldOfflineDtl A ");
            SQL.AppendLine("Inner Join TblIMMStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("Inner Join TblIMMProduct C On B.ProdCode=C.ProdCode ");
            SQL.AppendLine("Left Join TblIMMSeller D On B.SellerCode=D.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo Order By A.Bin, A.Source;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Bin", 

                    //1-5
                    "LocCode", "LocName", "Source", "ProdCode", "ProdDesc",   
                    
                    //6-7
                    "Color", "SellerName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                Sm.ClearGrd(Grd1, true);
            }
        }


        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }


        #endregion

        #endregion
    }
}
