﻿#region Update
/*
    01/03/2022 [BRI/PHT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPeriodicPromotionSettingFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPeriodicPromotionSetting mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPeriodicPromotionSettingFind(FrmPeriodicPromotionSetting FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-3
                        "Document#",
                        "Date",
                        "CancelInd",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 80,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelInd ");
            SQL.AppendLine("From TblPeriodicPromotionSettingHdr ");

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = "where 1=1 ";

                Sm.FilterDt(ref Filter, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-2
                            "DocDt", "CancelInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion
    }
}
