﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptBudget : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mBudgetBasedOn = string.Empty;

        #endregion

        #region Constructor

        public FrmRptBudget(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueYr(LueYr, "");
                if (mBudgetBasedOn == "1")
                {
                    Sl.SetLueDeptCode(ref LueDeptCode);
                }
                if (mBudgetBasedOn == "2")
                {
                    LblDeptCode.Text = "Site";
                    Sl.SetLueSiteCode2(ref LueDeptCode, string.Empty);
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, A.DeptCode, ");

            for (int Mth = 1; Mth <= 12; Mth++)
                SetSubSQL1(ref SQL, Sm.Right("0" + Mth.ToString(), 2));

            if (mBudgetBasedOn == "1") 
                SQL.AppendLine("B.DeptName From  ");
            if (mBudgetBasedOn == "2")
                SQL.AppendLine("B.SiteName As DeptName From  ");
            SQL.AppendLine("( ");

            SQL.AppendLine("    Select T.Yr, T.DeptCode ");
            for(int Mth=1;Mth<=12;Mth++)
                SetSubSQL2(ref SQL, Sm.Right("0"+Mth.ToString(), 2));
           

            SQL.AppendLine("    From (Select Distinct Yr, DeptCode from TblBudget) T ");

            SQL.AppendLine(") A ");
            if (mBudgetBasedOn == "1")
                SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            if (mBudgetBasedOn == "2")
                SQL.AppendLine("Left Join TblSite B On A.DeptCode=B.SiteCode ");
            mSQL = SQL.ToString();
        }

        private void SetSubSQL1(ref StringBuilder SQL, string Mth)
        {
            SQL.AppendLine("IfNull(A.Budget" + Mth + ", 0) As Budget" + Mth + ", IfNull(A.DO" + Mth + ", 0)-IfNull(Recv" + Mth + ", 0) As Actual" + Mth + ",");
        }

        private void SetSubSQL2(ref StringBuilder SQL, string Mth)
        {

            SQL.AppendLine("    ,(Select Amt From TblBudget Where Yr=T.Yr And DeptCode=T.DeptCode And Mth='" + Mth + "') As Budget" + Mth + ", ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Sum(T1.Qty*T2.UPrice*T2.ExcRate) As Amt ");
            SQL.AppendLine("        From TblStockMovement T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source ");
            SQL.AppendLine("        Inner Join TblDODeptHdr T3 On T1.DocNo=T3.DocNo  ");
            SQL.AppendLine("        Where T1.DocType='05'  And Left(T1.DocDt, 6)=Concat(T.Yr, '" + Mth + "') And T3.DeptCode=T.DeptCode ");
            SQL.AppendLine("    ) As DO" + Mth + ", ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Sum(T1.Qty*T2.UPrice*T2.ExcRate*-1) As Amt ");
            SQL.AppendLine("        From TblStockMovement T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source ");
            SQL.AppendLine("        Inner Join TblRecvDeptHdr T3 On T1.DocNo=T3.DocNo  ");
            SQL.AppendLine("        Where T1.DocType='06'  And Left(T1.DocDt, 6)=Concat(T.Yr, '" + Mth + "') And T3.DeptCode=T.DeptCode ");
            SQL.AppendLine("    ) As Recv" + Mth);
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 39;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Header.Cells[0, 1].Value = "Year";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = mBudgetBasedOn == "1" ? "Department" : "Site";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "January";
            Grd1.Header.Cells[1, 6].Value = "February";
            Grd1.Header.Cells[1, 9].Value = "March";
            Grd1.Header.Cells[1, 12].Value = "April";
            Grd1.Header.Cells[1, 15].Value = "May";
            Grd1.Header.Cells[1, 18].Value = "June";
            Grd1.Header.Cells[1, 21].Value = "July";
            Grd1.Header.Cells[1, 24].Value = "August";
            Grd1.Header.Cells[1, 27].Value = "September";
            Grd1.Header.Cells[1, 30].Value = "October";
            Grd1.Header.Cells[1, 33].Value = "November";
            Grd1.Header.Cells[1, 36].Value = "December";

            for (int Col = 1; Col <= 12; Col++)
            {
                Grd1.Header.Cells[1, Col * 3].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[1, Col * 3].SpanCols = 3;
            }

            for (int Col = 1; Col <=12; Col++)
            {
                Grd1.Header.Cells[0, (Col * 3)].Value = "Budget" + Environment.NewLine + "Amount";
                Grd1.Header.Cells[0, (Col * 3) + 1].Value = "Actual" + Environment.NewLine + "Amount";
                Grd1.Header.Cells[0, (Col * 3) + 2].Value = "Balance";          
            }

            for (int Col = 0; Col <= 38; Col++)
                Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;

            Sm.GrdFormatDec(Grd1,
                new int[] { 
                    3, 4, 5, 
                    6, 7, 8, 9, 10, 
                    11, 12, 13, 14, 15, 
                    16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 
                    26, 27, 28, 29, 30, 
                    31, 32, 33, 34, 35,  
                    36, 37, 38
                }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[]
                        {
                            "Yr", 
                            "DeptName", 
                            "Budget01", "Actual01",
                            "Budget02", "Actual02",
                            "Budget03", "Actual03",
                            "Budget04", "Actual04",
                            "Budget05", "Actual05",
                            "Budget06", "Actual06",
                            "Budget07", "Actual07",
                            "Budget08", "Actual08",
                            "Budget09", "Actual09",
                            "Budget10", "Actual10",
                            "Budget11", "Actual11",
                            "Budget12", "Actual12"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);

                            for (int Index = 1; Index <= 12; Index++)
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, (3*Index), ((2*Index) +0));
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, (3*Index)+1, ((2*Index) +1));
                                Grd1.Cells[Row, (3*Index) +2].Value = dr.GetDecimal(c[((2*Index) +0)]) - dr.GetDecimal(c[((2*Index) +1)]);
                            }
                        }, true, false, false, true
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                    3, 4, 5, 6, 7, 8, 9, 10,
                    11, 12, 13, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                    31, 32, 33, 34, 35, 36, 37, 38
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.SetGrdProperty(Grd1, true);
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, mBudgetBasedOn == "1" ? "Department" : "Site");
        }

        #endregion

        #endregion
    }
}
