﻿namespace RunSystem
{
    partial class FrmRptMaterialQCInspection3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkPlanningDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.ChkWhsCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPlanningDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtQCPDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkQCPDesc = new DevExpress.XtraEditors.CheckEdit();
            this.TxtWorkCenterDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.ChkItName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkWorkCenterDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LblList = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ChkStartDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteStartDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkDeptCode = new DevExpress.XtraEditors.CheckEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtBatchNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkBatchNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQCPDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQCPDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWorkCenterDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(877, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.ChkDeptCode);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.ChkStartDt);
            this.panel2.Controls.Add(this.DteStartDt2);
            this.panel2.Controls.Add(this.DteStartDt1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkPlanningDocNo);
            this.panel2.Controls.Add(this.ChkDocNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPlanningDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Size = new System.Drawing.Size(877, 117);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(877, 356);
            this.Grd1.TabIndex = 36;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 117);
            this.panel3.Size = new System.Drawing.Size(877, 356);
            // 
            // ChkPlanningDocNo
            // 
            this.ChkPlanningDocNo.Location = new System.Drawing.Point(392, 65);
            this.ChkPlanningDocNo.Name = "ChkPlanningDocNo";
            this.ChkPlanningDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPlanningDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkPlanningDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPlanningDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPlanningDocNo.Properties.Caption = " ";
            this.ChkPlanningDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPlanningDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkPlanningDocNo.TabIndex = 22;
            this.ChkPlanningDocNo.ToolTip = "Remove filter";
            this.ChkPlanningDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPlanningDocNo.ToolTipTitle = "Run System";
            this.ChkPlanningDocNo.CheckedChanged += new System.EventHandler(this.ChkPlanningDocNo_CheckedChanged);
            // 
            // ChkWhsCode
            // 
            this.ChkWhsCode.Location = new System.Drawing.Point(419, 4);
            this.ChkWhsCode.Name = "ChkWhsCode";
            this.ChkWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWhsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkWhsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWhsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWhsCode.Properties.Caption = " ";
            this.ChkWhsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWhsCode.Size = new System.Drawing.Size(19, 22);
            this.ChkWhsCode.TabIndex = 26;
            this.ChkWhsCode.ToolTip = "Remove filter";
            this.ChkWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWhsCode.ToolTipTitle = "Run System";
            this.ChkWhsCode.CheckedChanged += new System.EventHandler(this.ChkWhsCode_CheckedChanged);
            // 
            // ChkDocNo
            // 
            this.ChkDocNo.Location = new System.Drawing.Point(392, 24);
            this.ChkDocNo.Name = "ChkDocNo";
            this.ChkDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNo.Properties.Caption = " ";
            this.ChkDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNo.TabIndex = 14;
            this.ChkDocNo.ToolTip = "Remove filter";
            this.ChkDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNo.ToolTipTitle = "Run System";
            this.ChkDocNo.CheckedChanged += new System.EventHandler(this.ChkDocNo_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(252, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 10;
            this.label3.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "Planning Document#";
            // 
            // TxtPlanningDocNo
            // 
            this.TxtPlanningDocNo.EnterMoveNextControl = true;
            this.TxtPlanningDocNo.Location = new System.Drawing.Point(128, 67);
            this.TxtPlanningDocNo.Name = "TxtPlanningDocNo";
            this.TxtPlanningDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningDocNo.Properties.MaxLength = 250;
            this.TxtPlanningDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtPlanningDocNo.TabIndex = 21;
            this.TxtPlanningDocNo.Validated += new System.EventHandler(this.TxtPlanningDocNo_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "Location";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(85, 4);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 350;
            this.LueWhsCode.Size = new System.Drawing.Size(331, 20);
            this.LueWhsCode.TabIndex = 25;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(32, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Document Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(266, 4);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt2.TabIndex = 11;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(128, 4);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt1.TabIndex = 9;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(128, 25);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 250;
            this.TxtDocNo.Size = new System.Drawing.Size(261, 20);
            this.TxtDocNo.TabIndex = 13;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtBatchNo);
            this.panel4.Controls.Add(this.ChkBatchNo);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtQCPDesc);
            this.panel4.Controls.Add(this.ChkQCPDesc);
            this.panel4.Controls.Add(this.TxtWorkCenterDocNo);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtItName);
            this.panel4.Controls.Add(this.ChkItName);
            this.panel4.Controls.Add(this.ChkWorkCenterDocNo);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.LblList);
            this.panel4.Controls.Add(this.LueWhsCode);
            this.panel4.Controls.Add(this.ChkWhsCode);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(428, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(445, 113);
            this.panel4.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 14);
            this.label10.TabIndex = 33;
            this.label10.Text = "Parameter";
            // 
            // TxtQCPDesc
            // 
            this.TxtQCPDesc.EnterMoveNextControl = true;
            this.TxtQCPDesc.Location = new System.Drawing.Point(85, 67);
            this.TxtQCPDesc.Name = "TxtQCPDesc";
            this.TxtQCPDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQCPDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtQCPDesc.Properties.MaxLength = 250;
            this.TxtQCPDesc.Size = new System.Drawing.Size(331, 20);
            this.TxtQCPDesc.TabIndex = 34;
            this.TxtQCPDesc.Validated += new System.EventHandler(this.TxtQCPDesc_Validated);
            // 
            // ChkQCPDesc
            // 
            this.ChkQCPDesc.Location = new System.Drawing.Point(419, 67);
            this.ChkQCPDesc.Name = "ChkQCPDesc";
            this.ChkQCPDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkQCPDesc.Properties.Appearance.Options.UseFont = true;
            this.ChkQCPDesc.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkQCPDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkQCPDesc.Properties.Caption = " ";
            this.ChkQCPDesc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkQCPDesc.Size = new System.Drawing.Size(19, 22);
            this.ChkQCPDesc.TabIndex = 35;
            this.ChkQCPDesc.ToolTip = "Remove filter";
            this.ChkQCPDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkQCPDesc.ToolTipTitle = "Run System";
            this.ChkQCPDesc.CheckedChanged += new System.EventHandler(this.ChkQCPDesc_CheckedChanged);
            // 
            // TxtWorkCenterDocNo
            // 
            this.TxtWorkCenterDocNo.EnterMoveNextControl = true;
            this.TxtWorkCenterDocNo.Location = new System.Drawing.Point(85, 25);
            this.TxtWorkCenterDocNo.Name = "TxtWorkCenterDocNo";
            this.TxtWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkCenterDocNo.Properties.MaxLength = 250;
            this.TxtWorkCenterDocNo.Size = new System.Drawing.Size(331, 20);
            this.TxtWorkCenterDocNo.TabIndex = 28;
            this.TxtWorkCenterDocNo.Validated += new System.EventHandler(this.TxtWorkCenterDocNo_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(49, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Item";
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(85, 46);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(331, 20);
            this.TxtItName.TabIndex = 31;
            this.TxtItName.Validated += new System.EventHandler(this.TxtItName_Validated);
            // 
            // ChkItName
            // 
            this.ChkItName.Location = new System.Drawing.Point(419, 46);
            this.ChkItName.Name = "ChkItName";
            this.ChkItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItName.Properties.Appearance.Options.UseFont = true;
            this.ChkItName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItName.Properties.Caption = " ";
            this.ChkItName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItName.Size = new System.Drawing.Size(19, 22);
            this.ChkItName.TabIndex = 32;
            this.ChkItName.ToolTip = "Remove filter";
            this.ChkItName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItName.ToolTipTitle = "Run System";
            this.ChkItName.CheckedChanged += new System.EventHandler(this.ChkItName_CheckedChanged);
            // 
            // ChkWorkCenterDocNo
            // 
            this.ChkWorkCenterDocNo.Location = new System.Drawing.Point(419, 25);
            this.ChkWorkCenterDocNo.Name = "ChkWorkCenterDocNo";
            this.ChkWorkCenterDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWorkCenterDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkWorkCenterDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWorkCenterDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWorkCenterDocNo.Properties.Caption = " ";
            this.ChkWorkCenterDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWorkCenterDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkWorkCenterDocNo.TabIndex = 29;
            this.ChkWorkCenterDocNo.ToolTip = "Remove filter";
            this.ChkWorkCenterDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWorkCenterDocNo.ToolTipTitle = "Run System";
            this.ChkWorkCenterDocNo.CheckedChanged += new System.EventHandler(this.ChkWorkCenterDocNo_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(5, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Work Center";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblList
            // 
            this.LblList.AutoSize = true;
            this.LblList.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblList.ForeColor = System.Drawing.Color.Red;
            this.LblList.Location = new System.Drawing.Point(6, 27);
            this.LblList.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblList.Name = "LblList";
            this.LblList.Size = new System.Drawing.Size(0, 14);
            this.LblList.TabIndex = 19;
            this.LblList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(252, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(32, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 15;
            this.label9.Text = "Inspection Date";
            // 
            // ChkStartDt
            // 
            this.ChkStartDt.Location = new System.Drawing.Point(392, 45);
            this.ChkStartDt.Name = "ChkStartDt";
            this.ChkStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkStartDt.Properties.Appearance.Options.UseFont = true;
            this.ChkStartDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkStartDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkStartDt.Properties.Caption = " ";
            this.ChkStartDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkStartDt.Size = new System.Drawing.Size(19, 22);
            this.ChkStartDt.TabIndex = 19;
            this.ChkStartDt.ToolTip = "Remove filter";
            this.ChkStartDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkStartDt.ToolTipTitle = "Run System";
            this.ChkStartDt.CheckedChanged += new System.EventHandler(this.ChkStartDt_CheckedChanged);
            // 
            // DteStartDt2
            // 
            this.DteStartDt2.EditValue = null;
            this.DteStartDt2.EnterMoveNextControl = true;
            this.DteStartDt2.Location = new System.Drawing.Point(266, 46);
            this.DteStartDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt2.Name = "DteStartDt2";
            this.DteStartDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt2.Size = new System.Drawing.Size(123, 20);
            this.DteStartDt2.TabIndex = 18;
            this.DteStartDt2.EditValueChanged += new System.EventHandler(this.DteStartDt2_EditValueChanged);
            // 
            // DteStartDt1
            // 
            this.DteStartDt1.EditValue = null;
            this.DteStartDt1.EnterMoveNextControl = true;
            this.DteStartDt1.Location = new System.Drawing.Point(128, 46);
            this.DteStartDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt1.Name = "DteStartDt1";
            this.DteStartDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt1.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt1.Size = new System.Drawing.Size(121, 20);
            this.DteStartDt1.TabIndex = 16;
            this.DteStartDt1.EditValueChanged += new System.EventHandler(this.DteStartDt1_EditValueChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(128, 88);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(261, 20);
            this.LueDeptCode.TabIndex = 28;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // ChkDeptCode
            // 
            this.ChkDeptCode.Location = new System.Drawing.Point(392, 88);
            this.ChkDeptCode.Name = "ChkDeptCode";
            this.ChkDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDeptCode.Properties.Appearance.Options.UseFont = true;
            this.ChkDeptCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDeptCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDeptCode.Properties.Caption = " ";
            this.ChkDeptCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDeptCode.Size = new System.Drawing.Size(19, 22);
            this.ChkDeptCode.TabIndex = 29;
            this.ChkDeptCode.ToolTip = "Remove filter";
            this.ChkDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDeptCode.ToolTipTitle = "Run System";
            this.ChkDeptCode.CheckedChanged += new System.EventHandler(this.ChkDeptCode_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(53, 92);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 14);
            this.label11.TabIndex = 27;
            this.label11.Text = "Department";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 36;
            this.label12.Text = "Batch#";
            // 
            // TxtBatchNo
            // 
            this.TxtBatchNo.EnterMoveNextControl = true;
            this.TxtBatchNo.Location = new System.Drawing.Point(85, 88);
            this.TxtBatchNo.Name = "TxtBatchNo";
            this.TxtBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBatchNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBatchNo.Properties.MaxLength = 250;
            this.TxtBatchNo.Size = new System.Drawing.Size(331, 20);
            this.TxtBatchNo.TabIndex = 37;
            this.TxtBatchNo.Validated += new System.EventHandler(this.TxtBatchNo_Validated);
            // 
            // ChkBatchNo
            // 
            this.ChkBatchNo.Location = new System.Drawing.Point(419, 88);
            this.ChkBatchNo.Name = "ChkBatchNo";
            this.ChkBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBatchNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBatchNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBatchNo.Properties.Caption = " ";
            this.ChkBatchNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBatchNo.Size = new System.Drawing.Size(19, 22);
            this.ChkBatchNo.TabIndex = 38;
            this.ChkBatchNo.ToolTip = "Remove filter";
            this.ChkBatchNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBatchNo.ToolTipTitle = "Run System";
            this.ChkBatchNo.CheckedChanged += new System.EventHandler(this.ChkBatchNo_CheckedChanged);
            // 
            // FrmRptMaterialQCInspection3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 473);
            this.Name = "FrmRptMaterialQCInspection3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkPlanningDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQCPDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQCPDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWorkCenterDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkPlanningDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkWhsCode;
        private DevExpress.XtraEditors.CheckEdit ChkDocNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtPlanningDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkWorkCenterDocNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblList;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtItName;
        private DevExpress.XtraEditors.CheckEdit ChkItName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.CheckEdit ChkStartDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt2;
        internal DevExpress.XtraEditors.DateEdit DteStartDt1;
        private DevExpress.XtraEditors.TextEdit TxtWorkCenterDocNo;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit TxtQCPDesc;
        private DevExpress.XtraEditors.CheckEdit ChkQCPDesc;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private DevExpress.XtraEditors.CheckEdit ChkDeptCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtBatchNo;
        private DevExpress.XtraEditors.CheckEdit ChkBatchNo;
    }
}