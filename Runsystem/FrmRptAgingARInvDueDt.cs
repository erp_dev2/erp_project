﻿#region update 
/*
     * 27/05/2021 [RDH/PHT] :  reporting baru untuk melihat piutang yang akan jatuh tempo
 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInvDueDt : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mMInd = false, mIsRptAgingARInvShowDOCt = false;
        internal bool mIsUseMInd = false, mIsRptAgingARInvUseDocDtFilter = false, mIsRptShowVoucherLoop = false;

        private List<AmountDueDt> lAmountDueDt = null;
        #endregion

        #region Constructor

        public FrmRptAgingARInvDueDt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion-

        #region method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueMth(LueMth);
                Sl.SetLueYr(LueYr,"");
                int day = DateTime.DaysInMonth(2021, 5);

            }catch(Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        private string SetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            string Filter = " ", Filter2 = " ", Filter3 = " ", Filter4 = " ";

            //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

            Filter2 = Filter;

            //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DueDt");
            //Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
            //Sm.FilterDt(ref Filter3, ref cm, Sm.GetDte(DteDocumentDt1), Sm.GetDte(DteDocumentDt2), "A.DocDt");
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T1.CtCode", true);
            Sm.CmParam(ref cm, "@years", Sm.GetLue(LueYr));
            Sm.CmParam(ref cm, "@months", Sm.GetLue(LueMth));

           
            SQL.AppendLine("Select T1.Period, T1.DueDt, T1.AgingDays, T1.CtCode, T1.DocNo, T1.DocDt, T2.CtName, T4.CtCtName, T1.CurCode, T1.LocalDocNo, T1.TaxInvDocument, ");
            SQL.AppendLine("T1.TotalAmt, T1.TotalTax, T1.Downpayment, T1.Amt, T1.PaidAmt, T1.VoucherDocNo,T1.Balance ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
            SQL.AppendLine("    A.DueDt, DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays,  ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)+IfNull(C.ARSAmt, 0) As PaidAmt, B.VoucherDocNo,  ");
            SQL.AppendLine("    A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    g.WhsName, G.EntName, G.DOCtDocNo, ");
            SQL.AppendLine("    A.TaxCode1, ((D.Taxrate/100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate/100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate/100) * A.TotalAmt) As taxAmt3 ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' ");
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1  ");
            SQL.AppendLine("        Inner Join TblARSHdr T2 On T1.DocNo=T2.SalesInvoiceDocNo And T2.CancelInd='N'  ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.DocNo  ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesInvoiceHdr A ");
            SQL.AppendLine("	        Inner Join TblsalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("    )G On A.DocNo = G.DocNo ");
            //if (mIsUseMInd)
            //{
            //    if (!mMInd)
            //        SQL.AppendLine("        And A.MInd='N' ");
            //}
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
            SQL.AppendLine("        A.DocDt As DueDt,  ");
            SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), A.DocDt) As AgingDays,  ");
            SQL.AppendLine("        A.CtCode, A.DocNo, A.DocDt,  A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
            SQL.AppendLine("        -1*A.TotalAmt As TotalAmt,  ");
            SQL.AppendLine("        0 As TotalTax,  ");
            SQL.AppendLine("        0 As Downpayment,  ");
            SQL.AppendLine("        -1*A.TotalAmt As Amt,  ");
            SQL.AppendLine("        IfNull(B.PaidAmt, 0) As PaidAmt, B.VoucherDocNo,   ");
            SQL.AppendLine("        (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("        C.WhsName, C.EntName, C.DOCtDocNo, ");
            SQL.AppendLine("        null, 0 taxAmt1, null, 0 taxAmt2, null, 0 As taxAmt3 ");
            SQL.AppendLine("        From TblSalesReturnInvoiceHdr A  ");
            SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("            From TblSalesReturnInvoiceHdr B1  ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='2'  ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("            Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("            Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("            Where B1.CancelInd='N' ");
            SQL.AppendLine("            Group By B1.DocNo  ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesReturnInvoiceDtl A  ");
            SQL.AppendLine("	        Inner Join tblRecvCtHdr B On A.RecvCtDocno = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("        )C On A.DocNo = C.Docno ");
            SQL.AppendLine("        Where A.CancelInd='N'  ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            //if (mIsRptAgingARInvShowDOCt)
            //{
            //    SQL.AppendLine("Left Join ( ");
            //    SQL.AppendLine("    Select A.DocNo, ");
            //    SQL.AppendLine("    Group_Concat(Distinct B.DOCtDocNo Order By B.DOCtDocNo Separator ', ') As DOCtDocNo ");
            //    SQL.AppendLine("    From TblSalesInvoiceHdr A, TblSalesInvoiceDtl B ");
            //    SQL.AppendLine("    Where A.DocNo=B.DocNo ");
            //    SQL.AppendLine("    And B.DOCtDocNo Is Not Null ");
            //    SQL.AppendLine("    And A.CancelInd='N' ");
            //    SQL.AppendLine(Filter);
            //    SQL.AppendLine("    Group By A.DocNo ");
            //    SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            //}
            SQL.AppendLine("Left Join TblCustomerCategory T4 On T2.CtCtCode=T4.CtCtCode ");
            SQL.AppendLine("Where T1.Balance<>0.00 AND DueDt between CONCAT(@years,'-',@months,'-01') and last_day(CONCAT(@years,'-',@months,'-01'))");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By T1.Period, T1.CurCode, T2.CtName, T1.DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {

            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Customer",
                    "Curency",
                    "Period",
                    "Invoice#", 
                    "Local Document#",
                    
                    //6-10
                    "Tax Invoice", 
                    "Downpayment",
                    "Invoice Amount",
                    "Paid/Settled",
                    "Balance",
                    //11
                    "DueDt"

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 100, 100, 165, 100,  
                    
                    //6-10
                    150, 120, 120, 120, 120,

                    //11
                    0

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8,9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);

        }

        override protected void ShowData()
        {
            try{
                if (isSearchData()) return;
                Cursor.Current = Cursors.WaitCursor;             

                var cm = new MySqlCommand();
                
                Sm.ShowDataInGrid(ref Grd1, ref cm, SetSQL(ref cm), new string[]
                    {
                        // 0
                        "CtName",
                        //1-5
                        "CurCode",
                        "Period",
                        "DocNo",
                        "LocalDocNo",
                        "TaxInvDocument",
                        //6-10
                        "Downpayment",
                        "Amt",
                        "PaidAmt",
                        "Balance",
                        "DueDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int row) =>
                    {
                        Grd1.Cells[row, 0].Value = row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, row, 11, 10);


                    }, true, false, false, false
                );

                lAmountDueDt = new List<AmountDueDt>();
                setDaysToCol();
                setColumn();
            }catch(Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning,Exc.Message);
            }
        }

        private void setDaysToCol()
        {
            Grd1.Cols.Count = 12;
            var LatestColumn = Grd1.Cols.Count - 1;
            //Grd1.Cols.Count += DateTime.DaysInMonth(Convert.ToInt32(TxtYear.Text), Convert.ToInt32(TxtMonth.Text));
            
            var cm1 = new MySqlCommand();

            
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                var daysColumn = new StringBuilder();
               
                daysColumn.AppendLine("select Date_Format(a.Date,'%Y%m%d') Date ");
                daysColumn.AppendLine("from (");
                daysColumn.AppendLine("select last_day(CONCAT(@years,'-',@months,'-01')) - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date");
                daysColumn.AppendLine("from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a");
                daysColumn.AppendLine("cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b");
                daysColumn.AppendLine("cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c");
                daysColumn.AppendLine(")a");
                daysColumn.AppendLine("where a.Date between CONCAT(@years,'-',@months,'-01') and last_day(CONCAT(@years,'-',@months,'-01')) order by a.Date;");


                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandTimeout = 600;
                cm1.CommandText = daysColumn.ToString();
                Sm.CmParam(ref cm1, "@years", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm1, "@months", Sm.GetLue(LueMth));

                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { 
                    "Date" 
                });
                
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        LatestColumn += 1;
                        //if (mFirstColumnForWhs == -1) mFirstColumnForWhs = LatestColumn;
                        lAmountDueDt.Add(new AmountDueDt()
                        {
                            dates = Sm.DrStr(dr1, c1[0])
                        });
                        Grd1.Cols.Count +=1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.FormatDate2("dd/MMM/yyyy",Sm.DrStr(dr1, c1[0]));
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 120;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                
                    dr1.Close();
            }
        }

        private void setColumn()
        {
            for (int grdRowCount = 0; grdRowCount < Grd1.Rows.Count; grdRowCount++)
            {
                for (int coldata = 12; coldata < lAmountDueDt.Count; coldata++)
                {
                    string staticCol = Sm.FormatDate2("dd/MMM/yyyy",Sm.GetGrdStr(Grd1, grdRowCount, 11)); // 20210112
                    string DynamicCol = Grd1.Header.Cells[0, coldata].Value.ToString(); //20210112
                    if (staticCol == DynamicCol)
                    {
                        Grd1.Cells[grdRowCount, coldata].Value = Sm.GetGrdDec(Grd1, grdRowCount, 8);
                    }
                }
            }
        }

        private bool isSearchData()
        {
            return
               Sm.IsLueEmpty(LueMth, "Month") ||
               Sm.IsLueEmpty(LueYr, "Year");
        }

        #endregion

        #region class 
        private class AmountDueDt {
            public string dates { get; set; }
            public decimal amt { get; set; }
            public int Column { get; set; }
        }
        #endregion
    }
}
