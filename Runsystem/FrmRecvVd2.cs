﻿#region Update
/*
    13/10/2017 [TKG] tambah local document# dan Vendor's DO#    
    06/02/2018 [ARI] printout KIM 
    19/05/2018 [TKG] saat cancel tidak boleh dicancel ulang.
    29/05/2018 [TKG] tambah status PO.
    01/10/2018 [TKG] untuk KIM menggunakan document date sebagai batch# kalau kosong
    03/10/2018 [TKG] kalau saat insert fixed item tidak perlu dilakukan validasi dan update ke summary dan movement saat cancel data.
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/02/2019 [TKG] tambah informasi kawasan berikat
    13/09/2019 [WED] tambah tarik data dari Dropping Request
    23/09/2019 [WED] tampilkan informasi Dropping Payment
    25/09/2019 [TKG] disable validasi IsDR_VendorInvalid() saat insert
    14/10/2019 [WED/MAI] masuk ke stock movement, kolom CustomsDocCode
    23/10/2019 [DITA/MAI] tambah KBContractNo, KBSubmissionNo, KBRegistrationNo saat save
    15/11/2019 [WED/TWC] Site Mandatory berdasarkan parameter IsSiteMandatory
    28/01/2020 [WED/YK] tambah auto input DO, berdasarkan parameter MenuCodeForRecvVd2AutoCreateDO
    04/03/2020 [VIN/KBN] Generate Docno 6 digit
    18/03/2020 [IBL/MMM] tambah keterangan vendor name pada kolom batch#
    20/03/2020 [HAR/KBN] tambah filter item category 
    30/03/2020 [TKG/YK] bug saat auto do akan mengupdate stock movement dan stock summary.
    18/05/2020 [TKG/IMS] menambah project code, menggunakan project code sbg batch# berdasarkan parameter IsRecvVd2ProjectCodeEnabled. 
    19/05/2020 [VIN/MMM] tambah inputan expired date 
    11/06/2020 [HAR/TWC] bug : saat save approval POR belum  melihat department alhasil doc approvalnya terbuat sebanyak approval settingnya
    17/06/2020 [HAR/TWC] bug : param MenuCodeForRecvVd2AutoCreateDO, item yg diambil item service,item service tidak masuk ke stock, bug : waktu cancel masih melihat stock 
    25/06/2020 [HAR/TWC] bug : data doubel saat show data karena join ke DO dept
    06/07/2020 [TKG/IMS] tambah specification dan item local code
    13/07/2020 [TKG/IMS] tambah default untuk kebutuhan migrasi.
    25/07/2020 [TKG/TWC] Edit data tidak bisa ubah price dan rounding
    10/08/2020 [TKG/IMS] Project yg tidak aktif tidak dimunculkan.
    14/08/2020 [TKG/TWC] Merubah proses validasi cost category ada atau tidak ada dengan langsung mencek di database
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    29/09/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    07/10/2020 [WED/IMS] tambah inputan SO Contract# berdasarkan parameter IsRecvVd2AutoCreateDOUseSOContract & IsRecvVd2AutoCreateDOSOContractBasedOnProject
    09/10/2020 [WED/TWC] bug kolom unknown kolom SOContractDocNo
    20/11/2020 [IBL/ALL] Membuat jurnal saat cancel RecvVd2 Auto DO
    11/01/2021 [ICA/SRN] Validasi COA saat save dan cancel auto journal based on param mIsCheckCOAJournalNotExists 
    14/01/2021 [ICA/IMS] Membuat printout AutoDO dan penyesuaian printout yg bukan AutoDO
    28/01/2021 [DITA/PHT] Account number untk journal saat param IsItemCategoryUseCOAAPAR = Y sumber ambil dari AcNo AP di  ditem category
    11/03/2021 [TKG/IMS] menambah heat number dan attachment file
    18/03/2021 [VIN/IMS] bug: parameter belum dipanggil di GetParameter()
    31/03/2021 [VIN/IMS] Cost Center tidak dapat dipilih jika tidak aktiv
    06/04/2021 [VIN/PHT] save cccode ke save journal 
    15/04/2021 [WED/IMS] saat insert, cost center belom di clear dan load ulang
    15/04/2021 [WED/IMS] ketika auto DO di cancel, maka DO nya juga ikut di cancel
    04/05/2021 [TKG/IMS] merubah cara menyimpan batch# di TblDODeptDtl
    21/05/2021 [VIN/IMS] remark SetCostCategory, Cost category ambil dr dialog
    28/05/2021 [TRI/IMS] Pilihan departemen dibuat hanya menampilkan departemen yang aktif
    07/06/2021 [VIN/ALL] Bug GenerateDocNo
    10/06/2021 [ICA/ALL] tambah validasi setting journal
    20/06/2021 [TKG/PHT] 
    profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
    cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    30/06/2021 [VIN/ALL] Bug GenerateDocNo PO
    04/08/2021 [IBL/PHT] penyesuaian coa jurnal (Without PO). AcNo credit dari item category (COA AP Uninvoiced). Berdasarkan parameter RecvVd2CreditAcNoSource
    27/08/2021 [IBL/PHT] Pada saat receiving belum ambil coa dari item category
    29/10/2021 [ICA/IMS] Journal Receiving Without PO Auto DO tidak melihat MAP
    03/11/2021 [ICA/IMS] bug saat save journal
    11/11/2021 [ARI/AMKA] set lue cost center terfilter berdasarkan group dengan menggunakan filter IsFilterByCC
    30/12/2021 [RDA/PHT] support : opsi lue cost center belum muncul berdasarkan param IsRecvVd2UseProfitCenter
    04/01/2022 [TRI/AMKA] BUG opsi lue cost center saat insert kemudian klik refresh di luenya masih belum sesuai
    27/01/2022 [TKG/PHT] ubah GetParameter() dan proses save
    31/01/2022 [DITA/AMKA] Jurnal Cancel Receiving Item From Vendor Without PO - Auto DO) tidak balance
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    08/04/2022 [DITA/PHT] saat save journal cancel tambah validasi currendt based on param IsClosingJournalBasedOnMultiProfitCenter
    04/08/2022 [HPH/SIER] Otorisasi vendor category tiap user pada masing masing group dan memudahkan user mencari berdasarkan hak aksesnya
    23/08/2022 [DITA/SKI] tambah param IsJournalRecvVdAutoDOUseCOAEntity untuk magerin jurnal auto do supaya hanya ambil dari coa entity atau cost categor saja
    02/09/2022 [BRI/VIR] detail terdouble
    08/09/2022 [MYA/PRODUCT] Membuat 1 parameter untuk menampung warehouse bayangan yang digunakan ketika client membeli modul ASM tapi tidak membeli IMM dan hide filed warehouse saat insert maupun find dan menambahkan status approval di detail
    14/09/2022 [TYO/PRODUCT] Update MRDocNo di Dropping request set NULL saat cancel
    19/09/2022 [TYO/PRODUCT] penyesuaian UpdateDroppingRequestDtl()
    20/09/2022 [RDA/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial
    22/09/2022 [TYO/PRODUCT] penyesuaian UpdateDroppingRequestDtl()
    28/09/2022 [VIN/ALL] BUG UpdateDroppingRequestDtl()
    29/09/2022 [RDA/VIR] penyesuaian query penarikan data dropping request berdasarkan param IsDroppingRequestUseMRReceivingPartial (RECV)
    03/10/2022 [BRI/VIR] Bug save data
    04/10/2022 [RDA/VIR] Bug show data + warning command text ketika cancel data (based on trx dgn param IsDroppingRequestUseMRReceivingPartial)
    19/10/2022 [VIN/SIER] BUG lue Department
    25/10/2022 [MAU/BBT] hide field Project dengan parameter
    11/11/2022 [BRI/PHT] rubah source profit center code
    11/11/2022 [MAU/BBT] penyesuaian printout receiving item vendor without po 
    15/11/2022 [MAU/BBT] menambah parameter 'IsRecvVdWithoutPOAutoDOUploadFileMandatory' untuk upload file bersifat mandatory
    27/11/2022 [MAU/BBT] Feedback - penyesuaian printout
    12/12/2022 [MAU/BBT] Feedback - validasi label field file dan ukuran upload file 
    11/01/2023 [MAU/BBT] Validasi tidak bisa cancel ketika dokumen sedang digunakan pada DO to Department
    12/01/2023 [MYA/MNET] Mengubah Field upload file di menu Receiving item From Vendor (Without PO)
    12/01/2023 [RDA/PHT] generate new journal docno pht
    16/01/2023 [RDA/PHT] menambah validasi untuk field price dan quantity pada grid tidak boleh negatif
    17/01/2023 [MYA/VIR] Mengubah format jurnal AP uninvoiced agar bisa masuk ke masing-masing project dan departemen
    17/01/2023 [RDA/PHT] menambah validasi untuk field diskon pada grid tidak boleh negatif dan tidak boleh lebih dari 100
    31/01/2023 [BRI/SIER] tambah flagging pada PO berdasarkan param IsPOShowFlaggingInformation
    09/02/2023 [MYA/MNET] Penyesuaian Print Out di menu Receiving Item From Vendor (Without PO)
    09/02/2023 [RDA/MNET] cost center untuk journal melihat cccode whs / lop berdasarkan docno DRQ yang exist Recvvd2CostCenterJournalFormat
    13/02/2023 [BRI/MNET] tambah approval untuk RecvVd2 berdasarkan param IsRecvVd2NeedApproval & IsRecvVd2ApprovalBasedOnDept
    16/02/2023 [IBL/MNET] Approval lihat ke whs berdasarkan IsRecvVdApprovalBasedOnWhs
    23/02/2023 [RDA/MNET] penyesuaian ac no debit recvvd2 berdasarkan param IsRecvVd2ForDroppingRequestEnabled == "Y" && mRecvvd2JournalFormat == "2"
    27/02/2023 [RDA/MNET] fix if null untuk acno ketika project code kosong 
    28/02/2023 [RDA/MNET] fix bug warning ketika method SaveJournalAutoDO
    16/03/2023 [RDA/MNET] bug fix method untuk ubah debit ac no receiving
    27/03/2023 [BRI/TWC] bug print data
    12/04/2023 [SET/MNET] Penyesuaian validasi Document Approval berdasar Start Amount & End Amount DAS
    18/04/2023 [SET/BBT] Penyesuaian dokumen tidak bisa dicancel ketika masih digunakan di trc lain
    02/05/2023 [BRI/MNET] Bug Download File
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;
using System.Threading;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            IsProcFormat = string.Empty,
            mDroppingRequestBCCode = string.Empty,
            mDocNo = string.Empty,
            mWhsRecvVd2ForAsset = string.Empty;

        internal FrmRecvVd2Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private int mGrd3 = 0;
        private string
            mDocType = "13",
            mRecvVdCustomsDocCodeManualInput = string.Empty,
            mDocNoFormat = string.Empty,
            mDocTitle = string.Empty,
            mEntCode = string.Empty, // 0m // 0.01 --> double, 0.01m decimal TaxRate * 0.01m * x
            mRecvVd2PtCodeDefault = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mRecvVd2CreditAcNoSource = string.Empty,
            mFormatFTPClient = string.Empty,
            mRecv2UploadFileFormat = string.Empty,
            mJournalDocNoFormat = string.Empty,
            mRecvvd2CostCenterJournalFormat = "1",
            mRecvvd2JournalFormat = string.Empty
            ;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsRecvVd2NeedApproval = false,
            mIsRemarkForApprovalMandatory = false,
            mIsRecvVdSaveJournal = false,
            mIsRecvVdApprovalBasedOnWhs = false,
            mIsAutoJournalActived = false,
            mIsBatchNoUseDocDtIfEmpty = false,
            mIsSiteMandatory = false,
            mIsMovingAvgEnabled = false,
            mIsRecvVd2ProjectCodeEnabled = false,
            mIsPurchaseRequestForProductionEnabled = false,
            mIsRecvVd2AutoCreateDOUseSOContract = false,
            mIsCheckCOAJournalNotExists = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsRecvVd2AllowToUploadFile = false,
            mIsRecvVd2JournalUseCCCode = false,
            mIsApprove = false,
            mIsAllCancel = false,
            mIsAPfromReceivingUseCOAfromProjectandDeptShortCode = false,
            mIsPOShowFlaggingInformation = false,
            mIsRecvvd2UploadfileMandatory = false,
            mIsRecvVd2ApprovalBasedOnDept = false;
        internal bool
            mIsAutoGenerateBatchNo = false,
            mIsAutoGenerateBatchNoEditable = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsKawasanBerikatEnabled = false,
            mIsRecvVd2ForDroppingRequestEnabled = false,
            mMenuCodeForRecvVd2AutoCreateDO = false,
            mIsRecvVdUseOptionBatchNoFormula = false,
            mIsFilterByItCt = false,
            mIsRecvVd2ShowSpecificationEnabled = false,
            mIsRecvVd2AutoCreateDOSOContractBasedOnProject = false,
            mIsRecvVd2HeatNumberEnabled = false,
            mIsRecvVd2UseProfitCenter = false,
            mIsFilterByCC = false,
            mIsFilterByVendorCategory = false,
            mIsJournalRecvVdAutoDOUseCOAEntity = false,
            mIsDroppingRequestUseMRReceivingPartial = false,
            mIsRecvVdWithoutPOAutoDOUploadFileMandatory = false,
            mIsDroppingRequestUseType = false
            ;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmRecvVd2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Vendor (Without PO)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Tp4.PageVisible = mIsRecvVd2AllowToUploadFile;
                if (!mIsRecvVd2AutoCreateDOUseSOContract)
                    LblSOContractDocNo.Visible = TxtSOContractDocNo.Visible = BtnSOContractDocNo.Visible = BtnSOContractDocNo2.Visible = false;
                SetGrd();
                if (mRecv2UploadFileFormat == "2")
                {
                    TcRecvVd.TabPages.Remove(Tp4);
                }
                else
                {
                    TcRecvVd.TabPages.Remove(Tp5);
                }
                SetFormControl(mState.View);
                if (mIsSiteMandatory) LblSite.ForeColor = Color.Red;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                //Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                SetLueVdCode2(ref LueVdCode, string.Empty);
                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);
                if (!mIsRecvVd2UseProfitCenter)
                    Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin);
                LueLot.Visible = false;
                LueBin.Visible = false;
                label35.Visible = true;
                LueProjectCode.Visible = true;
                if (!mMenuCodeForRecvVd2AutoCreateDO)
                {
                    LblCCCode.Visible = LueCCCode.Visible =
                    LblEntCode.Visible = LueEntCode.Visible = false;
                }
                if (mMenuCodeForRecvVd2AutoCreateDO && mWhsRecvVd2ForAsset.Length > 0)
                {
                    label4.Visible = LueWhsCode.Visible = false;
                    TxtVdDONo.Top -= 21; LueVdCode.Top -= 21; LueSiteCode.Top -= 21; LuePtCode.Top -= 21; LueCurCode.Top -= 21;
                    label3.Top -= 21; label14.Top -= 21; LblSite.Top -= 21; label7.Top -= 21; label6.Top -= 21;
                }
                Tp3.PageVisible = mIsRecvVd2ForDroppingRequestEnabled;
                if (mIsKawasanBerikatEnabled)
                {
                    Tp2.PageVisible = true;
                    TcRecvVd.SelectedTabPage = Tp2;
                    Sl.SetLueOption(ref LueCustomsDocCode, "CustomsDocCode");
                }
                TcRecvVd.SelectedTabPage = Tp1;
                if (!mIsRecvVd2ProjectCodeEnabled)
                {
                    label35.Visible = false;
                    LueProjectCode.Visible = false;
                }

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 42;
            Grd1.FrozenArea.ColCount = 10;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                        //0
                    "DNo",

                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Cancel Reason",
                    "Status",
                    "",

                    //6-10
                    "",
                    "Item's Code",
                    "",
                    "Item's Name",
                    "Group",

                    //11-15
                    "Foreign Name",
                    "ItScCode",
                    "Sub-Category",
                    "Batch#",
                    "Source",

                    //16-20
                    "Lot",
                    "Bin",
                    "Price",
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",  

                    //21-25
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Discount"+Environment.NewLine+"%",

                    //26-30
                    "Rounding",
                    "Total",
                    "Remark",
                    "Dropping Request Quantity",
                    "Dropping Request's"+Environment.NewLine+"Amount",

                    //31-35
                    "Dropping Request Vendor Code",
                    "Vendor"+Environment.NewLine+"(Dropping Request)",
                    "Dropping Payment's" +Environment.NewLine+ "Amount",
                    "Cost Category Code",
                    "Cost Category",

                    //36-40
                    "LOP#",
                    "Inventory COA#",
                    "Expired",
                    "Local Code",
                    "Specification",

                    //41
                    "Heat Number"
                },
                    new int[]
                {
                    //0
                    0,
                    //1-5
                    50, 0, 200, 80, 20, 
                    //6-10
                    20, 120, 20, 200, 150,  
                    //11-15
                    150, 0, 150, 200, 180,
                    //16-20
                    130, 130, 120, 120, 120,  
                    //21-25
                    120, 120, 120, 120, 80, 
                    //26-30
                    100, 120, 200, 0, 130,
                    //31-35
                    0, 180, 180, 0, 200,
                    //36-40
                    200, 200, 100, 120, 200,
                    //41
                    200
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 38 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 21, 23, 25, 26, 27, 29, 30, 33 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColButton(Grd1, new int[] { 6, 8, 5 });
            if (IsProcFormat == "0")
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 15, 16, 17, 21, 22, 23, 24, 12, 13, 4, 5, 10, 11 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 15, 16, 17, 21, 22, 23, 24, 12, 4, 5, 10, 11 }, false);
            if (!mIsShowForeignName) Grd1.Cols[11].Visible = true;
            if (mIsItGrpCodeShow) Grd1.Cols[10].Visible = true;
            if (!mMenuCodeForRecvVd2AutoCreateDO) Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37 });
            Grd1.Cols[33].Move(31);
            if (!mIsRecvVd2ForDroppingRequestEnabled) Sm.GrdColInvisible(Grd1, new int[] { 29, 30, 31, 32, 33 });
            if (!mIsRecvVd2ShowSpecificationEnabled) Sm.GrdColInvisible(Grd1, new int[] { 40 });
            Grd1.Cols[39].Move(10);
            Grd1.Cols[40].Move(11);
            if (mIsRecvVd2HeatNumberEnabled)
                Grd1.Cols[41].Move(18);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 41 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                    "Total Quantity 1", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[]
                    {
                    130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            #region Grid 3

            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2")
            {
                Grd3.Cols.Count = 8;
                Grd3.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd3,
                        new string[]
                        {
                //0
                "D No",
                //1-5
                "File Name",
                "",
                "D",
                "Upload By",
                "Date",

                //6
                "Time",
                "FileName2"
                        },
                            new int[]
                        {
                0,
                250, 20, 20, 100, 100,
                100,0
                        }
                    );

                Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd3, new int[] { 5 });
                Sm.GrdFormatTime(Grd3, new int[] { 6 });
                Sm.GrdColButton(Grd3, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd3.Cols[2].Move(1);
            }

            #endregion



            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
            if (mIsRecvVd2NeedApproval)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
            {
                DteDocDt, TxtLocalDocNo, LueDeptCode, LueWhsCode, LueVdCode,
                TxtVdDONo, LuePtCode, LueCurCode, LueTaxCode1, LueTaxCode2,
                LueTaxCode3, MeeRemark, LueSiteCode, ChkKBNonDocInd, LueCustomsDocCode,
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo,
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty, LueCCCode,
                LueEntCode, LueProjectCode
            }, true);
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41 });
                    BtnKBContractNo.Enabled = false;
                    BtnDroppingRequestDocNo.Enabled = false;
                    BtnSOContractDocNo.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVd2AllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVd2AllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVd2AllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2")
                    {
                        Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
            {
                DteDocDt, TxtLocalDocNo, LueDeptCode, LueWhsCode, LueVdCode,
                TxtVdDONo, LuePtCode, LueCurCode, LueTaxCode1, LueTaxCode2,
                LueTaxCode3, MeeRemark, LueSiteCode, ChkKBNonDocInd, LueCustomsDocCode,
                LueCCCode, LueEntCode, LueProjectCode
            }, false);
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                    if (mIsAutoGenerateBatchNo)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 16, 17, 18, 19, 21, 23, 25, 26, 28, 38, 41 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 14, 16, 17, 18, 19, 21, 23, 25, 26, 28, 38, 41 });

                    //BtnKBRegistrationNo.Enabled = true;
                    BtnDroppingRequestDocNo.Enabled = true;
                    BtnSOContractDocNo.Enabled = true;
                    if (mIsRecvVd2AllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                        if (mRecv2UploadFileFormat == "2") Sm.GrdColReadOnly(false, true, Grd3, new int[] { 2, 3 });
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2") Sm.GrdColReadOnly(false, true, Grd3, new int[] { 2 });

                    mGrd3 = Grd3.Rows.Count - 1;
                    if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2" && mIsApprove) Sm.GrdColReadOnly(true, true, Grd3, new int[] { 2 });
                    if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2" && mIsAllCancel) Sm.GrdColReadOnly(true, true, Grd3, new int[] { 2 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDroppingRequestBCCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
    {
        TxtDocNo, DteDocDt, TxtLocalDocNo, LueDeptCode, LueWhsCode,
        LueVdCode, TxtVdDONo, LuePtCode, LueCurCode, LueTaxCode1,
        LueTaxCode2, LueTaxCode3, MeeRemark, LueSiteCode, LueLot,
        LueBin, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt,
        TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging,
        LueCustomsDocCode, TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth,
        TxtDR_PRJIDocNo, TxtDR_BCCode, MeeDR_Remark, LueCCCode, LueEntCode,
        LueProjectCode, TxtSOContractDocNo, TxtFile, TxtFile2, TxtFile3, LueCCCode, LueEntCode
    });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
    {
        TxtTaxAmt, TxtDiscountAmt, TxtAmt, TxtKBPackagingQty,
        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance
    }, 0);
            ChkKBNonDocInd.Checked = false;
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2") Sm.ClearGrd(Grd3, true);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 18, 19, 21, 23, 25, 26, 27, 29, 30 });
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvVd2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                //Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
                SetLueVdCode2(ref LueVdCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mMenuCodeForRecvVd2AutoCreateDO)
                {
                    if (mIsRecvVd2UseProfitCenter)
                        Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, string.Empty, "N");
                    else
                        Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
                }
                if (mRecvVd2PtCodeDefault.Length > 0) Sm.SetLue(LuePtCode, mRecvVd2PtCodeDefault);
                if (mIsRecvVd2ProjectCodeEnabled) SetLueProjectCode(ref LueProjectCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            mIsApprove = false;
            mIsAllCancel = true;
            mGrd3 = 0;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (!Sm.GetGrdBool(Grd1, Row, 1))
                {
                    mIsAllCancel = false;
                }
                if (Sm.GetGrdStr(Grd1, Row, 4) == "Approved" && !Sm.GetGrdBool(Grd1, Row, 1))
                {
                    mIsApprove = true;
                }
            }
            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2" && mIsApprove) Grd3.Rows.Count = Grd3.Rows.Count - 1;
            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2" && mIsAllCancel) Grd3.Rows.Count = Grd3.Rows.Count - 1;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var x = new StringBuilder();

            x.AppendLine("Select C.EntCode ");
            x.AppendLine("From TblWarehouse A ");
            x.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            x.AppendLine("    And A.WhsCode = @Param ");
            x.AppendLine("Inner Join TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode ");

            mEntCode = Sm.GetValue(x.ToString(), Sm.GetLue(LueWhsCode));

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string par = Sm.GetParameter("NumberOfInventoryUomCode");
            if (par == "1")
            {
                ParPrint(1);
            }
            else if (par == "2")
            {
                ParPrint(2);
            }
            else
                ParPrint(3);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 38) Sm.DteRequestEdit(Grd1, DteExpiredDt, ref fCell, ref fAccept, e);

            if (e.ColIndex == 6 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (mMenuCodeForRecvVd2AutoCreateDO)
                    {
                        if (!Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                            Sm.FormShowDialog(new FrmRecvVd2Dlg(this));
                    }
                    else
                        Sm.FormShowDialog(new FrmRecvVd2Dlg(this));
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));
            }

            if (e.ColIndex == 5)
            {
                e.DoDefault = false;
                ShowApprovalInfo(e.RowIndex);
            }

            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 0))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && !IsItemEmpty(e) && Sm.IsGrdColSelected(new int[] { 14, 16, 17, 18, 19, 21, 23, 25, 26, 28, 41 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);

                if (Sm.IsGrdColSelected(new int[] { 16 }, e.ColIndex))
                {
                    LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    //SetLueLot(ref LueLot);
                }

                if (Sm.IsGrdColSelected(new int[] { 17 }, e.ColIndex))
                {
                    LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    //SetLueBin(ref LueBin);
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
                ComputeTotalQty();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && TxtDocNo.Text.Length == 0)
            {
                if (mMenuCodeForRecvVd2AutoCreateDO)
                {
                    if (!Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                        Sm.FormShowDialog(new FrmRecvVd2Dlg(this));
                }
                else
                    Sm.FormShowDialog(new FrmRecvVd2Dlg(this));
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 5) ShowApprovalInfo(e.RowIndex);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 41 }, e);

                if (Sm.IsGrdColSelected(new int[] { 18, 19, 21, 23, 25, 26 }, e.ColIndex) &&
                    Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

                if (e.ColIndex == 19)
                {
                    if (Sm.CompareStr(
                        Sm.GetGrdStr(Grd1, e.RowIndex, 20),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 22))
                        )
                        Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 19);
                    else
                    {
                        Grd1.Cells[e.RowIndex, 21].Value =
                            Sm.GetGrdDec(Grd1, e.RowIndex, 19) *
                            ConvertUom(
                                Sm.GetGrdStr(Grd1, e.RowIndex, 20),
                                Sm.GetGrdStr(Grd1, e.RowIndex, 22));
                    }
                }

                if (e.ColIndex == 19)
                {
                    if (Sm.CompareStr(
                        Sm.GetGrdStr(Grd1, e.RowIndex, 20),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 24))
                        )
                        Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 19);
                    else
                    {
                        Grd1.Cells[e.RowIndex, 23].Value =
                            Sm.GetGrdDec(Grd1, e.RowIndex, 19) *
                            ConvertUom(
                                Sm.GetGrdStr(Grd1, e.RowIndex, 20),
                                Sm.GetGrdStr(Grd1, e.RowIndex, 24));
                    }
                }

                if (e.ColIndex == 21)
                {
                    if (Sm.CompareStr(
                        Sm.GetGrdStr(Grd1, e.RowIndex, 22),
                        Sm.GetGrdStr(Grd1, e.RowIndex, 24))
                        )
                        Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 21);
                    else
                    {
                        Grd1.Cells[e.RowIndex, 23].Value =
                            Sm.GetGrdDec(Grd1, e.RowIndex, 21) *
                            ConvertUom(
                                Sm.GetGrdStr(Grd1, e.RowIndex, 22),
                                Sm.GetGrdStr(Grd1, e.RowIndex, 24));
                    }
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 18, 19, 25, 26 }, e.ColIndex)) ComputeTotal(e.RowIndex);
                if (Sm.IsGrdColSelected(new int[] { 19, 21, 23 }, e.ColIndex)) ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 19, 21, 23 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        //UPLOAD FILE - START

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFileGrd(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFileGrd(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedRow = 0;
            for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
                SelectedRow = Grd3.SelectedRows[Index].Index;
            if (SelectedRow >= mGrd3)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }


        //UPLOAD FILE - END

        #endregion

        #region Save Data

        #region Insert Data

        private string GeneratePODocNoIMS()
        {
            //0001/IMS/PO-UMUM/04/20 (Untuk Umum)
            //0001/IMS/PO-LOG/04/20 (Untuk Logistik)

            string DocDt = Sm.GetDte(DteDocDt);
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2);
            var SQL = new StringBuilder();
            var Abbr = (Sm.GetLue(LueProjectCode).Length > 0) ? "PO-LOG" : "PO-UMUM";
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblPOHdr ");
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       And DocNo Like '%" + Abbr + "%' ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/IMS/', '" + Abbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblPOHdr ");
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       And DocNo Like '%" + Abbr + "%' ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/IMS/', '" + Abbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            string SubCategory = Sm.GetGrdStr(Grd1, 0, 22);
            string
                //DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "RecvVd2", "TblRecvVdHdr", SubCategory),
                MaterialRequestDocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory),
                QtDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Qt", "TblQtHdr"),
                //GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "Qt", "TblQtHdr", SubCategory),
                PORequestDocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PORequest", "TblPORequestHdr", SubCategory);
            string PODocNo = string.Empty;

            if (mDocTitle == "IMS")
                PODocNo = GeneratePODocNoIMS();
            else
                PODocNo = GenerateDocNo2ForPO(IsProcFormat, Sm.GetDte(DteDocDt), "PO", "TblPOHdr", ref LueTaxCode1, SubCategory);

            if (mDocNoFormat == "1")
                DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "RecvVd2", "TblRecvVdHdr", SubCategory);
            else
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "RecvVd2", "TblRecvVdHdr", mEntCode, "1");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvVdHdr(DocNo));
            cml.Add(SaveRecvVdDtl(DocNo, PODocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0) 
            //        cml.Add(SaveRecvVdDtl(DocNo, PODocNo, Row));

            //auto DO
            var lDOHdr = new List<DODept2Hdr>();
            var lDODtl = new List<DODept2Dtl>();

            if (mMenuCodeForRecvVd2AutoCreateDO)
            {
                ProcessDODtl(ref lDODtl, DocNo);
                if (lDODtl.Count > 0)
                {
                    bool mFlag = false;
                    for (int i = 0; i < lDODtl.Count; ++i)
                    {
                        if (lDODtl[i].LOPDocNo.Length > 0)
                        {
                            mFlag = true;
                            break;
                        }
                    }

                    if (mFlag)
                    {
                        lDODtl.OrderByDescending(x => x.LOPDocNo);
                        for (int i = 0; i < lDODtl.Count; ++i)
                        {
                            ProcessDODtl2(ref lDODtl);
                        }
                        ProcessDOHdr2(ref lDOHdr, ref lDODtl, DocNo);
                    }
                    else
                    {
                        string mDODeptDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr");
                        UpdateDODtl(ref lDODtl, mDODeptDocNo);
                        ProcessDOHdr(ref lDOHdr, mDODeptDocNo, DocNo);
                    }
                }

                cml.Add(SaveDODept2(ref lDOHdr, ref lDODtl));
                //for (int x = 0; x < lDOHdr.Count; ++x)
                //{
                //    cml.Add(SaveDODept2Hdr(ref lDOHdr, x));
                //    for (int i = 0; i < lDODtl.Count; ++i)
                //    {
                //        if (lDOHdr[x].DocNo == lDODtl[i].DocNo)
                //            cml.Add(SaveDODept2Dtl(ref lDODtl, i));
                //    }
                //}
            }

            cml.Add(SaveStock(DocNo));

            if (mMenuCodeForRecvVd2AutoCreateDO)
            {
                for (int x = 0; x < lDOHdr.Count; ++x)
                    cml.Add(SaveStockDODept(lDOHdr[x].DocNo));
            }

            cml.Add(SaveMiscTransactions(DocNo, MaterialRequestDocNo, QtDocNo, PORequestDocNo, PODocNo));

            if (mIsAutoJournalActived)
            {
                if (!mMenuCodeForRecvVd2AutoCreateDO) cml.Add(SaveJournal(DocNo));
                else
                {
                    string mDODeptDocNo = string.Empty;
                    foreach (var x in lDOHdr)
                    {
                        if (mDODeptDocNo.Length > 0) mDODeptDocNo += ",";
                        mDODeptDocNo += x.DocNo;
                    }
                    cml.Add(SaveJournalAutoDO(DocNo, mDODeptDocNo));
                }
            }

            if (mRecv2UploadFileFormat != "2")
            {
                if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(DocNo);
                if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                    UploadFile2(DocNo);
                if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                    UploadFile3(DocNo);
            }
            else
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsRecvVd2AllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(DocNo));
            }

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (mIsRecvVd2AllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                    {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd3, Row, 1));
                    }
                }
            }

            Sm.StdMsg(mMsgType.Info,
                        "New Document# : " + DocNo + Environment.NewLine +
                        "Saving new data is completed.");



            ShowData(DocNo);

            lDODtl.Clear(); lDOHdr.Clear();
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            if (DocTitle.Length == 0) DocTitle = "XXX";
            var SQL = new StringBuilder();

            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNo2ForPO(string IsProcFormat, string DocDt, string DocType, string Tbl, ref LookUpEdit LueTax, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                IsPOSplit = Sm.GetParameter("IsPOSplitBasedOnTax");
            if (DocTitle.Length == 0) DocTitle = "XXX";

            int LengthOfDocNoWithTitle = 4 + DocTitle.Length + DocAbbr.Length + 4 + 4; //4=nomor urut(0001), 4=bln+tahun(1115), 4 = jmlh "/"
            int LengthOfDocNoWithoutTitle = 4 + DocAbbr.Length + 4 + 3;

            var SQL = new StringBuilder();

            if (IsPOSplit == "Y" && Sm.GetLue(LueTax).Length == 0)
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("        Select Convert(left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("        And DocNo Not Like '%" + DocTitle + "%' ");
                    SQL.Append("        Order By left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("    Select Convert(left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4), Decimal) As DocNo  From " + Tbl);
                    SQL.Append("    Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("    And DocNo Not Like '%" + DocTitle + "%' ");
                    SQL.Append("    Order By left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
                    SQL.Append(") As DocNo");
                }
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("        And DocNo Like '%" + DocTitle + "%' ");
                    SQL.Append("        Order By left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       And DocNo Like '%" + DocTitle + "%' ");
                    SQL.Append("       Order By left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mWhsRecvVd2ForAsset.Length == 0 && Sm.IsLueEmpty(LueWhsCode, "Warehouse")) ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsLueEmpty(LuePtCode, "Term of Payment") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mMenuCodeForRecvVd2AutoCreateDO && Sm.IsLueEmpty(LueCCCode, "Cost Center")) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyNotValid() ||

                (mIsDroppingRequestUseMRReceivingPartial && IsTotalPerItemInvalid()) ||

                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsSubcategoryDifferent() ||
                //IsDR_VendorInvalid() ||
                IsDR_QtyInvalid() ||
                IsDR_AmtInvalid() ||
                IsDR_BalanceInvalid() ||
                (mMenuCodeForRecvVd2AutoCreateDO && IsCostCategoryStillEmpty()) ||
                //(mMenuCodeForRecvVd2AutoCreateDO)||
                (mIsRecvVdWithoutPOAutoDOUploadFileMandatory && Sm.IsTxtEmpty(TxtFile, "File", false)) ||
                IsJournalSettingInvalid() || IsUploadFileNotValid() ||
                (mIsRecvvd2UploadfileMandatory && IsUploadFileMandatory())
                ;
        }

        private bool IsUploadFileMandatory()
        {

            if (Grd3.Rows.Count == 1 && mRecv2UploadFileFormat == "2")
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                TcRecvVd.SelectedTabPage = Tp5;
                return true;
            }
            return false;
        }

        private bool IsTotalPerItemInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length > 0 && !IsGrdEmpty())
            {
                string mDroppingRequestDocNo = TxtDroppingRequestDocNo.Text;

                var l = new List<DroppingRequest>();
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DocNo, B.ResourceItCode AS ItemCode, A.RemunerationAmt + A.DirectCostAmt AS MaxTotal ");
                SQL.AppendLine("From TblDroppingRequestDtl A ");
                SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo = B.DocNo And A.DocNo = @DroppingRequestDocNo ");
                SQL.AppendLine("Inner Join TblProjectImplementationRBPDtl C On B.DocNo = C.DocNo And A.PRBPDNo = C.DNo ");
                SQL.AppendLine("Inner Join TblItem D On B.ResourceItCode = D.ItCode ");
                SQL.AppendLine("Left Join TblVendor E On A.VdCode = E.VdCode ");
                SQL.AppendLine("Left Join TblVendorBankAccount F On E.VdCode = F.VdCode And A.VdBankAcDNo = F.DNo ");
                SQL.AppendLine("Left Join TblBank G On F.BankCode = G.BankCode ");
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("Select A.DocNo, A.ItCode AS ItemCode, A.Qty * A.Amt AS MaxTotal ");
                SQL.AppendLine("From TblDroppingRequestDtl2 A ");
                SQL.AppendLine("Inner Join TblBudgetCategory B On A.BCCode = B.BCCode And A.DocNo = @DroppingRequestDocNo ");
                SQL.AppendLine("Left Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("Left Join TblVendor D On A.VdCode = D.VdCode ");
                SQL.AppendLine("Left Join TblVendorBankAccount E On A.VdCode = E.VdCode And A.VdBankAcDNo = E.DNo ");
                SQL.AppendLine("Left join TblBank F On E.BankCode = F.BankCode ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();

                    Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", mDroppingRequestDocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "ItemCode", "MaxTotal" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new DroppingRequest()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                ItCode = Sm.DrStr(dr, c[1]),
                                MaxTotal = Sm.DrDec(dr, c[2]),
                            });
                        }
                    }
                    dr.Close();
                }

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    foreach (var x in l)
                    {
                        if (TxtDroppingRequestDocNo.Text == x.DocNo && Sm.GetGrdStr(Grd1, i, 7) == x.ItCode && Sm.GetGrdDec(Grd1, i, 27) > x.MaxTotal)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Total item " + Sm.GetGrdStr(Grd1, i, 9) + " exceeds maximum limit (" + Sm.FormatNum(x.MaxTotal, 0) + ")");
                            Sm.FocusGrd(Grd1, i, 27);
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCCCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter  " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    Value);
        }

        private bool IsCostCategoryStillEmpty()
        {
            // SetCostCategory();
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 34).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, i, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, i, 9) + Environment.NewLine +
                        "Cost category's COA Setting still empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !");
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, i, 37).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, i, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, i, 9) + Environment.NewLine +
                        "COA's account# is empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !");
                    return true;
                }
            }

            return false;
        }

        private bool IsDR_VendorInvalid()
        {
            if (!mIsRecvVd2ForDroppingRequestEnabled) return false;

            if (TxtDroppingRequestDocNo.Text.Length > 0)
            {
                string mVdCode = Sm.GetLue(LueVdCode);

                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 7).Length > 0)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 31) != mVdCode)
                        {
                            var mMsg = new StringBuilder();

                            mMsg.AppendLine("Item Code : " + Sm.GetGrdStr(Grd1, i, 7));
                            mMsg.AppendLine("Item Name : " + Sm.GetGrdStr(Grd1, i, 9));
                            mMsg.AppendLine("Dropping's Vendor : " + Sm.GetGrdStr(Grd1, i, 32));
                            mMsg.AppendLine("Dropping's Vendor is different with Receiving's Vendor. ");

                            Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                            Sm.FocusGrd(Grd1, i, 32);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsDR_AlreadyProcessed1()
        {
            if (!mIsRecvVd2ForDroppingRequestEnabled) return false;

            if (TxtDR_PRJIDocNo.Text.Length > 0)
            {
                if (Sm.IsDataExist("Select 1 From TblDroppingRequestDtl Where MRDocNo Is Not Null And DocNo=@Param Limit 1;", TxtDroppingRequestDocNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            if (mDroppingRequestBCCode.Length > 0)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblDroppingRequestDtl2 Where MRDocNo Is Not Null And DocNo=@Param1 And BCCode=@Param2 Limit 1;", TxtDroppingRequestDocNo.Text, mDroppingRequestBCCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_QtyInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Qty1 = 0m, Qty2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty1 = 0m;
                Qty2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 19).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 19);
                if (Sm.GetGrdStr(Grd1, r, 29).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 29);

                if (Qty1 > Qty2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                        "Requested Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 19), 0) + Environment.NewLine +
                        "Dropping Request's Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 29), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Quantity is bigger than Dropping Request's Quantity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_AmtInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Amt1 = 0m, Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Amt1 = 0m;
                Amt2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 27).Length != 0) Amt1 = Sm.GetGrdDec(Grd1, r, 27);
                if (Sm.GetGrdStr(Grd1, r, 30).Length != 0) Amt2 = Sm.GetGrdDec(Grd1, r, 30);

                if (Amt1 > Amt2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                        "Requested Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 27), 0) + Environment.NewLine +
                        "Dropping Request's Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 30), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Amount is bigger than Dropping Request's Amount.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_BalanceInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            //ComputeDR_OtherMRAmt();
            ComputeDR_Balance();
            if (decimal.Parse(TxtDR_Balance.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Dropping Request's Balance : " + TxtDR_Balance.Text + Environment.NewLine +
                        "Dropping Request's balance is less than 0.00.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "Item is empty.") ||
                    //Sm.IsGrdValueEmpty(Grd1, Row, 11, true, "Unit price should be greater than 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 19, true, "Quantity (Inventory 1) should be greater than 0.") ||
                    (mNumberOfInventoryUomCode > 1 && Sm.IsGrdValueEmpty(Grd1, Row, 21, true, "Quantity (Inventory 2) should be greater than 0.")) ||
                    (mNumberOfInventoryUomCode > 2 && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quantity (Inventory 3) should be greater than 0.")) ||
                    (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 28, false, "Remark is empty."))
                    )
                    return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    if (
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 20), Sm.GetGrdStr(Grd1, Row, 22)) && Sm.GetGrdDec(Grd1, Row, 19) != Sm.GetGrdDec(Grd1, Row, 21)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 20), Sm.GetGrdStr(Grd1, Row, 24)) && Sm.GetGrdDec(Grd1, Row, 19) != Sm.GetGrdDec(Grd1, Row, 23)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 22), Sm.GetGrdStr(Grd1, Row, 24)) && Sm.GetGrdDec(Grd1, Row, 21) != Sm.GetGrdDec(Grd1, Row, 23))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine +
                            "Lot : " + Sm.GetGrdStr(Grd1, Row, 16) + Environment.NewLine +
                            "Bin : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine +
                            "Quantity is not valid.");
                        return true;
                    }
                }
            }

            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 12).Length == 0)
                {
                    Grd1.Cells[Row, 12].Value = Grd1.Cells[Row, 13].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 12);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different sub category ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        //private bool IsCOAJournalNotValid()
        //{
        //    var l = new List<COA>();
        //    var SQL = new StringBuilder();
        //    string mItCode = string.Empty;
        //    var cm = new MySqlCommand();

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
        //        {
        //            if (mItCode.Length > 0) mItCode += " Or ";
        //            mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
        //        }
        //    }

        //    if (!mMenuCodeForRecvVd2AutoCreateDO)
        //    {
        //        SQL.AppendLine("Select B.AcNo From TblItem A");
        //        SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
        //        SQL.AppendLine("Where "+mItCode);
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue as AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode='VendorAcNoUnInvoiceAP' ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Select AcNo2 as AcNo ");
        //        SQL.AppendLine("From TblEntity Where EntCode=@EntCodeWhs ");
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select C.AcNo as AcNo ");
        //        SQL.AppendLine("From TblItem A ");
        //        SQL.AppendLine("Inner Join TblItemCostCategory B On A.ItCode = B.ItCode And B.CCCode = @CCCode ");
        //        SQL.AppendLine("Inner Join TblCostCategory C On B.CCtCode = C.CCtCOde And B.CCCode = C.CCCode ");
        //        SQL.AppendLine("Where "+mItCode);

        //        //Credit RecvVd

        //        if (mIsItemCategoryUseCOAAPAR)
        //        {
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select B.AcNo8 as AcNo ");
        //            SQL.AppendLine("From TblItem A ");
        //            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode Where " +mItCode);
        //        }
        //        else
        //        {
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select ParValue as AcNo ");
        //            SQL.AppendLine("From TblParameter where ParCode='VendorAcNoUnInvoiceAP' ");
        //        }
        //    }

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();

        //        Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
        //        Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new COA()
        //                {
        //                    AcNo = Sm.DrStr(dr, c[0])

        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    foreach (var x in l.Where(w => w.AcNo.Length <= 0))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //        return true;
        //    }

        //    return false;
        //}

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            string mVendorAcNoUnInvoiceAP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='VendorAcNoUnInvoiceAP'");
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            if (!mMenuCodeForRecvVd2AutoCreateDO)
            {
                //Parameter
                if (mVendorAcNoUnInvoiceAP.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter VendorAcNoUnInvoiceAP is empty.");
                    return true;
                }

                //Table
                if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;
            }
            else
            {
                if (Sm.GetValue("Select AcNo2 From TblEntity Where EntCode = @Param", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode))).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Warehouse's COA account# (" + LueWhsCode.Text + ") is empty.");
                    return true;
                }

                if (IsJournalSettingInvalid_CostCategory(Msg)) return true;

                if (mIsItemCategoryUseCOAAPAR)
                {
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo8")) return true;
                }
                else
                {
                    if (mVendorAcNoUnInvoiceAP.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter VendorAcNoUnInvoiceAP is empty.");
                        return true;
                    }

                }

            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 7);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + (COA == "AcNo8" ? "(AP Uninvoiced)" : "") + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, CCtName = string.Empty;

            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode And B.AcNo is Null ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine("    And A.ItCode In ( ");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 7);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRecvVdHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("/* RecvVd - Hdr */ ");

            SQL.AppendLine("Insert Into TblRecvVdHdr(DocNo, DocDt, LocalDocNo, POInd, WhsCode, VdCode, VdDONo, ");
            SQL.AppendLine("DeptCode, PtCode, CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, DiscountAmt, Amt, Remark, SiteCode, ");
            SQL.AppendLine("CustomsDocCode, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, KBPackaging, KBPackagingQty, KBNonDocInd, ");
            if (mIsRecvVd2ProjectCodeEnabled)
                SQL.AppendLine("ProjectCode, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0)
                SQL.AppendLine("FileName, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0)
                SQL.AppendLine("FileName2, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0)
                SQL.AppendLine("FileName3, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, 'N', @WhsCode, @VdCode, @VdDONo, ");
            SQL.AppendLine("@DeptCode, @PtCode, @CurCode, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt, @DiscountAmt, @Amt, @Remark, @SiteCode, ");
            SQL.AppendLine("@CustomsDocCode, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @KBPackaging, @KBPackagingQty, @KBNonDocInd, ");
            if (mIsRecvVd2ProjectCodeEnabled)
                SQL.AppendLine("@ProjectCode, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0)
                SQL.AppendLine("@FileName, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0)
                SQL.AppendLine("@FileName2, ");
            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0)
                SQL.AppendLine("@FileName3, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            if (mWhsRecvVd2ForAsset.Length > 0)
            {
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsRecvVd2ForAsset);
            }
            else
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdDONo", TxtVdDONo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", decimal.Parse(TxtDiscountAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", Sm.GetLue(LueCustomsDocCode));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@KBPackaging", TxtKBPackaging.Text);
            Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", decimal.Parse(TxtKBPackagingQty.Text));
            Sm.CmParam<String>(ref cm, "@KBNonDocInd", ChkKBNonDocInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetLue(LueProjectCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@FileName", TxtFile.Text);
            Sm.CmParam<String>(ref cm, "@FileName2", TxtFile2.Text);
            Sm.CmParam<String>(ref cm, "@FileName3", TxtFile3.Text);
            return cm;
        }

        private MySqlCommand SaveRecvVdDtl(string DocNo, string PODocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* RecvVd - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");
            SQL.AppendLine("Set @ShorName=IfNull((Select ShortName From TblVendor Where VdCode=@VdCode), ''); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        if (mIsAutoGenerateBatchNo)
                        {
                            SQL.AppendLine("Insert Into TblRecvVdDtl ");
                            SQL.AppendLine("(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, ");
                            SQL.AppendLine("QtyPurchase, Qty, Qty2, Qty3, UPrice, Discount, RoundingValue, Remark, ExpiredDt, ");
                            if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("HeatNumber,  ");
                            SQL.AppendLine("CreateBy, CreateDt) ");
                        }
                        else
                        {
                            SQL.AppendLine("Insert Into TblRecvVdDtl ");
                            SQL.AppendLine("(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, ");
                            SQL.AppendLine("QtyPurchase, Qty, Qty2, Qty3, UPrice, Discount, RoundingValue, Remark, ExpiredDt, ");
                            if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("HeatNumber,  ");
                            SQL.AppendLine("CreateBy, CreateDt) ");
                        }
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    if (mIsAutoGenerateBatchNo)
                    {
                        SQL.AppendLine(
                            "(@DocNo, @DNo_" + r.ToString() +
                            ", 'N', @Status, @PODocNo, @DNo_" + r.ToString() +
                            ", @ItCode_" + r.ToString() +
                            ", ");
                        if (mIsRecvVdUseOptionBatchNoFormula)
                            SQL.AppendLine("@BatchNo_" + r.ToString() + ", ");
                        else
                        {
                            if (mIsRecvVd2ProjectCodeEnabled && Sm.GetLue(LueProjectCode).Length > 0)
                                SQL.AppendLine("@ProjectCode, ");
                            else
                                SQL.AppendLine("Concat(IfNull(@ShortName, ''), '-', @DocDt, '-', @DocNo), ");
                        }
                        SQL.AppendLine(
                            "Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                            "), IfNull(@Lot_" + r.ToString() +
                            ", '-'), IfNull(@Bin_" + r.ToString() +
                            ", '-'), ");
                        SQL.AppendLine(
                            "@Qty_" + r.ToString() +
                            ", @Qty_" + r.ToString() +
                            ", @Qty2_" + r.ToString() +
                            ", @Qty3_" + r.ToString() +
                            ", @UPrice_" + r.ToString() +
                            ", @Discount_" + r.ToString() +
                            ", @RoundingValue_" + r.ToString() +
                            ", @Remark_" + r.ToString() +
                            ", @ExpiredDt_" + r.ToString() +
                            ", ");
                        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ", ");
                        SQL.AppendLine("@UserCode, @Dt) ");
                    }
                    else
                    {
                        SQL.AppendLine(
                            "(@DocNo, @DNo_" + r.ToString() +
                            ", 'N', @Status, @PODocNo, @DNo_" + r.ToString() +
                            ", @ItCode_" + r.ToString() +
                            ", IfNull(@BatchNo_" + r.ToString() +
                            ", ");
                        SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                        SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                            "), IfNull(@Lot_" + r.ToString() +
                            ", '-'), IfNull(@Bin_" + r.ToString() +
                            ", '-'), ");
                        SQL.AppendLine("@Qty_" + r.ToString() +
                            ", @Qty_" + r.ToString() +
                            ", @Qty2_" + r.ToString() +
                            ", @Qty3_" + r.ToString() +
                            ", @UPrice_" + r.ToString() +
                            ", @Discount_" + r.ToString() +
                            ", @RoundingValue_" + r.ToString() +
                            ", @Remark_" + r.ToString() +
                            ", @ExpiredDt_" + r.ToString() +
                            ", ");
                        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ", ");
                        SQL.AppendLine("@UserCode, @Dt) ");
                    }

                    #region DocApproval

                    if (mIsRecvVd2NeedApproval)
                    {
                        SQL2.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Select T.DocType, @DocNo, @DNo_" + r.ToString() + ", T.DNo, @UserCode, @Dt ");
                        SQL2.AppendLine("From TblDocApprovalSetting T ");
                        SQL2.AppendLine("Where T.DocType='RecvVd2' ");
                        
                        if (mIsRecvVdApprovalBasedOnWhs)
                        {
                            SQL2.AppendLine("And T.WhsCode In ( ");
                            SQL2.AppendLine("   Select WhsCode From TblRecvVdHdr Where DocNo=@DocNo ");
                            SQL2.AppendLine(") ");
                        }

                        if (mIsRecvVd2ApprovalBasedOnDept)
                        {
                            SQL2.AppendLine("And T.DeptCode In ( ");
                            SQL2.AppendLine("    Select DeptCode From TblRecvVdHdr Where DocNo=@DocNo ");
                            SQL2.AppendLine(") ");
                        }
                        SQL2.AppendLine("And (T.EndAmt=0.00 ");
                        SQL2.AppendLine("Or T.EndAmt>=IfNull(( ");
                        SQL2.AppendLine("    Select Amt ");
                        SQL2.AppendLine("    From TblRecvVdHdr  ");
                        SQL2.AppendLine("    Where DocNo=@DocNo ");
                        SQL2.AppendLine("), 0.00)) ");
                        SQL2.AppendLine("And (T.StartAmt=0.00 ");
                        SQL2.AppendLine("Or T.StartAmt<=IfNull(( ");
                        SQL2.AppendLine("    Select Amt ");
                        SQL2.AppendLine("    From TblRecvVdHdr  ");
                        SQL2.AppendLine("    Where DocNo=@DocNo ");
                        SQL2.AppendLine("), 0.00)) ");
                        SQL2.AppendLine("; ");

                        SQL2.AppendLine("Update TblRecvVdDtl Set Status='A' ");
                        SQL2.AppendLine("Where DocNo=@DocNo And DNo=@DNo_" + r.ToString());
                        SQL2.AppendLine(" And Not Exists( ");
                        SQL2.AppendLine("    Select 1 From TblDocApproval ");
                        SQL2.AppendLine("    Where DocType='RecvVd2' And DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ); ");
                    }

                    #endregion

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 14));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 16));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 17));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 21));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<Decimal>(ref cm, "@Discount_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 25));
                    Sm.CmParam<Decimal>(ref cm, "@RoundingValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 28));
                    Sm.CmParamDt(ref cm, "@ExpiredDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 38));
                    Sm.CmParam<String>(ref cm, "@HeatNumber_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 41));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@Status", mIsRecvVd2NeedApproval ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetLue(LueProjectCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveRecvVdDtl(string DocNo, string PODocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    if (mIsAutoGenerateBatchNo)
        //    {
        //        SQL.AppendLine("Insert Into TblRecvVdDtl ");
        //        SQL.AppendLine("(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, ");
        //        SQL.AppendLine("QtyPurchase, Qty, Qty2, Qty3, UPrice, Discount, RoundingValue, Remark, ExpiredDt, ");
        //        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("HeatNumber,  ");
        //        SQL.AppendLine("CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select @DocNo, @DNo, 'N', @Status, @PODocNo, @DNo, @ItCode, ");
        //        if (mIsRecvVdUseOptionBatchNoFormula)
        //            SQL.AppendLine("@BatchNo, ");
        //        else
        //        {
        //            if (mIsRecvVd2ProjectCodeEnabled && Sm.GetLue(LueProjectCode).Length>0)
        //                SQL.AppendLine("@ProjectCode As BatchNo, ");
        //            else
        //                SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo) As BatchNo, ");
        //        }
        //        SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), ");
        //        SQL.AppendLine("@Qty, @Qty, @Qty2, @Qty3, @UPrice, @Discount, @RoundingValue, @Remark, @ExpiredDt, ");
        //        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("@HeatNumber,  ");
        //        SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //        SQL.AppendLine("From TblRecvVdHdr A ");
        //        SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
        //        SQL.AppendLine("Where A.DocNo=@DocNo; ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Insert Into TblRecvVdDtl ");
        //        SQL.AppendLine("(DocNo, DNo, CancelInd, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, ");
        //        SQL.AppendLine("QtyPurchase, Qty, Qty2, Qty3, UPrice, Discount, RoundingValue, Remark, ExpiredDt, ");
        //        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("HeatNumber,  ");
        //        SQL.AppendLine("CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@DocNo, @DNo, 'N', @Status, @PODocNo, @DNo, @ItCode, IfNull(@BatchNo, ");
        //        SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
        //        SQL.AppendLine("), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), ");
        //        SQL.AppendLine("@Qty, @Qty, @Qty2, @Qty3, @UPrice, @Discount, @RoundingValue, @Remark, @ExpiredDt, ");
        //        if (mIsRecvVd2HeatNumberEnabled) SQL.AppendLine("@HeatNumber,  ");
        //        SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    //Sm.CmParam<String>(ref cm, "@Status", mIsRecvVd2NeedApproval ? "O" : "A");
        //    Sm.CmParam<String>(ref cm, "@Status", "A");
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 14));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 16));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 17));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 19));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 21));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 23));
        //    Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 25));
        //    Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 26));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 28));
        //    Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetLue(LueProjectCode));
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetGrdDate(Grd1, Row, 38));
        //    Sm.CmParam<String>(ref cm, "@HeatNumber", Sm.GetGrdStr(Grd1, Row, 41));

        //    return cm;
        //}

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.POInd='N'; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.POInd='N'; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, ");
            //SQL.AppendLine("B.UPrice, ");

            SQL.AppendLine("B.UPrice-");
            SQL.AppendLine("(B.UPrice*B.Discount/100)+");
            SQL.AppendLine("((1/B.Qty)*B.RoundingValue) ");
            SQL.AppendLine("As UPrice, ");

            SQL.AppendLine("Case When A.CurCode='IDR' Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2='IDR' ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.POInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockDODept(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, B.Lot, B.Bin, B.Source ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=0.00, T1.Qty2=0.00, T1.Qty3=0.00;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "05");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMiscTransactions(string DocNo, string MaterialRequestDocNo, string QtDocNo, string PORequestDocNo, string PODocNo)
        {
            bool PORequestApprovalForAllDept = IsPORequestApprovalForAllDept();

            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - Misc Transaction */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblQtDtl As T1  ");
            SQL.AppendLine("Inner Join TblQtHdr T2 On T1.DocNo=T2.DocNo And T2.VdCode=@VdCode And T2.PtCode=@PtCode ");
            SQL.AppendLine("Set T1.ActInd='N', T1.LastUpBy=@CreateBy, T1.LastUpDt=@Dt ");
            SQL.AppendLine("Where T1.ActInd='Y' ");
            SQL.AppendLine("And T1.ItCode In (");
            SQL.AppendLine("    Select Distinct B.ItCode ");
            SQL.AppendLine("    From TblRecvVdHdr A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocNo=@DocNo And A.POInd='N' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblQtHdr(DocNo, DocDt, SystemNo, VdCode, PtCode, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNo, DocDt, Concat(Left(DocDt, 6), Left(@QtDocNo, 4)), VdCode, PtCode, CurCode, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblQtDtl(DocNo, DNo, ItCode, ActInd, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNo, B.DNo, ");
            SQL.AppendLine("B.ItCode, 'Y', B.UPrice, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.POInd='N'; ");

            SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, DocDt, CancelInd, Status, DeptCode, SiteCode, ReqType, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine("DroppingRequestDocNo, DroppingRequestBCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @MaterialRequestDocNo, DocDt, 'N', 'A', DeptCode, @SiteCode, '2', ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine("@DroppingRequestDocNo, @DroppingRequestBCCode, ");
            SQL.AppendLine("Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, Status, ProcessInd, ItCode, Qty, UsageDt, QtDocNo, QtDNo, UPrice, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Select @MaterialRequestDocNo, B.DNo, ");
            SQL.AppendLine("'N', 'A', 'F', B.ItCode, B.QtyPurchase, A.DocDt, ");
            SQL.AppendLine("@QtDocNo, (Select Max(DNo) As DNo From TblQtDtl Where DocNo=@QtDocNo And ItCode=B.ItCode And UPrice=B.UPrice) As QtDNo, ");
            SQL.AppendLine("B.UPrice, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.POInd='N'; ");

            if (TxtDroppingRequestDocNo.Text.Length > 0 && !mIsDroppingRequestUseMRReceivingPartial)
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl A ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@MaterialRequestDocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
                if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@MaterialRequestDocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                    SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    //SQL.AppendLine("And A.ItCode=@ItCode ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
            }

            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl A ");
                    SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@MaterialRequestDocNo ");
                    SQL.AppendLine("Set A.DRSourceInd='2' ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
                if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@MaterialRequestDocNo ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                    SQL.AppendLine("Set A.DRSourceInd='2' ");
                    SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                    //SQL.AppendLine("And A.ItCode=@ItCode ");
                    SQL.AppendLine("And A.MRDocNo Is Null; ");
                }
            }

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, UserCode, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select 'MaterialRequest', A.DocNo, B.DNo, C.DNo As ApprovalDNo, 'System', 'A', Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting C On A.DeptCode=C.DeptCode And C.DocType='MaterialRequest' ");
            SQL.AppendLine("Where A.DocNo=@MaterialRequestDocNo; ");

            SQL.AppendLine("Insert Into TblPORequestHdr(DocNo, DocDt, SiteCode, DeptCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PORequestDocNo, DocDt, @SiteCode, @DeptCode, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblPORequestDtl");
            SQL.AppendLine("(DocNo, DNo, CancelInd, Status, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, CreditLimit, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PORequestDocNo, DNo, 'N', 'A', DocNo, DNo, Qty, QtDocNo, QtDNo, 0, Null, CreateBy, CreateDt ");
            SQL.AppendLine("From TblMaterialRequestDtl Where DocNo=@MaterialRequestDocNo And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, UserCode, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select 'PORequest', A.DocNo, B.DNo, C.DNo As ApprovalDNo, 'System', 'A', Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting C On C.DocType='PORequest' ");
            SQL.AppendLine("Where A.DocNo = @PORequestDocNo ");
            if (!PORequestApprovalForAllDept)
            {
                SQL.AppendLine("And C.DeptCode =@DeptCode ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblPOHdr(DocNo, DocDt, Status, LocalDocNo, DocNoSource, RevNo, VdCode, SiteCode, VdContactPerson, ShipTo, BillTo, CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, DownPayment, Remark, CreateBy, CreateDt) ");
            if (!mIsPOShowFlaggingInformation)
                SQL.AppendLine("Select @PODocNo, DocDt, 'A', Null, @PODocNo, 0, VdCode, @SiteCode, Null, Null, Null, @CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, 0, DiscountAmt, Amt, 0, Null, @CreateBy, @Dt ");
            else
                SQL.AppendLine("Select @PODocNo, DocDt, 'A', 'Create By System', @PODocNo, 0, VdCode, @SiteCode, Null, Null, Null, @CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, 0, DiscountAmt, Amt, 0, 'Automatic Process from Receiving Item From Vendor (Without PO) No. " + DocNo + "', @CreateBy, @Dt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo And POInd='N'; ");

            SQL.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, ProcessInd, PORequestDocNo, PORequestDNo, Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PODocNo, DNo, 'N', 'F', A.DocNo, B.DNo, B.Qty, ");
            SQL.AppendLine("(Select Discount From TblRecvVdDtl Where DocNo=@DocNo And DNo=B.DNo) As Discount, ");
            SQL.AppendLine("0 As DiscountAmt, ");
            SQL.AppendLine("(Select RoundingValue From TblRecvVdDtl Where DocNo=@DocNo And DNo=B.DNo) As RoundingValue, ");
            SQL.AppendLine("A.DocDt, Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @PORequestDocNo; ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, UserCode, Status, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select 'PO', A.DocNo, '001', B.DNo As ApprovalDNo, 'System', 'A', Null, @CreateBy, @Dt ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblDocApprovalSetting B On B.DocType='PO' ");
            SQL.AppendLine("Where A.DocNo=@PODocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@QtDocNo", QtDocNo);
            Sm.CmParam<String>(ref cm, "@PORequestDocNo", PORequestDocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            string DroppingRequestDocNo = TxtDroppingRequestDocNo.Text;
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd2' And DocNo=@DocNo ");
            SQL.AppendLine("    ) ; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Vendor (Without Material Request) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd2' And DocNo=@DocNo ");
            SQL.AppendLine("    ) ; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            if(mIsRecvVd2ForDroppingRequestEnabled && mRecvvd2JournalFormat == "2")
                SQL.AppendLine("    Select IF(J.DroppingRequestDocNo is null, D.AcNo, IFNULL(CONCAT(O.ParValue, N.ProjectCode2), O.ParValue) )  AS AcNo,  ");
            else
                SQL.AppendLine("    Select D.AcNo,  ");
            SQL.AppendLine("        A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt ");
            SQL.AppendLine("        From TblRecvVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            if (mIsRecvVd2ForDroppingRequestEnabled && mRecvvd2JournalFormat == "2")
            {
                SQL.AppendLine("    LEFT JOIN tblpodtl E ON A.PODocNo=E.DocNo AND A.PODNo=E.DNo  ");
                SQL.AppendLine("    LEFT JOIN tblpohdr F ON E.DocNo=F.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tblporequestdtl G ON E.PORequestDocNo=G.DocNo AND E.PORequestDNo=G.DNo  ");
                SQL.AppendLine("    LEFT JOIN tblporequesthdr H ON G.DocNo=H.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tblmaterialrequestdtl I ON G.MaterialRequestDocNo=I.DocNo AND G.MaterialRequestDNo=I.DNo  ");
                SQL.AppendLine("    LEFT JOIN tblmaterialrequesthdr J ON I.DocNo=J.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tbldroppingrequesthdr K ON J.DroppingRequestDocNo=K.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tblprojectimplementationhdr L ON K.PRJIDocNo=L.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tblsocontractrevisionhdr M ON L.SOContractDocNo=M.DocNo  ");
                SQL.AppendLine("    LEFT JOIN tblsocontracthdr N ON M.SOCDocNo=N.DocNo  ");
                SQL.AppendLine("    LEFT Join TblParameter O On O.ParCode='DefferedChargesAcNo' AND O.ParValue IS NOT NULL ");
            }
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");

            if (mRecvVd2CreditAcNoSource == "1")
            {
                if (mIsAPfromReceivingUseCOAfromProjectandDeptShortCode)
                {
                    SQL.AppendLine("        SELECT T1.AcNo, ");
                    SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblRecvVdHdr A ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        INNER JOIN ( ");
                    SQL.AppendLine("        SELECT A.DocNo, Concat(E.ParValue, D.ProjectCode2) As AcNo");
                    SQL.AppendLine("        From TblDroppingRequestHdr A");
                    SQL.AppendLine("        Inner Join TblProjectImplementationHdr B on A.PRJIDocNo = B.DocNo");
                    SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                    SQL.AppendLine("        Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
                    SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And IfNull(E.ParValue, '')<>''");
                    SQL.AppendLine("        Where A.DocNo = @DroppingRequestDocNo");

                    SQL.AppendLine("        Union All ");


                    SQL.AppendLine("        SELECT A.DocNo, Concat(C.ParValue, B.ShortCode) As AcNo ");
                    SQL.AppendLine("        FROM TblDroppingRequestHdr A");
                    SQL.AppendLine("        INNER JOIN tbldepartment B ON A.DeptCode = B.DeptCode ");
                    SQL.AppendLine("        INNER Join TblParameter C On C.ParCode='VendorAcNoUnInvoiceAP' And IfNull(C.ParValue, '')<>''  ");
                    SQL.AppendLine("        Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("        )T1 ON T1.DocNo = @DroppingRequestDocNo");
                    SQL.AppendLine("        Where A.DocNo=@DocNo");
                }
                else
                {
                    SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblRecvVdHdr A ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }
            else
            {
                SQL.AppendLine("        Select D.AcNo8 As AcNo, ");
                SQL.AppendLine("        0 As DAmt, A.Qty*B.Uprice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo8 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo = @DocNo ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            if (mIsRecvVd2JournalUseCCCode)
            {
                if (mRecvvd2CostCenterJournalFormat == "1")
                {
                    SQL.AppendLine("Update TblJournalHdr A ");
                    SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo ");
                    SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ");
                    SQL.AppendLine("    Set A.CCCode = C.CCCode");
                    SQL.AppendLine("Where B.DocNo = @DocNo; ");
                }
                else if (mRecvvd2CostCenterJournalFormat == "2")
                {
                    if (TxtDroppingRequestDocNo.Text.Length > 0)
                    {
                        SQL.AppendLine("Update TblJournalHdr A ");
                        SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo ");
                        SQL.AppendLine("    Set A.CCCode = @CCCodeDRQ");
                        SQL.AppendLine("Where B.DocNo = @DocNo; ");
                    }
                    else
                    {
                        SQL.AppendLine("Update TblJournalHdr A ");
                        SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo ");
                        SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ");
                        SQL.AppendLine("    Set A.CCCode = C.CCCode");
                        SQL.AppendLine("Where B.DocNo = @DocNo; ");
                    }
                }


            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", DroppingRequestDocNo);
            if (mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

            if (mRecvvd2CostCenterJournalFormat == "2")
            {
                string GetCCCodeDRQ = Sm.GetValue("SELECT F.CCCode " +
                        "FROM tbldroppingrequesthdr A " +
                        "INNER JOIN tblprojectimplementationhdr B ON A.PRJIDocNo = B.DocNo " +
                        "INNER JOIN tblsocontractrevisionhdr C ON B.SOContractDocNo = C.DocNo " +
                        "INNER JOIN tblsocontracthdr D ON C.SOCDocNo = D.DocNo " +
                        "INNER JOIN tblboqhdr E ON D.BOQDocNo = E.DocNo " +
                        "INNER JOIN tbllophdr F ON E.LOPDocNo = F.DocNo " +
                        "Where A.DocNo=@Param", TxtDroppingRequestDocNo.Text.Length > 0 ? TxtDroppingRequestDocNo.Text : string.Empty
                        );
                Sm.CmParam<String>(ref cm, "@CCCodeDRQ", GetCCCodeDRQ);
            }

            return cm;
        }

        private MySqlCommand SaveDODept2(ref List<DODept2Hdr> l, ref List<DODept2Dtl> l2)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true, IsFirstOrExisted2 = true;

            SQL.AppendLine("/* RecvVd - DODept */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int i = 0; i < l.Count; ++i)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, RecvVdDocNo, LOPDocNo, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@DODept2DocNo_" + i.ToString() +
                    ", @DocDt_" + i.ToString() +
                    ", @LocalDocNo_" + i.ToString() +
                    ", @WhsCode_" + i.ToString() +
                    ", @DeptCode_" + i.ToString() +
                    ", @EntCode_" + i.ToString() +
                    ", @CCCode_" + i.ToString() +
                    ", Null, @UserCode, @RecvVdDocNo_" + i.ToString() +
                    ", @LOPDocNo_" + i.ToString() +
                    ", @Remark_" + i.ToString() +
                    ", @UserCode, @Dt) ");

                if (mIsRecvVd2AutoCreateDOUseSOContract)
                {
                    SQL2.AppendLine("Update TblDODeptHdr ");
                    SQL2.AppendLine("    Set SOContractDocNo = @SOContractDocNo_" + i.ToString());
                    SQL2.AppendLine("Where DocNo = @DODept2DocNo_" + i.ToString() + "; ");
                }

                for (int j = 0; j < l2.Count; ++j)
                {
                    if (Sm.CompareStr(l[i].DocNo, l2[j].DocNo))
                    {
                        if (IsFirstOrExisted2)
                        {
                            SQL3.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, Remark, CreateBy, CreateDt) ");
                            SQL3.AppendLine("Values ");
                            IsFirstOrExisted2 = false;
                        }
                        else
                            SQL3.AppendLine(", ");

                        SQL3.AppendLine(
                            "(@DODept2DocNo__" + j.ToString() +
                            ", @DNo__" + j.ToString() +
                            ", 'N', @ItCode__" + j.ToString() +
                            ", 'N', @PropCode__" + j.ToString() +
                            ", (Select BatchNo From TblRecvVdDtl Where Source=@Source__" + j.ToString() +
                            " Limit 1), @Source__" + j.ToString() +
                            ", @Lot__" + j.ToString() +
                            ", @Bin__" + j.ToString() +
                            ", @Qty__" + j.ToString() +
                            ", @Qty2__" + j.ToString() +
                            ", @Qty3__" + j.ToString() +
                            ", Null, Null, Null, @Remark__" + j.ToString() +
                            ", @UserCode, @Dt) ");

                        Sm.CmParam<String>(ref cm, "@DODept2DocNo__" + j.ToString(), l2[j].DocNo);
                        Sm.CmParam<String>(ref cm, "@DNo__" + j.ToString(), l2[j].DNo);
                        Sm.CmParam<String>(ref cm, "@ItCode__" + j.ToString(), l2[j].ItCode);
                        Sm.CmParam<String>(ref cm, "@PropCode__" + j.ToString(), l2[j].PropCode);
                        Sm.CmParam<String>(ref cm, "@Source__" + j.ToString(), l2[j].Source);
                        Sm.CmParam<String>(ref cm, "@Lot__" + j.ToString(), l2[j].Lot);
                        Sm.CmParam<String>(ref cm, "@Bin__" + j.ToString(), l2[j].Bin);
                        Sm.CmParam<Decimal>(ref cm, "@Qty__" + j.ToString(), l2[j].Qty);
                        Sm.CmParam<Decimal>(ref cm, "@Qty2__" + j.ToString(), l2[j].Qty2);
                        Sm.CmParam<Decimal>(ref cm, "@Qty3__" + j.ToString(), l2[j].Qty3);
                        Sm.CmParam<String>(ref cm, "@Remark__" + j.ToString(), l2[j].Remark);
                    }
                }

                Sm.CmParam<String>(ref cm, "@DODept2DocNo_" + i.ToString(), l[i].DocNo);
                Sm.CmParamDt(ref cm, "@DocDt_" + i.ToString(), l[i].DocDt);
                Sm.CmParam<String>(ref cm, "@LocalDocNo_" + i.ToString(), l[i].LocalDocNo);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo_" + i.ToString(), l[i].SOContractDocNo);
                if (mWhsRecvVd2ForAsset.Length > 0)
                    Sm.CmParam<String>(ref cm, "@WhsCode_" + i.ToString(), mWhsRecvVd2ForAsset);
                else
                    Sm.CmParam<String>(ref cm, "@WhsCode_" + i.ToString(), l[i].WhsCode);
                Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), l[i].DeptCode);
                Sm.CmParam<String>(ref cm, "@EntCode_" + i.ToString(), l[i].EntCode);
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), l[i].CCCode);
                Sm.CmParam<String>(ref cm, "@Remark_" + i.ToString(), l[i].Remark);
                Sm.CmParam<String>(ref cm, "@RecvVdDocNo_" + i.ToString(), l[i].RecvVdDocNo);
                Sm.CmParam<String>(ref cm, "@LOPDocNo_" + i.ToString(), l[i].LOPDocNo);
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");
            if (!IsFirstOrExisted2) SQL3.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString() + SQL3.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #region Old Code
        //private MySqlCommand SaveDODept2Hdr(ref List<DODept2Hdr> l, int x)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, RecvVdDocNo, LOPDocNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DODept2DocNo, @DocDt, @LocalDocNo, @WhsCode, @DeptCode, @EntCode, @CCCode, Null, @EmpCode, @RecvVdDocNo, @LOPDocNo, @Remark, @UserCode, CurrentDateTime()); ");

        //    //TASK/IMS/2020/41/001 by wed
        //    if (mIsRecvVd2AutoCreateDOUseSOContract)
        //    {
        //        SQL.AppendLine("Update TblDODeptHdr ");
        //        SQL.AppendLine("    Set SOContractDocNo = @SOContractDocNo ");
        //        SQL.AppendLine("Where DocNo = @DODept2DocNo; ");
        //    }

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DODept2DocNo", l[x].DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", l[x].DocDt);
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", l[x].LocalDocNo);
        //    Sm.CmParam<String>(ref cm, "@SOContractDocNo", l[x].SOContractDocNo);
        //    Sm.CmParam<String>(ref cm, "@WhsCode", l[x].WhsCode);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", l[x].DeptCode);
        //    Sm.CmParam<String>(ref cm, "@EntCode", l[x].EntCode);
        //    Sm.CmParam<String>(ref cm, "@CCCode", l[x].CCCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@Remark", l[x].Remark);
        //    Sm.CmParam<String>(ref cm, "@RecvVdDocNo", l[x].RecvVdDocNo);
        //    Sm.CmParam<String>(ref cm, "@LOPDocNo", l[x].LOPDocNo);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    return cm;
        //}

        //private MySqlCommand SaveDODept2Dtl(ref List<DODept2Dtl> l, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DODept2DocNo, @DNo, 'N', @ItCode, 'N', @PropCode, (Select BatchNo From TblRecvVdDtl Where Source=@Source Limit 1), @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, Null, Null, Null, @Remark, @UserCode, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DODept2DocNo", l[Row].DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
        //    Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
        //    //Sm.CmParam<String>(ref cm, "@BatchNo", l[Row].BatchNo);
        //    Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
        //    Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
        //    Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
        //    Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveJournalAutoDO(string DocNo, string DODeptDocNo)
        {
            string DroppingRequestDocNo = TxtDroppingRequestDocNo.Text;
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@RecvVdDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@RecvVdDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Vendor (Without Material Request) [Auto DO] : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@RecvVdDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            if (mMenuCodeForRecvVd2AutoCreateDO && mIsRecvVd2JournalUseCCCode)
            {
                SQL.AppendLine("Update TblJournalHdr ");
                SQL.AppendLine("    Set CCCode = @CCCode ");
                SQL.AppendLine("Where DocNo = @JournalDocNo; ");

            }
            //TASK/IMS/2020/41/001 by wed

            if (mIsRecvVd2AutoCreateDOUseSOContract)
            {
                SQL.AppendLine("Update TblJournalHdr ");
                SQL.AppendLine("    Set Remark = Concat('SO Contract# : ', @SOContractDocNo, ' | ', Remark) ");
                SQL.AppendLine("Where DocNo = @JournalDocNo; ");
            }
            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EntCode, AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //Debit DO
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                SQL.AppendLine("    Union All ");
                if (mIsRecvVd2ForDroppingRequestEnabled && mRecvvd2JournalFormat == "2")
                {
                    SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                    SQL.AppendLine("    FROM( ");
                    SQL.AppendLine("    	 SELECT @EntCodeCC As EntCode,  ");
                    SQL.AppendLine("        T.AcNo,  ");
                    SQL.AppendLine("        Sum(T.Amt) As DAmt,  ");
                    SQL.AppendLine("        0.00 As CAmt  ");
                    SQL.AppendLine("        From (  ");
                    SQL.AppendLine("            Select E.AcNo,  ");
                    SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt  ");
                    SQL.AppendLine("            From TblDODeptHdr A  ");
                    SQL.AppendLine("            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo  ");
                    SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source  ");
                    SQL.AppendLine("            Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode  ");
                    SQL.AppendLine("            Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode  ");
                    SQL.AppendLine("            Inner Join TblItem F On C.ItCode=F.ItCode  ");
                    SQL.AppendLine("            Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N'  ");
                    SQL.AppendLine("            Where Find_In_Set(A.DocNo, @DODeptDocNo)  ");
                    SQL.AppendLine("        ) T Group By T.AcNo  ");
                    SQL.AppendLine("    )X1 ");
                    SQL.AppendLine("    LEFT JOIN ( ");
                    SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                    SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                    SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                    SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                    SQL.AppendLine("    	WHERE A.DocNo=@RecvVdDocNo ");
                    SQL.AppendLine("    )X2 ON 1=1 ");
                    //SQL.AppendLine("    GROUP BY X2.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                //SQL.AppendLine("    Union All ");

                //SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                //SQL.AppendLine("    D.AcNo2 As AcNo, ");
                //SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                //SQL.AppendLine("    0.00 As CAmt ");
                //SQL.AppendLine("    From TblDODeptHdr A ");
                //SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                //SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                //SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                //SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                //SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                //SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                //SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                //SQL.AppendLine("    Union All ");
                //SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                //SQL.AppendLine("    T.AcNo, ");
                //SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                //SQL.AppendLine("    0.00 As CAmt ");
                //SQL.AppendLine("    From ( ");
                //SQL.AppendLine("        Select E.AcNo, ");
                //SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                //SQL.AppendLine("        From TblDODeptHdr A ");
                //SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                //SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                //SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                //SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                //SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                //SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                //SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                //SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                //SQL.AppendLine("    ) T Group By T.AcNo ");
            }
            else
            {
                if (mIsJournalRecvVdAutoDOUseCOAEntity)
                {
                    SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                }
                //SQL.AppendLine("    Union All ");
                else
                {
                    if (mIsRecvVd2ForDroppingRequestEnabled && mRecvvd2JournalFormat == "2")
                    {
                        SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                        SQL.AppendLine("    FROM( ");
                        SQL.AppendLine("    	 SELECT @EntCodeCC As EntCode,  ");
                        SQL.AppendLine("        T.AcNo,  ");
                        SQL.AppendLine("        Sum(T.Amt) As DAmt,  ");
                        SQL.AppendLine("        0.00 As CAmt  ");
                        SQL.AppendLine("        From (  ");
                        SQL.AppendLine("            Select E.AcNo, ");
                        SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("            From TblDODeptHdr A ");
                        SQL.AppendLine("            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("            Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("            Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        SQL.AppendLine("            Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                        SQL.AppendLine("        ) T Group By T.AcNo  ");
                        SQL.AppendLine("    )X1 ");
                        SQL.AppendLine("    LEFT JOIN ( ");
                        SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                        SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                        SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                        SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                        SQL.AppendLine("    	WHERE A.DocNo=@RecvVdDocNo ");
                        SQL.AppendLine("    )X2 ON 1=1 ");
                        //SQL.AppendLine("    GROUP BY X2.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                        SQL.AppendLine("    T.AcNo, ");
                        SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                        SQL.AppendLine("    0.00 As CAmt ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }
                }
            }

            //Credit RecvVd

            if (mIsItemCategoryUseCOAAPAR)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCode As EntCode, E.AcNo8 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode = D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode And E.AcNo8 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo ");
            }
            else
            {
                SQL.AppendLine("    Union All ");
                if (mIsAPfromReceivingUseCOAfromProjectandDeptShortCode)
                {
                    SQL.AppendLine("        SELECT @EntCode As EntCode, T1.AcNo, ");
                    SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblRecvVdHdr A ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        INNER JOIN ( ");
                    SQL.AppendLine("        SELECT A.DocNo, Concat(E.ParValue, D.ProjectCode2) As AcNo");
                    SQL.AppendLine("        From TblDroppingRequestHdr A");
                    SQL.AppendLine("        Inner Join TblProjectImplementationHdr B on A.PRJIDocNo = B.DocNo");
                    SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                    SQL.AppendLine("        Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
                    SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And IfNull(E.ParValue, '')<>''");
                    SQL.AppendLine("        Where A.DocNo = @DroppingRequestDocNo");

                    SQL.AppendLine("        Union All ");


                    SQL.AppendLine("        SELECT A.DocNo, Concat(C.ParValue, B.ShortCode) As AcNo ");
                    SQL.AppendLine("        FROM TblDroppingRequestHdr A");
                    SQL.AppendLine("        INNER JOIN tbldepartment B ON A.DeptCode = B.DeptCode ");
                    SQL.AppendLine("        INNER Join TblParameter C On C.ParCode='VendorAcNoUnInvoiceAP' And IfNull(C.ParValue, '')<>''  ");
                    SQL.AppendLine("        Where A.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("        )T1 ON T1.DocNo = @DroppingRequestDocNo");
                    SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo");
                }
                else
                {
                    SQL.AppendLine("        Select @EntCode As EntCode, Concat(D.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("        From TblRecvVdHdr A ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                    SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo ");
                }

            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo is Not Null ");
            SQL.AppendLine("    Group By EntCode, AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", DroppingRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
            if (mJournalDocNoFormat == "1") //Default
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else if (mJournalDocNoFormat == "2") //PHT
            {
                string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd2", string.Empty, string.Empty, mJournalDocNoFormat);
                string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            if (mRecvvd2CostCenterJournalFormat == "1")
            {
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            }
            else if (mRecvvd2CostCenterJournalFormat == "2")
            {
                if (TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    var CCCodeDRQ = Sm.GetValue("SELECT F.CCCode " +
                   "FROM tbldroppingrequesthdr A " +
                   "INNER JOIN tblprojectimplementationhdr B ON A.PRJIDocNo = B.DocNo " +
                   "INNER JOIN tblsocontractrevisionhdr C ON B.SOContractDocNo = C.DocNo " +
                   "INNER JOIN tblsocontracthdr D ON C.SOCDocNo = D.DocNo " +
                   "INNER JOIN tblboqhdr E ON D.BOQDocNo = E.DocNo " +
                   "INNER JOIN tbllophdr F ON E.LOPDocNo = F.DocNo " +
                   "Where A.DocNo=@Param ", TxtDroppingRequestDocNo.Text.Length > 0 ? TxtDroppingRequestDocNo.Text : null
                   );

                    Sm.CmParam<String>(ref cm, "@CCCode", CCCodeDRQ);
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue("Select CCCode from tblwarehouse where WhsCode=@Param", Sm.GetLue(LueWhsCode)));
                }
            }

            return cm;
        }

        private MySqlCommand UpdateRecvVd2File(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVd2File2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVd2File3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #region Edit Data

        private void CancelData()
        {
            UpdateCancelledRecvVd();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";


            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var JournalDocNo = string.Empty;
            if (mIsRecvVdSaveJournal)
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    if (mDocNoFormat == "1")
                        JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
                    else
                        JournalDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1");
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd2", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    JournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty));
                }
            }

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateRecvVdHdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    if (mIsRecvVdSaveJournal && Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                        cml.Add(UpdateRecvVdDtl(Row, JournalDocNo, true));
                    else
                        cml.Add(UpdateRecvVdDtl(Row, JournalDocNo, false));
                }
            }

            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2") cml.Add(SaveUploadFile(TxtDocNo.Text));

            cml.Add(UpdatePOProcessInd(TxtDocNo.Text));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                {
                    cml.Add(CancelRecvVdDtl(TxtDocNo.Text, Row));
                }
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            if (TxtDR_PRJIDocNo.Text.Length > 0 || TxtDR_BCCode.Text.Length > 0) cml.Add(UpdateDroppingRequestDtl());
            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (mIsRecvVd2AllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 7))
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledRecvVdNotExisted(DNo) ||
                IsCancelledItemInvalid() ||
                IsCancelReasonEmpty() ||
                //IsRecvProcessedPI() ||
                IsDOUsedByAnotherTransaction(DNo) ||
                IsJournalSettingInvalid() ||
            IsDocAlreadyProcessToDODept() || 
            IsDocAlreadyProcessToMCC();
        }

        private bool IsDocAlreadyProcessToDODept()
        {

            //string mDODDocNo = GetPropertyInventoryCostComponentCode();

            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT D.DocNo FROM tblpropertyinventorycostcomponenthdr A ");
            SQL.AppendLine(" inner join tblpropertyinventorycostcomponentdtl B on A.DocNo = B.DocNo ");
            SQL.AppendLine(" LEFT JOIN tbldodepthdr C ON B.DODDocNo = C.DocNo ");
            SQL.AppendLine(" LEFT JOIN tblrecvvdhdr D ON C.RecvVdDocNo = D.DocNo ");
            SQL.AppendLine(" WHERE A.CancelInd = 'N' And D.DocNo = @Param ");
            SQL.AppendLine("  ");
            SQL.AppendLine(" LIMIT 1; ");
            string DODDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);


            return Sm.IsDataExist(
                " SELECT 1 FROM tblrecvvdhdr WHERE DocNo IN( " +
                " SELECT C.DocNo FROM tblpropertyinventorycostcomponentdtl A " +
                " LEFT JOIN tbldodepthdr B ON A.DODDocNo = B.DocNo " +
                " LEFT JOIN tblrecvvdhdr C ON B.RecvVdDocNo = C.DocNo " +
                " WHERE C.DocNo = @Param " +
                " ); ",
                TxtDocNo.Text,
                "The document is already processed in DO to Department :  " + DODDocNo
            );
        }
        
        private bool IsDocAlreadyProcessToMCC()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT D.DocNo FROM tblpropertyinventorycostcomponenthdr A ");
            SQL.AppendLine("Inner Join tblpropertyinventorycostcomponentdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner JOIN tblpropertyinventorycostcomponentdtl4 C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner JOIN tbldodepthdr D ON B.DODDocNo = D.DocNo ");
            SQL.AppendLine("Inner JOIN tblrecvvdhdr E ON D.RecvVdDocNo = E.DocNo ");
            SQL.AppendLine("WHERE A.CancelInd='N' AND E.DocNo = @Param ");
            //SQL.AppendLine("GROUP BY A.DocNo, E.DocNo  ");
            SQL.AppendLine(" LIMIT 1; ");
            string DODDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text);


            return Sm.IsDataExist(
                " SELECT 1 FROM tblrecvvdhdr WHERE DocNo IN( " +
                " SELECT C.DocNo FROM tblpropertyinventorycostcomponentdtl A " +
                " LEFT JOIN tbldodepthdr B ON A.DODDocNo = B.DocNo " +
                " LEFT JOIN tblrecvvdhdr C ON B.RecvVdDocNo = C.DocNo " +
                " WHERE C.DocNo = @Param " +
                " ); ",
                TxtDocNo.Text,
                "The document is already processed in DO to Department :  " + DODDocNo
            );
        }

        private bool IsDOUsedByAnotherTransaction(string DNo)
        {
            if (!mMenuCodeForRecvVd2AutoCreateDO) return false;

            var SQL = new StringBuilder();
            string DODeptDocNo = Sm.GetValue("Select DocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null And RecvVdDocNo = @Param Limit 1; ", TxtDocNo.Text);

            SQL.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin ");
            SQL.AppendLine("From TblRecvDeptDtl A, TblDODeptDtl B, TblItem C ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DODeptDocNo=B.DocNo ");
            SQL.AppendLine("And A.DODeptDNo=B.DNo ");
            SQL.AppendLine("And B.ItCode=C.ItCode ");
            SQL.AppendLine("And A.DODeptDocNo=@DODeptDocNo ");
            SQL.AppendLine("And Position(Concat('##', A.DODeptDNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "BatchNo", "Source", "Lot", "Bin"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                                "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                                "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                                "Batch# : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                                "Source : " + Sm.DrStr(dr, 3) + Environment.NewLine +
                                "Lot : " + Sm.DrStr(dr, 4) + Environment.NewLine +
                                "Bin : " + Sm.DrStr(dr, 5) + Environment.NewLine + Environment.NewLine +
                                "Department already returned this item.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private bool IsRecvProcessedPI()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From TblPurchaseInvoicehdr A ");
            SQL.AppendLine("Inner Join tblPurchaseInvoiceDtl B On A.DocNo = b.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And B.RecvVdDocno=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Received# (" + TxtDocNo.Text + ") has been processed to Purchase Invoice.");
                return true;
            }
            return false;
        }

        //private bool IsCOAJournalCancelNotValid()
        //{
        //    var l = new List<COA>();
        //    var SQL = new StringBuilder();
        //    string Filter = string.Empty;
        //    var cm = new MySqlCommand();
        //    string mItCode = string.Empty;
        //    string cek = string.Empty;

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
        //        {
        //            if (mItCode.Length > 0) mItCode += " Or ";
        //            mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
        //        }
        //    }

        //    if (mMenuCodeForRecvVd2AutoCreateDO)
        //    {
        //        SQL.AppendLine("Select AcNo2 as AcNo ");
        //        SQL.AppendLine("From TblEntity D On D.EntCode=@EntCodeWhs ");
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select D.AcNo as AcNo ");
        //        SQL.AppendLine("From TblDoDeptHdr A ");
        //        SQL.AppendLine("Inner Join TblDoDeptDtl B On A.DocNo = B.DocNo And A.RecvVdDocNo = @DocNo) ");
        //        SQL.AppendLine("Inner Join TblItemCostCategory C On C.ItCode = B.ItCode And A.CCCode = C.CCCode ");
        //        SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCOde And C.CCCode = D.CCCode ");


        //        //Debit RecvVd
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode='VendorAcNoUnInvoiceAP' ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Select B.AcNo From TblItem A");
        //        SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
        //        SQL.AppendLine("Where " + mItCode);
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where ParCode='VendorAcNoUnInvoiceAP' ");
        //    }

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();


        //        Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
        //        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new COA()
        //                {
        //                    AcNo = Sm.DrStr(dr, c[0])

        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    foreach (var x in l.Where(w => w.AcNo.Length <= 0))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //        return true;
        //    }


        //    return false;
        //}

        private bool IsCancelledRecvVdNotExisted(string DNo)
        {
            if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2" && !mIsApprove) return false;
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelRecvVdDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdDtl Set ");
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));

            return cm;
        }

        private void UpdateCancelledRecvVd()
        {
            decimal UPriceOld = 0m, UPriceNew = 0m;
            string Message = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd, UPrice ");
            SQL.AppendLine("From TblRecvVdDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd", "UPrice" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.DrStr(dr, 1) == "Y" && !Sm.GetGrdBool(Grd1, Index, 2))
                                {
                                    UPriceOld = Sm.DrDec(dr, 2);
                                    UPriceNew = Sm.GetGrdDec(Grd1, Index, 11);

                                    if (UPriceOld != UPriceNew)
                                        Message =
                                            "Unit Price (Previous) : " + Sm.FormatNum(UPriceOld, 0) + Environment.NewLine +
                                            "Unit Price (Current) : " + Sm.FormatNum(UPriceNew, 0) + Environment.NewLine + Environment.NewLine;

                                    Message =
                                        "Item Code : " + Sm.GetGrdStr(Grd1, Index, 7) + Environment.NewLine +
                                        "Item Name : " + Sm.GetGrdStr(Grd1, Index, 9) + Environment.NewLine +
                                        "Batch Number : " + Sm.GetGrdStr(Grd1, Index, 14) + Environment.NewLine +
                                        Message +
                                        "Data already cancelled.";

                                    Sm.StdMsg(mMsgType.Warning, Message);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Index, 18, 2);
                                }
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }


        private bool IsCancelledItemInvalid()
        {
            if (!mMenuCodeForRecvVd2AutoCreateDO)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdBool(Grd1, Row, 1) &&
                        !Sm.GetGrdBool(Grd1, Row, 2) &&
                        Sm.GetGrdStr(Grd1, Row, 7).Length > 0 &&
                        IsCancelledItemInvalid(Row))
                        return true;
            }

            return false;
        }

        private bool IsItemFixedWhenInserted(int r)
        {
            if (Sm.IsDataExist("Select 1 From TblStockSummary Where Source=@Param Limit 1;", Sm.GetGrdStr(Grd1, r, 15)))
                return false;
            else
                return true;
        }

        private bool IsCancelledItemInvalid(int r)
        {
            if (IsItemFixedWhenInserted(r)) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source ");
            SQL.AppendLine("And Qty>=@Qty ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 16));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 17));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 19));
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item's Code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, r, 14) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, r, 16) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine + Environment.NewLine +
                    "Not enough stock." + Environment.NewLine +
                    "You can't cancel this item.");
                return true;
            }
            return false;
        }

        private MySqlCommand UpdateRecvVdHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr Set ");
            SQL.AppendLine("    TaxAmt=@TaxAmt, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Exists(Select DocNo From TblRecvVdHdr Where DocNo=@DocNo And (TaxAmt<>@TaxAmt Or Amt<>@Amt)) ");
            SQL.AppendLine("And DocNo In (Select Distinct PODocNo From TblRecvVdDtl Where DocNo=@DocNo); ");

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    TaxAmt=@TaxAmt, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (TaxAmt<>@TaxAmt Or Amt<>@Amt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDroppingRequestDtl()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                {
                    if (Sm.GetGrdBool(Grd1, Index, 1) == true)
                    {
                        if (TxtDR_PRJIDocNo.Text.Length > 0)
                        {
                            var SQLi = new StringBuilder();

                            SQLi.AppendLine("SELECT COUNT(MRDocNo) TotalMR  ");
                            SQLi.AppendLine("FROM(  ");
                            SQLi.AppendLine("	SELECT A.DocNo MRDocNo, I.DocNo AS DroppingReqDocNo, G.ItCode, M.ItName  ");
                            SQLi.AppendLine(" 	FROM tblmaterialrequesthdr A  ");
                            SQLi.AppendLine(" 	INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo  ");
                            SQLi.AppendLine(" 	INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo  ");
                            SQLi.AppendLine(" 	INNER JOIN tbldroppingrequestdtl J ON I.DocNo = J.DocNo  ");
                            SQLi.AppendLine(" 	INNER JOIN TblProjectImplementationRBPHdr K On J.PRBPDocNo = K.DocNo  ");
                            SQLi.AppendLine(" 	INNER JOIN TblProjectImplementationRBPDtl L On K.DocNo = L.DocNo And J.PRBPDNo = L.DNo   ");
                            SQLi.AppendLine(" 	LEFT JOIN tblitem M ON K.ResourceItCode = M.ItCode  ");
                            SQLi.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N'   ");
                            SQLi.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A'   ");
                            SQLi.AppendLine("	AND B.ItCode = K.ResourceItCode ");
                            SQLi.AppendLine("	AND A.DroppingRequestDocNo = @DroppingReqDocNo ");
                            SQLi.AppendLine(")X  ");
                            SQLi.AppendLine("WHERE X.DroppingReqDocNo = @DroppingReqDocNo AND X.ItCode = @ItCode ");

                            var cmi = new MySqlCommand() { CommandText = SQLi.ToString() };
                            Sm.CmParam<String>(ref cmi, "@DroppingReqDocNo", TxtDroppingRequestDocNo.Text);
                            Sm.CmParam<String>(ref cmi, "@ItCode", Sm.GetGrdStr(Grd1, Index, 7));

                            string IsMRWithItemExist = Sm.GetValue(cmi);

                            if (decimal.Parse(IsMRWithItemExist) <= 1)
                            {
                                SQL.AppendLine("UPDATE tbldroppingrequestdtl X1 ");
                                SQL.AppendLine("INNER JOIN tblprojectimplementationrbphdr X2 ON X1.PRBPDocNo = X2.DocNo ");
                                SQL.AppendLine(" SET X1.DRSourceInd = NULL ");
                                SQL.AppendLine("WHERE X1.DocNo = @DrDocNo AND X2.ResourceItCode = @ItCode_" + Index + ";");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 7));
                            }
                            else
                            {
                                SQL.AppendLine("UPDATE tbldroppingrequestdtl X1 ");
                                SQL.AppendLine("INNER JOIN tblprojectimplementationrbphdr X2 ON X1.PRBPDocNo = X2.DocNo ");
                                SQL.AppendLine(" SET X1.DRSourceInd = NULL ");
                                SQL.AppendLine("WHERE X1.DocNo = @DrDocNo AND X2.ResourceItCode = @ItCode_" + Index + " And 1=0 ;");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 7));
                            }
                        }
                        else if (TxtDR_BCCode.Text.Length > 0)
                        {
                            var SQLi = new StringBuilder();

                            SQLi.AppendLine("SELECT COUNT(MRDocNo) TotalMR ");
                            SQLi.AppendLine("FROM( ");
                            SQLi.AppendLine("	SELECT A.DocNo MRDocNo, I.DocNo AS DroppingReqDocNo, G.ItCode, K.ItName ");
                            SQLi.AppendLine("	FROM tblmaterialrequesthdr A  ");
                            SQLi.AppendLine("	INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo  ");
                            SQLi.AppendLine("	LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo  ");
                            SQLi.AppendLine("	INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo  ");
                            SQLi.AppendLine("	INNER JOIN tbldroppingrequestdtl2 J ON I.DocNo = J.DocNo AND A.DroppingRequestBCCode = J.BCCode  ");
                            SQLi.AppendLine("	LEFT JOIN tblitem K ON J.ItCode = K.ItCode  ");
                            SQLi.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N'  ");
                            SQLi.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A'  ");
                            SQLi.AppendLine("	AND B.ItCode = J.ItCode ");
                            SQLi.AppendLine("	AND A.DroppingRequestDocNo = @DroppingReqDocNo   ");
                            SQLi.AppendLine(")X ");
                            SQLi.AppendLine("WHERE X.DroppingReqDocNo = @DroppingReqDocNo AND X.ItCode = @ItCode ");

                            var cmi = new MySqlCommand() { CommandText = SQLi.ToString() };
                            Sm.CmParam<String>(ref cmi, "@DroppingReqDocNo", TxtDroppingRequestDocNo.Text);
                            Sm.CmParam<String>(ref cmi, "@ItCode", Sm.GetGrdStr(Grd1, Index, 7));

                            string IsMRWithItemExist = Sm.GetValue(cmi);

                            if (decimal.Parse(IsMRWithItemExist) <= 1)
                            {
                                SQL.AppendLine("UPDATE TblDroppingRequestDtl2 ");
                                SQL.AppendLine(" SET DRSourceInd = NULL ");
                                SQL.AppendLine("WHERE DocNo = @DrDocNo and ItCode = @ItCode_" + Index + ";");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 7));
                            }
                            else
                            {
                                SQL.AppendLine("UPDATE TblDroppingRequestDtl2 ");
                                SQL.AppendLine(" SET DRSourceInd = NULL ");
                                SQL.AppendLine("WHERE DocNo = @DrDocNo and ItCode = @ItCode_" + Index + " And 1=0 ;");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 7));
                            }
                        }
                    }

                }
            }
            else
            {
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("UPDATE TblDroppingRequestDtl ");
                    SQL.AppendLine(" SET MRDocNo = NULL, MRDNo = NULL ");
                    SQL.AppendLine("WHERE DocNo = @DrDocNo And MrDocNo = @MRDocNo ");
                }
                else if (TxtDR_BCCode.Text.Length > 0)
                {
                    SQL.AppendLine("UPDATE TblDroppingRequestDtl2 ");
                    SQL.AppendLine(" SET MRDocNo = NULL, MRDNo = NULL ");
                    SQL.AppendLine("WHERE DocNo = @DrDocNo And MrDocNo = @MRDocNo ");
                }
            }

            Sm.CmParam<String>(ref cm, "@DRDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MRDocNo", GetMaterialRequestDocNo(TxtDocNo.Text));

            cm.CommandText = SQL.ToString();
            return cm;
        }

        private MySqlCommand UpdateRecvVdDtl(int Row, string JournalDocNo, bool IsCancelled)
        {
            var IsFixedWhenInserted = IsItemFixedWhenInserted(Row);
            var SQL = new StringBuilder();
            string DODeptDocNo = Sm.GetValue("Select DocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null And RecvVdDocNo = @Param Limit 1; ", TxtDocNo.Text);

            SQL.AppendLine("Set @QtDocNoNew := 'XXX'; ");
            SQL.AppendLine("Set @QtDNoNew := '001'; ");

            SQL.AppendLine("Set @QtDocNoNew := Concat( ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("            Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblQtHdr ");
            SQL.AppendLine("            Where Right(DocNo, 5)=Concat(Substring(@DocDt, 5, 2),'/', Substring(@DocDt, 3, 2))  ");
            SQL.AppendLine("            Order By Left(DocNo, 5) Desc Limit 1  ");
            SQL.AppendLine("            ) As Temp  ");
            SQL.AppendLine("    ), '0001'),  ");
            SQL.AppendLine("    '/', IfNull((Select ParValue From TblParameter Where ParCode='DocTitle'), ''), ");
            SQL.AppendLine("    '/', IfNull((Select DocAbbr From TblDocAbbreviation Where DocType='Qt'), ''), ");
            SQL.AppendLine("    '/', Substring(@DocDt, 5, 2),'/', Substring(@DocDt, 3, 2) ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblQtHdr(DocNo, DocDt, VdCode, PtCode, CurCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNoNew, DocDt, VdCode, PtCode, CurCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr Where DocNo=@DocNo ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select UPrice From TblRecvVdDtl Where DocNo=@DocNo And DNo=@DNo And UPrice<>@UPrice ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblQtDtl(DocNo, DNo, ItCode, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @QtDocNoNew, @QtDNoNew, ItCode, @UPrice, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdDtl Where DocNo=@DocNo And DNo=@DNo And UPrice<>@UPrice; ");

            if (!(IsCancelled && IsFixedWhenInserted))
            {
                if (mMenuCodeForRecvVd2AutoCreateDO)
                {
                    SQL.AppendLine("Update TblDODeptDtl Set ");
                    SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where DocNo=@DODeptDocNo ");
                    SQL.AppendLine("And DNo=@DNo ");
                    SQL.AppendLine("; ");

                    SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @DODeptDocType, A.DocNo, B.DNo, 'Y', A.DocDt, ");
                    SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                    SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, @CancelReason, ");
                    SQL.AppendLine("@UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblDODeptHdr A ");
                    SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
                    SQL.AppendLine("Where A.DocNo=@DODeptDocNo And B.CancelInd = 'Y'; ");

                    SQL.AppendLine("Update TblStockSummary A ");
                    SQL.AppendLine("Inner Join TblRecvVdHdr B On A.WhsCode = B.WhsCode And B.DocNo = @DocNo ");
                    SQL.AppendLine("Inner Join TblRecvVdDtl C On B.DocNo = C.DocNo And C.DNo = @DNo ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    A.Qty=A.Qty+C.Qty, A.Qty2=A.Qty2+C.Qty2, A.Qty3=A.Qty3+C.Qty3, ");
                    SQL.AppendLine("    A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where A.WhsCode=B.WhsCode And A.Lot=C.Lot And A.Bin=C.Bin And A.Source=C.Source ");
                    SQL.AppendLine("And Exists (Select 1 From TblDODeptDtl Where DocNo = @DODeptDocNo And DNo = @DNo And CancelInd = 'Y'); ");
                }

                SQL.AppendLine("Update TblStockMovement Set ");
                SQL.AppendLine("    Qty=0, Qty2=0, Qty3=0, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo and DNo=@DNo And DocType='13' And (Qty<>0 Or Qty2<>0 Or Qty3<>0) ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select CancelInd From TblRecvVdDtl ");
                SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo And CancelInd<>@CancelInd And CancelInd='N' ");
                SQL.AppendLine("    Limit 1); ");

                SQL.AppendLine("Update TblStockSummary As T1  ");
                SQL.AppendLine("Inner Join TblRecvVdHdr T2 On T1.WhsCode=T2.WhsCode And T2.DocNo=@DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl T3 ");
                SQL.AppendLine("    On T1.Lot=T3.Lot And T1.Bin=T3.Bin And T1.ItCode=T3.ItCode And T1.BatchNo=T3.BatchNo And T1.Source=T3.Source ");
                SQL.AppendLine("    And T2.DocNo=T3.DocNo And T3.DNo=@DNo And T3.CancelInd<>@CancelInd And T3.CancelInd='N' ");
                SQL.AppendLine("Set T1.Qty=T1.Qty-T3.Qty, T1.Qty2=T1.Qty2-T3.Qty2, T1.Qty3=T1.Qty3-T3.Qty3, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");
            }

            SQL.AppendLine("Update TblStockPrice As T1  ");
            SQL.AppendLine("Inner Join TblRecvVdDtl T2 ");
            SQL.AppendLine("    On T1.ItCode=T2.ItCode And T1.BatchNo=T2.BatchNo And T1.Source=T2.Source ");
            SQL.AppendLine("    And T2.DocNo=@DocNo And T2.DNo=@DNo And T2.UPrice<>@UPrice ");
            SQL.AppendLine("Set T1.UPrice=@UPrice, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Concat(DocNo, DNo, CancelInd) In ( ");
            SQL.AppendLine("    Select Concat(T3.MaterialRequestDocNo, T3.MaterialRequestDNo, T1.CancelInd) ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.DNo=@DNo And T1.CancelInd<>@CancelInd And T1.CancelInd='N' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblPORequestDtl Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Concat(DocNo, DNo, CancelInd) In ( ");
            SQL.AppendLine("    Select Concat(T2.PORequestDocNo, T2.PORequestDNo, T1.CancelInd) ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.DNo=@DNo And T1.CancelInd<>@CancelInd And T1.CancelInd='N' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblPORequestDtl Set ");
            SQL.AppendLine("    QtDocNo=@QtDocNoNew, QtDNo=@QtDNoNew, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Concat(DocNo, DNo, CancelInd) In ( ");
            SQL.AppendLine("    Select Concat(T2.PORequestDocNo, T2.PORequestDNo, T1.CancelInd) ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo And T1.DNo=@DNo And T1.UPrice<>@UPrice ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Concat(DocNo, DNo, CancelInd) In ( ");
            SQL.AppendLine("    Select Concat(PODocNo, DNo, CancelInd) ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo And CancelInd<>@CancelInd And CancelInd='N' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    RoundingValue=@RoundingValue, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("    Where Concat(DocNo, DNo) In ( ");
            SQL.AppendLine("    Select Concat(PODocNo, DNo) ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo And RoundingValue<>@RoundingValue ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblRecvVdDtl Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, UPrice=@UPrice, RoundingValue=@RoundingValue, ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And (CancelInd<>@CancelInd Or UPrice<>@UPrice Or RoundingValue<>@RoundingValue); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@DODeptDocType", "05");
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", Sm.GetGrdBool(Grd1, Row, 1) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", IsCancelled ? JournalDocNo : string.Empty);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
            SQL.AppendLine("    IfNull(C.RecvVdQty, 0)+IfNull(D.POQtyCancelQty, 0) As Qty2  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.DocNo=B.PODocNo And A.DNo=B.PODNo And B.DocNo=@DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T2 On T1.PODocNo=T2.PODocNo And T1.PODNo=T2.PODNo And T2.DocNo=@DocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As POQtyCancelQty ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T2 On T1.PODocNo=T2.PODocNo And T1.PODNo=T2.PODNo And T2.DocNo=@DocNo ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd= ");
            SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
            SQL.AppendLine("    End ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var Filter2 = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);
            string DroppingRequestDocNo = TxtDroppingRequestDocNo.Text;

            int No = 1;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DNo=@DNo" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 0));
                    No += 1;
                }
            }

            if (Filter.Length > 0)
            {
                Filter2 = " And (" + Filter + ")";
            }

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdDtl A Set A.JournalDocNo=@JournalDocNo Where A.DocNo=@DocNo " + Filter2 + " ;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling Receiving Item From Vendor (Without Material Request) : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblRecvVdHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            if (mMenuCodeForRecvVd2AutoCreateDO)
            {
                //Credit DO
                if (mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");

                    SQL.AppendLine("    Union All ");

                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }

                //Debit RecvVd

                if (mIsItemCategoryUseCOAAPAR)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select E.AcNo8 As AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblRecvVdHdr A ");
                    SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine(Filter2.Replace("A.", "B."));
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode = D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode And E.AcNo8 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    if (mIsAPfromReceivingUseCOAfromProjectandDeptShortCode)
                    {
                        SQL.AppendLine("        SELECT T1.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        INNER JOIN ( ");
                        SQL.AppendLine("        SELECT A.DocNo, Concat(E.ParValue, D.ProjectCode2) As AcNo");
                        SQL.AppendLine("        From TblDroppingRequestHdr A");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr B on A.PRJIDocNo = B.DocNo");
                        SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
                        SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And IfNull(E.ParValue, '')<>''");
                        SQL.AppendLine("        Where A.DocNo = @DroppingRequestDocNo");

                        SQL.AppendLine("        Union All ");


                        SQL.AppendLine("        SELECT A.DocNo, Concat(C.ParValue, B.ShortCode) As AcNo ");
                        SQL.AppendLine("        FROM TblDroppingRequestHdr A");
                        SQL.AppendLine("        INNER JOIN tbldepartment B ON A.DeptCode = B.DeptCode ");
                        SQL.AppendLine("        INNER Join TblParameter C On C.ParCode='VendorAcNoUnInvoiceAP' And IfNull(C.ParValue, '')<>''  ");
                        SQL.AppendLine("        Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("        )T1 ON T1.DocNo = @DroppingRequestDocNo");
                        SQL.AppendLine("        Where A.DocNo=@DocNo;");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine(Filter2.Replace("A.", "B."));
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                }
            }
            else
            {
                SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter2);
                SQL.AppendLine("    Union All ");
                if (mRecvVd2CreditAcNoSource == "1")
                {
                    if (mIsAPfromReceivingUseCOAfromProjectandDeptShortCode)
                    {
                        SQL.AppendLine("        SELECT T1.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        INNER JOIN ( ");
                        SQL.AppendLine("        SELECT A.DocNo, Concat(E.ParValue, D.ProjectCode2) As AcNo");
                        SQL.AppendLine("        From TblDroppingRequestHdr A");
                        SQL.AppendLine("        Inner Join TblProjectImplementationHdr B on A.PRJIDocNo = B.DocNo");
                        SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                        SQL.AppendLine("        Inner Join TblSOContractHdr D On C.SOCDocNo = D.DocNo ");
                        SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And IfNull(E.ParValue, '')<>''");
                        SQL.AppendLine("        Where A.DocNo = @DroppingRequestDocNo");

                        SQL.AppendLine("        Union All ");


                        SQL.AppendLine("        SELECT A.DocNo, Concat(C.ParValue, B.ShortCode) As AcNo ");
                        SQL.AppendLine("        FROM TblDroppingRequestHdr A");
                        SQL.AppendLine("        INNER JOIN tbldepartment B ON A.DeptCode = B.DeptCode ");
                        SQL.AppendLine("        INNER Join TblParameter C On C.ParCode='VendorAcNoUnInvoiceAP' And IfNull(C.ParValue, '')<>''  ");
                        SQL.AppendLine("        Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("        )T1 ON T1.DocNo = @DroppingRequestDocNo");
                        SQL.AppendLine("        Where A.DocNo=@DocNo;");
                    }
                    else
                    {
                        SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt  ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo " + Filter2.Replace("A.", "B."));
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    //SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                    //SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt  ");
                    //SQL.AppendLine("        From TblRecvVdHdr A ");
                    //SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo " + Filter2.Replace("A.", "B."));
                    //SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    //SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                    //SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select D.AcNo8 As AcNo, ");
                    SQL.AppendLine("        A.Qty*B.UPrice*B.ExcRate As DAmt, 0 As CAmt  ");
                    SQL.AppendLine("        From TblRecvVdDtl A ");
                    SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                    SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo8 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo " + Filter2);
                }
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", DroppingRequestDocNo);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    if (mDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                    else
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd2", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(CurrentDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }
            else
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    if (mDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                    else
                        Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd2", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }

            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                string MaterialRequestDocNo = GetMaterialRequestDocNo(DocNo);
                ClearData();
                ShowRecvVdHdr(DocNo);
                ShowRecvVdDtl(DocNo);
                if (mIsRecvVd2AllowToUploadFile && mRecv2UploadFileFormat == "2") ShowUploadFile(DocNo);
                ShowDroppingRequestInfo(MaterialRequestDocNo);
                ComputeDR_Balance();
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvVdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.WhsCode, A.VdCode, A.VdDONo, ");
            SQL.AppendLine("A.DeptCode, A.PtCode, A.CurCode, A.TaxCode1, A.TaxCode2, A.TaxCode3, ");
            SQL.AppendLine("A.TaxAmt, A.DiscountAmt, A.Amt, A.Remark, A.SiteCode, ");
            SQL.AppendLine("A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt, A.KBRegistrationNo, ");
            SQL.AppendLine("A.KBRegistrationDt, A.KBPackaging, A.KBPackagingQty, A.KBNonDocInd,  ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode, ");
            if (mMenuCodeForRecvVd2AutoCreateDO)
                SQL.AppendLine("B.CCCode, B.EntCode, ");
            else
                SQL.AppendLine("Null as CCCode, Null As EntCode, ");
            if (mIsRecvVd2ProjectCodeEnabled)
                SQL.AppendLine("A.ProjectCode, ");
            else
                SQL.AppendLine("Null As ProjectCode, ");

            if (mMenuCodeForRecvVd2AutoCreateDO)
                SQL.AppendLine("B.SOContractDocNo, ");
            else
                SQL.AppendLine("Null As SOContractDocNo, ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode, ");
            if (mIsRecvVd2AllowToUploadFile)
                SQL.AppendLine("A.FileName, A.FileName2, A.FileName3 ");
            else
                SQL.AppendLine("Null As FileName, Null As FileName2, Null As FileName3 ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            if (mMenuCodeForRecvVd2AutoCreateDO)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct CCCode, EntCode, RecvVdDocNo, ");
                if (mIsRecvVd2AutoCreateDOUseSOContract)
                    SQL.AppendLine("    SOContractDocNo ");
                else
                    SQL.AppendLine("    Null As SOContractDocNo ");
                SQL.AppendLine("    From TblDODeptHdr ");
                SQL.AppendLine("    Where RecvVdDocNo Is Not Null ");
                SQL.AppendLine("    And RecvVdDocNo = @DocNo ");
                SQL.AppendLine(") B On A.DocNo = B.RecvVdDocNo ");
            }
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.POInd = 'N'; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt",  "LocalDocNo", "WhsCode", "VdCode", "VdDONo", 
                    
                    //6-10
                    "DeptCode", "PtCode", "CurCode", "TaxCode1", "TaxCode2", 
                    
                    //11-15
                    "TaxCode3", "TaxAmt", "DiscountAmt", "Amt", "Remark", 
                    
                    //16-20
                    "SiteCode", "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", 

                    //21-25
                    "KBRegistrationNo", "KBRegistrationDt", "KBPackaging", "KBPackagingQty", "KBNonDocInd", 
                    
                    //26-30
                    "KBSubmissionNo", "CustomsDocCode", "CCCode", "EntCode", "ProjectCode", 

                    //31-34
                    "SOContractDocNo", "FileName", "FileName2", "FileName3"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                    // Sl.SetLueVdCode2(ref LueVdCode, Sm.DrStr(dr, c[4]));
                    SetLueVdCode2(ref LueVdCode, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[4]));
                    TxtVdDONo.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[11]));
                    TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtDiscountAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[16]), "N");
                    TxtKBContractNo.EditValue = Sm.DrStr(dr, c[17]);
                    Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[18]));
                    TxtKBPLNo.EditValue = Sm.DrStr(dr, c[19]);
                    Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[20]));
                    TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[21]);
                    Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[22]));
                    TxtKBPackaging.EditValue = Sm.DrStr(dr, c[23]);
                    TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    ChkKBNonDocInd.Checked = Sm.DrStr(dr, c[25]) == "Y";
                    TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[26]);
                    Sm.SetLue(LueCustomsDocCode, Sm.DrStr(dr, c[27]));
                    if (mMenuCodeForRecvVd2AutoCreateDO)
                    {
                        //Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[28]));
                        if (mIsRecvVd2UseProfitCenter)
                            Sl.SetLueCCCodeFilterByProfitCenter(ref LueCCCode, Sm.DrStr(dr, c[28]), "N");
                        else
                            SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[29]));
                    }
                    if (mIsRecvVd2ProjectCodeEnabled) SetLueProjectCode(ref LueProjectCode, Sm.DrStr(dr, c[30]));
                    if (mIsRecvVd2AutoCreateDOUseSOContract) TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[31]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[32]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[33]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[34]);
                }, true
            );
        }

        private void ShowRecvVdDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, A.ItCode, B.ItName, B.ItCodeInternal, B.Specification, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.UPrice, A.Discount, A.RoundingValue, A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, ");
            SQL.AppendLine("A.Remark, ifnull(B.ItScCode, 'XXX') As ItScCode, ifnull(C.ItScName,'XXX') As ItScName, A.ExpiredDt, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, B.ItGrpCode, B.ForeignName ");
            if (mIsRecvVd2ForDroppingRequestEnabled && TxtDroppingRequestDocNo.Text.Length > 0)
                SQL.AppendLine(", F.VdCode, F.VdName, F.Amt, F.PaymentAmt ");
            else
                SQL.AppendLine(", null As VdCode, null As VdName, 0.00 As Amt, 0.00 PaymentAmt ");

            if (mMenuCodeForRecvVd2AutoCreateDO)
                SQL.AppendLine(", H.CCtCode, H.CCtName, H.LOPDocNo, H.AcNo ");
            else
                SQL.AppendLine(", null as CCtCode, null as CCtName, null As LOPDocNo, null As AcNo ");
            if (mIsRecvVd2HeatNumberEnabled)
                SQL.AppendLine(", A.HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("left join TblItemSubcategory C On B.ItScCode = C.ItScCode ");
            if (mIsRecvVd2ForDroppingRequestEnabled && TxtDroppingRequestDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Inner Join TblPODtl D On A.PODocNo = D.DocNo And A.PODNo = D.DNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo And D.PORequestDNo = E.DNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select X2.MRDocNo, X2.MRDNo, X2.VdCode, X3.VdName, X1.Amt, X4.Amt As PaymentAmt ");
                SQL.AppendLine("    From TblDroppingRequestHdr X1 ");
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("        And X1.Docno = @DroppingRequestDocNo ");
                }

                if (TxtDR_DeptCode.Text.Length > 0)
                {
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("        And X1.Docno = @DroppingRequestDocNo ");
                }
                SQL.AppendLine("    Left Join TblVendor X3 On X2.VdCode = X3.VdCode ");
                SQL.AppendLine("    Left Join TblDroppingPaymentDtl X4 On X1.DocNo = X4.DRQDocNo And X2.DNo = X4.DRQDNo ");
                SQL.AppendLine(") F On E.MaterialRequestDocNo = F.MRDocNo And E.MaterialRequestDNo = F.MRDNo ");
            }
            if (mMenuCodeForRecvVd2AutoCreateDO)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.RecvVdDocNo, T2.ItCode, T3.CCtCode, T4.CCtName, T1.LOPDocNo, T22.AcNo ");
                SQL.AppendLine("    From TblDODeptHdr T1 ");
                SQL.AppendLine("    Inner Join TblDODeptDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.RecvVdDocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblItem T21 On T2.ItCode = T21.ItCode ");
                SQL.AppendLine("    Inner join TblItemCategory T22 On T21.ItCtCode = T22.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemCostCategory T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("        And T2.ItCode = T3.ItCode ");
                SQL.AppendLine("    Left Join TblCostCategory T4 On T3.CCtCode = T4.CCtCode ");
                SQL.AppendLine("        And T3.CCCode = T4.CCCode ");
                SQL.AppendLine(") H On A.ItCode = H.ItCode And A.Docno  = H.RecvVdDocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "ItCode", "ItName", 
                    //6-10
                    "ItGrpCode", "ForeignName",  "ItScCode", "ItScName", "BatchNo",
                    //11-15
                    "Source", "Lot", "Bin", "UPrice", "Qty", 
                    //16-20
                    "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                    //21-25
                    "Discount", "RoundingValue", "Remark", "VdCode", "VdName", 
                    //26-30
                    "Amt", "PaymentAmt", "CCtCode", "CCtName", "LOPDocNo", 
                    //31-35
                    "AcNo", "ExpiredDt", "ItCodeInternal", "Specification", "HeatNumber"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 23);
                    Grd1.Cells[Row, 29].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 25);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 38, 32);
                    if (mMenuCodeForRecvVd2AutoCreateDO)
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 31);
                    }
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 34);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 35);
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 19, 21, 23, 25, 26, 27, 29, 30, 33 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDroppingRequestInfo(string MaterialRequestDocNo)
        {
            if (!mIsRecvVd2ForDroppingRequestEnabled) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string DRBCCode = string.Empty;

            SQL.AppendLine("Select B.DocNo, C.DeptName, B.Yr, B.Mth, B.PRJIDocNo, D.BCName, B.Remark, ");
            SQL.AppendLine("Case When A.DroppingRequestBCCode Is Not Null Then ");
            SQL.AppendLine("(Select Sum(Amt*Qty) From TblDroppingRequestDtl2 Where DocNo=A.DroppingRequestDocNo And BCCode=A.DroppingRequestBCCode) ");
            SQL.AppendLine("Else B.Amt End As Amt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr B On A.DroppingRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblBudgetCategory D On A.DroppingRequestBCCode=D.BCCode ");
            SQL.AppendLine("Where A.DocNo=@MaterialRequestDocNo;");

            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    "DeptName",
                    "Yr", "Mth", "PRJIDocNo", "BCName", "Remark",
                    "Amt", "DocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDR_DeptCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtDR_Yr.EditValue = Sm.DrStr(dr, c[1]);
                    TxtDR_Mth.EditValue = Sm.DrStr(dr, c[2]);
                    TxtDR_PRJIDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    TxtDR_BCCode.EditValue = Sm.DrStr(dr, c[4]);
                    MeeDR_Remark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtDroppingRequestDocNo.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );

            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                string tempDt = Sm.GetValue("Select CreateDt From TblRecvvdhdr Where DocNo = @Param", TxtDocNo.Text);
                string BCCode = TxtDR_BCCode.Text.Length > 0 ? Sm.GetValue("Select BCCode From TblBudgetCategory Where BCName = @Param", TxtDR_BCCode.Text) : string.Empty;

                var pSQL = new StringBuilder();

                pSQL.AppendLine("SELECT SUM(G.UPrice*G.Qty) TotalMRAmt ");
                pSQL.AppendLine("FROM tblmaterialrequesthdr A ");
                pSQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                pSQL.AppendLine("LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                pSQL.AppendLine("LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                pSQL.AppendLine("LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequestdtl J ON I.DocNo = J.DocNo ");
                pSQL.AppendLine("inner Join TblProjectImplementationRBPHdr K On J.PRBPDocNo = K.DocNo ");
                pSQL.AppendLine("Inner Join TblProjectImplementationRBPDtl L On K.DocNo = L.DocNo And J.PRBPDNo = L.DNo  ");
                pSQL.AppendLine("LEFT JOIN tblitem M ON K.ResourceItCode = M.ItCode ");
                pSQL.AppendLine("WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                pSQL.AppendLine("AND B.CancelInd = 'N' AND B.Status = 'A' ");
                pSQL.AppendLine("AND B.ItCode = K.ResourceItCode  ");
                pSQL.AppendLine("and A.DroppingRequestDocNo = @Param1 ");
                pSQL.AppendLine("AND H.CreateDt < @Param2 ");
                pSQL.AppendLine("GROUP BY I.DocNo ");
                pSQL.AppendLine("UNION ALL ");
                pSQL.AppendLine("SELECT SUM(G.UPrice*G.Qty) TotalMRAmt ");
                pSQL.AppendLine("FROM tblmaterialrequesthdr A ");
                pSQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                pSQL.AppendLine("LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                pSQL.AppendLine("LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                pSQL.AppendLine("LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                pSQL.AppendLine("LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequestdtl2 J ON I.DocNo = J.DocNo AND A.DroppingRequestBCCode = J.BCCode And J.BCCode = @Param3 ");
                pSQL.AppendLine("LEFT JOIN tblitem K ON J.ItCode = K.ItCode ");
                pSQL.AppendLine("WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                pSQL.AppendLine("AND B.CancelInd = 'N' AND B.Status = 'A' ");
                pSQL.AppendLine("AND B.ItCode = J.ItCode   ");
                pSQL.AppendLine("AND A.DroppingRequestDocNo = @Param1 ");
                pSQL.AppendLine("AND H.CreateDt < @Param2 ");
                pSQL.AppendLine("GROUP BY I.DocNo ");

                decimal TotalMRAmt = Sm.GetValueDec(pSQL.ToString(), TxtDroppingRequestDocNo.Text, tempDt, BCCode, string.Empty);
                decimal DroppingReqAmt = Sm.GetDecValue(TxtDR_DroppingRequestAmt.Text) - TotalMRAmt;
                TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(DroppingReqAmt, 0);
            }
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                    "select DNo, FileName, CreateBy, CreateDt from  TblRecvVdFile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd3, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 7, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Additional Method 

        //UPLOAD FILE - START
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Receiving Item From Vendor (Without PO) - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblRecvVdFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFileGrd(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            //if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
                ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsRecvVd2AllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblRecvVdFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteEmployeeFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblRecvVdFile Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtDocNo.Text);

            return cm;
        }


        //UPLOAD FILE - END

        internal void SetLueVdCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.VdCode As Col1 ");
            if (Sm.GetParameterBoo("IsVendorComboShowCategory"))
                SQL.AppendLine(", CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2 ");
            else
                SQL.AppendLine(", T.VdName As Col2 ");
            SQL.AppendLine("From TblVendor T ");
            SQL.AppendLine("LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode ");
            //SQL.AppendLine("inner JOIN tblgroupvendorcategory T3 ON T.VdCtCode = T3.VdCtCode");

            if (Code.Length > 0)
            {
                SQL.AppendLine("Where T.VdCode=@Code ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (mIsFilterByVendorCategory)
                {
                    SQL.AppendLine("And Exists(");
                    SQL.AppendLine("Select 1 From tblgroupvendorcategory");
                    SQL.AppendLine("Where VdCtCode = T.VdCtCode");
                    SQL.AppendLine("And GrpCode In(Select GrpCode From TblUser Where UserCode = @UserCode)");
                    SQL.AppendLine(");");
                }
            }



            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void UploadFile(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvVd2File(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvVd2File2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvVd2File3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {
            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsRecvVd2AllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsRecvVd2AllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsRecvVd2AllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private void SetCostCategory()
        {
            var SQL = new StringBuilder();
            string ItCode = string.Empty, CCtCode = string.Empty, CCtName = string.Empty;
            var cm = new MySqlCommand();

            Grd1.BeginUpdate();
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Grd1.Cells[r, 34].Value = string.Empty;
                Grd1.Cells[r, 35].Value = string.Empty;
            }
            Grd1.EndUpdate();

            SQL.AppendLine("Select A.ItCode, A.CCtCode, B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A, TblCostCategory B ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine("And A.CCtCode=B.CCtCode ");
            SQL.AppendLine("And B.AcNo Is Not Null ");
            SQL.AppendLine("And (1=1 ");

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 7);
                    if (ItCode.Length > 0)
                    {
                        SQL.AppendLine(" Or (A.ItCode=@ItCode" + r.ToString() + ") ");
                        Sm.CmParam<String>(ref cm, "@ItCode" + r.ToString(), ItCode);
                    }
                }
                SQL.AppendLine(") Order By A.ItCode;");
            }
            else
                SQL.AppendLine(" And 1=0); ");

            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCtCode", "CCtName" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        CCtCode = Sm.DrStr(dr, 1);
                        CCtName = Sm.DrStr(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count; r++)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 7).Length > 0 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 7), ItCode))
                            {
                                Grd1.Cells[r, 34].Value = CCtCode;
                                Grd1.Cells[r, 35].Value = CCtName;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsPORequestApprovalForAllDept()
        {
            return Sm.GetParameter("PORequestApprovalForAllDept") == "Y";
        }

        internal void GenerateBatchNo(int Row)
        {
            if (mIsRecvVdUseOptionBatchNoFormula &&
                mIsAutoGenerateBatchNo &&
                mIsAutoGenerateBatchNoEditable)
            {
                var SQL = new StringBuilder();
                string mMaterialCategoryCode = string.Empty, mMaterialCode = string.Empty,
                    mBatchMaterialCode = string.Empty, mSequenceNo = string.Empty,
                    mBatchNo = string.Empty;
                string mItCode = Sm.GetGrdStr(Grd1, Row, 7);

                mMaterialCode = Sm.GetValue("Select ItGrpCode From TblItem Where ItCode = @Param Limit 1; ", mItCode);
                if (DteDocDt.Text.Length > 0) mBatchMaterialCode = GetDteFormat(DteDocDt.DateTime);

                SQL.AppendLine("Select B.OptDesc ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblOption B On A.ItCtCode = B.OptCode ");
                SQL.AppendLine("    And B.OptCat = 'RecvVdBatchNoFormula' ");
                SQL.AppendLine("    And A.ItCode = @Param ");
                SQL.AppendLine("Limit 1; ");

                mMaterialCategoryCode = Sm.GetValue(SQL.ToString(), mItCode);
                mSequenceNo = GetBatchNoSequenceNo(string.Concat(mMaterialCategoryCode, mMaterialCode, mBatchMaterialCode));

                mBatchNo = string.Concat(mMaterialCategoryCode, mMaterialCode, "-", mBatchMaterialCode, mSequenceNo, "-", LueVdCode.Text);

                Grd1.Cells[Row, 14].Value = mBatchNo;
            }
        }

        private string GenerateBatchNo2(int Row, string DocNo, string DNo)
        {
            string mBatchNo = string.Empty;
            string mShortName = Sm.GetValue("Select ShortName From TblVendor Where VdCode = @Param ", Sm.GetLue(LueVdCode));
            string mDocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (mIsAutoGenerateBatchNo)
                mBatchNo = string.Concat(mShortName, "-", mDocDt, "-", DocNo);
            else
                mBatchNo = mIsBatchNoUseDocDtIfEmpty ? mDocDt : "-";

            return mBatchNo;
        }

        private void ProcessDODtl(ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 7).Length > 0)
                {
                    lDODtl.Add(new DODept2Dtl()
                    {
                        DocNo = "1",
                        DNo = "1",
                        ItCode = Sm.GetGrdStr(Grd1, i, 7),
                        PropCode = "-",
                        BatchNo = GenerateBatchNo2(i, DocNo, Sm.Right(string.Concat("000", (i + 1)), 3)),
                        Source = string.Concat(mDocType, "*", DocNo, "*", Sm.Right(string.Concat("000", (i + 1)), 3)),
                        Lot = Sm.GetGrdStr(Grd1, i, 16).Length > 0 ? Sm.GetGrdStr(Grd1, i, 16) : "-",
                        Bin = Sm.GetGrdStr(Grd1, i, 17).Length > 0 ? Sm.GetGrdStr(Grd1, i, 17) : "-",
                        Qty = Sm.GetGrdDec(Grd1, i, 19),
                        Qty2 = Sm.GetGrdDec(Grd1, i, 21),
                        Qty3 = Sm.GetGrdDec(Grd1, i, 23),
                        LOPDocNo = Sm.GetGrdStr(Grd1, i, 36),
                        Remark = string.Concat(Sm.GetGrdStr(Grd1, i, 28), " [Auto Create From RecvVd #", DocNo)
                    });
                }
            }
        }

        private void ProcessDODtl2(ref List<DODept2Dtl> lDODtl)
        {
            int mDocNoIndex = 0;
            int mDNoIndex = 0;
            string mLOPDocNo = string.Empty, mDocNo = string.Empty, mDNo = string.Empty;

            for (int i = 0; i < lDODtl.Count; ++i)
            {
                if (i == 0)
                {
                    mLOPDocNo = lDODtl[i].LOPDocNo;
                    mDocNoIndex = 1;
                }

                if (mLOPDocNo == lDODtl[i].LOPDocNo)
                {
                    mDNoIndex += 1;
                }
                else
                {
                    mDocNoIndex += 1;
                    mDNoIndex = 1;
                    mLOPDocNo = lDODtl[i].LOPDocNo;
                }

                if (mLOPDocNo.Length > 0) mDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODeptLOP", "TblDODeptHdr", mDocNoIndex.ToString());
                else mDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr", mDocNoIndex.ToString());
                mDNo = Sm.Right(string.Concat("000", mDNoIndex.ToString()), 3);

                lDODtl[i].DocNo = mDocNo;
                lDODtl[i].DNo = mDNo;
            }
        }

        private void UpdateDODtl(ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            for (int i = 0; i < lDODtl.Count; ++i)
            {
                lDODtl[i].DocNo = DocNo;
                lDODtl[i].DNo = Sm.Right(string.Concat("000", (i + 1)), 3);
            }
        }

        private void ProcessDOHdr(ref List<DODept2Hdr> lDOHdr, string DODeptDocNo, string DocNo)
        {
            lDOHdr.Add(new DODept2Hdr()
            {
                DocNo = DODeptDocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                LocalDocNo = TxtLocalDocNo.Text,
                SOContractDocNo = TxtSOContractDocNo.Text,
                DeptCode = Sm.GetLue(LueDeptCode),
                EntCode = Sm.GetLue(LueEntCode),
                CCCode = Sm.GetLue(LueCCCode),
                LOPDocNo = string.Empty,
                RecvVdDocNo = DocNo,
                Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
            });
        }

        private void ProcessDOHdr2(ref List<DODept2Hdr> lDOHdr, ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < lDODtl.Count; ++i)
            {
                if (i == 0)
                {
                    mDocNo = lDODtl[i].DocNo;

                    lDOHdr.Add(new DODept2Hdr()
                    {
                        DocNo = lDODtl[i].DocNo,
                        DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                        WhsCode = Sm.GetLue(LueWhsCode),
                        LocalDocNo = TxtLocalDocNo.Text,
                        SOContractDocNo = TxtSOContractDocNo.Text,
                        DeptCode = Sm.GetLue(LueDeptCode),
                        EntCode = Sm.GetLue(LueEntCode),
                        CCCode = Sm.GetLue(LueCCCode),
                        LOPDocNo = lDODtl[i].LOPDocNo,
                        RecvVdDocNo = DocNo,
                        Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
                    });
                }
                else
                {
                    if (mDocNo != lDODtl[i].DocNo)
                    {
                        mDocNo = lDODtl[i].DocNo;

                        lDOHdr.Add(new DODept2Hdr()
                        {
                            DocNo = lDODtl[i].DocNo,
                            DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                            WhsCode = Sm.GetLue(LueWhsCode),
                            LocalDocNo = TxtLocalDocNo.Text,
                            SOContractDocNo = TxtSOContractDocNo.Text,
                            DeptCode = Sm.GetLue(LueDeptCode),
                            EntCode = Sm.GetLue(LueEntCode),
                            CCCode = Sm.GetLue(LueCCCode),
                            LOPDocNo = lDODtl[i].LOPDocNo,
                            RecvVdDocNo = DocNo,
                            Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
                        });
                    }
                }
            }
        }

        internal void ProcessConvertUom(int Row)
        {
            if (Sm.CompareStr(
                    Sm.GetGrdStr(Grd1, Row, 20),
                    Sm.GetGrdStr(Grd1, Row, 22))
                    )
                Sm.CopyGrdValue(Grd1, Row, 21, Grd1, Row, 19);
            else
            {
                Grd1.Cells[Row, 21].Value =
                    Sm.GetGrdDec(Grd1, Row, 19) *
                    ConvertUom(
                        Sm.GetGrdStr(Grd1, Row, 20),
                        Sm.GetGrdStr(Grd1, Row, 22));
            }

            if (Sm.CompareStr(
                    Sm.GetGrdStr(Grd1, Row, 20),
                    Sm.GetGrdStr(Grd1, Row, 24))
                    )
                Sm.CopyGrdValue(Grd1, Row, 23, Grd1, Row, 19);
            else
            {
                Grd1.Cells[Row, 23].Value =
                    Sm.GetGrdDec(Grd1, Row, 19) *
                    ConvertUom(
                        Sm.GetGrdStr(Grd1, Row, 20),
                        Sm.GetGrdStr(Grd1, Row, 24));
            }

            ComputeTotal(Row);
        }

        internal void SetDroppingReqProjImplItQty()
        {
            if (TxtDR_PRJIDocNo.Text.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select T2.ResourceItCode, T1.Qty, IfNull(T3.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T2 On T1.PRBPDocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 T3 On T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
            SQL.AppendLine("Where T1.DocNo=@DroppingRequestDocNo And T1.MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ResourceItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) && Sm.GetGrdDec(Grd1, r, 31) == 0m)
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        internal void SetDroppingReqBCItQty()
        {
            if (mDroppingRequestBCCode.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select ItCode, Qty, Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl2 ");
            SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
            SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
            SQL.AppendLine("And MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) &&
                                Sm.GetGrdDec(Grd1, r, 31) == 0m
                                )
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        private string GetMaterialRequestDocNo(string RecvVdDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct C.MaterialRequestDocNo ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODNo = B.DNo ");
            SQL.AppendLine("    And A.DocNo = @Param ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), RecvVdDocNo);
        }

        private void ComputeDR_Balance()
        {
            if (!mIsRecvVd2ForDroppingRequestEnabled) return;
            decimal
                DR_DroppingRequestAmt = decimal.Parse(TxtDR_DroppingRequestAmt.Text),
                //DR_OtherMRAmt = decimal.Parse(TxtDR_OtherMRAmt.Text),
                DR_MRAmt = 0m,
                Qty = 0m,
                UPrice = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty = 0m;
                UPrice = 0m;

                if (Sm.GetGrdStr(Grd1, r, 19).Length != 0) Qty = Sm.GetGrdDec(Grd1, r, 19);
                if (Sm.GetGrdStr(Grd1, r, 18).Length != 0) UPrice = Sm.GetGrdDec(Grd1, r, 18);

                DR_MRAmt += (Qty * UPrice);
            }
            TxtDR_MRAmt.Text = Sm.FormatNum(DR_MRAmt, 0);
            //TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_OtherMRAmt - DR_MRAmt, 0);
            TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_MRAmt, 0);
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));
            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }
        private void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter ");
            if (Code.Length == 0)
                SQL.AppendLine("Where ActInd='Y' ");
            else
                SQL.AppendLine("Where CCCode=@Code ");
            SQL.AppendLine("Order By CCName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void ShowApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='RecvVd2' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                            new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ProcFormatDocNo', 'IsItGrpCodeShow', 'DocTitle', 'IsItemCategoryUseCOAAPAR', 'IsFilterByCC', ");
            SQL.AppendLine("'PortForFTPClient', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'RecvVd2CreditAcNoSource', 'IsRecvVd2UseProfitCenter', 'IsRecvVd2JournalUseCCCode', 'IsRecvVd2AllowToUploadFile', ");
            SQL.AppendLine("'IsRecvVd2ProjectCodeEnabled', 'IsRecvVd2ShowSpecificationEnabled', 'IsRecvVd2AutoCreateDOUseSOContract', 'IsRecvVd2AutoCreateDOSOContractBasedOnProject', 'IsCheckCOAJournalNotExists', ");
            SQL.AppendLine("'IsSiteMandatory', 'IsMovingAvgEnabled', 'IsRecvVdUseOptionBatchNoFormula', 'IsAutoGenerateBatchNoEditable', 'IsFilterByItCt', ");
            SQL.AppendLine("'IsRecvVd2NeedApproval', 'IsRecvVd2ApprovalBasedOnDept', 'IsBatchNoUseDocDtIfEmpty', 'IsAutoGenerateBatchNo', 'IsFilterBySite', 'IsFilterByDept', 'IsRecvVd2ForDroppingRequestEnabled', ");
            SQL.AppendLine("'IsShowForeignName', 'IsAutoJournalActived', 'IsRecvVdSaveJournal', 'IsRecvVd2RemarkForApprovalMandatory', 'IsRecvVdApprovalBasedOnWhs', ");
            SQL.AppendLine("'MenuCodeForRecvVd2AutoCreateDO', 'KB_Server', 'RecvVdCustomsDocCodeManualInput', 'RecvVd2PtCodeDefault', 'DocNoFormat', ");
            SQL.AppendLine("'NumberOfInventoryUomCode', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsFilterByVendorCategory', 'IsJournalRecvVdAutoDOUseCOAEntity', 'WhsRecvVd2ForAsset',  ");
            SQL.AppendLine("'IsDroppingRequestUseMRReceivingPartial', 'IsRecvVdWithoutPOAutoDOUploadFileMandatory', 'Recv2UploadFileFormat', 'JournalDocNoFormat', ");
            SQL.AppendLine("'IsAPfromReceivingUseCOAfromProjectandDeptShortCode', 'IsPOShowFlaggingInformation', 'IsDroppingRequestUseType', 'IsRecvvd2UploadfileMandatory', 'Recvvd2CostCenterJournalFormat', 'Recvvd2JournalFormat' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "MenuCodeForRecvVd2AutoCreateDO":
                                if (ParValue.Length > 0) mMenuCodeForRecvVd2AutoCreateDO = ParValue == mMenuCode;
                                break;
                            case "KB_Server": mIsKawasanBerikatEnabled = ParValue.Length > 0; break;
                            case "RecvVdCustomsDocCodeManualInput": mRecvVdCustomsDocCodeManualInput = ParValue; break;
                            case "RecvVd2PtCodeDefault": mRecvVd2PtCodeDefault = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "RecvVd2CreditAcNoSource": mRecvVd2CreditAcNoSource = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "WhsRecvVd2ForAsset": mWhsRecvVd2ForAsset = ParValue; break;
                            case "Recv2UploadFileFormat": mRecv2UploadFileFormat = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                            case "Recvvd2CostCenterJournalFormat": mRecvvd2CostCenterJournalFormat = ParValue; break;
                            case "Recvvd2JournalFormat": mRecvvd2JournalFormat = ParValue; break;

                            //boolean
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "N"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsRecvVdSaveJournal": mIsRecvVdSaveJournal = ParValue == "Y"; break;
                            case "IsRecvVd2RemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsRecvVdApprovalBasedOnWhs": mIsRecvVdApprovalBasedOnWhs = ParValue == "Y"; break;
                            case "IsRecvVd2NeedApproval": mIsRecvVd2NeedApproval = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsAutoGenerateBatchNo": mIsAutoGenerateBatchNo = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsRecvVd2ForDroppingRequestEnabled": mIsRecvVd2ForDroppingRequestEnabled = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsRecvVd2AutoCreateDOSOContractBasedOnProject": mIsRecvVd2AutoCreateDOSOContractBasedOnProject = ParValue == "Y"; break;
                            case "IsRecvVd2AutoCreateDOUseSOContract": mIsRecvVd2AutoCreateDOUseSOContract = ParValue == "Y"; break;
                            case "IsRecvVd2ShowSpecificationEnabled": mIsRecvVd2ShowSpecificationEnabled = ParValue == "Y"; break;
                            case "IsRecvVd2ProjectCodeEnabled": mIsRecvVd2ProjectCodeEnabled = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsAutoGenerateBatchNoEditable": mIsAutoGenerateBatchNoEditable = ParValue == "Y"; break;
                            case "IsRecvVdUseOptionBatchNoFormula": mIsRecvVdUseOptionBatchNoFormula = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsRecvVd2AllowToUploadFile": mIsRecvVd2AllowToUploadFile = ParValue == "Y"; break;
                            case "IsRecvVd2JournalUseCCCode": mIsRecvVd2JournalUseCCCode = ParValue == "Y"; break;
                            case "IsRecvVd2UseProfitCenter": mIsRecvVd2UseProfitCenter = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsFilterByVendorCategory": mIsFilterByVendorCategory = ParValue == "Y"; break;
                            case "IsJournalRecvVdAutoDOUseCOAEntity": mIsJournalRecvVdAutoDOUseCOAEntity = ParValue == "Y"; break;
                            case "IsDroppingRequestUseMRReceivingPartial": mIsDroppingRequestUseMRReceivingPartial = ParValue == "Y"; break;
                            case "IsRecvVdWithoutPOAutoDOUploadFileMandatory": mIsRecvVdWithoutPOAutoDOUploadFileMandatory = ParValue == "Y"; break;
                            case "IsAPfromReceivingUseCOAfromProjectandDeptShortCode": mIsAPfromReceivingUseCOAfromProjectandDeptShortCode = ParValue == "Y"; break;
                            case "IsPOShowFlaggingInformation": mIsPOShowFlaggingInformation = ParValue == "Y"; break;
                            case "IsDroppingRequestUseType": mIsDroppingRequestUseType = ParValue == "Y"; break;
                            case "IsRecvvd2UploadfileMandatory": mIsRecvvd2UploadfileMandatory = ParValue == "Y"; break;
                            case "IsRecvVd2ApprovalBasedOnDept": mIsRecvVd2ApprovalBasedOnDept = ParValue == "Y"; break;

                            //integer 
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length > 0)
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
            if (mRecvVd2CreditAcNoSource.Length == 0) mRecvVd2CreditAcNoSource = "1";
            if (!mMenuCodeForRecvVd2AutoCreateDO) mIsRecvVd2HeatNumberEnabled = Sm.GetParameterBoo("IsRecvVd2HeatNumberEnabled");
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
            if (mRecvvd2JournalFormat.Length == 0) mRecvvd2JournalFormat = "1";
        }

        private bool IsItemEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        private void ParPrint(int parValue)
        {

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvVd2>();
            var ldtl = new List<RecvVd2Dtl>();
            var ldtl2 = new List<RecvVd2Dtl2>();
            var ldtl3 = new List<RecvVd2Dtl3>();
            var ldtl4 = new List<RecvVd2Dtl4>();
            var lSignMnet = new List<SignMnet>();

            string[] TableName = { "RecvVd2", "RecvVd2Dtl", "RecvVd2Dtl2", "RecvVd2Dtl3", "RecvVd2Dtl4", "SignMnet" };
            List<IList> myLists = new List<IList>();

            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, @CompanyFooterImage As CompanyFooterImage, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            // reporttitle 3 harusnya Addresscity
            if (Sm.GetParameter("Doctitle") == "MNET")
            {
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            }
            else
            {
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("Null As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("Null As 'CompanyFax', ");
            }
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, D.VdName, C.DeptName, E.PtName, A.CurCode, Concat(H.ProjectCode, ' : ', H.ProjectName) as ProjectName, ");
            SQL.AppendLine("IfNull(A.TaxCode1, '') As TaxCode1, ");
            SQL.AppendLine("IfNull(A.TaxCode2, '') As TaxCode2, IfNull(A.TaxCode3, '') As TaxCode3, ");
            SQL.AppendLine("A.TaxAmt, A.DiscountAmt, A.Amt, A.Remark, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL.AppendLine("(Select  Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
            SQL.AppendLine("(Select  Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblRecvVdHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
            SQL.AppendLine("F.SiteName, I.ItemName,  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone2', D.Address, G.CityName As CityVd, A.VdDONo, E.PtDay, A.LocalDocNo ");
            SQL.AppendLine("From TblRecvVdHdr A");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Inner Join TblVendor D On A.VdCode = D.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm E On A.PtCode = E.PtCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Left Join TblCity G On D.CityCode= G.CityCode ");
            SQL.AppendLine("Left Join TblProjectGroup H On A.ProjectCode=H.ProjectCode ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("    SELECT X1.DocNo, GROUP_CONCAT(X2.ItName, '\n' SEPARATOR '\n') ItemName ");
            SQL.AppendLine("    FROM tblRecvVdDtl X1 ");
            SQL.AppendLine("    LEFT JOIN TblItem X2 ON X1.ItCode = X2.ItCode ");
            SQL.AppendLine("    Where X1.DocNo=@DocNo ");
            SQL.AppendLine(")I ON A.DocNo = I.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And POInd='N' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@CompanyFooterImage", "FooterImage.png");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                            //0
                                "CompanyLogo",

                                //1-5
                                "CompanyName",
                                "CompanyAddress",
                                "CompanyPhone",
                                "DocNo",
                                "DocDt",
                                //6-10
                                "WhsName",
                                "VdName",
                                "DeptName",
                                "PtName",
                                "CurCode",
                                //11-15
                                "TaxCode1",
                                "TaxCode2",
                                "TaxCode3",
                                "TaxAmt",
                                "DiscountAmt",
                                //16-20
                                "Amt",
                                "Remark",
                                "TaxName1",
                                "TaxName2",
                                "taxName3",
                                //21-25
                                "TaxRate1",
                                "TaxRate2",
                                "taxRate3",
                                "SiteName",
                                "CompanyPhone2",
                                //26-30
                                "Address",
                                "CityVd",
                                "VdDONo",
                                "PtDay",
                                "ProjectName",

                                //31-32
                                "ItemName",
                                "CompanyFooterImage",
                                "LocalDocNo",
                                "CompanyAddressCity"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvVd2()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            VdName = Sm.DrStr(dr, c[7]),
                            DeptName = Sm.DrStr(dr, c[8]),
                            PtName = Sm.DrStr(dr, c[9]),
                            CurCode = Sm.DrStr(dr, c[10]),

                            TaxCode1 = Sm.DrStr(dr, c[11]),
                            TaxCode2 = Sm.DrStr(dr, c[12]),
                            TaxCode3 = Sm.DrStr(dr, c[13]),
                            TaxAmt = Sm.DrDec(dr, c[14]),
                            DiscountAmt = Sm.DrDec(dr, c[15]),

                            Amt = Sm.DrDec(dr, c[16]),
                            HRemark = Sm.DrStr(dr, c[17]),
                            TaxName1 = Sm.DrStr(dr, c[18]),
                            TaxName2 = Sm.DrStr(dr, c[19]),
                            TaxName3 = Sm.DrStr(dr, c[20]),

                            TaxRate1 = Sm.DrDec(dr, c[21]),
                            TaxRate2 = Sm.DrDec(dr, c[22]),
                            TaxRate3 = Sm.DrDec(dr, c[23]),
                            SiteName = Sm.DrStr(dr, c[24]),
                            CompanyPhone2 = Sm.DrStr(dr, c[25]),

                            AddressVd = Sm.DrStr(dr, c[26]),
                            CityVd = Sm.DrStr(dr, c[27]),
                            VdDONo = Sm.DrStr(dr, c[28]),
                            PtDay = Sm.DrDec(dr, c[29]),
                            ProjectName = Sm.DrStr(dr, c[30]),

                            ItemNameHdr = Sm.DrStr(dr, c[31]),
                            CompanyFooterImage = Sm.DrStr(dr, c[32]),
                            LocalDocNo = Sm.DrStr(dr, c[33]),
                            CompanyAddressCity = Sm.DrStr(dr, c[34]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
                SQLDtl.AppendLine("A.UPrice, A.Discount, A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, B.ItCodeInternal, B.Specification, ");
                SQLDtl.AppendLine("(A.Uprice * A.Qty) - (A.Uprice*A.Qty*A.Discount/100) As Cost, A.Remark, B.ItGrpCode ");
                SQLDtl.AppendLine("From TblRecvVdDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Where DocNo=@DocNo And A.CancelInd = 'N' ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                                //0
                                "ItCode" ,

                                //1-5
                                "ItName" ,
                                "BatchNo",
                                "Source",
                                "Lot",
                                "Bin",

                                //6-10
                                "UPrice" ,
                                "Discount",
                                "Qty" ,
                                "InventoryUomCode",
                                "Qty2",

                                //11-15
                                "InventoryUomCode2",
                                "Qty3",
                                "InventoryUomCode3",
                                "Cost",
                                "Remark",

                                //16-18
                                "ItGrpCode",
                                "ItCodeInternal",
                                "Specification",
                        });
                int nomor = 0;
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new RecvVd2Dtl()
                        {
                            Nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            Discount = Sm.DrDec(drDtl, cDtl[7]),
                            Qty = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[10]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[11]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[12]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[13]),
                            Cost = Sm.DrDec(drDtl, cDtl[14]),
                            Remark = Sm.DrStr(drDtl, cDtl[15]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[16]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[17]),
                            Specification = Sm.DrStr(drDtl, cDtl[18]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') As EmpPict, D.GrpName ");
                SQLDtl2.AppendLine("From TblRecvVdHdr A ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("LEFT JOIN tblgroup D ON B.GrpCode = D.GrpCode ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                        {
                                //0-2
                                "UserCode" ,
                                "UserName",
                                "EmpPict",
                                "GrpName"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new RecvVd2Dtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            Position = Sm.DrStr(drDtl2, cDtl2[3]),
                            Space = "                       ",
                            LabelName = "Name : ",
                            LabelPos = "Position : ",
                            LabelDt = "Date : "

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail3
            var cmDtl3 = new MySqlCommand();
            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;
                SQLDtl3.AppendLine("Select T.DocNo, Sum(T.Cost)As Cost from ( ");
                SQLDtl3.AppendLine("Select A.DocNo, A.UPrice, A.Qty, B.InventoryUomCode, A.Qty2, (A.Uprice * A.Qty) - (A.Uprice*A.Qty*A.Discount/100) As Cost ");
                SQLDtl3.AppendLine("From TblRecvVdDtl A ");
                SQLDtl3.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl3.AppendLine("Where DocNo=@DocNo And A.CancelInd = 'N' ");
                SQLDtl3.AppendLine(")T");
                SQLDtl3.AppendLine("Group by T.DocNo");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                        {
                                //0
                            "DocNo" ,
                            "Cost"

                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new RecvVd2Dtl3()
                        {
                            DocNo = Sm.DrStr(drDtl3, cDtl3[0]),
                            Cost = Sm.DrDec(drDtl3, cDtl3[1]),
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region Detail Signature [kim]

            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl4.AppendLine("From ( ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Acknowledge By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblRecvVdDtl A ");
                SQLDtl4.AppendLine("    Inner Join TblDocApproval B On B.DocType='RecvVd' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQLDtl4.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl4.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'RecvVd' ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine("    Union All ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    '00' As DNo, 0 As Level, 'Received By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblRecvVdDtl A ");
                SQLDtl4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine(") T1 ");
                SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl4.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl4.AppendLine("Order By T1.DNo desc; ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[]
                        {
                                //0
                                "Signature" ,

                                //1-5
                                "Username" ,
                                "PosName",
                                "Space",
                                "Level",
                                "Title",
                                "LastupDt"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {

                        ldtl4.Add(new RecvVd2Dtl4()
                        {
                            Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                            UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                            PosName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Space = Sm.DrStr(drDtl4, cDtl4[3]),
                            DNo = Sm.DrStr(drDtl4, cDtl4[4]),
                            Title = Sm.DrStr(drDtl4, cDtl4[5]),
                            LastUpDt = Sm.DrStr(drDtl4, cDtl4[6])
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Detail Signature MNET
            if (Sm.GetParameter("Doctitle") == "MNET")
            {
                var cmSignMnet = new MySqlCommand();

                var SQLS3 = new StringBuilder();
                using (var cnSign3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnSign3.Open();
                    cmSignMnet.Connection = cnSign3;
                    SQLS3.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d %M %Y') as LastUpDt, A.EmpPict  ");
                    SQLS3.AppendLine("From (  ");
                    SQLS3.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    From TblRecvVdHdr A  ");
                    SQLS3.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 6  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");
                    SQLS3.AppendLine("    ) A  ");
                    SQLS3.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode  ");
                    SQLS3.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode  ");
                    SQLS3.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt  ");
                    SQLS3.AppendLine("Order By A.Seq Desc ");

                    cmSignMnet.CommandText = SQLS3.ToString();
                    Sm.CmParam<String>(ref cmSignMnet, "@DocNo", TxtDocNo.Text);
                    var drSign3 = cmSignMnet.ExecuteReader();


                    var cSign3 = Sm.GetOrdinal(drSign3, new string[]
                            {
                     //0-4
                     "UserName",
                     "EmpPict",
                     "Posname",
                     "LastUpDt",
                     "Seq",
                     "Title",
                            });
                    if (drSign3.HasRows)
                    {
                        while (drSign3.Read())
                        {
                            lSignMnet.Add(new SignMnet()
                            {
                                UserName = Sm.DrStr(drSign3, cSign3[0]),
                                EmpPict = Sm.DrStr(drSign3, cSign3[1]),
                                PosName = Sm.DrStr(drSign3, cSign3[2]),
                                LastUpDt = Sm.DrStr(drSign3, cSign3[3]),
                                Sequence = Sm.DrStr(drSign3, cSign3[4]),
                                Title = Sm.DrStr(drSign3, cSign3[5]),
                                Space = "                       ",
                                LabelName = "Name : ",
                                LabelPos = "Position : ",
                                LabelDt = "Date : ",
                            });
                        }
                    }

                    drSign3.Close();
                }
                myLists.Add(lSignMnet);
            }

            #endregion

            switch (parValue)
            {
                case 1:
                    if (Sm.GetParameter("Doctitle") == "MNET")
                        Sm.PrintReport("RecvVd2MNET", myLists, TableName, false);
                    else
                        Sm.PrintReport("RecvVd2_1", myLists, TableName, false);
                    break;
                case 2:
                    if (Sm.GetParameter("Doctitle") == "KIM")
                        Sm.PrintReport("RecvVd2_2KIM", myLists, TableName, false);
                    else if (Sm.GetParameter("Doctitle") == "IMS")
                    {
                        if (mMenuCodeForRecvVd2AutoCreateDO)
                            Sm.PrintReport("RecvVd2_2IMS2", myLists, TableName, false);
                        else
                            Sm.PrintReport("RecvVd2_2IMS", myLists, TableName, false);
                    }
                    else if (Sm.GetParameter("Doctitle") == "BBT")
                    {
                        if (mMenuCodeForRecvVd2AutoCreateDO && mWhsRecvVd2ForAsset.Length > 0)
                            Sm.PrintReport("RecvVd2_2BBT", myLists, TableName, false);
                        else
                            Sm.PrintReport("RecvVd2_2BBT2", myLists, TableName, false);

                    }
                    else
                        Sm.PrintReport("RecvVd2_2", myLists, TableName, false);
                    break;
                case 3:
                    Sm.PrintReport("RecvVd2_3", myLists, TableName, false);
                    break;
            }
        }

        private decimal ConvertUom(string UomCode1, string UomCode2)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select IfNull(Qty, 0) From TblUomConvertion Where UomCode=@UomCode And UomCode2=@UomCode2; "
            };
            Sm.CmParam<String>(ref cm, "@UomCode", UomCode1);
            Sm.CmParam<String>(ref cm, "@UomCode2", UomCode2);

            string Convert = Sm.GetValue(cm);

            if (Convert.Length == 0)
                return 0m;
            else
                return decimal.Parse(Convert);
        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 19;
            int col2 = 0;
            while (col <= 23)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0) Total += Sm.GetGrdDec(Grd1, row, col);

                if (col == 19) col2 = 0;
                if (col == 21) col2 = 1;
                if (col == 23) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 2;
            }
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m, Discount = 0m, RoundingValue = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 19).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 19);
            if (Sm.GetGrdStr(Grd1, Row, 18).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 18);
            if (Sm.GetGrdStr(Grd1, Row, 25).Length != 0) Discount = Sm.GetGrdDec(Grd1, Row, 25);
            if (Sm.GetGrdStr(Grd1, Row, 26).Length != 0) RoundingValue = Sm.GetGrdDec(Grd1, Row, 26);

            Grd1.Cells[Row, 27].Value = (((100 - Discount) / 100) * (Qty * UPrice)) + RoundingValue;

            ComputeAmt();
        }

        private decimal ComputeTotalBeforeTax()
        {
            decimal TotalBeforeTax = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                if (!Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 27).Length != 0) TotalBeforeTax += Sm.GetGrdDec(Grd1, Row, 27);
            return TotalBeforeTax;
        }


        internal void ComputeAmt()
        {
            decimal
                TaxAmt = 0m, DiscountAmt = 0m,
                TotalBeforeTax = ComputeTotalBeforeTax();

            string TaxAmt1 = "0", TaxAmt2 = "0", TaxAmt3 = "0";

            if (Sm.GetLue(LueTaxCode1).Length != 0)
            {
                TaxAmt1 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode1) + "'");
                if (TaxAmt1.Length != 0)
                {
                    TaxAmt += (TotalBeforeTax * (decimal.Parse(TaxAmt1) / 100m));
                }
            }

            if (Sm.GetLue(LueTaxCode2).Length != 0)
            {
                TaxAmt2 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode2) + "'");
                if (TaxAmt2.Length != 0)
                {
                    TaxAmt += (TotalBeforeTax * (decimal.Parse(TaxAmt2) / 100m));
                }
            }

            if (Sm.GetLue(LueTaxCode3).Length != 0)
            {
                TaxAmt3 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode3) + "'");
                if (TaxAmt3.Length != 0)
                {
                    TaxAmt += (TotalBeforeTax * (decimal.Parse(TaxAmt3) / 100m));
                }
            }

            TxtTaxAmt.Text = Sm.FormatNum(TaxAmt, 0);
            if (TxtDiscountAmt.Text.Length != 0) DiscountAmt = decimal.Parse(TxtDiscountAmt.Text);

            TxtAmt.Text = Sm.FormatNum(TotalBeforeTax + TaxAmt - DiscountAmt, 0);
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblRecvVddtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private bool IsCustomsDocCodeInputManual()
        {
            if (mRecvVdCustomsDocCodeManualInput.Length > 0)
            {
                var CustomsDocCode = Sm.GetLue(LueCustomsDocCode);
                if (CustomsDocCode.Length == 0) return false;
                var l = new List<string>(mRecvVdCustomsDocCodeManualInput.Split(',').Select(s => s));
                foreach (var s in l)
                    if (Sm.CompareStr(CustomsDocCode, s)) return true;
            }
            return false;
        }

        private string GetDteFormat(DateTime Value)
        {
            return
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                Sm.Right(Value.Year.ToString(), 2);
        }

        private string GetBatchNoSequenceNo(string BatchNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RIGHT(CONCAT('000', (T.BatchNo + 1)), 3) ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select IfNull(Max(Right(BatchNo, 3)), '000') As BatchNo ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where BatchNo Is Not Null ");
            SQL.AppendLine("    And Length(BatchNo) > 2 ");
            SQL.AppendLine("    And BatchNo Like @Param ");
            SQL.AppendLine(") T; ");

            return Sm.GetValue(SQL.ToString(), string.Concat(BatchNo, "%"));
        }

        private void SetLueProjectCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct ProjectCode As Col1, Concat(ProjectCode, ' : ', ProjectName) As Col2 ");
            SQL.AppendLine("From TblProjectGroup ");
            if (Code.Length == 0)
                SQL.AppendLine("Where ActInd='Y' ");
            else
                SQL.AppendLine("Where ProjectCode=@Code ");
            SQL.AppendLine("Order By Concat(ProjectCode, ' : ', ProjectName);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Grid Events

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            // 18, 19, 21, 23, 25 (price + qty + discount) should not be less than 0
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 18, 19, 21, 23, 25 }, e);

            //discount should not be more than 100
            if (e.ColIndex == 25)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, e.ColIndex) > 100)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be more than 100.");
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                }
            }
        }

        #endregion

        #region Button Events

        private void BtnDroppingRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmRecvVd2Dlg3(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode)));
        }

        private void BtnDroppingRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDroppingRequestDocNo, "Dropping request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDroppingRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsRecvVd2AutoCreateDOUseSOContract)
            {
                Sm.FormShowDialog(new FrmRecvVd2Dlg4(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (mIsRecvVd2AutoCreateDOUseSOContract)
            {
                if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
                {
                    var f = new FrmSOContract2(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOContractDocNo.Text;
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Misc Control Event

        private void DteExpiredDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteExpiredDt, ref fCell, ref fAccept);
        }
        private void DteExpiredDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }
        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (mIsAutoGenerateBatchNo &&
                    mIsAutoGenerateBatchNoEditable &&
                    mIsRecvVdUseOptionBatchNoFormula)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        GenerateBatchNo(i);
                    }
                }
            }
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueVdCode, "Vendor") && !Sm.IsLueEmpty(LueCustomsDocCode, "Customs document code"))
                Sm.FormShowDialog(new FrmRecvVd2Dlg2(this, Sm.GetLue(LueCustomsDocCode)));
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVdDONo);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);

            //Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode2), string.Empty); //Sl.SetLueVdCode2
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
        {
            TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, DteKBRegistrationDt
        });
            }
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtDiscountAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDiscountAmt, 0);
                ComputeAmt();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 17)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 17].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 17].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 16)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 16].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 16].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }

        private void LueCustomsDocCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCustomsDocCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomsDocCode");
                if (IsCustomsDocCodeInputManual())
                {
                    BtnKBContractNo.Enabled = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
            {
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo,
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
            }, false);
                }
                else
                {
                    BtnKBContractNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
            {
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo,
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
            }, true);
                }

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
        {
            TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo,
            DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging
        });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty }, 0);
                ChkKBNonDocInd.Checked = false;
                ClearGrd();
            }
        }

        private void TxtKBContractNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBContractNo);
        }

        private void TxtKBPLNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBPLNo);
        }

        private void TxtKBRegistrationNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBRegistrationNo);
        }

        private void TxtKBSubmissionNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBSubmissionNo);
        }

        private void TxtKBPackaging_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBPackaging);
        }

        private void TxtKBPackagingQty_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtKBPackagingQty, 0);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
                if (mIsRecvVd2UseProfitCenter)
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCodeFilterByProfitCenter), string.Empty, "N");
                else
                    Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                if (Grd1.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Grd1.Cells[r, 34].Value = null;
                        Grd1.Cells[r, 35].Value = null;
                    }
                }
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
        }

        private void LueProjectCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProjectCode, new Sm.RefreshLue2(SetLueProjectCode), string.Empty);
                //TASK/IMS/2020/41/001 by wed
                if (mIsRecvVd2AutoCreateDOSOContractBasedOnProject)
                {
                    TxtSOContractDocNo.EditValue = null;
                }
            }
        }

        #endregion

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile.Checked) TxtFile.EditValue = string.Empty;
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile2.Checked) TxtFile2.EditValue = string.Empty;
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile3.Checked) TxtFile3.EditValue = string.Empty;
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = Path.GetExtension(SFD.FileName);
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = Path.GetExtension(SFD.FileName);
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = Path.GetExtension(SFD.FileName);
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Class

        #region Auto DO

        private class DODept2Hdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string LocalDocNo { get; set; }
            public string SOContractDocNo { get; set; }
            public string DeptCode { get; set; }
            public string EntCode { get; set; }
            public string CCCode { get; set; }
            public string LOPDocNo { get; set; }
            public string RecvVdDocNo { get; set; }
            public string Remark { get; set; }
        }

        private class DODept2Dtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string LOPDocNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
        }

        #endregion

        private class DroppingRequest
        {
            public string DocNo { get; set; }
            public string ItCode { get; set; }
            public decimal MaxTotal { get; set; }
        }

        #endregion
    }

#region Reporting Class

class RecvVd2
{
public string CompanyLogo { set; get; }
public string CompanyFooterImage { set; get; }
public string CompanyName { get; set; }
public string CompanyAddress { get; set; }
public string CompanyAddressCity { get; set; }
public string CompanyPhone { get; set; }
public string DocNo { get; set; }
public string DocDt { get; set; }

public string WhsName { get; set; }
public string VdName { get; set; }
public string DeptName { get; set; }
public string PtName { get; set; }
public string CurCode { get; set; }


public decimal TaxAmt { get; set; }
public decimal DiscountAmt { get; set; }

public decimal Amt { get; set; }
public string HRemark { get; set; }
public string PrintBy { get; set; }

public string TaxCode1 { get; set; }
public string TaxCode2 { get; set; }
public string TaxCode3 { get; set; }
public string TaxName1 { get; set; }
public string TaxName2 { get; set; }
public string TaxName3 { get; set; }
public decimal TaxRate1 { get; set; }
public decimal TaxRate2 { get; set; }
public decimal TaxRate3 { get; set; }
public string SiteName { get; set; }
public string CompanyPhone2 { get; set; }
public string AddressVd { get; set; }
public string CityVd { get; set; }
public string VdDONo { get; set; }
public decimal PtDay { get; set; }
public string ProjectName { get; set; }
public string ItemNameHdr { get; set; }

public string LocalDocNo { get; set; }
}

class RecvVd2Dtl
{
public int Nomor { get; set; }
public string ItCode { get; set; }

public string ItName { get; set; }
public string BatchNo { get; set; }
public string Source { get; set; }
public string Lot { get; set; }
public string Bin { get; set; }

public decimal UPrice { get; set; }
public decimal Discount { get; set; }
public decimal Qty { get; set; }
public decimal Qty2 { get; set; }
public decimal Qty3 { get; set; }

public string InventoryUomCode { get; set; }
public string InventoryUomCode2 { get; set; }
public string InventoryUomCode3 { get; set; }
public decimal Cost { get; set; }
public string Remark { get; set; }
public string ItGrpCode { get; set; }

public string ItCodeInternal { get; set; }
public string Specification { get; set; }


}

class RecvVd2Dtl3
{
public string DocNo { get; set; }
public decimal Cost { get; set; }
}

class RecvVd2Dtl4
{
public string Signature { get; set; }
public string UserName { get; set; }
public string PosName { get; set; }
public string Space { get; set; }
public string DNo { get; set; }
public string Title { get; set; }
public string LastUpDt { get; set; }
}

class RecvVd2Dtl2
{
public string UserCode { get; set; }
public string UserName { get; set; }
public string EmpPict { get; set; }

public string Position { get; set; }
public string Space { get; set; }
public string LabelName { get; set; }
public string LabelPos { get; set; }
public string LabelDt { get; set; }
}

class SignMnet
    {
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string EmpPict { get; set; }
        public string LastUpDt { get; set; }
        public string PosName { get; set; }
        public string Position { get; set; }
        public string Date { get; set; }
        public string Sequence { get; set; }
        public string Title { get; set; }
        public string Space { get; set; }
        public string LabelName { get; set; }
        public string LabelPos { get; set; }
        public string LabelDt { get; set; }

    }

#endregion
}