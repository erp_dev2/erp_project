﻿#region Update
/*
    16/09/2018 [WED] tambahan tabel TblPLPDtl
    24/09/2018 [WED] tambah Penalty di TblPLPDtl, detail dibuka (1 dokumen bisa banyak detail)
    24/09/2018 [WED] AmtMain, AmtRate bisa diedit selama tidak melebihi nilai awalnya
    24/09/2018 [WED] perhitungan LoanSummary dirubah, berdasarkan parameter LoanPaymentCalculationType
    22/10/2018 [WED] print data yang sama dua kali
    25/10/2018 [WED] perbaikan nominal di print out
    08/01/2018 [DITA] menambahkan parameter pada loan payment validasi nilai initial amount
    07/10/2019 [WED/YK] nomor voucher di generate berdasarkan parameter VoucherCodeFormatType
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPLP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mPnCode = string.Empty;
        internal FrmPLPFind FrmFind;
        private string 
            mSurveyDeptCode = string.Empty,
            mPaymentType = string.Empty,
            mBankAcCode = string.Empty,
            mBankCode = string.Empty,
            mVoucherDocType = "53",
            mVRAcType = "D",
            mGiroNo = string.Empty,
            mDueDt = string.Empty,
            mDebitTo = string.Empty,
            mLoanPaymentCalculationType = string.Empty,
            mVoucherCodeFormatType = string.Empty;
        private bool mIsRateAmtCanExceedInitialRate = false;

        #endregion

        #region Constructor

        public FrmPLP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueMonth(ref LueMonth);
                Sl.SetLueYr(LueYr, string.Empty);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",
                    //1-5
                    "Month", "Year", "Amount", "Interest"+Environment.NewLine+"Amount", "Total"+Environment.NewLine+"Amount",
                    //6-9
                    "SurveyDNo", "InitAmtMain", "InitAmtRate", "Penalty"
                },
                new int[] 
                {
                    //0
                    20, 
                    //1-5
                    150, 60, 120, 120, 120,
                    //6-9
                    150, 0, 0, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 7, 8, 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 5, 6, 7, 8 });
        }

        override protected void HideInfoInGrd()
        {
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, MeeRemark, MeeCancelReason
                    }, true);
                    BtnPnName.Enabled = false;
                    //Grd1.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    BtnPnName.Enabled = true;
                    //Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, MeeCancelReason
                    }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPnCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueMonth, LueYr, MeeRemark, TxtVoucherRequestDocNo,
                MeeCancelReason, TxtPnName, TxtSurveyDocNo, TxtVoucherDocNo, LueMonth, LueYr
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt, TxtInterestRateAmt, TxtInterestRate, TxtTotalAmt, TxtOutstandingAmt
            }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 7, 8, 9 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPLPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblPLPHdr Where DocNo = @Param And Status = 'A' And CancelInd = 'N'; ");

                if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
                {
                    ParPrint();
                }
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 4 }, e);

            ComputeAmtRow(e.RowIndex);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;

                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtPnName, "Partner", false) && !Sm.IsTxtEmpty(TxtSurveyDocNo, "Survey#", false))
                {
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmPLPDlg(this, mPnCode, TxtSurveyDocNo.Text));
                }                
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);            
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtPnName, "Partner", false) && !Sm.IsTxtEmpty(TxtSurveyDocNo, "Survey#", false))
                Sm.FormShowDialog(new FrmPLPDlg(this, mPnCode, TxtSurveyDocNo.Text));
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PLP", "TblPLPHdr");
            string VoucherRequestDocNo = string.Empty;
            if (mVoucherCodeFormatType == "2") 
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else 
                VoucherRequestDocNo = Sm.GenerateDocNoVoucher(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", mVRAcType, mBankAcCode);
            decimal mAmt = 0m;

            var cml = new List<MySqlCommand>();
            var l = new List<LoanSummary>();
            var l2 = new List<Payment>();

            cml.Add(SavePLPHdr(DocNo, VoucherRequestDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SavePLPDtl(DocNo, Row));
                    if (!IsDocNeedApproval()) cml.Add(UpdatePaymentLoanSummary(Row));
                    mAmt += Sm.GetGrdDec(Grd1, Row, 5);
                }
            }

            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo, mAmt));

            if (!IsDocNeedApproval()) cml.Add(UpdateRequestLP());

            Sm.ExecCommands(cml);

            if (!IsDocNeedApproval() && mLoanPaymentCalculationType == "2")
            {
                PrepSurveyData(ref l);
                PrepPLPData(ref l2);
                ProcessPaymentAmt(ref l, ref l2);
                RecalculateLoanSummary(ref l);
                InsertNewLoanSummary(ref l);
            }

            l.Clear();
            l2.Clear();

            ShowData(DocNo);
        }

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPnName, "Partner", false) ||
                Sm.IsTxtEmpty(TxtSurveyDocNo, "Survey#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsPaymentZero();
        }

        private bool IsPaymentZero()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, i, 3) == 0 && Sm.GetGrdDec(Grd1, i, 4) == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Amount and Interest rate should not be zero.");
                        Sm.FocusGrd(Grd1, i, 3);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool mFlag = false;
            var mMsg = new StringBuilder();
            int mCols = 0, mRows = 0;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 3) > Sm.GetGrdDec(Grd1, Row, 7))
                    {
                        mMsg.AppendLine("Amount            : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 3), 0));
                        mMsg.AppendLine("Initial Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 7), 0));
                        mMsg.AppendLine(" ");
                        mMsg.AppendLine("Amount could not be bigger than Initial Amount.");

                        mFlag = true; mCols = 3; mRows = Row;
                        break;
                    }
                    if (!mIsRateAmtCanExceedInitialRate)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 4) > Sm.GetGrdDec(Grd1, Row, 8))
                        {
                            mMsg.AppendLine("Rate            : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 4), 0));
                            mMsg.AppendLine("Initial Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 8), 0));
                            mMsg.AppendLine(" ");
                            mMsg.AppendLine("Interest Amount could not be bigger than Initial Interest Amount.");

                            mFlag = true; mCols = 4; mRows = Row;
                            break;
                        }
                    }
                }
            }

            if (mFlag)
            {
                Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                Sm.FocusGrd(Grd1, mRows, mCols);
            }

            return mFlag;
        }

        private bool IsDocNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'PLP' Limit 1; ");
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to save at least 1 record.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePLPHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPLPHdr(DocNo, DocDt, Status, CancelInd, SurveyDocNo, VoucherRequestDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @SurveyDocNo, @VoucherRequestDocNo, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='PLP'; ");

            SQL.AppendLine("Update TblPLPHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PLP' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);

            return cm;
        }

        private MySqlCommand SavePLPDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPLPDtl(DocNo, DNo, SurveyDocNo, SurveyDNo, AmtMain, AmtRate, Amt, Penalty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @SurveyDocNo, @SurveyDNo, @AmtMain, @AmtRate, @Amt, @Penalty, @CreateBy, CurrentDateTime()); ");

            //if (!IsDocNeedApproval())
            //{
            //    SQL.AppendLine("Update TblSurveyDtl ");
            //    SQL.AppendLine("Set PaymentInd = 'F', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            //    SQL.AppendLine("Where DocNo = @SurveyDocNo And DNo = @SurveyDNo And PaymentInd = 'O'; ");

            //    SQL.AppendLine("Update TblRequestLP A ");
            //    SQL.AppendLine("Set A.ProcessInd = 'F', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            //    SQL.AppendLine("Where A.DocNo = (Select T1.RQLPDocNo From TblSurvey T1 Where T1.DocNo = @SurveyDocNo) ");
            //    SQL.AppendLine("And Not Exists (Select 1 From TblSurveyDtl Where DocNo = @SurveyDocNo And PaymentInd = 'O'); ");
            //}

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@AmtMain", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@AmtRate", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@SurveyDNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePaymentLoanSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLoanSummary Set ");
            SQL.AppendLine("    PrevPaymentAmtMain = PrevPaymentAmtMain + PaymentAmtMain ");
            SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo ");
            SQL.AppendLine("And PaymentAmtMain > 0; ");

            SQL.AppendLine("Update TblLoanSummary Set ");
            SQL.AppendLine("    PrevPaymentAmtRate = PrevPaymentAmtRate + PaymentAmtRate ");
            SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo ");
            SQL.AppendLine("And PaymentAmtRate > 0; ");

            SQL.AppendLine("Update TblLoanSummary ");
            SQL.AppendLine("    Set PaymentAmtMain = PaymentAmtMain + @PaymentAmtMain, PaymentAmtRate = PaymentAmtRate + @PaymentAmtRate, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo; ");

            SQL.AppendLine("Update TblLoanSummary Set ");
            SQL.AppendLine("    OutstandingPaymentAmtMain = (AmtMain - PaymentAmtMain), OutstandingPaymentAmtRate = (AmtRate - PaymentAmtRate) ");
            SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo; ");

            if (mIsRateAmtCanExceedInitialRate)
            {
                SQL.AppendLine("Update TblLoanSummary Set ");
                SQL.AppendLine("  OutstandingPaymentAmtRate = 0 ");
                SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo And OutstandingPaymentAmtRate < 0; ");

                SQL.AppendLine("Update TblLoanSummary ");
                SQL.AppendLine("    Set PaymentInd = ");
                SQL.AppendLine("    Case When (AmtMain = PaymentAmtMain And OutstandingPaymentAmtRate = 0) Then 'F' ");
                SQL.AppendLine("    Else 'P' End ");
                SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo; ");
            }
            else
            {
                SQL.AppendLine("Update TblLoanSummary ");
                SQL.AppendLine("    Set PaymentInd = ");
                SQL.AppendLine("    Case When (AmtMain = PaymentAmtMain And AmtRate = PaymentAmtRate) Then 'F' ");
                SQL.AppendLine("    Else 'P' End ");
                SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo And SurveyDNo = @SurveyDNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SurveyDNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@PaymentAmtMain", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@PaymentAmtRate", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateRequestLP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRequestLP A ");
            SQL.AppendLine("Set A.ProcessInd = 'F', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo = (Select T1.RQLPDocNo From TblSurvey T1 Where T1.DocNo = @SurveyDocNo) ");
            SQL.AppendLine("And Not Exists (Select 1 From TblLoanSummary Where SurveyDocNo = @SurveyDocNo And PaymentInd = 'O'); ");

            SQL.AppendLine("Update TblRequestLP A ");
            SQL.AppendLine("Set A.ProcessInd = 'O', A.LastUpBy = @CreateBy, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo = (Select T1.RQLPDocNo From TblSurvey T1 Where T1.DocNo = @SurveyDocNo) ");
            SQL.AppendLine("And Exists (Select 1 From TblLoanSummary Where SurveyDocNo = @SurveyDocNo And PaymentInd In ('O', 'P')); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest(string DocNo, string PLPDocNo, decimal Amt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, LocalDocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, OpeningDt, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
            SQL.AppendLine("Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, null, @DocDt, 'N', 'O', 'N', @DeptCode, @DocType, null, ");
            SQL.AppendLine("@AcType, @BankAcCode, null, null, @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, null, @DueDt, @PIC, @CurCode, @Amt, null, 1, null,");
            SQL.AppendLine("@Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, '001', @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PLP' ");
            SQL.AppendLine("    And DocNo In (Select T.DocNo From TblPLPHdr T Where T.VoucherRequestDocNo = @DocNo) ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", mSurveyDeptCode);
            Sm.CmParam<String>(ref cm, "@DocType", mVoucherDocType);
            Sm.CmParam<String>(ref cm, "@AcType", mVRAcType);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCode);
            Sm.CmParam<String>(ref cm, "@PaymentType", mPaymentType);
            Sm.CmParam<String>(ref cm, "@GiroNo", mGiroNo);
            Sm.CmParam<String>(ref cm, "@DueDt", mDueDt);
            Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
            Sm.CmParam<String>(ref cm, "@PIC", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurCode", "IDR");
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Description", string.Concat("Partner's Loan Payment #", PLPDocNo));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;
            
            var l = new List<LoanSummary>();
            var l2 = new List<Payment>();

            EditPLPHdr();

            UpdateLoanSummary1(ref l, ref l2);
            if (mLoanPaymentCalculationType == "2") RecalculateLoanSummary(ref l);

            InsertNewLoanSummary(ref l);

            l.Clear();
            l2.Clear();

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToVoucher() ||
                IsThisNotTheLastData();
        }

        private bool IsThisNotTheLastData()
        {
            string mDocNo = string.Empty;
            mDocNo = Sm.GetValue("Select DocNo From TblPLPHdr Where SurveyDocNo = @Param And Status In ('O', 'A') And CancelInd = 'N' Order By DocNo Desc Limit 1; ", TxtSurveyDocNo.Text);

            if (mDocNo != TxtDocNo.Text)
            {
                Sm.StdMsg(mMsgType.Warning, "You should cancel this document first : " + mDocNo);
                TxtDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            if (Sm.IsDataExist("Select DocNo From TblVoucherHdr Where VoucherRequestDocNo = @Param And CancelInd = 'N' Limit 1; ", TxtVoucherRequestDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher.");
                return true;
            }

            return false;
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblPLPHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private void EditPLPHdr()
        {
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Update TblPLPHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd = 'N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VRDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            //SQL.AppendLine("Update TblLoanSummary ");
            //SQL.AppendLine("Set PaymentInd = 'O', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo = (Select A.SurveyDocNo From TblPLPDtl A Where A.DocNo = @DocNo) ");
            //SQL.AppendLine("And DNo = (Select A.SurveyDNo From TblPLPDtl A Where A.DocNo = @DocNo) ");
            //SQL.AppendLine("And PaymentInd = 'F' ");
            //SQL.AppendLine("And Exists (Select T.DocNo From TblVoucherRequestHdr T Where T.DocNo = @VRDocNo And (T.CancelInd = 'Y' Or T.Status = 'C')); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VRDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cml.Add(cm);

            Sm.ExecCommands(cml);
        }

        private void UpdateLoanSummary1(ref List<LoanSummary> l, ref List<Payment> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select * ");
            SQL.AppendLine("From TblLoanSummary ");
            SQL.AppendLine("Where SurveyDocNo = @SurveyDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "SurveyDocNo",

                    //1-5
                    "SurveyDNo",
                    "Mth",
                    "Yr",
                    "AmtMain",
                    "AmtRate",

                    //6-10
                    "Amt",
                    "OutstandingLoanAmt",
                    "PaymentInd",
                    "PaymentAmtMain",
                    "PaymentAmtRate",

                    //11-14
                    "OutstandingPaymentAmtMain",
                    "OutstandingPaymentAmtRate",
                    "PrevPaymentAmtMain",
                    "PrevPaymentAmtRate",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LoanSummary()
                        {
                            SurveyDocNo = Sm.DrStr(dr, c[0]),

                            SurveyDNo = Sm.DrStr(dr, c[1]),
                            Mth = Sm.DrStr(dr, c[2]),
                            Yr = Sm.DrStr(dr, c[3]),
                            AmtMain = Sm.DrDec(dr, c[4]),
                            AmtRate = Sm.DrDec(dr, c[5]),

                            Amt = Sm.DrDec(dr, c[6]),
                            OutstandingLoanAmt = Sm.DrDec(dr, c[7]),
                            PaymentInd = Sm.DrStr(dr, c[8]),
                            PaymentAmtMain = Sm.DrDec(dr, c[9]),
                            PaymentAmtRate = Sm.DrDec(dr, c[10]),

                            OutstandingPaymentAmtMain = Sm.DrDec(dr, c[11]),
                            OutstandingPaymentAmtRate = Sm.DrDec(dr, c[12]),
                            PrevPaymentAmtMain = Sm.DrDec(dr, c[13]),
                            PrevPaymentAmtRate = Sm.DrDec(dr, c[14]),
                        });
                    }
                }
                dr.Close();
            }

            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        l2.Add(new Payment()
                        {
                            SurveyDocNo = TxtSurveyDocNo.Text,
                            SurveyDNo = Sm.GetGrdStr(Grd1, Row, 6),
                            AmtMain = Sm.GetGrdDec(Grd1, Row, 3),
                            AmtRate = Sm.GetGrdDec(Grd1, Row, 4)
                        });
                    }
                }
            }

            if(l.Count > 0)
            {
                if(l2.Count > 0)
                {
                    for (int Row1 = 0; Row1 < l.Count; Row1++)
                    {
                        for (int Row2 = 0; Row2 < l2.Count; Row2++)
                        {
                            if(l[Row1].SurveyDocNo == l2[Row2].SurveyDocNo &&
                                l[Row1].SurveyDNo == l2[Row2].SurveyDNo)
                            {
                                l[Row1].PaymentAmtMain -= l2[Row2].AmtMain;
                                l[Row1].PaymentAmtRate -= l2[Row2].AmtRate;

                                //l[Row1].OutstandingPaymentAmtMain += l2[Row2].AmtMain;
                                //l[Row1].OutstandingPaymentAmtRate += l2[Row2].AmtRate;

                                l[Row1].OutstandingPaymentAmtMain = l[Row1].AmtMain - l[Row1].PaymentAmtMain;
                                l[Row1].OutstandingPaymentAmtRate = l[Row1].AmtRate - l[Row1].PaymentAmtRate;

                                if (l[Row1].OutstandingPaymentAmtRate < 0)
                                {
                                    l[Row1].OutstandingPaymentAmtRate = 0;
                                }

                                if (l[Row1].AmtMain == l[Row1].PaymentAmtMain && l[Row1].AmtRate == l[Row1].PaymentAmtRate)
                                {
                                    l[Row1].PaymentInd = "F";
                                }
                                else if (l[Row1].PaymentAmtMain == 0 && l[Row1].PaymentAmtRate == 0)
                                {
                                    l[Row1].PaymentInd = "O";
                                }
                                else l[Row1].PaymentInd = "P";

                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPLPHdr(DocNo);
                ShowPLPDtl(DocNo);
                ComputeOutstandingAmt(TxtSurveyDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPLPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("C.PnCode, D.PnName, A.VoucherRequestDocNo, E.VoucherDocNo As VoucherDocNo, A.Remark, B.Amt, B.InterestRate, ");
            SQL.AppendLine("B.InterestRateAmt, B.TotalAmt, B.StartMth, B.Yr, E.DeptCode, A.SurveyDocNo ");
            SQL.AppendLine("From (Select * From TblPLPHdr Where DocNo = @DocNo) A ");
            SQL.AppendLine("Inner Join TblSurvey B On A.SurveyDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblRequestLP C On B.RQLPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPartner D On C.PnCode = D.PnCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr E On A.VoucherRequestDocNo = E.DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "StatusDesc", "PnCode",  
                        //6-10
                        "PnName", "VoucherRequestDocNo",  "VoucherDocNo", "Remark", "Amt", 
                        //11-15
                        "InterestRate", "InterestRateAmt", "TotalAmt", "StartMth",  "Yr", 
                        //16-17
                        "DeptCode", "SurveyDocNo"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    mPnCode = Sm.DrStr(dr, c[5]);
                    TxtPnName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    TxtInterestRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtInterestRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                    Sm.SetLue(LueMonth, Sm.DrStr(dr, c[14]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[15]));
                    mSurveyDeptCode = Sm.DrStr(dr, c[16]);
                    TxtSurveyDocNo.EditValue = Sm.DrStr(dr, c[17]);
                }, true
            );
        }

        private void ShowPLPDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, MonthName(Str_To_Date(B.Mth, '%m')) As Mth, B.Yr, A.AmtMain, A.AmtRate, A.Amt, A.SurveyDNo, A.Penalty ");
            SQL.AppendLine("From (Select * From TblPLPDtl Where DocNo = @DocNo) A ");
            SQL.AppendLine("Inner Join TblLoanSummary B On A.SurveyDocNo = B.SurveyDocNo And A.SurveyDNo = B.SurveyDNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "Mth", "Yr", "AmtMain", "AmtRate", "Amt",
                    "SurveyDNo", "Penalty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 7, 8, 9 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ComputeAmtRow(int Row)
        {
            Grd1.Cells[Row, 5].Value = Sm.GetGrdDec(Grd1, Row, 3) + Sm.GetGrdDec(Grd1, Row, 4);
        }

        internal string GetSelectedSurvey()
        {
            string SQL = string.Empty;
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ",";
                        SQL +=
                            TxtSurveyDocNo.Text + 
                            Sm.GetGrdStr(Grd1, Row, 6);
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void PrepSurveyData(ref List<LoanSummary> l)
        {
            int mStartMth = 0;
            int mStartYr = 0;
            decimal mInstallment = 0;

            decimal
                mDNo = 1m,
                mRate = 0m,
                mMain = 0m,
                mOutstandingLoanAmt = 0m,
                mCurrLoanAmt = 0m,
                mInterestRate = 3;

            mStartMth = Int32.Parse(Sm.GetValue("Select StartMth From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));
            mStartYr = Int32.Parse(Sm.GetValue("Select Yr From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));
            mInstallment = Decimal.Parse(Sm.GetValue("Select Installment From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));

            mCurrLoanAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));
            mOutstandingLoanAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));
            mInterestRate = Decimal.Parse(Sm.GetParameter("InterestRate"));
            mMain = mCurrLoanAmt / mInstallment;

            if (mInstallment > 0 && mCurrLoanAmt > 0 && mStartMth > 0 && mStartYr > 0)
            {
                for (int i = 0; i < mInstallment; i++)
                {
                    if (mStartMth == 13)
                    {
                        mStartMth = 1;
                        mStartYr += 1;
                    }

                    if (mStartMth == 1) mCurrLoanAmt = mOutstandingLoanAmt;

                    mRate = ((mInterestRate / 12) / 100) * mCurrLoanAmt;

                    mOutstandingLoanAmt -= mMain;

                    l.Add(new LoanSummary()
                    {
                        SurveyDocNo = TxtSurveyDocNo.Text,
                        SurveyDNo = Sm.Right(string.Concat("000", mDNo.ToString()), 3),
                        Mth = Sm.Right(string.Concat("00", mStartMth.ToString()), 2),
                        Yr = mStartYr.ToString(),
                        AmtMain = mMain,
                        AmtRate = mRate,
                        Amt = mMain + mRate,
                        OutstandingLoanAmt = mOutstandingLoanAmt,
                        PaymentInd = "O",
                        OutstandingPaymentAmtMain = mMain,
                        OutstandingPaymentAmtRate = mRate,
                    });

                    mStartMth += 1;
                    mDNo += 1;
                }
            }
        }

        private void PrepPLPData(ref List<Payment> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SurveyDocNo, A.SurveyDNo, ");
            SQL.AppendLine("Sum(A.AmtMain) AmtMain, Sum(A.AmtRate) AmtRate ");
            SQL.AppendLine("From TblPLPDtl A ");
            SQL.AppendLine("Inner Join TblPLPHdr B On A.DocNo = B.DocNo And B.Status = 'A' And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.SurveyDocNo = @SurveyDocNo ");
            SQL.AppendLine("Group By A.SurveyDocNo, A.SurveyDNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "SurveyDocNo",

                         //1-5
                         "SurveyDNo",
                         "AmtMain",
                         "AmtRate"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Payment()
                        {
                            SurveyDocNo = Sm.DrStr(dr, c[0]),

                            SurveyDNo = Sm.DrStr(dr, c[1]),
                            AmtMain = Sm.DrDec(dr, c[2]),
                            AmtRate = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessPaymentAmt(ref List<LoanSummary> l, ref List<Payment> l2)
        {
            if(l2.Count > 0)
            {
                if (l.Count > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        for (int j = 0; j < l2.Count; j++)
                        {
                            if (l[i].SurveyDocNo == l2[j].SurveyDocNo && l[i].SurveyDNo == l2[j].SurveyDNo)
                            {
                                l[i].PaymentAmtMain = l2[j].AmtMain;
                                l[i].PaymentAmtRate = l2[j].AmtRate;

                                l[i].OutstandingPaymentAmtMain = l[i].AmtMain - l[i].PaymentAmtMain;
                                l[i].OutstandingPaymentAmtRate = l[i].AmtRate - l[i].PaymentAmtRate;

                                if (l[i].AmtMain == l[i].PaymentAmtMain && l[i].AmtRate == l[i].PaymentAmtRate)
                                {
                                    l[i].PaymentInd = "F";
                                }
                                else if (l[i].PaymentAmtMain == 0 && l[i].PaymentAmtRate == 0)
                                {
                                    l[i].PaymentInd = "O";
                                }
                                else l[i].PaymentInd = "P";

                                break;
                            }
                        }
                    }
                }
            }
        }

        private void RecalculateLoanSummary(ref List<LoanSummary> l)
        {
            if (l.Count > 0)
            {
                decimal mOutstandingLoanAmt = 0m, mCurrLoanAmt = 0m, mInterestRate = 3;

                mOutstandingLoanAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));
                mInterestRate = Decimal.Parse(Sm.GetParameter("InterestRate"));
                mCurrLoanAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblSurvey Where DocNo = @Param; ", TxtSurveyDocNo.Text));

                for (int x = 0; x < l.Count; x++)
                {
                    if (l[x].Mth == "01") mCurrLoanAmt = mOutstandingLoanAmt;

                    l[x].AmtRate = ((mInterestRate / 12) / 100) * mCurrLoanAmt;

                    mOutstandingLoanAmt -= l[x].PaymentAmtMain;

                    l[x].OutstandingLoanAmt = mOutstandingLoanAmt;

                    l[x].OutstandingPaymentAmtMain = l[x].AmtMain - l[x].PaymentAmtMain;
                    l[x].OutstandingPaymentAmtRate = l[x].AmtRate - l[x].PaymentAmtRate;

                    l[x].Amt = l[x].AmtMain + l[x].AmtRate;
                }
            }
        }

        private void InsertNewLoanSummary(ref List<LoanSummary> l)
        {
            if(l.Count > 0)
            {
                var cml = new List<MySqlCommand>();

                cml.Add(DeleteLoanSummary());

                for (int i = 0; i < l.Count; i++)
                {
                    cml.Add(InsertLoanSummary(ref l, i));
                }

                cml.Add(UpdateRequestLP());

                Sm.ExecCommands(cml);
            }
        }

        private MySqlCommand DeleteLoanSummary()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblLoanSummary Where SurveyDocNo = @SurveyDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", TxtSurveyDocNo.Text);

            return cm;
        }

        private MySqlCommand InsertLoanSummary(ref List<LoanSummary> l, int i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLoanSummary(SurveyDocNo, SurveyDNo, Mth, Yr, AmtMain, AmtRate, ");
            SQL.AppendLine("Amt, OutstandingLoanAmt, PaymentInd, PaymentAmtMain, PaymentAmtRate, ");
            SQL.AppendLine("OutstandingPaymentAmtMain, OutstandingPaymentAmtRate, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SurveyDocNo, @SurveyDNo, @Mth, @Yr, @AmtMain, @AmtRate, ");
            SQL.AppendLine("@Amt, @OutstandingLoanAmt, @PaymentInd, @PaymentAmtMain, @PaymentAmtRate, ");
            SQL.AppendLine("@OutstandingPaymentAmtMain, @OutstandingPaymentAmtRate, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SurveyDocNo", l[i].SurveyDocNo);
            Sm.CmParam<String>(ref cm, "@SurveyDNo", l[i].SurveyDNo);
            Sm.CmParam<String>(ref cm, "@Mth", l[i].Mth);
            Sm.CmParam<String>(ref cm, "@Yr", l[i].Yr);
            Sm.CmParam<Decimal>(ref cm, "@AmtMain", l[i].AmtMain);
            Sm.CmParam<Decimal>(ref cm, "@AmtRate", l[i].AmtRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", l[i].Amt);
            Sm.CmParam<Decimal>(ref cm, "@OutstandingLoanAmt", l[i].OutstandingLoanAmt);
            Sm.CmParam<String>(ref cm, "@PaymentInd", l[i].PaymentInd);
            Sm.CmParam<Decimal>(ref cm, "@PaymentAmtMain", l[i].PaymentAmtMain);
            Sm.CmParam<Decimal>(ref cm, "@PaymentAmtRate", l[i].PaymentAmtRate);
            Sm.CmParam<Decimal>(ref cm, "@OutstandingPaymentAmtMain", l[i].OutstandingPaymentAmtMain);
            Sm.CmParam<Decimal>(ref cm, "@OutstandingPaymentAmtRate", l[i].OutstandingPaymentAmtRate);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        internal void ClearData2()
        {
            mPnCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueMonth, LueYr, TxtVoucherRequestDocNo,
                TxtPnName, TxtSurveyDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt, TxtInterestRateAmt, TxtInterestRate, TxtTotalAmt, TxtOutstandingAmt
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5 });
        }

        internal void ShowSurveyHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select B.PnCode, C.PnName, A.DocNo, A.Amt, A.InterestRate, A.InterestRateAmt, A.TotalAmt, A.StartMth, A.Yr ");
            SQL.AppendLine("From (Select * From TblSurvey Where DocNo = @DocNo) A ");
            SQL.AppendLine("Inner Join TblRequestLP B On A.RQLPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPartner C On B.PnCode = C.PnCode; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                    {
                        //0
                        "PnCode",
                        //1-5
                        "PnName", "DocNo", "Amt", "InterestRate", "InterestRateAmt",  
                        //6-8
                        "TotalAmt", "StartMth",  "Yr"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    mPnCode = Sm.DrStr(dr, c[0]);
                    TxtPnName.EditValue = Sm.DrStr(dr, c[1]);
                    TxtSurveyDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                    TxtInterestRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtInterestRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    Sm.SetLue(LueMonth, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[8]));
                }, true
            );
        }

        internal void ShowSurveyDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Mth, MonthName(Str_To_Date(Mth, '%m')) as Mths, Yr, AmtMain, AmtRate, Amt, DNo ");
            SQL.AppendLine("From TblLoanSummary ");
            SQL.AppendLine("Where SurveyDocNo = @DocNo And PaymentInd = 'O' ");
            SQL.AppendLine("Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "Mth", 
                    "Mths", "Yr", "AmtMain", "AmtRate", "Amt",
                    "DNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void GetParameter()
        {
            mSurveyDeptCode = Sm.GetParameter("SurveyDeptCode");
            mLoanPaymentCalculationType = Sm.GetParameter("LoanPaymentCalculationType");
            mIsRateAmtCanExceedInitialRate = Sm.GetParameterBoo("IsRateAmtCanExceedInitialRate");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
        }

        public static void SetLueMonth(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "select '01' As Col1, 'January' As Col2 union All " +
                "select '02','February' Union All " +
                "select '03','March' Union All " +
                "select '04','April' Union All " +
                "select '05','May' Union All " +
                "select '06','June' Union All " +
                "select '07','July' Union All " +
                "select '08','August' Union All " +
                "select '09','September' Union All " +
                "select '10','October' Union All " +
                "select '11','November' Union All " +
                "select '12','December'",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        //private void GrdRemoveRow(iGrid Grd, KeyEventArgs e, SimpleButton Btn)
        //{
        //    if (Btn.Enabled && e.KeyCode == Keys.Delete)
        //    {
        //        if (Grd.SelectedRows.Count > 0)
        //        {
        //            if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
        //                MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            else if (Grd.Rows[0].Selected)
        //                MessageBox.Show("You can't remove first row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            else
        //            {
        //                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        //                {
        //                    for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
        //                        Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
        //                    if (Grd.Rows.Count <= 0) Grd.Rows.Add();
        //                }
        //            }
        //        }
        //    }
        //}

        internal void ComputeOutstandingAmt(string SurveyDocNo)
        {
            decimal mOutstandingAmt = 0m;
            string mVoucheredAmt = string.Empty;
            if (Decimal.Parse(TxtTotalAmt.Text) != 0) mOutstandingAmt = Decimal.Parse(TxtTotalAmt.Text);

            if (Grd1.Rows.Count > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select B.SurveyDocNo, (A.TotalAmt - (Sum(B.PaymentAmtMain + B.PaymentAmtRate))) OutstandingTotalAmt ");
                SQL.AppendLine("From TblSurvey A ");
                SQL.AppendLine("Inner Join TblLoanSummary B On A.DocNo = B.SurveyDocNo And A.DocNo = @SurveyDocNo ");
                SQL.AppendLine("Group By B.SurveyDocNo ");
                SQL.AppendLine("; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@SurveyDocNo", SurveyDocNo);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        { "SurveyDocNo", "OutstandingTotalAmt" });
                    if (dr.HasRows)
                        while (dr.Read())
                            mOutstandingAmt = Sm.DrDec(dr, c[1]);
                    
                    dr.Close();
                }
                
                //SQL.AppendLine("Select Sum(B.Amt) Amt ");
                //SQL.AppendLine("From (Select * From TblPLPHdr Where SurveyDocNo = @Param And Status <> 'C' And CancelInd = 'N') A ");
                //SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo And B.CancelInd = 'N' ");
                //SQL.AppendLine("Group By A.SurveyDocNo; ");

                //mVoucheredAmt = Sm.GetValue(SQL.ToString(), TxtSurveyDocNo.Text);
                //if(mVoucheredAmt.Length > 0)
                //    mOutstandingAmt -= Decimal.Parse(mVoucheredAmt);
            }
            TxtOutstandingAmt.EditValue = Sm.FormatNum(mOutstandingAmt, 0);
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PLPDtl>();
            var l2 = new List<PLPDtlPartial>();
            var l3 = new List<PLPDtlBalance>();

            string[] TableName = { "PLPDtl" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F.PnName, IfNull(Concat(', ', I.CityName), '') CityName, Truncate(C.NoOfLoan, 0) NoOfLoan, Concat(C.DueDt, '-', D.Mth, '-', D.Yr) DueDt, ");
            SQL.AppendLine("(B.Amt + B.Penalty) Amt, B.SurveyDNo, B.AmtMain, B.AmtRate, B.Penalty, C.Amt As LoanAmt,  ");
            SQL.AppendLine("Date_Format(A.DocDt, '%d-%m-%Y') DocDt, H.UserName, B.SurveyDocNo ");
            SQL.AppendLine("From TblPLPHdr A ");
            SQL.AppendLine("Inner Join TblPLPDtl B ON A.DocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSurvey C On A.SurveyDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblLoanSummary D On B.SurveyDocNo = D.SurveyDocNo And B.SurveyDNo = D.SurveyDNo ");
            SQL.AppendLine("Inner Join TblRequestLP E On C.RQLPDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblPartner F On E.PnCode = F.PnCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr G On A.VoucherRequestDocNo = G.DocNo ");
            SQL.AppendLine("Left Join TblUser H On G.PIC = H.UserCode ");
            SQL.AppendLine("Left Join TblCity I On F.CityCode = I.CityCode ");
            //SQL.AppendLine("Group By F.PnName, I.CityName, C.NoOfLoan, C.DueDt, D.Mth, D.Yr, B.SurveyDNo, B.AmtMain, ");
            //SQL.AppendLine("B.AmtRate, B.Penalty, C.Amt, A.DocDt, H.UserName, B.SurveyDocNo ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "PnName",

                    //1-5
                    "CityName",
                    "NoOfLoan",
                    "DueDt",
                    "Amt",
                    "SurveyDNo",

                    //6-10
                    "AmtMain",
                    "AmtRate",
                    "Penalty",
                    "LoanAmt",
                    "DocDt",

                    //11-13
                    "UserName",
                    "CompanyLogo",
                    "SurveyDocNo"
                });
                if (dr.HasRows)
                {
                    decimal mIndex = 0, mGroupHeader = 1;
                    while (dr.Read())
                    {
                        if(mIndex == 2)
                        {
                            mGroupHeader += 1;
                            mIndex = 0;
                        }
                        l.Add(new PLPDtl()
                        {
                            PnName = Sm.DrStr(dr, c[0]),

                            CityName = Sm.DrStr(dr, c[1]),
                            NoOfLoan = Sm.ToRoman(Sm.DrDec(dr, c[2])),
                            DueDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                            SurveyDNo = Decimal.Parse(Sm.DrStr(dr, c[5])).ToString(),

                            AmtMain = Sm.DrDec(dr, c[6]),
                            AmtRate = Sm.DrDec(dr, c[7]),
                            Penalty = Sm.DrDec(dr, c[8]),
                            LoanAmt = Sm.DrDec(dr, c[9]),
                            DocDt = Sm.DrStr(dr, c[10]),

                            UserName = Sm.DrStr(dr, c[11]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[4])),
                            CompanyLogo = Sm.DrStr(dr, c[12]),
                            SurveyDocNo = Sm.DrStr(dr, c[13]),
                            GroupHeader = mGroupHeader.ToString()
                        });
                        l.Add(new PLPDtl()
                        {
                            PnName = Sm.DrStr(dr, c[0]),

                            CityName = Sm.DrStr(dr, c[1]),
                            NoOfLoan = Sm.ToRoman(Sm.DrDec(dr, c[2])),
                            DueDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                            SurveyDNo = Decimal.Parse(Sm.DrStr(dr, c[5])).ToString(),

                            AmtMain = Sm.DrDec(dr, c[6]),
                            AmtRate = Sm.DrDec(dr, c[7]),
                            Penalty = Sm.DrDec(dr, c[8]),
                            LoanAmt = Sm.DrDec(dr, c[9]),
                            DocDt = Sm.DrStr(dr, c[10]),

                            UserName = Sm.DrStr(dr, c[11]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[4])),
                            CompanyLogo = Sm.DrStr(dr, c[12]),
                            SurveyDocNo = Sm.DrStr(dr, c[13]),
                            GroupHeader = mGroupHeader.ToString()
                        });

                        mIndex += 1;
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                CalculatePartialAmt(ref l, ref l2);
                CalculatePartialAmt2(ref l, ref l2);
                //CalculateBalance(ref l3);
                InputPrevAmt(ref l, ref l2, ref l3);                
            }

            myLists.Add(l);

            Sm.PrintReport("PLP", myLists, TableName, false);

            l.Clear();
            l2.Clear();
            l3.Clear();

        }

        private void CalculatePartialAmt(ref List<PLPDtl> l, ref List<PLPDtlPartial> l2)
        {
            string mSurvey = string.Empty, mDt = string.Empty;
            mSurvey = TxtSurveyDocNo.Text;

            //for (int i = 0; i < l.Count; i++)
            //{
            //    if (mSurvey.Length > 0) mSurvey += ",";
            //    mSurvey += string.Concat(TxtSurveyDocNo.Text, Sm.Right(string.Concat("000", l[i].SurveyDNo), 3));
            //    //mSurvey += TxtSurveyDocNo.Text;
            //}

            mDt = Sm.GetValue("Select Concat(DocDt, Left(DocNo, 4)) Dt From TblPLPHdr Where DocNo = @Param; ", TxtDocNo.Text);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Sum(A.AmtMain) AmtMain, A.SurveyDocNo, A.SurveyDNo ");
            SQL.AppendLine("from tblplpdtl A ");
            SQL.AppendLine("inner join tblplphdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' And B.`Status` In ('A') ");
            //SQL.AppendLine("    And Find_In_Set(Concat(A.SurveyDocNo, A.SurveyDNo), @Survey) ");
            SQL.AppendLine("      And Find_In_Set(A.SurveyDocNo, @Survey) ");
            SQL.AppendLine("Where Concat(B.DocDt, Left(A.DocNo, 4)) <= @Dt ");
            SQL.AppendLine("Group By A.SurveyDocNo, A.SurveyDNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Survey", mSurvey);
                Sm.CmParam<String>(ref cm, "@Dt", mDt);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "AmtMain",

                    //1-2
                    "SurveyDocNo",
                    "SurveyDNo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        //mPartialAmt += Sm.DrDec(dr, c[0]);
                        l2.Add(new PLPDtlPartial()
                        {
                            PartialAmt = Sm.DrDec(dr, c[0]),

                            SurveyDocNo = Sm.DrStr(dr, c[1]),
                            SurveyDNo = Decimal.Parse(Sm.DrStr(dr, c[2])).ToString(),
                            Index = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void CalculatePartialAmt2(ref List<PLPDtl> l, ref List<PLPDtlPartial> l2)
        {
            if(l2.Count > 0)
            {
                decimal mPartialAmt = 0m;
                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < l2.Count; j++)
                    {
                        if(l[i].SurveyDocNo == l2[j].SurveyDocNo && l[i].SurveyDNo == l2[j].SurveyDNo)
                        {
                            l2[j].Index = "Y";
                            break;
                        }
                    }
                }

                for (int k = 0; k < l2.Count; k++)
                {
                    if (l2[k].Index.Length > 0)
                    {
                        l2[k].PartialAmt += mPartialAmt;
                        mPartialAmt = l2[k].PartialAmt;
                    }
                    else
                    {
                        mPartialAmt += l2[k].PartialAmt;
                    }
                }
            }
        }

        //private void CalculateBalance(ref List<PLPDtlBalance> l3)
        //{
        //    string mSurvey = string.Empty, mDt = string.Empty;
        //    mSurvey = TxtSurveyDocNo.Text;
        //    mDt = Sm.GetValue("Select Concat(DocDt, Left(DocNo, 4)) Dt From TblPLPHdr Where DocNo = @Param; ", TxtDocNo.Text);

        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Select Sum(A.AmtMain) AmtMain, A.SurveyDocNo ");
        //    SQL.AppendLine("from tblplpdtl A ");
        //    SQL.AppendLine("inner join tblplphdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' And B.`Status` In ('O', 'A') ");
        //    SQL.AppendLine("    And A.SurveyDocNo = @Survey And A.DocNo Not In (@DocNo) ");
        //    SQL.AppendLine("Where Concat(B.DocDt, Left(A.DocNo, 4)) <= @Dt ");
        //    SQL.AppendLine("Group By A.SurveyDocNo; ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //        Sm.CmParam<String>(ref cm, "@Survey", mSurvey);
        //        Sm.CmParam<String>(ref cm, "@Dt", mDt);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "AmtMain", "SurveyDocNo" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l3.Add(new PLPDtlBalance()
        //                {
        //                    Balance = Sm.DrDec(dr, c[0]),
        //                    SurveyDocNo = Sm.DrStr(dr, c[1]),
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        private void InputPrevAmt(ref List<PLPDtl> l, ref List<PLPDtlPartial> l2, ref List<PLPDtlBalance> l3)
        {
            if (l2.Count > 0)
            {
                //decimal mPartialAmt = 0m;
                //string mSurveyDocNo = string.Empty, mSurveyDNo = string.Empty;

                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < l2.Count; j++)
                    {
                        //if (mSurveyDocNo.Length <= 0)
                        //{
                        //    mSurveyDocNo = l[i].SurveyDocNo;
                        //    mSurveyDNo = l[i].SurveyDNo;
                        //    mPartialAmt += l2[j].PartialAmt;
                        //}
                        //else
                        //{
                        //    if (mSurveyDocNo == l[i].SurveyDocNo && mSurveyDNo == l[i].SurveyDNo) break;
                        //}
                        
                        if (l[i].SurveyDocNo == l2[j].SurveyDocNo && l[i].SurveyDNo == l2[j].SurveyDNo)
                        {
                            //mSurveyDocNo = l[i].SurveyDocNo;
                            //mSurveyDNo = l[i].SurveyDNo;
                            //mPartialAmt += l2[j].PartialAmt;
                            l[i].PartialAmt = l2[j].PartialAmt;
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i < l.Count; i++)
            {
                l[i].Balance = l[i].LoanAmt - l[i].PartialAmt;
            }

            //if(l3.Count > 0)
            //{
            //    for (int i = 0; i < l.Count; i++)
            //    {
            //        for (int j = 0; j < l3.Count; j++)
            //        {
            //            if (l[i].SurveyDocNo == l3[j].SurveyDocNo)
            //            {
            //                l[i].Balance = l[i].LoanAmt - l3[j].Balance;
            //                break;
            //            }
            //        }
            //    }
            //}
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnPnName_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmPLPDlg2(this));
        }

        private void BtnPnName2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPnName, "Partner", false))
            {
                var f = new FrmPartner(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mPnCode = mPnCode;
                f.ShowDialog();
            }
        }

        private void BtnSurveyDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSurveyDocNo, "Survey#", false))
            {
                var f = new FrmSurvey(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSurveyDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "VR#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

        #region Class

        private class LoanSummary
        {
            public string SurveyDocNo { get; set; }
            public string SurveyDNo { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public decimal AmtMain { get; set; }
            public decimal AmtRate { get; set; }
            public decimal Amt { get; set; }
            public decimal OutstandingLoanAmt { get; set; }
            public string PaymentInd { get; set; }
            public decimal PaymentAmtMain { get; set; }
            public decimal PaymentAmtRate { get; set; }
            public decimal OutstandingPaymentAmtMain { get; set; }
            public decimal OutstandingPaymentAmtRate { get; set; }
            public decimal PrevPaymentAmtMain { get; set; }
            public decimal PrevPaymentAmtRate { get; set; }
        }

        private class Payment
        {
            public string SurveyDocNo { get; set; }
            public string SurveyDNo { get; set; }
            public decimal AmtMain { get; set; }
            public decimal AmtRate { get; set; }
        }

        private class PLPDtl
        {
            public string PnName { get; set; }
            public string CityName { get; set; }
            public string NoOfLoan { get; set; }
            public string DueDt { get; set; }
            public decimal Amt { get; set; }
            public string SurveyDNo { get; set; }
            public decimal AmtMain { get; set; }
            public decimal AmtRate { get; set; }
            public decimal Penalty { get; set; }
            public decimal LoanAmt { get; set; }
            public string DocDt { get; set; }
            public string UserName { get; set; }
            public string Terbilang { get; set; }
            public string SurveyDocNo { get; set; }
            public string CompanyLogo { get; set; }
            public decimal PartialAmt { get; set; }
            public decimal Balance { get; set; }
            public string GroupHeader { get; set; }
        }

        private class PLPDtlPartial
        {
            public string SurveyDocNo { get; set; }
            public string SurveyDNo { get; set; }
            public decimal PartialAmt { get; set; }
            public string Index { get; set; }
        }

        private class PLPDtlBalance
        {
            public string SurveyDocNo { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
