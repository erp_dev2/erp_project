﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOQuotPromo : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmSOQuotPromoFind FrmFind;
        public string CusCode;

        #endregion

        #region Constructor

        public FrmSOQuotPromo(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of Quotation Promo";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueCurCode(ref LueCurCode);
            Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode' "));
            Sl.SetLueCtCode(ref LueCostumerCode);
            Sl.SetLueReasonCode(ref LueReason1);
            //Sl.SetLueReasonCode(ref LueReason2);
            //Sl.SetLueReasonCode(ref LueReason3);
            //Sl.SetLueReasonCode(ref LueReason4);
            
            SetGrd1();
            SetGrd2();
            //SetGrd4();

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid
        private void SetGrd1()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Find",

                        //1-2
                        "Agent Code", 
                        "Agent Name"
                    },
                    new int[] 
                    {
                        40, 
                        100, 400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2 });
        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    {
                        //0
                        "Find",

                        //1-3
                        "Item Code", 
                        "Item Name",
                        "Discount Rate"
                    },
                    new int[] 
                    {
                        40, 
                        100, 400, 200
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 3 }, 0);
        }

       
        //private void SetGrd4()
        //{
        //    Grd4.Cols.Count = 3;
        //    Grd4.FrozenArea.ColCount = 1;
        //    Sm.GrdHdrWithColWidth(
        //            Grd4, new string[] 
        //            {
        //                "User Code", 
        //                "User Name",
        //                "Status"
        //            },
        //            new int[] 
        //            {
        //                100, 
        //                100, 100
        //            }
        //        );
        //    Sm.GrdColReadOnly(Grd4, new int[] { 0, 1, 2 });
        //}
        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtUom, LueCurCode, DteDocDt, ChkActiveInd,  TxtCashBackAmt, TxtCashBack2, TxtCashBack1,
                        TxtTargetOmset, DteDtEnd, DteDtStart, LueCostumerCode, LueReason1, LueReason2, LueReason3, LueReason4
                    }, true);
                    Grd1.ReadOnly = true; 
                    Grd2.ReadOnly = true;
                    //Grd3.ReadOnly = true;
                    //Grd4.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         DteDocDt, LueCurCode, TxtCashBackAmt, TxtCashBack2, TxtCashBack1, 
                         TxtTargetOmset, DteDtEnd, DteDtStart, LueCostumerCode, LueReason1
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    //Grd3.ReadOnly = false;
                    //Grd4.ReadOnly = false;
                    TxtUom.Properties.ReadOnly = true;
                    ChkActiveInd.Checked = true;
                    DteDocDt.Focus();
                    //LueReason2.Properties.ReadOnly = true;
                    //LueReason3.Properties.ReadOnly = true;
                    //LueReason4.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode, TxtCashBackAmt, TxtCashBack2, TxtCashBack1, TxtUom,
                        TxtTargetOmset, DteDtEnd, DteDtStart, LueCostumerCode, LueReason1, LueReason2, LueReason3, LueReason4
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    //Grd3.ReadOnly = true;
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, TxtUom, DteDocDt, LueReason1, ChkActiveInd, LueReason2, LueReason3, LueReason4,
                  LueCurCode, TxtTargetOmset, DteDtEnd, DteDtStart, LueCostumerCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                 TxtCashBackAmt, TxtCashBack2, TxtCashBack1, TxtTargetOmset, 
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
            //Sm.ClearGrd(Grd3, true);
            //Sm.FocusGrd(Grd3, 0, 0);
            //Sm.ClearGrd(Grd4, true);
            //Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOQuotPromoFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteDtStart.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteDtEnd.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                TxtUom.Text = Sm.GetValue("Select ParValue From TblParameter Where ParCode='ShipmentUomCode' ");
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode' "));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSoQuotPromo Where SOQtPrNo=@DocNo" };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method 1
        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false; CusCode = Sm.GetLue(LueCostumerCode);
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSOQuotPromoDlg(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmSOQuotPromoDlg(this));
        }

        #endregion

        #region Grid Method 2
        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSOQuotPromoDlg2(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd2, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmSOQuotPromoDlg2(this));
        }


        #endregion

        #region Insert Data

        private void InsertData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;

                string DocNo = GenerateDocNo("TblSoQuotPromo");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveQuotPromoHdr(DocNo));
                //cml.Add(SaveQuotPromoDocApproval(DocNo));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveQuotPromoDtlAgent(DocNo, Row));
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveQuotPromoDtlItem(DocNo, Row));
               
                    Sm.ExecCommands(cml);

                

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand SaveQuotPromoHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSoQuotPromo(DocNo, DocDt, ActInd, DteStart, "+
                    "DteEnd, CurCode, TrgtNett, NoOfShpM, TtlShipM, UomCode, CashBack, CtCode, "+
                    "RsnNo1, RsnNo2, RsnNo3, RsnNo4, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @ActInd, @DteStart, @DteEnd, @CurCode, "+
                    "@TrgtNett, @NoOfShpM, @TtlShipM, @UomCode, @CashBack, @CtCode, @RsnNo1, " +
                    "@RsnNo2, @RsnNo3, @RsnNo4, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@DteStart", Sm.GetDte(DteDtStart));

            Sm.CmParamDt(ref cm, "@DteEnd", Sm.GetDte(DteDtEnd));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TrgtNett", Decimal.Parse(TxtTargetOmset.Text));
            Sm.CmParam<Decimal>(ref cm, "@NoOfShpM", Decimal.Parse(TxtCashBack1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TtlShipM", Decimal.Parse(TxtCashBack2.Text));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUom.Text);
            Sm.CmParam<Decimal>(ref cm, "@CashBack", Decimal.Parse(TxtCashBackAmt.Text));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCostumerCode));

            Sm.CmParam<String>(ref cm, "@RsnNo1", Sm.GetLue(LueReason1));
            Sm.CmParam<String>(ref cm, "@RsnNo2", Sm.GetLue(LueReason2));
            Sm.CmParam<String>(ref cm, "@RsnNo3", Sm.GetLue(LueReason3));
            Sm.CmParam<String>(ref cm, "@RsnNo4", Sm.GetLue(LueReason4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveQuotPromoDtlAgent(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSoQuotPromoAgt(DocNo, DNo, AgtCode, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @AgtCode, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveQuotPromoDtlItem(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSoQuotPromoItem(DocNo, DNo, ItCode, DiscRate, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @ItCode, @DiscRate, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DiscRate", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveQuotPromoDocApproval(String DocNo)
        {
            var SQL = new StringBuilder();
     
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='SOQuotPromo' ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString() 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private void CancelData()
        {
            try
            {
                string DocNo = TxtDocNo.Text;
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DocNo)) return;
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                cml.Add(CancelActive(DocNo));

                Sm.ExecCommands(cml);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand CancelActive(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOQuotPromo Set ");
            SQL.AppendLine("ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

       
        private bool IsCancelledSOQuotPromoProcessedAlready(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDocApproval ");
            SQL.AppendLine("Where DocNo=@DocNo And DocType='SOQuotPromo' And Status Is Not Null  ");
            SQL.AppendLine("Limit 1 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is processed already.");
                return true;
            }
            return false;
        }
        
        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Quotation Date") ||
                Sm.IsDteEmpty(DteDtStart, "Promo Date Start") ||
                Sm.IsDteEmpty(DteDtEnd, "Promo Date End") ||
                //IsTargetOmsetNotValid()||
                //IsTargetCashBackNotValid()||
                //IsTargetCashBackAmountNotValid()||
                IsDateNotvalid() ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LueCostumerCode, "Costumer") ||
                Sm.IsLueEmpty(LueReason1, "Reason (1)") ||
                IsGrdValueNotValid() ||
                //IsDocApprovalSettingNotExisted()||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in tab Item.");
                return true;
            }
            
            return false;
        }

        private bool IsCancelledDataNotValid(string DocNo)
        {
            return
                IsCancelledSOQuotPromoProcessedAlready(DocNo);
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 3, true, "Description : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine + "Discount Rate is 0.")) return true; 
            }
            return false;
        }

        private bool IsTargetOmsetNotValid()
        {
            decimal TargetOmset = 0m;

            if (TxtTargetOmset.Text.Length != 0) TargetOmset = decimal.Parse(TxtTargetOmset.Text);

            if (TargetOmset <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Target Omset is not valid.");
                return true;
            }
            return false;
        }

        private bool IsTargetCashBackNotValid()
        {
            decimal casb1= 0m;
            decimal casb2 = 0m; 

            if (TxtCashBack1.Text.Length != 0 || TxtCashBack2.Text.Length != 0)
            {
                casb1 = decimal.Parse(TxtCashBack1.Text);
                casb2 = decimal.Parse(TxtCashBack2.Text);
            }
            if (casb1 <= 0 || casb2 <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Cash Back Shipment is not valid.");
                return true;
            }
            return false;
        }

        private bool IsTargetCashBackAmountNotValid()
        {
            decimal CbAmount = 0m;

            if (TxtCashBackAmt.Text.Length != 0) CbAmount = decimal.Parse(TxtCashBackAmt.Text);

            if (CbAmount <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Cash Back Amount is not valid.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            if (!Sm.IsDataExist("Select DocType From TblDocApprovalSetting Where DocType='SOQuotPromo' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "No approval setting for this SO Quotation Promo request.");
                return true;
            }
            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowQuotPromo(DocNo);
                ShowQuotPromoDtlAgent(DocNo);
                ShowQuotPromoDtlItem(DocNo);
                //ShowQuotPromoDtlApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowQuotPromo(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, ActInd, DteStart, DteEnd, CurCode, TrgtNett, NoOfShpM, "+
                    "TtlShipM, UomCode, CashBack, CtCode, RsnNo1, "+
                    "RsnNo2, RsnNo3, RsnNo4 From TblSoQuotPromo Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", "DocDt", "ActInd", "DteStart", "DteEnd",
                        "CurCode", "TrgtNett", "NoOfShpM", "TtlShipM", "UomCode", "CashBack",
                        "CtCode", "RsnNo1", "RsnNo2", "RsnNo3", "RsnNo4"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetDte(DteDtStart, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteDtEnd, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[5]));
                        TxtTargetOmset.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtCashBack1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtCashBack2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtUom.EditValue = Sm.DrStr(dr, c[9]);
                        TxtCashBackAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);

                        Sm.SetLue(LueCostumerCode, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueReason1, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueReason2, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueReason3, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueReason4, Sm.DrStr(dr, c[15]));

                    }, true
                );
        }

        private void ShowQuotPromoDtlAgent(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AgtCode, B.AgtName  ");
                SQL.AppendLine("From TblSoQuotPromoAgt A Join TblAgent B On A.AgtCode = B.AgtCode");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "AgtCode","AgtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    }, false, false, false, true
            );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowQuotPromoDtlItem(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ItCode, B.ItName, ");
                SQL.AppendLine("A.DiscRate ");
                SQL.AppendLine("From TblSoQuotPromoItem A Join TblItem B On A.ItCode = B.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo");

                Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "ItCode","ItName","DiscRate"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 3, 2);
                    }, false, false, false, true
            );
                Sm.FocusGrd(Grd2, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        //private void ShowQuotPromoDtlApproval(string DocNo)
        //{
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;

        //        var cm = new MySqlCommand();
        //        Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select A.UserCode, B.UserName, A.Status ");
        //        SQL.AppendLine("From TblDocApproval A ");
        //        SQL.AppendLine("Inner Join TblUser B ");
        //        SQL.AppendLine("On A.UserCode = B.UserCode ");
        //        SQL.AppendLine("Where A.DocType = 'SOQuotPromo' ");
        //        SQL.AppendLine("And A.DocNo=@DocNo ");

        //        Sm.ShowDataInGrid(
        //            ref Grd4, ref cm, SQL.ToString(),
        //            new string[] 
        //            { 
        //                "UserCode","UserName", "Status"
        //            },
        //            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //            {
        //                Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
        //                Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
        //                Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 2);
        //            }, false, false, false, true
        //    );
        //        Sm.FocusGrd(Grd4, 0, 0);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.StdMsg(mMsgType.Warning, Exc.Message);
        //    }
        //    finally
        //    {
        //        SetFormControl(mState.View);
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

       

        #endregion

        #region Additional Method

        private string GenerateDocNo(string Tbl)
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'");
            
            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSoQuotPromo ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 5) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/SOQT-PR/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private bool IsDateNotvalid()
        {
           if(decimal.Parse(Sm.GetDte(DteDtStart)) >= decimal.Parse(Sm.GetDte(DteDtEnd)))
           {
               string date = Sm.GetDte(DteDtStart).Substring(0, 8);
               Sm.StdMsg(mMsgType.Warning, "Date Promo End Not Valid");
               return true;
           }
           return false;
        }

        public static void SetLue(LookUpEdit Lue, string Value)
        {
            Lue.EditValue = (Value.Length == 0 ? null : Value);
        }

        public static string GetLue(LookUpEdit Lue)
        {
            return (Lue.EditValue == null) ? "" : Lue.EditValue.ToString();
        }

        internal string GetSelectedTabPage1()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedTabPage2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        //internal string GetSelectedTabPage4()
        //{
        //    var SQL = string.Empty;
        //    if (Grd4.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL +=
        //                    "'" +
        //                    Sm.GetGrdStr(Grd4, Row, 1) +
        //                    "'";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "'XXX'" : SQL);
        //}

        #endregion       

        #endregion

        #region Event

        private void TxtTargetOmset_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTargetOmset, 0);
        }

        private void ChkActiveInd_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void TxtCashBack1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCashBack1, 0);
        }

        private void TxtCashBack2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCashBack2, 0);
        }

        private void TxtCashBackAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCashBackAmt, 0);
        }
     
        private void LueCostumerCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostumerCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
        }

        private void LueReason1_EditValueChanged(object sender, EventArgs e)
        {
            if (LueReason1.EditValue == null ||
                LueReason1.Text.Trim().Length == 0 ||
                LueReason1.EditValue.ToString().Trim().Length == 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                  LueReason2, LueReason3, LueReason4
                }, true);
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                  LueReason3, LueReason4
                }, true);
                Sm.SetControlReadOnly(LueReason2, false);
                Sl.SetLueReasonCode(ref LueReason2);
            }
            Sm.RefreshLookUpEdit(LueReason1, new Sm.RefreshLue1(Sl.SetLueReasonCode));
        }

        private void LueReason2_EditValueChanged(object sender, EventArgs e)
        {
            if (LueReason2.EditValue == null ||
                LueReason2.Text.Trim().Length == 0 ||
                LueReason2.EditValue.ToString().Trim().Length == 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                  LueReason3, LueReason4
                }, true);
            }
            else
            {
                Sm.SetControlReadOnly(LueReason4, true); 
                Sm.SetControlReadOnly(LueReason3, false);
                Sl.SetLueReasonCode(ref LueReason3);
            }
            Sm.RefreshLookUpEdit(LueReason2, new Sm.RefreshLue1(Sl.SetLueReasonCode));
        }

        private void LueReason3_EditValueChanged(object sender, EventArgs e)
        {
            if (LueReason3.EditValue == null ||
                LueReason3.Text.Trim().Length == 0 ||
                LueReason3.EditValue.ToString().Trim().Length == 0)
            {
                Sm.SetControlReadOnly(LueReason4, true);
            }
            else
            {
                Sm.SetControlReadOnly(LueReason4, false);
                Sl.SetLueReasonCode(ref LueReason4);
            }
            Sm.RefreshLookUpEdit(LueReason3, new Sm.RefreshLue1(Sl.SetLueReasonCode));
        }

        private void LueReason4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReason4, new Sm.RefreshLue1(Sl.SetLueReasonCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }
        
        private void LueUom_EditValueChanged(object sender, EventArgs e)
        {
        }

        #endregion

    }
}
