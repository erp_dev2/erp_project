﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollAuthorization : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPayrollAuthorizationFind FrmFind;

        #endregion

        #region Constructor

        public FrmPayrollAuthorization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueUserCode(ref LueUserCode);
            SetGrd();
            base.FrmLoad(sender, e);

            //if this application is called from other application
            //if (mDocNo.Length != 0)
            //{
            //    ShowData(mDocNo);
            //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            //}
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Grade Level" + Environment.NewLine + "Code",
                        "",
                        "Grade Level" + Environment.NewLine + "Name",           
                        "Grade Level" + Environment.NewLine + "Group"
                    },
                     new int[] 
                    {
                        20, 
                        100, 20, 150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueUserCode,
                    }, true);
                    Grd1.ReadOnly = true;
                    LueUserCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueUserCode, 
                    }, false);
                    Grd1.ReadOnly = false;
                    LueUserCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         LueUserCode,
                    }, true);
                    Grd1.ReadOnly = false;
                    Grd1.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueUserCode
            });
            Sm.FocusGrd(Grd1, 0, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPayrollAuthorizationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (LueUserCode.Properties.ReadOnly == false)
                {
                    if (IsDataNotValid() || IsUserCodeAlreadyExist() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
                    Cursor.Current = Cursors.WaitCursor;
                    string UserCode = Sm.GetLue(LueUserCode);

                    var cml = new List<MySqlCommand>();

                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SavePayRollAutho(UserCode, Row));

                    Sm.ExecCommands(cml);
                    ShowData(UserCode);
                }
                else
                {
                    if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
                    Cursor.Current = Cursors.WaitCursor;
                    string UserCode = Sm.GetLue(LueUserCode);
                    var cml = new List<MySqlCommand>();

                    string CreateBy = Sm.GetValue("Select CreateBy From TblPayrollAuthorization Where UserCode = '" + UserCode + "' Limit 1 ");
                    string CreateDt = Sm.GetValue("Select CreateDt From TblPayrollAuthorization Where UserCode = '" + UserCode + "' limit 1 ");
                    cml.Add(DeletePayRollAutho(UserCode));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(UpdateSavePayRollAutho(UserCode, Row, CreateBy, CreateDt));

                    Sm.ExecCommands(cml);
                    ShowData(UserCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (!Sm.IsLueEmpty(LueUserCode, "UserCode")) 
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdEnter(Grd1, e);
                Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueUserCode, "User Code"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPayrollAuthorizationDlg(this));
            }

            //if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0)
            //{
            //    e.DoDefault = false;
            //    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmItemCostDlg2(this, e.RowIndex));
            //}

            //if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmItem(mMenuCode);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
            //    f.ShowDialog();
            //}

            //if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmItem(mMenuCode);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
            //    f.ShowDialog();
            //}

            //if (BtnSave.Enabled)
            //{
            //    if (TxtDocNo.Text.Length == 0 && !IsItemEmpty(e) && Sm.IsGrdColSelected(new int[] { 2, 4, 6, 8, 9 }, e.ColIndex))
            //        Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //}

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueUserCode, "User Code"))
                Sm.FormShowDialog(new FrmPayrollAuthorizationDlg(this));

            //if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0)
            //    Sm.FormShowDialog(new FrmItemCostDlg2(this, e.RowIndex));


            //if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            //{
            //    var f = new FrmItem(mMenuCode);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
            //    f.ShowDialog();
            //}

            //if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            //{
            //    var f = new FrmItem(mMenuCode);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
            //    f.ShowDialog();
            //}
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            //if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
            //    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

            //if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length != 0)
            //    Grd1.Cells[e.RowIndex, e.ColIndex].Value = Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Trim();
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueUserCode, "User") ||
                IsGrdEmpty();
        }

        
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Grade level in list.");
                return true;
            }
            return false;
        }

        
        private MySqlCommand SavePayRollAutho(string UserCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPayrollAuthorization(UserCode, GrdLvlCode, CreateBy, CreateDt) " +
                    "Values(@UserCode, @GrdLvlCode, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSavePayRollAutho(string UserCode, int Row, string CreateBy, string CreateDt)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPayrollAuthorization(UserCode, GrdLvlCode, CreateBy, CreateDt, LastUpBy, LastUpDt ) " +
                    "Values(@UserCode, @GrdLvlCode, @CreateBy, @CreateDt, @LastUpBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", CreateBy);
            Sm.CmParam<String>(ref cm, "@CreateDt", CreateDt);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand DeletePayRollAutho(string UserCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                     "Delete From TblPayrollAuthorization Where UserCode=@UserCode "
            };
            Sm.CmParam<String>(ref cm, "@UserCode", Sm.GetLue(LueUserCode));
            return cm;
        }

        private bool IsUserCodeAlreadyExist()
        {
            if (Sm.IsDataExist("Select UserCode From TblPayrollAuthorization Where UserCode='" + Sm.GetLue(LueUserCode) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "User code ( " + Sm.GetLue(LueUserCode) + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPayRollAutho(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowPayRollAutho(string UserCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.UserCode, A.GrdLvlCode, B.GrdLvlName, C.GrdLvlGrpName " +
                    "From TblPayrollAuthorization A " +
                    "Inner Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode " +
                    "Inner Join TblGradeLevelGroup C On B.GrdLvlGrpCode=C.GrdLvlGrpCode " +
                    "Where A.UserCode=@UserCode Order By A.UserCode ",
                    new string[] 
                    { 
                        //0
                       "UserCode", 
                       
                       //1-5
                       "GrdLvlCode", "GrdLvlName", "GrdLvlGrpName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetLue(LueUserCode, Sm.DrStr(dr, c[0]));
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private bool IsItemEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        private void LueUserCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUserCode, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        #endregion

    }
}
