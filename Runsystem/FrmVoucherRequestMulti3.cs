#region Update
/*
    26/02/2021 [WED/PHT] New application
    02/06/2021 [TKG/PHT] mempercepat proses import, merubah GetParameter()
    02/06/2021 [IBL/PHT] Validasi no available maintenance budget & amount > available maintenance budget
    08/07/2021 [IBL/PHT] Custom bisa input data manual, budget ambil dari budget maintenance yearly
    14/07/2021 [DITA/PHT] Bug -> btnimport tidak ada action click
    28/09/2021 [ICA/PHT] mengubah source budget dari CBP profit loss monthly param MultiVRAvailableBudgetMaintenanceSource
    15/10/2021 [ICA/PHT] Year available budget untuk type manual ambil dari field Year, untuk type CSV ambil dari DocDt. 
    17/11/2021 [SET/PHT] Validasi Switching cost center debit kredit dibuat berdasarkan bank group berdasar parameter IsFilterByBankAccount
    25/11/2021 [IBL/PHT] Bug filter Left(DocDt,6) = Concat(@Param2, '" + mYr "') -> Concat(@Param2, '" + mMth "')
    14/02/2022 [DITA/PHT] menghide validsi maintenance budget sementara
    24/02/2022 [DITA/PHT] tambah save paired VR saat save VR
    21/04/2022 [TKG/PHT] mempercepat proses save
    17/05/2022 [TKG/PHT] tambah document approval berdasarkan department
    03/06/2022 [TKG/PHT] approval setting berdasarkan department menggunakan department dari  bank account (credit)
    10/06/2022 [BRI/PHT] tambah remark
    20/06/2022 [TKG/PHT] kedua approval voucher request switching cost center berdasarkan bank account credit
    21/02/2023 [WED/PHT] tambah validasi closing journal berdasarkan parameter IsClosingJournalBasedOnMultiProfitCenter
    15/03/2023 [DITA/PHT] validasi ketika amount yg diinputkan - (minus)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti3 : RunSystem.FrmBase15
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mVRMultiSwitchingCostCenterGeneratedFormat = "1";
        private bool
            mIsDocApprovalBasedOnDeptEnabled = false,
            mIsFilterByDept = false,
            mIsBudget2YearlyFormat = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsVRForBudgetUseAvailableBudget = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsAutoJournalActived = false
            ;
        internal bool mIsFilterByBankAccount = false;
        internal string
            mMultiVRAvailableBudgetMaintenanceSource = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestMulti3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueUserCode(ref LuePIC);
                SetLueMode(ref LueMode);
                Sm.SetLue(LueMode, "1");

                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    TxtPlafond, LueYr, LueMth
                }, true);

                BtnBCCode.Enabled = false;
                BtnImport.Enabled = true;

                ClearGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsDocApprovalBasedOnDeptEnabled = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='VoucherRequestSwitchingCostCenter' And DeptCode Is Not Null Limit 1;");

            //mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            //mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            //mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            //mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            //mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            //mIsVRForBudgetUseAvailableBudget = Sm.GetParameterBoo("IsVRForBudgetUseAvailableBudget");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ReqTypeForNonBudget', 'IsFilterByDept', 'IsBudget2YearlyFormat', 'IsMRShowEstimatedPrice', 'IsBudgetCalculateFromEstimatedPrice', ");
            SQL.AppendLine("'IsVRForBudgetUseAvailableBudget', 'VRMultiSwitchingCostCenterGeneratedFormat', 'MultiVRAvailableBudgetMaintenanceSource', ");
            SQL.AppendLine("'IsFilterByBankAccount', 'IsClosingJournalBasedOnMultiProfitCenter', 'IsAutoJournalActived' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                            case "IsVRForBudgetUseAvailableBudget": mIsVRForBudgetUseAvailableBudget = ParValue == "Y"; break;
                            case "IsFilterByBankAccount": mIsFilterByBankAccount = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;

                            //string
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "VRMultiSwitchingCostCenterGeneratedFormat":
                                mVRMultiSwitchingCostCenterGeneratedFormat = ParValue;
                                if (mVRMultiSwitchingCostCenterGeneratedFormat.Length == 0)
                                    mVRMultiSwitchingCostCenterGeneratedFormat = "1";
                                break;
                            case "MultiVRAvailableBudgetMaintenanceSource":
                                mMultiVRAvailableBudgetMaintenanceSource = ParValue;
                                if (mMultiVRAvailableBudgetMaintenanceSource.Length == 0)
                                    mMultiVRAvailableBudgetMaintenanceSource = "1";
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Bank Account (C)",
                        "Bank Account (C)",
                        "Bank Account (D)",
                        "Bank Account (D)",
                        "Amount",

                        //6-10
                        "Site Code",
                        "Site",
                        "Department" + Environment.NewLine + "Code",
                        "Department",
                        "Budget Category" + Environment.NewLine + "Code",

                        //11-15
                        "Budget Category",
                        "Available Budget",
                        "Voucher Request 1#",
                        "Voucher Request 2#",
                        "CCCode 1",

                        //16-20
                        "CCCode 2",
                        "BugdetMaintenanceExistsInd",
                        "AvailableBudgetMaintenance",
                        "CCCode 3",
                        "Amount",

                        //21-25
                        "",
                        "",
                        "",
                        "Bank Code",
                        "Entity Code",

                        //26-29
                        "Currency Code",
                        "Remark",
                        "ProfitCenterCode (C)",
                        "ProfitCenterCode (D)"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 300, 150, 300, 130,
                        
                        //6-10
                        100, 150, 100, 200, 100, 
                        
                        //11-15
                        180, 150, 130, 130, 0,

                        //16-20
                        0, 0, 150, 0, 0,

                        //21-25
                        20, 20, 20, 0, 0,

                        //26-29
                        0, 150, 0, 0
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 27, 28, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6, 8, 10, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23, 28, 29 }, false);
            Sm.GrdColButton(Grd1, new int[] { 21, 22, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 12, 18, 20 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 17 });
            Grd1.Cols[21].Move(1);
            Grd1.Cols[22].Move(2);
            Grd1.Cols[23].Move(5);
            Grd1.Cols[27].Move(15);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 1, 3, 6, 8, 10, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               LueYr, LueMth, TxtBCCode, TxtBCName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPlafond }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsFirst = true;
                var SQL = new StringBuilder();
                var SQL2 = new StringBuilder();
                var SQL3 = new StringBuilder();
                var SQL4 = new StringBuilder();
                var cm = new MySqlCommand();
                var cml = new List<MySqlCommand>();
                var SQLForNewVoucherRequestDocNo = Sm.GetNewVoucherRequestDocNo(Sm.Left(Sm.GetDte(DteDocDt), 8), "VoucherRequest", 1);

                SQL2.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL2.AppendLine("Values  ");

                if (mIsDocApprovalBasedOnDeptEnabled)
                    SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

                Sm.CmParam<String>(ref cm, "@DocType", "73");
                Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
                Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
                SQL.AppendLine("Set @MainCurCode:=(Select ParValue From TblParameter Where ParCode='MainCurCode' And ParValue Is Not Null); ");

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        SaveVoucherRequest(ref SQL, ref cm, r, SQLForNewVoucherRequestDocNo, ref SQL2, ref SQL3, ref SQL4, ref IsFirst);

                cm.CommandText =
                    SQL.ToString() +
                    (IsFirst ? string.Empty : (SQL2.ToString() + ";")) +
                    (mIsDocApprovalBasedOnDeptEnabled ? SQL3.ToString() + ";" : string.Empty) +
                    SQL4.ToString()
                    ;
                cml.Add(cm);
                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
                ClearGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SaveVoucherRequest(ref StringBuilder SQL, ref MySqlCommand cm, int r, string SQLForNewVoucherRequestDocNo, ref StringBuilder SQL2, ref StringBuilder SQL3, ref StringBuilder SQL4, ref bool IsFirst)
        {
            string CCCode = Sm.GetGrdStr(Grd1, r, 15), CCCode2 = Sm.GetGrdStr(Grd1, r, 16);

            if (mVRMultiSwitchingCostCenterGeneratedFormat == "1" ||
                (mVRMultiSwitchingCostCenterGeneratedFormat != "1" && CCCode != CCCode2)
                ) // bikin 2 VR semua nya
            {
                #region bikin 2 vr semuanya

                for (int j = 0; j < 2; ++j)
                {
                    SQL.Append("Set @DocNo_" + j.ToString() + "_" + r.ToString() + ":=" + SQLForNewVoucherRequestDocNo + ";");

                    SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                    SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
                    SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, ");
                    SQL.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
                    SQL.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, ReqType, BCCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("@DocNo_" + j.ToString() + "_" + r.ToString() + ", @DocDt, 'N', 'O', 'N', @DeptCode_" + j.ToString() + "_" + r.ToString() + ", @DocType, ");
                    if (j == 0)
                        SQL.AppendLine("'C', ");
                    else
                        SQL.AppendLine("'D', ");
                    SQL.AppendLine("BankAcCode, NULL, NULL, @PaymentType, ");
                    SQL.AppendLine("BankCode, @PIC, CurCode, @Amt_" + j.ToString() + "_" + r.ToString() + ", CurCode, 1.00, ");
                    SQL.AppendLine("0.00, EntCode, @Remark_" + j.ToString() + "_" + r.ToString() + ", @SiteCode_" + j.ToString() + "_" + r.ToString() + ", 'Y', '1', @BCCode_" + j.ToString() + "_" + r.ToString() + ", @UserCode, @Dt ");
                    SQL.AppendLine("From TblBankAccount ");
                    if (j == 0)
                        SQL.AppendLine("Where BankAcCode=@BankAcCode_" + j.ToString() + "_" + r.ToString() + "; ");
                    else
                        SQL.AppendLine("Where BankAcCode=@BankAcCode2_" + j.ToString() + "_" + r.ToString() + "; ");

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        SQL2.AppendLine(", ");
                        if (mIsDocApprovalBasedOnDeptEnabled) SQL3.AppendLine(" Union All ");
                    }

                    SQL2.AppendLine("(@DocNo_" + j.ToString() + "_" + r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + j.ToString() + "_" + r.ToString() + ", @Remark_" + j.ToString() + "_" + r.ToString() + ", @UserCode, @Dt) ");

                    if (mIsDocApprovalBasedOnDeptEnabled)
                    {
                        SQL3.AppendLine("Select T.DocType, @DocNo_" + j.ToString() + "_" + r.ToString() + ", '001', T.DNo, @UserCode, @Dt ");
                        SQL3.AppendLine("From TblDocApprovalSetting T ");
                        SQL3.AppendLine("Where T.DocType='VoucherRequestSwitchingCostCenter' ");
                        //SQL3.AppendLine("And T.DeptCode=@DeptCode_" + j.ToString() + "_" + r.ToString());
                        SQL3.AppendLine("And T.DeptCode=IfNull(( ");
                        SQL3.AppendLine("   Select Distinct B.DeptCode From TblBankAccount A, TblCostCenter B ");
                        SQL3.AppendLine("   Where A.CCCode = B.CCCode And B.DeptCode Is Not Null And A.BankAcCode = @BankAcCode" + "_" + j.ToString() + "_" + r.ToString() + " And A.CCCode Is Not Null Limit 1 ");
                        SQL3.AppendLine("),'***') ");
                        SQL3.AppendLine(" And (T.StartAmt=0 ");
                        SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                        SQL3.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                        SQL3.AppendLine("    From TblVoucherRequestHdr A ");
                        SQL3.AppendLine("    Left Join ( ");
                        SQL3.AppendLine("        Select B1.CurCode1, B1.Amt ");
                        SQL3.AppendLine("        From TblCurrencyRate B1 ");
                        SQL3.AppendLine("        Inner Join ( ");
                        SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                        SQL3.AppendLine("            From TblCurrencyRate ");
                        SQL3.AppendLine("            Where CurCode2=@MainCurCode ");
                        SQL3.AppendLine("            Group By CurCode1 ");
                        SQL3.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                        SQL3.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                        SQL3.AppendLine("    Where A.DocNo=@DocNo_" + j.ToString() + "_" + r.ToString());
                        SQL3.AppendLine("), 0)) ");

                        SQL4.AppendLine("Update TblVoucherRequestHdr Set Status='A' Where DocNo=@DocNo_" + r.ToString());
                        SQL4.AppendLine(" And DocNo Not In (Select DocNo From TblDocApproval Where DocType='VoucherRequestSwitchingCostCenter' And DocNo=@DocNo_" + r.ToString() + "); ");

                    }

                    Sm.CmParam<String>(ref cm, "@BankAcCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@BankAcCode2_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@SiteCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@DeptCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@BCCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@Remark_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 27));
                }

                SQL.AppendLine("Update TblVoucherRequestHdr Set PairedVRDocNo=@DocNo_1_" + r.ToString());
                SQL.AppendLine(" Where DocNo=@DocNo_0_" + r.ToString() + "; ");

                SQL.AppendLine("Update TblVoucherRequestHdr Set PairedVRDocNo=@DocNo_0_" + r.ToString());
                SQL.AppendLine(" Where DocNo=@DocNo_1_" + r.ToString() + "; ");

                #endregion
            }
            else // kalau CCCode nya baris itu sama, cukup bikin 1 VR. kalau beda, bikin 2 VR
            {
                #region cek berdasarkan cost center nya

                SQL.Append("Set @DocNo_" + r.ToString() + ":=" + SQLForNewVoucherRequestDocNo + ";");

                SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
                SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
                SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, ");
                SQL.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
                SQL.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, ReqType, BCCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@DocNo_" + r.ToString() + ", @DocDt, 'N', 'A', 'N', @DeptCode_" + r.ToString() + ", @DocType, ");
                SQL.AppendLine("'C', @BankAcCode_" + r.ToString() + ", 'D', @BankAcCode2_" + r.ToString() + ", @PaymentType, ");
                SQL.AppendLine("@BankCode_" + r.ToString() + ", @PIC, @CurCode_" + r.ToString() + ", @Amt_" + r.ToString() + ", @CurCode_" + r.ToString() + ", 1.00, ");
                SQL.AppendLine("0.00, @EntCode_" + r.ToString() + ", @Remark_" + r.ToString() + ", @SiteCode_" + r.ToString() + ", 'Y', '1', @BCCode_" + r.ToString() + ", @UserCode, @Dt); ");

                if (IsFirst)
                    IsFirst = false;
                else
                {
                    SQL2.AppendLine(", ");
                    if (mIsDocApprovalBasedOnDeptEnabled) SQL3.AppendLine(" Union All ");
                }

                SQL2.AppendLine("(@DocNo_" + r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + r.ToString() + ", @Remark_" + r.ToString() + ", @UserCode, @Dt) ");

                if (mIsDocApprovalBasedOnDeptEnabled)
                {
                    SQL3.AppendLine("Select T.DocType, @DocNo_" + r.ToString() + ", '001', T.DNo, @UserCode, @Dt ");
                    SQL3.AppendLine("From TblDocApprovalSetting T ");
                    SQL3.AppendLine("Where T.DocType='VoucherRequestSwitchingCostCenter' ");
                    // SQL3.AppendLine("And T.DeptCode=@DeptCode_" + r.ToString());
                    SQL3.AppendLine("And T.DeptCode=IfNull(( ");
                    SQL3.AppendLine("   Select Distinct B.DeptCode From TblBankAccount A, TblCostCenter B ");
                    SQL3.AppendLine("   Where A.CCCode = B.CCCode And B.DeptCode Is Not Null And A.BankAcCode = @BankAcCode_" + r.ToString() + " And A.CCCode Is Not Null Limit 1 ");
                    SQL3.AppendLine("),'***') ");
                    SQL3.AppendLine(" And (T.StartAmt=0 ");
                    SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                    SQL3.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                    SQL3.AppendLine("    From TblVoucherRequestHdr A ");
                    SQL3.AppendLine("    Left Join ( ");
                    SQL3.AppendLine("        Select B1.CurCode1, B1.Amt ");
                    SQL3.AppendLine("        From TblCurrencyRate B1 ");
                    SQL3.AppendLine("        Inner Join ( ");
                    SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL3.AppendLine("            From TblCurrencyRate ");
                    SQL3.AppendLine("            Where CurCode2=@MainCurCode ");
                    SQL3.AppendLine("            Group By CurCode1 ");
                    SQL3.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                    SQL3.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                    SQL3.AppendLine("    Where A.DocNo=@DocNo_" + r.ToString());
                    SQL3.AppendLine("), 0)) ");

                    SQL4.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
                    SQL4.AppendLine("Where DocNo=@DocNo_" + r.ToString());
                    SQL4.AppendLine(" And DocNo Not In (Select DocNo From TblDocApproval Where DocType='VoucherRequestSwitchingCostCenter' And DocNo=@DocNo_" + r.ToString() + "); ");

                }
                Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                Sm.CmParam<String>(ref cm, "@BankAcCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                Sm.CmParam<String>(ref cm, "@BCCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                Sm.CmParam<String>(ref cm, "@BankCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 24));
                Sm.CmParam<String>(ref cm, "@EntCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 25));
                Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 26));
                Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 27));

                #endregion
            }
        }

        private MySqlCommand UpdatePairedVRDocNo(string DocNo, string PairedVRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/*Update Paired VR Docno - Multi VR Switching CC*/ ");
            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    PairedVRDocNo = @PairedVRDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PairedVRDocNo", PairedVRDocNo);

            return cm;
        }

        #region Old Code

        //override protected void BtnSaveClick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

        //        Cursor.Current = Cursors.WaitCursor;

        //        var SQL = new StringBuilder();
        //        var cm = new MySqlCommand();
        //        var cml = new List<MySqlCommand>();

        //        bool
        //          IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");

        //        string
        //            YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
        //            Yr = YrMth.Substring(2, 2),
        //            Mth = YrMth.Substring(4, 2),
        //            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //            DocSeqNo = "4";

        //        if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
        //        string IterasiNum = string.Empty;
        //        for (int i = 0; i < Convert.ToInt32(DocSeqNo); i++)
        //        {
        //            IterasiNum = string.Concat(IterasiNum, "0");
        //        }

        //        var v = Sm.GetValueDec(
        //            "Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp " +
        //            "From TblVoucherRequestHdr " +
        //            "Where Left(DocDt, 6)=@Param " +
        //            "Order By Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) Desc Limit 1;", 
        //            YrMth
        //            );


        //        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
        //            {
        //                cml.Add(SaveVoucherRequest(r, ref v, Yr, Mth, IterasiNum, DocSeqNo, DocAbbr));
        //                cml.Add(UpdatePairedVRDocNo(Sm.GetGrdStr(Grd1, r, (13)), Sm.GetGrdStr(Grd1, r, (14))));
        //                cml.Add(UpdatePairedVRDocNo(Sm.GetGrdStr(Grd1, r, (14)), Sm.GetGrdStr(Grd1, r, (13))));
        //            }
        //        }

        //        //cm.CommandText = SQL.ToString();
        //        //cml.Add(cm);
        //        Sm.ExecCommands(cml);

        //        Sm.StdMsg(mMsgType.Info, "Process is completed.");
        //        ClearGrd();
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        //private MySqlCommand SaveVoucherRequest(int r, ref decimal v, string Yr, string Mth, string IterasiNum, string DocSeqNo, string DocAbbr)
        //{
        //    var SQL = new StringBuilder();
        //    string CCCode = Sm.GetGrdStr(Grd1, r, 15);
        //    string CCCode2 = Sm.GetGrdStr(Grd1, r, 16);
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

        //    if (mVRMultiSwitchingCostCenterGeneratedFormat == "1" ||
        //        (mVRMultiSwitchingCostCenterGeneratedFormat != "1" && CCCode != CCCode2)
        //        ) // bikin 2 VR semua nya
        //    {
        //        #region bikin 2 vr semuanya
        //        for (int j = 0; j < 2; ++j)
        //        {
        //            v++;
        //            Grd1.Cells[r, (13 + j)].Value = string.Concat(Yr, "/", Mth, "/", Sm.Right(string.Concat(IterasiNum, v.ToString()), Convert.ToInt32(DocSeqNo)), "/", DocAbbr);

        //            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
        //            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
        //            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, ");
        //            SQL.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
        //            SQL.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, ReqType, BCCode, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select ");
        //            SQL.AppendLine("@DocNo_" + j.ToString() + "_" + r.ToString() + ", @DocDt, 'N', 'A', 'N', @DeptCode_" + j.ToString() + "_" + r.ToString() + ", @DocType, ");
        //            if (j == 0)
        //                SQL.AppendLine("'C', ");
        //            else
        //                SQL.AppendLine("'D', ");
        //            SQL.AppendLine("BankAcCode, NULL, NULL, @PaymentType, ");
        //            SQL.AppendLine("BankCode, @PIC, CurCode, @Amt_" + j.ToString() + "_" + r.ToString() + ", CurCode, 1.00, ");
        //            SQL.AppendLine("0.00, EntCode, @Remark, @SiteCode_" + j.ToString() + "_" + r.ToString() + ", 'Y', '1', @BCCode_" + j.ToString() + "_" + r.ToString() + ", @UserCode, @Dt ");
        //            SQL.AppendLine("From TblBankAccount ");
        //            if (j == 0)
        //                SQL.AppendLine("Where BankAcCode=@BankAcCode_" + j.ToString() + "_" + r.ToString() + "; ");
        //            else
        //                SQL.AppendLine("Where BankAcCode=@BankAcCode2_" + j.ToString() + "_" + r.ToString() + "; ");

        //            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Values (@DocNo_" + j.ToString() + "_" + r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + j.ToString() + "_" + r.ToString() + ", @Remark, @UserCode, @Dt); ");

        //            Sm.CmParam<String>(ref cm, "@DocNo_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, (13 + j)));
        //            Sm.CmParam<String>(ref cm, "@BankAcCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //            Sm.CmParam<String>(ref cm, "@BankAcCode2_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
        //            Sm.CmParam<String>(ref cm, "@SiteCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
        //            Sm.CmParam<String>(ref cm, "@DeptCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
        //            Sm.CmParam<String>(ref cm, "@BCCode_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
        //            Sm.CmParam<Decimal>(ref cm, "@Amt_" + j.ToString() + "_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
        //        }
        //        #endregion

        //    }
        //    else // kalau CCCode nya baris itu sama, cukup bikin 1 VR. kalau beda, bikin 2 VR
        //    {
        //        #region cek berdasarkan cost center nya

        //        v++;
        //        Grd1.Cells[r, 13].Value = string.Concat(Yr, "/", Mth, "/", Sm.Right(string.Concat(IterasiNum, v.ToString()), Convert.ToInt32(DocSeqNo)), "/", DocAbbr);

        //        SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
        //        SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, ");
        //        SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, ");
        //        SQL.AppendLine("BankCode, PIC, CurCode, Amt, CurCode2, ExcRate, ");
        //        SQL.AppendLine("DocEnclosure, EntCode, Remark, SiteCode, MultiInd, ReqType, BCCode, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select ");
        //        SQL.AppendLine("@DocNo_" + r.ToString() + ", @DocDt, 'N', 'A', 'N', @DeptCode_" + r.ToString() + ", @DocType, ");
        //        SQL.AppendLine("'C', BankAcCode, 'D', @BankAcCode2_" + r.ToString() + ", @PaymentType, ");
        //        SQL.AppendLine("BankCode, @PIC, CurCode, @Amt_" + r.ToString() + ", CurCode, 1.00, ");
        //        SQL.AppendLine("0.00, EntCode, @Remark, @SiteCode_" + r.ToString() + ", 'Y', '1', @BCCode_" + r.ToString() + ", @UserCode, @Dt ");
        //        SQL.AppendLine("From TblBankAccount ");
        //        SQL.AppendLine("Where BankAcCode=@BankAcCode_" + r.ToString() + "; ");

        //        SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values (@DocNo_" + r.ToString() + ", '001', 'Multi Voucher Request', @Amt_" + r.ToString() + ", @Remark, @UserCode, @Dt); ");


        //        Sm.CmParam<String>(ref cm, "@DocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
        //        Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //        Sm.CmParam<String>(ref cm, "@BankAcCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
        //        Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
        //        Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
        //        Sm.CmParam<String>(ref cm, "@BCCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
        //        Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));

        //        #endregion
        //    }


        //    cm.CommandText = SQL.ToString();

        //    Sm.CmParam<String>(ref cm, "@DocType", "73");
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
        //    Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdatePairedVRDocNo(string DocNo, string PairedVRDocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/*Update Paired VR Docno - Multi VR Switching CC*/ ");
        //    SQL.AppendLine("Update TblVoucherRequestHdr Set ");
        //    SQL.AppendLine("    PairedVRDocNo = @PairedVRDocNo ");
        //    SQL.AppendLine("Where DocNo = @DocNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@PairedVRDocNo", PairedVRDocNo);

        //    return cm;
        //}

        #endregion

        #endregion

        #region Additional Method


        internal void ComputePlafondAmount()
        {
            if (Grd1.Rows.Count == 1) return;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 5].Value = (Convert.ToDecimal(TxtPlafond.Text) / 100) * Sm.GetGrdDec(Grd1, Row, 20);
            }
        }

        internal void ProcessAvailableMaintenanceBudget()
        {
            var l = new List<DataTemp>();
            Process1();
            Process2(ref l);
            Process3(ref l);
        }

        private void Process1()
        {
            string mCCCode = string.Empty, mYr = string.Empty, mMth = string.Empty,
                   YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6);

            var SQL = new StringBuilder();

            if (Sm.GetLue(LueMode) == "1")
            {
                mYr = YrMth.Substring(0, 4);
                mMth = YrMth.Substring(4, 2);
            }
            else
            {
                mYr = Sm.GetLue(LueYr);
                mMth = Sm.GetLue(LueMth);
            }

            if (mMultiVRAvailableBudgetMaintenanceSource == "2")
            {
                SQL.AppendLine("Select Sum(T1.Amt - IfNull(T2.Amt, 0.00) - IfNull(T3.Amt, 0.00)) Amt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select C.BCCode, A.CCCode, Sum(B.Amt" + mMth + ") Amt ");
                SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
                SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    	And A.CancelInd = 'N' ");
                SQL.AppendLine("    	And A.CCCode Is Not Null ");
                SQL.AppendLine("    	And A.CompletedInd = 'Y' ");
                SQL.AppendLine("    	And A.Yr = @Param2 ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.AcNo = C.AcNo And Find_In_set(C.BCCode, @Param3) ");
                SQL.AppendLine("    Group By C.BCCode, A.CCCode ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select A.CCCode, C.BCCode, Sum(B.Amt) Amt ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.CCCode Is Not Null ");
                SQL.AppendLine("    	And A.Status = 'A' ");
                SQL.AppendLine("    	And A.CancelInd = 'N' ");
                SQL.AppendLine("    	And Left(A.DocDt, 6) = Concat(@Param2, '" + mMth + "') ");
                SQL.AppendLine("    	And B.ItCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode And A.CCCode = C.CCCode ");
                SQL.AppendLine("    And Find_In_Set(C.BCCode, @Param3) ");
                SQL.AppendLine("    Inner Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct X1.CCCode, X3.ItCode ");
                SQL.AppendLine("    	From TblCompanyBudgetPlanHdr X1 ");
                SQL.AppendLine("    	Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("    		And X1.CancelInd = 'N' ");
                SQL.AppendLine("    		And X1.CompletedInd = 'Y' ");
                SQL.AppendLine("            And X1.CCCode Is Not Null ");
                SQL.AppendLine("    		And X1.Yr = @Param2 ");
                SQL.AppendLine("    	Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo ");
                SQL.AppendLine("    	) D On A.CCCode = D.CCCode And B.ItCode = D.ItCode ");
                SQL.AppendLine("        Group By A.CCCode, C.BCCode ");
                SQL.AppendLine("    ) T2 On T1.CCCode = T2.CCCode AND T1.BCCode = T2.BCCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select A.CCCode, E.BCCode, Sum( ");
                SQL.AppendLine("    	Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End) As Amt ");
                SQL.AppendLine("    	From TblJournalHdr A ");
                SQL.AppendLine("    	Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    		And Left(A.DocDt, 6) = Concat(@Param2, '" + mMth + "') ");
                SQL.AppendLine("    		And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
                SQL.AppendLine("    		And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58') ");
                SQL.AppendLine("    		And A.CCCode Is Not Null ");
                SQL.AppendLine("    	Inner Join ");
                SQL.AppendLine("    	( ");
                SQL.AppendLine("    		Select Distinct X1.CCCode, X2.AcNo ");
                SQL.AppendLine("    		From TblCompanyBudgetPlanHdr X1 ");
                SQL.AppendLine("    		Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("    			And X1.CancelInd = 'N' ");
                SQL.AppendLine("    			And X1.CompletedInd = 'Y' ");
                SQL.AppendLine("    			And X1.CCCode Is Not Null ");
                SQL.AppendLine("    			And X1.Yr = @Param2 ");
                SQL.AppendLine("    		    And X2.AcNo In (Select Distinct AcNo From TblBudgetCategory Where Find_In_Set(BCCode, @Param3)) ");
                SQL.AppendLine("    	) C On A.CCCode = C.CCCode And B.AcNo = C.AcNo ");
                SQL.AppendLine("    	Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y' ");
                SQL.AppendLine("    	Inner Join TblBudgetCategory E On B.AcNo = E.AcNo And Find_In_Set(E.BCCode, @Param3) ");
                SQL.AppendLine("    	Group By A.CCCode, E.BCCode ");
                SQL.AppendLine("    ) T3 On T1.CCCode = T3.CCCode And T1.BCCode = T3.BCCode ");
                SQL.AppendLine("Where T1.CCCode = @Param1 ");
                SQL.AppendLine("Group By T1.BCCode ");
            }
            else
            {
                if (Sm.GetLue(LueMode) == "1")
                    SQL.AppendLine("Select D.Amt" + mMth + " As Amt  ");
                else
                    SQL.AppendLine("Select Sum(D.Amt" + mMth + ") As Amt  ");
                SQL.AppendLine("From TblBudgetRequestYearlyHdr A");
                SQL.AppendLine("Inner Join TblBudgetRequestYearlyDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    And A.CancelInd = 'N' ");
                SQL.AppendLine("    And A.Status = 'A' ");
                SQL.AppendLine("    And A.CCCode = @Param1 ");
                SQL.AppendLine("    And A.Yr = @Param2 ");
                SQL.AppendLine("    And B.BCCode = @Param3 ");
                SQL.AppendLine("Inner Join TblBudgetMaintenanceYearlyHdr C On A.DocNo = C.BudgetRequestDocNo ");
                SQL.AppendLine("Inner Join TblBudgetMaintenanceYearlyDtl D On C.DocNo = C.DocNo ");
                SQL.AppendLine("     And B.SeqNo = D.BudgetRequestSeqNo ");
                if (Sm.GetLue(LueMode) == "1")
                    SQL.AppendLine("Order By C.CreateDt Desc Limit 1; ");
                else
                    SQL.AppendLine("Group By A.CCCode, B.BCCode; ");
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                mCCCode = Sm.GetValue("Select CCCode From TblBudgetCategory Where BCCode = @Param", Sm.GetGrdStr(Grd1, Row, 10));
                Grd1.Cells[Row, 17].Value = Sm.IsDataExist(SQL.ToString(), mCCCode, mYr, Sm.GetGrdStr(Grd1, Row, 10));
                Grd1.Cells[Row, 18].Value = Sm.FormatNum(Sm.GetValue(SQL.ToString(), mCCCode, mYr, Sm.GetGrdStr(Grd1, Row, 10)), 0);
                Grd1.Cells[Row, 19].Value = mCCCode;
            }
        }

        private void Process2(ref List<DataTemp> l)
        {
            if (Grd1.Rows.Count > 0)
            {
                string BCCodeTemp = Sm.GetGrdStr(Grd1, 0, 10),
                   CCCodeTemp = Sm.GetGrdStr(Grd1, 0, 19);

                l.Add(new DataTemp() { BCCode = BCCodeTemp, CCCode = CCCodeTemp });

                for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10) != BCCodeTemp || Sm.GetGrdStr(Grd1, Row, 19) != CCCodeTemp)
                    {
                        l.Add(new DataTemp()
                        {
                            BCCode = Sm.GetGrdStr(Grd1, Row, 10),
                            CCCode = Sm.GetGrdStr(Grd1, Row, 19)
                        });
                        BCCodeTemp = Sm.GetGrdStr(Grd1, Row, 10);
                        CCCodeTemp = Sm.GetGrdStr(Grd1, Row, 19);
                    }
                }
            }
        }

        private void Process3(ref List<DataTemp> l)
        {
            bool isFirst = true;
            int RowTemp = 0;

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 10) == x.BCCode &&
                           Sm.GetGrdStr(Grd1, Row, 19) == x.CCCode)
                        {
                            if (isFirst)
                            {
                                RowTemp = Row;
                                isFirst = false;
                            }
                            else
                            {
                                if (Sm.GetGrdBool(Grd1, RowTemp, 17))
                                {
                                    Grd1.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, RowTemp, 18) - Sm.GetGrdDec(Grd1, RowTemp, 5);
                                    RowTemp = Row;
                                }
                            }
                        }
                    }
                    isFirst = true;
                }
            }
        }

        internal void ProcessAvailableBudget()
        {
            if (Grd1.Rows.Count == 1) return;

            string DeptBCCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DeptBCCode.Length > 0) DeptBCCode += ",";
                DeptBCCode += string.Concat(Sm.GetGrdStr(Grd1, i, 8), "#", Sm.GetGrdStr(Grd1, i, 10));
            }

            if (DeptBCCode.Length > 0)
            {
                var l = new List<BudgetSummary>();
                CalculateAvailableBudget(DeptBCCode, ref l);
                if (l.Count > 0)
                {
                    ProcessAvailableBudget2(ref l);
                }
                l.Clear();
            }
        }

        private void ProcessAvailableBudget2(ref List<BudgetSummary> l)
        {
            bool IsCalc = false;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                IsCalc = false;
                foreach (var x in l.Where(w =>
                    w.DeptCode == Sm.GetGrdStr(Grd1, i, 8)
                    ))
                {
                    if (Sm.GetGrdStr(Grd1, i, 10).ToUpper() == "ALL")
                    {
                        foreach (var y in l
                            .Where(w => w.DeptCode == x.DeptCode)
                            .OrderByDescending(o => o.AvailableBudget))
                        {
                            Grd1.Cells[i, 12].Value = Sm.FormatNum(y.AvailableBudget, 0);
                            y.AvailableBudget -= Sm.GetGrdDec(Grd1, i, 5);
                            IsCalc = true;
                            break;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, i, 10) == x.BCCode)
                    {
                        Grd1.Cells[i, 12].Value = Sm.FormatNum(x.AvailableBudget, 0);
                        x.AvailableBudget -= Sm.GetGrdDec(Grd1, i, 5);
                        IsCalc = true;
                    }

                    if (IsCalc) break;
                }
            }
        }

        private void CalculateAvailableBudget(string DeptBCCode, ref List<BudgetSummary> l)
        {
            string
                YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
                Yr = YrMth.Substring(0, 4),
                Mth = string.Empty;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetLue(LueMode) == "1")
            {
                Yr = YrMth.Substring(0, 4);
                Mth = YrMth.Substring(4, 2);
            }
            else
            {
                Yr = Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth);
            }

            if (mIsBudget2YearlyFormat)
            {
                SQL.AppendLine("Select A.Yr, '00' As Mth, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("A.Amt2, (IfNull(D.Amt3, 0.00) + IfNull(E.Amt, 0.00) + IfNull(F.Amt, 0.00)+ IfNull(G.Amt, 0.00)) As Amt3 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
                SQL.AppendLine("    From TblBudgetSummary A ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine("    And A.Yr = @Yr ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.BCCode, A.DeptCode, Left(A.DocDt, 4) As Yr, ");
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                else
                    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where B.Cancelind = 'N' ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, ");
                SQL.AppendLine("    Sum(Amt) Amt ");
                SQL.AppendLine("    From TblVoucherRequestHdr ");
                SQL.AppendLine("    Where ReqType Is Not Null ");
                SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(DocType, ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                SQL.AppendLine("    And DeptCode Is Not Null And BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(DeptCode, '#', BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 4) ");
                SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                SQL.AppendLine("    From TblTravelRequestHdr A ");
                SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And C.DeptCOde Is Not Null And B.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(C.DeptCOde, '#', B.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, SUM(T.Amt) As Amt From( ");
                SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("	    ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("            And X3.DeptCOde Is Not Null And X3.BCCode Is Not Null ");
                SQL.AppendLine("            And Find_In_set(Concat(X3.DeptCOde, '#', X3.BCCode), @DeptBCCode) ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("		  ) T ");
                SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4) ");
                SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr ");
                SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
            }
            else
            {
                SQL.AppendLine("Select A.Yr, A.Mth, A.DeptCode, A.BCCode, ");
                SQL.AppendLine("A.Amt2, ");
                SQL.AppendLine("(IfNull(D.Amt3, 0) + IfNull(E.Amt, 0) + IfNull(F.Amt, 0)+ IfNull(G.Amt, 0)) As Amt3 ");
                SQL.AppendLine("From TblBudgetSummary A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    And A.Yr = @Yr ");
                SQL.AppendLine("    And A.Mth = @Mth ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.BCCode, A.DeptCode, Left(A.DocDt, 4) As Yr, ");
                SQL.AppendLine("    Substring(A.DocDt, 5, 2) As Mth, ");
                if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    SQL.AppendLine("    Sum(B.Qty * B.EstPrice) As Amt3 ");
                else
                    SQL.AppendLine("    Sum(B.Qty*B.UPrice) As Amt3  ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where B.cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And A.DeptCOde Is Not Null And A.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(A.DeptCOde, '#', A.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By A.BCCode, A.DeptCode, Left(A.DocDt, 4) ");
                SQL.AppendLine("    , Substring(A.DocDt, 5, 2) ");
                SQL.AppendLine(") D On A.BCCode=D.BCCode And A.DeptCode=D.DeptCode And A.Yr=D.Yr And A.Mth=D.Mth ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select DeptCode, BCCode, Left(DocDt, 4) Yr, Substr(DocDt, 5, 2) Mth, ");
                SQL.AppendLine("    Sum(Amt) Amt ");
                SQL.AppendLine("    From TblVoucherRequestHdr ");
                SQL.AppendLine("    Where ReqType Is Not Null ");
                SQL.AppendLine("    And Reqtype <> @ReqTypeForNonBudget ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("    And Find_In_Set(DocType, ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("    And Left(DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substr(DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And DeptCOde Is Not Null And BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(DeptCOde, '#', BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By DeptCode, BCCode, Left(DocDt, 6) ");
                SQL.AppendLine(") E On A.BCCode=E.BCCode And A.DeptCode=E.DeptCode And A.Yr=E.Yr And A.Mth=E.Mth ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.BCCode, C.DeptCode, Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                SQL.AppendLine("    Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) As Amt  ");
                SQL.AppendLine("    From TblTravelRequestHdr A ");
                SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("    Where A.Cancelind = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("    And C.DeptCOde Is Not Null And B.BCCode Is Not Null ");
                SQL.AppendLine("    And Find_In_set(Concat(C.DeptCOde, '#', B.BCCode), @DeptBCCode) ");
                SQL.AppendLine("    Group By B.BCCode, C.DeptCode, Left(A.DocDt, 4), Substring(A.DocDt, 5, 2) ");
                SQL.AppendLine(") F On A.BCCode=F.BCCode And A.DeptCode=F.DeptCode And A.Yr=F.Yr And A.Mth=F.Mth ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine(" Select T.BcCode, T.DeptCode, T.Yr, T.Mth, SUM(T.Amt) As Amt From( ");
                SQL.AppendLine("    Select D.BCCode, D.DeptCode, A.DocDt ,Left(A.DocDt, 4) As Yr, Substring(A.DocDt, 5, 2) As Mth, ");
                SQL.AppendLine("    Case when A.AcType = 'D' Then IFNULL((A.Amt)*-1, 0.00) ELSE IFNULL((A.Amt), 0.00) END As Amt  ");
                SQL.AppendLine("        From tblvoucherhdr A ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("	    (");
                SQL.AppendLine("			SELECT X1.DocNo, X1.VoucherRequestDocNo  ");
                SQL.AppendLine("		  	From tblvoucherhdr X1 ");
                SQL.AppendLine("			INNER JOIN tblvoucherrequesthdr X2 ON X2.DocNo = X1.VoucherRequestDocNo ");
                SQL.AppendLine("			AND X1.DocType = '58' ");
                SQL.AppendLine("            AND X1.CancelInd = 'N' ");
                SQL.AppendLine("            AND X2.Status In ('O', 'A') ");
                SQL.AppendLine("		  ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("        And Substring(A.DocDt, 5, 2) = @Mth ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("		  ( ");
                SQL.AppendLine("		  	SELECT X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("		  	From tblvoucherhdr X1  ");
                SQL.AppendLine("		  	Inner Join tblcashadvancesettlementDTL X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("		  	INNER JOIN tblvoucherrequesthdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("			AND X1.DocType = '56' ");
                SQL.AppendLine("		    AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("            AND X3.Status In ('O', 'A')  ");
                SQL.AppendLine("            And X3.DeptCOde Is Not Null And X3.BCCode Is Not Null ");
                SQL.AppendLine("            And Find_In_set(Concat(X3.DeptCOde, '#', X3.BCCode), @DeptBCCode) ");
                SQL.AppendLine("		  ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("		  ) T ");
                SQL.AppendLine("    Group By T.BCCode, T.DeptCode, Left(T.DocDt, 4), Substring(T.DocDt, 5, 2) ");
                SQL.AppendLine(") G On A.BCCode=G.BCCode And A.DeptCode=G.DeptCode And A.Yr=G.Yr And A.Mth=G.Mth ");
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptBCCode", DeptBCCode);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "DeptCode", "BCCode", "Amt2", "Amt3" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetSummary()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            DeptCode = Sm.DrStr(dr, c[2]),
                            BCCode = Sm.DrStr(dr, c[3]),
                            Amt2 = Sm.DrDec(dr, c[4]),
                            Amt3 = Sm.DrDec(dr, c[5]),
                            AvailableBudget = Sm.DrDec(dr, c[4]) - Sm.DrDec(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string BankAcCodeTemp = string.Empty, BankAcCode2Temp = string.Empty, DeptCodeTemp = string.Empty, BCCodeTemp = string.Empty, SiteCodeTemp = string.Empty;
            var AmtTemp = 0m;
            bool IsFirst = true;
            string RemarkTemp = string.Empty;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            BankAcCodeTemp = arr[0].Trim();
                            BankAcCode2Temp = arr[1].Trim();
                            if (arr[2].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[2].Trim());
                            else
                                AmtTemp = 0m;
                            if (AmtTemp < 0m)
                            {
                                Sm.StdMsg(mMsgType.Warning, "Amount should not be less than 0.00 ");
                                return;
                            }
                            SiteCodeTemp = arr[3].Trim();
                            DeptCodeTemp = arr[4].Trim();
                            BCCodeTemp = arr[5].Trim();
                            RemarkTemp = arr[6].Trim();

                            l.Add(new Result()
                            {
                                BankAcCode = BankAcCodeTemp,
                                BankAcCode2 = BankAcCode2Temp,
                                BankAcNm = string.Empty,
                                BankAcNm2 = string.Empty,
                                Amt = AmtTemp,
                                SiteCode = SiteCodeTemp,
                                SiteName = string.Empty,
                                DeptCode = DeptCodeTemp,
                                DeptName = string.Empty,
                                BCCode = BCCodeTemp,
                                BCName = string.Empty,
                                AvailableBudget = 0m,
                                CCCode = string.Empty,
                                CCCode2 = string.Empty,
                                Remark = RemarkTemp,
                            });
                        }
                    }
                }
            }
        }

        private void Import2(ref List<BankAccount> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode, BankAcName, CCCode, BankCode, EntCode, CurCode From ( ");
            SQL.AppendLine("    Select BankAcCode, ");
            SQL.AppendLine("    Trim(Concat( ");
            SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
            SQL.AppendLine("    Case When A.BankAcNo Is Not Null Then Concat(A.BankAcNo) Else IfNull(A.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
            SQL.AppendLine(")) As BankAcName, A.CCCode, A.BankCode, A.EntCode, A.CurCode ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine(") T Order By BankAcName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{
                    "BankAcCode",

                    "BankAcName",
                    "CCCode",
                    "BankCode",
                    "EntCode",
                    "CurCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BankAccount()
                        {
                            BankAcCode = Sm.DrStr(dr, c[0]),
                            BankAcName = Sm.DrStr(dr, c[1]),
                            CCCode = Sm.DrStr(dr, c[2]),
                            BankCode = Sm.DrStr(dr, c[3]),
                            EntCode = Sm.DrStr(dr, c[4]),
                            CurCode = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_a(ref List<Department> l, ref List<Result> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subQuery = new StringBuilder(); // budget category
            bool IsFirst = true;
            int i = 0;

            foreach (var x in l2.Select(c => new { c.DeptCode }).Distinct())
            {
                if (x.DeptCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        subQuery.AppendLine(", ");
                    subQuery.AppendLine("@DeptCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), x.DeptCode);
                    i++;
                }
            }

            SQL.AppendLine("Select Distinct DeptCode, DeptName ");
            SQL.AppendLine("From TblDepartment ");
            if (subQuery.Length > 0)
            {
                SQL.AppendLine("Where DeptCode In (" + subQuery.ToString() + ") ");
                SQL.AppendLine("And ActInd = 'Y'; ");
            }
            else
                SQL.AppendLine("Where 1=0;");

            subQuery.Length = 0;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DeptCode", "DeptName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Department()
                        {
                            DeptCode = Sm.DrStr(dr, c[0]),
                            DeptName = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_b(ref List<BudgetCategory> l, ref List<Result> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subQuery = new StringBuilder(); // budget category
            bool IsFirst = true;
            int i = 0;

            foreach (var x in l2.Select(c => new { c.BCCode }).Distinct())
            {
                if (x.BCCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        subQuery.AppendLine(", ");
                    subQuery.AppendLine("@BCCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@BCCode_" + i.ToString(), x.BCCode);
                    i++;
                }
            }

            SQL.AppendLine("Select Distinct BCCode, BCName ");
            SQL.AppendLine("From TblBudgetCategory ");
            if (subQuery.Length > 0)
                SQL.AppendLine("Where BCCode In (" + subQuery.ToString() + "); ");
            else
                SQL.AppendLine("Where 1=0; ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select DeptCode, 'ALL' AS BCCode, 'ALL' As BCName ");
            //SQL.AppendLine("From TblDepartment ");
            //SQL.AppendLine("Where ActInd = 'Y';");

            subQuery.Length = 0;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ //"DeptCode", 
                    "BCCode", "BCName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetCategory()
                        {
                            //DeptCode = Sm.DrStr(dr, c[0]),
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import2_c(ref List<Site> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct SiteCode, SiteName ");
            SQL.AppendLine("From TblSite ");
            SQL.AppendLine("Where ActInd = 'Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Site()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            SiteName = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Import3(ref List<Result> l, ref List<BankAccount> l2, ref List<Department> l3, ref List<BudgetCategory> l4, ref List<Site> l5)
        {
            byte v = 0;
            foreach (var i in l)
            {
                v = 0;
                foreach (var j in l2)
                {
                    if (Sm.CompareStr(i.BankAcCode, j.BankAcCode))
                    {
                        i.BankAcNm = j.BankAcName;
                        i.CCCode = j.CCCode;
                        i.BankCode = j.BankCode;
                        i.EntCode = j.EntCode;
                        i.CurCode = j.CurCode;
                        v++;
                    }

                    if (Sm.CompareStr(i.BankAcCode2, j.BankAcCode))
                    {
                        i.BankAcNm2 = j.BankAcName;
                        i.CCCode2 = j.CCCode;
                        v++;
                    }
                    if (v == 2) break;
                }

                foreach (var x in l3.Where(w => w.DeptCode == i.DeptCode))
                {
                    i.DeptName = x.DeptName;
                }

                foreach (var y in l4.Where(w => w.BCCode == i.BCCode))
                {
                    i.BCName = y.BCName;
                }

                foreach (var z in l5.Where(w => w.SiteCode == i.SiteCode && i.SiteCode.Length > 0))
                {
                    i.SiteName = z.SiteName;
                }
            }
        }

        private void Import4(ref List<Result> l)
        {
            int No = 1;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = No;
                r.Cells[1].Value = l[i].BankAcCode;
                r.Cells[2].Value = l[i].BankAcNm;
                r.Cells[3].Value = l[i].BankAcCode2;
                r.Cells[4].Value = l[i].BankAcNm2;
                r.Cells[5].Value = l[i].Amt;
                r.Cells[6].Value = l[i].SiteCode;
                r.Cells[7].Value = l[i].SiteName;
                r.Cells[8].Value = l[i].DeptCode;
                r.Cells[9].Value = l[i].DeptName;
                r.Cells[10].Value = l[i].BCCode;
                r.Cells[11].Value = l[i].BCName;
                r.Cells[12].Value = Sm.FormatNum(l[i].AvailableBudget, 0);
                r.Cells[13].Value = null;
                r.Cells[14].Value = null;
                r.Cells[15].Value = l[i].CCCode;
                r.Cells[16].Value = l[i].CCCode2;
                r.Cells[24].Value = l[i].BankCode;
                r.Cells[25].Value = l[i].EntCode;
                r.Cells[26].Value = l[i].CurCode;
                r.Cells[27].Value = l[i].Remark;
                No++;
            }
            r = Grd1.Rows.Add();
            r.Cells[0].Value = No;
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 12 });
            Grd1.EndUpdate();
        }

        private bool IsInsertedDataNotValid()
        {
            string menuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmVoucherRequestMulti2' Limit 1; ");
            var VRM2 = new FrmVoucherRequestMulti2(menuCode);
            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (mIsClosingJournalBasedOnMultiProfitCenter && mIsAutoJournalActived)
            {
                VRM2.ProcessProfitCenter(ref Grd1, 1, 3, 28, 29);
            }

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                VRM2.IsClosingJournalForMultiVRInvalid(ref Grd1, DocDt, mIsClosingJournalBasedOnMultiProfitCenter, mIsAutoJournalActived, 1, 3, 28, 29)
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 bank account.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (C) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Bank Account (D) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is 0.00.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Department is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 11, false, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Budget Category is empty.")) return true;

                decimal Amt = 0m, AvailableBudget = 0m;
                Amt = Sm.GetGrdDec(Grd1, Row, 5);
                AvailableBudget = Sm.GetGrdDec(Grd1, Row, 12);


                if (mIsVRForBudgetUseAvailableBudget)
                {
                    if (Amt > AvailableBudget)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Amount is bigger than available budget (" + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) + ")");
                        Sm.FocusGrd(Grd1, Row, 5);
                        return true;
                    }
                }

                //remark by dita seementara 14/02/2022
                /* if (!Sm.GetGrdBool(Grd1, Row, 17))
                 {
                     Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Maintenance Budget " + Sm.GetGrdStr(Grd1, Row, 11) + " not available.");
                     return true;
                 }

                 if (Sm.GetGrdDec(Grd1, Row, 5) > Sm.GetGrdDec(Grd1, Row, 18))
                 {
                     Sm.StdMsg(mMsgType.Warning, "Row : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + "Available Maintenance Budget " + Sm.GetGrdStr(Grd1, Row, 11) + " is only " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0));
                     return true;
                 }*/
            }
            return false;
        }

        private void SetLueMode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'VoucherRequestMultiMode' ",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
        }

        private void LueMode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMode, new Sm.RefreshLue1(SetLueMode));
            if (Sm.GetLue(LueMode) == "1")
            {
                ClearData();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    TxtPlafond, LueYr, LueMth
                }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 21, 22, 23 });
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23 }, false);
                label3.ForeColor = label4.ForeColor = Color.Black;
                BtnBCCode.Enabled = false;
                BtnImport.Enabled = true;
            }
            else if (Sm.GetLue(LueMode) == "2")
            {
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPlafond }, 0);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtPlafond, LueYr, LueMth
                    }, false);
                Sm.GrdColReadOnly(false, true, Grd1, new int[] { 21, 22, 23 });
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23 }, true);
                TxtPlafond.EditValue = Sm.FormatNum(100, 0);
                label3.ForeColor = label4.ForeColor = Color.Red;
                BtnBCCode.Enabled = true;
                BtnImport.Enabled = false;
            }
            else if (Sm.GetLue(LueMode) == "")
            {
                ClearData();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    TxtPlafond, LueYr, LueMth
                }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 21, 22, 23 });
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23 }, false);
                label3.ForeColor = label4.ForeColor = Color.Black;
                BtnBCCode.Enabled = BtnImport.Enabled = false;
                Sm.StdMsg(mMsgType.Warning, "You have to choose one of these mode (CSV or Manual)");
                Sm.SetLue(LueMode, "1");
            }
        }

        private void TxtPlafond_Validated(object sender, EventArgs e)
        {
            decimal mPlafondAmt = Convert.ToDecimal(TxtPlafond.Text);
            if (mPlafondAmt > 100)
            {
                Sm.StdMsg(mMsgType.Warning, "Value should not more than 100.");
                TxtPlafond.EditValue = Sm.FormatNum(100, 0);
            }
            Sm.FormatNumTxt(TxtPlafond, 0);
            ComputePlafondAmount();
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void TxtBCCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void BtnBCCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg(this));
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();
            var l2 = new List<BankAccount>();
            var l3 = new List<Department>();
            var l4 = new List<BudgetCategory>();
            var l5 = new List<Site>();

            ClearGrd();
            try
            {
                Import1(ref l);
                if (l.Count > 0)
                {
                    Import2(ref l2);
                    Import2_a(ref l3, ref l);
                    Import2_b(ref l4, ref l);
                    Import2_c(ref l5);
                    Import3(ref l, ref l2, ref l3, ref l4, ref l5);
                    Import4(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                ProcessAvailableBudget();
                ProcessAvailableMaintenanceBudget();
                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Control Event

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 1 || e.ColIndex == 2)
            {
                if (Sm.GetGrdStr(Grd1, 0, 1).Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                        {
                            Grd1.Cells[Row, 1].Value = Sm.GetGrdStr(Grd1, 0, 1);
                            Grd1.Cells[Row, 2].Value = Sm.GetGrdStr(Grd1, 0, 2);
                        }
                    }
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Sm.GetLue(LueMode) == "2")
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    Grd1.Cells[i, 0].Value = i + 1;
            }

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 21)
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg2(this, TxtBCCode.Text, Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            }
            if (e.ColIndex == 22)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg3(this, e.RowIndex));
            }
            if (e.ColIndex == 23)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Center still empty.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg4(this, e.RowIndex));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 21)
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg2(this, TxtBCCode.Text, Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            }
            if (e.ColIndex == 22)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg3(this, e.RowIndex));
            }
            if (e.ColIndex == 23)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to choose Budget first.");
                    return;
                }
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Center still empty.");
                    return;
                }

                Sm.FormShowDialog(new FrmVoucherRequestMulti3Dlg4(this, e.RowIndex));
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string BankAcCode { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcCode2 { get; set; }
            public string BankAcNm2 { get; set; }
            public decimal Amt { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public decimal AvailableBudget { get; set; }
            public string CCCode { get; set; }
            public string CCCode2 { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string CurCode { get; set; }
            public string Remark { get; set; }
        }

        private class Site
        {
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
        }

        private class BankAccount
        {
            public string BankAcCode { get; set; }
            public string BankAcName { get; set; }
            public string CCCode { get; set; }
            public string BankCode { get; set; }
            public string EntCode { get; set; }
            public string CurCode { get; set; }
        }

        private class BudgetSummary
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string DeptCode { get; set; }
            public string BCCode { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal AvailableBudget { get; set; }
        }

        private class Department
        {
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
        }

        private class BudgetCategory
        {
            //public string DeptCode { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
        }

        private class DataTemp
        {
            public string BCCode { get; set; }
            public string CCCode { get; set; }
        }

        #endregion
    }
}
