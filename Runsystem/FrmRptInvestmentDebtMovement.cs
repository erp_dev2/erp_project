﻿#region Update
    /*
     *  24/05/2022 [RDA/PRODUCT] New Reporting 
     *  12/07/2022 [SET/PRODUCT] menyesuaikan perhitungan Moving Avg Price
     *                           merubah tampilan data Price & Moving Avg Price (%)
     */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptInvestmentDebtMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string
            mRptStockMovementDocNoSource = string.Empty,
            mDebtInvestmentCtCode = string.Empty;
        private bool
            mIsInventoryShowTotalQty = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovementShowProjectInfo = false,
            mIsStockMovementHeatNumberEnabled = false;

        #endregion

        #region Constructor

        public FrmRptInvestmentDebtMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsInventoryShowTotalQty', 'IsItGrpCodeShow', 'IsShowForeignName', 'IsInvTrnShowItSpec', 'RptStockMovementDocNoSource', ");
            SQL.AppendLine("'DebtInvestmentCtCode' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsInventoryShowTotalQty": mIsInventoryShowTotalQty = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCode = ParValue == "N"; break;
                            case "IsInvTrnShowItSpec": mIsInvTrnShowItSpec = ParValue == "Y"; break;

                            //String
                            case "RptStockMovementDocNoSource": mRptStockMovementDocNoSource = ParValue; break;
                            case "DebtInvestmentCtCode": mDebtInvestmentCtCode = ParValue; break; //harusnya ganti debt dan parvalue nya IVT_002,IVT_003
                        }
                    }
                }
                dr.Close();
            }
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T2.OptDesc As MenuDesc, T1.InvestmentCode As InvestmentDebtCode, T5.PortofolioName As InvestmentName, T3.BankAcNo, T3.BankAcNm, ");
            SQL.AppendLine("T5.Issuer, T6.InvestmentCtCode, T6.InvestmentCtName, T7.OptDesc As InvestmentType, T1.NominalAmt, ");
            SQL.AppendLine("CONCAT(FORMAT(IfNull(T8.UPrice, 0.00), 2), '%') As Price, IfNull(T1.NominalAmt, 0.00) * (IfNull(T8.UPrice, 0.00) * 0.01) As InvestmentCost, T1.MaturityDt, T1.PortofolioId As InvestmentCode, T4.InterestRateAmt, ");
            SQL.AppendLine("T1.CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select A.InvestmentType, A.DocType, A.DocNo, A.DNo, A.DocDt, A.BankAcCode, A.Lot, A.Bin, ");
            SQL.AppendLine("	A.InvestmentCode, A.BatchNo, A.Source, Sum(A.NominalAmt) NominalAmt, Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3, A.MaturityDt, B.PortofolioId, A.CreateDt ");
            SQL.AppendLine("	From TblInvestmentStockMovement A ");
            SQL.AppendLine("	Inner Join TblInvestmentItemDebt B On A.InvestmentCode = B.InvestmentDebtCode ");
            SQL.AppendLine("	Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("	Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("	And FIND_IN_SET(C.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("	Group By A.DocType, A.InvestmentType, A.DocNo, A.DNo, A.BankAcCode, A.Lot, A.BatchNo, A.Source ");
            SQL.AppendLine("	Having Sum(A.NominalAmt)<>0.00 ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Inner Join TblOption T2 On T1.DocType = T2.OptCode And T2.OptCat = 'InvestmentTransType' ");
            SQL.AppendLine("Left Join TblBankAccount T3 On T1.BankAcCode = T3.BankAcCode ");
            SQL.AppendLine("Inner Join TblInvestmentItemDebt T4 On T1.InvestmentCode = T4.InvestmentDebtCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio T5 On T4.PortofolioId = T5.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentCategory T6 On T5.InvestmentCtCode = T6.InvestmentCtCode ");
            SQL.AppendLine("	And FIND_IN_SET(T6.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("Inner Join TblOption T7 On T1.InvestmentType = T7.OptCode And T7.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice T8 On T1.Source = T8.Source And T1.InvestmentCode = T8.InvestmentCode ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5  
                        "Document#",
                        "Date",
                        "Menu Description",
                        "Investment's" + Environment.NewLine + "Debt Code",
                        "Investment Name",

                        //6-10
                        "Investment's" + Environment.NewLine + "Bank Account (RDN)#",
                        "Investment's" + Environment.NewLine + "Bank Account Name",
                        "Issuer",
                        "Category Code",
                        "Category",

                        //11-15
                        "Type",
                        "Nominal Amount",
                        "Price (%)",
                        "Investment Cost",
                        "Acc. Nominal Amount",

                        //16-20
                        "Acc. Investment Cost",
                        "Moving Average" + Environment.NewLine + "Price",
                        "Maturity Date",
                        "Interest Rate",
                        "Investment Code",

                        //21
                        "Create Date",


                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 180, 150, 250, 

                        //6-10
                        180, 200, 250, 0, 130,

                        //11-15
                        150, 150, 130, 130, 130,

                        //16-20
                        150, 150, 150, 150, 150,

                        //21
                        80,

                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 4, 9, 21 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 19 }, 0);
            Grd1.Cols[20].Move(4);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mDebtInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItInvestmentCode.Text, new string[] { "C.PortofolioId", "C.PortofolioName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter) + " Order By T1.InvestmentCode, T7.OptDesc, T1.CreateDt; ",
                new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt", "MenuDesc", "InvestmentDebtCode", "InvestmentName", "BankAcNo",
                        //6-10
                        "BankAcNm", "Issuer", "InvestmentCtCode", "InvestmentCtName", "InvestmentType",
                        //11-15
                        "NominalAmt", "Price", "InvestmentCost", "MaturityDt", "InterestRateAmt",  
                        //16-17
                        "InvestmentCode", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10); //Type
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11); //Nominal Amount
                        //Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12); //Price
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12); //Price
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13); //Investment Cost
                        Grd.Cells[Row, 15].Value = 0m; //Acc. Nominal Amount
                        Grd.Cells[Row, 16].Value = 0m; //Acc. Investment Cost
                        Grd.Cells[Row, 17].Value = 0m; //Moving Average Price
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 14); //Maturity Date
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 15); //Interest Rate
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16); //Investment Code
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17); //Create Date
                    }, true, false, false, false
                );

                ComputeStockAccumulaton(Sm.GetDte(DteDocDt2), mDebtInvestmentCtCode, TxtItInvestmentCode.Text, ref Grd1, 21, 4, 9, 11, 15, 16, 17);
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    var a = Sm.FormatNum(Sm.Round(Sm.GetGrdDec(Grd1, i, 17), 2), 0);
                    Grd1.Cells[i, 17].Value = a+"%";
                }
                Grd1.GroupObject.Add(11);
                Grd1.GroupObject.Add(20);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void ComputeStockAccumulaton(
            string DocDt, string InvestmentCtCode, string InvestmentCode,
            ref iGrid Grd, int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColNominalAmt, int ColInvestmentCost, int ColMovingAvgPrice
            )
        {
            var lacc = PrepDataStockAccumulation(DocDt, InvestmentCtCode, InvestmentCode);
            ProcessAccumulationStock(ref Grd, ref lacc, ColCreateDt, ColInvCode, ColInvCtCode, ColInvType, ColNominalAmt, ColInvestmentCost, ColMovingAvgPrice);
        }

        private List<StockAccumulation> PrepDataStockAccumulation(string DocDt, string InvestmentCtCode, string InvestmentCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockAccumulation>();
            var l2 = new List<StockAccumulation>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, InvestmentCode, new string[] { "C.PortofolioId", "C.PortofolioName" });

            SQL.AppendLine("Select A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType, E.OptDesc,  ");
            SQL.AppendLine("IfNull(A.NominalAmt, 0.00) As NominalAmt, IfNull(A.NominalAmt, 0.00) * (IfNull(D.UPrice, 0.00) * 0.01) As InvestmentCost  ");
            SQL.AppendLine("From TblInvestmentStockMovement A  ");
            SQL.AppendLine("Inner Join TblInvestmentItemDebt B On A.InvestmentCode = B.InvestmentDebtCode  ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source = D.Source ");
            SQL.AppendLine("	And A.InvestmentCode = D.InvestmentCode ");
            SQL.AppendLine("Inner Join TblOption E On A.InvestmentType = E.OptCode And E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            if (DocDt.Length > 0) SQL.AppendLine("And A.DocDt <= @DocDt  ");
            SQL.AppendLine("And FIND_IN_SET(C.InvestmentCtCode, @InvestmentCtCode) ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("	Select Distinct(X1.DocNo) ");
            SQL.AppendLine("	From TblInvestmentStockMovement X1 ");
            SQL.AppendLine("	Where 1=1 ");
            if (DocDt.Length > 0) SQL.AppendLine("	And X1.DocDt <= @DocDt ");
            SQL.AppendLine("	Group By X1.DocType, X1.InvestmentType, X1.DocNo, X1.DNo, X1.DocDt, X1.BankAcCode, X1.Lot, X1.BatchNo, X1.Source   ");
            SQL.AppendLine("	Having Sum(X1.NominalAmt)=0.00 ");
            SQL.AppendLine(") ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter + "Order By A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType;",
                new string[]
                {
                    "CreateDt",
                    "InvestmentCode", "InvestmentCtCode", "InvestmentType", "OptDesc", "NominalAmt",
                    "InvestmentCost",
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockAccumulation()
                        {
                            CreateDt = Convert.ToDecimal(Sm.DrStr(dr, 0)),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentCtCode = Sm.DrStr(dr, 2),
                            InvestmentType = Sm.DrStr(dr, 3),
                            InvestmentTypeName = Sm.DrStr(dr, 4),
                            NominalAmt = Sm.DrDec(dr, 5),
                            InvestmentCost = Sm.DrDec(dr, 6),
                        }
                    );
               }, false
            );

            var lg = l.GroupBy(g => new { CreateDt = g.CreateDt, InvCode = g.InvestmentCode, InvCtCode = g.InvestmentCtCode, InvType = g.InvestmentType, InvTypeNm = g.InvestmentTypeName })
                .Select(group => new
                {
                    CreateDt = group.Key.CreateDt,
                    InvestmentCode = group.Key.InvCode,
                    InvestmentCtCode = group.Key.InvCtCode,
                    InvestmentType = group.Key.InvType,
                    InvestmentTypeName = group.Key.InvTypeNm,
                });

            foreach (var x in lg)
            {
                decimal mNominalAmt = 0m, mInvestmentCost = 0m;
                foreach (var y in l)
                {
                    if (y.CreateDt <= x.CreateDt &&
                        y.InvestmentCode == x.InvestmentCode &&
                        y.InvestmentCtCode == x.InvestmentCtCode &&
                        y.InvestmentType == x.InvestmentType)
                    {
                        mNominalAmt += y.NominalAmt;
                        mInvestmentCost += y.InvestmentCost;
                    }
                }
                l2.Add(
                    new StockAccumulation()
                    {
                        CreateDt2 = x.CreateDt.ToString(),
                        InvestmentCode = x.InvestmentCode,
                        InvestmentCtCode = x.InvestmentCtCode,
                        InvestmentType = x.InvestmentType,
                        InvestmentTypeName = x.InvestmentTypeName,
                        NominalAmt = mNominalAmt,
                        InvestmentCost = mInvestmentCost,
                    }
                );
            }

            return l2;
        }

        private void ProcessAccumulationStock(
            ref iGrid Grd, ref List<StockAccumulation> lacc,
            int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColNominalAmt, int ColInvestmentCost, int ColMovingAvgPrice
            )
        {
            foreach (var x in lacc)
            {
                for (int row = 0; row < Grd.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(Grd, row, 0).Length > 0 &&
                        Sm.GetGrdStr(Grd, row, ColCreateDt) == x.CreateDt2 &&
                        Sm.GetGrdStr(Grd, row, ColInvCode) == x.InvestmentCode &&
                        Sm.GetGrdStr(Grd, row, ColInvCtCode) == x.InvestmentCtCode &&
                        Sm.GetGrdStr(Grd, row, ColInvType) == x.InvestmentTypeName)
                    {
                        Grd.Cells[row, ColNominalAmt].Value = x.NominalAmt;
                        Grd.Cells[row, ColInvestmentCost].Value = x.InvestmentCost;
                        Grd.Cells[row, ColMovingAvgPrice].Value = (x.NominalAmt == 0 ? 0 : ((x.InvestmentCost / x.NominalAmt) * 100));
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #region Class

        private class StockAccumulation
        {
            public decimal CreateDt { get; set; }
            public string CreateDt2 { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentType { get; set; }
            public string InvestmentTypeName { get; set; }
            public decimal NominalAmt { get; set; }
            public decimal InvestmentCost { get; set; }
        }

        #endregion
    }
}
