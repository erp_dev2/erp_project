﻿#region Update
/*
    12/08/2020 [WED/TWC] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestDeductionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestDeduction mFrmParent;
        private string mDeductionType = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestDeductionDlg(FrmVoucherRequestDeduction FrmParent, string DeductionType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDeductionType = DeductionType;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Payrun Code", 
                    "Payrun Name", 
                    "Department" + Environment.NewLine + "Code",
                    "Department",

                    //6
                    "Amount"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 130, 200, 100, 250,

                    //6
                    150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.PayrunCode, T1.PayrunName, T1.DeptCode, T3.DeptName, Sum(T2.Amt) Amt ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct A.PayrunCode, A.PayrunName, A.DeptCode ");
            SQL.AppendLine("    From TblPayrun A ");
            SQL.AppendLine("    Inner Join TblPayrollProcess1 B On A.PayrunCode = B.PayrunCode ");
            SQL.AppendLine("        And A.Status = 'C' ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("        And A.DeptCode Is Not Null ");
                SQL.AppendLine("        And Exists ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select 1 ");
                SQL.AppendLine("            From TblGroupDepartment ");
                SQL.AppendLine("            Where DeptCode = A.DeptCode ");
                SQL.AppendLine("            And GrpCode In ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select GrpCode ");
                SQL.AppendLine("                From TblUser ");
                SQL.AppendLine("                Where UserCode = @UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("    Inner Join TblPayrollProcessAD C On A.PayrunCode = C.PayrunCode ");
            SQL.AppendLine("        And B.EmpCode = C.EmpCode ");
            SQL.AppendLine("        And C.ADCode = @ADCode ");
            SQL.AppendLine("    Where Not Find_In_set(A.PayrunCode, @GetSelectedPayrun) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblPayrollProcessAD T2 On T1.PayrunCode = T2.PayrunCode ");
            SQL.AppendLine("    And T2.ADCode = @ADCode ");
            SQL.AppendLine("    And Concat(T2.PayrunCode, T2.ADCode) Not In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct Concat(X2.PayrunCode, X1.DeductionType) ");
            SQL.AppendLine("        From TblVoucherRequestDeductionHdr X1 ");
            SQL.AppendLine("        inner Join TblVoucherRequestDeductionDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("            And X1.Status In ('O', 'A') ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.DeductionType = @ADCode ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblDepartment T3 On T1.DeptCode = T3.DeptCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPayrunCode.Text, new string[] { "T1.PayrunCode", "T1.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T1.DeptCode", true);

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@ADCode", mDeductionType);
                Sm.CmParam<String>(ref cm, "@GetSelectedPayrun", mFrmParent.GetSelectedPayrun());

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Group By T1.PayrunCode, T1.PayrunName, T1.DeptCode, T3.DeptName Order By T1.PayrunName; ",
                    new string[] 
                    { 
                        //0
                        "PayrunCode",

                        //1-4
                        "PayrunName", "DeptCode", "DeptName", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 6);

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 3].Value = 0;
                    }
                }
                mFrmParent.ComputeAmt();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            //if (mFrmParent.mIsJournalUseDuplicateCOA) return false;

            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Grid Methods

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtPayrunCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void ChkPayrunCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

    }
}
