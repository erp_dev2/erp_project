﻿#region Update
/*
    02/12/2017 [TKG] New Reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptExchangeHolidays : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptExchangeHolidays(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dt, A.EmpCode, B.EmpName, B.EmpCodeOld, C.SiteName, D.DeptName, E.PosName ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Inner Join TblSite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition E On B.PosCode=E.PosCode ");
            SQL.AppendLine("Where A.ExchangeHolidayInd='Y' ");
            SQL.AppendLine("And Left(A.Dt, 6)=@YrMth ");
            SQL.AppendLine("Order By A.Dt, C.SiteName, D.DeptName, B.EmpName;");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Site", 
                        
                        //6-7
                        "Department",
                        "Position"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 200, 100, 180, 
                        
                        //6-7
                        180, 180
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL,
                    new string[]
                    { 
                        //0
                        "Dt", 

                        //1-5
                        "EmpCode", "EmpName", "EmpCodeOld", "SiteName", "DeptName", 

                        //6
                        "PosName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    }, true, false, false, false
                );
   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion
    }
}
