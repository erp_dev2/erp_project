﻿#region Update
/*
    06/03/2020 [WED/KBN] new apps
    01/04/2020 [WED/KBN] perbaikan performance
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPrintRptFicoSetting2 mFrmParent;
        private int mGrdCount = 5, mFirstCol = -1;

        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting2Dlg(FrmPrintRptFicoSetting2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = BtnRefresh.Visible = BtnPrint.Visible = false;
            SetGrd();
            GetCOA();
            SetAdditionalColumn();
            SetAmount();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = mGrdCount;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "COA#",

                    //1-4
                    "Account",
                    "Alias",
                    "Parent",
                    "Level"
                },
                new int[]
                {
                    //0
                    200,
                    //1-4
                    300, 200, 0, 0
                }
            );

            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
        }

        #endregion

        #region Additional Methods

        private void GetCOA()
        {
            int Row = 0;
            foreach(var x in mFrmParent.l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = x.AcNo;
                Grd1.Cells[Row, 1].Value = x.AcDesc;
                Grd1.Cells[Row, 2].Value = x.Alias;
                Grd1.Cells[Row, 3].Value = x.Parent;
                Grd1.Cells[Row, 4].Value = x.Level.ToString();

                Row += 1;
            }
        }

        private void SetAdditionalColumn()
        {
            var LatestColumn = Grd1.Cols.Count - 1;

            foreach(var x in mFrmParent.l2)
            {
                LatestColumn += 1;
                if (mFirstCol == -1) mFirstCol = LatestColumn;
                Grd1.Cols.Count += 1;
                Grd1.Header.Cells[0, LatestColumn].Value = x.ColumnName;
                Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Cols[LatestColumn].Width = 250;
                Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                Grd1.Cols[LatestColumn].CellStyle.ReadOnly = iGBool.True;
            }
        }

        private void SetAmount()
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                int Cols = mGrdCount;
                foreach(var k in mFrmParent.l2)
                {
                    Grd1.Cells[i, Cols].Value = 0m;
                    Cols += 1;
                }

                foreach (var x in mFrmParent.l3.Where(p => p.AcNo == Sm.GetGrdStr(Grd1, i, 0)))
                {
                    Grd1.Cells[i, ((Int32.Parse(x.No) + mGrdCount) - 1)].Value = x.Amt;
                    break;
                }
            }
        }

        #endregion

        #endregion

    }
}
