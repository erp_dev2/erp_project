﻿#region Update
/*
    21/12/2020 [IBL/SRN] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestConsignmentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestConsignment mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestConsignmentDlg(FrmVoucherRequestConsignment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Vendor";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.VdCode, A.VdCodeInternal, A.VdName ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Inner Join " + mFrmParent.mDBNamePOS + " .Tbl_Consignment_Billing B ");
            SQL.AppendLine("    On A.VdCodeInternal = B.Vendor_Code ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new String[]
                {
                     //0
                    "No.",

                    //1-5
                    "Vendor Code",
                    "Vendor" + Environment.NewLine + "Local Code",
                    "Vendor Name",

                }, new int[]
                {
                    //0
                    60,

                    //1-5
                    80, 80, 150,
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVdCode.Text, new string[] { "A.VdCode", "A.VdName", "A.VdCodeInternal" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By VdName ",
                        new string[] 
                        { 
                            //0
                            "VdCode", "VdCodeInternal", "VdName"
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.mVdCode = Sm.GetGrdText(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.mVdCodeInternal = Sm.GetGrdText(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtVdCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);

                this.Close();
            }

        }

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor");
        }

        #endregion

        #region Grid Control Event
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }
        #endregion
        #endregion
    }
}
