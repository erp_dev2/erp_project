﻿#region update

/*
 * 17/11/2021 [HAR] BUG item bisa diketik
 * 24/11/2021 [SET/PHT] BUG : Pada menu CPP, ketika insert dan loop milih item, UoM nya tidak sesuai dengan master item
 * 22/12/2021 [VIN/PHT] BUG :Item yang dapat dipilih item yang aktif sajo
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyProcurementPlanDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCompanyProcurementPlan mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmCompanyProcurementPlanDlg(FrmCompanyProcurementPlan FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.PurchaseUoMCode, A.InventoryUoMCode ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Where A.ActInd ='Y'  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "", 
                        "Item's Code", 
                        "Item's Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-3
                        20, 100, 125
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
        }

        override protected void HideInfoInGrd()
        {
            
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1 = 1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());

                Sm.FilterStr(ref Filter, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.ItCode",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1)/* && !IsItCodeAlreadyChosen(Row)*/)
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
