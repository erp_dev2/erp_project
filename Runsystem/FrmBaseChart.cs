﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Windows.Forms.DataVisualization.Charting;
using TenTec.Windows.iGridLib;
using Sm = RunSystem.StdMtd;

namespace RunSystem
{
    public partial class FrmBaseChart : Form
    {
        #region Field

        string ChartTitle1;
        string ChartTitle2;
        iGrid ChartGrd;
        bool ChartSeriesByColumn;
        int[] ChartXSeriesCol; 
        int ChartXCol;
        int ChartYCol;
        string CancelLoadText = "Cancel Loading";
        string ReloadText = "ReLoad Data";
        
        string ChartAxisXFormat = string.Empty;
        string ChartAxisYFormat = string.Empty;
        string SeriesValueFormat = string.Empty;
        string ToolTipFormat = "#SER\n#VALX : #VALY";

        #endregion

        #region Constructor

        public FrmBaseChart(string ParentFormName, string FilterString, iGrid Grd, bool SeriesByColumn, int[] XSeriesCol, 
            int XCol, int YCol, string AxisXFormat, string AxisYFormat, string LabelPointFormat)
        {
            // ** ParentFormName
            //      Menu Caller Title

            // ** FilterString
            //      Chart Title Caption - Based on data filtering

            // ** Grd
            //      Data Grid to generate chart

            // ** SeriesByColumn
            //      Is Series based on column ( value = false if it is a single series

            // ** XSeriesCol
            //       If SeriesByColumn = true then this parameter is to list which column will act as series

            // ** XCol
            //      Column number for X Value

            // ** YCol
            //      Column number for Y ValueX

            // ** AxisXFormat
            //      Blank : No Format
            //      d : Short Date e.g : 23/12/2015
            //      M : Month Day e.g : 23 December
            //      Y : Month Year e.g : December 2015
            //      ddd,d : Week Day eg : wed 
            //      D : Long Date
            //      G : General Date/Time

            // ** AxisYFormat, LabelPointFormat
            //      Blank : No Format
            //      N : Numeric
            //      N0 : Numeric Rounded
            //      C : Currency
            //      C0 : Currency Rounded
            //      E : Scientific
            //      E0 : Scientific  Rounded
            //      P : Percent
            //      P0 : Percent Rounded

            ChartGrd = Grd;

            ChartGrd.GroupObject.Clear();
            ChartGrd.Group();

            ChartSeriesByColumn = SeriesByColumn;
            ChartXSeriesCol = XSeriesCol;
            ChartXCol = XCol;
            ChartYCol = YCol;
            ChartTitle1 = ParentFormName;
            ChartTitle2 = FilterString;
            ChartAxisXFormat = AxisXFormat;
            ChartAxisYFormat = AxisYFormat;
            SeriesValueFormat = LabelPointFormat;

            InitializeComponent();

            chart1.Titles[0].Text = ChartTitle2;
            chart1.Titles[0].Visible = true;

            chart1.Titles[1].Text = ChartTitle1;
            chart1.Titles[1].Visible = false;

            // Enable range selection and zooming UI by default
            chart1.ChartAreas["Default"].CursorX.IsUserEnabled = true;
            chart1.ChartAreas["Default"].CursorX.IsUserSelectionEnabled = true;
            chart1.ChartAreas["Default"].CursorY.IsUserEnabled = true;
            chart1.ChartAreas["Default"].CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas["Default"].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas["Default"].AxisY.ScaleView.Zoomable = true;

            chart1.ChartAreas["Default"].AxisX.ScrollBar.IsPositionedInside = true;
            chart1.ChartAreas["Default"].AxisY.ScrollBar.IsPositionedInside = true;

            chart1.ChartAreas["Default"].AxisX.LabelStyle.Format = ChartAxisXFormat;
            chart1.ChartAreas["Default"].AxisY.LabelStyle.Format = ChartAxisYFormat;

            chart1.Series.Clear();

        }

        #endregion

        #region Methods

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            // Calculate title string position
            Rectangle titlePosition = new Rectangle(ev.MarginBounds.X, ev.MarginBounds.Y, ev.MarginBounds.Width, ev.MarginBounds.Height);
            Font fontTitle = new Font("Tahoma", 16, FontStyle.Bold);

            SizeF titleSize = ev.Graphics.MeasureString(ChartTitle1, fontTitle);
            titlePosition.Height = (int)titleSize.Height;

            // Draw charts title
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            ev.Graphics.DrawString(ChartTitle1, fontTitle, Brushes.Black, titlePosition, format);

            // Calculate first chart position rectangle
            Rectangle chartPosition = new Rectangle(ev.MarginBounds.X, titlePosition.Bottom, chart1.Size.Width, chart1.Size.Height);

            // Align first chart position on the page
            //float chartWidthScale = ((float)ev.MarginBounds.Width / 2f) / ((float)chartPosition.Width);
            float chartWidthScale = ((float)ev.MarginBounds.Width) / ((float)chartPosition.Width);
            float chartHeightScale = ((float)ev.MarginBounds.Height) / ((float)chartPosition.Height);
            chartPosition.Width = (int)(chartPosition.Width * Math.Min(chartWidthScale, chartHeightScale));
            chartPosition.Height = (int)(chartPosition.Height * Math.Min(chartWidthScale, chartHeightScale));

            // Draw first chart on the printer graphics
            chart1.Printing.PrintPaint(ev.Graphics, chartPosition);


            // Adjust position rectangle for the second chart
            //chartPosition.X += chartPosition.Width;

            // Draw second chart on the printer graphics
            //chart2.Printing.PrintPaint(ev.Graphics, chartPosition);
        }

        #endregion


        #region Event
 

        private void FrmBaseChart_Load(object sender, EventArgs e)
        {
            Width = MdiParent.Width - 100;
            Height = MdiParent.Height - 100;
            Left = 50;
            Top = 10;

            Text = ChartTitle1;

            chkLegend.Checked = true;
            chkDataPoint.Checked = false;
            btnCancelLoad.Text = ReloadText;

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                // Set new print document with custom page printing event handler
                

                chart1.Printing.PrintDocument = new PrintDocument();
                int margin = (int)(50f);
                chart1.Printing.PrintDocument.DefaultPageSettings.Margins =
                    new System.Drawing.Printing.Margins(margin, margin, margin, margin);

                chart1.Printing.PrintDocument.DefaultPageSettings.Landscape = true;
                chart1.Printing.PrintDocument.PrintPage += new PrintPageEventHandler(pd_PrintPage);

                // Print preview chart
           
                chart1.Printing.PrintPreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Chart Control for .NET Framework", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            // Create a new save file dialog
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            // Sets the current file name filter string, which determines 
            // the choices that appear in the "Save as file type" or 
            // "Files of type" box in the dialog box.
            saveFileDialog1.Filter = "Bitmap (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|EMF-Plus (*.emf)|*.emf|EMF-Dual (*.emf)|*.emf|EMF (*.emf)|*.emf|PNG (*.png)|*.png|GIF (*.gif)|*.gif|TIFF (*.tif)|*.tif";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            // Set image file format
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                chart1.Titles[1].Visible = true;

                ChartImageFormat format = ChartImageFormat.Bmp;

                if (saveFileDialog1.FileName.EndsWith("bmp"))
                {
                    format = ChartImageFormat.Bmp;
                }
                else if (saveFileDialog1.FileName.EndsWith("jpg"))
                {
                    format = ChartImageFormat.Jpeg;
                }
                else if (saveFileDialog1.FileName.EndsWith("emf") && saveFileDialog1.FilterIndex == 3)
                {
                    format = ChartImageFormat.EmfDual;
                }
                else if (saveFileDialog1.FileName.EndsWith("emf") && saveFileDialog1.FilterIndex == 4)
                {
                    format = ChartImageFormat.EmfPlus;
                }
                else if (saveFileDialog1.FileName.EndsWith("emf"))
                {
                    format = ChartImageFormat.Emf;
                }
                else if (saveFileDialog1.FileName.EndsWith("gif"))
                {
                    format = ChartImageFormat.Gif;
                }
                else if (saveFileDialog1.FileName.EndsWith("png"))
                {
                    format = ChartImageFormat.Png;
                }
                else if (saveFileDialog1.FileName.EndsWith("tif"))
                {
                    format = ChartImageFormat.Tiff;
                }

                // Save image
                chart1.SaveImage(saveFileDialog1.FileName, format);
            }
            chart1.Titles[1].Visible = false;

        }

        private void chkLegend_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLegend.Checked) chart1.Legends[0].Enabled = true;
            else chart1.Legends[0].Enabled = false;
        }

        private void chkDataPoint_CheckedChanged(object sender, EventArgs e)
        {
            for (int iSeries = 0; iSeries <= chart1.Series.Count - 1; iSeries++)
                if (chkDataPoint.Checked)
                    chart1.Series[iSeries].IsValueShownAsLabel = true;
                else
                    chart1.Series[iSeries].IsValueShownAsLabel = false;
        }

        private void btnCancelLoad_Click(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void btnResetZoom_Click(object sender, EventArgs e)
        {
            chart1.ChartAreas["Default"].AxisX.ScaleView.ZoomReset(0);
            chart1.ChartAreas["Default"].AxisY.ScaleView.ZoomReset(0);
        }
        private void UpdateLabel(int state)
        {
            this.resultLabel.Text = "Wait, Loading " + state + "%.";
            this.resultLabel.Update();
        }
        public void ReloadData()
        {
            chart1.Visible = false;
            int ProgressVal = 0;
            Font SeriesFont = new Font("Tahoma", 7, FontStyle.Regular);

            if (ChartSeriesByColumn)
            {
                for (int Col = 0; Col <= ChartXSeriesCol.Length - 1; Col++)
                {
                    string SeriesName = ChartGrd.Cols[ChartXSeriesCol[Col]].Text.ToString();
                    SeriesName = (SeriesName == string.Empty ? "Empty" : SeriesName);
                    chart1.Series.Add(SeriesName);
                    chart1.Series[SeriesName].Font = SeriesFont;
                    chart1.Series[SeriesName].LabelFormat = SeriesValueFormat;
                    chart1.Series[SeriesName].ToolTip = ToolTipFormat + "{" + SeriesValueFormat + "}";
                    chart1.Series[SeriesName].BackGradientStyle = GradientStyle.VerticalCenter;
                    chart1.Series[SeriesName].BorderColor = Color.Black;
                    chart1.Series[SeriesName].SmartLabelStyle.Enabled = true;
                    chart1.Series[SeriesName].IsValueShownAsLabel = false;
                    chart1.Series[SeriesName].Points.Clear();

                    for (int Row = 0; Row <= ChartGrd.Rows.Count - 1; Row++)
                    {
                        if (iGSubtotalManager.IsSubtotal(ChartGrd.Rows[Row]) == false)
                        {
                            Decimal ChartYValue = Sm.GetGrdDec(ChartGrd, Row, ChartXSeriesCol[Col]);
                            string ChartXValue = Sm.GetGrdStr(ChartGrd, Row, ChartXCol);

                            chart1.Series[SeriesName].Points.AddXY(ChartXValue, ChartYValue);
                        }

                        ProgressVal = (((Row * Col) + 1) * 100) / (ChartXSeriesCol.Length * ChartGrd.Rows.Count);
                        UpdateLabel(ProgressVal);
                    }
                }
            }
            else
            {
                for (int Row = 0; Row <= ChartGrd.Rows.Count - 1; Row++)
                {
                    if (iGSubtotalManager.IsSubtotal(ChartGrd.Rows[Row]) == false)
                    {
                        string SeriesName = Sm.GetGrdStr(ChartGrd, Row, ChartXSeriesCol[0]);

                        SeriesName = (SeriesName == string.Empty ? "Empty" : SeriesName);

                        Decimal ChartYValue = Sm.GetGrdDec(ChartGrd, Row, ChartYCol);
                        string ChartXValue = Sm.GetGrdStr(ChartGrd, Row, ChartXCol);

                        if (chart1.Series.IndexOf(SeriesName) == -1)
                        {
                            chart1.Series.Add(SeriesName);
                            chart1.Series[SeriesName].Font = SeriesFont;
                            chart1.Series[SeriesName].LabelFormat = SeriesValueFormat;
                            chart1.Series[SeriesName].ToolTip = ToolTipFormat + "{" + SeriesValueFormat + "}";
                            chart1.Series[SeriesName].BackGradientStyle = GradientStyle.VerticalCenter;
                            chart1.Series[SeriesName].BorderColor = Color.Black;
                            chart1.Series[SeriesName].SmartLabelStyle.Enabled = true;
                            chart1.Series[SeriesName].IsValueShownAsLabel = false;
                            chart1.Series[SeriesName].Points.Clear();
                            chart1.Series[SeriesName].Points.AddXY(ChartXValue, ChartYValue);
                        }
                        else
                            chart1.Series[SeriesName].Points.AddXY(ChartXValue, ChartYValue);

                        ProgressVal = ((Row + 1) * 100) / ChartGrd.Rows.Count;
                        UpdateLabel(ProgressVal);
                    }
                }
            }
            this.resultLabel.Text = "Loading Chart is done.";
            chart1.Visible = true;

        }

        #endregion


    } 
}
