﻿#region Update
/*
    04/01/2022 [ISD/SIER] dialog baru untuk budget category
    03/02/2022 [DITA/SIER] Cost center pada filter di dialog CAS belum filter by group user cost center
    24/01/2023 [BRI/SIER] list of Budget category tervalidasi berdasarkan Remaining of budget category dan Department pada group setting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlementDlg5 : RunSystem.FrmBase2
    {
        #region Field

        private FrmCashAdvanceSettlement mFrmParent;
        private string mSQL = string.Empty, mVCDocNo = string.Empty, mYr = string.Empty;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmCashAdvanceSettlementDlg5(FrmCashAdvanceSettlement FrmParent, int Row, string VCDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = Row;
            mVCDocNo = VCDocNo;

        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Budget Category";

                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //mFrmParent.SetLueCCtCode(ref LueCCtCode);
                Sl.SetLueBCCode(ref LueBCCode, string.Empty);
                if (mFrmParent.mIsSystemUseCostCenter) Sl.SetLueCCCode(ref LueCCCode, string.Empty, "Y");
                else Sl.SetLueCCCode(ref LueCCCode);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Budget Category Code",
                        "Budget Category Name",
                        "Cost Category Code", 
                        "Cost Category Name",
                        "COA",

                        //6-9
                        "COA Description",
                        "Cost Center",
                        "Budget",
                        "Used Budget"

                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 300, 120, 300, 130, 

                        //6-9
                        250, 200, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mFrmParent.mIsCASListofBudgetCategoryFilteredbyRemainingBudget)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            mYr = Sm.GetValue("Select Left(DocDt, 4) From TblVoucherHdr Where DocNo=@Param", mVCDocNo);

            if (mFrmParent.mIsCASListofBudgetCategoryFilteredbyRemainingBudget)
            {
                if (mFrmParent.mIsBudget2YearlyFormat)
                {
                    SQL.AppendLine("Select C.BCCode, C.BCName, D.CCtCode, CONCAT(D.CCtName, ' (', IfNull(F.CCName, '-'), ')' ) CCtName, D.AcNo, E.AcDesc, F.CCName, A.Amt2, ");
                    SQL.AppendLine(Sm.SelectUsedBudget());
                    SQL.AppendLine("As Amt3 ");
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("    Select A.Yr, A.DeptCode, A.BCCode, ");
                    SQL.AppendLine("    Sum(A.Amt1) Amt1, Sum(A.Amt2) Amt2 ");
                    SQL.AppendLine("    From TblBudgetSummary A ");
                    SQL.AppendLine("    Where 1=1 ");
                    SQL.AppendLine("    Group By A.Yr, A.DeptCode, A.BCCode ");
                    SQL.AppendLine(") A ");
                    SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode=C.BCCode ");
                    SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCode ");
                    SQL.AppendLine("Inner Join TblCOA E On D.AcNo = E.AcNo ");
                    SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode = F.CCCode ");
                    SQL.AppendLine(Sm.ComputeUsedBudget(2, string.Empty, mYr, string.Empty, string.Empty));
                    SQL.AppendLine("Where A.Amt2 > 0 ");
                    SQL.AppendLine("And (A.Amt2 - " + Sm.SelectUsedBudget() + ") > 0 ");
                    if (mFrmParent.mIsSystemUseCostCenter)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                        SQL.AppendLine("    Where CCCode=D.CCCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mFrmParent.mIsCASListofBudgetCategoryFilteredbyDepartmentGroup)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mFrmParent.mIsCostCategoryFilteredInCashAdvanceSettlement)
                        SQL.AppendLine("And F.DeptCode = @DeptCode ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("Order By A.Yr, B.DeptName, C.BCName; ");
                }
                else
                {
                    SQL.AppendLine("Select C.BCCode, C.BCName, D.CCtCode, CONCAT(D.CCtName, ' (', IfNull(F.CCName, '-'), ')' ) CCtName, D.AcNo, E.AcDesc, F.CCName, A.Amt2, ");
                    SQL.AppendLine(Sm.SelectUsedBudget());
                    SQL.AppendLine("As Amt3 ");
                    SQL.AppendLine("From TblBudgetSummary A ");
                    SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("        Where DeptCode=B.DeptCode ");
                    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("Inner Join TblBudgetCategory C On A.BCCode = C.BCCode ");
                    SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCode ");
                    SQL.AppendLine("Inner Join TblCOA E On D.AcNo = E.AcNo ");
                    SQL.AppendLine("Inner Join TblCostCenter F On D.CCCode = F.CCCode ");
                    SQL.AppendLine(Sm.ComputeUsedBudget(2, string.Empty, mYr, string.Empty, string.Empty));
                    SQL.AppendLine("Where A.Amt2 > 0 ");
                    SQL.AppendLine("And (A.Amt2 - " + Sm.SelectUsedBudget() + ") > 0 ");
                    if (mFrmParent.mIsSystemUseCostCenter)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                        SQL.AppendLine("    Where CCCode=D.CCCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mFrmParent.mIsCASListofBudgetCategoryFilteredbyDepartmentGroup)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mFrmParent.mIsCostCategoryFilteredInCashAdvanceSettlement)
                        SQL.AppendLine("And F.DeptCode = @DeptCode ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("Order By A.Yr, A.Mth, B.DeptName, C.BCName; ");
                }
            }
            else
            {
                SQL.AppendLine("Select A.BCCode, A.BCName, B.CCtCode, CONCAT(B.CCtName, ' (', IfNull(D.CCName, '-'), ')' ) CCtName, B.AcNo, C.AcDesc, D.CCName, ");
                SQL.AppendLine("0 As Amt2, 0 As Amt3 ");
                SQL.AppendLine("From TblBudgetCategory A ");
                SQL.AppendLine("Inner Join TblCostCategory B On A.CCtCode = B.CCtCode ");
                SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
                SQL.AppendLine("Inner Join TblCostCenter D On B.CCCode = D.CCCode ");
                SQL.AppendLine("Where 0 = 0");
                if (mFrmParent.mIsSystemUseCostCenter)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                    SQL.AppendLine("    Where CCCode=B.CCCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsCASListofBudgetCategoryFilteredbyDepartmentGroup)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }

                if (mFrmParent.mIsCostCategoryFilteredInCashAdvanceSettlement)
                    SQL.AppendLine("And D.DeptCode = @DeptCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Order By A.BCCode; ");
            }

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "E.AcNo", "E.AcDesc" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBCCode), "C.BCCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "D.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[] 
                        { 
                        
                            //0
                            "BCCode",
                            
                            //1-5
                            "BCName", "CCtCode", "CCtName", "AcNo", "AcDesc",

                            //6-8
                            "CCName", "Amt2", "Amt3"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 31, Grd1, Row, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 32, Grd1, Row, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 3, Grd1, Row, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 4, Grd1, Row, 4);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 21, Grd1, Row, 5);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 22, Grd1, Row, 6);

                //Sm.CopyGrdValue(mFrmParent.Grd2, mCurRow, 1, Grd1, Row, 2);
                mFrmParent.ComputeAmt1();
                mFrmParent.SetCostCategoryInfo();
                mFrmParent.SetVoucherInfo();
                mFrmParent.ComputeAmt2();
                mFrmParent.ClearItem();
                this.Close();
            }
        }
        

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event 

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA");
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue2(Sl.SetLueBCCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Budget Category");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsSystemUseCostCenter)  Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, "Y");
            else Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion
    }
    
}
