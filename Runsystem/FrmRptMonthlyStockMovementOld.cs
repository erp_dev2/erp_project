﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem   
{
    public partial class FrmRptMonthlyStockMovementOld : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            DocDt = string.Empty;
        private bool mIsFilterByItCt = false;
        int mNumberOfInventoryUomCode = 1;
   
        #endregion

        #region Constructor

        public FrmRptMonthlyStockMovementOld(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
	        SQL.AppendLine("Select ");
            SQL.AppendLine("T1.WhsCode, T1.DocDate, T2.WhsName, T1.ItCode, T3.ItName, T1.BatchNo, T3.InventoryUomCode,  T3.InventoryUomCode2, T3.InventoryUomCode3, ");
	        SQL.AppendLine("T1.Qty00_1, T1.Qty00_2, T1.Qty00_3 ");

            for (int i = 1; i <= 16; i++) 
            {
                if (i >= 1 && i < 10)
                    for (int j = 1; j <= 3; j++)
                        SQL.AppendLine(", Sum(Case When DocType='0" + i + "' Then Qty Else 0 End) As Qty0" + i + "_" + j + " ");
                    
                else 
                    for (int j = 1; j <= 3; j++)
                        SQL.AppendLine(", Sum(Case When DocType='" + i + "' Then Qty Else 0 End) As Qty" + i + "_" + j + " ");
            }

	        SQL.AppendLine("From (  ");
            SQL.AppendLine("Select A.WhsCode, A.ItCode, A.BatchNo, A.DocDate, ");
	        SQL.AppendLine("IfNull(B.Qty,0) As Qty00_1, IfNull(B.Qty2,0) As Qty00_2, IfNull(B.Qty3,0) As Qty00_3, ");
	        SQL.AppendLine("C.DocType, IfNull(C.Qty,0) As Qty, IfNull(C.Qty2,0) As Qty2, IfNull(C.Qty3,0) As Qty3 ");
	        SQL.AppendLine("From ( ");
	        SQL.AppendLine("Select Distinct WhsCode, ItCode, BatchNo, DocDt As DocDate ");
	        SQL.AppendLine("From TblStockMovement ");
	        SQL.AppendLine("Where Left(DocDt, 6)<=@DocDt ");
	        SQL.AppendLine(") A ");
	        SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("Select WhsCode, ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
	        SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where Left(DocDt, 6)<@DocDt  ");
	        SQL.AppendLine("Group By WhsCode, ItCode  ");
	        SQL.AppendLine("Having Sum(Qty)<>0 ");
	        SQL.AppendLine(") B On A.WhsCode=B.WhsCode And A.ItCode=B.ItCode ");
	        SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("Select WhsCode, ItCode, DocType, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
	        SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where Left(DocDt, 6)=@DocDt ");
	        SQL.AppendLine("Group By WhsCode, ItCode, DocType  ");
	        SQL.AppendLine("Having Sum(Qty)<>0 ");
            SQL.AppendLine(") C On A.WhsCode=C.WhsCode And A.ItCode=C.ItCode ");
	        SQL.AppendLine(") T1 ");
	        SQL.AppendLine("Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode ");
	        SQL.AppendLine("Inner Join TblItem T3 On T1.ItCode=T3.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T3.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
	        SQL.AppendLine("Group By  ");
	        SQL.AppendLine("T1.WhsCode, T2.WhsName, T1.ItCode, T3.ItName,  ");
	        SQL.AppendLine("T3.InventoryUomCode,  T3.InventoryUomCode2, T3.InventoryUomCode3, ");
	        SQL.AppendLine("T1.Qty00_1, T1.Qty00_2, T1.Qty00_3 ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where ");
	        SQL.AppendLine("(T.Qty00_1<>0 ");
            for (int i = 1; i <= 16; i++)
            {   if(i<10)
                    SQL.AppendLine(" Or T.Qty0" + i + "_1<>0 Or T.Qty0" + i + "_2 <> 0 Or T.Qty0" + i + "_3<>0  ");
                else if(i>=10 && i<=15)
                    SQL.AppendLine(" Or T.Qty" + i + "_1<>0 Or T.Qty" + i + "_2 <> 0 Or T.Qty" + i + "_3<>0 ");
                else if(i==16)
                    SQL.AppendLine(" Or T.Qty" + i + "_1<>0 Or T.Qty" + i + "_2 <> 0 Or T.Qty" + i + "_3<>0) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 3;
            Grd1.Cols.Count = 62;
            Grd1.FrozenArea.ColCount = 4;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 3;

            Grd1.Header.Cells[0, 1].Value = "Warehouse";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 3;

            Grd1.Header.Cells[0, 2].Value = "Item's Code";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 3;

            Grd1.Header.Cells[0, 3].Value = "Item's Name";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 3;

            Grd1.Header.Cells[0, 4].Value = "Batch#";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 3;

            Grd1.Header.Cells[2, 5].Value = "Inventory";
            Grd1.Header.Cells[2, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[2, 5].SpanCols = 3;

            Grd1.Header.Cells[0, 5].Value = "UoM";
            Grd1.Header.Cells[0, 5].SpanRows = 2;
            Grd1.Header.Cells[0, 6].Value = "UoM";
            Grd1.Header.Cells[0, 6].SpanRows = 2;
            Grd1.Header.Cells[0, 7].Value = "UoM";
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Header.Cells[2, 8].Value = "Initial Stock ";
            Grd1.Header.Cells[2, 8].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[2, 8].SpanCols = 3;
            
            Grd1.Header.Cells[0, 8].Value = "Quantity";
            Grd1.Header.Cells[0, 8].SpanRows = 2;
            Grd1.Header.Cells[0, 9].Value = "Quantity";
            Grd1.Header.Cells[0, 9].SpanRows = 2;
            Grd1.Header.Cells[0, 10].Value = "Quantity";
            Grd1.Header.Cells[0, 10].SpanRows = 2;


            Grd1.Header.Cells[2, 59].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[2, 59].SpanCols = 3;
            Grd1.Header.Cells[2, 59].Value = "Last Stock";

            Grd1.Header.Cells[0, 59].Value = "Quantity";
            Grd1.Header.Cells[0, 59].SpanRows = 2;
            Grd1.Header.Cells[0, 60].Value = "Quantity";
            Grd1.Header.Cells[0, 60].SpanRows = 2;
            Grd1.Header.Cells[0, 61].Value = "Quantity";
            Grd1.Header.Cells[0, 61].SpanRows = 2;

            Grd1.Header.Cells[2, 11].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[2, 11].SpanCols = 3;
            Grd1.Header.Cells[2, 11].Value = "Stock Ajustment";

            Grd1.Header.Cells[0, 11].Value = "Quantity";
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Header.Cells[0, 12].Value = "Quantity";
            Grd1.Header.Cells[0, 12].SpanRows = 2;
            Grd1.Header.Cells[0, 13].Value = "Quantity";
            Grd1.Header.Cells[0, 13].SpanRows = 2;

            Grd1.Header.Cells[2, 14].Value = "IN";
            Grd1.Header.Cells[2, 38].Value = "OUT";


                Grd1.Header.Cells[2, 14].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[2, 14].SpanCols = 24;

                Grd1.Header.Cells[1, 14].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Vendor";
                Grd1.Header.Cells[1, 14 + 3].Value = "Initial " + Environment.NewLine + " Stock  " ;    
                Grd1.Header.Cells[1, 14 + 6].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Department";
                Grd1.Header.Cells[1, 14 + 9].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Customer";
                Grd1.Header.Cells[1, 14 + 12].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Other Warehouse";
                Grd1.Header.Cells[1, 14 + 15].Value = "Stock Mutation " + Environment.NewLine + "(To)";
                Grd1.Header.Cells[1, 14 + 18].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Vendor Without PO";
                Grd1.Header.Cells[1, 14 + 21].Value = "Receiving Item " + Environment.NewLine + "From " + Environment.NewLine + "Other Warehouse Without DO";

                Grd1.Header.Cells[2, 38].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[2, 38].SpanCols = 21;

                Grd1.Header.Cells[1, 38].Value = "Returning Item " + Environment.NewLine + "To Vendor";
                Grd1.Header.Cells[1, 38 + 3].Value = "DO To " + Environment.NewLine + "Department";
                Grd1.Header.Cells[1, 38 + 6].Value = "DO To " + Environment.NewLine + "Customer";
                Grd1.Header.Cells[1, 38 + 9].Value = "DO To " + Environment.NewLine + "Other Warehouse";
                Grd1.Header.Cells[1, 38 + 12].Value = "Stock Mutation " + Environment.NewLine + "(From)";
                Grd1.Header.Cells[1, 38 + 15].Value = "DO To " + Environment.NewLine + "Department " + Environment.NewLine + "Without DO Request";
                Grd1.Header.Cells[1, 38 + 18].Value = "Transfer To " + Environment.NewLine + "Other Warehouse" + Environment.NewLine + "Without DO";


                for (int Col = 0; Col < 15; Col++)
                {
                    Grd1.Header.Cells[1, 14 + (Col * 3)].TextAlign = iGContentAlignment.TopCenter;
                    Grd1.Header.Cells[1, 14 + (Col * 3)].SpanCols = 3;
                }

                for (int Col = 1; Col < 16; Col++)
                {
                    if (Col == 1)
                    {
                        Grd1.Header.Cells[0, (Col * 14)].Value = "Quantity";
                        Grd1.Header.Cells[0, (Col * 14) + 1].Value = "Quantity";
                        Grd1.Header.Cells[0, (Col * 14) + 2].Value = "Quantity";
                    }
                    if (Col > 1) {
                        Grd1.Header.Cells[0, (Col * 3)+11].Value = "Quantity";
                        Grd1.Header.Cells[0, (Col * 3)+12].Value = "Quantity";
                        Grd1.Header.Cells[0, (Col * 3)+13].Value = "Quantity";
                    }
                }
            Sm.GrdFormatDec(Grd1,
                new int[] { 

                    8, 9, 10, 11,
                    12, 13, 14, 15,16,
                    17, 18, 19, 20, 21,
                    22, 23, 24, 25, 26,
                    27, 28, 29, 30, 31,
                    32, 33, 34, 35, 36,
                    37, 38, 39, 40, 41, 
                    42, 43, 44, 45, 46, 
                    47, 48, 49, 50, 51, 
                    52, 53, 54, 55, 56,
                    57, 58, 59, 60, 61

                }, 0);

            Sm.GrdColInvisible(Grd1, new int[] { 
                5, 6, 7,
                9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60,
                10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 44, 49, 52, 55, 58, 61
            }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, true);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 
                    9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60,
                    10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61 
                }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
            DocDt = string.Concat(Year,Month );
            
            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty) {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " And 0=0 ";
                    var cml = new List<MySqlCommand>();
                    decimal Total = 0, Total2 = 0, Total3 = 0, Total4 = 0, Total5 = 0, Total6 = 0;
                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "WhsCode", true);
                    Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDate");
                    Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                    Sm.FilterStr(ref Filter, ref cm, TxtBatch.Text, new string[] { "BatchNo" });


                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By WhsName, ItCode;",
                            new string[]
                            {
                                //0
                                "WhsName",
                                //1-5
                                "ItCode","ItName", "BatchNo", "InventoryUOMCode","InventoryUOMCode2",
                                //6-10, 
                                "InventoryUOMCode3","Qty00_1","Qty00_2","Qty00_3","Qty01_1",
                                //11-15
                                "Qty01_2","Qty01_3","Qty02_1","Qty02_2","Qty02_3",
                                //16-20
                                "Qty03_1","Qty03_2","Qty03_3","Qty04_1","Qty04_2",
                                //21-25
                                "Qty04_3","Qty05_1","Qty05_2","Qty05_3","Qty06_1",
                                //26-30
                                "Qty06_2","Qty06_3","Qty07_1","Qty07_2","Qty07_3",
                                //31-35
                                "Qty08_1","Qty08_2","Qty08_3","Qty09_1","Qty09_2",
                                //36-40
                                "Qty09_3","Qty10_1","Qty10_2","Qty10_3","Qty11_1",
                                //41-45
                                "Qty11_2","Qty11_3","Qty12_1","Qty12_2","Qty12_3",
                                //46-50
                                "Qty13_1","Qty13_2","Qty13_3","Qty14_1","Qty14_2",
                                //51-55
                                "Qty14_3","Qty15_1","Qty15_2","Qty15_3","Qty16_1",
                                //56-57
                                "Qty16_2","Qty16_3"
                            },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 29);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 31);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 32);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 35);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 36);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 37);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 38);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 39);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 40);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 41);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 42);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 43);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 44);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 45);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 46);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 47);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 48);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 49);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 50);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 51);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 52);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 53);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 54);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 55);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 56);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 57);
                                
                                Total = Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 11) + Sm.GetGrdDec(Grd1, Row, 14) + Sm.GetGrdDec(Grd1, Row, 17) + Sm.GetGrdDec(Grd1, Row, 20) + Sm.GetGrdDec(Grd1, Row, 23) + Sm.GetGrdDec(Grd1, Row, 26) + Sm.GetGrdDec(Grd1, Row, 29) + Sm.GetGrdDec(Grd1, Row, 32) + Sm.GetGrdDec(Grd1, Row, 35);
                                Total2 = Sm.GetGrdDec(Grd1, Row, 9) + Sm.GetGrdDec(Grd1, Row, 12) + Sm.GetGrdDec(Grd1, Row, 15) + Sm.GetGrdDec(Grd1, Row, 18) + Sm.GetGrdDec(Grd1, Row, 21) + Sm.GetGrdDec(Grd1, Row, 24) + Sm.GetGrdDec(Grd1, Row, 27) + Sm.GetGrdDec(Grd1, Row, 30) + Sm.GetGrdDec(Grd1, Row, 33) + Sm.GetGrdDec(Grd1, Row, 36);
                                Total3 = Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 13) + Sm.GetGrdDec(Grd1, Row, 15) + Sm.GetGrdDec(Grd1, Row, 19) + Sm.GetGrdDec(Grd1, Row, 22) + Sm.GetGrdDec(Grd1, Row, 25) + Sm.GetGrdDec(Grd1, Row, 28) + Sm.GetGrdDec(Grd1, Row, 31) + Sm.GetGrdDec(Grd1, Row, 34) + Sm.GetGrdDec(Grd1, Row, 38);
                                Total4 = Sm.GetGrdDec(Grd1, Row, 41) + Sm.GetGrdDec(Grd1, Row, 44) + Sm.GetGrdDec(Grd1, Row, 47) + Sm.GetGrdDec(Grd1, Row, 50) + Sm.GetGrdDec(Grd1, Row, 53) + Sm.GetGrdDec(Grd1, Row, 56);
                                Total5 = Sm.GetGrdDec(Grd1, Row, 42) + Sm.GetGrdDec(Grd1, Row, 45) + Sm.GetGrdDec(Grd1, Row, 48) + Sm.GetGrdDec(Grd1, Row, 51) + Sm.GetGrdDec(Grd1, Row, 54) + Sm.GetGrdDec(Grd1, Row, 57);
                                Total6 = Sm.GetGrdDec(Grd1, Row, 43) + Sm.GetGrdDec(Grd1, Row, 46) + Sm.GetGrdDec(Grd1, Row, 49) + Sm.GetGrdDec(Grd1, Row, 52) + Sm.GetGrdDec(Grd1, Row, 55) + Sm.GetGrdDec(Grd1, Row, 58);
                                Grd1.Cells[Row, 59].Value = Total + Total4;
                                Grd1.Cells[Row, 60].Value = Total2 + Total5;
                                Grd1.Cells[Row, 61].Value = Total3 + Total6;
                            }, true, true, false, true
                        );


                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                    8, 9, 10, 11,
                    12, 13, 14, 15,16,
                    17, 18, 19, 20, 21,
                    22, 23, 24, 25, 26,
                    27, 28, 29, 30, 31,
                    32, 33, 34, 35, 36,
                    37, 38, 39, 40, 41, 
                    42, 43, 44, 45, 46, 
                    47, 48, 49, 50, 51, 
                    52, 53, 54, 55, 56,
                    57, 58, 59, 60, 61
                });
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.SetGrdProperty(Grd1, true);
                    Sm.FocusGrd(Grd1, 0, 2);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkBatch_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtBatch_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        
        #endregion

       
        #endregion

    }
}
