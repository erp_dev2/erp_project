﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemGroupFormula : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmItemGroupFormulaFind FrmFind;
        internal int mStateInd = 0;
        internal string
            mItGroupFormula = string.Empty;

        #endregion

        #region Constructor

        public FrmItemGroupFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Item Group";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueItGrpCode(ref LueItGrpCode, "");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Code",
                        
                        //1-2
                        "Description",
                        "Value",
                        
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-2
                        100, 200
                     
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueItGrpCode
                    }, true);
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueItGrpCode
                    }, false);
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               LueItGrpCode
            });
            mStateInd = 0;
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemGroupFormulaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateInd = 1;
                SetFormControl(mState.Insert);
                Sl.SetLueItGrpCode(ref LueItGrpCode, "");
                InsertRow(mItGroupFormula);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
           
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueItGrpCode,"")) return;
            mStateInd = 2;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)

        {

            try
            {
                if (mStateInd == 1)
                    InsertData(sender, e);
                else if (mStateInd == 2)
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }


        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert

        private void InsertData(object sender, EventArgs e)
        {
            if ((Sm.StdMsgYN("Save", "") == DialogResult.No) || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveItemGroup(Sm.GetLue(LueItGrpCode), Row));

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueItGrpCode));
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueItGrpCode, "Item Group") ||
                IsGrdEmpty()||
                IsGrdValueNotValid()||
                IsDataExists();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Detail Item Group.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (
                        Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Detail Item Group is empty.")
                        )
                        return true;
                }
            }

            return false;
        }
        private bool IsDataExists()
        {
            if (Sm.IsDataExist("Select ItGrpCode From TblItemGroupFormulaSfc Where ItGrpCode = '" + Sm.GetLue(LueItGrpCode) + "';"))
            {
                Sm.StdMsg(mMsgType.Warning, "This data ('" + LueItGrpCode.Text+ "') is already exists.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItemGroup(string ItGroupCode, int Row)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into Tblitemgroupformulasfc(ItGrpCode, OptCode, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ItGrpCode, @OptCode, @Value, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItGrpCode", ItGroupCode);
            Sm.CmParam<String>(ref cm, "@OptCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Value", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

           
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(UpdateItemGroup(Sm.GetLue(LueItGrpCode), Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueItGrpCode));
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueItGrpCode, "Item Group")||
               IsGrdValueNotValid();
        }

        private MySqlCommand UpdateItemGroup(string ItGrpCode, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Update Tblitemgroupformulasfc ");
            SQLDtl2.AppendLine("Set Value = @Value, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQLDtl2.AppendLine("Where ItGrpCode = @ItGrpCode And OptCode = @OptCode; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };
            Sm.CmParam<String>(ref cm, "@ItGrpCode", ItGrpCode);
            Sm.CmParam<String>(ref cm, "@OptCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Value", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region ShowData

        public void ShowData(string ItGrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItGrpFormulaHdr(ItGrpCode);
                ShowItGroupSFC(ItGrpCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowItGrpFormulaHdr(string ItGrpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItGrpCode ");
            SQL.AppendLine("From TblItemGroupFormulaSFC ");
            SQL.AppendLine("Where ItGrpCode=@ItGrpCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItGrpCode", ItGrpCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "ItGrpCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueItGrpCode, Sm.DrStr(dr, c[0]));
                    }, true
                );
        }

        private void ShowItGroupSFC(string ItGroup)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItGrpCode", ItGroup);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.OptCode, B.OptDesc, A.Value From TblItemgroupformulasfc A ");
                SQL.AppendLine("Inner Join TblOption B On A.OptCode = B.OptCode And B.OptCat='ItemGroupFormula' ");
                SQL.AppendLine("Where A.ItGrpCode=@ItGrpCode;");
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "OptCode", "OptDesc", "Value"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    }, false, false, true, false
                     );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mItGroupFormula = Sm.GetParameter("ItemGroupFormula");
        }

        private void InsertRow(string SourceCount)
        {
           
            var SQL1 = new StringBuilder();
            var cm1 = new MySqlCommand();

            SQL1.AppendLine("Select OptCode, OptDesc From TblOption Where OptCat = 'ItemGroupFormula'; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQL1.ToString(),
                new string[] 
                { 
                    //0
                    "OptCode",
                    
                    //1
                    "OptDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                }, false, false, true, false
            );

            if (SourceCount.Length > 0 && SourceCount != "0")
                Grd1.Rows.Count = int.Parse(SourceCount);
            else
                Grd1.Rows.Count = 1;
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue2(Sl.SetLueItGrpCode), string.Empty);
        }

        #endregion
    }
}
