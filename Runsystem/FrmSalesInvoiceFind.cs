﻿#region Update
/* 
    28/08/2017 [TKG] Tambah filter berdasarkan local document
    15/01/2018 [TKG] tambah total aount, tax, downpayment
    14/08/2018 [WED] tambah kolom dan filter Customer PO# dari SO
    29/11/2019 [DITA/IMS] tambah kolom project code, project name
    10/07/2020 [IBL/KSM] Menyambungkan dengan DOCt Based On Sales Contract
    02/10/2020 [TKG/MSI] tambah item's code dan item's name
    06/10/2020 [TKG/MSI] ubah query spy lbh cepat dan tambah SO/SO Contract#
    18/02/2021 [VIN/KSM] tambah kolom customer category berdasrkan parameter IsCustomerComboShowCategory
    24/02/2021 [WED/SIER] tambah kolom customer category berdasrkan parameter IsCustomerComboShowCategory atau IsCustomerComboBasedOnCategory
 *  08/03/2021 [ICA/SRN] tambah kolom DocNo Do to Customer Based on DR 
 *  10/06/2021 [VIN/IMS] bug Show data 
 *  17/01/2023 [VIN/IOK] bug Show data downpayment 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoiceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesInvoice mFrmParent;
        private string mSQL = string.Empty; 
        internal string mMenuCode = "";

        #endregion

        #region Constructor

        public FrmSalesInvoiceFind(FrmSalesInvoice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueCtCode(ref LueCtCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, B.CtName, ");
            //SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(C.CtPONoDR, D.CtPONoPL) CtPONo, ");
            //SQL.AppendLine("G.ItCode, G.ItName, ");
            //if (mFrmParent.mIsBOMShowSpecifications)
            //    SQL.AppendLine("E.ProjectCode, E.ProjectName, ");
            //else
            //    SQL.AppendLine("Null As ProjectCode, Null As ProjectName, ");
            //SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            //SQL.AppendLine("From TblSalesInvoiceHdr A ");
            //SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T0.DocNo, Group_Concat(Distinct IfNull(T5.CtPONo, '') Separator ', ') As CtPONoDR ");
            //SQL.AppendLine("    From TblSalesInvoiceHdr T0 ");
            //SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T1 On T0.DocNo = T1.DocNo And (T0.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCtDocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblDRDtl T4 On T2.DRDocNo = T4.DocNo And T3.DRDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblSOHdr T5 On T4.SODocNo = T5.DocNo ");
            //SQL.AppendLine("    Group By T0.DocNo ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("    Select T0.DocNo, Group_Concat(Distinct IfNull(T5.PONo, '') Separator ', ') As CtPONoDR ");
            //SQL.AppendLine("    From TblSalesInvoiceHdr T0 ");
            //SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T1 On T0.DocNo = T1.DocNo And (T0.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCtDocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblDRDtl T4 On T2.DRDocNo = T4.DocNo And T3.DRDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblSOContractHdr T5 On T4.SODocNo = T5.DocNo ");
            //SQL.AppendLine("    Group By T0.DocNo ");
            //SQL.AppendLine(") C On A.DocNo = C.DocNo ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T0.DocNo, Group_Concat(Distinct IfNull(T5.CtPONo, '') Separator ', ') As CtPONoPL ");
            //SQL.AppendLine("    From TblSalesInvoiceHdr T0 ");
            //SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T1 On T0.DocNo = T1.DocNo And (T0.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCtDocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl3 T3 On T1.DOCtDocNo = T3.DocNo And T1.DOCtDNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblPLDtl T4 On T2.PLDocNo = T4.DocNo And T3.PLDNo = T4.DNo ");
            //SQL.AppendLine("    Inner Join TblSOHdr T5 On T4.SODocNo = T5.DocNo ");
            //SQL.AppendLine("    Group By T0.DocNo ");
            //SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            //if (mFrmParent.mIsBOMShowSpecifications)
            //{
            //    SQL.AppendLine("Left Join ");
            //    SQL.AppendLine("( ");
            //    SQL.AppendLine("Select T0.DocNo ,Group_Concat( Distinct IFNULL(T8.ProjectCode, T5.ProjectCode2)) ProjectCode, Group_Concat( Distinct IFNULL(T8.ProjectName, T7.ProjectName)) ProjectName ");
            //    SQL.AppendLine("From TblSalesInvoiceHdr T0 ");
            //    SQL.AppendLine("Inner Join TblSalesInvoiceDtl T1 On T0.DocNo = T1.DocNo ");
            //    SQL.AppendLine("Inner Join TblDOCt2Hdr T2 On T1.DOCtDocNo = T2.DocNo ");
            //    SQL.AppendLine("Inner Join TblDRDtl T4 On T2.DRDocNo = T4.DocNo ");
            //    SQL.AppendLine("Inner Join TblSOContractHdr T5 ON T4.SODocNo = T5.DocNo ");
            //    SQL.AppendLine("Inner Join TblBOQHdr T6 ON T5.BOQDocNo = T6.DocNo ");
            //    SQL.AppendLine("Inner Join TblLOPHdr T7 ON T6.LOPDocNo = T7.DocNo ");
            //    SQL.AppendLine("Left Join TblProjectGroup T8 ON T7.PGCode = T8.PGCode ");
            //    SQL.AppendLine("Group By T0.DocNo ");
            //    SQL.AppendLine(") E ON E.DocNo = A.DocNo ");
            //}
            //SQL.AppendLine("Left Join TblSalesInvoiceDtl F On A.DocNo=F.DocNo ");
            //SQL.AppendLine("Left Join TblItem G On F.ItCode=G.ItCode ");
            //SQL.AppendLine("Where A.SODocNo Is Null ");
            //SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("And Exists( ");
            //SQL.AppendLine("    Select 1 ");
            //SQL.AppendLine("    From TblSalesInvoiceHdr T1, TblSalesInvoiceDtl T2 ");
            //SQL.AppendLine("    Where T1.DocNo=A.DocNo ");
            //SQL.AppendLine("    And T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And T1.SODocNo Is Null ");
            //SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    And T2.DocType<>'3' ");
            //SQL.AppendLine("    ) ");

            #endregion

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, D.CtName, F.CtCtName, ");
            SQL.AppendLine("A.CurCode, A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, C.DOCt2DocNo, C.SODocNo, C.CtPONo, ");
            SQL.AppendLine("E.ItCode, E.ItName, C.ProjectCode, C.ProjectName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblSalesInvoiceHdr A");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo And B.DocType<>'3' ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, T3.DocNo as DOCt2DocNo, T6.DocNo As SODocNo, T6.CtPONo, ");
            SQL.AppendLine("    Null As ProjectCode, Null As ProjectName ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo And T2.DocType<>'3' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr T6 On T5.SODocNo=T6.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.SODocNo Is Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, T3.DocNo as DOCt2DocNo, T6.DocNo As SODocNo, T6.PONo As CtPONo, ");
            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("    T9.ProjectCode, T9.ProjectName ");
            else
                SQL.AppendLine("    Null As ProjectCode, Null As ProjectName ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo And T2.DocType<>'3' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblDRDtl T5 On T3.DRDocNo=T5.DocNo And T4.DRDNo=T5.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr T6 On T5.SODocNo=T6.DocNo ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("    Left Join TblBOQHdr T7 On T6.BOQDocNo=T7.DocNo ");
                SQL.AppendLine("    Left Join TblLOPHdr T8 On T7.LOPDocNo=T8.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T9 On T8.PGCode=T9.PGCode ");
            }
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.SODocNo Is Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, T3.DocNo as DOCt2DocNo, T6.DocNo As SODocNo, T6.CtPONo, ");
            SQL.AppendLine("    Null As ProjectCode, Null As ProjectName ");
            SQL.AppendLine("    From TblSalesInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo And T2.DocType<>'3' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 T4 On T2.DOCtDocNo=T4.DocNo And T2.DOCtDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblPLDtl T5 On T3.PLDocNo=T5.DocNo And T4.PLDNo=T5.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr T6 On T5.SODocNo=T6.DocNo ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And T1.SODocNo Is Null ");
            SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode=D.CtCode ");
            SQL.AppendLine("Left Join TblItem E On B.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblCustomerCategory F On D.CtCtCode=F.CtCtCode ");
            SQL.AppendLine("Where A.SODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel", 
                    "Local Document",
                    "Customer",
                    
                    //6-10
                    "Customer Category",
                    "Currency", 
                    "Total Amount",
                    "Total Tax",
                    "Downpayment",

                    //11-15
                    "Invoice Amount",
                    "",
                    "DO Based DR#",
                    "SO#",
                    "Customer PO#",
                    
                    //16-20
                    "Item's Code",
                    "Item's Name",
                    "Project's Code",
                    "Project's Name",
                    "Created By",
 
                    //21-22
                    "Created Date",
                    "Created Time", 
                    "Last Updated By",
                    "Last Updated Date",
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 50, 150, 200, 
                    
                    //6-10
                    200, 60, 120, 120, 120,  
                    
                    //11-15
                    120, 20, 150, 150, 250, 
                    
                    //16-20
                    130, 300, 130, 200, 130, 
                    
                    //21-22
                    130, 130, 130, 130, 130
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 12 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, false);
            if (!mFrmParent.mIsCustomerComboShowCategory && !mFrmParent.mIsCustomerComboBasedOnCategory) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked) Filter = " And A.CancelInd='N' ";
                
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "C.SODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtCtPONo.Text, "C.CtPONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "CtName", "CtCtName",  
                            
                            //6-10
                            "CurCode", "TotalAmt", "TotalTax", "Downpayment", "Amt", 
                            
                            //11-15
                            "DOCt2DocNo", "SODocNo", "CtPONo", "ItCode", "ItName", 
                            
                            //16-20
                            "ProjectCode", "ProjectName", "CreateBy", "CreateDt", "LastUpBy", 

                            //21
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 21);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f1 = new FrmDOCt2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f1.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f1 = new FrmDOCt2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f1.ShowDialog();
            }
        }

        #endregion


        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void TxtCtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer's PO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion


        #endregion
    }
}
