﻿namespace RunSystem
{
    partial class FrmBOQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBOQ));
            this.BtnCtContactPersonName = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtQtMth = new DevExpress.XtraEditors.TextEdit();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteQtStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueRingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LuePPN = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkBankGuaranteeInd = new DevExpress.XtraEditors.CheckEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtSubTotalRBPS = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtPolicy = new DevExpress.XtraEditors.TextEdit();
            this.ChkTaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtIndirectCost = new DevExpress.XtraEditors.TextEdit();
            this.TxtDirectCost = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtGrandTotal = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.BtnCtReplace = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnCtReplace2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCtReplace = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTax = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtSubTotal = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkPrintSignatureInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LueAgingAP = new DevExpress.XtraEditors.LookUpEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueResourceType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueResourceName = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnLOPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.MeeApprovalRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TcBOQ = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankGuaranteeInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotalRBPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPolicy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResourceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResourceName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).BeginInit();
            this.TcBOQ.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(849, 0);
            this.panel1.Size = new System.Drawing.Size(70, 504);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeApprovalRemark);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.LueProcessInd);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.BtnLOPDocNo);
            this.panel2.Controls.Add(this.TxtLOPDocNo);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.BtnCtContactPersonName);
            this.panel2.Controls.Add(this.LueCtContactPersonName);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.LueSPCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueRingCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.LuePtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DteQtStartDt);
            this.panel2.Controls.Add(this.TxtQtMth);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Size = new System.Drawing.Size(849, 304);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TcBOQ);
            this.panel3.Location = new System.Drawing.Point(0, 304);
            this.panel3.Size = new System.Drawing.Size(849, 200);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.TcBOQ, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 482);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(849, 200);
            this.Grd1.TabIndex = 75;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCtContactPersonName
            // 
            this.BtnCtContactPersonName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtContactPersonName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtContactPersonName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtContactPersonName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtContactPersonName.Appearance.Options.UseBackColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseFont = true;
            this.BtnCtContactPersonName.Appearance.Options.UseForeColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseTextOptions = true;
            this.BtnCtContactPersonName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtContactPersonName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtContactPersonName.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtContactPersonName.Image")));
            this.BtnCtContactPersonName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtContactPersonName.Location = new System.Drawing.Point(388, 174);
            this.BtnCtContactPersonName.Name = "BtnCtContactPersonName";
            this.BtnCtContactPersonName.Size = new System.Drawing.Size(24, 19);
            this.BtnCtContactPersonName.TabIndex = 30;
            this.BtnCtContactPersonName.ToolTip = "Add Contact Person";
            this.BtnCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtContactPersonName.ToolTipTitle = "Run System";
            this.BtnCtContactPersonName.Click += new System.EventHandler(this.BtnCtContactPersonName_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(276, 259);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 14);
            this.label11.TabIndex = 39;
            this.label11.Text = "month(s)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(128, 260);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 14);
            this.label7.TabIndex = 37;
            this.label7.Text = "For";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtMth
            // 
            this.TxtQtMth.EnterMoveNextControl = true;
            this.TxtQtMth.Location = new System.Drawing.Point(156, 257);
            this.TxtQtMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtMth.Name = "TxtQtMth";
            this.TxtQtMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtQtMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtMth.Properties.MaxLength = 6;
            this.TxtQtMth.Size = new System.Drawing.Size(117, 20);
            this.TxtQtMth.TabIndex = 38;
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(156, 173);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 30;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 300;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(230, 20);
            this.LueCtContactPersonName.TabIndex = 29;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtContactPersonName.EditValueChanged += new System.EventHandler(this.LueCtContactPersonName_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(77, 218);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 33;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(156, 215);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 300;
            this.LueSPCode.Size = new System.Drawing.Size(230, 20);
            this.LueSPCode.TabIndex = 34;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged);
            // 
            // DteQtStartDt
            // 
            this.DteQtStartDt.EditValue = null;
            this.DteQtStartDt.EnterMoveNextControl = true;
            this.DteQtStartDt.Location = new System.Drawing.Point(156, 236);
            this.DteQtStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtStartDt.Name = "DteQtStartDt";
            this.DteQtStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.MaxLength = 8;
            this.DteQtStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtStartDt.Size = new System.Drawing.Size(117, 20);
            this.DteQtStartDt.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(59, 239);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 14);
            this.label8.TabIndex = 35;
            this.label8.Text = "BOQ Start Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(37, 197);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 14);
            this.label6.TabIndex = 31;
            this.label6.Text = "Customer Ring Area";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueRingCode
            // 
            this.LueRingCode.EnterMoveNextControl = true;
            this.LueRingCode.Location = new System.Drawing.Point(156, 194);
            this.LueRingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRingCode.Name = "LueRingCode";
            this.LueRingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.Appearance.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRingCode.Properties.DropDownRows = 30;
            this.LueRingCode.Properties.NullText = "[Empty]";
            this.LueRingCode.Properties.PopupWidth = 300;
            this.LueRingCode.Size = new System.Drawing.Size(230, 20);
            this.LueRingCode.TabIndex = 32;
            this.LueRingCode.ToolTip = "F4 : Show/hide list";
            this.LueRingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRingCode.EditValueChanged += new System.EventHandler(this.LueRingCode_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(5, 176);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 28;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(156, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(79, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(156, 25);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(93, 155);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 26;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(156, 152);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 27;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(156, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(119, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.LuePPN);
            this.panel5.Controls.Add(this.ChkBankGuaranteeInd);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.TxtPercentage);
            this.panel5.Controls.Add(this.TxtSubTotalRBPS);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtPolicy);
            this.panel5.Controls.Add(this.ChkTaxInd);
            this.panel5.Controls.Add(this.TxtIndirectCost);
            this.panel5.Controls.Add(this.TxtDirectCost);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.TxtGrandTotal);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.BtnCtReplace);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.BtnCtReplace2);
            this.panel5.Controls.Add(this.TxtCtReplace);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.TxtTax);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.TxtSubTotal);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.ChkPrintSignatureInd);
            this.panel5.Controls.Add(this.LueCurCode);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.LueAgingAP);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.TxtCreditLimit);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(426, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(423, 304);
            this.panel5.TabIndex = 42;
            // 
            // LuePPN
            // 
            this.LuePPN.EnterMoveNextControl = true;
            this.LuePPN.Location = new System.Drawing.Point(215, 256);
            this.LuePPN.Margin = new System.Windows.Forms.Padding(5);
            this.LuePPN.Name = "LuePPN";
            this.LuePPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.Appearance.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePPN.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePPN.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePPN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePPN.Properties.DropDownRows = 30;
            this.LuePPN.Properties.NullText = "[Empty]";
            this.LuePPN.Properties.PopupWidth = 300;
            this.LuePPN.Size = new System.Drawing.Size(68, 20);
            this.LuePPN.TabIndex = 72;
            this.LuePPN.ToolTip = "F4 : Show/hide list";
            this.LuePPN.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePPN.EditValueChanged += new System.EventHandler(this.LuePPN_EditValueChanged);
            // 
            // ChkBankGuaranteeInd
            // 
            this.ChkBankGuaranteeInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkBankGuaranteeInd.Location = new System.Drawing.Point(164, 274);
            this.ChkBankGuaranteeInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkBankGuaranteeInd.Name = "ChkBankGuaranteeInd";
            this.ChkBankGuaranteeInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkBankGuaranteeInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBankGuaranteeInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkBankGuaranteeInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkBankGuaranteeInd.Properties.Appearance.Options.UseFont = true;
            this.ChkBankGuaranteeInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkBankGuaranteeInd.Properties.Caption = "Bank Guarantee";
            this.ChkBankGuaranteeInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBankGuaranteeInd.Size = new System.Drawing.Size(146, 22);
            this.ChkBankGuaranteeInd.TabIndex = 74;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(393, 235);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 14);
            this.label25.TabIndex = 70;
            this.label25.Text = "%";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPercentage
            // 
            this.TxtPercentage.EnterMoveNextControl = true;
            this.TxtPercentage.Location = new System.Drawing.Point(331, 232);
            this.TxtPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPercentage.Name = "TxtPercentage";
            this.TxtPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPercentage.Properties.MaxLength = 18;
            this.TxtPercentage.Properties.ReadOnly = true;
            this.TxtPercentage.Size = new System.Drawing.Size(59, 20);
            this.TxtPercentage.TabIndex = 69;
            // 
            // TxtSubTotalRBPS
            // 
            this.TxtSubTotalRBPS.EnterMoveNextControl = true;
            this.TxtSubTotalRBPS.Location = new System.Drawing.Point(166, 172);
            this.TxtSubTotalRBPS.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotalRBPS.Name = "TxtSubTotalRBPS";
            this.TxtSubTotalRBPS.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotalRBPS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotalRBPS.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotalRBPS.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotalRBPS.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotalRBPS.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotalRBPS.Properties.MaxLength = 18;
            this.TxtSubTotalRBPS.Properties.ReadOnly = true;
            this.TxtSubTotalRBPS.Size = new System.Drawing.Size(164, 20);
            this.TxtSubTotalRBPS.TabIndex = 62;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(94, 175);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 14);
            this.label23.TabIndex = 61;
            this.label23.Text = "Total RBPS";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPolicy
            // 
            this.TxtPolicy.EnterMoveNextControl = true;
            this.TxtPolicy.Location = new System.Drawing.Point(166, 151);
            this.TxtPolicy.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPolicy.Name = "TxtPolicy";
            this.TxtPolicy.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPolicy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPolicy.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPolicy.Properties.Appearance.Options.UseFont = true;
            this.TxtPolicy.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPolicy.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPolicy.Properties.MaxLength = 18;
            this.TxtPolicy.Size = new System.Drawing.Size(230, 20);
            this.TxtPolicy.TabIndex = 60;
            this.TxtPolicy.Validated += new System.EventHandler(this.TxtPolicy_Validated);
            // 
            // ChkTaxInd
            // 
            this.ChkTaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInd.Location = new System.Drawing.Point(164, 255);
            this.ChkTaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInd.Name = "ChkTaxInd";
            this.ChkTaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInd.Properties.Caption = "Tax";
            this.ChkTaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInd.Size = new System.Drawing.Size(52, 22);
            this.ChkTaxInd.TabIndex = 71;
            this.ChkTaxInd.CheckedChanged += new System.EventHandler(this.ChkTaxInd_CheckedChanged);
            // 
            // TxtIndirectCost
            // 
            this.TxtIndirectCost.EnterMoveNextControl = true;
            this.TxtIndirectCost.Location = new System.Drawing.Point(166, 130);
            this.TxtIndirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIndirectCost.Name = "TxtIndirectCost";
            this.TxtIndirectCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIndirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIndirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtIndirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtIndirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtIndirectCost.Properties.MaxLength = 18;
            this.TxtIndirectCost.Size = new System.Drawing.Size(230, 20);
            this.TxtIndirectCost.TabIndex = 58;
            this.TxtIndirectCost.Validated += new System.EventHandler(this.TxtIndirectCost_Validated);
            // 
            // TxtDirectCost
            // 
            this.TxtDirectCost.EnterMoveNextControl = true;
            this.TxtDirectCost.Location = new System.Drawing.Point(166, 109);
            this.TxtDirectCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDirectCost.Name = "TxtDirectCost";
            this.TxtDirectCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDirectCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDirectCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseFont = true;
            this.TxtDirectCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDirectCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDirectCost.Properties.MaxLength = 18;
            this.TxtDirectCost.Size = new System.Drawing.Size(230, 20);
            this.TxtDirectCost.TabIndex = 56;
            this.TxtDirectCost.Validated += new System.EventHandler(this.TxtDirectCost_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(59, 154);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(102, 14);
            this.label22.TabIndex = 59;
            this.label22.Text = "Management Risk";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(84, 133);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 14);
            this.label21.TabIndex = 57;
            this.label21.Text = "Indirect Cost";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.EnterMoveNextControl = true;
            this.TxtGrandTotal.Location = new System.Drawing.Point(166, 232);
            this.TxtGrandTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrandTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGrandTotal.Properties.MaxLength = 18;
            this.TxtGrandTotal.Properties.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(164, 20);
            this.TxtGrandTotal.TabIndex = 68;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(94, 112);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 14);
            this.label20.TabIndex = 55;
            this.label20.Text = "Direct Cost";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtReplace
            // 
            this.BtnCtReplace.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace.Appearance.Options.UseFont = true;
            this.BtnCtReplace.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace.Image")));
            this.BtnCtReplace.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace.Location = new System.Drawing.Point(366, 69);
            this.BtnCtReplace.Name = "BtnCtReplace";
            this.BtnCtReplace.Size = new System.Drawing.Size(24, 18);
            this.BtnCtReplace.TabIndex = 51;
            this.BtnCtReplace.ToolTip = "Find Replacement Quotation ";
            this.BtnCtReplace.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace.ToolTipTitle = "Run System";
            this.BtnCtReplace.Click += new System.EventHandler(this.BtnCtReplace_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(90, 234);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 14);
            this.label17.TabIndex = 67;
            this.label17.Text = "Grand Total";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtReplace2
            // 
            this.BtnCtReplace2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace2.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace2.Appearance.Options.UseFont = true;
            this.BtnCtReplace2.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace2.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace2.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace2.Image")));
            this.BtnCtReplace2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace2.Location = new System.Drawing.Point(390, 70);
            this.BtnCtReplace2.Name = "BtnCtReplace2";
            this.BtnCtReplace2.Size = new System.Drawing.Size(24, 17);
            this.BtnCtReplace2.TabIndex = 52;
            this.BtnCtReplace2.ToolTip = "Show Replacement Quotation";
            this.BtnCtReplace2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace2.ToolTipTitle = "Run System";
            this.BtnCtReplace2.Click += new System.EventHandler(this.BtnCtReplace2_Click);
            // 
            // TxtCtReplace
            // 
            this.TxtCtReplace.EnterMoveNextControl = true;
            this.TxtCtReplace.Location = new System.Drawing.Point(166, 67);
            this.TxtCtReplace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtReplace.Name = "TxtCtReplace";
            this.TxtCtReplace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtReplace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtReplace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtReplace.Properties.Appearance.Options.UseFont = true;
            this.TxtCtReplace.Properties.MaxLength = 16;
            this.TxtCtReplace.Size = new System.Drawing.Size(198, 20);
            this.TxtCtReplace.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(16, 70);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 14);
            this.label4.TabIndex = 49;
            this.label4.Text = "Replacement\'s Quotation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTax
            // 
            this.TxtTax.EnterMoveNextControl = true;
            this.TxtTax.Location = new System.Drawing.Point(166, 212);
            this.TxtTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTax.Properties.MaxLength = 18;
            this.TxtTax.Properties.ReadOnly = true;
            this.TxtTax.Size = new System.Drawing.Size(164, 20);
            this.TxtTax.TabIndex = 66;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(134, 214);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 14);
            this.label14.TabIndex = 65;
            this.label14.Text = "Tax";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubTotal
            // 
            this.TxtSubTotal.EnterMoveNextControl = true;
            this.TxtSubTotal.Location = new System.Drawing.Point(166, 192);
            this.TxtSubTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubTotal.Name = "TxtSubTotal";
            this.TxtSubTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSubTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtSubTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSubTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSubTotal.Properties.MaxLength = 18;
            this.TxtSubTotal.Properties.ReadOnly = true;
            this.TxtSubTotal.Size = new System.Drawing.Size(164, 20);
            this.TxtSubTotal.TabIndex = 64;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(97, 195);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 14);
            this.label13.TabIndex = 63;
            this.label13.Text = "Total BOQ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(114, 91);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 53;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(166, 88);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // ChkPrintSignatureInd
            // 
            this.ChkPrintSignatureInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPrintSignatureInd.Location = new System.Drawing.Point(292, 255);
            this.ChkPrintSignatureInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPrintSignatureInd.Name = "ChkPrintSignatureInd";
            this.ChkPrintSignatureInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPrintSignatureInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPrintSignatureInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPrintSignatureInd.Properties.Caption = "Print Signature";
            this.ChkPrintSignatureInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPrintSignatureInd.Size = new System.Drawing.Size(117, 22);
            this.ChkPrintSignatureInd.TabIndex = 73;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(166, 25);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 300;
            this.LueCurCode.Size = new System.Drawing.Size(164, 20);
            this.LueCurCode.TabIndex = 46;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(93, 48);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 14);
            this.label19.TabIndex = 47;
            this.label19.Text = "Credit Limit";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(106, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Currency";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAgingAP
            // 
            this.LueAgingAP.EnterMoveNextControl = true;
            this.LueAgingAP.Location = new System.Drawing.Point(166, 4);
            this.LueAgingAP.Margin = new System.Windows.Forms.Padding(5);
            this.LueAgingAP.Name = "LueAgingAP";
            this.LueAgingAP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.Appearance.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAgingAP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAgingAP.Properties.DropDownRows = 30;
            this.LueAgingAP.Properties.NullText = "[Empty]";
            this.LueAgingAP.Properties.PopupWidth = 300;
            this.LueAgingAP.Size = new System.Drawing.Size(230, 20);
            this.LueAgingAP.TabIndex = 44;
            this.LueAgingAP.ToolTip = "F4 : Show/hide list";
            this.LueAgingAP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAgingAP.EditValueChanged += new System.EventHandler(this.LueAgingAP_EditValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(48, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 14);
            this.label18.TabIndex = 43;
            this.label18.Text = "Aging AR Based On";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(166, 46);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Properties.MaxLength = 18;
            this.TxtCreditLimit.Size = new System.Drawing.Size(164, 20);
            this.TxtCreditLimit.TabIndex = 48;
            this.TxtCreditLimit.Validated += new System.EventHandler(this.TxtCreditLimit_Validated);
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(156, 278);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 30;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 300;
            this.LuePtCode.Size = new System.Drawing.Size(230, 20);
            this.LuePtCode.TabIndex = 41;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(47, 281);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 40;
            this.label10.Text = "Term Of Payment";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueResourceType
            // 
            this.LueResourceType.EnterMoveNextControl = true;
            this.LueResourceType.Location = new System.Drawing.Point(158, 96);
            this.LueResourceType.Margin = new System.Windows.Forms.Padding(5);
            this.LueResourceType.Name = "LueResourceType";
            this.LueResourceType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceType.Properties.Appearance.Options.UseFont = true;
            this.LueResourceType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueResourceType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueResourceType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueResourceType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueResourceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueResourceType.Properties.DropDownRows = 5;
            this.LueResourceType.Properties.NullText = "[Empty]";
            this.LueResourceType.Properties.PopupWidth = 150;
            this.LueResourceType.Size = new System.Drawing.Size(224, 20);
            this.LueResourceType.TabIndex = 79;
            this.LueResourceType.ToolTip = "F4 : Show/hide list";
            this.LueResourceType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueResourceType.EditValueChanged += new System.EventHandler(this.LueResourceType_EditValueChanged);
            this.LueResourceType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueResourceType_KeyDown);
            this.LueResourceType.Leave += new System.EventHandler(this.LueResourceType_Leave);
            // 
            // LueResourceName
            // 
            this.LueResourceName.EnterMoveNextControl = true;
            this.LueResourceName.Location = new System.Drawing.Point(152, 66);
            this.LueResourceName.Margin = new System.Windows.Forms.Padding(5);
            this.LueResourceName.Name = "LueResourceName";
            this.LueResourceName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceName.Properties.Appearance.Options.UseFont = true;
            this.LueResourceName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueResourceName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueResourceName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueResourceName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResourceName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueResourceName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueResourceName.Properties.DropDownRows = 5;
            this.LueResourceName.Properties.NullText = "[Empty]";
            this.LueResourceName.Properties.PopupWidth = 150;
            this.LueResourceName.Size = new System.Drawing.Size(224, 20);
            this.LueResourceName.TabIndex = 78;
            this.LueResourceName.ToolTip = "F4 : Show/hide list";
            this.LueResourceName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueResourceName.EditValueChanged += new System.EventHandler(this.LueResourceName_EditValueChanged);
            this.LueResourceName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueResourceName_KeyDown);
            this.LueResourceName.Leave += new System.EventHandler(this.LueResourceName_Leave);
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(156, 131);
            this.TxtLOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 30;
            this.TxtLOPDocNo.Properties.ReadOnly = true;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtLOPDocNo.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(114, 134);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 14);
            this.label9.TabIndex = 23;
            this.label9.Text = "LOP#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnLOPDocNo
            // 
            this.BtnLOPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLOPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLOPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLOPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLOPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseFont = true;
            this.BtnLOPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnLOPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnLOPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLOPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLOPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnLOPDocNo.Image")));
            this.BtnLOPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLOPDocNo.Location = new System.Drawing.Point(388, 132);
            this.BtnLOPDocNo.Name = "BtnLOPDocNo";
            this.BtnLOPDocNo.Size = new System.Drawing.Size(24, 19);
            this.BtnLOPDocNo.TabIndex = 25;
            this.BtnLOPDocNo.ToolTip = "Add Contact Person";
            this.BtnLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLOPDocNo.ToolTipTitle = "Run System";
            this.BtnLOPDocNo.Click += new System.EventHandler(this.BtnLOPDocNo_Click);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(156, 68);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(230, 20);
            this.TxtStatus.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(110, 70);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 17;
            this.label12.Text = "Status";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(156, 110);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 30;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 300;
            this.LueProcessInd.Size = new System.Drawing.Size(230, 20);
            this.LueProcessInd.TabIndex = 22;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(104, 113);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 14);
            this.label24.TabIndex = 21;
            this.label24.Text = "Process";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeApprovalRemark
            // 
            this.MeeApprovalRemark.EnterMoveNextControl = true;
            this.MeeApprovalRemark.Location = new System.Drawing.Point(156, 89);
            this.MeeApprovalRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeApprovalRemark.Name = "MeeApprovalRemark";
            this.MeeApprovalRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeApprovalRemark.Properties.MaxLength = 1000;
            this.MeeApprovalRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeApprovalRemark.Properties.ShowIcon = false;
            this.MeeApprovalRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeApprovalRemark.TabIndex = 20;
            this.MeeApprovalRemark.ToolTip = "F4 : Show/hide text";
            this.MeeApprovalRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeApprovalRemark.ToolTipTitle = "Run System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(46, 91);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 14);
            this.label26.TabIndex = 19;
            this.label26.Text = "Approval\'s Remark";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcBOQ
            // 
            this.TcBOQ.Controls.Add(this.tabPage1);
            this.TcBOQ.Controls.Add(this.tabPage2);
            this.TcBOQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcBOQ.Location = new System.Drawing.Point(0, 0);
            this.TcBOQ.Name = "TcBOQ";
            this.TcBOQ.SelectedIndex = 0;
            this.TcBOQ.Size = new System.Drawing.Size(849, 200);
            this.TcBOQ.TabIndex = 76;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueResourceType);
            this.tabPage1.Controls.Add(this.LueResourceName);
            this.tabPage1.Controls.Add(this.Grd2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(841, 173);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 3);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(835, 167);
            this.Grd2.TabIndex = 77;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.GrdColHdrDoubleClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.GrdRequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdAfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GrdKeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Grd3);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 142);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Approval Information";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 3);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 136);
            this.Grd3.TabIndex = 80;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // FrmBOQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 504);
            this.Name = "FrmBOQ";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankGuaranteeInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotalRBPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPolicy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIndirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDirectCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResourceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResourceName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).EndInit();
            this.TcBOQ.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnCtContactPersonName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtQtMth;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        internal DevExpress.XtraEditors.DateEdit DteQtStartDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueRingCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Panel panel5;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace2;
        internal DevExpress.XtraEditors.TextEdit TxtCtReplace;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        private DevExpress.XtraEditors.CheckEdit ChkPrintSignatureInd;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.LookUpEdit LueAgingAP;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.LookUpEdit LueResourceType;
        private DevExpress.XtraEditors.LookUpEdit LueResourceName;
        internal DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton BtnLOPDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotal;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.CheckEdit ChkTaxInd;
        internal DevExpress.XtraEditors.TextEdit TxtGrandTotal;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtTax;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtPolicy;
        internal DevExpress.XtraEditors.TextEdit TxtIndirectCost;
        internal DevExpress.XtraEditors.TextEdit TxtDirectCost;
        internal DevExpress.XtraEditors.TextEdit TxtSubTotalRBPS;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtPercentage;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.CheckEdit ChkBankGuaranteeInd;
        private DevExpress.XtraEditors.MemoExEdit MeeApprovalRemark;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.LookUpEdit LuePPN;
        private System.Windows.Forms.TabControl TcBOQ;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
    }
}