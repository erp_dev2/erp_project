﻿#region Update
/*
    24/01/2023 [DITA/MNET] New Apps -> based on FrmSOContract, note:soc rev belum kesentuh karna planning nya akan ada beda task kedepannya
    16/02/2023 [DITA/MNET] posisinya grid ditarik agak bawah, biar data detail resourcenya kelihatan
    16/02/2023 [DITA/MNET] PRJI Docno belum muncul
    20/02/2023 [DITA/MNET] saat generate sequence no, trigger nya dari account set yg ada di tblcoa
    21/02/2023 [BRI/MNET] tambah validasi, kolom, filter tab revenue item SO Contract
    21/03/2023 [MYA/MNET] Menghilangkan validasi COA Sales ketika save SO Contract
    29/03/2023 [RDA/MNET] tambah field start date pada menu SO Contract (include perubahan find dan printout)
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Xml;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmSOContract3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mCity = string.Empty, 
            mCnt = string.Empty, 
            mSADNo = string.Empty,
            mJOType = string.Empty,
            mListItCtCodeForResource = string.Empty,
            mItGrpCodeForDirectCost = string.Empty,
            mItGrpCodeForIndirectCost = string.Empty,
            mItGrpCodeForRemuneration = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsFilterByItCt = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated =false,
            mIsSOContractUseTax = false,
            mIsCustomerContactPersonNonMandatory = false,
            mIsSOContractJointOperationMandatory = false,
            mIsShippingNameValidatedContactPerson = false;
        private bool IsInsert = false;
        internal string mIsSoUseDefaultPrintout,
            mProjectAcNoFormula = string.Empty,
            mGenerateCustomerCOAFormat = string.Empty;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmSOContract3Find FrmFind;
        private string mJointOperationKSOType = string.Empty,
            mSOContractCOAFormat = string.Empty;
        private decimal BOQAmt = 0m;

        private string
         mPortForFTPClient = string.Empty,
         mHostAddrForFTPClient = string.Empty,
         mSharedFolderForFTPClient = string.Empty,
         mUsernameForFTPClient = string.Empty,
         mPasswordForFTPClient = string.Empty,
         mFileSizeMaxUploadFTPClient = string.Empty,
         mFormatFTPClient = string.Empty;

        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContract3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "SO Contract";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfSalesUomCode();
                GetParameter();
                if (mJointOperationKSOType.Length <= 0) mJointOperationKSOType = "1";
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueStatus(ref LueStatus);
                Sl.SetLueOption(ref LueJOType, "JointOperationType");
                if (mIsSOContractJointOperationMandatory)
                    label24.ForeColor = Color.Red;

                TcSOContract.SelectedTabPage = TpBankGuarantee;
                Sl.SetLueBankCode(ref LueBankCode);

                TcSOContract.SelectedTabPage = TpJO;
                SetLueJOCode(ref LueJOCode, string.Empty);
                LueJOCode.Visible = false;
                DteJODocDt.Visible = false;

                TcSOContract.SelectedTabPage = TpItem;
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                LueTaxCode1.Visible = LueTaxCode2.Visible =  false;
                TcSOContract.SelectedTabPage = TpBOQ;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mIsCustomerContactPersonNonMandatory)
                {
                    BtnContact.Visible = true;
                    BtnContact.Enabled = true;
                }
                else
                {
                    BtnContact.Visible = false;
                    BtnContact.Enabled = false;
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {

            #region Grid 3 - Item

            Grd3.Cols.Count = 37;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3, new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Agent Code",
                        "Agent",
                        "Item's"+Environment.NewLine+"Code",
                        "",

                        //6-10
                        "Item's Name",
                        "Packaging",
                        "Quantity"+Environment.NewLine+"Packaging",
                        "Quantity",
                        "UoM",

                        //11-15
                        "Price"+Environment.NewLine+"(Price List)",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Price After"+Environment.NewLine+"Discount",                        
                        "Promo"+Environment.NewLine+"%",

                        //16-20
                        "Price"+Environment.NewLine+"Before Tax",
                        "Tax 1",
                        "Tax"+Environment.NewLine+"Amount",
                        "Price"+Environment.NewLine+"After Tax",                        
                        "Total",

                        //21-25
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",
                        "CtQtDNo",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",

                        //26-30
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Volume",
                        "Total Volume",
                        "Uom"+Environment.NewLine+"Volume",
                        "Tax 2",

                        //31-35
                        "Tax"+Environment.NewLine+"Amount",
                        "TaxCode",
                        "TaxCode2",
                        "Revenue"+Environment.NewLine+"Before Tax",
                        "Total"+Environment.NewLine+"Revenue Amount",

                        //36
                        "Item's Category"
                    },
                    new int[] 
                    {
                        //0
                        40,

                        //1-5
                        20, 0, 180, 80, 20, 
                        
                        //6-10
                        200, 100, 100, 80, 80, 
                        
                        //11-15
                        100, 80, 100, 100, 80,
                        
                        //16-20
                        100, 80, 100, 100, 120,   
                        
                        //21-25
                        100, 400, 0, 120, 120,

                        //26-30
                        250, 100, 100, 100, 100,

                        //31-35
                        100, 0, 0, 100, 120,

                        //36
                        0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 27, 28, 31, 34, 35 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,31, 32, 33, 34, 35, 36 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 7, 8, 12, 14, 15, 16, 19, 20,21, 23, 24, 25, 26, 27, 28, 29, 32, 33 }, false);
            Grd3.Cols[30].Move(19);
            Grd3.Cols[31].Move(20);
            Grd3.Cols[34].Move(16);
            Grd3.Cols[35].Move(25);


            #endregion

            #region Grid 1 - ARDP

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            #endregion

            #region Grid 2 - BOQ

            Grd2.Cols.Count = 11;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "BOQ Document", 
                        
                        //1-5
                        "BOQ Date",
                        "LOP Document",
                        "Project",
                        "BOQ Item Code",
                        "BOQ Item Name",
                        //6-10
                        "BOQ Amount",
                        "Amount",
                        "Allow",
                        "Amt",
                        "parentInd"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        120, 150, 250, 100, 250,
                        //6-8
                        150, 150, 80, 150, 80
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 });

            #endregion

            #region Grid 5 - Joint Operation

            Grd5.Cols.Count = 6;

            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[] 
                {
                    //0
                    "JOCode", 
                    
                    //1-5
                    "Joint Operation",
                    "Joint Operation's" + Environment.NewLine + "Date",
                    "Joint Operation's" + Environment.NewLine + "Document#",
                    "Portion (%)",
                    "NPWP"
                },
                new int[] 
                {
                    //0
                    0,
                    //1-3
                    120, 150, 150, 120, 120
                }
            );
            Sm.GrdFormatDate(Grd5, new int[] { 2 });
            Sm.GrdFormatDec(Grd5, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0 });
            Sm.GrdColInvisible(Grd5, new int[] { 0 });

            #endregion

            #region Grid 6 - Resource

            Grd6.Cols.Count = 16;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd6, new string[]
                {
                    "",
                    "ResourceItCode", "Resource", "Group", "Remark", "Quantity 1"+Environment.NewLine+"(Unit)",
                    "Quantity 2"+Environment.NewLine+"(Time)", "Unit Price", "Tax", "Total Without Tax", "Total Tax",
                    "Total With Tax", "Group It Code", "", "Detail#", "Sequence"
                },
                new int[]
                {
                    20,
                    0, 200, 200, 200, 150,
                    150, 180, 180, 200, 200,
                    200,150, 20, 120, 65
                }
            );
            Sm.GrdFormatDec(Grd6, new int[] { 5, 6, 7, 8, 9, 10, 11 }, 8);
            Sm.GrdColButton(Grd6, new int[] { 0, 13 });
            Sm.GrdColReadOnly(Grd6, new int[] { 1, 2, 3, 9, 10, 11, 14 });
            Sm.GrdColInvisible(Grd6, new int[] { 12 });
            Grd6.Cols[15].Move(1);
            #endregion

            #region Grid 7 - Approval

            Grd7.Cols.Count = 5;
                Sm.GrdHdrWithColWidth(
                        Grd7,
                        new string[]
                        {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                        },
                        new int[] { 50, 150, 100, 100, 200 }
                    );
                Sm.GrdFormatDate(Grd7, new int[] { 3 });
                Sm.GrdColReadOnly(Grd7, new int[] { 0, 1, 2, 3, 4 });
           

            #endregion

            #region Grid 8 - Upload File

          
            Grd8.Cols.Count = 7;
            Grd8.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[]
                    {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                    },
                     new int[]
                    {
                        0,
                        250, 20, 20, 100, 100,
                        100
                    }
                  );

            Sm.GrdColInvisible(Grd8, new int[] { 0 }, false);
            Sm.GrdFormatDate(Grd8, new int[] { 5 });
            Sm.GrdFormatTime(Grd8, new int[] { 6 });
            Sm.GrdColButton(Grd8, new int[] { 2, 3 });
            Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Grd8.Cols[2].Move(1);
                
         

            #endregion

        }
        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtBOQDocNo, TxtLocalDocNo, LueCtCode, MeeCancelReason,
                        LueCustomerContactperson, MeeRemark,  ChkCancelInd, LueStatus, TxtPICSales, 
                        LueShpMCode, TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                        LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt,TxtRemunerationCost, TxtRemunerationPerc,TxtDirectCost, TxtDirectPerc,
                        TxtIndirectCost, TxtIndirectPerc,TxtTotal,TxtTotalPerc, TxtCostPerc, LueTaxCode1, LueTaxCode2, DteStartDt, DteEndDt
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnContact.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7 });
                    Grd5.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 17, 9, 11, 13, 22, 30 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 4, 5, 6, 7, 8, 15 });
                    for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd8, Row, 4).Length == 0)
                        {
                            Grd8.Cells[Row, 2].ReadOnly = iGBool.False;
                            Grd8.Cells[Row, 3].ReadOnly = iGBool.True;
                        }
                        else
                        {
                            Grd8.Cells[Row, 2].ReadOnly = iGBool.True;
                            Grd8.Cells[Row, 3].ReadOnly = iGBool.False;
                        }
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueCtCode, LueCustomerContactperson, LueShpMCode, 
                       MeeRemark, MeeProjectDesc, LueJOCode, DteJODocDt, LueJOType, TxtBankGuaranteeNo,
                        LueBankCode, DteBankGuaranteeDt, LueTaxCode1, LueTaxCode2,  DteStartDt, DteEndDt
                    }, false);
                    BtnCustomerShipAddress.Enabled = true;
                    BtnContact.Enabled = true;
                    DteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 17,9, 11, 13, 22, 30 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 4, 5, 6, 7, 8, 15 });
                    Grd5.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd8, new int[] { 2, 3 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    if (Sm.IsDataExist("Select 1 From TblSOContractHdr Where DocNo = @Param And Status = 'A';", TxtDocNo.Text))
                    {
                        for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd8, Row, 4).Length == 0)
                            {
                                Grd8.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd8.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd8.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd8.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }
                        
                    LueStatus.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtBOQDocNo, LueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtEmail, TxtMobile, MeeCancelReason,
                  LueCtCode, LueCustomerContactperson, MeeRemark, LueShpMCode, TxtPICSales,
                  TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                  LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt, LueTaxCode1, LueTaxCode2, DteStartDt, DteEndDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtTotalResource, TxtContractAmtAfterTax, TxtContractAmtBefTax, TxtRemunerationCost, 
                TxtRemunerationPerc,TxtDirectCost, TxtDirectPerc, TxtIndirectCost, TxtIndirectPerc,
                TxtTotal,TxtTotalPerc, TxtCostPerc, TxtProfitMarginAmt, TxtProfitMarginPerc
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 4 });
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtEmail, TxtMobile
            });
        }


        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContract3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, Sm.GetValue("Select 'O' "));
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            try
            {
                PrintData(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 &&
                       !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                       !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmSOContract3Dlg3(this));
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (Sm.IsGrdColSelected(new int[] { 17, 30 }, e.ColIndex))
            {
                if (e.ColIndex == 17) Sm.LueRequestEdit(ref Grd3, ref LueTaxCode1, ref fCell, ref fAccept, e);
                if (e.ColIndex == 30) Sm.LueRequestEdit(ref Grd3, ref LueTaxCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 31, 34, 35 });
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
            ComputeContractAmt();
            ComputeTotal();
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                    !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                    !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
                Sm.FormShowDialog(new FrmSOContract3Dlg3(this));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    f.ShowDialog();
                }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 27, 28, 31, 34, 35 }, e.ColIndex))
                {
                    ComputeItem(e.RowIndex);
                    ComputeTotal();
                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7}, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalBOQ();
                //ComputeItem();
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd5, LueJOCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 2) Sm.DteRequestEdit(Grd5, DteJODocDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                }
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 5, 6, 7, 8, 9, 10, 11 }, e);
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 9, 10, 11 }, e.ColIndex))
                {
                    decimal mQty1 = 0m, mQty2 = 0m, mUPrice = 0m, mTax, mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;
                    mQty1 = Sm.GetGrdDec(Grd6, e.RowIndex, 5);
                    mQty2 = Sm.GetGrdDec(Grd6, e.RowIndex, 6);
                    mUPrice = Sm.GetGrdDec(Grd6, e.RowIndex, 7);
                    mTax = Sm.GetGrdDec(Grd6, e.RowIndex, 8);

                    mTotalWithoutTax = mQty1 * mQty2 * mUPrice;
                    mTotalTax = mQty1 * mQty2 * mTax;
                    mAmt = mTotalWithoutTax + mTotalTax;

                    Grd6.Cells[e.RowIndex, 9].Value = Math.Round(mTotalWithoutTax, 2);
                    Grd6.Cells[e.RowIndex, 10].Value = Math.Round(mTotalTax, 2);
                    Grd6.Cells[e.RowIndex, 11].Value = Math.Round(mAmt, 2);
                    ComputeTotalResource();
                    ComputeDirectCost(); ComputeRemunerationCost(); ComputeIndirectCost();
                    ComputeItem(e.RowIndex);
                }
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd6.ReadOnly)
            {
                if (e.ColIndex == 0 ) Sm.FormShowDialog(new FrmSOContract3Dlg4(this));
               
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd6.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 0 && TxtDocNo.Text.Length <= 0)
                {
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 3 });
                }
               
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd6, e, BtnSave);
                ComputeTotalResource(); ComputeDirectCost(); ComputeRemunerationCost(); ComputeIndirectCost();
            }

        }

        private void Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd8, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd8, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd8, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd8, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.ShowDialog();
                    Grd8.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd8.SelectedRows.Count > 0)
            {
                for (int Index = Grd8.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd8.SelectedRows[Index].Index;
            }

            if (Sm.GetLue(LueStatus) == "O")
            {
                if (Sm.GetGrdStr(Grd8, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd8, e, BtnSave);
                    Sm.GrdEnter(Grd8, e);
                    Sm.GrdTabInLastCell(Grd8, e, BtnFind, BtnSave);
                }
            }
          
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContract", "TblSOContractHdr");
            string mLOPDocNo = Sm.GetValue("Select LOPDocNo From TblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"' ");
            string mSiteCode = Sm.GetValue("Select SiteCode From TblLOPHdr Where DocNo = @Param ",mLOPDocNo);
            string mProjectScope = Sm.GetValue("Select ProjectScope From TblLOPHdr  Where DocNo = @Param ", mLOPDocNo);
            string ProjectSequenceNo = GenerateProjectSequenceNo(mSiteCode, mProjectScope);
            string ProjectCode = string.Concat(mSiteCode, ".", ProjectSequenceNo, mProjectScope);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractHdr(DocNo, ProjectSequenceNo, ProjectCode, mSiteCode));
            // save SO Contract Revision
            cml.Add(SaveSOContractRevisionHdr(DocNo));
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                {
                    cml.Add(SaveSOContractDtl(DocNo, Row));
                    cml.Add(SaveSOContractRevisionDtl(DocNo, Row));
                }
            }

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                {
                    cml.Add(SaveSOContractDtl2(DocNo, Row));
                    cml.Add(SaveSOContractRevisionDtl2(DocNo, Row));
                }
            }

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                        cml.Add(SaveSOContractDtl3(DocNo, Row));
                }
            }

            
            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
                    cml.Add(SaveSOContractDtl4(DocNo, Row));
            }
            

            //upload file
            for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0 && Sm.GetGrdStr(Grd8, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd8, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(DocNo));
            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0 && Sm.GetGrdStr(Grd8, Row, 1) != "openFileDialog1")
                    {
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd8, Row, 1));
                    }
                }
            }
            
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueCustomerContactperson, "Contact Person") ||
                Sm.IsTxtEmpty(TxtSAName, "Shipping name", false) ||
                Sm.IsTxtEmpty(TxtAddress, "Address", false) ||
                Sm.IsTxtEmpty(TxtBOQDocNo, "Bill Of Quantity", false) ||
                IsBOQAlreadyProceed() ||
                IsDocumentLOPProcessIndInvalid()||
                (mIsSOContractJointOperationMandatory && Sm.IsLueEmpty(LueJOType, "Joint Operation")) ||
                Sm.IsTxtEmpty(TxtPICSales, "PIC Sales", false) ||
                Sm.IsLueEmpty(LueShpMCode, "Shipment Method") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsGrdEmpty() ||
                IsJointOperationEmpty() ||
                IsJointOperationInvalid() ||
                IsBankGuaranteeEmpty()||
                //IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsCOASalesEmpty()||
                //IsCOAOptionEmpty() ||
                IsItCtCodeNotValid() ||
                (IsUploadFileMandatory());
        }

        private bool IsBOQAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where BOQDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This BOQ data already proceed in SOC#" + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsBankGuaranteeEmpty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr A ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And BankGuaranteeInd = 'Y' ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                if (Sm.IsTxtEmpty(TxtBankGuaranteeNo, "Bank Guarantee#", false)) { TcSOContract.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) { TcSOContract.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsDteEmpty(DteBankGuaranteeDt, "Date")) { TcSOContract.SelectedTabPage = TpBankGuarantee; return true; }
                //Sm.StdMsg(mMsgType.Warning, "You need to input Bank Guarantee data. " );
                //return true;
            }

            //if (TxtBankGuaranteeNo.Text.Length == 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Bank Guarantee# is Empty. ");
            //    return true;
            //}

            //if ( Sm.GetLue(LueBankCode).Length == 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, "Bank Name is Empty. ");
            //    return true;
            //}

            return false;
        }

        private bool IsJointOperationEmpty()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                if(Grd5.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Joint Operation data.");
                    TcSOContract.SelectedTabPage = TpJO;
                    Sm.FocusGrd(Grd5, 0, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsJointOperationInvalid()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                for (int i = 0; i < Grd5.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, i, 1, false, "Joint Operation is empty.")) { TcSOContract.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 2, false, "Joint Operation's Date is empty.")) { TcSOContract.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 3, false, "Joint Operation's Number is empty.")) { TcSOContract.SelectedTabPage = TpJO; return true; }
                }
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Item is empty.")) return true;

            return false;
        }

        private bool IsDocumentLOPProcessIndInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LOPDocNo From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("B.ProcessInd In ('S', 'C') ");
            SQL.AppendLine("Or B.CancelInd = 'Y' ");
            SQL.AppendLine("Or B.Status = 'C' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data is either Lose or Cancelled. LOP# : " + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd3.Rows.Count-1 > 1)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum 1.");
                return true;
            }
            return false;
        }

        private bool IsCOASalesEmpty()
        {
            string AcNo  = Sm.GetValue("Select B.AcNo4 From tblItem A "+
                            "Inner Join TblItemcategory B On A.ItCtCode = B.ItCtCode "+
                            "Where A.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'"); 
            if (AcNo.Length<=0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, "+Environment.NewLine+" for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsCOAOptionEmpty()
        {
            string AcNo = Sm.GetValue("Select A.OptCode From TblOption A "+
                "Inner Join TblItemcategory B On A.OptCode = B.Acno4 "+
                "Inner Join TblItem C on B.ItCtCode = C.itCtCode " +
                "Where A.Optcat = 'AccountSet' And C.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'");

            if (AcNo.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, " + Environment.NewLine + " for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") "+Environment.NewLine+" should be listed on system option menu.");
                return true;
            }
            return false;
        }

        private bool IsItCtCodeNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, 0, 36) != Sm.GetGrdStr(Grd3, Row, 36))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item's Category is deferent.");
                    Sm.FocusGrd(Grd3, Row, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSOContractHdr(string DocNo, string ProjectSequenceNo, string ProjectCode, string SiteCode)
        {
            //string HOInd = Sm.GetValue("Select if(B.HOInd = 'Y', '0', '1') As HoInd From tblLOPhdr A "+
            //                                                     "Inner Join TblSite B On A.SiteCode = b.SiteCode "+
            //                                                     "Where DocNo = (Select LopDocNo From tblBOQHdr Where DocNO = '"+TxtBOQDocNo.Text+"')");
            mJOType = Sm.GetLue(LueJOType);
            string ProfitCode =  Sm.GetValue("Select B.profitCenterCode From tblLOPhdr A "+
                                                                  "Inner Join TblSite B On A.SiteCode = b.SiteCode "+
                                                                  "Where DocNo = (Select LopDocNo From tblBOQHdr Where DocNO = '"+TxtBOQDocNo.Text+"')");
           
            string CCCode = Sm.GetValue("Select A.CCCode From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectScope =  Sm.GetValue("Select A.ProjectScope From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectResource = Sm.GetValue("Select A.projectresource From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectType = Sm.GetValue("Select A.ProjectType From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '"+TxtBOQDocNo.Text+"'  ) ");
            string ProjectName = Sm.GetValue("Select A.ProjectName From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string CoaOption = Sm.GetValue("Select OptCode From tblOption Where Optcat = 'AccountSet' limit 1 ");
            string Yr =  Sm.Right(Sm.GetDte(DteDocDt).Substring(0, 4), 2);

            string Acno = string.Concat(ProfitCode, CCCode, ProjectScope, ProjectResource, ProjectType, Yr);
            
            //ngeceknya ke salah satu account saja buat dapetin 2 digit terakhir
            string lastDigit = GetDigitAcno(string.Concat(CoaOption,'.', Acno));

            // new COA format
            string AcNo2 = string.Empty;
            string SequenceNo = string.Empty;

            if (mSOContractCOAFormat == "2") // VIR
            {
                if (ProjectResource.Length == 0) ProjectResource = "0";
                if (ProjectType.Length == 0) ProjectType = "0";
                if (ProjectScope.Length == 0) ProjectScope = "0";

                if (SiteCode.Length == 0) SiteCode = "000"; else SiteCode = Sm.Left(string.Concat(SiteCode, "0"), 3);

                SequenceNo = GetSequenceNo(ProjectType, Yr);
                if (SequenceNo.Length == 0) SequenceNo = "001"; else SequenceNo = Sm.Right(string.Concat("000", SequenceNo), 3);

                //command by dita 03/01/2022
                // AcNo2 = string.Concat(ProjectResource, ProjectType, ProjectScope, ".", Yr, SequenceNo);
                AcNo2 = string.Concat(ProjectType, ".", Yr, SequenceNo);
               
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractHdr(DocNo, LocalDocNo, DocDt,  CancelInd, ");
            SQL.AppendLine("CtCode, CtContactPersonName, BOQDocNo, CurCode, Amt, Amt2, TotalResource, ");
            SQL.AppendLine("SADNo, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, JOType, ");
            SQL.AppendLine("SpCode, ShpMCode, Remark, ProjectDesc, ProjectCode, ProjectSequenceNo,ProjectCode2, CreateBy, CreateDt, BankGuaranteeNo, BankCode, BankGuaranteeDt, ");
            SQL.AppendLine("ProfitMarginAmt, ProfitMarginPerc, StartDt, EndDt, RemunerationAmt, DirectCostAmt, IndirectCostAmt, TotalCost, CostPerc, RemunerationPerc, DirectCostPerc, IndirectCostPerc, TotalCostPerc)");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, @CancelInd, ");
            SQL.AppendLine("@CtCode, @CtContactPersonName, @BOQDocNo, ");
            SQL.AppendLine("(Select CurCode From TblBOQHdr Where DocNo=@BOQDocNo), @Amt, @Amt2,@TotalResource, ");
            SQL.AppendLine("@SADNo, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @JOType, ");
            SQL.AppendLine("@SpCode, @ShpMCode, @Remark, @ProjectDesc, @AcNo, @ProjectSequenceNo, @ProjectCode2, @CreateBy, CurrentDateTime(), @BankGuaranteeNo, @BankCode, @BankGuaranteeDt, ");
            SQL.AppendLine("@ProfitMarginAmt, @ProfitMarginPerc, @StartDt, @EndDt, @RemunerationAmt, @DirectCostAmt, @IndirectCostAmt, @TotalCost, @CostPerc, @RemunerationPerc, @DirectCostPerc, @IndirectCostPerc, @TotalCostPerc); ");
        


            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='SOC'; ");

            SQL.AppendLine("Update TblSOContractHdr Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists(  ");
            SQL.AppendLine("    Select 1 From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='SOC' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine("    );  ");


            if (IsInsert && mGenerateCustomerCOAFormat == "3")
            {
                if (mSOContractCOAFormat == "1")
                {
                    SQL.AppendLine("Insert into TblCoa(AcNo, AcDesc, ActInd, Parent, Level, AcType, EntCode, SOCDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select Concat(OptCode,'.', @AcNo) As AcNo, Concat(OptDesc, ' ', @ProjectName) As AcDesc, ");
                    SQL.AppendLine("'Y' ActiveInd,  ");
                    if(mProjectAcNoFormula == "2")
                        SQL.AppendLine("Concat(OptCode, '.', '"+ SiteCode +"') As Parent,  ");
                    else 
                        SQL.AppendLine("OptCode As Parent,  ");
                    SQL.AppendLine("(Length(Concat(OptCode,'.', @AcNo)) - Length(Replace(Concat(OptCode,'.', @AcNo), '.', ''))+1)   As Lvl, ");
                    SQL.AppendLine("B.Actype, null, @DocNo, 'Sys', CurrentDatetime() , null, null  ");
                    SQL.AppendLine("From tblOption A ");
                    SQL.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                    SQL.AppendLine("Where A.OptCat = 'AccountSet'  ");

                    SQL.AppendLine("On Duplicate Key Update LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("; ");
                }

                else
                {
                    SQL.AppendLine("Insert into TblCoa(AcNo, AcDesc, ActInd, Parent, Level, AcType, EntCode, SOCDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine("Select Concat(OptCode,'.', @AcNo) As AcNo, Concat(OptDesc, ' ', @ProjectName) As AcDesc, ");
                    SQL.AppendLine("'Y' ActiveInd, OptCode As Parent,  (Length(Concat(OptCode,'.', @AcNo2)) - Length(Replace(Concat(OptCode,'.', @AcNo2), '.', ''))+1)   As Lvl, ");
                    SQL.AppendLine("B.Actype, null, @DocNo, 'Sys', CurrentDatetime() , null, null  ");
                    SQL.AppendLine("From tblOption A ");
                    SQL.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                    SQL.AppendLine("Where A.OptCat = 'AccountSet'  ");
                    if (mJOType.Length > 0)
                    {
                        SQL.AppendLine("And ( ");
                        SQL.AppendLine("    A.OptCode Not In ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Distinct(Property1) From TblOption ");
                        SQL.AppendLine("        Where OptCat = 'JointOperationType' ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine("    Or A.OptCode In ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select Distinct(Property1) From TblOption ");
                        SQL.AppendLine("        Where OptCat = 'JointOperationType' ");
                        SQL.AppendLine("        And OptCode = @JOType ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }

                    #region Old
                    /*
                    if (mJOType == "1")
                        SQL.AppendLine("And A.OptCode != '4.0.0.0' ");
                    else if(mJOType == "2")
                        SQL.AppendLine("And A.OptCode != '4.1.0.0' ");
                    */
                    #endregion

                    SQL.AppendLine("On Duplicate Key Update LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("; ");
                }
            }
                
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCustomerContactperson));
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtContractAmtBefTax.Text));
            Sm.CmParam<String>(ref cm, "@SADNo", mSADNo);
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<String>(ref cm, "@JOType", Sm.GetLue(LueJOType));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDesc", MeeProjectDesc.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ProjectName", ProjectName);

            if (mProjectAcNoFormula == "2") Sm.CmParam<String>(ref cm, "@AcNo", ProjectCode);
            else
            {
                if (mSOContractCOAFormat == "1")
                    Sm.CmParam<String>(ref cm, "@AcNo", String.Concat(Acno, lastDigit));
                else
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo2);
            }
            
            if (mSOContractCOAFormat == "2" || mSOContractCOAFormat == "3")
                Sm.CmParam<String>(ref cm, "@AcNo2", string.Concat(SiteCode,".",CCCode, ".", AcNo2));

            Sm.CmParam<String>(ref cm, "@ProjectSequenceNo", ProjectSequenceNo);

            if (mProjectAcNoFormula == "2")
                Sm.CmParam<String>(ref cm, "@ProjectCode2", ProjectCode);
            else
                Sm.CmParam<String>(ref cm, "@ProjectCode2", AcNo2);
            
            Sm.CmParam<String>(ref cm, "@BankGuaranteeNo", TxtBankGuaranteeNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@BankGuaranteeDt", Sm.GetDte(DteBankGuaranteeDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtContractAmtAfterTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Decimal.Parse(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@ProfitMarginAmt", Decimal.Parse(TxtProfitMarginAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ProfitMarginPerc", Decimal.Parse(TxtProfitMarginPerc.Text));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Decimal.Parse(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostAmt", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCost", Decimal.Parse(TxtTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@CostPerc", Decimal.Parse(TxtCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationPerc", Decimal.Parse(TxtRemunerationPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostPerc", Decimal.Parse(TxtDirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostPerc", Decimal.Parse(TxtIndirectPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCostPerc", Decimal.Parse(TxtTotalPerc.Text));

            return cm;
        }

        private MySqlCommand SaveSOContractDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSOContractDtl(DocNo, DNo, ItCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, TaxRate, TaxAmt, UPrice, UPriceBefTax, UPriceAfTax, Amt, DeliveryDt,  Remark, CreateBy, CreateDt, " +
                " TaxAmt2, TaxCode, TaxCode2, RevenueBefTax, TotalRevenueAmt, DiscAmt) " +
                "Values(@DocNo, @DNo, @ItCode, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, 0, @TaxAmt, @UPrice, @UPriceBefTax, @UPriceAfTax, @Amt, @DeliveryDt,  @Remark, @CreateBy, CurrentDateTime(), "+
                " @TaxAmt2, @TaxCode, @TaxCode2, @RevenueBefTax, @TotalRevenueAmt, @DiscAmt) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd3, Row, 31));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetGrdStr(Grd3, Row, 32));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetGrdStr(Grd3, Row, 33));
            Sm.CmParam<Decimal>(ref cm, "@RevenueBefTax", Sm.GetGrdDec(Grd3, Row, 34));
            Sm.CmParam<Decimal>(ref cm, "@TotalRevenueAmt", Sm.GetGrdDec(Grd3, Row, 35));
            Sm.CmParam<Decimal>(ref cm, "@DiscAmt", Sm.GetGrdDec(Grd3, Row, 13));

            return cm;
        }

        private MySqlCommand SaveSOContractDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblSOContractDtl2(DocNo, DNo, BOQDocNo, ItBOQCode, Amt, CreateBy, CreateDt) " +
                "Values(@DocNo, @DNo, @BOQDocNo, @ItBOQCode, @Amt, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@BOQDocNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl3(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblSOContractDtl3(DocNo, DNo, JOCode, JODocDt, JODocNo, PortionPercentage, NPWP, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @JOCode, @JODocDt, @JODocNo, @PortionPercentage, @NPWP, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@JOCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParamDt(ref cm, "@JODocDt", Sm.GetGrdDate(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@JODocNo", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@PortionPercentage", Sm.GetGrdDec(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@NPWP", Sm.GetGrdStr(Grd5, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblSOContractDtl6(DocNo, DNo, ResourceItCode, Sequence, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @ResourceItCode, @Sequence, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd6, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd6, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd6, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd6, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd6, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveSOContractRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, Status, Amt, PrevAmt, Amt2,  ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, 'O', @Amt, @Amt, @Amt2, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsRevisionNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='SOCRev'; ");
            }

            SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtContractAmtAfterTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtTotalResource.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, Amt, PrevAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl2(DocNo, SOCDocNo, DNo, Amt, PrevAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo, "/0001"));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        public static string GetDigitAcno(string AcNo)
        {            
            string Lastdigit = Sm.GetValue("Select MAX(Right(AcNo, 2)) From TblCoa Where Acno like '"+AcNo+"%'");

            if (Lastdigit.Length > 0)
            {
                Lastdigit = Sm.Right(Convert.ToString(string.Concat("00",(decimal.Parse(Lastdigit) + 1))), 2);
            }
            else
            {
                Lastdigit = "01";
            }

            return Lastdigit;
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GetSequenceNo(string ProjectType, string Yr)
        {
            string seq = string.Empty;
            string sampleAccountSet = Sm.GetValue("Select OptCode From TblOption A Inner Join TblCOA B On A.OptCode = B.AcNo Where OptCat = 'AccountSet' And B.ActInd = 'Y' Limit 1; ");
            string param = string.Concat(sampleAccountSet, ".", ProjectType, ".", Yr);
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Count(*) ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where AcNo Like @Param; ");

            seq = Sm.GetValue(SQL.ToString(), string.Concat(param, "%"));

            if (seq == "0") seq = string.Empty;
            else
            {
                if (Int32.Parse(seq) > 0) seq = (Int32.Parse(seq) + 1).ToString();
            }

            return seq;
        }

        private void InputAmount(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 9);
                if (Sm.GetGrdStr(Grd2, RowIndex, 8) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 4);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 4) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 7));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 7].Value = Qty;
                                    Grd2.Cells[y, 9].Value = Qty;
                                    Grd2.Cells[RowIndex, 9].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }

           
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (ChkCancelInd.Checked == true)
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid2()) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateSOContractHdr());

            //EDIT UPLOAD
            
            for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                {
                    if ( Sm.GetGrdStr(Grd8, Row, 1).Length > 0 && Sm.GetGrdStr(Grd8, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd8, Row, 4).Length == 0)
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd8, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(TxtDocNo.Text));
            
            //EDIT UPLOAD

            Sm.ExecCommands(cml);

            
            for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                {
                    if ( Sm.GetGrdStr(Grd8, Row, 1).Length > 0 && Sm.GetGrdStr(Grd8, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd8, Row, 4).Length == 0)
                    {
                        UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd8, Row, 1));
                    }
                }
            }
            

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToProjectImplementation() ||
                IsSOContractAlreadyProcessedToARDP()
                ;
        }

        private bool IsSOContractAlreadyProcessedToProjectImplementation()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Where B.SOCDocNo = @Param ");
            SQL.AppendLine("And (A.CancelInd = 'N' And A.Status <> 'C') ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has already processed to Project Implementation : " + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }

        private bool IsCancelledDataNotValid2()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToARDP();
        }

        private MySqlCommand UpdateSOContractHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsSOContractAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractHdr " +
                    "Where DocNo=@DocNo And (CancelInd='Y' Or Status = 'C') ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already cancelled.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsSOContractAlreadyFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractDtl " +
                    "Where DocNo=@DocNo And processInd = 'F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already fullfiled.");
                Sm.SetLue(LueStatus, "F");
                return true;
            }
            return false;
        }

     
        private bool IsSOContractAlreadyProcessedToARDP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SODocNo ");
            SQL.AppendLine("From TblARDownpayment ");
            SQL.AppendLine("Where SODocNo=@SODocNo And CancelInd = 'N' And Status = 'A' And CtCode=@CtCode ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            if (Sm.IsDataExist(cm) && mIsSOUseARDPValidated)
            {
                Sm.StdMsg(mMsgType.Warning, "This document already use to AR DownPayment.");
                return true;
            }
            return false;


        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOContractHdr(DocNo);
                ShowSOContractDtl(DocNo);
                ShowSOContractDtl2(DocNo);
                ShowSOContractDtl3(DocNo);
                ShowSOContractDtl4(DocNo);
                ShowARDownPayment(DocNo);
                Sm.ShowDocApproval(DocNo, "SOC", ref Grd7);
                ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.Amt2, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, A.JOType, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname,  A.BankGuaranteeNo, A.BankCode, A.BankGuaranteeDt, J.DocNo PRJIDocNo, K.UserCode PICSales, ");
            SQL.AppendLine("A.TotalResource, A.ProfitMarginAmt, A.ProfitMarginPerc, A.EndDt, A.RemunerationAmt, A.DirectCostAmt, A.IndirectCostAmt, A.TotalCost, A.CostPerc, A.RemunerationPerc, A.DirectCostPerc, A.IndirectCostPerc, A.TotalCostPerc, A.StartDt ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Left Join TblSOContractRevisionHdr I On A.DocNo = I.SOCDocNo And I.Status  = 'A' ");
            SQL.AppendLine("Left Join TblProjectImplementationHdr J On I.DocNo = J.SOContractDocNo And J.CancelInd  = 'N' ");
            SQL.AppendLine("Left Join TblUser K On H.PICCode = K.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                       "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-30
                        "ProjectName", "JOType",  "BankGuaranteeNo", "BankCode", "BankGuaranteeDt",

                        //31-35
                        "Amt2", "PRJIDocNo", "PICSales","TotalResource", "ProfitMarginAmt", 
                        
                        //36-40
                        "ProfitMarginPerc", "EndDt", "RemunerationAmt", "DirectCostAmt", "IndirectCostAmt", 
                        
                        //41-45
                        "TotalCost", "CostPerc","RemunerationPerc", "DirectCostPerc", "IndirectCostPerc", 
                        //46-47
                        "TotalCostPerc", "StartDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        SetLueCtPersonCode(ref LueCustomerContactperson, Sm.DrStr(dr, c[5]), Sm.DrStr(dr, c[6]));
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtContractAmtAfterTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetLue(LueJOType, Sm.DrStr(dr, c[27]));
                        TxtBankGuaranteeNo.EditValue = Sm.DrStr(dr, c[28]);
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[29]));
                        Sm.SetDte(DteBankGuaranteeDt, Sm.DrStr(dr, c[30]));
                        TxtContractAmtBefTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[32]);
                        TxtPICSales.EditValue = Sm.DrStr(dr, c[33]);
                        TxtTotalResource.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[34]), 0);
                        TxtProfitMarginAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                        TxtProfitMarginPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[37]));
                        TxtRemunerationCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                        TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);
                        TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                        TxtTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                        TxtCostPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 0);
                        TxtRemunerationPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[43]), 0);
                        TxtDirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 0);
                        TxtIndirectPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[45]), 0);
                        TxtTotalPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[46]), 0);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[47]));
                    }, true
                );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItCtCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   B.Uprice, 0 As Discount, DiscAmt, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceAfterDiscount, ");
            SQL.AppendLine("   0 As PromoRate, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate, B.TaxAmt, B.UPriceAfTax UPriceAfterTax, B.Amt Total, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  "); 
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom, B.TaxAmt2, B.TaxCode, K.TaxName, B.TaxCode2, L.TaxName TaxName2 ,B.RevenueBefTax, B.TotalRevenueAmt, G.InventoryUOMCode  ");
            SQL.AppendLine("   From TblSOContractHdr A    ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo   ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Left Join TblTax K On K.TaxCode = B.TaxCode ");
            SQL.AppendLine("   Left Join TblTax L On L.TaxCode = B.TaxCode2 ");
            SQL.AppendLine("   Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T Order By DNo; ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-25
                    "TotalVolume", "VolUom",  "TaxAmt2", "TaxCode", "TaxName",
                    //26-30
                    "TaxCode2", "TaxName2", "RevenueBefTax", "TotalRevenueAmt", "InventoryUOMCode",
                    //31
                    "ItCtCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 31);


                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 27, 28, 31, 34, 35 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A2.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From tblSOContracthdr A1 ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.DocNo = A2.Docno ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A1.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A1.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.ItBOQCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowSOContractDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.JOCode, B.JOName, A.JODocDt, A.JODocNo, IfNull(A.PortionPercentage, 0.00) As PortionPercentage, A.NPWP ");
            SQL.AppendLine("FROM TblSOContractDtl3 A ");
            SQL.AppendLine("INNER JOIN TblJointOperationHdr B ON A.JOCode = B.JOCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");
            SQL.AppendLine("ORDER BY A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "JOCode", 

                    //1-5
                    "JOName", "JODocDt", "JODocNo", "PortionPercentage", "NPWP"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowSOContractDtl4(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.ResourceItCode, A.Sequence, B.ItName,C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt ");
            SQLDtl2.AppendLine(", D.DocNo As RBPDocNo ");
            SQLDtl2.AppendLine("From TblSOContractDtl6 A ");
            SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd6, ref cm2, SQLDtl2.ToString(),
                new string[]
                {
                    "ResourceItCode",
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2",
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ShowBOQ(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("C.ItBoqCode, D.ItBOQName, C.totalAMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("from tblBOQhdr A ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
	        SQL.AppendLine("    From TblItemBOQ ");
	        SQL.AppendLine("    Where ItBOQCode not in ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
		    SQL.AppendLine("        Where parent is not null ");
	        SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo, C.ItBOQCode ;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "TotalAmt", "Allow", "parentInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                    {
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                        Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                        Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }
                    else
                    {
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }

                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        public void ShowRevenueItem(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.RevenueItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.Qty,B.Price, 0 As Discount, DiscAmt, null as PackagingUnitUomCode, 0 as QtyPackagingUnit,");
            SQL.AppendLine("   0 As UPriceAfterDiscount, ");
            SQL.AppendLine("   0 As PromoRate, ");
            SQL.AppendLine("   0 As UPriceBefore, ");
            SQL.AppendLine("   0 As TaxRate, B.TaxAmt1, 0 As UPriceAfterTax, 0 As Total, ");
            SQL.AppendLine("   null as DeliveryDt, B.Remark, 0 as Volume, 0 As TotalVolume,  ");
            SQL.AppendLine("   0 As VolUom, B.TaxAmt2, B.TaxCode1, K.TaxName, B.TaxCode2, L.TaxName TaxName2 ,B.RevenueBefTax, B.TotalRevenueAmt, G.InventoryUomCode   ");
            SQL.AppendLine("   From TblBOQHdr A    ");
            SQL.AppendLine("   Inner Join TblBOQDtl5 B On A.DocNo=B.DocNo   ");
            SQL.AppendLine("   Inner Join TblItem G On B.RevenueItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.RevenueItCode=I.ItCode And A.CtCode=I.CtCode  ");
           // SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Left Join TblTax K On K.TaxCode = B.TaxCode1 ");
            SQL.AppendLine("   Left Join TblTax L On L.TaxCode = B.TaxCode2 ");
            SQL.AppendLine("   Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T Order By DNo; ");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "RevenueItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "Price", "Discount", "DiscAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt1", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-25
                    "TotalVolume", "VolUom",  "TaxAmt2", "TaxCode1", "TaxName",
                    //26-30
                    "TaxCode2", "TaxName2", "RevenueBefTax", "TotalRevenueAmt", "InventoryUomCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 30);


                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 27, 28, 31, 34, 35 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        public void ShowResource(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ResourceItCode, A.Sequence, B.ItName,C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt ");
            SQL.AppendLine(", D.DocNo As RBPDocNo ");
            SQL.AppendLine("From TblBOQDtl6 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQL.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd6, ref cm2, SQL.ToString(),
                new string[]
                {
                    "ResourceItCode",
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2",
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

        }

        #endregion

        #region Additional Method

        private void PrintData(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<SOCHdr>();
            var ldtl = new List<SOCDtl>();
            var ldtl2 = new List<SOCDtl2>();
            var lSign = new List<SOCSign>();
           
            string[] TableName = { "SOCHdr", "SOCDtl", "SOCDtl2", "SOCSign" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As 'Kop', ");
           
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.Remark, C.ProjectName, D.CtName, DATE_FORMAT(B.QtStartDt,'%d %M %Y') As BOQStartDt, DATE_FORMAT(A.StartDt,'%d %M %Y') As StartDt, DATE_FORMAT(A.EndDt,'%d %M %Y') As EndDt, ");
            SQL.AppendLine("ifnull(E.SubTotal1, 0) SubTotal1, ifnull(F.SubTotal2, 0)SubTotal2, ifnull( E.SubTotal1-F.SubTotal2, 0) As NetRev, ifnull((E.SubTotal1-F.SubTotal2)/F.SubTotal2, 0) As GPM ");
            SQL.AppendLine("From TblSOCOntractHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLOphdr C On B.LOPDocNo = C.DocNO ");
            SQL.AppendLine("Inner Join TblCustomer D On B.CtCode = D.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine(" Select DocNo, Sum(Qty*UPrice) SubTotal1 ");
            SQL.AppendLine(" From TblSOContractDtl ");
            SQL.AppendLine(" Where DocNo = @DocNo ");
            SQL.AppendLine(" Group By DocNo");
            SQL.AppendLine(") E On E.DocNo = A.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine(" Select DocNo, Sum(Qty1*UPrice) SubTotal2 ");
            SQL.AppendLine(" From TblSOContractDtl6 ");
            SQL.AppendLine(" Where DocNo = @DocNo ");
            SQL.AppendLine(" Group By DocNo");
            SQL.AppendLine(") F On F.DocNo = A.DocNo ");

            SQL.AppendLine("Where A.DocNo=@DocNo ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
               
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyLogo",
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         
                         //6-10
                         "DocNo",
                         "DocDt",
                         "Remark",
                         "ProjectName",
                         "CtName",

                         //11-15
                         "BOQStartDt",
                         "EndDt",
                         "SubTotal1",
                         "SubTotal2",
                         "NetRev",

                         //16-17
                         "GPM",
                         "StartDt"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SOCHdr()
                        {

                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            ProjectName = Sm.DrStr(dr, c[9]),
                            CtName = Sm.DrStr(dr, c[10]),

                            BOQStartDt = Sm.DrStr(dr, c[11]),
                            EndDt = Sm.DrStr(dr, c[12]),
                            SubTotal1 = Sm.DrDec(dr, c[13]),
                            SubTotal2 = Sm.DrDec(dr, c[14]),
                            NetRev = Sm.DrDec(dr, c[15]),
                            GPM = Sm.DrDec(dr, c[16]),
                            StartDt = Sm.DrStr(dr, c[17]),


                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Remark, A.Qty, A.UPrice, A.Qty*A.UPrice As Amt ");
                SQLDtl.AppendLine("From TblSOContractDtl A  ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");


                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName",
                         "Remark" ,
                         "Qty",
                         "UPrice" ,
                         "Amt"
                        
                         
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new SOCDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Remark = Sm.DrStr(drDtl, cDtl[2]),
                            Qty = Sm.DrDec(drDtl, cDtl[3]),
                            UPrice = Sm.DrDec(drDtl, cDtl[4]),
                            Amt = Sm.DrDec(drDtl, cDtl[5]),
                           
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.ResourceItCode, B.ItName, A.Remark, A.Qty1, A.UPrice, A.Qty1*A.UPrice As Amt ");
                SQLDtl2.AppendLine("From TblSOContractDtl6 A  ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode=B.ItCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");


                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                        {
                         //0
                         "ResourceItCode" ,

                         //1-5
                         "ItName",
                         "Remark" ,
                         "Qty1",
                         "UPrice" ,
                         "Amt"


                        });
                if (drDtl2.HasRows)
                {
                    int nomor2 = 0;
                    while (drDtl2.Read())
                    {
                        nomor2 = nomor2 + 1;
                        ldtl2.Add(new SOCDtl2()
                        {
                            nomor = nomor2,
                            ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItName = Sm.DrStr(drDtl2, cDtl2[1]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[2]),
                            Qty = Sm.DrDec(drDtl2, cDtl2[3]),
                            UPrice = Sm.DrDec(drDtl2, cDtl2[4]),
                            Amt = Sm.DrDec(drDtl2, cDtl2[5]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail Signature
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;
               
                SQLDtl3.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt, A.EmpPict ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    From TblSOContractHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 6 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    ) A ");
                SQLDtl3.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode ");
                SQLDtl3.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode ");
                SQLDtl3.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt ");
                SQLDtl3.AppendLine("Order By A.Seq Desc; ");
                
                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();
               
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                        {
                        //0-4
                        "UserName",
                        "EmpPict",
                        "Posname",
                        "LastUpDt",
                        "Seq",
                        "Title",
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        lSign.Add(new SOCSign()
                        {
                            UserName = Sm.DrStr(drDtl3, cDtl3[0]),
                            EmpPict = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[3]),
                            Sequence = Sm.DrStr(drDtl3, cDtl3[4]),
                            Title = Sm.DrStr(drDtl3, cDtl3[5]),
                            Space = "                       ",
                            LabelName = "Name : ",
                            LabelPos = "Position : ",
                            LabelDt = "Date : ",

                        });
                    }
                   
                }
                drDtl3.Close();
            }
            myLists.Add(lSign);
            #endregion

            Sm.PrintReport("SOContractMNET", myLists, TableName, false);
           
        }

        #region Upload File

        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* SO Contract - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd8.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd8, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSOContractFile (DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd8, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName), LastUpBy = @UserCode , LastUpDt = @Dt; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd8, ref cm,
                   "Select A.DNo, A.FileName, B.UserName, A.CreateDt " +
                   "From  TblSOContractFile  A " +
                   "Inner Join TblUser B On A.CreateBy = B.UserCode " +
                   "Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "UserName", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd8, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd8, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName);
        }

        private bool IsUploadFileMandatory()
        {

            if (Grd8.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                TcSOContract.SelectedTabPage = TpUploadFile;
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if ( FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if ( FileName.Length > 0 && Sm.GetGrdStr(Grd8, Row, 1) != Sm.GetGrdStr(Grd8, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblSOContractFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }
        #endregion

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode, string ContactPersonName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode=@CtCode ");
            if (ContactPersonName.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @ContactPersonName As Col1 ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand();
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactPersonName);

            Sm.SetLue1(ref Lue, ref cm, "Contact Person");
            if (ContactPersonName.Length > 0) Sm.SetLue(Lue, ContactPersonName);

        }

        private bool IsRevisionNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'SOCRev' Limit 1; ");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueJOCode(ref LookUpEdit Lue, string JOCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select JOCode As Col1, JOName As Col2 ");
            SQL.AppendLine("From TblJointOperationHdr ");
            SQL.AppendLine("Where 0 = 0 ");
            
            if (JOCode.Length > 0)
                SQL.AppendLine("And JOCode = @JOCode ");
            else
                SQL.AppendLine("And ActInd = 'Y' ");
            
            SQL.AppendLine("Order By JOName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", JOCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var TaxRate = Sm.GetValue("Select TaxRate from TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        internal void ComputeTotalBOQ()
        {
            decimal Amt = 0m;
            string BOQDocNo = TxtBOQDocNo.Text;
            //Amt = Decimal.Parse(Sm.GetValue("Select SUM(A.totalAmt) From TblBoqDtl A "+
            //      "Inner Join TblItemBOQ B On A.ItBOQCode = B.ItBOQCode And B.Parent Is null " +
            //      "Where A.DocNo = '"+BOQDocNo+"' ; "));

            for (int i = 0; i < Grd2.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 10).Length > 0 && Sm.GetGrdStr(Grd2, i, 10) == "Y")
                {
                    Amt += Sm.GetGrdDec(Grd2, i, 9);
                }
            }
        
            BOQAmt = Amt;
            
           // ComputeItem();
        }

        internal void ComputeContractAmt()
        {
            decimal ContratAmtBef = 0m, ContractAmtAft = 0m, TotalResource = 0m;
            for (int row = 0; row < Grd3.Rows.Count; row++)
            {
                if (Sm.GetGrdDec(Grd3, row, 34) > 0)
                {
                    ContratAmtBef += Sm.GetGrdDec(Grd3, row, 34);
                }
            }

            for (int row = 0; row < Grd3.Rows.Count; row++)
            {
                if (Sm.GetGrdDec(Grd3, row, 35) > 0)
                {
                    ContractAmtAft += Sm.GetGrdDec(Grd3, row, 35);
                }
            }

            TotalResource = Sm.GetDecValue(TxtTotalResource.Text);
            TxtContractAmtBefTax.EditValue = Sm.FormatNum(ContratAmtBef, 0);
            TxtContractAmtAfterTax.EditValue = Sm.FormatNum(ContractAmtAft, 0);
            TxtProfitMarginAmt.EditValue = Sm.FormatNum(ContratAmtBef - TotalResource, 0);
            if (ContratAmtBef > 0)
            {
                TxtProfitMarginPerc.EditValue = Sm.FormatNum((ContratAmtBef - TotalResource) / (ContratAmtBef * 0.01m), 2);
            }


        }
        internal void ComputeItem(int r)
        {
            decimal Qty = 0m,  PriceGrid3 = 0m, TaxAmt = 0m, TaxPercentage = 0m, TaxAmt2 = 0m, DiscountAmt = 0m, Total = 0m ;
            string TaxCode = string.Empty, TaxCode2 = string.Empty;
            Qty = Sm.GetGrdDec(Grd3, r, 9);
            PriceGrid3 = Sm.GetGrdDec(Grd3, r, 11);            
            DiscountAmt = Sm.GetGrdDec(Grd3, r, 13);

            if (PriceGrid3 > 0 && Qty>0)
                Total = PriceGrid3 * Qty;

            Grd3.Cells[r, 14].Value = PriceGrid3; // Price After Discount
            Grd3.Cells[r, 16].Value = PriceGrid3; // Price Before Tax
            Grd3.Cells[r, 34].Value = Total - DiscountAmt; //Rev Bef Tax

            ComputeTax(r);
            TaxAmt = Sm.GetGrdDec(Grd3, r, 18);
            TaxAmt2 = Sm.GetGrdDec(Grd3, r, 31);

            Grd3.Cells[r, 19].Value = PriceGrid3 + TaxAmt + TaxAmt2; // Price After Tax
            Grd3.Cells[r, 20].Value = Total + TaxAmt + TaxAmt2; // Total
            Grd3.Cells[r, 35].Value = (Total - DiscountAmt) + TaxAmt + TaxAmt2; // Total Rev
            ComputeContractAmt();
        }

        private void ComputeTax(int r)
        {
            Grd3.Cells[r, 18].Value = Grd3.Cells[r, 31].Value = 0m;

            decimal Tax = 0m;
            string TaxCode = Sm.GetGrdStr(Grd3, r, 32);
            if (TaxCode.Length > 0 && Sm.GetGrdStr(Grd3, r, 34).Length > 0)
                Grd3.Cells[r, 18].Value = GetTaxRate(TaxCode) * 0.01m * Sm.GetGrdDec(Grd3, r, 34);

            string TaxCode2 = Sm.GetGrdStr(Grd3, r, 33);
            if (TaxCode2.Length > 0 && Sm.GetGrdStr(Grd3, r, 34).Length > 0)
                Grd3.Cells[r, 31].Value = GetTaxRate(TaxCode2) * 0.01m * Sm.GetGrdDec(Grd3, r, 34);
           


        }

        internal void ComputeTotal()
        {
            decimal Total = 0m,
                CostPerc = 0m,
                ContractAmtBefTax = 0m,
                RemunerationCost = 0m,
                DirectCost = 0m,
                IndirectCost = 0m, RemunerationPerc = 0m, DirectCostPerc = 0m, IndirectCostPerc = 0m;
            ;

            RemunerationCost = Sm.GetDecValue(TxtRemunerationCost.Text);
            DirectCost = Sm.GetDecValue(TxtDirectCost.Text);
            IndirectCost = Sm.GetDecValue(TxtIndirectCost.Text);

            Total = Sm.GetDecValue(TxtRemunerationCost.Text) + Sm.GetDecValue(TxtDirectCost.Text) + Sm.GetDecValue(TxtIndirectCost.Text);
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);
            if (Sm.GetDecValue(TxtContractAmtBefTax.Text) != 0)
            {
                ContractAmtBefTax = Sm.GetDecValue(TxtContractAmtBefTax.Text);
                CostPerc = Total / (ContractAmtBefTax * 0.01m);
                TxtCostPerc.EditValue = Sm.FormatNum(CostPerc, 0);
            }
            if (RemunerationCost > 0 && Total > 0) 
            {
                RemunerationPerc = RemunerationCost / (Total * 0.01m);
                DirectCostPerc = DirectCost / (Total * 0.01m);
                IndirectCostPerc = IndirectCost / (Total * 0.01m);
            }

            TxtRemunerationPerc.EditValue = Sm.FormatNum(RemunerationPerc, 2);
            TxtDirectPerc.EditValue = Sm.FormatNum(DirectCostPerc, 2);
            TxtIndirectPerc.EditValue = Sm.FormatNum(IndirectCostPerc, 2);
            TxtTotalPerc.EditValue = Sm.FormatNum(100m, 0);

        }
        internal void ComputeTotalResource()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeRemunerationCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForRemuneration && mItGrpCodeForRemuneration.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtRemunerationCost.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForDirectCost && mItGrpCodeForDirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtDirectCost.EditValue = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }

        internal void ComputeIndirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForIndirectCost && mItGrpCodeForIndirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtIndirectCost.EditValue = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }


        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mIsSOUseARDPValidated = Sm.GetParameterBoo("IsSOUseARDPValidated");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mJointOperationKSOType = Sm.GetParameter("JointOperationKSOType");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectAcNoFormula = Sm.GetParameter("ProjectAcNoFormula");
            mIsSOContractUseTax = Sm.GetParameterBoo("IsSOContractUseTax");
            mSOContractCOAFormat = Sm.GetParameter("SOContractCOAFormat");
            mIsCustomerContactPersonNonMandatory = Sm.GetParameterBoo("IsCustomerContactPersonNonMandatory");
            mIsSOContractJointOperationMandatory = Sm.GetParameterBoo("IsSOContractJointOperationMandatory");
            mIsShippingNameValidatedContactPerson = Sm.GetParameterBoo("IsShippingNameValidatedContactPerson");
            mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            mItGrpCodeForIndirectCost = Sm.GetParameter("ItGrpCodeForIndirectCost");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

            //Upload File
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");

            if (mSOContractCOAFormat.Length == 0) mSOContractCOAFormat = "1";
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
        }

        private string GenerateProjectSequenceNo(string SiteCode, string ProjectScope)
        {
            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(ProjectSequenceNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(Left(A.ProjectSequenceNo, 3), Decimal) As ProjectSequenceNo  ");
            SQL.Append("       From TblSOContractHdr A ");
            SQL.Append("       Inner Join TblBOQHdr B On A.BOQDocNo= B.DocNo ");
            SQL.Append("       Inner Join TblLOPHdr C On B.LOPDocNo= C.DocNo ");
            SQL.Append("       Where C.SiteCode='" + SiteCode + "' And C.ProjectScope='" + ProjectScope + "' ");
            SQL.Append("       Order By Left(ProjectSequenceNo, 3) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001') ");
            SQL.Append(") As ProjectSequenceNo");

            return Sm.GetValue(SQL.ToString());
           
        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }



        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCustomerContactperson_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCustomerContactperson, new Sm.RefreshLue3(SetLueCtPersonCode), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

      
        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueJOCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueJOCode, new Sm.RefreshLue2(SetLueJOCode), string.Empty);
            }
        }

        private void LueJOCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd5, ref fAccept, e);
            }
        }

        private void LueJOCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueJOCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueJOCode).Length == 0)
                        Grd5.Cells[fCell.RowIndex, 0].Value =
                        Grd5.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueJOCode);
                        Grd5.Cells[fCell.RowIndex, 1].Value = LueJOCode.GetColumnValue("Col2");
                    }
                    LueJOCode.Visible = false;
                }
            }
        }

        private void DteJODocDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteKeyDown(Grd5, ref fAccept, e);
            }
        }

        private void DteJODocDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteLeave(DteJODocDt, ref fCell, ref fAccept);
            }
        }

       

        #endregion

        #region Button Event


        private void BtnContact_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    //var f = new FrmCustomer(mMenuCode);
                    //f.Tag = mMenuCode;
                    //f.WindowState = FormWindowState.Normal;
                    //f.StartPosition = FormStartPosition.CenterScreen;
                    //f.mCtCode = Sm.GetLue(LueCtCode);
                    //f.ShowDialog();
                        
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCustomerContactperson, Sm.GetLue(LueCtCode), string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

      
      
        private void BtnSOMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtBOQDocNo.EditValue != null && TxtBOQDocNo.Text.Length != 0)
                        {
                            var f = new FrmBOQ(mMenuCode);
                            f.Tag = "XXX";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtBOQDocNo.Text;
                            f.ShowDialog();
                        }
                        else
                        {
                            var f = new FrmBOQ(mMenuCode);
                            f.Tag = "XXX";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mCtCode = Sm.GetLue(LueCtCode);
                            f.ShowDialog();
                            TxtBOQDocNo.EditValue = Sm.GetValue("Select MAX(DocNo) From TblBOQhdr Where CtCode = '" + Sm.GetLue(LueCtCode) + "' ");
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

       
        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }


        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmSOContract3Dlg(this, Sm.GetLue(LueCtCode));
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCustomerShipAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSOContract3Dlg2(this, Sm.GetLue(LueCtCode), Sm.GetValue("Select CtContactPersonName From tblBOQhdr Where DocNo ='" + TxtBOQDocNo.Text + "'")));
        }

        private void BtnCtShippingAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

      
        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCustomerContactperson.EditValue = null;
                    Sm.SetControlReadOnly(LueCustomerContactperson, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCustomerContactperson, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueCustomerContactperson, false);
                }

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueTaxCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueTaxCode1_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode1.Visible && fAccept && fCell.ColIndex == 17)
            {
                if (Sm.GetLue(LueTaxCode1).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 32].Value = null;
                    Grd3.Cells[fCell.RowIndex, 17].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 32].Value = Sm.GetLue(LueTaxCode1);
                    Grd3.Cells[fCell.RowIndex, 17].Value = LueTaxCode1.GetColumnValue("Col2");
                }
                ComputeItem(fCell.RowIndex);
                LueTaxCode1.Visible = false;
            }
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeItem(fCell.RowIndex);
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeItem(fCell.RowIndex);
        }

        private void LueTaxCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueTaxCode2_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode2.Visible && fAccept && fCell.ColIndex == 30)
            {
                if (Sm.GetLue(LueTaxCode2).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 33].Value = null;
                    Grd3.Cells[fCell.RowIndex, 30].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 33].Value = Sm.GetLue(LueTaxCode2);
                    Grd3.Cells[fCell.RowIndex, 30].Value = LueTaxCode2.GetColumnValue("Col2");
                }
                ComputeItem(fCell.RowIndex);
                LueTaxCode2.Visible = false;
            }
        }


        #endregion

        #endregion

        #region Class
        private class SOCHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string ProjectName { get; set; }
            public string CtName { get; set; }
            public string BOQStartDt { get; set; }
            public string EndDt { get; set; }
            public string PrintBy { get; set; }
            public decimal SubTotal1 { get; set; }
            public decimal SubTotal2 { get; set; }
            public decimal NetRev { get; set; }
            public decimal GPM { get; set; }
            public string StartDt { get; set; }

        }

        private class SOCDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }

        }

        private class SOCDtl2
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }

        }

        private class SOCSign
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string LastUpDt { get; set; }
            public string PosName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }

        }


        #endregion
    }
}


