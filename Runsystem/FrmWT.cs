﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWT : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mWTCode = string.Empty;
        internal FrmWTFind FrmFind;

        #endregion

        #region Constructor

        public FrmWT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Working Time";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                if (mWTCode.Length != 0)
                {
                    ShowData(mWTCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
          if (FrmFind != null) FrmFind.Close();
          
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWTCode, TxtWTName, TxtDailyWorkingHr, TxtWeeklyWorkingHr, TxtWeeklyWorkdays, 
                        TxtMonthlyWorkingHr, TxtAnnualWorkingHr
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    TxtWTCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWTCode, TxtWTName, TxtDailyWorkingHr, TxtWeeklyWorkingHr, TxtWeeklyWorkdays, 
                        TxtMonthlyWorkingHr, TxtAnnualWorkingHr
                    }, false);
                    TxtWTCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWTName, TxtDailyWorkingHr, TxtWeeklyWorkingHr, TxtWeeklyWorkdays, TxtMonthlyWorkingHr, 
                        TxtAnnualWorkingHr
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    TxtWTName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtWTCode, TxtWTName });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtDailyWorkingHr, TxtWeeklyWorkingHr, TxtWeeklyWorkdays, TxtMonthlyWorkingHr, TxtAnnualWorkingHr
            }, 0);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWTFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWTCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWTCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblWT Where WTCode=@WTCode" };
                Sm.CmParam<String>(ref cm, "@WTCode", TxtWTCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsInsert = !TxtWTCode.Properties.ReadOnly;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWT(WTCode, WTName, ActInd, DailyWorkingHr, WeeklyWorkingHr, WeeklyWorkdays, MonthlyWorkingHr, AnnualWorkingHr, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WTCode, @WTName, @ActInd, @DailyWorkingHr, @WeeklyWorkingHr, @WeeklyWorkdays, @MonthlyWorkingHr, @AnnualWorkingHr, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update WTName=@WTName, ActInd=@ActInd, ");
                SQL.AppendLine("   DailyWorkingHr=@DailyWorkingHr, WeeklyWorkingHr=@WeeklyWorkingHr, WeeklyWorkdays=@WeeklyWorkdays, ");
                SQL.AppendLine("   MonthlyWorkingHr=@MonthlyWorkingHr, AnnualWorkingHr=@AnnualWorkingHr, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@WTCode", TxtWTCode.Text);
                Sm.CmParam<String>(ref cm, "@WTName", TxtWTName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<Decimal>(ref cm, "@DailyWorkingHr", Decimal.Parse(TxtDailyWorkingHr.Text));
                Sm.CmParam<Decimal>(ref cm, "@WeeklyWorkingHr", Decimal.Parse(TxtWeeklyWorkingHr.Text));
                Sm.CmParam<Decimal>(ref cm, "@WeeklyWorkdays", Decimal.Parse(TxtWeeklyWorkdays.Text));
                Sm.CmParam<Decimal>(ref cm, "@MonthlyWorkingHr", Decimal.Parse(TxtMonthlyWorkingHr.Text));
                Sm.CmParam<Decimal>(ref cm, "@AnnualWorkingHr", Decimal.Parse(TxtAnnualWorkingHr.Text));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                
                Sm.ExecCommand(cm);

                if (IsInsert)
                    BtnInsertClick(sender, e);
                else
                    ShowData(TxtWTCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string WTCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@WTCode", WTCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select WTCode, WTName, ActInd, DailyWorkingHr, WeeklyWorkingHr, WeeklyWorkdays, MonthlyWorkingHr, AnnualWorkingHr " +
                        "From TblWT Where WTCode=@WTCode;",
                        new string[] 
                        {
                            "WTCode", 
                            "WTName", "ActInd", "DailyWorkingHr", "WeeklyWorkingHr", "WeeklyWorkdays", 
                            "MonthlyWorkingHr", "AnnualWorkingHr"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtWTCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtWTName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]),"Y");
                            TxtDailyWorkingHr.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                            TxtWeeklyWorkingHr.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                            TxtWeeklyWorkdays.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            TxtMonthlyWorkingHr.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                            TxtAnnualWorkingHr.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWTCode, "Working time code", false) ||
                Sm.IsTxtEmpty(TxtWTName, "Working time name", false) ||
                IsWTCodeExisted() ||
                IsWTNameExisted(); 
        }

        private bool IsWTCodeExisted()
        {
            if (!TxtWTCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select WTCode From TblWT Where WTCode=@WTCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@WTCode", TxtWTCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Working time code ( " + TxtWTCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWTNameExisted()
        {
            
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select WTCode From TblWT " +
                    "Where WTName=@WTName " +
                    (TxtWTCode.Properties.ReadOnly?"And WTCode<>@WTCode ":string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@WTName", TxtWTName.Text);
            if (TxtWTCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@WTCode", TxtWTCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Working time name ( " + TxtWTName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWTCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWTCode);
        }

        private void TxtWTName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWTName);
        }

        private void TxtDailyWorkingHr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDailyWorkingHr, 0);
        }

        private void TxtWeeklyWorkingHr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtWeeklyWorkingHr, 0);
        }

        private void TxtWeeklyWorkdays_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtWeeklyWorkdays, 0);
        }

        private void TxtMonthlyWorkingHr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMonthlyWorkingHr, 0);
        }

        private void TxtAnnualWorkingHr_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAnnualWorkingHr, 0);
        }

        #endregion

        #endregion

    }
}
