﻿#region Update
/*
    23/11/2021 [WED/RM] new apps
    16/12/2021 [WED/RM] bisa untuk show data, tapi tanpa find
    22/03/2022 [WED/RM] validasi Start Time dan End Time
                        validasi Open & Submit checked
                        validasi gak bisa insert baru untuk MR yang sama, ketika sudah masuk masa Submit
    06/04/2022 [WED/RM] validasi tanggal dokumen tidak boleh lebih besar dari Start Date terkecil di Procurement schedule
    20/04/2022 [WED/RM] validasi tanggal dokumen harus sama dengan tanggal hari ini, dan validasi tanggal yang sudah ada sebelumnya itu tetap ada juga
    27/06/2022 [VIN/PHT] bug show data -> kolom 6 -> DNo
 */
#endregion

#region Namespace
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Sm = RunSystem.StdMtd;
using Gv = RunSystem.GlobalVar;
using DXE = DevExpress.XtraEditors;
using Sl = RunSystem.SetLue;
using TenTec.Windows.iGridLib;
#endregion

namespace RunSystem
{
    public partial class FrmRFQ : RunSystem.FrmBase17
    {
        #region Field

        internal string 
            mAccessInd = string.Empty, 
            mMenuCode = string.Empty, 
            mMaterialRequestDocNo = string.Empty,
            mSeqNo = string.Empty
            ;
        private string mProcurementTypeCodeScheduleMandatory = string.Empty;

        internal List<VdSector> mVdSector = null;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor
        public FrmRFQ(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmRFQ");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                SetGrd();
                ClearGrd();
                SetFormControl(1);
                Sl.SetLueOption(ref LueProcurementType, "ProcurementType");
                Sm.SetDteCurrentDate(DteDocDt);

                mVdSector = new List<VdSector>();

                if (mMaterialRequestDocNo.Length > 0 && mSeqNo.Length > 0)
                {
                    ShowData(mMaterialRequestDocNo, mSeqNo);
                    BtnSave.Visible = false;
                    SetFormControl(2);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "SectorCode", 

                    //1-5
                    "", 
                    "Sector",
                    "SubSector Code",
                    "",
                    "Sub-Sector",

                    //6
                    "DNo"
                },
                new int[] 
                { 
                    0, 
                    20, 200, 0, 20, 200, 
                    0
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "VendorCode", 

                    //1-3
                    "",
                    "Vendor",
                    "DNo"
                },
                new int[]
                {
                    0,
                    20, 200, 0
                }
            );
            Sm.GrdColInvisible(Grd2, new int[] { 0, 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 2, 3 });
            Sm.GrdColButton(Grd2, new int[] { 1 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "DNo", 

                    //1-5
                    "Stage",
                    "Stage Name",
                    "Start Date",
                    "Start Time",
                    "End Date",

                    //6-8
                    "End Time",
                    "Submit",
                    "Open"
                },
                new int[]
                {
                    0,
                    80, 200, 100, 100, 100, 
                    100, 100, 100
                }
            );
            Sm.GrdColInvisible(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1 });
            Sm.GrdColCheck(Grd3, new int[] { 7, 8 });
            Sm.GrdFormatDate(Grd3, new int[] { 3, 5 });
            Sm.GrdFormatTime(Grd3, new int[] { 4, 6 });

            #endregion
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtMaterialRequestDocNo, MeeMaterialRequestRemark, LueProcurementType, DteDocDt,
                DteStartDt, DteEndDt, TmeStartTm, TmeEndTm
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
        }

        private void SetFormControl(byte Type)
        {
            if (Type == 1)
                DteDocDt.Focus();
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueProcurementType, DteDocDt,
                    DteStartDt, DteEndDt, TmeStartTm, TmeEndTm

                }, true);
                BtnMaterialRequestDocNo.Enabled = BtnMaterialRequestDocNo2.Enabled = false;
                Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = true;
            }
        }

        private void GetParameter()
        {
            mProcurementTypeCodeScheduleMandatory = Sm.GetParameter("ProcurementTypeCodeScheduleMandatory");
        }

        #endregion

        #region Grid Methods

        #region Grid 1
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 || e.ColIndex == 4)
                {
                    if (e.ColIndex == 1)
                    {
                        if (!Sm.IsTxtEmpty(TxtMaterialRequestDocNo, "MR#", false))
                        {
                            Sm.FormShowDialog(new FrmRFQDlg2(this, 1, e.RowIndex, string.Empty));
                        }
                    }

                    if (e.ColIndex == 4)
                    {
                        if (!Sm.IsTxtEmpty(TxtMaterialRequestDocNo, "MR#", false) && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 2, false, "Sector is empty."))
                        {
                            string mSectorCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                            Sm.FormShowDialog(new FrmRFQDlg2(this, 2, e.RowIndex, mSectorCode));
                        }
                    }
                }
            }
        }
        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnSave, BtnSave);
            }
        }

        #endregion

        #region Grid 2
        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    GetSectorData(ref mVdSector);
                    Sm.FormShowDialog(new FrmRFQDlg3(this));

                    if (mVdSector != null) mVdSector.Clear();
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                Sm.GrdKeyDown(Grd2, e, BtnSave, BtnSave);
            }
        }

        #endregion

        #region Grid 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                {
                    if (e.ColIndex == 3) Sm.DteRequestEdit(Grd3, DteStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd3, DteEndDt, ref fCell, ref fAccept, e);

                    if (e.ColIndex == 4) Sm.TmeRequestEdit(Grd3, TmeStartTm, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 6) Sm.TmeRequestEdit(Grd3, TmeEndTm, ref fCell, ref fAccept, e);
                }

                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdBoolValueFalse(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
                SetGrdNo(Grd3, 1);
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdKeyDown(Grd3, e, BtnSave, BtnSave);
                SetGrdNo(Grd3, 1);
            }
        }

        #endregion

        #endregion

        #region Save
        override protected void SaveData()
        {
            if (Sm.StdMsgYN("Question", "Do you want to send this data ?") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            InsertData();

            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            SetFormControl(1);
        }

        private bool IsInsertedDataNotValid()
        {
            return (
                Sm.IsTxtEmpty(TxtMaterialRequestDocNo, "MR#", false) ||
                Sm.IsLueEmpty(LueProcurementType, "Procurement Type") ||
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecord() ||
                IsInsertedStageNotValid() ||
                IsDocDtInvalid()
                );
        }

        private bool IsDocDtInvalid()
        {
            if (Grd3.Rows.Count <= 1) return false;

            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string CurrDt = Sm.Left(Sm.GetValue("Select CurrentDateTime()"), 8);
            string MinStartDt = string.Empty;
            int index = 0;

            if (DocDt != CurrDt)
            {
                var msg = new StringBuilder();

                msg.AppendLine("Document Date : " + string.Concat(
                    Sm.Right(DocDt, 2), "/", DocDt.Substring(4, 2), "/", Sm.Left(DocDt, 4)
                    ));
                msg.AppendLine("Current Date : " + string.Concat(
                    Sm.Right(CurrDt, 2), "/", CurrDt.Substring(4, 2), "/", Sm.Left(CurrDt, 4)
                    ));
                msg.AppendLine(Environment.NewLine);
                msg.AppendLine("Document date should be the same as current date. ");

                Sm.StdMsg(mMsgType.Warning, msg.ToString());
                DteDocDt.Focus();
                return true;
            }

            for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
            {
                if (MinStartDt.Length == 0) MinStartDt = Sm.Left(Sm.GetGrdDate(Grd3, i, 3), 8);
                string CurrStartDt = Sm.Left(Sm.GetGrdDate(Grd3, i, 3), 8);

                if (MinStartDt != CurrStartDt)
                {
                    if (decimal.Parse(CurrStartDt) < decimal.Parse(MinStartDt))
                    {
                        MinStartDt = CurrStartDt;
                        index = i;
                    }
                }
            }

            if (decimal.Parse(DocDt) > decimal.Parse(MinStartDt))
            {
                Tc1.SelectedTab = Tp3;
                Sm.FocusGrd(Grd3, index, 3);
                Sm.StdMsg(mMsgType.Warning, "Document date should be earlier than or equal to the earliest start date on procurement schedule.");

                return true;
            }

            return false;
        }

        private bool IsInsertedStageNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(StartDt, StartTm) ");
            SQL.AppendLine("From TblRFQDtl3 ");
            SQL.AppendLine("Where MaterialRequestDocNo = @Param ");
            SQL.AppendLine("And SubmitInd = 'Y' ");
            SQL.AppendLine("Order By SeqNo, DNo ");
            SQL.AppendLine("Limit 1; ");

            string SubmitDtTm = Sm.GetValue(SQL.ToString(), TxtMaterialRequestDocNo.Text);

            if (SubmitDtTm.Length > 0)
            {
                string CurrTime = Sm.GetValue("Select Date_Format(Now(), '%Y%m%d%H%i'); ");

                if (decimal.Parse(SubmitDtTm) < decimal.Parse(CurrTime))
                {
                    var mMsg = new StringBuilder();
                    string LatestSubmitStage = string.Concat(
                        SubmitDtTm.Substring(6, 2), "/",
                        SubmitDtTm.Substring(4, 2), "/",
                        SubmitDtTm.Substring(0, 4), " ",
                        SubmitDtTm.Substring(8, 2), ":",
                        Sm.Right(SubmitDtTm, 2)
                        );

                    mMsg.AppendLine("Latest Submit Stage : " + LatestSubmitStage);
                    mMsg.AppendLine("You could not process this update due to it exceed latest submit stage.");

                    Sm.StdMsg(mMsgType.Warning, mMsg.ToString());
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 sector and subsector.");
                Tc1.SelectedTab = Tp1;
                return true;
            }

            bool IsProcurementScheduleMandatory = false;
            ProcessMandatoryProcurementSchedule(ref IsProcurementScheduleMandatory);

            if (IsProcurementScheduleMandatory)
            {
                if (Grd3.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 procurement schedule, due to your selected Procurement Type.");
                    Tc1.SelectedTab = Tp3;
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (
            Grd1Validation(Grd1, Tp1) ||
            Grd2Validation(Grd2, Tp2) ||
            Grd3Validation(Grd3, Tp3)
                ) return true;

            return false;
        }

        private bool Grd1Validation(iGrid Grd, TabPage Tp)
        {
            for (int i = 0; i < Grd.Rows.Count - 1; ++i)
            {
                if (Sm.IsGrdValueEmpty(Grd, i, 2, false, "Sector is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 2); return true; }
                if (Sm.IsGrdValueEmpty(Grd, i, 5, false, "Sub Sector is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 5); return true; }
                
                for (int j = (i+1); j < Grd.Rows.Count - 1; ++j)
                {
                    string SectorSub1 = string.Concat(Sm.GetGrdStr(Grd, i, 0), "#", Sm.GetGrdStr(Grd, i, 3));
                    string SectorSub2 = string.Concat(Sm.GetGrdStr(Grd, j, 0), "#", Sm.GetGrdStr(Grd, j, 3));

                    if (Sm.CompareStr(SectorSub1, SectorSub2))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate Sector and SubSector found.");
                        Tc1.SelectedTab = Tp;
                        Sm.FocusGrd(Grd, j, 5);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool Grd2Validation(iGrid Grd, TabPage Tp)
        {
            for (int i = 0; i < Grd.Rows.Count - 1; ++i)
            {
                if (Sm.IsGrdValueEmpty(Grd, i, 2, false, "Vendor is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 2); return true; }

                for (int j = (i + 1); j < Grd.Rows.Count - 1; ++j)
                {
                    string Vendor1 = Sm.GetGrdStr(Grd, i, 0);
                    string Vendor2 = Sm.GetGrdStr(Grd, j, 0);

                    if (Sm.CompareStr(Vendor1, Vendor2))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate Vendor found.");
                        Tc1.SelectedTab = Tp;
                        Sm.FocusGrd(Grd, j, 5);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool Grd3Validation(iGrid Grd, TabPage Tp)
        {
            int SubmitCounter = 0, OpenCounter = 0;
            bool IsSubmitChecked = false;

            for (int i = 0; i < Grd.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd, i, 7))
                {
                    SubmitCounter += 1;
                    IsSubmitChecked = true;
                }

                if (Sm.GetGrdBool(Grd, i, 8))
                {
                    OpenCounter += 1;

                    //Open only checked when submit is checked before
                    if (!IsSubmitChecked)
                    {
                        Tc1.SelectedTab = Tp;
                        Sm.FocusGrd(Grd, i, 8);
                        Sm.StdMsg(mMsgType.Warning, "You need to check the Submit Stage before you can check the Open Stage.");
                        return true;
                    }

                    IsSubmitChecked = false;
                }

                if (Sm.IsGrdValueEmpty(Grd, i, 2, false, "Stage Name is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 2); return true; }
                if (Sm.IsGrdValueEmpty(Grd, i, 3, false, "Start Date is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 3); return true; }
                if (Sm.IsGrdValueEmpty(Grd, i, 4, false, "Start Time is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 4); return true; }
                if (Sm.IsGrdValueEmpty(Grd, i, 5, false, "End Date is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 5); return true; }
                if (Sm.IsGrdValueEmpty(Grd, i, 6, false, "End Time is empty.")) { Tc1.SelectedTab = Tp; Sm.FocusGrd(Grd, i, 6); return true; }

                //Start Date+Time and End Date+Time Validation
                string StartDtTm = string.Concat(Sm.Left(Sm.GetGrdDate(Grd, i, 3), 8), Sm.GetGrdStr(Grd, i, 4).Replace(":", string.Empty));
                string EndDtTm = string.Concat(Sm.Left(Sm.GetGrdDate(Grd, i, 5), 8), Sm.GetGrdStr(Grd, i, 6).Replace(":", string.Empty));

                if (decimal.Parse(StartDtTm) > decimal.Parse(EndDtTm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Start Date and Time should be earlier than or equal to End Date and Time.");
                    Tc1.SelectedTab = Tp;
                    Sm.FocusGrd(Grd, i, 4);
                    return true;
                }
                //End Start Date+Time and End Date+Time Validation
            }

            if ((SubmitCounter == OpenCounter) && SubmitCounter == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to confirm submit and open quotation indicator.");
                Tc1.SelectedTab = Tp;
                return true;
            }

            if (SubmitCounter != OpenCounter)
            {
                Sm.StdMsg(mMsgType.Warning, "Submit and Open quotation should be equal.");
                Tc1.SelectedTab = Tp;
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecord()
        {
            if (GrdExceedMaxRecord(Grd1, Tp1)) return true;
            if (GrdExceedMaxRecord(Grd2, Tp2)) return true;
            if (GrdExceedMaxRecord(Grd3, Tp3)) return true;

            return false;
        }

        private bool GrdExceedMaxRecord(iGrid Grd, TabPage Tp)
        {
            if (Grd.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds maximum limit (999).");
                Tc1.SelectedTab = Tp;
                return true;
            }

            return false;
        }

        private void InsertData()
        {
            var cml = new List<MySqlCommand>();
            string SeqNo = string.Empty;
            GetSeqNo(ref SeqNo);

            cml.Add(SaveRFQHdr(SeqNo));

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                cml.Add(SaveRFQDtl(Grd1, SeqNo, i));

            for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                cml.Add(SaveRFQDtl2(Grd2, SeqNo, i));

            if (Grd3.Rows.Count > 1)
                for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                    cml.Add(SaveRFQDtl3(Grd3, SeqNo, i));

            cml.Add(SaveMaterialRequestDtl4(SeqNo));

            Sm.ExecCommands(cml);

            Sm.StdMsg(mMsgType.Info, "Success sent data to Available Request.");
        }

        private MySqlCommand SaveRFQHdr(string SeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRFQHdr(MaterialRequestDocNo, SeqNo, DocDt, ProcurementType, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@MaterialRequestDocNo, @SeqNo, @DocDt, @ProcurementType, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetLue(LueProcurementType));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRFQDtl(iGrid Grd, string SeqNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRFQDtl(MaterialRequestDocNo, SeqNo, DNo, SectorCode, SubSectorCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@MaterialRequestDocNo, @SeqNo, @DNo, @SectorCode, @SubSectorCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", (Row+1).ToString()), 3));
            Sm.CmParam<String>(ref cm, "@SectorCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@SubSectorCode", Sm.GetGrdStr(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRFQDtl2(iGrid Grd, string SeqNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRFQDtl2(MaterialRequestDocNo, SeqNo, DNo, VdCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@MaterialRequestDocNo, @SeqNo, @DNo, @VdCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", (Row + 1).ToString()), 3));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRFQDtl3(iGrid Grd, string SeqNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRFQDtl3(MaterialRequestDocNo, SeqNo, DNo, StageName, StartDt, EndDt, StartTm, EndTm, SubmitInd, OpenInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@MaterialRequestDocNo, @SeqNo, @DNo, @StageName, @StartDt, @EndDt, @StartTm, @EndTm, @SubmitInd, @OpenInd, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", (Row + 1).ToString()), 3));
            Sm.CmParam<String>(ref cm, "@StageName", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd, Row, 3));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd, Row, 5));
            string Tm1 = Sm.GetGrdStr(Grd, Row, 4);
            Sm.CmParam<String>(ref cm, "@StartTm", string.Concat(Sm.Left(Tm1, 2), Sm.Right(Tm1, 2)));
            string Tm2 = Sm.GetGrdStr(Grd, Row, 6);
            Sm.CmParam<String>(ref cm, "@EndTm", string.Concat(Sm.Left(Tm2, 2), Sm.Right(Tm2, 2)));
            Sm.CmParam<String>(ref cm, "@SubmitInd", Sm.GetGrdBool(Grd, Row, 7) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@OpenInd", Sm.GetGrdBool(Grd, Row, 8) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl4(string SeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl4(DocNo, SeqNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SeqNo, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtMaterialRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        private void ShowData(string MaterialRequestDocNo, string SeqNo)
        {
            ShowDataHdr(MaterialRequestDocNo, SeqNo);
            ShowDataDtl(MaterialRequestDocNo, SeqNo);
            ShowDataDtl2(MaterialRequestDocNo, SeqNo);
            ShowDataDtl3(MaterialRequestDocNo, SeqNo);
        }

        private void ShowDataHdr(string MaterialRequestDocNo, string SeqNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);

            SQL.AppendLine("Select A.MaterialRequestDocNo, B.Remark, A.ProcurementType, A.DocDt ");
            SQL.AppendLine("From TblRFQHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo = B.DocNo ");
            SQL.AppendLine("    And A.MaterialRequestDocNo = @MaterialRequestDocNo ");
            SQL.AppendLine("    And A.SeqNo = @SeqNo ");
            SQL.AppendLine("; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    "MaterialRequestDocNo",
                    "Remark", "ProcurementType", "DocDt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtMaterialRequestDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    MeeMaterialRequestRemark.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetLue(LueProcurementType, Sm.DrStr(dr, c[2]));
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[3]));
                }, true
            );
        }
        
        private void ShowDataDtl(string MaterialRequestDocNo, string SeqNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);

            SQL.AppendLine("Select A.SectorCode, B.SectorName, A.SubSectorCode, C.SubSectorName, A.DNo ");
            SQL.AppendLine("From TblRFQDtl A ");
            SQL.AppendLine("Inner Join TblSector B On A.SectorCode = B.SectorCode ");
            SQL.AppendLine("    And A.MaterialRequestDocNo = @MaterialRequestDocNo ");
            SQL.AppendLine("    And A.SeqNo = @SeqNo ");
            SQL.AppendLine("Left Join TblSubSector C On A.SectorCode = C.SectorCode ");
            SQL.AppendLine("    And A.SubSectorCode = C.SubSectorCode ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "SectorCode",
 
                    //1-4
                    "SectorName", "SubSectorCode", "SubSectorName", "DNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDataDtl2(string MaterialRequestDocNo, string SeqNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);

            SQL.AppendLine("Select A.VdCode, B.VdName, A.DNo ");
            SQL.AppendLine("From TblRFQDtl2 A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("    And A.MaterialRequestDocNo = @MaterialRequestDocNo ");
            SQL.AppendLine("    And A.SeqNo = @SeqNo ");
            SQL.AppendLine("Order by A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "VdCode",
 
                    //1-2
                    "VdName", "DNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowDataDtl3(string MaterialRequestDocNo, string SeqNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", MaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);

            SQL.AppendLine("Select DNo, StageName, StartDt, Concat(Left(StartTm, 2), ':', Right(StartTm, 2)) StartTm, ");
            SQL.AppendLine("EndDt, Concat(Left(EndTm, 2), ':', Right(EndTm, 2)) EndTm, SubmitInd, OpenInd ");
            SQL.AppendLine("From TblRFQDtl3 ");
            SQL.AppendLine("Where MaterialRequestDocNo = @MaterialRequestDocNo ");
            SQL.AppendLine("And SeqNo = @SeqNo ");
            SQL.AppendLine("Order By DNo; ");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "StageName", "StartDt", "StartTm", "EndDt", "EndTm",

                    //6-7
                    "SubmitInd", "OpenInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Grd3.Cells[Row, 1].Value = (Row + 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd3, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("B", Grd3, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("B", Grd3, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void GetSeqNo(ref string SeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select (Count(*) + 1) ");
            SQL.AppendLine("From TblRFQHdr ");
            SQL.AppendLine("Where MaterialRequestDocNo = @Param ");
            SQL.AppendLine("; ");

            SeqNo = Sm.GetValue(SQL.ToString(), TxtMaterialRequestDocNo.Text);
        }

        private void ProcessMandatoryProcurementSchedule(ref bool IsProcurementScheduleMandatory)
        {
            if (mProcurementTypeCodeScheduleMandatory.Length == 0) return;

            string[] datas = mProcurementTypeCodeScheduleMandatory.Trim().Split(',');
            string ProcurementType = Sm.GetLue(LueProcurementType);

            foreach (var x in datas)
            {
                if (ProcurementType == x)
                {
                    IsProcurementScheduleMandatory = true;
                    break;
                }
            }
        }

        internal string GetSelectedVdCode()
        {
            string data = string.Empty;

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (data.Length > 0) data += ",";
                    data += Sm.GetGrdStr(Grd2, i, 0);
                }
            }

            return data;
        }

        private void SetGrdNo(iGrid Grd, int Cols)
        {
            if (Grd.Rows.Count > 1)
            {
                int No = 1;
                for (int i = 0; i < Grd.Rows.Count - 1; ++i)
                {
                    Grd.Cells[i, Cols].Value = No;
                    No += 1;
                }
            }
        }

        private void GetSectorData(ref List<VdSector> l)
        {
            mVdSector.Clear();
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    l.Add(new VdSector()
                    {
                        SectorCode = Sm.GetGrdStr(Grd1, i, 0),
                        SubSectorCode = Sm.GetGrdStr(Grd1, i, 3),
                    });
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueProcurementType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcurementType, new Sm.RefreshLue2(Sl.SetLueOption), "ProcurementType");
            }
        }

        private void DteStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteKeyDown(Grd3, ref fAccept, e);
            }
        }

        private void DteStartDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteLeave(DteStartDt, ref fCell, ref fAccept);
            }
        }

        private void DteEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteKeyDown(Grd3, ref fAccept, e);
            }
        }

        private void DteEndDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.DteLeave(DteEndDt, ref fCell, ref fAccept);
            }
        }

        private void TmeStartTm_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.TmeKeyDown(Grd3, ref fAccept, e);
        }

        private void TmeStartTm_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TmeLeave(TmeStartTm, ref fCell, ref fAccept);
        }

        private void TmeEndTm_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.TmeKeyDown(Grd3, ref fAccept, e);
        }

        private void TmeEndTm_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TmeLeave(TmeEndTm, ref fCell, ref fAccept);
        }

        #endregion

        #region Button Click

        private void BtnMaterialRequestDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmRFQDlg(this));
            }
        }

        private void BtnMaterialRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtMaterialRequestDocNo, "MR#", false))
            {
                var f = new FrmMaterialRequest(mMenuCode)
                {
                    Tag = "***",
                    Text = Sm.GetMenuDesc("FrmMaterialRequest"),
                    WindowState = FormWindowState.Normal,
                    StartPosition = FormStartPosition.CenterScreen,
                    mDocNo = TxtMaterialRequestDocNo.Text
                };
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class VdSector
        {
            public string SectorCode { get; set; }
            public string SubSectorCode { get; set; }
        }

        #endregion
    }
}
