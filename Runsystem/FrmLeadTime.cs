﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmLeadTime : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmLeadTimeFind FrmFind;

        #endregion

        #region Constructor

        public FrmLeadTime(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Item Lead Time";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetLueItCtCode(ref LueItCtCode, "");
                SetLueItGrpCode(ref LueItGrpCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Lead Time"+Environment.NewLine+"(Day)",

                        //6
                        "Remark"
                    },
                     new int[] 
                    {
                        0,
                        20, 100, 20, 300, 120,
                        300
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkActInd, LueItCtCode, LueItGrpCode, 
                        LueItScCode, TxtDocNoSource, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 6 });
                    BtnSourceLeadTime.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueItGrpCode, LueItCtCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 6 });
                    BtnSourceLeadTime.Enabled = true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueItCtCode, LueItScCode, LueItGrpCode,
                TxtDocNoSource, MeeRemark
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {   
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeadTimeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemLeadTime", "TblItemLeadTimeHdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveItemLeadTimeHdr(DocNo));
                if (Grd1.Rows.Count > 1)
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveItemLeadTimeDtl(DocNo, Row));
                
                Sm.ExecCommands(cml);
                
                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
     
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmLeadTimeDlg(this, Sm.GetLue(LueItCtCode), Sm.GetLue(LueItScCode), Sm.GetLue(LueItGrpCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmLeadTimeDlg(this, Sm.GetLue(LueItCtCode), Sm.GetLue(LueItScCode), Sm.GetLue(LueItGrpCode)));
            }
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy lead based on first record ?") == DialogResult.Yes)
                {
                    if (Sm.GetGrdStr(Grd1, 0, 5).Length != 0)
                    {
                        var LeadTime = Sm.GetGrdDec(Grd1, 0, 5);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                                Grd1.Cells[Row, 5].Value = LeadTime;
                        }
                    }
                }
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowItemLeadTimeHdr(DocNo);
                ShowItemLeadTimeDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowItemLeadTimeHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                          "Select DocNo, DocDt, ActInd, ItCtCode, ItScCode, Remark " +
                          "From TblItemLeadTimeHdr Where DocNo = @DocNo;",
                         new string[] 
                           {
                              //0
                              "DocNo",
                              //1-5
                              "DocDt", "ActInd", "ItCtCode", "ItScCode", "Remark",

                           },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                             ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                             SetLueItCtCode(ref LueItCtCode, Sm.DrStr(dr, c[0]));
                             Sm.SetLue(LueItCtCode, Sm.DrStr(dr, c[3]));
                             SetLueItScCode(ref LueItScCode, Sm.GetLue(LueItCtCode));
                             Sm.SetLue(LueItScCode, Sm.DrStr(dr, c[4])); 
                             MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                         }, true
                     );
        }

        public void ShowItemLeadTimeDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.DNo,A.ItCode,B.ItName, A.LeadTimeValue, A.Remark "+
                    "From TblItemLeadTimeDtl A "+
                    "Left Join TblItem B On A.ItCode=B.ItCode "+
                    "Where A.DocNo=@DocNo Order By B.ItName",

                    new string[] 
                       { 
                           //0
                           "DNo",
                           //1-5
                           "ItCode","ItName","LeadTimeValue","Remark",

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueItCtCode, "Category") ||
                IsGrd1Empty() ||
                IsGrd1ValueNotValid();
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record item .");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item Name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Lead Time Value is empty.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveItemLeadTimeHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemLeadTimeHdr Set ActInd='N' Where ActInd='Y' And ItCtCode=@ItCtCode ");
            if(Sm.GetLue(LueItScCode).Length>0)
                SQL.AppendLine("And ItScCode=@ItScCode ");
            SQL.AppendLine(" ;");

            SQL.AppendLine("Insert Into TblItemLeadTimeHdr(DocNo, DocDt, ActInd, ItCtCode, ItScCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @ActInd, @ItCtCode, @ItScCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueItScCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemLeadTimeDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemLeadTimeDtl(DocNo,DNo,ItCode,LeadTimeValue,Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo,@DNo,@ItCode,@LeadTimeValue,@Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@LeadTimeValue", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLueItGrpCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("    Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ActInd = 'Y'  ");
            SQL.AppendLine(") Tbl Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItScCode(ref LookUpEdit Lue, string ItCtCode)
        {
            var SQL = new StringBuilder();

            if (TxtDocNo.Text.Length != 0)
            {
                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "'  ");
                SQL.AppendLine("Union All");
                SQL.AppendLine("Select B.ItScCode As Col1, A.ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItScCode = B.ItScCode ");
                SQL.AppendLine("Where B.ItCode='" + TxtDocNo.Text + "'  ");
                SQL.AppendLine(")Tbl Order By Col2 ");
            }
            else
            {
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "' Order By ItScName ");
            }

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLueItCtCode(ref LookUpEdit Lue, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Where ActInd = 'Y'  ");
            if (TxtDocNo.Text.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.ItCtCode As Col1, A.ItCtName As Col2 From TblItemCategory A ");
                SQL.AppendLine("Inner Join TblItemLeadTimeHdr B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("Where B.DocNo = '" + DocNo + "' ");
            }
            SQL.AppendLine(")Tbl Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 2) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            ClearData2();
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(SetLueItCtCode), TxtDocNo.Text);
            Sm.ClearGrd(Grd1, true);
            var ItCtCode = Sm.GetLue(LueItCtCode);
            LueItScCode.EditValue = null;
            if (ItCtCode.Length != 0)
                SetLueItScCode(ref LueItScCode, ItCtCode);
           
            
            Sm.SetControlReadOnly(LueItScCode, ItCtCode.Length == 0);
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void LueItScCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItScCode, new Sm.RefreshLue2(SetLueItScCode), Sm.GetLue(LueItCtCode));
        }

        private void BtnSourceLeadTime_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueItCtCode, "Category"))
            {
                TxtDocNoSource.Text = string.Empty;
                Sm.ClearGrd(Grd1, true);
                Sm.FormShowDialog(new FrmLeadTimeDlg2(this, Sm.GetLue(LueItCtCode)));
            }
        }

        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue1(SetLueItGrpCode));
        }


        #endregion

       
    

      

        #endregion        
    }
}
