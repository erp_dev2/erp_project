﻿#region Update
/*
    05/07/2018 [TKG] department tidak mandatory.
    13/06/2019 [WED] BUG di query Process2_1
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHADlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHA mFrmParent;
        private string
            mSQL = string.Empty,
            mSalaryInd = string.Empty,
            mHolidayDt = string.Empty,
            mDeptCode = string.Empty,
            mSiteCode = string.Empty,
            mADCodeFunctionalAllowance = string.Empty,
            mADCodeFieldAssignment = string.Empty,
            mADCodeSalary = string.Empty,
            mPayrunCode = string.Empty;
        private decimal
            mFunctionalExpenses = 0m,
            mFunctionalExpensesMaxAmt = 0m,
            mNonNPWPTaxPercentage = 0m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m;
        private List<TI> mlTI = null;
        private List<NTI> mlNTI = null;

        #endregion

        #region Constructor

        public FrmRHADlg(
            FrmRHA FrmParent, 
            string SalaryInd, 
            string HolidayDt, 
            string DeptCode, 
            string SiteCode,
            string PayrunCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSalaryInd = SalaryInd;
            mHolidayDt = HolidayDt;
            mDeptCode = DeptCode;
            mSiteCode = SiteCode;
            mPayrunCode = PayrunCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                GetValue();
                GetData();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                if (mDeptCode.Length > 0)
                {
                    Sm.SetLue(LueDeptCode, mDeptCode);
                    Sm.SetControlReadOnly(LueDeptCode, true);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mADCodeFunctionalAllowance = Sm.GetParameter("ADCodeFunctionalAllowance");
            mADCodeFieldAssignment = Sm.GetParameter("ADCodeFieldAssignment");
            mADCodeSalary = Sm.GetParameter("ADCodeSalary");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
            mNonNPWPTaxPercentage = Sm.GetParameterDec("NonNPWPTaxPercentage");
        }

        private void GetValue()
        {
            if (mSalaryInd == "2")
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Sum(EmployerPerc) ");
                SQL.AppendLine("From TblSS ");
                SQL.AppendLine("Where Find_In_Set(");
                SQL.AppendLine("    IfNull(SSCode, ''), ");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSCodeEmployment'), '') ");
                SQL.AppendLine("    );");

                var SSEmploymentErPerc = Sm.GetValue(SQL.ToString());
                if (SSEmploymentErPerc.Length > 0) mSSEmploymentErPerc = decimal.Parse(SSEmploymentErPerc);

                SQL.Length = 0;
                SQL.Capacity = 0;

                SQL.AppendLine("Select EmployeePerc ");
                SQL.AppendLine("From TblSS ");
                SQL.AppendLine("Where SSCode In (");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance'), '') ");
                SQL.AppendLine("    );");

                var SSOldAgeEePerc = Sm.GetValue(SQL.ToString());
                if (SSOldAgeEePerc.Length > 0) mSSOldAgeEePerc = decimal.Parse(SSOldAgeEePerc);

                SQL.Length = 0;
                SQL.Capacity = 0;

                SQL.AppendLine("Select EmployeePerc ");
                SQL.AppendLine("From TblSS ");
                SQL.AppendLine("Where SSCode In (");
                SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSForRetiring'), '') ");
                SQL.AppendLine("    );");

                var SSPensionEePerc = Sm.GetValue(SQL.ToString());
                if (SSPensionEePerc.Length > 0) mSSPensionEePerc = decimal.Parse(SSPensionEePerc);
            }
        }

        private void GetData()
        {
            mlTI = new List<TI>();
            mlNTI = new List<NTI>();

            ProcessTI(ref mlTI);
            ProcessNTI(ref mlNTI);
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",

                        //6-10
                        "Grade Level",
                        "Amount",
                        "Tax",
                        "Total",
                        "Payrun Period",

                        //11
                        "Department"
                      },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 250, 80, 150, 
                        
                        //6-10
                        150, 0, 0, 130, 170,

                        //11
                        200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);
            Grd1.Cols[11].Move(6);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[Row1, 0].Value = mFrmParent.Grd1.Rows.Count - 1;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), EmpCode)) return true;
            return false;
        }

        override protected void ShowData()
        {
            if (mSalaryInd == "1") ShowData1();
            if (mSalaryInd == "2") ShowData2();
        }

        #region IOK

        private void ShowData1()
        {
            Sm.ClearGrd(Grd1, false);
            var l = new List<EmpRHA1>();
            try
            {
                Process1_1(ref l);
                Process1_2(ref l);
                Process1_3(ref l);

                if (l.Count <= 0) Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void Process1_1(ref List<EmpRHA1> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mFrmParent.Grd1.Rows.Count; i++)
            {
                EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, i, 1);
                if (EmpCode.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " And ";
                    Filter += " (A.EmpCode<>@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length != 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=0 ";

            Filter2 = Filter;

            Sm.FilterStr(ref Filter2, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LuePayrunPeriod), "A.PayrunPeriod", true);

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, ");
            SQL.AppendLine("C.GrdLvlName, H.OptDesc As PayrunPeriodDesc, I.DeptName, A.JoinDt, IfNull(A.TGDt, A.JoinDt) As TGDt, ");
            SQL.AppendLine("A.PTKP, A.NPWP, ");
            SQL.AppendLine("C.BasicSalary As Salary, ");
            SQL.AppendLine("D.TaxableFixedAllowance, ");
            SQL.AppendLine("E.SSEmployerHealth, ");
            SQL.AppendLine("F.SSEmployerEmployment, ");
            SQL.AppendLine("G.SSEmployeeEmployment ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where (A.StartDt Is Null Or (A.StartDt Is Not Null And A.StartDt<=@CurrentDate)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    And T1.SSPCode=@SSPCodeForHealth ");
            SQL.AppendLine("    And T1.DocNo=@DocNoSSPHealth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    And T1.SSPCode=@SSPCodeForEmployment ");
            SQL.AppendLine("    And T1.DocNo=@DocNoSSPEmployment");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            //SQL.AppendLine("    And T1.SSPCode=@SSPCodeForEmployment ");
            SQL.AppendLine("    And T1.DocNo=@DocNoSSPEmployment ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
            SQL.AppendLine("Left Join TblOption H On A.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblDepartment I On A.DeptCode=I.DeptCode ");
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@CurrentDate) Or A.ResignDt Is Null) ");
            //SQL.AppendLine("And A.DeptCode=@DeptCode ");
            SQL.AppendLine(Filter2);
            if (mSiteCode.Length > 0) SQL.AppendLine("And A.SiteCode=@SiteCode ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParamDt(ref cm, "@HolidayDt", mHolidayDt);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
            Sm.CmParam<String>(ref cm, "@DocNoSSPHealth", GetDocNo(Sm.GetParameter("SSPCodeForHealth")));
            Sm.CmParam<String>(ref cm, "@DocNoSSPEmployment", GetDocNo(Sm.GetParameter("SSPCodeForEmployment")));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "GrdLvlName", 
                    
                    //6-10
                    "JoinDt", "TGDt", "PTKP", "NPWP", "Salary", 
                    
                    //11-15
                    "TaxableFixedAllowance", "SSEmployerHealth", "SSEmployerEmployment", "SSEmployeeEmployment", "PayrunPeriodDesc"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpRHA1()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            GrdLvlName = Sm.DrStr(dr, c[5]),
                            JoinDt = Sm.DrStr(dr, c[6]),
                            TGDt = Sm.DrStr(dr, c[7]),
                            PTKP = Sm.DrStr(dr, c[8]),
                            NPWP = Sm.DrStr(dr, c[9]),
                            Salary = Sm.DrDec(dr, c[10]),
                            TaxableFixedAllowance = Sm.DrDec(dr, c[11]),
                            SSEmployerHealth = Sm.DrDec(dr, c[12]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[13]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[14]),
                            PayrunPeriodDesc = Sm.DrStr(dr, c[15]),
                            Tax = 0m,
                            Amt = 0m,
                            Brutto = 0m,
                            Annualized = 0m,
                            FunctionalExpensesNo1 = 0m,
                            Netto1 = 0m,
                            PTKP1 = 0m,
                            PPH21Annual1 = 0m,
                            FunctionalExpensesNo2 = 0m,
                            Netto2 = 0m,
                            PTKP2 = 0m,
                            PPH21Annual2 = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1_2(ref List<EmpRHA1> l)
        {
            double multiplier = Math.Pow(10, 0);
            decimal  
                Value = 0m, 
                TaxTemp = 0m, Amt2Temp = 0m;
            DateTime Dt1, Dt2;
            string 
                NTI = string.Empty,
                HolidayYr = Sm.Left(mHolidayDt, 4),
                HolidayMth = mHolidayDt.Substring(4, 2);
            
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].NPWP.Length > 0 && l[i].PTKP.Length > 0)
                {
                    NTI = l[i].PTKP;
                    l[i].Brutto =
                        l[i].Salary +
                        //l[i].TaxableFixedAllowance +
                        l[i].SSEmployerHealth +
                        l[i].SSEmployerEmployment -
                        l[i].SSEmployeeEmployment;


                    l[i].Annualized = 12m * l[i].Brutto;

                    //201706, 201702
                    //201706, 201606
                    //201706, 201605
                    //201706, 201607
                    //201706, 201608
                    
                    //Value =
                    //    (decimal.Parse(HolidayMth)-decimal.Parse(l[i].JoinDt.Substring(4, 2))) +
                    //    (12*(decimal.Parse(HolidayYr)-decimal.Parse(Sm.Left(l[i].JoinDt, 4))));
                    
                    //if (Value+1m<12m) l[i].Annualized = ((Value+1m)/12m) * l[i].Brutto;
                    
                    l[i].FunctionalExpensesNo1 = (l[i].Annualized * (mFunctionalExpenses / 100m));
                    if (l[i].FunctionalExpensesNo1 > mFunctionalExpensesMaxAmt*12)
                        l[i].FunctionalExpensesNo1 = mFunctionalExpensesMaxAmt*12;

                    l[i].Netto1 = l[i].Annualized - l[i].FunctionalExpensesNo1;

                    foreach (var x in mlNTI.Where(x => x.Status == NTI))
                        l[i].PTKP1 = x.Amt;

                    if (l[i].Netto1 > l[i].PTKP1)
                        l[i].PPH21Annual1 = l[i].Netto1 - l[i].PTKP1;
                    else
                        l[i].PPH21Annual1 = 0m;

                    if (l[i].PPH21Annual1 > 0m && mlTI.Count > 0)
                    {
                        TaxTemp = l[i].PPH21Annual1;
                        Amt2Temp = 0m;
                        l[i].PPH21Annual1 = 0m;

                        foreach (TI t in mlTI.OrderBy(x => x.SeqNo))
                        {
                            if (TaxTemp > 0m)
                            {
                                if (TaxTemp <= (t.Amt2 - Amt2Temp))
                                {
                                    l[i].PPH21Annual1 += (TaxTemp * t.TaxRate / 100);
                                    TaxTemp = 0m;
                                }
                                else
                                {
                                    l[i].PPH21Annual1 += ((t.Amt2 - Amt2Temp) * t.TaxRate / 100);
                                    TaxTemp -= (t.Amt2 - Amt2Temp);
                                }
                            }
                            Amt2Temp = t.Amt2;
                        }
                    }

                    l[i].FunctionalExpensesNo2 = ((l[i].Annualized + l[i].Salary) * (mFunctionalExpenses / 100m));
                    if (l[i].FunctionalExpensesNo2 > mFunctionalExpensesMaxAmt*12)
                        l[i].FunctionalExpensesNo2 = mFunctionalExpensesMaxAmt*12;

                    l[i].Netto2 = l[i].Annualized + l[i].Salary - l[i].FunctionalExpensesNo2;

                    foreach (var x in mlNTI.Where(x => x.Status == NTI))
                        l[i].PTKP2 = x.Amt;

                    if (l[i].Netto2 > l[i].PTKP2)
                        l[i].PPH21Annual2 = l[i].Netto2 - l[i].PTKP2;
                    else
                        l[i].PPH21Annual2 = 0m;

                    if (l[i].PPH21Annual2 > 0m && mlTI.Count > 0)
                    {
                        TaxTemp = l[i].PPH21Annual2;
                        Amt2Temp = 0m;
                        l[i].PPH21Annual2 = 0m;

                        foreach (TI t in mlTI.OrderBy(x => x.SeqNo))
                        {
                            if (TaxTemp > 0m)
                            {
                                if (TaxTemp <= (t.Amt2 - Amt2Temp))
                                {
                                    l[i].PPH21Annual2 += (TaxTemp * t.TaxRate / 100);
                                    TaxTemp = 0m;
                                }
                                else
                                {
                                    l[i].PPH21Annual2 += ((t.Amt2 - Amt2Temp) * t.TaxRate / 100);
                                    TaxTemp -= (t.Amt2 - Amt2Temp);
                                }
                            }
                            Amt2Temp = t.Amt2;
                        }
                    }

                }

                l[i].Tax = l[i].PPH21Annual2 - l[i].PPH21Annual1;

                //Value =
                //    (decimal.Parse(HolidayMth) - decimal.Parse(l[i].JoinDt.Substring(4, 2))) +
                //    (12 * (decimal.Parse(HolidayYr) - decimal.Parse(Sm.Left(l[i].JoinDt, 4))));

                //if (Value + 1m < 12m) l[i].Salary = ((Value + 1m) / 12m) * l[i].Salary;

                Dt1 = new DateTime(
                        Int32.Parse(l[i].TGDt.Substring(0, 4)),
                        Int32.Parse(l[i].TGDt.Substring(4, 2)),
                        Int32.Parse(l[i].TGDt.Substring(6, 2)),
                        0, 0, 0
                        );
                Dt2 = new DateTime(
                    Int32.Parse(mHolidayDt.Substring(0, 4)),
                    Int32.Parse(mHolidayDt.Substring(4, 2)),
                    Int32.Parse(mHolidayDt.Substring(6, 2)),
                    0, 0, 0
                    );

                Value = (decimal)(Math.Ceiling((Dt2 - Dt1).Days / 30m));
                if (Value< 12m) l[i].Salary = decimal.Round(((Value) / 12m) * l[i].Salary, 2);
                
                l[i].Amt = l[i].Salary - l[i].Tax;
                if (l[i].Amt < 0m) l[i].Amt = 0m;
            }
        }

        private void Process1_3(ref List<EmpRHA1> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[2].Value = l[i].EmpCode;
                r.Cells[3].Value = l[i].EmpName;
                r.Cells[4].Value = l[i].EmpCodeOld;
                r.Cells[5].Value = l[i].PosName;
                r.Cells[6].Value = l[i].GrdLvlName;
                r.Cells[7].Value = l[i].Salary;
                r.Cells[8].Value = l[i].Tax;
                r.Cells[9].Value = l[i].Amt;
                r.Cells[10].Value = l[i].PayrunPeriodDesc;
                r.Cells[11].Value = l[i].DeptName;
            }
            Grd1.EndUpdate();
        }

        private string GetDocNo(string Param)
        {
            var Value = Sm.GetValue(
                "Select DocNo From TblEmpSSListHdr " +
                "Where CancelInd='N' And SSPCode=@Param " +
                "Order By Yr Desc, Mth Desc Limit 1;", Param
                );
            if (Value.Length == 0) Value = "X";
            return Value;
        }

        #endregion

        #region KMI

        private void ShowData2()
        {
            Sm.ClearGrd(Grd1, false);
            var l = new List<EmpRHA2>();
            try
            {
                Process2_1(ref l);
                Process2_2(ref l);
                Process2_3(ref l);

                if (l.Count <= 0) Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void Process2_1(ref List<EmpRHA2> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mFrmParent.Grd1.Rows.Count; i++)
            {
                EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, i, 1);
                if (EmpCode.Length > 0)
                {
                    if (Filter.Length > 0) Filter += " And ";
                    Filter += " (A.EmpCode<>@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length != 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=0 ";

            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "A.PayrunPeriod", true);

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, H.DeptName, E.GrdLvlName, G.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.JoinDt, A.PTKP, A.NPWP, ");
            SQL.AppendLine("IfNull(C.Amt, 0)+IfNull(D.Amt, 0) As Salary, ");
            SQL.AppendLine("IfNull(F.Salary, 0)+");
            SQL.AppendLine("IfNull(F.TaxableFixAllowance, 0)+");
            SQL.AppendLine("IfNull(F.FieldAssignment, 0)+");
            SQL.AppendLine("IfNull(F.Meal, 0)+");
            SQL.AppendLine("IfNull(F.Transport, 0)+");
            SQL.AppendLine("IfNull(F.IncProduction, 0)+");
            SQL.AppendLine("IfNull(F.OTTotalAmt, 0)+");
            SQL.AppendLine("IfNull(F.SalaryAdjustment, 0) ");
            SQL.AppendLine("As Brutto ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            //SQL.AppendLine("Left Join TblEmployeeSalary C On A.EmpCode=C.EmpCode And C.ActInd='Y' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select EmpCode, Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("    Where Find_In_Set(IfNull(ADCode, ''), @ADCodeSalary) ");
            SQL.AppendLine("    And EmpCode In ( ");
            SQL.AppendLine("        Select EmpCode From TblEmployee ");
            SQL.AppendLine("        Where ((ResignDt Is Not Null And ResignDt>=@CurrentDate) Or ResignDt Is Null) ");
            SQL.AppendLine("        And EmpCode In (Select EmpCode From TblPayrollProcess1 Where PayrunCode=@PayrunCode) ");
            SQL.AppendLine(Filter.Replace("A.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Inner Join TblPayrollProcess1 F On F.PayrunCode=@PayrunCode And A.EmpCode=F.EmpCode ");
            SQL.AppendLine("Left Join TblOption G On A.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblDepartment H On A.DeptCode=H.DeptCode ");
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@CurrentDate) Or A.ResignDt Is Null) ");
            
            SQL.AppendLine(Filter);
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            Sm.CmParam<String>(ref cm, "@ADCodeSalary", mADCodeSalary);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "GrdLvlName", 
                    
                    //6-10
                    "PayrunPeriodDesc", "JoinDt", "PTKP", "NPWP", "Salary", 
                    
                    //11
                    "Brutto"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpRHA2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            GrdLvlName = Sm.DrStr(dr, c[5]),
                            PayrunPeriodDesc = Sm.DrStr(dr, c[6]),
                            JoinDt = Sm.DrStr(dr, c[7]),
                            PTKP = Sm.DrStr(dr, c[8]),
                            NPWP = Sm.DrStr(dr, c[9]),
                            Salary = Sm.DrDec(dr, c[10]),
                            Brutto = Sm.DrDec(dr, c[11]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2_2(ref List<EmpRHA2> l)
        {
            decimal
                Tax = 0m,
                NTIAmt = 0m,
                FunctionalExpenses = 0m,
                Amt = 0m,
                JoinYr = 0m,
                TaxTemp = 0m,
                Amt2Temp = 0m,

                THR = 0m,
                BruttoIn1Yr = 0m,
                FunctionalExpenses2 = 0m,
                Netto = 0m,
                PPH21In1Yr = 0m,
                PTKP = 0m
                ;
            string NTI = string.Empty;
            bool IsFullYr = true;
            for (int i = 0; i < l.Count; i++)
            {
                Tax = 0m;
                NTIAmt = 0m;
                FunctionalExpenses = 0m;
                Amt = 0m;
                JoinYr = 0m;
                TaxTemp = 0m;
                Amt2Temp = 0m;

                THR = 0m;
                BruttoIn1Yr = 0m;
                FunctionalExpenses2 = 0m;
                Netto = 0m;
                PPH21In1Yr = 0m;
                PTKP = 0m;

                if (l[i].PTKP.Length > 0 && l[i].PayrunCode.Length>0)
                {
                    NTI = l[i].PTKP;
                    IsFullYr = true;

                    Amt =
                        l[i].PayrunSalary +
                        l[i].TaxableFixAllowance +
                        l[i].FieldAssignment +
                        l[i].Meal +
                        l[i].Transport +
                        l[i].IncProduction +
                        l[i].OT1Amt +
                        l[i].OT2Amt +
                        l[i].OTHolidayAmt +
                        l[i].SalaryAdjustment;

                    Amt = Amt + ((l[i].PayrunSalary + l[i].TaxableFixAllowance) * mSSEmploymentErPerc * 0.01m);

                    BruttoIn1Yr = Amt * 12;

                    FunctionalExpenses = (Amt * (mFunctionalExpenses / 100m));
                    if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                        FunctionalExpenses = mFunctionalExpensesMaxAmt;

                    Tax =
                        Amt -
                        FunctionalExpenses -
                        ((l[i].PayrunSalary + l[i].TaxableFixAllowance) * 0.01m * (mSSOldAgeEePerc + mSSPensionEePerc));


                    if (l[i].JoinDt.Length >= 4)
                        JoinYr = Decimal.Parse(Sm.Left(l[i].JoinDt, 4));

                    if (JoinYr < Decimal.Parse(Sm.Left(l[i].PayrunCode, 4)))
                        Tax = Tax * 12m;
                    else
                    {
                        if (Sm.CompareStr(Sm.Left(l[i].JoinDt, 4), Sm.Left(l[i].PayrunCode, 4)))
                        {
                            Tax = Tax * (12m - decimal.Parse(l[i].JoinDt.Substring(4, 2)) + 1);
                            IsFullYr = false;
                        }
                    }

                    foreach (var x in mlNTI.Where(x => x.Status == NTI))
                    {
                        NTIAmt = x.Amt;
                        PTKP = x.Amt;
                    }
                    if (Tax > NTIAmt)
                        Tax -= NTIAmt;
                    else
                        Tax = 0m;

                    Tax = Tax % 1000 >= 500 ? Tax + 1000 - Tax % 1000 : Tax - Tax % 1000;

                    if (Tax > 0 && mlTI.Count > 0)
                    {
                        TaxTemp = Tax;
                        Amt2Temp = 0m;
                        Tax = 0m;

                        foreach (TI t in mlTI.OrderBy(x => x.SeqNo))
                        {
                            if (TaxTemp > 0m)
                            {
                                if (TaxTemp <= (t.Amt2 - Amt2Temp))
                                {
                                    Tax += (TaxTemp * t.TaxRate / 100);
                                    TaxTemp = 0m;
                                }
                                else
                                {
                                    Tax += ((t.Amt2 - Amt2Temp) * t.TaxRate / 100);
                                    TaxTemp -= (t.Amt2 - Amt2Temp);
                                }
                            }
                            Amt2Temp = t.Amt2;
                        }
                        PPH21In1Yr = Tax;
                        Tax = Tax / (IsFullYr ? 12m : (12m - decimal.Parse(l[i].JoinDt.Substring(4, 2)) + 1));
                        if (l[i].NPWP.Length == 0)
                            Tax = Tax * (mNonNPWPTaxPercentage / 100);
                    }

                    if (Tax <= 0) Tax = 0m;
                    l[i].Tax = Tax;

                    THR = l[i].PayrunSalary + l[i].TaxableFixAllowance;

                    FunctionalExpenses2 = ((THR + BruttoIn1Yr) * (mFunctionalExpenses / 100m));
                    
                    Netto =
                        THR +
                        BruttoIn1Yr -
                        FunctionalExpenses2 -
                        (12m * (l[i].PayrunSalary + l[i].TaxableFixAllowance) * 0.01m * (mSSOldAgeEePerc + mSSPensionEePerc))
                        ;


                    foreach (var x in mlNTI.Where(x => x.Status == NTI))
                    {
                        NTIAmt = x.Amt;
                    }
                    if (Netto > NTIAmt)
                        Netto -= NTIAmt;
                    else
                        Netto = 0m;

                    Netto = Netto % 1000 >= 500 ? Netto + 1000 - Netto % 1000 : Netto - Netto % 1000;

                    if (Netto > 0 && mlTI.Count > 0)
                    {
                        TaxTemp = Netto;
                        Amt2Temp = 0m;
                        Netto = 0m;

                        foreach (TI t in mlTI.OrderBy(x => x.SeqNo))
                        {
                            if (TaxTemp > 0m)
                            {
                                if (TaxTemp <= (t.Amt2 - Amt2Temp))
                                {
                                    Netto += (TaxTemp * t.TaxRate / 100);
                                    TaxTemp = 0m;
                                }
                                else
                                {
                                    Netto += ((t.Amt2 - Amt2Temp) * t.TaxRate / 100);
                                    TaxTemp -= (t.Amt2 - Amt2Temp);
                                }
                            }
                            Amt2Temp = t.Amt2;
                        }
                        Netto -= PPH21In1Yr;
                        //Netto = Netto / (IsFullYr ? 12m : (12m - decimal.Parse(l[i].JoinDt.Substring(4, 2)) + 1));
                        if (l[i].NPWP.Length == 0)
                            Netto = Netto * (mNonNPWPTaxPercentage / 100);
                    }
                    l[i].Tax += Netto;
                }
            }
        }

        private void Process2_3(ref List<EmpRHA2> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[2].Value = l[i].EmpCode;
                r.Cells[3].Value = l[i].EmpName;
                r.Cells[4].Value = l[i].EmpCodeOld;
                r.Cells[5].Value = l[i].PosName;
                r.Cells[6].Value = l[i].GrdLvlName;
                r.Cells[7].Value = l[i].Salary;
                r.Cells[8].Value = l[i].Tax;
                r.Cells[9].Value = l[i].Amt;
                r.Cells[10].Value = l[i].PayrunPeriodDesc;
                r.Cells[11].Value = l[i].DeptName;
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }


        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }
      
        #endregion
       
        #endregion

        #region Class

        private class EmpRHA1
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string GrdLvlName { get; set; }
            public string PayrunPeriodDesc { get; set; }
            public string JoinDt { get; set; }
            public string TGDt { get; set; }
            public string PTKP { get; set; }
            public string NPWP { get; set; }
            public decimal Salary { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public decimal TaxableFixedAllowance { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }

            public decimal Brutto { get; set; }
            public decimal Annualized { get; set; }

            //Tanpa THR
            public decimal FunctionalExpensesNo1 { get; set; }
            public decimal Netto1 { get; set; }
            public decimal PTKP1 { get; set; }
            public decimal PPH21Annual1 { get; set; }

            //Dengan THR
            public decimal FunctionalExpensesNo2 { get; set; }
            public decimal Netto2 { get; set; }
            public decimal PTKP2 { get; set; }
            public decimal PPH21Annual2 { get; set; }            
            
        }

        private class EmpRHA2
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string GrdLvlName { get; set; }
            public string PayrunPeriodDesc { get; set; }
            public string JoinDt { get; set; }
            public string PTKP { get; set; }
            public string NPWP { get; set; }
            public decimal Salary { get; set; }
            public decimal Brutto { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }

            public string PayrunCode { get; set; }
            public decimal PayrunSalary { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal IncProduction { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion
    }
}
