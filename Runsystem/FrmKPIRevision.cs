﻿#region Update
/*
    23/08/2017 [HAR] tambah source KPI, edit cuma bisa menonaktifkan document
    19/12/2017 [HAR] tambah info site berdasarkan parameter isfilterbysite
    27/03/2018 [HAR] responsibility wajib di isi, jobdesc di hide
    23/04/2018 [HAR] target bulanan bisa di isi minus
    23/08/2018 [WED] KPIDtl tambah ControlInd, untuk keperluan rumus di Performance Review. ControlInd == "N", makin besar makin baik. ControlInd == "Y", makin kecil makin baik
    18/01/2019 [HAR] KPIDtl tidak bisa di delete jika pake source
    21/01/2019 [WED] Validasi Result name tidak boleh sama / duplicate
    22/01/2019 [HAR] tambah inputan year mandatory 
    20/02/2019 [MEY] menu baru master KPI Revision
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmKPIRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmKPIRevisionFind FrmFind;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmKPIRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "KPI";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetLueOption(ref LueOption);
                SetLueType(ref LueType);
                SetLueKPIParentCode(ref LueParentKPI);
                SetFormControl(mState.View);
                LueOption.Visible = false;
                LueType.Visible = false;
                LueJobDesc.Visible = false;
                Sl.SetLuePosCode(ref LuePosCode);
                if (mIsFilterBySite)
                {
                    LblSiteCode.ForeColor = Color.Red;
                }
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, 0);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd1, 0, 0, "", 2, 0);
            SetGrdHdr(ref Grd1, 0, 1, "Result" + Environment.NewLine + "Indicator", 2, 250);
            SetGrdHdr(ref Grd1, 0, 2, "Bobot", 2, 80);
            SetGrdHdr(ref Grd1, 0, 3, "Unit of Measurement", 2, 150);
            SetGrdHdr(ref Grd1, 0, 4, "Year / Month", 2, 100);
            SetGrdHdr(ref Grd1, 0, 5, "Target of Year", 2, 150);
            SetGrdHdr2(ref Grd1, 1, 6, "Month", 12);
            SetGrdHdr(ref Grd1, 0, 18, "Type", 2, 100);
            SetGrdHdr(ref Grd1, 0, 19, "Job Desc", 2, 250);
            SetGrdHdr(ref Grd1, 0, 20, "KPI Present DocNo", 2, 100);
            SetGrdHdr(ref Grd1, 0, 21, "KPI Present DNo", 2, 100);
            SetGrdHdr(ref Grd1, 0, 22, "KPI Present DTbl", 2, 100);
            SetGrdHdr(ref Grd1, 0, 23, "Control", 2, 80);
            SetGrdHdr(ref Grd1, 0, 24, "DNo", 2 , 50);
            Sm.GrdFormatDec(Grd1, new int[]{ 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 24, 19, 20, 21, 22 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 23 });

            Grd1.Cols[18].Move(5);
            Grd1.Cols[19].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 24 }, !ChkHideInfoInGrd.Checked);
        }


        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;
            
            SetGrdHdr(ref Grd1, 0, col, "01", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "02", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "03", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "04", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 4, "05", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 5, "06", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 6, "07", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 7, "08", 1, 80); 
            SetGrdHdr(ref Grd1, 0, col + 8, "09", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 9, "10", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 10, "11", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 11, "12", 1, 80);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtKPIName, TxtPICCode, TxtPICName, LueParentKPI, LuePosCode, LueSiteCode, MeeRemark,
                        TxtSourceKPI,LueYr, TxtKPIDocNo,
                    }, true);
                  
                    BtnPIC.Enabled = false;
                    BtnSourceKPI.Enabled = false;
                    Grd1.ReadOnly = true;
                 
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtKPIName, LueParentKPI, LuePosCode, LueSiteCode, MeeRemark, LueYr
                    }, false);
                  
                    BtnPIC.Enabled = true;
                    BtnSourceKPI.Enabled = true;
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                     // DteDocDt, TxtKPIName, LueParentKPI, LuePosCode, MeeRemark
                    }, false);
                 
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23 });
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
                    BtnPIC.Enabled = false;
                    BtnSourceKPI.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtKPIName, TxtPICCode, TxtPICName, LueParentKPI, LuePosCode, 
                LueYr, MeeRemark, TxtSourceKPI, LueSiteCode, TxtKPIDocNo,
            });
            ClearGrd();
            
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 5,6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmKPIRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);                       
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 23 && BtnSave.Enabled)
            {
                if (Grd1.Rows.Count > 0)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 23);
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, 23].Value = !IsSelected;
                }
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
           // Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (Grd1.SelectedRows.Count > 0 && TxtKPIDocNo.Text.Length == 0)
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                SetLueOption(ref LueOption);
                LueRequestEdit(Grd1, LueOption, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 19)
            {
                SetLueJobDesc(ref LueJobDesc);
                LueRequestEdit(Grd1, LueJobDesc, ref fCell, ref fAccept, e);
            }

            if (e.ColIndex == 18)
            {
                SetLueType(ref LueType);
                LueRequestEdit(Grd1, LueType, ref fCell, ref fAccept, e);
            }

            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex + 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            }
        }



        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = string.Concat(TxtKPIDocNo.Text, "/", GenerateDocNo());         
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveKPIRevisionHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveKPIRevisionDtl(DocNo, Row));
                    cml.Add(UpdateKPIDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo, 0);
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1 Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblKPIRevisionHdr ");
            //SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblKPIRevisionHdr ");
            SQL.AppendLine("       Where KPIDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.AppendLine("   ), '0001' ");
            SQL.AppendLine(") As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtKPIDocNo.Text);
        }
        private bool IsInsertedDataNotValid()
        {
            return
               Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsTxtEmpty(TxtKPIName, "KPI Name", false) ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC ", false)||
                Sm.IsLueEmpty(LuePosCode, "Responsibility")||
                //IsDocNoAlreadyProcess()||
                IsGrdEmpty() ||
                IsResultNameDuplicate() ||
                IsValidateSite();
        }

        private bool IsResultNameDuplicate()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if(Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    for (int j = i + 1; j < Grd1.Rows.Count; j++)
                    {
                        if(Sm.GetGrdStr(Grd1, j, 1).Length > 0)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 1) == Sm.GetGrdStr(Grd1, j, 1))
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate result indicator found at row #" + (j+1) + " : " + Sm.GetGrdStr(Grd1, j, 1));
                                Sm.FocusGrd(Grd1, j, 1);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsDocNoAlreadyProcess()
        {
    
            var cm = new MySqlCommand()
            {
                CommandText = "Select KPIDocNo From TblKPIProcessHdr Where KPIDocNo=@DocNo And CancelInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                return true;
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 city.");
                return true;
            }
            return false;
        }

        private bool IsValidateSite()
        {
            if (mIsFilterBySite && Sm.IsLueEmpty(LueSiteCode, "Site"))
            {
                return true;
            }
            return false;
        }

        private MySqlCommand SaveKPIRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKPIRevisionHdr(DocNo, KPIDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Values(@DocNo, @KPIDocNo, @UserCode, CurrentDateTime(), @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblKPIHdr Set KPIName = @KPIName, Yr = @Yr, PICCode = @PICCode, SiteCode=@SiteCode, KPIParent = @KPIParent, PosCode = @PosCode, ");
            SQL.AppendLine("Remark = @Remark,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where DocNo = @KPIDocNo;");

            //SQL.AppendLine("Insert Into TblKPIHdr(DocNo, DocDt, ActInd, Yr, KPIName, PICCode, KPIParent, PosCode, SiteCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @Yr, @KPIName, @PICCode, @KPIParent, @PosCode, @SiteCode, @Remark, @UserCode, CurrentDateTime()) ");
            ////region bisa edit hdr dan detail
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("    Update DocDt=@DocDt, ActInd=@ActInd, SiteCode=@SiteCode, KPIName=@KPIName, PICCode=@PICCode, KPIParent=@KPIParent, PosCode=@PosCode, Remark=@Remark, ");
            //SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            //SQL.AppendLine("Delete From TblKPIDtl Where DocNo = @DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@KPIDocNo", TxtKPIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@KPIName", TxtKPIName.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@KPIParent", Sm.GetLue(LueParentKPI));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveKPIRevisionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Insert Into TblKPIRevisionDtl(DocNo, DNo, KPIDocNo, KPIDNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @KPIDocNo, @KPIDNo, @UserCode, CurrentDateTime(), @UserCode, CurrentDateTime()); ");
   
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@KPIDocNo", TxtKPIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@KPIDNo", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateKPIDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblKPIDtl Set Result = @Result, bobot = @bobot, uom = @uom, option= @option, target = @target, mth1 = @mth1, ");
            SQL.AppendLine("mth2 = @mth2, mth3 = @mth3, mth4 = @mth4, mth5 = @mth5, mth6 = @mth6, mth7 = @mth7, mth8 = @mth8, mth9 = @mth9, mth10 = @mth10, mth11 = @mth11, mth12 = @mth12, type = @type, JobDesc = @JobDesc, KPIPerspectiveDocNo = @KPIPerspectiveDocNo, KPIPerspectiveDNo = @KPIPerspectiveDNo, KPIPerspectiveTblDtl = @KPIPerspectiveTblDtl, ControlInd = @ControlInd, CreateBy = @CreateBy, CreateDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @KPIDocNo And DNo = @KPIDNo; ");
            //SQL.AppendLine("Values(@DocNo, @DNo, @Result, @bobot, @uom, @option, @target, @mth1, ");
            //SQL.AppendLine("@mth2, @mth3, @mth4, @mth5, @mth6, @mth7, @mth8, @mth9, @mth10, @mth11, @mth12, @type, @JobDesc, @KPIPerspectiveDocNo, @KPIPerspectiveDNo, @KPIPerspectiveTblDtl, @ControlInd, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@KPIDocNo", TxtKPIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@KPIDNo", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@Result", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@bobot", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@uom", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@option", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@target", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@mth1", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@mth2", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@mth3", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@mth4", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@mth5", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@mth6", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@mth7", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@mth8", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@mth9", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@mth10", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@mth11", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@mth12", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@type", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@JobDesc", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveDocNo", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveDNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@KPIPerspectiveTblDtl", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@ControlInd", Sm.GetGrdBool(Grd1, Row, 23) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, int SourceNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                if (SourceNo == 0)
                {
                    ShowKPIRevisionHdr(DocNo);
                    ShowKPIRevisionDtl(DocNo);
                }
                else
                {
                    ShowKPIHdr(DocNo, SourceNo);
                    ShowKPIDtl(DocNo);
                }
           //     if (SourceNo != 0) Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (SourceNo == 0)
                {
                    SetFormControl(mState.View);
                }
                else
                {
                    SetFormControl(mState.Insert);
                }
                Cursor.Current = Cursors.Default;

            }
        }

        private void ShowKPIRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.KPIDocNo, B.Yr, B.KPIName, B.PICCode, ");
            SQL.AppendLine("C.EmpName, B.KPIParent, B.PosCode, B.SiteCode, B.Remark ");
            SQL.AppendLine("From TblKPIRevisionHdr A ");
            SQL.AppendLine("Inner Join TblKPIHdr B On A.KPIDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "KPIDocNo", "Yr", "KPIName", "PICCode", "EmpName",   
                    
                    //6-9
                    "KPIParent", "PosCode", "SiteCode", "Remark"
                    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtKPIDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                    TxtKPIName.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPICCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtPICName.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueParentKPI, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[8]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowKPIRevisionDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select B.* ");
            SQLDtl.AppendLine("From TblKPIRevisionDtl A ");
            SQLDtl.AppendLine("Inner Join TblKPIDtl B On A.KPIDocNo = B.DocNo And A.KPIDNo = B.DNo ");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Result", "bobot", "uom", "option", "target",
                    //6-10
                    "mth1", "mth2", "mth3", "mth4", "mth5", 
                    //11-15
                    "mth6", "mth7", "mth8", "mth9", "mth10", 
                    //16-20
                    "mth11", "mth12", "type", "JobDesc", "KPIPerspectiveDocNo", 
                    //21-23
                    "KPIPerspectiveDNo", "KPIPerspectiveTblDtl", "ControlInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 22);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 23, 23);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 0);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowKPIHdr(string DocNo, int SourceNO)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.ActInd, A.Yr, A.KPIName, A.PICCode,  B.EmpName, A.KPIParent,  A.PosCode, A.SiteCode,  A.Remark From TblKPIHdr  A "+
                    "Inner Join TblEmployee B On A.PICCode=B.EmpCode "+
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ActInd", "Yr", "KPIName", "PICCode",   
                        
                        //6-10
                        "EmpName", "KPIParent", "PosCode", "SiteCode", "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (SourceNO == 0)
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                          
                   
                        }
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        TxtKPIName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueParentKPI, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[8]));
                        Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[9]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ShowKPIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, Result, bobot, uom, option, target, mth1, ");
            SQL.AppendLine("mth2, mth3, mth4, mth5, mth6, mth7, mth8, mth9, mth10, mth11, mth12, type, ");
            SQL.AppendLine("JobDesc, KPIPerspectiveDocNo, KPIPerspectiveDNo, KPIPerspectiveTblDtl, ControlInd ");
            SQL.AppendLine("From TblKPIDtl ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Result", "bobot", "uom", "option", "target",
                    //6-10
                    "mth1", "mth2", "mth3", "mth4", "mth5", 
                    //11-15
                    "mth6", "mth7", "mth8", "mth9", "mth10", 
                    //16-20
                    "mth11", "mth12", "type", "JobDesc", "KPIPerspectiveDocNo", 
                    //21-23
                    "KPIPerspectiveDNo", "KPIPerspectiveTblDtl", "ControlInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 19); 
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 22);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 23, 23);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 0);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowKPIJobDesc()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT Docno, DNo, '1' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '2' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl2 ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '3' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl3 ");
            SQL.AppendLine("Where PosCode = @PosCode ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("SELECT Docno, DNo, '4' As DtlInd, Initiative ");
            SQL.AppendLine("FROM tblkpiperspectivedtl4 ");
            SQL.AppendLine("Where PosCode = @PosCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "Dno", "DtlInd", "Initiative"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 3);
                    Grd1.Cells[Row, 2].Value = 0;
                    Grd1.Cells[Row, 5].Value = 0;
                    Grd1.Cells[Row, 6].Value = 0;
                    Grd1.Cells[Row, 7].Value = 0;
                    Grd1.Cells[Row, 8].Value = 0;
                    Grd1.Cells[Row, 9].Value = 0;
                    Grd1.Cells[Row, 10].Value = 0;
                    Grd1.Cells[Row, 11].Value = 0;
                    Grd1.Cells[Row, 12].Value = 0;
                    Grd1.Cells[Row, 13].Value = 0;
                    Grd1.Cells[Row, 14].Value = 0;
                    Grd1.Cells[Row, 15].Value = 0;
                    Grd1.Cells[Row, 16].Value = 0;
                    Grd1.Cells[Row, 17].Value = 0;
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        private void SetLueJobDesc(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "SELECT B.Description As Col1 "+
                "FROM tblorganizationalstructure A "+
                "Inner Join tblorganizationalstructuredtl2 B On A.PosCode = B.PosCode "+
                "Where A.PosCode = '"+Sm.GetLue(LuePosCode)+"' ", "Job Desc");
        }


        private void SetLueOption(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select Col1 From ( "+
                "Select 'Year' As Col1 "+
                "union all "+
                "Select 'Month' " +
                ")X ",
                "Name");
        }


        private void SetLueType(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select Col1 From ( " +
                "Select 'SUM' As Col1 " +
                "union all " +
                "Select 'AVG' " +
                ")X ",
                "Name");
        }

        public static void SetLueKPIParentCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DocNo As Col1, KPIName As Col2 From TblKPIHdr Where ActInd = 'Y' Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        
        #endregion

        #region Event

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueOption);
            }
        }

        private void LueOption_Validated(object sender, EventArgs e)
        {
            LueOption.Visible = false;
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOption));
        }


        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 18)
            {
                if (Sm.GetLue(LueType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueType);
            }
        }

        private void LueType_Validated(object sender, EventArgs e)
        {
            LueType.Visible = false;
        }

       
        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmKPIRevisionDlg(this));
        }

        private void LueParentKPI_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParentKPI, new Sm.RefreshLue1(SetLueKPIParentCode));
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
            {
                Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
                if (Sm.GetLue(LuePosCode).Length > 0) ShowKPIJobDesc();
            }
        }

        private void LueJobDesc_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueJobDesc, new Sm.RefreshLue1(SetLueJobDesc));
        }

        private void LueJobDesc_Leave(object sender, EventArgs e)
        {
            if (LueJobDesc.Visible && fAccept && fCell.ColIndex == 19)
            {
                if (Sm.GetLue(LueJobDesc).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueJobDesc);
            }
            LueJobDesc.Visible = false;
        }

        private void BtnSourceKPI_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmKPIRevisionDlg2(this));
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        #endregion    

      
     
    }
}
