﻿#region Update
/*
    05/11/2021 [IBL/PRODUCT] Menu baru Reporting News Dialog
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptNewsDlg : RunSystem.FrmBase8
    {
        #region Field

        private FrmRptNews mFrmParent;
        private string mFileName = string.Empty;
        private byte[] mImageByte = null;

        #endregion

        #region Constructor

        public FrmRptNewsDlg(FrmRptNews FrmParent, string FileName, byte[] image)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mFileName = FileName;
            mImageByte = image;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "Photo";
                BtnAdd.Visible = false;
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Additional Method

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        #endregion

        #region Show Data

        private void ShowData()
        {
            TxtPhoto.EditValue = mFileName;
            if (mImageByte != null && mImageByte.Length > 0)
                PicBoxContent.Image = ByteToImage(mImageByte);
        }

        #endregion

        #endregion
    }
}
