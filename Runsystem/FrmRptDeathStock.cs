﻿#region Update
/*
    06/07/2017 [TKG] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDeathStock : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptDeathStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",
                        "Item's Code", 
                        "Item's Name", 
                        "Vendor",
                        "Quantity",
                        
                        //6-10
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 200, 200, 100, 
                        
                        //6-10
                        80, 100, 80, 100, 80
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 9 }, 0);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            string v = Sm.GetParameter("NumberOfInventoryUomCode");
            int NumberOfInventoryUomCode = 1;
            if (v.Length != 0) NumberOfInventoryUomCode = int.Parse(v);

            if (NumberOfInventoryUomCode == 1)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10 }, false);

            if (NumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 }, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            
            Cursor.Current = Cursors.WaitCursor;

            var cm = new MySqlCommand();
            string Filter1 = " ", Filter2 = string.Empty;
            var SQL = new StringBuilder();

            if (ChkItCode.Checked)
                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

            if (ChkVdCode.Checked)
            {
                Filter2 = " And C.VdCode=@VdCode ";
                Sm.CmParam<string>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            }

            SQL.AppendLine("Select T4.OptDesc, T1.ItCode, T2.ItName, T3.VdName, ");
            SQL.AppendLine("T1.Qty, T2.InventoryUomCode, ");
            SQL.AppendLine("T1.Qty2, T2.InventoryUomCode2, ");
            SQL.AppendLine("T1.Qty3, T2.InventoryUomCode3 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.ItCode, C.VdCode, Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl B On A.Source=B.Source ");
            SQL.AppendLine("    Inner Join TblRecvVdHdr C On B.DocNo=C.DocNo ");
            SQL.AppendLine("        And C.DocDt>Replace(Date_Add(CurDate(), Interval -6 Month), '-', '') ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        And D.Information1 Is Not Null ");
            SQL.AppendLine("        And Find_In_Set(D.Information1, (Select ParValue From TblParameter Where ParCode='RptDeathStockItInformation1' And ParValue Is Not Null))>0 ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Where A.Qty>0 ");
            SQL.AppendLine("    And Find_In_Set(A.WhsCode, (Select ParValue From TblParameter Where ParCode='RptDeathStockWhsCode' And ParValue Is Not Null))>0 ");
            SQL.AppendLine("    Group By A.ItCode, C.VdCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode=T2.ItCode ");
            SQL.AppendLine("Inner Join TblVendor T3 On T1.VdCode=T3.VdCode ");
            SQL.AppendLine("Inner Join TblOption T4 On T2.Information1=T4.OptCode And T4.OptCat='ItemInformation1' ");
            SQL.AppendLine("Order By T4.OptDesc, T2.ItName, T2.ItCode, T3.VdName; ");

            try
            {
                Sm.ShowDataInGrid(
                     ref Grd1, ref cm, SQL.ToString(),
                     new string[]
                        {
                            //0
                            "OptDesc", 

                            //1-5
                            "ItCode", "ItName", "VdName", "Qty", "InventoryUomCode", 
                            
                            //6-9
                            "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3"
                        },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Grd.Cells[Row, 0].Value = Row + 1;
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                     }, true, true, false, false
                 );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #endregion
    }
}
